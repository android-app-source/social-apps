.class public Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;
.super LX/CnR;
.source ""

# interfaces
.implements LX/Cjr;
.implements LX/CnL;
.implements LX/CnP;
.implements LX/CnQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnR;",
        "LX/Cjr;",
        "LX/CnL;",
        "LX/CnP;",
        "LX/CnQ",
        "<",
        "LX/ClW;",
        ">;"
    }
.end annotation


# static fields
.field private static final L:Ljava/util/Set;

.field private static final M:Ljava/util/Set;

.field private static final N:Ljava/util/Set;

.field public static final r:Ljava/lang/String;

.field private static final u:Lcom/facebook/graphql/model/GraphQLFeedback;


# instance fields
.field private A:Z

.field private B:LX/0wd;

.field private C:Z

.field private D:Z

.field private E:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field public F:Lcom/facebook/graphql/model/GraphQLFeedback;

.field private G:LX/Cly;

.field private H:LX/CmD;

.field private I:LX/CmQ;

.field private J:Ljava/lang/String;

.field private final K:LX/K2J;

.field public O:Ljava/lang/String;

.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/20K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/20w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/20h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CjE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/K1t;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CoV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20j;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/20i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/K1u;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/CqV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/K2Y;

.field private final t:LX/K2I;

.field public v:LX/ClW;

.field private w:LX/CnN;

.field private x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

.field private y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2763290
    const-class v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->r:Ljava/lang/String;

    .line 2763291
    invoke-static {}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->getEmptyFeedback()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2763292
    sget-object v0, LX/20X;->LIKE:LX/20X;

    sget-object v1, LX/20X;->COMMENT:LX/20X;

    sget-object v2, LX/20X;->SHARE:LX/20X;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->L:Ljava/util/Set;

    .line 2763293
    sget-object v0, LX/20X;->LIKE:LX/20X;

    sget-object v1, LX/20X;->SHARE:LX/20X;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->M:Ljava/util/Set;

    .line 2763294
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2763295
    sput-object v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->N:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2763296
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2763297
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2763298
    invoke-direct {p0, p1, p2}, LX/CnR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2763299
    new-instance v0, LX/K2I;

    invoke-direct {v0, p0}, LX/K2I;-><init>(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->t:LX/K2I;

    .line 2763300
    iput-boolean v6, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->A:Z

    .line 2763301
    iput-boolean v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->D:Z

    .line 2763302
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->E:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2763303
    new-instance v0, LX/K2J;

    invoke-direct {v0, p0}, LX/K2J;-><init>(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->K:LX/K2J;

    .line 2763304
    const-class v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2763305
    const v0, 0x7f030120

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2763306
    const v0, 0x7f0d05cc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    .line 2763307
    const v0, 0x7f0d05cb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 2763308
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2763309
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->b:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2763310
    invoke-virtual {p0, v6}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->setLayoutDirection(I)V

    .line 2763311
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->e:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4026000000000000L    # 11.0

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 2763312
    iput-boolean v6, v0, LX/0wd;->c:Z

    .line 2763313
    move-object v0, v0

    .line 2763314
    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->B:LX/0wd;

    .line 2763315
    new-instance v0, LX/K2Y;

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a062c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, LX/K2Y;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->s:LX/K2Y;

    .line 2763316
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->O:Ljava/lang/String;

    .line 2763317
    return-void

    .line 2763318
    :cond_1
    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->setLayoutDirection(I)V

    goto :goto_0
.end method

.method private static a(LX/CnN;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 3

    .prologue
    .line 2763319
    new-instance v0, LX/170;

    invoke-direct {v0}, LX/170;-><init>()V

    .line 2763320
    iget-object v1, p0, LX/CnN;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2763321
    iput-object v1, v0, LX/170;->o:Ljava/lang/String;

    .line 2763322
    move-object v0, v0

    .line 2763323
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2763324
    iget v2, p0, LX/CnN;->f:I

    move v2, v2

    .line 2763325
    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2763326
    iput-object v1, v0, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2763327
    move-object v0, v0

    .line 2763328
    invoke-virtual {v0}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    .line 2763329
    sget-object v1, LX/21D;->INSTANT_ARTICLE:LX/21D;

    const-string v2, "richdocumentReactionsUfiView"

    invoke-static {v0}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v0

    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2763330
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->J:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2763331
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->J:Ljava/lang/String;

    .line 2763332
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->P()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;LX/03V;LX/Crz;LX/Chi;LX/20K;LX/0wW;LX/20w;LX/1zf;LX/20h;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/20i;LX/K1u;LX/CqV;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/Crz;",
            "LX/Chi;",
            "LX/20K;",
            "LX/0wW;",
            "LX/20w;",
            "LX/1zf;",
            "LX/20h;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CjE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/K1t;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CoV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/20j;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            "LX/K1u;",
            "LX/CqV;",
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2763333
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->a:LX/03V;

    iput-object p2, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->b:LX/Crz;

    iput-object p3, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->c:LX/Chi;

    iput-object p4, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->d:LX/20K;

    iput-object p5, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->e:LX/0wW;

    iput-object p6, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->f:LX/20w;

    iput-object p7, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->g:LX/1zf;

    iput-object p8, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->h:LX/20h;

    iput-object p9, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->i:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->j:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->k:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->l:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->m:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->n:LX/20i;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->o:LX/K1u;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->p:LX/CqV;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->q:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 20

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v18

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;

    invoke-static/range {v18 .. v18}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static/range {v18 .. v18}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v3

    check-cast v3, LX/Crz;

    invoke-static/range {v18 .. v18}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v4

    check-cast v4, LX/Chi;

    invoke-static/range {v18 .. v18}, LX/20K;->a(LX/0QB;)LX/20K;

    move-result-object v5

    check-cast v5, LX/20K;

    invoke-static/range {v18 .. v18}, LX/0wW;->a(LX/0QB;)LX/0wW;

    move-result-object v6

    check-cast v6, LX/0wW;

    const-class v7, LX/20w;

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/20w;

    invoke-static/range {v18 .. v18}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v8

    check-cast v8, LX/1zf;

    invoke-static/range {v18 .. v18}, LX/20h;->a(LX/0QB;)LX/20h;

    move-result-object v9

    check-cast v9, LX/20h;

    const/16 v10, 0x3be

    move-object/from16 v0, v18

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x31e6

    move-object/from16 v0, v18

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x3228

    move-object/from16 v0, v18

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x3239

    move-object/from16 v0, v18

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x475

    move-object/from16 v0, v18

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {v18 .. v18}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v15

    check-cast v15, LX/20i;

    invoke-static/range {v18 .. v18}, LX/K1u;->a(LX/0QB;)LX/K1u;

    move-result-object v16

    check-cast v16, LX/K1u;

    invoke-static/range {v18 .. v18}, LX/CqV;->a(LX/0QB;)LX/CqV;

    move-result-object v17

    check-cast v17, LX/CqV;

    const/16 v19, 0x3216

    invoke-static/range {v18 .. v19}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {v1 .. v18}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->a(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;LX/03V;LX/Crz;LX/Chi;LX/20K;LX/0wW;LX/20w;LX/1zf;LX/20h;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/20i;LX/K1u;LX/CqV;LX/0Ot;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2763334
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CjE;

    new-instance v1, LX/K2E;

    invoke-direct {v1, p0}, LX/K2E;-><init>(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V

    invoke-virtual {v0, p1, v1}, LX/CjE;->a(Ljava/lang/String;LX/2h1;)V

    .line 2763335
    return-void
.end method

.method public static a$redex0(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;LX/1zt;)V
    .locals 4

    .prologue
    .line 2763336
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v1

    invoke-static {v0, v1, p1}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;ILX/1zt;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    move-result-object v1

    .line 2763337
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20j;

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->n:LX/20i;

    invoke-virtual {v2}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2763338
    return-void
.end method

.method private f()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 2763339
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_0

    .line 2763340
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->g()V

    .line 2763341
    :goto_0
    return-void

    .line 2763342
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    if-eqz v0, :cond_1

    .line 2763343
    invoke-static {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->l(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V

    .line 2763344
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->t:LX/K2I;

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2763345
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    new-instance v1, LX/K2F;

    invoke-direct {v1, p0}, LX/K2F;-><init>(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setReactionsClickListener(Landroid/view/View$OnClickListener;)V

    .line 2763346
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->K:LX/K2J;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->B:LX/0wd;

    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->f:LX/20w;

    iget-object v5, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v6

    const-string v7, "native_article_story"

    invoke-virtual {v4, v5, v6, v7}, LX/20w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/20z;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->g:LX/1zf;

    sget-object v6, LX/20I;->LIGHT:LX/20I;

    iget-object v7, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->g:LX/1zf;

    iget-object v8, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/1zf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;Lcom/facebook/graphql/model/GraphQLFeedback;LX/21M;LX/0wd;LX/20z;LX/1zf;LX/20I;LX/0Px;LX/21H;)V

    .line 2763347
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->getFooterButtonOptions()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setButtons(Ljava/util/Set;)V

    .line 2763348
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    const/4 v1, 0x3

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setButtonWeights([F)V

    .line 2763349
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v0, v9}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setShowIcons(Z)V

    .line 2763350
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    sget-object v1, LX/1Wl;->VISIBLE:LX/1Wl;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setTopDividerStyle(LX/1Wl;)V

    .line 2763351
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->j()LX/20Z;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setOnButtonClickedListener(LX/20Z;)V

    .line 2763352
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2763353
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->h()V

    .line 2763354
    iput-boolean v9, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->C:Z

    goto/16 :goto_0

    .line 2763355
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2763441
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->getFooterButtonOptions()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setButtons(Ljava/util/Set;)V

    .line 2763442
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    const/4 v1, 0x3

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setButtonWeights([F)V

    .line 2763443
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setShowIcons(Z)V

    .line 2763444
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    sget-object v1, LX/1Wl;->VISIBLE:LX/1Wl;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setTopDividerStyle(LX/1Wl;)V

    .line 2763445
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setVisibility(I)V

    .line 2763446
    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private static getEmptyFeedback()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 1

    .prologue
    .line 2763356
    new-instance v0, LX/3dM;

    invoke-direct {v0}, LX/3dM;-><init>()V

    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    return-object v0
.end method

.method public static getFeedbackLoggingparams(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;
    .locals 2

    .prologue
    .line 2763428
    new-instance v0, LX/21A;

    invoke-direct {v0}, LX/21A;-><init>()V

    const-string v1, "native_article_story"

    .line 2763429
    iput-object v1, v0, LX/21A;->c:Ljava/lang/String;

    .line 2763430
    move-object v0, v0

    .line 2763431
    const-string v1, "instant_article_ufi"

    .line 2763432
    iput-object v1, v0, LX/21A;->b:Ljava/lang/String;

    .line 2763433
    move-object v0, v0

    .line 2763434
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->getTrackingCodes()LX/162;

    move-result-object v1

    .line 2763435
    iput-object v1, v0, LX/21A;->a:LX/162;

    .line 2763436
    move-object v0, v0

    .line 2763437
    sget-object v1, LX/21D;->INSTANT_ARTICLE:LX/21D;

    .line 2763438
    iput-object v1, v0, LX/21A;->i:LX/21D;

    .line 2763439
    move-object v0, v0

    .line 2763440
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    return-object v0
.end method

.method private getFooterButtonOptions()Ljava/util/Set;
    .locals 2

    .prologue
    .line 2763422
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->E:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-ne v0, v1, :cond_0

    .line 2763423
    sget-object v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->M:Ljava/util/Set;

    .line 2763424
    :goto_0
    return-object v0

    .line 2763425
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->E:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-ne v0, v1, :cond_1

    .line 2763426
    sget-object v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->L:Ljava/util/Set;

    goto :goto_0

    .line 2763427
    :cond_1
    sget-object v0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->N:Ljava/util/Set;

    goto :goto_0
.end method

.method private getTrackingCodes()LX/162;
    .locals 4

    .prologue
    .line 2763407
    const/4 v0, 0x0

    .line 2763408
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->c:LX/Chi;

    .line 2763409
    iget-object v2, v1, LX/Chi;->g:LX/0lF;

    move-object v1, v2

    .line 2763410
    instance-of v1, v1, LX/162;

    if-eqz v1, :cond_0

    .line 2763411
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->c:LX/Chi;

    .line 2763412
    iget-object v1, v0, LX/Chi;->g:LX/0lF;

    move-object v0, v1

    .line 2763413
    check-cast v0, LX/162;

    .line 2763414
    :cond_0
    if-nez v0, :cond_1

    .line 2763415
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 2763416
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 2763417
    const-string v2, "node_id"

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->c:LX/Chi;

    .line 2763418
    iget-object p0, v3, LX/Chi;->c:Ljava/lang/String;

    move-object v3, p0

    .line 2763419
    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2763420
    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 2763421
    :cond_1
    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2763405
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    new-instance v1, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl$3;

    invoke-direct {v1, p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl$3;-><init>(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->post(Ljava/lang/Runnable;)Z

    .line 2763406
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2763401
    iget-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->C:Z

    if-nez v0, :cond_0

    .line 2763402
    :goto_0
    return-void

    .line 2763403
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->d:LX/20K;

    invoke-static {v0, v1}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;LX/20K;)V

    .line 2763404
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->C:Z

    goto :goto_0
.end method

.method private j()LX/20Z;
    .locals 1

    .prologue
    .line 2763400
    new-instance v0, LX/K2G;

    invoke-direct {v0, p0}, LX/K2G;-><init>(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V

    return-object v0
.end method

.method public static k(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V
    .locals 4

    .prologue
    .line 2763218
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_0

    .line 2763219
    :goto_0
    return-void

    .line 2763220
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->g:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->e()LX/1zt;

    move-result-object v0

    .line 2763221
    :goto_1
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->K:LX/K2J;

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, LX/K2J;->a(Landroid/view/View;LX/1zt;LX/0Ve;)V

    .line 2763222
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->y:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setReaction(LX/1zt;)V

    goto :goto_0

    .line 2763223
    :cond_1
    sget-object v0, LX/1zt;->c:LX/1zt;

    goto :goto_1
.end method

.method public static l(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V
    .locals 4

    .prologue
    .line 2763389
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    if-nez v0, :cond_1

    .line 2763390
    :cond_0
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->m()V

    .line 2763391
    :goto_0
    return-void

    .line 2763392
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    .line 2763393
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    .line 2763394
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v2}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v2

    .line 2763395
    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    if-nez v0, :cond_2

    if-nez v1, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v3, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setVisibility(I)V

    .line 2763396
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v3}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setReactorsCount(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 2763397
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setCommentsCount(I)V

    .line 2763398
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSharesCount(I)V

    goto :goto_0

    .line 2763399
    :cond_3
    const/16 v0, 0x8

    goto :goto_1
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2763383
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    if-nez v0, :cond_0

    .line 2763384
    :goto_0
    return-void

    .line 2763385
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setVisibility(I)V

    .line 2763386
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    sget-object v1, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setReactorsCount(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 2763387
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setCommentsCount(I)V

    .line 2763388
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSharesCount(I)V

    goto :goto_0
.end method

.method public static n(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2763357
    invoke-static {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->getFeedbackLoggingparams(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    .line 2763358
    new-instance v1, LX/8qL;

    invoke-direct {v1}, LX/8qL;-><init>()V

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2763359
    iput-object v2, v1, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2763360
    move-object v1, v1

    .line 2763361
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    .line 2763362
    iput-object v2, v1, LX/8qL;->d:Ljava/lang/String;

    .line 2763363
    move-object v1, v1

    .line 2763364
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    .line 2763365
    iput-object v2, v1, LX/8qL;->e:Ljava/lang/String;

    .line 2763366
    move-object v1, v1

    .line 2763367
    const/4 v2, 0x1

    .line 2763368
    iput-boolean v2, v1, LX/8qL;->i:Z

    .line 2763369
    move-object v1, v1

    .line 2763370
    iput-boolean v3, v1, LX/8qL;->h:Z

    .line 2763371
    move-object v1, v1

    .line 2763372
    iput-boolean v3, v1, LX/8qL;->j:Z

    .line 2763373
    move-object v1, v1

    .line 2763374
    iput-object v0, v1, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2763375
    move-object v0, v1

    .line 2763376
    invoke-virtual {v0}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v2

    .line 2763377
    new-instance v0, LX/8qO;

    invoke-direct {v0}, LX/8qO;-><init>()V

    .line 2763378
    iput-boolean v3, v0, LX/8qO;->a:Z

    .line 2763379
    move-object v0, v0

    .line 2763380
    invoke-virtual {v0}, LX/8qO;->a()Lcom/facebook/ufiservices/flyout/PopoverParams;

    move-result-object v3

    .line 2763381
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K1t;

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->G:LX/Cly;

    iget-object v5, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->H:LX/CmD;

    iget-object v6, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->I:LX/CmQ;

    iget-object v7, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->b:LX/Crz;

    invoke-virtual {v7}, LX/Crz;->a()Z

    move-result v7

    invoke-virtual/range {v0 .. v7}, LX/K1t;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;LX/Cly;LX/CmD;LX/CmQ;Z)Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;

    .line 2763382
    return-void
.end method

.method public static o(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V
    .locals 4

    .prologue
    .line 2763261
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->o:LX/K1u;

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->getFeedbackLoggingparams(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/K1u;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 2763262
    return-void
.end method

.method public static p(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;)V
    .locals 5

    .prologue
    .line 2763263
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    if-nez v0, :cond_0

    .line 2763264
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2763265
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CoV;

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0, v3}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x3ec

    invoke-virtual {v0, v2, v1, v3, v4}, LX/CoV;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 2763266
    :goto_0
    return-void

    .line 2763267
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    .line 2763268
    iget-object v1, v0, LX/CnN;->b:Landroid/app/Activity;

    move-object v0, v1

    .line 2763269
    if-eqz v0, :cond_1

    .line 2763270
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    .line 2763271
    iget-object v2, v1, LX/CnN;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2763272
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    invoke-static {v2}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->a(LX/CnN;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    .line 2763273
    iget v4, v3, LX/CnN;->a:I

    move v3, v4

    .line 2763274
    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    .line 2763275
    iget-object p0, v4, LX/CnN;->b:Landroid/app/Activity;

    move-object v4, p0

    .line 2763276
    invoke-interface {v0, v1, v2, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_0

    .line 2763277
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    .line 2763278
    iget-object v1, v0, LX/CnN;->c:Landroid/support/v4/app/Fragment;

    move-object v0, v1

    .line 2763279
    if-eqz v0, :cond_2

    .line 2763280
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    .line 2763281
    iget-object v2, v1, LX/CnN;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2763282
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    invoke-static {v2}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->a(LX/CnN;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    .line 2763283
    iget v4, v3, LX/CnN;->a:I

    move v3, v4

    .line 2763284
    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    .line 2763285
    iget-object p0, v4, LX/CnN;->c:Landroid/support/v4/app/Fragment;

    move-object v4, p0

    .line 2763286
    invoke-interface {v0, v1, v2, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2763287
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    .line 2763288
    iget-object v2, v1, LX/CnN;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2763289
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    invoke-static {v2}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->a(LX/CnN;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static setFeedback(Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 1

    .prologue
    .line 2763204
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2763205
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->z:Z

    .line 2763206
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->f()V

    .line 2763207
    return-void
.end method

.method private setFeedbackOptions(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;)V
    .locals 0

    .prologue
    .line 2763229
    if-nez p1, :cond_0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    :cond_0
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->E:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2763230
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2763228
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2763224
    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2763225
    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->E:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2763226
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->setVisibility(I)V

    .line 2763227
    return-void
.end method

.method public final c()Landroid/view/View;
    .locals 0

    .prologue
    .line 2763217
    return-object p0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2763216
    const/4 v0, 0x1

    return v0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2763212
    invoke-super {p0, p1}, LX/CnR;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2763213
    iget-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->D:Z

    if-eqz v0, :cond_0

    .line 2763214
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->s:LX/K2Y;

    invoke-virtual {v0, p0, p1}, LX/K2Y;->a(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 2763215
    :cond_0
    return-void
.end method

.method public bridge synthetic getAnnotation()LX/ClU;
    .locals 1

    .prologue
    .line 2763210
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->v:LX/ClW;

    move-object v0, v0

    .line 2763211
    return-object v0
.end method

.method public getAnnotation()LX/ClW;
    .locals 1

    .prologue
    .line 2763209
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->v:LX/ClW;

    return-object v0
.end method

.method public getExtraPaddingBottom()I
    .locals 1

    .prologue
    .line 2763208
    const/4 v0, 0x0

    return v0
.end method

.method public getIsDirtyAndReset()Z
    .locals 2

    .prologue
    .line 2763201
    iget-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->z:Z

    .line 2763202
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->z:Z

    .line 2763203
    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x81cb2a3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2763231
    invoke-super {p0}, LX/CnR;->onAttachedToWindow()V

    .line 2763232
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->f()V

    .line 2763233
    const/16 v1, 0x2d

    const v2, 0x6141539

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x379d7615

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2763234
    invoke-super {p0}, LX/CnR;->onDetachedFromWindow()V

    .line 2763235
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->i()V

    .line 2763236
    const/16 v1, 0x2d

    const v2, -0x216348d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAnnotation(LX/ClW;)V
    .locals 1

    .prologue
    .line 2763237
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->v:LX/ClW;

    .line 2763238
    if-nez p1, :cond_1

    .line 2763239
    :cond_0
    :goto_0
    return-void

    .line 2763240
    :cond_1
    iget-object v0, p1, LX/ClW;->a:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-object v0, v0

    .line 2763241
    invoke-direct {p0, v0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->setFeedbackOptions(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;)V

    .line 2763242
    iget-object v0, p1, LX/ClW;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v0

    .line 2763243
    if-eqz v0, :cond_0

    .line 2763244
    iget-object v0, p1, LX/ClW;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v0

    .line 2763245
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setComposerLaunchParams(LX/CnN;)V
    .locals 0

    .prologue
    .line 2763246
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->w:LX/CnN;

    .line 2763247
    return-void
.end method

.method public setFeedbackHeaderAuthorByline(LX/CmD;)V
    .locals 0
    .param p1    # LX/CmD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2763248
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->H:LX/CmD;

    .line 2763249
    return-void
.end method

.method public setFeedbackHeaderTitle(LX/Cly;)V
    .locals 0
    .param p1    # LX/Cly;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2763250
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->G:LX/Cly;

    .line 2763251
    return-void
.end method

.method public setFeedbackLoggingParams(LX/162;)V
    .locals 0

    .prologue
    .line 2763252
    return-void
.end method

.method public setIsOverlay(Z)V
    .locals 0

    .prologue
    .line 2763253
    return-void
.end method

.method public setLogoInformation(LX/CmQ;)V
    .locals 0
    .param p1    # LX/CmQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2763254
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->I:LX/CmQ;

    .line 2763255
    return-void
.end method

.method public setShareUrl(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2763256
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->J:Ljava/lang/String;

    .line 2763257
    return-void
.end method

.method public setShowShareButton(Z)V
    .locals 0

    .prologue
    .line 2763258
    return-void
.end method

.method public setShowTopDivider(Z)V
    .locals 0

    .prologue
    .line 2763259
    iput-boolean p1, p0, Lcom/facebook/richdocument/optional/impl/ReactionsUfiViewImpl;->D:Z

    .line 2763260
    return-void
.end method
