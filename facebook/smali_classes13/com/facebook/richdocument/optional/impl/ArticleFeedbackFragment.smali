.class public Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;
.super Lcom/facebook/feedback/ui/BaseFeedbackFragment;
.source ""


# instance fields
.field public B:LX/Clf;

.field public C:LX/Clf;

.field private D:Landroid/content/Context;

.field public E:LX/CmQ;

.field public F:LX/K1s;

.field public G:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2762807
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;-><init>()V

    .line 2762808
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->G:Z

    .line 2762809
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2762806
    const-string v0, "native_article_story"

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2762805
    const-string v0, "flyout_feedback_animation_perf"

    return-object v0
.end method

.method public final m()LX/21B;
    .locals 1

    .prologue
    .line 2762804
    sget-object v0, LX/21B;->INSTANT_ARTICLE:LX/21B;

    return-object v0
.end method

.method public final o()LX/1Cw;
    .locals 2

    .prologue
    .line 2762793
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    if-nez v0, :cond_0

    .line 2762794
    new-instance v0, LX/K1s;

    invoke-direct {v0}, LX/K1s;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    .line 2762795
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->E:LX/CmQ;

    .line 2762796
    iput-object v1, v0, LX/K1s;->c:LX/CmQ;

    .line 2762797
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->B:LX/Clf;

    .line 2762798
    iput-object v1, v0, LX/K1s;->a:LX/Clf;

    .line 2762799
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->C:LX/Clf;

    .line 2762800
    iput-object v1, v0, LX/K1s;->b:LX/Clf;

    .line 2762801
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    iget-boolean v1, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->G:Z

    .line 2762802
    iput-boolean v1, v0, LX/K1s;->d:Z

    .line 2762803
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->F:LX/K1s;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x87ddefa

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2762783
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->r()Landroid/content/Context;

    move-result-object v1

    .line 2762784
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2762785
    const v2, 0x7f03011e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2762786
    const/16 v2, 0x2b

    const v3, 0x7c3b657a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final r()Landroid/content/Context;
    .locals 3

    .prologue
    .line 2762788
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->D:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 2762789
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->D:Landroid/content/Context;

    .line 2762790
    :goto_0
    return-object v0

    .line 2762791
    :cond_0
    new-instance v0, LX/3uc;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0606

    invoke-direct {v0, v1, v2}, LX/3uc;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->D:Landroid/content/Context;

    .line 2762792
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/ArticleFeedbackFragment;->D:Landroid/content/Context;

    goto :goto_0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 2762787
    const/4 v0, 0x0

    return v0
.end method
