.class public Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;
.super LX/CnK;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public b:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/20D;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1za;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field private f:Lcom/facebook/widget/text/BetterTextView;

.field private g:LX/20D;

.field private h:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2763649
    new-instance v0, LX/K2L;

    invoke-direct {v0}, LX/K2L;-><init>()V

    sput-object v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2763645
    invoke-direct {p0, p1}, LX/CnK;-><init>(Landroid/content/Context;)V

    .line 2763646
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->i:Z

    .line 2763647
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->a()V

    .line 2763648
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2763641
    invoke-direct {p0, p1, p2}, LX/CnK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2763642
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->i:Z

    .line 2763643
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->a()V

    .line 2763644
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2763596
    invoke-direct {p0, p1, p2, p3}, LX/CnK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2763597
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->i:Z

    .line 2763598
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->a()V

    .line 2763599
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2763636
    const-class v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2763637
    const v0, 0x7f03011a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2763638
    const v0, 0x7f0d05c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2763639
    const v0, 0x7f0d05c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2763640
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;LX/154;LX/0Or;LX/1za;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;",
            "LX/154;",
            "LX/0Or",
            "<",
            "LX/20D;",
            ">;",
            "LX/1za;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2763635
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->b:LX/154;

    iput-object p2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->c:LX/0Or;

    iput-object p3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->d:LX/1za;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;

    invoke-static {v1}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v0

    check-cast v0, LX/154;

    const/16 v2, 0x7af

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v1}, LX/1za;->a(LX/0QB;)LX/1za;

    move-result-object v1

    check-cast v1, LX/1za;

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->a(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;LX/154;LX/0Or;LX/1za;)V

    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 2763612
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    .line 2763613
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    .line 2763614
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    .line 2763615
    if-lez v0, :cond_2

    .line 2763616
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->b:LX/154;

    invoke-virtual {v3, v0}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2763617
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2763618
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20D;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->g:LX/20D;

    .line 2763619
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->g:LX/20D;

    iget-boolean v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->i:Z

    invoke-virtual {v0, v2}, LX/20D;->a(Z)V

    .line 2763620
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->g:LX/20D;

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->d:LX/1za;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2, v3}, LX/1za;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/20D;->a(Ljava/util/List;)V

    .line 2763621
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2763622
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2763623
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->g:LX/20D;

    aget-object v4, v0, v4

    aget-object v5, v0, v5

    aget-object v0, v0, v7

    invoke-virtual {v2, v3, v4, v5, v0}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2763624
    :goto_0
    if-lez v1, :cond_3

    .line 2763625
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->f:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->b:LX/154;

    invoke-virtual {v2, v1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2763626
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2763627
    :cond_0
    :goto_1
    return-void

    .line 2763628
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2763629
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->g:LX/20D;

    aget-object v4, v0, v4

    aget-object v5, v0, v5

    aget-object v0, v0, v7

    invoke-virtual {v2, v3, v4, v5, v0}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2763630
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->e:Lcom/facebook/widget/text/BetterTextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2763631
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3, v3, v3, v3}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2763632
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->e:Lcom/facebook/widget/text/BetterTextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 2763633
    :cond_3
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->f:Lcom/facebook/widget/text/BetterTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2763634
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->f:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public setClipTokens(Z)V
    .locals 0

    .prologue
    .line 2763610
    iput-boolean p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->i:Z

    .line 2763611
    return-void
.end method

.method public setCommentOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2763608
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2763609
    return-void
.end method

.method public setFeedback(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 2763605
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->h:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2763606
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->b()V

    .line 2763607
    return-void
.end method

.method public setReactorsOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2763603
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2763604
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 2763600
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2763601
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedPillsBlingBarView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2763602
    return-void
.end method
