.class public Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/20Q;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/13Q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/20X;",
            "Lcom/facebook/richdocument/view/widget/PressStateButton;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/view/ViewGroup;

.field private final f:LX/20Y;

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2763547
    new-instance v0, LX/K2K;

    invoke-direct {v0}, LX/K2K;-><init>()V

    sput-object v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2763548
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2763549
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2763550
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2763551
    const-class v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2763552
    const v0, 0x7f03011c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2763553
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setOrientation(I)V

    .line 2763554
    const v0, -0x6e685d

    iput v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->g:I

    .line 2763555
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2763556
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b23c6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2763557
    const v0, 0x7f0d05c8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 2763558
    invoke-static {v0, v2}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(Landroid/view/View;I)V

    .line 2763559
    const v3, 0x7f02080a

    iget v4, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->g:I

    invoke-direct {p0, v3, v4}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const v4, 0x7f0d05c9

    const v5, 0x7f081c4f

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(Landroid/graphics/drawable/Drawable;II)Lcom/facebook/richdocument/view/widget/PressStateButton;

    move-result-object v3

    .line 2763560
    invoke-static {v3, v2}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(Landroid/view/View;I)V

    .line 2763561
    const v4, 0x7f0209c7

    iget v5, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->g:I

    invoke-direct {p0, v4, v5}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const v5, 0x7f0d05ca

    const v6, 0x7f081c55

    invoke-direct {p0, v4, v5, v6}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(Landroid/graphics/drawable/Drawable;II)Lcom/facebook/richdocument/view/widget/PressStateButton;

    move-result-object v4

    .line 2763562
    invoke-static {v4, v2}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(Landroid/view/View;I)V

    .line 2763563
    sget-object v2, LX/20X;->LIKE:LX/20X;

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2763564
    sget-object v0, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {v1, v0, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2763565
    sget-object v0, LX/20X;->SHARE:LX/20X;

    invoke-virtual {v1, v0, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2763566
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    .line 2763567
    const v0, 0x7f0d05c7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->e:Landroid/view/ViewGroup;

    .line 2763568
    new-instance v0, LX/20Y;

    invoke-direct {v0}, LX/20Y;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->f:LX/20Y;

    .line 2763569
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2763570
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20X;

    .line 2763571
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 2763572
    new-instance v3, LX/20a;

    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->f:LX/20Y;

    iget-object v5, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->c:LX/13Q;

    invoke-direct {v3, v1, v4, v5}, LX/20a;-><init>(LX/20X;LX/20Z;LX/13Q;)V

    invoke-virtual {v0, v3}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2763573
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setTextVisibility(I)V

    .line 2763574
    sget-object v0, LX/1vY;->FEEDBACK_SECTION:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2763575
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    sget-object v1, LX/20X;->LIKE:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget-object v1, LX/1vY;->LIKE_LINK:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2763576
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    sget-object v1, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget-object v1, LX/1vY;->COMMENT_LINK:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2763577
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    sget-object v1, LX/20X;->SHARE:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget-object v1, LX/1vY;->SHARE_LINK:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2763578
    return-void
.end method

.method private a(II)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2763579
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->b:LX/0wM;

    invoke-virtual {v0, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/graphics/drawable/Drawable;II)Lcom/facebook/richdocument/view/widget/PressStateButton;
    .locals 2

    .prologue
    .line 2763580
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 2763581
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setSoundEffectsEnabled(Z)V

    .line 2763582
    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2763583
    invoke-virtual {v0, p3}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setText(I)V

    .line 2763584
    return-object v0
.end method

.method private static a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 2763585
    if-eqz p0, :cond_0

    .line 2763586
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2763587
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;LX/0wM;LX/13Q;)V
    .locals 0

    .prologue
    .line 2763588
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->b:LX/0wM;

    iput-object p2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->c:LX/13Q;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v1}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v1

    check-cast v1, LX/13Q;

    invoke-static {p0, v0, v1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;LX/0wM;LX/13Q;)V

    return-void
.end method


# virtual methods
.method public final a(LX/20X;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2763589
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 2763590
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2763591
    return-void
.end method

.method public getReactionsDockAnchor()Landroid/view/View;
    .locals 0

    .prologue
    .line 2763592
    return-object p0
.end method

.method public setBottomDividerStyle(LX/1Wl;)V
    .locals 0

    .prologue
    .line 2763593
    return-void
.end method

.method public setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 2763541
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->e:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2763542
    return-void
.end method

.method public setButtonContainerHeight(I)V
    .locals 2

    .prologue
    .line 2763543
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2763544
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2763545
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2763546
    return-void
.end method

.method public setButtonWeights([F)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2763492
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->values()LX/0Py;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v3

    move v1, v0

    move v2, v0

    .line 2763493
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2763494
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 2763495
    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/PressStateButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2763496
    if-eqz v0, :cond_2

    iget v4, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    aget v5, p1, v2

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_2

    .line 2763497
    aget v1, p1, v2

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2763498
    const/4 v1, 0x1

    move v0, v1

    .line 2763499
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    .line 2763500
    goto :goto_0

    .line 2763501
    :cond_0
    if-eqz v1, :cond_1

    .line 2763502
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 2763503
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 2763504
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public setButtons(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/20X;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2763505
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20X;

    .line 2763506
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 2763507
    :goto_1
    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    invoke-virtual {v3, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    goto :goto_0

    .line 2763508
    :cond_0
    const/16 v1, 0x8

    goto :goto_1

    .line 2763509
    :cond_1
    return-void
.end method

.method public setButtonsColor(I)V
    .locals 5

    .prologue
    .line 2763510
    iput p1, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->g:I

    .line 2763511
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2763512
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 2763513
    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2763514
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->b:LX/0wM;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget v4, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->g:I

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 2763515
    iget v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->g:I

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setTextColor(I)V

    .line 2763516
    instance-of v2, v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    if-eqz v2, :cond_0

    .line 2763517
    check-cast v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    iget v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->g:I

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->setDefaultIconColor(I)V

    goto :goto_0

    .line 2763518
    :cond_1
    return-void
.end method

.method public setDownstateType(LX/1Wk;)V
    .locals 0

    .prologue
    .line 2763540
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    .prologue
    .line 2763519
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 2763520
    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setEnabled(Z)V

    goto :goto_0

    .line 2763521
    :cond_0
    return-void
.end method

.method public setFooterAlpha(F)V
    .locals 0

    .prologue
    .line 2763522
    invoke-static {p0, p1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 2763523
    return-void
.end method

.method public setFooterVisibility(I)V
    .locals 0

    .prologue
    .line 2763524
    invoke-virtual {p0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->setVisibility(I)V

    .line 2763525
    return-void
.end method

.method public setHasCachedComments(Z)V
    .locals 3

    .prologue
    .line 2763526
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    sget-object v1, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    const v1, 0x7f021668

    iget v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->g:I

    invoke-direct {p0, v1, v2}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2763527
    return-void
.end method

.method public setIsLiked(Z)V
    .locals 2

    .prologue
    .line 2763528
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    sget-object v1, LX/20X;->LIKE:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedUfiLikeButton;->setIsLiked(Z)V

    .line 2763529
    return-void
.end method

.method public setOnButtonClickedListener(LX/20Z;)V
    .locals 1

    .prologue
    .line 2763530
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->f:LX/20Y;

    .line 2763531
    iput-object p1, v0, LX/20Y;->a:LX/20Z;

    .line 2763532
    return-void
.end method

.method public setProgressiveUfiState(LX/21H;)V
    .locals 0

    .prologue
    .line 2763533
    return-void
.end method

.method public setShowIcons(Z)V
    .locals 0

    .prologue
    .line 2763534
    return-void
.end method

.method public setSprings(Ljava/util/EnumMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2763535
    return-void
.end method

.method public setTextVisibility(I)V
    .locals 3

    .prologue
    .line 2763536
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20X;

    .line 2763537
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/RichDocumentCondensedFooterView;->d:LX/0P1;

    invoke-virtual {v2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setTextVisibility(I)V

    goto :goto_0

    .line 2763538
    :cond_0
    return-void
.end method

.method public setTopDividerStyle(LX/1Wl;)V
    .locals 0

    .prologue
    .line 2763539
    return-void
.end method
