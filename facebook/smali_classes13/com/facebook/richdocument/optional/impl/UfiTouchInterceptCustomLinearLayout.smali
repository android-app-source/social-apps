.class public Lcom/facebook/richdocument/optional/impl/UfiTouchInterceptCustomLinearLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/CqV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2764588
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2764589
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UfiTouchInterceptCustomLinearLayout;->a()V

    .line 2764590
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2764585
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2764586
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UfiTouchInterceptCustomLinearLayout;->a()V

    .line 2764587
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2764582
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2764583
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UfiTouchInterceptCustomLinearLayout;->a()V

    .line 2764584
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2764591
    const-class v0, Lcom/facebook/richdocument/optional/impl/UfiTouchInterceptCustomLinearLayout;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/optional/impl/UfiTouchInterceptCustomLinearLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2764592
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/optional/impl/UfiTouchInterceptCustomLinearLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/richdocument/optional/impl/UfiTouchInterceptCustomLinearLayout;

    invoke-static {v0}, LX/CqV;->a(LX/0QB;)LX/CqV;

    move-result-object v0

    check-cast v0, LX/CqV;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UfiTouchInterceptCustomLinearLayout;->a:LX/CqV;

    return-void
.end method


# virtual methods
.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 3

    .prologue
    .line 2764578
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UfiTouchInterceptCustomLinearLayout;->a:LX/CqV;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-object v2, LX/CqU;->UFI:LX/CqU;

    invoke-virtual {v1, v0, v2}, LX/CqV;->a(ZLX/CqU;)V

    .line 2764579
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 2764580
    return-void

    .line 2764581
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
