.class public Lcom/facebook/richdocument/optional/impl/UFIViewImpl;
.super LX/CnR;
.source ""

# interfaces
.implements LX/Cjr;
.implements LX/CnP;
.implements LX/CnQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnR;",
        "LX/Cjr;",
        "LX/CnP;",
        "LX/CnQ",
        "<",
        "LX/ClW;",
        ">;"
    }
.end annotation


# static fields
.field public static final m:Ljava/lang/String;

.field private static final n:Landroid/graphics/Typeface;


# instance fields
.field private A:Z

.field private B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field public C:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public D:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field private E:LX/162;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:I

.field private G:I

.field private H:I

.field public I:Z

.field private final J:Ljava/lang/Runnable;

.field private final K:LX/K2k;

.field private final L:LX/K2i;

.field private final M:LX/K2i;

.field private final N:LX/K2j;

.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3iG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1K9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20j;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ck0;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cju;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CjE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/K1u;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/ClW;

.field public p:LX/CnN;

.field private final q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/6Vi;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/richdocument/view/widget/PressStateButton;

.field public s:Lcom/facebook/richdocument/view/widget/PressStateButton;

.field public t:Lcom/facebook/richdocument/view/widget/PressStateButton;

.field public u:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public v:Lcom/facebook/richdocument/view/widget/RichTextView;

.field private w:Landroid/view/ViewStub;

.field private x:Landroid/view/ViewStub;

.field private y:Landroid/view/View;

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2764461
    const-class v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->m:Ljava/lang/String;

    .line 2764462
    const-string v0, "sans-serif"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->n:Landroid/graphics/Typeface;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2764401
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2764402
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 2764403
    invoke-direct {p0, p1, p2}, LX/CnR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2764404
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->q:Ljava/util/ArrayList;

    .line 2764405
    iput-boolean v9, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->A:Z

    .line 2764406
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2764407
    iput-boolean v2, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->I:Z

    .line 2764408
    new-instance v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl$1;

    invoke-direct {v0, p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl$1;-><init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;)V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->J:Ljava/lang/Runnable;

    .line 2764409
    const-class v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2764410
    const v0, 0x7f031217

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2764411
    new-instance v0, LX/K2k;

    invoke-direct {v0, p0}, LX/K2k;-><init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;)V

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->K:LX/K2k;

    .line 2764412
    new-instance v1, LX/K2i;

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20j;

    invoke-direct {v1, p0, v0}, LX/K2i;-><init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;LX/20j;)V

    iput-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->L:LX/K2i;

    .line 2764413
    new-instance v1, LX/K2i;

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20j;

    invoke-direct {v1, p0, v0}, LX/K2i;-><init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;LX/20j;)V

    iput-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->M:LX/K2i;

    .line 2764414
    new-instance v1, LX/K2j;

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20j;

    invoke-direct {v1, p0, v0}, LX/K2j;-><init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;LX/20j;)V

    iput-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->N:LX/K2j;

    .line 2764415
    const v0, 0x7f0d2a4d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->r:Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 2764416
    const v0, 0x7f0d2a4e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->s:Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 2764417
    const v0, 0x7f0d2a4f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->t:Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 2764418
    const v0, 0x7f0d2a51

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->x:Landroid/view/ViewStub;

    .line 2764419
    const v0, 0x7f0d2a53

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->w:Landroid/view/ViewStub;

    .line 2764420
    const v0, 0x7f0d2a50

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->y:Landroid/view/View;

    .line 2764421
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a061b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->F:I

    .line 2764422
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->r:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const v4, 0x7f0d011c

    move v3, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 2764423
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->j:LX/Chi;

    .line 2764424
    iget-object v1, v0, LX/Chi;->g:LX/0lF;

    move-object v0, v1

    .line 2764425
    if-eqz v0, :cond_0

    instance-of v0, v0, LX/162;

    if-eqz v0, :cond_0

    .line 2764426
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->j:LX/Chi;

    .line 2764427
    iget-object v1, v0, LX/Chi;->g:LX/0lF;

    move-object v0, v1

    .line 2764428
    check-cast v0, LX/162;

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->E:LX/162;

    .line 2764429
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->E:LX/162;

    invoke-virtual {p0, v0}, LX/CnR;->setFeedbackLoggingParams(LX/162;)V

    .line 2764430
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->r:Lcom/facebook/richdocument/view/widget/PressStateButton;

    new-instance v1, LX/K2Z;

    invoke-direct {v1, p0}, LX/K2Z;-><init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;)V

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2764431
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->s:Lcom/facebook/richdocument/view/widget/PressStateButton;

    new-instance v1, LX/K2a;

    invoke-direct {v1, p0}, LX/K2a;-><init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;)V

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2764432
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->t:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const v5, 0x7f0d011c

    move v6, v2

    move v7, v2

    move v8, v2

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 2764433
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->t:Lcom/facebook/richdocument/view/widget/PressStateButton;

    new-instance v1, LX/K2b;

    invoke-direct {v1, p0}, LX/K2b;-><init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;)V

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2764434
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cju;

    const v1, 0x7f0d016b

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    .line 2764435
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->r:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v1, v3, v4, v5}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2764436
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->s:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v1, v3, v4, v5}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2764437
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->x:Landroid/view/ViewStub;

    new-instance v3, LX/K2d;

    invoke-direct {v3, p0, v0}, LX/K2d;-><init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;I)V

    invoke-virtual {v1, v3}, Landroid/view/ViewStub;->setOnInflateListener(Landroid/view/ViewStub$OnInflateListener;)V

    .line 2764438
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->w:Landroid/view/ViewStub;

    new-instance v3, LX/K2f;

    invoke-direct {v3, p0, v0}, LX/K2f;-><init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;I)V

    invoke-virtual {v1, v3}, Landroid/view/ViewStub;->setOnInflateListener(Landroid/view/ViewStub$OnInflateListener;)V

    .line 2764439
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->s:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v9}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setAlignImageToTextTop(Z)V

    .line 2764440
    invoke-virtual {p0, v2}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->setClipChildren(Z)V

    .line 2764441
    invoke-virtual {p0, v2}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->setClipToPadding(Z)V

    .line 2764442
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2764443
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->i:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2764444
    invoke-virtual {p0, v9}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->setLayoutDirection(I)V

    .line 2764445
    :cond_1
    :goto_0
    return-void

    .line 2764446
    :cond_2
    invoke-virtual {p0, v2}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->setLayoutDirection(I)V

    goto :goto_0
.end method

.method private static a(LX/CnN;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 3

    .prologue
    .line 2764447
    new-instance v0, LX/170;

    invoke-direct {v0}, LX/170;-><init>()V

    .line 2764448
    iget-object v1, p0, LX/CnN;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2764449
    iput-object v1, v0, LX/170;->o:Ljava/lang/String;

    .line 2764450
    move-object v0, v0

    .line 2764451
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2764452
    iget v2, p0, LX/CnN;->f:I

    move v2, v2

    .line 2764453
    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2764454
    iput-object v1, v0, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2764455
    move-object v0, v0

    .line 2764456
    invoke-virtual {v0}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    .line 2764457
    sget-object v1, LX/21D;->INSTANT_ARTICLE:LX/21D;

    const-string v2, "richdocumentUfiView"

    invoke-static {v0}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v0

    .line 2764458
    iput-object p1, v0, LX/89G;->d:Ljava/lang/String;

    .line 2764459
    move-object v0, v0

    .line 2764460
    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;LX/0Ot;LX/3iG;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/03V;LX/Crz;LX/Chi;LX/K1u;LX/154;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/richdocument/optional/impl/UFIViewImpl;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/3iG;",
            "LX/0Ot",
            "<",
            "LX/1K9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/20j;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ck0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cju;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CjE;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/Crz;",
            "LX/Chi;",
            "LX/K1u;",
            "LX/154;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2764463
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->b:LX/3iG;

    iput-object p3, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->e:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->f:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->g:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->h:LX/03V;

    iput-object p9, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->i:LX/Crz;

    iput-object p10, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->j:LX/Chi;

    iput-object p11, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->k:LX/K1u;

    iput-object p12, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->l:LX/154;

    return-void
.end method

.method private static a(Lcom/facebook/richdocument/view/widget/RichTextView;)V
    .locals 3

    .prologue
    const/16 v2, 0x10

    .line 2764464
    if-nez p0, :cond_1

    .line 2764465
    :cond_0
    :goto_0
    return-void

    .line 2764466
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->a()V

    .line 2764467
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2764468
    instance-of v1, v0, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v1, :cond_2

    .line 2764469
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    goto :goto_0

    .line 2764470
    :cond_2
    instance-of v1, v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v1, :cond_0

    .line 2764471
    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 13

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v0, p0

    check-cast v0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;

    const/16 v1, 0x3be

    invoke-static {v12, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const-class v2, LX/3iG;

    invoke-interface {v12, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/3iG;

    const/16 v3, 0x6cc

    invoke-static {v12, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x475

    invoke-static {v12, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x320f

    invoke-static {v12, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x320e

    invoke-static {v12, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x31e6

    invoke-static {v12, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v12}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v12}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v9

    check-cast v9, LX/Crz;

    invoke-static {v12}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v10

    check-cast v10, LX/Chi;

    invoke-static {v12}, LX/K1u;->a(LX/0QB;)LX/K1u;

    move-result-object v11

    check-cast v11, LX/K1u;

    invoke-static {v12}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v12

    check-cast v12, LX/154;

    invoke-static/range {v0 .. v12}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;LX/0Ot;LX/3iG;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/03V;LX/Crz;LX/Chi;LX/K1u;LX/154;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2764576
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CjE;

    new-instance v1, LX/K2g;

    invoke-direct {v1, p0}, LX/K2g;-><init>(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;)V

    invoke-virtual {v0, p1, v1}, LX/CjE;->a(Ljava/lang/String;LX/2h1;)V

    .line 2764577
    return-void
.end method

.method public static f(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;)V
    .locals 5

    .prologue
    .line 2764472
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->E:LX/162;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    .line 2764473
    :goto_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    .line 2764474
    iget-object v2, v0, LX/CnN;->b:Landroid/app/Activity;

    move-object v0, v2

    .line 2764475
    if-eqz v0, :cond_1

    .line 2764476
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    .line 2764477
    iget-object v3, v2, LX/CnN;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2764478
    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    invoke-static {v3, v1}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a(LX/CnN;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    .line 2764479
    iget v4, v3, LX/CnN;->a:I

    move v3, v4

    .line 2764480
    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    .line 2764481
    iget-object p0, v4, LX/CnN;->b:Landroid/app/Activity;

    move-object v4, p0

    .line 2764482
    invoke-interface {v0, v2, v1, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2764483
    :goto_1
    return-void

    .line 2764484
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->E:LX/162;

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2764485
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    .line 2764486
    iget-object v2, v0, LX/CnN;->c:Landroid/support/v4/app/Fragment;

    move-object v0, v2

    .line 2764487
    if-eqz v0, :cond_2

    .line 2764488
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    .line 2764489
    iget-object v3, v2, LX/CnN;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2764490
    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    invoke-static {v3, v1}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a(LX/CnN;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    .line 2764491
    iget v4, v3, LX/CnN;->a:I

    move v3, v4

    .line 2764492
    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    .line 2764493
    iget-object p0, v4, LX/CnN;->c:Landroid/support/v4/app/Fragment;

    move-object v4, p0

    .line 2764494
    invoke-interface {v0, v2, v1, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    goto :goto_1

    .line 2764495
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    .line 2764496
    iget-object v3, v2, LX/CnN;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2764497
    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    invoke-static {v3, v1}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a(LX/CnN;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v1, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    goto :goto_1
.end method

.method private g()V
    .locals 5

    .prologue
    .line 2764498
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-nez v0, :cond_1

    .line 2764499
    :cond_0
    :goto_0
    return-void

    .line 2764500
    :cond_1
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->n()V

    .line 2764501
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->q:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1K9;

    const-class v2, LX/82h;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->K:LX/K2k;

    invoke-virtual {v0, v2, v3, v4}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2764502
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->q:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1K9;

    const-class v2, LX/8px;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->L:LX/K2i;

    invoke-virtual {v0, v2, v3, v4}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2764503
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->q:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1K9;

    const-class v2, LX/8q4;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->M:LX/K2i;

    invoke-virtual {v0, v2, v3, v4}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2764504
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->q:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1K9;

    const-class v2, LX/8py;

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->N:LX/K2j;

    invoke-virtual {v0, v2, v3, v4}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 2764505
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-ne v0, v1, :cond_2

    .line 2764506
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->r:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    .line 2764507
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->r:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2764508
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2764509
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0615

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v0, v1}, LX/CoV;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2764510
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->r:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0615

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setTextColor(I)V

    .line 2764511
    :goto_0
    return-void

    .line 2764512
    :cond_1
    iget v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->G:I

    invoke-static {v0, v1}, LX/CoV;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2764513
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->r:Lcom/facebook/richdocument/view/widget/PressStateButton;

    iget v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->F:I

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setTextColor(I)V

    goto :goto_0

    .line 2764514
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->r:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2764515
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-ne v0, v1, :cond_0

    .line 2764516
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->s:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    .line 2764517
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->s:Lcom/facebook/richdocument/view/widget/PressStateButton;

    iget v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->F:I

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setTextColor(I)V

    .line 2764518
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->s:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2764519
    iget v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->G:I

    invoke-static {v0, v1}, LX/CoV;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2764520
    :goto_0
    return-void

    .line 2764521
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->s:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 2764522
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2764523
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->t:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    .line 2764524
    :goto_0
    return-void

    .line 2764525
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->t:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    .line 2764526
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->t:Lcom/facebook/richdocument/view/widget/PressStateButton;

    iget v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->F:I

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setTextColor(I)V

    .line 2764527
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->t:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2764528
    iget v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->G:I

    invoke-static {v0, v1}, LX/CoV;->a(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_0
.end method

.method private k()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2764529
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2764530
    :cond_0
    :goto_0
    return-void

    .line 2764531
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v0

    .line 2764532
    if-lez v0, :cond_3

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-ne v1, v2, :cond_3

    .line 2764533
    :cond_2
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->x:Landroid/view/ViewStub;

    invoke-virtual {v1, v6}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 2764534
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->u:Lcom/facebook/richdocument/view/widget/RichTextView;

    if-eqz v1, :cond_0

    .line 2764535
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->u:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2764536
    iget-object v2, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v2

    .line 2764537
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00d2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->l:LX/154;

    invoke-virtual {v5, v0}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 2764538
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->u:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2764539
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 2764540
    iget v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->F:I

    invoke-virtual {v0, v1}, LX/CtG;->setTextColor(I)V

    .line 2764541
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->y:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2764542
    :cond_3
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->u:Lcom/facebook/richdocument/view/widget/RichTextView;

    if-eqz v0, :cond_0

    .line 2764543
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->u:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private l()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2764544
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2764545
    :cond_0
    :goto_0
    return-void

    .line 2764546
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->a()I

    move-result v0

    .line 2764547
    if-lez v0, :cond_2

    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-ne v1, v2, :cond_2

    .line 2764548
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->w:Landroid/view/ViewStub;

    invoke-virtual {v1, v6}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 2764549
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->v:Lcom/facebook/richdocument/view/widget/RichTextView;

    if-eqz v1, :cond_0

    .line 2764550
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->v:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2764551
    iget-object v2, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v2

    .line 2764552
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00d3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->l:LX/154;

    invoke-virtual {v5, v0}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 2764553
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->v:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2764554
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 2764555
    iget v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->F:I

    invoke-virtual {v0, v1}, LX/CtG;->setTextColor(I)V

    .line 2764556
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->y:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2764557
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->v:Lcom/facebook/richdocument/view/widget/RichTextView;

    if-eqz v0, :cond_0

    .line 2764558
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->v:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private m()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2764559
    iget v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->H:I

    const/4 v2, 0x3

    if-lt v0, v2, :cond_1

    .line 2764560
    :cond_0
    :goto_0
    return-void

    .line 2764561
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->I:Z

    if-nez v0, :cond_0

    .line 2764562
    const/4 v0, 0x0

    .line 2764563
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->r:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v2}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    sget-object v3, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->n:Landroid/graphics/Typeface;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 2764564
    :cond_2
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->s:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v2}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    sget-object v3, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->n:Landroid/graphics/Typeface;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 2764565
    :cond_3
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->u:Lcom/facebook/richdocument/view/widget/RichTextView;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->u:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2764566
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 2764567
    invoke-virtual {v2}, LX/CtG;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    sget-object v3, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->n:Landroid/graphics/Typeface;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 2764568
    :cond_4
    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->v:Lcom/facebook/richdocument/view/widget/RichTextView;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->v:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2764569
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 2764570
    invoke-virtual {v2}, LX/CtG;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    sget-object v3, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->n:Landroid/graphics/Typeface;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    move v0, v1

    .line 2764571
    :cond_5
    if-eqz v0, :cond_0

    .line 2764572
    iget v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->H:I

    .line 2764573
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->J:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2764574
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->J:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2764575
    iput-boolean v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->I:Z

    goto :goto_0
.end method

.method private n()V
    .locals 4

    .prologue
    .line 2764395
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Vi;

    .line 2764396
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1K9;

    invoke-virtual {v1, v0}, LX/1K9;->a(LX/6Vi;)V

    .line 2764397
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2764398
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2764399
    return-void
.end method

.method private o()Z
    .locals 2

    .prologue
    .line 2764400
    iget-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->A:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setFeedback(Lcom/facebook/richdocument/optional/impl/UFIViewImpl;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 1

    .prologue
    .line 2764308
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2764309
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->g()V

    .line 2764310
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->z:Z

    .line 2764311
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a()V

    .line 2764312
    return-void
.end method

.method private setFeedbackOptions(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;)V
    .locals 0

    .prologue
    .line 2764325
    if-nez p1, :cond_0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    :cond_0
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2764326
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2764327
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_2

    .line 2764328
    :cond_0
    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->setVisibility(I)V

    .line 2764329
    :cond_1
    :goto_0
    return-void

    .line 2764330
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->y:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2764331
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->h()V

    .line 2764332
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->i()V

    .line 2764333
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->j()V

    .line 2764334
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->k()V

    .line 2764335
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->l()V

    .line 2764336
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->getAlpha()F

    move-result v0

    sget v1, LX/CoL;->s:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 2764337
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2764338
    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2764339
    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->B:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2764340
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->H:I

    .line 2764341
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->setVisibility(I)V

    .line 2764342
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->u:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-static {v0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a(Lcom/facebook/richdocument/view/widget/RichTextView;)V

    .line 2764343
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->v:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-static {v0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a(Lcom/facebook/richdocument/view/widget/RichTextView;)V

    .line 2764344
    return-void
.end method

.method public final c()Landroid/view/View;
    .locals 0

    .prologue
    .line 2764345
    return-object p0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2764346
    const/4 v0, 0x1

    return v0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 2764347
    invoke-super {p0, p1}, LX/CnR;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2764348
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->m()V

    .line 2764349
    return-void
.end method

.method public bridge synthetic getAnnotation()LX/ClU;
    .locals 1

    .prologue
    .line 2764350
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->o:LX/ClW;

    move-object v0, v0

    .line 2764351
    return-object v0
.end method

.method public getAnnotation()LX/ClW;
    .locals 1

    .prologue
    .line 2764352
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->o:LX/ClW;

    return-object v0
.end method

.method public getExtraPaddingBottom()I
    .locals 4

    .prologue
    .line 2764353
    iget-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->s:Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 2764354
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object v0, v1

    .line 2764355
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 2764356
    invoke-virtual {v0}, LX/CtG;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 2764357
    iget-object v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->s:Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 2764358
    iget-object v2, v1, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object v1, v2

    .line 2764359
    invoke-virtual {v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->getBottom()I

    move-result v1

    .line 2764360
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->s:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v3}, Lcom/facebook/richdocument/view/widget/PressStateButton;->getTop()I

    move-result v3

    add-int/2addr v1, v3

    sub-int v1, v2, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public getIsDirtyAndReset()Z
    .locals 2

    .prologue
    .line 2764361
    iget-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->z:Z

    .line 2764362
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->z:Z

    .line 2764363
    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4512e7f0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2764364
    invoke-super {p0}, LX/CnR;->onAttachedToWindow()V

    .line 2764365
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->g()V

    .line 2764366
    const/16 v1, 0x2d

    const v2, -0x5c456b0d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1c8604ce

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2764367
    invoke-super {p0}, LX/CnR;->onDetachedFromWindow()V

    .line 2764368
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->n()V

    .line 2764369
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->H:I

    .line 2764370
    const/16 v1, 0x2d

    const v2, -0x3e011914

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAnnotation(LX/ClW;)V
    .locals 1

    .prologue
    .line 2764371
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->o:LX/ClW;

    .line 2764372
    iget-object v0, p1, LX/ClW;->a:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-object v0, v0

    .line 2764373
    invoke-direct {p0, v0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->setFeedbackOptions(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;)V

    .line 2764374
    iget-object v0, p1, LX/ClW;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v0

    .line 2764375
    if-eqz v0, :cond_0

    .line 2764376
    iget-object v0, p1, LX/ClW;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v0

    .line 2764377
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a(Ljava/lang/String;)V

    .line 2764378
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a()V

    .line 2764379
    return-void
.end method

.method public setComposerLaunchParams(LX/CnN;)V
    .locals 0

    .prologue
    .line 2764380
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->p:LX/CnN;

    .line 2764381
    return-void
.end method

.method public setFeedbackLoggingParams(LX/162;)V
    .locals 2

    .prologue
    .line 2764313
    if-nez p1, :cond_0

    .line 2764314
    :goto_0
    return-void

    .line 2764315
    :cond_0
    new-instance v0, LX/21A;

    invoke-direct {v0}, LX/21A;-><init>()V

    const-string v1, "native_article_story"

    .line 2764316
    iput-object v1, v0, LX/21A;->c:Ljava/lang/String;

    .line 2764317
    move-object v0, v0

    .line 2764318
    iput-object p1, v0, LX/21A;->a:LX/162;

    .line 2764319
    move-object v0, v0

    .line 2764320
    sget-object v1, LX/21D;->INSTANT_ARTICLE:LX/21D;

    .line 2764321
    iput-object v1, v0, LX/21A;->i:LX/21D;

    .line 2764322
    move-object v0, v0

    .line 2764323
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->D:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2764324
    iput-object p1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->E:LX/162;

    goto :goto_0
.end method

.method public setIsOverlay(Z)V
    .locals 2

    .prologue
    .line 2764382
    if-eqz p1, :cond_0

    const v0, 0x7f0a0617

    .line 2764383
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->F:I

    .line 2764384
    if-eqz p1, :cond_1

    const v0, 0x7f0a0617

    .line 2764385
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->G:I

    .line 2764386
    invoke-virtual {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->a()V

    .line 2764387
    return-void

    .line 2764388
    :cond_0
    const v0, 0x7f0a061b

    goto :goto_0

    .line 2764389
    :cond_1
    const v0, 0x7f0a061c

    goto :goto_1
.end method

.method public setShowShareButton(Z)V
    .locals 1

    .prologue
    .line 2764390
    iget-boolean v0, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->A:Z

    if-eq v0, p1, :cond_0

    .line 2764391
    iput-boolean p1, p0, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->A:Z

    .line 2764392
    invoke-direct {p0}, Lcom/facebook/richdocument/optional/impl/UFIViewImpl;->j()V

    .line 2764393
    :cond_0
    return-void
.end method

.method public setShowTopDivider(Z)V
    .locals 0

    .prologue
    .line 2764394
    return-void
.end method
