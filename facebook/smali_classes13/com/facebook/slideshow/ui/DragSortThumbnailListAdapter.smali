.class public Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Landroid/view/LayoutInflater;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2765433
    const-class v0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    const-string v1, "slideshow_in_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2765428
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2765429
    iput-object p1, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->b:Landroid/content/Context;

    .line 2765430
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->c:Landroid/view/LayoutInflater;

    .line 2765431
    iput-object p2, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2765432
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2765425
    if-nez p2, :cond_0

    .line 2765426
    new-instance v0, LX/K3H;

    iget-object v1, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f031349

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/K3H;-><init>(Landroid/view/View;)V

    .line 2765427
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/K3G;

    iget-object v1, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f03134d

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/K3G;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2765420
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2765421
    check-cast p1, LX/K3G;

    iget-object v0, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2765422
    iget-object v1, p1, LX/K3G;->m:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object p0

    sget-object p2, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, p0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2765423
    :goto_0
    return-void

    .line 2765424
    :cond_0
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v1, LX/K3F;

    invoke-direct {v1, p0}, LX/K3F;-><init>(Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2765416
    iget-object v0, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2765417
    iget-object v0, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 2765418
    const/4 v0, 0x1

    .line 2765419
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x7

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method
