.class public Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/K3I;
.implements LX/K3M;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/1Ad;

.field private final c:LX/K3N;

.field private final d:Landroid/content/Context;

.field public e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

.field private f:Ljava/lang/String;

.field public g:Landroid/media/MediaPlayer;

.field private h:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I

.field private final k:Landroid/view/View$OnClickListener;

.field private final l:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2765732
    const-class v0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    const-string v1, "slideshow_in_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/slideshow/ui/PlayableSlideshowView;Ljava/lang/String;Landroid/content/Context;LX/1Ad;)V
    .locals 4
    .param p1    # Lcom/facebook/slideshow/ui/PlayableSlideshowView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2765720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2765721
    iput-object p1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    .line 2765722
    iput-object p2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->f:Ljava/lang/String;

    .line 2765723
    iput-object p3, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->d:Landroid/content/Context;

    .line 2765724
    iput-object p4, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->b:LX/1Ad;

    .line 2765725
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->b:LX/1Ad;

    sget-object v1, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 2765726
    new-instance v0, LX/K3N;

    const/16 v1, 0x7d0

    const/16 v2, 0xc8

    invoke-direct {v0, v3, v1, v2, v3}, LX/K3N;-><init>(IIIZ)V

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->c:LX/K3N;

    .line 2765727
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    .line 2765728
    iput-object p0, v0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->e:LX/K3M;

    .line 2765729
    new-instance v0, LX/K3O;

    invoke-direct {v0, p0}, LX/K3O;-><init>(Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;)V

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->k:Landroid/view/View$OnClickListener;

    .line 2765730
    new-instance v0, LX/K3P;

    invoke-direct {v0, p0}, LX/K3P;-><init>(Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;)V

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->l:Landroid/view/View$OnClickListener;

    .line 2765731
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2765718
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    invoke-virtual {v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f()V

    .line 2765719
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2765716
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->a(LX/0Px;Z)V

    .line 2765717
    return-void
.end method

.method public final a(LX/0Px;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2765704
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2765705
    iput v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->j:I

    .line 2765706
    :goto_0
    return-void

    .line 2765707
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->c:LX/K3N;

    iget v2, v2, LX/K3N;->b:I

    mul-int/2addr v1, v2

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->c:LX/K3N;

    iget v3, v3, LX/K3N;->c:I

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->j:I

    .line 2765708
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2765709
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2765710
    iget-object v4, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->b:LX/1Ad;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2765711
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2765712
    :cond_1
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    iget-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->f:Ljava/lang/String;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->c:LX/K3N;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->a(Ljava/lang/String;LX/0Px;LX/K3N;)V

    .line 2765713
    if-eqz p2, :cond_2

    .line 2765714
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    invoke-virtual {v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->e()V

    goto :goto_0

    .line 2765715
    :cond_2
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    iget-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;)V
    .locals 9
    .param p1    # Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2765674
    if-eqz p1, :cond_3

    .line 2765675
    invoke-virtual {p1}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->b()LX/0Px;

    move-result-object v0

    iget v2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->j:I

    const/4 v4, 0x0

    .line 2765676
    if-nez v0, :cond_4

    .line 2765677
    :cond_0
    move-object v0, v4

    .line 2765678
    :goto_0
    if-nez v0, :cond_1

    .line 2765679
    iput-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->h:Landroid/net/Uri;

    .line 2765680
    iput-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->i:Ljava/lang/String;

    .line 2765681
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    invoke-virtual {v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f()V

    .line 2765682
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    invoke-virtual {v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->e()V

    .line 2765683
    :goto_1
    return-void

    .line 2765684
    :cond_1
    iget-object v2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    invoke-virtual {v2}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f()V

    .line 2765685
    invoke-virtual {v0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->h:Landroid/net/Uri;

    .line 2765686
    invoke-virtual {p1}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->i:Ljava/lang/String;

    .line 2765687
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 2765688
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 2765689
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 2765690
    iput-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    .line 2765691
    :cond_2
    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    .line 2765692
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 2765693
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 2765694
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    .line 2765695
    invoke-static {v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->l(Lcom/facebook/slideshow/ui/PlayableSlideshowView;)V

    .line 2765696
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2765697
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2765698
    :catch_0
    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0

    .line 2765699
    :cond_4
    const v5, 0x7fffffff

    .line 2765700
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v7

    const/4 v3, 0x0

    move v6, v3

    :goto_2
    if-ge v6, v7, :cond_0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;

    .line 2765701
    invoke-virtual {v3}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->c()I

    move-result v8

    if-le v8, v2, :cond_5

    invoke-virtual {v3}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->c()I

    move-result v8

    sub-int/2addr v8, v2

    if-ge v8, v5, :cond_5

    .line 2765702
    invoke-virtual {v3}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;->c()I

    move-result v4

    sub-int/2addr v4, v2

    .line 2765703
    :goto_3
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v4

    move-object v4, v3

    goto :goto_2

    :cond_5
    move-object v3, v4

    move v4, v5

    goto :goto_3
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2765657
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    invoke-virtual {v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f()V

    .line 2765658
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2765669
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->h:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 2765670
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 2765671
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 2765672
    :cond_0
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    iget-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2765673
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2765665
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 2765666
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 2765667
    :cond_0
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    iget-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2765668
    return-void
.end method

.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    .line 2765659
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->g:Landroid/media/MediaPlayer;

    if-eq v0, p1, :cond_0

    .line 2765660
    new-instance v0, Ljava/lang/IllegalThreadStateException;

    const-string v1, "Media player and prepared audio are not in sync"

    invoke-direct {v0, v1}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2765661
    :cond_0
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    .line 2765662
    invoke-static {v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->k(Lcom/facebook/slideshow/ui/PlayableSlideshowView;)V

    .line 2765663
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->e:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    invoke-virtual {v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->e()V

    .line 2765664
    return-void
.end method
