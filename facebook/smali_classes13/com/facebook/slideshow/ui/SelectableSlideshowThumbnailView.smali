.class public Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# static fields
.field private static final c:LX/0wT;


# instance fields
.field public d:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0wd;

.field private f:LX/4Ab;

.field public g:Z

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2765777
    const-wide v0, 0x4050400000000000L    # 65.0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->c:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2765778
    invoke-direct {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2765779
    invoke-direct {p0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->c()V

    .line 2765780
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1af;)V
    .locals 0

    .prologue
    .line 2765781
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;LX/1af;)V

    .line 2765782
    invoke-direct {p0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->c()V

    .line 2765783
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2765771
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2765772
    invoke-direct {p0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->c()V

    .line 2765773
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2765774
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2765775
    invoke-direct {p0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->c()V

    .line 2765776
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    iput-object v0, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->d:LX/0wW;

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 2765761
    const-class v0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    invoke-static {v0, p0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2765762
    iget-object v0, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->d:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->c:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 2765763
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 2765764
    move-object v0, v0

    .line 2765765
    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->e:LX/0wd;

    .line 2765766
    iget-object v0, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->e:LX/0wd;

    new-instance v1, LX/K3R;

    invoke-direct {v1, p0}, LX/K3R;-><init>(Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 2765767
    new-instance v0, LX/K3S;

    invoke-direct {v0, p0}, LX/K3S;-><init>(Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2765768
    const/4 v0, 0x0

    invoke-static {v0}, LX/4Ab;->b(F)LX/4Ab;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b26f1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->f:LX/4Ab;

    .line 2765769
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->h:Z

    .line 2765770
    return-void
.end method


# virtual methods
.method public setAutoSelectedOnTap(Z)V
    .locals 0

    .prologue
    .line 2765759
    iput-boolean p1, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->g:Z

    .line 2765760
    return-void
.end method

.method public setIsSelected(Z)V
    .locals 4

    .prologue
    .line 2765751
    iput-boolean p1, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->h:Z

    .line 2765752
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 2765753
    if-eqz v0, :cond_0

    .line 2765754
    iget-boolean v1, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->h:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->f:LX/4Ab;

    :goto_0
    invoke-virtual {v0, v1}, LX/1af;->a(LX/4Ab;)V

    .line 2765755
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->e:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    const-wide v2, 0x3fee666666666666L    # 0.95

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 2765756
    iget-object v0, p0, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->e:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2765757
    :cond_1
    return-void

    .line 2765758
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
