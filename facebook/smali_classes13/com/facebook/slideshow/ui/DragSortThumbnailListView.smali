.class public Lcom/facebook/slideshow/ui/DragSortThumbnailListView;
.super Lcom/facebook/widget/recyclerview/BetterRecyclerView;
.source ""


# instance fields
.field public i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2765434
    invoke-direct {p0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;)V

    .line 2765435
    invoke-direct {p0}, Lcom/facebook/slideshow/ui/DragSortThumbnailListView;->o()V

    .line 2765436
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2765437
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2765438
    invoke-direct {p0}, Lcom/facebook/slideshow/ui/DragSortThumbnailListView;->o()V

    .line 2765439
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2765440
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2765441
    invoke-direct {p0}, Lcom/facebook/slideshow/ui/DragSortThumbnailListView;->o()V

    .line 2765442
    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2765443
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Lcom/facebook/slideshow/ui/DragSortThumbnailListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v2, v2}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2765444
    return-void
.end method


# virtual methods
.method public setIsDraggingItem(Z)V
    .locals 0

    .prologue
    .line 2765445
    iput-boolean p1, p0, Lcom/facebook/slideshow/ui/DragSortThumbnailListView;->i:Z

    .line 2765446
    return-void
.end method
