.class public Lcom/facebook/slideshow/ui/PlayableSlideshowView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/3l1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/ProgressBar;

.field public e:LX/K3M;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/K3N;

.field private g:LX/4Ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4Ac",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/1Up;

.field private i:I

.field private j:I

.field private k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1aZ;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/1ap;

.field private m:Ljava/util/Timer;

.field public n:Landroid/os/Handler;

.field private o:Ljava/lang/String;

.field public final p:Ljava/lang/Runnable;

.field private final q:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2765589
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2765590
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2765591
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2765592
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2765593
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2765594
    const-class v0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    invoke-static {v0, p0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2765595
    const v0, 0x7f030fe0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2765596
    const v0, 0x7f0d2646

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2765597
    const v0, 0x7f0d2647

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->c:Landroid/widget/ImageView;

    .line 2765598
    const v0, 0x7f0d2648

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->d:Landroid/widget/ProgressBar;

    .line 2765599
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    .line 2765600
    new-instance v0, Lcom/facebook/slideshow/ui/PlayableSlideshowView$1;

    invoke-direct {v0, p0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView$1;-><init>(Lcom/facebook/slideshow/ui/PlayableSlideshowView;)V

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->p:Ljava/lang/Runnable;

    .line 2765601
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->n:Landroid/os/Handler;

    .line 2765602
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2765603
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    invoke-static {v0}, LX/3l1;->b(LX/0QB;)LX/3l1;

    move-result-object v0

    check-cast v0, LX/3l1;

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->a:LX/3l1;

    return-void
.end method

.method private a(Ljava/lang/String;LX/0Px;LX/K3N;LX/1Up;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/1aZ;",
            ">;",
            "LX/K3N;",
            "LX/1Up;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2765604
    iput-object p1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->o:Ljava/lang/String;

    .line 2765605
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->a:LX/3l1;

    iget-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->o:Ljava/lang/String;

    .line 2765606
    iput-object v1, v0, LX/3l1;->b:Ljava/lang/String;

    .line 2765607
    iput-object p2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->k:LX/0Px;

    .line 2765608
    iput-object p3, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f:LX/K3N;

    .line 2765609
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->h:LX/1Up;

    if-ne p4, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    if-nez v0, :cond_1

    .line 2765610
    :cond_0
    iput-object p4, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->h:LX/1Up;

    .line 2765611
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->c()V

    .line 2765612
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2765613
    iget-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    .line 2765614
    invoke-static {v0}, LX/1Uo;->v(LX/1Uo;)V

    .line 2765615
    move-object v2, v0

    .line 2765616
    iget-object v3, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->h:LX/1Up;

    invoke-virtual {v2, v3}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v2

    invoke-virtual {v2}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4Ac;->a(LX/1aX;)V

    .line 2765617
    iget-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    .line 2765618
    invoke-static {v0}, LX/1Uo;->v(LX/1Uo;)V

    .line 2765619
    move-object v0, v0

    .line 2765620
    iget-object v2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->h:LX/1Up;

    invoke-virtual {v0, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4Ac;->a(LX/1aX;)V

    .line 2765621
    new-instance v0, LX/1ap;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    invoke-virtual {v2, v4}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v2

    invoke-virtual {v2}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    invoke-virtual {v2, v5}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v2

    invoke-virtual {v2}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-direct {v0, v1}, LX/1ap;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->l:LX/1ap;

    .line 2765622
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->l:LX/1ap;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2765623
    :cond_1
    invoke-direct {p0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->i()V

    .line 2765624
    invoke-direct {p0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->j()V

    .line 2765625
    return-void
.end method

.method public static h(Lcom/facebook/slideshow/ui/PlayableSlideshowView;)V
    .locals 4

    .prologue
    .line 2765626
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2765627
    iget v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->i:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->k:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    rem-int v1, v0, v1

    .line 2765628
    iget v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->j:I

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    invoke-virtual {v2}, LX/4Ac;->d()I

    move-result v2

    rem-int v2, v0, v2

    .line 2765629
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    invoke-virtual {v0, v2}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->k:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aZ;

    invoke-virtual {v3, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 2765630
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->l:LX/1ap;

    iget-object v3, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f:LX/K3N;

    iget v3, v3, LX/K3N;->c:I

    invoke-virtual {v0, v3}, LX/1ap;->c(I)V

    .line 2765631
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->l:LX/1ap;

    invoke-virtual {v0, v2}, LX/1ap;->f(I)V

    .line 2765632
    iput v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->i:I

    .line 2765633
    iput v2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->j:I

    .line 2765634
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2765635
    :cond_0
    iget v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->i:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f:LX/K3N;

    iget-boolean v0, v0, LX/K3N;->d:Z

    if-nez v0, :cond_1

    .line 2765636
    invoke-virtual {p0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f()V

    .line 2765637
    :cond_1
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 2765638
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->m:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 2765639
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->m:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 2765640
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->m:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 2765641
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->m:Ljava/util/Timer;

    .line 2765642
    :cond_0
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2765580
    iput v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->j:I

    .line 2765581
    iput v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->i:I

    .line 2765582
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    iget v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->j:I

    invoke-virtual {v0, v1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->k:LX/0Px;

    iget v2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->i:I

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aZ;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 2765583
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->l:LX/1ap;

    iget v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->j:I

    invoke-virtual {v0, v1}, LX/1ap;->f(I)V

    .line 2765584
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->l:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->f()V

    .line 2765585
    return-void
.end method

.method public static k(Lcom/facebook/slideshow/ui/PlayableSlideshowView;)V
    .locals 2

    .prologue
    .line 2765586
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2765587
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2765588
    return-void
.end method

.method public static l(Lcom/facebook/slideshow/ui/PlayableSlideshowView;)V
    .locals 2

    .prologue
    .line 2765577
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->c:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2765578
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->d:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2765579
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0Px;LX/K3N;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/1aZ;",
            ">;",
            "LX/K3N;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2765575
    sget-object v0, LX/1Up;->g:LX/1Up;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->a(Ljava/lang/String;LX/0Px;LX/K3N;LX/1Up;)V

    .line 2765576
    return-void
.end method

.method public final e()V
    .locals 6

    .prologue
    const/16 v2, 0x8

    .line 2765566
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->a:LX/3l1;

    .line 2765567
    sget-object v1, LX/BOR;->SLIDESHOW_PREVIEW_PLAY:LX/BOR;

    invoke-static {v1}, LX/3l1;->a(LX/BOR;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/3l1;->a(LX/3l1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2765568
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->e:LX/K3M;

    if-eqz v0, :cond_0

    .line 2765569
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->e:LX/K3M;

    invoke-interface {v0}, LX/K3M;->c()V

    .line 2765570
    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->m:Ljava/util/Timer;

    .line 2765571
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2765572
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2765573
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->m:Ljava/util/Timer;

    new-instance v1, Lcom/facebook/slideshow/ui/PlayableSlideshowView$2;

    invoke-direct {v1, p0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView$2;-><init>(Lcom/facebook/slideshow/ui/PlayableSlideshowView;)V

    iget-object v2, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f:LX/K3N;

    iget v2, v2, LX/K3N;->a:I

    iget-object v3, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f:LX/K3N;

    iget v3, v3, LX/K3N;->b:I

    add-int/2addr v2, v3

    int-to-long v2, v2

    iget-object v4, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f:LX/K3N;

    iget v4, v4, LX/K3N;->b:I

    iget-object v5, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f:LX/K3N;

    iget v5, v5, LX/K3N;->c:I

    add-int/2addr v4, v5

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 2765574
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2765558
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->a:LX/3l1;

    .line 2765559
    sget-object v1, LX/BOR;->SLIDESHOW_PREVIEW_STOP:LX/BOR;

    invoke-static {v1}, LX/3l1;->a(LX/BOR;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/3l1;->a(LX/3l1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2765560
    invoke-direct {p0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->i()V

    .line 2765561
    invoke-direct {p0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->j()V

    .line 2765562
    invoke-static {p0}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->k(Lcom/facebook/slideshow/ui/PlayableSlideshowView;)V

    .line 2765563
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->e:LX/K3M;

    if-eqz v0, :cond_0

    .line 2765564
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->e:LX/K3M;

    invoke-interface {v0}, LX/K3M;->d()V

    .line 2765565
    :cond_0
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x24cfb558

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2765555
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2765556
    iget-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->a()V

    .line 2765557
    const/16 v1, 0x2d

    const v2, 0x3764a70

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5f280d63

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2765552
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2765553
    iget-object v1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->b()V

    .line 2765554
    const/16 v1, 0x2d

    const v2, 0x2d5c59db

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 2765549
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishTemporaryDetach()V

    .line 2765550
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->a()V

    .line 2765551
    return-void
.end method

.method public final onMeasure(II)V
    .locals 0

    .prologue
    .line 2765547
    invoke-super {p0, p1, p1}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2765548
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 2765544
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onStartTemporaryDetach()V

    .line 2765545
    iget-object v0, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->g:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->b()V

    .line 2765546
    return-void
.end method

.method public setPlayableListener(LX/K3M;)V
    .locals 0

    .prologue
    .line 2765542
    iput-object p1, p0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->e:LX/K3M;

    .line 2765543
    return-void
.end method
