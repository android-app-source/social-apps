.class public Lcom/facebook/slideshow/ui/SoundListAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/K3U;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Landroid/view/LayoutInflater;

.field public c:LX/23P;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLInterfaces$FBApplicationWithMoodsFragment$MovieFactoryConfig$Moods;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/K3V;

.field private f:Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2765802
    const-class v0, Lcom/facebook/slideshow/ui/SoundListAdapter;

    const-string v1, "slideshow_in_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/slideshow/ui/SoundListAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/23P;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2765803
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2765804
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->b:Landroid/view/LayoutInflater;

    .line 2765805
    iput-object p2, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->c:LX/23P;

    .line 2765806
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2765807
    new-instance v0, LX/K3U;

    iget-object v1, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f03134b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/K3U;-><init>(Lcom/facebook/slideshow/ui/SoundListAdapter;Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2765808
    check-cast p1, LX/K3U;

    const/4 v0, 0x0

    .line 2765809
    iget-object v1, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->d:LX/0Px;

    if-nez v1, :cond_0

    move v1, v0

    .line 2765810
    :goto_0
    if-ne p2, v1, :cond_1

    const/4 v0, 0x1

    move v1, v0

    :goto_1
    if-nez p2, :cond_2

    const/4 v0, 0x0

    .line 2765811
    :goto_2
    iput-object v0, p1, LX/K3U;->m:Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    .line 2765812
    if-nez v0, :cond_3

    .line 2765813
    iget-object v2, p1, LX/K3U;->o:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->setBackgroundColor(I)V

    .line 2765814
    iget-object v2, p1, LX/K3U;->o:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    const/4 v3, 0x0

    sget-object p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2765815
    iget-object v2, p1, LX/K3U;->p:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p1, LX/K3U;->n:Lcom/facebook/slideshow/ui/SoundListAdapter;

    iget-object v3, v3, Lcom/facebook/slideshow/ui/SoundListAdapter;->c:LX/23P;

    iget-object p0, p1, LX/K3U;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f083c2d

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    iget-object p2, p1, LX/K3U;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3, p0, p2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2765816
    :goto_3
    iget-object v2, p1, LX/K3U;->o:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    invoke-virtual {v2, v1}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->setIsSelected(Z)V

    .line 2765817
    iget-object v3, p1, LX/K3U;->p:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v1, :cond_4

    iget-object v2, p1, LX/K3U;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0a008d

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    :goto_4
    invoke-virtual {v3, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2765818
    return-void

    .line 2765819
    :cond_0
    iget-object v1, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->d:LX/0Px;

    iget-object v2, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->f:Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    invoke-virtual {v1, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v1, v0

    .line 2765820
    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->d:LX/0Px;

    add-int/lit8 v2, p2, -0x1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    goto :goto_2

    .line 2765821
    :cond_3
    iget-object v2, p1, LX/K3U;->o:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    iget-object v3, p1, LX/K3U;->q:[I

    iget-object p0, p1, LX/K3U;->n:Lcom/facebook/slideshow/ui/SoundListAdapter;

    iget-object p0, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->d:LX/0Px;

    iget-object p2, p1, LX/K3U;->m:Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    invoke-virtual {p0, p2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result p0

    iget-object p2, p1, LX/K3U;->q:[I

    array-length p2, p2

    rem-int/2addr p0, p2

    aget v3, v3, p0

    invoke-virtual {v2, v3}, Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;->setBackgroundColor(I)V

    .line 2765822
    iget-object v2, p1, LX/K3U;->o:Lcom/facebook/slideshow/ui/SelectableSlideshowThumbnailView;

    iget-object v3, p1, LX/K3U;->r:Landroid/net/Uri;

    sget-object p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2765823
    iget-object v2, p1, LX/K3U;->p:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p1, LX/K3U;->n:Lcom/facebook/slideshow/ui/SoundListAdapter;

    iget-object v3, v3, Lcom/facebook/slideshow/ui/SoundListAdapter;->c:LX/23P;

    invoke-virtual {v0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->d()Ljava/lang/String;

    move-result-object p0

    iget-object p2, p1, LX/K3U;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3, p0, p2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2765824
    :cond_4
    iget-object v2, p1, LX/K3U;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0a00a4

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_4
.end method

.method public final a(Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;)V
    .locals 2
    .param p1    # Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2765825
    iget-object v0, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->d:LX/0Px;

    if-eqz v0, :cond_0

    .line 2765826
    iget-object v0, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->d:LX/0Px;

    iget-object v1, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->f:Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    invoke-virtual {v0, v1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2765827
    iget-object v1, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->d:LX/0Px;

    invoke-virtual {v1, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 2765828
    iput-object p1, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->f:Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    .line 2765829
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->i_(I)V

    .line 2765830
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->i_(I)V

    .line 2765831
    iget-object v0, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->e:LX/K3V;

    iget-object v1, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->f:Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    .line 2765832
    iget-object p0, v0, LX/K3V;->a:LX/K3W;

    iget-object p0, p0, LX/K3W;->c:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    if-eqz p0, :cond_0

    .line 2765833
    iget-object p0, v0, LX/K3V;->a:LX/K3W;

    iget-object p0, p0, LX/K3W;->c:Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;

    invoke-virtual {p0, v1}, Lcom/facebook/slideshow/ui/PlayableSlideshowViewController;->a(Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;)V

    .line 2765834
    :cond_0
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2765835
    iget-object v0, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->d:LX/0Px;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/slideshow/ui/SoundListAdapter;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
