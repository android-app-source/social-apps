.class public Lcom/facebook/slideshow/SlideshowEditFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

.field public b:Lcom/facebook/slideshow/ui/DragSortThumbnailListView;

.field public c:Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator;

.field public d:Lcom/facebook/widget/CustomViewPager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2764879
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xc0aaf3e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2764880
    const v0, 0x7f031346

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2764881
    const v0, 0x7f0d2ca3

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    iput-object v0, p0, Lcom/facebook/slideshow/SlideshowEditFragment;->a:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    .line 2764882
    const v0, 0x7f0d2ca4

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/slideshow/ui/DragSortThumbnailListView;

    iput-object v0, p0, Lcom/facebook/slideshow/SlideshowEditFragment;->b:Lcom/facebook/slideshow/ui/DragSortThumbnailListView;

    .line 2764883
    const v0, 0x7f0d2ca5

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/slideshow/SlideshowEditFragment;->c:Lcom/facebook/fbui/pagerindicator/IconTabbedViewPagerIndicator;

    .line 2764884
    const v0, 0x7f0d2ca6

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    iput-object v0, p0, Lcom/facebook/slideshow/SlideshowEditFragment;->d:Lcom/facebook/widget/CustomViewPager;

    .line 2764885
    const/16 v0, 0x2b

    const v3, 0x15110e34

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x411eb9ec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2764886
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2764887
    iget-object v1, p0, Lcom/facebook/slideshow/SlideshowEditFragment;->a:Lcom/facebook/slideshow/ui/PlayableSlideshowView;

    .line 2764888
    invoke-virtual {v1}, Lcom/facebook/slideshow/ui/PlayableSlideshowView;->f()V

    .line 2764889
    const/16 v1, 0x2b

    const v2, 0x3eb2bcd9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
