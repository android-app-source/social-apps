.class public Lcom/facebook/slideshow/SlideshowEditActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/K2z;

.field public q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

.field private r:LX/K30;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/3l1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:Lcom/facebook/ui/titlebar/Fb4aTitleBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2764770
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2764771
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2764755
    iget-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->u:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v1, 0x7f083c2b

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2764756
    iget-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->u:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/K2s;

    invoke-direct {v1, p0}, LX/K2s;-><init>(Lcom/facebook/slideshow/SlideshowEditActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2764757
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f083c2c

    invoke-virtual {p0, v1}, Lcom/facebook/slideshow/SlideshowEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2764758
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2764759
    move-object v0, v0

    .line 2764760
    const/4 v1, 0x1

    .line 2764761
    iput-boolean v1, v0, LX/108;->q:Z

    .line 2764762
    move-object v0, v0

    .line 2764763
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2764764
    iget-object v1, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->u:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 2764765
    sget-object v0, LX/K2v;->a:[I

    iget-object v1, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-virtual {v1}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getActionWhenDone()LX/B63;

    move-result-object v1

    invoke-virtual {v1}, LX/B63;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2764766
    iget-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->u:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-direct {p0}, Lcom/facebook/slideshow/SlideshowEditActivity;->l()LX/63W;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setOnToolbarButtonListener(LX/63W;)V

    .line 2764767
    :goto_0
    return-void

    .line 2764768
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-virtual {v0}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getComposerConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2764769
    iget-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->u:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-direct {p0}, Lcom/facebook/slideshow/SlideshowEditActivity;->b()LX/63W;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setOnToolbarButtonListener(LX/63W;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lcom/facebook/slideshow/SlideshowEditActivity;LX/K30;LX/3l1;LX/1Kf;)V
    .locals 0

    .prologue
    .line 2764754
    iput-object p1, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->r:LX/K30;

    iput-object p2, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->s:LX/3l1;

    iput-object p3, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->t:LX/1Kf;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/slideshow/SlideshowEditActivity;

    const-class v0, LX/K30;

    invoke-interface {v2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/K30;

    invoke-static {v2}, LX/3l1;->b(LX/0QB;)LX/3l1;

    move-result-object v1

    check-cast v1, LX/3l1;

    invoke-static {v2}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v2

    check-cast v2, LX/1Kf;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/slideshow/SlideshowEditActivity;->a(Lcom/facebook/slideshow/SlideshowEditActivity;LX/K30;LX/3l1;LX/1Kf;)V

    return-void
.end method

.method private b()LX/63W;
    .locals 1

    .prologue
    .line 2764753
    new-instance v0, LX/K2t;

    invoke-direct {v0, p0}, LX/K2t;-><init>(Lcom/facebook/slideshow/SlideshowEditActivity;)V

    return-object v0
.end method

.method private l()LX/63W;
    .locals 1

    .prologue
    .line 2764752
    new-instance v0, LX/K2u;

    invoke-direct {v0, p0}, LX/K2u;-><init>(Lcom/facebook/slideshow/SlideshowEditActivity;)V

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2764706
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2764707
    invoke-static {p0, p0}, Lcom/facebook/slideshow/SlideshowEditActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2764708
    const v0, 0x7f031345

    invoke-virtual {p0, v0}, Lcom/facebook/slideshow/SlideshowEditActivity;->setContentView(I)V

    .line 2764709
    invoke-virtual {p0}, Lcom/facebook/slideshow/SlideshowEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_slideshow_configuration"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    iput-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    .line 2764710
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->u:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2764711
    invoke-direct {p0}, Lcom/facebook/slideshow/SlideshowEditActivity;->a()V

    .line 2764712
    if-eqz p1, :cond_2

    .line 2764713
    const-string v0, "SLIDESHOW_MEDIA"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2764714
    const-string v0, "SLIDESHOW_DATA"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 2764715
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d2ca2

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/slideshow/SlideshowEditFragment;

    .line 2764716
    iget-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->r:LX/K30;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    iget-object v4, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-virtual {v4}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getSessionId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/K30;->a(Lcom/facebook/slideshow/SlideshowEditFragment;LX/0Px;Lcom/facebook/ipc/composer/model/ComposerSlideshowData;Ljava/lang/String;LX/0gc;)LX/K2z;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    .line 2764717
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2764718
    const v2, 0x7f0d2ca4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2764719
    iget-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    .line 2764720
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2764721
    const v3, 0x7f0d2ca4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget-object v3, LX/K2y;->TAB_THUMBNAIL_LIST:LX/K2y;

    invoke-virtual {v0, v2, v3}, LX/K2z;->a(Landroid/view/View;LX/K2y;)V

    .line 2764722
    :cond_0
    iget-object v0, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2764723
    const v2, 0x7f0d2d2c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2764724
    iget-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    .line 2764725
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v2

    .line 2764726
    const v2, 0x7f0d2d2c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget-object v2, LX/K2y;->TAB_SOUND_LIST:LX/K2y;

    invoke-virtual {v0, v1, v2}, LX/K2z;->a(Landroid/view/View;LX/K2y;)V

    .line 2764727
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->s:LX/3l1;

    iget-object v1, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-virtual {v1}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 2764728
    iput-object v1, v0, LX/3l1;->b:Ljava/lang/String;

    .line 2764729
    iget-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->s:LX/3l1;

    iget-object v1, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-virtual {v1}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getSource()LX/B66;

    move-result-object v1

    invoke-virtual {v1}, LX/B66;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2764730
    sget-object v2, LX/BOR;->SLIDESHOW_PREVIEW_SEEN:LX/BOR;

    invoke-static {v2}, LX/3l1;->a(LX/BOR;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "source"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0, v2}, LX/3l1;->a(LX/3l1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2764731
    return-void

    .line 2764732
    :cond_2
    iget-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->r:LX/K30;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const v2, 0x7f0d2ca2

    invoke-virtual {v1, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/slideshow/SlideshowEditFragment;

    iget-object v2, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-virtual {v2}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getMediaItems()LX/0Px;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-virtual {v3}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-virtual {v4}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getSessionId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/K30;->a(Lcom/facebook/slideshow/SlideshowEditFragment;LX/0Px;Lcom/facebook/ipc/composer/model/ComposerSlideshowData;Ljava/lang/String;LX/0gc;)LX/K2z;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2764745
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_1

    .line 2764746
    invoke-virtual {p0, p2, p3}, Lcom/facebook/slideshow/SlideshowEditActivity;->setResult(ILandroid/content/Intent;)V

    .line 2764747
    invoke-virtual {p0}, Lcom/facebook/slideshow/SlideshowEditActivity;->finish()V

    .line 2764748
    :cond_0
    :goto_0
    return-void

    .line 2764749
    :cond_1
    if-eqz p2, :cond_0

    .line 2764750
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2764751
    iget-object v1, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/K2z;->a(LX/0Px;)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 2764740
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2764741
    const-string v1, "extra_media_items"

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->q:Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-virtual {v3}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getMediaItems()LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2764742
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/slideshow/SlideshowEditActivity;->setResult(ILandroid/content/Intent;)V

    .line 2764743
    invoke-virtual {p0}, Lcom/facebook/slideshow/SlideshowEditActivity;->finish()V

    .line 2764744
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2764733
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2764734
    const-string v0, "SLIDESHOW_MEDIA"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    invoke-virtual {v2}, LX/K2z;->a()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2764735
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;

    move-result-object v0

    .line 2764736
    iget-object v1, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    invoke-virtual {v1}, LX/K2z;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2764737
    iget-object v1, p0, Lcom/facebook/slideshow/SlideshowEditActivity;->p:LX/K2z;

    invoke-virtual {v1}, LX/K2z;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;->setMoodId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;

    .line 2764738
    :cond_0
    const-string v1, "SLIDESHOW_DATA"

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2764739
    return-void
.end method
