.class public final Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/K37;
.implements LX/K36;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66fb5842
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2765264
    const-class v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2765263
    const-class v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2765221
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2765222
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2765260
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2765261
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2765262
    :cond_0
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2765258
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->f:Ljava/lang/String;

    .line 2765259
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2765248
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2765249
    invoke-direct {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2765250
    invoke-direct {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2765251
    invoke-virtual {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x40ed712c

    invoke-static {v3, v2, v4}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2765252
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2765253
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2765254
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2765255
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2765256
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2765257
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2765238
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2765239
    invoke-virtual {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2765240
    invoke-virtual {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x40ed712c

    invoke-static {v2, v0, v3}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2765241
    invoke-virtual {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2765242
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;

    .line 2765243
    iput v3, v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->g:I

    .line 2765244
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2765245
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2765246
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2765247
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2765237
    new-instance v0, LX/K39;

    invoke-direct {v0, p1}, LX/K39;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2765236
    invoke-direct {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2765233
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2765234
    const/4 v0, 0x2

    const v1, -0x40ed712c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->g:I

    .line 2765235
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2765231
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2765232
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2765230
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2765227
    new-instance v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;

    invoke-direct {v0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;-><init>()V

    .line 2765228
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2765229
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2765226
    const v0, 0x76fbbf48

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2765225
    const v0, 0x252222

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMovieFactoryConfig"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2765223
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2765224
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$PagesSlideshowAudioQueryModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
