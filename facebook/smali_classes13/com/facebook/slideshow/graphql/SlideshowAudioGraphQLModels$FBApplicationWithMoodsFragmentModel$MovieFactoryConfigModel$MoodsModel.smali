.class public final Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x48326c5b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2765114
    const-class v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2765121
    const-class v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2765119
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2765120
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2765117
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->f:Ljava/lang/String;

    .line 2765118
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2765115
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->i:Ljava/lang/String;

    .line 2765116
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2765073
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->j:Ljava/lang/String;

    .line 2765074
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2765097
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2765098
    invoke-virtual {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2765099
    invoke-direct {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2765100
    invoke-virtual {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2765101
    invoke-direct {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2765102
    invoke-direct {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2765103
    invoke-virtual {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2765104
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2765105
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2765106
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2765107
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2765108
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2765109
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2765110
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2765111
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2765112
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2765113
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2765089
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2765090
    invoke-virtual {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2765091
    invoke-virtual {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2765092
    if-eqz v1, :cond_0

    .line 2765093
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    .line 2765094
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->e:Ljava/util/List;

    .line 2765095
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2765096
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2765122
    invoke-virtual {p0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2765086
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2765087
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->h:Z

    .line 2765088
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2765084
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel$CutdownsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->e:Ljava/util/List;

    .line 2765085
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2765081
    new-instance v0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;

    invoke-direct {v0}, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;-><init>()V

    .line 2765082
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2765083
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2765079
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->g:Ljava/lang/String;

    .line 2765080
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2765077
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->k:Ljava/lang/String;

    .line 2765078
    iget-object v0, p0, Lcom/facebook/slideshow/graphql/SlideshowAudioGraphQLModels$FBApplicationWithMoodsFragmentModel$MovieFactoryConfigModel$MoodsModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2765076
    const v0, -0x22a64e1f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2765075
    const v0, 0x61a31093

    return v0
.end method
