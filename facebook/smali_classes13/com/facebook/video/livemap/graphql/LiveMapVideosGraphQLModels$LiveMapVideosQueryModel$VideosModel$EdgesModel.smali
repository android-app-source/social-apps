.class public final Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x170ea31c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2778466
    const-class v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2778465
    const-class v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2778463
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2778464
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2778467
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2778468
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;->a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2778469
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2778470
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2778471
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2778472
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2778455
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2778456
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;->a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2778457
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;->a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    .line 2778458
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;->a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2778459
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;

    .line 2778460
    iput-object v0, v1, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;->e:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    .line 2778461
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2778462
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778453
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;->e:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;->e:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    .line 2778454
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;->e:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2778450
    new-instance v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel;-><init>()V

    .line 2778451
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2778452
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2778449
    const v0, -0x684862f8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2778448
    const v0, -0x3602ce75

    return v0
.end method
