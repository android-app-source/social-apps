.class public final Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2778103
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2778104
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2778105
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2778106
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 2778107
    if-nez p1, :cond_0

    .line 2778108
    :goto_0
    return v1

    .line 2778109
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2778110
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2778111
    :sswitch_0
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2778112
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2778113
    invoke-virtual {p3, v0}, LX/186;->c(I)V

    move-object v0, p3

    .line 2778114
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2778115
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2778116
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2778117
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2778118
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2778119
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2778120
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2778121
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2778122
    :sswitch_2
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2778123
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2778124
    invoke-virtual {p3, v0}, LX/186;->c(I)V

    move-object v0, p3

    .line 2778125
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2778126
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2778127
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x21c3c385 -> :sswitch_0
        -0x20825d27 -> :sswitch_2
        0x2cc77694 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2778135
    if-nez p0, :cond_0

    move v0, v1

    .line 2778136
    :goto_0
    return v0

    .line 2778137
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2778138
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2778139
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2778140
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2778141
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2778142
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2778143
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2778128
    const/4 v7, 0x0

    .line 2778129
    const/4 v1, 0x0

    .line 2778130
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2778131
    invoke-static {v2, v3, v0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2778132
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2778133
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2778134
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2778147
    new-instance v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2778144
    sparse-switch p0, :sswitch_data_0

    .line 2778145
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2778146
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x21c3c385 -> :sswitch_0
        -0x20825d27 -> :sswitch_0
        0x2cc77694 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2778100
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2778101
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->b(I)V

    .line 2778102
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2778069
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2778070
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2778071
    :cond_0
    iput-object p1, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2778072
    iput p2, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->b:I

    .line 2778073
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2778074
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2778075
    new-instance v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2778076
    iget v0, p0, LX/1vt;->c:I

    .line 2778077
    move v0, v0

    .line 2778078
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2778079
    iget v0, p0, LX/1vt;->c:I

    .line 2778080
    move v0, v0

    .line 2778081
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2778082
    iget v0, p0, LX/1vt;->b:I

    .line 2778083
    move v0, v0

    .line 2778084
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778085
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2778086
    move-object v0, v0

    .line 2778087
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2778088
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2778089
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2778090
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2778091
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2778092
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2778093
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2778094
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2778095
    invoke-static {v3, v9, v2}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2778096
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2778097
    iget v0, p0, LX/1vt;->c:I

    .line 2778098
    move v0, v0

    .line 2778099
    return v0
.end method
