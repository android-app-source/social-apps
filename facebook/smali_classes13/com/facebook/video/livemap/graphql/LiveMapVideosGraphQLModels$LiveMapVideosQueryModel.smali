.class public final Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1f7bdeca
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2778529
    const-class v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2778528
    const-class v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2778526
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2778527
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2778520
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2778521
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;->a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2778522
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2778523
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2778524
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2778525
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2778512
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2778513
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;->a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2778514
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;->a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    .line 2778515
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;->a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2778516
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;

    .line 2778517
    iput-object v0, v1, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;->e:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    .line 2778518
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2778519
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVideos"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778505
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;->e:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    iput-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;->e:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    .line 2778506
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;->e:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2778509
    new-instance v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;

    invoke-direct {v0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel;-><init>()V

    .line 2778510
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2778511
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2778508
    const v0, 0x6eae45f9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2778507
    const v0, 0x611e8180

    return v0
.end method
