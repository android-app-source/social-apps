.class public final Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x32c49221
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2778329
    const-class v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2778330
    const-class v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2778331
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2778332
    return-void
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778375
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->o:Ljava/lang/String;

    .line 2778376
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->o:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2778333
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2778334
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2778335
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2778336
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->l()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2778337
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2778338
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2cc77694

    invoke-static {v2, v0, v3}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2778339
    invoke-direct {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2778340
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2778341
    iget-wide v2, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2778342
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2778343
    const/4 v0, 0x2

    iget v2, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->g:I

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 2778344
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2778345
    const/4 v0, 0x4

    iget-boolean v2, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->i:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 2778346
    const/4 v0, 0x5

    iget-boolean v2, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->j:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 2778347
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2778348
    const/4 v0, 0x7

    iget v2, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->l:I

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 2778349
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2778350
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2778351
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 2778352
    const/16 v0, 0xb

    iget v2, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->p:I

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 2778353
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2778354
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2778355
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2778356
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2778357
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2778358
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2778359
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    .line 2778360
    iput-object v0, v1, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2778361
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->l()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2778362
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->l()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    .line 2778363
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->l()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2778364
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    .line 2778365
    iput-object v0, v1, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->k:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    .line 2778366
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2778367
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2cc77694

    invoke-static {v2, v0, v3}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2778368
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2778369
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    .line 2778370
    iput v3, v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->n:I

    move-object v1, v0

    .line 2778371
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2778372
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2778373
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2778374
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778319
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2778320
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2778321
    const-wide/16 v0, 0x0

    invoke-virtual {p1, p2, v2, v0, v1}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->e:J

    .line 2778322
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->g:I

    .line 2778323
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->i:Z

    .line 2778324
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->j:Z

    .line 2778325
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->l:I

    .line 2778326
    const/16 v0, 0x9

    const v1, 0x2cc77694

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->n:I

    .line 2778327
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->p:I

    .line 2778328
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2778316
    new-instance v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    invoke-direct {v0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;-><init>()V

    .line 2778317
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2778318
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2778315
    const v0, -0x412e4751

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2778314
    const v0, 0x4ed245b

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778312
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2778313
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778310
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->h:Ljava/lang/String;

    .line 2778311
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778308
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->k:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->k:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    .line 2778309
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->k:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel$OwnerModel;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778306
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->m:Ljava/lang/String;

    .line 2778307
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSavableDescription"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2778304
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2778305
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;->n:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
