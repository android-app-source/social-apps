.class public final Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x15753b44
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I

.field private k:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2778436
    const-class v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2778435
    const-class v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2778433
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2778434
    return-void
.end method

.method private o()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerLocations"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2778431
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->k:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x6

    const v4, -0x20825d27

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->k:LX/3Sb;

    .line 2778432
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->k:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2778414
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2778415
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x21c3c385

    invoke-static {v1, v0, v2}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2778416
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2778417
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2778418
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2778419
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->m()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2778420
    invoke-direct {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->o()LX/2uF;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v5

    .line 2778421
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2778422
    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 2778423
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2778424
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2778425
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2778426
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2778427
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->j:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 2778428
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2778429
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2778430
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2778394
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2778395
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2778396
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x21c3c385

    invoke-static {v2, v0, v3}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2778397
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2778398
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    .line 2778399
    iput v3, v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->e:I

    move-object v1, v0

    .line 2778400
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->m()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2778401
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->m()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    .line 2778402
    invoke-virtual {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->m()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2778403
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    .line 2778404
    iput-object v0, v1, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->i:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    .line 2778405
    :cond_1
    invoke-direct {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->o()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2778406
    invoke-direct {p0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->o()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 2778407
    if-eqz v2, :cond_2

    .line 2778408
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    .line 2778409
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->k:LX/3Sb;

    move-object v1, v0

    .line 2778410
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2778411
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2778412
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2778413
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2778392
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2778393
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2778437
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2778438
    const v0, -0x21c3c385

    invoke-static {p1, p2, v2, v0}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->e:I

    .line 2778439
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->j:I

    .line 2778440
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2778389
    new-instance v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;-><init>()V

    .line 2778390
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2778391
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2778388
    const v0, -0x7b7e8bc0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2778387
    const v0, 0x7aa2b97

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778377
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 2778378
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778385
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 2778386
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778383
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 2778384
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVideo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2778381
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->i:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    iput-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->i:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    .line 2778382
    iget-object v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->i:Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel$VideoModel;

    return-object v0
.end method

.method public final n()I
    .locals 2

    .prologue
    .line 2778379
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2778380
    iget v0, p0, Lcom/facebook/video/livemap/graphql/LiveMapVideosGraphQLModels$LiveMapVideosQueryModel$VideosModel$EdgesModel$NodeModel;->j:I

    return v0
.end method
