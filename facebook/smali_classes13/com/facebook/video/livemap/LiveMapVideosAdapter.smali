.class public Lcom/facebook/video/livemap/LiveMapVideosAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/K9k;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final a:LX/1qa;

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/K9l;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2777842
    const-class v0, Lcom/facebook/video/livemap/LiveMapVideosAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/livemap/LiveMapVideosAdapter;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1qa;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2777843
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2777844
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/video/livemap/LiveMapVideosAdapter;->c:Ljava/util/ArrayList;

    .line 2777845
    iput-object p1, p0, Lcom/facebook/video/livemap/LiveMapVideosAdapter;->a:LX/1qa;

    .line 2777846
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2777847
    new-instance v0, LX/K9k;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030a1a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/K9k;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 8

    .prologue
    .line 2777848
    check-cast p1, LX/K9k;

    const/16 v3, 0x8

    const/4 v7, 0x0

    .line 2777849
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapVideosAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K9l;

    .line 2777850
    iget-object v1, p1, LX/K9k;->l:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2777851
    iget-object v1, v0, LX/K9l;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2777852
    iget-object v1, p1, LX/K9k;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2777853
    :goto_0
    iget-object v1, v0, LX/K9l;->m:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2777854
    iget-object v1, p1, LX/K9k;->q:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/EllipsizingTextView;->setVisibility(I)V

    .line 2777855
    :goto_1
    iget-object v1, v0, LX/K9l;->l:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2777856
    iget-object v1, p1, LX/K9k;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2777857
    :goto_2
    iget-object v1, p1, LX/K9k;->r:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p1, LX/K9k;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0195

    iget v4, v0, LX/K9l;->n:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, v0, LX/K9l;->n:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2777858
    iget-object v1, p1, LX/K9k;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, v0, LX/K9l;->j:Landroid/net/Uri;

    sget-object v3, Lcom/facebook/video/livemap/LiveMapVideosAdapter;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2777859
    iget-object v1, p1, LX/K9k;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, v0, LX/K9l;->i:Landroid/net/Uri;

    sget-object v3, Lcom/facebook/video/livemap/LiveMapVideosAdapter;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2777860
    iget-object v1, p1, LX/K9k;->l:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2777861
    iput-object p1, v0, LX/K9l;->q:LX/K9k;

    .line 2777862
    return-void

    .line 2777863
    :cond_0
    iget-object v1, p1, LX/K9k;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v7}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2777864
    iget-object v1, p1, LX/K9k;->o:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, v0, LX/K9l;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2777865
    :cond_1
    iget-object v1, p1, LX/K9k;->q:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-virtual {v1, v7}, Lcom/facebook/resources/ui/EllipsizingTextView;->setVisibility(I)V

    .line 2777866
    iget-object v1, p1, LX/K9k;->q:Lcom/facebook/resources/ui/EllipsizingTextView;

    iget-object v2, v0, LX/K9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/EllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2777867
    :cond_2
    iget-object v1, p1, LX/K9k;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v7}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2777868
    iget-object v1, p1, LX/K9k;->p:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, v0, LX/K9l;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2777869
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapVideosAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const v0, -0x6b29c8b9

    invoke-static {v4, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2777870
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K9l;

    .line 2777871
    iget-object v0, v0, LX/K9l;->p:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2777872
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 2777873
    if-eqz v2, :cond_0

    invoke-static {v2}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2777874
    :cond_0
    const v0, -0x4d9cb1e6

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2777875
    :goto_0
    return-void

    .line 2777876
    :cond_1
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2777877
    invoke-virtual {v0, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2777878
    iget-object v6, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 2777879
    move-object v9, v6

    check-cast v9, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2777880
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    invoke-static {v6}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v10

    .line 2777881
    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v6

    .line 2777882
    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    .line 2777883
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2777884
    invoke-static {v7}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v7

    .line 2777885
    new-instance v8, LX/0AW;

    invoke-direct {v8, v6}, LX/0AW;-><init>(LX/162;)V

    .line 2777886
    iput-boolean v7, v8, LX/0AW;->d:Z

    .line 2777887
    move-object v6, v8

    .line 2777888
    invoke-virtual {v6}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v8

    .line 2777889
    new-instance v6, LX/395;

    new-instance v7, LX/0AV;

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v7, v11}, LX/0AV;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, LX/0AV;->a()Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    move-result-object v7

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v9

    .line 2777890
    iget-object v11, p0, Lcom/facebook/video/livemap/LiveMapVideosAdapter;->a:LX/1qa;

    sget-object v12, LX/26P;->Video:LX/26P;

    invoke-virtual {v11, v9, v12}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v11

    move-object v9, v11

    .line 2777891
    move-object v11, v0

    invoke-direct/range {v6 .. v11}, LX/395;-><init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2777892
    sget-object v7, LX/04D;->LIVE_MAP:LX/04D;

    invoke-virtual {v6, v7}, LX/395;->a(LX/04D;)LX/395;

    .line 2777893
    move-object v2, v6

    .line 2777894
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, LX/0f8;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f8;

    .line 2777895
    if-nez v0, :cond_2

    .line 2777896
    const v0, -0x48855592

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2777897
    :cond_2
    invoke-interface {v0}, LX/0f8;->i()LX/0hE;

    move-result-object v0

    .line 2777898
    invoke-interface {v0, v5}, LX/0hE;->setAllowLooping(Z)V

    .line 2777899
    invoke-interface {v0, v2}, LX/0hE;->a(LX/395;)V

    .line 2777900
    const v0, -0x24a68eac

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
