.class public Lcom/facebook/video/livemap/LiveMapFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements LX/6Zz;
.implements LX/2ec;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->LIVE_MAP_FRAGMENT:LX/0cQ;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/03V;

.field public c:LX/0y2;

.field public d:LX/K9f;

.field public e:Lcom/facebook/maps/FbMapViewDelegate;

.field private f:LX/680;

.field public g:LX/K9m;

.field public h:Lcom/facebook/video/livemap/LiveMapVideosAdapter;

.field public i:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

.field private k:Landroid/view/View;

.field private l:Landroid/widget/Toast;

.field private m:I

.field public final n:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/K9l;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2777816
    const-class v0, Lcom/facebook/video/livemap/LiveMapFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/livemap/LiveMapFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2777814
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2777815
    new-instance v0, LX/K9g;

    invoke-direct {v0, p0}, LX/K9g;-><init>(Lcom/facebook/video/livemap/LiveMapFragment;)V

    iput-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->n:Ljava/util/Comparator;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/video/livemap/LiveMapFragment;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v3

    check-cast v3, LX/0y2;

    new-instance v5, LX/K9f;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {v5, v4}, LX/K9f;-><init>(LX/0tX;)V

    move-object v4, v5

    check-cast v4, LX/K9f;

    new-instance p1, Lcom/facebook/video/livemap/LiveMapVideosAdapter;

    invoke-static {p0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v5

    check-cast v5, LX/1qa;

    invoke-direct {p1, v5}, Lcom/facebook/video/livemap/LiveMapVideosAdapter;-><init>(LX/1qa;)V

    move-object v5, p1

    check-cast v5, Lcom/facebook/video/livemap/LiveMapVideosAdapter;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    iput-object v2, v1, Lcom/facebook/video/livemap/LiveMapFragment;->b:LX/03V;

    iput-object v3, v1, Lcom/facebook/video/livemap/LiveMapFragment;->c:LX/0y2;

    iput-object v4, v1, Lcom/facebook/video/livemap/LiveMapFragment;->d:LX/K9f;

    iput-object v5, v1, Lcom/facebook/video/livemap/LiveMapFragment;->h:Lcom/facebook/video/livemap/LiveMapVideosAdapter;

    iput-object p0, v1, Lcom/facebook/video/livemap/LiveMapFragment;->i:LX/1Ck;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/livemap/LiveMapFragment;Ljava/lang/Throwable;)V
    .locals 3
    .param p0    # Lcom/facebook/video/livemap/LiveMapFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2777807
    invoke-static {p0}, Lcom/facebook/video/livemap/LiveMapFragment;->b(Lcom/facebook/video/livemap/LiveMapFragment;)V

    .line 2777808
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->l:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 2777809
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f083c40

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->l:Landroid/widget/Toast;

    .line 2777810
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->l:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2777811
    if-eqz p1, :cond_1

    .line 2777812
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->b:LX/03V;

    sget-object v1, Lcom/facebook/video/livemap/LiveMapFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2777813
    :cond_1
    return-void
.end method

.method public static b(Lcom/facebook/video/livemap/LiveMapFragment;)V
    .locals 4

    .prologue
    .line 2777805
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0079

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2777806
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 2777801
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->g:LX/K9m;

    iget-object v1, p0, Lcom/facebook/video/livemap/LiveMapFragment;->h:Lcom/facebook/video/livemap/LiveMapVideosAdapter;

    .line 2777802
    iget-object p0, v1, Lcom/facebook/video/livemap/LiveMapVideosAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/K9l;

    move-object v1, p0

    .line 2777803
    invoke-virtual {v0, v1}, LX/K9m;->a(LX/K9l;)V

    .line 2777804
    return-void
.end method

.method public final a(LX/6al;)V
    .locals 8

    .prologue
    .line 2777791
    iget-object v0, p1, LX/6al;->a:LX/680;

    move-object v0, v0

    .line 2777792
    iput-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->f:LX/680;

    .line 2777793
    new-instance v0, LX/K9m;

    iget-object v1, p0, Lcom/facebook/video/livemap/LiveMapFragment;->f:LX/680;

    invoke-direct {v0, v1}, LX/K9m;-><init>(LX/680;)V

    iput-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->g:LX/K9m;

    .line 2777794
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->g:LX/K9m;

    .line 2777795
    iput-object p0, v0, LX/K9m;->I:Lcom/facebook/video/livemap/LiveMapFragment;

    .line 2777796
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->f:LX/680;

    iget-object v1, p0, Lcom/facebook/video/livemap/LiveMapFragment;->g:LX/K9m;

    invoke-virtual {v0, v1}, LX/680;->a(LX/67m;)LX/67m;

    .line 2777797
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->c:LX/0y2;

    invoke-virtual {v0}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 2777798
    if-eqz v0, :cond_0

    .line 2777799
    iget-object v1, p0, Lcom/facebook/video/livemap/LiveMapFragment;->f:LX/680;

    new-instance v2, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-static {v2}, LX/67e;->a(Lcom/facebook/android/maps/model/LatLng;)LX/67d;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/680;->b(LX/67d;)V

    .line 2777800
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2777782
    const-class v0, Lcom/facebook/video/livemap/LiveMapFragment;

    invoke-static {v0, p0}, Lcom/facebook/video/livemap/LiveMapFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2777783
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2777784
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->i:LX/1Ck;

    const-string v1, "LiveMapQuery"

    iget-object v2, p0, Lcom/facebook/video/livemap/LiveMapFragment;->d:LX/K9f;

    const/16 v7, 0xc8

    .line 2777785
    new-instance v4, LX/K9n;

    invoke-direct {v4}, LX/K9n;-><init>()V

    move-object v4, v4

    .line 2777786
    const-string v5, "video_count"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2777787
    const-string v5, "limit"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2777788
    iget-object v5, v2, LX/K9f;->a:LX/0tX;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v6, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v4, v6}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v4

    sget-object v6, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    const-wide/16 v6, 0xa

    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    move-object v2, v4

    .line 2777789
    new-instance v3, LX/K9h;

    invoke-direct {v3, p0}, LX/K9h;-><init>(Lcom/facebook/video/livemap/LiveMapFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2777790
    return-void
.end method

.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2777781
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 2777740
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2777741
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2777780
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2777779
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1a94fce3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2777769
    const v0, 0x7f030a1b

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2777770
    const v0, 0x7f0d1990

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbMapViewDelegate;

    iput-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->e:Lcom/facebook/maps/FbMapViewDelegate;

    .line 2777771
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p3}, LX/6Zn;->a(Landroid/os/Bundle;)V

    .line 2777772
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p0}, LX/6Zn;->a(LX/6Zz;)V

    .line 2777773
    const v0, 0x7f0d1991

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iput-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2777774
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2777775
    iput-object p0, v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->n:LX/2ec;

    .line 2777776
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v3, p0, Lcom/facebook/video/livemap/LiveMapFragment;->h:Lcom/facebook/video/livemap/LiveMapVideosAdapter;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2777777
    const v0, 0x7f0d1992

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->k:Landroid/view/View;

    .line 2777778
    const/16 v0, 0x2b

    const v3, -0x2ce5b08e

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7b841c9e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2777765
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2777766
    iget-object v1, p0, Lcom/facebook/video/livemap/LiveMapFragment;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v1}, LX/6Zn;->a()V

    .line 2777767
    iget-object v1, p0, Lcom/facebook/video/livemap/LiveMapFragment;->i:LX/1Ck;

    const-string v2, "LiveMapQuery"

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2777768
    const/16 v1, 0x2b

    const v2, -0x649fe52b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 2777762
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onLowMemory()V

    .line 2777763
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->b()V

    .line 2777764
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x425903e5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2777757
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2777758
    iget v1, p0, Lcom/facebook/video/livemap/LiveMapFragment;->m:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 2777759
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/facebook/video/livemap/LiveMapFragment;->m:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2777760
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/livemap/LiveMapFragment;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v1}, LX/6Zn;->c()V

    .line 2777761
    const/16 v1, 0x2b

    const v2, -0x45df9cb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v0, 0x2a

    const v1, -0x9dee0d7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2777750
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2777751
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 2777752
    invoke-virtual {v1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v2

    iput v2, p0, Lcom/facebook/video/livemap/LiveMapFragment;->m:I

    .line 2777753
    iget v2, p0, Lcom/facebook/video/livemap/LiveMapFragment;->m:I

    if-eq v2, v3, :cond_0

    .line 2777754
    invoke-virtual {v1, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2777755
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/livemap/LiveMapFragment;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v1}, LX/6Zn;->d()V

    .line 2777756
    const/16 v1, 0x2b

    const v2, 0x7127df3f

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2777747
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2777748
    iget-object v0, p0, Lcom/facebook/video/livemap/LiveMapFragment;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p1}, LX/6Zn;->b(Landroid/os/Bundle;)V

    .line 2777749
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4d7ad40d    # 2.6301256E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2777742
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2777743
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2777744
    if-eqz v0, :cond_0

    .line 2777745
    const v2, 0x7f083c3f

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2777746
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x126d5717

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
