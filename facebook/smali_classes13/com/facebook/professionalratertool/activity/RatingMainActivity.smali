.class public Lcom/facebook/professionalratertool/activity/RatingMainActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field private A:LX/0h5;

.field public B:LX/Jxf;

.field public C:Lcom/facebook/widget/ListViewFriendlyViewPager;

.field private D:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private E:LX/0g8;

.field public F:LX/J82;

.field public G:LX/Jxh;

.field private H:Lcom/facebook/resources/ui/FbTextView;

.field private I:Lcom/facebook/resources/ui/FbTextView;

.field public p:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/J7f;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/1Db;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/1DS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/J8C;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public v:LX/Jxi;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public w:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public x:LX/1Qq;

.field private y:LX/JxZ;

.field private z:LX/62C;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2753175
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2753176
    return-void
.end method

.method private a(LX/J8B;)LX/1Qq;
    .locals 3

    .prologue
    .line 2753171
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->s:LX/1DS;

    iget-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->t:LX/0Ot;

    iget-object v2, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->F:LX/J82;

    invoke-virtual {v0, v1, v2}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v0

    .line 2753172
    iput-object p1, v0, LX/1Ql;->f:LX/1PW;

    .line 2753173
    move-object v0, v0

    .line 2753174
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2753184
    const v0, 0x7f0310f6

    invoke-virtual {p0, v0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->setContentView(I)V

    .line 2753185
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2753186
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->A:LX/0h5;

    .line 2753187
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->A:LX/0h5;

    new-instance v1, LX/JxR;

    invoke-direct {v1, p0}, LX/JxR;-><init>(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2753188
    const v0, 0x7f0d284d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->D:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2753189
    const v0, 0x7f0d284f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->H:Lcom/facebook/resources/ui/FbTextView;

    .line 2753190
    const v0, 0x7f0d2850

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->I:Lcom/facebook/resources/ui/FbTextView;

    .line 2753191
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->I:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/JxT;

    invoke-direct {v1, p0}, LX/JxT;-><init>(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2753192
    invoke-direct {p0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->b()LX/0g8;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->E:LX/0g8;

    .line 2753193
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->E:LX/0g8;

    iget-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->r:LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(LX/1St;)V

    .line 2753194
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->E:LX/0g8;

    iget-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->D:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-interface {v0, v1}, LX/0g8;->f(Landroid/view/View;)V

    .line 2753195
    return-void
.end method

.method private static a(Lcom/facebook/professionalratertool/activity/RatingMainActivity;LX/03V;LX/J7f;LX/1Db;LX/1DS;LX/0Ot;LX/J8C;LX/Jxi;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/professionalratertool/activity/RatingMainActivity;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/J7f;",
            "LX/1Db;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;",
            "LX/J8C;",
            "LX/Jxi;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2753196
    iput-object p1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->p:LX/03V;

    iput-object p2, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->q:LX/J7f;

    iput-object p3, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->r:LX/1Db;

    iput-object p4, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->s:LX/1DS;

    iput-object p5, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->t:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->u:LX/J8C;

    iput-object p7, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->v:LX/Jxi;

    iput-object p8, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;

    invoke-static {v8}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v8}, LX/J7f;->b(LX/0QB;)LX/J7f;

    move-result-object v2

    check-cast v2, LX/J7f;

    invoke-static {v8}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v3

    check-cast v3, LX/1Db;

    invoke-static {v8}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v4

    check-cast v4, LX/1DS;

    const/16 v5, 0x6bd

    invoke-static {v8, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const-class v6, LX/J8C;

    invoke-interface {v8, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/J8C;

    new-instance p0, LX/Jxi;

    invoke-static {v8}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-direct {p0, v7}, LX/Jxi;-><init>(LX/0Zb;)V

    move-object v7, p0

    check-cast v7, LX/Jxi;

    invoke-static {v8}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v0 .. v8}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->a(Lcom/facebook/professionalratertool/activity/RatingMainActivity;LX/03V;LX/J7f;LX/1Db;LX/1DS;LX/0Ot;LX/J8C;LX/Jxi;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    return-void
.end method

.method private b()LX/0g8;
    .locals 4

    .prologue
    .line 2753197
    const v0, 0x7f0d2851

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2753198
    new-instance v1, LX/1Oz;

    invoke-direct {v1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2753199
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2753200
    const v2, 0x7f0310bc

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2753201
    const v2, 0x7f0d27c8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/ListViewFriendlyViewPager;

    iput-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->C:Lcom/facebook/widget/ListViewFriendlyViewPager;

    .line 2753202
    new-instance v1, LX/0g7;

    invoke-direct {v1, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    return-object v1
.end method

.method public static l(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2753203
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->I:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2753204
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->H:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2753205
    return-void
.end method

.method public static m(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V
    .locals 0

    .prologue
    .line 2753206
    invoke-direct {p0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->p()V

    .line 2753207
    invoke-direct {p0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->r()V

    .line 2753208
    invoke-direct {p0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->q()V

    .line 2753209
    return-void
.end method

.method public static n(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V
    .locals 4

    .prologue
    .line 2753210
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2753211
    const v1, 0x7f083c16

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f083c17

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    const v2, 0x7f083c18

    new-instance v3, LX/JxU;

    invoke-direct {v3, p0}, LX/JxU;-><init>(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2753212
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2753213
    return-void
.end method

.method public static o(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V
    .locals 3

    .prologue
    .line 2753177
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->G:LX/Jxh;

    .line 2753178
    iget-object v1, v0, LX/Jxh;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/Jxh;->j:LX/0Tn;

    iget p0, v0, LX/Jxh;->g:I

    invoke-interface {v1, v2, p0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2753179
    iget v1, v0, LX/Jxh;->g:I

    iget-object v2, v0, LX/Jxh;->e:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2753180
    iget-object v1, v0, LX/Jxh;->f:LX/JxY;

    invoke-virtual {v1}, LX/JxY;->b()V

    .line 2753181
    :cond_0
    :goto_0
    return-void

    .line 2753182
    :cond_1
    iget v1, v0, LX/Jxh;->g:I

    iget-object v2, v0, LX/Jxh;->e:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 2753183
    iget-object v1, v0, LX/Jxh;->f:LX/JxY;

    invoke-virtual {v1}, LX/JxY;->c()V

    goto :goto_0
.end method

.method private p()V
    .locals 4

    .prologue
    .line 2753163
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->H:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ad "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->G:LX/Jxh;

    .line 2753164
    iget v3, v2, LX/Jxh;->g:I

    move v2, v3

    .line 2753165
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->G:LX/Jxh;

    .line 2753166
    iget-object v3, v2, LX/Jxh;->e:LX/0Px;

    if-eqz v3, :cond_0

    .line 2753167
    iget-object v3, v2, LX/Jxh;->e:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    .line 2753168
    :goto_0
    move v2, v3

    .line 2753169
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2753170
    return-void

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 2753160
    invoke-direct {p0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->s()LX/Jxf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->B:LX/Jxf;

    .line 2753161
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->C:Lcom/facebook/widget/ListViewFriendlyViewPager;

    iget-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->B:LX/Jxf;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2753162
    return-void
.end method

.method private r()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2753146
    invoke-direct {p0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->v()V

    .line 2753147
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->x:LX/1Qq;

    if-nez v0, :cond_0

    .line 2753148
    invoke-direct {p0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->w()LX/J8B;

    move-result-object v0

    .line 2753149
    invoke-direct {p0, v0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->a(LX/J8B;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->x:LX/1Qq;

    .line 2753150
    invoke-direct {p0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->s()LX/Jxf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->B:LX/Jxf;

    .line 2753151
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->C:Lcom/facebook/widget/ListViewFriendlyViewPager;

    iget-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->B:LX/Jxf;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2753152
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->C:Lcom/facebook/widget/ListViewFriendlyViewPager;

    .line 2753153
    iput-boolean v2, v0, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 2753154
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->C:Lcom/facebook/widget/ListViewFriendlyViewPager;

    iget-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->B:LX/Jxf;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2753155
    new-instance v0, LX/JxZ;

    iget-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->C:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-direct {v0, v1}, LX/JxZ;-><init>(Lcom/facebook/widget/ListViewFriendlyViewPager;)V

    iput-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->y:LX/JxZ;

    .line 2753156
    const/4 v0, 0x2

    new-array v0, v0, [LX/1Cw;

    iget-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->x:LX/1Qq;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->y:LX/JxZ;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/62C;->b([LX/1Cw;)LX/62C;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->z:LX/62C;

    .line 2753157
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->E:LX/0g8;

    iget-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->z:LX/62C;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2753158
    :goto_0
    return-void

    .line 2753159
    :cond_0
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->x:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private s()LX/Jxf;
    .locals 2

    .prologue
    .line 2753140
    new-instance v0, LX/Jxf;

    invoke-direct {v0, p0}, LX/Jxf;-><init>(Landroid/content/Context;)V

    .line 2753141
    new-instance v1, LX/JxV;

    invoke-direct {v1, p0}, LX/JxV;-><init>(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V

    .line 2753142
    iput-object v1, v0, LX/Jxf;->d:LX/JxV;

    .line 2753143
    new-instance v1, LX/JxW;

    invoke-direct {v1, p0}, LX/JxW;-><init>(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V

    .line 2753144
    iput-object v1, v0, LX/Jxf;->c:LX/JxW;

    .line 2753145
    return-object v0
.end method

.method public static t(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)Z
    .locals 5

    .prologue
    .line 2753104
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->B:LX/Jxf;

    .line 2753105
    iget-object v1, v0, LX/Jxf;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jxl;

    .line 2753106
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2753107
    sget-object p0, LX/Jxj;->a:[I

    iget-object v0, v1, LX/Jxl;->b:LX/Jxk;

    invoke-virtual {v0}, LX/Jxk;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_0

    move v3, v4

    .line 2753108
    :cond_1
    :goto_0
    move v1, v3

    .line 2753109
    if-nez v1, :cond_0

    .line 2753110
    const/4 v1, 0x0

    .line 2753111
    :goto_1
    move v0, v1

    .line 2753112
    if-eqz v0, :cond_2

    .line 2753113
    const/4 v0, 0x1

    .line 2753114
    :goto_2
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v1, 0x1

    goto :goto_1

    .line 2753115
    :pswitch_0
    iget p0, v1, LX/Jxl;->c:I

    if-nez p0, :cond_1

    move v3, v4

    goto :goto_0

    .line 2753116
    :pswitch_1
    iget-object p0, v1, LX/Jxl;->a:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_1

    move v3, v4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static u(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V
    .locals 3

    .prologue
    .line 2753135
    invoke-virtual {p0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 2753136
    if-eqz v1, :cond_0

    .line 2753137
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2753138
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2753139
    :cond_0
    return-void
.end method

.method private v()V
    .locals 2

    .prologue
    .line 2753131
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->F:LX/J82;

    if-nez v0, :cond_0

    .line 2753132
    new-instance v0, LX/J82;

    iget-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->G:LX/Jxh;

    invoke-virtual {v1}, LX/Jxh;->c()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/J82;-><init>(LX/0Px;)V

    iput-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->F:LX/J82;

    .line 2753133
    :goto_0
    return-void

    .line 2753134
    :cond_0
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->F:LX/J82;

    iget-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->G:LX/Jxh;

    invoke-virtual {v1}, LX/Jxh;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/J82;->a(LX/0Px;)V

    goto :goto_0
.end method

.method private w()LX/J8B;
    .locals 3

    .prologue
    .line 2753128
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->u:LX/J8C;

    .line 2753129
    sget-object v1, LX/J8D;->a:LX/J8D;

    move-object v1, v1

    .line 2753130
    new-instance v2, Lcom/facebook/professionalratertool/activity/RatingMainActivity$6;

    invoke-direct {v2, p0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity$6;-><init>(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V

    invoke-virtual {v0, p0, v1, v2}, LX/J8C;->a(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;)LX/J8B;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2753119
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2753120
    invoke-static {p0, p0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2753121
    new-instance v0, LX/Jxh;

    iget-object v1, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->p:LX/03V;

    iget-object v2, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->q:LX/J7f;

    iget-object v3, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v0, v1, v2, v3}, LX/Jxh;-><init>(LX/03V;LX/J7f;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    iput-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->G:LX/Jxh;

    .line 2753122
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->G:LX/Jxh;

    new-instance v1, LX/JxY;

    invoke-direct {v1, p0}, LX/JxY;-><init>(Lcom/facebook/professionalratertool/activity/RatingMainActivity;)V

    .line 2753123
    iput-object v1, v0, LX/Jxh;->f:LX/JxY;

    .line 2753124
    invoke-direct {p0}, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->a()V

    .line 2753125
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->G:LX/Jxh;

    invoke-virtual {v0}, LX/Jxh;->a()V

    .line 2753126
    iget-object v0, p0, Lcom/facebook/professionalratertool/activity/RatingMainActivity;->D:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2753127
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x2c72a974

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2753117
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 2753118
    const/16 v1, 0x23

    const v2, -0x6ac36d17

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
