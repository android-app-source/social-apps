.class public Lcom/facebook/maps/components/PrefetchMapComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2718266
    const-class v0, Lcom/facebook/maps/rows/MapPartDefinition;

    const-string v1, "newsfeed_map_view"

    const-string v2, "map"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/maps/components/PrefetchMapComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2718267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2718268
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2718269
    iput-object v0, p0, Lcom/facebook/maps/components/PrefetchMapComponentSpec;->b:LX/0Ot;

    .line 2718270
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/maps/components/PrefetchMapComponentSpec;
    .locals 4

    .prologue
    .line 2718271
    const-class v1, Lcom/facebook/maps/components/PrefetchMapComponentSpec;

    monitor-enter v1

    .line 2718272
    :try_start_0
    sget-object v0, Lcom/facebook/maps/components/PrefetchMapComponentSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2718273
    sput-object v2, Lcom/facebook/maps/components/PrefetchMapComponentSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2718274
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2718275
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2718276
    new-instance v3, Lcom/facebook/maps/components/PrefetchMapComponentSpec;

    invoke-direct {v3}, Lcom/facebook/maps/components/PrefetchMapComponentSpec;-><init>()V

    .line 2718277
    const/16 p0, 0x2be

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2718278
    iput-object p0, v3, Lcom/facebook/maps/components/PrefetchMapComponentSpec;->b:LX/0Ot;

    .line 2718279
    move-object v0, v3

    .line 2718280
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2718281
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/maps/components/PrefetchMapComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2718282
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2718283
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/res/Resources;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;IILX/1Pt;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;",
            "IITE;)V"
        }
    .end annotation

    .prologue
    .line 2718284
    const/4 v2, 0x2

    iget-object v0, p0, Lcom/facebook/maps/components/PrefetchMapComponentSpec;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/0sI;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    move v0, p3

    move v1, p4

    move-object v3, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, LX/3BP;->a(IIILandroid/content/res/Resources;Ljava/lang/String;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)Landroid/net/Uri;

    move-result-object v0

    .line 2718285
    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    sget-object v1, Lcom/facebook/maps/components/PrefetchMapComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p5, v0, v1}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2718286
    return-void
.end method


# virtual methods
.method public final a(LX/1De;IILcom/facebook/feed/rows/core/props/FeedProps;LX/1Pt;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;Z)LX/1Dg;
    .locals 6
    .param p4    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1Pt;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;",
            "Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2718287
    invoke-static {p2}, LX/1mh;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3}, LX/1mh;->a(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 2718288
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unspecified height or width not supported!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2718289
    :cond_1
    if-nez p4, :cond_2

    .line 2718290
    const/4 v0, 0x0

    .line 2718291
    :goto_0
    return-object v0

    .line 2718292
    :cond_2
    invoke-static {p2}, LX/1mh;->b(I)I

    move-result v3

    .line 2718293
    invoke-static {p3}, LX/1mh;->b(I)I

    move-result v4

    .line 2718294
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move-object v0, p0

    move-object v2, p6

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/maps/components/PrefetchMapComponentSpec;->a(Landroid/content/res/Resources;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;IILX/1Pt;)V

    .line 2718295
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/Jcf;->c(LX/1De;)LX/Jcd;

    move-result-object v1

    invoke-virtual {v1, p6}, LX/Jcd;->a(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)LX/Jcd;

    move-result-object v1

    invoke-virtual {v1, p7}, LX/Jcd;->a(Z)LX/Jcd;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v3}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v4}, LX/1Di;->o(I)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0
.end method
