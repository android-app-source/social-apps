.class public Lcom/facebook/resources/impl/loading/LanguagePackInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/resources/impl/loading/LanguagePackInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2762680
    const-class v0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;

    new-instance v1, Lcom/facebook/resources/impl/loading/LanguagePackInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/resources/impl/loading/LanguagePackInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2762681
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2762679
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/resources/impl/loading/LanguagePackInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2762682
    if-nez p0, :cond_0

    .line 2762683
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2762684
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2762685
    invoke-static {p0, p1, p2}, Lcom/facebook/resources/impl/loading/LanguagePackInfoSerializer;->b(Lcom/facebook/resources/impl/loading/LanguagePackInfo;LX/0nX;LX/0my;)V

    .line 2762686
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2762687
    return-void
.end method

.method private static b(Lcom/facebook/resources/impl/loading/LanguagePackInfo;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2762672
    const-string v0, "download_url"

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2762673
    const-string v0, "download_checksum"

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadChecksum:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2762674
    const-string v0, "content_checksum"

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->contentChecksum:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2762675
    const-string v0, "release_number"

    iget v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->releaseNumber:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2762676
    const-string v0, "locale"

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->locale:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2762677
    const-string v0, "delta"

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->delta:Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2762678
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2762671
    check-cast p1, Lcom/facebook/resources/impl/loading/LanguagePackInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/resources/impl/loading/LanguagePackInfoSerializer;->a(Lcom/facebook/resources/impl/loading/LanguagePackInfo;LX/0nX;LX/0my;)V

    return-void
.end method
