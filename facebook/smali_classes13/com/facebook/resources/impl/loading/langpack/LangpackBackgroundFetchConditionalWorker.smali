.class public Lcom/facebook/resources/impl/loading/langpack/LangpackBackgroundFetchConditionalWorker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# instance fields
.field private final a:LX/2z6;


# direct methods
.method public constructor <init>(LX/2z6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2762688
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2762689
    iput-object p1, p0, Lcom/facebook/resources/impl/loading/langpack/LangpackBackgroundFetchConditionalWorker;->a:LX/2z6;

    .line 2762690
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 10

    .prologue
    .line 2762691
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/langpack/LangpackBackgroundFetchConditionalWorker;->a:LX/2z6;

    .line 2762692
    new-instance v1, LX/0ed;

    sget-object v2, LX/0ee;->UPDATE:LX/0ee;

    iget-object v3, v0, LX/2z6;->b:Landroid/content/Context;

    iget-object v4, v0, LX/2z6;->e:LX/0Vv;

    invoke-virtual {v4}, LX/0Vv;->b()Ljava/util/Locale;

    move-result-object v4

    iget-object v5, v0, LX/2z6;->c:LX/0WV;

    sget-object v6, LX/0eh;->LANGPACK:LX/0eh;

    iget-object v7, v0, LX/2z6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v8, LX/0Vv;->a:LX/0Tn;

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, LX/0ed;-><init>(LX/0ee;Landroid/content/Context;Ljava/util/Locale;LX/0WV;LX/0eh;Ljava/lang/String;)V

    .line 2762693
    invoke-virtual {v1}, LX/0ed;->c()I

    .line 2762694
    iget-object v2, v0, LX/2z6;->f:Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    .line 2762695
    iget-object v3, v2, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->e:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->b()V

    .line 2762696
    :try_start_0
    iget-object v3, v2, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->i:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/316;

    .line 2762697
    iget-object v4, v1, LX/0ed;->a:Landroid/content/Context;

    move-object v4, v4

    .line 2762698
    invoke-virtual {v1}, LX/0ed;->c()I

    move-result v5

    invoke-virtual {v1}, LX/0ed;->e()Ljava/lang/String;

    move-result-object v6

    .line 2762699
    iget-object v7, v3, LX/316;->b:LX/315;

    invoke-interface {v7, v4, v5, v6}, LX/315;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 2762700
    iget-object v3, v2, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->g:LX/0em;

    .line 2762701
    iget-object v4, v1, LX/0ed;->a:Landroid/content/Context;

    move-object v4, v4

    .line 2762702
    invoke-virtual {v3, v4}, LX/0em;->a(Landroid/content/Context;)V

    .line 2762703
    iget-object v3, v2, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->g:LX/0em;

    invoke-virtual {v3, v1}, LX/0em;->b(LX/0ed;)LX/0ff;

    move-result-object v3

    .line 2762704
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/0ff;->f()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2762705
    invoke-virtual {v3}, LX/0ff;->f()Ljava/lang/String;

    move-result-object v4

    .line 2762706
    iput-object v4, v1, LX/0ed;->g:Ljava/lang/String;

    .line 2762707
    :cond_0
    invoke-static {v2, v1}, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->d(Lcom/facebook/resources/impl/loading/LanguagePackDownloader;LX/0ed;)Lcom/facebook/resources/impl/loading/LanguagePackInfo;

    move-result-object v4

    .line 2762708
    iget-object v5, v1, LX/0ed;->a:Landroid/content/Context;

    move-object v5, v5

    .line 2762709
    invoke-static {v2, v5, v3, v4}, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->a(Lcom/facebook/resources/impl/loading/LanguagePackDownloader;Landroid/content/Context;LX/0ff;Lcom/facebook/resources/impl/loading/LanguagePackInfo;)LX/2zH;

    move-result-object v3

    .line 2762710
    iget-boolean v4, v3, LX/2zH;->a:Z

    if-nez v4, :cond_1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2762711
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2762712
    :cond_1
    :try_start_1
    new-instance v4, LX/34X;

    iget-object v5, v3, LX/2zH;->c:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    iget-object v3, v3, LX/2zH;->d:LX/1uy;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    invoke-virtual {v1}, LX/0ed;->i()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v7

    invoke-direct {v4, v5, v3, v6, v7}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 2762713
    iget-object v3, v2, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Wn;

    invoke-virtual {v3}, LX/0Wn;->j()V

    .line 2762714
    iget-object v3, v2, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3AP;

    invoke-virtual {v3, v4}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    .line 2762715
    iget-object v3, v2, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Wn;

    invoke-virtual {v3}, LX/0Wn;->k()V

    .line 2762716
    iget-object v3, v2, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->g:LX/0em;

    .line 2762717
    iget-object v4, v1, LX/0ed;->a:Landroid/content/Context;

    move-object v4, v4

    .line 2762718
    invoke-virtual {v3, v4}, LX/0em;->a(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2762719
    :catch_0
    iget-object v3, v2, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Wn;

    invoke-virtual {v3}, LX/0Wn;->l()V

    goto :goto_0
.end method
