.class public Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2762532
    const-class v0, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    new-instance v1, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2762533
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2762531
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2762525
    if-nez p0, :cond_0

    .line 2762526
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2762527
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2762528
    invoke-static {p0, p1, p2}, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfoSerializer;->b(Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;LX/0nX;LX/0my;)V

    .line 2762529
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2762530
    return-void
.end method

.method private static b(Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2762523
    const-string v0, "download_url"

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;->downloadUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2762524
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2762522
    check-cast p1, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfoSerializer;->a(Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;LX/0nX;LX/0my;)V

    return-void
.end method
