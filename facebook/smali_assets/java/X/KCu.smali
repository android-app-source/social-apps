.class public final LX/KCu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;)V
    .locals 0

    .prologue
    .line 2779848
    iput-object p1, p0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x2

    const v1, -0x5a9501ce

    invoke-static {v0, v6, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2779849
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v2, 0x7f0d095b

    if-ne v0, v2, :cond_0

    .line 2779850
    iget-object v0, p0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    const v2, 0x7f0d095b

    invoke-virtual {v0, v2}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2779851
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 2779852
    iget-object v0, p0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    invoke-virtual {v0, v6}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->showDialog(I)V

    .line 2779853
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2779854
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2779855
    iget-object v0, p0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->x:LX/27w;

    invoke-virtual {v0}, LX/27w;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2779856
    new-instance v4, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;

    iget-object v0, p0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v0, v2, v6}, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2779857
    const-string v0, "checkCodeParams"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2779858
    const-string v0, "fetch_code"

    .line 2779859
    :goto_0
    iget-object v2, p0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v2, v2, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->u:LX/0aG;

    const v4, 0x7242c9b1

    invoke-static {v2, v0, v3, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2779860
    iget-object v2, p0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v2, v2, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->t:LX/0Sh;

    new-instance v3, LX/KCt;

    invoke-direct {v3, p0}, LX/KCt;-><init>(LX/KCu;)V

    invoke-virtual {v2, v0, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2779861
    :cond_0
    const v0, 0x482c7767

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2779862
    :cond_1
    new-instance v4, Lcom/facebook/katana/activity/codegenerator/data/LegacyFetchCodeParams;

    iget-object v0, p0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v5, p0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v5, v5, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->p:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v0, v5, v2}, Lcom/facebook/katana/activity/codegenerator/data/LegacyFetchCodeParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2779863
    const-string v0, "checkCodeParams"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2779864
    const-string v0, "legacy_fetch_code"

    goto :goto_0
.end method
