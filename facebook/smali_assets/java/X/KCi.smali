.class public final LX/KCi;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/KCj;


# direct methods
.method public constructor <init>(LX/KCj;)V
    .locals 0

    .prologue
    .line 2779565
    iput-object p1, p0, LX/KCi;->a:LX/KCj;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 6

    .prologue
    .line 2779553
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;

    .line 2779554
    iget-object v1, v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;->a:Ljava/lang/String;

    .line 2779555
    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2779556
    sget-object v0, LX/1CA;->e:LX/0Tn;

    iget-object v4, p0, LX/KCi;->a:LX/KCj;

    iget-object v4, v4, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-wide v4, v4, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->r:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2779557
    iget-object v4, p0, LX/KCi;->a:LX/KCj;

    iget-object v4, v4, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v4, v4, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    invoke-interface {v4, v0, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2779558
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2779559
    sget-object v0, LX/1CA;->f:LX/0Tn;

    iget-object v2, p0, LX/KCi;->a:LX/KCj;

    iget-object v2, v2, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-wide v2, v2, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->r:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2779560
    iget-object v2, p0, LX/KCi;->a:LX/KCj;

    iget-object v2, v2, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v2, v2, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    invoke-interface {v2, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2779561
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/KCi;->a:LX/KCj;

    iget-object v1, v1, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    const-class v2, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2779562
    iget-object v1, p0, LX/KCi;->a:LX/KCj;

    iget-object v1, v1, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v1, v1, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->v:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/KCi;->a:LX/KCj;

    iget-object v2, v2, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2779563
    iget-object v0, p0, LX/KCi;->a:LX/KCj;

    iget-object v0, v0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    invoke-virtual {v0}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->finish()V

    .line 2779564
    :cond_0
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2779567
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2779568
    sget-object v1, LX/1nY;->NO_ERROR:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 2779569
    :goto_0
    return-void

    .line 2779570
    :cond_0
    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-eq v0, v1, :cond_1

    sget-object v1, LX/1nY;->ORCA_SERVICE_UNKNOWN_OPERATION:LX/1nY;

    if-eq v0, v1, :cond_1

    sget-object v1, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_2

    .line 2779571
    :cond_1
    iget-object v0, p0, LX/KCi;->a:LX/KCj;

    iget-object v0, v0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v1, p0, LX/KCi;->a:LX/KCj;

    iget-object v1, v1, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    const v2, 0x7f0831e2

    invoke-virtual {v1, v2}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2779572
    iput-object v1, v0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->q:Ljava/lang/String;

    .line 2779573
    iget-object v0, p0, LX/KCi;->a:LX/KCj;

    iget-object v0, v0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v1, p0, LX/KCi;->a:LX/KCj;

    iget-object v1, v1, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    const v2, 0x7f0831eb

    invoke-virtual {v1, v2}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2779574
    iput-object v1, v0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->p:Ljava/lang/String;

    .line 2779575
    :goto_1
    iget-object v0, p0, LX/KCi;->a:LX/KCj;

    iget-object v0, v0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    invoke-virtual {v0, v3}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->showDialog(I)V

    .line 2779576
    iget-object v0, p0, LX/KCi;->a:LX/KCj;

    iget-object v0, v0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->x:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setClickable(Z)V

    goto :goto_0

    .line 2779577
    :cond_2
    iget-object v0, p0, LX/KCi;->a:LX/KCj;

    iget-object v0, v0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v1, p0, LX/KCi;->a:LX/KCj;

    iget-object v1, v1, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    const v2, 0x7f0831e0

    invoke-virtual {v1, v2}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2779578
    iput-object v1, v0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->q:Ljava/lang/String;

    .line 2779579
    iget-object v0, p0, LX/KCi;->a:LX/KCj;

    iget-object v0, v0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v1, p0, LX/KCi;->a:LX/KCj;

    iget-object v1, v1, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    const v2, 0x7f0831de

    invoke-virtual {v1, v2}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2779580
    iput-object v1, v0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->p:Ljava/lang/String;

    .line 2779581
    goto :goto_1
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2779566
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/KCi;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
