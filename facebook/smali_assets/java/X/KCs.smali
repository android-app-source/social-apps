.class public final LX/KCs;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;)V
    .locals 0

    .prologue
    .line 2779681
    iput-object p1, p0, LX/KCs;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 5

    .prologue
    .line 2779673
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;

    .line 2779674
    iget-object v1, v0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2779675
    iget-object v1, v0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;->a:Ljava/lang/String;

    .line 2779676
    sget-object v4, LX/1CA;->e:LX/0Tn;

    iget-object v0, p0, LX/KCs;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2779677
    iget-object v4, p0, LX/KCs;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v4, v4, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    invoke-interface {v4, v0, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2779678
    const-string v0, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/KCs;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->H:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    sget-wide v2, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->B:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 2779679
    iget-object v0, p0, LX/KCs;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v1, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, LX/KCs;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v2, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->q:LX/0Sh;

    iget-object v0, p0, LX/KCs;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v3, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->r:LX/0aG;

    iget-object v0, p0, LX/KCs;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, LX/KCs;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v4, v4, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->y:LX/27w;

    invoke-virtual {v4}, LX/27w;->b()Z

    move-result v4

    invoke-static {v1, v2, v3, v0, v4}, LX/2NZ;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/0aG;Ljava/lang/String;Z)V

    .line 2779680
    :cond_0
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 2779672
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2779671
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/KCs;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
