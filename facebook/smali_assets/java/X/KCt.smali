.class public final LX/KCt;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/KCu;


# direct methods
.method public constructor <init>(LX/KCu;)V
    .locals 0

    .prologue
    .line 2779813
    iput-object p1, p0, LX/KCt;->a:LX/KCu;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 5

    .prologue
    .line 2779814
    iget-object v0, p0, LX/KCt;->a:LX/KCu;

    iget-object v0, v0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->removeDialog(I)V

    .line 2779815
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;

    .line 2779816
    iget-object v1, v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;->a:Ljava/lang/String;

    .line 2779817
    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2779818
    sget-object v4, LX/1CA;->e:LX/0Tn;

    iget-object v0, p0, LX/KCt;->a:LX/KCu;

    iget-object v0, v0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2779819
    iget-object v4, p0, LX/KCt;->a:LX/KCu;

    iget-object v4, v4, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v4, v4, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    invoke-interface {v4, v0, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2779820
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2779821
    sget-object v2, LX/1CA;->f:LX/0Tn;

    iget-object v0, p0, LX/KCt;->a:LX/KCu;

    iget-object v0, v0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2779822
    iget-object v2, p0, LX/KCt;->a:LX/KCu;

    iget-object v2, v2, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v2, v2, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    invoke-interface {v2, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2779823
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/KCt;->a:LX/KCu;

    iget-object v1, v1, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    const-class v2, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2779824
    iget-object v1, p0, LX/KCt;->a:LX/KCu;

    iget-object v1, v1, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v1, v1, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->w:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/KCt;->a:LX/KCu;

    iget-object v2, v2, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2779825
    iget-object v0, p0, LX/KCt;->a:LX/KCu;

    iget-object v0, v0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    invoke-virtual {v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->finish()V

    .line 2779826
    :cond_0
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2779827
    iget-object v0, p0, LX/KCt;->a:LX/KCu;

    iget-object v0, v0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    invoke-virtual {v0, v3}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->removeDialog(I)V

    .line 2779828
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2779829
    sget-object v1, LX/1nY;->NO_ERROR:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 2779830
    :goto_0
    return-void

    .line 2779831
    :cond_0
    sget-object v1, LX/1nY;->HTTP_500_CLASS:LX/1nY;

    if-ne v0, v1, :cond_1

    iget-object v1, p0, LX/KCt;->a:LX/KCu;

    iget-object v1, v1, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v1, v1, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->x:LX/27w;

    invoke-virtual {v1}, LX/27w;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2779832
    iget-object v1, p0, LX/KCt;->a:LX/KCu;

    iget-object v1, v1, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    invoke-static {v1, p1}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->a(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;Lcom/facebook/fbservice/service/ServiceException;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2779833
    iget-object v0, p0, LX/KCt;->a:LX/KCu;

    iget-object v0, v0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    invoke-virtual {v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->finish()V

    goto :goto_0

    .line 2779834
    :cond_1
    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-eq v0, v1, :cond_2

    sget-object v1, LX/1nY;->ORCA_SERVICE_UNKNOWN_OPERATION:LX/1nY;

    if-eq v0, v1, :cond_2

    sget-object v1, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_3

    .line 2779835
    :cond_2
    iget-object v0, p0, LX/KCt;->a:LX/KCu;

    iget-object v0, v0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v1, p0, LX/KCt;->a:LX/KCu;

    iget-object v1, v1, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    const v2, 0x7f0831e2

    invoke-virtual {v1, v2}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2779836
    iput-object v1, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->r:Ljava/lang/String;

    .line 2779837
    iget-object v0, p0, LX/KCt;->a:LX/KCu;

    iget-object v0, v0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v1, p0, LX/KCt;->a:LX/KCu;

    iget-object v1, v1, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    const v2, 0x7f0831eb

    invoke-virtual {v1, v2}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2779838
    iput-object v1, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->q:Ljava/lang/String;

    .line 2779839
    :goto_1
    iget-object v0, p0, LX/KCt;->a:LX/KCu;

    iget-object v0, v0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->showDialog(I)V

    .line 2779840
    iget-object v0, p0, LX/KCt;->a:LX/KCu;

    iget-object v0, v0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    const v1, 0x7f0d095b

    invoke-virtual {v0, v1}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2779841
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setClickable(Z)V

    goto :goto_0

    .line 2779842
    :cond_3
    iget-object v0, p0, LX/KCt;->a:LX/KCu;

    iget-object v0, v0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v1, p0, LX/KCt;->a:LX/KCu;

    iget-object v1, v1, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    const v2, 0x7f0831e0

    invoke-virtual {v1, v2}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2779843
    iput-object v1, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->r:Ljava/lang/String;

    .line 2779844
    iget-object v0, p0, LX/KCt;->a:LX/KCu;

    iget-object v0, v0, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    iget-object v1, p0, LX/KCt;->a:LX/KCu;

    iget-object v1, v1, LX/KCu;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    const v2, 0x7f0831de

    invoke-virtual {v1, v2}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2779845
    iput-object v1, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->q:Ljava/lang/String;

    .line 2779846
    goto :goto_1
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2779847
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/KCt;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
