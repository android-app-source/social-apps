.class public final LX/KCj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;)V
    .locals 0

    .prologue
    .line 2779582
    iput-object p1, p0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0xe9e09d5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2779583
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0d039a

    if-ne v1, v2, :cond_1

    .line 2779584
    iget-object v1, p0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v1, v1, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2779585
    const v1, -0xd0dd9d8

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2779586
    :goto_0
    return-void

    .line 2779587
    :cond_0
    iget-object v1, p0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v1, v1, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2779588
    iget-object v2, p0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v2, v2, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->x:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 2779589
    iget-object v2, p0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v2, v2, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->y:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2779590
    new-instance v3, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;

    iget-object v4, p0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-wide v4, v4, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->r:J

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v5, v2, v1}, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    .line 2779591
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2779592
    const-string v2, "checkCodeParams"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2779593
    iget-object v2, p0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v2, v2, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->t:LX/0aG;

    const-string v3, "activation_code"

    const v4, -0x34c838fe    # -1.2044034E7f

    invoke-static {v2, v3, v1, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 2779594
    iget-object v2, p0, LX/KCj;->a:Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    iget-object v2, v2, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->s:LX/0Sh;

    new-instance v3, LX/KCi;

    invoke-direct {v3, p0}, LX/KCi;-><init>(LX/KCj;)V

    invoke-virtual {v2, v1, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2779595
    :cond_1
    const v1, -0x5c5e9977

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
