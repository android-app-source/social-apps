.class public final Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;)V
    .locals 0

    .prologue
    .line 2779653
    iput-object p1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 2779654
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-boolean v2, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->O:Z

    .line 2779655
    if-eqz v2, :cond_0

    const-wide/16 v0, 0x3c

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2779656
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    sget-object v1, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->C:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2779657
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v1, v1, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->v:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2779658
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    iget-object v6, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v6, v6, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->H:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v4, v6

    iget-object v6, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v6, v6, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->I:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 2779659
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v1, v1, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->H:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v1, v1, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->I:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    add-long/2addr v8, v10

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    add-long/2addr v6, v8

    sget-object v1, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->C:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    div-long/2addr v6, v8

    long-to-double v6, v6

    .line 2779660
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    rem-long v0, v10, v0

    sub-long v0, v8, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2779661
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v1, v1, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->M:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2779662
    if-eqz v2, :cond_1

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v4, 0x3c

    div-long/2addr v0, v4

    const-wide/16 v4, 0x2

    mul-long/2addr v0, v4

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2779663
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v1, v1, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->G:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->a(Ljava/lang/String;Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v0

    .line 2779664
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    invoke-virtual {v1, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->b(Ljava/lang/String;)V

    .line 2779665
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-double v0, v0

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-double v4, v4

    rem-double v4, v6, v4

    sub-double/2addr v0, v4

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-double v2, v2

    div-double/2addr v0, v2

    .line 2779666
    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v2, v2, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->J:Lcom/facebook/widget/FacebookProgressCircleView;

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v4

    invoke-virtual {v2, v0, v1}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(D)V

    .line 2779667
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;->a:Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->F:Landroid/os/Handler;

    sget-object v1, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->C:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const v1, 0x3b9c3b13

    invoke-static {v0, p0, v2, v3, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2779668
    return-void

    .line 2779669
    :cond_0
    const-wide/16 v0, 0x1e

    goto/16 :goto_0

    .line 2779670
    :cond_1
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v4, 0x1e

    div-long/2addr v0, v4

    goto :goto_1
.end method
