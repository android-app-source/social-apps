.class public Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;
.super Lcom/facebook/katana/activity/BaseFacebookActivity;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private p:J

.field private q:Lcom/facebook/content/SecureContextHelper;

.field private r:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2779919
    invoke-direct {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 2779920
    const v0, 0x7f0831d7

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x108009b

    const v0, 0x7f0831e1

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f080036

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/KCx;

    invoke-direct {v5, p0}, LX/KCx;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;)V

    const/4 v9, 0x0

    move-object v0, p0

    move-object v7, v6

    move-object v8, v6

    invoke-static/range {v0 .. v9}, LX/2Tz;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Z)LX/2EJ;

    move-result-object v0

    .line 2779921
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 2779922
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2779923
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->b(Landroid/os/Bundle;)V

    .line 2779924
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iz_()LX/0QA;

    move-result-object v1

    .line 2779925
    const/16 v0, 0x15e7

    invoke-static {v1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    .line 2779926
    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->p:J

    .line 2779927
    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2779928
    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->q:Lcom/facebook/content/SecureContextHelper;

    .line 2779929
    const v0, 0x7f0302a1

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->setContentView(I)V

    .line 2779930
    const v0, 0x7f0d095e

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2779931
    const v0, 0x7f0d095c

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2779932
    const-string v1, "http://fb.me/cg"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2779933
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x55463ca9

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2779934
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v2, 0x7f0d095e

    if-ne v0, v2, :cond_1

    .line 2779935
    const v0, 0x7f0d095d

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 2779936
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Zs;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2779937
    invoke-static {v2}, LX/2Zs;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2779938
    sget-object v0, LX/1CA;->f:LX/0Tn;

    iget-wide v4, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->p:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2779939
    iget-object v3, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    invoke-interface {v3, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2779940
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2779941
    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->q:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2779942
    invoke-virtual {p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->finish()V

    .line 2779943
    const v0, -0x473dd2b8

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2779944
    :goto_0
    return-void

    .line 2779945
    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorManualProvisionSecretActivity;->a()V

    .line 2779946
    :cond_1
    const v0, 0x6f6a62ab

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0xfe15836

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2779947
    invoke-super {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->onResume()V

    .line 2779948
    const/16 v1, 0x23

    const v2, -0x621f2774

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
