.class public Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;
.super Lcom/facebook/katana/activity/BaseFacebookActivity;
.source ""


# instance fields
.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/0Sh;

.field public u:LX/0aG;

.field public v:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public w:Lcom/facebook/content/SecureContextHelper;

.field public x:LX/27w;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2779873
    invoke-direct {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/ServiceException;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2779874
    if-eqz p1, :cond_0

    .line 2779875
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2779876
    if-eqz v0, :cond_0

    .line 2779877
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2779878
    iget-object v2, v0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v0, v2

    .line 2779879
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 2779880
    :goto_0
    return v0

    .line 2779881
    :cond_1
    const-class v0, LX/2Oo;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 2779882
    if-nez v0, :cond_2

    move v0, v1

    .line 2779883
    goto :goto_0

    .line 2779884
    :cond_2
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    .line 2779885
    iget v2, v0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorSubCode:I

    move v2, v2

    .line 2779886
    const v3, 0x156cd8

    if-ne v2, v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const/16 v2, 0x19f

    if-ne v0, v2, :cond_3

    .line 2779887
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2779888
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->w:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2779889
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2779890
    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;Lcom/facebook/fbservice/service/ServiceException;)Z
    .locals 1

    .prologue
    .line 2779891
    invoke-direct {p0, p1}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->a(Lcom/facebook/fbservice/service/ServiceException;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2779892
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->b(Landroid/os/Bundle;)V

    .line 2779893
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iz_()LX/0QA;

    move-result-object v1

    .line 2779894
    const/16 v0, 0x15e7

    invoke-static {v1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->s:LX/0Or;

    .line 2779895
    invoke-static {v1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->u:LX/0aG;

    .line 2779896
    invoke-static {v1}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->t:LX/0Sh;

    .line 2779897
    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2779898
    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->w:Lcom/facebook/content/SecureContextHelper;

    .line 2779899
    invoke-static {v1}, LX/27w;->b(LX/0QB;)LX/27w;

    move-result-object v0

    check-cast v0, LX/27w;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->x:LX/27w;

    .line 2779900
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->f:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->p:Ljava/lang/String;

    .line 2779901
    const v0, 0x7f0302a0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->setContentView(I)V

    .line 2779902
    const v0, 0x7f0d095b

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/KCu;

    invoke-direct {v1, p0}, LX/KCu;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2779903
    return-void
.end method

.method public final onCreateDialog(I)Landroid/app/Dialog;
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2779904
    packed-switch p1, :pswitch_data_0

    .line 2779905
    :goto_0
    return-object v8

    .line 2779906
    :pswitch_0
    new-instance v8, Landroid/app/ProgressDialog;

    invoke-direct {v8, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 2779907
    invoke-virtual {v8, v9}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 2779908
    const v0, 0x7f0831d8

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2779909
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 2779910
    invoke-virtual {v8, v9}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 2779911
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->r:Ljava/lang/String;

    const v2, 0x108009b

    iget-object v3, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->q:Ljava/lang/String;

    const v0, 0x7f080036

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/KCv;

    invoke-direct {v5, p0}, LX/KCv;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;)V

    const v0, 0x7f0831db

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, LX/KCw;

    invoke-direct {v7, p0}, LX/KCw;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;)V

    move-object v0, p0

    invoke-static/range {v0 .. v9}, LX/2Tz;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Z)LX/2EJ;

    move-result-object v8

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x48f86e2b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2779912
    invoke-super {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->onPause()V

    .line 2779913
    const/16 v1, 0x23

    const v2, 0x404633c9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x5ec95a9a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2779914
    invoke-super {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->onResume()V

    .line 2779915
    const/16 v1, 0x23

    const v2, 0x6b9dff58

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
