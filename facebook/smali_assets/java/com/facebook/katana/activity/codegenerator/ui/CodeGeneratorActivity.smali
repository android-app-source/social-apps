.class public Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field public static B:J

.field public static final C:Ljava/lang/Long;

.field private static D:J

.field private static final E:[I


# instance fields
.field public A:Ljava/lang/Runnable;

.field public F:Landroid/os/Handler;

.field public G:Ljava/lang/String;

.field public H:Ljava/lang/Long;

.field public I:Ljava/lang/Long;

.field public J:Lcom/facebook/widget/FacebookProgressCircleView;

.field private K:Landroid/widget/TextView;

.field public L:Landroid/widget/TextView;

.field public M:Landroid/widget/TextView;

.field private N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public O:Z

.field public p:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FiT;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/tablet/IsTablet;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/27w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2779787
    const-wide/16 v0, 0xf

    sput-wide v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->B:J

    .line 2779788
    const-wide/16 v0, 0x10

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->C:Ljava/lang/Long;

    .line 2779789
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->D:J

    .line 2779790
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->E:[I

    return-void

    :array_0
    .array-data 4
        0x1
        0xa
        0x64
        0x3e8
        0x2710
        0x186a0
        0xf4240
        0x989680
        0x5f5e100
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2779791
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2779792
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->F:Landroid/os/Handler;

    .line 2779793
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->I:Ljava/lang/Long;

    .line 2779794
    new-instance v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;

    invoke-direct {v0, p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity$5;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->A:Ljava/lang/Runnable;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Long;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2779795
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 2779796
    invoke-static {p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->c(Ljava/lang/String;)[B

    move-result-object v1

    .line 2779797
    invoke-static {v1, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->a([B[B)[B

    move-result-object v0

    .line 2779798
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-byte v1, v0, v1

    and-int/lit8 v1, v1, 0xf

    .line 2779799
    aget-byte v2, v0, v1

    and-int/lit8 v2, v2, 0x7f

    shl-int/lit8 v2, v2, 0x18

    add-int/lit8 v3, v1, 0x1

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    add-int/lit8 v3, v1, 0x2

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x3

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v2

    .line 2779800
    sget-object v1, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->E:[I

    const/4 v2, 0x6

    aget v1, v1, v2

    rem-int/2addr v0, v1

    .line 2779801
    const-string v1, "%06d"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2779802
    return-object v0
.end method

.method private static a(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;LX/0Or;LX/0Sh;LX/0aG;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0SG;Ljava/lang/Boolean;LX/0kL;LX/27w;LX/0if;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0aG;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/FiT;",
            ">;",
            "LX/0SG;",
            "Ljava/lang/Boolean;",
            "LX/0kL;",
            "LX/27w;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2779803
    iput-object p1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->p:LX/0Or;

    iput-object p2, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->q:LX/0Sh;

    iput-object p3, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->r:LX/0aG;

    iput-object p4, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p5, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->t:Lcom/facebook/content/SecureContextHelper;

    iput-object p6, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->u:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->v:LX/0SG;

    iput-object p8, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->w:Ljava/lang/Boolean;

    iput-object p9, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->x:LX/0kL;

    iput-object p10, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->y:LX/27w;

    iput-object p11, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->z:LX/0if;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v0, p0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;

    const/16 v1, 0x15e7

    invoke-static {v11, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v11}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {v11}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {v11}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v11}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    const/16 v6, 0x34e9

    invoke-static {v11, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v11}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v11}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-static {v11}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    invoke-static {v11}, LX/27w;->b(LX/0QB;)LX/27w;

    move-result-object v10

    check-cast v10, LX/27w;

    invoke-static {v11}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v11

    check-cast v11, LX/0if;

    invoke-static/range {v0 .. v11}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->a(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;LX/0Or;LX/0Sh;LX/0aG;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0SG;Ljava/lang/Boolean;LX/0kL;LX/27w;LX/0if;)V

    return-void
.end method

.method private static a([B[B)[B
    .locals 3

    .prologue
    .line 2779804
    :try_start_0
    const-string v0, "HmacSHA1"

    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v0

    .line 2779805
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "RAW"

    invoke-direct {v1, p0, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 2779806
    invoke-virtual {v0, p1}, Ljavax/crypto/Mac;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2779807
    :catch_0
    move-exception v0

    .line 2779808
    const-string v1, "hmac function failed"

    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2779809
    new-instance v1, Ljava/lang/reflect/UndeclaredThrowableException;

    invoke-direct {v1, v0}, Ljava/lang/reflect/UndeclaredThrowableException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a$redex0(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 2779810
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/47Y;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2779811
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->x:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0831d9

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2779812
    return-void
.end method

.method private static c(Ljava/lang/String;)[B
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2779762
    invoke-static {p0}, LX/2Zs;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2779763
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2779764
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    mul-int/lit8 v1, v1, 0x5

    div-int/lit8 v1, v1, 0x8

    .line 2779765
    new-array v6, v1, [B

    move v1, v0

    move v2, v0

    move v3, v0

    .line 2779766
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 2779767
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 2779768
    const/16 v5, 0x41

    if-gt v5, v4, :cond_1

    const/16 v5, 0x5a

    if-gt v4, v5, :cond_1

    .line 2779769
    add-int/lit8 v4, v4, -0x41

    move v5, v4

    .line 2779770
    :goto_1
    add-int/lit8 v4, v2, 0x5

    .line 2779771
    shl-int/lit8 v2, v3, 0x5

    .line 2779772
    and-int/lit8 v3, v5, 0x1f

    or-int/2addr v3, v2

    .line 2779773
    const/16 v2, 0x8

    if-lt v4, v2, :cond_3

    .line 2779774
    add-int/lit8 v2, v1, 0x1

    add-int/lit8 v5, v4, -0x8

    shr-int v5, v3, v5

    int-to-byte v5, v5

    aput-byte v5, v6, v1

    .line 2779775
    add-int/lit8 v1, v4, -0x8

    move v7, v2

    move v2, v1

    move v1, v7

    .line 2779776
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2779777
    :cond_1
    add-int/lit8 v4, v4, -0x32

    add-int/lit8 v4, v4, 0x1a

    move v5, v4

    goto :goto_1

    .line 2779778
    :cond_2
    return-object v6

    :cond_3
    move v2, v4

    goto :goto_2
.end method

.method private l()V
    .locals 2

    .prologue
    .line 2779779
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2779780
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/KCq;

    invoke-direct {v1, p0}, LX/KCq;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2779781
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/KCr;

    invoke-direct {v1, p0}, LX/KCr;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;)V

    .line 2779782
    iput-object v1, v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->k:Landroid/view/View$OnClickListener;

    .line 2779783
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2779784
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->K:Landroid/widget/TextView;

    .line 2779785
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->K:Landroid/widget/TextView;

    const v1, 0x7f0831dc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2779786
    return-void
.end method

.method private m()J
    .locals 3

    .prologue
    .line 2779758
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1CA;->e:LX/0Tn;

    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->c(LX/0Tn;)Ljava/lang/Object;

    move-result-object v0

    .line 2779759
    if-nez v0, :cond_0

    .line 2779760
    const-wide/16 v0, 0x0

    .line 2779761
    :goto_0
    return-wide v0

    :cond_0
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :cond_1
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method private n()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2779757
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1CA;->f:LX/0Tn;

    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v2, ""

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 8

    .prologue
    .line 2779745
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->v:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2779746
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->H:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2779747
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x1e

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2779748
    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->G:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->a(Ljava/lang/String;Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v2

    .line 2779749
    sget-wide v4, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->D:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x258

    sub-long/2addr v4, v6

    sget-wide v6, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->D:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    .line 2779750
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sput-wide v4, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->D:J

    .line 2779751
    new-instance v3, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;

    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v0, v2, v1}, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2779752
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2779753
    const-string v1, "checkCodeParams"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2779754
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->r:LX/0aG;

    const-string v2, "legacy_check_code"

    const v3, 0x75a21ef0

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2779755
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->q:LX/0Sh;

    new-instance v2, LX/KCs;

    invoke-direct {v2, p0}, LX/KCs;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2779756
    :cond_1
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/16 v4, 0x8

    const-wide/16 v8, 0x3c

    const-wide/16 v2, 0x1e

    .line 2779709
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2779710
    invoke-static {p0, p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2779711
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->w:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2779712
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->setRequestedOrientation(I)V

    .line 2779713
    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->G:Ljava/lang/String;

    .line 2779714
    invoke-direct {p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->m()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->H:Ljava/lang/Long;

    .line 2779715
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->G:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2779716
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2779717
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->t:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2779718
    invoke-virtual {p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->finish()V

    .line 2779719
    :goto_0
    return-void

    .line 2779720
    :cond_1
    const v0, 0x7f03029f

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->setContentView(I)V

    .line 2779721
    const v0, 0x7f0d0955

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->L:Landroid/widget/TextView;

    .line 2779722
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->L:Landroid/widget/TextView;

    new-instance v1, LX/KCm;

    invoke-direct {v1, p0}, LX/KCm;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2779723
    const v0, 0x7f0d0959

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->M:Landroid/widget/TextView;

    .line 2779724
    const v0, 0x7f0d0958

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FacebookProgressCircleView;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->J:Lcom/facebook/widget/FacebookProgressCircleView;

    .line 2779725
    invoke-direct {p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->l()V

    .line 2779726
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->y:LX/27w;

    .line 2779727
    iget-object v1, v0, LX/27w;->a:LX/0Uh;

    const/16 v5, 0x37

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2779728
    iput-boolean v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->O:Z

    .line 2779729
    iget-boolean v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->O:Z

    if-eqz v0, :cond_2

    .line 2779730
    const v0, 0x7f0d0957

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0831e9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2779731
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->M:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2779732
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->J:Lcom/facebook/widget/FacebookProgressCircleView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/FacebookProgressCircleView;->setVisibility(I)V

    .line 2779733
    const v0, 0x7f0d0956

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FacebookProgressCircleView;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->J:Lcom/facebook/widget/FacebookProgressCircleView;

    .line 2779734
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->J:Lcom/facebook/widget/FacebookProgressCircleView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FacebookProgressCircleView;->setVisibility(I)V

    .line 2779735
    :cond_2
    const v0, 0x7f0d095a

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/KCp;

    invoke-direct {v1, p0}, LX/KCp;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2779736
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->z:LX/0if;

    sget-object v1, LX/0ig;->Q:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2779737
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->v:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    iget-object v4, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->H:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v4, v0

    .line 2779738
    iget-boolean v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->O:Z

    if-eqz v0, :cond_4

    .line 2779739
    const-wide/16 v0, 0x0

    .line 2779740
    rem-long v6, v4, v8

    cmp-long v6, v6, v2

    if-ltz v6, :cond_3

    move-wide v0, v2

    .line 2779741
    :cond_3
    add-long/2addr v0, v4

    rem-long/2addr v0, v8

    sub-long v0, v8, v0

    .line 2779742
    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->z:LX/0if;

    sget-object v3, LX/0ig;->Q:LX/0ih;

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2779743
    :cond_4
    rem-long v0, v4, v2

    sub-long v0, v2, v0

    .line 2779744
    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->z:LX/0if;

    sget-object v3, LX/0ig;->Q:LX/0ih;

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2779707
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->L:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2779708
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0xb3cc87b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2779703
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2779704
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->z:LX/0if;

    if-eqz v1, :cond_0

    .line 2779705
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->z:LX/0if;

    sget-object v2, LX/0ig;->Q:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 2779706
    :cond_0
    const/16 v1, 0x23

    const v2, 0x3ec0ae20

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x60576cf1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2779700
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->F:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->A:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2779701
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2779702
    const/16 v1, 0x23

    const v2, -0x568c37c7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1e

    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x791d8d4b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2779682
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2779683
    invoke-direct {p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->G:Ljava/lang/String;

    .line 2779684
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->G:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2779685
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorAutoProvisionSecretActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2779686
    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->t:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2779687
    invoke-virtual {p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->finish()V

    .line 2779688
    const/16 v1, 0x23

    const v2, 0xe4316d9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2779689
    :goto_0
    return-void

    .line 2779690
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->y:LX/27w;

    invoke-virtual {v1}, LX/27w;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2779691
    invoke-direct {p0}, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->o()V

    .line 2779692
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->O:Z

    if-eqz v1, :cond_2

    .line 2779693
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->v:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->H:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2779694
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3c

    rem-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-ltz v1, :cond_3

    .line 2779695
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->I:Ljava/lang/Long;

    .line 2779696
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->F:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->A:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2779697
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->F:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->A:Ljava/lang/Runnable;

    const v3, -0x5102b27f

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2779698
    const v1, -0x427a50b4

    invoke-static {v1, v0}, LX/02F;->c(II)V

    goto :goto_0

    .line 2779699
    :cond_3
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/CodeGeneratorActivity;->I:Ljava/lang/Long;

    goto :goto_1
.end method
