.class public Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:J

.field public s:LX/0Sh;

.field public t:LX/0aG;

.field public u:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public v:Lcom/facebook/content/SecureContextHelper;

.field public w:Landroid/widget/EditText;

.field public x:Landroid/widget/Button;

.field public y:LX/0So;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2779628
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2779614
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2779615
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iz_()LX/0QA;

    move-result-object v1

    .line 2779616
    const/16 v0, 0x15e7

    invoke-static {v1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    .line 2779617
    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->r:J

    .line 2779618
    invoke-static {v1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->t:LX/0aG;

    .line 2779619
    invoke-static {v1}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->s:LX/0Sh;

    .line 2779620
    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2779621
    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->v:Lcom/facebook/content/SecureContextHelper;

    .line 2779622
    invoke-static {v1}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->y:LX/0So;

    .line 2779623
    const v0, 0x7f03002d

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->setContentView(I)V

    .line 2779624
    const v0, 0x7f0d039a

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->x:Landroid/widget/Button;

    .line 2779625
    const v0, 0x7f0d0399

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->w:Landroid/widget/EditText;

    .line 2779626
    iget-object v0, p0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->x:Landroid/widget/Button;

    new-instance v1, LX/KCj;

    invoke-direct {v1, p0}, LX/KCj;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2779627
    return-void
.end method

.method public final onCreateDialog(I)Landroid/app/Dialog;
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 2779604
    packed-switch p1, :pswitch_data_0

    .line 2779605
    :goto_0
    return-object v8

    .line 2779606
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->q:Ljava/lang/String;

    const v2, 0x108009b

    iget-object v3, p0, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->p:Ljava/lang/String;

    const v0, 0x7f080036

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/KCk;

    invoke-direct {v5, p0}, LX/KCk;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;)V

    const v0, 0x7f0831db

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, LX/KCl;

    invoke-direct {v7, p0}, LX/KCl;-><init>(Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;)V

    const/4 v9, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v9}, LX/2Tz;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Z)LX/2EJ;

    move-result-object v8

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x2484b1ac

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2779612
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2779613
    const/16 v1, 0x23

    const v2, -0x422740ff

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x3dd6413a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2779607
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2779608
    const v1, 0x7f0d0398

    invoke-virtual {p0, v1}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2779609
    const v1, 0x7f0d0398

    invoke-virtual {p0, v1}, Lcom/facebook/katana/activity/codegenerator/ui/ActivateCodeGeneratorWithCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 2779610
    :cond_0
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2779611
    const/16 v1, 0x23

    const v2, 0x76a49557

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
