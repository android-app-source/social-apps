.class public final enum LX/095;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/095;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/095;

.field public static final enum INITIAL:LX/095;

.field public static final enum MQTT:LX/095;

.field public static final enum PERIODIC:LX/095;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22000
    new-instance v0, LX/095;

    const-string v1, "PERIODIC"

    const-string v2, "periodic"

    invoke-direct {v0, v1, v3, v2}, LX/095;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/095;->PERIODIC:LX/095;

    .line 22001
    new-instance v0, LX/095;

    const-string v1, "INITIAL"

    const-string v2, "initial"

    invoke-direct {v0, v1, v4, v2}, LX/095;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/095;->INITIAL:LX/095;

    .line 22002
    new-instance v0, LX/095;

    const-string v1, "MQTT"

    const-string v2, "mqtt"

    invoke-direct {v0, v1, v5, v2}, LX/095;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/095;->MQTT:LX/095;

    .line 22003
    const/4 v0, 0x3

    new-array v0, v0, [LX/095;

    sget-object v1, LX/095;->PERIODIC:LX/095;

    aput-object v1, v0, v3

    sget-object v1, LX/095;->INITIAL:LX/095;

    aput-object v1, v0, v4

    sget-object v1, LX/095;->MQTT:LX/095;

    aput-object v1, v0, v5

    sput-object v0, LX/095;->$VALUES:[LX/095;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21997
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21998
    iput-object p3, p0, LX/095;->value:Ljava/lang/String;

    .line 21999
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/095;
    .locals 1

    .prologue
    .line 21995
    const-class v0, LX/095;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/095;

    return-object v0
.end method

.method public static values()[LX/095;
    .locals 1

    .prologue
    .line 21996
    sget-object v0, LX/095;->$VALUES:[LX/095;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/095;

    return-object v0
.end method
