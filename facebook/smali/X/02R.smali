.class public final LX/02R;
.super LX/02S;
.source ""


# static fields
.field private static final SECONDARY_PROGRAM_DEX_JARS:Ljava/lang/String; = "secondary-program-dex-jars"


# instance fields
.field private mApkZip:Ljava/util/zip/ZipFile;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 7636
    invoke-direct {p0}, LX/02S;-><init>()V

    .line 7637
    iput-object p1, p0, LX/02R;->mContext:Landroid/content/Context;

    .line 7638
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;LX/0FH;)V
    .locals 0

    .prologue
    .line 7635
    invoke-direct {p0, p1}, LX/02R;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final close()V
    .locals 4

    .prologue
    .line 7639
    iget-object v0, p0, LX/02R;->mApkZip:Ljava/util/zip/ZipFile;

    .line 7640
    if-eqz v0, :cond_0

    .line 7641
    :try_start_0
    invoke-virtual {v0}, Ljava/util/zip/ZipFile;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 7642
    :cond_0
    :goto_0
    return-void

    .line 7643
    :catch_0
    move-exception v1

    .line 7644
    const-string v2, "error closing %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v0, v3, p0

    invoke-static {v1, v2, v3}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final markRootRelative()V
    .locals 2

    .prologue
    .line 7632
    iget-object v0, p0, LX/02R;->mApkZip:Ljava/util/zip/ZipFile;

    if-eqz v0, :cond_0

    .line 7633
    :goto_0
    return-void

    .line 7634
    :cond_0
    new-instance v0, Ljava/util/zip/ZipFile;

    iget-object v1, p0, LX/02R;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/02R;->mApkZip:Ljava/util/zip/ZipFile;

    goto :goto_0
.end method

.method public final open(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 7625
    iget-object v0, p0, LX/02R;->mApkZip:Ljava/util/zip/ZipFile;

    if-eqz v0, :cond_0

    const-string v0, "metadata.txt"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7626
    :cond_0
    iget-object v0, p0, LX/02R;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "secondary-program-dex-jars/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 7627
    :goto_0
    return-object v0

    .line 7628
    :cond_1
    iget-object v0, p0, LX/02R;->mApkZip:Ljava/util/zip/ZipFile;

    invoke-virtual {v0, p1}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v0

    .line 7629
    if-nez v0, :cond_2

    .line 7630
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot find root-relative resource: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7631
    :cond_2
    iget-object v1, p0, LX/02R;->mApkZip:Ljava/util/zip/ZipFile;

    invoke-virtual {v1, v0}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0
.end method
