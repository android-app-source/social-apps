.class public LX/0Jl;
.super LX/0Ji;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final h:LX/0Gb;

.field public final i:LX/0OC;

.field public final j:LX/1m0;

.field public final k:Z


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/content/Context;Landroid/os/Handler;ZLX/1m0;LX/0Jv;LX/0Js;II)V
    .locals 6

    .prologue
    .line 39730
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, LX/0Ji;-><init>(Landroid/net/Uri;Landroid/content/Context;Landroid/os/Handler;LX/0Jv;LX/0Js;)V

    .line 39731
    new-instance v0, LX/0OC;

    invoke-direct {v0}, LX/0OC;-><init>()V

    iput-object v0, p0, LX/0Jl;->i:LX/0OC;

    .line 39732
    iput p8, p0, LX/0Jl;->a:I

    .line 39733
    iput p9, p0, LX/0Jl;->b:I

    .line 39734
    new-instance v0, LX/0Kt;

    new-instance v1, LX/0OB;

    invoke-direct {v1, p8}, LX/0OB;-><init>(I)V

    invoke-direct {v0, v1}, LX/0Kt;-><init>(LX/0O1;)V

    iput-object v0, p0, LX/0Jl;->h:LX/0Gb;

    .line 39735
    iput-object p5, p0, LX/0Jl;->j:LX/1m0;

    .line 39736
    iput-boolean p4, p0, LX/0Jl;->k:Z

    .line 39737
    return-void
.end method


# virtual methods
.method public final a()LX/09L;
    .locals 1

    .prologue
    .line 39738
    sget-object v0, LX/09L;->HLS:LX/09L;

    return-object v0
.end method

.method public final a(LX/0Jp;)V
    .locals 6

    .prologue
    .line 39739
    new-instance v0, LX/0Nq;

    invoke-direct {v0}, LX/0Nq;-><init>()V

    .line 39740
    new-instance v1, LX/0Od;

    iget-object v2, p0, LX/0Ji;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/0OD;

    const-string v4, "ExoPlayer_HLS"

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, LX/0OD;-><init>(Ljava/lang/String;LX/0OH;)V

    invoke-direct {v1, v2, v3, v0}, LX/0Od;-><init>(Ljava/lang/String;LX/0G7;LX/0Aa;)V

    .line 39741
    iget-object v0, p0, LX/0Ji;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, LX/0Jk;

    invoke-direct {v2, p0, p1}, LX/0Jk;-><init>(LX/0Jl;LX/0Jp;)V

    invoke-virtual {v1, v0, v2}, LX/0Od;->a(Landroid/os/Looper;LX/0GJ;)V

    .line 39742
    return-void
.end method
