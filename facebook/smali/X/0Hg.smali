.class public abstract LX/0Hg;
.super LX/05s;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "LX/05s",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 37949
    invoke-direct {p0}, LX/05s;-><init>()V

    .line 37950
    iput-object p1, p0, LX/0Hg;->a:Landroid/os/Handler;

    .line 37951
    return-void
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 37952
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, LX/0Hg;->a:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 37953
    invoke-virtual {p0}, LX/05s;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37954
    const-string v0, "Must not call get() function from this Handler thread. Will deadlock!"

    .line 37955
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 37956
    :cond_0
    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 37957
    invoke-direct {p0}, LX/0Hg;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37958
    invoke-direct {p0}, LX/0Hg;->d()V

    .line 37959
    :cond_0
    invoke-super {p0}, LX/05s;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 37960
    invoke-direct {p0}, LX/0Hg;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37961
    invoke-direct {p0}, LX/0Hg;->d()V

    .line 37962
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/05s;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
