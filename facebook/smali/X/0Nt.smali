.class public final LX/0Nt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0NJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52204
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/0Nt;->a:Landroid/util/SparseArray;

    .line 52205
    return-void
.end method


# virtual methods
.method public final a(ZIJ)LX/0NJ;
    .locals 7

    .prologue
    .line 52206
    iget-object v0, p0, LX/0Nt;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0NJ;

    .line 52207
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    .line 52208
    new-instance v0, LX/0NJ;

    invoke-direct {v0, p3, p4}, LX/0NJ;-><init>(J)V

    .line 52209
    iget-object v1, p0, LX/0Nt;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 52210
    :cond_0
    if-nez p1, :cond_1

    if-eqz v0, :cond_2

    .line 52211
    iget-wide v3, v0, LX/0NJ;->c:J

    const-wide/high16 v5, -0x8000000000000000L

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_0
    move v1, v3

    .line 52212
    if-eqz v1, :cond_2

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method
