.class public LX/09x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/09x;


# instance fields
.field private final a:LX/09y;


# direct methods
.method public constructor <init>(LX/09y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 23134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23135
    iput-object p1, p0, LX/09x;->a:LX/09y;

    .line 23136
    return-void
.end method

.method public static a(LX/0QB;)LX/09x;
    .locals 3

    .prologue
    .line 23137
    sget-object v0, LX/09x;->b:LX/09x;

    if-nez v0, :cond_1

    .line 23138
    const-class v1, LX/09x;

    monitor-enter v1

    .line 23139
    :try_start_0
    sget-object v0, LX/09x;->b:LX/09x;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 23140
    if-eqz v2, :cond_0

    .line 23141
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/09x;->b(LX/0QB;)LX/09x;

    move-result-object v0

    sput-object v0, LX/09x;->b:LX/09x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23142
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 23143
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 23144
    :cond_1
    sget-object v0, LX/09x;->b:LX/09x;

    return-object v0

    .line 23145
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 23146
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 23147
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "video_cache_counters"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 23148
    const-string v1, "video"

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23149
    iget-object v1, p0, LX/09x;->a:LX/09y;

    invoke-virtual {v1, v0}, LX/0Vx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 23150
    return-object v0
.end method

.method private static b(LX/0QB;)LX/09x;
    .locals 2

    .prologue
    .line 23151
    new-instance v1, LX/09x;

    invoke-static {p0}, LX/09y;->a(LX/0QB;)LX/09y;

    move-result-object v0

    check-cast v0, LX/09y;

    invoke-direct {v1, v0}, LX/09x;-><init>(LX/09y;)V

    .line 23152
    return-object v1
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 1

    .prologue
    .line 23153
    invoke-direct {p0}, LX/09x;->a()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 23154
    return-object v0
.end method
