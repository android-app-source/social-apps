.class public LX/00A;
.super LX/00B;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1776
    invoke-direct {p0}, LX/00B;-><init>()V

    return-void
.end method


# virtual methods
.method public final translate(Ljava/lang/Throwable;Ljava/util/Map;)Ljava/lang/Throwable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Throwable;"
        }
    .end annotation

    .prologue
    .line 1777
    :try_start_0
    invoke-static {}, LX/008;->getMainDexStoreLoadInformation()LX/02X;

    move-result-object v0

    invoke-virtual {v0}, LX/02X;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1778
    :goto_0
    const-string v1, "mainDexStore"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1779
    invoke-static {}, LX/008;->verifyCanaryClasses()Ljava/lang/RuntimeException;

    move-result-object v0

    .line 1780
    if-eqz v0, :cond_0

    .line 1781
    invoke-static {v0, p1}, LX/00B;->staplePreviousException(Ljava/lang/Throwable;Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1782
    :cond_0
    return-object p1

    .line 1783
    :catch_0
    move-exception v0

    .line 1784
    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
