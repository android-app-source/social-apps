.class public final LX/0C6;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field public static b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27085
    const-class v0, LX/0C6;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0C6;->a:Ljava/lang/String;

    .line 27086
    const/4 v0, 0x0

    sput v0, LX/0C6;->b:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 27088
    sget v0, LX/0C6;->b:I

    add-int/lit8 v0, v0, 0x1

    sput v0, LX/0C6;->b:I

    .line 27089
    return-void
.end method

.method public static b()V
    .locals 5

    .prologue
    .line 27090
    sget v0, LX/0C6;->b:I

    add-int/lit8 v0, v0, -0x1

    .line 27091
    sput v0, LX/0C6;->b:I

    if-gez v0, :cond_0

    .line 27092
    sget-object v0, LX/0C6;->a:Ljava/lang/String;

    const-string v1, "sCounter = %d < 0! This should not happen!"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget v4, LX/0C6;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0Dg;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27093
    :cond_0
    return-void
.end method
