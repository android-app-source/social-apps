.class public LX/00G;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static volatile a:LX/00G;


# instance fields
.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/014;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2024
    invoke-direct {p0, v0, v0}, LX/00G;-><init>(Ljava/lang/String;LX/014;)V

    .line 2025
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;LX/014;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/014;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2021
    iput-object p1, p0, LX/00G;->b:Ljava/lang/String;

    .line 2022
    iput-object p2, p0, LX/00G;->c:LX/014;

    .line 2023
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/00G;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2012
    if-nez p0, :cond_0

    .line 2013
    new-instance v0, LX/00G;

    invoke-direct {v0, v1, v1}, LX/00G;-><init>(Ljava/lang/String;LX/014;)V

    .line 2014
    :goto_0
    return-object v0

    .line 2015
    :cond_0
    const-string v0, ":"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2016
    array-length v1, v0

    if-le v1, v2, :cond_1

    .line 2017
    aget-object v0, v0, v2

    .line 2018
    :goto_1
    new-instance v1, LX/00G;

    invoke-static {v0}, LX/014;->a(Ljava/lang/String;)LX/014;

    move-result-object v0

    invoke-direct {v1, p0, v0}, LX/00G;-><init>(Ljava/lang/String;LX/014;)V

    move-object v0, v1

    goto :goto_0

    .line 2019
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public static g()LX/00G;
    .locals 1

    .prologue
    .line 2008
    sget-object v0, LX/00G;->a:LX/00G;

    .line 2009
    if-nez v0, :cond_0

    .line 2010
    invoke-static {}, LX/00y;->a()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getProcessName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/00G;->a(Ljava/lang/String;)LX/00G;

    move-result-object v0

    sput-object v0, LX/00G;->a:LX/00G;

    .line 2011
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2007
    iget-object v0, p0, LX/00G;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2026
    iget-object v0, p0, LX/00G;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2004
    iget-object v0, p0, LX/00G;->c:LX/014;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/00G;->c:LX/014;

    .line 2005
    iget-object p0, v0, LX/014;->b:Ljava/lang/String;

    move-object v0, p0

    .line 2006
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2003
    sget-object v0, LX/014;->a:LX/014;

    iget-object v1, p0, LX/00G;->c:LX/014;

    invoke-virtual {v0, v1}, LX/014;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1995
    if-ne p0, p1, :cond_1

    .line 1996
    :cond_0
    :goto_0
    return v0

    .line 1997
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1998
    goto :goto_0

    .line 1999
    :cond_3
    check-cast p1, LX/00G;

    .line 2000
    iget-object v2, p0, LX/00G;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2001
    iget-object v2, p1, LX/00G;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2002
    :cond_4
    iget-object v0, p0, LX/00G;->b:Ljava/lang/String;

    iget-object v1, p1, LX/00G;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1987
    invoke-virtual {p0}, LX/00G;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1988
    const-string v0, "<unknown>"

    .line 1989
    :goto_0
    return-object v0

    .line 1990
    :cond_0
    invoke-virtual {p0}, LX/00G;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1991
    const-string v0, "<default>"

    goto :goto_0

    .line 1992
    :cond_1
    iget-object v0, p0, LX/00G;->c:LX/014;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/00G;->c:LX/014;

    .line 1993
    iget-object p0, v0, LX/014;->b:Ljava/lang/String;

    move-object v0, p0

    .line 1994
    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1986
    iget-object v0, p0, LX/00G;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/00G;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1985
    iget-object v0, p0, LX/00G;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "<unknown>"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/00G;->b:Ljava/lang/String;

    goto :goto_0
.end method
