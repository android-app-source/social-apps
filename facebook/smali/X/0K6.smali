.class public final LX/0K6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0K5;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x5
.end annotation


# instance fields
.field private a:J

.field private b:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 40155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40156
    iput p1, p0, LX/0K6;->b:I

    .line 40157
    return-void
.end method

.method public static synthetic a(LX/0K6;)J
    .locals 2

    .prologue
    .line 40148
    iget-wide v0, p0, LX/0K6;->a:J

    return-wide v0
.end method


# virtual methods
.method public final a(LX/0OQ;LX/0OT;)V
    .locals 6

    .prologue
    .line 40151
    iget-wide v0, p2, LX/0OT;->b:J

    iget v2, p0, LX/0K6;->b:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 40152
    iget-wide v2, p0, LX/0K6;->a:J

    iget-wide v0, p2, LX/0OT;->b:J

    iget-wide v4, p2, LX/0OT;->c:J

    add-long/2addr v0, v4

    iget v4, p0, LX/0K6;->b:I

    int-to-long v4, v4

    cmp-long v0, v0, v4

    if-gtz v0, :cond_1

    iget-wide v0, p2, LX/0OT;->c:J

    :goto_0
    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0K6;->a:J

    .line 40153
    :cond_0
    return-void

    .line 40154
    :cond_1
    iget v0, p0, LX/0K6;->b:I

    int-to-long v0, v0

    iget-wide v4, p2, LX/0OT;->b:J

    sub-long/2addr v0, v4

    goto :goto_0
.end method

.method public final a(LX/0OQ;LX/0OT;LX/0OT;)V
    .locals 0

    .prologue
    .line 40150
    return-void
.end method

.method public final a(LX/0OT;)V
    .locals 0

    .prologue
    .line 40149
    return-void
.end method
