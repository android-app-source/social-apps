.class public abstract LX/0GV;
.super LX/0GU;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public A:Z

.field public B:I

.field public C:I

.field public D:Z

.field public E:Z

.field public F:I

.field private G:Z

.field private H:Z

.field public I:Z

.field public J:Z

.field public final a:LX/0Kp;

.field public final b:Landroid/os/Handler;

.field private final c:LX/0L1;

.field private final d:LX/0M3;

.field private final e:Z

.field private final f:LX/0L7;

.field public final g:LX/0L5;

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/media/MediaCodec$BufferInfo;

.field public final j:LX/0Jq;

.field public final k:Z

.field public l:LX/0L4;

.field private m:LX/0Lz;

.field public n:Landroid/media/MediaCodec;

.field private o:Z

.field private p:Z

.field public q:Z

.field private r:Z

.field public s:Z

.field public t:Z

.field private u:[Ljava/nio/ByteBuffer;

.field private v:[Ljava/nio/ByteBuffer;

.field public w:J

.field public x:I

.field public y:I

.field private z:Z


# direct methods
.method public constructor <init>(LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jq;)V
    .locals 7

    .prologue
    .line 35662
    const/4 v0, 0x1

    new-array v1, v0, [LX/0L9;

    const/4 v0, 0x0

    aput-object p1, v1, v0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, LX/0GV;-><init>([LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jq;)V

    .line 35663
    return-void
.end method

.method public constructor <init>([LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jq;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 35664
    invoke-direct {p0, p1}, LX/0GU;-><init>([LX/0L9;)V

    .line 35665
    sget v0, LX/08x;->a:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 35666
    invoke-static {p2}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0L1;

    iput-object v0, p0, LX/0GV;->c:LX/0L1;

    .line 35667
    iput-object p3, p0, LX/0GV;->d:LX/0M3;

    .line 35668
    iput-boolean p4, p0, LX/0GV;->e:Z

    .line 35669
    iput-object p5, p0, LX/0GV;->b:Landroid/os/Handler;

    .line 35670
    iput-object p6, p0, LX/0GV;->j:LX/0Jq;

    .line 35671
    sget v0, LX/08x;->a:I

    const/16 v2, 0x16

    if-gt v0, v2, :cond_1

    const-string v0, "foster"

    sget-object v2, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "NVIDIA"

    sget-object v2, LX/08x;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 35672
    iput-boolean v0, p0, LX/0GV;->k:Z

    .line 35673
    new-instance v0, LX/0Kp;

    invoke-direct {v0}, LX/0Kp;-><init>()V

    iput-object v0, p0, LX/0GV;->a:LX/0Kp;

    .line 35674
    new-instance v0, LX/0L7;

    invoke-direct {v0, v1}, LX/0L7;-><init>(I)V

    iput-object v0, p0, LX/0GV;->f:LX/0L7;

    .line 35675
    new-instance v0, LX/0L5;

    invoke-direct {v0}, LX/0L5;-><init>()V

    iput-object v0, p0, LX/0GV;->g:LX/0L5;

    .line 35676
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0GV;->h:Ljava/util/List;

    .line 35677
    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v0, p0, LX/0GV;->i:Landroid/media/MediaCodec$BufferInfo;

    .line 35678
    iput v1, p0, LX/0GV;->B:I

    .line 35679
    iput v1, p0, LX/0GV;->C:I

    .line 35680
    return-void

    :cond_0
    move v0, v1

    .line 35681
    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static A(LX/0GV;)V
    .locals 2

    .prologue
    .line 35682
    iget v0, p0, LX/0GV;->C:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 35683
    invoke-virtual {p0}, LX/0GV;->o()V

    .line 35684
    invoke-virtual {p0}, LX/0GV;->l()V

    .line 35685
    :goto_0
    return-void

    .line 35686
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0GV;->H:Z

    .line 35687
    invoke-virtual {p0}, LX/0GV;->k()V

    goto :goto_0
.end method

.method private a(LX/0L3;)V
    .locals 3

    .prologue
    .line 35688
    iget-object v0, p0, LX/0GV;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0GV;->j:LX/0Jq;

    if-eqz v0, :cond_0

    .line 35689
    iget-object v0, p0, LX/0GV;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/MediaCodecTrackRenderer$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/exoplayer/MediaCodecTrackRenderer$1;-><init>(LX/0GV;LX/0L3;)V

    const v2, -0xebeb553

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 35690
    :cond_0
    new-instance v0, LX/0Kv;

    invoke-direct {v0, p1}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 3

    .prologue
    .line 35691
    iget-object v0, p0, LX/0GV;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0GV;->j:LX/0Jq;

    if-eqz v0, :cond_0

    .line 35692
    iget-object v0, p0, LX/0GV;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/MediaCodecTrackRenderer$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/exoplayer/MediaCodecTrackRenderer$2;-><init>(LX/0GV;Landroid/media/MediaCodec$CryptoException;)V

    const v2, -0x1db44a16

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 35693
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;JJ)V
    .locals 8

    .prologue
    .line 35694
    iget-object v0, p0, LX/0GV;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0GV;->j:LX/0Jq;

    if-eqz v0, :cond_0

    .line 35695
    iget-object v0, p0, LX/0GV;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/MediaCodecTrackRenderer$3;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer/MediaCodecTrackRenderer$3;-><init>(LX/0GV;Ljava/lang/String;JJ)V

    const v2, 0x3450e52b

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 35696
    :cond_0
    return-void
.end method

.method private a(JZ)Z
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v9, -0x1

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 35759
    iget-boolean v0, p0, LX/0GV;->G:Z

    if-nez v0, :cond_0

    iget v0, p0, LX/0GV;->C:I

    if-ne v0, v8, :cond_1

    .line 35760
    :cond_0
    :goto_0
    return v2

    .line 35761
    :cond_1
    iget v0, p0, LX/0GV;->x:I

    if-gez v0, :cond_2

    .line 35762
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v0

    iput v0, p0, LX/0GV;->x:I

    .line 35763
    iget v0, p0, LX/0GV;->x:I

    if-ltz v0, :cond_0

    .line 35764
    iget-object v0, p0, LX/0GV;->f:LX/0L7;

    iget-object v1, p0, LX/0GV;->u:[Ljava/nio/ByteBuffer;

    iget v3, p0, LX/0GV;->x:I

    aget-object v1, v1, v3

    iput-object v1, v0, LX/0L7;->b:Ljava/nio/ByteBuffer;

    .line 35765
    iget-object v0, p0, LX/0GV;->f:LX/0L7;

    invoke-virtual {v0}, LX/0L7;->d()V

    .line 35766
    :cond_2
    iget v0, p0, LX/0GV;->C:I

    if-ne v0, v7, :cond_4

    .line 35767
    iget-boolean v0, p0, LX/0GV;->r:Z

    if-nez v0, :cond_3

    .line 35768
    iput-boolean v7, p0, LX/0GV;->E:Z

    .line 35769
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    iget v1, p0, LX/0GV;->x:I

    const/4 v6, 0x4

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 35770
    iput v9, p0, LX/0GV;->x:I

    .line 35771
    :cond_3
    iput v8, p0, LX/0GV;->C:I

    goto :goto_0

    .line 35772
    :cond_4
    iget-boolean v0, p0, LX/0GV;->I:Z

    if-eqz v0, :cond_7

    .line 35773
    const/4 v0, -0x3

    .line 35774
    :cond_5
    :goto_1
    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    .line 35775
    const/4 v1, -0x4

    if-ne v0, v1, :cond_a

    .line 35776
    iget v0, p0, LX/0GV;->B:I

    if-ne v0, v8, :cond_6

    .line 35777
    iget-object v0, p0, LX/0GV;->f:LX/0L7;

    invoke-virtual {v0}, LX/0L7;->d()V

    .line 35778
    iput v7, p0, LX/0GV;->B:I

    .line 35779
    :cond_6
    iget-object v0, p0, LX/0GV;->g:LX/0L5;

    invoke-virtual {p0, v0}, LX/0GV;->a(LX/0L5;)V

    move v2, v7

    .line 35780
    goto :goto_0

    .line 35781
    :cond_7
    iget v0, p0, LX/0GV;->B:I

    if-ne v0, v7, :cond_9

    move v1, v2

    .line 35782
    :goto_2
    iget-object v0, p0, LX/0GV;->l:LX/0L4;

    iget-object v0, v0, LX/0L4;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 35783
    iget-object v0, p0, LX/0GV;->l:LX/0L4;

    iget-object v0, v0, LX/0L4;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 35784
    iget-object v3, p0, LX/0GV;->f:LX/0L7;

    iget-object v3, v3, LX/0L7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 35785
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 35786
    :cond_8
    iput v8, p0, LX/0GV;->B:I

    .line 35787
    :cond_9
    iget-object v0, p0, LX/0GV;->g:LX/0L5;

    iget-object v1, p0, LX/0GV;->f:LX/0L7;

    invoke-virtual {p0, p1, p2, v0, v1}, LX/0GU;->a(JLX/0L5;LX/0L7;)I

    move-result v0

    .line 35788
    if-eqz p3, :cond_5

    iget v1, p0, LX/0GV;->F:I

    if-ne v1, v7, :cond_5

    const/4 v1, -0x2

    if-ne v0, v1, :cond_5

    .line 35789
    iput v8, p0, LX/0GV;->F:I

    goto :goto_1

    .line 35790
    :cond_a
    if-ne v0, v9, :cond_d

    .line 35791
    iget v0, p0, LX/0GV;->B:I

    if-ne v0, v8, :cond_b

    .line 35792
    iget-object v0, p0, LX/0GV;->f:LX/0L7;

    invoke-virtual {v0}, LX/0L7;->d()V

    .line 35793
    iput v7, p0, LX/0GV;->B:I

    .line 35794
    :cond_b
    iput-boolean v7, p0, LX/0GV;->G:Z

    .line 35795
    iget-boolean v0, p0, LX/0GV;->D:Z

    if-nez v0, :cond_c

    .line 35796
    invoke-static {p0}, LX/0GV;->A(LX/0GV;)V

    goto/16 :goto_0

    .line 35797
    :cond_c
    :try_start_0
    iget-boolean v0, p0, LX/0GV;->r:Z

    if-nez v0, :cond_0

    .line 35798
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0GV;->E:Z

    .line 35799
    iget-object v4, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    iget v5, p0, LX/0GV;->x:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x4

    invoke-virtual/range {v4 .. v10}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 35800
    const/4 v0, -0x1

    iput v0, p0, LX/0GV;->x:I
    :try_end_0
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 35801
    :catch_0
    move-exception v0

    .line 35802
    invoke-direct {p0, v0}, LX/0GV;->a(Landroid/media/MediaCodec$CryptoException;)V

    .line 35803
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 35804
    :cond_d
    iget-boolean v0, p0, LX/0GV;->J:Z

    if-eqz v0, :cond_10

    .line 35805
    iget-object v0, p0, LX/0GV;->f:LX/0L7;

    invoke-virtual {v0}, LX/0L7;->c()Z

    move-result v0

    if-nez v0, :cond_f

    .line 35806
    iget-object v0, p0, LX/0GV;->f:LX/0L7;

    invoke-virtual {v0}, LX/0L7;->d()V

    .line 35807
    iget v0, p0, LX/0GV;->B:I

    if-ne v0, v8, :cond_e

    .line 35808
    iput v7, p0, LX/0GV;->B:I

    :cond_e
    move v2, v7

    .line 35809
    goto/16 :goto_0

    .line 35810
    :cond_f
    iput-boolean v2, p0, LX/0GV;->J:Z

    .line 35811
    :cond_10
    iget-object v0, p0, LX/0GV;->f:LX/0L7;

    invoke-virtual {v0}, LX/0L7;->a()Z

    move-result v0

    .line 35812
    invoke-direct {p0, v0}, LX/0GV;->a(Z)Z

    move-result v1

    iput-boolean v1, p0, LX/0GV;->I:Z

    .line 35813
    iget-boolean v1, p0, LX/0GV;->I:Z

    if-nez v1, :cond_0

    .line 35814
    iget-boolean v1, p0, LX/0GV;->p:Z

    if-eqz v1, :cond_12

    if-nez v0, :cond_12

    .line 35815
    iget-object v1, p0, LX/0GV;->f:LX/0L7;

    iget-object v1, v1, LX/0L7;->b:Ljava/nio/ByteBuffer;

    const/4 v4, 0x0

    .line 35816
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    move v5, v4

    move v3, v4

    .line 35817
    :goto_3
    add-int/lit8 v8, v5, 0x1

    if-ge v8, v6, :cond_18

    .line 35818
    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    and-int/lit16 v8, v8, 0xff

    .line 35819
    const/4 v9, 0x3

    if-ne v3, v9, :cond_15

    .line 35820
    const/4 v9, 0x1

    if-ne v8, v9, :cond_16

    add-int/lit8 v9, v5, 0x1

    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v9

    and-int/lit8 v9, v9, 0x1f

    const/4 v10, 0x7

    if-ne v9, v10, :cond_16

    .line 35821
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 35822
    add-int/lit8 v5, v5, -0x3

    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 35823
    invoke-virtual {v3, v6}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 35824
    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 35825
    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 35826
    :goto_4
    iget-object v1, p0, LX/0GV;->f:LX/0L7;

    iget-object v1, v1, LX/0L7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    if-nez v1, :cond_11

    move v2, v7

    .line 35827
    goto/16 :goto_0

    .line 35828
    :cond_11
    iput-boolean v2, p0, LX/0GV;->p:Z

    .line 35829
    :cond_12
    :try_start_1
    iget-object v1, p0, LX/0GV;->f:LX/0L7;

    iget-object v1, v1, LX/0L7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    .line 35830
    iget-object v1, p0, LX/0GV;->f:LX/0L7;

    iget v1, v1, LX/0L7;->c:I

    sub-int v1, v3, v1

    .line 35831
    iget-object v2, p0, LX/0GV;->f:LX/0L7;

    iget-wide v4, v2, LX/0L7;->e:J

    .line 35832
    iget-object v2, p0, LX/0GV;->f:LX/0L7;

    .line 35833
    iget v6, v2, LX/0L7;->d:I

    const/high16 v8, 0x8000000

    and-int/2addr v6, v8

    if-eqz v6, :cond_19

    const/4 v6, 0x1

    :goto_5
    move v2, v6

    .line 35834
    if-eqz v2, :cond_13

    .line 35835
    iget-object v2, p0, LX/0GV;->h:Ljava/util/List;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35836
    :cond_13
    if-eqz v0, :cond_14

    .line 35837
    iget-object v0, p0, LX/0GV;->f:LX/0L7;

    .line 35838
    iget-object v2, v0, LX/0L7;->a:LX/0Kq;

    .line 35839
    iget-object v3, v2, LX/0Kq;->g:Landroid/media/MediaCodec$CryptoInfo;

    move-object v2, v3

    .line 35840
    if-nez v1, :cond_1a

    .line 35841
    :goto_6
    move-object v3, v2

    .line 35842
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    iget v1, p0, LX/0GV;->x:I

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueSecureInputBuffer(IILandroid/media/MediaCodec$CryptoInfo;JI)V

    .line 35843
    :goto_7
    const/4 v0, -0x1

    iput v0, p0, LX/0GV;->x:I

    .line 35844
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0GV;->D:Z

    .line 35845
    const/4 v0, 0x0

    iput v0, p0, LX/0GV;->B:I

    .line 35846
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->c:I

    move v2, v7

    .line 35847
    goto/16 :goto_0

    .line 35848
    :cond_14
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    iget v1, p0, LX/0GV;->x:I

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V
    :try_end_1
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_7

    .line 35849
    :catch_1
    move-exception v0

    .line 35850
    invoke-direct {p0, v0}, LX/0GV;->a(Landroid/media/MediaCodec$CryptoException;)V

    .line 35851
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 35852
    :cond_15
    if-nez v8, :cond_16

    .line 35853
    add-int/lit8 v3, v3, 0x1

    .line 35854
    :cond_16
    if-eqz v8, :cond_17

    move v3, v4

    .line 35855
    :cond_17
    add-int/lit8 v5, v5, 0x1

    .line 35856
    goto/16 :goto_3

    .line 35857
    :cond_18
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto/16 :goto_4

    :cond_19
    :try_start_2
    const/4 v6, 0x0

    goto :goto_5
    :try_end_2
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_2 .. :try_end_2} :catch_1

    .line 35858
    :cond_1a
    iget-object v3, v2, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    if-nez v3, :cond_1b

    .line 35859
    const/4 v3, 0x1

    new-array v3, v3, [I

    iput-object v3, v2, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    .line 35860
    :cond_1b
    iget-object v3, v2, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    const/4 v6, 0x0

    aget v8, v3, v6

    add-int/2addr v8, v1

    aput v8, v3, v6

    goto :goto_6
.end method

.method private a(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 35697
    iget-boolean v1, p0, LX/0GV;->z:Z

    if-nez v1, :cond_1

    .line 35698
    :cond_0
    :goto_0
    return v0

    .line 35699
    :cond_1
    iget-object v1, p0, LX/0GV;->d:LX/0M3;

    invoke-interface {v1}, LX/0M3;->a()I

    move-result v1

    .line 35700
    if-nez v1, :cond_2

    .line 35701
    new-instance v0, LX/0Kv;

    iget-object v1, p0, LX/0GV;->d:LX/0M3;

    invoke-interface {v1}, LX/0M3;->d()Ljava/lang/Exception;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 35702
    :cond_2
    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    if-nez p1, :cond_3

    iget-boolean v1, p0, LX/0GV;->e:Z

    if-nez v1, :cond_0

    .line 35703
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(JJ)Z
    .locals 11

    .prologue
    .line 35704
    iget-boolean v0, p0, LX/0GV;->H:Z

    if-eqz v0, :cond_0

    .line 35705
    const/4 v0, 0x0

    .line 35706
    :goto_0
    return v0

    .line 35707
    :cond_0
    iget v0, p0, LX/0GV;->y:I

    if-gez v0, :cond_1

    .line 35708
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    iget-object v1, p0, LX/0GV;->i:Landroid/media/MediaCodec$BufferInfo;

    invoke-static {}, LX/0GV;->y()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v0

    iput v0, p0, LX/0GV;->y:I

    .line 35709
    :cond_1
    iget v0, p0, LX/0GV;->y:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_3

    .line 35710
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    .line 35711
    iget-boolean v1, p0, LX/0GV;->t:Z

    if-eqz v1, :cond_2

    .line 35712
    const-string v1, "channel-count"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 35713
    :cond_2
    invoke-virtual {p0, v0}, LX/0GV;->a(Landroid/media/MediaFormat;)V

    .line 35714
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->d:I

    .line 35715
    const/4 v0, 0x1

    goto :goto_0

    .line 35716
    :cond_3
    iget v0, p0, LX/0GV;->y:I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_4

    .line 35717
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/0GV;->v:[Ljava/nio/ByteBuffer;

    .line 35718
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->e:I

    .line 35719
    const/4 v0, 0x1

    goto :goto_0

    .line 35720
    :cond_4
    iget v0, p0, LX/0GV;->y:I

    if-gez v0, :cond_7

    .line 35721
    iget-boolean v0, p0, LX/0GV;->r:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, LX/0GV;->G:Z

    if-nez v0, :cond_5

    iget v0, p0, LX/0GV;->C:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 35722
    :cond_5
    invoke-static {p0}, LX/0GV;->A(LX/0GV;)V

    .line 35723
    const/4 v0, 0x1

    goto :goto_0

    .line 35724
    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 35725
    :cond_7
    iget-object v0, p0, LX/0GV;->i:Landroid/media/MediaCodec$BufferInfo;

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    .line 35726
    invoke-static {p0}, LX/0GV;->A(LX/0GV;)V

    .line 35727
    const/4 v0, 0x0

    goto :goto_0

    .line 35728
    :cond_8
    iget-object v0, p0, LX/0GV;->i:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v0, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-static {p0, v0, v1}, LX/0GV;->f(LX/0GV;J)I

    move-result v0

    .line 35729
    iget-object v6, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    iget-object v1, p0, LX/0GV;->v:[Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0GV;->y:I

    aget-object v7, v1, v2

    iget-object v8, p0, LX/0GV;->i:Landroid/media/MediaCodec$BufferInfo;

    iget v9, p0, LX/0GV;->y:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_a

    const/4 v10, 0x1

    :goto_1
    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v10}, LX/0GV;->a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 35730
    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    .line 35731
    iget-object v1, p0, LX/0GV;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 35732
    :cond_9
    const/4 v0, -0x1

    iput v0, p0, LX/0GV;->y:I

    .line 35733
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 35734
    :cond_a
    const/4 v10, 0x0

    goto :goto_1

    .line 35735
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private static f(LX/0GV;J)I
    .locals 7

    .prologue
    .line 35736
    iget-object v0, p0, LX/0GV;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 35737
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 35738
    iget-object v0, p0, LX/0GV;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v4, p1

    if-nez v0, :cond_0

    move v0, v1

    .line 35739
    :goto_1
    return v0

    .line 35740
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 35741
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static y()J
    .locals 2

    .prologue
    .line 35742
    const-wide/16 v0, 0x0

    return-wide v0
.end method


# virtual methods
.method public a(LX/0L1;Ljava/lang/String;Z)LX/08w;
    .locals 1

    .prologue
    .line 35743
    invoke-interface {p1, p2, p3}, LX/0L1;->a(Ljava/lang/String;Z)LX/08w;

    move-result-object v0

    return-object v0
.end method

.method public a(JJZ)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35744
    if-eqz p5, :cond_6

    iget v0, p0, LX/0GV;->F:I

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    iput v0, p0, LX/0GV;->F:I

    .line 35745
    iget-object v0, p0, LX/0GV;->l:LX/0L4;

    if-nez v0, :cond_0

    .line 35746
    iget-object v0, p0, LX/0GV;->g:LX/0L5;

    const/4 v3, 0x0

    invoke-virtual {p0, p1, p2, v0, v3}, LX/0GU;->a(JLX/0L5;LX/0L7;)I

    move-result v0

    .line 35747
    const/4 v3, -0x4

    if-ne v0, v3, :cond_0

    .line 35748
    iget-object v0, p0, LX/0GV;->g:LX/0L5;

    invoke-virtual {p0, v0}, LX/0GV;->a(LX/0L5;)V

    .line 35749
    :cond_0
    invoke-virtual {p0}, LX/0GV;->l()V

    .line 35750
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    if-eqz v0, :cond_4

    .line 35751
    const-string v0, "drainAndFeed"

    invoke-static {v0}, LX/08K;->a(Ljava/lang/String;)V

    .line 35752
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, LX/0GV;->b(JJ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 35753
    invoke-direct {p0, p1, p2, v1}, LX/0GV;->a(JZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 35754
    :cond_2
    invoke-direct {p0, p1, p2, v2}, LX/0GV;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 35755
    :cond_3
    invoke-static {}, LX/08K;->a()V

    .line 35756
    :cond_4
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    invoke-virtual {v0}, LX/0Kp;->a()V

    .line 35757
    return-void

    .line 35758
    :cond_5
    iget v0, p0, LX/0GV;->F:I

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_0
.end method

.method public a(LX/0L5;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 35651
    iget-object v0, p0, LX/0GV;->l:LX/0L4;

    .line 35652
    iget-object v1, p1, LX/0L5;->a:LX/0L4;

    iput-object v1, p0, LX/0GV;->l:LX/0L4;

    .line 35653
    iget-object v1, p1, LX/0L5;->b:LX/0Lz;

    iput-object v1, p0, LX/0GV;->m:LX/0Lz;

    .line 35654
    iget-object v1, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/0GV;->o:Z

    iget-object v2, p0, LX/0GV;->l:LX/0L4;

    invoke-virtual {p0, v1, v0, v2}, LX/0GV;->a(ZLX/0L4;LX/0L4;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35655
    iput-boolean v3, p0, LX/0GV;->A:Z

    .line 35656
    iput v3, p0, LX/0GV;->B:I

    .line 35657
    :goto_0
    return-void

    .line 35658
    :cond_0
    iget-boolean v0, p0, LX/0GV;->D:Z

    if-eqz v0, :cond_1

    .line 35659
    iput v3, p0, LX/0GV;->C:I

    goto :goto_0

    .line 35660
    :cond_1
    invoke-virtual {p0}, LX/0GV;->o()V

    .line 35661
    invoke-virtual {p0}, LX/0GV;->l()V

    goto :goto_0
.end method

.method public abstract a(Landroid/media/MediaCodec;ZLandroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
.end method

.method public a(Landroid/media/MediaFormat;)V
    .locals 0

    .prologue
    .line 35515
    return-void
.end method

.method public abstract a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
.end method

.method public abstract a(LX/0L1;LX/0L4;)Z
.end method

.method public final a(LX/0L4;)Z
    .locals 1

    .prologue
    .line 35516
    iget-object v0, p0, LX/0GV;->c:LX/0L1;

    invoke-virtual {p0, v0, p1}, LX/0GV;->a(LX/0L1;LX/0L4;)Z

    move-result v0

    return v0
.end method

.method public a(ZLX/0L4;LX/0L4;)Z
    .locals 1

    .prologue
    .line 35517
    const/4 v0, 0x0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 35518
    iget-boolean v0, p0, LX/0GV;->H:Z

    return v0
.end method

.method public c(J)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 35519
    iput v0, p0, LX/0GV;->F:I

    .line 35520
    iput-boolean v0, p0, LX/0GV;->G:Z

    .line 35521
    iput-boolean v0, p0, LX/0GV;->H:Z

    .line 35522
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    if-eqz v0, :cond_1

    .line 35523
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 35524
    const-wide/16 v1, -0x1

    iput-wide v1, p0, LX/0GV;->w:J

    .line 35525
    iput v3, p0, LX/0GV;->x:I

    .line 35526
    iput v3, p0, LX/0GV;->y:I

    .line 35527
    iput-boolean v5, p0, LX/0GV;->J:Z

    .line 35528
    iput-boolean v4, p0, LX/0GV;->I:Z

    .line 35529
    iget-object v1, p0, LX/0GV;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 35530
    iget-boolean v1, p0, LX/0GV;->q:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, LX/0GV;->s:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, LX/0GV;->E:Z

    if-eqz v1, :cond_2

    .line 35531
    :cond_0
    invoke-virtual {p0}, LX/0GV;->o()V

    .line 35532
    invoke-virtual {p0}, LX/0GV;->l()V

    .line 35533
    :goto_0
    iget-boolean v1, p0, LX/0GV;->A:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0GV;->l:LX/0L4;

    if-eqz v1, :cond_1

    .line 35534
    iput v5, p0, LX/0GV;->B:I

    .line 35535
    :cond_1
    return-void

    .line 35536
    :cond_2
    iget v1, p0, LX/0GV;->C:I

    if-eqz v1, :cond_3

    .line 35537
    invoke-virtual {p0}, LX/0GV;->o()V

    .line 35538
    invoke-virtual {p0}, LX/0GV;->l()V

    goto :goto_0

    .line 35539
    :cond_3
    iget-object v1, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->flush()V

    .line 35540
    iput-boolean v4, p0, LX/0GV;->D:Z

    goto :goto_0
.end method

.method public c()Z
    .locals 7

    .prologue
    .line 35541
    iget-object v0, p0, LX/0GV;->l:LX/0L4;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/0GV;->I:Z

    if-nez v0, :cond_1

    iget v0, p0, LX/0GV;->F:I

    if-nez v0, :cond_0

    iget v0, p0, LX/0GV;->y:I

    if-gez v0, :cond_0

    .line 35542
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-wide v3, p0, LX/0GV;->w:J

    const-wide/16 v5, 0x3e8

    add-long/2addr v3, v5

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 35543
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 35544
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 35545
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 35546
    iput-object v0, p0, LX/0GV;->l:LX/0L4;

    .line 35547
    iput-object v0, p0, LX/0GV;->m:LX/0Lz;

    .line 35548
    :try_start_0
    invoke-virtual {p0}, LX/0GV;->o()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 35549
    :try_start_1
    iget-boolean v0, p0, LX/0GV;->z:Z

    if-eqz v0, :cond_0

    .line 35550
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0GV;->z:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 35551
    :cond_0
    invoke-super {p0}, LX/0GU;->j()V

    .line 35552
    return-void

    .line 35553
    :catchall_0
    move-exception v0

    invoke-super {p0}, LX/0GU;->j()V

    throw v0

    .line 35554
    :catchall_1
    move-exception v0

    .line 35555
    :try_start_2
    iget-boolean v1, p0, LX/0GV;->z:Z

    if-eqz v1, :cond_1

    .line 35556
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/0GV;->z:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 35557
    :cond_1
    invoke-super {p0}, LX/0GU;->j()V

    throw v0

    :catchall_2
    move-exception v0

    invoke-super {p0}, LX/0GU;->j()V

    throw v0
.end method

.method public k()V
    .locals 0

    .prologue
    .line 35558
    return-void
.end method

.method public final l()V
    .locals 14

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, -0x1

    const/4 v2, 0x0

    .line 35559
    invoke-virtual {p0}, LX/0GV;->m()Z

    move-result v0

    if-nez v0, :cond_1

    .line 35560
    :cond_0
    :goto_0
    return-void

    .line 35561
    :cond_1
    iget-object v0, p0, LX/0GV;->l:LX/0L4;

    iget-object v3, v0, LX/0L4;->b:Ljava/lang/String;

    .line 35562
    const/4 v0, 0x0

    .line 35563
    iget-object v1, p0, LX/0GV;->m:LX/0Lz;

    if-eqz v1, :cond_b

    .line 35564
    iget-object v0, p0, LX/0GV;->d:LX/0M3;

    if-nez v0, :cond_2

    .line 35565
    new-instance v0, LX/0Kv;

    const-string v1, "Media requires a DrmSessionManager"

    invoke-direct {v0, v1}, LX/0Kv;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35566
    :cond_2
    iget-boolean v0, p0, LX/0GV;->z:Z

    if-nez v0, :cond_3

    .line 35567
    iput-boolean v9, p0, LX/0GV;->z:Z

    .line 35568
    :cond_3
    iget-object v0, p0, LX/0GV;->d:LX/0M3;

    invoke-interface {v0}, LX/0M3;->a()I

    move-result v0

    .line 35569
    if-nez v0, :cond_4

    .line 35570
    new-instance v0, LX/0Kv;

    iget-object v1, p0, LX/0GV;->d:LX/0M3;

    invoke-interface {v1}, LX/0M3;->d()Ljava/lang/Exception;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 35571
    :cond_4
    if-eq v0, v10, :cond_5

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 35572
    :cond_5
    iget-object v0, p0, LX/0GV;->d:LX/0M3;

    invoke-interface {v0}, LX/0M3;->b()Landroid/media/MediaCrypto;

    move-result-object v1

    .line 35573
    iget-object v0, p0, LX/0GV;->d:LX/0M3;

    invoke-interface {v0}, LX/0M3;->c()Z

    move-result v0

    move v6, v0

    move-object v0, v1

    .line 35574
    :goto_1
    :try_start_0
    iget-object v1, p0, LX/0GV;->c:LX/0L1;

    invoke-virtual {p0, v1, v3, v6}, LX/0GV;->a(LX/0L1;Ljava/lang/String;Z)LX/08w;
    :try_end_0
    .catch LX/090; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v3, v1

    .line 35575
    :goto_2
    if-nez v3, :cond_6

    .line 35576
    new-instance v1, LX/0L3;

    iget-object v4, p0, LX/0GV;->l:LX/0L4;

    const v5, -0xc34f

    invoke-direct {v1, v4, v2, v6, v5}, LX/0L3;-><init>(LX/0L4;Ljava/lang/Throwable;ZI)V

    invoke-direct {p0, v1}, LX/0GV;->a(LX/0L3;)V

    .line 35577
    :cond_6
    iget-object v1, v3, LX/08w;->a:Ljava/lang/String;

    .line 35578
    iget-boolean v2, v3, LX/08w;->b:Z

    iput-boolean v2, p0, LX/0GV;->o:Z

    .line 35579
    iget-object v2, p0, LX/0GV;->l:LX/0L4;

    .line 35580
    sget v4, LX/08x;->a:I

    const/16 v5, 0x15

    if-ge v4, v5, :cond_c

    iget-object v4, v2, LX/0L4;->f:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_c

    const-string v4, "OMX.MTK.VIDEO.DECODER.AVC"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v4, 0x1

    :goto_3
    move v2, v4

    .line 35581
    iput-boolean v2, p0, LX/0GV;->p:Z

    .line 35582
    const/16 v4, 0x12

    .line 35583
    sget v2, LX/08x;->a:I

    if-lt v2, v4, :cond_8

    sget v2, LX/08x;->a:I

    if-ne v2, v4, :cond_7

    const-string v2, "OMX.SEC.avc.dec"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    const-string v2, "OMX.SEC.avc.dec.secure"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_7
    sget v2, LX/08x;->a:I

    const/16 v4, 0x13

    if-ne v2, v4, :cond_d

    sget-object v2, LX/08x;->d:Ljava/lang/String;

    const-string v4, "SM-G800"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    const-string v2, "OMX.Exynos.avc.dec"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    const-string v2, "OMX.Exynos.avc.dec.secure"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_8
    const/4 v2, 0x1

    :goto_4
    move v2, v2

    .line 35584
    iput-boolean v2, p0, LX/0GV;->q:Z

    .line 35585
    sget v2, LX/08x;->a:I

    const/16 v4, 0x11

    if-gt v2, v4, :cond_e

    const-string v2, "OMX.rk.video_decoder.avc"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_5
    move v2, v2

    .line 35586
    iput-boolean v2, p0, LX/0GV;->r:Z

    .line 35587
    sget v2, LX/08x;->a:I

    const/16 v4, 0x17

    if-gt v2, v4, :cond_f

    const-string v2, "OMX.google.vorbis.decoder"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    :goto_6
    move v2, v2

    .line 35588
    iput-boolean v2, p0, LX/0GV;->s:Z

    .line 35589
    iget-object v2, p0, LX/0GV;->l:LX/0L4;

    const/4 v4, 0x1

    .line 35590
    sget v5, LX/08x;->a:I

    const/16 v7, 0x12

    if-gt v5, v7, :cond_10

    iget v5, v2, LX/0L4;->n:I

    if-ne v5, v4, :cond_10

    const-string v5, "OMX.MTK.AUDIO.DECODER.MP3"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    :goto_7
    move v2, v4

    .line 35591
    iput-boolean v2, p0, LX/0GV;->t:Z

    .line 35592
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 35593
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "createByCodecName("

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ")"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/08K;->a(Ljava/lang/String;)V

    .line 35594
    invoke-static {v1}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v2

    iput-object v2, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    .line 35595
    invoke-static {}, LX/08K;->a()V

    .line 35596
    const-string v2, "configureCodec"

    invoke-static {v2}, LX/08K;->a(Ljava/lang/String;)V

    .line 35597
    iget-object v2, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    iget-boolean v3, v3, LX/08w;->b:Z

    iget-object v7, p0, LX/0GV;->l:LX/0L4;

    .line 35598
    invoke-virtual {v7}, LX/0L4;->b()Landroid/media/MediaFormat;

    move-result-object v11

    .line 35599
    iget-boolean v12, p0, LX/0GV;->k:Z

    if-eqz v12, :cond_9

    .line 35600
    const-string v12, "auto-frc"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 35601
    :cond_9
    move-object v7, v11

    .line 35602
    invoke-virtual {p0, v2, v3, v7, v0}, LX/0GV;->a(Landroid/media/MediaCodec;ZLandroid/media/MediaFormat;Landroid/media/MediaCrypto;)V

    .line 35603
    invoke-static {}, LX/08K;->a()V

    .line 35604
    const-string v0, "codec.start()"

    invoke-static {v0}, LX/08K;->a(Ljava/lang/String;)V

    .line 35605
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V

    .line 35606
    invoke-static {}, LX/08K;->a()V

    .line 35607
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 35608
    sub-long v4, v2, v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/0GV;->a(Ljava/lang/String;JJ)V

    .line 35609
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/0GV;->u:[Ljava/nio/ByteBuffer;

    .line 35610
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/0GV;->v:[Ljava/nio/ByteBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 35611
    :goto_8
    iget v0, p0, LX/0GT;->a:I

    move v0, v0

    .line 35612
    if-ne v0, v10, :cond_a

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    :goto_9
    iput-wide v0, p0, LX/0GV;->w:J

    .line 35613
    iput v8, p0, LX/0GV;->x:I

    .line 35614
    iput v8, p0, LX/0GV;->y:I

    .line 35615
    iput-boolean v9, p0, LX/0GV;->J:Z

    .line 35616
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->a:I

    goto/16 :goto_0

    .line 35617
    :catch_0
    move-exception v1

    .line 35618
    new-instance v3, LX/0L3;

    iget-object v4, p0, LX/0GV;->l:LX/0L4;

    const v5, -0xc34e

    invoke-direct {v3, v4, v1, v6, v5}, LX/0L3;-><init>(LX/0L4;Ljava/lang/Throwable;ZI)V

    invoke-direct {p0, v3}, LX/0GV;->a(LX/0L3;)V

    move-object v3, v2

    goto/16 :goto_2

    .line 35619
    :catch_1
    move-exception v0

    .line 35620
    new-instance v2, LX/0L3;

    iget-object v3, p0, LX/0GV;->l:LX/0L4;

    invoke-direct {v2, v3, v0, v6, v1}, LX/0L3;-><init>(LX/0L4;Ljava/lang/Throwable;ZLjava/lang/String;)V

    invoke-direct {p0, v2}, LX/0GV;->a(LX/0L3;)V

    goto :goto_8

    .line 35621
    :cond_a
    const-wide/16 v0, -0x1

    goto :goto_9

    :cond_b
    move v6, v0

    move-object v0, v2

    goto/16 :goto_1

    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_6

    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_7
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 35622
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0GV;->l:LX/0L4;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 35623
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    .line 35624
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0GV;->w:J

    .line 35625
    iput v4, p0, LX/0GV;->x:I

    .line 35626
    iput v4, p0, LX/0GV;->y:I

    .line 35627
    iput-boolean v2, p0, LX/0GV;->I:Z

    .line 35628
    iget-object v0, p0, LX/0GV;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 35629
    iput-object v3, p0, LX/0GV;->u:[Ljava/nio/ByteBuffer;

    .line 35630
    iput-object v3, p0, LX/0GV;->v:[Ljava/nio/ByteBuffer;

    .line 35631
    iput-boolean v2, p0, LX/0GV;->A:Z

    .line 35632
    iput-boolean v2, p0, LX/0GV;->D:Z

    .line 35633
    iput-boolean v2, p0, LX/0GV;->o:Z

    .line 35634
    iput-boolean v2, p0, LX/0GV;->p:Z

    .line 35635
    iput-boolean v2, p0, LX/0GV;->q:Z

    .line 35636
    iput-boolean v2, p0, LX/0GV;->r:Z

    .line 35637
    iput-boolean v2, p0, LX/0GV;->s:Z

    .line 35638
    iput-boolean v2, p0, LX/0GV;->t:Z

    .line 35639
    iput-boolean v2, p0, LX/0GV;->E:Z

    .line 35640
    iput v2, p0, LX/0GV;->B:I

    .line 35641
    iput v2, p0, LX/0GV;->C:I

    .line 35642
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->b:I

    .line 35643
    :try_start_0
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 35644
    :try_start_1
    iget-object v0, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 35645
    iput-object v3, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    .line 35646
    :cond_0
    return-void

    .line 35647
    :catchall_0
    move-exception v0

    iput-object v3, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    throw v0

    .line 35648
    :catchall_1
    move-exception v0

    .line 35649
    :try_start_2
    iget-object v1, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->release()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 35650
    iput-object v3, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    throw v0

    :catchall_2
    move-exception v0

    iput-object v3, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    throw v0
.end method
