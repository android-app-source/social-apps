.class public LX/0GX;
.super LX/0GV;
.source ""

# interfaces
.implements LX/0GW;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final c:LX/0Jr;

.field private final d:LX/0LN;

.field private e:Z

.field private f:Landroid/media/MediaFormat;

.field private g:I

.field private h:J

.field private i:Z

.field private j:Z

.field private k:J


# direct methods
.method public constructor <init>(LX/0L9;LX/0L1;)V
    .locals 2

    .prologue
    .line 35978
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, LX/0GX;-><init>(LX/0L9;LX/0L1;LX/0M3;Z)V

    .line 35979
    return-void
.end method

.method private constructor <init>(LX/0L9;LX/0L1;LX/0M3;Z)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 35976
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, LX/0GX;-><init>(LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jr;)V

    .line 35977
    return-void
.end method

.method public constructor <init>(LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jr;)V
    .locals 9

    .prologue
    .line 35974
    const/4 v7, 0x0

    const/4 v8, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v8}, LX/0GX;-><init>(LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jr;LX/0LG;I)V

    .line 35975
    return-void
.end method

.method private constructor <init>(LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jr;LX/0LG;I)V
    .locals 9

    .prologue
    .line 35972
    const/4 v0, 0x1

    new-array v1, v0, [LX/0L9;

    const/4 v0, 0x0

    aput-object p1, v1, v0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v8}, LX/0GX;-><init>([LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jr;LX/0LG;I)V

    .line 35973
    return-void
.end method

.method public constructor <init>(LX/0L9;LX/0L1;Landroid/os/Handler;LX/0Jr;)V
    .locals 7

    .prologue
    .line 35970
    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/0GX;-><init>(LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jr;)V

    .line 35971
    return-void
.end method

.method private constructor <init>([LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jr;LX/0LG;I)V
    .locals 1

    .prologue
    .line 35965
    invoke-direct/range {p0 .. p6}, LX/0GV;-><init>([LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jq;)V

    .line 35966
    iput-object p6, p0, LX/0GX;->c:LX/0Jr;

    .line 35967
    const/4 v0, 0x0

    iput v0, p0, LX/0GX;->g:I

    .line 35968
    new-instance v0, LX/0LN;

    invoke-direct {v0, p7, p8}, LX/0LN;-><init>(LX/0LG;I)V

    iput-object v0, p0, LX/0GX;->d:LX/0LN;

    .line 35969
    return-void
.end method

.method private a(IJJ)V
    .locals 8

    .prologue
    .line 35962
    iget-object v0, p0, LX/0GV;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0GX;->c:LX/0Jr;

    if-eqz v0, :cond_0

    .line 35963
    iget-object v0, p0, LX/0GV;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$3;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$3;-><init>(LX/0GX;IJJ)V

    const v2, 0x2b85237d

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 35964
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 35957
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    .line 35958
    iget-object v1, v0, LX/0LN;->c:LX/0LG;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/0LN;->c:LX/0LG;

    invoke-static {p1}, LX/0LN;->b(Ljava/lang/String;)I

    move-result p0

    .line 35959
    iget-object v0, v1, LX/0LG;->b:[I

    invoke-static {v0, p0}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v1, v0

    .line 35960
    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 35961
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 35951
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {p0}, LX/0GX;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/0LN;->a(Z)J

    move-result-wide v0

    .line 35952
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 35953
    iget-boolean v2, p0, LX/0GX;->i:Z

    if-eqz v2, :cond_1

    :goto_0
    iput-wide v0, p0, LX/0GX;->h:J

    .line 35954
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0GX;->i:Z

    .line 35955
    :cond_0
    iget-wide v0, p0, LX/0GX;->h:J

    return-wide v0

    .line 35956
    :cond_1
    iget-wide v2, p0, LX/0GX;->h:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final a(LX/0L1;Ljava/lang/String;Z)LX/08w;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 35943
    invoke-direct {p0, p2}, LX/0GX;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35944
    invoke-interface {p1}, LX/0L1;->a()Ljava/lang/String;

    move-result-object v1

    .line 35945
    if-eqz v1, :cond_0

    .line 35946
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0GX;->e:Z

    .line 35947
    new-instance v0, LX/08w;

    invoke-direct {v0, v1, v2}, LX/08w;-><init>(Ljava/lang/String;Z)V

    .line 35948
    :goto_0
    return-object v0

    .line 35949
    :cond_0
    iput-boolean v2, p0, LX/0GX;->e:Z

    .line 35950
    invoke-super {p0, p1, p2, p3}, LX/0GV;->a(LX/0L1;Ljava/lang/String;Z)LX/08w;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 35936
    packed-switch p1, :pswitch_data_0

    .line 35937
    invoke-super {p0, p1, p2}, LX/0GV;->a(ILjava/lang/Object;)V

    .line 35938
    :goto_0
    return-void

    .line 35939
    :pswitch_0
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, LX/0LN;->a(F)V

    goto :goto_0

    .line 35940
    :pswitch_1
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    check-cast p2, Landroid/media/PlaybackParams;

    .line 35941
    iget-object v1, v0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v1, p2}, LX/0LH;->a(Landroid/media/PlaybackParams;)V

    .line 35942
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/media/MediaCodec;ZLandroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 35980
    const-string v0, "mime"

    invoke-virtual {p3, v0}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 35981
    iget-boolean v1, p0, LX/0GX;->e:Z

    if-eqz v1, :cond_0

    .line 35982
    const-string v1, "mime"

    const-string v2, "audio/raw"

    invoke-virtual {p3, v1, v2}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 35983
    invoke-virtual {p1, p3, v3, p4, v4}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 35984
    const-string v1, "mime"

    invoke-virtual {p3, v1, v0}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 35985
    iput-object p3, p0, LX/0GX;->f:Landroid/media/MediaFormat;

    .line 35986
    :goto_0
    return-void

    .line 35987
    :cond_0
    invoke-virtual {p1, p3, v3, p4, v4}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 35988
    iput-object v3, p0, LX/0GX;->f:Landroid/media/MediaFormat;

    goto :goto_0
.end method

.method public final a(Landroid/media/MediaFormat;)V
    .locals 2

    .prologue
    .line 35861
    iget-object v0, p0, LX/0GX;->f:Landroid/media/MediaFormat;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 35862
    :goto_0
    iget-object v1, p0, LX/0GX;->d:LX/0LN;

    if-eqz v0, :cond_0

    iget-object p1, p0, LX/0GX;->f:Landroid/media/MediaFormat;

    :cond_0
    invoke-virtual {v1, p1, v0}, LX/0LN;->a(Landroid/media/MediaFormat;Z)V

    .line 35863
    return-void

    .line 35864
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
    .locals 6

    .prologue
    .line 35865
    iget-boolean v0, p0, LX/0GX;->e:Z

    if-eqz v0, :cond_0

    iget v0, p7, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 35866
    const/4 v0, 0x0

    invoke-virtual {p5, p8, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 35867
    const/4 v0, 0x1

    .line 35868
    :goto_0
    return v0

    .line 35869
    :cond_0
    if-eqz p9, :cond_2

    .line 35870
    const/4 v0, 0x0

    invoke-virtual {p5, p8, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 35871
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->g:I

    .line 35872
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    .line 35873
    iget v1, v0, LX/0LN;->A:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 35874
    const/4 v1, 0x2

    iput v1, v0, LX/0LN;->A:I

    .line 35875
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 35876
    :cond_2
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->a()Z

    move-result v0

    if-nez v0, :cond_7

    .line 35877
    :try_start_0
    iget v0, p0, LX/0GX;->g:I

    if-eqz v0, :cond_5

    .line 35878
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    iget v1, p0, LX/0GX;->g:I

    invoke-virtual {v0, v1}, LX/0LN;->a(I)I

    .line 35879
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0GX;->j:Z
    :try_end_0
    .catch LX/0LK; {:try_start_0 .. :try_end_0} :catch_0

    .line 35880
    iget v0, p0, LX/0GT;->a:I

    move v0, v0

    .line 35881
    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 35882
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->e()V

    .line 35883
    :cond_3
    :goto_2
    :try_start_1
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    iget v2, p7, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v3, p7, Landroid/media/MediaCodec$BufferInfo;->size:I

    iget-wide v4, p7, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    move-object v1, p6

    invoke-virtual/range {v0 .. v5}, LX/0LN;->a(Ljava/nio/ByteBuffer;IIJ)I

    move-result v0

    .line 35884
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, LX/0GX;->k:J
    :try_end_1
    .catch LX/0LM; {:try_start_1 .. :try_end_1} :catch_1

    .line 35885
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_4

    .line 35886
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/0GX;->i:Z

    .line 35887
    :cond_4
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_a

    .line 35888
    const/4 v0, 0x0

    invoke-virtual {p5, p8, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 35889
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->f:I

    .line 35890
    const/4 v0, 0x1

    goto :goto_0

    .line 35891
    :cond_5
    :try_start_2
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->b()I

    move-result v0

    iput v0, p0, LX/0GX;->g:I
    :try_end_2
    .catch LX/0LK; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 35892
    :catch_0
    move-exception v0

    .line 35893
    iget-object v1, p0, LX/0GV;->b:Landroid/os/Handler;

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/0GX;->c:LX/0Jr;

    if-eqz v1, :cond_6

    .line 35894
    iget-object v1, p0, LX/0GV;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$1;-><init>(LX/0GX;LX/0LK;)V

    const v3, 0x2af6e8c

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 35895
    :cond_6
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 35896
    :cond_7
    iget-boolean v0, p0, LX/0GX;->j:Z

    .line 35897
    iget-object v1, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {v1}, LX/0LN;->h()Z

    move-result v1

    iput-boolean v1, p0, LX/0GX;->j:Z

    .line 35898
    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/0GX;->j:Z

    if-nez v0, :cond_3

    .line 35899
    iget v0, p0, LX/0GT;->a:I

    move v0, v0

    .line 35900
    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 35901
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, LX/0GX;->k:J

    sub-long v4, v0, v2

    .line 35902
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->d()J

    move-result-wide v0

    .line 35903
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_8

    const-wide/16 v2, -0x1

    .line 35904
    :goto_3
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    .line 35905
    iget v1, v0, LX/0LN;->o:I

    move v1, v1

    .line 35906
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/0GX;->a(IJJ)V

    goto/16 :goto_2

    .line 35907
    :cond_8
    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    goto :goto_3

    .line 35908
    :catch_1
    move-exception v0

    .line 35909
    iget-object v1, p0, LX/0GV;->b:Landroid/os/Handler;

    if-eqz v1, :cond_9

    iget-object v1, p0, LX/0GX;->c:LX/0Jr;

    if-eqz v1, :cond_9

    .line 35910
    iget-object v1, p0, LX/0GV;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$2;

    invoke-direct {v2, p0, v0}, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$2;-><init>(LX/0GX;LX/0LM;)V

    const v3, -0x33037036

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 35911
    :cond_9
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 35912
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/0L1;LX/0L4;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 35913
    iget-object v1, p2, LX/0L4;->b:Ljava/lang/String;

    .line 35914
    invoke-static {v1}, LX/0Al;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "audio/x-unknown"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, v1}, LX/0GX;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, LX/0L1;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    invoke-interface {p1, v1, v0}, LX/0L1;->a(Ljava/lang/String;Z)LX/08w;

    move-result-object v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 35915
    invoke-super {p0}, LX/0GV;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(J)V
    .locals 1

    .prologue
    .line 35916
    invoke-super {p0, p1, p2}, LX/0GV;->c(J)V

    .line 35917
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->j()V

    .line 35918
    iput-wide p1, p0, LX/0GX;->h:J

    .line 35919
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0GX;->i:Z

    .line 35920
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 35921
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, LX/0GV;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()LX/0GW;
    .locals 0

    .prologue
    .line 35922
    return-object p0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 35923
    invoke-super {p0}, LX/0GV;->h()V

    .line 35924
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->e()V

    .line 35925
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 35926
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->i()V

    .line 35927
    invoke-super {p0}, LX/0GV;->i()V

    .line 35928
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 35929
    const/4 v0, 0x0

    iput v0, p0, LX/0GX;->g:I

    .line 35930
    :try_start_0
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35931
    invoke-super {p0}, LX/0GV;->j()V

    .line 35932
    return-void

    .line 35933
    :catchall_0
    move-exception v0

    invoke-super {p0}, LX/0GV;->j()V

    throw v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 35934
    iget-object v0, p0, LX/0GX;->d:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->g()V

    .line 35935
    return-void
.end method
