.class public final LX/0Nm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:D

.field public final c:I

.field public final d:J

.field public final e:Z

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:J

.field public final i:J


# direct methods
.method public constructor <init>(Ljava/lang/String;DIJZLjava/lang/String;Ljava/lang/String;JJ)V
    .locals 0

    .prologue
    .line 51572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51573
    iput-object p1, p0, LX/0Nm;->a:Ljava/lang/String;

    .line 51574
    iput-wide p2, p0, LX/0Nm;->b:D

    .line 51575
    iput p4, p0, LX/0Nm;->c:I

    .line 51576
    iput-wide p5, p0, LX/0Nm;->d:J

    .line 51577
    iput-boolean p7, p0, LX/0Nm;->e:Z

    .line 51578
    iput-object p8, p0, LX/0Nm;->f:Ljava/lang/String;

    .line 51579
    iput-object p9, p0, LX/0Nm;->g:Ljava/lang/String;

    .line 51580
    iput-wide p10, p0, LX/0Nm;->h:J

    .line 51581
    iput-wide p12, p0, LX/0Nm;->i:J

    .line 51582
    return-void
.end method


# virtual methods
.method public final compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 51583
    check-cast p1, Ljava/lang/Long;

    .line 51584
    iget-wide v0, p0, LX/0Nm;->d:J

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, LX/0Nm;->d:J

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
