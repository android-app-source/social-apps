.class public final enum LX/0BA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0BA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0BA;

.field public static final enum BY_REQUEST:LX/0BA;

.field public static final enum CONNECTION_LOST:LX/0BA;

.field public static final enum CONNECT_FAILED:LX/0BA;

.field public static final enum DISCONNECTED:LX/0BA;

.field public static final enum PREEMPTIVE_RECONNECT_SUCCESS:LX/0BA;

.field public static final enum STALED_CONNECTION:LX/0BA;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26202
    new-instance v0, LX/0BA;

    const-string v1, "CONNECT_FAILED"

    invoke-direct {v0, v1, v3}, LX/0BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BA;->CONNECT_FAILED:LX/0BA;

    .line 26203
    new-instance v0, LX/0BA;

    const-string v1, "CONNECTION_LOST"

    invoke-direct {v0, v1, v4}, LX/0BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BA;->CONNECTION_LOST:LX/0BA;

    .line 26204
    new-instance v0, LX/0BA;

    const-string v1, "BY_REQUEST"

    invoke-direct {v0, v1, v5}, LX/0BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BA;->BY_REQUEST:LX/0BA;

    .line 26205
    new-instance v0, LX/0BA;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v6}, LX/0BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BA;->DISCONNECTED:LX/0BA;

    .line 26206
    new-instance v0, LX/0BA;

    const-string v1, "STALED_CONNECTION"

    invoke-direct {v0, v1, v7}, LX/0BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BA;->STALED_CONNECTION:LX/0BA;

    .line 26207
    new-instance v0, LX/0BA;

    const-string v1, "PREEMPTIVE_RECONNECT_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BA;->PREEMPTIVE_RECONNECT_SUCCESS:LX/0BA;

    .line 26208
    const/4 v0, 0x6

    new-array v0, v0, [LX/0BA;

    sget-object v1, LX/0BA;->CONNECT_FAILED:LX/0BA;

    aput-object v1, v0, v3

    sget-object v1, LX/0BA;->CONNECTION_LOST:LX/0BA;

    aput-object v1, v0, v4

    sget-object v1, LX/0BA;->BY_REQUEST:LX/0BA;

    aput-object v1, v0, v5

    sget-object v1, LX/0BA;->DISCONNECTED:LX/0BA;

    aput-object v1, v0, v6

    sget-object v1, LX/0BA;->STALED_CONNECTION:LX/0BA;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0BA;->PREEMPTIVE_RECONNECT_SUCCESS:LX/0BA;

    aput-object v2, v0, v1

    sput-object v0, LX/0BA;->$VALUES:[LX/0BA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26209
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0BA;
    .locals 1

    .prologue
    .line 26201
    const-class v0, LX/0BA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0BA;

    return-object v0
.end method

.method public static values()[LX/0BA;
    .locals 1

    .prologue
    .line 26200
    sget-object v0, LX/0BA;->$VALUES:[LX/0BA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0BA;

    return-object v0
.end method
