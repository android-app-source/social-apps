.class public final LX/06M;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:LX/06K;


# direct methods
.method public constructor <init>(LX/06K;)V
    .locals 0

    .prologue
    .line 17805
    iput-object p1, p0, LX/06M;->a:LX/06K;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x59489262

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 17806
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/06M;->a:LX/06K;

    iget-object v2, v2, LX/06K;->b:Ljava/lang/String;

    invoke-static {v0, v2}, LX/06P;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17807
    const/16 v0, 0x27

    const v2, -0x27da9fa2

    invoke-static {p2, v3, v0, v2, v1}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 17808
    :goto_0
    return-void

    .line 17809
    :cond_0
    const-string v0, "KeepaliveManager"

    const-string v2, "receiver/keepalive; action=%s delay=%d"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, LX/06M;->a:LX/06K;

    iget-object v5, v5, LX/06K;->k:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v5}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v6

    iget-object v5, p0, LX/06M;->a:LX/06K;

    iget-wide v8, v5, LX/06K;->x:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17810
    iget-object v2, p0, LX/06M;->a:LX/06K;

    monitor-enter v2

    .line 17811
    :try_start_0
    iget-object v0, p0, LX/06M;->a:LX/06K;

    iget-wide v4, v0, LX/06K;->w:J

    const-wide/32 v6, 0xdbba0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_1

    .line 17812
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v0, -0x4c789882

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 17813
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/06M;->a:LX/06K;

    iget-object v3, p0, LX/06M;->a:LX/06K;

    iget-object v3, v3, LX/06K;->k:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v3}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    iget-object v3, p0, LX/06M;->a:LX/06K;

    iget-wide v6, v3, LX/06K;->w:J

    add-long/2addr v4, v6

    .line 17814
    iput-wide v4, v0, LX/06K;->x:J

    .line 17815
    iget-object v0, p0, LX/06M;->a:LX/06K;

    iget-boolean v0, v0, LX/06K;->v:Z

    if-eqz v0, :cond_2

    .line 17816
    iget-object v0, p0, LX/06M;->a:LX/06K;

    iget v0, v0, LX/06K;->l:I

    const/16 v3, 0x17

    if-lt v0, v3, :cond_3

    iget-object v0, p0, LX/06M;->a:LX/06K;

    iget-boolean v0, v0, LX/06K;->z:Z

    if-eqz v0, :cond_3

    .line 17817
    iget-object v0, p0, LX/06M;->a:LX/06K;

    iget-object v0, v0, LX/06K;->j:Landroid/app/AlarmManager;

    const/4 v3, 0x2

    iget-object v4, p0, LX/06M;->a:LX/06K;

    iget-wide v4, v4, LX/06K;->x:J

    iget-object v6, p0, LX/06M;->a:LX/06K;

    iget-object v6, v6, LX/06K;->q:Landroid/app/PendingIntent;

    .line 17818
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    .line 17819
    :cond_2
    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17820
    iget-object v0, p0, LX/06M;->a:LX/06K;

    iget-object v0, v0, LX/06K;->u:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 17821
    const v0, 0x3fb46533

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto/16 :goto_0

    .line 17822
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/06M;->a:LX/06K;

    iget v0, v0, LX/06K;->l:I

    const/16 v3, 0x13

    if-lt v0, v3, :cond_2

    .line 17823
    iget-object v0, p0, LX/06M;->a:LX/06K;

    iget-object v0, v0, LX/06K;->j:Landroid/app/AlarmManager;

    const/4 v3, 0x2

    iget-object v4, p0, LX/06M;->a:LX/06K;

    iget-wide v4, v4, LX/06K;->x:J

    iget-object v6, p0, LX/06M;->a:LX/06K;

    iget-object v6, v6, LX/06K;->q:Landroid/app/PendingIntent;

    .line 17824
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 17825
    goto :goto_1

    .line 17826
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const v2, 0xc6671fa

    invoke-static {p2, v2, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    throw v0
.end method
