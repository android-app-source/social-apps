.class public final LX/011;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/reflect/Method;

.field private static final b:Ljava/lang/reflect/Method;

.field private static final c:Ljava/lang/reflect/Method;

.field private static final d:Ljava/lang/reflect/Method;

.field private static volatile e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 3438
    const/4 v2, 0x0

    .line 3439
    :try_start_0
    const-string v0, "android.os.SystemProperties"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 3440
    const-string v3, "get"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 3441
    const-string v3, "getLong"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 3442
    const-string v3, "set"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v0, v3, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 3443
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x10

    if-lt v3, v7, :cond_1

    const-string v3, "addChangeCallback"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/lang/Runnable;

    aput-object v9, v7, v8

    invoke-virtual {v0, v3, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    move-object v3, v0

    .line 3444
    :goto_0
    new-instance v0, LX/012;

    invoke-direct {v0, v3, v4, v5, v6}, LX/012;-><init>(Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3445
    :goto_1
    move-object v0, v0

    .line 3446
    if-eqz v0, :cond_0

    .line 3447
    iget-object v1, v0, LX/012;->a:Ljava/lang/reflect/Method;

    sput-object v1, LX/011;->d:Ljava/lang/reflect/Method;

    .line 3448
    iget-object v1, v0, LX/012;->b:Ljava/lang/reflect/Method;

    sput-object v1, LX/011;->a:Ljava/lang/reflect/Method;

    .line 3449
    iget-object v1, v0, LX/012;->c:Ljava/lang/reflect/Method;

    sput-object v1, LX/011;->b:Ljava/lang/reflect/Method;

    .line 3450
    iget-object v0, v0, LX/012;->d:Ljava/lang/reflect/Method;

    sput-object v0, LX/011;->c:Ljava/lang/reflect/Method;

    .line 3451
    const/4 v0, 0x1

    sput-boolean v0, LX/011;->e:Z

    .line 3452
    :goto_2
    return-void

    .line 3453
    :cond_0
    sput-object v1, LX/011;->d:Ljava/lang/reflect/Method;

    .line 3454
    sput-object v1, LX/011;->a:Ljava/lang/reflect/Method;

    .line 3455
    sput-object v1, LX/011;->b:Ljava/lang/reflect/Method;

    .line 3456
    sput-object v1, LX/011;->c:Ljava/lang/reflect/Method;

    .line 3457
    const/4 v0, 0x0

    sput-boolean v0, LX/011;->e:Z

    goto :goto_2

    :cond_1
    move-object v3, v2

    .line 3458
    goto :goto_0

    .line 3459
    :catch_0
    move-object v0, v2

    goto :goto_1

    .line 3460
    :catch_1
    move-object v0, v2

    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 3461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3462
    return-void
.end method

.method public static a(Ljava/lang/String;J)J
    .locals 5

    .prologue
    .line 3463
    sget-boolean v0, LX/011;->e:Z

    if-nez v0, :cond_1

    .line 3464
    :cond_0
    :goto_0
    return-wide p1

    .line 3465
    :cond_1
    sget-object v0, LX/011;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/011;->a(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 3466
    if-eqz v0, :cond_0

    .line 3467
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    goto :goto_0
.end method

.method private static varargs a(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3468
    if-nez p0, :cond_0

    .line 3469
    :goto_0
    return-object v0

    .line 3470
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, v1, p1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 3471
    :catch_0
    const/4 v1, 0x0

    sput-boolean v1, LX/011;->e:Z

    goto :goto_0

    .line 3472
    :catch_1
    move-exception v1

    .line 3473
    invoke-static {v1}, LX/0C0;->a(Ljava/lang/reflect/InvocationTargetException;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 3474
    sget-boolean v0, LX/011;->e:Z

    if-nez v0, :cond_1

    .line 3475
    const-string v0, ""

    .line 3476
    :cond_0
    :goto_0
    return-object v0

    .line 3477
    :cond_1
    sget-object v0, LX/011;->a:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, LX/011;->a(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3478
    if-nez v0, :cond_0

    .line 3479
    const-string v0, ""

    goto :goto_0
.end method

.method public static a(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 3480
    sget-boolean v0, LX/011;->e:Z

    if-nez v0, :cond_0

    .line 3481
    :goto_0
    return-void

    .line 3482
    :cond_0
    sget-object v0, LX/011;->d:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, LX/011;->a(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
