.class public LX/01F;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Z

.field public f:J

.field public g:J


# direct methods
.method public constructor <init>(LX/01F;)V
    .locals 4

    .prologue
    .line 3930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3931
    iget-object v0, p1, LX/01F;->a:Ljava/lang/String;

    move-object v0, v0

    .line 3932
    iput-object v0, p0, LX/01F;->a:Ljava/lang/String;

    .line 3933
    iget-object v0, p1, LX/01F;->b:Ljava/lang/String;

    move-object v0, v0

    .line 3934
    iput-object v0, p0, LX/01F;->b:Ljava/lang/String;

    .line 3935
    iget v0, p1, LX/01F;->c:I

    move v0, v0

    .line 3936
    iput v0, p0, LX/01F;->c:I

    .line 3937
    new-instance v0, LX/01J;

    .line 3938
    iget-object v1, p1, LX/01F;->d:LX/01J;

    move-object v1, v1

    .line 3939
    invoke-direct {v0, v1}, LX/01J;-><init>(LX/01J;)V

    iput-object v0, p0, LX/01F;->d:LX/01J;

    .line 3940
    iget-boolean v0, p1, LX/01F;->e:Z

    move v0, v0

    .line 3941
    iput-boolean v0, p0, LX/01F;->e:Z

    .line 3942
    iget-wide v2, p1, LX/01F;->f:J

    move-wide v0, v2

    .line 3943
    iput-wide v0, p0, LX/01F;->f:J

    .line 3944
    iget-wide v2, p1, LX/01F;->g:J

    move-wide v0, v2

    .line 3945
    iput-wide v0, p0, LX/01F;->g:J

    .line 3946
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IZJ)V
    .locals 3

    .prologue
    .line 3947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3948
    iput-object p1, p0, LX/01F;->a:Ljava/lang/String;

    .line 3949
    iput-object p2, p0, LX/01F;->b:Ljava/lang/String;

    .line 3950
    iput p3, p0, LX/01F;->c:I

    .line 3951
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/01F;->d:LX/01J;

    .line 3952
    iput-boolean p4, p0, LX/01F;->e:Z

    .line 3953
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/01F;->f:J

    .line 3954
    iput-wide p5, p0, LX/01F;->g:J

    .line 3955
    return-void
.end method
