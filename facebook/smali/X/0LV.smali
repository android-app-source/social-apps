.class public final LX/0LV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0LU;
.implements LX/0LS;


# instance fields
.field private final a:LX/0ME;

.field private b:Z

.field private c:LX/0LT;

.field private d:Z


# direct methods
.method public constructor <init>(LX/0ME;)V
    .locals 0

    .prologue
    .line 43634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43635
    iput-object p1, p0, LX/0LV;->a:LX/0ME;

    .line 43636
    return-void
.end method


# virtual methods
.method public final a(LX/0MA;)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 43607
    iget-object v1, p0, LX/0LV;->a:LX/0ME;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, LX/0ME;->a(LX/0MA;LX/0MM;)I

    move-result v1

    .line 43608
    if-eq v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43609
    return v1

    .line 43610
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0MA;IZ)I
    .locals 1

    .prologue
    .line 43611
    iget-object v0, p0, LX/0LV;->c:LX/0LT;

    invoke-interface {v0, p1, p2, p3}, LX/0LS;->a(LX/0MA;IZ)I

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 43612
    iget-boolean v0, p0, LX/0LV;->d:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43613
    return-void
.end method

.method public final a(JIII[B)V
    .locals 9

    .prologue
    .line 43614
    iget-object v1, p0, LX/0LV;->c:LX/0LT;

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    .line 43615
    return-void
.end method

.method public final a(LX/0L4;)V
    .locals 1

    .prologue
    .line 43616
    iget-object v0, p0, LX/0LV;->c:LX/0LT;

    invoke-interface {v0, p1}, LX/0LS;->a(LX/0L4;)V

    .line 43617
    return-void
.end method

.method public final a(LX/0LT;)V
    .locals 1

    .prologue
    .line 43618
    iput-object p1, p0, LX/0LV;->c:LX/0LT;

    .line 43619
    iget-boolean v0, p0, LX/0LV;->b:Z

    if-nez v0, :cond_0

    .line 43620
    iget-object v0, p0, LX/0LV;->a:LX/0ME;

    invoke-interface {v0, p0}, LX/0ME;->a(LX/0LU;)V

    .line 43621
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0LV;->b:Z

    .line 43622
    :goto_0
    return-void

    .line 43623
    :cond_0
    iget-object v0, p0, LX/0LV;->a:LX/0ME;

    invoke-interface {v0}, LX/0ME;->b()V

    goto :goto_0
.end method

.method public final a(LX/0Lz;)V
    .locals 1

    .prologue
    .line 43624
    iget-object v0, p0, LX/0LV;->c:LX/0LT;

    invoke-interface {v0, p1}, LX/0LT;->a(LX/0Lz;)V

    .line 43625
    return-void
.end method

.method public final a(LX/0M8;)V
    .locals 1

    .prologue
    .line 43626
    iget-object v0, p0, LX/0LV;->c:LX/0LT;

    invoke-interface {v0, p1}, LX/0LT;->a(LX/0M8;)V

    .line 43627
    return-void
.end method

.method public final a(LX/0Oj;I)V
    .locals 1

    .prologue
    .line 43628
    iget-object v0, p0, LX/0LV;->c:LX/0LT;

    invoke-interface {v0, p1, p2}, LX/0LS;->a(LX/0Oj;I)V

    .line 43629
    return-void
.end method

.method public final a_(I)LX/0LS;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 43630
    iget-boolean v0, p0, LX/0LV;->d:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43631
    iput-boolean v1, p0, LX/0LV;->d:Z

    .line 43632
    return-object p0

    .line 43633
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
