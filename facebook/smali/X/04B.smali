.class public LX/04B;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile k:LX/04B;


# instance fields
.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0sV;

.field public g:Ljava/lang/String;

.field private h:I

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12713
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    sget-object v2, LX/04A;->VIDEO_DISPLAYED:LX/04A;

    iget-object v2, v2, LX/04A;->value:Ljava/lang/String;

    aput-object v2, v1, v3

    sget-object v2, LX/04A;->VIDEO_REQUESTED_PLAYING:LX/04A;

    iget-object v2, v2, LX/04A;->value:Ljava/lang/String;

    aput-object v2, v1, v4

    sget-object v2, LX/04A;->VIDEO_START:LX/04A;

    iget-object v2, v2, LX/04A;->value:Ljava/lang/String;

    aput-object v2, v1, v5

    sget-object v2, LX/04A;->VIDEO_UNPAUSED:LX/04A;

    iget-object v2, v2, LX/04A;->value:Ljava/lang/String;

    aput-object v2, v1, v6

    sget-object v2, LX/04A;->VIDEO_UNMUTED:LX/04A;

    iget-object v2, v2, LX/04A;->value:Ljava/lang/String;

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/04B;->a:Ljava/util/Set;

    .line 12714
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, LX/04B;->b:Ljava/util/Set;

    .line 12715
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v4, [Ljava/lang/String;

    sget-object v2, LX/04C;->VIDEO_CHAINING_IMPRESSION:LX/04C;

    iget-object v2, v2, LX/04C;->value:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/04B;->c:Ljava/util/Set;

    .line 12716
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v7, [Ljava/lang/String;

    sget-object v2, LX/04D;->VIDEO_SETS:LX/04D;

    iget-object v2, v2, LX/04D;->subOrigin:Ljava/lang/String;

    aput-object v2, v1, v3

    sget-object v2, LX/04D;->SINGLE_CREATOR_SET_VIDEO:LX/04D;

    iget-object v2, v2, LX/04D;->subOrigin:Ljava/lang/String;

    aput-object v2, v1, v4

    sget-object v2, LX/04D;->SINGLE_CREATOR_SET_FOOTER:LX/04D;

    iget-object v2, v2, LX/04D;->subOrigin:Ljava/lang/String;

    aput-object v2, v1, v5

    sget-object v2, LX/04D;->SINGLE_CREATOR_SET_INLINE:LX/04D;

    iget-object v2, v2, LX/04D;->subOrigin:Ljava/lang/String;

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/04B;->d:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/0sV;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 12709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12710
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/04B;->e:Ljava/util/Map;

    .line 12711
    iput-object p1, p0, LX/04B;->f:LX/0sV;

    .line 12712
    return-void
.end method

.method public static a(LX/0QB;)LX/04B;
    .locals 3

    .prologue
    .line 12699
    sget-object v0, LX/04B;->k:LX/04B;

    if-nez v0, :cond_1

    .line 12700
    const-class v1, LX/04B;

    monitor-enter v1

    .line 12701
    :try_start_0
    sget-object v0, LX/04B;->k:LX/04B;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 12702
    if-eqz v2, :cond_0

    .line 12703
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/04B;->b(LX/0QB;)LX/04B;

    move-result-object v0

    sput-object v0, LX/04B;->k:LX/04B;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 12704
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 12705
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 12706
    :cond_1
    sget-object v0, LX/04B;->k:LX/04B;

    return-object v0

    .line 12707
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 12708
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/04B;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 12695
    sget-object v0, LX/04E;->VIDEO_CHAINING_SESSION_ID:LX/04E;

    iget-object v0, v0, LX/04E;->value:Ljava/lang/String;

    iget-object v1, p0, LX/04B;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 12696
    sget-object v0, LX/04B;->c:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12697
    :goto_0
    return-void

    .line 12698
    :cond_0
    sget-object v0, LX/04E;->VIDEO_CHAINING_DEPTH_LEVEL:LX/04E;

    iget-object v0, v0, LX/04E;->value:Ljava/lang/String;

    iget-object v1, p0, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public static a(LX/04B;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 12680
    sget-object v0, LX/04F;->VIDEO_PLAYER_TYPE:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/04G;->fromString(Ljava/lang/String;)LX/04G;

    move-result-object v0

    .line 12681
    invoke-direct {p0, p1, v0}, LX/04B;->a(Ljava/lang/String;LX/04G;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 12682
    invoke-virtual {p2}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->i()Ljava/lang/String;

    move-result-object v0

    .line 12683
    iget-object v1, p0, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    const/4 p2, -0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object v1, LX/04A;->VIDEO_REQUESTED_PLAYING:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/04A;->VIDEO_UNMUTED:LX/04A;

    iget-object v1, v1, LX/04A;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 12684
    if-eqz v0, :cond_2

    .line 12685
    iget-object v0, p0, LX/04B;->e:Ljava/util/Map;

    iget v1, p0, LX/04B;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/04B;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12686
    :cond_1
    :goto_1
    return-void

    .line 12687
    :cond_2
    iget-object v0, p0, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 12688
    iget-object v0, p0, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/04B;->h:I

    goto :goto_1

    .line 12689
    :cond_3
    sget-object v0, LX/04F;->CHANNEL_ELIGIBILITY:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 12690
    if-eqz v0, :cond_4

    sget-object v1, LX/04H;->ELIGIBLE:LX/04H;

    iget-object v1, v1, LX/04H;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 12691
    :cond_4
    invoke-direct {p0}, LX/04B;->b()V

    goto :goto_1

    .line 12692
    :cond_5
    invoke-static {p2}, LX/04B;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 12693
    invoke-direct {p0, p1, p2}, LX/04B;->b(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_1

    .line 12694
    :cond_6
    invoke-direct {p0, p1}, LX/04B;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 12675
    invoke-direct {p0}, LX/04B;->b()V

    .line 12676
    iput-object p1, p0, LX/04B;->i:Ljava/lang/String;

    .line 12677
    iget-object v0, p0, LX/04B;->e:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12678
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/04B;->g:Ljava/lang/String;

    .line 12679
    return-void
.end method

.method private a(Ljava/lang/String;LX/04G;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 12717
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    if-ne p2, v0, :cond_2

    iget-object v0, p0, LX/04B;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 12718
    :goto_0
    sget-object v3, LX/04G;->INLINE_PLAYER:LX/04G;

    if-ne p2, v3, :cond_3

    iget-object v3, p0, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v1

    .line 12719
    :goto_1
    sget-object v4, LX/04G;->CHANNEL_PLAYER:LX/04G;

    if-ne p2, v4, :cond_4

    iget-object v4, p0, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v4, v1

    .line 12720
    :goto_2
    iget-object v5, p0, LX/04B;->f:LX/0sV;

    iget-boolean v5, v5, LX/0sV;->p:Z

    if-eqz v5, :cond_5

    if-nez v0, :cond_0

    if-nez v3, :cond_0

    if-eqz v4, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    :goto_3
    return v2

    :cond_2
    move v0, v2

    .line 12721
    goto :goto_0

    :cond_3
    move v3, v2

    .line 12722
    goto :goto_1

    :cond_4
    move v4, v2

    .line 12723
    goto :goto_2

    .line 12724
    :cond_5
    iget-object v0, p0, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/04B;
    .locals 2

    .prologue
    .line 12673
    new-instance v1, LX/04B;

    invoke-static {p0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v0

    check-cast v0, LX/0sV;

    invoke-direct {v1, v0}, LX/04B;-><init>(LX/0sV;)V

    .line 12674
    return-object v1
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12650
    iput-object v1, p0, LX/04B;->i:Ljava/lang/String;

    .line 12651
    iput-object v1, p0, LX/04B;->j:Ljava/lang/String;

    .line 12652
    iget-object v0, p0, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 12653
    iput-object v1, p0, LX/04B;->g:Ljava/lang/String;

    .line 12654
    const/4 v0, 0x0

    iput v0, p0, LX/04B;->h:I

    .line 12655
    return-void
.end method

.method public static b(LX/04B;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 12669
    invoke-direct {p0}, LX/04B;->b()V

    .line 12670
    iput-object p1, p0, LX/04B;->j:Ljava/lang/String;

    .line 12671
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/04B;->g:Ljava/lang/String;

    .line 12672
    return-void
.end method

.method private b(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 12664
    sget-object v0, LX/04I;->STORY_SET_ID:LX/04I;

    iget-object v0, v0, LX/04I;->value:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 12665
    iget-object v1, p0, LX/04B;->j:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/04B;->j:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 12666
    :cond_0
    invoke-static {p0, v0}, LX/04B;->b(LX/04B;Ljava/lang/String;)V

    .line 12667
    :cond_1
    iget-object v0, p0, LX/04B;->e:Ljava/util/Map;

    sget-object v1, LX/04I;->STORY_SET_VIDEO_POSITION:LX/04I;

    iget-object v1, v1, LX/04I;->value:Ljava/lang/String;

    invoke-virtual {p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12668
    return-void
.end method

.method private static b(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z
    .locals 2

    .prologue
    .line 12663
    sget-object v0, LX/04F;->PLAYER_SUBORIGIN:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, LX/04B;->d:Ljava/util/Set;

    sget-object v1, LX/04F;->PLAYER_SUBORIGIN:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/04I;->STORY_SET_ID:LX/04I;

    iget-object v0, v0, LX/04I;->value:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, LX/04I;->STORY_SET_VIDEO_POSITION:LX/04I;

    iget-object v0, v0, LX/04I;->value:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 12656
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 12657
    :cond_0
    return-void

    .line 12658
    :cond_1
    iget-object v0, p0, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 12659
    invoke-direct {p0, p1}, LX/04B;->a(Ljava/lang/String;)V

    .line 12660
    :cond_2
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 12661
    iget-object v2, p0, LX/04B;->e:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 12662
    iget-object v2, p0, LX/04B;->e:Ljava/util/Map;

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
