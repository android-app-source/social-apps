.class public final LX/0EG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)V
    .locals 0

    .prologue
    .line 31512
    iput-object p1, p0, LX/0EG;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 31517
    iget-object v0, p0, LX/0EG;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    .line 31518
    iput-boolean v2, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a:Z

    .line 31519
    iget-object v0, p0, LX/0EG;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    const/4 v1, 0x0

    .line 31520
    iput-boolean v1, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->b:Z

    .line 31521
    iget-object v0, p0, LX/0EG;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-virtual {v0, v2}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->setClickable(Z)V

    .line 31522
    iget-object v0, p0, LX/0EG;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->requestFocus()Z

    .line 31523
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31524
    iget-object v0, p0, LX/0EG;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    .line 31525
    iput-boolean v1, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a:Z

    .line 31526
    iget-object v0, p0, LX/0EG;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    .line 31527
    iput-boolean v1, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->b:Z

    .line 31528
    iget-object v0, p0, LX/0EG;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->setClickable(Z)V

    .line 31529
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 31516
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 31513
    iget-object v0, p0, LX/0EG;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    const/4 v1, 0x1

    .line 31514
    iput-boolean v1, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->b:Z

    .line 31515
    return-void
.end method
