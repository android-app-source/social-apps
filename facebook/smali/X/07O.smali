.class public LX/07O;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/07P;

.field private final b:LX/05h;

.field private final c:I

.field private final d:LX/05K;

.field private final e:LX/075;

.field public f:Ljava/io/DataInputStream;


# direct methods
.method public constructor <init>(LX/07P;LX/05h;ILX/05K;LX/075;)V
    .locals 0

    .prologue
    .line 19641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19642
    iput-object p1, p0, LX/07O;->a:LX/07P;

    .line 19643
    iput-object p2, p0, LX/07O;->b:LX/05h;

    .line 19644
    iput p3, p0, LX/07O;->c:I

    .line 19645
    iput-object p4, p0, LX/07O;->d:LX/05K;

    .line 19646
    iput-object p5, p0, LX/07O;->e:LX/075;

    .line 19647
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/07W;
    .locals 8

    .prologue
    .line 19648
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/07O;->f:Ljava/io/DataInputStream;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/01n;->a(Z)V

    .line 19649
    iget-object v1, p0, LX/07O;->f:Ljava/io/DataInputStream;

    invoke-static {v1}, LX/07a;->a(Ljava/io/DataInputStream;)Landroid/util/Pair;

    move-result-object v7

    .line 19650
    iget-object v2, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, LX/07R;

    .line 19651
    iget v1, v2, LX/07R;->d:I

    .line 19652
    new-instance v4, LX/07b;

    invoke-direct {v4, v2, v1}, LX/07b;-><init>(LX/07R;I)V

    .line 19653
    iget-object v1, p0, LX/07O;->f:Ljava/io/DataInputStream;

    .line 19654
    sget-object v0, LX/07d;->a:[I

    iget-object v3, v4, LX/07c;->a:LX/07R;

    iget-object v3, v3, LX/07R;->a:LX/07S;

    invoke-virtual {v3}, LX/07S;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 19655
    const/4 v0, 0x0

    :goto_1
    move-object v3, v0

    .line 19656
    iget v0, v4, LX/07c;->b:I

    move v4, v0

    .line 19657
    new-instance v1, LX/07f;

    iget v5, p0, LX/07O;->c:I

    iget-object v6, p0, LX/07O;->d:LX/05K;

    invoke-direct/range {v1 .. v6}, LX/07f;-><init>(LX/07R;Ljava/lang/Object;IILX/05K;)V

    .line 19658
    iget-object v4, p0, LX/07O;->f:Ljava/io/DataInputStream;

    invoke-virtual {v1, v4}, LX/07f;->c(Ljava/io/DataInputStream;)Ljava/lang/Object;

    move-result-object v4

    .line 19659
    iget v0, v1, LX/07c;->b:I

    move v1, v0

    .line 19660
    if-eqz v1, :cond_1

    .line 19661
    iget-object v1, p0, LX/07O;->b:LX/05h;

    iget-object v3, v2, LX/07R;->a:LX/07S;

    invoke-virtual {v3}, LX/07S;->name()Ljava/lang/String;

    move-result-object v3

    iget v2, v2, LX/07R;->d:I

    .line 19662
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "message_type"

    aput-object v5, v0, v4

    const/4 v4, 0x1

    aput-object v3, v0, v4

    const/4 v4, 0x2

    const-string v5, "message_size"

    aput-object v5, v0, v4

    const/4 v4, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    invoke-static {v0}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 19663
    const-string v4, "mqtt_invalid_message"

    invoke-virtual {v1, v4, v0}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 19664
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unexpected bytes remaining in payload"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 19665
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 19666
    :cond_1
    :try_start_1
    invoke-static {v2, v3, v4}, LX/07P;->a(LX/07R;Ljava/lang/Object;Ljava/lang/Object;)LX/07W;

    move-result-object v2

    .line 19667
    invoke-virtual {v2}, LX/07W;->e()LX/07S;

    move-result-object v1

    invoke-virtual {v1}, LX/07S;->name()Ljava/lang/String;

    move-result-object v4

    .line 19668
    const-string v1, ""

    .line 19669
    instance-of v3, v2, LX/07X;

    if-eqz v3, :cond_3

    .line 19670
    move-object v0, v2

    check-cast v0, LX/07X;

    move-object v1, v0

    .line 19671
    invoke-virtual {v1}, LX/07X;->a()LX/0B5;

    move-result-object v1

    iget-object v1, v1, LX/0B5;->a:Ljava/lang/String;

    .line 19672
    invoke-static {v1}, LX/0AJ;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 19673
    if-nez v3, :cond_2

    move-object v3, v1

    .line 19674
    :cond_2
    :goto_2
    iget-object v5, p0, LX/07O;->e:LX/075;

    iget-object v1, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v5, v1}, LX/075;->b(I)V

    .line 19675
    iget-object v5, p0, LX/07O;->e:LX/075;

    iget-object v1, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v5, v4, v3, v1}, LX/075;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 19676
    monitor-exit p0

    return-object v2

    :cond_3
    move-object v3, v1

    goto :goto_2

    .line 19677
    :pswitch_0
    invoke-static {v4, v1}, LX/07b;->d(LX/07b;Ljava/io/DataInputStream;)LX/07U;

    move-result-object v0

    goto/16 :goto_1

    .line 19678
    :pswitch_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedByte()I

    .line 19679
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readByte()B

    move-result v0

    .line 19680
    iget v3, v4, LX/07c;->b:I

    add-int/lit8 v3, v3, -0x2

    iput v3, v4, LX/07b;->b:I

    .line 19681
    new-instance v3, LX/07e;

    invoke-direct {v3, v0}, LX/07e;-><init>(B)V

    move-object v0, v3

    .line 19682
    goto/16 :goto_1

    .line 19683
    :pswitch_2
    invoke-virtual {v4, v1}, LX/07c;->b(Ljava/io/DataInputStream;)I

    move-result v0

    .line 19684
    new-instance v3, LX/0B6;

    invoke-direct {v3, v0}, LX/0B6;-><init>(I)V

    move-object v0, v3

    .line 19685
    goto/16 :goto_1

    .line 19686
    :pswitch_3
    invoke-virtual {v4, v1}, LX/07c;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v3

    .line 19687
    const/4 v0, -0x1

    .line 19688
    iget-object v5, v4, LX/07c;->a:LX/07R;

    iget v5, v5, LX/07R;->c:I

    if-lez v5, :cond_4

    .line 19689
    invoke-virtual {v4, v1}, LX/07c;->b(Ljava/io/DataInputStream;)I

    move-result v0

    .line 19690
    :cond_4
    new-instance v5, LX/0B5;

    invoke-direct {v5, v3, v0}, LX/0B5;-><init>(Ljava/lang/String;I)V

    move-object v0, v5

    .line 19691
    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
