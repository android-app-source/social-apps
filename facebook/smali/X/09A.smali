.class public LX/09A;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0So;

.field private b:J

.field private c:J


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 0

    .prologue
    .line 22146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22147
    iput-object p1, p0, LX/09A;->a:LX/0So;

    .line 22148
    invoke-virtual {p0}, LX/09A;->d()V

    .line 22149
    return-void
.end method

.method private a(J)J
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 22143
    iget-wide v2, p0, LX/09A;->c:J

    cmp-long v2, v2, v0

    if-ltz v2, :cond_0

    .line 22144
    iget-wide v0, p0, LX/09A;->c:J

    sub-long v0, p1, v0

    .line 22145
    :cond_0
    return-wide v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 22150
    iget-wide v0, p0, LX/09A;->b:J

    iget-object v2, p0, LX/09A;->a:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, LX/09A;->a(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 22140
    iget-wide v0, p0, LX/09A;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 22141
    :goto_0
    return-void

    .line 22142
    :cond_0
    iget-object v0, p0, LX/09A;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/09A;->c:J

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 22132
    iget-object v0, p0, LX/09A;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LX/09A;->a(J)J

    move-result-wide v0

    .line 22133
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 22134
    iget-wide v2, p0, LX/09A;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/09A;->b:J

    .line 22135
    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/09A;->c:J

    .line 22136
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 22137
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/09A;->b:J

    .line 22138
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/09A;->c:J

    .line 22139
    return-void
.end method
