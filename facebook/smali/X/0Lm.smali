.class public final enum LX/0Lm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Lm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Lm;

.field public static final enum DEFAULT:LX/0Lm;

.field public static final enum REFRESH_AFTER_FINISH_CURRENT:LX/0Lm;

.field public static final enum REFRESH_AFTER_FINISH_INIT:LX/0Lm;


# instance fields
.field public final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44226
    new-instance v0, LX/0Lm;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2, v2}, LX/0Lm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0Lm;->DEFAULT:LX/0Lm;

    .line 44227
    new-instance v0, LX/0Lm;

    const-string v1, "REFRESH_AFTER_FINISH_INIT"

    invoke-direct {v0, v1, v3, v3}, LX/0Lm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0Lm;->REFRESH_AFTER_FINISH_INIT:LX/0Lm;

    .line 44228
    new-instance v0, LX/0Lm;

    const-string v1, "REFRESH_AFTER_FINISH_CURRENT"

    invoke-direct {v0, v1, v4, v4}, LX/0Lm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0Lm;->REFRESH_AFTER_FINISH_CURRENT:LX/0Lm;

    .line 44229
    const/4 v0, 0x3

    new-array v0, v0, [LX/0Lm;

    sget-object v1, LX/0Lm;->DEFAULT:LX/0Lm;

    aput-object v1, v0, v2

    sget-object v1, LX/0Lm;->REFRESH_AFTER_FINISH_INIT:LX/0Lm;

    aput-object v1, v0, v3

    sget-object v1, LX/0Lm;->REFRESH_AFTER_FINISH_CURRENT:LX/0Lm;

    aput-object v1, v0, v4

    sput-object v0, LX/0Lm;->$VALUES:[LX/0Lm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 44216
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44217
    iput p3, p0, LX/0Lm;->value:I

    .line 44218
    return-void
.end method

.method public static fromValue(I)LX/0Lm;
    .locals 5

    .prologue
    .line 44221
    invoke-static {}, LX/0Lm;->values()[LX/0Lm;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 44222
    iget v4, v0, LX/0Lm;->value:I

    if-ne v4, p0, :cond_0

    .line 44223
    :goto_1
    return-object v0

    .line 44224
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 44225
    :cond_1
    sget-object v0, LX/0Lm;->DEFAULT:LX/0Lm;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Lm;
    .locals 1

    .prologue
    .line 44220
    const-class v0, LX/0Lm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Lm;

    return-object v0
.end method

.method public static values()[LX/0Lm;
    .locals 1

    .prologue
    .line 44219
    sget-object v0, LX/0Lm;->$VALUES:[LX/0Lm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Lm;

    return-object v0
.end method
