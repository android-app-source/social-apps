.class public LX/0HM;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/io/File;

.field private final c:LX/0HI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37584
    const-class v0, LX/0HM;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0HM;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LX/05N;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "LX/05N",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37586
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 37587
    new-instance v1, LX/0HI;

    new-instance v2, LX/0HJ;

    invoke-direct {v2, p3}, LX/0HJ;-><init>(LX/05N;)V

    invoke-direct {v1, v0, v2, p4}, LX/0HI;-><init>(Ljava/lang/String;LX/0HJ;Ljava/lang/String;)V

    iput-object v1, p0, LX/0HM;->c:LX/0HI;

    .line 37588
    invoke-static {p1, p2}, LX/06c;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/0HM;->b:Ljava/io/File;

    .line 37589
    return-void
.end method

.method public static a(LX/0HM;Ljava/io/File;)Z
    .locals 9

    .prologue
    const/16 v7, 0xc8

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 37590
    sget-object v2, LX/0HM;->a:Ljava/lang/String;

    const-string v3, "Uploading file %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p1, v4, v0

    invoke-static {v2, v3, v4}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37591
    invoke-static {}, LX/06o;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 37592
    :cond_0
    :goto_0
    return v0

    .line 37593
    :cond_1
    :try_start_0
    invoke-static {p1}, LX/0HM;->b(Ljava/io/File;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 37594
    iget-object v3, p0, LX/0HM;->c:LX/0HI;

    .line 37595
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 37596
    const-string v4, "method"

    const-string v6, "logging.clientevent"

    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37597
    const-string v4, "format"

    const-string v6, "json"

    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37598
    const-string v4, "access_token"

    iget-object v6, v3, LX/0HI;->b:Ljava/lang/String;

    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37599
    :try_start_1
    const-string v4, "message"

    .line 37600
    if-nez v2, :cond_2

    .line 37601
    sget-object v6, LX/0HI;->a:Ljava/lang/String;

    const-string v8, "Json data cannot be null"

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {v6, v8, p0}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37602
    :cond_2
    const-string v6, "UTF-8"

    invoke-virtual {v2, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    .line 37603
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 37604
    new-instance p0, Ljava/util/zip/DeflaterOutputStream;

    invoke-direct {p0, v8}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 37605
    invoke-virtual {p0, v6}, Ljava/util/zip/DeflaterOutputStream;->write([B)V

    .line 37606
    invoke-virtual {p0}, Ljava/util/zip/DeflaterOutputStream;->close()V

    .line 37607
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    .line 37608
    const/4 v8, 0x2

    invoke-static {v6, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v6

    move-object v6, v6

    .line 37609
    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37610
    const-string v4, "compressed"

    const-string v6, "1"

    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 37611
    :goto_1
    iget-object v4, v3, LX/0HI;->c:LX/0HJ;

    iget-object v6, v3, LX/0HI;->d:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/0HJ;->a(Ljava/util/Map;Ljava/lang/String;)I

    move-result v4

    move v2, v4

    .line 37612
    if-ne v2, v7, :cond_4

    .line 37613
    sget-object v3, LX/0HM;->a:Ljava/lang/String;

    const-string v4, "Successful upload"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37614
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v3

    .line 37615
    if-nez v3, :cond_3

    .line 37616
    sget-object v3, LX/0HM;->a:Ljava/lang/String;

    const-string v4, "File %s was not deleted"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p1, v5, v0

    invoke-static {v3, v4, v5}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37617
    :cond_3
    :goto_2
    if-ne v2, v7, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 37618
    :catch_0
    move-exception v1

    .line 37619
    sget-object v2, LX/0HM;->a:Ljava/lang/String;

    const-string v3, "Unable to read file"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 37620
    :cond_4
    sget-object v3, LX/0HM;->a:Ljava/lang/String;

    const-string v4, "Unsuccessful upload. response code=%d"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 37621
    :catch_1
    move-exception v4

    .line 37622
    sget-object v6, LX/0HI;->a:Ljava/lang/String;

    const-string v8, "Unable to compress message to Json object, using original message"

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {v6, v4, v8, p0}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37623
    const-string v4, "message"

    invoke-interface {v5, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private static b(Ljava/io/File;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 37624
    const/4 v2, 0x0

    .line 37625
    :try_start_0
    new-instance v1, Ljava/io/InputStreamReader;

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v3, "UTF-8"

    invoke-direct {v1, v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 37626
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 37627
    const/16 v2, 0x400

    new-array v2, v2, [C

    .line 37628
    :goto_0
    invoke-virtual {v1, v2}, Ljava/io/Reader;->read([C)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 37629
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 37630
    :catchall_0
    move-exception v0

    .line 37631
    :goto_1
    if-eqz v1, :cond_0

    .line 37632
    :try_start_2
    invoke-virtual {v1}, Ljava/io/Reader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 37633
    :cond_0
    :goto_2
    throw v0

    .line 37634
    :cond_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 37635
    :try_start_4
    invoke-virtual {v1}, Ljava/io/Reader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 37636
    :goto_3
    return-object v0

    :catch_0
    goto :goto_3

    :catch_1
    goto :goto_2

    .line 37637
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method
