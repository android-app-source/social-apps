.class public final LX/0MW;
.super LX/0MR;
.source ""


# instance fields
.field private final b:LX/0Oj;

.field private final c:LX/0Oj;

.field private d:I

.field private e:Z

.field private f:I


# direct methods
.method public constructor <init>(LX/0LS;)V
    .locals 2

    .prologue
    .line 46163
    invoke-direct {p0, p1}, LX/0MR;-><init>(LX/0LS;)V

    .line 46164
    new-instance v0, LX/0Oj;

    sget-object v1, LX/0Oh;->a:[B

    invoke-direct {v0, v1}, LX/0Oj;-><init>([B)V

    iput-object v0, p0, LX/0MW;->b:LX/0Oj;

    .line 46165
    new-instance v0, LX/0Oj;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, LX/0MW;->c:LX/0Oj;

    .line 46166
    return-void
.end method

.method private static b(LX/0Oj;)LX/0MV;
    .locals 8

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 46167
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 46168
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    add-int/lit8 v2, v0, 0x1

    .line 46169
    const/4 v0, 0x3

    if-eq v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 46170
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 46171
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    and-int/lit8 v6, v0, 0x1f

    move v0, v3

    .line 46172
    :goto_1
    if-ge v0, v6, :cond_1

    .line 46173
    invoke-static {p0}, LX/0Oh;->a(LX/0Oj;)[B

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46174
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v3

    .line 46175
    goto :goto_0

    .line 46176
    :cond_1
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v5

    move v0, v3

    .line 46177
    :goto_2
    if-ge v0, v5, :cond_2

    .line 46178
    invoke-static {p0}, LX/0Oh;->a(LX/0Oj;)[B

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46179
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 46180
    :cond_2
    const/high16 v5, 0x3f800000    # 1.0f

    .line 46181
    if-lez v6, :cond_3

    .line 46182
    new-instance v4, LX/0Oi;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {v4, v0}, LX/0Oi;-><init>([B)V

    .line 46183
    add-int/lit8 v0, v2, 0x1

    mul-int/lit8 v0, v0, 0x8

    invoke-virtual {v4, v0}, LX/0Oi;->a(I)V

    .line 46184
    invoke-static {v4}, LX/0Oh;->a(LX/0Oi;)LX/0Og;

    move-result-object v0

    .line 46185
    iget v3, v0, LX/0Og;->b:I

    .line 46186
    iget v4, v0, LX/0Og;->c:I

    .line 46187
    iget v5, v0, LX/0Og;->d:F

    .line 46188
    :goto_3
    new-instance v0, LX/0MV;

    invoke-direct/range {v0 .. v5}, LX/0MV;-><init>(Ljava/util/List;IIIF)V

    return-object v0

    :cond_3
    move v3, v4

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/0Oj;J)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 46189
    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v1

    .line 46190
    invoke-virtual {p1}, LX/0Oj;->j()I

    move-result v3

    .line 46191
    int-to-long v4, v3

    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    add-long v8, p2, v4

    .line 46192
    if-nez v1, :cond_1

    iget-boolean v3, p0, LX/0MW;->e:Z

    if-nez v3, :cond_1

    .line 46193
    new-instance v1, LX/0Oj;

    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v3

    new-array v3, v3, [B

    invoke-direct {v1, v3}, LX/0Oj;-><init>([B)V

    .line 46194
    iget-object v3, v1, LX/0Oj;->a:[B

    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v4

    invoke-virtual {p1, v3, v6, v4}, LX/0Oj;->a([BII)V

    .line 46195
    invoke-static {v1}, LX/0MW;->b(LX/0Oj;)LX/0MV;

    move-result-object v3

    .line 46196
    iget v1, v3, LX/0MV;->b:I

    iput v1, p0, LX/0MW;->d:I

    .line 46197
    const-string v1, "video/avc"

    invoke-virtual {p0}, LX/0MR;->a()J

    move-result-wide v4

    iget v6, v3, LX/0MV;->d:I

    iget v7, v3, LX/0MV;->e:I

    iget-object v8, v3, LX/0MV;->a:Ljava/util/List;

    iget v10, v3, LX/0MV;->c:F

    move v3, v2

    move v9, v2

    invoke-static/range {v0 .. v10}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)LX/0L4;

    move-result-object v0

    .line 46198
    iget-object v1, p0, LX/0MR;->a:LX/0LS;

    invoke-interface {v1, v0}, LX/0LS;->a(LX/0L4;)V

    .line 46199
    iput-boolean v11, p0, LX/0MW;->e:Z

    .line 46200
    :cond_0
    :goto_0
    return-void

    .line 46201
    :cond_1
    if-ne v1, v11, :cond_0

    .line 46202
    iget-object v1, p0, LX/0MW;->c:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    .line 46203
    aput-byte v6, v1, v6

    .line 46204
    aput-byte v6, v1, v11

    .line 46205
    const/4 v2, 0x2

    aput-byte v6, v1, v2

    .line 46206
    iget v1, p0, LX/0MW;->d:I

    rsub-int/lit8 v1, v1, 0x4

    move v5, v6

    .line 46207
    :goto_1
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v2

    if-lez v2, :cond_2

    .line 46208
    iget-object v2, p0, LX/0MW;->c:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    iget v3, p0, LX/0MW;->d:I

    invoke-virtual {p1, v2, v1, v3}, LX/0Oj;->a([BII)V

    .line 46209
    iget-object v2, p0, LX/0MW;->c:LX/0Oj;

    invoke-virtual {v2, v6}, LX/0Oj;->b(I)V

    .line 46210
    iget-object v2, p0, LX/0MW;->c:LX/0Oj;

    invoke-virtual {v2}, LX/0Oj;->s()I

    move-result v2

    .line 46211
    iget-object v3, p0, LX/0MW;->b:LX/0Oj;

    invoke-virtual {v3, v6}, LX/0Oj;->b(I)V

    .line 46212
    iget-object v3, p0, LX/0MR;->a:LX/0LS;

    iget-object v4, p0, LX/0MW;->b:LX/0Oj;

    const/4 v7, 0x4

    invoke-interface {v3, v4, v7}, LX/0LS;->a(LX/0Oj;I)V

    .line 46213
    add-int/lit8 v3, v5, 0x4

    .line 46214
    iget-object v4, p0, LX/0MR;->a:LX/0LS;

    invoke-interface {v4, p1, v2}, LX/0LS;->a(LX/0Oj;I)V

    .line 46215
    add-int v5, v3, v2

    goto :goto_1

    .line 46216
    :cond_2
    iget-object v1, p0, LX/0MR;->a:LX/0LS;

    iget v2, p0, LX/0MW;->f:I

    if-ne v2, v11, :cond_3

    move v4, v11

    :goto_2
    move-wide v2, v8

    move-object v7, v0

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    goto :goto_0

    :cond_3
    move v4, v6

    goto :goto_2
.end method

.method public final a(LX/0Oj;)Z
    .locals 4

    .prologue
    .line 46217
    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v0

    .line 46218
    shr-int/lit8 v1, v0, 0x4

    and-int/lit8 v1, v1, 0xf

    .line 46219
    and-int/lit8 v0, v0, 0xf

    .line 46220
    const/4 v2, 0x7

    if-eq v0, v2, :cond_0

    .line 46221
    new-instance v1, LX/0MU;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Video format not supported: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0MU;-><init>(Ljava/lang/String;)V

    throw v1

    .line 46222
    :cond_0
    iput v1, p0, LX/0MW;->f:I

    .line 46223
    const/4 v0, 0x5

    if-eq v1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
