.class public LX/0J0;
.super LX/03T;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/0J0;


# instance fields
.field private final b:LX/0eM;

.field private final c:LX/0eL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39130
    const-class v0, LX/0J0;

    sput-object v0, LX/0J0;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0eM;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 39131
    invoke-direct {p0}, LX/03T;-><init>()V

    .line 39132
    new-instance v0, LX/0Iz;

    invoke-direct {v0, p0}, LX/0Iz;-><init>(LX/0J0;)V

    iput-object v0, p0, LX/0J0;->c:LX/0eL;

    .line 39133
    iput-object p1, p0, LX/0J0;->b:LX/0eM;

    .line 39134
    return-void
.end method

.method public static a(LX/0QB;)LX/0J0;
    .locals 3

    .prologue
    .line 39118
    sget-object v0, LX/0J0;->d:LX/0J0;

    if-nez v0, :cond_1

    .line 39119
    const-class v1, LX/0J0;

    monitor-enter v1

    .line 39120
    :try_start_0
    sget-object v0, LX/0J0;->d:LX/0J0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 39121
    if-eqz v2, :cond_0

    .line 39122
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0J0;->b(LX/0QB;)LX/0J0;

    move-result-object v0

    sput-object v0, LX/0J0;->d:LX/0J0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 39123
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 39124
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 39125
    :cond_1
    sget-object v0, LX/0J0;->d:LX/0J0;

    return-object v0

    .line 39126
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 39127
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/0J0;
    .locals 2

    .prologue
    .line 39128
    new-instance v1, LX/0J0;

    invoke-static {p0}, LX/0eM;->a(LX/0QB;)LX/0eM;

    move-result-object v0

    check-cast v0, LX/0eM;

    invoke-direct {v1, v0}, LX/0J0;-><init>(LX/0eM;)V

    .line 39129
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 39114
    iget-object v0, p0, LX/0J0;->b:LX/0eM;

    iget-object v1, p0, LX/0J0;->c:LX/0eL;

    invoke-virtual {v0, v1}, LX/0eM;->a(LX/0eL;)V

    .line 39115
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 39116
    iget-object v0, p0, LX/0J0;->b:LX/0eM;

    iget-object v1, p0, LX/0J0;->c:LX/0eL;

    invoke-virtual {v0, v1}, LX/0eM;->b(LX/0eL;)V

    .line 39117
    return-void
.end method
