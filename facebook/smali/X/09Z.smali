.class public LX/09Z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/07G;

.field private final b:Ljava/net/InetAddress;

.field public final c:Ljava/net/InetAddress;

.field private final d:I

.field private final e:I

.field private final f:Ljava/util/concurrent/ScheduledExecutorService;

.field private final g:I

.field private h:Ljava/net/Socket;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/net/InetAddress;Ljava/net/InetAddress;IILX/07G;Ljava/util/concurrent/ScheduledExecutorService;I)V
    .locals 0

    .prologue
    .line 22565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22566
    iput-object p5, p0, LX/09Z;->a:LX/07G;

    .line 22567
    iput-object p1, p0, LX/09Z;->b:Ljava/net/InetAddress;

    .line 22568
    iput-object p2, p0, LX/09Z;->c:Ljava/net/InetAddress;

    .line 22569
    iput p3, p0, LX/09Z;->d:I

    .line 22570
    iput p4, p0, LX/09Z;->e:I

    .line 22571
    iput-object p6, p0, LX/09Z;->f:Ljava/util/concurrent/ScheduledExecutorService;

    .line 22572
    iput p7, p0, LX/09Z;->g:I

    .line 22573
    return-void
.end method

.method public static a$redex0(LX/09Z;Ljava/net/Socket;Ljava/net/InetAddress;Ljava/net/Socket;)V
    .locals 4

    .prologue
    .line 22557
    invoke-static {p1}, LX/07H;->a(Ljava/net/Socket;)V

    .line 22558
    const-string v0, "HappyEyeballsSocketFactory"

    const-string v1, "Connecting to %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22559
    new-instance v0, Ljava/net/InetSocketAddress;

    iget v1, p0, LX/09Z;->d:I

    invoke-direct {v0, p2, v1}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    iget v1, p0, LX/09Z;->e:I

    invoke-virtual {p1, v0, v1}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 22560
    monitor-enter p0

    .line 22561
    :try_start_0
    iget-object v0, p0, LX/09Z;->h:Ljava/net/Socket;

    if-nez v0, :cond_0

    .line 22562
    iput-object p1, p0, LX/09Z;->h:Ljava/net/Socket;

    .line 22563
    invoke-static {p3}, LX/07H;->b(Ljava/net/Socket;)V

    .line 22564
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()Ljava/net/Socket;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 22536
    const-string v0, "HappyEyeballsSocketFactory"

    const-string v1, "getSocket for %s and %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/09Z;->b:Ljava/net/InetAddress;

    aput-object v3, v2, v7

    iget-object v3, p0, LX/09Z;->c:Ljava/net/InetAddress;

    aput-object v3, v2, v8

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22537
    invoke-static {}, LX/07G;->a()Ljava/net/Socket;

    move-result-object v1

    .line 22538
    invoke-static {}, LX/07G;->a()Ljava/net/Socket;

    move-result-object v2

    .line 22539
    iget-object v0, p0, LX/09Z;->f:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, LX/09a;

    invoke-direct {v3, p0, v2, v1}, LX/09a;-><init>(LX/09Z;Ljava/net/Socket;Ljava/net/Socket;)V

    iget v4, p0, LX/09Z;->g:I

    int-to-long v4, v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v3, v4, v5, v6}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v3

    .line 22540
    :try_start_0
    iget-object v0, p0, LX/09Z;->b:Ljava/net/InetAddress;

    invoke-static {p0, v1, v0, v2}, LX/09Z;->a$redex0(LX/09Z;Ljava/net/Socket;Ljava/net/InetAddress;Ljava/net/Socket;)V

    .line 22541
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22542
    :goto_0
    monitor-enter p0

    .line 22543
    :try_start_1
    iget-object v0, p0, LX/09Z;->h:Ljava/net/Socket;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/09Z;->h:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22544
    const-string v0, "HappyEyeballsSocketFactory"

    const-string v1, "HE is returning socket connected to %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/09Z;->h:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22545
    iget-object v0, p0, LX/09Z;->h:Ljava/net/Socket;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0

    .line 22546
    :catch_0
    move-exception v0

    .line 22547
    invoke-static {v1}, LX/07H;->b(Ljava/net/Socket;)V

    .line 22548
    const v1, -0x110a1eb9

    :try_start_2
    invoke-static {v3, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 22549
    :catch_1
    move-exception v1

    .line 22550
    invoke-static {v2}, LX/07H;->b(Ljava/net/Socket;)V

    .line 22551
    const-string v2, "HappyEyeballsSocketFactory"

    const-string v3, "Failed to connect to both sockets: %s"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v1, v4, v7

    invoke-static {v2, v0, v3, v4}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22552
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-lt v2, v3, :cond_0

    .line 22553
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to connect to both sockets: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 22554
    :cond_0
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to connect to both sockets: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 22555
    :cond_1
    :try_start_3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "socket connect call succeeded but socket is not connected."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22556
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method
