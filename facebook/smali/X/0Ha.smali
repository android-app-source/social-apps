.class public LX/0Ha;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:LX/0Hd;

.field private final b:LX/0BF;

.field private final c:LX/06p;

.field private final d:LX/0BB;

.field private final e:LX/0Hb;

.field private final f:LX/06s;

.field private final g:LX/06u;

.field private final h:LX/06v;

.field public final i:Z

.field private final j:Z


# direct methods
.method public constructor <init>(LX/0Hd;LX/0BF;LX/06p;LX/0BB;LX/0Hb;LX/06s;LX/06u;LX/06v;ZZ)V
    .locals 0

    .prologue
    .line 37837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37838
    iput-object p1, p0, LX/0Ha;->a:LX/0Hd;

    .line 37839
    iput-object p2, p0, LX/0Ha;->b:LX/0BF;

    .line 37840
    iput-object p3, p0, LX/0Ha;->c:LX/06p;

    .line 37841
    iput-object p4, p0, LX/0Ha;->d:LX/0BB;

    .line 37842
    iput-object p5, p0, LX/0Ha;->e:LX/0Hb;

    .line 37843
    iput-object p6, p0, LX/0Ha;->f:LX/06s;

    .line 37844
    iput-object p7, p0, LX/0Ha;->g:LX/06u;

    .line 37845
    iput-object p8, p0, LX/0Ha;->h:LX/06v;

    .line 37846
    iput-boolean p9, p0, LX/0Ha;->i:Z

    .line 37847
    iput-boolean p10, p0, LX/0Ha;->j:Z

    .line 37848
    return-void
.end method

.method public constructor <init>(LX/0Hd;LX/0BF;LX/06p;LX/0BB;LX/0Hb;LX/06s;LX/06u;Z)V
    .locals 11

    .prologue
    .line 37899
    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v10}, LX/0Ha;-><init>(LX/0Hd;LX/0BF;LX/06p;LX/0BB;LX/0Hb;LX/06s;LX/06u;LX/06v;ZZ)V

    .line 37900
    return-void
.end method

.method public static a(LX/0Ha;Z)Lorg/json/JSONObject;
    .locals 8

    .prologue
    .line 37853
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 37854
    iget-object v1, p0, LX/0Ha;->a:LX/0Hd;

    if-eqz v1, :cond_0

    .line 37855
    iget-object v1, p0, LX/0Ha;->a:LX/0Hd;

    .line 37856
    iget-object v2, v1, LX/06q;->b:Ljava/lang/String;

    move-object v1, v2

    .line 37857
    iget-object v2, p0, LX/0Ha;->a:LX/0Hd;

    invoke-virtual {v2, p1}, LX/06q;->a(Z)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37858
    :cond_0
    iget-object v1, p0, LX/0Ha;->b:LX/0BF;

    if-eqz v1, :cond_1

    .line 37859
    iget-object v1, p0, LX/0Ha;->b:LX/0BF;

    .line 37860
    iget-object v2, v1, LX/06q;->b:Ljava/lang/String;

    move-object v1, v2

    .line 37861
    iget-object v2, p0, LX/0Ha;->b:LX/0BF;

    invoke-virtual {v2, p1}, LX/06q;->a(Z)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37862
    :cond_1
    iget-object v1, p0, LX/0Ha;->c:LX/06p;

    if-eqz v1, :cond_2

    .line 37863
    iget-object v1, p0, LX/0Ha;->c:LX/06p;

    .line 37864
    iget-object v2, v1, LX/06q;->b:Ljava/lang/String;

    move-object v1, v2

    .line 37865
    iget-object v2, p0, LX/0Ha;->c:LX/06p;

    invoke-virtual {v2, p1}, LX/06q;->a(Z)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37866
    :cond_2
    iget-object v1, p0, LX/0Ha;->d:LX/0BB;

    if-eqz v1, :cond_3

    .line 37867
    iget-object v1, p0, LX/0Ha;->d:LX/0BB;

    .line 37868
    iget-object v2, v1, LX/06q;->b:Ljava/lang/String;

    move-object v1, v2

    .line 37869
    iget-object v2, p0, LX/0Ha;->d:LX/0BB;

    invoke-virtual {v2, p1}, LX/06q;->a(Z)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37870
    :cond_3
    iget-object v1, p0, LX/0Ha;->e:LX/0Hb;

    if-eqz v1, :cond_4

    .line 37871
    const-string v1, "ss"

    iget-object v2, p0, LX/0Ha;->e:LX/0Hb;

    .line 37872
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 37873
    const-string v5, "ssr"

    iget-object v6, v2, LX/0Hb;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37874
    const-string v5, "ssg"

    iget-wide v6, v2, LX/0Hb;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37875
    const-string v5, "mcd"

    iget-wide v6, v2, LX/0Hb;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37876
    const-string v5, "mfcl"

    iget-wide v6, v2, LX/0Hb;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37877
    const-string v5, "mcg"

    iget-wide v6, v2, LX/0Hb;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37878
    const-string v5, "ssgp"

    iget-object v6, v2, LX/0Hb;->f:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37879
    const-string v5, "msgp"

    iget-object v6, v2, LX/0Hb;->g:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37880
    const-string v5, "ntgp"

    iget-object v6, v2, LX/0Hb;->h:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37881
    const-string v5, "mntgp"

    iget-object v6, v2, LX/0Hb;->i:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37882
    const-string v5, "ssggp"

    iget-wide v6, v2, LX/0Hb;->j:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37883
    const-string v5, "mcggp"

    iget-wide v6, v2, LX/0Hb;->k:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37884
    move-object v2, v4

    .line 37885
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37886
    :cond_4
    iget-object v1, p0, LX/0Ha;->f:LX/06s;

    if-eqz v1, :cond_5

    .line 37887
    iget-object v1, p0, LX/0Ha;->f:LX/06s;

    .line 37888
    iget-object v2, v1, LX/06t;->e:Ljava/lang/String;

    move-object v1, v2

    .line 37889
    iget-object v2, p0, LX/0Ha;->f:LX/06s;

    iget-boolean v3, p0, LX/0Ha;->j:Z

    invoke-virtual {v2, v3}, LX/06t;->a(Z)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37890
    :cond_5
    iget-object v1, p0, LX/0Ha;->g:LX/06u;

    if-eqz v1, :cond_6

    .line 37891
    iget-object v1, p0, LX/0Ha;->g:LX/06u;

    .line 37892
    iget-object v2, v1, LX/06t;->e:Ljava/lang/String;

    move-object v1, v2

    .line 37893
    iget-object v2, p0, LX/0Ha;->g:LX/06u;

    iget-boolean v3, p0, LX/0Ha;->j:Z

    invoke-virtual {v2, v3}, LX/06t;->a(Z)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37894
    :cond_6
    iget-object v1, p0, LX/0Ha;->h:LX/06v;

    if-eqz v1, :cond_7

    .line 37895
    iget-object v1, p0, LX/0Ha;->h:LX/06v;

    .line 37896
    iget-object v2, v1, LX/06t;->e:Ljava/lang/String;

    move-object v1, v2

    .line 37897
    iget-object v2, p0, LX/0Ha;->h:LX/06v;

    iget-boolean v3, p0, LX/0Ha;->j:Z

    invoke-virtual {v2, v3}, LX/06t;->a(Z)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37898
    :cond_7
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37851
    :try_start_0
    iget-boolean v0, p0, LX/0Ha;->i:Z

    invoke-static {p0, v0}, LX/0Ha;->a(LX/0Ha;Z)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 37852
    :goto_0
    return-object v0

    :catch_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37849
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, v0}, LX/0Ha;->a(LX/0Ha;Z)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 37850
    :goto_0
    return-object v0

    :catch_0
    const-string v0, ""

    goto :goto_0
.end method
