.class public LX/07a;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/DataInputStream;)Landroid/util/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInputStream;",
            ")",
            "Landroid/util/Pair",
            "<",
            "LX/07R;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 20044
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v4

    .line 20045
    shr-int/lit8 v1, v4, 0x4

    invoke-static {v1}, LX/07S;->fromInt(I)LX/07S;

    move-result-object v1

    .line 20046
    and-int/lit8 v2, v4, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    move v2, v0

    .line 20047
    :goto_0
    and-int/lit8 v3, v4, 0x6

    shr-int/lit8 v3, v3, 0x1

    .line 20048
    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    move v4, v0

    :goto_1
    move v6, v0

    .line 20049
    :cond_0
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v7

    .line 20050
    and-int/lit8 v8, v7, 0x7f

    mul-int/2addr v8, v0

    add-int/2addr v5, v8

    .line 20051
    mul-int/lit16 v0, v0, 0x80

    .line 20052
    add-int/lit8 v6, v6, 0x1

    .line 20053
    and-int/lit16 v7, v7, 0x80

    if-nez v7, :cond_0

    .line 20054
    add-int/2addr v6, v5

    .line 20055
    new-instance v0, LX/07R;

    invoke-direct/range {v0 .. v5}, LX/07R;-><init>(LX/07S;ZIZI)V

    .line 20056
    new-instance v1, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1

    :cond_1
    move v2, v5

    .line 20057
    goto :goto_0

    :cond_2
    move v4, v5

    .line 20058
    goto :goto_1
.end method
