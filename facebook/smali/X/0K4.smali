.class public LX/0K4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x8
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "LX/03z;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40123
    iput-object p1, p0, LX/0K4;->a:Ljava/lang/String;

    .line 40124
    return-void
.end method


# virtual methods
.method public final a()LX/03z;
    .locals 6

    .prologue
    .line 40125
    sget-object v0, LX/03z;->UNKNOWN:LX/03z;

    .line 40126
    iget-object v1, p0, LX/0K4;->b:Ljava/util/LinkedHashSet;

    if-nez v1, :cond_2

    .line 40127
    const/16 v5, 0x22

    .line 40128
    iget-object v1, p0, LX/0K4;->b:Ljava/util/LinkedHashSet;

    if-nez v1, :cond_2

    .line 40129
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v1, p0, LX/0K4;->b:Ljava/util/LinkedHashSet;

    .line 40130
    const/4 v1, 0x0

    .line 40131
    :cond_0
    :goto_0
    if-ltz v1, :cond_2

    .line 40132
    iget-object v2, p0, LX/0K4;->a:Ljava/lang/String;

    const-string v3, "value=\""

    invoke-virtual {v2, v3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 40133
    if-ltz v1, :cond_0

    .line 40134
    iget-object v2, p0, LX/0K4;->a:Ljava/lang/String;

    const/16 v3, 0x3c

    invoke-virtual {v2, v3, v1}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v2

    .line 40135
    iget-object v3, p0, LX/0K4;->a:Ljava/lang/String;

    const-string v4, "AudioChannelConfiguration"

    invoke-virtual {v3, v4, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v3

    .line 40136
    if-ge v2, v3, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 40137
    if-eqz v2, :cond_1

    .line 40138
    iget-object v2, p0, LX/0K4;->a:Ljava/lang/String;

    invoke-virtual {v2, v5, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 40139
    iget-object v2, p0, LX/0K4;->a:Ljava/lang/String;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 40140
    if-ltz v2, :cond_1

    .line 40141
    iget-object v3, p0, LX/0K4;->a:Ljava/lang/String;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v3, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/03z;->fromChannelConfiguration(Ljava/lang/String;)LX/03z;

    move-result-object v2

    .line 40142
    sget-object v3, LX/03z;->UNKNOWN:LX/03z;

    if-eq v2, v3, :cond_1

    .line 40143
    iget-object v3, p0, LX/0K4;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v3, v2}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 40144
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40145
    :cond_2
    iget-object v1, p0, LX/0K4;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 40146
    iget-object v0, p0, LX/0K4;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03z;

    .line 40147
    :cond_3
    return-object v0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method
