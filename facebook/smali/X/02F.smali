.class public LX/02F;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)V
    .locals 1

    .prologue
    const/4 v0, 0x2

    .line 6640
    invoke-static {v0, v0, p0, p1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 6641
    return-void
.end method

.method public static a(Landroid/content/Intent;II)V
    .locals 2

    .prologue
    .line 6638
    const/4 v0, 0x2

    const/16 v1, 0x27

    invoke-static {p0, v0, v1, p1, p2}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 6639
    return-void
.end method

.method public static a(Landroid/content/Intent;IIII)V
    .locals 8
    .param p0    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 6632
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 6633
    :goto_0
    if-nez v7, :cond_1

    .line 6634
    invoke-static {p1, p2, p3, p4}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 6635
    :goto_1
    return-void

    .line 6636
    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    .line 6637
    :cond_1
    const-wide/16 v4, 0x0

    const-string v6, "Intent action"

    move v0, p1

    move v1, p2

    move v2, p3

    move v3, p4

    invoke-static/range {v0 .. v7}, Lcom/facebook/loom/logger/Logger;->a(IIIIJLjava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static c(II)V
    .locals 2

    .prologue
    .line 6622
    const/4 v0, 0x2

    const/16 v1, 0x23

    invoke-static {v0, v1, p0, p1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 6623
    return-void
.end method

.method public static d(II)V
    .locals 2

    .prologue
    .line 6643
    const/4 v0, 0x2

    const/16 v1, 0x25

    invoke-static {v0, v1, p0, p1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 6644
    return-void
.end method

.method public static e(II)V
    .locals 2

    .prologue
    .line 6630
    const/4 v0, 0x2

    const/16 v1, 0x27

    invoke-static {v0, v1, p0, p1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 6631
    return-void
.end method

.method public static f(II)V
    .locals 2

    .prologue
    .line 6628
    const/4 v0, 0x2

    const/16 v1, 0x2b

    invoke-static {v0, v1, p0, p1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 6629
    return-void
.end method

.method public static g(II)V
    .locals 2

    .prologue
    .line 6626
    const/4 v0, 0x2

    const/16 v1, 0x2d

    invoke-static {v0, v1, p0, p1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 6627
    return-void
.end method

.method public static h(II)V
    .locals 2

    .prologue
    .line 6624
    const/16 v0, 0x8

    const/16 v1, 0x1f

    invoke-static {v0, v1, p0, p1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 6625
    return-void
.end method
