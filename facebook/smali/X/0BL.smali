.class public final LX/0BL;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0BN;

.field private static final b:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "LX/0Iy;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0BQ;

.field private static final d:LX/0BQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26331
    new-instance v0, LX/0BM;

    invoke-direct {v0}, LX/0BM;-><init>()V

    sput-object v0, LX/0BL;->a:LX/0BN;

    .line 26332
    new-instance v0, LX/0BO;

    invoke-direct {v0}, LX/0BO;-><init>()V

    sput-object v0, LX/0BL;->b:Ljava/lang/ThreadLocal;

    .line 26333
    new-instance v0, LX/0BP;

    invoke-direct {v0}, LX/0BP;-><init>()V

    sput-object v0, LX/0BL;->c:LX/0BQ;

    .line 26334
    new-instance v0, LX/0BR;

    invoke-direct {v0}, LX/0BR;-><init>()V

    sput-object v0, LX/0BL;->d:LX/0BQ;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26330
    return-void
.end method

.method public static a(J)LX/0BN;
    .locals 2

    .prologue
    .line 26318
    sget-object v0, LX/0BL;->d:LX/0BQ;

    const-string v1, ""

    invoke-static {p0, p1, v0, v1}, LX/0BL;->a(JLX/0BQ;Ljava/lang/String;)LX/0BN;

    move-result-object v0

    return-object v0
.end method

.method private static a(JLX/0BQ;Ljava/lang/String;)LX/0BN;
    .locals 2

    .prologue
    .line 26320
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26321
    sget-object v0, LX/0BL;->a:LX/0BN;

    .line 26322
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0BL;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Iy;

    .line 26323
    iput-object p2, v0, LX/0Iy;->a:LX/0BQ;

    .line 26324
    iget-object v1, v0, LX/0Iy;->b:Ljava/lang/StringBuilder;

    const/4 p0, 0x0

    iget-object p1, v0, LX/0Iy;->b:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 26325
    iget-object v1, v0, LX/0Iy;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26326
    const/16 v1, 0x7c

    iput-char v1, v0, LX/0Iy;->c:C

    .line 26327
    move-object v0, v0

    .line 26328
    goto :goto_0
.end method

.method public static a(JLjava/lang/String;)LX/0BN;
    .locals 2

    .prologue
    .line 26319
    sget-object v0, LX/0BL;->c:LX/0BQ;

    invoke-static {p0, p1, v0, p2}, LX/0BL;->a(JLX/0BQ;Ljava/lang/String;)LX/0BN;

    move-result-object v0

    return-object v0
.end method
