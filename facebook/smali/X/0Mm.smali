.class public final LX/0Mm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 48107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([B)Ljava/util/UUID;
    .locals 11

    .prologue
    .line 48108
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v1, 0x0

    .line 48109
    new-instance v2, LX/0Oj;

    invoke-direct {v2, p0}, LX/0Oj;-><init>([B)V

    .line 48110
    iget v3, v2, LX/0Oj;->c:I

    move v3, v3

    .line 48111
    const/16 v4, 0x20

    if-ge v3, v4, :cond_2

    .line 48112
    :cond_0
    :goto_0
    move-object v0, v1

    .line 48113
    if-nez v0, :cond_1

    .line 48114
    const/4 v0, 0x0

    .line 48115
    :goto_1
    return-object v0

    :cond_1
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/UUID;

    goto :goto_1

    .line 48116
    :cond_2
    invoke-virtual {v2, v9}, LX/0Oj;->b(I)V

    .line 48117
    invoke-virtual {v2}, LX/0Oj;->m()I

    move-result v3

    .line 48118
    invoke-virtual {v2}, LX/0Oj;->b()I

    move-result v4

    add-int/lit8 v4, v4, 0x4

    if-ne v3, v4, :cond_0

    .line 48119
    invoke-virtual {v2}, LX/0Oj;->m()I

    move-result v3

    .line 48120
    sget v4, LX/0Mc;->R:I

    if-ne v3, v4, :cond_0

    .line 48121
    invoke-virtual {v2}, LX/0Oj;->m()I

    move-result v3

    invoke-static {v3}, LX/0Mc;->a(I)I

    move-result v3

    .line 48122
    if-le v3, v10, :cond_3

    .line 48123
    const-string v2, "PsshAtomUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unsupported pssh version: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 48124
    :cond_3
    new-instance v4, Ljava/util/UUID;

    invoke-virtual {v2}, LX/0Oj;->o()J

    move-result-wide v5

    invoke-virtual {v2}, LX/0Oj;->o()J

    move-result-wide v7

    invoke-direct {v4, v5, v6, v7, v8}, Ljava/util/UUID;-><init>(JJ)V

    .line 48125
    if-ne v3, v10, :cond_4

    .line 48126
    invoke-virtual {v2}, LX/0Oj;->s()I

    move-result v3

    .line 48127
    mul-int/lit8 v3, v3, 0x10

    invoke-virtual {v2, v3}, LX/0Oj;->c(I)V

    .line 48128
    :cond_4
    invoke-virtual {v2}, LX/0Oj;->s()I

    move-result v3

    .line 48129
    invoke-virtual {v2}, LX/0Oj;->b()I

    move-result v5

    if-ne v3, v5, :cond_0

    .line 48130
    new-array v1, v3, [B

    .line 48131
    invoke-virtual {v2, v1, v9, v3}, LX/0Oj;->a([BII)V

    .line 48132
    invoke-static {v4, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    goto :goto_0
.end method
