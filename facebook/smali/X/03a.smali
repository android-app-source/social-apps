.class public LX/03a;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/03b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/03b",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 10520
    invoke-static {}, LX/03b;->a()LX/03b;

    move-result-object v0

    sput-object v0, LX/03a;->a:LX/03b;

    .line 10521
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    sput-object v0, LX/03a;->b:Ljava/lang/ref/ReferenceQueue;

    .line 10522
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    sput-object v0, LX/03a;->c:Ljava/lang/ref/ReferenceQueue;

    .line 10523
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/03a;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Runnable;I)Ljava/lang/Runnable;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 10514
    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10515
    :goto_0
    return-object p0

    .line 10516
    :cond_0
    const/16 v0, 0xe

    invoke-static {v1, v0, p1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 10517
    new-instance v0, Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;

    invoke-direct {v0, p0, v1, p1}, Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;-><init>(Ljava/lang/Runnable;II)V

    .line 10518
    invoke-static {p0, v0}, LX/03a;->a(Ljava/lang/Runnable;Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;)V

    move-object p0, v0

    .line 10519
    goto :goto_0
.end method

.method private static a()V
    .locals 5

    .prologue
    const/16 v4, 0x32

    .line 10498
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 10499
    :cond_0
    :goto_0
    return-void

    .line 10500
    :cond_1
    sget-object v0, LX/03a;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    if-lt v0, v4, :cond_0

    .line 10501
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 10502
    sget-object v1, LX/03a;->a:LX/03b;

    monitor-enter v1

    .line 10503
    :try_start_0
    sget-object v0, LX/03a;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-ge v0, v4, :cond_2

    .line 10504
    monitor-exit v1

    goto :goto_0

    .line 10505
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 10506
    :cond_2
    :goto_1
    :try_start_1
    sget-object v0, LX/03a;->b:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 10507
    check-cast v0, LX/03b;

    .line 10508
    invoke-virtual {v0}, LX/03b;->b()V

    goto :goto_1

    .line 10509
    :cond_3
    :goto_2
    sget-object v0, LX/03a;->c:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 10510
    check-cast v0, LX/03b;

    .line 10511
    invoke-virtual {v0}, LX/03b;->b()V

    goto :goto_2

    .line 10512
    :cond_4
    sget-object v0, LX/03a;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 10513
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static a(Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 10525
    invoke-virtual {p0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 10526
    sget-object v2, LX/03a;->a:LX/03b;

    monitor-enter v2

    .line 10527
    :try_start_0
    sget-object v0, LX/03a;->a:LX/03b;

    invoke-static {v0, p1}, LX/03b;->a(LX/03b;Ljava/lang/Object;)LX/03b;

    move-result-object v3

    .line 10528
    sget-object v0, LX/03a;->a:LX/03b;

    if-eq v3, v0, :cond_1

    .line 10529
    iget-object v0, v3, LX/03b;->b:LX/03b;

    move-object v1, v0

    .line 10530
    :goto_0
    if-eq v1, v3, :cond_1

    .line 10531
    invoke-virtual {v1}, LX/03b;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 10532
    if-eqz v0, :cond_0

    .line 10533
    invoke-virtual {p0, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 10534
    :cond_0
    iget-object v0, v1, LX/03b;->b:LX/03b;

    move-object v1, v0

    .line 10535
    goto :goto_0

    .line 10536
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10537
    invoke-static {}, LX/03a;->a()V

    .line 10538
    return-void

    .line 10539
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(Ljava/lang/Runnable;Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;)V
    .locals 4

    .prologue
    .line 10488
    sget-object v1, LX/03a;->a:LX/03b;

    monitor-enter v1

    .line 10489
    :try_start_0
    sget-object v0, LX/03a;->a:LX/03b;

    invoke-static {v0, p0}, LX/03b;->a(LX/03b;Ljava/lang/Object;)LX/03b;

    move-result-object v0

    .line 10490
    sget-object v2, LX/03a;->a:LX/03b;

    if-ne v0, v2, :cond_0

    .line 10491
    new-instance v0, LX/03b;

    sget-object v2, LX/03a;->b:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0, p0, v2}, LX/03b;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 10492
    sget-object v2, LX/03a;->a:LX/03b;

    invoke-virtual {v2, v0}, LX/03b;->b(LX/03b;)V

    .line 10493
    :cond_0
    new-instance v2, LX/03b;

    sget-object v3, LX/03a;->c:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v2, p1, v3}, LX/03b;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-virtual {v0, v2}, LX/03b;->a(LX/03b;)V

    .line 10494
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10495
    invoke-static {}, LX/03a;->a()V

    .line 10496
    return-void

    .line 10497
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    .locals 1

    .prologue
    .line 10487
    invoke-static {p1, p2}, LX/03a;->a(Ljava/lang/Runnable;I)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    .locals 2

    .prologue
    .line 10486
    invoke-static {p1, p4}, LX/03a;->a(Ljava/lang/Runnable;I)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    .locals 1

    .prologue
    .line 10484
    invoke-static {p1, p2}, LX/03a;->a(Ljava/lang/Runnable;I)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    .locals 2

    .prologue
    .line 10485
    invoke-static {p1, p4}, LX/03a;->a(Ljava/lang/Runnable;I)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    return v0
.end method
