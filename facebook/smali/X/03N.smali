.class public LX/03N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "StringFormatUse",
        "BadMethodUse-android.util.Log.d"
    }
.end annotation


# instance fields
.field private a:Landroid/os/HandlerThread;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private b:Landroid/os/Handler;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final c:LX/02q;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private d:LX/037;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/02q;)V
    .locals 1
    .param p1    # LX/02q;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 10213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10214
    iput-object v0, p0, LX/03N;->a:Landroid/os/HandlerThread;

    .line 10215
    iput-object v0, p0, LX/03N;->b:Landroid/os/Handler;

    .line 10216
    iput-object p1, p0, LX/03N;->c:LX/02q;

    .line 10217
    iput-object v0, p0, LX/03N;->d:LX/037;

    .line 10218
    return-void
.end method

.method private static declared-synchronized b(LX/03N;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.os.HandlerThread._Constructor",
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .prologue
    .line 10207
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/03N;->b:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 10208
    :goto_0
    monitor-exit p0

    return-void

    .line 10209
    :cond_0
    :try_start_1
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Trace Controller"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/03N;->a:Landroid/os/HandlerThread;

    .line 10210
    iget-object v0, p0, LX/03N;->a:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 10211
    new-instance v0, LX/09h;

    iget-object v1, p0, LX/03N;->a:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/09h;-><init>(LX/03N;Landroid/os/Looper;)V

    iput-object v0, p0, LX/03N;->b:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 10212
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 10204
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/03N;->b(LX/03N;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10205
    monitor-exit p0

    return-void

    .line 10206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/037;)V
    .locals 2

    .prologue
    .line 10152
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/03N;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 10153
    iget-object v0, p0, LX/03N;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 10154
    :cond_0
    invoke-static {}, Lcom/facebook/loom/logger/Logger;->a()V

    .line 10155
    iget-object v0, p0, LX/03N;->c:LX/02q;

    if-eqz v0, :cond_2

    .line 10156
    iget-object v0, p0, LX/03N;->c:LX/02q;

    if-eqz v0, :cond_1

    .line 10157
    iget-object v0, p0, LX/03N;->c:LX/02q;

    invoke-interface {v0, p1}, LX/02q;->b(LX/037;)V

    .line 10158
    :cond_1
    invoke-static {}, Lcom/facebook/loom/logger/Logger;->b()V

    .line 10159
    iget-object v0, p0, LX/03N;->c:LX/02q;

    if-eqz v0, :cond_2

    .line 10160
    iget-object v0, p0, LX/03N;->c:LX/02q;

    invoke-interface {v0, p1}, LX/02q;->c(LX/037;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10161
    :cond_2
    monitor-exit p0

    return-void

    .line 10162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/037;I)V
    .locals 4

    .prologue
    .line 10191
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/03N;->d:LX/037;

    .line 10192
    iget-object v0, p0, LX/03N;->c:LX/02q;

    if-eqz v0, :cond_0

    .line 10193
    iget-object v0, p0, LX/03N;->c:LX/02q;

    iget-object v1, p0, LX/03N;->d:LX/037;

    invoke-interface {v0, v1}, LX/02q;->a(LX/037;)V

    .line 10194
    :cond_0
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 10195
    if-eqz v0, :cond_1

    .line 10196
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, LX/037;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p1, LX/037;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 10197
    :cond_1
    iget-object v0, p0, LX/03N;->b:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 10198
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 10199
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 10200
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 10201
    iget-object v1, p0, LX/03N;->b:Landroid/os/Handler;

    int-to-long v2, p2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10202
    :cond_2
    monitor-exit p0

    return-void

    .line 10203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/037;)V
    .locals 2

    .prologue
    .line 10185
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/03N;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 10186
    iget-object v0, p0, LX/03N;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 10187
    :cond_0
    iget-object v0, p0, LX/03N;->c:LX/02q;

    if-eqz v0, :cond_1

    .line 10188
    iget-object v0, p0, LX/03N;->c:LX/02q;

    invoke-interface {v0, p1}, LX/02q;->d(LX/037;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10189
    :cond_1
    monitor-exit p0

    return-void

    .line 10190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(LX/037;)V
    .locals 4

    .prologue
    .line 10174
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/03N;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 10175
    iget-object v0, p0, LX/03N;->d:LX/037;

    if-eqz v0, :cond_0

    iget-wide v0, p1, LX/037;->a:J

    iget-object v2, p0, LX/03N;->d:LX/037;

    iget-wide v2, v2, LX/037;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 10176
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 10177
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 10178
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 10179
    iget-object v1, p0, LX/03N;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 10180
    :cond_0
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 10181
    if-eqz v0, :cond_1

    .line 10182
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, LX/037;->b:Ljava/lang/String;

    aput-object v2, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10183
    :cond_1
    monitor-exit p0

    return-void

    .line 10184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(LX/037;)V
    .locals 4

    .prologue
    .line 10163
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/03N;->d:LX/037;

    if-eqz v0, :cond_1

    iget-wide v0, p1, LX/037;->a:J

    iget-object v2, p0, LX/03N;->d:LX/037;

    iget-wide v2, v2, LX/037;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 10164
    iget-object v0, p0, LX/03N;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 10165
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 10166
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 10167
    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    .line 10168
    iget-object v1, p0, LX/03N;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 10169
    :cond_0
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 10170
    if-eqz v0, :cond_1

    .line 10171
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, LX/037;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-short v2, p1, LX/037;->i:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    aput-object v2, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10172
    :cond_1
    monitor-exit p0

    return-void

    .line 10173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
