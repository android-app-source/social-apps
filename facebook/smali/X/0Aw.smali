.class public final LX/0Aw;
.super LX/0Ah;
.source ""


# instance fields
.field public final e:Landroid/net/Uri;

.field public final f:J

.field private final g:LX/0Au;

.field private final h:LX/0Lv;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLX/0AR;LX/0Aj;Ljava/lang/String;JLjava/lang/String;)V
    .locals 11

    .prologue
    .line 25245
    const/4 v10, 0x0

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p9

    invoke-direct/range {v2 .. v10}, LX/0Ah;-><init>(Ljava/lang/String;JLX/0AR;LX/0Ag;Ljava/lang/String;Ljava/lang/String;B)V

    .line 25246
    move-object/from16 v0, p5

    iget-object v2, v0, LX/0Aj;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, LX/0Aw;->e:Landroid/net/Uri;

    .line 25247
    invoke-virtual/range {p5 .. p5}, LX/0Aj;->b()LX/0Au;

    move-result-object v2

    iput-object v2, p0, LX/0Aw;->g:LX/0Au;

    .line 25248
    move-wide/from16 v0, p7

    iput-wide v0, p0, LX/0Aw;->f:J

    .line 25249
    iget-object v2, p0, LX/0Aw;->g:LX/0Au;

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    iput-object v2, p0, LX/0Aw;->h:LX/0Lv;

    .line 25250
    return-void

    .line 25251
    :cond_0
    new-instance v2, LX/0Lv;

    new-instance v3, LX/0Au;

    move-object/from16 v0, p5

    iget-object v4, v0, LX/0Aj;->d:Ljava/lang/String;

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-wide/from16 v8, p7

    invoke-direct/range {v3 .. v9}, LX/0Au;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-direct {v2, v3}, LX/0Lv;-><init>(LX/0Au;)V

    goto :goto_0
.end method


# virtual methods
.method public final e()LX/0Au;
    .locals 1

    .prologue
    .line 25252
    iget-object v0, p0, LX/0Aw;->g:LX/0Au;

    return-object v0
.end method

.method public final f()LX/0BX;
    .locals 1

    .prologue
    .line 25253
    iget-object v0, p0, LX/0Aw;->h:LX/0Lv;

    return-object v0
.end method
