.class public final LX/077;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/056;

.field public final b:LX/072;

.field public final c:Z


# direct methods
.method public constructor <init>(LX/056;LX/072;Z)V
    .locals 0

    .prologue
    .line 19185
    iput-object p1, p0, LX/077;->a:LX/056;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19186
    iput-object p2, p0, LX/077;->b:LX/072;

    .line 19187
    iput-boolean p3, p0, LX/077;->c:Z

    .line 19188
    return-void
.end method

.method public static d(LX/077;)V
    .locals 4

    .prologue
    .line 19180
    iget-object v0, p0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->m:LX/072;

    iget-object v1, p0, LX/077;->b:LX/072;

    if-ne v0, v1, :cond_0

    .line 19181
    const-string v0, "FbnsConnectionManager"

    const-string v1, "Preemptive connection succeeded, switch to new connection"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19182
    iget-object v0, p0, LX/077;->a:LX/056;

    iget-object v1, p0, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->b:LX/072;

    sget-object v2, LX/0AX;->PREEMPTIVE_RECONNECT_SUCCESS:LX/0AX;

    sget-object v3, LX/0BA;->PREEMPTIVE_RECONNECT_SUCCESS:LX/0BA;

    invoke-virtual {v0, v1, v2, v3}, LX/056;->a(LX/072;LX/0AX;LX/0BA;)Ljava/util/concurrent/Future;

    .line 19183
    iget-object v0, p0, LX/077;->a:LX/056;

    invoke-static {v0}, LX/056;->s(LX/056;)V

    .line 19184
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 19165
    iget-object v0, p0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->k:LX/06J;

    .line 19166
    iget-object v1, v0, LX/06J;->g:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0B1;

    .line 19167
    if-eqz v1, :cond_0

    .line 19168
    invoke-virtual {v1, p2}, LX/0B1;->a(Ljava/lang/Throwable;)V

    .line 19169
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 19178
    iget-object v0, p0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->y:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$5;-><init>(LX/077;Ljava/lang/String;I)V

    const v2, -0x65d9ff66

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 19179
    return-void
.end method

.method public final a(Ljava/lang/String;JZ)V
    .locals 2

    .prologue
    .line 19175
    iget-object v0, p0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->D:LX/055;

    .line 19176
    iget-object v1, v0, LX/055;->a:LX/051;

    invoke-virtual {v1, p1, p2, p3, p4}, LX/051;->a(Ljava/lang/String;JZ)V

    .line 19177
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 19173
    iget-object v0, p0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->y:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$7;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$7;-><init>(LX/077;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const v2, -0xbbe8407

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 19174
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 19170
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/lost"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19171
    iget-object v0, p0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->y:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$4;

    invoke-direct {v1, p0}, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$4;-><init>(LX/077;)V

    const v2, -0x741cbe17

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 19172
    return-void
.end method
