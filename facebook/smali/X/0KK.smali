.class public LX/0KK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0KH;


# instance fields
.field private a:J

.field private b:J

.field private c:LX/0OQ;


# direct methods
.method public constructor <init>(LX/0OQ;)V
    .locals 0

    .prologue
    .line 40673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40674
    iput-object p1, p0, LX/0KK;->c:LX/0OQ;

    .line 40675
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/0KJ;
    .locals 12

    .prologue
    const-wide/16 v6, 0x0

    .line 40663
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0KK;->c:LX/0OQ;

    invoke-interface {v0}, LX/0OQ;->b()J

    move-result-wide v2

    .line 40664
    iget-object v0, p0, LX/0KK;->c:LX/0OQ;

    invoke-interface {v0}, LX/0OQ;->a()Ljava/util/Set;

    move-result-object v0

    .line 40665
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-wide v4, v6

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 40666
    iget-object v1, p0, LX/0KK;->c:LX/0OQ;

    invoke-interface {v1, v0}, LX/0OQ;->a(Ljava/lang/String;)Ljava/util/NavigableSet;

    move-result-object v0

    .line 40667
    invoke-interface {v0}, Ljava/util/NavigableSet;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0OT;

    .line 40668
    iget-wide v10, v0, LX/0OT;->f:J

    cmp-long v1, v10, v6

    if-nez v1, :cond_2

    .line 40669
    iget-wide v0, v0, LX/0OT;->c:J

    add-long/2addr v0, v4

    :goto_1
    move-wide v4, v0

    .line 40670
    goto :goto_0

    .line 40671
    :cond_1
    new-instance v1, LX/0KJ;

    sub-long v4, v2, v4

    iget-wide v6, p0, LX/0KK;->a:J

    iget-wide v8, p0, LX/0KK;->b:J

    add-long/2addr v6, v8

    iget-wide v8, p0, LX/0KK;->a:J

    invoke-direct/range {v1 .. v9}, LX/0KJ;-><init>(JJJJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 40672
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-wide v0, v4

    goto :goto_1
.end method

.method public final declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 40660
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/0KK;->b:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/0KK;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40661
    monitor-exit p0

    return-void

    .line 40662
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 40658
    iget-wide v0, p0, LX/0KK;->a:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/0KK;->a:J

    .line 40659
    return-void
.end method
