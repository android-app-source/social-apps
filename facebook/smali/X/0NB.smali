.class public final LX/0NB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0LS;

.field public final b:Z

.field public final c:Z

.field private final d:LX/0Oi;

.field private final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0Og;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0Of;",
            ">;"
        }
    .end annotation
.end field

.field private g:[B

.field public h:I

.field public i:I

.field public j:J

.field public k:Z

.field public l:J

.field public m:LX/0NA;

.field public n:LX/0NA;

.field private o:Z

.field private p:J

.field private q:J

.field private r:Z


# direct methods
.method public constructor <init>(LX/0LS;ZZ)V
    .locals 2

    .prologue
    .line 49246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49247
    iput-object p1, p0, LX/0NB;->a:LX/0LS;

    .line 49248
    iput-boolean p2, p0, LX/0NB;->b:Z

    .line 49249
    iput-boolean p3, p0, LX/0NB;->c:Z

    .line 49250
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/0NB;->e:Landroid/util/SparseArray;

    .line 49251
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/0NB;->f:Landroid/util/SparseArray;

    .line 49252
    new-instance v0, LX/0NA;

    invoke-direct {v0}, LX/0NA;-><init>()V

    iput-object v0, p0, LX/0NB;->m:LX/0NA;

    .line 49253
    new-instance v0, LX/0NA;

    invoke-direct {v0}, LX/0NA;-><init>()V

    iput-object v0, p0, LX/0NB;->n:LX/0NA;

    .line 49254
    new-instance v0, LX/0Oi;

    invoke-direct {v0}, LX/0Oi;-><init>()V

    iput-object v0, p0, LX/0NB;->d:LX/0Oi;

    .line 49255
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, LX/0NB;->g:[B

    .line 49256
    invoke-virtual {p0}, LX/0NB;->b()V

    .line 49257
    return-void
.end method

.method private static a(LX/0NB;I)V
    .locals 8

    .prologue
    .line 49258
    iget-boolean v0, p0, LX/0NB;->r:Z

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    .line 49259
    :goto_0
    iget-wide v0, p0, LX/0NB;->j:J

    iget-wide v2, p0, LX/0NB;->p:J

    sub-long/2addr v0, v2

    long-to-int v5, v0

    .line 49260
    iget-object v1, p0, LX/0NB;->a:LX/0LS;

    iget-wide v2, p0, LX/0NB;->q:J

    const/4 v7, 0x0

    move v6, p1

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    .line 49261
    return-void

    .line 49262
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JI)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 49263
    iget v2, p0, LX/0NB;->i:I

    const/16 v3, 0x9

    if-eq v2, v3, :cond_5

    iget-boolean v2, p0, LX/0NB;->c:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/0NB;->n:LX/0NA;

    iget-object v3, p0, LX/0NB;->m:LX/0NA;

    const/4 v4, 0x1

    .line 49264
    iget-boolean v5, v2, LX/0NA;->a:Z

    if-eqz v5, :cond_b

    iget-boolean v5, v3, LX/0NA;->a:Z

    if-eqz v5, :cond_4

    iget v5, v2, LX/0NA;->f:I

    iget v6, v3, LX/0NA;->f:I

    if-ne v5, v6, :cond_4

    iget v5, v2, LX/0NA;->g:I

    iget v6, v3, LX/0NA;->g:I

    if-ne v5, v6, :cond_4

    iget-boolean v5, v2, LX/0NA;->h:Z

    iget-boolean v6, v3, LX/0NA;->h:Z

    if-ne v5, v6, :cond_4

    iget-boolean v5, v2, LX/0NA;->i:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v3, LX/0NA;->i:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v2, LX/0NA;->j:Z

    iget-boolean v6, v3, LX/0NA;->j:Z

    if-ne v5, v6, :cond_4

    :cond_0
    iget v5, v2, LX/0NA;->d:I

    iget v6, v3, LX/0NA;->d:I

    if-eq v5, v6, :cond_1

    iget v5, v2, LX/0NA;->d:I

    if-eqz v5, :cond_4

    iget v5, v3, LX/0NA;->d:I

    if-eqz v5, :cond_4

    :cond_1
    iget-object v5, v2, LX/0NA;->c:LX/0Og;

    iget v5, v5, LX/0Og;->h:I

    if-nez v5, :cond_2

    iget-object v5, v3, LX/0NA;->c:LX/0Og;

    iget v5, v5, LX/0Og;->h:I

    if-nez v5, :cond_2

    iget v5, v2, LX/0NA;->m:I

    iget v6, v3, LX/0NA;->m:I

    if-ne v5, v6, :cond_4

    iget v5, v2, LX/0NA;->n:I

    iget v6, v3, LX/0NA;->n:I

    if-ne v5, v6, :cond_4

    :cond_2
    iget-object v5, v2, LX/0NA;->c:LX/0Og;

    iget v5, v5, LX/0Og;->h:I

    if-ne v5, v4, :cond_3

    iget-object v5, v3, LX/0NA;->c:LX/0Og;

    iget v5, v5, LX/0Og;->h:I

    if-ne v5, v4, :cond_3

    iget v5, v2, LX/0NA;->o:I

    iget v6, v3, LX/0NA;->o:I

    if-ne v5, v6, :cond_4

    iget v5, v2, LX/0NA;->p:I

    iget v6, v3, LX/0NA;->p:I

    if-ne v5, v6, :cond_4

    :cond_3
    iget-boolean v5, v2, LX/0NA;->k:Z

    iget-boolean v6, v3, LX/0NA;->k:Z

    if-ne v5, v6, :cond_4

    iget-boolean v5, v2, LX/0NA;->k:Z

    if-eqz v5, :cond_b

    iget-boolean v5, v3, LX/0NA;->k:Z

    if-eqz v5, :cond_b

    iget v5, v2, LX/0NA;->l:I

    iget v6, v3, LX/0NA;->l:I

    if-eq v5, v6, :cond_b

    :cond_4
    :goto_0
    move v2, v4

    .line 49265
    if-eqz v2, :cond_7

    .line 49266
    :cond_5
    iget-boolean v2, p0, LX/0NB;->o:Z

    if-eqz v2, :cond_6

    .line 49267
    iget-wide v2, p0, LX/0NB;->j:J

    sub-long v2, p1, v2

    long-to-int v2, v2

    .line 49268
    add-int/2addr v2, p3

    invoke-static {p0, v2}, LX/0NB;->a(LX/0NB;I)V

    .line 49269
    :cond_6
    iget-wide v2, p0, LX/0NB;->j:J

    iput-wide v2, p0, LX/0NB;->p:J

    .line 49270
    iget-wide v2, p0, LX/0NB;->l:J

    iput-wide v2, p0, LX/0NB;->q:J

    .line 49271
    iput-boolean v0, p0, LX/0NB;->r:Z

    .line 49272
    iput-boolean v1, p0, LX/0NB;->o:Z

    .line 49273
    :cond_7
    iget-boolean v2, p0, LX/0NB;->r:Z

    iget v3, p0, LX/0NB;->i:I

    const/4 v4, 0x5

    if-eq v3, v4, :cond_9

    iget-boolean v3, p0, LX/0NB;->b:Z

    if-eqz v3, :cond_a

    iget v3, p0, LX/0NB;->i:I

    if-ne v3, v1, :cond_a

    iget-object v3, p0, LX/0NB;->n:LX/0NA;

    .line 49274
    iget-boolean v4, v3, LX/0NA;->b:Z

    if-eqz v4, :cond_c

    iget v4, v3, LX/0NA;->e:I

    const/4 v5, 0x7

    if-eq v4, v5, :cond_8

    iget v4, v3, LX/0NA;->e:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_c

    :cond_8
    const/4 v4, 0x1

    :goto_1
    move v3, v4

    .line 49275
    if-eqz v3, :cond_a

    :cond_9
    move v0, v1

    :cond_a
    or-int/2addr v0, v2

    iput-boolean v0, p0, LX/0NB;->r:Z

    .line 49276
    return-void

    :cond_b
    const/4 v4, 0x0

    goto :goto_0

    :cond_c
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final a(LX/0Of;)V
    .locals 2

    .prologue
    .line 49277
    iget-object v0, p0, LX/0NB;->f:Landroid/util/SparseArray;

    iget v1, p1, LX/0Of;->a:I

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 49278
    return-void
.end method

.method public final a(LX/0Og;)V
    .locals 2

    .prologue
    .line 49279
    iget-object v0, p0, LX/0NB;->e:Landroid/util/SparseArray;

    iget v1, p1, LX/0Og;->a:I

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 49280
    return-void
.end method

.method public final a([BII)V
    .locals 19

    .prologue
    .line 49281
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/0NB;->k:Z

    if-nez v2, :cond_1

    .line 49282
    :cond_0
    :goto_0
    return-void

    .line 49283
    :cond_1
    sub-int v2, p3, p2

    .line 49284
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0NB;->g:[B

    array-length v3, v3

    move-object/from16 v0, p0

    iget v4, v0, LX/0NB;->h:I

    add-int/2addr v4, v2

    if-ge v3, v4, :cond_2

    .line 49285
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0NB;->g:[B

    move-object/from16 v0, p0

    iget v4, v0, LX/0NB;->h:I

    add-int/2addr v4, v2

    mul-int/lit8 v4, v4, 0x2

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, LX/0NB;->g:[B

    .line 49286
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0NB;->g:[B

    move-object/from16 v0, p0

    iget v4, v0, LX/0NB;->h:I

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49287
    move-object/from16 v0, p0

    iget v3, v0, LX/0NB;->h:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, LX/0NB;->h:I

    .line 49288
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0NB;->g:[B

    move-object/from16 v0, p0

    iget v4, v0, LX/0NB;->h:I

    invoke-virtual {v2, v3, v4}, LX/0Oi;->a([BI)V

    .line 49289
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v2}, LX/0Oi;->a()I

    move-result v2

    const/16 v3, 0x8

    if-lt v2, v3, :cond_0

    .line 49290
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/0Oi;->b(I)V

    .line 49291
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/0Oi;->c(I)I

    move-result v4

    .line 49292
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, LX/0Oi;->b(I)V

    .line 49293
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v2}, LX/0Oi;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49294
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v2}, LX/0Oi;->d()I

    .line 49295
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v2}, LX/0Oi;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49296
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v2}, LX/0Oi;->d()I

    move-result v5

    .line 49297
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/0NB;->c:Z

    if-nez v2, :cond_3

    .line 49298
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/0NB;->k:Z

    .line 49299
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->n:LX/0NA;

    invoke-virtual {v2, v5}, LX/0NA;->a(I)V

    goto/16 :goto_0

    .line 49300
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v2}, LX/0Oi;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49301
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v2}, LX/0Oi;->d()I

    move-result v7

    .line 49302
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->f:Landroid/util/SparseArray;

    invoke-virtual {v2, v7}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v2

    if-gez v2, :cond_4

    .line 49303
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/0NB;->k:Z

    goto/16 :goto_0

    .line 49304
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->f:Landroid/util/SparseArray;

    invoke-virtual {v2, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Of;

    .line 49305
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0NB;->e:Landroid/util/SparseArray;

    iget v6, v2, LX/0Of;->b:I

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Og;

    .line 49306
    iget-boolean v6, v3, LX/0Og;->e:Z

    if-eqz v6, :cond_5

    .line 49307
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v6}, LX/0Oi;->a()I

    move-result v6

    const/4 v8, 0x2

    if-lt v6, v8, :cond_0

    .line 49308
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0NB;->d:LX/0Oi;

    const/4 v8, 0x2

    invoke-virtual {v6, v8}, LX/0Oi;->b(I)V

    .line 49309
    :cond_5
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v6}, LX/0Oi;->a()I

    move-result v6

    iget v8, v3, LX/0Og;->g:I

    if-lt v6, v8, :cond_0

    .line 49310
    const/4 v8, 0x0

    .line 49311
    const/4 v9, 0x0

    .line 49312
    const/4 v10, 0x0

    .line 49313
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0NB;->d:LX/0Oi;

    iget v11, v3, LX/0Og;->g:I

    invoke-virtual {v6, v11}, LX/0Oi;->c(I)I

    move-result v6

    .line 49314
    iget-boolean v11, v3, LX/0Og;->f:Z

    if-nez v11, :cond_6

    .line 49315
    move-object/from16 v0, p0

    iget-object v8, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v8}, LX/0Oi;->a()I

    move-result v8

    if-lez v8, :cond_0

    .line 49316
    move-object/from16 v0, p0

    iget-object v8, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v8}, LX/0Oi;->b()Z

    move-result v8

    .line 49317
    if-eqz v8, :cond_6

    .line 49318
    move-object/from16 v0, p0

    iget-object v9, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v9}, LX/0Oi;->a()I

    move-result v9

    if-lez v9, :cond_0

    .line 49319
    move-object/from16 v0, p0

    iget-object v9, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v9}, LX/0Oi;->b()Z

    move-result v10

    .line 49320
    const/4 v9, 0x1

    .line 49321
    :cond_6
    move-object/from16 v0, p0

    iget v11, v0, LX/0NB;->i:I

    const/4 v12, 0x5

    if-ne v11, v12, :cond_9

    const/4 v11, 0x1

    .line 49322
    :goto_1
    const/4 v12, 0x0

    .line 49323
    if-eqz v11, :cond_7

    .line 49324
    move-object/from16 v0, p0

    iget-object v12, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v12}, LX/0Oi;->c()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 49325
    move-object/from16 v0, p0

    iget-object v12, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v12}, LX/0Oi;->d()I

    move-result v12

    .line 49326
    :cond_7
    const/4 v13, 0x0

    .line 49327
    const/4 v14, 0x0

    .line 49328
    const/4 v15, 0x0

    .line 49329
    const/16 v16, 0x0

    .line 49330
    iget v0, v3, LX/0Og;->h:I

    move/from16 v17, v0

    if-nez v17, :cond_a

    .line 49331
    move-object/from16 v0, p0

    iget-object v13, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v13}, LX/0Oi;->a()I

    move-result v13

    iget v0, v3, LX/0Og;->i:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v13, v0, :cond_0

    .line 49332
    move-object/from16 v0, p0

    iget-object v13, v0, LX/0NB;->d:LX/0Oi;

    iget v0, v3, LX/0Og;->i:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v13, v0}, LX/0Oi;->c(I)I

    move-result v13

    .line 49333
    iget-boolean v2, v2, LX/0Of;->c:Z

    if-eqz v2, :cond_8

    if-nez v8, :cond_8

    .line 49334
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v2}, LX/0Oi;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49335
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v2}, LX/0Oi;->e()I

    move-result v14

    .line 49336
    :cond_8
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->n:LX/0NA;

    invoke-virtual/range {v2 .. v16}, LX/0NA;->a(LX/0Og;IIIIZZZZIIIII)V

    .line 49337
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/0NB;->k:Z

    goto/16 :goto_0

    .line 49338
    :cond_9
    const/4 v11, 0x0

    goto :goto_1

    .line 49339
    :cond_a
    iget v0, v3, LX/0Og;->h:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_8

    iget-boolean v0, v3, LX/0Og;->j:Z

    move/from16 v17, v0

    if-nez v17, :cond_8

    .line 49340
    move-object/from16 v0, p0

    iget-object v15, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v15}, LX/0Oi;->c()Z

    move-result v15

    if-eqz v15, :cond_0

    .line 49341
    move-object/from16 v0, p0

    iget-object v15, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v15}, LX/0Oi;->e()I

    move-result v15

    .line 49342
    iget-boolean v2, v2, LX/0Of;->c:Z

    if-eqz v2, :cond_8

    if-nez v8, :cond_8

    .line 49343
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v2}, LX/0Oi;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49344
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0NB;->d:LX/0Oi;

    invoke-virtual {v2}, LX/0Oi;->e()I

    move-result v16

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 49345
    iget-boolean v0, p0, LX/0NB;->c:Z

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49346
    iput-boolean v0, p0, LX/0NB;->k:Z

    .line 49347
    iput-boolean v0, p0, LX/0NB;->o:Z

    .line 49348
    iget-object v0, p0, LX/0NB;->n:LX/0NA;

    invoke-virtual {v0}, LX/0NA;->a()V

    .line 49349
    return-void
.end method
