.class public final enum LX/02j;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/02j;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/02j;

.field public static final enum FAILURE:LX/02j;

.field public static final enum NOT_ATTEMPTED:LX/02j;

.field public static final enum SUCCESS:LX/02j;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9156
    new-instance v0, LX/02j;

    const-string v1, "NOT_ATTEMPTED"

    invoke-direct {v0, v1, v2}, LX/02j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/02j;->NOT_ATTEMPTED:LX/02j;

    .line 9157
    new-instance v0, LX/02j;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v3}, LX/02j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/02j;->FAILURE:LX/02j;

    .line 9158
    new-instance v0, LX/02j;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4}, LX/02j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/02j;->SUCCESS:LX/02j;

    .line 9159
    const/4 v0, 0x3

    new-array v0, v0, [LX/02j;

    sget-object v1, LX/02j;->NOT_ATTEMPTED:LX/02j;

    aput-object v1, v0, v2

    sget-object v1, LX/02j;->FAILURE:LX/02j;

    aput-object v1, v0, v3

    sget-object v1, LX/02j;->SUCCESS:LX/02j;

    aput-object v1, v0, v4

    sput-object v0, LX/02j;->$VALUES:[LX/02j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 9161
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/02j;
    .locals 1

    .prologue
    .line 9162
    const-class v0, LX/02j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/02j;

    return-object v0
.end method

.method public static values()[LX/02j;
    .locals 1

    .prologue
    .line 9160
    sget-object v0, LX/02j;->$VALUES:[LX/02j;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/02j;

    return-object v0
.end method
