.class public final LX/08T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final initialStatus:J

.field private mCommitLock:LX/02W;

.field private mOptLock:LX/02W;

.field private mPhase:I

.field public final synthetic this$1:LX/08Z;


# direct methods
.method public constructor <init>(LX/08Z;)V
    .locals 2

    .prologue
    .line 21164
    iput-object p1, p0, LX/08T;->this$1:LX/08Z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21165
    :try_start_0
    iget-object v0, p1, LX/08Z;->this$0:LX/02U;

    iget-object v0, v0, LX/02U;->mLockFile:LX/02V;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/02V;->acquireInterruptubly(I)LX/02W;

    move-result-object v0

    iput-object v0, p0, LX/08T;->mCommitLock:LX/02W;

    .line 21166
    iget-object v0, p1, LX/08Z;->this$0:LX/02U;

    invoke-static {v0}, LX/02U;->readStatusLocked(LX/02U;)J

    move-result-wide v0

    iput-wide v0, p0, LX/08T;->initialStatus:J

    .line 21167
    iget-wide v0, p0, LX/08T;->initialStatus:J

    invoke-direct {p0, v0, v1}, LX/08T;->checkBadStatus(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 21168
    return-void

    :catchall_0
    move-exception v0

    .line 21169
    invoke-virtual {p0}, LX/08T;->close()V

    throw v0
.end method

.method private checkBadStatus(J)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 21159
    const-wide/16 v0, 0xf

    and-long/2addr v0, p1

    long-to-int v0, v0

    int-to-byte v0, v0

    .line 21160
    if-eqz v0, :cond_0

    if-eq v0, v5, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_1

    .line 21161
    :cond_0
    new-instance v0, LX/08c;

    const-string v1, "bad status %x for dex store %s starting tx"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v3, p0, LX/08T;->this$1:LX/08Z;

    iget-object v3, v3, LX/08Z;->this$0:LX/02U;

    iget-object v3, v3, LX/02U;->root:Ljava/io/File;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/08c;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21162
    :cond_1
    iget-object v0, p0, LX/08T;->this$1:LX/08Z;

    invoke-virtual {v0}, LX/08Z;->checkShouldStop()V

    .line 21163
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21128
    iget-object v0, p0, LX/08T;->mCommitLock:LX/02W;

    if-eqz v0, :cond_0

    .line 21129
    iget-object v0, p0, LX/08T;->mCommitLock:LX/02W;

    invoke-virtual {v0}, LX/02W;->close()V

    .line 21130
    iput-object v1, p0, LX/08T;->mCommitLock:LX/02W;

    .line 21131
    :cond_0
    iget-object v0, p0, LX/08T;->mOptLock:LX/02W;

    if-eqz v0, :cond_1

    .line 21132
    iget-object v0, p0, LX/08T;->mOptLock:LX/02W;

    invoke-virtual {v0}, LX/02W;->close()V

    .line 21133
    iput-object v1, p0, LX/08T;->mOptLock:LX/02W;

    .line 21134
    :cond_1
    return-void
.end method

.method public final finishCommit(J)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 21152
    iget v0, p0, LX/08T;->mPhase:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "wrong phase"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 21153
    iget-object v0, p0, LX/08T;->this$1:LX/08Z;

    iget-object v0, v0, LX/08Z;->this$0:LX/02U;

    invoke-virtual {v0, p1, p2}, LX/02U;->writeStatusLocked(J)V

    .line 21154
    iget-object v0, p0, LX/08T;->mCommitLock:LX/02W;

    invoke-virtual {v0}, LX/02W;->close()V

    .line 21155
    const/4 v0, 0x0

    iput-object v0, p0, LX/08T;->mCommitLock:LX/02W;

    .line 21156
    const/4 v0, 0x3

    iput v0, p0, LX/08T;->mPhase:I

    .line 21157
    return-void

    :cond_0
    move v0, v1

    .line 21158
    goto :goto_0
.end method

.method public final startCommitting()J
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 21142
    iget v2, p0, LX/08T;->mPhase:I

    if-ne v2, v0, :cond_0

    :goto_0
    const-string v2, "wrong phase"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 21143
    iget-object v0, p0, LX/08T;->mOptLock:LX/02W;

    invoke-virtual {v0}, LX/02W;->close()V

    .line 21144
    const/4 v0, 0x0

    iput-object v0, p0, LX/08T;->mOptLock:LX/02W;

    .line 21145
    iget-object v0, p0, LX/08T;->this$1:LX/08Z;

    iget-object v0, v0, LX/08Z;->this$0:LX/02U;

    iget-object v0, v0, LX/02U;->mLockFile:LX/02V;

    invoke-virtual {v0, v1}, LX/02V;->acquireInterruptubly(I)LX/02W;

    move-result-object v0

    iput-object v0, p0, LX/08T;->mCommitLock:LX/02W;

    .line 21146
    iget-object v0, p0, LX/08T;->this$1:LX/08Z;

    iget-object v0, v0, LX/08Z;->this$0:LX/02U;

    invoke-static {v0}, LX/02U;->readStatusLocked(LX/02U;)J

    move-result-wide v0

    .line 21147
    invoke-direct {p0, v0, v1}, LX/08T;->checkBadStatus(J)V

    .line 21148
    iget-object v2, p0, LX/08T;->this$1:LX/08Z;

    iget-object v2, v2, LX/08Z;->this$0:LX/02U;

    const-wide/16 v4, 0x1

    const/4 v3, 0x4

    shl-long v6, v0, v3

    or-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/02U;->writeStatusLocked(J)V

    .line 21149
    const/4 v2, 0x2

    iput v2, p0, LX/08T;->mPhase:I

    .line 21150
    return-wide v0

    :cond_0
    move v0, v1

    .line 21151
    goto :goto_0
.end method

.method public final startOptimizing()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 21135
    iget v0, p0, LX/08T;->mPhase:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "wrong phase"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 21136
    iget-object v0, p0, LX/08T;->this$1:LX/08Z;

    iget-object v0, v0, LX/08Z;->mOptLockFile:LX/02V;

    invoke-virtual {v0, v1}, LX/02V;->acquireInterruptubly(I)LX/02W;

    move-result-object v0

    iput-object v0, p0, LX/08T;->mOptLock:LX/02W;

    .line 21137
    iget-object v0, p0, LX/08T;->mCommitLock:LX/02W;

    invoke-virtual {v0}, LX/02W;->close()V

    .line 21138
    const/4 v0, 0x0

    iput-object v0, p0, LX/08T;->mCommitLock:LX/02W;

    .line 21139
    iput v1, p0, LX/08T;->mPhase:I

    .line 21140
    return-void

    :cond_0
    move v0, v2

    .line 21141
    goto :goto_0
.end method
