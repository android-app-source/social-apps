.class public LX/03S;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/03S;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03T;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03T;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 10308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10309
    iput-object p1, p0, LX/03S;->a:LX/0Or;

    .line 10310
    return-void
.end method

.method public static a(LX/0QB;)LX/03S;
    .locals 3

    .prologue
    .line 10295
    sget-object v0, LX/03S;->b:LX/03S;

    if-nez v0, :cond_1

    .line 10296
    const-class v1, LX/03S;

    monitor-enter v1

    .line 10297
    :try_start_0
    sget-object v0, LX/03S;->b:LX/03S;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 10298
    if-eqz v2, :cond_0

    .line 10299
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/03S;->b(LX/0QB;)LX/03S;

    move-result-object v0

    sput-object v0, LX/03S;->b:LX/03S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 10300
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 10301
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 10302
    :cond_1
    sget-object v0, LX/03S;->b:LX/03S;

    return-object v0

    .line 10303
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 10304
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/03S;
    .locals 2

    .prologue
    .line 10306
    new-instance v0, LX/03S;

    const/16 v1, 0x35cc

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/03S;-><init>(LX/0Or;)V

    .line 10307
    return-object v0
.end method


# virtual methods
.method public final a()LX/03T;
    .locals 1

    .prologue
    .line 10305
    iget-object v0, p0, LX/03S;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03T;

    return-object v0
.end method
