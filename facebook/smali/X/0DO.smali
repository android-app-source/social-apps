.class public LX/0DO;
.super Landroid/app/Fragment;
.source ""

# interfaces
.implements LX/0DN;


# instance fields
.field private final a:LX/08Q;

.field private b:I

.field private c:J

.field private d:LX/0ES;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29994
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 29995
    new-instance v0, LX/08Q;

    invoke-direct {v0, p0}, LX/08Q;-><init>(LX/0DN;)V

    iput-object v0, p0, LX/0DO;->a:LX/08Q;

    .line 29996
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 29984
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 29985
    iget-wide v2, p0, LX/0DO;->c:J

    sub-long v2, v0, v2

    .line 29986
    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 29987
    iget v2, p0, LX/0DO;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/0DO;->b:I

    .line 29988
    :goto_0
    iput-wide v0, p0, LX/0DO;->c:J

    .line 29989
    iget v0, p0, LX/0DO;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, LX/0DO;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "dump_debug_info_dialog_fragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 29990
    iget-object v0, p0, LX/0DO;->d:LX/0ES;

    invoke-virtual {p0}, LX/0DO;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dump_debug_info_dialog_fragment"

    invoke-virtual {v0, v1, v2}, LX/0ES;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 29991
    new-instance v0, Lcom/facebook/browser/lite/bugreport/RageShakeListenerFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/browser/lite/bugreport/RageShakeListenerFragment$1;-><init>(LX/0DO;)V

    const v1, -0x47c78505

    invoke-static {v0, v1}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 29992
    :cond_0
    return-void

    .line 29993
    :cond_1
    const/4 v2, 0x1

    iput v2, p0, LX/0DO;->b:I

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 29997
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 29998
    new-instance v0, LX/0ES;

    invoke-direct {v0}, LX/0ES;-><init>()V

    .line 29999
    move-object v0, v0

    .line 30000
    iput-object v0, p0, LX/0DO;->d:LX/0ES;

    .line 30001
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    .line 29975
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 29976
    iget-object v0, p0, LX/0DO;->a:LX/08Q;

    const/4 v3, 0x0

    .line 29977
    iget-object v1, v0, LX/08Q;->d:Landroid/hardware/Sensor;

    if-eqz v1, :cond_0

    .line 29978
    iget-object v1, v0, LX/08Q;->c:Landroid/hardware/SensorManager;

    iget-object v2, v0, LX/08Q;->d:Landroid/hardware/Sensor;

    invoke-virtual {v1, v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 29979
    iput-object v3, v0, LX/08Q;->c:Landroid/hardware/SensorManager;

    .line 29980
    iput-object v3, v0, LX/08Q;->d:Landroid/hardware/Sensor;

    .line 29981
    :cond_0
    invoke-virtual {p0}, LX/0DO;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "dump_debug_info_dialog_fragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 29982
    invoke-virtual {p0}, LX/0DO;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, LX/0DO;->d:LX/0ES;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 29983
    :cond_1
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    .line 29965
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 29966
    invoke-virtual {p0}, LX/0DO;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 29967
    iget-object v1, p0, LX/0DO;->a:LX/08Q;

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 29968
    iget-object p0, v1, LX/08Q;->d:Landroid/hardware/Sensor;

    if-eqz p0, :cond_1

    .line 29969
    :cond_0
    :goto_0
    return-void

    .line 29970
    :cond_1
    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object p0

    iput-object p0, v1, LX/08Q;->d:Landroid/hardware/Sensor;

    .line 29971
    iget-object p0, v1, LX/08Q;->d:Landroid/hardware/Sensor;

    if-eqz p0, :cond_2

    .line 29972
    iput-object v0, v1, LX/08Q;->c:Landroid/hardware/SensorManager;

    .line 29973
    iget-object p0, v1, LX/08Q;->d:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, p0, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 29974
    :cond_2
    iget-object p0, v1, LX/08Q;->d:Landroid/hardware/Sensor;

    if-nez p0, :cond_0

    goto :goto_0
.end method
