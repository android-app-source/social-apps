.class public final enum LX/0IJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0IJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0IJ;

.field public static final enum JSON_PARSE_ERROR:LX/0IJ;

.field public static final enum UNEXPECTED_TOPIC:LX/0IJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38535
    new-instance v0, LX/0IJ;

    const-string v1, "JSON_PARSE_ERROR"

    invoke-direct {v0, v1, v2}, LX/0IJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0IJ;->JSON_PARSE_ERROR:LX/0IJ;

    .line 38536
    new-instance v0, LX/0IJ;

    const-string v1, "UNEXPECTED_TOPIC"

    invoke-direct {v0, v1, v3}, LX/0IJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0IJ;->UNEXPECTED_TOPIC:LX/0IJ;

    .line 38537
    const/4 v0, 0x2

    new-array v0, v0, [LX/0IJ;

    sget-object v1, LX/0IJ;->JSON_PARSE_ERROR:LX/0IJ;

    aput-object v1, v0, v2

    sget-object v1, LX/0IJ;->UNEXPECTED_TOPIC:LX/0IJ;

    aput-object v1, v0, v3

    sput-object v0, LX/0IJ;->$VALUES:[LX/0IJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38532
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0IJ;
    .locals 1

    .prologue
    .line 38533
    const-class v0, LX/0IJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0IJ;

    return-object v0
.end method

.method public static values()[LX/0IJ;
    .locals 1

    .prologue
    .line 38534
    sget-object v0, LX/0IJ;->$VALUES:[LX/0IJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0IJ;

    return-object v0
.end method
