.class public LX/018;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 3819
    const/4 v0, 0x1

    .line 3820
    sget-boolean v1, LX/019;->e:Z

    if-nez v1, :cond_0

    .line 3821
    :goto_0
    const/4 v1, 0x0

    invoke-static {v1}, LX/00k;->c(Z)V

    .line 3822
    return-void

    .line 3823
    :cond_0
    sget-object v1, LX/019;->d:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, LX/019;->a(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3809
    return-void
.end method

.method public static a(J)V
    .locals 2

    .prologue
    .line 3810
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3811
    :goto_0
    return-void

    .line 3812
    :cond_0
    invoke-static {}, Lcom/facebook/systrace/TraceDirect;->a()V

    goto :goto_0
.end method

.method public static a(JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 3813
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3814
    :goto_0
    return-void

    .line 3815
    :cond_0
    invoke-static {p2}, Lcom/facebook/systrace/TraceDirect;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(JLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 3816
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3817
    :goto_0
    return-void

    .line 3818
    :cond_0
    invoke-static {p2, p3}, Lcom/facebook/systrace/TraceDirect;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static a(JLjava/lang/String;IJ)V
    .locals 2

    .prologue
    .line 3824
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3825
    :goto_0
    return-void

    .line 3826
    :cond_0
    invoke-static {p4, p5}, LX/0Ix;->a(J)J

    move-result-wide v0

    .line 3827
    invoke-static {p2, p3, v0, v1}, Lcom/facebook/systrace/TraceDirect;->a(Ljava/lang/String;IJ)V

    goto :goto_0
.end method

.method public static a(JLjava/lang/String;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 3802
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3803
    :goto_0
    return-void

    .line 3804
    :cond_0
    invoke-static {p2, p3, p4}, Lcom/facebook/systrace/TraceDirect;->a(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public static a(JLjava/lang/String;LX/03W;)V
    .locals 2

    .prologue
    .line 3805
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3806
    :goto_0
    return-void

    .line 3807
    :cond_0
    const-string v0, ""

    invoke-virtual {p3}, LX/03W;->getCode()C

    move-result v1

    invoke-static {v0, p2, v1}, Lcom/facebook/systrace/TraceDirect;->a(Ljava/lang/String;Ljava/lang/String;C)V

    goto :goto_0
.end method

.method public static a(JLjava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 3777
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3778
    :goto_0
    return-void

    .line 3779
    :cond_0
    invoke-static {p2, p3, p4}, Lcom/facebook/systrace/TraceDirect;->b(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static b(JLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 3780
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3781
    :goto_0
    return-void

    .line 3782
    :cond_0
    const-wide/16 v0, 0x0

    invoke-static {p2, p3, v0, v1}, Lcom/facebook/systrace/TraceDirect;->a(Ljava/lang/String;IJ)V

    goto :goto_0
.end method

.method public static b(JLjava/lang/String;IJ)V
    .locals 2

    .prologue
    .line 3783
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3784
    :goto_0
    return-void

    .line 3785
    :cond_0
    invoke-static {p4, p5}, LX/0Ix;->a(J)J

    move-result-wide v0

    .line 3786
    invoke-static {p2, p3, v0, v1}, Lcom/facebook/systrace/TraceDirect;->b(Ljava/lang/String;IJ)V

    goto :goto_0
.end method

.method public static b(JLjava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 3799
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3800
    :goto_0
    return-void

    .line 3801
    :cond_0
    invoke-static {p2, p3, p4}, Lcom/facebook/systrace/TraceDirect;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static c(JLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 3787
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3788
    :goto_0
    return-void

    .line 3789
    :cond_0
    const-wide/16 v0, 0x0

    invoke-static {p2, p3, v0, v1}, Lcom/facebook/systrace/TraceDirect;->b(Ljava/lang/String;IJ)V

    goto :goto_0
.end method

.method public static d(JLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 3790
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3791
    :goto_0
    return-void

    .line 3792
    :cond_0
    invoke-static {p2, p3}, Lcom/facebook/systrace/TraceDirect;->c(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static e(JLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 3793
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3794
    :goto_0
    return-void

    .line 3795
    :cond_0
    invoke-static {p2, p3}, Lcom/facebook/systrace/TraceDirect;->d(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static f(JLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 3796
    invoke-static {p0, p1}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3797
    :goto_0
    return-void

    .line 3798
    :cond_0
    invoke-static {p2, p3}, Lcom/facebook/systrace/TraceDirect;->b(Ljava/lang/String;I)V

    goto :goto_0
.end method
