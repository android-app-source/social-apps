.class public final LX/0KO;
.super Landroid/util/LruCache;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
        "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/vps/VideoPlayerService;


# direct methods
.method public constructor <init>(Lcom/facebook/video/vps/VideoPlayerService;I)V
    .locals 0

    .prologue
    .line 40833
    iput-object p1, p0, LX/0KO;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    return-void
.end method

.method private a(ZLcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 1

    .prologue
    .line 40834
    if-eqz p1, :cond_0

    .line 40835
    iget-object v0, p0, LX/0KO;->a:Lcom/facebook/video/vps/VideoPlayerService;

    .line 40836
    invoke-static {v0, p2, p3}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40837
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 40838
    check-cast p2, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    check-cast p3, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-direct {p0, p1, p2, p3}, LX/0KO;->a(ZLcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    return-void
.end method
