.class public LX/09k;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/09k;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Uh;

.field private final d:LX/0ad;

.field private final e:LX/01T;

.field public final f:Z

.field public final g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Uh;LX/0ad;LX/01T;)V
    .locals 3
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "LX/01T;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 22879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22880
    iput-object p1, p0, LX/09k;->a:Landroid/content/Context;

    .line 22881
    iput-object p2, p0, LX/09k;->b:LX/0Or;

    .line 22882
    iput-object p3, p0, LX/09k;->c:LX/0Uh;

    .line 22883
    iput-object p4, p0, LX/09k;->d:LX/0ad;

    .line 22884
    iput-object p5, p0, LX/09k;->e:LX/01T;

    .line 22885
    iget-object v0, p0, LX/09k;->c:LX/0Uh;

    const/16 v1, 0x20e

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/09k;->f:Z

    .line 22886
    iget-object v0, p0, LX/09k;->c:LX/0Uh;

    const/16 v1, 0x210

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/09k;->g:Z

    .line 22887
    return-void
.end method

.method public static a(LX/0QB;)LX/09k;
    .locals 3

    .prologue
    .line 22869
    sget-object v0, LX/09k;->h:LX/09k;

    if-nez v0, :cond_1

    .line 22870
    const-class v1, LX/09k;

    monitor-enter v1

    .line 22871
    :try_start_0
    sget-object v0, LX/09k;->h:LX/09k;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 22872
    if-eqz v2, :cond_0

    .line 22873
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/09k;->b(LX/0QB;)LX/09k;

    move-result-object v0

    sput-object v0, LX/09k;->h:LX/09k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 22874
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 22875
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 22876
    :cond_1
    sget-object v0, LX/09k;->h:LX/09k;

    return-object v0

    .line 22877
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 22878
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/09k;
    .locals 6

    .prologue
    .line 22867
    new-instance v0, LX/09k;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0x15e7

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v5

    check-cast v5, LX/01T;

    invoke-direct/range {v0 .. v5}, LX/09k;-><init>(Landroid/content/Context;LX/0Or;LX/0Uh;LX/0ad;LX/01T;)V

    .line 22868
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 22864
    sget-object v0, LX/01T;->MESSENGER:LX/01T;

    iget-object v1, p0, LX/09k;->e:LX/01T;

    if-eq v0, v1, :cond_0

    sget-object v0, LX/01T;->FB4A:LX/01T;

    iget-object v1, p0, LX/09k;->e:LX/01T;

    if-eq v0, v1, :cond_0

    sget-object v0, LX/01T;->PAA:LX/01T;

    iget-object v1, p0, LX/09k;->e:LX/01T;

    if-eq v0, v1, :cond_0

    .line 22865
    const/4 v0, 0x0

    .line 22866
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 22859
    invoke-virtual {p0}, LX/09k;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 22860
    :cond_0
    :goto_0
    return v0

    .line 22861
    :cond_1
    iget-object v1, p0, LX/09k;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 22862
    iget-boolean v0, p0, LX/09k;->f:Z

    move v0, v0

    .line 22863
    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 22856
    sget-object v0, LX/01T;->MESSENGER:LX/01T;

    iget-object v1, p0, LX/09k;->e:LX/01T;

    if-eq v0, v1, :cond_0

    sget-object v0, LX/01T;->FB4A:LX/01T;

    iget-object v1, p0, LX/09k;->e:LX/01T;

    if-eq v0, v1, :cond_0

    .line 22857
    const/4 v0, 0x0

    .line 22858
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 22849
    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    iget-object v2, p0, LX/09k;->e:LX/01T;

    if-eq v1, v2, :cond_1

    sget-object v1, LX/01T;->FB4A:LX/01T;

    iget-object v2, p0, LX/09k;->e:LX/01T;

    if-eq v1, v2, :cond_1

    .line 22850
    :cond_0
    :goto_0
    return v0

    .line 22851
    :cond_1
    iget-object v1, p0, LX/09k;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 22852
    iget-object v1, p0, LX/09k;->a:Landroid/content/Context;

    invoke-static {v1}, LX/04u;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 22853
    invoke-virtual {p0}, LX/09k;->i()Z

    move-result v0

    goto :goto_0

    .line 22854
    :cond_2
    iget-object v1, p0, LX/09k;->d:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget v4, LX/08j;->c:I

    const/4 v5, -0x1

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 22855
    iget-object v0, p0, LX/09k;->a:Landroid/content/Context;

    invoke-static {v0}, LX/04u;->b(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 22845
    invoke-virtual {p0}, LX/09k;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/09k;->a:Landroid/content/Context;

    invoke-static {v0}, LX/04u;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 22848
    sget-object v2, LX/01T;->FB4A:LX/01T;

    iget-object v3, p0, LX/09k;->e:LX/01T;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, LX/09k;->c:LX/0Uh;

    const/16 v3, 0x7c

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    sget-object v2, LX/01T;->MESSENGER:LX/01T;

    iget-object v3, p0, LX/09k;->e:LX/01T;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, LX/09k;->c:LX/0Uh;

    const/16 v3, 0x7c

    invoke-virtual {v2, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0
.end method

.method public final j()I
    .locals 3

    .prologue
    .line 22847
    iget-object v0, p0, LX/09k;->d:LX/0ad;

    sget v1, LX/08j;->b:I

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 3

    .prologue
    .line 22846
    iget-object v0, p0, LX/09k;->d:LX/0ad;

    sget v1, LX/08j;->a:I

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method
