.class public LX/0Ak;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Ah;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Lu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IILjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LX/0Ah;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24948
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/0Ak;-><init>(IILjava/util/List;Ljava/util/List;)V

    .line 24949
    return-void
.end method

.method public constructor <init>(IILjava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LX/0Ah;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/0Lu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24940
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24941
    iput p1, p0, LX/0Ak;->a:I

    .line 24942
    iput p2, p0, LX/0Ak;->b:I

    .line 24943
    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0Ak;->c:Ljava/util/List;

    .line 24944
    if-nez p4, :cond_0

    .line 24945
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0Ak;->d:Ljava/util/List;

    .line 24946
    :goto_0
    return-void

    .line 24947
    :cond_0
    invoke-static {p4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0Ak;->d:Ljava/util/List;

    goto :goto_0
.end method
