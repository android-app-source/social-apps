.class public LX/0Gp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0G6;


# instance fields
.field public final a:[B

.field public final b:I

.field private c:LX/0Gg;

.field private d:I


# direct methods
.method public constructor <init>([BI)V
    .locals 1

    .prologue
    .line 36481
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/0Gp;-><init>([BILX/0Gg;)V

    .line 36482
    return-void
.end method

.method public constructor <init>([BILX/0Gg;)V
    .locals 0

    .prologue
    .line 36483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36484
    iput-object p1, p0, LX/0Gp;->a:[B

    .line 36485
    iput p2, p0, LX/0Gp;->b:I

    .line 36486
    iput-object p3, p0, LX/0Gp;->c:LX/0Gg;

    .line 36487
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 3

    .prologue
    .line 36488
    iget v0, p0, LX/0Gp;->b:I

    iget v1, p0, LX/0Gp;->d:I

    sub-int/2addr v0, v1

    .line 36489
    if-nez v0, :cond_1

    .line 36490
    const/4 v0, -0x1

    .line 36491
    :cond_0
    :goto_0
    return v0

    .line 36492
    :cond_1
    if-le p3, v0, :cond_2

    .line 36493
    :goto_1
    if-lez v0, :cond_0

    .line 36494
    iget-object v1, p0, LX/0Gp;->a:[B

    iget v2, p0, LX/0Gp;->d:I

    invoke-static {v1, v2, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 36495
    iget v1, p0, LX/0Gp;->d:I

    add-int/2addr v1, v0

    iput v1, p0, LX/0Gp;->d:I

    .line 36496
    iget-object v1, p0, LX/0Gp;->c:LX/0Gg;

    if-eqz v1, :cond_0

    .line 36497
    iget-object v1, p0, LX/0Gp;->c:LX/0Gg;

    invoke-virtual {v1, v0}, LX/0Gg;->a(I)V

    goto :goto_0

    :cond_2
    move v0, p3

    goto :goto_1
.end method

.method public final a(LX/0OA;)J
    .locals 3

    .prologue
    .line 36498
    iget-object v0, p0, LX/0Gp;->c:LX/0Gg;

    if-eqz v0, :cond_0

    .line 36499
    iget-object v0, p0, LX/0Gp;->c:LX/0Gg;

    iget-object v1, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0AC;->CACHED:LX/0AC;

    invoke-virtual {v0, v1, v2}, LX/0Gg;->a(Ljava/lang/String;LX/0AC;)V

    .line 36500
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, LX/0Gp;->d:I

    .line 36501
    iget-object v0, p0, LX/0Gp;->c:LX/0Gg;

    if-eqz v0, :cond_1

    .line 36502
    iget-object v0, p0, LX/0Gp;->c:LX/0Gg;

    invoke-virtual {v0}, LX/0Gg;->b()V

    .line 36503
    :cond_1
    iget v0, p0, LX/0Gp;->b:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 36504
    iget-object v0, p0, LX/0Gp;->c:LX/0Gg;

    if-eqz v0, :cond_0

    .line 36505
    iget-object v0, p0, LX/0Gp;->c:LX/0Gg;

    invoke-virtual {v0}, LX/0Gg;->c()V

    .line 36506
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Gp;->c:LX/0Gg;

    .line 36507
    :cond_0
    return-void
.end method
