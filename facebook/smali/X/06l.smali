.class public final enum LX/06l;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/06l;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/06l;

.field public static final enum BACK_OFF:LX/06l;

.field public static final enum BACK_TO_BACK:LX/06l;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18135
    new-instance v0, LX/06l;

    const-string v1, "BACK_TO_BACK"

    invoke-direct {v0, v1, v2}, LX/06l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06l;->BACK_TO_BACK:LX/06l;

    .line 18136
    new-instance v0, LX/06l;

    const-string v1, "BACK_OFF"

    invoke-direct {v0, v1, v3}, LX/06l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06l;->BACK_OFF:LX/06l;

    .line 18137
    const/4 v0, 0x2

    new-array v0, v0, [LX/06l;

    sget-object v1, LX/06l;->BACK_TO_BACK:LX/06l;

    aput-object v1, v0, v2

    sget-object v1, LX/06l;->BACK_OFF:LX/06l;

    aput-object v1, v0, v3

    sput-object v0, LX/06l;->$VALUES:[LX/06l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/06l;
    .locals 1

    .prologue
    .line 18139
    const-class v0, LX/06l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/06l;

    return-object v0
.end method

.method public static values()[LX/06l;
    .locals 1

    .prologue
    .line 18140
    sget-object v0, LX/06l;->$VALUES:[LX/06l;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/06l;

    return-object v0
.end method
