.class public final LX/09h;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/03N;


# direct methods
.method public constructor <init>(LX/03N;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 22770
    iput-object p1, p0, LX/09h;->a:LX/03N;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    .line 22771
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/037;

    .line 22772
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 22773
    :goto_0
    :pswitch_0
    return-void

    .line 22774
    :pswitch_1
    iget-wide v0, v0, LX/037;->a:J

    .line 22775
    sget-boolean v2, LX/007;->i:Z

    move v2, v2

    .line 22776
    if-eqz v2, :cond_0

    .line 22777
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 22778
    :cond_0
    sget-object v2, LX/02r;->b:LX/02r;

    move-object v2, v2

    .line 22779
    iget-object v6, v2, LX/02r;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/037;

    .line 22780
    if-eqz v6, :cond_1

    iget-wide v6, v6, LX/037;->a:J

    cmp-long v6, v6, v0

    if-eqz v6, :cond_2

    .line 22781
    :cond_1
    :goto_1
    goto :goto_0

    .line 22782
    :pswitch_2
    iget-object v1, p0, LX/09h;->a:LX/03N;

    invoke-virtual {v1, v0}, LX/03N;->a(LX/037;)V

    goto :goto_0

    .line 22783
    :pswitch_3
    iget-object v1, p0, LX/09h;->a:LX/03N;

    invoke-virtual {v1, v0}, LX/03N;->b(LX/037;)V

    goto :goto_0

    .line 22784
    :cond_2
    invoke-static {}, Lcom/facebook/loom/logger/Logger;->d()V

    .line 22785
    const/16 v6, 0x71

    invoke-virtual {v2, v0, v1, v6}, LX/02r;->a(JS)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
