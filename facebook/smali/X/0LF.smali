.class public final LX/0LF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:LX/0LE;

.field public final b:Z

.field private final c:J

.field private final d:J

.field private e:J

.field private f:J

.field private g:J

.field public h:Z

.field private i:J

.field private j:J

.field private k:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 43189
    const/high16 v0, -0x40800000    # -1.0f

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/0LF;-><init>(FZ)V

    .line 43190
    return-void
.end method

.method private constructor <init>(FZ)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 43178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43179
    iput-boolean p2, p0, LX/0LF;->b:Z

    .line 43180
    if-eqz p2, :cond_0

    .line 43181
    sget-object v0, LX/0LE;->b:LX/0LE;

    move-object v0, v0

    .line 43182
    iput-object v0, p0, LX/0LF;->a:LX/0LE;

    .line 43183
    const-wide v0, 0x41cdcd6500000000L    # 1.0E9

    float-to-double v2, p1

    div-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, LX/0LF;->c:J

    .line 43184
    iget-wide v0, p0, LX/0LF;->c:J

    const-wide/16 v2, 0x50

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x64

    div-long/2addr v0, v2

    iput-wide v0, p0, LX/0LF;->d:J

    .line 43185
    :goto_0
    return-void

    .line 43186
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0LF;->a:LX/0LE;

    .line 43187
    iput-wide v2, p0, LX/0LF;->c:J

    .line 43188
    iput-wide v2, p0, LX/0LF;->d:J

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 43174
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 43175
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRefreshRate()F

    move-result v0

    move v0, v0

    .line 43176
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/0LF;-><init>(FZ)V

    .line 43177
    return-void
.end method

.method private static a(JJJ)J
    .locals 10

    .prologue
    .line 43137
    sub-long v0, p0, p2

    div-long/2addr v0, p4

    .line 43138
    mul-long/2addr v0, p4

    add-long/2addr v0, p2

    .line 43139
    cmp-long v2, p0, v0

    if-gtz v2, :cond_0

    .line 43140
    sub-long v2, v0, p4

    .line 43141
    :goto_0
    sub-long v4, v0, p0

    .line 43142
    sub-long v6, p0, v2

    .line 43143
    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    :goto_1
    return-wide v0

    .line 43144
    :cond_0
    add-long v2, v0, p4

    move-wide v8, v2

    move-wide v2, v0

    move-wide v0, v8

    goto :goto_0

    :cond_1
    move-wide v0, v2

    .line 43145
    goto :goto_1
.end method

.method private static b(LX/0LF;JJ)Z
    .locals 5

    .prologue
    .line 43171
    iget-wide v0, p0, LX/0LF;->j:J

    sub-long v0, p1, v0

    .line 43172
    iget-wide v2, p0, LX/0LF;->i:J

    sub-long v2, p3, v2

    .line 43173
    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/32 v2, 0x1312d00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JJ)J
    .locals 11

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 43146
    const-wide/16 v0, 0x3e8

    mul-long v4, p1, v0

    .line 43147
    iget-boolean v0, p0, LX/0LF;->h:Z

    if-eqz v0, :cond_5

    .line 43148
    iget-wide v0, p0, LX/0LF;->e:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 43149
    iget-wide v0, p0, LX/0LF;->k:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0LF;->k:J

    .line 43150
    iget-wide v0, p0, LX/0LF;->g:J

    iput-wide v0, p0, LX/0LF;->f:J

    .line 43151
    :cond_0
    iget-wide v0, p0, LX/0LF;->k:J

    const-wide/16 v2, 0x6

    cmp-long v0, v0, v2

    if-ltz v0, :cond_4

    .line 43152
    iget-wide v0, p0, LX/0LF;->j:J

    sub-long v0, v4, v0

    iget-wide v2, p0, LX/0LF;->k:J

    div-long/2addr v0, v2

    .line 43153
    iget-wide v2, p0, LX/0LF;->f:J

    add-long/2addr v2, v0

    .line 43154
    invoke-static {p0, v2, v3, p3, p4}, LX/0LF;->b(LX/0LF;JJ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 43155
    iput-boolean v6, p0, LX/0LF;->h:Z

    move-wide v0, p3

    move-wide v2, v4

    .line 43156
    :goto_0
    iget-boolean v6, p0, LX/0LF;->h:Z

    if-nez v6, :cond_1

    .line 43157
    iput-wide v4, p0, LX/0LF;->j:J

    .line 43158
    iput-wide p3, p0, LX/0LF;->i:J

    .line 43159
    iput-wide v8, p0, LX/0LF;->k:J

    .line 43160
    const/4 v4, 0x1

    iput-boolean v4, p0, LX/0LF;->h:Z

    .line 43161
    :cond_1
    iput-wide p1, p0, LX/0LF;->e:J

    .line 43162
    iput-wide v2, p0, LX/0LF;->g:J

    .line 43163
    iget-object v2, p0, LX/0LF;->a:LX/0LE;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/0LF;->a:LX/0LE;

    iget-wide v2, v2, LX/0LE;->a:J

    cmp-long v2, v2, v8

    if-nez v2, :cond_6

    .line 43164
    :cond_2
    :goto_1
    return-wide v0

    .line 43165
    :cond_3
    iget-wide v0, p0, LX/0LF;->i:J

    add-long/2addr v0, v2

    iget-wide v6, p0, LX/0LF;->j:J

    sub-long/2addr v0, v6

    .line 43166
    goto :goto_0

    .line 43167
    :cond_4
    invoke-static {p0, v4, v5, p3, p4}, LX/0LF;->b(LX/0LF;JJ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 43168
    iput-boolean v6, p0, LX/0LF;->h:Z

    :cond_5
    move-wide v0, p3

    move-wide v2, v4

    goto :goto_0

    .line 43169
    :cond_6
    iget-object v2, p0, LX/0LF;->a:LX/0LE;

    iget-wide v2, v2, LX/0LE;->a:J

    iget-wide v4, p0, LX/0LF;->c:J

    invoke-static/range {v0 .. v5}, LX/0LF;->a(JJJ)J

    move-result-wide v0

    .line 43170
    iget-wide v2, p0, LX/0LF;->d:J

    sub-long/2addr v0, v2

    goto :goto_1
.end method
