.class public LX/03Q;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    .locals 7

    .prologue
    const/16 v1, 0x3d

    const/4 v0, 0x1

    .line 10250
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    .line 10251
    const/16 v3, 0x3c

    int-to-long v4, v2

    invoke-static {v0, v3, p1, v4, v5}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    move-result v3

    .line 10252
    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 10253
    int-to-long v4, v2

    move v2, p1

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    return-object v6

    :catchall_0
    move-exception v6

    int-to-long v4, v2

    move v2, p1

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    throw v6
.end method

.method public static a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;
    .locals 7

    .prologue
    const/16 v1, 0x3d

    const/4 v0, 0x1

    .line 10254
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    .line 10255
    const/16 v3, 0x3c

    int-to-long v4, v2

    invoke-static {v0, v3, p4, v4, v5}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    move-result v3

    .line 10256
    :try_start_0
    invoke-interface {p0, p1, p2, p3}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 10257
    int-to-long v4, v2

    move v2, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    return-object v6

    :catchall_0
    move-exception v6

    int-to-long v4, v2

    move v2, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    throw v6
.end method

.method public static a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 10258
    const/4 v0, 0x1

    const/16 v1, 0x3e

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v1, p2, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 10259
    invoke-virtual {p0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
