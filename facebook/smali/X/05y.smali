.class public LX/05y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:LX/05z;

.field private final c:LX/05w;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/05z;LX/05w;)V
    .locals 0

    .prologue
    .line 17125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17126
    iput-object p1, p0, LX/05y;->a:Ljava/util/concurrent/ExecutorService;

    .line 17127
    iput-object p2, p0, LX/05y;->b:LX/05z;

    .line 17128
    iput-object p3, p0, LX/05y;->c:LX/05w;

    .line 17129
    return-void
.end method


# virtual methods
.method public final a()LX/07G;
    .locals 15

    .prologue
    .line 17130
    iget-object v0, p0, LX/05y;->b:LX/05z;

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 17131
    iget v4, v0, LX/05z;->c:I

    const/16 v5, 0x8

    if-gt v4, v5, :cond_1

    move-object v4, v12

    .line 17132
    :goto_0
    move-object v1, v4

    .line 17133
    if-eqz v1, :cond_0

    .line 17134
    new-instance v0, LX/08t;

    iget-object v2, p0, LX/05y;->a:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, v2, v1}, LX/08t;-><init>(Ljava/util/concurrent/ExecutorService;LX/08s;)V

    .line 17135
    :goto_1
    return-object v0

    :cond_0
    new-instance v1, LX/07F;

    iget-object v2, p0, LX/05y;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {}, Ljavax/net/ssl/SSLSocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocketFactory;

    iget-object v3, p0, LX/05y;->c:LX/05w;

    invoke-direct {v1, v2, v0, v3}, LX/07F;-><init>(Ljava/util/concurrent/ExecutorService;Ljavax/net/ssl/SSLSocketFactory;LX/05w;)V

    move-object v0, v1

    goto :goto_1

    .line 17136
    :cond_1
    :try_start_0
    iget v4, v0, LX/05z;->c:I

    const/16 v5, 0x10

    if-gt v4, v5, :cond_2

    .line 17137
    iget-object v4, v0, LX/05z;->d:Ljava/util/Set;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 17138
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/066;

    .line 17139
    sget-object v9, LX/05z;->a:Ljava/lang/String;

    const-string v10, "trying check %s"

    new-array v11, v7, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v11, v6

    invoke-static {v9, v10, v11}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17140
    invoke-interface {v5}, LX/066;->a()Z

    move-result v9

    if-nez v9, :cond_3

    .line 17141
    sget-object v8, LX/05z;->a:Ljava/lang/String;

    const-string v9, "check fail %s"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v6

    invoke-static {v8, v9, v7}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v5, v6

    .line 17142
    :goto_3
    move v4, v5

    .line 17143
    if-eqz v4, :cond_2

    .line 17144
    sget-object v4, LX/05z;->a:Ljava/lang/String;

    const-string v5, "all checks passed, using TicketEnabledOpenSSLSocketFactory"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17145
    new-instance v4, LX/08s;

    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v5

    iget-object v6, v0, LX/05z;->i:LX/05w;

    iget-object v7, v0, LX/05z;->e:LX/060;

    iget-object v8, v0, LX/05z;->f:LX/062;

    iget-object v9, v0, LX/05z;->g:LX/063;

    iget-object v10, v0, LX/05z;->h:LX/064;

    iget v11, v0, LX/05z;->b:I

    invoke-direct/range {v4 .. v11}, LX/08s;-><init>(Ljavax/net/ssl/SSLSocketFactory;LX/05w;LX/060;LX/062;LX/063;LX/064;I)V
    :try_end_0
    .catch LX/061; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 17146
    :catch_0
    move-exception v4

    .line 17147
    sget-object v5, LX/05z;->a:Ljava/lang/String;

    const-string v6, "exception occurred while trying to create the socket factory"

    new-array v7, v13, [Ljava/lang/Object;

    invoke-static {v5, v4, v6, v7}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    move-object v4, v12

    .line 17148
    goto/16 :goto_0

    .line 17149
    :cond_3
    sget-object v5, LX/05z;->a:Ljava/lang/String;

    const-string v9, "check pass"

    new-array v10, v6, [Ljava/lang/Object;

    invoke-static {v5, v9, v10}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_4
    move v5, v7

    .line 17150
    goto :goto_3
.end method
