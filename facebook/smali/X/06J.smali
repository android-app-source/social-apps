.class public LX/06J;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/05m;

.field private final b:LX/05q;

.field private final c:LX/05h;

.field private final d:LX/05i;

.field private final e:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field private final f:LX/059;

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/0B1;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "itself"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/05m;LX/05q;LX/05h;LX/05i;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;LX/059;)V
    .locals 1

    .prologue
    .line 17664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17665
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/06J;->g:Ljava/util/Map;

    .line 17666
    iput-object p1, p0, LX/06J;->a:LX/05m;

    .line 17667
    iput-object p2, p0, LX/06J;->b:LX/05q;

    .line 17668
    iput-object p3, p0, LX/06J;->c:LX/05h;

    .line 17669
    iput-object p4, p0, LX/06J;->d:LX/05i;

    .line 17670
    iput-object p5, p0, LX/06J;->e:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 17671
    iput-object p6, p0, LX/06J;->f:LX/059;

    .line 17672
    return-void
.end method

.method private static a(Ljava/lang/String;LX/0B1;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 17660
    if-nez p1, :cond_0

    .line 17661
    const-string v0, "MqttOperationManager"

    const-string v1, "operation/%s; operation=null"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17662
    :goto_0
    return-void

    .line 17663
    :cond_0
    const-string v0, "MqttOperationManager"

    const-string v1, "operation/%s; operation=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    aput-object p1, v2, v4

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/06J;LX/0B1;I)V
    .locals 18

    .prologue
    .line 17638
    move-object/from16 v0, p1

    iget-object v2, v0, LX/0B1;->b:LX/07S;

    .line 17639
    move-object/from16 v0, p1

    iget v5, v0, LX/0B1;->c:I

    .line 17640
    const-string v3, "MqttOperationManager"

    const-string v4, "operation/timeout; name=%s, id=%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v2}, LX/07S;->name()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v4, v6}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17641
    move-object/from16 v0, p1

    iget-object v13, v0, LX/0B1;->a:LX/072;

    .line 17642
    invoke-virtual {v13}, LX/072;->h()Landroid/net/NetworkInfo;

    move-result-object v12

    .line 17643
    invoke-virtual {v13}, LX/072;->i()J

    move-result-wide v8

    .line 17644
    move-object/from16 v0, p0

    iget-object v3, v0, LX/06J;->f:LX/059;

    invoke-virtual {v3}, LX/059;->h()J

    move-result-wide v10

    .line 17645
    move-object/from16 v0, p0

    iget-object v14, v0, LX/06J;->g:Ljava/util/Map;

    monitor-enter v14

    .line 17646
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, LX/06J;->g:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, p1

    if-ne v3, v0, :cond_2

    .line 17647
    move-object/from16 v0, p0

    iget-object v3, v0, LX/06J;->g:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17648
    move-object/from16 v0, p0

    iget-object v3, v0, LX/06J;->c:LX/05h;

    invoke-virtual {v2}, LX/07S;->name()Ljava/lang/String;

    move-result-object v4

    move/from16 v0, p2

    int-to-long v6, v0

    const-wide/16 v16, 0x3e8

    mul-long v6, v6, v16

    invoke-virtual/range {v3 .. v12}, LX/05h;->a(Ljava/lang/String;IJJJLandroid/net/NetworkInfo;)V

    .line 17649
    :goto_0
    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17650
    new-instance v3, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v3}, Ljava/util/concurrent/TimeoutException;-><init>()V

    .line 17651
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/0B1;->a(Ljava/lang/Throwable;)V

    .line 17652
    const-string v4, "timeout"

    move-object/from16 v0, p1

    invoke-static {v4, v0}, LX/06J;->a(Ljava/lang/String;LX/0B1;)V

    .line 17653
    sget-object v4, LX/07S;->PINGRESP:LX/07S;

    invoke-virtual {v2, v4}, LX/07S;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, LX/07S;->PUBACK:LX/07S;

    invoke-virtual {v2, v4}, LX/07S;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 17654
    :cond_0
    const-string v4, "MqttOperationManager"

    const-string v5, "connection/disconnect/request_timeout; client=%s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v13, v6, v7

    invoke-static {v4, v5, v6}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17655
    sget-object v4, LX/07S;->PINGRESP:LX/07S;

    invoke-virtual {v2, v4}, LX/07S;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, LX/0BJ;->PING:LX/0BJ;

    :goto_1
    invoke-virtual {v13, v3, v2}, LX/072;->a(Ljava/lang/Exception;LX/0BJ;)Ljava/util/concurrent/Future;

    .line 17656
    :cond_1
    return-void

    .line 17657
    :cond_2
    :try_start_1
    const-string v3, "MqttOperationManager"

    const-string v4, "operation/timeout/duplicate; id=%d, operation=%s, client=%s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v6, v7

    const/4 v5, 0x1

    invoke-virtual {v2}, LX/07S;->name()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/4 v5, 0x2

    aput-object v13, v6, v5

    invoke-static {v3, v4, v6}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 17658
    :catchall_0
    move-exception v2

    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 17659
    :cond_3
    sget-object v2, LX/0BJ;->PUBLISH:LX/0BJ;

    goto :goto_1
.end method


# virtual methods
.method public final a(I)LX/0B1;
    .locals 14

    .prologue
    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 17583
    iget-object v3, p0, LX/06J;->g:Ljava/util/Map;

    monitor-enter v3

    .line 17584
    :try_start_0
    iget-object v2, p0, LX/06J;->g:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, LX/0B1;

    move-object v11, v0

    .line 17585
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17586
    if-eqz v11, :cond_1

    .line 17587
    const-string v2, "complete"

    invoke-static {v2, v11}, LX/06J;->a(Ljava/lang/String;LX/0B1;)V

    .line 17588
    invoke-virtual {v11}, LX/0B1;->b()V

    .line 17589
    const-string v2, "MqttOperationManager"

    const-string v3, "Complete operation,  currentTime: %d"

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, LX/06J;->e:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v5}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v3, v4}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17590
    iget-object v2, v11, LX/0B1;->a:LX/072;

    .line 17591
    iget-object v0, v2, LX/072;->B:Landroid/net/NetworkInfo;

    move-object v10, v0

    .line 17592
    iget-wide v12, v2, LX/072;->C:J

    move-wide v6, v12

    .line 17593
    iget-object v2, p0, LX/06J;->f:LX/059;

    invoke-virtual {v2}, LX/059;->h()J

    move-result-wide v8

    .line 17594
    iget-object v2, p0, LX/06J;->e:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    iget-wide v4, v11, LX/0B1;->d:J

    sub-long v4, v2, v4

    .line 17595
    iget-object v2, v11, LX/0B1;->b:LX/07S;

    sget-object v3, LX/07S;->PUBACK:LX/07S;

    invoke-virtual {v2, v3}, LX/07S;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 17596
    iget-object v2, p0, LX/06J;->d:LX/05i;

    const-class v3, LX/0BB;

    invoke-virtual {v2, v3}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v2

    check-cast v2, LX/0BB;

    sget-object v3, LX/0BC;->PublishAcknowledgementMs:LX/0BC;

    invoke-virtual {v2, v3, v4, v5}, LX/0BB;->a(LX/0BC;J)V

    .line 17597
    :cond_0
    iget-object v2, p0, LX/06J;->c:LX/05h;

    iget-object v3, v11, LX/0B1;->b:LX/07S;

    invoke-virtual {v3}, LX/07S;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v2 .. v10}, LX/05h;->a(Ljava/lang/String;JJJLandroid/net/NetworkInfo;)V

    .line 17598
    :goto_0
    return-object v11

    .line 17599
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 17600
    :cond_1
    const-string v2, "MqttOperationManager"

    const-string v3, "operation/complete/not_found; id=%d"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17601
    const/4 v11, 0x0

    goto :goto_0
.end method

.method public final a(LX/072;LX/07S;II)LX/0B1;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 17625
    invoke-static {p1}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17626
    new-instance v0, LX/0B1;

    iget-object v1, p0, LX/06J;->e:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, LX/0B1;-><init>(LX/072;LX/07S;IJ)V

    .line 17627
    iget-object v2, p0, LX/06J;->g:Ljava/util/Map;

    monitor-enter v2

    .line 17628
    :try_start_0
    iget-object v1, p0, LX/06J;->g:Ljava/util/Map;

    iget v3, v0, LX/0B1;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0B1;

    .line 17629
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17630
    if-eqz v1, :cond_0

    .line 17631
    new-instance v2, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v2}, Ljava/util/concurrent/TimeoutException;-><init>()V

    invoke-virtual {v1, v2}, LX/0B1;->a(Ljava/lang/Throwable;)V

    .line 17632
    const-string v2, "MqttOperationManager"

    const-string v3, "operation/add/duplicate; id=%d, name=%s"

    new-array v4, v8, [Ljava/lang/Object;

    iget v5, v1, LX/0B1;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v1, v1, LX/0B1;->b:LX/07S;

    invoke-virtual {v1}, LX/07S;->name()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v7

    invoke-static {v2, v3, v4}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17633
    :cond_0
    iget-object v1, p0, LX/06J;->b:LX/05q;

    new-instance v2, Lcom/facebook/rti/mqtt/manager/MqttOperationManager$1;

    invoke-direct {v2, p0, v0, p4}, Lcom/facebook/rti/mqtt/manager/MqttOperationManager$1;-><init>(LX/06J;LX/0B1;I)V

    int-to-long v4, p4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v4, v5, v3}, LX/05q;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    move-result-object v1

    .line 17634
    invoke-virtual {v0, v1}, LX/0B1;->a(LX/05u;)V

    .line 17635
    const-string v1, "MqttOperationManager"

    const-string v2, "operation/add; id=%d, name=%s, timeoutSec=%d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p2}, LX/07S;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v1, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17636
    return-object v0

    .line 17637
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 17614
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 17615
    iget-object v1, p0, LX/06J;->g:Ljava/util/Map;

    monitor-enter v1

    .line 17616
    :try_start_0
    iget-object v2, p0, LX/06J;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 17617
    iget-object v2, p0, LX/06J;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 17618
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17619
    const-string v1, "MqttOperationManager"

    const-string v2, "operation/abort; pendingSize=%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17620
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0B1;

    .line 17621
    invoke-virtual {v0, p1}, LX/0B1;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 17622
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 17623
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "abort:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/06J;->a(Ljava/lang/String;LX/0B1;)V

    .line 17624
    return-void
.end method

.method public final a(LX/0B1;ILjava/lang/Runnable;)Z
    .locals 5

    .prologue
    .line 17602
    iget-object v1, p0, LX/06J;->g:Ljava/util/Map;

    monitor-enter v1

    .line 17603
    :try_start_0
    iget-object v0, p0, LX/06J;->g:Ljava/util/Map;

    iget v2, p1, LX/0B1;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17604
    const/4 v0, 0x0

    monitor-exit v1

    .line 17605
    :goto_0
    return v0

    .line 17606
    :cond_0
    iget-object v0, p0, LX/06J;->g:Ljava/util/Map;

    iget v2, p1, LX/0B1;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17607
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17608
    iget-object v0, p0, LX/06J;->b:LX/05q;

    new-instance v1, Lcom/facebook/rti/mqtt/manager/MqttOperationManager$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/rti/mqtt/manager/MqttOperationManager$2;-><init>(LX/06J;LX/0B1;I)V

    int-to-long v2, p2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/05q;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    move-result-object v0

    .line 17609
    invoke-virtual {p1, v0}, LX/0B1;->a(LX/05u;)V

    .line 17610
    iget-object v1, p0, LX/06J;->a:LX/05m;

    invoke-interface {v0, p3, v1}, LX/05u;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 17611
    const-string v0, "add"

    invoke-static {v0, p1}, LX/06J;->a(Ljava/lang/String;LX/0B1;)V

    .line 17612
    const/4 v0, 0x1

    goto :goto_0

    .line 17613
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
