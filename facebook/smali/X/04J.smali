.class public LX/04J;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/04J;


# instance fields
.field private a:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 12997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12998
    new-instance v0, LX/0aq;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/04J;->a:LX/0aq;

    .line 12999
    return-void
.end method

.method public static a(LX/0QB;)LX/04J;
    .locals 3

    .prologue
    .line 13000
    sget-object v0, LX/04J;->b:LX/04J;

    if-nez v0, :cond_1

    .line 13001
    const-class v1, LX/04J;

    monitor-enter v1

    .line 13002
    :try_start_0
    sget-object v0, LX/04J;->b:LX/04J;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 13003
    if-eqz v2, :cond_0

    .line 13004
    :try_start_1
    new-instance v0, LX/04J;

    invoke-direct {v0}, LX/04J;-><init>()V

    .line 13005
    move-object v0, v0

    .line 13006
    sput-object v0, LX/04J;->b:LX/04J;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 13007
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 13008
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 13009
    :cond_1
    sget-object v0, LX/04J;->b:LX/04J;

    return-object v0

    .line 13010
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 13011
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13012
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 13013
    iget-object v0, p0, LX/04J;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 13014
    if-nez v0, :cond_0

    .line 13015
    const/4 v0, 0x0

    .line 13016
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 13017
    if-nez p3, :cond_0

    .line 13018
    const-string v0, "VideoLoggingPropertyBag"

    const-string v1, "Cannot put null value for videoId=%s and key=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13019
    :goto_0
    return-void

    .line 13020
    :cond_0
    iget-object v0, p0, LX/04J;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 13021
    if-nez v0, :cond_1

    .line 13022
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 13023
    iget-object v1, p0, LX/04J;->a:LX/0aq;

    invoke-virtual {v1, p1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13024
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
