.class public final LX/0GQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0GJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0GJ",
        "<",
        "LX/0AY;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0GR;

.field private b:LX/0GP;


# direct methods
.method public constructor <init>(LX/0GR;LX/0GP;)V
    .locals 0

    .prologue
    .line 35276
    iput-object p1, p0, LX/0GQ;->a:LX/0GR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35277
    iput-object p2, p0, LX/0GQ;->b:LX/0GP;

    .line 35278
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 35279
    check-cast p1, LX/0AY;

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 35280
    invoke-virtual {p1, v6}, LX/0AY;->a(I)LX/0Am;

    move-result-object v0

    .line 35281
    iget-object v0, v0, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v7

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    .line 35282
    iget v1, v0, LX/0Ak;->b:I

    packed-switch v1, :pswitch_data_0

    move-object v1, v7

    :cond_0
    move-object v7, v1

    .line 35283
    goto :goto_0

    .line 35284
    :pswitch_0
    iget-object v0, v0, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v2

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 35285
    if-nez v1, :cond_1

    .line 35286
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Audio representation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, LX/0Ah;->c:LX/0AR;

    iget-object v2, v2, LX/0AR;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v1, v0

    .line 35287
    goto :goto_1

    :cond_2
    move-object v2, v1

    .line 35288
    goto :goto_0

    .line 35289
    :pswitch_1
    if-nez v7, :cond_5

    move-object v1, v0

    .line 35290
    :goto_2
    iget-object v0, v0, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 35291
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Add video representation "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 35292
    :cond_3
    iget-object v0, p0, LX/0GQ;->a:LX/0GR;

    invoke-static {v0, p1}, LX/0GR;->b(LX/0GR;LX/0AY;)LX/0Jw;

    move-result-object v2

    .line 35293
    iget-object v0, p0, LX/0GQ;->a:LX/0GR;

    invoke-static {v0, p1}, LX/0GR;->a(LX/0GR;LX/0AY;)LX/0GX;

    move-result-object v3

    .line 35294
    if-eqz v7, :cond_4

    iget-object v0, v7, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    .line 35295
    :cond_4
    iget-object v1, p0, LX/0GQ;->b:LX/0GP;

    iget-wide v4, p1, LX/0AY;->j:J

    iget-object v0, p0, LX/0GQ;->a:LX/0GR;

    iget-boolean v0, v0, LX/0GR;->n:Z

    invoke-static {v7, v0}, LX/0Gj;->a(LX/0Ak;Z)Ljava/lang/String;

    move-result-object v7

    invoke-interface/range {v1 .. v7}, LX/0GP;->a(LX/0Jw;LX/0GX;JILjava/lang/String;)V

    .line 35296
    return-void

    :cond_5
    move-object v1, v7

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 35297
    invoke-static {p1}, LX/0Gj;->a(Ljava/io/IOException;)I

    move-result v0

    .line 35298
    const/16 v1, 0x19a

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/0GQ;->a:LX/0GR;

    iget-boolean v0, v0, LX/0GR;->G:Z

    if-eqz v0, :cond_0

    .line 35299
    iget-object v0, p0, LX/0GQ;->b:LX/0GP;

    const-string v1, "DISMISS"

    invoke-interface {v0, v1, p1}, LX/0GP;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 35300
    :goto_0
    return-void

    .line 35301
    :cond_0
    iget-object v0, p0, LX/0GQ;->b:LX/0GP;

    const-string v1, "ERROR_IO"

    invoke-interface {v0, v1, p1}, LX/0GP;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
