.class public LX/0KG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:J

.field public final c:Landroid/net/Uri;

.field public final d:Ljava/lang/String;

.field public final e:I

.field public f:J

.field private final g:[B


# direct methods
.method public constructor <init>(LX/0OA;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 40489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40490
    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    iput-object v0, p0, LX/0KG;->c:Landroid/net/Uri;

    .line 40491
    iput-object p2, p0, LX/0KG;->d:Ljava/lang/String;

    .line 40492
    iget v0, p1, LX/0OA;->g:I

    iput v0, p0, LX/0KG;->e:I

    .line 40493
    iget-object v0, p1, LX/0OA;->b:[B

    iput-object v0, p0, LX/0KG;->g:[B

    .line 40494
    iget-wide v0, p1, LX/0OA;->c:J

    iput-wide v0, p0, LX/0KG;->a:J

    .line 40495
    iget-wide v0, p1, LX/0OA;->d:J

    iput-wide v0, p0, LX/0KG;->b:J

    .line 40496
    iget-wide v0, p1, LX/0OA;->e:J

    iput-wide v0, p0, LX/0KG;->f:J

    .line 40497
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 40488
    iget-wide v0, p0, LX/0KG;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LX/0KG;->f:J

    goto :goto_0
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 40483
    iget-wide v0, p0, LX/0KG;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 40484
    iput-wide p1, p0, LX/0KG;->f:J

    .line 40485
    return-void

    .line 40486
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 40487
    iget-wide v0, p0, LX/0KG;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
