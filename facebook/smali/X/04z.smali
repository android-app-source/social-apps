.class public LX/04z;
.super Ljava/io/FilterWriter;
.source ""


# static fields
.field private static final UPPER_CASE_DIGITS:[B


# instance fields
.field public mUtf16State:C


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15013
    const/16 v0, 0x24

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/04z;->UPPER_CASE_DIGITS:[B

    return-void

    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x5at
    .end array-data
.end method

.method public constructor <init>(Ljava/io/Writer;)V
    .locals 1

    .prologue
    .line 15010
    invoke-direct {p0, p1}, Ljava/io/FilterWriter;-><init>(Ljava/io/Writer;)V

    .line 15011
    const/4 v0, 0x0

    iput-char v0, p0, LX/04z;->mUtf16State:C

    .line 15012
    return-void
.end method

.method public static resetState(LX/04z;)V
    .locals 1

    .prologue
    .line 15008
    const/4 v0, 0x0

    iput-char v0, p0, LX/04z;->mUtf16State:C

    .line 15009
    return-void
.end method

.method private static utf16IsSurrogate(C)Z
    .locals 2

    .prologue
    .line 15007
    const v0, 0xf800

    and-int/2addr v0, p0

    const v1, 0xd800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static writeHexByte(LX/04z;B)V
    .locals 3

    .prologue
    .line 15003
    iget-object v0, p0, Ljava/io/FilterWriter;->out:Ljava/io/Writer;

    const/16 v1, 0x25

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 15004
    iget-object v0, p0, Ljava/io/FilterWriter;->out:Ljava/io/Writer;

    sget-object v1, LX/04z;->UPPER_CASE_DIGITS:[B

    shr-int/lit8 v2, p1, 0x4

    and-int/lit8 v2, v2, 0xf

    aget-byte v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 15005
    iget-object v0, p0, Ljava/io/FilterWriter;->out:Ljava/io/Writer;

    sget-object v1, LX/04z;->UPPER_CASE_DIGITS:[B

    and-int/lit8 v2, p1, 0xf

    aget-byte v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 15006
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 14951
    invoke-virtual {p0}, LX/04z;->flush()V

    .line 14952
    return-void
.end method

.method public flush()V
    .locals 1

    .prologue
    .line 14998
    iget-char v0, p0, LX/04z;->mUtf16State:C

    if-eqz v0, :cond_0

    .line 14999
    const/16 v0, 0x3f

    invoke-static {p0, v0}, LX/04z;->writeHexByte(LX/04z;B)V

    .line 15000
    invoke-static {p0}, LX/04z;->resetState(LX/04z;)V

    .line 15001
    :cond_0
    invoke-super {p0}, Ljava/io/FilterWriter;->flush()V

    .line 15002
    return-void
.end method

.method public write(I)V
    .locals 5

    .prologue
    const v4, 0xffff

    const/16 v3, 0x3f

    .line 14963
    int-to-char v0, p1

    .line 14964
    iget-char v1, p0, LX/04z;->mUtf16State:C

    if-eqz v1, :cond_1

    .line 14965
    invoke-static {v0}, LX/04z;->utf16IsSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14966
    and-int/lit16 v1, v0, 0x400

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 14967
    if-eqz v1, :cond_0

    .line 14968
    iget-char v1, p0, LX/04z;->mUtf16State:C

    .line 14969
    shl-int/lit8 v2, v1, 0xa

    add-int/2addr v2, v0

    const v3, 0x35fdc00

    sub-int/2addr v2, v3

    move v0, v2

    .line 14970
    shr-int/lit8 v1, v0, 0x12

    or-int/lit16 v1, v1, 0xf0

    int-to-byte v1, v1

    invoke-static {p0, v1}, LX/04z;->writeHexByte(LX/04z;B)V

    .line 14971
    shr-int/lit8 v1, v0, 0xc

    and-int/lit8 v1, v1, 0x3f

    or-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    invoke-static {p0, v1}, LX/04z;->writeHexByte(LX/04z;B)V

    .line 14972
    shr-int/lit8 v1, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    invoke-static {p0, v1}, LX/04z;->writeHexByte(LX/04z;B)V

    .line 14973
    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    invoke-static {p0, v0}, LX/04z;->writeHexByte(LX/04z;B)V

    .line 14974
    invoke-static {p0}, LX/04z;->resetState(LX/04z;)V

    .line 14975
    :goto_1
    return-void

    .line 14976
    :cond_0
    invoke-static {p0, v3}, LX/04z;->writeHexByte(LX/04z;B)V

    .line 14977
    invoke-static {p0}, LX/04z;->resetState(LX/04z;)V

    .line 14978
    invoke-virtual {p0, v0}, LX/04z;->write(I)V

    goto :goto_1

    .line 14979
    :cond_1
    and-int v1, v0, v4

    const/16 v2, 0x80

    if-ge v1, v2, :cond_6

    .line 14980
    int-to-byte v0, v0

    .line 14981
    const/16 v1, 0x61

    if-lt v0, v1, :cond_2

    const/16 v1, 0x7a

    if-le v0, v1, :cond_5

    :cond_2
    const/16 v1, 0x41

    if-lt v0, v1, :cond_3

    const/16 v1, 0x5a

    if-le v0, v1, :cond_5

    :cond_3
    const/16 v1, 0x30

    if-lt v0, v1, :cond_4

    const/16 v1, 0x39

    if-le v0, v1, :cond_5

    :cond_4
    const/16 v1, 0x2e

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_5

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_5

    const/16 v1, 0x5f

    if-ne v0, v1, :cond_b

    .line 14982
    :cond_5
    iget-object v1, p0, Ljava/io/FilterWriter;->out:Ljava/io/Writer;

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(I)V

    .line 14983
    :goto_2
    goto :goto_1

    .line 14984
    :cond_6
    and-int v1, v0, v4

    const/16 v2, 0x800

    if-ge v1, v2, :cond_7

    .line 14985
    shr-int/lit8 v1, v0, 0x6

    or-int/lit16 v1, v1, 0xc0

    int-to-byte v1, v1

    invoke-static {p0, v1}, LX/04z;->writeHexByte(LX/04z;B)V

    .line 14986
    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    invoke-static {p0, v0}, LX/04z;->writeHexByte(LX/04z;B)V

    goto :goto_1

    .line 14987
    :cond_7
    invoke-static {v0}, LX/04z;->utf16IsSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 14988
    and-int/lit16 v1, v0, 0x400

    if-nez v1, :cond_d

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 14989
    if-eqz v1, :cond_8

    .line 14990
    iput-char v0, p0, LX/04z;->mUtf16State:C

    goto :goto_1

    .line 14991
    :cond_8
    invoke-static {p0, v3}, LX/04z;->writeHexByte(LX/04z;B)V

    goto :goto_1

    .line 14992
    :cond_9
    shr-int/lit8 v1, v0, 0xc

    or-int/lit16 v1, v1, 0xe0

    int-to-byte v1, v1

    invoke-static {p0, v1}, LX/04z;->writeHexByte(LX/04z;B)V

    .line 14993
    shr-int/lit8 v1, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    invoke-static {p0, v1}, LX/04z;->writeHexByte(LX/04z;B)V

    .line 14994
    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    invoke-static {p0, v0}, LX/04z;->writeHexByte(LX/04z;B)V

    goto/16 :goto_1

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 14995
    :cond_b
    const/16 v1, 0x20

    if-ne v0, v1, :cond_c

    .line 14996
    iget-object v1, p0, Ljava/io/FilterWriter;->out:Ljava/io/Writer;

    const/16 v2, 0x2b

    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(I)V

    goto :goto_2

    .line 14997
    :cond_c
    invoke-static {p0, v0}, LX/04z;->writeHexByte(LX/04z;B)V

    goto :goto_2

    :cond_d
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public write(Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 14958
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    move v0, p2

    .line 14959
    :goto_0
    add-int v2, p2, p3

    if-ge v0, v2, :cond_0

    .line 14960
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, LX/04z;->write(I)V

    .line 14961
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14962
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public write([CII)V
    .locals 3

    .prologue
    .line 14953
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    move v0, p2

    .line 14954
    :goto_0
    add-int v2, p2, p3

    if-ge v0, v2, :cond_0

    .line 14955
    :try_start_0
    aget-char v2, p1, v0

    invoke-virtual {p0, v2}, LX/04z;->write(I)V

    .line 14956
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14957
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
