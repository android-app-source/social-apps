.class public LX/026;
.super LX/01J;
.source ""

# interfaces
.implements Ljava/util/Map;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/01J",
        "<TK;TV;>;",
        "Ljava/util/Map",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public a:LX/118;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/118",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6308
    invoke-direct {p0}, LX/01J;-><init>()V

    .line 6309
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 6306
    invoke-direct {p0, p1}, LX/01J;-><init>(I)V

    .line 6307
    return-void
.end method

.method private a()LX/118;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/118",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 6303
    iget-object v0, p0, LX/026;->a:LX/118;

    if-nez v0, :cond_0

    .line 6304
    new-instance v0, LX/03t;

    invoke-direct {v0, p0}, LX/03t;-><init>(LX/026;)V

    iput-object v0, p0, LX/026;->a:LX/118;

    .line 6305
    :cond_0
    iget-object v0, p0, LX/026;->a:LX/118;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 6302
    invoke-static {p0, p1}, LX/118;->c(Ljava/util/Map;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 6295
    invoke-direct {p0}, LX/026;->a()LX/118;

    move-result-object v0

    invoke-virtual {v0}, LX/118;->d()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 6301
    invoke-direct {p0}, LX/026;->a()LX/118;

    move-result-object v0

    invoke-virtual {v0}, LX/118;->e()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final putAll(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 6297
    iget v0, p0, LX/01J;->h:I

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/01J;->a(I)V

    .line 6298
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 6299
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 6300
    :cond_0
    return-void
.end method

.method public final values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 6296
    invoke-direct {p0}, LX/026;->a()LX/118;

    move-result-object v0

    invoke-virtual {v0}, LX/118;->f()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
