.class public LX/050;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field private a:Ljava/io/InputStream;

.field private b:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 0

    .prologue
    .line 15034
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 15035
    iput-object p1, p0, LX/050;->a:Ljava/io/InputStream;

    .line 15036
    iput p2, p0, LX/050;->b:I

    .line 15037
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 1

    .prologue
    .line 15014
    iget-object v0, p0, LX/050;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 15032
    iget-object v0, p0, LX/050;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 15033
    return-void
.end method

.method public final declared-synchronized mark(I)V
    .locals 1

    .prologue
    .line 15029
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/050;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15030
    monitor-exit p0

    return-void

    .line 15031
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 15028
    iget-object v0, p0, LX/050;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public final read()I
    .locals 5

    .prologue
    const/16 v4, 0x18

    const/16 v3, 0x8

    .line 15025
    const/16 v0, 0x17

    iget v1, p0, LX/050;->b:I

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 15026
    :try_start_0
    iget-object v0, p0, LX/050;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 15027
    iget v2, p0, LX/050;->b:I

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v0

    :catchall_0
    move-exception v0

    iget v2, p0, LX/050;->b:I

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0
.end method

.method public final read([B)I
    .locals 5

    .prologue
    const/16 v4, 0x18

    const/16 v3, 0x8

    .line 15022
    const/16 v0, 0x17

    iget v1, p0, LX/050;->b:I

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 15023
    :try_start_0
    iget-object v0, p0, LX/050;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 15024
    iget v2, p0, LX/050;->b:I

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v0

    :catchall_0
    move-exception v0

    iget v2, p0, LX/050;->b:I

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0
.end method

.method public final read([BII)I
    .locals 5

    .prologue
    const/16 v4, 0x18

    const/16 v3, 0x8

    .line 15019
    const/16 v0, 0x17

    iget v1, p0, LX/050;->b:I

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 15020
    :try_start_0
    iget-object v0, p0, LX/050;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 15021
    iget v2, p0, LX/050;->b:I

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v0

    :catchall_0
    move-exception v0

    iget v2, p0, LX/050;->b:I

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0
.end method

.method public final declared-synchronized reset()V
    .locals 1

    .prologue
    .line 15016
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/050;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15017
    monitor-exit p0

    return-void

    .line 15018
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final skip(J)J
    .locals 3

    .prologue
    .line 15015
    iget-object v0, p0, LX/050;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    return-wide v0
.end method
