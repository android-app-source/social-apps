.class public LX/01t;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Ljava/lang/reflect/Field;

.field private static b:Ljava/lang/reflect/Field;

.field private static c:Ljava/lang/reflect/Field;

.field private static d:Ljava/lang/reflect/Method;

.field private static e:Ljava/lang/reflect/Field;

.field private static f:Z


# instance fields
.field private final g:Landroid/os/Handler;

.field private h:Landroid/os/Message;

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5704
    invoke-static {}, LX/01t;->e()V

    .line 5705
    new-instance v0, LX/01w;

    invoke-direct {v0, p0}, LX/01w;-><init>(LX/01t;)V

    iput-object v0, p0, LX/01t;->g:Landroid/os/Handler;

    .line 5706
    iget-object v0, p0, LX/01t;->g:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput-object v0, p0, LX/01t;->h:Landroid/os/Message;

    .line 5707
    return-void
.end method

.method private static a(Landroid/os/MessageQueue;)Landroid/os/Message;
    .locals 2

    .prologue
    .line 5708
    :try_start_0
    sget-object v0, LX/01t;->e:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 5709
    :catch_0
    move-exception v0

    .line 5710
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public static a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 5634
    const-class v0, Landroid/os/Message;

    const-string v1, "flags"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 5635
    sput-object v0, LX/01t;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 5636
    const-class v0, Landroid/os/Message;

    const-string v1, "target"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 5637
    sput-object v0, LX/01t;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 5638
    const-class v0, Landroid/os/Message;

    const-string v1, "next"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 5639
    sput-object v0, LX/01t;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 5640
    const-class v0, Landroid/os/MessageQueue;

    const-string v1, "next"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 5641
    sput-object v0, LX/01t;->d:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 5642
    const-class v0, Landroid/os/MessageQueue;

    const-string v1, "mMessages"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 5643
    sput-object v0, LX/01t;->e:Ljava/lang/reflect/Field;

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 5644
    sput-boolean v3, LX/01t;->f:Z

    .line 5645
    return-void
.end method

.method public static a(LX/03o;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5685
    invoke-static {}, LX/01t;->e()V

    .line 5686
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v3

    .line 5687
    monitor-enter v3

    .line 5688
    :try_start_0
    invoke-static {v3}, LX/01t;->a(Landroid/os/MessageQueue;)Landroid/os/Message;

    move-result-object v0

    .line 5689
    if-eqz v0, :cond_1

    .line 5690
    :cond_0
    invoke-static {v0}, LX/01t;->e(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 5691
    invoke-virtual {p0, v0}, LX/03o;->a(Landroid/os/Message;)I

    move-result v4

    .line 5692
    and-int/lit8 v5, v4, 0x2

    if-eqz v5, :cond_3

    .line 5693
    invoke-static {v0}, LX/01t;->g(Landroid/os/Message;)V

    .line 5694
    const/4 v5, 0x0

    invoke-static {v0, v5}, LX/01t;->a(Landroid/os/Message;Landroid/os/Message;)V

    .line 5695
    if-nez v1, :cond_2

    .line 5696
    invoke-static {v3, v2}, LX/01t;->a(Landroid/os/MessageQueue;Landroid/os/Message;)V

    :goto_0
    move-object v0, v2

    .line 5697
    :goto_1
    and-int/lit8 v2, v4, 0x1

    if-nez v2, :cond_1

    .line 5698
    if-nez v0, :cond_0

    .line 5699
    :cond_1
    monitor-exit v3

    return-void

    .line 5700
    :cond_2
    invoke-static {v1, v2}, LX/01t;->a(Landroid/os/Message;Landroid/os/Message;)V

    goto :goto_0

    .line 5701
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move-object v1, v0

    move-object v0, v2

    .line 5702
    goto :goto_1
.end method

.method public static a(Landroid/os/Message;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x20

    .line 5678
    const-string v0, "nonodex manually pumping one message"

    invoke-static {v2, v3, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 5679
    :try_start_0
    invoke-static {}, LX/01t;->e()V

    .line 5680
    invoke-static {p0}, LX/01t;->c(Landroid/os/Message;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 5681
    invoke-static {p0}, LX/01t;->d(Landroid/os/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5682
    invoke-static {v2, v3}, LX/018;->a(J)V

    .line 5683
    return-void

    .line 5684
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, LX/018;->a(J)V

    throw v0
.end method

.method private static a(Landroid/os/Message;I)V
    .locals 2

    .prologue
    .line 5674
    :try_start_0
    sget-object v0, LX/01t;->a:Ljava/lang/reflect/Field;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5675
    return-void

    .line 5676
    :catch_0
    move-exception v0

    .line 5677
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static a(Landroid/os/Message;Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 5670
    :try_start_0
    sget-object v0, LX/01t;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5671
    return-void

    .line 5672
    :catch_0
    move-exception v0

    .line 5673
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static a(Landroid/os/MessageQueue;Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 5666
    :try_start_0
    sget-object v0, LX/01t;->e:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5667
    return-void

    .line 5668
    :catch_0
    move-exception v0

    .line 5669
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static a(Ljava/lang/reflect/InvocationTargetException;)V
    .locals 2

    .prologue
    .line 5661
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 5662
    if-nez v0, :cond_0

    move-object v0, p0

    .line 5663
    :cond_0
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_1

    .line 5664
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 5665
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static b(Landroid/os/MessageQueue;)Landroid/os/Message;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5646
    const v0, -0x6b2e1ee8

    .line 5647
    move v0, v0

    .line 5648
    invoke-static {v2, v0}, Lcom/facebook/loom/logger/api/LoomLogger;->c(II)I

    .line 5649
    :try_start_0
    sget-object v0, LX/01t;->d:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5650
    const v1, -0x5efa9309

    .line 5651
    move v1, v1

    .line 5652
    invoke-static {v2, v1}, Lcom/facebook/loom/logger/api/LoomLogger;->d(II)I

    return-object v0

    .line 5653
    :catch_0
    move-exception v0

    .line 5654
    :try_start_1
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5655
    :catchall_0
    move-exception v0

    const v1, -0x16f4d1ed

    .line 5656
    move v1, v1

    .line 5657
    invoke-static {v2, v1}, Lcom/facebook/loom/logger/api/LoomLogger;->d(II)I

    throw v0

    .line 5658
    :catch_1
    move-exception v0

    .line 5659
    invoke-static {v0}, LX/01t;->a(Ljava/lang/reflect/InvocationTargetException;)V

    .line 5660
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public static c(Landroid/os/Message;)Landroid/os/Handler;
    .locals 2

    .prologue
    .line 5711
    :try_start_0
    sget-object v0, LX/01t;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 5712
    :catch_0
    move-exception v0

    .line 5713
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static d(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 5584
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/01t;->a(Landroid/os/Message;I)V

    .line 5585
    invoke-virtual {p0}, Landroid/os/Message;->recycle()V

    .line 5586
    return-void
.end method

.method private static e(Landroid/os/Message;)Landroid/os/Message;
    .locals 2

    .prologue
    .line 5587
    :try_start_0
    sget-object v0, LX/01t;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 5588
    :catch_0
    move-exception v0

    .line 5589
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public static e()V
    .locals 2

    .prologue
    .line 5590
    sget-boolean v0, LX/01t;->f:Z

    if-nez v0, :cond_0

    .line 5591
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "init not called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 5592
    :cond_0
    return-void
.end method

.method private static f(Landroid/os/Message;)I
    .locals 2

    .prologue
    .line 5593
    :try_start_0
    sget-object v0, LX/01t;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 5594
    :catch_0
    move-exception v0

    .line 5595
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public static g(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 5596
    invoke-static {p0}, LX/01t;->f(Landroid/os/Message;)I

    move-result v0

    .line 5597
    and-int/lit8 v0, v0, -0x2

    .line 5598
    invoke-static {p0, v0}, LX/01t;->a(Landroid/os/Message;I)V

    .line 5599
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Handler;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 5600
    invoke-virtual {p1, p2}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 5601
    invoke-static {p2}, LX/01t;->d(Landroid/os/Message;)V

    .line 5602
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 5603
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/01t;->h:Landroid/os/Message;

    if-eqz v0, :cond_1

    .line 5604
    iget-boolean v0, p0, LX/01t;->i:Z

    if-eqz v0, :cond_0

    .line 5605
    iget-object v0, p0, LX/01t;->g:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 5606
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/01t;->i:Z

    .line 5607
    :cond_0
    iget-object v0, p0, LX/01t;->h:Landroid/os/Message;

    const v1, -0x24aaff48

    .line 5608
    move v1, v1

    .line 5609
    invoke-static {v1}, Lcom/facebook/loom/logger/api/LoomLogger;->a(I)I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 5610
    iget-object v0, p0, LX/01t;->g:Landroid/os/Handler;

    iget-object v1, p0, LX/01t;->h:Landroid/os/Message;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 5611
    const/4 v0, 0x0

    iput-object v0, p0, LX/01t;->h:Landroid/os/Message;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5612
    :cond_1
    monitor-exit p0

    return-void

    .line 5613
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x20

    .line 5614
    const-string v0, "nonodex pumping messages"

    invoke-static {v4, v5, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 5615
    :try_start_0
    iget-object v0, p0, LX/01t;->g:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 5616
    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5617
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "MessagePumper has thread affinity"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5618
    :catchall_0
    move-exception v0

    invoke-static {v4, v5}, LX/018;->a(J)V

    throw v0

    .line 5619
    :cond_0
    :try_start_1
    invoke-virtual {p0}, LX/01t;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5620
    invoke-virtual {p0}, LX/01t;->b()V

    .line 5621
    :cond_1
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    .line 5622
    :cond_2
    invoke-static {v0}, LX/01t;->b(Landroid/os/MessageQueue;)Landroid/os/Message;

    move-result-object v1

    .line 5623
    invoke-static {v1}, LX/01t;->c(Landroid/os/Message;)Landroid/os/Handler;

    move-result-object v2

    .line 5624
    iget-object v3, p0, LX/01t;->g:Landroid/os/Handler;

    if-ne v2, v3, :cond_4

    .line 5625
    invoke-virtual {v2, v1}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 5626
    invoke-static {v1}, LX/01t;->d(Landroid/os/Message;)V

    .line 5627
    :goto_0
    iget-boolean v1, p0, LX/01t;->j:Z

    if-nez v1, :cond_3

    invoke-virtual {p0}, LX/01t;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 5628
    invoke-virtual {p0}, LX/01t;->b()V

    .line 5629
    :cond_3
    iget-boolean v1, p0, LX/01t;->j:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_2

    .line 5630
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 5631
    return-void

    .line 5632
    :cond_4
    :try_start_2
    invoke-virtual {p0, v2, v1}, LX/01t;->a(Landroid/os/Handler;Landroid/os/Message;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 5633
    const/4 v0, 0x0

    return v0
.end method
