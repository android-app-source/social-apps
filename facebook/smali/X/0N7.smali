.class public final LX/0N7;
.super LX/0N4;
.source ""


# instance fields
.field private final b:LX/0Oj;

.field private c:I

.field public d:I

.field public e:I

.field private f:J

.field private g:LX/0L4;

.field private h:I

.field private i:J


# direct methods
.method public constructor <init>(LX/0LS;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 49044
    invoke-direct {p0, p1}, LX/0N4;-><init>(LX/0LS;)V

    .line 49045
    new-instance v0, LX/0Oj;

    const/16 v1, 0xf

    new-array v1, v1, [B

    invoke-direct {v0, v1}, LX/0Oj;-><init>([B)V

    iput-object v0, p0, LX/0N7;->b:LX/0Oj;

    .line 49046
    iget-object v0, p0, LX/0N7;->b:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    const/16 v1, 0x7f

    aput-byte v1, v0, v3

    .line 49047
    iget-object v0, p0, LX/0N7;->b:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    const/4 v1, -0x2

    aput-byte v1, v0, v4

    .line 49048
    iget-object v0, p0, LX/0N7;->b:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    const/4 v1, 0x2

    const/16 v2, -0x80

    aput-byte v2, v0, v1

    .line 49049
    iget-object v0, p0, LX/0N7;->b:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    const/4 v1, 0x3

    aput-byte v4, v0, v1

    .line 49050
    iput v3, p0, LX/0N7;->c:I

    .line 49051
    return-void
.end method

.method private c()V
    .locals 15

    .prologue
    const/4 v4, 0x0

    .line 49052
    iget-object v0, p0, LX/0N7;->b:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    .line 49053
    iget-object v1, p0, LX/0N7;->g:LX/0L4;

    if-nez v1, :cond_0

    .line 49054
    const-wide/16 v2, -0x1

    const/4 v8, -0x1

    .line 49055
    sget-object v5, LX/0OZ;->d:LX/0Oi;

    .line 49056
    array-length v6, v0

    invoke-virtual {v5, v0, v6}, LX/0Oi;->a([BI)V

    .line 49057
    const/16 v6, 0x3c

    invoke-virtual {v5, v6}, LX/0Oi;->b(I)V

    .line 49058
    const/4 v6, 0x6

    invoke-virtual {v5, v6}, LX/0Oi;->c(I)I

    move-result v6

    .line 49059
    sget-object v7, LX/0OZ;->a:[I

    aget v6, v7, v6

    .line 49060
    const/4 v7, 0x4

    invoke-virtual {v5, v7}, LX/0Oi;->c(I)I

    move-result v7

    .line 49061
    sget-object v9, LX/0OZ;->b:[I

    aget v12, v9, v7

    .line 49062
    const/4 v7, 0x5

    invoke-virtual {v5, v7}, LX/0Oi;->c(I)I

    move-result v7

    .line 49063
    sget-object v9, LX/0OZ;->c:[I

    array-length v9, v9

    if-lt v7, v9, :cond_1

    move v7, v8

    .line 49064
    :goto_0
    const/16 v9, 0xa

    invoke-virtual {v5, v9}, LX/0Oi;->b(I)V

    .line 49065
    const/4 v9, 0x2

    invoke-virtual {v5, v9}, LX/0Oi;->c(I)I

    move-result v5

    if-lez v5, :cond_2

    const/4 v5, 0x1

    :goto_1
    add-int v11, v6, v5

    .line 49066
    const-string v6, "audio/vnd.dts"

    const/4 v13, 0x0

    move-object v5, v4

    move-wide v9, v2

    move-object v14, v4

    invoke-static/range {v5 .. v14}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v5

    move-object v1, v5

    .line 49067
    iput-object v1, p0, LX/0N7;->g:LX/0L4;

    .line 49068
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    iget-object v2, p0, LX/0N7;->g:LX/0L4;

    invoke-interface {v1, v2}, LX/0LS;->a(LX/0L4;)V

    .line 49069
    :cond_0
    const/4 v1, 0x5

    aget-byte v1, v0, v1

    and-int/lit8 v1, v1, 0x2

    shl-int/lit8 v1, v1, 0xc

    const/4 v2, 0x6

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x4

    or-int/2addr v1, v2

    const/4 v2, 0x7

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xf0

    shr-int/lit8 v2, v2, 0x4

    or-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    move v1, v1

    .line 49070
    iput v1, p0, LX/0N7;->h:I

    .line 49071
    const-wide/32 v2, 0xf4240

    .line 49072
    const/4 v1, 0x4

    aget-byte v1, v0, v1

    and-int/lit8 v1, v1, 0x1

    shl-int/lit8 v1, v1, 0x6

    const/4 v4, 0x5

    aget-byte v4, v0, v4

    and-int/lit16 v4, v4, 0xfc

    shr-int/lit8 v4, v4, 0x2

    or-int/2addr v1, v4

    .line 49073
    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0x20

    move v0, v1

    .line 49074
    int-to-long v0, v0

    mul-long/2addr v0, v2

    iget-object v2, p0, LX/0N7;->g:LX/0L4;

    iget v2, v2, LX/0L4;->o:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/0N7;->f:J

    .line 49075
    return-void

    .line 49076
    :cond_1
    sget-object v9, LX/0OZ;->c:[I

    aget v7, v9, v7

    mul-int/lit16 v7, v7, 0x3e8

    div-int/lit8 v7, v7, 0x2

    goto :goto_0

    .line 49077
    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49078
    iput v0, p0, LX/0N7;->c:I

    .line 49079
    iput v0, p0, LX/0N7;->d:I

    .line 49080
    iput v0, p0, LX/0N7;->e:I

    .line 49081
    return-void
.end method

.method public final a(JZ)V
    .locals 1

    .prologue
    .line 49082
    iput-wide p1, p0, LX/0N7;->i:J

    .line 49083
    return-void
.end method

.method public final a(LX/0Oj;)V
    .locals 9

    .prologue
    const/16 v8, 0xf

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 49084
    :cond_0
    :goto_0
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    if-lez v0, :cond_3

    .line 49085
    iget v0, p0, LX/0N7;->c:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 49086
    :pswitch_0
    const/4 v0, 0x0

    .line 49087
    :cond_1
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v1

    if-lez v1, :cond_2

    .line 49088
    iget v1, p0, LX/0N7;->e:I

    shl-int/lit8 v1, v1, 0x8

    iput v1, p0, LX/0N7;->e:I

    .line 49089
    iget v1, p0, LX/0N7;->e:I

    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v2

    or-int/2addr v1, v2

    iput v1, p0, LX/0N7;->e:I

    .line 49090
    iget v1, p0, LX/0N7;->e:I

    const v2, 0x7ffe8001

    if-ne v1, v2, :cond_1

    .line 49091
    iput v0, p0, LX/0N7;->e:I

    .line 49092
    const/4 v0, 0x1

    .line 49093
    :cond_2
    move v0, v0

    .line 49094
    if-eqz v0, :cond_0

    .line 49095
    const/4 v0, 0x4

    iput v0, p0, LX/0N7;->d:I

    .line 49096
    iput v4, p0, LX/0N7;->c:I

    goto :goto_0

    .line 49097
    :pswitch_1
    iget-object v0, p0, LX/0N7;->b:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    .line 49098
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v1

    iget v2, p0, LX/0N7;->d:I

    sub-int v2, v8, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 49099
    iget v2, p0, LX/0N7;->d:I

    invoke-virtual {p1, v0, v2, v1}, LX/0Oj;->a([BII)V

    .line 49100
    iget v2, p0, LX/0N7;->d:I

    add-int/2addr v1, v2

    iput v1, p0, LX/0N7;->d:I

    .line 49101
    iget v1, p0, LX/0N7;->d:I

    if-ne v1, v8, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 49102
    if-eqz v0, :cond_0

    .line 49103
    invoke-direct {p0}, LX/0N7;->c()V

    .line 49104
    iget-object v0, p0, LX/0N7;->b:LX/0Oj;

    invoke-virtual {v0, v6}, LX/0Oj;->b(I)V

    .line 49105
    iget-object v0, p0, LX/0N4;->a:LX/0LS;

    iget-object v1, p0, LX/0N7;->b:LX/0Oj;

    invoke-interface {v0, v1, v8}, LX/0LS;->a(LX/0Oj;I)V

    .line 49106
    const/4 v0, 0x2

    iput v0, p0, LX/0N7;->c:I

    goto :goto_0

    .line 49107
    :pswitch_2
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    iget v1, p0, LX/0N7;->h:I

    iget v2, p0, LX/0N7;->d:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 49108
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    invoke-interface {v1, p1, v0}, LX/0LS;->a(LX/0Oj;I)V

    .line 49109
    iget v1, p0, LX/0N7;->d:I

    add-int/2addr v0, v1

    iput v0, p0, LX/0N7;->d:I

    .line 49110
    iget v0, p0, LX/0N7;->d:I

    iget v1, p0, LX/0N7;->h:I

    if-ne v0, v1, :cond_0

    .line 49111
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    iget-wide v2, p0, LX/0N7;->i:J

    iget v5, p0, LX/0N7;->h:I

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    .line 49112
    iget-wide v0, p0, LX/0N7;->i:J

    iget-wide v2, p0, LX/0N7;->f:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0N7;->i:J

    .line 49113
    iput v6, p0, LX/0N7;->c:I

    goto/16 :goto_0

    .line 49114
    :cond_3
    return-void

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 49115
    return-void
.end method
