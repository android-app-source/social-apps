.class public abstract LX/000;
.super LX/001;
.source ""

# interfaces
.implements LX/003;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "StringFormatUse",
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:Z

.field private E:Ljava/lang/Class;

.field public F:Ljava/lang/reflect/Field;

.field private G:Ljava/lang/reflect/Field;

.field public H:Ljava/lang/reflect/Field;

.field private I:Ljava/lang/reflect/Method;

.field private J:Ljava/util/Random;

.field public K:I

.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field private m:Z

.field private n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field public o:Landroid/os/Handler;

.field public p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Ljava/lang/Object;

.field private r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCustomMessageLock"
    .end annotation
.end field

.field private volatile s:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCustomMessageLock"
    .end annotation
.end field

.field public t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field public final x:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/base/app/SplashScreenActivity;",
            ">;"
        }
    .end annotation
.end field

.field public final z:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 327
    invoke-direct {p0}, LX/001;-><init>()V

    .line 328
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/000;->q:Ljava/lang/Object;

    .line 329
    iput-boolean v1, p0, LX/000;->s:Z

    .line 330
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/000;->x:Ljava/util/ArrayList;

    .line 331
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/000;->y:Ljava/util/ArrayList;

    .line 332
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/000;->z:Ljava/util/ArrayList;

    .line 333
    iput v1, p0, LX/000;->A:I

    .line 334
    iput v1, p0, LX/000;->B:I

    .line 335
    iput v1, p0, LX/000;->C:I

    .line 336
    iput-boolean v1, p0, LX/000;->D:Z

    .line 337
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 546
    :try_start_0
    invoke-static {p0, p1}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 547
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    :goto_0
    return p2

    .line 548
    :catch_0
    goto :goto_0
.end method

.method private a(Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;)Landroid/app/Activity;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 549
    iget-boolean v1, p1, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->d:Z

    .line 550
    invoke-virtual {p1}, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->recreate()V

    .line 551
    new-instance v2, LX/03o;

    invoke-direct {v2, p0, p1}, LX/03o;-><init>(LX/000;Landroid/app/Activity;)V

    .line 552
    invoke-static {v2}, LX/01t;->a(LX/03o;)V

    .line 553
    iget-object v0, v2, LX/03o;->b:Landroid/os/Message;

    if-nez v0, :cond_0

    .line 554
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should have found RELAUNCH_ACTIVITY message after Activity.recreate()"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 555
    :cond_0
    iget-object v0, v2, LX/03o;->b:Landroid/os/Message;

    invoke-static {v0}, LX/01t;->a(Landroid/os/Message;)V

    .line 556
    const/4 v0, 0x0

    .line 557
    :try_start_0
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v3

    iget-object v4, v2, LX/03o;->a:Landroid/os/IBinder;

    invoke-virtual {v3, v4}, Landroid/app/ActivityThread;->getActivity(Landroid/os/IBinder;)Landroid/app/Activity;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 558
    :goto_0
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 559
    :try_start_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-ge v1, v3, :cond_2

    .line 560
    iget-object v1, p0, LX/000;->I:Ljava/lang/reflect/Method;

    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v2, v2, LX/03o;->a:Landroid/os/IBinder;

    aput-object v2, v4, v5

    const/4 v2, 0x1

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2

    .line 561
    :cond_1
    :goto_1
    return-object v0

    .line 562
    :catch_0
    const-string v3, "SplashScreenApplication"

    const-string v4, "new activity not found?! rhaId:%x"

    new-array v5, v5, [Ljava/lang/Object;

    iget-wide v6, p1, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 563
    :cond_2
    :try_start_2
    iget-object v1, p0, LX/000;->I:Ljava/lang/reflect/Method;

    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v2, v2, LX/03o;->a:Landroid/os/IBinder;

    aput-object v2, v4, v5

    const/4 v2, 0x1

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x2

    const-string v5, "nonodex-recreateSafelyAndSynchronously"

    aput-object v5, v4, v2

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 564
    :catch_1
    move-exception v0

    .line 565
    :goto_2
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 566
    :catch_2
    move-exception v0

    goto :goto_2
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;
    .locals 2

    .prologue
    .line 567
    invoke-virtual {p0, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 568
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 569
    return-object v0
.end method

.method private static varargs a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 2

    .prologue
    .line 570
    invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 571
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 572
    return-object v0
.end method

.method private static a(Ljava/lang/Thread;)V
    .locals 2

    .prologue
    .line 573
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 574
    return-void

    .line 575
    :catch_0
    move-exception v0

    .line 576
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public static a$redex0(LX/000;LX/01v;Landroid/os/Handler;Landroid/os/Message;)Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 577
    iget-boolean v0, p0, LX/000;->t:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 578
    :goto_0
    return v0

    .line 579
    :cond_0
    iget-object v0, p0, LX/000;->o:Landroid/os/Handler;

    if-ne p2, v0, :cond_8

    .line 580
    iget v0, p3, Landroid/os/Message;->what:I

    if-eqz v0, :cond_2

    const/16 v0, 0x64

    iget v3, p3, Landroid/os/Message;->what:I

    if-gt v0, v3, :cond_1

    iget v0, p3, Landroid/os/Message;->what:I

    const/16 v3, 0x95

    if-le v0, v3, :cond_2

    .line 581
    :cond_1
    const-string v0, "SplashScreenApplication"

    const-string v3, "stopping early-init message pump: unexpected message (what=%s) %s"

    new-array v4, v6, [Ljava/lang/Object;

    iget v5, p3, Landroid/os/Message;->what:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    iput-boolean v1, p0, LX/000;->t:Z

    .line 583
    invoke-virtual {p1}, LX/01t;->b()V

    move v0, v1

    .line 584
    goto :goto_0

    .line 585
    :cond_2
    iget v0, p3, Landroid/os/Message;->what:I

    iget v3, p0, LX/000;->f:I

    if-ne v0, v3, :cond_6

    .line 586
    iget-object v3, p3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 587
    if-eqz v3, :cond_3

    iget-object v0, p0, LX/000;->E:Ljava/lang/Class;

    invoke-virtual {v0, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 588
    :cond_3
    const-string v4, "SplashScreenApplication"

    const-string v5, "stopping early-init message pump: LAUNCH_ACTIVITY with unexpected ACR %s %s"

    new-array v6, v6, [Ljava/lang/Object;

    if-nez v3, :cond_4

    const-string v0, "null"

    :goto_1
    aput-object v0, v6, v2

    aput-object v3, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    iput-boolean v1, p0, LX/000;->t:Z

    .line 590
    iput-boolean v2, p0, LX/000;->v:Z

    .line 591
    invoke-virtual {p1}, LX/01t;->b()V

    move v0, v1

    .line 592
    goto :goto_0

    .line 593
    :cond_4
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_1

    .line 594
    :cond_5
    :try_start_0
    iget-object v0, p0, LX/000;->G:Ljava/lang/reflect/Field;

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 595
    iget v0, v0, Landroid/content/pm/ActivityInfo;->launchMode:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_6

    .line 596
    const-string v0, "SplashScreenApplication"

    const-string v3, "stopping early-init message pump: LAUNCH_ACTIVITY for LAUNCH_SINGLE_INSTANCE: %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p3, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    iput-boolean v1, p0, LX/000;->t:Z

    .line 598
    invoke-virtual {p1}, LX/01t;->b()V

    move v0, v1

    .line 599
    goto/16 :goto_0

    .line 600
    :catch_0
    move-exception v0

    .line 601
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 602
    :cond_6
    iget v0, p3, Landroid/os/Message;->what:I

    iget v3, p0, LX/000;->a:I

    if-eq v0, v3, :cond_7

    iget v0, p3, Landroid/os/Message;->what:I

    iget v3, p0, LX/000;->b:I

    if-eq v0, v3, :cond_7

    iget v0, p3, Landroid/os/Message;->what:I

    iget v3, p0, LX/000;->c:I

    if-eq v0, v3, :cond_7

    iget v0, p3, Landroid/os/Message;->what:I

    iget v3, p0, LX/000;->d:I

    if-eq v0, v3, :cond_7

    iget v0, p3, Landroid/os/Message;->what:I

    iget v3, p0, LX/000;->e:I

    if-eq v0, v3, :cond_7

    iget v0, p3, Landroid/os/Message;->what:I

    iget v3, p0, LX/000;->g:I

    if-eq v0, v3, :cond_7

    iget v0, p3, Landroid/os/Message;->what:I

    iget v3, p0, LX/000;->i:I

    if-eq v0, v3, :cond_7

    iget v0, p3, Landroid/os/Message;->what:I

    iget v3, p0, LX/000;->j:I

    if-eq v0, v3, :cond_7

    iget v0, p3, Landroid/os/Message;->what:I

    iget v3, p0, LX/000;->k:I

    if-eq v0, v3, :cond_7

    iget v0, p3, Landroid/os/Message;->what:I

    iget v3, p0, LX/000;->l:I

    if-ne v0, v3, :cond_8

    :cond_7
    move v0, v1

    .line 603
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 604
    goto/16 :goto_0
.end method

.method private static b(Ljava/lang/Class;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 605
    invoke-static {p0, p1}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 606
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static c(LX/000;Landroid/os/Message;)Z
    .locals 2

    .prologue
    .line 607
    iget-object v1, p0, LX/000;->q:Ljava/lang/Object;

    monitor-enter v1

    .line 608
    :try_start_0
    iget-boolean v0, p0, LX/000;->s:Z

    if-eqz v0, :cond_0

    .line 609
    const/4 v0, 0x0

    monitor-exit v1

    .line 610
    :goto_0
    return v0

    .line 611
    :cond_0
    iget-object v0, p0, LX/000;->r:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 612
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/000;->r:Ljava/util/ArrayList;

    .line 613
    :cond_1
    iget-object v0, p0, LX/000;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 614
    monitor-exit v1

    .line 615
    const/4 v0, 0x1

    goto :goto_0

    .line 616
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private l()V
    .locals 16

    .prologue
    const-wide/16 v14, 0x20

    const/4 v5, 0x0

    .line 617
    const-string v2, "SplashScreenApplication.finishActivityCreations"

    invoke-static {v14, v15, v2}, LX/018;->a(JLjava/lang/String;)V

    .line 618
    :try_start_0
    move-object/from16 v0, p0

    iget-object v7, v0, LX/000;->z:Ljava/util/ArrayList;

    .line 619
    :cond_0
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 620
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;

    .line 621
    invoke-static/range {p0 .. p0}, LX/000;->n(LX/000;)V

    .line 622
    const/4 v4, 0x0

    .line 623
    invoke-virtual {v2}, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_9

    iget-boolean v3, v2, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->e:Z

    if-nez v3, :cond_9

    .line 624
    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/000;->w:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 625
    const/4 v3, 0x1

    :try_start_1
    move-object/from16 v0, p0

    iput-boolean v3, v0, LX/000;->w:Z

    .line 626
    move-object/from16 v0, p0

    iget v3, v0, LX/000;->K:I

    if-eqz v3, :cond_1

    .line 627
    new-instance v2, Ljava/lang/AssertionError;

    const-string v3, "previous splash screen setup was not cleaned up"

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 628
    :catchall_0
    move-exception v2

    const/4 v3, 0x0

    :try_start_2
    move-object/from16 v0, p0

    iput v3, v0, LX/000;->K:I

    .line 629
    move-object/from16 v0, p0

    iput-boolean v6, v0, LX/000;->w:Z

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 630
    :catchall_1
    move-exception v2

    invoke-static {v14, v15}, LX/018;->a(J)V

    throw v2

    .line 631
    :cond_1
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, LX/000;->y:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v4, v5

    :goto_0
    if-ge v4, v8, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, LX/000;->y:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/base/app/SplashScreenActivity;

    .line 632
    iget-wide v10, v3, Lcom/facebook/base/app/SplashScreenActivity;->d:J

    iget-wide v12, v2, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->c:J

    cmp-long v9, v10, v12

    if-nez v9, :cond_2

    iget v9, v3, Lcom/facebook/base/app/SplashScreenActivity;->e:I

    move-object/from16 v0, p0

    iget v10, v0, LX/000;->K:I

    if-le v9, v10, :cond_2

    .line 633
    iget v3, v3, Lcom/facebook/base/app/SplashScreenActivity;->e:I

    move-object/from16 v0, p0

    iput v3, v0, LX/000;->K:I

    .line 634
    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 635
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/000;->a(Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;)Landroid/app/Activity;

    move-result-object v4

    .line 636
    move-object/from16 v0, p0

    iget v3, v0, LX/000;->K:I

    if-lez v3, :cond_4

    if-eqz v4, :cond_4

    instance-of v3, v4, LX/03p;

    if-eqz v3, :cond_4

    invoke-virtual {v4}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_4

    .line 637
    move-object v0, v4

    check-cast v0, LX/03p;

    move-object v3, v0

    move-object/from16 v0, p0

    iget v8, v0, LX/000;->A:I

    move-object/from16 v0, p0

    iget v9, v0, LX/000;->B:I

    move-object/from16 v0, p0

    iget v10, v0, LX/000;->C:I

    move-object/from16 v0, p0

    iget-boolean v11, v0, LX/000;->D:Z

    invoke-interface {v3, v8, v9, v10, v11}, LX/03p;->a(IIIZ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 638
    :cond_4
    const/4 v3, 0x0

    :try_start_4
    move-object/from16 v0, p0

    iput v3, v0, LX/000;->K:I

    .line 639
    move-object/from16 v0, p0

    iput-boolean v6, v0, LX/000;->w:Z

    move-object v6, v4

    .line 640
    :goto_1
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 641
    if-eqz v6, :cond_0

    .line 642
    iget-object v8, v2, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->b:Ljava/util/ArrayList;

    .line 643
    if-eqz v8, :cond_6

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v4, v2

    .line 644
    :goto_2
    if-lez v4, :cond_0

    .line 645
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActivityThread;->getInstrumentation()Landroid/app/Instrumentation;

    move-result-object v9

    move v3, v5

    .line 646
    :goto_3
    if-ge v3, v4, :cond_0

    .line 647
    invoke-virtual {v6}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_5

    .line 648
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    invoke-virtual {v9, v6, v2}, Landroid/app/Instrumentation;->callActivityOnNewIntent(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 649
    :cond_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    :cond_6
    move v4, v5

    .line 650
    goto :goto_2

    .line 651
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, LX/000;->y:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v5

    :goto_4
    if-ge v3, v4, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, LX/000;->y:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/base/app/SplashScreenActivity;

    .line 652
    invoke-virtual {v2}, Lcom/facebook/base/app/SplashScreenActivity;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 653
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 654
    :cond_8
    invoke-static {v14, v15}, LX/018;->a(J)V

    .line 655
    return-void

    :cond_9
    move-object v6, v4

    goto :goto_1
.end method

.method private static n(LX/000;)V
    .locals 1

    .prologue
    .line 656
    new-instance v0, LX/03n;

    invoke-direct {v0, p0}, LX/03n;-><init>(LX/000;)V

    invoke-virtual {v0}, LX/01t;->c()V

    .line 657
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Intent;)Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/app/SplashScreenActivity;",
            ">;"
        }
    .end annotation
.end method

.method public final a(Lcom/facebook/base/app/SplashScreenActivity;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 533
    iget-wide v4, p1, Lcom/facebook/base/app/SplashScreenActivity;->c:J

    .line 534
    iget-wide v6, p1, Lcom/facebook/base/app/SplashScreenActivity;->d:J

    .line 535
    iget-object v0, p0, LX/000;->x:Ljava/util/ArrayList;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v8

    .line 536
    const/4 v1, 0x0

    .line 537
    iget-object v0, p0, LX/000;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v2, v3

    :goto_0
    if-ge v2, v9, :cond_4

    iget-object v0, p0, LX/000;->z:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;

    .line 538
    iget-wide v10, v0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->c:J

    cmp-long v10, v10, v6

    if-nez v10, :cond_0

    .line 539
    :goto_1
    const/4 v1, 0x4

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v1, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v3, 0x2

    if-eqz v8, :cond_1

    const-string v1, "yes"

    :goto_2
    aput-object v1, v2, v3

    const/4 v3, 0x3

    if-eqz v0, :cond_2

    const-string v1, "yes"

    :goto_3
    aput-object v1, v2, v3

    .line 540
    if-nez v0, :cond_3

    .line 541
    invoke-virtual {p1}, Lcom/facebook/base/app/SplashScreenActivity;->a()V

    .line 542
    :goto_4
    return-void

    .line 543
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 544
    :cond_1
    const-string v1, "no"

    goto :goto_2

    :cond_2
    const-string v1, "no"

    goto :goto_3

    .line 545
    :cond_3
    iget-object v0, p0, LX/000;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Lcom/facebook/base/app/SplashScreenActivity;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 526
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p1, Lcom/facebook/base/app/SplashScreenActivity;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    iget-wide v2, p1, Lcom/facebook/base/app/SplashScreenActivity;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 527
    packed-switch p2, :pswitch_data_0

    .line 528
    :goto_0
    return-void

    .line 529
    :pswitch_0
    iget v0, p0, LX/000;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/000;->A:I

    goto :goto_0

    .line 530
    :pswitch_1
    iget v0, p0, LX/000;->B:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/000;->B:I

    goto :goto_0

    .line 531
    :pswitch_2
    iget v0, p0, LX/000;->C:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/000;->C:I

    goto :goto_0

    .line 532
    :pswitch_3
    iput-boolean v4, p0, LX/000;->D:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/os/Message;)Z
    .locals 1

    .prologue
    .line 525
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 524
    iget-boolean v0, p0, LX/000;->v:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/000;->t:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/000;->w:Z

    if-nez v0, :cond_0

    const-string v0, "com.facebook.showSplashScreen"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 509
    invoke-super {p0}, LX/001;->d()V

    .line 510
    invoke-static {}, LX/00G;->g()LX/00G;

    .line 511
    invoke-virtual {p0}, LX/000;->i()V

    .line 512
    invoke-virtual {p0}, LX/000;->h()Z

    move-result v0

    iput-boolean v0, p0, LX/000;->m:Z

    .line 513
    iget-boolean v0, p0, LX/000;->m:Z

    if-nez v0, :cond_1

    .line 514
    invoke-virtual {p0}, LX/001;->c()V

    .line 515
    :cond_0
    return-void

    .line 516
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/000;->v:Z

    .line 517
    new-instance v0, LX/01v;

    invoke-direct {v0, p0}, LX/01v;-><init>(LX/000;)V

    .line 518
    new-instance v1, Lcom/facebook/base/app/SplashScreenApplication$1;

    const-string v2, "EnsureDelegate"

    invoke-direct {v1, p0, v2, v0}, Lcom/facebook/base/app/SplashScreenApplication$1;-><init>(LX/000;Ljava/lang/String;LX/01v;)V

    .line 519
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 520
    invoke-virtual {v0}, LX/01t;->c()V

    .line 521
    invoke-static {v1}, LX/000;->a(Ljava/lang/Thread;)V

    .line 522
    iget-object v0, p0, LX/000;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 523
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "application was not found on mAllApplications"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final f()V
    .locals 5

    .prologue
    .line 505
    invoke-static {}, LX/00z;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, LX/00k;->a()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 506
    if-eqz v0, :cond_1

    .line 507
    invoke-super {p0}, LX/001;->f()V

    .line 508
    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x20

    .line 482
    iget-boolean v0, p0, LX/000;->m:Z

    if-nez v0, :cond_1

    .line 483
    invoke-virtual {p0}, LX/001;->e()Lcom/facebook/base/app/ApplicationLike;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/base/app/ApplicationLike;->b()V

    .line 484
    iput-boolean v6, p0, LX/000;->u:Z

    .line 485
    :cond_0
    return-void

    .line 486
    :cond_1
    invoke-virtual {p0}, LX/001;->e()Lcom/facebook/base/app/ApplicationLike;

    move-result-object v0

    .line 487
    new-instance v1, LX/01v;

    invoke-direct {v1, p0}, LX/01v;-><init>(LX/000;)V

    .line 488
    new-instance v2, Lcom/facebook/base/app/SplashScreenApplication$2;

    const-string v3, "CallOnCreateImpl"

    invoke-direct {v2, p0, v3, v0, v1}, Lcom/facebook/base/app/SplashScreenApplication$2;-><init>(LX/000;Ljava/lang/String;Lcom/facebook/base/app/ApplicationLike;LX/01v;)V

    .line 489
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 490
    const-string v0, "Waiting for onCreate"

    invoke-static {v4, v5, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 491
    :try_start_0
    invoke-virtual {v1}, LX/01t;->c()V

    .line 492
    invoke-static {v2}, LX/000;->a(Ljava/lang/Thread;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 493
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 494
    iget-object v0, p0, LX/000;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 495
    iget-object v0, p0, LX/000;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, LX/000;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 496
    invoke-static {v0}, LX/01t;->a(Landroid/os/Message;)V

    .line 497
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 498
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, LX/000;->p:Ljava/util/ArrayList;

    .line 499
    :cond_3
    iput-boolean v6, p0, LX/000;->u:Z

    .line 500
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/000;->v:Z

    .line 501
    invoke-direct {p0}, LX/000;->l()V

    .line 502
    iget-object v0, p0, LX/000;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 503
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should have recreated everything"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 504
    :catchall_0
    move-exception v0

    invoke-static {v4, v5}, LX/018;->a(J)V

    throw v0
.end method

.method public h()Z
    .locals 25

    .prologue
    .line 364
    const/4 v4, 0x0

    .line 365
    :try_start_0
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v13

    .line 366
    invoke-virtual {v13}, Landroid/app/ActivityThread;->getProcessName()Ljava/lang/String;

    move-result-object v3

    .line 367
    if-eqz v3, :cond_0

    const/16 v5, 0x3a

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 368
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "not using nonodex startup: in auxiliary process "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    const/4 v3, 0x0

    .line 370
    :goto_0
    return v3

    .line 371
    :cond_0
    invoke-static {}, LX/01t;->a()V

    .line 372
    const-string v3, "android.app.ActivityThread$ActivityClientRecord"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 373
    move-object/from16 v0, p0

    iput-object v3, v0, LX/000;->E:Ljava/lang/Class;

    .line 374
    const-string v5, "token"

    invoke-static {v3, v5}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, LX/000;->F:Ljava/lang/reflect/Field;

    .line 375
    const-string v5, "activityInfo"

    invoke-static {v3, v5}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, LX/000;->G:Ljava/lang/reflect/Field;

    .line 376
    const-class v3, Landroid/app/Activity;

    const-string v5, "mToken"

    invoke-static {v3, v5}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, LX/000;->H:Ljava/lang/reflect/Field;

    .line 377
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x18

    if-ge v3, v5, :cond_1

    .line 378
    const-class v3, Landroid/app/ActivityThread;

    const-string v5, "performStopActivity"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/os/IBinder;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-static {v3, v5, v6}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, LX/000;->I:Ljava/lang/reflect/Method;

    .line 379
    :goto_1
    const-class v3, Landroid/app/ActivityThread;

    const-string v5, "getHandler"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-static {v3, v5, v6}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v14

    .line 380
    const-class v3, Landroid/app/ActivityThread;

    const-string v5, "mBoundApplication"

    invoke-static {v3, v5}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v15

    .line 381
    const-class v3, Landroid/app/ActivityThread;

    const-string v5, "mInitialApplication"

    invoke-static {v3, v5}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v16

    .line 382
    const-class v3, Landroid/app/ActivityThread;

    const-string v5, "mAllApplications"

    invoke-static {v3, v5}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v17

    .line 383
    const-string v3, "android.app.ActivityThread$AppBindData"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v18

    .line 384
    const-string v3, "info"

    move-object/from16 v0, v18

    invoke-static {v0, v3}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v19

    .line 385
    const-string v3, "android.app.LoadedApk"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    .line 386
    const-string v3, "mApplication"

    invoke-static {v9, v3}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v20

    .line 387
    const-string v3, "android.app.ContextImpl"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 388
    const-string v5, "setOuterContext"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/content/Context;

    aput-object v8, v6, v7

    invoke-static {v3, v5, v6}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v21

    .line 389
    const-class v3, Landroid/app/Application;

    const-string v5, "mLoadedApk"

    invoke-static {v3, v5}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v22

    .line 390
    const/4 v8, 0x0

    .line 391
    const/4 v7, 0x0

    .line 392
    const/4 v6, 0x0

    .line 393
    const/4 v5, 0x0

    .line 394
    const/4 v3, 0x0

    .line 395
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x15

    if-lt v10, v11, :cond_e

    .line 396
    const-string v3, "getResources"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/app/ActivityThread;

    aput-object v7, v5, v6

    invoke-static {v9, v3, v5}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    .line 397
    const-string v3, "getAssets"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/app/ActivityThread;

    aput-object v7, v5, v6

    invoke-static {v9, v3, v5}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v7

    .line 398
    const-string v3, "getClassLoader"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-static {v9, v3, v5}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 399
    const-string v3, "rewriteRValues"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v10, 0x0

    const-class v11, Ljava/lang/ClassLoader;

    aput-object v11, v5, v10

    const/4 v10, 0x1

    const-class v11, Ljava/lang/String;

    aput-object v11, v5, v10

    const/4 v10, 0x2

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v5, v10

    invoke-static {v9, v3, v5}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 400
    const-class v3, Landroid/content/res/AssetManager;

    const-string v9, "getAssignedPackageIdentifiers"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Class;

    invoke-static {v3, v9, v10}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    move-object v9, v5

    move-object v10, v6

    move-object v11, v7

    move-object v12, v8

    move-object v8, v3

    .line 401
    :goto_2
    invoke-virtual {v15, v13}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .line 402
    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    .line 403
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iput-object v3, v0, LX/000;->n:Ljava/util/ArrayList;

    .line 404
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v14, v13, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    move-object/from16 v0, p0

    iput-object v3, v0, LX/000;->o:Landroid/os/Handler;

    .line 405
    move-object/from16 v0, p0

    iget-object v3, v0, LX/000;->o:Landroid/os/Handler;

    if-nez v3, :cond_2

    .line 406
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v5, "main handler unexpectedly null"

    invoke-direct {v3, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    .line 407
    :catch_0
    move-exception v3

    .line 408
    :goto_3
    if-eqz v4, :cond_c

    .line 409
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 410
    :cond_1
    :try_start_1
    const-class v3, Landroid/app/ActivityThread;

    const-string v5, "performStopActivity"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/os/IBinder;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v3, v5, v6}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, LX/000;->I:Ljava/lang/reflect/Method;

    goto/16 :goto_1

    .line 411
    :catch_1
    move-exception v3

    goto :goto_3

    .line 412
    :cond_2
    const-string v3, "android.app.ActivityThread$H"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 413
    const-string v5, "BIND_SERVICE"

    invoke-static {v3, v5}, LX/000;->b(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, LX/000;->a:I

    .line 414
    const-string v5, "CREATE_SERVICE"

    invoke-static {v3, v5}, LX/000;->b(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, LX/000;->b:I

    .line 415
    const-string v5, "DUMP_PROVIDER"

    const/4 v6, -0x1

    invoke-static {v3, v5, v6}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, LX/000;->c:I

    .line 416
    const-string v5, "EXIT_APPLICATION"

    invoke-static {v3, v5}, LX/000;->b(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, LX/000;->d:I

    .line 417
    const-string v5, "INSTALL_PROVIDER"

    const/4 v6, -0x1

    invoke-static {v3, v5, v6}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, LX/000;->e:I

    .line 418
    const-string v5, "LAUNCH_ACTIVITY"

    invoke-static {v3, v5}, LX/000;->b(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, LX/000;->f:I

    .line 419
    const-string v5, "RECEIVER"

    invoke-static {v3, v5}, LX/000;->b(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, LX/000;->g:I

    .line 420
    const-string v5, "RELAUNCH_ACTIVITY"

    invoke-static {v3, v5}, LX/000;->b(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, LX/000;->h:I

    .line 421
    const-string v5, "REMOVE_PROVIDER"

    invoke-static {v3, v5}, LX/000;->b(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, LX/000;->i:I

    .line 422
    const-string v5, "SERVICE_ARGS"

    invoke-static {v3, v5}, LX/000;->b(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, LX/000;->j:I

    .line 423
    const-string v5, "STOP_SERVICE"

    invoke-static {v3, v5}, LX/000;->b(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, LX/000;->k:I

    .line 424
    const-string v5, "UNBIND_SERVICE"

    invoke-static {v3, v5}, LX/000;->b(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, LX/000;->l:I

    .line 425
    invoke-virtual {v13}, Landroid/app/ActivityThread;->getInstrumentation()Landroid/app/Instrumentation;

    move-result-object v5

    .line 426
    if-nez v5, :cond_3

    .line 427
    const-string v3, "SplashScreenApplication"

    const-string v5, "not using nonodex startup: ActivityThread has no instrumentation"

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 429
    :cond_3
    const/4 v7, 0x0

    .line 430
    const/4 v6, 0x0

    .line 431
    const/4 v3, 0x0

    .line 432
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    const-class v17, Landroid/app/Instrumentation;

    move-object/from16 v0, v17

    if-eq v14, v0, :cond_d

    .line 433
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 434
    const-string v6, "io.selendroid.server.LightweightInstrumentation"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 435
    const/4 v7, 0x1

    .line 436
    const-string v3, "instrumentationArgs"

    move-object/from16 v0, v18

    invoke-static {v0, v3}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 437
    invoke-virtual {v3, v15}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    .line 438
    const-string v6, "yes"

    const-string v14, "want_splash"

    invoke-virtual {v3, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 439
    const-string v3, "SplashScreenApplication"

    const-string v5, "not using nonodex startup: selendroid test does not want one"

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 441
    :cond_4
    const-string v6, "main_activity"

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v24, v3

    move v3, v7

    move-object v7, v6

    move-object/from16 v6, v24

    .line 442
    :goto_4
    const-class v14, Landroid/app/Instrumentation;

    invoke-virtual {v14}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v14

    const-class v15, Ljava/lang/Object;

    if-eq v14, v15, :cond_6

    .line 443
    const-string v3, "SplashScreenApplication"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "not using nonodex startup: Instrumentation has unknown superclass "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v6, Landroid/app/Instrumentation;

    invoke-virtual {v6}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 445
    :cond_5
    const-string v5, "SplashScreenApplication"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "not using nonodex startup: custom instrumentation in place: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 447
    :cond_6
    new-instance v14, LX/01u;

    if-eqz v3, :cond_7

    move-object v3, v5

    :goto_5
    move-object/from16 v0, p0

    invoke-direct {v14, v0, v3}, LX/01u;-><init>(LX/000;Landroid/app/Instrumentation;)V

    .line 448
    const-class v3, Landroid/app/Instrumentation;

    invoke-virtual {v3}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v15

    array-length v0, v15

    move/from16 v17, v0

    const/4 v3, 0x0

    :goto_6
    move/from16 v0, v17

    if-ge v3, v0, :cond_8

    aget-object v18, v15, v3

    .line 449
    const/16 v23, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 450
    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v14, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 451
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 452
    :cond_7
    const/4 v3, 0x0

    goto :goto_5

    .line 453
    :cond_8
    const-class v3, Landroid/app/ActivityThread;

    const-string v5, "mInstrumentation"

    invoke-static {v3, v5}, LX/000;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 454
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 455
    const/4 v4, 0x1

    .line 456
    if-eqz v7, :cond_9

    .line 457
    const-string v5, "main_activity"

    invoke-virtual {v6, v5}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 458
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 459
    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v7}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 460
    const/high16 v6, 0x34020000

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 461
    const-string v6, "android.intent.action.MAIN"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 462
    const-string v6, "android.intent.category.LAUNCHER"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 463
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, LX/000;->startActivity(Landroid/content/Intent;)V

    .line 464
    :cond_9
    invoke-virtual {v3, v13, v14}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 465
    invoke-virtual/range {p0 .. p0}, LX/000;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 466
    move-object/from16 v0, p0

    iget-object v3, v0, LX/000;->n:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    move-object/from16 v2, p0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 468
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v3, v5, :cond_b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v13, v3, v5

    move-object/from16 v0, v19

    invoke-virtual {v12, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 469
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v13, v3, v5

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/AssetManager;

    .line 470
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v8, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/SparseArray;

    .line 471
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v6

    .line 472
    const/4 v5, 0x0

    :goto_7
    if-ge v5, v6, :cond_b

    .line 473
    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v7

    .line 474
    const/4 v8, 0x1

    if-eq v7, v8, :cond_a

    const/16 v8, 0x7f

    if-eq v7, v8, :cond_a

    .line 475
    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    move-object/from16 v0, v19

    invoke-virtual {v10, v0, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v8, v11

    const/4 v11, 0x1

    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v8, v11

    const/4 v11, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v8, v11

    move-object/from16 v0, v19

    invoke-virtual {v9, v0, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 477
    :cond_b
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v13, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3

    .line 478
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 479
    :cond_c
    const-string v4, "SplashScreenApplication"

    const-string v5, "error initializing nonodex"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 480
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 481
    :catch_2
    move-exception v3

    goto/16 :goto_3

    :catch_3
    move-exception v3

    goto/16 :goto_3

    :catch_4
    move-exception v3

    goto/16 :goto_3

    :cond_d
    move-object/from16 v24, v3

    move v3, v7

    move-object v7, v6

    move-object/from16 v6, v24

    goto/16 :goto_4

    :cond_e
    move-object v9, v5

    move-object v10, v6

    move-object v11, v7

    move-object v12, v8

    move-object v8, v3

    goto/16 :goto_2
.end method

.method public i()V
    .locals 0

    .prologue
    .line 363
    return-void
.end method

.method public final i_()V
    .locals 4

    .prologue
    .line 350
    iget-object v1, p0, LX/000;->q:Ljava/lang/Object;

    monitor-enter v1

    .line 351
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/000;->s:Z

    .line 352
    iget-object v2, p0, LX/000;->r:Ljava/util/ArrayList;

    .line 353
    const/4 v0, 0x0

    iput-object v0, p0, LX/000;->r:Ljava/util/ArrayList;

    .line 354
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    if-eqz v2, :cond_0

    .line 356
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 357
    invoke-static {}, LX/01t;->e()V

    .line 358
    invoke-static {v0}, LX/01t;->g(Landroid/os/Message;)V

    .line 359
    invoke-static {v0}, LX/01t;->c(Landroid/os/Message;)Landroid/os/Handler;

    move-result-object p0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 360
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 361
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 362
    :cond_0
    return-void
.end method

.method public final j()J
    .locals 6

    .prologue
    .line 344
    iget-object v0, p0, LX/000;->J:Ljava/util/Random;

    .line 345
    if-nez v0, :cond_0

    .line 346
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/000;->J:Ljava/util/Random;

    .line 347
    :cond_0
    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    .line 348
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 349
    return-wide v2
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 341
    iget-boolean v0, p0, LX/000;->u:Z

    if-eqz v0, :cond_0

    .line 342
    invoke-super {p0}, LX/001;->onLowMemory()V

    .line 343
    :cond_0
    return-void
.end method

.method public final onTrimMemory(I)V
    .locals 1

    .prologue
    .line 338
    iget-boolean v0, p0, LX/000;->u:Z

    if-eqz v0, :cond_0

    .line 339
    invoke-super {p0, p1}, LX/001;->onTrimMemory(I)V

    .line 340
    :cond_0
    return-void
.end method
