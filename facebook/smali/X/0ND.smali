.class public final LX/0ND;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0LS;

.field public b:J

.field public c:Z

.field public d:I

.field public e:J

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:J

.field public j:J

.field public k:Z


# direct methods
.method public constructor <init>(LX/0LS;)V
    .locals 0

    .prologue
    .line 49451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49452
    iput-object p1, p0, LX/0ND;->a:LX/0LS;

    .line 49453
    return-void
.end method

.method public static a(LX/0ND;I)V
    .locals 8

    .prologue
    .line 49454
    iget-boolean v0, p0, LX/0ND;->k:Z

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    .line 49455
    :goto_0
    iget-wide v0, p0, LX/0ND;->b:J

    iget-wide v2, p0, LX/0ND;->i:J

    sub-long/2addr v0, v2

    long-to-int v5, v0

    .line 49456
    iget-object v1, p0, LX/0ND;->a:LX/0LS;

    iget-wide v2, p0, LX/0ND;->j:J

    const/4 v7, 0x0

    move v6, p1

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    .line 49457
    return-void

    .line 49458
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JIIJ)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49459
    iput-boolean v2, p0, LX/0ND;->g:Z

    .line 49460
    iput-wide p5, p0, LX/0ND;->e:J

    .line 49461
    iput v2, p0, LX/0ND;->d:I

    .line 49462
    iput-wide p1, p0, LX/0ND;->b:J

    .line 49463
    const/16 v0, 0x20

    if-lt p4, v0, :cond_0

    iget-boolean v0, p0, LX/0ND;->h:Z

    if-eqz v0, :cond_0

    .line 49464
    invoke-static {p0, p3}, LX/0ND;->a(LX/0ND;I)V

    .line 49465
    iput-boolean v2, p0, LX/0ND;->h:Z

    .line 49466
    :cond_0
    const/16 v0, 0x10

    if-lt p4, v0, :cond_3

    const/16 v0, 0x15

    if-gt p4, v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/0ND;->c:Z

    .line 49467
    iget-boolean v0, p0, LX/0ND;->c:Z

    if-nez v0, :cond_1

    const/16 v0, 0x9

    if-gt p4, v0, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    iput-boolean v2, p0, LX/0ND;->f:Z

    .line 49468
    return-void

    :cond_3
    move v0, v2

    .line 49469
    goto :goto_0
.end method
