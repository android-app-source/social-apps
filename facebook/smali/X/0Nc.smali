.class public final LX/0Nc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Nb;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I


# direct methods
.method private constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 51216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51217
    iput-object p1, p0, LX/0Nc;->a:Landroid/content/Context;

    .line 51218
    iput p2, p0, LX/0Nc;->b:I

    .line 51219
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/0Nc;
    .locals 2

    .prologue
    .line 51220
    new-instance v0, LX/0Nc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LX/0Nc;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method private static a(LX/0Nv;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 51208
    iget-object v0, p0, LX/0Nv;->b:LX/0AR;

    iget-object v0, v0, LX/0AR;->k:Ljava/lang/String;

    .line 51209
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 51210
    :cond_0
    :goto_0
    return v1

    .line 51211
    :cond_1
    const-string v2, "(\\s*,\\s*)|(\\s*$)"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move v0, v1

    .line 51212
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 51213
    aget-object v3, v2, v0

    invoke-virtual {v3, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 51214
    const/4 v1, 0x1

    goto :goto_0

    .line 51215
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0Nl;LX/0Nh;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 51176
    iget v0, p0, LX/0Nc;->b:I

    if-eq v0, v7, :cond_0

    iget v0, p0, LX/0Nc;->b:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 51177
    :cond_0
    iget v0, p0, LX/0Nc;->b:I

    if-ne v0, v7, :cond_1

    iget-object v0, p1, LX/0Nl;->b:Ljava/util/List;

    move-object v2, v0

    .line 51178
    :goto_0
    if-eqz v2, :cond_b

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 51179
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 51180
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nv;

    invoke-interface {p2, v0}, LX/0Nh;->a(LX/0Nv;)V

    .line 51181
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 51182
    :cond_1
    iget-object v0, p1, LX/0Nl;->c:Ljava/util/List;

    move-object v2, v0

    goto :goto_0

    .line 51183
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 51184
    iget-object v0, p0, LX/0Nc;->a:Landroid/content/Context;

    iget-object v2, p1, LX/0Nl;->a:Ljava/util/List;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, LX/0Li;->a(Landroid/content/Context;Ljava/util/List;[Ljava/lang/String;Z)[I

    move-result-object v2

    move v0, v1

    .line 51185
    :goto_2
    array-length v3, v2

    if-ge v0, v3, :cond_3

    .line 51186
    iget-object v3, p1, LX/0Nl;->a:Ljava/util/List;

    aget v5, v2, v0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51187
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 51188
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 51189
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v2, v1

    .line 51190
    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 51191
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nv;

    .line 51192
    iget-object v6, v0, LX/0Nv;->b:LX/0AR;

    iget v6, v6, LX/0AR;->g:I

    if-gtz v6, :cond_4

    const-string v6, "avc"

    invoke-static {v0, v6}, LX/0Nc;->a(LX/0Nv;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 51193
    :cond_4
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51194
    :cond_5
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 51195
    :cond_6
    const-string v6, "mp4a"

    invoke-static {v0, v6}, LX/0Nc;->a(LX/0Nv;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 51196
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 51197
    :cond_7
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    move-object v2, v3

    .line 51198
    :goto_5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v7, :cond_8

    .line 51199
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LX/0Nv;

    .line 51200
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 51201
    invoke-interface {p2, p1, v0}, LX/0Nh;->a(LX/0Nl;[LX/0Nv;)V

    .line 51202
    :cond_8
    :goto_6
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 51203
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nv;

    invoke-interface {p2, v0}, LX/0Nh;->a(LX/0Nv;)V

    .line 51204
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 51205
    :cond_9
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_a

    .line 51206
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    :cond_a
    move-object v2, v4

    goto :goto_5

    .line 51207
    :cond_b
    return-void
.end method
