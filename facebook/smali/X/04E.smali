.class public final enum LX/04E;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/04E;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/04E;

.field public static final enum VIDEO_CHAINING_DEPTH_LEVEL:LX/04E;

.field public static final enum VIDEO_CHAINING_SESSION_ID:LX/04E;

.field public static final enum VIDEO_CHAINING_SOURCE:LX/04E;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12828
    new-instance v0, LX/04E;

    const-string v1, "VIDEO_CHAINING_DEPTH_LEVEL"

    const-string v2, "video_chaining_depth_level"

    invoke-direct {v0, v1, v3, v2}, LX/04E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04E;->VIDEO_CHAINING_DEPTH_LEVEL:LX/04E;

    .line 12829
    new-instance v0, LX/04E;

    const-string v1, "VIDEO_CHAINING_SESSION_ID"

    const-string v2, "video_chaining_session_id"

    invoke-direct {v0, v1, v4, v2}, LX/04E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04E;->VIDEO_CHAINING_SESSION_ID:LX/04E;

    .line 12830
    new-instance v0, LX/04E;

    const-string v1, "VIDEO_CHAINING_SOURCE"

    const-string v2, "video_chaining_source"

    invoke-direct {v0, v1, v5, v2}, LX/04E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04E;->VIDEO_CHAINING_SOURCE:LX/04E;

    .line 12831
    const/4 v0, 0x3

    new-array v0, v0, [LX/04E;

    sget-object v1, LX/04E;->VIDEO_CHAINING_DEPTH_LEVEL:LX/04E;

    aput-object v1, v0, v3

    sget-object v1, LX/04E;->VIDEO_CHAINING_SESSION_ID:LX/04E;

    aput-object v1, v0, v4

    sget-object v1, LX/04E;->VIDEO_CHAINING_SOURCE:LX/04E;

    aput-object v1, v0, v5

    sput-object v0, LX/04E;->$VALUES:[LX/04E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12832
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12833
    iput-object p3, p0, LX/04E;->value:Ljava/lang/String;

    .line 12834
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/04E;
    .locals 1

    .prologue
    .line 12835
    const-class v0, LX/04E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/04E;

    return-object v0
.end method

.method public static values()[LX/04E;
    .locals 1

    .prologue
    .line 12836
    sget-object v0, LX/04E;->$VALUES:[LX/04E;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/04E;

    return-object v0
.end method
