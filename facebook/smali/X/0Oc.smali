.class public final LX/0Oc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0LX;


# instance fields
.field public final synthetic a:LX/0Od;

.field public final b:LX/0OP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0OP",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final c:Landroid/os/Looper;

.field private final d:LX/0GJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0GJ",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final e:LX/0ON;

.field public f:J


# direct methods
.method public constructor <init>(LX/0Od;LX/0OP;Landroid/os/Looper;LX/0GJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0OP",
            "<TT;>;",
            "Landroid/os/Looper;",
            "LX/0GJ",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 53427
    iput-object p1, p0, LX/0Oc;->a:LX/0Od;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53428
    iput-object p2, p0, LX/0Oc;->b:LX/0OP;

    .line 53429
    iput-object p3, p0, LX/0Oc;->c:Landroid/os/Looper;

    .line 53430
    iput-object p4, p0, LX/0Oc;->d:LX/0GJ;

    .line 53431
    new-instance v0, LX/0ON;

    const-string v1, "manifestLoader:single"

    invoke-direct {v0, v1}, LX/0ON;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0Oc;->e:LX/0ON;

    .line 53432
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 53415
    iget-object v0, p0, LX/0Oc;->e:LX/0ON;

    invoke-virtual {v0}, LX/0ON;->c()V

    .line 53416
    return-void
.end method


# virtual methods
.method public final a(LX/0LO;)V
    .locals 6

    .prologue
    .line 53417
    :try_start_0
    iget-object v0, p0, LX/0Oc;->b:LX/0OP;

    .line 53418
    iget-object v1, v0, LX/0OP;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 53419
    iget-object v1, p0, LX/0Oc;->a:LX/0Od;

    iget-wide v2, p0, LX/0Oc;->f:J

    .line 53420
    iput-object v0, v1, LX/0Od;->m:Ljava/lang/Object;

    .line 53421
    iput-wide v2, v1, LX/0Od;->n:J

    .line 53422
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, v1, LX/0Od;->o:J

    .line 53423
    iget-object v1, p0, LX/0Oc;->d:LX/0GJ;

    invoke-interface {v1, v0}, LX/0GJ;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53424
    invoke-direct {p0}, LX/0Oc;->b()V

    .line 53425
    return-void

    .line 53426
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/0Oc;->b()V

    throw v0
.end method

.method public final a(LX/0LO;Ljava/io/IOException;)V
    .locals 1

    .prologue
    .line 53411
    :try_start_0
    iget-object v0, p0, LX/0Oc;->d:LX/0GJ;

    invoke-interface {v0, p2}, LX/0GJ;->b(Ljava/io/IOException;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53412
    invoke-direct {p0}, LX/0Oc;->b()V

    .line 53413
    return-void

    .line 53414
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/0Oc;->b()V

    throw v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 53406
    :try_start_0
    new-instance v0, LX/0Ob;

    new-instance v1, Ljava/util/concurrent/CancellationException;

    invoke-direct {v1}, Ljava/util/concurrent/CancellationException;-><init>()V

    invoke-direct {v0, v1}, LX/0Ob;-><init>(Ljava/lang/Throwable;)V

    .line 53407
    iget-object v1, p0, LX/0Oc;->d:LX/0GJ;

    invoke-interface {v1, v0}, LX/0GJ;->b(Ljava/io/IOException;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53408
    invoke-direct {p0}, LX/0Oc;->b()V

    .line 53409
    return-void

    .line 53410
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/0Oc;->b()V

    throw v0
.end method
