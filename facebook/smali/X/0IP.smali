.class public LX/0IP;
.super LX/05I;
.source ""


# static fields
.field public static final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final i:Lcom/facebook/rti/push/service/FbnsService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38615
    new-instance v0, LX/0IO;

    invoke-direct {v0}, LX/0IO;-><init>()V

    sput-object v0, LX/0IP;->h:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/rti/push/service/FbnsService;LX/04v;LX/01o;)V
    .locals 6

    .prologue
    .line 38616
    const-string v0, "FBNS"

    move-object v4, v0

    .line 38617
    sget-object v5, LX/05J;->FBNS_LITE:LX/05J;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/05I;-><init>(Landroid/content/Context;LX/04v;LX/01o;Ljava/lang/String;LX/05J;)V

    .line 38618
    iput-object p1, p0, LX/0IP;->i:Lcom/facebook/rti/push/service/FbnsService;

    .line 38619
    return-void
.end method

.method public static a(LX/0IP;Landroid/content/Intent;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 38620
    const-string v1, "com.facebook.rti.fbns.intent.RECEIVE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 38621
    iget-object v1, p0, LX/05I;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 38622
    iget-object v1, p0, LX/05I;->a:Landroid/content/Context;

    invoke-static {v1, p2}, LX/0Hk;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/05I;->b:LX/04v;

    invoke-virtual {v1, p2}, LX/04v;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 38623
    :cond_0
    iget-object v0, p0, LX/05I;->b:LX/04v;

    invoke-virtual {v0, p1, p2}, LX/04v;->a(Landroid/content/Intent;Ljava/lang/String;)Z

    move-result v0

    .line 38624
    :cond_1
    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 38625
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/0IP;->a(LX/0IP;Landroid/content/Intent;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
