.class public LX/0G8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0G7;


# instance fields
.field private final a:LX/0OE;

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Z

.field public final d:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Z

.field private f:Landroid/net/Uri;


# direct methods
.method public constructor <init>(LX/0OE;Ljava/lang/String;ZLandroid/net/Uri;Z)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 34281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34282
    iput-object p1, p0, LX/0G8;->a:LX/0OE;

    .line 34283
    iput-object p2, p0, LX/0G8;->b:Ljava/lang/String;

    .line 34284
    iput-boolean p3, p0, LX/0G8;->c:Z

    .line 34285
    iput-object p4, p0, LX/0G8;->d:Landroid/net/Uri;

    .line 34286
    iput-boolean p5, p0, LX/0G8;->e:Z

    .line 34287
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 1

    .prologue
    .line 34288
    iget-object v0, p0, LX/0G8;->a:LX/0OE;

    invoke-virtual {v0, p1, p2, p3}, LX/0OE;->a([BII)I

    move-result v0

    return v0
.end method

.method public final a(LX/0OA;)J
    .locals 12

    .prologue
    .line 34289
    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    iput-object v0, p0, LX/0G8;->f:Landroid/net/Uri;

    .line 34290
    iget-object v0, p0, LX/0G8;->d:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 34291
    iget-object v0, p0, LX/0G8;->a:LX/0OE;

    invoke-virtual {v0, p1}, LX/0OE;->a(LX/0OA;)J

    move-result-wide v0

    .line 34292
    :goto_0
    return-wide v0

    .line 34293
    :cond_0
    new-instance v1, LX/0OA;

    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    iget-object v2, p0, LX/0G8;->b:Ljava/lang/String;

    .line 34294
    if-nez v0, :cond_2

    .line 34295
    const/4 v0, 0x0

    .line 34296
    :cond_1
    :goto_1
    move-object v2, v0

    .line 34297
    iget-object v3, p1, LX/0OA;->b:[B

    iget-wide v4, p1, LX/0OA;->c:J

    iget-wide v6, p1, LX/0OA;->d:J

    iget-wide v8, p1, LX/0OA;->e:J

    iget-object v10, p1, LX/0OA;->f:Ljava/lang/String;

    iget v11, p1, LX/0OA;->g:I

    invoke-direct/range {v1 .. v11}, LX/0OA;-><init>(Landroid/net/Uri;[BJJJLjava/lang/String;I)V

    .line 34298
    iget-object v0, p0, LX/0G8;->a:LX/0OE;

    invoke-virtual {v0, v1}, LX/0OE;->a(LX/0OA;)J

    move-result-wide v0

    goto :goto_0

    .line 34299
    :cond_2
    iget-object v3, p0, LX/0G8;->d:Landroid/net/Uri;

    if-eqz v3, :cond_1

    .line 34300
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    const-string v3, "127.0.0.1"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 34301
    :cond_3
    iget-object v3, p0, LX/0G8;->d:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "remote-uri"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "vid"

    invoke-virtual {v3, v4, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 34302
    iget-boolean v4, p0, LX/0G8;->e:Z

    if-eqz v4, :cond_4

    .line 34303
    const-string v4, "is-live"

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 34304
    :cond_4
    iget-boolean v4, p0, LX/0G8;->c:Z

    if-nez v4, :cond_5

    .line 34305
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 34306
    const-string v5, "disable-cache"

    invoke-virtual {v4}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 34307
    :cond_5
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 34308
    iget-object v0, p0, LX/0G8;->a:LX/0OE;

    invoke-virtual {v0}, LX/0OE;->a()V

    .line 34309
    const/4 v0, 0x0

    iput-object v0, p0, LX/0G8;->f:Landroid/net/Uri;

    .line 34310
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 34311
    iget-object v0, p0, LX/0G8;->f:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0G8;->f:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
