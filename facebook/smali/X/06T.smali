.class public LX/06T;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field private final c:Landroid/content/Context;

.field public final d:Landroid/app/AlarmManager;

.field public final e:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field public final f:I

.field private final g:Landroid/os/Handler;

.field private final h:Landroid/content/BroadcastReceiver;

.field private final i:LX/05H;

.field public final j:Landroid/app/PendingIntent;

.field public volatile k:Ljava/lang/Runnable;

.field private l:Z

.field public m:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17941
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/06T;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ACTION_ALARM."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/06T;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Landroid/app/AlarmManager;Landroid/os/Handler;LX/05H;)V
    .locals 4

    .prologue
    .line 17924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17925
    iput-object p1, p0, LX/06T;->c:Landroid/content/Context;

    .line 17926
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, LX/06T;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 17927
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 17928
    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 17929
    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17930
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/06T;->b:Ljava/lang/String;

    .line 17931
    iput-object p4, p0, LX/06T;->d:Landroid/app/AlarmManager;

    .line 17932
    iput-object p3, p0, LX/06T;->e:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 17933
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, LX/06T;->f:I

    .line 17934
    iput-object p5, p0, LX/06T;->g:Landroid/os/Handler;

    .line 17935
    iput-object p6, p0, LX/06T;->i:LX/05H;

    .line 17936
    new-instance v0, LX/06U;

    invoke-direct {v0, p0}, LX/06U;-><init>(LX/06T;)V

    iput-object v0, p0, LX/06T;->h:Landroid/content/BroadcastReceiver;

    .line 17937
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/06T;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 17938
    iget-object v1, p0, LX/06T;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 17939
    iget-object v1, p0, LX/06T;->c:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, LX/06T;->j:Landroid/app/PendingIntent;

    .line 17940
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 17916
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/06T;->d()V

    .line 17917
    iget-boolean v0, p0, LX/06T;->l:Z

    if-eqz v0, :cond_0

    .line 17918
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/06T;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17919
    :try_start_1
    iget-object v0, p0, LX/06T;->c:Landroid/content/Context;

    iget-object v1, p0, LX/06T;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17920
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 17921
    :catch_0
    move-exception v0

    .line 17922
    :try_start_2
    const-string v1, "PingUnreceivedAlarm"

    const-string v2, "Failed to unregister broadcast receiver"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 17923
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    .line 17942
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06T;->k:Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 17943
    :goto_0
    monitor-exit p0

    return-void

    .line 17944
    :cond_0
    :try_start_1
    iput-object p1, p0, LX/06T;->k:Ljava/lang/Runnable;

    .line 17945
    iget-object v0, p0, LX/06T;->c:Landroid/content/Context;

    iget-object v1, p0, LX/06T;->h:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    iget-object v3, p0, LX/06T;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    iget-object v4, p0, LX/06T;->g:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 17946
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/06T;->l:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 17947
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 17912
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/06T;->m:Z

    if-nez v0, :cond_0

    .line 17913
    invoke-virtual {p0}, LX/06T;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17914
    :cond_0
    monitor-exit p0

    return-void

    .line 17915
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 13

    .prologue
    .line 17897
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/06T;->m:Z

    if-nez v0, :cond_0

    .line 17898
    iget-object v0, p0, LX/06T;->i:LX/05H;

    invoke-interface {v0}, LX/05H;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x3c

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    const-wide/16 v10, 0x3e8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 17899
    iput-boolean v9, p0, LX/06T;->m:Z

    .line 17900
    iget-object v2, p0, LX/06T;->e:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    .line 17901
    add-long/2addr v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17902
    :try_start_1
    iget v4, p0, LX/06T;->f:I

    const/16 v5, 0x13

    if-lt v4, v5, :cond_1

    .line 17903
    iget-object v4, p0, LX/06T;->d:Landroid/app/AlarmManager;

    const/4 v5, 0x2

    iget-object v6, p0, LX/06T;->j:Landroid/app/PendingIntent;

    .line 17904
    invoke-virtual {v4, v5, v2, v3, v6}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 17905
    :goto_0
    const-string v2, "PingUnreceivedAlarm"

    const-string v3, "start; intervalSec=%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-wide/16 v6, 0x3e8

    div-long v6, v0, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17906
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 17907
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 17908
    :cond_1
    :try_start_2
    iget-object v4, p0, LX/06T;->d:Landroid/app/AlarmManager;

    const/4 v5, 0x2

    iget-object v6, p0, LX/06T;->j:Landroid/app/PendingIntent;

    invoke-virtual {v4, v5, v2, v3, v6}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 17909
    :catch_0
    move-exception v2

    .line 17910
    iput-boolean v8, p0, LX/06T;->m:Z

    .line 17911
    const-string v3, "PingUnreceivedAlarm"

    const-string v4, "alarm_failed; intervalSec=%s"

    new-array v5, v9, [Ljava/lang/Object;

    div-long v6, v0, v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3, v2, v4, v5}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final declared-synchronized d()V
    .locals 3

    .prologue
    .line 17891
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/06T;->m:Z

    if-eqz v0, :cond_0

    .line 17892
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/06T;->m:Z

    .line 17893
    const-string v0, "PingUnreceivedAlarm"

    const-string v1, "stop"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17894
    iget-object v0, p0, LX/06T;->d:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/06T;->j:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17895
    :cond_0
    monitor-exit p0

    return-void

    .line 17896
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
