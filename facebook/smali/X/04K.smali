.class public LX/04K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/04K;


# instance fields
.field public a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public b:Z

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 13025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13026
    iput-object p1, p0, LX/04K;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 13027
    return-void
.end method

.method public static a(LX/0QB;)LX/04K;
    .locals 3

    .prologue
    .line 13028
    sget-object v0, LX/04K;->e:LX/04K;

    if-nez v0, :cond_1

    .line 13029
    const-class v1, LX/04K;

    monitor-enter v1

    .line 13030
    :try_start_0
    sget-object v0, LX/04K;->e:LX/04K;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 13031
    if-eqz v2, :cond_0

    .line 13032
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/04K;->b(LX/0QB;)LX/04K;

    move-result-object v0

    sput-object v0, LX/04K;->e:LX/04K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 13033
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 13034
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 13035
    :cond_1
    sget-object v0, LX/04K;->e:LX/04K;

    return-object v0

    .line 13036
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 13037
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/04K;
    .locals 2

    .prologue
    .line 13038
    new-instance v1, LX/04K;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {v1, v0}, LX/04K;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 13039
    return-object v1
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 13040
    iget-boolean v0, p0, LX/04K;->b:Z

    if-nez v0, :cond_0

    .line 13041
    iget-object v0, p0, LX/04K;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1d000a

    const/16 v2, 0xaa

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 13042
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/04K;->b:Z

    .line 13043
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 13044
    iget-boolean v0, p0, LX/04K;->c:Z

    if-nez v0, :cond_0

    .line 13045
    iget-object v0, p0, LX/04K;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1d000a

    const/16 v2, 0xab

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 13046
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/04K;->c:Z

    .line 13047
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 13048
    iget-boolean v0, p0, LX/04K;->d:Z

    if-nez v0, :cond_0

    .line 13049
    iget-object v0, p0, LX/04K;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1d000a

    const/16 v2, 0x74

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 13050
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/04K;->d:Z

    .line 13051
    :cond_0
    return-void
.end method
