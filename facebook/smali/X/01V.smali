.class public final enum LX/01V;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/01V;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/01V;

.field public static final enum ACRA_CRASH_REPORT:LX/01V;

.field public static final enum ANR_REPORT:LX/01V;

.field public static final enum CACHED_ANR_REPORT:LX/01V;

.field public static final enum CPUSPIN_REPORT:LX/01V;

.field public static final enum NATIVE_CRASH_REPORT:LX/01V;


# instance fields
.field public final attachmentField:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final defaultMaxSize:J

.field private final directory:Ljava/lang/String;

.field private final fileExtensions:[Ljava/lang/String;

.field private final handler:LX/01X;

.field private final mLock:Ljava/lang/Object;

.field private mSpool:LX/01e;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    .line 4804
    new-instance v0, LX/01V;

    const-string v1, "ACRA_CRASH_REPORT"

    const/4 v2, 0x0

    const-string v3, "acra-reports"

    const-wide/32 v4, 0x100000

    const/4 v6, 0x0

    new-instance v7, LX/01W;

    invoke-direct {v7}, LX/01W;-><init>()V

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, ".stacktrace"

    aput-object v10, v8, v9

    invoke-direct/range {v0 .. v8}, LX/01V;-><init>(Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;LX/01X;[Ljava/lang/String;)V

    sput-object v0, LX/01V;->ACRA_CRASH_REPORT:LX/01V;

    .line 4805
    new-instance v0, LX/01V;

    const-string v1, "NATIVE_CRASH_REPORT"

    const/4 v2, 0x1

    const-string v3, "minidumps"

    const-wide/32 v4, 0x800000

    const-string v6, "MINIDUMP"

    const/4 v7, 0x0

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, ".dmp"

    aput-object v10, v8, v9

    invoke-direct/range {v0 .. v8}, LX/01V;-><init>(Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;LX/01X;[Ljava/lang/String;)V

    sput-object v0, LX/01V;->NATIVE_CRASH_REPORT:LX/01V;

    .line 4806
    new-instance v0, LX/01V;

    const-string v1, "ANR_REPORT"

    const/4 v2, 0x2

    const-string v3, "traces"

    const-wide/32 v4, 0x80000

    const-string v6, "SIGQUIT"

    const/4 v7, 0x0

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, ".stacktrace"

    aput-object v10, v8, v9

    invoke-direct/range {v0 .. v8}, LX/01V;-><init>(Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;LX/01X;[Ljava/lang/String;)V

    sput-object v0, LX/01V;->ANR_REPORT:LX/01V;

    .line 4807
    new-instance v0, LX/01V;

    const-string v1, "CPUSPIN_REPORT"

    const/4 v2, 0x3

    const-string v3, "traces_cpuspin"

    const-wide/32 v4, 0x80000

    const-string v6, "SIGQUIT"

    const/4 v7, 0x0

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, ".stacktrace"

    aput-object v10, v8, v9

    invoke-direct/range {v0 .. v8}, LX/01V;-><init>(Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;LX/01X;[Ljava/lang/String;)V

    sput-object v0, LX/01V;->CPUSPIN_REPORT:LX/01V;

    .line 4808
    new-instance v0, LX/01V;

    const-string v1, "CACHED_ANR_REPORT"

    const/4 v2, 0x4

    const-string v3, "traces"

    const-wide/32 v4, 0x80000

    const-string v6, "SIGQUIT"

    new-instance v7, LX/01Y;

    invoke-direct {v7}, LX/01Y;-><init>()V

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, ".cachedreport"

    aput-object v10, v8, v9

    invoke-direct/range {v0 .. v8}, LX/01V;-><init>(Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;LX/01X;[Ljava/lang/String;)V

    sput-object v0, LX/01V;->CACHED_ANR_REPORT:LX/01V;

    .line 4809
    const/4 v0, 0x5

    new-array v0, v0, [LX/01V;

    const/4 v1, 0x0

    sget-object v2, LX/01V;->ACRA_CRASH_REPORT:LX/01V;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/01V;->NATIVE_CRASH_REPORT:LX/01V;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/01V;->ANR_REPORT:LX/01V;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/01V;->CPUSPIN_REPORT:LX/01V;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/01V;->CACHED_ANR_REPORT:LX/01V;

    aput-object v2, v0, v1

    sput-object v0, LX/01V;->$VALUES:[LX/01V;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;LX/01X;[Ljava/lang/String;)V
    .locals 2
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "LX/01X;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 4810
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 4811
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/01V;->mLock:Ljava/lang/Object;

    .line 4812
    iput-object p3, p0, LX/01V;->directory:Ljava/lang/String;

    .line 4813
    iput-wide p4, p0, LX/01V;->defaultMaxSize:J

    .line 4814
    iput-object p6, p0, LX/01V;->attachmentField:Ljava/lang/String;

    .line 4815
    iput-object p7, p0, LX/01V;->handler:LX/01X;

    .line 4816
    iput-object p8, p0, LX/01V;->fileExtensions:[Ljava/lang/String;

    .line 4817
    return-void
.end method

.method public static synthetic access$1300(LX/01V;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 4818
    iget-object v0, p0, LX/01V;->directory:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic access$1400(LX/01V;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 4819
    iget-object v0, p0, LX/01V;->attachmentField:Ljava/lang/String;

    return-object v0
.end method

.method public static getCrashReports(LX/01V;Landroid/content/Context;)LX/01h;
    .locals 14

    .prologue
    .line 4820
    iget-object v0, p0, LX/01V;->fileExtensions:[Ljava/lang/String;

    .line 4821
    new-instance v1, LX/01d;

    invoke-direct {v1, p0, v0}, LX/01d;-><init>(LX/01V;[Ljava/lang/String;)V

    .line 4822
    invoke-virtual {p0, p1}, LX/01V;->getSpool(Landroid/content/Context;)LX/01e;

    move-result-object v0

    new-instance v2, LX/01f;

    invoke-direct {v2}, LX/01f;-><init>()V

    const/4 v5, 0x0

    .line 4823
    iget-object v4, v0, LX/01e;->mDirectoryName:Ljava/io/File;

    invoke-virtual {v4, v1}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v4

    .line 4824
    if-nez v4, :cond_0

    .line 4825
    new-array v4, v5, [Ljava/lang/String;

    .line 4826
    :cond_0
    array-length v6, v4

    .line 4827
    new-array v7, v6, [LX/01g;

    .line 4828
    :goto_0
    if-ge v5, v6, :cond_1

    .line 4829
    aget-object v8, v4, v5

    .line 4830
    new-instance v9, Ljava/io/File;

    iget-object v10, v0, LX/01e;->mDirectoryName:Ljava/io/File;

    invoke-direct {v9, v10, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 4831
    new-instance v10, LX/01g;

    invoke-virtual {v9}, Ljava/io/File;->lastModified()J

    move-result-wide v12

    invoke-direct {v10, v8, v12, v13, v9}, LX/01g;-><init>(Ljava/lang/String;JLjava/io/File;)V

    aput-object v10, v7, v5

    .line 4832
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 4833
    :cond_1
    if-eqz v2, :cond_2

    .line 4834
    invoke-static {v7, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 4835
    :cond_2
    new-instance v4, LX/01h;

    invoke-direct {v4, v0, v7}, LX/01h;-><init>(LX/01e;[LX/01g;)V

    move-object v0, v4

    .line 4836
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/01V;
    .locals 1

    .prologue
    .line 4837
    const-class v0, LX/01V;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/01V;

    return-object v0
.end method

.method public static values()[LX/01V;
    .locals 1

    .prologue
    .line 4838
    sget-object v0, LX/01V;->$VALUES:[LX/01V;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/01V;

    return-object v0
.end method


# virtual methods
.method public final getHandler()LX/01X;
    .locals 1

    .prologue
    .line 4839
    iget-object v0, p0, LX/01V;->handler:LX/01X;

    return-object v0
.end method

.method public final getSpool(Landroid/content/Context;)LX/01e;
    .locals 4

    .prologue
    .line 4840
    iget-object v1, p0, LX/01V;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 4841
    :try_start_0
    iget-object v0, p0, LX/01V;->mSpool:LX/01e;

    if-nez v0, :cond_0

    .line 4842
    new-instance v0, LX/01e;

    iget-object v2, p0, LX/01V;->directory:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    invoke-direct {v0, v2}, LX/01e;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/01V;->mSpool:LX/01e;

    .line 4843
    :cond_0
    iget-object v0, p0, LX/01V;->mSpool:LX/01e;

    monitor-exit v1

    return-object v0

    .line 4844
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
