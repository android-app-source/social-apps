.class public final enum LX/0Hi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Hi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Hi;

.field public static final enum ExecutionException:LX/0Hi;

.field public static final enum SecurityException:LX/0Hi;

.field public static final enum Success:LX/0Hi;

.field public static final enum TimedOut:LX/0Hi;

.field public static final enum UnknownHost:LX/0Hi;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37985
    new-instance v0, LX/0Hi;

    const-string v1, "Success"

    invoke-direct {v0, v1, v2}, LX/0Hi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hi;->Success:LX/0Hi;

    .line 37986
    new-instance v0, LX/0Hi;

    const-string v1, "TimedOut"

    invoke-direct {v0, v1, v3}, LX/0Hi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hi;->TimedOut:LX/0Hi;

    .line 37987
    new-instance v0, LX/0Hi;

    const-string v1, "UnknownHost"

    invoke-direct {v0, v1, v4}, LX/0Hi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hi;->UnknownHost:LX/0Hi;

    .line 37988
    new-instance v0, LX/0Hi;

    const-string v1, "SecurityException"

    invoke-direct {v0, v1, v5}, LX/0Hi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hi;->SecurityException:LX/0Hi;

    .line 37989
    new-instance v0, LX/0Hi;

    const-string v1, "ExecutionException"

    invoke-direct {v0, v1, v6}, LX/0Hi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hi;->ExecutionException:LX/0Hi;

    .line 37990
    const/4 v0, 0x5

    new-array v0, v0, [LX/0Hi;

    sget-object v1, LX/0Hi;->Success:LX/0Hi;

    aput-object v1, v0, v2

    sget-object v1, LX/0Hi;->TimedOut:LX/0Hi;

    aput-object v1, v0, v3

    sget-object v1, LX/0Hi;->UnknownHost:LX/0Hi;

    aput-object v1, v0, v4

    sget-object v1, LX/0Hi;->SecurityException:LX/0Hi;

    aput-object v1, v0, v5

    sget-object v1, LX/0Hi;->ExecutionException:LX/0Hi;

    aput-object v1, v0, v6

    sput-object v0, LX/0Hi;->$VALUES:[LX/0Hi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37991
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Hi;
    .locals 1

    .prologue
    .line 37992
    const-class v0, LX/0Hi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Hi;

    return-object v0
.end method

.method public static values()[LX/0Hi;
    .locals 1

    .prologue
    .line 37993
    sget-object v0, LX/0Hi;->$VALUES:[LX/0Hi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Hi;

    return-object v0
.end method
