.class public final LX/0NI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0N4;

.field public final b:LX/0NJ;

.field public final c:LX/0Oi;

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:I

.field public h:J


# direct methods
.method public constructor <init>(LX/0N4;LX/0NJ;)V
    .locals 2

    .prologue
    .line 49816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49817
    iput-object p1, p0, LX/0NI;->a:LX/0N4;

    .line 49818
    iput-object p2, p0, LX/0NI;->b:LX/0NJ;

    .line 49819
    new-instance v0, LX/0Oi;

    const/16 v1, 0x40

    new-array v1, v1, [B

    invoke-direct {v0, v1}, LX/0Oi;-><init>([B)V

    iput-object v0, p0, LX/0NI;->c:LX/0Oi;

    .line 49820
    return-void
.end method


# virtual methods
.method public final a(LX/0Oj;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 49821
    iget-object v0, p0, LX/0NI;->c:LX/0Oi;

    iget-object v0, v0, LX/0Oi;->a:[B

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v2, v1}, LX/0Oj;->a([BII)V

    .line 49822
    iget-object v0, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v0, v2}, LX/0Oi;->a(I)V

    .line 49823
    const/16 v3, 0x8

    .line 49824
    iget-object v0, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v0, v3}, LX/0Oi;->b(I)V

    .line 49825
    iget-object v0, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v0}, LX/0Oi;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/0NI;->d:Z

    .line 49826
    iget-object v0, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v0}, LX/0Oi;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/0NI;->e:Z

    .line 49827
    iget-object v0, p0, LX/0NI;->c:LX/0Oi;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, LX/0Oi;->b(I)V

    .line 49828
    iget-object v0, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v0, v3}, LX/0Oi;->c(I)I

    move-result v0

    iput v0, p0, LX/0NI;->g:I

    .line 49829
    iget-object v0, p0, LX/0NI;->c:LX/0Oi;

    iget-object v0, v0, LX/0Oi;->a:[B

    iget v1, p0, LX/0NI;->g:I

    invoke-virtual {p1, v0, v2, v1}, LX/0Oj;->a([BII)V

    .line 49830
    iget-object v0, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v0, v2}, LX/0Oi;->a(I)V

    .line 49831
    const/16 v12, 0x1e

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/16 v11, 0xf

    const/4 v10, 0x1

    .line 49832
    const-wide/16 v4, 0x0

    iput-wide v4, p0, LX/0NI;->h:J

    .line 49833
    iget-boolean v4, p0, LX/0NI;->d:Z

    if-eqz v4, :cond_1

    .line 49834
    iget-object v4, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v4, v9}, LX/0Oi;->b(I)V

    .line 49835
    iget-object v4, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v4, v8}, LX/0Oi;->c(I)I

    move-result v4

    int-to-long v4, v4

    shl-long/2addr v4, v12

    .line 49836
    iget-object v6, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v6, v10}, LX/0Oi;->b(I)V

    .line 49837
    iget-object v6, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v6, v11}, LX/0Oi;->c(I)I

    move-result v6

    shl-int/lit8 v6, v6, 0xf

    int-to-long v6, v6

    or-long/2addr v4, v6

    .line 49838
    iget-object v6, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v6, v10}, LX/0Oi;->b(I)V

    .line 49839
    iget-object v6, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v6, v11}, LX/0Oi;->c(I)I

    move-result v6

    int-to-long v6, v6

    or-long/2addr v4, v6

    .line 49840
    iget-object v6, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v6, v10}, LX/0Oi;->b(I)V

    .line 49841
    iget-boolean v6, p0, LX/0NI;->f:Z

    if-nez v6, :cond_0

    iget-boolean v6, p0, LX/0NI;->e:Z

    if-eqz v6, :cond_0

    .line 49842
    iget-object v6, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v6, v9}, LX/0Oi;->b(I)V

    .line 49843
    iget-object v6, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v6, v8}, LX/0Oi;->c(I)I

    move-result v6

    int-to-long v6, v6

    shl-long/2addr v6, v12

    .line 49844
    iget-object v8, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v8, v10}, LX/0Oi;->b(I)V

    .line 49845
    iget-object v8, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v8, v11}, LX/0Oi;->c(I)I

    move-result v8

    shl-int/lit8 v8, v8, 0xf

    int-to-long v8, v8

    or-long/2addr v6, v8

    .line 49846
    iget-object v8, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v8, v10}, LX/0Oi;->b(I)V

    .line 49847
    iget-object v8, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v8, v11}, LX/0Oi;->c(I)I

    move-result v8

    int-to-long v8, v8

    or-long/2addr v6, v8

    .line 49848
    iget-object v8, p0, LX/0NI;->c:LX/0Oi;

    invoke-virtual {v8, v10}, LX/0Oi;->b(I)V

    .line 49849
    iget-object v8, p0, LX/0NI;->b:LX/0NJ;

    invoke-virtual {v8, v6, v7}, LX/0NJ;->a(J)J

    .line 49850
    iput-boolean v10, p0, LX/0NI;->f:Z

    .line 49851
    :cond_0
    iget-object v6, p0, LX/0NI;->b:LX/0NJ;

    invoke-virtual {v6, v4, v5}, LX/0NJ;->a(J)J

    move-result-wide v4

    iput-wide v4, p0, LX/0NI;->h:J

    .line 49852
    :cond_1
    iget-object v0, p0, LX/0NI;->a:LX/0N4;

    iget-wide v2, p0, LX/0NI;->h:J

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v3, v1}, LX/0N4;->a(JZ)V

    .line 49853
    iget-object v0, p0, LX/0NI;->a:LX/0N4;

    invoke-virtual {v0, p1}, LX/0N4;->a(LX/0Oj;)V

    .line 49854
    iget-object v0, p0, LX/0NI;->a:LX/0N4;

    invoke-virtual {v0}, LX/0N4;->b()V

    .line 49855
    return-void
.end method
