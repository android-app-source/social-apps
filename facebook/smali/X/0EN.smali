.class public final LX/0EN;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Landroid/widget/ListAdapter;


# instance fields
.field public final synthetic a:LX/0EO;


# direct methods
.method public constructor <init>(LX/0EO;)V
    .locals 0

    .prologue
    .line 31716
    iput-object p1, p0, LX/0EN;->a:LX/0EO;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method private a(LX/0EK;)Z
    .locals 2

    .prologue
    .line 31715
    iget-object v0, p0, LX/0EN;->a:LX/0EO;

    iget-object v0, v0, LX/0EO;->b:Ljava/util/ArrayList;

    invoke-virtual {p0}, LX/0EN;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 31717
    iget-object v0, p0, LX/0EN;->a:LX/0EO;

    iget-object v0, v0, LX/0EO;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31714
    iget-object v0, p0, LX/0EN;->a:LX/0EO;

    iget-object v0, v0, LX/0EO;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 31693
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 31694
    invoke-virtual {p0, p1}, LX/0EN;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0EK;

    .line 31695
    iget-object v1, v0, LX/0EK;->b:Ljava/lang/String;

    move-object v4, v1

    .line 31696
    const/4 v1, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 31697
    instance-of v1, p2, Lcom/facebook/browser/lite/widget/MenuItemTextView;

    if-nez v1, :cond_5

    .line 31698
    iget-object v1, p0, LX/0EN;->a:LX/0EO;

    iget-object v1, v1, LX/0EO;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v4, 0x7f0301ed

    invoke-virtual {v1, v4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/browser/lite/widget/MenuItemTextView;

    .line 31699
    :goto_1
    iget-object v4, p0, LX/0EN;->a:LX/0EO;

    iget-object v4, v4, LX/0EO;->c:LX/0Cf;

    invoke-direct {p0, v0}, LX/0EN;->a(LX/0EK;)Z

    move-result v5

    if-nez v5, :cond_6

    :goto_2
    invoke-virtual {v1, v0, v4, v2}, Lcom/facebook/browser/lite/widget/MenuItemTextView;->a(LX/0EK;LX/0Cf;Z)V

    .line 31700
    :goto_3
    return-object v1

    .line 31701
    :sswitch_0
    const-string v5, "navigation"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v3

    goto :goto_0

    :sswitch_1
    const-string v5, "zoom"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    goto :goto_0

    .line 31702
    :pswitch_0
    instance-of v1, p2, Lcom/facebook/browser/lite/widget/MenuItemNavigationView;

    if-nez v1, :cond_1

    .line 31703
    iget-object v1, p0, LX/0EN;->a:LX/0EO;

    iget-object v1, v1, LX/0EO;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v4, 0x7f0301ee

    invoke-virtual {v1, v4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/browser/lite/widget/MenuItemNavigationView;

    .line 31704
    :goto_4
    iget-object v4, p0, LX/0EN;->a:LX/0EO;

    iget-object v4, v4, LX/0EO;->c:LX/0Cf;

    invoke-direct {p0, v0}, LX/0EN;->a(LX/0EK;)Z

    move-result v5

    if-nez v5, :cond_2

    :goto_5
    invoke-virtual {v1, v0, v4, v2}, Lcom/facebook/browser/lite/widget/MenuItemNavigationView;->a(LX/0EK;LX/0Cf;Z)V

    goto :goto_3

    .line 31705
    :cond_1
    check-cast p2, Lcom/facebook/browser/lite/widget/MenuItemNavigationView;

    move-object v1, p2

    goto :goto_4

    :cond_2
    move v2, v3

    .line 31706
    goto :goto_5

    .line 31707
    :pswitch_1
    instance-of v1, p2, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;

    if-nez v1, :cond_3

    .line 31708
    iget-object v1, p0, LX/0EN;->a:LX/0EO;

    iget-object v1, v1, LX/0EO;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v4, 0x7f0301ef

    invoke-virtual {v1, v4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;

    .line 31709
    :goto_6
    iget-object v4, p0, LX/0EN;->a:LX/0EO;

    iget-object v4, v4, LX/0EO;->c:LX/0Cf;

    iget-object v5, p0, LX/0EN;->a:LX/0EO;

    iget-object v5, v5, LX/0EO;->d:LX/0EL;

    invoke-direct {p0, v0}, LX/0EN;->a(LX/0EK;)Z

    move-result v6

    if-nez v6, :cond_4

    :goto_7
    invoke-virtual {v1, v0, v4, v5, v2}, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->a(LX/0EK;LX/0Cf;LX/0EL;Z)V

    goto :goto_3

    .line 31710
    :cond_3
    check-cast p2, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;

    move-object v1, p2

    goto :goto_6

    :cond_4
    move v2, v3

    .line 31711
    goto :goto_7

    .line 31712
    :cond_5
    check-cast p2, Lcom/facebook/browser/lite/widget/MenuItemTextView;

    move-object v1, p2

    goto :goto_1

    :cond_6
    move v2, v3

    .line 31713
    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x3923d3 -> :sswitch_1
        0x6f060a14 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
