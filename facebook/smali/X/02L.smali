.class public LX/02L;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;I)V
    .locals 7

    .prologue
    const/16 v1, 0x3d

    const/4 v0, 0x1

    .line 7364
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    .line 7365
    const/16 v3, 0x3c

    int-to-long v4, v2

    invoke-static {v0, v3, p1, v4, v5}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    move-result v3

    .line 7366
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7367
    int-to-long v4, v2

    move v2, p1

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 7368
    return-void

    .line 7369
    :catchall_0
    move-exception v6

    int-to-long v4, v2

    move v2, p1

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    throw v6
.end method

.method public static a(Ljava/lang/Object;JI)V
    .locals 7

    .prologue
    const/16 v1, 0x3d

    const/4 v0, 0x1

    .line 7373
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    .line 7374
    const/16 v3, 0x3c

    int-to-long v4, v2

    invoke-static {v0, v3, p3, v4, v5}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    move-result v3

    .line 7375
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7376
    int-to-long v4, v2

    move v2, p3

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 7377
    return-void

    .line 7378
    :catchall_0
    move-exception v6

    int-to-long v4, v2

    move v2, p3

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    throw v6
.end method

.method public static a(Ljava/lang/Object;JII)V
    .locals 7

    .prologue
    const/16 v1, 0x3d

    const/4 v0, 0x1

    .line 7379
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    .line 7380
    const/16 v3, 0x3c

    int-to-long v4, v2

    invoke-static {v0, v3, p4, v4, v5}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    move-result v3

    .line 7381
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Ljava/lang/Object;->wait(JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7382
    int-to-long v4, v2

    move v2, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 7383
    return-void

    .line 7384
    :catchall_0
    move-exception v6

    int-to-long v4, v2

    move v2, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    throw v6
.end method

.method public static b(Ljava/lang/Object;I)V
    .locals 4

    .prologue
    .line 7370
    const/4 v0, 0x1

    const/16 v1, 0x3e

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v1, p1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 7371
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 7372
    return-void
.end method

.method public static c(Ljava/lang/Object;I)V
    .locals 4

    .prologue
    .line 7361
    const/4 v0, 0x1

    const/16 v1, 0x3e

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v1, p1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 7362
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 7363
    return-void
.end method
