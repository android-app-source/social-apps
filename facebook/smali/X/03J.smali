.class public LX/03J;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/03G;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9944
    sget-object v0, LX/03K;->a:LX/03K;

    move-object v0, v0

    .line 9945
    sput-object v0, LX/03J;->a:LX/03G;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 9990
    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 9989
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/03G;)V
    .locals 1

    .prologue
    .line 9985
    if-nez p0, :cond_0

    .line 9986
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 9987
    :cond_0
    sput-object p0, LX/03J;->a:LX/03G;

    .line 9988
    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 9982
    sget-object v0, LX/03J;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9983
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-static {p0}, LX/03J;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9984
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 9979
    sget-object v0, LX/03J;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9980
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-static {p0}, LX/03J;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9981
    :cond_0
    return-void
.end method

.method public static varargs a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 9976
    sget-object v0, LX/03J;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9977
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-static {p0}, LX/03J;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2}, LX/03J;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9978
    :cond_0
    return-void
.end method

.method public static varargs a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Throwable;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 9971
    const/4 v0, 0x5

    invoke-static {v0}, LX/03J;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9972
    invoke-static {p2, p3}, LX/03J;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 9973
    sget-object p2, LX/03J;->a:LX/03G;

    const/4 p3, 0x5

    invoke-interface {p2, p3}, LX/03G;->b(I)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 9974
    sget-object p2, LX/03J;->a:LX/03G;

    invoke-static {p0}, LX/03J;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p3

    invoke-interface {p2, p3, v0, p1}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9975
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 9968
    sget-object v0, LX/03J;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9969
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-interface {v0, p0, p1}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9970
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 9965
    sget-object v0, LX/03J;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9966
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-interface {v0, p0, p1, p2}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9967
    :cond_0
    return-void
.end method

.method public static varargs a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 9962
    sget-object v0, LX/03J;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9963
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-static {p1, p2}, LX/03J;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p0, v1}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9964
    :cond_0
    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 9961
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-interface {v0, p0}, LX/03G;->b(I)Z

    move-result v0

    return v0
.end method

.method public static b(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 9958
    sget-object v0, LX/03J;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9959
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-static {p0}, LX/03J;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9960
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 9955
    sget-object v0, LX/03J;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9956
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-interface {v0, p0, p1}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 9957
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 9952
    sget-object v0, LX/03J;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9953
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-interface {v0, p0, p1, p2}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9954
    :cond_0
    return-void
.end method

.method public static varargs b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 9949
    sget-object v0, LX/03J;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9950
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-static {p1, p2}, LX/03J;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p0, v1}, LX/03G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 9951
    :cond_0
    return-void
.end method

.method public static varargs b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 9946
    sget-object v0, LX/03J;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9947
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-static {p2, p3}, LX/03J;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p0, v1, p1}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9948
    :cond_0
    return-void
.end method
