.class public LX/03e;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/03e;


# instance fields
.field private final a:LX/0Uh;

.field public volatile b:Z

.field public volatile c:Z


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 10587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10588
    iput-object p1, p0, LX/03e;->a:LX/0Uh;

    .line 10589
    return-void
.end method

.method public static a(LX/0QB;)LX/03e;
    .locals 3

    .prologue
    .line 10605
    sget-object v0, LX/03e;->d:LX/03e;

    if-nez v0, :cond_1

    .line 10606
    const-class v1, LX/03e;

    monitor-enter v1

    .line 10607
    :try_start_0
    sget-object v0, LX/03e;->d:LX/03e;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 10608
    if-eqz v2, :cond_0

    .line 10609
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/03e;->b(LX/0QB;)LX/03e;

    move-result-object v0

    sput-object v0, LX/03e;->d:LX/03e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 10610
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 10611
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 10612
    :cond_1
    sget-object v0, LX/03e;->d:LX/03e;

    return-object v0

    .line 10613
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 10614
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/03e;
    .locals 2

    .prologue
    .line 10603
    new-instance v1, LX/03e;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v0}, LX/03e;-><init>(LX/0Uh;)V

    .line 10604
    return-object v1
.end method

.method public static declared-synchronized d(LX/03e;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 10590
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/03e;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 10591
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 10592
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/03e;->a:LX/0Uh;

    const/16 v2, 0x47c

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    .line 10593
    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-eq v1, v2, :cond_0

    .line 10594
    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_2

    .line 10595
    const-string v1, "java.vm.version"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 10596
    if-eqz v1, :cond_3

    const-string v2, "0."

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "1."

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 10597
    const/4 v1, 0x0

    .line 10598
    :goto_1
    move v1, v1

    .line 10599
    if-eqz v1, :cond_2

    invoke-static {}, Lcom/facebook/common/dextricks/DalvikInternals;->initGcInstrumentation()Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_2
    iput-boolean v0, p0, LX/03e;->b:Z

    .line 10600
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/03e;->c:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 10601
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 10602
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v1, 0x1

    goto :goto_1
.end method
