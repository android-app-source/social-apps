.class public final LX/0Ky;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/0Kz;


# direct methods
.method public constructor <init>(LX/0Kz;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 42417
    iput-object p1, p0, LX/0Ky;->a:LX/0Kz;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 42418
    iget-object v0, p0, LX/0Ky;->a:LX/0Kz;

    const/4 p0, 0x0

    .line 42419
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 42420
    :cond_0
    return-void

    .line 42421
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v2, v0, LX/0Kz;->d:[[LX/0L4;

    iget-object v3, v0, LX/0Kz;->d:[[LX/0L4;

    array-length v3, v3

    invoke-static {v1, p0, v2, p0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 42422
    iget v1, p1, Landroid/os/Message;->arg1:I

    iput v1, v0, LX/0Kz;->g:I

    .line 42423
    iget-object v1, v0, LX/0Kz;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0KW;

    .line 42424
    iget-boolean v3, v0, LX/0Kz;->f:Z

    iget p0, v0, LX/0Kz;->g:I

    invoke-interface {v1, v3, p0}, LX/0KW;->a(ZI)V

    goto :goto_0

    .line 42425
    :pswitch_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    iput v1, v0, LX/0Kz;->g:I

    .line 42426
    iget-object v1, v0, LX/0Kz;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0KW;

    .line 42427
    iget-boolean v3, v0, LX/0Kz;->f:Z

    iget p0, v0, LX/0Kz;->g:I

    invoke-interface {v1, v3, p0}, LX/0KW;->a(ZI)V

    goto :goto_1

    .line 42428
    :pswitch_2
    iget v1, v0, LX/0Kz;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, LX/0Kz;->h:I

    .line 42429
    iget v1, v0, LX/0Kz;->h:I

    if-nez v1, :cond_0

    .line 42430
    iget-object v1, v0, LX/0Kz;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_2

    .line 42431
    :pswitch_3
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, LX/0Kv;

    .line 42432
    iget-object v2, v0, LX/0Kz;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0KW;

    .line 42433
    invoke-interface {v2, v1}, LX/0KW;->a(LX/0Kv;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
