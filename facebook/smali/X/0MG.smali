.class public final LX/0MG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0LO;


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:LX/0G6;

.field private final c:LX/0MH;

.field private final d:LX/0O1;

.field private final e:I

.field private final f:LX/0MM;

.field private volatile g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/net/Uri;LX/0G6;LX/0MH;LX/0O1;IJ)V
    .locals 2

    .prologue
    .line 45392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45393
    invoke-static {p1}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, LX/0MG;->a:Landroid/net/Uri;

    .line 45394
    invoke-static {p2}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0G6;

    iput-object v0, p0, LX/0MG;->b:LX/0G6;

    .line 45395
    invoke-static {p3}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MH;

    iput-object v0, p0, LX/0MG;->c:LX/0MH;

    .line 45396
    invoke-static {p4}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0O1;

    iput-object v0, p0, LX/0MG;->d:LX/0O1;

    .line 45397
    iput p5, p0, LX/0MG;->e:I

    .line 45398
    new-instance v0, LX/0MM;

    invoke-direct {v0}, LX/0MM;-><init>()V

    iput-object v0, p0, LX/0MG;->f:LX/0MM;

    .line 45399
    iget-object v0, p0, LX/0MG;->f:LX/0MM;

    iput-wide p6, v0, LX/0MM;->a:J

    .line 45400
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0MG;->h:Z

    .line 45401
    return-void
.end method


# virtual methods
.method public final f()V
    .locals 1

    .prologue
    .line 45402
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0MG;->g:Z

    .line 45403
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 45404
    iget-boolean v0, p0, LX/0MG;->g:Z

    return v0
.end method

.method public final h()V
    .locals 15

    .prologue
    const-wide/16 v12, -0x1

    const/4 v8, 0x0

    const/4 v11, 0x1

    const/4 v7, 0x0

    .line 45405
    move v9, v7

    .line 45406
    :goto_0
    if-nez v9, :cond_5

    iget-boolean v0, p0, LX/0MG;->g:Z

    if-nez v0, :cond_5

    .line 45407
    :try_start_0
    iget-object v0, p0, LX/0MG;->f:LX/0MM;

    iget-wide v2, v0, LX/0MM;->a:J

    .line 45408
    iget-object v10, p0, LX/0MG;->b:LX/0G6;

    new-instance v0, LX/0OA;

    iget-object v1, p0, LX/0MG;->a:Landroid/net/Uri;

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LX/0OA;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    invoke-interface {v10, v0}, LX/0G6;->a(LX/0OA;)J

    move-result-wide v4

    .line 45409
    cmp-long v0, v4, v12

    if-eqz v0, :cond_0

    .line 45410
    add-long/2addr v4, v2

    .line 45411
    :cond_0
    new-instance v0, LX/0MB;

    iget-object v1, p0, LX/0MG;->b:LX/0G6;

    invoke-direct/range {v0 .. v5}, LX/0MB;-><init>(LX/0G6;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45412
    :try_start_1
    iget-object v1, p0, LX/0MG;->c:LX/0MH;

    invoke-virtual {v1, v0}, LX/0MH;->a(LX/0MA;)LX/0ME;

    move-result-object v2

    .line 45413
    iget-boolean v1, p0, LX/0MG;->h:Z

    if-eqz v1, :cond_1

    .line 45414
    invoke-interface {v2}, LX/0ME;->b()V

    .line 45415
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/0MG;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    move v1, v9

    .line 45416
    :goto_1
    if-nez v1, :cond_2

    :try_start_2
    iget-boolean v3, p0, LX/0MG;->g:Z

    if-nez v3, :cond_2

    .line 45417
    iget-object v3, p0, LX/0MG;->d:LX/0O1;

    iget v4, p0, LX/0MG;->e:I

    invoke-interface {v3, v4}, LX/0O1;->b(I)V

    .line 45418
    iget-object v3, p0, LX/0MG;->f:LX/0MM;

    invoke-interface {v2, v0, v3}, LX/0ME;->a(LX/0MA;LX/0MM;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v9

    move v1, v9

    goto :goto_1

    .line 45419
    :cond_2
    if-ne v1, v11, :cond_3

    move v0, v7

    .line 45420
    :goto_2
    iget-object v1, p0, LX/0MG;->b:LX/0G6;

    invoke-interface {v1}, LX/0G6;->a()V

    move v9, v0

    .line 45421
    goto :goto_0

    .line 45422
    :cond_3
    iget-object v2, p0, LX/0MG;->f:LX/0MM;

    invoke-interface {v0}, LX/0MA;->c()J

    move-result-wide v4

    iput-wide v4, v2, LX/0MM;->a:J

    move v0, v1

    goto :goto_2

    .line 45423
    :catchall_0
    move-exception v0

    move-object v1, v8

    move v2, v9

    :goto_3
    if-eq v2, v11, :cond_4

    .line 45424
    if-eqz v1, :cond_4

    .line 45425
    iget-object v2, p0, LX/0MG;->f:LX/0MM;

    invoke-interface {v1}, LX/0MA;->c()J

    move-result-wide v4

    iput-wide v4, v2, LX/0MM;->a:J

    .line 45426
    :cond_4
    iget-object v1, p0, LX/0MG;->b:LX/0G6;

    invoke-interface {v1}, LX/0G6;->a()V

    throw v0

    .line 45427
    :cond_5
    return-void

    .line 45428
    :catchall_1
    move-exception v1

    move v2, v9

    move-object v14, v0

    move-object v0, v1

    move-object v1, v14

    goto :goto_3

    :catchall_2
    move-exception v2

    move-object v14, v2

    move v2, v1

    move-object v1, v0

    move-object v0, v14

    goto :goto_3
.end method
