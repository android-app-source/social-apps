.class public final LX/0GG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/04o;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:LX/04m;

.field private final d:Z

.field private final e:Landroid/net/ConnectivityManager;

.field private final f:LX/0GF;

.field private final g:LX/0GB;

.field private final h:Ljava/lang/String;

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:F

.field private final n:F

.field private final o:Z

.field private final p:Z

.field private final q:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34699
    const-class v0, LX/0GG;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0GG;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/04m;Ljava/util/Map;LX/0GB;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/04m;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0GB;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 34645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34646
    iput-object p3, p0, LX/0GG;->b:Ljava/lang/String;

    .line 34647
    iput-object p4, p0, LX/0GG;->c:LX/04m;

    .line 34648
    iput-boolean p7, p0, LX/0GG;->d:Z

    .line 34649
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, LX/0GG;->e:Landroid/net/ConnectivityManager;

    .line 34650
    iput-object p6, p0, LX/0GG;->g:LX/0GB;

    .line 34651
    iput-object p2, p0, LX/0GG;->h:Ljava/lang/String;

    .line 34652
    new-instance v0, LX/0GF;

    invoke-static {p5}, LX/040;->h(Ljava/util/Map;)I

    move-result v4

    invoke-static {p5}, LX/040;->i(Ljava/util/Map;)I

    move-result v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p6

    invoke-direct/range {v0 .. v5}, LX/0GF;-><init>(Landroid/content/Context;Ljava/lang/String;LX/0GB;II)V

    iput-object v0, p0, LX/0GG;->f:LX/0GF;

    .line 34653
    sget-object v0, LX/040;->f:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34654
    sget-object v0, LX/040;->f:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 34655
    :goto_0
    move v0, v0

    .line 34656
    iput v0, p0, LX/0GG;->i:I

    .line 34657
    sget-object v0, LX/040;->g:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34658
    sget-object v0, LX/040;->g:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 34659
    :goto_1
    move v0, v0

    .line 34660
    iput v0, p0, LX/0GG;->j:I

    .line 34661
    sget-object v0, LX/040;->j:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 34662
    sget-object v0, LX/040;->j:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 34663
    :goto_2
    move v0, v0

    .line 34664
    iput v0, p0, LX/0GG;->k:I

    .line 34665
    sget-object v0, LX/040;->k:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 34666
    sget-object v0, LX/040;->k:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 34667
    :goto_3
    move v0, v0

    .line 34668
    iput v0, p0, LX/0GG;->l:I

    .line 34669
    sget-object v0, LX/040;->m:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 34670
    sget-object v0, LX/040;->m:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 34671
    :goto_4
    move v0, v0

    .line 34672
    iput v0, p0, LX/0GG;->m:F

    .line 34673
    sget-object v0, LX/040;->n:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 34674
    sget-object v0, LX/040;->n:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 34675
    :goto_5
    move v0, v0

    .line 34676
    iput v0, p0, LX/0GG;->n:F

    .line 34677
    const/4 v1, 0x0

    .line 34678
    sget-object v0, LX/040;->l:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 34679
    sget-object v0, LX/040;->l:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    .line 34680
    :goto_6
    move v0, v0

    .line 34681
    iput-boolean v0, p0, LX/0GG;->o:Z

    .line 34682
    const/4 v1, 0x1

    .line 34683
    sget-object v0, LX/040;->o:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 34684
    sget-object v0, LX/040;->o:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    .line 34685
    :goto_7
    move v0, v0

    .line 34686
    iput-boolean v0, p0, LX/0GG;->p:Z

    .line 34687
    const/4 v1, 0x1

    .line 34688
    sget-object v0, LX/040;->r:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 34689
    sget-object v0, LX/040;->r:Ljava/lang/String;

    invoke-interface {p5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    .line 34690
    :goto_8
    move v0, v0

    .line 34691
    iput-boolean v0, p0, LX/0GG;->q:Z

    .line 34692
    return-void

    :cond_0
    const/16 v0, 0x1aa

    goto/16 :goto_0

    :cond_1
    const/16 v0, 0x3e8

    goto/16 :goto_1

    :cond_2
    const/16 v0, 0x1388

    goto/16 :goto_2

    :cond_3
    const/16 v0, 0x61a8

    goto/16 :goto_3

    :cond_4
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_4

    :cond_5
    const v0, 0x3f666666    # 0.9f

    goto :goto_5

    :cond_6
    move v0, v1

    .line 34693
    goto :goto_6

    :cond_7
    move v0, v1

    .line 34694
    goto :goto_6

    .line 34695
    :cond_8
    const/4 v0, 0x0

    goto :goto_7

    :cond_9
    move v0, v1

    .line 34696
    goto :goto_7

    .line 34697
    :cond_a
    const/4 v0, 0x0

    goto :goto_8

    :cond_b
    move v0, v1

    .line 34698
    goto :goto_8
.end method

.method private static a(JF)J
    .locals 2

    .prologue
    .line 34644
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    const-wide/32 v0, 0xc350

    :goto_0
    return-wide v0

    :cond_0
    long-to-float v0, p0

    mul-float/2addr v0, p2

    float-to-long v0, v0

    goto :goto_0
.end method

.method private static a(LX/0GG;[LX/0AR;LX/0AR;JZ)LX/0AR;
    .locals 11

    .prologue
    .line 34628
    iget v0, p0, LX/0GG;->m:F

    invoke-static {p3, p4, v0}, LX/0GG;->a(JF)J

    move-result-wide v6

    .line 34629
    iget v0, p0, LX/0GG;->m:F

    iget v1, p0, LX/0GG;->n:F

    mul-float/2addr v0, v1

    invoke-static {p3, p4, v0}, LX/0GG;->a(JF)J

    move-result-wide v4

    .line 34630
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static/range {p5 .. p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 34631
    if-nez p5, :cond_0

    const-wide/16 v0, -0x1

    cmp-long v0, p3, v0

    if-nez v0, :cond_1

    .line 34632
    :cond_0
    iget-object v0, p0, LX/0GG;->f:LX/0GF;

    invoke-virtual {v0, p1}, LX/0GF;->a([LX/0AR;)LX/0AR;

    move-result-object v0

    .line 34633
    :goto_0
    return-object v0

    .line 34634
    :cond_1
    const/4 v0, 0x0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_4

    .line 34635
    aget-object v1, p1, v0

    .line 34636
    if-eqz p2, :cond_2

    iget v2, v1, LX/0AR;->c:I

    iget v3, p2, LX/0AR;->c:I

    if-le v2, v3, :cond_2

    move-wide v2, v4

    .line 34637
    :goto_2
    iget v8, v1, LX/0AR;->c:I

    int-to-long v8, v8

    cmp-long v2, v8, v2

    if-gtz v2, :cond_3

    invoke-static {p0, v1, p2}, LX/0GG;->a(LX/0GG;LX/0AR;LX/0AR;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 34638
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, v1, LX/0AR;->a:Ljava/lang/String;

    aput-object v3, v0, v2

    move-object v0, v1

    .line 34639
    goto :goto_0

    :cond_2
    move-wide v2, v6

    .line 34640
    goto :goto_2

    .line 34641
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 34642
    :cond_4
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    aget-object v2, p1, v2

    iget-object v2, v2, LX/0AR;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 34643
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p1, v0

    goto :goto_0
.end method

.method private static a(LX/0GG;LX/0AR;LX/0AR;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 34617
    const v0, 0x7fffffff

    .line 34618
    iget-object v3, p0, LX/0GG;->e:Landroid/net/ConnectivityManager;

    invoke-static {v3}, LX/0GG;->b(Landroid/net/ConnectivityManager;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 34619
    iget v0, p0, LX/0GG;->i:I

    .line 34620
    :cond_0
    :goto_0
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/0GG;->e:Landroid/net/ConnectivityManager;

    invoke-static {v4}, LX/0GG;->b(Landroid/net/ConnectivityManager;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v1

    iget-object v4, p0, LX/0GG;->b:Ljava/lang/String;

    aput-object v4, v3, v2

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 34621
    iget v3, p1, LX/0AR;->f:I

    move v3, v3

    .line 34622
    if-gt v3, v0, :cond_1

    move v1, v2

    :cond_1
    return v1

    .line 34623
    :cond_2
    const-string v3, "full_screen"

    iget-object v4, p0, LX/0GG;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 34624
    if-nez p2, :cond_3

    move v0, v1

    .line 34625
    :goto_1
    iget v3, p0, LX/0GG;->j:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 34626
    :cond_3
    iget v0, p2, LX/0AR;->f:I

    move v0, v0

    .line 34627
    goto :goto_1
.end method

.method private b(Ljava/util/List;J[LX/0AR;LX/0Ld;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;",
            ">;J[",
            "LX/0AR;",
            "LX/0Ld;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34700
    const-wide/16 v4, 0x0

    .line 34701
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_14

    .line 34702
    iget-boolean v2, p0, LX/0GG;->p:Z

    if-eqz v2, :cond_3

    .line 34703
    const-wide/16 v4, 0x0

    .line 34704
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 34705
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;

    iget-wide v6, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->d:J

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;

    iget-wide v8, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->c:J

    cmp-long v2, v6, v8

    if-lez v2, :cond_0

    .line 34706
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;

    iget-wide v6, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->d:J

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;

    iget-wide v8, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->c:J

    sub-long/2addr v6, v8

    add-long/2addr v4, v6

    .line 34707
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_1
    move-wide v10, v4

    .line 34708
    :goto_1
    iget-boolean v2, p0, LX/0GG;->d:Z

    if-eqz v2, :cond_4

    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-nez v2, :cond_4

    const/4 v8, 0x1

    .line 34709
    :goto_2
    move-object/from16 v0, p5

    iget-object v5, v0, LX/0Ld;->c:LX/0AR;

    .line 34710
    iget-object v2, p0, LX/0GG;->c:LX/04m;

    invoke-interface {v2}, LX/04m;->a()J

    move-result-wide v6

    move-object v3, p0

    move-object/from16 v4, p4

    invoke-static/range {v3 .. v8}, LX/0GG;->a(LX/0GG;[LX/0AR;LX/0AR;JZ)LX/0AR;

    move-result-object v4

    .line 34711
    if-eqz v4, :cond_5

    if-eqz v5, :cond_5

    iget v2, v4, LX/0AR;->c:I

    iget v3, v5, LX/0AR;->c:I

    if-le v2, v3, :cond_5

    const/4 v2, 0x1

    move v6, v2

    .line 34712
    :goto_3
    if-eqz v4, :cond_6

    if-eqz v5, :cond_6

    iget v2, v4, LX/0AR;->c:I

    iget v3, v5, LX/0AR;->c:I

    if-ge v2, v3, :cond_6

    const/4 v2, 0x1

    .line 34713
    :goto_4
    const/16 v3, 0x9

    new-array v7, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v3

    const/4 v3, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v3

    const/4 v8, 0x2

    if-nez v4, :cond_7

    const/4 v3, -0x1

    :goto_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v8

    const/4 v8, 0x3

    if-nez v4, :cond_8

    const/4 v3, -0x1

    :goto_6
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v8

    const/4 v8, 0x4

    if-nez v5, :cond_9

    const/4 v3, -0x1

    :goto_7
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v8

    const/4 v3, 0x5

    const-wide/16 v8, 0x3e8

    div-long v8, v10, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v3

    const/4 v3, 0x6

    iget-object v8, p0, LX/0GG;->b:Ljava/lang/String;

    aput-object v8, v7, v3

    const/4 v3, 0x7

    const-wide/16 v8, 0x3e8

    div-long v8, p2, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v3

    const/16 v3, 0x8

    iget-object v8, p0, LX/0GG;->c:LX/04m;

    invoke-interface {v8}, LX/04m;->a()J

    move-result-wide v8

    const-wide/16 v12, 0x3e8

    div-long/2addr v8, v12

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v3

    .line 34714
    if-eqz v6, :cond_10

    .line 34715
    iget v2, p0, LX/0GG;->k:I

    int-to-long v2, v2

    const-wide/16 v6, 0x3e8

    mul-long/2addr v2, v6

    cmp-long v2, v10, v2

    if-gez v2, :cond_a

    move-object v2, v5

    .line 34716
    :goto_8
    if-eqz v5, :cond_2

    if-eq v2, v5, :cond_2

    .line 34717
    const/4 v3, 0x3

    move-object/from16 v0, p5

    iput v3, v0, LX/0Ld;->b:I

    .line 34718
    :cond_2
    move-object/from16 v0, p5

    iput-object v2, v0, LX/0Ld;->c:LX/0AR;

    .line 34719
    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    move-object/from16 v0, p5

    iget v4, v0, LX/0Ld;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v4, 0x1

    move-object/from16 v0, p5

    iget-object v2, v0, LX/0Ld;->c:LX/0AR;

    if-nez v2, :cond_11

    const-string v2, "null"

    :goto_9
    aput-object v2, v3, v4

    const/4 v4, 0x2

    move-object/from16 v0, p5

    iget-object v2, v0, LX/0Ld;->c:LX/0AR;

    if-nez v2, :cond_12

    const/4 v2, -0x1

    :goto_a
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v4

    .line 34720
    return-void

    .line 34721
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;

    iget-wide v2, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->d:J

    sub-long v4, v2, p2

    move-wide v10, v4

    goto/16 :goto_1

    .line 34722
    :cond_4
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 34723
    :cond_5
    const/4 v2, 0x0

    move v6, v2

    goto/16 :goto_3

    .line 34724
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 34725
    :cond_7
    iget v3, v4, LX/0AR;->c:I

    div-int/lit16 v3, v3, 0x3e8

    goto/16 :goto_5

    :cond_8
    iget v3, v4, LX/0AR;->f:I

    goto/16 :goto_6

    :cond_9
    iget v3, v5, LX/0AR;->c:I

    div-int/lit16 v3, v3, 0x3e8

    goto/16 :goto_7

    .line 34726
    :cond_a
    iget v2, p0, LX/0GG;->l:I

    int-to-long v2, v2

    const-wide/16 v6, 0x3e8

    mul-long/2addr v2, v6

    cmp-long v2, v10, v2

    if-ltz v2, :cond_f

    .line 34727
    const-wide/16 v6, 0x0

    .line 34728
    const/4 v2, 0x0

    move v3, v2

    :goto_b
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_e

    .line 34729
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;

    .line 34730
    iget-boolean v8, p0, LX/0GG;->p:Z

    if-eqz v8, :cond_c

    .line 34731
    iget-wide v8, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->d:J

    iget-wide v10, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->c:J

    cmp-long v8, v8, v10

    if-lez v8, :cond_b

    .line 34732
    iget-wide v8, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->d:J

    iget-wide v10, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->c:J

    sub-long/2addr v8, v10

    add-long/2addr v6, v8

    .line 34733
    :cond_b
    :goto_c
    iget v8, p0, LX/0GG;->l:I

    int-to-long v8, v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    cmp-long v8, v6, v8

    if-ltz v8, :cond_d

    iget-object v8, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iget v8, v8, LX/0AR;->c:I

    iget v9, v4, LX/0AR;->c:I

    if-ge v8, v9, :cond_d

    iget-object v8, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iget v8, v8, LX/0AR;->g:I

    iget v9, v4, LX/0AR;->g:I

    if-ge v8, v9, :cond_d

    iget-object v8, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iget v8, v8, LX/0AR;->g:I

    const/16 v9, 0x2d0

    if-ge v8, v9, :cond_d

    iget-object v2, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iget v2, v2, LX/0AR;->f:I

    const/16 v8, 0x500

    if-ge v2, v8, :cond_d

    .line 34734
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    sub-int/2addr v7, v3

    add-int/lit8 v7, v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v2, v6

    .line 34735
    add-int/lit8 v2, v3, 0x1

    move-object/from16 v0, p5

    iput v2, v0, LX/0Ld;->a:I

    move-object v2, v4

    .line 34736
    goto/16 :goto_8

    .line 34737
    :cond_c
    iget-wide v6, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->d:J

    sub-long v6, v6, p2

    goto :goto_c

    .line 34738
    :cond_d
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_b

    :cond_e
    move-object v2, v4

    .line 34739
    goto/16 :goto_8

    :cond_f
    move-object v2, v4

    .line 34740
    goto/16 :goto_8

    .line 34741
    :cond_10
    if-eqz v2, :cond_13

    .line 34742
    if-eqz v5, :cond_13

    const-wide/32 v2, 0x17d7840

    cmp-long v2, v10, v2

    if-ltz v2, :cond_13

    move-object v2, v5

    .line 34743
    goto/16 :goto_8

    .line 34744
    :cond_11
    move-object/from16 v0, p5

    iget-object v2, v0, LX/0Ld;->c:LX/0AR;

    iget-object v2, v2, LX/0AR;->a:Ljava/lang/String;

    goto/16 :goto_9

    :cond_12
    move-object/from16 v0, p5

    iget-object v2, v0, LX/0Ld;->c:LX/0AR;

    iget v2, v2, LX/0AR;->c:I

    div-int/lit16 v2, v2, 0x3e8

    goto/16 :goto_a

    :cond_13
    move-object v2, v4

    goto/16 :goto_8

    :cond_14
    move-wide v10, v4

    goto/16 :goto_1
.end method

.method public static b(Landroid/net/ConnectivityManager;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 34615
    invoke-virtual {p0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 34616
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/List;J[LX/0AR;LX/0Ln;ZLX/0Lq;)LX/0AR;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/0LQ;",
            ">;J[",
            "LX/0AR;",
            "LX/0Ln;",
            "Z",
            "LX/0Lq;",
            ")",
            "LX/0AR;"
        }
    .end annotation

    .prologue
    .line 34598
    iget-boolean v0, p0, LX/0GG;->o:Z

    if-nez v0, :cond_0

    .line 34599
    const/4 v0, 0x0

    .line 34600
    :goto_0
    return-object v0

    .line 34601
    :cond_0
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    array-length v0, p4

    if-ge v6, v0, :cond_3

    .line 34602
    aget-object v7, p4, v6

    .line 34603
    iget-object v0, p5, LX/0Ln;->c:Ljava/util/HashMap;

    iget-object v1, v7, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Lo;

    .line 34604
    iget-object v0, v1, LX/0Lo;->d:LX/0BX;

    if-eqz v0, :cond_2

    move-object v0, p7

    move-wide v2, p2

    move v4, p6

    move-object v5, p1

    .line 34605
    invoke-virtual/range {v0 .. v5}, LX/0Lq;->a(LX/0Lo;JZLjava/util/List;)I

    move-result v0

    .line 34606
    :try_start_0
    invoke-virtual {v1, v0}, LX/0Lo;->e(I)LX/0Au;

    move-result-object v0

    invoke-virtual {v0}, LX/0Au;->a()Landroid/net/Uri;

    move-result-object v0

    .line 34607
    iget-boolean v1, p0, LX/0GG;->q:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0GG;->h:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0Gv;->a(Ljava/lang/String;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 34608
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    move-object v0, v7

    .line 34609
    goto :goto_0

    .line 34610
    :cond_1
    iget-object v1, p0, LX/0GG;->g:LX/0GB;

    iget-object v2, p0, LX/0GG;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0GB;->a(Ljava/lang/String;Landroid/net/Uri;)[B

    move-result-object v1

    if-eqz v1, :cond_2

    .line 34611
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2
    :try_end_0
    .catch LX/0Ll; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v7

    .line 34612
    goto :goto_0

    .line 34613
    :catch_0
    :cond_2
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 34614
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    goto :goto_2
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 34597
    return-void
.end method

.method public final a(Ljava/util/List;J[LX/0AR;LX/0Ld;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/0LQ;",
            ">;J[",
            "LX/0AR;",
            "LX/0Ld;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34592
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 34593
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0LQ;

    .line 34594
    new-instance v3, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;

    invoke-direct {v3, v0}, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;-><init>(LX/0LQ;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v0, p0

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    .line 34595
    invoke-direct/range {v0 .. v5}, LX/0GG;->b(Ljava/util/List;J[LX/0AR;LX/0Ld;)V

    .line 34596
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 34591
    return-void
.end method
