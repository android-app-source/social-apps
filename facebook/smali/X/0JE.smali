.class public final enum LX/0JE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JE;

.field public static final enum HOST_VIDEO_ID:LX/0JE;

.field public static final enum INSTREAM_VIDEO_AD_BREAK_INDEX:LX/0JE;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39294
    new-instance v0, LX/0JE;

    const-string v1, "HOST_VIDEO_ID"

    const-string v2, "host_video_id"

    invoke-direct {v0, v1, v3, v2}, LX/0JE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JE;->HOST_VIDEO_ID:LX/0JE;

    .line 39295
    new-instance v0, LX/0JE;

    const-string v1, "INSTREAM_VIDEO_AD_BREAK_INDEX"

    const-string v2, "instream_video_ad_break_index"

    invoke-direct {v0, v1, v4, v2}, LX/0JE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JE;->INSTREAM_VIDEO_AD_BREAK_INDEX:LX/0JE;

    .line 39296
    const/4 v0, 0x2

    new-array v0, v0, [LX/0JE;

    sget-object v1, LX/0JE;->HOST_VIDEO_ID:LX/0JE;

    aput-object v1, v0, v3

    sget-object v1, LX/0JE;->INSTREAM_VIDEO_AD_BREAK_INDEX:LX/0JE;

    aput-object v1, v0, v4

    sput-object v0, LX/0JE;->$VALUES:[LX/0JE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39297
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39298
    iput-object p3, p0, LX/0JE;->value:Ljava/lang/String;

    .line 39299
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JE;
    .locals 1

    .prologue
    .line 39300
    const-class v0, LX/0JE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JE;

    return-object v0
.end method

.method public static values()[LX/0JE;
    .locals 1

    .prologue
    .line 39301
    sget-object v0, LX/0JE;->$VALUES:[LX/0JE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JE;

    return-object v0
.end method
