.class public final LX/0OP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0LO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0LO;"
    }
.end annotation


# instance fields
.field private final a:LX/0OA;

.field private final b:LX/0G7;

.field private final c:LX/0Aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Aa",
            "<TT;>;"
        }
    .end annotation
.end field

.field public volatile d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private volatile e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0G7;LX/0Aa;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0G7;",
            "LX/0Aa",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 53006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53007
    iput-object p2, p0, LX/0OP;->b:LX/0G7;

    .line 53008
    iput-object p3, p0, LX/0OP;->c:LX/0Aa;

    .line 53009
    new-instance v0, LX/0OA;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/0OA;-><init>(Landroid/net/Uri;I)V

    iput-object v0, p0, LX/0OP;->a:LX/0OA;

    .line 53010
    return-void
.end method


# virtual methods
.method public final f()V
    .locals 1

    .prologue
    .line 53011
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0OP;->e:Z

    .line 53012
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 53013
    iget-boolean v0, p0, LX/0OP;->e:Z

    return v0
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 53014
    new-instance v1, LX/0O9;

    iget-object v0, p0, LX/0OP;->b:LX/0G7;

    iget-object v2, p0, LX/0OP;->a:LX/0OA;

    invoke-direct {v1, v0, v2}, LX/0O9;-><init>(LX/0G6;LX/0OA;)V

    .line 53015
    :try_start_0
    invoke-static {v1}, LX/0O9;->b(LX/0O9;)V

    .line 53016
    iget-object v0, p0, LX/0OP;->c:LX/0Aa;

    iget-object v2, p0, LX/0OP;->b:LX/0G7;

    invoke-interface {v2}, LX/0G7;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, LX/0Aa;->b(Ljava/lang/String;Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/0OP;->d:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53017
    invoke-virtual {v1}, LX/0O9;->close()V

    .line 53018
    return-void

    .line 53019
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/0O9;->close()V

    throw v0
.end method
