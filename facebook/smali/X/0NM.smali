.class public final LX/0NM;
.super LX/0NL;
.source ""


# instance fields
.field public final synthetic a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

.field private final b:LX/0Oi;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/extractor/ts/TsExtractor;)V
    .locals 2

    .prologue
    .line 49986
    iput-object p1, p0, LX/0NM;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    invoke-direct {p0}, LX/0NL;-><init>()V

    .line 49987
    new-instance v0, LX/0Oi;

    const/4 v1, 0x4

    new-array v1, v1, [B

    invoke-direct {v0, v1}, LX/0Oi;-><init>([B)V

    iput-object v0, p0, LX/0NM;->b:LX/0Oi;

    .line 49988
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 49989
    return-void
.end method

.method public final a(LX/0Oj;ZLX/0LU;)V
    .locals 8

    .prologue
    const/16 v7, 0xd

    const/16 v1, 0xc

    const/4 v6, 0x3

    .line 49990
    if-eqz p2, :cond_0

    .line 49991
    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v0

    .line 49992
    invoke-virtual {p1, v0}, LX/0Oj;->c(I)V

    .line 49993
    :cond_0
    iget-object v0, p0, LX/0NM;->b:LX/0Oi;

    invoke-virtual {p1, v0, v6}, LX/0Oj;->a(LX/0Oi;I)V

    .line 49994
    iget-object v0, p0, LX/0NM;->b:LX/0Oi;

    invoke-virtual {v0, v1}, LX/0Oi;->b(I)V

    .line 49995
    iget-object v0, p0, LX/0NM;->b:LX/0Oi;

    invoke-virtual {v0, v1}, LX/0Oi;->c(I)I

    move-result v0

    .line 49996
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/0Oj;->c(I)V

    .line 49997
    add-int/lit8 v0, v0, -0x9

    div-int/lit8 v1, v0, 0x4

    .line 49998
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 49999
    iget-object v2, p0, LX/0NM;->b:LX/0Oi;

    const/4 v3, 0x4

    invoke-virtual {p1, v2, v3}, LX/0Oj;->a(LX/0Oi;I)V

    .line 50000
    iget-object v2, p0, LX/0NM;->b:LX/0Oi;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, LX/0Oi;->c(I)I

    move-result v2

    .line 50001
    iget-object v3, p0, LX/0NM;->b:LX/0Oi;

    invoke-virtual {v3, v6}, LX/0Oi;->b(I)V

    .line 50002
    if-nez v2, :cond_1

    .line 50003
    iget-object v2, p0, LX/0NM;->b:LX/0Oi;

    invoke-virtual {v2, v7}, LX/0Oi;->b(I)V

    .line 50004
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50005
    :cond_1
    iget-object v2, p0, LX/0NM;->b:LX/0Oi;

    invoke-virtual {v2, v7}, LX/0Oi;->c(I)I

    move-result v2

    .line 50006
    iget-object v3, p0, LX/0NM;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    iget-object v3, v3, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->a:Landroid/util/SparseArray;

    new-instance v4, LX/0NO;

    iget-object v5, p0, LX/0NM;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    invoke-direct {v4, v5}, LX/0NO;-><init>(Lcom/google/android/exoplayer/extractor/ts/TsExtractor;)V

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 50007
    :cond_2
    return-void
.end method
