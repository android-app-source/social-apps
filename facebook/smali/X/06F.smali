.class public final LX/06F;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/rti/common/util/NonInjectProvider1",
        "<",
        "Ljava/util/List",
        "<",
        "LX/0AF;",
        ">;",
        "LX/072;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/05S;

.field public final synthetic b:Z

.field public final synthetic c:LX/05y;

.field public final synthetic d:Ljava/util/concurrent/ScheduledExecutorService;

.field public final synthetic e:LX/06B;

.field public final synthetic f:Ljava/util/concurrent/ExecutorService;

.field public final synthetic g:LX/05Q;


# direct methods
.method public constructor <init>(LX/05Q;LX/05S;ZLX/05y;Ljava/util/concurrent/ScheduledExecutorService;LX/06B;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 17434
    iput-object p1, p0, LX/06F;->g:LX/05Q;

    iput-object p2, p0, LX/06F;->a:LX/05S;

    iput-boolean p3, p0, LX/06F;->b:Z

    iput-object p4, p0, LX/06F;->c:LX/05y;

    iput-object p5, p0, LX/06F;->d:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p6, p0, LX/06F;->e:LX/06B;

    iput-object p7, p0, LX/06F;->f:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/06F;Ljava/util/List;)LX/072;
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;)",
            "LX/072;"
        }
    .end annotation

    .prologue
    .line 17435
    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->g:LX/05Q;

    iget-object v2, v2, LX/05Q;->u:LX/04p;

    invoke-virtual {v2}, LX/04p;->b()LX/04q;

    move-result-object v18

    .line 17436
    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->e:LX/05G;

    invoke-interface {v2}, LX/05G;->e()V

    .line 17437
    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->e:LX/05G;

    invoke-interface {v2}, LX/05G;->c()Ljava/lang/String;

    move-result-object v2

    .line 17438
    move-object/from16 v0, p0

    iget-object v3, v0, LX/06F;->a:LX/05S;

    iget-object v3, v3, LX/05S;->e:LX/05G;

    invoke-interface {v3}, LX/05G;->d()Ljava/lang/String;

    move-result-object v3

    .line 17439
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/06F;->b:Z

    if-nez v4, :cond_2

    invoke-static {v2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v3}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 17440
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->f:LX/05F;

    invoke-interface {v2}, LX/05F;->c()V

    .line 17441
    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->e:LX/05G;

    sget-object v3, LX/06y;->a:LX/06y;

    invoke-interface {v2, v3}, LX/05G;->a(LX/06y;)Z

    .line 17442
    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->h:LX/05F;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->g:LX/05G;

    if-eqz v2, :cond_2

    .line 17443
    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->g:LX/05G;

    invoke-interface {v2}, LX/05G;->e()V

    .line 17444
    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->g:LX/05G;

    invoke-interface {v2}, LX/05G;->c()Ljava/lang/String;

    move-result-object v2

    .line 17445
    move-object/from16 v0, p0

    iget-object v3, v0, LX/06F;->a:LX/05S;

    iget-object v3, v3, LX/05S;->g:LX/05G;

    invoke-interface {v3}, LX/05G;->d()Ljava/lang/String;

    move-result-object v3

    .line 17446
    invoke-static {v2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v3}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 17447
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->h:LX/05F;

    invoke-interface {v2}, LX/05F;->c()V

    .line 17448
    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->g:LX/05G;

    sget-object v3, LX/06y;->a:LX/06y;

    invoke-interface {v2, v3}, LX/05G;->a(LX/06y;)Z

    .line 17449
    :cond_2
    const/16 v25, 0x0

    .line 17450
    const/4 v3, 0x0

    .line 17451
    const/16 v27, 0x0

    .line 17452
    const/16 v28, 0x0

    .line 17453
    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->h:LX/05F;

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->g:LX/05G;

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, LX/06F;->a:LX/05S;

    iget-object v2, v2, LX/05S;->m:LX/05N;

    invoke-interface {v2}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v2, LX/0HZ;->USER_AND_DEVICE_AUTH:LX/0HZ;

    invoke-static {v2}, LX/0HT;->a(Ljava/lang/Enum;)J

    move-result-wide v6

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-eqz v2, :cond_5

    .line 17454
    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 17455
    move-object/from16 v0, p0

    iget-object v4, v0, LX/06F;->a:LX/05S;

    iget-object v4, v4, LX/05S;->h:LX/05F;

    invoke-interface {v4}, LX/05F;->a()LX/06k;

    move-result-object v4

    .line 17456
    if-eqz v4, :cond_4

    invoke-virtual {v4}, LX/06k;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 17457
    invoke-virtual {v4}, LX/06k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 17458
    invoke-virtual {v4}, LX/06k;->b()Ljava/lang/String;

    move-result-object v2

    .line 17459
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/06F;->a:LX/05S;

    iget-object v4, v4, LX/05S;->g:LX/05G;

    invoke-interface {v4}, LX/05G;->c()Ljava/lang/String;

    move-result-object v27

    .line 17460
    move-object/from16 v0, p0

    iget-object v4, v0, LX/06F;->a:LX/05S;

    iget-object v4, v4, LX/05S;->g:LX/05G;

    invoke-interface {v4}, LX/05G;->d()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v26, v2

    move-object/from16 v25, v3

    .line 17461
    :goto_1
    new-instance v2, LX/06z;

    move-object/from16 v0, v18

    iget-object v3, v0, LX/04q;->a:Ljava/lang/String;

    move-object/from16 v0, v18

    iget v4, v0, LX/04q;->c:I

    move-object/from16 v0, v18

    iget v5, v0, LX/04q;->d:I

    move-object/from16 v0, v18

    iget-boolean v6, v0, LX/04q;->x:Z

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->a:LX/05S;

    iget-object v7, v7, LX/05S;->f:LX/05F;

    invoke-interface {v7}, LX/05F;->a()LX/06k;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, LX/06F;->a:LX/05S;

    iget-object v8, v8, LX/05S;->e:LX/05G;

    invoke-interface {v8}, LX/05G;->c()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, LX/06F;->a:LX/05S;

    iget-object v9, v9, LX/05S;->e:LX/05G;

    invoke-interface {v9}, LX/05G;->d()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, LX/06F;->a:LX/05S;

    iget-object v10, v10, LX/05S;->e:LX/05G;

    invoke-interface {v10}, LX/05G;->a()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, LX/06F;->g:LX/05Q;

    iget-object v11, v11, LX/05Q;->s:LX/05a;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/06F;->g:LX/05Q;

    iget-object v12, v12, LX/05Q;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    move-object/from16 v0, v18

    iget v13, v0, LX/04q;->g:I

    move-object/from16 v0, v18

    iget v14, v0, LX/04q;->f:I

    move-object/from16 v0, v18

    iget v15, v0, LX/04q;->e:I

    move-object/from16 v0, v18

    iget-boolean v0, v0, LX/04q;->y:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/06F;->a:LX/05S;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LX/05S;->u:LX/05N;

    move-object/from16 v17, v0

    invoke-static/range {v16 .. v17}, LX/05Q;->a(ZLX/05N;)I

    move-result v16

    move-object/from16 v0, v18

    iget v0, v0, LX/04q;->r:I

    move/from16 v17, v0

    move-object/from16 v0, v18

    iget v0, v0, LX/04q;->t:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/06F;->a:LX/05S;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, LX/05S;->m:LX/05N;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/06F;->a:LX/05S;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, LX/05S;->s:LX/05N;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Boolean;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, LX/06F;->a:LX/05S;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, LX/05S;->l:LX/05N;

    move-object/from16 v20, v0

    if-eqz v20, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, LX/06F;->a:LX/05S;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, LX/05S;->l:LX/05N;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Boolean;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v22

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, LX/06F;->a:LX/05S;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, LX/05S;->w:LX/05N;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Boolean;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, LX/06F;->a:LX/05S;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, LX/05S;->x:LX/05N;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Boolean;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, LX/06F;->a:LX/05S;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-boolean v0, v0, LX/05S;->F:Z

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/06F;->a:LX/05S;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-boolean v0, v0, LX/05S;->J:Z

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/06F;->a:LX/05S;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, LX/05S;->K:Ljava/util/Map;

    move-object/from16 v31, v0

    move-object/from16 v20, p1

    invoke-direct/range {v2 .. v31}, LX/06z;-><init>(Ljava/lang/String;IIZLX/06k;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/05a;Ljava/util/concurrent/atomic/AtomicInteger;IIIIIILX/05N;Ljava/util/List;ZZZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/util/Map;)V

    .line 17462
    move-object/from16 v0, p0

    iget-object v3, v0, LX/06F;->a:LX/05S;

    iget-object v3, v3, LX/05S;->z:LX/05M;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/06F;->c:LX/05y;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/06F;->g:LX/05Q;

    iget-object v6, v5, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->d:Ljava/util/concurrent/ScheduledExecutorService;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/06F;->e:LX/06B;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/06F;->a:LX/05S;

    iget-object v9, v5, LX/05S;->j:LX/05K;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/06F;->a:LX/05S;

    iget-object v10, v5, LX/05S;->i:LX/05L;

    move-object v5, v2

    invoke-virtual/range {v3 .. v10}, LX/05M;->a(LX/05y;LX/06z;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Ljava/util/concurrent/ScheduledExecutorService;LX/06B;LX/05K;LX/05L;)LX/071;

    move-result-object v14

    .line 17463
    move-object/from16 v0, p0

    iget-object v3, v0, LX/06F;->g:LX/05Q;

    iget-object v3, v3, LX/05Q;->g:LX/05i;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/06F;->a:LX/05S;

    iget-object v4, v4, LX/05S;->z:LX/05M;

    invoke-virtual {v4}, LX/05M;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/05i;->a(Ljava/lang/String;)V

    .line 17464
    new-instance v3, LX/072;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/06F;->g:LX/05Q;

    iget-object v4, v4, LX/05Q;->c:LX/059;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/06F;->g:LX/05Q;

    iget-object v5, v5, LX/05Q;->f:LX/05h;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/06F;->g:LX/05Q;

    iget-object v6, v6, LX/05Q;->g:LX/05i;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->g:LX/05Q;

    iget-object v8, v7, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/06F;->f:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->g:LX/05Q;

    iget-object v10, v7, LX/05Q;->w:LX/05Z;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->a:LX/05S;

    iget-object v11, v7, LX/05S;->o:LX/05R;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->a:LX/05S;

    iget-object v12, v7, LX/05S;->f:LX/05F;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->g:LX/05Q;

    iget-object v13, v7, LX/05Q;->D:LX/05k;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->a:LX/05S;

    iget-object v15, v7, LX/05S;->t:LX/05N;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->a:LX/05S;

    iget-object v0, v7, LX/05S;->A:Ljava/util/concurrent/atomic/AtomicReference;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->a:LX/05S;

    iget-object v0, v7, LX/05S;->v:LX/05N;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->a:LX/05S;

    iget-object v0, v7, LX/05S;->l:LX/05N;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->a:LX/05S;

    iget-boolean v0, v7, LX/05S;->H:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/06F;->a:LX/05S;

    iget-boolean v0, v7, LX/05S;->I:Z

    move/from16 v20, v0

    move-object v7, v2

    invoke-direct/range {v3 .. v20}, LX/072;-><init>(LX/059;LX/05h;LX/05i;LX/06z;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Ljava/util/concurrent/ExecutorService;LX/05Z;LX/05R;LX/05F;LX/05k;LX/071;LX/05N;Ljava/util/concurrent/atomic/AtomicReference;LX/05N;LX/05N;ZZ)V

    return-object v3

    .line 17465
    :cond_3
    const/16 v22, 0x0

    goto/16 :goto_2

    :cond_4
    move-object/from16 v32, v3

    move-object v3, v2

    move-object/from16 v2, v32

    goto/16 :goto_0

    :cond_5
    move-object/from16 v26, v3

    goto/16 :goto_1
.end method
