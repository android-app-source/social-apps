.class public LX/07t;
.super LX/07u;
.source ""


# instance fields
.field public a:[LX/07v;

.field public final synthetic b:LX/024;

.field public final c:Ljava/util/zip/ZipFile;

.field private final d:LX/025;


# direct methods
.method public constructor <init>(LX/024;LX/025;)V
    .locals 2

    .prologue
    .line 20345
    iput-object p1, p0, LX/07t;->b:LX/024;

    invoke-direct {p0}, LX/07u;-><init>()V

    .line 20346
    new-instance v0, Ljava/util/zip/ZipFile;

    iget-object v1, p1, LX/024;->c:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/07t;->c:Ljava/util/zip/ZipFile;

    .line 20347
    iput-object p2, p0, LX/07t;->d:LX/025;

    .line 20348
    return-void
.end method


# virtual methods
.method public final a()LX/0AO;
    .locals 2

    .prologue
    .line 20344
    new-instance v0, LX/0AO;

    invoke-virtual {p0}, LX/07t;->c()[LX/07v;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0AO;-><init>([LX/07w;)V

    return-object v0
.end method

.method public a(Ljava/util/zip/ZipEntry;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 20343
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/0Ay;
    .locals 2

    .prologue
    .line 20349
    new-instance v0, LX/07x;

    invoke-direct {v0, p0}, LX/07x;-><init>(LX/07t;)V

    return-object v0
.end method

.method public final c()[LX/07v;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 20307
    iget-object v0, p0, LX/07t;->a:[LX/07v;

    if-nez v0, :cond_7

    .line 20308
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 20309
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 20310
    iget-object v0, p0, LX/07t;->b:LX/024;

    iget-object v0, v0, LX/024;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 20311
    invoke-static {}, LX/01z;->a()[Ljava/lang/String;

    move-result-object v6

    .line 20312
    iget-object v0, p0, LX/07t;->c:Ljava/util/zip/ZipFile;

    invoke-virtual {v0}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v7

    .line 20313
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 20314
    invoke-interface {v7}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/zip/ZipEntry;

    .line 20315
    invoke-virtual {v0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 20316
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 20317
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 20318
    const/4 v9, 0x2

    invoke-virtual {v1, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    .line 20319
    invoke-static {v6, v8}, LX/01z;->a([Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 20320
    if-ltz v10, :cond_0

    .line 20321
    invoke-interface {v3, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 20322
    invoke-virtual {v4, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/07v;

    .line 20323
    if-eqz v1, :cond_1

    iget v1, v1, LX/07v;->b:I

    if-ge v10, v1, :cond_0

    .line 20324
    :cond_1
    new-instance v1, LX/07v;

    invoke-direct {v1, v9, v0, v10}, LX/07v;-><init>(Ljava/lang/String;Ljava/util/zip/ZipEntry;I)V

    invoke-virtual {v4, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 20325
    :cond_2
    iget-object v1, p0, LX/07t;->d:LX/025;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 20326
    iput-object v0, v1, LX/025;->c:[Ljava/lang/String;

    .line 20327
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v1

    new-array v1, v1, [LX/07v;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/07v;

    .line 20328
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    move v1, v2

    move v3, v2

    .line 20329
    :goto_1
    array-length v4, v0

    if-ge v1, v4, :cond_4

    .line 20330
    aget-object v4, v0, v1

    .line 20331
    iget-object v5, v4, LX/07v;->a:Ljava/util/zip/ZipEntry;

    iget-object v4, v4, LX/07w;->c:Ljava/lang/String;

    invoke-virtual {p0, v5, v4}, LX/07t;->a(Ljava/util/zip/ZipEntry;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 20332
    add-int/lit8 v3, v3, 0x1

    .line 20333
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 20334
    :cond_3
    const/4 v4, 0x0

    aput-object v4, v0, v1

    goto :goto_2

    .line 20335
    :cond_4
    new-array v4, v3, [LX/07v;

    move v1, v2

    .line 20336
    :goto_3
    array-length v3, v0

    if-ge v1, v3, :cond_6

    .line 20337
    aget-object v5, v0, v1

    .line 20338
    if-eqz v5, :cond_5

    .line 20339
    add-int/lit8 v3, v2, 0x1

    aput-object v5, v4, v2

    move v2, v3

    .line 20340
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 20341
    :cond_6
    iput-object v4, p0, LX/07t;->a:[LX/07v;

    .line 20342
    :cond_7
    iget-object v0, p0, LX/07t;->a:[LX/07v;

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 20305
    iget-object v0, p0, LX/07t;->c:Ljava/util/zip/ZipFile;

    invoke-virtual {v0}, Ljava/util/zip/ZipFile;->close()V

    .line 20306
    return-void
.end method
