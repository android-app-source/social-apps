.class public LX/0A4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/2WF;",
            ">;>;"
        }
    .end annotation
.end field

.field public c:J

.field private d:J

.field public e:J

.field private f:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23464
    new-instance v0, LX/0aq;

    const/16 v1, 0x12c

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/0A4;->b:LX/0aq;

    .line 23465
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0A4;->a:Ljava/lang/Object;

    .line 23466
    return-void
.end method

.method private static a(Ljava/lang/Iterable;I)J
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/2WF;",
            ">;I)J"
        }
    .end annotation

    .prologue
    .line 23458
    const-wide/16 v0, 0x0

    .line 23459
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2WF;

    .line 23460
    invoke-virtual {v0}, LX/2WF;->a()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 23461
    goto :goto_0

    .line 23462
    :cond_0
    int-to-long v0, p1

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x1f40

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private a(Ljava/lang/String;JJI)V
    .locals 7

    .prologue
    .line 23445
    new-instance v1, LX/2WF;

    invoke-direct {v1, p2, p3, p4, p5}, LX/2WF;-><init>(JJ)V

    .line 23446
    if-eqz p1, :cond_1

    if-lez p6, :cond_1

    .line 23447
    iget-object v0, p0, LX/0A4;->b:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 23448
    if-nez v0, :cond_0

    .line 23449
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v0

    .line 23450
    :cond_0
    invoke-virtual {v1, v0}, LX/2WF;->b(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v2

    .line 23451
    invoke-virtual {v1, v0}, LX/2WF;->c(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    .line 23452
    iget-object v3, p0, LX/0A4;->b:LX/0aq;

    invoke-virtual {v3, p1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23453
    invoke-static {v2, p6}, LX/0A4;->a(Ljava/lang/Iterable;I)J

    move-result-wide v2

    .line 23454
    iget-wide v4, p0, LX/0A4;->c:J

    add-long/2addr v4, v2

    iput-wide v4, p0, LX/0A4;->c:J

    .line 23455
    :cond_1
    invoke-virtual {v1}, LX/2WF;->a()J

    move-result-wide v0

    .line 23456
    iget-wide v4, p0, LX/0A4;->e:J

    add-long/2addr v4, v0

    iput-wide v4, p0, LX/0A4;->e:J

    .line 23457
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 6

    .prologue
    .line 23440
    iget-object v1, p0, LX/0A4;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 23441
    :try_start_0
    iget-wide v2, p0, LX/0A4;->c:J

    iget-wide v4, p0, LX/0A4;->d:J

    sub-long/2addr v2, v4

    .line 23442
    iget-wide v4, p0, LX/0A4;->c:J

    iput-wide v4, p0, LX/0A4;->d:J

    .line 23443
    monitor-exit v1

    return-wide v2

    .line 23444
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/2WG;JJI)V
    .locals 8
    .param p1    # LX/2WG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 23432
    iget-object v7, p0, LX/0A4;->a:Ljava/lang/Object;

    monitor-enter v7

    .line 23433
    cmp-long v0, p2, p4

    if-ltz v0, :cond_0

    .line 23434
    :try_start_0
    monitor-exit v7

    .line 23435
    :goto_0
    return-void

    .line 23436
    :cond_0
    if-nez p1, :cond_1

    const/4 v1, 0x0

    :goto_1
    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    move v6, p6

    .line 23437
    invoke-direct/range {v0 .. v6}, LX/0A4;->a(Ljava/lang/String;JJI)V

    .line 23438
    monitor-exit v7

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 23439
    :cond_1
    :try_start_1
    invoke-virtual {p1}, LX/2WG;->a()LX/1bh;

    move-result-object v0

    invoke-interface {v0}, LX/1bh;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_1
.end method

.method public final a(Ljava/io/DataInputStream;)V
    .locals 13

    .prologue
    const/4 v8, 0x0

    .line 23416
    iget-object v10, p0, LX/0A4;->a:Ljava/lang/Object;

    monitor-enter v10

    .line 23417
    :try_start_0
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, LX/0A4;->c:J

    .line 23418
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, LX/0A4;->d:J

    .line 23419
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, LX/0A4;->e:J

    .line 23420
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, LX/0A4;->f:J

    .line 23421
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v11

    move v9, v8

    .line 23422
    :goto_0
    if-ge v9, v11, :cond_1

    .line 23423
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 23424
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v12

    move v7, v8

    .line 23425
    :goto_1
    if-ge v7, v12, :cond_0

    .line 23426
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    .line 23427
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    .line 23428
    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/0A4;->a(Ljava/lang/String;JJI)V

    .line 23429
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    .line 23430
    :cond_0
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    .line 23431
    :cond_1
    monitor-exit v10

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/io/DataOutputStream;)V
    .locals 6

    .prologue
    .line 23401
    iget-object v2, p0, LX/0A4;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 23402
    :try_start_0
    iget-wide v0, p0, LX/0A4;->c:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 23403
    iget-wide v0, p0, LX/0A4;->d:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 23404
    iget-wide v0, p0, LX/0A4;->e:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 23405
    iget-wide v0, p0, LX/0A4;->f:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 23406
    iget-object v0, p0, LX/0A4;->b:LX/0aq;

    invoke-virtual {v0}, LX/0aq;->d()Ljava/util/Map;

    move-result-object v0

    .line 23407
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 23408
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 23409
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 23410
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 23411
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 23412
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2WF;

    .line 23413
    iget-wide v4, v0, LX/2WF;->a:J

    invoke-virtual {p1, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 23414
    iget-wide v4, v0, LX/2WF;->b:J

    invoke-virtual {p1, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    goto :goto_0

    .line 23415
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final b()J
    .locals 6

    .prologue
    .line 23396
    iget-object v1, p0, LX/0A4;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 23397
    :try_start_0
    iget-wide v2, p0, LX/0A4;->e:J

    iget-wide v4, p0, LX/0A4;->f:J

    sub-long/2addr v2, v4

    .line 23398
    iget-wide v4, p0, LX/0A4;->e:J

    iput-wide v4, p0, LX/0A4;->f:J

    .line 23399
    monitor-exit v1

    return-wide v2

    .line 23400
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
