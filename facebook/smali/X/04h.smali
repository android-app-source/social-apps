.class public LX/04h;
.super LX/1iY;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 14066
    invoke-direct {p0}, LX/1iY;-><init>()V

    .line 14067
    return-void
.end method


# virtual methods
.method public final a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 8
    .param p3    # Lorg/apache/http/HttpResponse;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v6, 0x10

    .line 14044
    invoke-super/range {p0 .. p5}, LX/1iY;->a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V

    .line 14045
    invoke-interface {p2}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v1

    .line 14046
    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    .line 14047
    if-eqz p3, :cond_0

    .line 14048
    invoke-interface {p3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 14049
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "(FAILED: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 14050
    :goto_0
    invoke-static {v6, v7, v1, v2}, LX/018;->c(JLjava/lang/String;I)V

    .line 14051
    invoke-static {v6, v7, v1, v0, v2}, LX/018;->b(JLjava/lang/String;Ljava/lang/String;I)V

    .line 14052
    return-void

    .line 14053
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "(FAILED)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;LX/1iW;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x10

    .line 14060
    invoke-super {p0, p1, p2, p3}, LX/1iY;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;LX/1iW;)V

    .line 14061
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    .line 14062
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    .line 14063
    invoke-static {v4, v5, v0, v1}, LX/018;->b(JLjava/lang/String;I)V

    .line 14064
    const-string v2, "Latency"

    invoke-static {v4, v5, v0, v1, v2}, LX/018;->a(JLjava/lang/String;ILjava/lang/String;)V

    .line 14065
    return-void
.end method

.method public final a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 5

    .prologue
    .line 14057
    invoke-super {p0, p1, p2}, LX/1iY;->a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 14058
    const-wide/16 v0, 0x10

    invoke-virtual {p0}, LX/1iY;->b()Lorg/apache/http/HttpRequest;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, LX/1iY;->b()Lorg/apache/http/HttpRequest;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    const-string v4, "Download"

    invoke-static {v0, v1, v2, v3, v4}, LX/018;->a(JLjava/lang/String;ILjava/lang/String;)V

    .line 14059
    return-void
.end method

.method public final b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 4

    .prologue
    .line 14054
    invoke-super {p0, p1, p2}, LX/1iY;->b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 14055
    const-wide/16 v0, 0x10

    invoke-virtual {p0}, LX/1iY;->b()Lorg/apache/http/HttpRequest;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, LX/1iY;->b()Lorg/apache/http/HttpRequest;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, LX/018;->c(JLjava/lang/String;I)V

    .line 14056
    return-void
.end method
