.class public LX/07F;
.super LX/07G;
.source ""


# instance fields
.field private final a:Ljavax/net/ssl/SSLSocketFactory;

.field private final b:LX/05w;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljavax/net/ssl/SSLSocketFactory;LX/05w;)V
    .locals 0

    .prologue
    .line 19396
    invoke-direct {p0, p1}, LX/07G;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 19397
    iput-object p2, p0, LX/07F;->a:Ljavax/net/ssl/SSLSocketFactory;

    .line 19398
    iput-object p3, p0, LX/07F;->b:LX/05w;

    .line 19399
    return-void
.end method


# virtual methods
.method public final a(Ljava/net/Socket;Ljava/lang/String;I)Ljava/net/Socket;
    .locals 2

    .prologue
    .line 19400
    invoke-virtual {p1}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    invoke-static {v0}, LX/01n;->a(Z)V

    .line 19401
    iget-object v0, p0, LX/07F;->a:Ljavax/net/ssl/SSLSocketFactory;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, p3, v1}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    .line 19402
    iget-object v1, p0, LX/07F;->b:LX/05w;

    invoke-virtual {v1, v0, p2}, LX/05w;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V

    .line 19403
    return-object v0
.end method
