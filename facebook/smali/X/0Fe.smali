.class public final LX/0Fe;
.super LX/02c;
.source ""


# direct methods
.method public constructor <init>([LX/02Z;)V
    .locals 5

    .prologue
    .line 33918
    const/16 v0, 0x8

    .line 33919
    array-length v1, p1

    mul-int/lit8 v1, v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    .line 33920
    const/4 v1, 0x0

    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_0

    .line 33921
    aget-object v3, p1, v1

    invoke-static {v3}, LX/0Fe;->makeDexName(LX/02Z;)Ljava/lang/String;

    move-result-object v3

    .line 33922
    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v4, v4, 0x0

    aput-object v3, v2, v4

    .line 33923
    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3}, LX/0Fe;->makeOdexName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 33924
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 33925
    :cond_0
    move-object v1, v2

    .line 33926
    invoke-direct {p0, v0, v1}, LX/02c;-><init>(I[Ljava/lang/String;)V

    .line 33927
    return-void
.end method

.method public static makeDexName(LX/02Z;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 33935
    iget-object v0, p0, LX/02Z;->assetName:Ljava/lang/String;

    const-string v1, ".dex.xz"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/02Z;->assetName:Ljava/lang/String;

    const-string v1, ".dex"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33936
    :cond_0
    const-string v0, ".dex"

    .line 33937
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "prog-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/02Z;->hash:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 33938
    :cond_1
    const-string v0, ".dex.jar"

    goto :goto_0
.end method

.method public static makeOdexName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 33934
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, LX/02Q;->stripLastExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".odex"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configureClassLoader(Ljava/io/File;LX/02f;)V
    .locals 5

    .prologue
    .line 33930
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 33931
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    add-int/lit8 v3, v0, 0x0

    aget-object v2, v2, v3

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    add-int/lit8 v4, v0, 0x1

    aget-object v3, v3, v4

    invoke-direct {v2, p1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p2, v1, v2}, LX/02f;->addDex(Ljava/io/File;Ljava/io/File;)V

    .line 33932
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 33933
    :cond_0
    return-void
.end method

.method public final getSchemeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33929
    const-string v0, "OdexSchemeBoring"

    return-object v0
.end method

.method public final makeCompiler(LX/02U;I)LX/084;
    .locals 1

    .prologue
    .line 33928
    new-instance v0, LX/0Fd;

    invoke-direct {v0, p1, p2}, LX/0Fd;-><init>(LX/02U;I)V

    return-object v0
.end method
