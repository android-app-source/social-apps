.class public final LX/0EI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

.field private b:Landroid/view/View;

.field private c:F

.field private d:F


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 31562
    iput-object p1, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31563
    iput-object p2, p0, LX/0EI;->b:Landroid/view/View;

    .line 31564
    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31536
    iget-object v3, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    iget-boolean v3, v3, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->b:Z

    if-eqz v3, :cond_0

    move v0, v1

    .line 31537
    :goto_0
    return v0

    .line 31538
    :cond_0
    iget-object v3, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    iget-boolean v3, v3, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a:Z

    if-eqz v3, :cond_1

    .line 31539
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 31540
    packed-switch v3, :pswitch_data_0

    :cond_1
    :goto_1
    move v0, v2

    .line 31541
    goto :goto_0

    .line 31542
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, LX/0EI;->d:F

    .line 31543
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v0

    iput v0, p0, LX/0EI;->c:F

    goto :goto_1

    .line 31544
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    .line 31545
    iget v3, p0, LX/0EI;->c:F

    iget v4, p0, LX/0EI;->d:F

    sub-float v4, v1, v4

    add-float/2addr v3, v4

    iput v3, p0, LX/0EI;->c:F

    .line 31546
    iget-object v3, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    iget-boolean v3, v3, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->c:Z

    if-eqz v3, :cond_2

    .line 31547
    iget v3, p0, LX/0EI;->c:F

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iput v3, p0, LX/0EI;->c:F

    .line 31548
    :goto_2
    iget v3, p0, LX/0EI;->c:F

    invoke-virtual {p1, v3}, Landroid/view/View;->setX(F)V

    .line 31549
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 31550
    iget-object v3, p0, LX/0EI;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, LX/0EI;->c:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    sub-float/2addr v3, v4

    .line 31551
    iget-object v4, p0, LX/0EI;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    if-nez v4, :cond_3

    .line 31552
    :goto_3
    iget-object v3, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    iget-object v3, v3, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->j:Landroid/view/View;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    .line 31553
    iget-object v0, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    iget-object v0, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 31554
    iput v1, p0, LX/0EI;->d:F

    goto :goto_1

    .line 31555
    :cond_2
    iget v3, p0, LX/0EI;->c:F

    iget-object v4, p0, LX/0EI;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iput v3, p0, LX/0EI;->c:F

    goto :goto_2

    .line 31556
    :cond_3
    iget-object v0, p0, LX/0EI;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v3, v0

    goto :goto_3

    .line 31557
    :pswitch_2
    iget-object v0, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    iget-boolean v0, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->c:Z

    if-eqz v0, :cond_6

    iget v0, p0, LX/0EI;->c:F

    iget-object v3, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    iget v3, v3, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->e:I

    iget-object v4, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-static {v4}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->getEffectiveContentViewWidth(Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_5

    .line 31558
    :cond_4
    :goto_4
    if-eqz v1, :cond_7

    .line 31559
    iget-object v0, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->b()V

    goto/16 :goto_1

    :cond_5
    move v1, v2

    .line 31560
    goto :goto_4

    :cond_6
    iget v0, p0, LX/0EI;->c:F

    iget-object v3, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-static {v3}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->getEffectiveContentViewWidth(Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)I

    move-result v3

    iget-object v4, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    iget v4, v4, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->e:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-gez v0, :cond_4

    move v1, v2

    goto :goto_4

    .line 31561
    :cond_7
    iget-object v0, p0, LX/0EI;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a()V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
