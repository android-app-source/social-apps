.class public LX/0K0;
.super LX/0Ji;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final a:LX/04G;

.field private final b:Ljava/lang/String;

.field private final h:LX/0GB;

.field private final i:LX/0GN;

.field private final j:I

.field private final k:I

.field private final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/lang/String;

.field private final n:Z

.field private final o:LX/0KZ;


# direct methods
.method public constructor <init>(Landroid/net/Uri;LX/04G;Ljava/lang/String;Landroid/content/Context;Landroid/os/Handler;LX/0Jv;LX/0Js;IILjava/lang/String;LX/19j;LX/0KZ;)V
    .locals 8

    .prologue
    .line 40014
    move-object v2, p0

    move-object v3, p1

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v2 .. v7}, LX/0Ji;-><init>(Landroid/net/Uri;Landroid/content/Context;Landroid/os/Handler;LX/0Jv;LX/0Js;)V

    .line 40015
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    .line 40016
    iput-object p2, p0, LX/0K0;->a:LX/04G;

    .line 40017
    iput-object p3, p0, LX/0K0;->b:Ljava/lang/String;

    .line 40018
    new-instance v2, LX/0GB;

    invoke-direct {v2}, LX/0GB;-><init>()V

    iput-object v2, p0, LX/0K0;->h:LX/0GB;

    .line 40019
    new-instance v2, LX/0GN;

    iget-object v3, p0, LX/0K0;->h:LX/0GB;

    invoke-direct {v2, p4, v3}, LX/0GN;-><init>(Landroid/content/Context;LX/0GB;)V

    iput-object v2, p0, LX/0K0;->i:LX/0GN;

    .line 40020
    move/from16 v0, p8

    iput v0, p0, LX/0K0;->j:I

    .line 40021
    move/from16 v0, p9

    iput v0, p0, LX/0K0;->k:I

    .line 40022
    move-object/from16 v0, p10

    iput-object v0, p0, LX/0K0;->m:Ljava/lang/String;

    .line 40023
    move-object/from16 v0, p11

    iget-boolean v2, v0, LX/19j;->ae:Z

    iput-boolean v2, p0, LX/0K0;->n:Z

    .line 40024
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->t:Ljava/lang/String;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40025
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->u:Ljava/lang/String;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40026
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->d:Ljava/lang/String;

    const-string v4, "4000"

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40027
    move-object/from16 v0, p11

    iget-boolean v2, v0, LX/19j;->al:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    .line 40028
    :goto_0
    iget-object v3, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v4, LX/040;->ao:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40029
    move-object/from16 v0, p11

    iget-boolean v2, v0, LX/19j;->ae:Z

    if-eqz v2, :cond_0

    .line 40030
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->z:Ljava/lang/String;

    const-string v4, "10"

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40031
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->an:Ljava/lang/String;

    const-string v4, "1"

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40032
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->al:Ljava/lang/String;

    move-object/from16 v0, p11

    iget v4, v0, LX/19j;->ai:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40033
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->ap:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-wide v4, v0, LX/19j;->an:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40034
    iget-object v3, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v4, LX/040;->aq:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-boolean v2, v0, LX/19j;->ao:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40035
    iget-object v3, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v4, LX/040;->ar:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-boolean v2, v0, LX/19j;->ap:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40036
    :cond_0
    move-object/from16 v0, p11

    iget-boolean v2, v0, LX/19j;->aN:Z

    if-eqz v2, :cond_1

    .line 40037
    iget-object v3, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v4, LX/040;->e:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-boolean v2, v0, LX/19j;->aA:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40038
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->f:Ljava/lang/String;

    move-object/from16 v0, p11

    iget v4, v0, LX/19j;->aB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40039
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->g:Ljava/lang/String;

    move-object/from16 v0, p11

    iget v4, v0, LX/19j;->aC:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40040
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->h:Ljava/lang/String;

    move-object/from16 v0, p11

    iget v4, v0, LX/19j;->aD:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40041
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->i:Ljava/lang/String;

    move-object/from16 v0, p11

    iget v4, v0, LX/19j;->aE:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40042
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->j:Ljava/lang/String;

    move-object/from16 v0, p11

    iget v4, v0, LX/19j;->aF:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40043
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->k:Ljava/lang/String;

    move-object/from16 v0, p11

    iget v4, v0, LX/19j;->aG:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40044
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->m:Ljava/lang/String;

    move-object/from16 v0, p11

    iget v4, v0, LX/19j;->aH:F

    invoke-static {v4}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40045
    iget-object v2, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v3, LX/040;->n:Ljava/lang/String;

    move-object/from16 v0, p11

    iget v4, v0, LX/19j;->aI:F

    invoke-static {v4}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40046
    iget-object v3, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v4, LX/040;->l:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-boolean v2, v0, LX/19j;->aJ:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40047
    iget-object v3, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v4, LX/040;->o:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-boolean v2, v0, LX/19j;->aK:Z

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40048
    iget-object v3, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v4, LX/040;->p:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-boolean v2, v0, LX/19j;->aL:Z

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_6
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40049
    iget-object v3, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v4, LX/040;->r:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-boolean v2, v0, LX/19j;->aM:Z

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_7
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40050
    iget-object v3, p0, LX/0K0;->l:Ljava/util/Map;

    sget-object v4, LX/040;->s:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-boolean v2, v0, LX/19j;->aO:Z

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :goto_8
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40051
    :cond_1
    move-object/from16 v0, p12

    iput-object v0, p0, LX/0K0;->o:LX/0KZ;

    .line 40052
    return-void

    .line 40053
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 40054
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 40055
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 40056
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 40057
    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    .line 40058
    :cond_7
    const/4 v2, 0x0

    goto :goto_5

    .line 40059
    :cond_8
    const/4 v2, 0x0

    goto :goto_6

    .line 40060
    :cond_9
    const/4 v2, 0x0

    goto :goto_7

    .line 40061
    :cond_a
    const/4 v2, 0x0

    goto :goto_8
.end method


# virtual methods
.method public final a()LX/09L;
    .locals 1

    .prologue
    .line 40062
    sget-object v0, LX/09L;->DASH_LIVE:LX/09L;

    return-object v0
.end method

.method public final a(LX/0Jp;)V
    .locals 19

    .prologue
    .line 40063
    const/4 v13, 0x0

    .line 40064
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/0K0;->n:Z

    if-eqz v2, :cond_0

    .line 40065
    new-instance v2, LX/0AZ;

    invoke-direct {v2}, LX/0AZ;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0Ji;->c:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0K0;->m:Ljava/lang/String;

    invoke-static {v2, v3, v4}, LX/0Gj;->a(LX/0AZ;Ljava/lang/String;Ljava/lang/String;)LX/0AY;

    move-result-object v13

    .line 40066
    :cond_0
    new-instance v2, LX/0GK;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0Ji;->c:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Ji;->d:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Ji;->e:Landroid/os/Handler;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/0K0;->b:Ljava/lang/String;

    const-string v8, "default"

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, LX/0K0;->l:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/0K0;->i:LX/0GN;

    const/4 v12, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, LX/0K0;->h:LX/0GB;

    move-object/from16 v0, p0

    iget v0, v0, LX/0K0;->j:I

    move/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget v0, v0, LX/0K0;->k:I

    move/from16 v18, v0

    invoke-direct/range {v2 .. v18}, LX/0GK;-><init>(Landroid/net/Uri;Landroid/content/Context;Landroid/os/Handler;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;LX/0GN;LX/0Gk;LX/0AY;ZLX/0GB;IZI)V

    .line 40067
    new-instance v3, LX/0GR;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Ji;->c:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0K0;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0K0;->a:LX/04G;

    if-nez v6, :cond_1

    const/4 v6, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v7, v0, LX/0Ji;->d:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/0Ji;->e:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0K0;->l:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/0Ji;->f:LX/0Jv;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, LX/0K0;->o:LX/0KZ;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0K0;->h:LX/0GB;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v15, v2

    invoke-direct/range {v3 .. v18}, LX/0GR;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/os/Handler;Ljava/util/Map;LX/0Ju;LX/0Jr;LX/0KU;LX/0KZ;Landroid/net/Uri;LX/0GK;LX/0GB;LX/0Gk;Z)V

    .line 40068
    new-instance v2, LX/0Jz;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v2, v0, v1}, LX/0Jz;-><init>(LX/0K0;LX/0Jp;)V

    invoke-virtual {v3, v2}, LX/0GR;->a(LX/0GP;)V

    .line 40069
    return-void

    .line 40070
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0K0;->a:LX/04G;

    iget-object v6, v6, LX/04G;->value:Ljava/lang/String;

    goto :goto_0
.end method
