.class public final LX/02d;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final lockArt:Z

.field public final lockDexNum:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 9020
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/02d;-><init>(IZ)V

    .line 9021
    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 0

    .prologue
    .line 9022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9023
    iput p1, p0, LX/02d;->lockDexNum:I

    .line 9024
    iput-boolean p2, p0, LX/02d;->lockArt:Z

    .line 9025
    return-void
.end method

.method private static defaultQeExperimentValues()LX/02d;
    .locals 1

    .prologue
    .line 9026
    new-instance v0, LX/02d;

    invoke-direct {v0}, LX/02d;-><init>()V

    return-object v0
.end method

.method public static readExperimentFromDisk(Landroid/content/Context;)LX/02d;
    .locals 6

    .prologue
    .line 9027
    const-string v0, "fb4a_mlock_for_dex_files_enabled"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 9028
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 9029
    invoke-static {}, LX/02d;->defaultQeExperimentValues()LX/02d;

    move-result-object v0

    .line 9030
    :goto_0
    return-object v0

    .line 9031
    :cond_0
    const/4 v2, 0x0

    .line 9032
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9033
    const/16 v0, 0x100

    :try_start_1
    new-array v0, v0, [B

    .line 9034
    const/16 v2, 0x100

    invoke-static {v1, v0, v2}, LX/02Q;->slurp(Ljava/io/InputStream;[BI)I

    move-result v2

    .line 9035
    if-gtz v2, :cond_1

    .line 9036
    invoke-static {}, LX/02d;->defaultQeExperimentValues()LX/02d;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 9037
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    goto :goto_0

    .line 9038
    :cond_1
    const/4 v3, 0x0

    :try_start_2
    invoke-static {v0, v3, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 9039
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 9040
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v3

    if-nez v3, :cond_2

    .line 9041
    new-instance v0, LX/02d;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, LX/02d;-><init>(IZ)V

    move-object v0, v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 9042
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    goto :goto_0

    .line 9043
    :cond_2
    :try_start_3
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 9044
    new-instance v0, LX/02d;

    invoke-direct {v0, v2, v3}, LX/02d;-><init>(IZ)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 9045
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    goto :goto_0

    .line 9046
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 9047
    :goto_2
    :try_start_4
    const-string v2, "Cannot read file %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "fb4a_mlock_for_dex_files_enabled"

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/02P;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9048
    invoke-static {}, LX/02d;->defaultQeExperimentValues()LX/02d;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    .line 9049
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 9050
    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 9051
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/02d;

    if-nez v1, :cond_1

    .line 9052
    :cond_0
    :goto_0
    return v0

    .line 9053
    :cond_1
    check-cast p1, LX/02d;

    .line 9054
    iget v1, p0, LX/02d;->lockDexNum:I

    iget v2, p1, LX/02d;->lockDexNum:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, LX/02d;->lockArt:Z

    iget-boolean v2, p1, LX/02d;->lockArt:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 9055
    iget v0, p0, LX/02d;->lockDexNum:I

    add-int/lit8 v0, v0, 0x1f

    .line 9056
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, LX/02d;->lockArt:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 9057
    return v0

    .line 9058
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9059
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Lock Dex Num: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/02d;->lockDexNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Lock Art:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/02d;->lockArt:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeExperimentToDisk(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 9060
    const-string v0, "fb4a_mlock_for_dex_files_enabled"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 9061
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9062
    const/16 v0, 0x100

    :try_start_1
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 9063
    iget v3, p0, LX/02d;->lockDexNum:I

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 9064
    iget-boolean v3, p0, LX/02d;->lockArt:Z

    .line 9065
    if-eqz v3, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 9066
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 9067
    invoke-static {v2}, Ljava/nio/channels/Channels;->newChannel(Ljava/io/OutputStream;)Ljava/nio/channels/WritableByteChannel;

    move-result-object v1

    .line 9068
    invoke-interface {v1, v0}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 9069
    invoke-static {v2}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 9070
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 9071
    :goto_1
    return-void

    .line 9072
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 9073
    :goto_2
    :try_start_2
    const-string v3, "Cannot write to file %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "fb4a_mlock_for_dex_files_enabled"

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, LX/02P;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 9074
    invoke-static {v2}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 9075
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    goto :goto_1

    .line 9076
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    invoke-static {v2}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 9077
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    throw v0

    .line 9078
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 9079
    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v2, v1

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_2

    .line 9080
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
