.class public final enum LX/03W;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/03W;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/03W;

.field public static final enum GLOBAL:LX/03W;

.field public static final enum PROCESS:LX/03W;

.field public static final enum THREAD:LX/03W;


# instance fields
.field private final mCode:C


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 10452
    new-instance v0, LX/03W;

    const-string v1, "THREAD"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v3, v2}, LX/03W;-><init>(Ljava/lang/String;IC)V

    sput-object v0, LX/03W;->THREAD:LX/03W;

    .line 10453
    new-instance v0, LX/03W;

    const-string v1, "PROCESS"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v4, v2}, LX/03W;-><init>(Ljava/lang/String;IC)V

    sput-object v0, LX/03W;->PROCESS:LX/03W;

    .line 10454
    new-instance v0, LX/03W;

    const-string v1, "GLOBAL"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v5, v2}, LX/03W;-><init>(Ljava/lang/String;IC)V

    sput-object v0, LX/03W;->GLOBAL:LX/03W;

    .line 10455
    const/4 v0, 0x3

    new-array v0, v0, [LX/03W;

    sget-object v1, LX/03W;->THREAD:LX/03W;

    aput-object v1, v0, v3

    sget-object v1, LX/03W;->PROCESS:LX/03W;

    aput-object v1, v0, v4

    sget-object v1, LX/03W;->GLOBAL:LX/03W;

    aput-object v1, v0, v5

    sput-object v0, LX/03W;->$VALUES:[LX/03W;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IC)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C)V"
        }
    .end annotation

    .prologue
    .line 10456
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10457
    iput-char p3, p0, LX/03W;->mCode:C

    .line 10458
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/03W;
    .locals 1

    .prologue
    .line 10459
    const-class v0, LX/03W;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/03W;

    return-object v0
.end method

.method public static values()[LX/03W;
    .locals 1

    .prologue
    .line 10460
    sget-object v0, LX/03W;->$VALUES:[LX/03W;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/03W;

    return-object v0
.end method


# virtual methods
.method public final getCode()C
    .locals 1

    .prologue
    .line 10461
    iget-char v0, p0, LX/03W;->mCode:C

    return v0
.end method
