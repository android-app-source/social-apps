.class public LX/0DI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Landroid/view/View;

.field public c:Landroid/view/View;

.field public d:Landroid/widget/LinearLayout;

.field public e:LX/0D5;

.field public f:LX/0CQ;

.field public g:Landroid/os/Bundle;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29758
    const-class v0, LX/0DI;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0DI;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/widget/LinearLayout;Landroid/content/Intent;)V
    .locals 11

    .prologue
    .line 29759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29760
    iput-object p1, p0, LX/0DI;->b:Landroid/view/View;

    .line 29761
    iput-object p2, p0, LX/0DI;->c:Landroid/view/View;

    .line 29762
    iput-object p3, p0, LX/0DI;->d:Landroid/widget/LinearLayout;

    .line 29763
    invoke-static {}, LX/0CQ;->a()LX/0CQ;

    move-result-object v0

    iput-object v0, p0, LX/0DI;->f:LX/0CQ;

    .line 29764
    const-string v0, "BrowserLiteIntent.EXTRA_TRACKING"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, LX/0DI;->g:Landroid/os/Bundle;

    .line 29765
    const-string v0, "instant_experiences_webview_chrome_manifest"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 29766
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29767
    :goto_0
    return-void

    .line 29768
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x2

    const/4 v3, 0x0

    .line 29769
    iget-object v2, p0, LX/0DI;->d:Landroid/widget/LinearLayout;

    if-nez v2, :cond_2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29770
    :cond_1
    :goto_1
    goto :goto_0

    .line 29771
    :catch_0
    sget-object v1, LX/0DI;->a:Ljava/lang/String;

    const-string v2, "Failed to parse config string to JSON"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 29772
    :cond_2
    :try_start_1
    if-eqz v1, :cond_1

    .line 29773
    const-string v2, "chrome_actions"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 29774
    if-eqz v6, :cond_1

    .line 29775
    const-string v2, "chrome_icon_color"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v7, 0xff

    const/4 v9, 0x0

    .line 29776
    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_d

    .line 29777
    invoke-virtual {v2, v9}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    const/16 v5, 0x23

    if-eq v4, v5, :cond_3

    .line 29778
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "#"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 29779
    :cond_3
    :try_start_2
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    .line 29780
    invoke-static {v4}, Landroid/graphics/Color;->alpha(I)I

    move-result v5

    if-eq v5, v7, :cond_c

    .line 29781
    const/16 v5, 0xff

    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v7

    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v8

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    invoke-static {v5, v7, v8, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 29782
    :goto_2
    :try_start_3
    move-object v7, v4

    .line 29783
    iget-object v2, p0, LX/0DI;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    move v2, v3

    move v4, v3

    move v5, v3

    .line 29784
    :goto_3
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v2, v8, :cond_9

    .line 29785
    if-lt v4, p1, :cond_4

    if-nez v5, :cond_9

    .line 29786
    :cond_4
    invoke-virtual {v6, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 29787
    if-eqz v8, :cond_6

    .line 29788
    const-string v9, "action_type"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 29789
    if-eqz v9, :cond_6

    .line 29790
    if-nez v5, :cond_7

    const-string v10, "navigation"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 29791
    const/4 v9, 0x0

    .line 29792
    const-string v5, "navigation_urls"

    invoke-virtual {v8, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 29793
    if-nez v10, :cond_e

    .line 29794
    :cond_5
    :goto_4
    move v5, v9

    .line 29795
    :cond_6
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 29796
    :cond_7
    if-ge v4, p1, :cond_6

    .line 29797
    const-string v10, "basic"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 29798
    const/4 v9, 0x0

    .line 29799
    const-string v10, "action_id"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 29800
    const-string p2, "system_icon"

    invoke-virtual {v8, p2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 29801
    const-string p3, "redirect_url"

    invoke-virtual {v8, p3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 29802
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p4

    if-nez p4, :cond_8

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p4

    if-nez p4, :cond_8

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p4

    if-eqz p4, :cond_16

    .line 29803
    :cond_8
    :goto_6
    move v8, v9

    .line 29804
    if-eqz v8, :cond_6

    .line 29805
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 29806
    :cond_9
    if-nez v5, :cond_a

    if-lez v4, :cond_b

    .line 29807
    :cond_a
    iget-object v2, p0, LX/0DI;->c:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 29808
    :cond_b
    iget-object v2, p0, LX/0DI;->c:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 29809
    :cond_c
    :try_start_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v4

    goto/16 :goto_2

    .line 29810
    :catch_1
    sget-object v4, LX/0DI;->a:Ljava/lang/String;

    const-string v5, "Invalid chrome icon color provided in config. Defaulting to black."

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v4, v5, v7}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29811
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 29812
    :cond_e
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    move v5, v9

    .line 29813
    :goto_7
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result p3

    if-ge v5, p3, :cond_10

    .line 29814
    invoke-virtual {v10, v5}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object p3

    .line 29815
    if-eqz p3, :cond_f

    .line 29816
    const-string p4, "label"

    invoke-virtual {p3, p4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 29817
    const-string v1, "url"

    invoke-virtual {p3, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 29818
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static {p3}, LX/0DK;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 29819
    invoke-virtual {p2, p4, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29820
    :cond_f
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 29821
    :cond_10
    invoke-virtual {p2}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    .line 29822
    iget-object v5, p0, LX/0DI;->b:Landroid/view/View;

    sget v10, LX/0E2;->t:I

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewStub;

    .line 29823
    sget v10, LX/0E2;->u:I

    invoke-virtual {v5, v10}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 29824
    invoke-virtual {v5}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    .line 29825
    new-instance v10, LX/0DK;

    iget-object p3, p0, LX/0DI;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p3}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {v10, p3}, LX/0DK;-><init>(Landroid/content/Context;)V

    .line 29826
    invoke-virtual {v5, v10}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 29827
    new-instance p3, LX/0DF;

    invoke-direct {p3, p0, v10, v5}, LX/0DF;-><init>(LX/0DI;LX/0DK;Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)V

    invoke-virtual {v5, p3}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->setOnItemClickedListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 29828
    iget-object p3, v10, LX/0DK;->a:Ljava/util/ArrayList;

    if-nez p3, :cond_12

    .line 29829
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    iput-object p3, v10, LX/0DK;->a:Ljava/util/ArrayList;

    .line 29830
    :goto_8
    iget-object p3, v10, LX/0DK;->b:Ljava/util/ArrayList;

    if-nez p3, :cond_13

    .line 29831
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    iput-object p3, v10, LX/0DK;->b:Ljava/util/ArrayList;

    .line 29832
    :goto_9
    invoke-virtual {p2}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_11
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/Map$Entry;

    .line 29833
    invoke-interface {p3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/String;

    .line 29834
    invoke-interface {p3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    .line 29835
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_11

    .line 29836
    invoke-static {p3}, LX/0DK;->a(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 29837
    iget-object v8, v10, LX/0DK;->a:Ljava/util/ArrayList;

    invoke-virtual {v8, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29838
    iget-object p4, v10, LX/0DK;->b:Ljava/util/ArrayList;

    invoke-virtual {p4, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 29839
    :cond_12
    iget-object p3, v10, LX/0DK;->a:Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->clear()V

    goto :goto_8

    .line 29840
    :cond_13
    iget-object p3, v10, LX/0DK;->b:Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->clear()V

    goto :goto_9

    .line 29841
    :cond_14
    const p3, 0x6a22968d

    invoke-static {v10, p3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 29842
    iget-object v10, p0, LX/0DI;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    sget p2, LX/0E2;->j:I

    iget-object p3, p0, LX/0DI;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v10, p2, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 29843
    sget v10, LX/0E2;->m:I

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 29844
    if-eqz v7, :cond_15

    .line 29845
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v10

    sget-object p2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v9, v10, p2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 29846
    :cond_15
    new-instance v10, LX/0DG;

    invoke-direct {v10, p0, v5}, LX/0DG;-><init>(LX/0DI;Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)V

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29847
    iget-object v5, p0, LX/0DI;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 29848
    const/4 v9, 0x1

    goto/16 :goto_4

    .line 29849
    :cond_16
    const/4 p4, 0x0

    .line 29850
    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_17
    :goto_b
    packed-switch v0, :pswitch_data_0

    .line 29851
    :goto_c
    move p2, p4

    .line 29852
    if-eqz p2, :cond_8

    .line 29853
    invoke-static {p3}, LX/0DK;->a(Ljava/lang/String;)Z

    move-result p4

    if-eqz p4, :cond_8

    .line 29854
    iget-object p4, p0, LX/0DI;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p4}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p4

    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p4

    sget v0, LX/0E2;->j:I

    iget-object v1, p0, LX/0DI;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p4, v0, v1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 29855
    invoke-virtual {v9, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 29856
    if-eqz v7, :cond_18

    .line 29857
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result p2

    sget-object p4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v9, p2, p4}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 29858
    :cond_18
    new-instance p2, LX/0DH;

    invoke-direct {p2, p0, v10, p3}, LX/0DH;-><init>(LX/0DI;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29859
    iget-object v10, p0, LX/0DI;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v9, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 29860
    const/4 v9, 0x1

    goto/16 :goto_6

    .line 29861
    :sswitch_0
    const-string v1, "view_cart_drawable"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    move v0, p4

    goto :goto_b

    :sswitch_1
    const-string v1, "search_drawable"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    const/4 v0, 0x1

    goto :goto_b

    .line 29862
    :pswitch_0
    sget p4, LX/0E2;->l:I

    goto :goto_c

    .line 29863
    :pswitch_1
    sget p4, LX/0E2;->k:I

    goto :goto_c

    nop

    :sswitch_data_0
    .sparse-switch
        -0x12c4d6ab -> :sswitch_1
        0x3ffec2c3 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 29864
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 29865
    array-length v5, p1

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v0, p1, v2

    .line 29866
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 29867
    const-string v1, ""

    .line 29868
    :try_start_0
    check-cast v0, Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-virtual {v0, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v6, 0x2

    invoke-static {v0, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 29869
    :goto_1
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29870
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 29871
    :catch_0
    sget-object v0, LX/0DI;->a:Ljava/lang/String;

    const-string v6, "Could not encode string to base64 for JS format."

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_1

    .line 29872
    :cond_0
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 29873
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, p0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
