.class public LX/06K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x3
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final h:Landroid/content/Context;

.field private final i:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final j:Landroid/app/AlarmManager;

.field public final k:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field public final l:I

.field private final m:Landroid/os/Handler;

.field private final n:Landroid/content/BroadcastReceiver;

.field private final o:Landroid/content/BroadcastReceiver;

.field private final p:Landroid/content/BroadcastReceiver;

.field public final q:Landroid/app/PendingIntent;

.field public final r:Landroid/app/PendingIntent;

.field public final s:Landroid/app/PendingIntent;

.field private final t:Ljava/lang/String;

.field public volatile u:Ljava/lang/Runnable;

.field public v:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public w:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public x:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public y:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17673
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/06K;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ACTION_INEXACT_ALARM."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/06K;->a:Ljava/lang/String;

    .line 17674
    new-instance v0, LX/06L;

    invoke-direct {v0}, LX/06L;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/06K;->e:Ljava/util/List;

    .line 17675
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/06K;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ACTION_EXACT_ALARM."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/06K;->f:Ljava/lang/String;

    .line 17676
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/06K;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ACTION_BACKUP_ALARM."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/06K;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Landroid/app/AlarmManager;Landroid/os/Handler;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.app.PendingIntent.getBroadcast"
        }
    .end annotation

    .prologue
    const/high16 v3, 0x8000000

    const/4 v2, 0x0

    .line 17771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17772
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/06K;->y:J

    .line 17773
    iput-object p1, p0, LX/06K;->h:Landroid/content/Context;

    .line 17774
    iput-object p2, p0, LX/06K;->t:Ljava/lang/String;

    .line 17775
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/04u;->a(Ljava/lang/String;)Z

    move-result v0

    move v0, v0

    .line 17776
    iput-boolean v0, p0, LX/06K;->z:Z

    .line 17777
    iput-object p3, p0, LX/06K;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 17778
    iput-object p5, p0, LX/06K;->j:Landroid/app/AlarmManager;

    .line 17779
    iput-object p4, p0, LX/06K;->k:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 17780
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, LX/06K;->l:I

    .line 17781
    iput-object p6, p0, LX/06K;->m:Landroid/os/Handler;

    .line 17782
    new-instance v0, LX/06M;

    invoke-direct {v0, p0}, LX/06M;-><init>(LX/06K;)V

    iput-object v0, p0, LX/06K;->n:Landroid/content/BroadcastReceiver;

    .line 17783
    sget-object v0, LX/06K;->f:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, LX/06K;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/06K;->b:Ljava/lang/String;

    .line 17784
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/06K;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 17785
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 17786
    invoke-static {p1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, LX/06K;->q:Landroid/app/PendingIntent;

    .line 17787
    new-instance v0, LX/06N;

    invoke-direct {v0, p0}, LX/06N;-><init>(LX/06K;)V

    iput-object v0, p0, LX/06K;->o:Landroid/content/BroadcastReceiver;

    .line 17788
    sget-object v0, LX/06K;->a:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, LX/06K;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/06K;->c:Ljava/lang/String;

    .line 17789
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/06K;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 17790
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 17791
    invoke-static {p1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, LX/06K;->r:Landroid/app/PendingIntent;

    .line 17792
    new-instance v0, LX/06O;

    invoke-direct {v0, p0}, LX/06O;-><init>(LX/06K;)V

    iput-object v0, p0, LX/06K;->p:Landroid/content/BroadcastReceiver;

    .line 17793
    sget-object v0, LX/06K;->g:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, LX/06K;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/06K;->d:Ljava/lang/String;

    .line 17794
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/06K;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 17795
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 17796
    invoke-static {p1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, LX/06K;->s:Landroid/app/PendingIntent;

    .line 17797
    return-void
.end method

.method private static a(J)J
    .locals 6

    .prologue
    const-wide/32 v2, 0xdbba0

    .line 17765
    cmp-long v0, p0, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/01n;->a(Z)V

    .line 17766
    sget-object v0, LX/06K;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 17767
    cmp-long v5, p0, v0

    if-ltz v5, :cond_0

    .line 17768
    :goto_1
    return-wide v0

    .line 17769
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-wide v0, v2

    .line 17770
    goto :goto_1
.end method

.method private a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 17760
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/06K;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 17761
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 17762
    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 17763
    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17764
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(JJ)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetRepeatingUse",
            "BadMethodUse-android.app.AlarmManager.setRepeating"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 17751
    iget v0, p0, LX/06K;->l:I

    const/16 v2, 0x17

    if-lt v0, v2, :cond_0

    iget-boolean v0, p0, LX/06K;->z:Z

    if-eqz v0, :cond_0

    .line 17752
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v2, p0, LX/06K;->q:Landroid/app/PendingIntent;

    .line 17753
    invoke-virtual {v0, v1, p1, p2, v2}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    .line 17754
    :goto_0
    return-void

    .line 17755
    :cond_0
    iget v0, p0, LX/06K;->l:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_1

    .line 17756
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v2, p0, LX/06K;->q:Landroid/app/PendingIntent;

    .line 17757
    invoke-virtual {v0, v1, p1, p2, v2}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 17758
    goto :goto_0

    .line 17759
    :cond_1
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v6, p0, LX/06K;->q:Landroid/app/PendingIntent;

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static b(LX/06K;JJ)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.app.AlarmManager.setInexactRepeating",
            "SetInexactRepeatingArgs"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 17746
    iget v0, p0, LX/06K;->l:I

    const/16 v2, 0x17

    if-lt v0, v2, :cond_0

    iget-boolean v0, p0, LX/06K;->z:Z

    if-eqz v0, :cond_0

    .line 17747
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v2, p0, LX/06K;->r:Landroid/app/PendingIntent;

    .line 17748
    invoke-virtual {v0, v1, p1, p2, v2}, Landroid/app/AlarmManager;->setAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    .line 17749
    :goto_0
    return-void

    .line 17750
    :cond_0
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v6, p0, LX/06K;->r:Landroid/app/PendingIntent;

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static b$redex0(LX/06K;J)V
    .locals 3
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 17737
    iget v0, p0, LX/06K;->l:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    iget-boolean v0, p0, LX/06K;->z:Z

    if-eqz v0, :cond_0

    .line 17738
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/06K;->s:Landroid/app/PendingIntent;

    .line 17739
    invoke-virtual {v0, v2, p1, p2, v1}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    .line 17740
    :goto_0
    return-void

    .line 17741
    :cond_0
    iget v0, p0, LX/06K;->l:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 17742
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/06K;->s:Landroid/app/PendingIntent;

    .line 17743
    invoke-virtual {v0, v2, p1, p2, v1}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 17744
    goto :goto_0

    .line 17745
    :cond_1
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/06K;->s:Landroid/app/PendingIntent;

    invoke-virtual {v0, v2, p1, p2, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 17724
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/06K;->d()V

    .line 17725
    iget-object v0, p0, LX/06K;->u:Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 17726
    :try_start_1
    iget-object v0, p0, LX/06K;->h:Landroid/content/Context;

    iget-object v1, p0, LX/06K;->n:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17727
    :goto_0
    :try_start_2
    iget-object v0, p0, LX/06K;->h:Landroid/content/Context;

    iget-object v1, p0, LX/06K;->o:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 17728
    :goto_1
    :try_start_3
    iget-object v0, p0, LX/06K;->h:Landroid/content/Context;

    iget-object v1, p0, LX/06K;->p:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 17729
    :cond_0
    :goto_2
    monitor-exit p0

    return-void

    .line 17730
    :catch_0
    move-exception v0

    .line 17731
    :try_start_4
    const-string v1, "KeepaliveManager"

    const-string v2, "Failed to unregister broadcast receiver"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 17732
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 17733
    :catch_1
    move-exception v0

    .line 17734
    :try_start_5
    const-string v1, "KeepaliveManager"

    const-string v2, "Failed to unregister broadcast receiver"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 17735
    :catch_2
    move-exception v0

    .line 17736
    const-string v1, "KeepaliveManager"

    const-string v2, "Failed to unregister broadcast receiver"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2
.end method

.method public final declared-synchronized a(Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    .line 17717
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06K;->u:Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 17718
    :goto_0
    monitor-exit p0

    return-void

    .line 17719
    :cond_0
    :try_start_1
    iput-object p1, p0, LX/06K;->u:Ljava/lang/Runnable;

    .line 17720
    iget-object v0, p0, LX/06K;->h:Landroid/content/Context;

    iget-object v1, p0, LX/06K;->n:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    iget-object v3, p0, LX/06K;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    iget-object v4, p0, LX/06K;->m:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 17721
    iget-object v0, p0, LX/06K;->h:Landroid/content/Context;

    iget-object v1, p0, LX/06K;->o:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    iget-object v3, p0, LX/06K;->c:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    iget-object v4, p0, LX/06K;->m:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 17722
    iget-object v0, p0, LX/06K;->h:Landroid/content/Context;

    iget-object v1, p0, LX/06K;->p:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    iget-object v3, p0, LX/06K;->d:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    iget-object v4, p0, LX/06K;->m:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 17723
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 8

    .prologue
    .line 17712
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/06K;->v:Z

    if-nez v0, :cond_0

    .line 17713
    invoke-virtual {p0}, LX/06K;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17714
    :goto_0
    monitor-exit p0

    return-void

    .line 17715
    :cond_0
    :try_start_1
    const-string v0, "KeepaliveManager"

    const-string v1, "keepalive/no_op; nextWakeupSec=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, LX/06K;->x:J

    iget-object v6, p0, LX/06K;->k:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v6}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 17716
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 10

    .prologue
    const-wide/32 v4, 0xdbba0

    .line 17687
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06K;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    .line 17688
    cmp-long v2, v0, v4

    if-lez v2, :cond_0

    .line 17689
    invoke-static {v0, v1}, LX/06K;->a(J)J

    move-result-wide v0

    .line 17690
    :cond_0
    iput-wide v0, p0, LX/06K;->w:J

    .line 17691
    iget-object v0, p0, LX/06K;->k:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    .line 17692
    iget-wide v2, p0, LX/06K;->w:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/06K;->x:J

    .line 17693
    iget-boolean v0, p0, LX/06K;->v:Z

    if-eqz v0, :cond_3

    .line 17694
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/06K;->q:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 17695
    iget-boolean v0, p0, LX/06K;->z:Z

    if-nez v0, :cond_1

    .line 17696
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/06K;->s:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17697
    :cond_1
    :goto_0
    :try_start_1
    iget-wide v0, p0, LX/06K;->w:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_4

    .line 17698
    iget-wide v0, p0, LX/06K;->x:J

    iget-wide v2, p0, LX/06K;->w:J

    invoke-direct {p0, v0, v1, v2, v3}, LX/06K;->a(JJ)V

    .line 17699
    :cond_2
    :goto_1
    const-string v0, "KeepaliveManager"

    const-string v1, "keepalive/start; intervalSec=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, LX/06K;->w:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17700
    :goto_2
    monitor-exit p0

    return-void

    .line 17701
    :cond_3
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LX/06K;->v:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 17702
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 17703
    :cond_4
    :try_start_3
    iget-wide v0, p0, LX/06K;->y:J

    iget-wide v2, p0, LX/06K;->w:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 17704
    iget-wide v0, p0, LX/06K;->w:J

    iput-wide v0, p0, LX/06K;->y:J

    .line 17705
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/06K;->r:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 17706
    iget-wide v0, p0, LX/06K;->x:J

    iget-wide v2, p0, LX/06K;->w:J

    invoke-static {p0, v0, v1, v2, v3}, LX/06K;->b(LX/06K;JJ)V

    .line 17707
    :cond_5
    iget-boolean v0, p0, LX/06K;->z:Z

    if-nez v0, :cond_2

    .line 17708
    iget-wide v0, p0, LX/06K;->x:J

    const-wide/16 v2, 0x4e20

    add-long/2addr v0, v2

    invoke-static {p0, v0, v1}, LX/06K;->b$redex0(LX/06K;J)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 17709
    :catch_0
    move-exception v0

    .line 17710
    :try_start_4
    const-string v1, "KeepaliveManager"

    const-string v2, "keepalive/alarm_failed; intervalSec=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, LX/06K;->w:J

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17711
    invoke-virtual {p0}, LX/06K;->d()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method

.method public final declared-synchronized d()V
    .locals 3

    .prologue
    .line 17677
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/06K;->v:Z

    if-eqz v0, :cond_0

    .line 17678
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/06K;->v:Z

    .line 17679
    const-string v0, "KeepaliveManager"

    const-string v1, "keepalive/stop"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17680
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/06K;->r:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 17681
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/06K;->s:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 17682
    iget-object v0, p0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/06K;->q:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 17683
    :cond_0
    const-wide/32 v0, 0xdbba0

    iput-wide v0, p0, LX/06K;->w:J

    .line 17684
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/06K;->y:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17685
    monitor-exit p0

    return-void

    .line 17686
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
