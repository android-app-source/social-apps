.class public final LX/086;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private mArgv:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mEnvironCache:[B

.field public mEnvironKeys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mEnvironValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mExecutable:Ljava/lang/String;

.field private mKeepFds:Ljava/util/BitSet;

.field private mStreamDispositions:[I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 20741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20742
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/086;->mArgv:Ljava/util/ArrayList;

    .line 20743
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, LX/086;->mKeepFds:Ljava/util/BitSet;

    .line 20744
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, LX/086;->mStreamDispositions:[I

    .line 20745
    iget-object v0, p0, LX/086;->mKeepFds:Ljava/util/BitSet;

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->set(I)V

    .line 20746
    iget-object v0, p0, LX/086;->mKeepFds:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 20747
    iget-object v0, p0, LX/086;->mKeepFds:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 20748
    const-string v0, "ANDROID_PROPERTY_WORKSPACE"

    invoke-static {v0}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 20749
    if-eqz v0, :cond_0

    .line 20750
    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 20751
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 20752
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 20753
    :try_start_0
    iget-object v1, p0, LX/086;->mKeepFds:Ljava/util/BitSet;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->set(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20754
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0

    .line 20755
    nop

    :array_0
    .array-data 4
        -0x3
        -0x3
        -0x3
    .end array-data
.end method

.method public varargs constructor <init>(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 20780
    invoke-direct {p0}, LX/086;-><init>()V

    .line 20781
    iput-object p1, p0, LX/086;->mExecutable:Ljava/lang/String;

    .line 20782
    iget-object v0, p0, LX/086;->mArgv:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20783
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 20784
    iget-object v1, p0, LX/086;->mArgv:Ljava/util/ArrayList;

    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20785
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 20786
    :cond_0
    return-void
.end method

.method private static buildEnvironBlock(Ljava/util/ArrayList;Ljava/util/ArrayList;)[B
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)[B"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 20764
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    move v3, v1

    .line 20765
    :goto_0
    if-ge v2, v4, :cond_0

    .line 20766
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v5

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 20767
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 20768
    :cond_0
    :try_start_0
    new-instance v2, LX/08C;

    invoke-direct {v2, v3}, LX/08C;-><init>(I)V

    .line 20769
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v3, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 20770
    :goto_1
    if-ge v1, v4, :cond_1

    .line 20771
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 20772
    const/16 v0, 0x3d

    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 20773
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 20774
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 20775
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 20776
    :cond_1
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V

    .line 20777
    invoke-virtual {v2}, LX/08C;->getRawBuffer()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 20778
    :catch_0
    move-exception v0

    .line 20779
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static getArrayOfSetBits(Ljava/util/BitSet;)[I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 20759
    invoke-virtual {p0}, Ljava/util/BitSet;->cardinality()I

    move-result v1

    new-array v3, v1, [I

    .line 20760
    invoke-virtual {p0, v0}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v1

    move v2, v1

    :goto_0
    const/4 v1, -0x1

    if-eq v2, v1, :cond_0

    .line 20761
    add-int/lit8 v1, v0, 0x1

    aput v2, v3, v0

    .line 20762
    add-int/lit8 v0, v2, 0x1

    invoke-virtual {p0, v0}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    move v2, v0

    move v0, v1

    goto :goto_0

    .line 20763
    :cond_0
    return-object v3
.end method

.method private makeEnvironBlock()[B
    .locals 2

    .prologue
    .line 20756
    iget-object v0, p0, LX/086;->mEnvironKeys:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/086;->mEnvironCache:[B

    if-nez v0, :cond_0

    .line 20757
    iget-object v0, p0, LX/086;->mEnvironKeys:Ljava/util/ArrayList;

    iget-object v1, p0, LX/086;->mEnvironValues:Ljava/util/ArrayList;

    invoke-static {v0, v1}, LX/086;->buildEnvironBlock(Ljava/util/ArrayList;Ljava/util/ArrayList;)[B

    move-result-object v0

    iput-object v0, p0, LX/086;->mEnvironCache:[B

    .line 20758
    :cond_0
    iget-object v0, p0, LX/086;->mEnvironCache:[B

    return-object v0
.end method


# virtual methods
.method public final addArgument(Ljava/lang/String;)LX/086;
    .locals 1

    .prologue
    .line 20739
    iget-object v0, p0, LX/086;->mArgv:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20740
    return-object p0
.end method

.method public final addArguments(Ljava/lang/Iterable;)LX/086;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/086;"
        }
    .end annotation

    .prologue
    .line 20736
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 20737
    invoke-virtual {p0, v0}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    goto :goto_0

    .line 20738
    :cond_0
    return-object p0
.end method

.method public final varargs addArguments([Ljava/lang/String;)LX/086;
    .locals 3

    .prologue
    .line 20732
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 20733
    iget-object v1, p0, LX/086;->mArgv:Ljava/util/ArrayList;

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20734
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 20735
    :cond_0
    return-object p0
.end method

.method public final clearenv()LX/086;
    .locals 1

    .prologue
    .line 20787
    iget-object v0, p0, LX/086;->mEnvironKeys:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 20788
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/086;->mEnvironKeys:Ljava/util/ArrayList;

    .line 20789
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/086;->mEnvironValues:Ljava/util/ArrayList;

    .line 20790
    :goto_0
    return-object p0

    .line 20791
    :cond_0
    iget-object v0, p0, LX/086;->mEnvironKeys:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 20792
    iget-object v0, p0, LX/086;->mEnvironValues:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 20793
    const/4 v0, 0x0

    iput-object v0, p0, LX/086;->mEnvironCache:[B

    goto :goto_0
.end method

.method public final clone()LX/086;
    .locals 2

    .prologue
    .line 20719
    invoke-direct {p0}, LX/086;->makeEnvironBlock()[B

    .line 20720
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/086;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20721
    iget-object v1, p0, LX/086;->mArgv:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, v0, LX/086;->mArgv:Ljava/util/ArrayList;

    .line 20722
    iget-object v1, p0, LX/086;->mKeepFds:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/BitSet;

    iput-object v1, v0, LX/086;->mKeepFds:Ljava/util/BitSet;

    .line 20723
    iget-object v1, p0, LX/086;->mStreamDispositions:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iput-object v1, v0, LX/086;->mStreamDispositions:[I

    .line 20724
    iget-object v1, p0, LX/086;->mEnvironKeys:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 20725
    iget-object v1, p0, LX/086;->mEnvironKeys:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, v0, LX/086;->mEnvironKeys:Ljava/util/ArrayList;

    .line 20726
    iget-object v1, p0, LX/086;->mEnvironValues:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, v0, LX/086;->mEnvironValues:Ljava/util/ArrayList;

    .line 20727
    :cond_0
    iget-object v1, p0, LX/086;->mEnvironCache:[B

    if-eqz v1, :cond_1

    .line 20728
    iget-object v1, p0, LX/086;->mEnvironCache:[B

    invoke-virtual {v1}, [B->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, v0, LX/086;->mEnvironCache:[B

    .line 20729
    :cond_1
    return-object v0

    .line 20730
    :catch_0
    move-exception v0

    .line 20731
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20718
    invoke-virtual {p0}, LX/086;->clone()LX/086;

    move-result-object v0

    return-object v0
.end method

.method public final create()Lcom/facebook/forker/Process;
    .locals 6

    .prologue
    .line 20717
    new-instance v0, Lcom/facebook/forker/Process;

    iget-object v1, p0, LX/086;->mExecutable:Ljava/lang/String;

    iget-object v2, p0, LX/086;->mArgv:Ljava/util/ArrayList;

    iget-object v3, p0, LX/086;->mArgv:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-direct {p0}, LX/086;->makeEnvironBlock()[B

    move-result-object v3

    iget-object v4, p0, LX/086;->mKeepFds:Ljava/util/BitSet;

    invoke-static {v4}, LX/086;->getArrayOfSetBits(Ljava/util/BitSet;)[I

    move-result-object v4

    iget-object v5, p0, LX/086;->mStreamDispositions:[I

    invoke-direct/range {v0 .. v5}, Lcom/facebook/forker/Process;-><init>(Ljava/lang/String;[Ljava/lang/String;[B[I[I)V

    return-object v0
.end method

.method public final setExecutable(Ljava/lang/String;)LX/086;
    .locals 0

    .prologue
    .line 20671
    iput-object p1, p0, LX/086;->mExecutable:Ljava/lang/String;

    .line 20672
    return-object p0
.end method

.method public final setFdCloseOnExec(IZ)LX/086;
    .locals 1

    .prologue
    .line 20713
    if-eqz p2, :cond_0

    .line 20714
    iget-object v0, p0, LX/086;->mKeepFds:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->clear(I)V

    .line 20715
    :goto_0
    return-object p0

    .line 20716
    :cond_0
    iget-object v0, p0, LX/086;->mKeepFds:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->set(I)V

    goto :goto_0
.end method

.method public final setStream(II)LX/086;
    .locals 1

    .prologue
    .line 20711
    iget-object v0, p0, LX/086;->mStreamDispositions:[I

    aput p2, v0, p1

    .line 20712
    return-object p0
.end method

.method public final setenv(Ljava/lang/String;Ljava/lang/String;)LX/086;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 20684
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p2, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 20685
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "environment variables cannot contain NUL"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20686
    :cond_1
    const/16 v0, 0x3d

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-eq v0, v1, :cond_2

    .line 20687
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "environment variable names cannot contain \'=\'"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20688
    :cond_2
    iget-object v0, p0, LX/086;->mEnvironKeys:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    .line 20689
    invoke-static {}, Ljava/lang/System;->getenv()Ljava/util/Map;

    move-result-object v0

    .line 20690
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    .line 20691
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 20692
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 20693
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 20694
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20695
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 20696
    :cond_3
    iput-object v2, p0, LX/086;->mEnvironKeys:Ljava/util/ArrayList;

    .line 20697
    iput-object v3, p0, LX/086;->mEnvironValues:Ljava/util/ArrayList;

    .line 20698
    :cond_4
    iget-object v1, p0, LX/086;->mEnvironKeys:Ljava/util/ArrayList;

    .line 20699
    iget-object v2, p0, LX/086;->mEnvironValues:Ljava/util/ArrayList;

    .line 20700
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 20701
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_5

    .line 20702
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 20703
    const/4 v3, 0x0

    iput-object v3, p0, LX/086;->mEnvironCache:[B

    .line 20704
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 20705
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 20706
    :cond_5
    iget-object v0, p0, LX/086;->mEnvironKeys:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20707
    iget-object v0, p0, LX/086;->mEnvironValues:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20708
    const/4 v0, 0x0

    iput-object v0, p0, LX/086;->mEnvironCache:[B

    .line 20709
    return-object p0

    .line 20710
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 20673
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 20674
    new-instance v4, Ljava/io/PrintWriter;

    invoke-direct {v4, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 20675
    const-string v0, "<ProcessBuilder executable=[%s] argv=["

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v5, p0, LX/086;->mExecutable:Ljava/lang/String;

    aput-object v5, v1, v2

    invoke-virtual {v4, v0, v1}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    move v1, v2

    .line 20676
    :goto_0
    iget-object v0, p0, LX/086;->mArgv:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 20677
    if-lez v1, :cond_0

    .line 20678
    const-string v0, " "

    invoke-virtual {v4, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 20679
    :cond_0
    iget-object v0, p0, LX/086;->mArgv:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 20680
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 20681
    :cond_1
    const-string v0, "] keepFds=[%s] streamDispositions=[%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, LX/086;->mKeepFds:Ljava/util/BitSet;

    invoke-static {v5}, LX/086;->getArrayOfSetBits(Ljava/util/BitSet;)[I

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    iget-object v2, p0, LX/086;->mStreamDispositions:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v4, v0, v1}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 20682
    const-string v0, ">"

    invoke-virtual {v4, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 20683
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
