.class public LX/03h;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10668
    new-instance v0, LX/03i;

    invoke-direct {v0}, LX/03i;-><init>()V

    sput-object v0, LX/03h;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)V
    .locals 2

    .prologue
    .line 10670
    sget-object v0, LX/03h;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_0

    .line 10671
    const/16 v0, 0x8

    const/16 v1, 0x19

    invoke-static {v0, v1, p0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    .line 10672
    :cond_0
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 2

    .prologue
    .line 10673
    const/16 v0, 0x8

    const/16 v1, 0x1e

    invoke-static {v0, v1, p1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    .line 10674
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 10675
    sget-object v0, LX/03h;->a:Ljava/lang/ThreadLocal;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 10676
    return-void
.end method

.method public static b(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 4

    .prologue
    const/16 v3, 0x1f

    const/16 v2, 0x8

    .line 10677
    :try_start_0
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 10678
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    if-nez v0, :cond_0

    .line 10679
    sget-object v0, LX/03h;->a:Ljava/lang/ThreadLocal;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10680
    :cond_0
    invoke-static {v2, v3, p1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    .line 10681
    return-void

    .line 10682
    :catchall_0
    move-exception v0

    invoke-static {v2, v3, p1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    throw v0
.end method
