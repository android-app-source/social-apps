.class public final LX/0FO;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic this$0:Lcom/facebook/common/dextricks/DexOptimization$Service;


# direct methods
.method public constructor <init>(Lcom/facebook/common/dextricks/DexOptimization$Service;)V
    .locals 0

    .prologue
    .line 33100
    iput-object p1, p0, LX/0FO;->this$0:Lcom/facebook/common/dextricks/DexOptimization$Service;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/common/dextricks/DexOptimization$Service;LX/0FL;)V
    .locals 0

    .prologue
    .line 33101
    invoke-direct {p0, p1}, LX/0FO;-><init>(Lcom/facebook/common/dextricks/DexOptimization$Service;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v7, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    const/16 v0, 0x26

    const v1, -0x752c2c06

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 33102
    if-eqz p2, :cond_0

    .line 33103
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 33104
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 33105
    iget-object v1, p0, LX/0FO;->this$0:Lcom/facebook/common/dextricks/DexOptimization$Service;

    .line 33106
    iput-boolean v8, v1, Lcom/facebook/common/dextricks/DexOptimization$Service;->isScreenOn:Z

    .line 33107
    const-string v1, "[optsvc] noticed screen on"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33108
    :cond_0
    :goto_0
    const v1, -0x50fa1426

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    :goto_1
    return-void

    .line 33109
    :cond_1
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 33110
    iget-object v1, p0, LX/0FO;->this$0:Lcom/facebook/common/dextricks/DexOptimization$Service;

    .line 33111
    iput-boolean v6, v1, Lcom/facebook/common/dextricks/DexOptimization$Service;->isScreenOn:Z

    .line 33112
    const-string v1, "[optsvc] noticed screen off"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 33113
    :cond_2
    const-string v2, "com.facebook.dexopt-pause"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33114
    const-string v1, "com.facebook.dexopt-unpause-time"

    const-wide/16 v2, -0x1

    invoke-virtual {p2, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 33115
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 33116
    const-string v1, "bogus pause broadcast received"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33117
    const v1, -0x75d4fcd6

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_1

    .line 33118
    :cond_3
    iget-object v1, p0, LX/0FO;->this$0:Lcom/facebook/common/dextricks/DexOptimization$Service;

    iget-object v4, p0, LX/0FO;->this$0:Lcom/facebook/common/dextricks/DexOptimization$Service;

    iget-wide v4, v4, Lcom/facebook/common/dextricks/DexOptimization$Service;->unpauseTime:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 33119
    iput-wide v2, v1, Lcom/facebook/common/dextricks/DexOptimization$Service;->unpauseTime:J

    .line 33120
    const-string v1, "pause broadcast received: will unpause at time %s (%s ms from now)"

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, LX/0FO;->this$0:Lcom/facebook/common/dextricks/DexOptimization$Service;

    iget-wide v4, v3, Lcom/facebook/common/dextricks/DexOptimization$Service;->unpauseTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    iget-object v3, p0, LX/0FO;->this$0:Lcom/facebook/common/dextricks/DexOptimization$Service;

    iget-wide v6, v3, Lcom/facebook/common/dextricks/DexOptimization$Service;->unpauseTime:J

    sub-long/2addr v4, v6

    const-wide/32 v6, 0xf4240

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0
.end method
