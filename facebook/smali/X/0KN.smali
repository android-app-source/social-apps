.class public LX/0KN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0KM;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/io/File;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40828
    const-class v0, LX/0KN;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0KN;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 40803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40804
    invoke-static {p1, p2}, LX/0KN;->b(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/0KN;->d:Ljava/io/File;

    .line 40805
    iput-object p2, p0, LX/0KN;->b:Ljava/lang/String;

    .line 40806
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0KN;->c:Ljava/util/Map;

    .line 40807
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/0KN;->d:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 40808
    iget-object v0, p0, LX/0KN;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40809
    new-instance v3, Ljava/io/FileInputStream;

    iget-object v0, p0, LX/0KN;->d:Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 40810
    const/4 v2, 0x0

    .line 40811
    :try_start_0
    new-instance v1, Landroid/util/JsonReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v0}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40812
    :try_start_1
    invoke-direct {p0, v1}, LX/0KN;->a(Landroid/util/JsonReader;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 40813
    invoke-virtual {v1}, Landroid/util/JsonReader;->close()V

    .line 40814
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 40815
    :cond_0
    :goto_0
    return-void

    .line 40816
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 40817
    :goto_1
    :try_start_2
    sget-object v2, LX/0KN;->a:Ljava/lang/String;

    const-string v4, "Failed to read the metadata file. Deleting the existing file"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v4, v5}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40818
    iget-object v0, p0, LX/0KN;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 40819
    iget-object v0, p0, LX/0KN;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 40820
    if-eqz v1, :cond_1

    .line 40821
    invoke-virtual {v1}, Landroid/util/JsonReader;->close()V

    .line 40822
    :cond_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    goto :goto_0

    .line 40823
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    .line 40824
    invoke-virtual {v1}, Landroid/util/JsonReader;->close()V

    .line 40825
    :cond_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    throw v0

    .line 40826
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 40827
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private a(Landroid/util/JsonReader;)V
    .locals 8

    .prologue
    .line 40768
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 40769
    const/4 v0, 0x0

    .line 40770
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 40771
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    .line 40772
    const-string v2, "videoid"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 40773
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/0KN;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 40774
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Video id int he file is different from the passed videoid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40775
    :cond_1
    const-string v2, "hashcode"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 40776
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v0

    .line 40777
    :cond_2
    const-string v2, "streams"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40778
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 40779
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 40780
    :goto_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 40781
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    .line 40782
    new-instance v4, LX/0KM;

    invoke-direct {v4}, LX/0KM;-><init>()V

    .line 40783
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 40784
    :cond_3
    :goto_2
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 40785
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v5

    .line 40786
    const-string v6, "totallength"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 40787
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v6

    iput-wide v6, v4, LX/0KM;->a:J

    goto :goto_2

    .line 40788
    :cond_4
    const-string v6, "cachedlength"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 40789
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v6

    iput-wide v6, v4, LX/0KM;->b:J

    goto :goto_2

    .line 40790
    :cond_5
    const-string v6, "streamtype"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 40791
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v5

    iput v5, v4, LX/0KM;->c:I

    goto :goto_2

    .line 40792
    :cond_6
    const-string v6, "bitrate"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 40793
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v5

    iput v5, v4, LX/0KM;->d:I

    goto :goto_2

    .line 40794
    :cond_7
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 40795
    move-object v2, v4

    .line 40796
    iget-object v3, p0, LX/0KN;->c:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 40797
    :cond_8
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 40798
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    goto/16 :goto_0

    .line 40799
    :cond_9
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 40800
    invoke-static {p0}, LX/0KN;->b(LX/0KN;)I

    move-result v1

    if-eq v1, v0, :cond_a

    .line 40801
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Checksum mismatch"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40802
    :cond_a
    return-void
.end method

.method public static b(LX/0KN;)I
    .locals 6

    .prologue
    .line 40756
    iget-object v0, p0, LX/0KN;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 40757
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/0KN;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 40758
    new-instance v1, Ljava/util/TreeSet;

    iget-object v2, p0, LX/0KN;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    .line 40759
    invoke-virtual {v1}, Ljava/util/TreeSet;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/NavigableSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 40760
    iget-object v1, p0, LX/0KN;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0KM;

    .line 40761
    mul-int/lit8 v2, v2, 0x1f

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/2addr v0, v2

    .line 40762
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, v1, LX/0KM;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 40763
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, v1, LX/0KM;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 40764
    mul-int/lit8 v0, v0, 0x1f

    iget v2, v1, LX/0KM;->d:I

    add-int/2addr v0, v2

    .line 40765
    mul-int/lit8 v0, v0, 0x1f

    iget v1, v1, LX/0KM;->c:I

    add-int/2addr v0, v1

    move v2, v0

    .line 40766
    goto :goto_0

    .line 40767
    :cond_0
    return v2
.end method

.method public static b(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 40755
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".v1.meta"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static c(LX/0KN;)V
    .locals 14

    .prologue
    .line 40725
    const/4 v2, 0x0

    .line 40726
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v0, p0, LX/0KN;->d:Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 40727
    :try_start_0
    new-instance v1, Landroid/util/JsonWriter;

    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v0}, Landroid/util/JsonWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 40728
    :try_start_1
    invoke-virtual {v1}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 40729
    const-string v6, "hashcode"

    invoke-virtual {v1, v6}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v6

    invoke-static {p0}, LX/0KN;->b(LX/0KN;)I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 40730
    const-string v6, "videoid"

    invoke-virtual {v1, v6}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v6

    iget-object v7, p0, LX/0KN;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 40731
    const-string v6, "streams"

    invoke-virtual {v1, v6}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v6

    invoke-virtual {v6}, Landroid/util/JsonWriter;->beginArray()Landroid/util/JsonWriter;

    .line 40732
    invoke-virtual {v1}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 40733
    iget-object v6, p0, LX/0KN;->c:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 40734
    invoke-virtual {v1, v6}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v8

    invoke-virtual {v8}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 40735
    iget-object v8, p0, LX/0KN;->c:Ljava/util/Map;

    invoke-interface {v8, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0KM;

    .line 40736
    const-string v10, "totallength"

    invoke-virtual {v1, v10}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v10

    iget-wide v12, v6, LX/0KM;->a:J

    invoke-virtual {v10, v12, v13}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 40737
    const-string v10, "cachedlength"

    invoke-virtual {v1, v10}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v10

    iget-wide v12, v6, LX/0KM;->b:J

    invoke-virtual {v10, v12, v13}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 40738
    const-string v10, "streamtype"

    invoke-virtual {v1, v10}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v10

    iget v11, v6, LX/0KM;->c:I

    int-to-long v12, v11

    invoke-virtual {v10, v12, v13}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 40739
    const-string v10, "bitrate"

    invoke-virtual {v1, v10}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v10

    iget v11, v6, LX/0KM;->d:I

    int-to-long v12, v11

    invoke-virtual {v10, v12, v13}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 40740
    invoke-virtual {v1}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    goto :goto_0

    .line 40741
    :cond_0
    invoke-virtual {v1}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    .line 40742
    invoke-virtual {v1}, Landroid/util/JsonWriter;->endArray()Landroid/util/JsonWriter;

    .line 40743
    invoke-virtual {v1}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 40744
    invoke-virtual {v1}, Landroid/util/JsonWriter;->close()V

    .line 40745
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 40746
    return-void

    .line 40747
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 40748
    :goto_1
    :try_start_2
    sget-object v2, LX/0KN;->a:Ljava/lang/String;

    const-string v4, "Exception saving metadata "

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v4, v5}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40749
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 40750
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 40751
    invoke-virtual {v1}, Landroid/util/JsonWriter;->close()V

    .line 40752
    :cond_1
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    throw v0

    .line 40753
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 40754
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/facebook/exoplayer/ipc/VideoCacheStatus;
    .locals 14

    .prologue
    const-wide/16 v2, -0x1

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 40703
    iget-object v0, p0, LX/0KN;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v5, v6

    move v7, v6

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 40704
    iget-object v4, p0, LX/0KN;->c:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0KM;

    .line 40705
    iget-wide v10, v4, LX/0KM;->b:J

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-lez v9, :cond_0

    .line 40706
    const/4 v11, 0x5

    new-array v11, v11, [Ljava/lang/Object;

    aput-object v0, v11, v6

    iget-wide v12, v4, LX/0KM;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v11, v1

    const/4 v0, 0x2

    iget-wide v12, v4, LX/0KM;->b:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v11, v0

    const/4 v0, 0x3

    iget v12, v4, LX/0KM;->d:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v0

    const/4 v0, 0x4

    iget v12, v4, LX/0KM;->c:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v0

    .line 40707
    iget v0, v4, LX/0KM;->c:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    move v0, v5

    move v5, v7

    .line 40708
    :goto_1
    iget v4, v4, LX/0KM;->c:I

    const/4 v7, -0x1

    if-eq v4, v7, :cond_2

    move v7, v5

    move v5, v0

    .line 40709
    goto :goto_0

    :pswitch_0
    move v7, v1

    :pswitch_1
    move v0, v1

    move v5, v7

    .line 40710
    goto :goto_1

    :pswitch_2
    move v0, v5

    move v5, v1

    .line 40711
    goto :goto_1

    :cond_1
    move v0, v5

    move v5, v7

    .line 40712
    :cond_2
    if-eqz v5, :cond_3

    if-eqz v0, :cond_3

    .line 40713
    new-instance v0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;-><init>(ZJJ)V

    .line 40714
    :goto_2
    return-object v0

    :cond_3
    new-instance v0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;

    move v1, v6

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;-><init>(ZJJ)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;JIII)V
    .locals 4

    .prologue
    .line 40715
    iget-object v0, p0, LX/0KN;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0KM;

    .line 40716
    if-nez v0, :cond_0

    .line 40717
    new-instance v0, LX/0KM;

    invoke-direct {v0}, LX/0KM;-><init>()V

    .line 40718
    iput p5, v0, LX/0KM;->d:I

    .line 40719
    :cond_0
    iput p6, v0, LX/0KM;->c:I

    .line 40720
    int-to-long v2, p4

    iput-wide v2, v0, LX/0KM;->b:J

    .line 40721
    iput-wide p2, v0, LX/0KM;->a:J

    .line 40722
    iget-object v1, p0, LX/0KN;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40723
    invoke-static {p0}, LX/0KN;->c(LX/0KN;)V

    .line 40724
    return-void
.end method
