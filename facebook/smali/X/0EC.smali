.class public final LX/0EC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:Z

.field public h:Z

.field public i:Ljava/util/Date;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/Date;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V
    .locals 3

    .prologue
    .line 31466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31467
    iput-wide p1, p0, LX/0EC;->a:J

    .line 31468
    iput-object p3, p0, LX/0EC;->b:Ljava/lang/String;

    .line 31469
    iput-object p4, p0, LX/0EC;->c:Ljava/lang/String;

    .line 31470
    iput-object p5, p0, LX/0EC;->d:Ljava/lang/String;

    .line 31471
    iput-object p6, p0, LX/0EC;->e:Ljava/lang/String;

    .line 31472
    iput-wide p7, p0, LX/0EC;->f:J

    .line 31473
    iput-boolean p9, p0, LX/0EC;->g:Z

    .line 31474
    iput-boolean p10, p0, LX/0EC;->h:Z

    .line 31475
    const-wide/16 v0, 0x0

    cmp-long v0, p7, v0

    if-eqz v0, :cond_0

    .line 31476
    invoke-static {p7, p8}, LX/0EC;->b(J)J

    move-result-wide v0

    .line 31477
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, LX/0EC;->i:Ljava/util/Date;

    .line 31478
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_1

    .line 31479
    invoke-static {p1, p2}, LX/0EC;->b(J)J

    move-result-wide v0

    .line 31480
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, LX/0EC;->j:Ljava/util/Date;

    .line 31481
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 31465
    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method private static a(Z)I
    .locals 1

    .prologue
    .line 31464
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(J)J
    .locals 4

    .prologue
    .line 31463
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p0

    const-wide v2, 0x295e9648864000L

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)Z"
        }
    .end annotation

    .prologue
    .line 31457
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 31458
    const/4 v0, 0x1

    .line 31459
    :goto_0
    return v0

    .line 31460
    :cond_0
    if-eqz p0, :cond_1

    .line 31461
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 31462
    :cond_1
    invoke-virtual {p1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(J)J
    .locals 4

    .prologue
    .line 31456
    const-wide v0, 0x295e9648864000L

    sub-long v0, p0, v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private static c(J)I
    .locals 2

    .prologue
    .line 31482
    const/16 v0, 0x20

    ushr-long v0, p0, v0

    xor-long/2addr v0, p0

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31455
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v0, p0, LX/0EC;->g:Z

    if-eqz v0, :cond_0

    const-string v0, "https://"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0EC;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "http://"

    goto :goto_0
.end method

.method public final a(Ljava/util/Date;)Z
    .locals 1

    .prologue
    .line 31454
    iget-object v0, p0, LX/0EC;->i:Ljava/util/Date;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, LX/0EC;->i:Ljava/util/Date;

    invoke-virtual {v0, p1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 6

    .prologue
    .line 31427
    const/4 v0, 0x0

    .line 31428
    iget-object v1, p0, LX/0EC;->c:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 31429
    :cond_0
    :goto_0
    move-object v0, v0

    .line 31430
    return-object v0

    .line 31431
    :cond_1
    iget-object v1, p0, LX/0EC;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31432
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 31433
    iget-object v0, p0, LX/0EC;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, LX/0EC;->d:Ljava/lang/String;

    if-nez v0, :cond_7

    const-string v0, ""

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31434
    iget-object v0, p0, LX/0EC;->i:Ljava/util/Date;

    if-eqz v0, :cond_2

    .line 31435
    const-string v0, "Expires="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/0EC;->i:Ljava/util/Date;

    .line 31436
    if-nez v2, :cond_8

    .line 31437
    const/4 v3, 0x0

    .line 31438
    :goto_2
    move-object v2, v3

    .line 31439
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31440
    :cond_2
    const-string v0, "Domain="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/0EC;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31441
    iget-object v0, p0, LX/0EC;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 31442
    const-string v0, "Path="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/0EC;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31443
    :cond_3
    iget-boolean v0, p0, LX/0EC;->g:Z

    if-eqz v0, :cond_4

    .line 31444
    const-string v0, "Secure;"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31445
    :cond_4
    iget-boolean v0, p0, LX/0EC;->h:Z

    if-eqz v0, :cond_5

    .line 31446
    const-string v0, "httpOnly;"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31447
    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v2, 0x3b

    if-ne v0, v2, :cond_6

    .line 31448
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 31449
    :cond_6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 31450
    :cond_7
    iget-object v0, p0, LX/0EC;->d:Ljava/lang/String;

    goto :goto_1

    .line 31451
    :cond_8
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "EEE, dd MMM yyyy HH:mm:ss z"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 31452
    const-string v4, "UTC"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 31453
    invoke-virtual {v3, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 31413
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/0EC;

    if-nez v1, :cond_1

    .line 31414
    :cond_0
    :goto_0
    return v0

    .line 31415
    :cond_1
    check-cast p1, LX/0EC;

    .line 31416
    iget-wide v2, p0, LX/0EC;->a:J

    iget-wide v4, p1, LX/0EC;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 31417
    iget-object v1, p0, LX/0EC;->b:Ljava/lang/String;

    iget-object v2, p1, LX/0EC;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0EC;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31418
    iget-object v1, p0, LX/0EC;->c:Ljava/lang/String;

    iget-object v2, p1, LX/0EC;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0EC;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31419
    iget-object v1, p0, LX/0EC;->d:Ljava/lang/String;

    iget-object v2, p1, LX/0EC;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0EC;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31420
    iget-object v1, p0, LX/0EC;->e:Ljava/lang/String;

    iget-object v2, p1, LX/0EC;->e:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0EC;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31421
    iget-wide v2, p0, LX/0EC;->f:J

    iget-wide v4, p1, LX/0EC;->f:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 31422
    iget-boolean v1, p0, LX/0EC;->g:Z

    iget-boolean v2, p1, LX/0EC;->g:Z

    if-ne v1, v2, :cond_0

    .line 31423
    iget-boolean v1, p0, LX/0EC;->h:Z

    iget-boolean v2, p1, LX/0EC;->h:Z

    if-ne v1, v2, :cond_0

    .line 31424
    iget-object v1, p0, LX/0EC;->j:Ljava/util/Date;

    iget-object v2, p1, LX/0EC;->j:Ljava/util/Date;

    invoke-static {v1, v2}, LX/0EC;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31425
    iget-object v1, p0, LX/0EC;->i:Ljava/util/Date;

    iget-object v2, p1, LX/0EC;->i:Ljava/util/Date;

    invoke-static {v1, v2}, LX/0EC;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31426
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 31412
    iget-wide v0, p0, LX/0EC;->a:J

    invoke-static {v0, v1}, LX/0EC;->c(J)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/0EC;->b:Ljava/lang/String;

    invoke-static {v1}, LX/0EC;->a(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/0EC;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0EC;->a(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/0EC;->d:Ljava/lang/String;

    invoke-static {v1}, LX/0EC;->a(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/0EC;->e:Ljava/lang/String;

    invoke-static {v1}, LX/0EC;->a(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iget-wide v2, p0, LX/0EC;->f:J

    invoke-static {v2, v3}, LX/0EC;->c(J)I

    move-result v1

    add-int/2addr v0, v1

    iget-boolean v1, p0, LX/0EC;->g:Z

    invoke-static {v1}, LX/0EC;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    iget-boolean v1, p0, LX/0EC;->h:Z

    invoke-static {v1}, LX/0EC;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/0EC;->j:Ljava/util/Date;

    invoke-static {v1}, LX/0EC;->a(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/0EC;->i:Ljava/util/Date;

    invoke-static {v1}, LX/0EC;->a(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/0EC;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0EC;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0EC;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0EC;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, LX/0EC;->g:Z

    if-eqz v0, :cond_0

    const-string v0, ":secure"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, LX/0EC;->h:Z

    if-eqz v0, :cond_1

    const-string v0, ":httpOnly"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method
