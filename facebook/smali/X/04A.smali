.class public final enum LX/04A;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/04A;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/04A;

.field public static final enum VIDEO_ALBUM_PERMALINK_ENTER:LX/04A;

.field public static final enum VIDEO_CANCELLED_REQUESTED_PLAYING:LX/04A;

.field public static final enum VIDEO_COMPLETE:LX/04A;

.field public static final enum VIDEO_DISCONTINUED:LX/04A;

.field public static final enum VIDEO_DISPLAYED:LX/04A;

.field public static final enum VIDEO_ENTERED_HD:LX/04A;

.field public static final enum VIDEO_EXCEPTION:LX/04A;

.field public static final enum VIDEO_EXITED_HD:LX/04A;

.field public static final enum VIDEO_FRAMES_DROPPED:LX/04A;

.field public static final enum VIDEO_FULL_SCREEN_ONPAUSE:LX/04A;

.field public static final enum VIDEO_FULL_SCREEN_ONRESUME:LX/04A;

.field public static final enum VIDEO_GRAPHICS_INFO:LX/04A;

.field public static final enum VIDEO_GUIDE_ENTERED:LX/04A;

.field public static final enum VIDEO_GUIDE_EXITED:LX/04A;

.field public static final enum VIDEO_HEADSET_CONNECTED:LX/04A;

.field public static final enum VIDEO_HEADSET_DISCONNECTED:LX/04A;

.field public static final enum VIDEO_INVALID_EVENT:LX/04A;

.field public static final enum VIDEO_IN_TRANSITION:LX/04A;

.field public static final enum VIDEO_MUTED:LX/04A;

.field public static final enum VIDEO_NO_SURFACE_UPDATE:LX/04A;

.field public static final enum VIDEO_PAUSE:LX/04A;

.field public static final enum VIDEO_PLAYED_FOR_THREE_SECONDS:LX/04A;

.field public static final enum VIDEO_PLAYER_FORMAT_CHANGED:LX/04A;

.field public static final enum VIDEO_PLAYER_PAUSE:LX/04A;

.field public static final enum VIDEO_PLAYER_STOP:LX/04A;

.field public static final enum VIDEO_PLAYER_UNPAUSE:LX/04A;

.field public static final enum VIDEO_QUALITY_SELECTED:LX/04A;

.field public static final enum VIDEO_QUALITY_SELECTOR_TAPPED:LX/04A;

.field public static final enum VIDEO_REPLAYED:LX/04A;

.field public static final enum VIDEO_REPRESENTATION_ENDED:LX/04A;

.field public static final enum VIDEO_REQUESTED_PLAYING:LX/04A;

.field public static final enum VIDEO_SCRUBBED:LX/04A;

.field public static final enum VIDEO_SEEK:LX/04A;

.field public static final enum VIDEO_SELECT_ALBUM_PERMALINK:LX/04A;

.field public static final enum VIDEO_SPATIAL_AUDIO_BUFFER_UNDERRUN:LX/04A;

.field public static final enum VIDEO_SPHERICAL_FALLBACK_ENTERED:LX/04A;

.field public static final enum VIDEO_START:LX/04A;

.field public static final enum VIDEO_SURFACE_UPDATED:LX/04A;

.field public static final enum VIDEO_TEXTUREVIEW_INFO:LX/04A;

.field public static final enum VIDEO_UNMUTED:LX/04A;

.field public static final enum VIDEO_UNPAUSED:LX/04A;

.field public static final enum VIDEO_VOLUME_DECREASE:LX/04A;

.field public static final enum VIDEO_VOLUME_INCREASE:LX/04A;

.field public static final enum VIDEO_VR_CAST_CLICK:LX/04A;

.field public static final enum VIDEO_WATCH_AND_BROWSE_DISMISS_VIDEO_PLAYER:LX/04A;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 12604
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_REQUESTED_PLAYING"

    const-string v2, "requested_playing"

    invoke-direct {v0, v1, v4, v2}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_REQUESTED_PLAYING:LX/04A;

    .line 12605
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_CANCELLED_REQUESTED_PLAYING"

    const-string v2, "cancelled_requested_playing"

    invoke-direct {v0, v1, v5, v2}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_CANCELLED_REQUESTED_PLAYING:LX/04A;

    .line 12606
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_START"

    const-string v2, "started_playing"

    invoke-direct {v0, v1, v6, v2}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_START:LX/04A;

    .line 12607
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_PAUSE"

    const-string v2, "paused"

    invoke-direct {v0, v1, v7, v2}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_PAUSE:LX/04A;

    .line 12608
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_UNPAUSED"

    const-string v2, "unpaused"

    invoke-direct {v0, v1, v8, v2}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_UNPAUSED:LX/04A;

    .line 12609
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_REPLAYED"

    const/4 v2, 0x5

    const-string v3, "replayed"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_REPLAYED:LX/04A;

    .line 12610
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_COMPLETE"

    const/4 v2, 0x6

    const-string v3, "finished_playing"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_COMPLETE:LX/04A;

    .line 12611
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_DISCONTINUED"

    const/4 v2, 0x7

    const-string v3, "playback_discontinuity"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_DISCONTINUED:LX/04A;

    .line 12612
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_MUTED"

    const/16 v2, 0x8

    const-string v3, "muted"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_MUTED:LX/04A;

    .line 12613
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_UNMUTED"

    const/16 v2, 0x9

    const-string v3, "unmuted"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_UNMUTED:LX/04A;

    .line 12614
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_SEEK"

    const/16 v2, 0xa

    const-string v3, "seek"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_SEEK:LX/04A;

    .line 12615
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_SCRUBBED"

    const/16 v2, 0xb

    const-string v3, "scrubbed"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_SCRUBBED:LX/04A;

    .line 12616
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_VOLUME_INCREASE"

    const/16 v2, 0xc

    const-string v3, "volume_increase"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_VOLUME_INCREASE:LX/04A;

    .line 12617
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_VOLUME_DECREASE"

    const/16 v2, 0xd

    const-string v3, "volume_decrease"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_VOLUME_DECREASE:LX/04A;

    .line 12618
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_EXCEPTION"

    const/16 v2, 0xe

    const-string v3, "failed_playing"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_EXCEPTION:LX/04A;

    .line 12619
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_FULL_SCREEN_ONRESUME"

    const/16 v2, 0xf

    const-string v3, "full_screen_onresume"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_FULL_SCREEN_ONRESUME:LX/04A;

    .line 12620
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_FULL_SCREEN_ONPAUSE"

    const/16 v2, 0x10

    const-string v3, "full_screen_onpause"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_FULL_SCREEN_ONPAUSE:LX/04A;

    .line 12621
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_GRAPHICS_INFO"

    const/16 v2, 0x11

    const-string v3, "video_graphics_info"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_GRAPHICS_INFO:LX/04A;

    .line 12622
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_TEXTUREVIEW_INFO"

    const/16 v2, 0x12

    const-string v3, "video_texview_info"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_TEXTUREVIEW_INFO:LX/04A;

    .line 12623
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_PLAYER_STOP"

    const/16 v2, 0x13

    const-string v3, "video_player_stop"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_PLAYER_STOP:LX/04A;

    .line 12624
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_PLAYER_PAUSE"

    const/16 v2, 0x14

    const-string v3, "video_player_pause"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_PLAYER_PAUSE:LX/04A;

    .line 12625
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_PLAYER_UNPAUSE"

    const/16 v2, 0x15

    const-string v3, "video_player_unpause"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_PLAYER_UNPAUSE:LX/04A;

    .line 12626
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_PLAYER_FORMAT_CHANGED"

    const/16 v2, 0x16

    const-string v3, "player_format_changed"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_PLAYER_FORMAT_CHANGED:LX/04A;

    .line 12627
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_DISPLAYED"

    const/16 v2, 0x17

    const-string v3, "displayed"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_DISPLAYED:LX/04A;

    .line 12628
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_ALBUM_PERMALINK_ENTER"

    const/16 v2, 0x18

    const-string v3, "video_album_permalink_enter"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_ALBUM_PERMALINK_ENTER:LX/04A;

    .line 12629
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_SELECT_ALBUM_PERMALINK"

    const/16 v2, 0x19

    const-string v3, "video_select_album_permalink"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_SELECT_ALBUM_PERMALINK:LX/04A;

    .line 12630
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_ENTERED_HD"

    const/16 v2, 0x1a

    const-string v3, "entered_hd"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_ENTERED_HD:LX/04A;

    .line 12631
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_EXITED_HD"

    const/16 v2, 0x1b

    const-string v3, "exited_hd"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_EXITED_HD:LX/04A;

    .line 12632
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_VR_CAST_CLICK"

    const/16 v2, 0x1c

    const-string v3, "vr_cast_button_click"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_VR_CAST_CLICK:LX/04A;

    .line 12633
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_FRAMES_DROPPED"

    const/16 v2, 0x1d

    const-string v3, "playback_frames_drop"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_FRAMES_DROPPED:LX/04A;

    .line 12634
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_SURFACE_UPDATED"

    const/16 v2, 0x1e

    const-string v3, "surface_updated"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_SURFACE_UPDATED:LX/04A;

    .line 12635
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_NO_SURFACE_UPDATE"

    const/16 v2, 0x1f

    const-string v3, "no_surface_update"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_NO_SURFACE_UPDATE:LX/04A;

    .line 12636
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_REPRESENTATION_ENDED"

    const/16 v2, 0x20

    const-string v3, "representation_ended"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_REPRESENTATION_ENDED:LX/04A;

    .line 12637
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_WATCH_AND_BROWSE_DISMISS_VIDEO_PLAYER"

    const/16 v2, 0x21

    const-string v3, "watch_and_browse_dismiss_video_player"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_WATCH_AND_BROWSE_DISMISS_VIDEO_PLAYER:LX/04A;

    .line 12638
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_PLAYED_FOR_THREE_SECONDS"

    const/16 v2, 0x22

    const-string v3, "played_for_three_seconds"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_PLAYED_FOR_THREE_SECONDS:LX/04A;

    .line 12639
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_GUIDE_ENTERED"

    const/16 v2, 0x23

    const-string v3, "guide_entered"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_GUIDE_ENTERED:LX/04A;

    .line 12640
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_GUIDE_EXITED"

    const/16 v2, 0x24

    const-string v3, "guide_exited"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_GUIDE_EXITED:LX/04A;

    .line 12641
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_HEADSET_CONNECTED"

    const/16 v2, 0x25

    const-string v3, "headset_connected"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_HEADSET_CONNECTED:LX/04A;

    .line 12642
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_HEADSET_DISCONNECTED"

    const/16 v2, 0x26

    const-string v3, "headset_disconnected"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_HEADSET_DISCONNECTED:LX/04A;

    .line 12643
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_SPATIAL_AUDIO_BUFFER_UNDERRUN"

    const/16 v2, 0x27

    const-string v3, "spatial_audio_buffer_underrun"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_SPATIAL_AUDIO_BUFFER_UNDERRUN:LX/04A;

    .line 12644
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_SPHERICAL_FALLBACK_ENTERED"

    const/16 v2, 0x28

    const-string v3, "spherical_fallback_entered"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_SPHERICAL_FALLBACK_ENTERED:LX/04A;

    .line 12645
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_QUALITY_SELECTED"

    const/16 v2, 0x29

    const-string v3, "user_selected_quality"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_QUALITY_SELECTED:LX/04A;

    .line 12646
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_QUALITY_SELECTOR_TAPPED"

    const/16 v2, 0x2a

    const-string v3, "quality_selector_tapped"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_QUALITY_SELECTOR_TAPPED:LX/04A;

    .line 12647
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_IN_TRANSITION"

    const/16 v2, 0x2b

    const-string v3, "video_in_transition"

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_IN_TRANSITION:LX/04A;

    .line 12648
    new-instance v0, LX/04A;

    const-string v1, "VIDEO_INVALID_EVENT"

    const/16 v2, 0x2c

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, LX/04A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04A;->VIDEO_INVALID_EVENT:LX/04A;

    .line 12649
    const/16 v0, 0x2d

    new-array v0, v0, [LX/04A;

    sget-object v1, LX/04A;->VIDEO_REQUESTED_PLAYING:LX/04A;

    aput-object v1, v0, v4

    sget-object v1, LX/04A;->VIDEO_CANCELLED_REQUESTED_PLAYING:LX/04A;

    aput-object v1, v0, v5

    sget-object v1, LX/04A;->VIDEO_START:LX/04A;

    aput-object v1, v0, v6

    sget-object v1, LX/04A;->VIDEO_PAUSE:LX/04A;

    aput-object v1, v0, v7

    sget-object v1, LX/04A;->VIDEO_UNPAUSED:LX/04A;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/04A;->VIDEO_REPLAYED:LX/04A;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/04A;->VIDEO_COMPLETE:LX/04A;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/04A;->VIDEO_DISCONTINUED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/04A;->VIDEO_MUTED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/04A;->VIDEO_UNMUTED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/04A;->VIDEO_SEEK:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/04A;->VIDEO_SCRUBBED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/04A;->VIDEO_VOLUME_INCREASE:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/04A;->VIDEO_VOLUME_DECREASE:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/04A;->VIDEO_EXCEPTION:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/04A;->VIDEO_FULL_SCREEN_ONRESUME:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/04A;->VIDEO_FULL_SCREEN_ONPAUSE:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/04A;->VIDEO_GRAPHICS_INFO:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/04A;->VIDEO_TEXTUREVIEW_INFO:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/04A;->VIDEO_PLAYER_STOP:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/04A;->VIDEO_PLAYER_PAUSE:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/04A;->VIDEO_PLAYER_UNPAUSE:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/04A;->VIDEO_PLAYER_FORMAT_CHANGED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/04A;->VIDEO_DISPLAYED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/04A;->VIDEO_ALBUM_PERMALINK_ENTER:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/04A;->VIDEO_SELECT_ALBUM_PERMALINK:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/04A;->VIDEO_ENTERED_HD:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/04A;->VIDEO_EXITED_HD:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/04A;->VIDEO_VR_CAST_CLICK:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/04A;->VIDEO_FRAMES_DROPPED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/04A;->VIDEO_SURFACE_UPDATED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/04A;->VIDEO_NO_SURFACE_UPDATE:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/04A;->VIDEO_REPRESENTATION_ENDED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/04A;->VIDEO_WATCH_AND_BROWSE_DISMISS_VIDEO_PLAYER:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/04A;->VIDEO_PLAYED_FOR_THREE_SECONDS:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/04A;->VIDEO_GUIDE_ENTERED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/04A;->VIDEO_GUIDE_EXITED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/04A;->VIDEO_HEADSET_CONNECTED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/04A;->VIDEO_HEADSET_DISCONNECTED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/04A;->VIDEO_SPATIAL_AUDIO_BUFFER_UNDERRUN:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/04A;->VIDEO_SPHERICAL_FALLBACK_ENTERED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/04A;->VIDEO_QUALITY_SELECTED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/04A;->VIDEO_QUALITY_SELECTOR_TAPPED:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/04A;->VIDEO_IN_TRANSITION:LX/04A;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/04A;->VIDEO_INVALID_EVENT:LX/04A;

    aput-object v2, v0, v1

    sput-object v0, LX/04A;->$VALUES:[LX/04A;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12601
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12602
    iput-object p3, p0, LX/04A;->value:Ljava/lang/String;

    .line 12603
    return-void
.end method

.method public static asEvent(Ljava/lang/String;)LX/04A;
    .locals 5

    .prologue
    .line 12594
    invoke-static {}, LX/04A;->values()[LX/04A;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 12595
    iget-object v4, v0, LX/04A;->value:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 12596
    :goto_1
    return-object v0

    .line 12597
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 12598
    :cond_1
    sget-object v0, LX/04A;->VIDEO_INVALID_EVENT:LX/04A;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/04A;
    .locals 1

    .prologue
    .line 12600
    const-class v0, LX/04A;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/04A;

    return-object v0
.end method

.method public static values()[LX/04A;
    .locals 1

    .prologue
    .line 12599
    sget-object v0, LX/04A;->$VALUES:[LX/04A;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/04A;

    return-object v0
.end method
