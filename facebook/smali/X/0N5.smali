.class public final LX/0N5;
.super LX/0N4;
.source ""


# instance fields
.field private final b:Z

.field private final c:LX/0Oi;

.field private final d:LX/0Oj;

.field private e:I

.field public f:I

.field public g:Z

.field private h:J

.field private i:LX/0L4;

.field private j:I

.field private k:J


# direct methods
.method public constructor <init>(LX/0LS;Z)V
    .locals 2

    .prologue
    .line 48885
    invoke-direct {p0, p1}, LX/0N4;-><init>(LX/0LS;)V

    .line 48886
    iput-boolean p2, p0, LX/0N5;->b:Z

    .line 48887
    new-instance v0, LX/0Oi;

    const/16 v1, 0x8

    new-array v1, v1, [B

    invoke-direct {v0, v1}, LX/0Oi;-><init>([B)V

    iput-object v0, p0, LX/0N5;->c:LX/0Oi;

    .line 48888
    new-instance v0, LX/0Oj;

    iget-object v1, p0, LX/0N5;->c:LX/0Oi;

    iget-object v1, v1, LX/0Oi;->a:[B

    invoke-direct {v0, v1}, LX/0Oj;-><init>([B)V

    iput-object v0, p0, LX/0N5;->d:LX/0Oj;

    .line 48889
    const/4 v0, 0x0

    iput v0, p0, LX/0N5;->e:I

    .line 48890
    return-void
.end method

.method private c()V
    .locals 14

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 48794
    iget-object v0, p0, LX/0N5;->i:LX/0L4;

    if-nez v0, :cond_0

    .line 48795
    iget-boolean v0, p0, LX/0N5;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0N5;->c:LX/0Oi;

    const/4 v7, 0x3

    const/4 v6, -0x1

    const/4 v5, 0x2

    .line 48796
    const/16 v4, 0x20

    invoke-virtual {v0, v4}, LX/0Oi;->b(I)V

    .line 48797
    invoke-virtual {v0, v5}, LX/0Oi;->c(I)I

    move-result v4

    .line 48798
    if-ne v4, v7, :cond_7

    .line 48799
    sget-object v4, LX/0OW;->c:[I

    invoke-virtual {v0, v5}, LX/0Oi;->c(I)I

    move-result v5

    aget v11, v4, v5

    .line 48800
    :goto_0
    invoke-virtual {v0, v7}, LX/0Oi;->c(I)I

    move-result v4

    .line 48801
    invoke-virtual {v0}, LX/0Oi;->b()Z

    move-result v7

    .line 48802
    const-string v5, "audio/eac3"

    sget-object v8, LX/0OW;->d:[I

    aget v8, v8, v4

    if-eqz v7, :cond_8

    const/4 v4, 0x1

    :goto_1
    add-int v10, v8, v4

    const/4 v12, 0x0

    move-object v4, v1

    move v7, v6

    move-wide v8, v2

    move-object v13, v1

    invoke-static/range {v4 .. v13}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v4

    move-object v0, v4

    .line 48803
    :goto_2
    iput-object v0, p0, LX/0N5;->i:LX/0L4;

    .line 48804
    iget-object v0, p0, LX/0N4;->a:LX/0LS;

    iget-object v1, p0, LX/0N5;->i:LX/0L4;

    invoke-interface {v0, v1}, LX/0LS;->a(LX/0L4;)V

    .line 48805
    :cond_0
    iget-boolean v0, p0, LX/0N5;->b:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/0N5;->c:LX/0Oi;

    iget-object v0, v0, LX/0Oi;->a:[B

    .line 48806
    const/4 v1, 0x2

    aget-byte v1, v0, v1

    and-int/lit8 v1, v1, 0x7

    shl-int/lit8 v1, v1, 0x8

    const/4 v2, 0x3

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0x2

    move v0, v1

    .line 48807
    :goto_3
    iput v0, p0, LX/0N5;->j:I

    .line 48808
    iget-boolean v0, p0, LX/0N5;->b:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/0N5;->c:LX/0Oi;

    iget-object v0, v0, LX/0Oi;->a:[B

    const/4 v3, 0x4

    .line 48809
    aget-byte v1, v0, v3

    and-int/lit16 v1, v1, 0xc0

    shr-int/lit8 v1, v1, 0x6

    const/4 v2, 0x3

    if-ne v1, v2, :cond_9

    const/4 v1, 0x6

    :goto_4
    mul-int/lit16 v1, v1, 0x100

    move v0, v1

    .line 48810
    :goto_5
    const-wide/32 v2, 0xf4240

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iget-object v2, p0, LX/0N5;->i:LX/0L4;

    iget v2, v2, LX/0L4;->o:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/0N5;->h:J

    .line 48811
    return-void

    .line 48812
    :cond_1
    iget-object v0, p0, LX/0N5;->c:LX/0Oi;

    const/4 v4, 0x1

    const/4 v6, -0x1

    const/4 v9, 0x2

    .line 48813
    const/16 v5, 0x20

    invoke-virtual {v0, v5}, LX/0Oi;->b(I)V

    .line 48814
    invoke-virtual {v0, v9}, LX/0Oi;->c(I)I

    move-result v7

    .line 48815
    const/16 v5, 0xe

    invoke-virtual {v0, v5}, LX/0Oi;->b(I)V

    .line 48816
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/0Oi;->c(I)I

    move-result v8

    .line 48817
    and-int/lit8 v5, v8, 0x1

    if-eqz v5, :cond_2

    if-eq v8, v4, :cond_2

    .line 48818
    invoke-virtual {v0, v9}, LX/0Oi;->b(I)V

    .line 48819
    :cond_2
    and-int/lit8 v5, v8, 0x4

    if-eqz v5, :cond_3

    .line 48820
    invoke-virtual {v0, v9}, LX/0Oi;->b(I)V

    .line 48821
    :cond_3
    if-ne v8, v9, :cond_4

    .line 48822
    invoke-virtual {v0, v9}, LX/0Oi;->b(I)V

    .line 48823
    :cond_4
    invoke-virtual {v0}, LX/0Oi;->b()Z

    move-result v9

    .line 48824
    const-string v5, "audio/ac3"

    sget-object v10, LX/0OW;->d:[I

    aget v8, v10, v8

    if-eqz v9, :cond_a

    :goto_6
    add-int v10, v8, v4

    sget-object v4, LX/0OW;->b:[I

    aget v11, v4, v7

    const/4 v12, 0x0

    move-object v4, v1

    move v7, v6

    move-wide v8, v2

    move-object v13, v1

    invoke-static/range {v4 .. v13}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v4

    move-object v0, v4

    .line 48825
    goto/16 :goto_2

    .line 48826
    :cond_5
    iget-object v0, p0, LX/0N5;->c:LX/0Oi;

    iget-object v0, v0, LX/0Oi;->a:[B

    const/4 v2, 0x4

    .line 48827
    aget-byte v1, v0, v2

    and-int/lit16 v1, v1, 0xc0

    shr-int/lit8 v1, v1, 0x6

    .line 48828
    aget-byte v2, v0, v2

    and-int/lit8 v2, v2, 0x3f

    .line 48829
    sget-object v3, LX/0OW;->b:[I

    aget v3, v3, v1

    .line 48830
    const v4, 0xac44

    if-ne v3, v4, :cond_b

    .line 48831
    sget-object v3, LX/0OW;->f:[I

    div-int/lit8 v4, v2, 0x2

    aget v3, v3, v4

    rem-int/lit8 v4, v2, 0x2

    add-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x2

    .line 48832
    :goto_7
    move v1, v3

    .line 48833
    move v0, v1

    .line 48834
    goto/16 :goto_3

    .line 48835
    :cond_6
    const/16 v0, 0x600

    move v0, v0

    .line 48836
    goto/16 :goto_5

    .line 48837
    :cond_7
    invoke-virtual {v0, v5}, LX/0Oi;->b(I)V

    .line 48838
    sget-object v5, LX/0OW;->b:[I

    aget v11, v5, v4

    goto/16 :goto_0

    .line 48839
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_9
    sget-object v1, LX/0OW;->a:[I

    aget-byte v2, v0, v3

    and-int/lit8 v2, v2, 0x30

    shr-int/lit8 v2, v2, 0x4

    aget v1, v1, v2

    goto/16 :goto_4

    :cond_a
    const/4 v4, 0x0

    goto :goto_6

    .line 48840
    :cond_b
    sget-object v4, LX/0OW;->e:[I

    div-int/lit8 v0, v2, 0x2

    aget v4, v4, v0

    .line 48841
    const/16 v0, 0x7d00

    if-ne v3, v0, :cond_c

    .line 48842
    mul-int/lit8 v3, v4, 0x6

    goto :goto_7

    .line 48843
    :cond_c
    mul-int/lit8 v3, v4, 0x4

    goto :goto_7
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48881
    iput v0, p0, LX/0N5;->e:I

    .line 48882
    iput v0, p0, LX/0N5;->f:I

    .line 48883
    iput-boolean v0, p0, LX/0N5;->g:Z

    .line 48884
    return-void
.end method

.method public final a(JZ)V
    .locals 1

    .prologue
    .line 48879
    iput-wide p1, p0, LX/0N5;->k:J

    .line 48880
    return-void
.end method

.method public final a(LX/0Oj;)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 48844
    :cond_0
    :goto_0
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    if-lez v0, :cond_3

    .line 48845
    iget v0, p0, LX/0N5;->e:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 48846
    :pswitch_0
    const/16 v5, 0xb

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48847
    :goto_1
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    if-lez v0, :cond_6

    .line 48848
    iget-boolean v0, p0, LX/0N5;->g:Z

    if-nez v0, :cond_2

    .line 48849
    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v0

    if-ne v0, v5, :cond_1

    move v0, v1

    :goto_2
    iput-boolean v0, p0, LX/0N5;->g:Z

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_2

    .line 48850
    :cond_2
    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v0

    .line 48851
    const/16 v3, 0x77

    if-ne v0, v3, :cond_4

    .line 48852
    iput-boolean v2, p0, LX/0N5;->g:Z

    .line 48853
    :goto_3
    move v0, v1

    .line 48854
    if-eqz v0, :cond_0

    .line 48855
    iput v4, p0, LX/0N5;->e:I

    .line 48856
    iget-object v0, p0, LX/0N5;->d:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    const/16 v1, 0xb

    aput-byte v1, v0, v6

    .line 48857
    iget-object v0, p0, LX/0N5;->d:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    const/16 v1, 0x77

    aput-byte v1, v0, v4

    .line 48858
    iput v8, p0, LX/0N5;->f:I

    goto :goto_0

    .line 48859
    :pswitch_1
    iget-object v0, p0, LX/0N5;->d:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    .line 48860
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v1

    iget v2, p0, LX/0N5;->f:I

    sub-int v2, v9, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 48861
    iget v2, p0, LX/0N5;->f:I

    invoke-virtual {p1, v0, v2, v1}, LX/0Oj;->a([BII)V

    .line 48862
    iget v2, p0, LX/0N5;->f:I

    add-int/2addr v1, v2

    iput v1, p0, LX/0N5;->f:I

    .line 48863
    iget v1, p0, LX/0N5;->f:I

    if-ne v1, v9, :cond_7

    const/4 v1, 0x1

    :goto_4
    move v0, v1

    .line 48864
    if-eqz v0, :cond_0

    .line 48865
    invoke-direct {p0}, LX/0N5;->c()V

    .line 48866
    iget-object v0, p0, LX/0N5;->d:LX/0Oj;

    invoke-virtual {v0, v6}, LX/0Oj;->b(I)V

    .line 48867
    iget-object v0, p0, LX/0N4;->a:LX/0LS;

    iget-object v1, p0, LX/0N5;->d:LX/0Oj;

    invoke-interface {v0, v1, v9}, LX/0LS;->a(LX/0Oj;I)V

    .line 48868
    iput v8, p0, LX/0N5;->e:I

    goto :goto_0

    .line 48869
    :pswitch_2
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    iget v1, p0, LX/0N5;->j:I

    iget v2, p0, LX/0N5;->f:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 48870
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    invoke-interface {v1, p1, v0}, LX/0LS;->a(LX/0Oj;I)V

    .line 48871
    iget v1, p0, LX/0N5;->f:I

    add-int/2addr v0, v1

    iput v0, p0, LX/0N5;->f:I

    .line 48872
    iget v0, p0, LX/0N5;->f:I

    iget v1, p0, LX/0N5;->j:I

    if-ne v0, v1, :cond_0

    .line 48873
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    iget-wide v2, p0, LX/0N5;->k:J

    iget v5, p0, LX/0N5;->j:I

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    .line 48874
    iget-wide v0, p0, LX/0N5;->k:J

    iget-wide v2, p0, LX/0N5;->h:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0N5;->k:J

    .line 48875
    iput v6, p0, LX/0N5;->e:I

    goto/16 :goto_0

    .line 48876
    :cond_3
    return-void

    .line 48877
    :cond_4
    if-ne v0, v5, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, LX/0N5;->g:Z

    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto :goto_5

    :cond_6
    move v1, v2

    .line 48878
    goto/16 :goto_3

    :cond_7
    const/4 v1, 0x0

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 48793
    return-void
.end method
