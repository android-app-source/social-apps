.class public LX/05T;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/05T;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field private final d:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 16167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16168
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v1}, LX/05U;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 16169
    if-eqz v2, :cond_2

    .line 16170
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "unknown"

    :goto_0
    iput-object v0, p0, LX/05T;->b:Ljava/lang/String;

    .line 16171
    iget v0, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    if-gtz v0, :cond_1

    const-string v0, "1"

    :goto_1
    iput-object v0, p0, LX/05T;->c:Ljava/lang/String;

    .line 16172
    :goto_2
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 16173
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 16174
    :goto_3
    iput-boolean v0, p0, LX/05T;->d:Z

    .line 16175
    return-void

    .line 16176
    :cond_0
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_0

    .line 16177
    :cond_1
    iget v0, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 16178
    :cond_2
    const-string v0, "unknown"

    iput-object v0, p0, LX/05T;->b:Ljava/lang/String;

    .line 16179
    const-string v0, "1"

    iput-object v0, p0, LX/05T;->c:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move v0, v1

    .line 16180
    goto :goto_3

    :catch_0
    move v0, v1

    goto :goto_3
.end method

.method public static declared-synchronized a(Landroid/content/Context;)LX/05T;
    .locals 2

    .prologue
    .line 16181
    const-class v1, LX/05T;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/05T;->a:LX/05T;

    if-nez v0, :cond_0

    .line 16182
    new-instance v0, LX/05T;

    invoke-direct {v0, p0}, LX/05T;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/05T;->a:LX/05T;

    .line 16183
    :cond_0
    sget-object v0, LX/05T;->a:LX/05T;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 16184
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16166
    iget-object v0, p0, LX/05T;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16165
    iget-object v0, p0, LX/05T;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 16164
    iget-boolean v0, p0, LX/05T;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
