.class public LX/099;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/099;


# instance fields
.field public final a:LX/0aq;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "LX/09A;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0So;


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 22084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22085
    iput-object p1, p0, LX/099;->b:LX/0So;

    .line 22086
    new-instance v0, LX/0aq;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/099;->a:LX/0aq;

    .line 22087
    return-void
.end method

.method public static a(LX/0QB;)LX/099;
    .locals 3

    .prologue
    .line 22088
    sget-object v0, LX/099;->c:LX/099;

    if-nez v0, :cond_1

    .line 22089
    const-class v1, LX/099;

    monitor-enter v1

    .line 22090
    :try_start_0
    sget-object v0, LX/099;->c:LX/099;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 22091
    if-eqz v2, :cond_0

    .line 22092
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/099;->b(LX/0QB;)LX/099;

    move-result-object v0

    sput-object v0, LX/099;->c:LX/099;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 22093
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 22094
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 22095
    :cond_1
    sget-object v0, LX/099;->c:LX/099;

    return-object v0

    .line 22096
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 22097
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/099;
    .locals 2

    .prologue
    .line 22098
    new-instance v1, LX/099;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-direct {v1, v0}, LX/099;-><init>(LX/0So;)V

    .line 22099
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)J
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v2, 0x0

    .line 22100
    if-nez p1, :cond_0

    move-wide v0, v2

    .line 22101
    :goto_0
    return-wide v0

    .line 22102
    :cond_0
    iget-object v4, p0, LX/099;->a:LX/0aq;

    monitor-enter v4

    .line 22103
    :try_start_0
    iget-object v0, p0, LX/099;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/09A;

    .line 22104
    if-nez v0, :cond_1

    .line 22105
    monitor-exit v4

    move-wide v0, v2

    goto :goto_0

    .line 22106
    :cond_1
    invoke-virtual {v0}, LX/09A;->a()J

    move-result-wide v0

    monitor-exit v4

    goto :goto_0

    .line 22107
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 22108
    if-nez p1, :cond_0

    .line 22109
    :goto_0
    return-void

    .line 22110
    :cond_0
    iget-object v1, p0, LX/099;->a:LX/0aq;

    monitor-enter v1

    .line 22111
    :try_start_0
    iget-object v0, p0, LX/099;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/09A;

    .line 22112
    if-nez v0, :cond_1

    .line 22113
    new-instance v0, LX/09A;

    iget-object v2, p0, LX/099;->b:LX/0So;

    invoke-direct {v0, v2}, LX/09A;-><init>(LX/0So;)V

    .line 22114
    iget-object v2, p0, LX/099;->a:LX/0aq;

    invoke-virtual {v2, p1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22115
    :cond_1
    invoke-virtual {v0}, LX/09A;->b()V

    .line 22116
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 22117
    if-nez p1, :cond_0

    .line 22118
    :goto_0
    return-void

    .line 22119
    :cond_0
    iget-object v1, p0, LX/099;->a:LX/0aq;

    monitor-enter v1

    .line 22120
    :try_start_0
    iget-object v0, p0, LX/099;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/09A;

    .line 22121
    if-eqz v0, :cond_1

    .line 22122
    invoke-virtual {v0}, LX/09A;->c()V

    .line 22123
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 22124
    if-nez p1, :cond_0

    .line 22125
    :goto_0
    return-void

    .line 22126
    :cond_0
    iget-object v1, p0, LX/099;->a:LX/0aq;

    monitor-enter v1

    .line 22127
    :try_start_0
    iget-object v0, p0, LX/099;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/09A;

    .line 22128
    if-eqz v0, :cond_1

    .line 22129
    invoke-virtual {v0}, LX/09A;->d()V

    .line 22130
    iget-object v0, p0, LX/099;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22131
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
