.class public final LX/0F3;
.super Ljava/util/HashSet;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashSet",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32802
    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    .line 32803
    const-string v0, "tel"

    invoke-virtual {p0, v0}, LX/0F3;->add(Ljava/lang/Object;)Z

    .line 32804
    const-string v0, "tel-country-code"

    invoke-virtual {p0, v0}, LX/0F3;->add(Ljava/lang/Object;)Z

    .line 32805
    const-string v0, "tel-national"

    invoke-virtual {p0, v0}, LX/0F3;->add(Ljava/lang/Object;)Z

    .line 32806
    const-string v0, "tel-area-code"

    invoke-virtual {p0, v0}, LX/0F3;->add(Ljava/lang/Object;)Z

    .line 32807
    const-string v0, "tel-local"

    invoke-virtual {p0, v0}, LX/0F3;->add(Ljava/lang/Object;)Z

    .line 32808
    const-string v0, "tel-local-prefix"

    invoke-virtual {p0, v0}, LX/0F3;->add(Ljava/lang/Object;)Z

    .line 32809
    const-string v0, "tel-local-suffix"

    invoke-virtual {p0, v0}, LX/0F3;->add(Ljava/lang/Object;)Z

    .line 32810
    return-void
.end method
