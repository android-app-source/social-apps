.class public final LX/09i;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static d:Ljava/lang/String;

.field public static final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:LX/0Tn;

.field private static final g:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22796
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "loom/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 22797
    sput-object v0, LX/09i;->f:LX/0Tn;

    const-string v1, "manual_tracing"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/09i;->a:LX/0Tn;

    .line 22798
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "dextr/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 22799
    sput-object v0, LX/09i;->g:LX/0Tn;

    const-string v1, "last_remaining_bytes_update_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/09i;->b:LX/0Tn;

    .line 22800
    sget-object v0, LX/09i;->g:LX/0Tn;

    const-string v1, "remaining_bytes"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/09i;->c:LX/0Tn;

    .line 22801
    const-string v0, "0fbd0434-651b-4f00-bff3-9838004e1a61"

    sput-object v0, LX/09i;->d:Ljava/lang/String;

    .line 22802
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 22803
    const/4 v1, 0x1

    const-string v2, "async"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22804
    const/4 v1, 0x2

    const-string v2, "lifecycle"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22805
    const/4 v1, 0x4

    const-string v2, "qpl"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22806
    const/16 v1, 0x8

    const-string v2, "other"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22807
    const/16 v1, 0x10

    const-string v2, "fbtrace"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22808
    const/16 v1, 0x20

    const-string v2, "user_counters"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22809
    const/16 v1, 0x40

    const-string v2, "system_counters"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22810
    const/16 v1, 0x80

    const-string v2, "stack_trace"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22811
    const/16 v1, 0x100

    const-string v2, "liger"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22812
    const/16 v1, 0x200

    const-string v2, "major_faults"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22813
    const/16 v1, 0x400

    const-string v2, "thread_schedule"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22814
    const/16 v1, 0x800

    const-string v2, "class_load"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22815
    const/16 v1, 0x1000

    const-string v2, "native_stack_trace"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 22816
    sput-object v0, LX/09i;->e:Landroid/util/SparseArray;

    .line 22817
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
