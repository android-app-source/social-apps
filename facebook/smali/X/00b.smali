.class public final enum LX/00b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/00b;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/00b;

.field public static final enum ERROR_ENCRYPTING:LX/00b;

.field public static final enum ERROR_ENCRYPT_CHANNEL:LX/00b;

.field public static final enum ERROR_IP_ADDRESS:LX/00b;

.field public static final enum ERROR_MISMATCH:LX/00b;

.field public static final enum ERROR_NETWORK_IO:LX/00b;

.field public static final enum ERROR_SIZE_LIMIT:LX/00b;

.field public static final enum ERROR_TEMPLATE:LX/00b;

.field public static final enum FROZEN:LX/00b;

.field public static final enum NONE:LX/00b;

.field public static final enum SENT:LX/00b;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2492
    new-instance v0, LX/00b;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/00b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/00b;->NONE:LX/00b;

    .line 2493
    new-instance v0, LX/00b;

    const-string v1, "FROZEN"

    invoke-direct {v0, v1, v4}, LX/00b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/00b;->FROZEN:LX/00b;

    .line 2494
    new-instance v0, LX/00b;

    const-string v1, "ERROR_TEMPLATE"

    invoke-direct {v0, v1, v5}, LX/00b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/00b;->ERROR_TEMPLATE:LX/00b;

    .line 2495
    new-instance v0, LX/00b;

    const-string v1, "ERROR_ENCRYPT_CHANNEL"

    invoke-direct {v0, v1, v6}, LX/00b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/00b;->ERROR_ENCRYPT_CHANNEL:LX/00b;

    .line 2496
    new-instance v0, LX/00b;

    const-string v1, "ERROR_ENCRYPTING"

    invoke-direct {v0, v1, v7}, LX/00b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/00b;->ERROR_ENCRYPTING:LX/00b;

    .line 2497
    new-instance v0, LX/00b;

    const-string v1, "ERROR_IP_ADDRESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/00b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/00b;->ERROR_IP_ADDRESS:LX/00b;

    .line 2498
    new-instance v0, LX/00b;

    const-string v1, "ERROR_NETWORK_IO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/00b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/00b;->ERROR_NETWORK_IO:LX/00b;

    .line 2499
    new-instance v0, LX/00b;

    const-string v1, "ERROR_SIZE_LIMIT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/00b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/00b;->ERROR_SIZE_LIMIT:LX/00b;

    .line 2500
    new-instance v0, LX/00b;

    const-string v1, "ERROR_MISMATCH"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/00b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/00b;->ERROR_MISMATCH:LX/00b;

    .line 2501
    new-instance v0, LX/00b;

    const-string v1, "SENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/00b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/00b;->SENT:LX/00b;

    .line 2502
    const/16 v0, 0xa

    new-array v0, v0, [LX/00b;

    sget-object v1, LX/00b;->NONE:LX/00b;

    aput-object v1, v0, v3

    sget-object v1, LX/00b;->FROZEN:LX/00b;

    aput-object v1, v0, v4

    sget-object v1, LX/00b;->ERROR_TEMPLATE:LX/00b;

    aput-object v1, v0, v5

    sget-object v1, LX/00b;->ERROR_ENCRYPT_CHANNEL:LX/00b;

    aput-object v1, v0, v6

    sget-object v1, LX/00b;->ERROR_ENCRYPTING:LX/00b;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/00b;->ERROR_IP_ADDRESS:LX/00b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/00b;->ERROR_NETWORK_IO:LX/00b;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/00b;->ERROR_SIZE_LIMIT:LX/00b;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/00b;->ERROR_MISMATCH:LX/00b;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/00b;->SENT:LX/00b;

    aput-object v2, v0, v1

    sput-object v0, LX/00b;->$VALUES:[LX/00b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2503
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/00b;
    .locals 1

    .prologue
    .line 2504
    const-class v0, LX/00b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/00b;

    return-object v0
.end method

.method public static values()[LX/00b;
    .locals 1

    .prologue
    .line 2505
    sget-object v0, LX/00b;->$VALUES:[LX/00b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/00b;

    return-object v0
.end method
