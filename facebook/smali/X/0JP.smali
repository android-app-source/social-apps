.class public final enum LX/0JP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JP;

.field public static final enum GUIDE_LOADING:LX/0JP;

.field public static final enum INIT:LX/0JP;

.field public static final enum NEXT_UNITS:LX/0JP;

.field public static final enum PULL_TO_REFRESH:LX/0JP;

.field public static final enum RELOAD:LX/0JP;

.field public static final enum RESTORE:LX/0JP;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39401
    new-instance v0, LX/0JP;

    const-string v1, "INIT"

    const-string v2, "init"

    invoke-direct {v0, v1, v4, v2}, LX/0JP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JP;->INIT:LX/0JP;

    .line 39402
    new-instance v0, LX/0JP;

    const-string v1, "PULL_TO_REFRESH"

    const-string v2, "pull_to_refresh"

    invoke-direct {v0, v1, v5, v2}, LX/0JP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JP;->PULL_TO_REFRESH:LX/0JP;

    .line 39403
    new-instance v0, LX/0JP;

    const-string v1, "RESTORE"

    const-string v2, "restore"

    invoke-direct {v0, v1, v6, v2}, LX/0JP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JP;->RESTORE:LX/0JP;

    .line 39404
    new-instance v0, LX/0JP;

    const-string v1, "RELOAD"

    const-string v2, "reload"

    invoke-direct {v0, v1, v7, v2}, LX/0JP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JP;->RELOAD:LX/0JP;

    .line 39405
    new-instance v0, LX/0JP;

    const-string v1, "GUIDE_LOADING"

    const-string v2, "guide_loading"

    invoke-direct {v0, v1, v8, v2}, LX/0JP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JP;->GUIDE_LOADING:LX/0JP;

    .line 39406
    new-instance v0, LX/0JP;

    const-string v1, "NEXT_UNITS"

    const/4 v2, 0x5

    const-string v3, "next_units"

    invoke-direct {v0, v1, v2, v3}, LX/0JP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JP;->NEXT_UNITS:LX/0JP;

    .line 39407
    const/4 v0, 0x6

    new-array v0, v0, [LX/0JP;

    sget-object v1, LX/0JP;->INIT:LX/0JP;

    aput-object v1, v0, v4

    sget-object v1, LX/0JP;->PULL_TO_REFRESH:LX/0JP;

    aput-object v1, v0, v5

    sget-object v1, LX/0JP;->RESTORE:LX/0JP;

    aput-object v1, v0, v6

    sget-object v1, LX/0JP;->RELOAD:LX/0JP;

    aput-object v1, v0, v7

    sget-object v1, LX/0JP;->GUIDE_LOADING:LX/0JP;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0JP;->NEXT_UNITS:LX/0JP;

    aput-object v2, v0, v1

    sput-object v0, LX/0JP;->$VALUES:[LX/0JP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39398
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39399
    iput-object p3, p0, LX/0JP;->value:Ljava/lang/String;

    .line 39400
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JP;
    .locals 1

    .prologue
    .line 39408
    const-class v0, LX/0JP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JP;

    return-object v0
.end method

.method public static values()[LX/0JP;
    .locals 1

    .prologue
    .line 39397
    sget-object v0, LX/0JP;->$VALUES:[LX/0JP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JP;

    return-object v0
.end method
