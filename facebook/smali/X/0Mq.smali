.class public final LX/0Mq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Mj;

.field public b:J

.field public c:J

.field public d:I

.field public e:[I

.field public f:[I

.field public g:[J

.field public h:[Z

.field public i:Z

.field public j:[Z

.field public k:I

.field public l:LX/0Oj;

.field public m:Z

.field public n:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 48206
    iput p1, p0, LX/0Mq;->d:I

    .line 48207
    iget-object v0, p0, LX/0Mq;->e:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Mq;->e:[I

    array-length v0, v0

    iget v1, p0, LX/0Mq;->d:I

    if-ge v0, v1, :cond_1

    .line 48208
    :cond_0
    mul-int/lit8 v0, p1, 0x7d

    div-int/lit8 v0, v0, 0x64

    .line 48209
    new-array v1, v0, [I

    iput-object v1, p0, LX/0Mq;->e:[I

    .line 48210
    new-array v1, v0, [I

    iput-object v1, p0, LX/0Mq;->f:[I

    .line 48211
    new-array v1, v0, [J

    iput-object v1, p0, LX/0Mq;->g:[J

    .line 48212
    new-array v1, v0, [Z

    iput-object v1, p0, LX/0Mq;->h:[Z

    .line 48213
    new-array v0, v0, [Z

    iput-object v0, p0, LX/0Mq;->j:[Z

    .line 48214
    :cond_1
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 48215
    iget-object v0, p0, LX/0Mq;->l:LX/0Oj;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Mq;->l:LX/0Oj;

    .line 48216
    iget v2, v0, LX/0Oj;->c:I

    move v0, v2

    .line 48217
    if-ge v0, p1, :cond_1

    .line 48218
    :cond_0
    new-instance v0, LX/0Oj;

    invoke-direct {v0, p1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, LX/0Mq;->l:LX/0Oj;

    .line 48219
    :cond_1
    iput p1, p0, LX/0Mq;->k:I

    .line 48220
    iput-boolean v1, p0, LX/0Mq;->i:Z

    .line 48221
    iput-boolean v1, p0, LX/0Mq;->m:Z

    .line 48222
    return-void
.end method

.method public final c(I)J
    .locals 4

    .prologue
    .line 48223
    iget-object v0, p0, LX/0Mq;->g:[J

    aget-wide v0, v0, p1

    iget-object v2, p0, LX/0Mq;->f:[I

    aget v2, v2, p1

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method
