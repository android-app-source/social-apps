.class public LX/0IK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/059;

.field private final c:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field private final d:LX/05O;

.field private final e:J


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/059;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;LX/05O;)V
    .locals 2

    .prologue
    .line 38579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38580
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0IK;->a:Ljava/lang/String;

    .line 38581
    iput-object p2, p0, LX/0IK;->b:LX/059;

    .line 38582
    iput-object p3, p0, LX/0IK;->c:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 38583
    iput-object p4, p0, LX/0IK;->d:LX/05O;

    .line 38584
    iget-object v0, p0, LX/0IK;->c:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/0IK;->e:J

    .line 38585
    return-void
.end method

.method public static a(LX/0IK;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p0    # LX/0IK;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38574
    const-string v0, "FbnsAnalyticsLogger"

    const-string v1, "Event name: %s, Event parameters: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38575
    new-instance v0, LX/06d;

    iget-object v1, p0, LX/0IK;->a:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, LX/06d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 38576
    invoke-virtual {v0, p2}, LX/06d;->a(Ljava/util/Map;)LX/06d;

    .line 38577
    iget-object v1, p0, LX/0IK;->d:LX/05O;

    invoke-interface {v1, v0}, LX/05O;->a(LX/06d;)V

    .line 38578
    return-void
.end method


# virtual methods
.method public final a(LX/0IH;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZJ)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 38557
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "event_type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, LX/0IH;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 38558
    invoke-static {p2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 38559
    const-string v1, "event_extra_info"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38560
    :cond_0
    invoke-static {p3}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 38561
    const-string v1, "is_buffered"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38562
    :cond_1
    invoke-static {p4}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 38563
    const-string v1, "dpn"

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38564
    :cond_2
    iget-object v1, p0, LX/0IK;->c:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    .line 38565
    const-string v1, "s_boot_ms"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38566
    const-string v1, "s_svc_ms"

    iget-wide v4, p0, LX/0IK;->e:J

    sub-long v4, v2, v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38567
    const-string v1, "s_mqtt_ms"

    sub-long v4, v2, p5

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38568
    const-string v1, "s_net_ms"

    iget-object v4, p0, LX/0IK;->b:LX/059;

    invoke-virtual {v4}, LX/059;->h()J

    move-result-wide v4

    sub-long v4, v2, v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38569
    const-wide/16 v4, 0x0

    cmp-long v1, p8, v4

    if-lez v1, :cond_3

    .line 38570
    const-string v1, "is_scr_on"

    invoke-static {p7}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38571
    const-string v1, "s_scr_ms"

    sub-long/2addr v2, p8

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38572
    :cond_3
    const-string v1, "fbns_message_event"

    invoke-static {p0, v1, v0}, LX/0IK;->a(LX/0IK;Ljava/lang/String;Ljava/util/Map;)V

    .line 38573
    return-void
.end method

.method public final a(LX/0II;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 38552
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "event_type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, LX/0II;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 38553
    invoke-static {p2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 38554
    const-string v1, "event_extra_info"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38555
    :cond_0
    const-string v1, "fbns_registration_event"

    invoke-static {p0, v1, v0}, LX/0IK;->a(LX/0IK;Ljava/lang/String;Ljava/util/Map;)V

    .line 38556
    return-void
.end method

.method public final a(LX/0II;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 38543
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "event_type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, LX/0II;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 38544
    invoke-static {p2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 38545
    const-string v1, "event_extra_info"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38546
    :cond_0
    invoke-static {p3}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 38547
    const-string v1, "spn"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38548
    :cond_1
    invoke-static {p4}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 38549
    const-string v1, "dpn"

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38550
    :cond_2
    const-string v1, "fbns_registration_event"

    invoke-static {p0, v1, v0}, LX/0IK;->a(LX/0IK;Ljava/lang/String;Ljava/util/Map;)V

    .line 38551
    return-void
.end method

.method public final a(LX/0IJ;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 38538
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "event_type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, LX/0IJ;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 38539
    invoke-static {p2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 38540
    const-string v1, "event_extra_info"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38541
    :cond_0
    const-string v1, "fbns_service_event"

    invoke-static {p0, v1, v0}, LX/0IK;->a(LX/0IK;Ljava/lang/String;Ljava/util/Map;)V

    .line 38542
    return-void
.end method
