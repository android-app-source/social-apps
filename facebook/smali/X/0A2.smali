.class public LX/0A2;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/io/File;

.field public final c:I

.field public final d:LX/1bu;

.field public final e:Ljava/util/concurrent/ScheduledExecutorService;

.field private final f:LX/03V;

.field public g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public h:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23382
    const-class v0, LX/0A2;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0A2;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/util/concurrent/ScheduledExecutorService;ILX/03V;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 23383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23384
    iput-object p1, p0, LX/0A2;->b:Ljava/io/File;

    .line 23385
    iput-object p2, p0, LX/0A2;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 23386
    iput p3, p0, LX/0A2;->c:I

    .line 23387
    iput-object p4, p0, LX/0A2;->f:LX/03V;

    .line 23388
    const-string v0, "TimedMicroStorage"

    const/4 v1, 0x1

    const/16 v2, 0xa

    iget-object v3, p0, LX/0A2;->e:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2, v3}, LX/1bu;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;)LX/1bu;

    move-result-object v0

    iput-object v0, p0, LX/0A2;->d:LX/1bu;

    .line 23389
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/0A2;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 23390
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/0A2;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 23391
    return-void
.end method

.method public static c(LX/0A2;LX/0A7;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 23370
    :try_start_0
    iget-object v0, p0, LX/0A2;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 23371
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v0, p0, LX/0A2;->b:Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23372
    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 23373
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 23374
    iget-object v4, p1, LX/0A7;->a:LX/0A1;

    invoke-static {v4, v2}, LX/0A1;->a$redex0(LX/0A1;Ljava/io/DataOutputStream;)V

    .line 23375
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iget-object v2, p0, LX/0A2;->b:Ljava/io/File;

    invoke-static {v0, v2}, LX/1t3;->a([BLjava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23376
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 23377
    :goto_0
    return-void

    .line 23378
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 23379
    :catch_0
    move-exception v0

    .line 23380
    sget-object v1, LX/0A2;->a:Ljava/lang/String;

    const-string v2, "Cannot write to storage"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23381
    iget-object v1, p0, LX/0A2;->f:LX/03V;

    sget-object v2, LX/0A2;->a:Ljava/lang/String;

    const-string v3, "Cannot store video accumulated stats"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0A7;)V
    .locals 7

    .prologue
    .line 23366
    iget-object v0, p0, LX/0A2;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const-string v1, "Calling write without having read info first!"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 23367
    iget-object v2, p0, LX/0A2;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 23368
    :goto_0
    return-void

    .line 23369
    :cond_0
    iget-object v2, p0, LX/0A2;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lcom/facebook/video/analytics/TimedMicroStorage$2;

    invoke-direct {v3, p0, p1}, Lcom/facebook/video/analytics/TimedMicroStorage$2;-><init>(LX/0A2;LX/0A7;)V

    iget v4, p0, LX/0A2;->c:I

    int-to-long v4, v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v4, v5, v6}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method

.method public final a(LX/0A8;)V
    .locals 3

    .prologue
    .line 23364
    iget-object v0, p0, LX/0A2;->d:LX/1bu;

    new-instance v1, Lcom/facebook/video/analytics/TimedMicroStorage$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/analytics/TimedMicroStorage$1;-><init>(LX/0A2;LX/0A8;)V

    const v2, -0x49818741

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 23365
    return-void
.end method
