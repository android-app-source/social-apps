.class public final enum LX/0H9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0H9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0H9;

.field public static final enum HTTP_TRANSFER_END:LX/0H9;

.field public static final enum MANIFEST_FETECH_END:LX/0H9;

.field public static final enum MANIFEST_MISALIGNED:LX/0H9;

.field public static final enum PREFETCH_CACHE_EVICT:LX/0H9;

.field public static final enum PREFETCH_COMPLETE:LX/0H9;

.field private static final mReverseIndex:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0H9;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 37039
    new-instance v1, LX/0H9;

    const-string v2, "PREFETCH_COMPLETE"

    invoke-direct {v1, v2, v0, v0}, LX/0H9;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/0H9;->PREFETCH_COMPLETE:LX/0H9;

    .line 37040
    new-instance v1, LX/0H9;

    const-string v2, "MANIFEST_FETECH_END"

    invoke-direct {v1, v2, v3, v3}, LX/0H9;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/0H9;->MANIFEST_FETECH_END:LX/0H9;

    .line 37041
    new-instance v1, LX/0H9;

    const-string v2, "MANIFEST_MISALIGNED"

    invoke-direct {v1, v2, v4, v4}, LX/0H9;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/0H9;->MANIFEST_MISALIGNED:LX/0H9;

    .line 37042
    new-instance v1, LX/0H9;

    const-string v2, "HTTP_TRANSFER_END"

    invoke-direct {v1, v2, v5, v5}, LX/0H9;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/0H9;->HTTP_TRANSFER_END:LX/0H9;

    .line 37043
    new-instance v1, LX/0H9;

    const-string v2, "PREFETCH_CACHE_EVICT"

    invoke-direct {v1, v2, v6, v6}, LX/0H9;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/0H9;->PREFETCH_CACHE_EVICT:LX/0H9;

    .line 37044
    const/4 v1, 0x5

    new-array v1, v1, [LX/0H9;

    sget-object v2, LX/0H9;->PREFETCH_COMPLETE:LX/0H9;

    aput-object v2, v1, v0

    sget-object v2, LX/0H9;->MANIFEST_FETECH_END:LX/0H9;

    aput-object v2, v1, v3

    sget-object v2, LX/0H9;->MANIFEST_MISALIGNED:LX/0H9;

    aput-object v2, v1, v4

    sget-object v2, LX/0H9;->HTTP_TRANSFER_END:LX/0H9;

    aput-object v2, v1, v5

    sget-object v2, LX/0H9;->PREFETCH_CACHE_EVICT:LX/0H9;

    aput-object v2, v1, v6

    sput-object v1, LX/0H9;->$VALUES:[LX/0H9;

    .line 37045
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    sput-object v1, LX/0H9;->mReverseIndex:Landroid/util/SparseArray;

    .line 37046
    invoke-static {}, LX/0H9;->values()[LX/0H9;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 37047
    sget-object v4, LX/0H9;->mReverseIndex:Landroid/util/SparseArray;

    iget v5, v3, LX/0H9;->mValue:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 37048
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37049
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 37055
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37056
    iput p3, p0, LX/0H9;->mValue:I

    .line 37057
    return-void
.end method

.method public static fromVal(I)LX/0H9;
    .locals 2

    .prologue
    .line 37052
    sget-object v0, LX/0H9;->mReverseIndex:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 37053
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid EventType value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37054
    :cond_0
    sget-object v0, LX/0H9;->mReverseIndex:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0H9;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0H9;
    .locals 1

    .prologue
    .line 37051
    const-class v0, LX/0H9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0H9;

    return-object v0
.end method

.method public static values()[LX/0H9;
    .locals 1

    .prologue
    .line 37050
    sget-object v0, LX/0H9;->$VALUES:[LX/0H9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0H9;

    return-object v0
.end method
