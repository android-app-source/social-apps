.class public final LX/087;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public directory:Ljava/io/File;

.field private mTmpDirLock:LX/02W;

.field public final synthetic this$0:LX/02U;


# direct methods
.method public constructor <init>(LX/02U;LX/02W;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 20794
    iput-object p1, p0, LX/087;->this$0:LX/02U;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20795
    iput-object p2, p0, LX/087;->mTmpDirLock:LX/02W;

    .line 20796
    iput-object p3, p0, LX/087;->directory:Ljava/io/File;

    .line 20797
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 20798
    iget-object v0, p0, LX/087;->mTmpDirLock:LX/02W;

    if-eqz v0, :cond_0

    .line 20799
    iget-object v0, p0, LX/087;->this$0:LX/02U;

    iget-object v0, v0, LX/02U;->mLockFile:LX/02V;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/02V;->acquire(I)LX/02W;

    move-result-object v2

    .line 20800
    :try_start_0
    iget-object v0, p0, LX/087;->mTmpDirLock:LX/02W;

    .line 20801
    iget-object v3, v0, LX/02W;->this$0:LX/02V;

    move-object v0, v3

    .line 20802
    iget-object v0, v0, LX/02V;->lockFileName:Ljava/io/File;

    .line 20803
    iget-object v3, p0, LX/087;->mTmpDirLock:LX/02W;

    invoke-virtual {v3}, LX/02W;->close()V

    .line 20804
    const/4 v3, 0x0

    iput-object v3, p0, LX/087;->mTmpDirLock:LX/02W;

    .line 20805
    invoke-static {v0}, LX/02Q;->deleteRecursiveNoThrow(Ljava/io/File;)V

    .line 20806
    iget-object v0, p0, LX/087;->directory:Ljava/io/File;

    invoke-static {v0}, LX/02Q;->deleteRecursiveNoThrow(Ljava/io/File;)V

    .line 20807
    const/4 v0, 0x0

    iput-object v0, p0, LX/087;->directory:Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 20808
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/02W;->close()V

    .line 20809
    :cond_0
    return-void

    .line 20810
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 20811
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/02W;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/02W;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method
