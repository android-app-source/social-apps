.class public final LX/0N8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:[B

.field public d:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 49123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49124
    new-array v0, p1, [B

    iput-object v0, p0, LX/0N8;->c:[B

    .line 49125
    return-void
.end method


# virtual methods
.method public final a([BII)V
    .locals 3

    .prologue
    .line 49116
    iget-boolean v0, p0, LX/0N8;->d:Z

    if-nez v0, :cond_0

    .line 49117
    :goto_0
    return-void

    .line 49118
    :cond_0
    sub-int v0, p3, p2

    .line 49119
    iget-object v1, p0, LX/0N8;->c:[B

    array-length v1, v1

    iget v2, p0, LX/0N8;->a:I

    add-int/2addr v2, v0

    if-ge v1, v2, :cond_1

    .line 49120
    iget-object v1, p0, LX/0N8;->c:[B

    iget v2, p0, LX/0N8;->a:I

    add-int/2addr v2, v0

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iput-object v1, p0, LX/0N8;->c:[B

    .line 49121
    :cond_1
    iget-object v1, p0, LX/0N8;->c:[B

    iget v2, p0, LX/0N8;->a:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49122
    iget v1, p0, LX/0N8;->a:I

    add-int/2addr v0, v1

    iput v0, p0, LX/0N8;->a:I

    goto :goto_0
.end method
