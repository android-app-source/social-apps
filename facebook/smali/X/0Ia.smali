.class public final LX/0Ia;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0AM;


# instance fields
.field public final synthetic a:Lcom/facebook/rti/push/service/FbnsService;


# direct methods
.method public constructor <init>(Lcom/facebook/rti/push/service/FbnsService;)V
    .locals 0

    .prologue
    .line 38693
    iput-object p1, p0, LX/0Ia;->a:Lcom/facebook/rti/push/service/FbnsService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 38694
    const-string v0, "FbnsService"

    const-string v1, "service/register/publish/failed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38695
    iget-object v0, p0, LX/0Ia;->a:Lcom/facebook/rti/push/service/FbnsService;

    iget-object v0, v0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0II;->REQUEST_SENT_FAIL:LX/0II;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    .line 38696
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 38697
    const-string v0, "FbnsService"

    const-string v1, "service/register/publish/success"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38698
    iget-object v0, p0, LX/0Ia;->a:Lcom/facebook/rti/push/service/FbnsService;

    iget-object v0, v0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0II;->REQUEST_SENT_SUCCESS:LX/0II;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    .line 38699
    return-void
.end method
