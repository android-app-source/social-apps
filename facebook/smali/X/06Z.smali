.class public LX/06Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Executor;


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Ljava/util/concurrent/Executor;

.field public final c:I

.field public final d:I

.field public final e:I

.field private final f:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;


# direct methods
.method public constructor <init>(LX/06W;)V
    .locals 1

    .prologue
    .line 17985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17986
    iget-object v0, p1, LX/06W;->b:Ljava/lang/String;

    iput-object v0, p0, LX/06Z;->a:Ljava/lang/String;

    .line 17987
    iget-object v0, p1, LX/06W;->a:Ljava/util/concurrent/Executor;

    iput-object v0, p0, LX/06Z;->b:Ljava/util/concurrent/Executor;

    .line 17988
    iget v0, p1, LX/06W;->c:I

    iput v0, p0, LX/06Z;->c:I

    .line 17989
    iget v0, p1, LX/06W;->d:I

    iput v0, p0, LX/06Z;->d:I

    .line 17990
    iget v0, p1, LX/06W;->e:I

    iput v0, p0, LX/06Z;->e:I

    .line 17991
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/06Z;->f:Ljava/util/Queue;

    .line 17992
    return-void
.end method

.method public static declared-synchronized a(LX/06Z;)V
    .locals 3

    .prologue
    .line 17993
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06Z;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;

    iput-object v0, p0, LX/06Z;->g:Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;

    .line 17994
    iget-object v0, p0, LX/06Z;->g:Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;

    if-eqz v0, :cond_0

    .line 17995
    iget-object v0, p0, LX/06Z;->b:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/06Z;->g:Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;

    const v2, 0x71d1f7f3

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17996
    :cond_0
    monitor-exit p0

    return-void

    .line 17997
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized execute(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 17998
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06Z;->f:Ljava/util/Queue;

    new-instance v1, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;

    invoke-direct {v1, p0, p1}, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;-><init>(LX/06Z;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 17999
    iget-object v0, p0, LX/06Z;->g:Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;

    if-nez v0, :cond_0

    .line 18000
    invoke-static {p0}, LX/06Z;->a(LX/06Z;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18001
    :cond_0
    monitor-exit p0

    return-void

    .line 18002
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
