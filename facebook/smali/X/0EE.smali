.class public final LX/0EE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)V
    .locals 0

    .prologue
    .line 31505
    iput-object p1, p0, LX/0EE;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 31498
    iget-object v0, p0, LX/0EE;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    .line 31499
    iput-boolean v2, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a:Z

    .line 31500
    iget-object v0, p0, LX/0EE;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    const/4 v1, 0x0

    .line 31501
    iput-boolean v1, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->b:Z

    .line 31502
    iget-object v0, p0, LX/0EE;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-virtual {v0, v2}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->setClickable(Z)V

    .line 31503
    iget-object v0, p0, LX/0EE;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->requestFocus()Z

    .line 31504
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 31486
    iget-object v0, p0, LX/0EE;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    .line 31487
    iput-boolean v2, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a:Z

    .line 31488
    iget-object v0, p0, LX/0EE;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    const/4 v1, 0x0

    .line 31489
    iput-boolean v1, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->b:Z

    .line 31490
    iget-object v0, p0, LX/0EE;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-virtual {v0, v2}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->setClickable(Z)V

    .line 31491
    iget-object v0, p0, LX/0EE;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->requestFocus()Z

    .line 31492
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 31497
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 31493
    iget-object v0, p0, LX/0EE;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    iget-object v0, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 31494
    iget-object v0, p0, LX/0EE;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    const/4 v1, 0x1

    .line 31495
    iput-boolean v1, v0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->b:Z

    .line 31496
    return-void
.end method
