.class public final LX/05d;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:LX/059;


# direct methods
.method public constructor <init>(LX/059;)V
    .locals 0

    .prologue
    .line 16445
    iput-object p1, p0, LX/05d;->a:LX/059;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x7b73f8dd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 16446
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16447
    const/16 v0, 0x27

    const v2, -0x6c06c719

    invoke-static {p2, v3, v0, v2, v1}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 16448
    :goto_0
    return-void

    .line 16449
    :cond_0
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 16450
    iget-object v2, p0, LX/05d;->a:LX/059;

    invoke-static {v2, v0}, LX/059;->a$redex0(LX/059;Landroid/net/NetworkInfo;)V

    .line 16451
    invoke-virtual {p0}, LX/05d;->isInitialStickyBroadcast()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16452
    const-string v0, "MqttNetworkManager"

    const-string v2, "Not rebroadcasting initial sticky broadcast"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16453
    const v0, 0x3541f103

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 16454
    :cond_1
    iget-object v0, p0, LX/05d;->a:LX/059;

    invoke-static {v0}, LX/059;->n(LX/059;)V

    .line 16455
    const v0, 0x3308dc3a

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0
.end method
