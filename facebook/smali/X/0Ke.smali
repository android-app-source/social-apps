.class public final enum LX/0Ke;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Ke;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Ke;

.field public static final enum PAUSED:LX/0Ke;

.field public static final enum PLAYING:LX/0Ke;

.field public static final enum UNKNOWN:LX/0Ke;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41578
    new-instance v0, LX/0Ke;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/0Ke;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Ke;->UNKNOWN:LX/0Ke;

    .line 41579
    new-instance v0, LX/0Ke;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v3}, LX/0Ke;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Ke;->PLAYING:LX/0Ke;

    .line 41580
    new-instance v0, LX/0Ke;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v4}, LX/0Ke;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Ke;->PAUSED:LX/0Ke;

    .line 41581
    const/4 v0, 0x3

    new-array v0, v0, [LX/0Ke;

    sget-object v1, LX/0Ke;->UNKNOWN:LX/0Ke;

    aput-object v1, v0, v2

    sget-object v1, LX/0Ke;->PLAYING:LX/0Ke;

    aput-object v1, v0, v3

    sget-object v1, LX/0Ke;->PAUSED:LX/0Ke;

    aput-object v1, v0, v4

    sput-object v0, LX/0Ke;->$VALUES:[LX/0Ke;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41582
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Ke;
    .locals 1

    .prologue
    .line 41583
    const-class v0, LX/0Ke;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Ke;

    return-object v0
.end method

.method public static values()[LX/0Ke;
    .locals 1

    .prologue
    .line 41584
    sget-object v0, LX/0Ke;->$VALUES:[LX/0Ke;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Ke;

    return-object v0
.end method
