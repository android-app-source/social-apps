.class public LX/0GB;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:I

.field public static final b:Ljava/lang/String;


# instance fields
.field private final c:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "LX/0GA;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mDashChunkDataCache"
    .end annotation
.end field

.field public final d:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34375
    const/high16 v0, 0x100000

    sput v0, LX/0GB;->a:I

    .line 34376
    const-class v0, LX/0GB;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0GB;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34388
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/0GB;->c:Landroid/util/LruCache;

    .line 34389
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/0GB;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 34390
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 34377
    if-nez p1, :cond_0

    .line 34378
    move-object v0, v1

    .line 34379
    :goto_0
    return-object v0

    .line 34380
    :cond_0
    iget-object v2, p0, LX/0GB;->c:Landroid/util/LruCache;

    monitor-enter v2

    .line 34381
    :try_start_0
    iget-object v0, p0, LX/0GB;->c:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GA;

    .line 34382
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34383
    if-eqz v0, :cond_1

    .line 34384
    invoke-virtual {v0}, LX/0GA;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 34385
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object v0, v1

    .line 34386
    goto :goto_0
.end method

.method public final a(II)V
    .locals 6

    .prologue
    .line 34391
    mul-int/lit8 v0, p1, 0x2

    .line 34392
    iget-object v1, p0, LX/0GB;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 34393
    if-lez v0, :cond_0

    iget-object v2, p0, LX/0GB;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34394
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    .line 34395
    :cond_0
    iget-object v1, p0, LX/0GB;->c:Landroid/util/LruCache;

    monitor-enter v1

    .line 34396
    :try_start_0
    iget-object v0, p0, LX/0GB;->c:Landroid/util/LruCache;

    const-string v2, "DashChunkMemoryCache"

    invoke-static {v0, p2, v2}, LX/0Gj;->a(Landroid/util/LruCache;ILjava/lang/String;)Landroid/util/LruCache;

    .line 34397
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;[BI)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 34345
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 34346
    :cond_0
    :goto_0
    return-void

    .line 34347
    :cond_1
    iget-object v1, p0, LX/0GB;->c:Landroid/util/LruCache;

    monitor-enter v1

    .line 34348
    :try_start_0
    iget-object v0, p0, LX/0GB;->c:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GA;

    .line 34349
    if-nez v0, :cond_2

    .line 34350
    new-instance v0, LX/0GA;

    invoke-direct {v0, p0}, LX/0GA;-><init>(LX/0GB;)V

    .line 34351
    iget-object v2, p0, LX/0GB;->c:Landroid/util/LruCache;

    invoke-virtual {v2, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34352
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34353
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    const/4 v4, 0x1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 34354
    invoke-virtual {v0, p2, p3, p4}, LX/0GA;->a(Landroid/net/Uri;[BI)V

    goto :goto_0

    .line 34355
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 34356
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 34357
    :cond_0
    :goto_0
    return-void

    .line 34358
    :cond_1
    iget-object v1, p0, LX/0GB;->c:Landroid/util/LruCache;

    monitor-enter v1

    .line 34359
    :try_start_0
    iget-object v0, p0, LX/0GB;->c:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GA;

    .line 34360
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34361
    if-eqz v0, :cond_0

    .line 34362
    invoke-virtual {v0, p2}, LX/0GA;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 34363
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;)[B
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 34364
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 34365
    :cond_0
    move-object v0, v1

    .line 34366
    :goto_0
    return-object v0

    .line 34367
    :cond_1
    iget-object v2, p0, LX/0GB;->c:Landroid/util/LruCache;

    monitor-enter v2

    .line 34368
    :try_start_0
    iget-object v0, p0, LX/0GB;->c:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GA;

    .line 34369
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34370
    if-eqz v0, :cond_2

    .line 34371
    invoke-virtual {v0, p2}, LX/0GA;->a(Landroid/net/Uri;)LX/0G9;

    move-result-object v0

    .line 34372
    if-eqz v0, :cond_2

    .line 34373
    iget-object v0, v0, LX/0G9;->a:[B

    goto :goto_0

    .line 34374
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method
