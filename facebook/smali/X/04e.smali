.class public LX/04e;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/net/URLConnection;I)V
    .locals 4

    .prologue
    const/16 v3, 0x1f

    const/16 v2, 0x8

    .line 13969
    const/16 v0, 0x1e

    invoke-static {v2, v0, p1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 13970
    :try_start_0
    invoke-virtual {p0}, Ljava/net/URLConnection;->connect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13971
    invoke-static {v2, v3, p1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 13972
    return-void

    .line 13973
    :catchall_0
    move-exception v1

    invoke-static {v2, v3, p1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1
.end method

.method public static b(Ljava/net/URLConnection;I)Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 13967
    new-instance v0, LX/050;

    invoke-virtual {p0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/050;-><init>(Ljava/io/InputStream;I)V

    return-object v0
.end method

.method public static c(Ljava/net/URLConnection;I)Ljava/io/OutputStream;
    .locals 2

    .prologue
    .line 13968
    new-instance v0, LX/04f;

    invoke-virtual {p0}, Ljava/net/URLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/04f;-><init>(Ljava/io/OutputStream;I)V

    return-object v0
.end method
