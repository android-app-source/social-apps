.class public final LX/01v;
.super LX/01t;
.source ""


# instance fields
.field public final synthetic a:LX/000;


# direct methods
.method public constructor <init>(LX/000;)V
    .locals 1

    .prologue
    .line 5755
    iput-object p1, p0, LX/01v;->a:LX/000;

    invoke-direct {p0}, LX/01t;-><init>()V

    .line 5756
    iget-boolean v0, p1, LX/000;->t:Z

    if-eqz v0, :cond_0

    .line 5757
    invoke-virtual {p0}, LX/01t;->b()V

    .line 5758
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Handler;Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 5759
    iget-object v0, p0, LX/01v;->a:LX/000;

    iget-boolean v0, v0, LX/000;->t:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/01v;->a:LX/000;

    iget-object v0, v0, LX/000;->o:Landroid/os/Handler;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/01v;->a:LX/000;

    invoke-virtual {v0, p2}, LX/000;->a(Landroid/os/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/01v;->a:LX/000;

    invoke-static {v0, p2}, LX/000;->c(LX/000;Landroid/os/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5760
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Special delaying init message "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5761
    :goto_0
    return-void

    .line 5762
    :cond_0
    iget-object v0, p0, LX/01v;->a:LX/000;

    invoke-static {v0, p0, p1, p2}, LX/000;->a$redex0(LX/000;LX/01v;Landroid/os/Handler;Landroid/os/Message;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5763
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "delaying init message "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5764
    iget-object v0, p0, LX/01v;->a:LX/000;

    .line 5765
    iget-object v1, v0, LX/000;->p:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 5766
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, LX/000;->p:Ljava/util/ArrayList;

    .line 5767
    :cond_1
    iget-object v1, v0, LX/000;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5768
    goto :goto_0

    .line 5769
    :cond_2
    invoke-super {p0, p1, p2}, LX/01t;->a(Landroid/os/Handler;Landroid/os/Message;)V

    goto :goto_0
.end method
