.class public final enum LX/07E;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/07E;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/07E;

.field public static final enum DEFAULT:LX/07E;

.field public static final enum HE_NONPREFERRED:LX/07E;

.field public static final enum HE_PREFERRED:LX/07E;

.field public static final enum SEQ_NONPREFERRED:LX/07E;

.field public static final enum SEQ_PREFERRED:LX/07E;

.field public static final enum UNKNOWN:LX/07E;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19386
    new-instance v0, LX/07E;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/07E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/07E;->UNKNOWN:LX/07E;

    .line 19387
    new-instance v0, LX/07E;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v4}, LX/07E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/07E;->DEFAULT:LX/07E;

    .line 19388
    new-instance v0, LX/07E;

    const-string v1, "SEQ_PREFERRED"

    invoke-direct {v0, v1, v5}, LX/07E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/07E;->SEQ_PREFERRED:LX/07E;

    .line 19389
    new-instance v0, LX/07E;

    const-string v1, "SEQ_NONPREFERRED"

    invoke-direct {v0, v1, v6}, LX/07E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/07E;->SEQ_NONPREFERRED:LX/07E;

    .line 19390
    new-instance v0, LX/07E;

    const-string v1, "HE_PREFERRED"

    invoke-direct {v0, v1, v7}, LX/07E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/07E;->HE_PREFERRED:LX/07E;

    .line 19391
    new-instance v0, LX/07E;

    const-string v1, "HE_NONPREFERRED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/07E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/07E;->HE_NONPREFERRED:LX/07E;

    .line 19392
    const/4 v0, 0x6

    new-array v0, v0, [LX/07E;

    sget-object v1, LX/07E;->UNKNOWN:LX/07E;

    aput-object v1, v0, v3

    sget-object v1, LX/07E;->DEFAULT:LX/07E;

    aput-object v1, v0, v4

    sget-object v1, LX/07E;->SEQ_PREFERRED:LX/07E;

    aput-object v1, v0, v5

    sget-object v1, LX/07E;->SEQ_NONPREFERRED:LX/07E;

    aput-object v1, v0, v6

    sget-object v1, LX/07E;->HE_PREFERRED:LX/07E;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/07E;->HE_NONPREFERRED:LX/07E;

    aput-object v2, v0, v1

    sput-object v0, LX/07E;->$VALUES:[LX/07E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19394
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/07E;
    .locals 1

    .prologue
    .line 19395
    const-class v0, LX/07E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/07E;

    return-object v0
.end method

.method public static values()[LX/07E;
    .locals 1

    .prologue
    .line 19393
    sget-object v0, LX/07E;->$VALUES:[LX/07E;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/07E;

    return-object v0
.end method
