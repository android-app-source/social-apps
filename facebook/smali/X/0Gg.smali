.class public LX/0Gg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/04n;


# instance fields
.field private final a:LX/0OX;

.field private final b:Ljava/lang/String;

.field private final c:LX/0Gk;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Z

.field private final e:Z

.field private final f:LX/04m;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:J

.field private j:J

.field private k:I

.field private l:Ljava/io/IOException;

.field private m:I

.field private n:LX/0AC;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Gk;ZZLjava/lang/String;LX/04m;Ljava/lang/String;)V
    .locals 1
    .param p2    # LX/0Gk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 36189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36190
    iput-object p2, p0, LX/0Gg;->c:LX/0Gk;

    .line 36191
    new-instance v0, LX/08J;

    invoke-direct {v0}, LX/08J;-><init>()V

    iput-object v0, p0, LX/0Gg;->a:LX/0OX;

    .line 36192
    iput-object p1, p0, LX/0Gg;->b:Ljava/lang/String;

    .line 36193
    iput-boolean p3, p0, LX/0Gg;->d:Z

    .line 36194
    iput-boolean p4, p0, LX/0Gg;->e:Z

    .line 36195
    iput-object p5, p0, LX/0Gg;->g:Ljava/lang/String;

    .line 36196
    iput-object p6, p0, LX/0Gg;->f:LX/04m;

    .line 36197
    iput-object p7, p0, LX/0Gg;->o:Ljava/lang/String;

    .line 36198
    sget-object v0, LX/0AC;->NOT_APPLY:LX/0AC;

    iput-object v0, p0, LX/0Gg;->n:LX/0AC;

    .line 36199
    return-void
.end method

.method private d()V
    .locals 15

    .prologue
    const-wide/16 v12, -0x1

    .line 36200
    iget-object v0, p0, LX/0Gg;->a:LX/0OX;

    invoke-interface {v0}, LX/0OX;->a()J

    move-result-wide v8

    .line 36201
    new-instance v0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;

    iget-object v1, p0, LX/0Gg;->b:Ljava/lang/String;

    iget-object v2, p0, LX/0Gg;->h:Ljava/lang/String;

    iget-object v3, p0, LX/0Gg;->l:Ljava/io/IOException;

    if-eqz v3, :cond_1

    iget-object v3, p0, LX/0Gg;->l:Ljava/io/IOException;

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    :goto_0
    iget-boolean v4, p0, LX/0Gg;->d:Z

    iget v5, p0, LX/0Gg;->k:I

    iget-wide v6, p0, LX/0Gg;->j:J

    iget-wide v10, p0, LX/0Gg;->i:J

    sub-long/2addr v6, v10

    invoke-static {v6, v7, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    iget-wide v10, p0, LX/0Gg;->i:J

    sub-long/2addr v8, v10

    invoke-static {v8, v9, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    iget v10, p0, LX/0Gg;->m:I

    iget-object v11, p0, LX/0Gg;->n:LX/0AC;

    iget-boolean v12, p0, LX/0Gg;->e:Z

    iget-object v13, p0, LX/0Gg;->g:Ljava/lang/String;

    iget-object v14, p0, LX/0Gg;->o:Ljava/lang/String;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIJJILX/0AC;ZLjava/lang/String;Ljava/lang/String;)V

    .line 36202
    iget-object v1, p0, LX/0Gg;->c:LX/0Gk;

    if-eqz v1, :cond_0

    .line 36203
    iget-object v1, p0, LX/0Gg;->c:LX/0Gk;

    sget-object v2, LX/0H9;->HTTP_TRANSFER_END:LX/0H9;

    invoke-interface {v1, v2, v0}, LX/0Gk;->a(LX/0H9;Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V

    .line 36204
    :cond_0
    return-void

    .line 36205
    :cond_1
    const-string v3, ""

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 36206
    iget-object v0, p0, LX/0Gg;->f:LX/04m;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Gg;->n:LX/0AC;

    sget-object v1, LX/0AC;->NOT_CACHED:LX/0AC;

    if-ne v0, v1, :cond_0

    .line 36207
    iget-object v0, p0, LX/0Gg;->f:LX/04m;

    invoke-interface {v0, p1}, LX/04n;->a(I)V

    .line 36208
    :cond_0
    iget v0, p0, LX/0Gg;->k:I

    add-int/2addr v0, p1

    iput v0, p0, LX/0Gg;->k:I

    .line 36209
    return-void
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 0

    .prologue
    .line 36210
    iput-object p1, p0, LX/0Gg;->l:Ljava/io/IOException;

    .line 36211
    invoke-direct {p0}, LX/0Gg;->d()V

    .line 36212
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0AC;)V
    .locals 2

    .prologue
    .line 36213
    iput-object p1, p0, LX/0Gg;->h:Ljava/lang/String;

    .line 36214
    iget-object v0, p0, LX/0Gg;->a:LX/0OX;

    invoke-interface {v0}, LX/0OX;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0Gg;->i:J

    .line 36215
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0Gg;->j:J

    .line 36216
    const/4 v0, 0x0

    iput v0, p0, LX/0Gg;->k:I

    .line 36217
    iget v0, p0, LX/0Gg;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Gg;->m:I

    .line 36218
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Gg;->l:Ljava/io/IOException;

    .line 36219
    iput-object p2, p0, LX/0Gg;->n:LX/0AC;

    .line 36220
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 36221
    iget-object v0, p0, LX/0Gg;->f:LX/04m;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Gg;->n:LX/0AC;

    sget-object v1, LX/0AC;->NOT_CACHED:LX/0AC;

    if-ne v0, v1, :cond_0

    .line 36222
    iget-object v0, p0, LX/0Gg;->f:LX/04m;

    invoke-interface {v0}, LX/04n;->b()V

    .line 36223
    :cond_0
    iget-object v0, p0, LX/0Gg;->a:LX/0OX;

    invoke-interface {v0}, LX/0OX;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0Gg;->j:J

    .line 36224
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 36225
    iget-object v0, p0, LX/0Gg;->f:LX/04m;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Gg;->n:LX/0AC;

    sget-object v1, LX/0AC;->NOT_CACHED:LX/0AC;

    if-ne v0, v1, :cond_0

    .line 36226
    iget-object v0, p0, LX/0Gg;->f:LX/04m;

    invoke-interface {v0}, LX/04n;->c()V

    .line 36227
    :cond_0
    iget-object v0, p0, LX/0Gg;->l:Ljava/io/IOException;

    if-nez v0, :cond_1

    .line 36228
    invoke-direct {p0}, LX/0Gg;->d()V

    .line 36229
    :cond_1
    return-void
.end method
