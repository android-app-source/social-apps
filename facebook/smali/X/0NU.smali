.class public final LX/0NU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0NT;


# instance fields
.field private final a:[B

.field private final b:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "LX/0NS;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0NX;

.field private d:LX/0NV;

.field private e:I

.field private f:I

.field private g:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50437
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, LX/0NU;->a:[B

    .line 50438
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, LX/0NU;->b:Ljava/util/Stack;

    .line 50439
    new-instance v0, LX/0NX;

    invoke-direct {v0}, LX/0NX;-><init>()V

    iput-object v0, p0, LX/0NU;->c:LX/0NX;

    .line 50440
    return-void
.end method

.method public static a(LX/0NU;LX/0MA;I)J
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 50430
    iget-object v1, p0, LX/0NU;->a:[B

    invoke-interface {p1, v1, v0, p2}, LX/0MA;->b([BII)V

    .line 50431
    const-wide/16 v2, 0x0

    .line 50432
    :goto_0
    if-ge v0, p2, :cond_0

    .line 50433
    const/16 v1, 0x8

    shl-long/2addr v2, v1

    iget-object v1, p0, LX/0NU;->a:[B

    aget-byte v1, v1, v0

    and-int/lit16 v1, v1, 0xff

    int-to-long v4, v1

    or-long/2addr v2, v4

    .line 50434
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50435
    :cond_0
    return-wide v2
.end method

.method private b(LX/0MA;)J
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 50362
    invoke-interface {p1}, LX/0MA;->a()V

    .line 50363
    :goto_0
    iget-object v0, p0, LX/0NU;->a:[B

    invoke-interface {p1, v0, v4, v5}, LX/0MA;->c([BII)V

    .line 50364
    iget-object v0, p0, LX/0NU;->a:[B

    aget-byte v0, v0, v4

    invoke-static {v0}, LX/0NX;->a(I)I

    move-result v0

    .line 50365
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    if-gt v0, v5, :cond_0

    .line 50366
    iget-object v1, p0, LX/0NU;->a:[B

    invoke-static {v1, v0, v4}, LX/0NX;->a([BIZ)J

    move-result-wide v2

    long-to-int v1, v2

    .line 50367
    iget-object v2, p0, LX/0NU;->d:LX/0NV;

    invoke-interface {v2, v1}, LX/0NV;->b(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 50368
    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    .line 50369
    int-to-long v0, v1

    return-wide v0

    .line 50370
    :cond_0
    const/4 v0, 0x1

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 50426
    const/4 v0, 0x0

    iput v0, p0, LX/0NU;->e:I

    .line 50427
    iget-object v0, p0, LX/0NU;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 50428
    iget-object v0, p0, LX/0NU;->c:LX/0NX;

    invoke-virtual {v0}, LX/0NX;->a()V

    .line 50429
    return-void
.end method

.method public final a(LX/0NV;)V
    .locals 0

    .prologue
    .line 50424
    iput-object p1, p0, LX/0NU;->d:LX/0NV;

    .line 50425
    return-void
.end method

.method public final a(LX/0MA;)Z
    .locals 12

    .prologue
    const-wide/16 v4, 0x8

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 50371
    iget-object v0, p0, LX/0NU;->d:LX/0NV;

    if-eqz v0, :cond_0

    move v0, v6

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 50372
    :goto_1
    iget-object v0, p0, LX/0NU;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v2

    iget-object v0, p0, LX/0NU;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0NS;

    iget-wide v0, v0, LX/0NS;->b:J

    cmp-long v0, v2, v0

    if-ltz v0, :cond_1

    .line 50373
    iget-object v1, p0, LX/0NU;->d:LX/0NV;

    iget-object v0, p0, LX/0NU;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0NS;

    iget v0, v0, LX/0NS;->a:I

    invoke-interface {v1, v0}, LX/0NV;->c(I)V

    .line 50374
    :goto_2
    return v6

    :cond_0
    move v0, v7

    .line 50375
    goto :goto_0

    .line 50376
    :cond_1
    iget v0, p0, LX/0NU;->e:I

    if-nez v0, :cond_4

    .line 50377
    iget-object v0, p0, LX/0NU;->c:LX/0NX;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v6, v7, v1}, LX/0NX;->a(LX/0MA;ZZI)J

    move-result-wide v0

    .line 50378
    const-wide/16 v2, -0x2

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 50379
    invoke-direct {p0, p1}, LX/0NU;->b(LX/0MA;)J

    move-result-wide v0

    .line 50380
    :cond_2
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    move v6, v7

    .line 50381
    goto :goto_2

    .line 50382
    :cond_3
    long-to-int v0, v0

    iput v0, p0, LX/0NU;->f:I

    .line 50383
    iput v6, p0, LX/0NU;->e:I

    .line 50384
    :cond_4
    iget v0, p0, LX/0NU;->e:I

    if-ne v0, v6, :cond_5

    .line 50385
    iget-object v0, p0, LX/0NU;->c:LX/0NX;

    const/16 v1, 0x8

    invoke-virtual {v0, p1, v7, v6, v1}, LX/0NX;->a(LX/0MA;ZZI)J

    move-result-wide v0

    iput-wide v0, p0, LX/0NU;->g:J

    .line 50386
    const/4 v0, 0x2

    iput v0, p0, LX/0NU;->e:I

    .line 50387
    :cond_5
    iget-object v0, p0, LX/0NU;->d:LX/0NV;

    iget v1, p0, LX/0NU;->f:I

    invoke-interface {v0, v1}, LX/0NV;->a(I)I

    move-result v0

    .line 50388
    packed-switch v0, :pswitch_data_0

    .line 50389
    new-instance v1, LX/0L6;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid element type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v1

    .line 50390
    :pswitch_0
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v2

    .line 50391
    iget-wide v0, p0, LX/0NU;->g:J

    add-long/2addr v0, v2

    .line 50392
    iget-object v4, p0, LX/0NU;->b:Ljava/util/Stack;

    new-instance v5, LX/0NS;

    iget v8, p0, LX/0NU;->f:I

    invoke-direct {v5, v8, v0, v1}, LX/0NS;-><init>(IJ)V

    invoke-virtual {v4, v5}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 50393
    iget-object v0, p0, LX/0NU;->d:LX/0NV;

    iget v1, p0, LX/0NU;->f:I

    iget-wide v4, p0, LX/0NU;->g:J

    invoke-interface/range {v0 .. v5}, LX/0NV;->a(IJJ)V

    .line 50394
    iput v7, p0, LX/0NU;->e:I

    goto :goto_2

    .line 50395
    :pswitch_1
    iget-wide v0, p0, LX/0NU;->g:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_6

    .line 50396
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid integer size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/0NU;->g:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50397
    :cond_6
    iget-object v0, p0, LX/0NU;->d:LX/0NV;

    iget v1, p0, LX/0NU;->f:I

    iget-wide v2, p0, LX/0NU;->g:J

    long-to-int v2, v2

    invoke-static {p0, p1, v2}, LX/0NU;->a(LX/0NU;LX/0MA;I)J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0NV;->a(IJ)V

    .line 50398
    iput v7, p0, LX/0NU;->e:I

    goto/16 :goto_2

    .line 50399
    :pswitch_2
    iget-wide v0, p0, LX/0NU;->g:J

    const-wide/16 v2, 0x4

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7

    iget-wide v0, p0, LX/0NU;->g:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 50400
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid float size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/0NU;->g:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50401
    :cond_7
    iget-object v0, p0, LX/0NU;->d:LX/0NV;

    iget v1, p0, LX/0NU;->f:I

    iget-wide v2, p0, LX/0NU;->g:J

    long-to-int v2, v2

    .line 50402
    invoke-static {p0, p1, v2}, LX/0NU;->a(LX/0NU;LX/0MA;I)J

    move-result-wide v9

    .line 50403
    const/4 v11, 0x4

    if-ne v2, v11, :cond_9

    .line 50404
    long-to-int v9, v9

    invoke-static {v9}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v9

    float-to-double v9, v9

    .line 50405
    :goto_3
    move-wide v2, v9

    .line 50406
    invoke-interface {v0, v1, v2, v3}, LX/0NV;->a(ID)V

    .line 50407
    iput v7, p0, LX/0NU;->e:I

    goto/16 :goto_2

    .line 50408
    :pswitch_3
    iget-wide v0, p0, LX/0NU;->g:J

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-lez v0, :cond_8

    .line 50409
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "String element size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/0NU;->g:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50410
    :cond_8
    iget-object v0, p0, LX/0NU;->d:LX/0NV;

    iget v1, p0, LX/0NU;->f:I

    iget-wide v2, p0, LX/0NU;->g:J

    long-to-int v2, v2

    .line 50411
    if-nez v2, :cond_a

    .line 50412
    const-string v3, ""

    .line 50413
    :goto_4
    move-object v2, v3

    .line 50414
    invoke-interface {v0, v1, v2}, LX/0NV;->a(ILjava/lang/String;)V

    .line 50415
    iput v7, p0, LX/0NU;->e:I

    goto/16 :goto_2

    .line 50416
    :pswitch_4
    iget-object v0, p0, LX/0NU;->d:LX/0NV;

    iget v1, p0, LX/0NU;->f:I

    iget-wide v2, p0, LX/0NU;->g:J

    long-to-int v2, v2

    invoke-interface {v0, v1, v2, p1}, LX/0NV;->a(IILX/0MA;)V

    .line 50417
    iput v7, p0, LX/0NU;->e:I

    goto/16 :goto_2

    .line 50418
    :pswitch_5
    iget-wide v0, p0, LX/0NU;->g:J

    long-to-int v0, v0

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    .line 50419
    iput v7, p0, LX/0NU;->e:I

    goto/16 :goto_1

    .line 50420
    :cond_9
    invoke-static {v9, v10}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v9

    goto :goto_3

    .line 50421
    :cond_a
    new-array v4, v2, [B

    .line 50422
    const/4 v3, 0x0

    invoke-interface {p1, v4, v3, v2}, LX/0MA;->b([BII)V

    .line 50423
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method
