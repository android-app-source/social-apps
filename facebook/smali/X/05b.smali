.class public LX/05b;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/content/IntentFilter;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Landroid/os/PowerManager;

.field public final d:Landroid/os/Handler;

.field public final e:Landroid/content/BroadcastReceiver;

.field public final f:Ljava/util/concurrent/atomic/AtomicLong;

.field public final g:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0AD;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16422
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 16423
    sput-object v0, LX/05b;->a:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 16424
    sget-object v0, LX/05b;->a:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 16425
    sget-object v0, LX/05b;->a:Landroid/content/IntentFilter;

    const/16 v1, 0x3e7

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 16426
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/PowerManager;Landroid/os/Handler;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;)V
    .locals 4

    .prologue
    .line 16427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16428
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, -0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/05b;->f:Ljava/util/concurrent/atomic/AtomicLong;

    .line 16429
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/05b;->g:Ljava/util/concurrent/atomic/AtomicReference;

    .line 16430
    iput-object p1, p0, LX/05b;->b:Landroid/content/Context;

    .line 16431
    iput-object p2, p0, LX/05b;->c:Landroid/os/PowerManager;

    .line 16432
    iput-object p3, p0, LX/05b;->d:Landroid/os/Handler;

    .line 16433
    new-instance v0, LX/05c;

    invoke-direct {v0, p0, p4}, LX/05c;-><init>(LX/05b;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;)V

    iput-object v0, p0, LX/05b;->e:Landroid/content/BroadcastReceiver;

    .line 16434
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 16416
    iget-object v0, p0, LX/05b;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 16417
    if-eqz v0, :cond_0

    .line 16418
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 16419
    :goto_0
    return v0

    .line 16420
    :cond_0
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_1

    iget-object v0, p0, LX/05b;->c:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/05b;->c:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 16421
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 16415
    iget-object v0, p0, LX/05b;->f:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method
