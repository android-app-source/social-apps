.class public final LX/0B4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/0Hf;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26104
    const-class v0, LX/0B4;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0B4;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26106
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/0B4;->b:Ljava/util/Queue;

    .line 26107
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0B4;->c:Z

    .line 26108
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 26109
    iget-object v1, p0, LX/0B4;->b:Ljava/util/Queue;

    monitor-enter v1

    .line 26110
    :try_start_0
    iget-boolean v0, p0, LX/0B4;->c:Z

    if-eqz v0, :cond_1

    .line 26111
    monitor-exit v1

    .line 26112
    :cond_0
    return-void

    .line 26113
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0B4;->c:Z

    .line 26114
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26115
    :goto_0
    iget-object v0, p0, LX/0B4;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 26116
    iget-object v0, p0, LX/0B4;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Hf;

    invoke-virtual {v0}, LX/0Hf;->a()V

    goto :goto_0

    .line 26117
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 4

    .prologue
    .line 26118
    invoke-static {p1}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26119
    invoke-static {p2}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26120
    const/4 v0, 0x0

    .line 26121
    iget-object v1, p0, LX/0B4;->b:Ljava/util/Queue;

    monitor-enter v1

    .line 26122
    :try_start_0
    iget-boolean v2, p0, LX/0B4;->c:Z

    if-nez v2, :cond_1

    .line 26123
    iget-object v2, p0, LX/0B4;->b:Ljava/util/Queue;

    new-instance v3, LX/0Hf;

    invoke-direct {v3, p1, p2}, LX/0Hf;-><init>(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    invoke-interface {v2, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 26124
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26125
    if-eqz v0, :cond_0

    .line 26126
    new-instance v0, LX/0Hf;

    invoke-direct {v0, p1, p2}, LX/0Hf;-><init>(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v0}, LX/0Hf;->a()V

    .line 26127
    :cond_0
    return-void

    .line 26128
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 26129
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
