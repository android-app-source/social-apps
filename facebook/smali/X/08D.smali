.class public final LX/08D;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final cpuPriority:I

.field public final ioPriority:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 20948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20949
    iput p1, p0, LX/08D;->ioPriority:I

    .line 20950
    iput p2, p0, LX/08D;->cpuPriority:I

    .line 20951
    return-void
.end method

.method public static unchanged()LX/08D;
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 20952
    new-instance v0, LX/08D;

    invoke-direct {v0, v1, v1}, LX/08D;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public final isDefault()Z
    .locals 1

    .prologue
    .line 20953
    iget v0, p0, LX/08D;->ioPriority:I

    if-gez v0, :cond_0

    iget v0, p0, LX/08D;->cpuPriority:I

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final with()LX/0Fk;
    .locals 1

    .prologue
    .line 20954
    new-instance v0, LX/0Fk;

    invoke-direct {v0, p0}, LX/0Fk;-><init>(LX/08D;)V

    return-object v0
.end method
