.class public LX/00l;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Runnable;I)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 2896
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2897
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2898
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;

    invoke-direct {v0, p0, p1}, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;-><init>(Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/String;I)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 2899
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2900
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 2901
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;-><init>(Ljava/lang/Runnable;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;JI)Ljava/lang/Thread;
    .locals 7

    .prologue
    .line 2902
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2903
    new-instance v0, Ljava/lang/Thread;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;J)V

    .line 2904
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;JI)V

    goto :goto_0
.end method
