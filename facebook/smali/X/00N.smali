.class public LX/00N;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final LOG_TAG:Ljava/lang/String;


# instance fields
.field public mANRDataProvider:LX/09r;

.field public mContext:Landroid/content/Context;

.field public final mErrorReporter:LX/009;

.field private final mFileGenerator:LX/01i;

.field public mPerformanceMarker:LX/09s;

.field private mTracesFile:Ljava/io/File;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2201
    const-class v0, LX/00N;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/00N;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/009;)V
    .locals 3

    .prologue
    .line 2202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2203
    iput-object p1, p0, LX/00N;->mContext:Landroid/content/Context;

    .line 2204
    iput-object p2, p0, LX/00N;->mErrorReporter:LX/009;

    .line 2205
    new-instance v0, LX/01i;

    const-string v1, ".cachedreport"

    const-string v2, "traces"

    invoke-direct {v0, p1, v1, v2}, LX/01i;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/00N;->mFileGenerator:LX/01i;

    .line 2206
    return-void
.end method

.method public static purgeDirectory(Ljava/io/File;)V
    .locals 7

    .prologue
    .line 2207
    if-eqz p0, :cond_2

    .line 2208
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 2209
    if-eqz v0, :cond_2

    .line 2210
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2211
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2212
    invoke-static {v3}, LX/00N;->purgeDirectory(Ljava/io/File;)V

    .line 2213
    :cond_0
    const/4 v4, 0x1

    .line 2214
    if-nez v3, :cond_3

    .line 2215
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2216
    :cond_2
    return-void

    .line 2217
    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v5

    .line 2218
    if-nez v5, :cond_4

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_4

    .line 2219
    :goto_2
    if-nez v4, :cond_1

    .line 2220
    sget-object v5, LX/00N;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string p0, "Could not delete error report: "

    invoke-direct {v6, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    move v4, v5

    goto :goto_2
.end method


# virtual methods
.method public collectThreadDump(JJZ)V
    .locals 9

    .prologue
    .line 2221
    iget-object v0, p0, LX/00N;->mPerformanceMarker:LX/09s;

    if-eqz v0, :cond_0

    .line 2222
    iget-object v0, p0, LX/00N;->mPerformanceMarker:LX/09s;

    invoke-interface {v0}, LX/09s;->markerStart()V

    .line 2223
    :cond_0
    iget-object v0, p0, LX/00N;->mANRDataProvider:LX/09r;

    if-eqz v0, :cond_1

    .line 2224
    iget-object v0, p0, LX/00N;->mANRDataProvider:LX/09r;

    iget-object v1, p0, LX/00N;->mErrorReporter:LX/009;

    invoke-interface {v0, v1}, LX/09r;->provideStats(LX/009;)V

    .line 2225
    :cond_1
    iget-object v0, p0, LX/00N;->mErrorReporter:LX/009;

    const-string v1, "anr_timeout_delay"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 2226
    iget-object v0, p0, LX/00N;->mTracesFile:Ljava/io/File;

    if-nez v0, :cond_2

    .line 2227
    new-instance v0, LX/01i;

    iget-object v1, p0, LX/00N;->mContext:Landroid/content/Context;

    const-string v2, ".stacktrace"

    const-string v3, "traces"

    invoke-direct {v0, v1, v2, v3}, LX/01i;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, LX/01i;->generate()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/00N;->mTracesFile:Ljava/io/File;

    .line 2228
    :cond_2
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v0, p0, LX/00N;->mTracesFile:Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v1, 0x0

    .line 2229
    :try_start_1
    invoke-static {v2}, LX/0Bl;->dumpStackTraces(Ljava/io/OutputStream;)V

    .line 2230
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Dumped traces to: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/00N;->mTracesFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, LX/00N;->mTracesFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " bytes)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2231
    iget-object v0, p0, LX/00N;->mErrorReporter:LX/009;

    const-string v3, "anr_detect_time_tag"

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 2232
    iget-object v0, p0, LX/00N;->mErrorReporter:LX/009;

    const-string v3, "anr_recovery_delay"

    const-string v4, "-1"

    invoke-virtual {v0, v3, v4}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 2233
    iget-object v0, p0, LX/00N;->mErrorReporter:LX/009;

    const-string v3, "anr_detected_pre_gkstore"

    invoke-static {p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 2234
    iget-object v0, p0, LX/00N;->mErrorReporter:LX/009;

    const v3, 0x7fffffff

    iget-object v4, p0, LX/00N;->mFileGenerator:LX/01i;

    const/4 v5, 0x1

    new-array v5, v5, [LX/01V;

    const/4 v6, 0x0

    sget-object v7, LX/01V;->ANR_REPORT:LX/01V;

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v4, v5}, LX/009;->prepareReports(ILX/01i;[LX/01V;)I

    .line 2235
    iget-object v0, p0, LX/00N;->mPerformanceMarker:LX/09s;

    if-eqz v0, :cond_3

    .line 2236
    iget-object v0, p0, LX/00N;->mPerformanceMarker:LX/09s;

    const/4 v3, 0x2

    invoke-interface {v0, v3}, LX/09s;->markerEnd(S)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2237
    :cond_3
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2238
    return-void

    .line 2239
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2240
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_0
    if-eqz v1, :cond_5

    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :goto_1
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    .line 2241
    iget-object v1, p0, LX/00N;->mPerformanceMarker:LX/09s;

    if-eqz v1, :cond_4

    .line 2242
    iget-object v1, p0, LX/00N;->mPerformanceMarker:LX/09s;

    const/4 v2, 0x3

    invoke-interface {v1, v2}, LX/09s;->markerEnd(S)V

    .line 2243
    :cond_4
    throw v0

    .line 2244
    :catch_2
    move-exception v2

    :try_start_6
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method
