.class public LX/0DA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0D9;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/app/Activity;

.field public final d:Landroid/view/View;

.field public e:LX/0D5;

.field public f:LX/0DD;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 29649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29650
    const-string v0, "BrowserLiteIntent.AutofillExtras.EXTRA_FB_AUTOFILL_SDK_URL"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0DA;->b:Ljava/lang/String;

    .line 29651
    iput-object p1, p0, LX/0DA;->c:Landroid/app/Activity;

    .line 29652
    iput-object p2, p0, LX/0DA;->d:Landroid/view/View;

    .line 29653
    const-string v0, "BrowserLiteIntent.AutofillExtras.EXTRA_FB_AUTOFILL_SHOW_HORIZONTAL_SCROLL_BAR"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 29654
    new-instance v1, LX/0D9;

    iget-object v2, p0, LX/0DA;->c:Landroid/app/Activity;

    iget-object v3, p0, LX/0DA;->d:Landroid/view/View;

    invoke-direct {v1, v2, v3, v0}, LX/0D9;-><init>(Landroid/app/Activity;Landroid/view/View;Z)V

    iput-object v1, p0, LX/0DA;->a:LX/0D9;

    .line 29655
    return-void
.end method
