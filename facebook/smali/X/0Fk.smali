.class public final LX/0Fk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final savedCpuPriority:I

.field private final savedIoPriority:I

.field public final synthetic this$0:LX/08D;


# direct methods
.method public constructor <init>(LX/08D;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v1, -0x80000000

    .line 33975
    iput-object p1, p0, LX/0Fk;->this$0:LX/08D;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33976
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v2

    .line 33977
    iget v0, p1, LX/08D;->ioPriority:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_0
    iput v0, p0, LX/0Fk;->savedIoPriority:I

    .line 33978
    iget v0, p1, LX/08D;->cpuPriority:I

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    iput v0, p0, LX/0Fk;->savedCpuPriority:I

    .line 33979
    :try_start_0
    iget v0, p1, LX/08D;->cpuPriority:I

    if-eq v0, v1, :cond_0

    .line 33980
    iget v0, p1, LX/08D;->cpuPriority:I

    invoke-static {v2, v0}, Landroid/os/Process;->setThreadPriority(II)V

    .line 33981
    :cond_0
    iget v0, p1, LX/08D;->ioPriority:I

    if-eq v0, v1, :cond_1

    .line 33982
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p1, LX/08D;->ioPriority:I

    invoke-static {v0, v1, v2}, Lcom/facebook/common/dextricks/DalvikInternals;->ioprio_set(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33983
    :cond_1
    return-void

    .line 33984
    :cond_2
    invoke-static {v4, v3}, Lcom/facebook/common/dextricks/DalvikInternals;->ioprio_get(II)I

    move-result v0

    goto :goto_0

    .line 33985
    :cond_3
    invoke-static {v2}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v0

    goto :goto_1

    .line 33986
    :catchall_0
    move-exception v0

    .line 33987
    invoke-virtual {p0}, LX/0Fk;->close()V

    throw v0
.end method


# virtual methods
.method public final close()V
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 33988
    iget v0, p0, LX/0Fk;->savedIoPriority:I

    if-eq v0, v3, :cond_0

    .line 33989
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, LX/0Fk;->savedIoPriority:I

    invoke-static {v0, v1, v2}, Lcom/facebook/common/dextricks/DalvikInternals;->ioprio_set(III)V

    .line 33990
    :cond_0
    iget v0, p0, LX/0Fk;->savedCpuPriority:I

    if-eq v0, v3, :cond_1

    .line 33991
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    .line 33992
    iget v1, p0, LX/0Fk;->savedCpuPriority:I

    invoke-static {v0, v1}, Landroid/os/Process;->setThreadPriority(II)V

    .line 33993
    :cond_1
    return-void
.end method
