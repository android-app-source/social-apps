.class public LX/0J2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39156
    const/4 v0, 0x1

    .line 39157
    sput-boolean v0, LX/0Bx;->a:Z

    .line 39158
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)Landroid/hardware/Camera;
    .locals 2

    .prologue
    .line 39160
    invoke-static {p0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    .line 39161
    invoke-static {}, LX/0Bx;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39162
    invoke-static {v0}, LX/0Bx;->a(Landroid/hardware/Camera;)V

    .line 39163
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/hardware/Camera;I)V
    .locals 1

    .prologue
    .line 39164
    invoke-virtual {p0}, Landroid/hardware/Camera;->release()V

    .line 39165
    invoke-static {}, LX/0Bx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39166
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    .line 39167
    invoke-static {v0}, LX/0Bx;->b(I)V

    .line 39168
    :cond_0
    return-void
.end method

.method public static b(Landroid/hardware/Camera;I)V
    .locals 1

    .prologue
    .line 39169
    invoke-virtual {p0}, Landroid/hardware/Camera;->startPreview()V

    .line 39170
    invoke-static {}, LX/0Bx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39171
    invoke-static {p0}, LX/0Bx;->c(Landroid/hardware/Camera;)V

    .line 39172
    :cond_0
    return-void
.end method

.method public static c(Landroid/hardware/Camera;I)V
    .locals 1

    .prologue
    .line 39173
    invoke-virtual {p0}, Landroid/hardware/Camera;->stopPreview()V

    .line 39174
    invoke-static {}, LX/0Bx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39175
    invoke-static {p0}, LX/0Bx;->d(Landroid/hardware/Camera;)V

    .line 39176
    :cond_0
    return-void
.end method
