.class public LX/0Df;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/0Df;

.field public static b:Z


# instance fields
.field public c:Landroid/widget/TextView;

.field public d:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/StringBuilder;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 30907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30908
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/0Df;->d:Ljava/util/LinkedList;

    .line 30909
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/0Df;->e:Ljava/lang/StringBuilder;

    .line 30910
    return-void
.end method

.method public static a()LX/0Df;
    .locals 1

    .prologue
    .line 30927
    sget-object v0, LX/0Df;->a:LX/0Df;

    if-nez v0, :cond_0

    .line 30928
    new-instance v0, LX/0Df;

    invoke-direct {v0}, LX/0Df;-><init>()V

    sput-object v0, LX/0Df;->a:LX/0Df;

    .line 30929
    :cond_0
    sget-object v0, LX/0Df;->a:LX/0Df;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 30911
    sget-boolean v0, LX/0Df;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Df;->c:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 30912
    :cond_0
    :goto_0
    return-void

    .line 30913
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 30914
    sget-boolean v2, LX/0Df;->b:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/0Df;->c:Landroid/widget/TextView;

    if-nez v2, :cond_3

    .line 30915
    :cond_2
    :goto_1
    goto :goto_0

    .line 30916
    :cond_3
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 30917
    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 30918
    iget-object p1, p0, LX/0Df;->d:Ljava/util/LinkedList;

    invoke-virtual {p1, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 30919
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 30920
    :cond_4
    iget-object v2, p0, LX/0Df;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    move v2, v1

    .line 30921
    :goto_3
    iget-object v1, p0, LX/0Df;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v2, v1, :cond_5

    .line 30922
    iget-object v3, p0, LX/0Df;->e:Ljava/lang/StringBuilder;

    iget-object v1, p0, LX/0Df;->d:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30923
    iget-object v1, p0, LX/0Df;->e:Ljava/lang/StringBuilder;

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30924
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 30925
    :cond_5
    iget-object v1, p0, LX/0Df;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 30926
    iget-object v1, p0, LX/0Df;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    new-instance v3, Lcom/facebook/browser/lite/logging/DebugOverlayController$1;

    invoke-direct {v3, p0, v2}, Lcom/facebook/browser/lite/logging/DebugOverlayController$1;-><init>(LX/0Df;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1
.end method
