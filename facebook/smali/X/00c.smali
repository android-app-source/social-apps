.class public LX/00c;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/00c;


# instance fields
.field private b:Landroid/content/SharedPreferences;

.field public volatile c:Ljava/lang/String;

.field public volatile d:Ljava/lang/String;

.field public volatile e:Ljava/lang/String;

.field private volatile f:Ljava/lang/String;

.field private volatile g:I

.field private volatile h:I

.field private volatile i:J

.field public volatile j:J

.field private volatile k:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2560
    new-instance v0, LX/00c;

    invoke-direct {v0}, LX/00c;-><init>()V

    sput-object v0, LX/00c;->a:LX/00c;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 2526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2527
    iput-object v0, p0, LX/00c;->c:Ljava/lang/String;

    .line 2528
    iput-object v0, p0, LX/00c;->d:Ljava/lang/String;

    .line 2529
    iput-object v0, p0, LX/00c;->e:Ljava/lang/String;

    .line 2530
    iput-object v0, p0, LX/00c;->f:Ljava/lang/String;

    .line 2531
    iput v1, p0, LX/00c;->g:I

    .line 2532
    iput v1, p0, LX/00c;->h:I

    .line 2533
    iput-wide v2, p0, LX/00c;->i:J

    .line 2534
    iput-wide v2, p0, LX/00c;->j:J

    .line 2535
    iput-wide v2, p0, LX/00c;->k:J

    .line 2536
    return-void
.end method

.method private declared-synchronized f()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2513
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/00c;->b:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 2514
    :goto_0
    monitor-exit p0

    return v0

    .line 2515
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    const-string v1, "ENCRYPT_CHANNEL_INFO/CHANNEL_ID"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/00c;->c:Ljava/lang/String;

    .line 2516
    iget-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    const-string v1, "ENCRYPT_CHANNEL_INFO/MAC_KEY"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/00c;->d:Ljava/lang/String;

    .line 2517
    iget-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    const-string v1, "ENCRYPT_CHANNEL_INFO/ENCRYPTION_KEY"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/00c;->e:Ljava/lang/String;

    .line 2518
    iget-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    const-string v1, "ENCRYPT_CHANNEL_INFO/USER_AGENT"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/00c;->f:Ljava/lang/String;

    .line 2519
    iget-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    const-string v1, "ENCRYPT_CHANNEL_INFO/UPDATED_TIME"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, LX/00c;->i:J

    .line 2520
    iget-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    const-string v1, "ENCRYPT_CHANNEL_INFO/EXPIRE_TIME"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, LX/00c;->j:J

    .line 2521
    iget-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    const-string v1, "ENCRYPT_CHANNEL_INFO/TTL_SINCE_ACTIVATE"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/00c;->g:I

    .line 2522
    iget-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    const-string v1, "ENCRYPT_CHANNEL_INFO/ALGORITHM_VERSION"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/00c;->h:I

    .line 2523
    iget-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    const-string v1, "ENCRYPT_CHANNEL_INFO/CREATION_TIME"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, LX/00c;->k:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2524
    const/4 v0, 0x1

    goto :goto_0

    .line 2525
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIJ)V
    .locals 4

    .prologue
    .line 2537
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/00c;->c:Ljava/lang/String;

    .line 2538
    iput-object p2, p0, LX/00c;->d:Ljava/lang/String;

    .line 2539
    iput-object p3, p0, LX/00c;->e:Ljava/lang/String;

    .line 2540
    iput-object p4, p0, LX/00c;->f:Ljava/lang/String;

    .line 2541
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/00c;->i:J

    .line 2542
    iget-wide v0, p0, LX/00c;->i:J

    int-to-long v2, p5

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/00c;->j:J

    .line 2543
    iput p6, p0, LX/00c;->g:I

    .line 2544
    iput p7, p0, LX/00c;->h:I

    .line 2545
    iput-wide p8, p0, LX/00c;->k:J

    .line 2546
    iget-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 2547
    iget-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2548
    const-string v1, "ENCRYPT_CHANNEL_INFO/CHANNEL_ID"

    iget-object v2, p0, LX/00c;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2549
    const-string v1, "ENCRYPT_CHANNEL_INFO/MAC_KEY"

    iget-object v2, p0, LX/00c;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2550
    const-string v1, "ENCRYPT_CHANNEL_INFO/ENCRYPTION_KEY"

    iget-object v2, p0, LX/00c;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2551
    const-string v1, "ENCRYPT_CHANNEL_INFO/USER_AGENT"

    iget-object v2, p0, LX/00c;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2552
    const-string v1, "ENCRYPT_CHANNEL_INFO/UPDATED_TIME"

    iget-wide v2, p0, LX/00c;->i:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 2553
    const-string v1, "ENCRYPT_CHANNEL_INFO/EXPIRE_TIME"

    iget-wide v2, p0, LX/00c;->j:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 2554
    const-string v1, "ENCRYPT_CHANNEL_INFO/TTL_SINCE_ACTIVATE"

    iget v2, p0, LX/00c;->g:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2555
    const-string v1, "ENCRYPT_CHANNEL_INFO/ALGORITHM_VERSION"

    iget v2, p0, LX/00c;->h:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2556
    const-string v1, "ENCRYPT_CHANNEL_INFO/CREATION_TIME"

    iget-wide v2, p0, LX/00c;->k:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 2557
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2558
    :cond_0
    monitor-exit p0

    return-void

    .line 2559
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 2509
    iget-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 2510
    const-string v0, "ENCRYPT_CHANNEL_INFO_STORAGE"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, LX/00c;->b:Landroid/content/SharedPreferences;

    .line 2511
    invoke-direct {p0}, LX/00c;->f()Z

    move-result v0

    .line 2512
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    .line 2506
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/00c;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/00c;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/00c;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2507
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, LX/00c;->j:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2508
    if-nez v0, :cond_0

    iget-object v0, p0, LX/00c;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/00c;->f:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
