.class public final enum LX/0G3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0G3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0G3;

.field public static final enum BORN:LX/0G3;

.field public static final enum KILLED:LX/0G3;

.field public static final enum RUNNING:LX/0G3;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34217
    new-instance v0, LX/0G3;

    const-string v1, "BORN"

    invoke-direct {v0, v1, v2}, LX/0G3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0G3;->BORN:LX/0G3;

    .line 34218
    new-instance v0, LX/0G3;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, LX/0G3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0G3;->RUNNING:LX/0G3;

    .line 34219
    new-instance v0, LX/0G3;

    const-string v1, "KILLED"

    invoke-direct {v0, v1, v4}, LX/0G3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0G3;->KILLED:LX/0G3;

    .line 34220
    const/4 v0, 0x3

    new-array v0, v0, [LX/0G3;

    sget-object v1, LX/0G3;->BORN:LX/0G3;

    aput-object v1, v0, v2

    sget-object v1, LX/0G3;->RUNNING:LX/0G3;

    aput-object v1, v0, v3

    sget-object v1, LX/0G3;->KILLED:LX/0G3;

    aput-object v1, v0, v4

    sput-object v0, LX/0G3;->$VALUES:[LX/0G3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34221
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0G3;
    .locals 1

    .prologue
    .line 34222
    const-class v0, LX/0G3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0G3;

    return-object v0
.end method

.method public static values()[LX/0G3;
    .locals 1

    .prologue
    .line 34223
    sget-object v0, LX/0G3;->$VALUES:[LX/0G3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0G3;

    return-object v0
.end method
