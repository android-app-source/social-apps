.class public final LX/0Dn;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public final synthetic a:LX/0Do;


# direct methods
.method public constructor <init>(LX/0Do;)V
    .locals 0

    .prologue
    .line 31088
    iput-object p1, p0, LX/0Dn;->a:LX/0Do;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 31080
    iget-object v0, p0, LX/0Dn;->a:LX/0Do;

    invoke-static {v0}, LX/0Do;->d(LX/0Do;)V

    .line 31081
    return-void
.end method

.method public final shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 31082
    iget-object v1, p0, LX/0Dn;->a:LX/0Do;

    iget-object v1, v1, LX/0Do;->e:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 31083
    :cond_0
    :goto_0
    return-object v0

    .line 31084
    :cond_1
    iget-object v1, p0, LX/0Dn;->a:LX/0Do;

    iget-object v1, v1, LX/0Do;->e:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 31085
    iget-object v0, p0, LX/0Dn;->a:LX/0Do;

    iget-object v0, v0, LX/0Do;->f:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    invoke-static {v0}, LX/0Dm;->b(Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    goto :goto_0

    .line 31086
    :cond_2
    invoke-static {p2}, LX/047;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0Dn;->a:LX/0Do;

    iget-object v1, v1, LX/0Do;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0x32

    if-ge v1, v2, :cond_0

    .line 31087
    iget-object v1, p0, LX/0Dn;->a:LX/0Do;

    iget-object v1, v1, LX/0Do;->g:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
