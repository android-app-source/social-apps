.class public LX/07h;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20154
    const-class v0, LX/07h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/07h;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20156
    const-string v0, ""

    iput-object v0, p0, LX/07h;->a:Ljava/lang/String;

    .line 20157
    const-string v0, ""

    iput-object v0, p0, LX/07h;->b:Ljava/lang/String;

    .line 20158
    const-string v0, ""

    iput-object v0, p0, LX/07h;->c:Ljava/lang/String;

    .line 20159
    const-string v0, ""

    iput-object v0, p0, LX/07h;->d:Ljava/lang/String;

    .line 20160
    const-string v0, ""

    iput-object v0, p0, LX/07h;->e:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/07h;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 20161
    new-instance v0, LX/07h;

    invoke-direct {v0}, LX/07h;-><init>()V

    .line 20162
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 20163
    :cond_0
    :goto_0
    return-object v0

    .line 20164
    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 20165
    const-string v2, "ck"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/07h;->a:Ljava/lang/String;

    .line 20166
    const-string v2, "cs"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/07h;->b:Ljava/lang/String;

    .line 20167
    const-string v2, "sr"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v0, LX/07h;->f:I

    .line 20168
    const-string v2, "di"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/07h;->c:Ljava/lang/String;

    .line 20169
    const-string v2, "ds"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/07h;->d:Ljava/lang/String;

    .line 20170
    const-string v2, "rc"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/07h;->e:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 20171
    :catch_0
    move-exception v0

    .line 20172
    sget-object v1, LX/07h;->g:Ljava/lang/String;

    const-string v2, "Failed to serialize ConnAckPayload"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20173
    new-instance v0, LX/07h;

    invoke-direct {v0}, LX/07h;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 20174
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 20175
    const-string v1, "ck"

    iget-object v2, p0, LX/07h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 20176
    const-string v1, "cs"

    iget-object v2, p0, LX/07h;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 20177
    const-string v1, "di"

    iget-object v2, p0, LX/07h;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 20178
    const-string v1, "ds"

    iget-object v2, p0, LX/07h;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 20179
    const-string v1, "sr"

    iget v2, p0, LX/07h;->f:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 20180
    const-string v1, "rc"

    iget-object v2, p0, LX/07h;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 20181
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20182
    :goto_0
    return-object v0

    .line 20183
    :catch_0
    move-exception v0

    .line 20184
    sget-object v1, LX/07h;->g:Ljava/lang/String;

    const-string v2, "failed to serialize"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20185
    const-string v0, ""

    goto :goto_0
.end method
