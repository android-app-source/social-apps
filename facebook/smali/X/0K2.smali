.class public LX/0K2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0G6;


# instance fields
.field public final a:LX/0OE;

.field private final b:Ljava/lang/String;

.field private final c:LX/1m0;


# direct methods
.method public constructor <init>(LX/0OE;Ljava/lang/String;LX/1m0;)V
    .locals 0

    .prologue
    .line 40106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40107
    iput-object p1, p0, LX/0K2;->a:LX/0OE;

    .line 40108
    iput-object p2, p0, LX/0K2;->b:Ljava/lang/String;

    .line 40109
    iput-object p3, p0, LX/0K2;->c:LX/1m0;

    .line 40110
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 1

    .prologue
    .line 40111
    iget-object v0, p0, LX/0K2;->a:LX/0OE;

    invoke-virtual {v0, p1, p2, p3}, LX/0OE;->a([BII)I

    move-result v0

    return v0
.end method

.method public a(LX/0OA;)J
    .locals 12

    .prologue
    .line 40112
    new-instance v1, LX/0OA;

    iget-object v2, p0, LX/0K2;->c:LX/1m0;

    iget-object v3, p1, LX/0OA;->a:Landroid/net/Uri;

    iget-object v0, p0, LX/0K2;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, LX/1m0;->a(Landroid/net/Uri;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p1, LX/0OA;->b:[B

    iget-wide v4, p1, LX/0OA;->c:J

    iget-wide v6, p1, LX/0OA;->d:J

    iget-wide v8, p1, LX/0OA;->e:J

    iget-object v10, p1, LX/0OA;->f:Ljava/lang/String;

    iget v11, p1, LX/0OA;->g:I

    invoke-direct/range {v1 .. v11}, LX/0OA;-><init>(Landroid/net/Uri;[BJJJLjava/lang/String;I)V

    .line 40113
    iget-object v0, p0, LX/0K2;->a:LX/0OE;

    invoke-virtual {v0, v1}, LX/0OE;->a(LX/0OA;)J

    move-result-wide v0

    return-wide v0

    .line 40114
    :cond_0
    iget-object v0, p0, LX/0K2;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 40115
    iget-object v0, p0, LX/0K2;->a:LX/0OE;

    invoke-virtual {v0}, LX/0OE;->a()V

    .line 40116
    return-void
.end method
