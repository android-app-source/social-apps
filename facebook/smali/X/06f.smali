.class public final enum LX/06f;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/06f;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/06f;

.field public static final enum APP_FOREGROUND:LX/06f;

.field public static final enum AUTH_CREDENTIALS_CHANGE:LX/06f;

.field public static final enum CLIENT_KICK:LX/06f;

.field public static final enum CONFIG_CHANGED:LX/06f;

.field public static final enum CONNECTION_LOST:LX/06f;

.field public static final enum CONNECTIVITY_CHANGED:LX/06f;

.field public static final enum CONNECT_NOW:LX/06f;

.field public static final enum CREDENTIALS_UPDATED:LX/06f;

.field public static final enum EXPIRE_CONNECTION:LX/06f;

.field public static final enum FBNS_REGISTER:LX/06f;

.field public static final enum FBNS_REGISTER_RETRY:LX/06f;

.field public static final enum FBNS_UNREGISTER:LX/06f;

.field public static final enum KEEPALIVE:LX/06f;

.field public static final enum PERSISTENT_KICK:LX/06f;

.field public static final enum PERSISTENT_KICK_SCREEN_CHANGE:LX/06f;

.field public static final enum SERVICE_RESTART:LX/06f;

.field public static final enum SERVICE_START:LX/06f;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18066
    new-instance v0, LX/06f;

    const-string v1, "SERVICE_START"

    invoke-direct {v0, v1, v3}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->SERVICE_START:LX/06f;

    .line 18067
    new-instance v0, LX/06f;

    const-string v1, "SERVICE_RESTART"

    invoke-direct {v0, v1, v4}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->SERVICE_RESTART:LX/06f;

    .line 18068
    new-instance v0, LX/06f;

    const-string v1, "PERSISTENT_KICK"

    invoke-direct {v0, v1, v5}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->PERSISTENT_KICK:LX/06f;

    .line 18069
    new-instance v0, LX/06f;

    const-string v1, "CONNECTIVITY_CHANGED"

    invoke-direct {v0, v1, v6}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->CONNECTIVITY_CHANGED:LX/06f;

    .line 18070
    new-instance v0, LX/06f;

    const-string v1, "CONFIG_CHANGED"

    invoke-direct {v0, v1, v7}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->CONFIG_CHANGED:LX/06f;

    .line 18071
    new-instance v0, LX/06f;

    const-string v1, "EXPIRE_CONNECTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->EXPIRE_CONNECTION:LX/06f;

    .line 18072
    new-instance v0, LX/06f;

    const-string v1, "CONNECT_NOW"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->CONNECT_NOW:LX/06f;

    .line 18073
    new-instance v0, LX/06f;

    const-string v1, "CONNECTION_LOST"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->CONNECTION_LOST:LX/06f;

    .line 18074
    new-instance v0, LX/06f;

    const-string v1, "KEEPALIVE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->KEEPALIVE:LX/06f;

    .line 18075
    new-instance v0, LX/06f;

    const-string v1, "APP_FOREGROUND"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->APP_FOREGROUND:LX/06f;

    .line 18076
    new-instance v0, LX/06f;

    const-string v1, "FBNS_REGISTER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->FBNS_REGISTER:LX/06f;

    .line 18077
    new-instance v0, LX/06f;

    const-string v1, "FBNS_REGISTER_RETRY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->FBNS_REGISTER_RETRY:LX/06f;

    .line 18078
    new-instance v0, LX/06f;

    const-string v1, "FBNS_UNREGISTER"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->FBNS_UNREGISTER:LX/06f;

    .line 18079
    new-instance v0, LX/06f;

    const-string v1, "CREDENTIALS_UPDATED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->CREDENTIALS_UPDATED:LX/06f;

    .line 18080
    new-instance v0, LX/06f;

    const-string v1, "PERSISTENT_KICK_SCREEN_CHANGE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->PERSISTENT_KICK_SCREEN_CHANGE:LX/06f;

    .line 18081
    new-instance v0, LX/06f;

    const-string v1, "CLIENT_KICK"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->CLIENT_KICK:LX/06f;

    .line 18082
    new-instance v0, LX/06f;

    const-string v1, "AUTH_CREDENTIALS_CHANGE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/06f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/06f;->AUTH_CREDENTIALS_CHANGE:LX/06f;

    .line 18083
    const/16 v0, 0x11

    new-array v0, v0, [LX/06f;

    sget-object v1, LX/06f;->SERVICE_START:LX/06f;

    aput-object v1, v0, v3

    sget-object v1, LX/06f;->SERVICE_RESTART:LX/06f;

    aput-object v1, v0, v4

    sget-object v1, LX/06f;->PERSISTENT_KICK:LX/06f;

    aput-object v1, v0, v5

    sget-object v1, LX/06f;->CONNECTIVITY_CHANGED:LX/06f;

    aput-object v1, v0, v6

    sget-object v1, LX/06f;->CONFIG_CHANGED:LX/06f;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/06f;->EXPIRE_CONNECTION:LX/06f;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/06f;->CONNECT_NOW:LX/06f;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/06f;->CONNECTION_LOST:LX/06f;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/06f;->KEEPALIVE:LX/06f;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/06f;->APP_FOREGROUND:LX/06f;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/06f;->FBNS_REGISTER:LX/06f;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/06f;->FBNS_REGISTER_RETRY:LX/06f;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/06f;->FBNS_UNREGISTER:LX/06f;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/06f;->CREDENTIALS_UPDATED:LX/06f;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/06f;->PERSISTENT_KICK_SCREEN_CHANGE:LX/06f;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/06f;->CLIENT_KICK:LX/06f;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/06f;->AUTH_CREDENTIALS_CHANGE:LX/06f;

    aput-object v2, v0, v1

    sput-object v0, LX/06f;->$VALUES:[LX/06f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18084
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/06f;
    .locals 1

    .prologue
    .line 18065
    const-class v0, LX/06f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/06f;

    return-object v0
.end method

.method public static values()[LX/06f;
    .locals 1

    .prologue
    .line 18064
    sget-object v0, LX/06f;->$VALUES:[LX/06f;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/06f;

    return-object v0
.end method
