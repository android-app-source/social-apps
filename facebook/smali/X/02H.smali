.class public LX/02H;
.super Landroid/view/SurfaceView;
.source ""

# interfaces
.implements Landroid/view/SurfaceHolder$Callback2;


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Ljava/lang/Thread;

.field private c:Landroid/view/Choreographer$FrameCallback;

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/view/SurfaceHolder;

.field private h:I

.field private i:I

.field private j:Z

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 7273
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 7274
    const/4 v0, 0x0

    iput v0, p0, LX/02H;->d:I

    .line 7275
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/02H;->setZOrderOnTop(Z)V

    .line 7276
    invoke-virtual {p0}, LX/02H;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 7277
    const/4 v1, -0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 7278
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 7279
    return-void
.end method

.method private declared-synchronized a(I)V
    .locals 3

    .prologue
    .line 7266
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/02H;->d:I

    if-eq v0, p1, :cond_0

    .line 7267
    iget-object v0, p0, LX/02H;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 7268
    :goto_0
    iget v0, p0, LX/02H;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, p1, :cond_0

    .line 7269
    const v0, 0x70d7f7e9

    :try_start_1
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 7270
    :catch_0
    :try_start_2
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 7271
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 7272
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public static declared-synchronized a$redex0(LX/02H;Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 7249
    monitor-enter p0

    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 7250
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7251
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 7252
    :pswitch_0
    :try_start_1
    iget v0, p0, LX/02H;->d:I

    if-ne v0, v1, :cond_1

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_1

    .line 7253
    invoke-virtual {p0}, LX/02H;->d()V

    .line 7254
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, LX/02H;->d:I

    .line 7255
    :cond_0
    :goto_0
    const v0, -0x4dba06df

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7256
    :goto_1
    monitor-exit p0

    return-void

    .line 7257
    :cond_1
    :try_start_2
    iget v0, p0, LX/02H;->d:I

    if-nez v0, :cond_2

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v1, :cond_2

    .line 7258
    iget-object v0, p0, LX/02H;->g:Landroid/view/SurfaceHolder;

    iget v1, p0, LX/02H;->h:I

    iget v2, p0, LX/02H;->i:I

    invoke-virtual {p0, v0, v1, v2}, LX/02H;->a(Landroid/view/SurfaceHolder;II)V

    .line 7259
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, LX/02H;->d:I

    .line 7260
    invoke-virtual {p0}, LX/02H;->f()V

    goto :goto_0

    .line 7261
    :cond_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eq v0, v1, :cond_0

    .line 7262
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "unrecognized state"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 7263
    :pswitch_1
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto :goto_1

    .line 7264
    :pswitch_2
    iget-object v0, p0, LX/02H;->g:Landroid/view/SurfaceHolder;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, LX/02H;->b(Landroid/view/SurfaceHolder;J)V

    goto :goto_1

    .line 7265
    :pswitch_3
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/02H;->a(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(Landroid/view/SurfaceHolder;J)V
    .locals 2

    .prologue
    .line 7243
    invoke-virtual {p0}, LX/02H;->e()V

    .line 7244
    invoke-virtual {p0, p1, p2, p3}, LX/02H;->a(Landroid/view/SurfaceHolder;J)V

    .line 7245
    iget v0, p0, LX/02H;->f:I

    if-lez v0, :cond_0

    .line 7246
    iget v0, p0, LX/02H;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/02H;->e:I

    .line 7247
    const v0, 0x347a1572

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 7248
    :cond_0
    return-void
.end method

.method private declared-synchronized g()V
    .locals 2

    .prologue
    .line 7234
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/02H;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/02H;->f:I

    .line 7235
    iget v0, p0, LX/02H;->e:I

    .line 7236
    :goto_0
    iget v1, p0, LX/02H;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 7237
    const v1, -0x7509edf2

    :try_start_1
    invoke-static {p0, v1}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 7238
    :catch_0
    move-exception v0

    .line 7239
    :try_start_2
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 7240
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 7241
    :cond_0
    :try_start_3
    iget v0, p0, LX/02H;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/02H;->f:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 7242
    monitor-exit p0

    return-void
.end method

.method public static h(LX/02H;)V
    .locals 2

    .prologue
    .line 7218
    monitor-enter p0

    .line 7219
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 7220
    new-instance v0, LX/02M;

    invoke-direct {v0, p0}, LX/02M;-><init>(LX/02H;)V

    iput-object v0, p0, LX/02H;->a:Landroid/os/Handler;

    .line 7221
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 7222
    invoke-static {p0}, LX/02N;->a(LX/02H;)Landroid/view/Choreographer$FrameCallback;

    move-result-object v0

    iput-object v0, p0, LX/02H;->c:Landroid/view/Choreographer$FrameCallback;

    .line 7223
    :cond_0
    invoke-virtual {p0}, LX/02H;->b()V

    .line 7224
    const v0, 0x162b8763

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 7225
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7226
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 7227
    monitor-enter p0

    .line 7228
    :try_start_1
    invoke-virtual {p0}, LX/02H;->c()V

    .line 7229
    const/4 v0, 0x0

    iput-object v0, p0, LX/02H;->a:Landroid/os/Handler;

    .line 7230
    const/4 v0, 0x0

    iput-object v0, p0, LX/02H;->c:Landroid/view/Choreographer$FrameCallback;

    .line 7231
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 7232
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 7233
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7208
    iget-object v0, p0, LX/02H;->b:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    .line 7209
    invoke-direct {p0, v1}, LX/02H;->a(I)V

    .line 7210
    iget-object v0, p0, LX/02H;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 7211
    :try_start_0
    iget-object v0, p0, LX/02H;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7212
    iget-object v0, p0, LX/02H;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 7213
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 7214
    :catch_0
    move-exception v0

    .line 7215
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 7216
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/02H;->b:Ljava/lang/Thread;

    .line 7217
    :cond_1
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 7203
    invoke-virtual {p0}, LX/02H;->e()V

    .line 7204
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/02H;->j:Z

    .line 7205
    iget v0, p0, LX/02H;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 7206
    iget-object v0, p0, LX/02H;->g:Landroid/view/SurfaceHolder;

    invoke-direct {p0, v0, p1, p2}, LX/02H;->b(Landroid/view/SurfaceHolder;J)V

    .line 7207
    :cond_0
    return-void
.end method

.method public a(Landroid/view/SurfaceHolder;II)V
    .locals 0

    .prologue
    .line 7201
    invoke-virtual {p0}, LX/02H;->e()V

    .line 7202
    return-void
.end method

.method public a(Landroid/view/SurfaceHolder;J)V
    .locals 0

    .prologue
    .line 7200
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 7198
    invoke-virtual {p0}, LX/02H;->e()V

    .line 7199
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 7141
    invoke-virtual {p0}, LX/02H;->e()V

    .line 7142
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 7191
    invoke-virtual {p0}, LX/02H;->e()V

    .line 7192
    iget-boolean v0, p0, LX/02H;->j:Z

    if-eqz v0, :cond_0

    .line 7193
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 7194
    iget-object v0, p0, LX/02H;->c:Landroid/view/Choreographer$FrameCallback;

    invoke-static {v0}, LX/02N;->b(Landroid/view/Choreographer$FrameCallback;)V

    .line 7195
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/02H;->j:Z

    .line 7196
    :cond_0
    return-void

    .line 7197
    :cond_1
    iget-object v0, p0, LX/02H;->a:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 7188
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, LX/02H;->b:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 7189
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "method called on wrong thread"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 7190
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 7181
    invoke-virtual {p0}, LX/02H;->e()V

    .line 7182
    iget v0, p0, LX/02H;->d:I

    if-ne v0, v4, :cond_0

    iget-boolean v0, p0, LX/02H;->j:Z

    if-nez v0, :cond_0

    .line 7183
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 7184
    iget-object v0, p0, LX/02H;->c:Landroid/view/Choreographer$FrameCallback;

    invoke-static {v0}, LX/02N;->a(Landroid/view/Choreographer$FrameCallback;)V

    .line 7185
    :goto_0
    iput-boolean v4, p0, LX/02H;->j:Z

    .line 7186
    :cond_0
    return-void

    .line 7187
    :cond_1
    iget-object v0, p0, LX/02H;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/02H;->a:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, LX/02H;->k:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public final getState()I
    .locals 1

    .prologue
    .line 7180
    iget v0, p0, LX/02H;->d:I

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/high16 v1, 0x42700000    # 60.0f

    const/high16 v2, 0x41700000    # 15.0f

    const/4 v0, 0x2

    const/16 v3, 0x2c

    const v4, -0x344beaf7    # -2.360373E7f

    invoke-static {v0, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 7171
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-ge v0, v4, :cond_1

    iget v0, p0, LX/02H;->k:I

    if-nez v0, :cond_1

    .line 7172
    invoke-virtual {p0}, LX/02H;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 7173
    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRefreshRate()F

    move-result v0

    .line 7174
    cmpl-float v4, v0, v1

    if-ltz v4, :cond_2

    move v0, v1

    .line 7175
    :cond_0
    :goto_0
    const/high16 v1, 0x447a0000    # 1000.0f

    div-float v0, v1, v0

    float-to-int v0, v0

    iput v0, p0, LX/02H;->k:I

    .line 7176
    :cond_1
    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    .line 7177
    const v0, -0x3588c259

    invoke-static {v0, v3}, LX/02F;->g(II)V

    return-void

    .line 7178
    :cond_2
    cmpg-float v1, v0, v2

    if-gtz v1, :cond_0

    move v0, v2

    .line 7179
    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4e90a3d7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 7168
    invoke-virtual {p0}, LX/02H;->a()V

    .line 7169
    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    .line 7170
    const/16 v1, 0x2d

    const v2, 0x6146c535

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final declared-synchronized surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1

    .prologue
    .line 7161
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, LX/02H;->a(I)V

    .line 7162
    iput-object p1, p0, LX/02H;->g:Landroid/view/SurfaceHolder;

    .line 7163
    iput p3, p0, LX/02H;->h:I

    .line 7164
    iput p4, p0, LX/02H;->i:I

    .line 7165
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/02H;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7166
    monitor-exit p0

    return-void

    .line 7167
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 7153
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/02H;->b:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 7154
    new-instance v0, Lcom/facebook/common/asyncview/AsyncView$1;

    const-string v1, "AsyncView"

    invoke-direct {v0, p0, v1}, Lcom/facebook/common/asyncview/AsyncView$1;-><init>(LX/02H;Ljava/lang/String;)V

    iput-object v0, p0, LX/02H;->b:Ljava/lang/Thread;

    .line 7155
    iget-object v0, p0, LX/02H;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 7156
    :goto_0
    iget-object v0, p0, LX/02H;->a:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 7157
    const v0, 0x774b58c4

    :try_start_1
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 7158
    :catch_0
    :try_start_2
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 7159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 7160
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 7147
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, LX/02H;->a(I)V

    .line 7148
    const/4 v0, 0x0

    iput-object v0, p0, LX/02H;->g:Landroid/view/SurfaceHolder;

    .line 7149
    const/4 v0, 0x0

    iput v0, p0, LX/02H;->h:I

    .line 7150
    const/4 v0, 0x0

    iput v0, p0, LX/02H;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7151
    monitor-exit p0

    return-void

    .line 7152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized surfaceRedrawNeeded(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 7143
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/02H;->a:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 7144
    invoke-direct {p0}, LX/02H;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7145
    monitor-exit p0

    return-void

    .line 7146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
