.class public LX/0G4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-java.util.ArrayList._Constructor",
        "BadMethodUse-java.lang.Thread.start",
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:Ljava/lang/Process;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0G3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34224
    const-class v0, LX/0G4;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0G4;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34226
    iput-object p1, p0, LX/0G4;->c:Ljava/util/List;

    .line 34227
    sget-object v0, LX/0G3;->BORN:LX/0G3;

    iput-object v0, p0, LX/0G4;->d:LX/0G3;

    .line 34228
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 34229
    monitor-enter p0

    .line 34230
    :try_start_0
    iget-object v0, p0, LX/0G4;->d:LX/0G3;

    sget-object v1, LX/0G3;->BORN:LX/0G3;

    if-eq v0, v1, :cond_0

    .line 34231
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot start logcat process twice"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34232
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 34233
    :cond_0
    :try_start_1
    sget-object v0, LX/0G3;->RUNNING:LX/0G3;

    iput-object v0, p0, LX/0G4;->d:LX/0G3;

    .line 34234
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 34235
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 34236
    const-string v1, "logcat"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34237
    iget-object v1, p0, LX/0G4;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 34238
    :try_start_2
    new-instance v1, Ljava/lang/ProcessBuilder;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/ProcessBuilder;->command(Ljava/util/List;)Ljava/lang/ProcessBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;

    move-result-object v0

    iput-object v0, p0, LX/0G4;->b:Ljava/lang/Process;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 34239
    new-instance v0, Lcom/facebook/debug/logcat/raw/LogcatProcess$1;

    invoke-direct {v0, p0}, Lcom/facebook/debug/logcat/raw/LogcatProcess$1;-><init>(LX/0G4;)V

    const v1, -0x4926c9b9

    invoke-static {v0, v1}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 34240
    return-void

    .line 34241
    :catch_0
    move-exception v0

    .line 34242
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "unable to start logcat process"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 34243
    monitor-enter p0

    .line 34244
    :try_start_0
    iget-object v0, p0, LX/0G4;->d:LX/0G3;

    sget-object v1, LX/0G3;->RUNNING:LX/0G3;

    if-eq v0, v1, :cond_0

    .line 34245
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot stop non-running logcat process"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34246
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 34247
    :cond_0
    :try_start_1
    sget-object v0, LX/0G3;->KILLED:LX/0G3;

    iput-object v0, p0, LX/0G4;->d:LX/0G3;

    .line 34248
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 34249
    iget-object v0, p0, LX/0G4;->b:Ljava/lang/Process;

    invoke-virtual {v0}, Ljava/lang/Process;->destroy()V

    .line 34250
    return-void
.end method

.method public final finalize()V
    .locals 2

    .prologue
    .line 34251
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 34252
    :try_start_0
    iget-object v0, p0, LX/0G4;->d:LX/0G3;

    sget-object v1, LX/0G3;->RUNNING:LX/0G3;

    if-ne v0, v1, :cond_0

    .line 34253
    invoke-virtual {p0}, LX/0G4;->c()V

    .line 34254
    sget-object v0, LX/0G4;->a:Ljava/lang/String;

    const-string v1, "child process still alive when finalize() called"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34255
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
