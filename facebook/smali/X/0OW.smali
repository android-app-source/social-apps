.class public final LX/0OW;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[I

.field public static final b:[I

.field public static final c:[I

.field public static final d:[I

.field public static final e:[I

.field public static final f:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x13

    const/4 v1, 0x3

    .line 53318
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/0OW;->a:[I

    .line 53319
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, LX/0OW;->b:[I

    .line 53320
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, LX/0OW;->c:[I

    .line 53321
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, LX/0OW;->d:[I

    .line 53322
    new-array v0, v2, [I

    fill-array-data v0, :array_4

    sput-object v0, LX/0OW;->e:[I

    .line 53323
    new-array v0, v2, [I

    fill-array-data v0, :array_5

    sput-object v0, LX/0OW;->f:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x6
    .end array-data

    .line 53324
    :array_1
    .array-data 4
        0xbb80
        0xac44
        0x7d00
    .end array-data

    .line 53325
    :array_2
    .array-data 4
        0x5dc0
        0x5622
        0x3e80
    .end array-data

    .line 53326
    :array_3
    .array-data 4
        0x2
        0x1
        0x2
        0x3
        0x3
        0x4
        0x4
        0x5
    .end array-data

    .line 53327
    :array_4
    .array-data 4
        0x20
        0x28
        0x30
        0x38
        0x40
        0x50
        0x60
        0x70
        0x80
        0xa0
        0xc0
        0xe0
        0x100
        0x140
        0x180
        0x1c0
        0x200
        0x240
        0x280
    .end array-data

    .line 53328
    :array_5
    .array-data 4
        0x45
        0x57
        0x68
        0x79
        0x8b
        0xae
        0xd0
        0xf3
        0x116
        0x15c
        0x1a1
        0x1e7
        0x22d
        0x2b8
        0x343
        0x3cf
        0x45a
        0x4e5
        0x571
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 53329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Oj;Ljava/lang/String;JLjava/lang/String;)LX/0L4;
    .locals 10

    .prologue
    const/4 v2, -0x1

    .line 53330
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    and-int/lit16 v0, v0, 0xc0

    shr-int/lit8 v0, v0, 0x6

    .line 53331
    sget-object v1, LX/0OW;->b:[I

    aget v7, v1, v0

    .line 53332
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    .line 53333
    sget-object v1, LX/0OW;->d:[I

    and-int/lit8 v3, v0, 0x38

    shr-int/lit8 v3, v3, 0x3

    aget v6, v1, v3

    .line 53334
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 53335
    add-int/lit8 v6, v6, 0x1

    .line 53336
    :cond_0
    const-string v1, "audio/ac3"

    const/4 v8, 0x0

    move-object v0, p1

    move v3, v2

    move-wide v4, p2

    move-object v9, p4

    invoke-static/range {v0 .. v9}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0Oj;Ljava/lang/String;JLjava/lang/String;)LX/0L4;
    .locals 10

    .prologue
    const/4 v2, -0x1

    .line 53337
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/0Oj;->c(I)V

    .line 53338
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    and-int/lit16 v0, v0, 0xc0

    shr-int/lit8 v0, v0, 0x6

    .line 53339
    sget-object v1, LX/0OW;->b:[I

    aget v7, v1, v0

    .line 53340
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    .line 53341
    sget-object v1, LX/0OW;->d:[I

    and-int/lit8 v3, v0, 0xe

    shr-int/lit8 v3, v3, 0x1

    aget v6, v1, v3

    .line 53342
    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 53343
    add-int/lit8 v6, v6, 0x1

    .line 53344
    :cond_0
    const-string v1, "audio/eac3"

    const/4 v8, 0x0

    move-object v0, p1

    move v3, v2

    move-wide v4, p2

    move-object v9, p4

    invoke-static/range {v0 .. v9}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v0

    return-object v0
.end method
