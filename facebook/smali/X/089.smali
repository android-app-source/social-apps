.class public abstract LX/089;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Closeable;",
        "Ljava/util/Iterator",
        "<",
        "LX/08A;",
        ">;"
    }
.end annotation


# instance fields
.field private mDexPos:I

.field private final mDexes:[LX/02Z;


# direct methods
.method public constructor <init>(LX/02Y;)V
    .locals 1

    .prologue
    .line 20818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20819
    iget-object v0, p1, LX/02Y;->dexes:[LX/02Z;

    iput-object v0, p0, LX/089;->mDexes:[LX/02Z;

    .line 20820
    const/4 v0, 0x0

    iput v0, p0, LX/089;->mDexPos:I

    .line 20821
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 20822
    return-void
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 20823
    iget v0, p0, LX/089;->mDexPos:I

    iget-object v1, p0, LX/089;->mDexes:[LX/02Z;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()LX/08A;
    .locals 3

    .prologue
    .line 20824
    :try_start_0
    iget-object v0, p0, LX/089;->mDexes:[LX/02Z;

    iget v1, p0, LX/089;->mDexPos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/089;->mDexPos:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, LX/089;->nextImpl(LX/02Z;)LX/08A;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 20825
    :catch_0
    move-exception v0

    .line 20826
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20827
    invoke-virtual {p0}, LX/089;->next()LX/08A;

    move-result-object v0

    return-object v0
.end method

.method public abstract nextImpl(LX/02Z;)LX/08A;
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 20828
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
