.class public LX/01I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:LX/01I;


# instance fields
.field private final c:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

.field private final d:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4149
    const-class v0, LX/01I;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/01I;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/facebook/analytics/appstatelogger/AppStateLogger;)V
    .locals 1

    .prologue
    .line 4136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4137
    iput-object p1, p0, LX/01I;->c:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    .line 4138
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, LX/01I;->d:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 4139
    invoke-static {p0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 4140
    return-void
.end method

.method public static a(Lcom/facebook/analytics/appstatelogger/AppStateLogger;)V
    .locals 2

    .prologue
    .line 4145
    sget-object v0, LX/01I;->b:LX/01I;

    if-eqz v0, :cond_0

    .line 4146
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "AppStateLoggerExceptionHandler can only be initialized once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4147
    :cond_0
    new-instance v0, LX/01I;

    invoke-direct {v0, p0}, LX/01I;-><init>(Lcom/facebook/analytics/appstatelogger/AppStateLogger;)V

    sput-object v0, LX/01I;->b:LX/01I;

    .line 4148
    return-void
.end method


# virtual methods
.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 4141
    sget-object v0, LX/01I;->a:Ljava/lang/String;

    const-string v1, "Handing uncaught exception"

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4142
    iget-object v0, p0, LX/01I;->c:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    invoke-virtual {v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a()V

    .line 4143
    iget-object v0, p0, LX/01I;->d:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 4144
    return-void
.end method
