.class public LX/0I6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/06n;


# instance fields
.field public final a:I

.field private final b:I

.field private final c:I

.field private final d:Ljava/util/Random;

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>(III)V
    .locals 1

    .prologue
    .line 38366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38367
    iput p1, p0, LX/0I6;->a:I

    .line 38368
    iput p2, p0, LX/0I6;->b:I

    .line 38369
    iput p3, p0, LX/0I6;->c:I

    .line 38370
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/0I6;->d:Ljava/util/Random;

    .line 38371
    const/4 v0, 0x0

    iput v0, p0, LX/0I6;->e:I

    .line 38372
    iget v0, p0, LX/0I6;->a:I

    iput v0, p0, LX/0I6;->f:I

    .line 38373
    return-void
.end method


# virtual methods
.method public final a(Z)I
    .locals 6

    .prologue
    .line 38374
    iget v0, p0, LX/0I6;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0I6;->e:I

    .line 38375
    iget v0, p0, LX/0I6;->f:I

    .line 38376
    if-nez p1, :cond_0

    iget v1, p0, LX/0I6;->b:I

    if-ge v0, v1, :cond_0

    .line 38377
    iget v0, p0, LX/0I6;->b:I

    .line 38378
    :cond_0
    mul-int/lit8 v0, v0, 0x2

    iget v1, p0, LX/0I6;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 38379
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    iget-object v1, p0, LX/0I6;->d:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v1

    float-to-double v4, v1

    add-double/2addr v2, v4

    int-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 38380
    iput v0, p0, LX/0I6;->f:I

    .line 38381
    iget v0, p0, LX/0I6;->f:I

    return v0
.end method

.method public final a()LX/06l;
    .locals 1

    .prologue
    .line 38382
    sget-object v0, LX/06l;->BACK_OFF:LX/06l;

    return-object v0
.end method

.method public final b(Z)Z
    .locals 2

    .prologue
    .line 38383
    iget v0, p0, LX/0I6;->e:I

    const v1, 0x7fffffff

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 38384
    const-string v0, "BackoffRetryStrategy: attempt:%d/Infinite, delay:%d seconds"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, LX/0I6;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, LX/0I6;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/05V;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
