.class public final LX/02f;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final coldstartSetSize:I

.field public configFlags:I

.field public final dexExperiment:LX/02d;

.field public final enableExecProtForOat:Z

.field public final enableOatRandomAccessMode:Z

.field public final enableOatSequentialAccessMode:Z

.field public final mDexFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldalvik/system/DexFile;",
            ">;"
        }
    .end annotation
.end field

.field public storeLocators:[I
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(IZZZILX/02d;)V
    .locals 1

    .prologue
    .line 9083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9084
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/02f;->mDexFiles:Ljava/util/ArrayList;

    .line 9085
    iput p1, p0, LX/02f;->configFlags:I

    .line 9086
    iput-boolean p2, p0, LX/02f;->enableOatSequentialAccessMode:Z

    .line 9087
    iput-boolean p3, p0, LX/02f;->enableOatRandomAccessMode:Z

    .line 9088
    iput-boolean p4, p0, LX/02f;->enableExecProtForOat:Z

    .line 9089
    iput p5, p0, LX/02f;->coldstartSetSize:I

    .line 9090
    iput-object p6, p0, LX/02f;->dexExperiment:LX/02d;

    .line 9091
    return-void
.end method


# virtual methods
.method public final addDex(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 9092
    iget-object v0, p0, LX/02f;->mDexFiles:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Ldalvik/system/DexFile;->loadDex(Ljava/lang/String;Ljava/lang/String;I)Ldalvik/system/DexFile;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 9093
    return-void
.end method

.method public final addDex(Ljava/io/File;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 9094
    iget-object v0, p0, LX/02f;->mDexFiles:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Ldalvik/system/DexFile;->loadDex(Ljava/lang/String;Ljava/lang/String;I)Ldalvik/system/DexFile;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 9095
    return-void
.end method

.method public final getNumberConfiguredDexFiles()I
    .locals 1

    .prologue
    .line 9096
    iget-object v0, p0, LX/02f;->mDexFiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
