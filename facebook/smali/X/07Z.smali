.class public LX/07Z;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/07R;)I
    .locals 2

    .prologue
    .line 20010
    iget-object v0, p0, LX/07R;->a:LX/07S;

    iget v0, v0, LX/07S;->mValue:I

    shl-int/lit8 v0, v0, 0x4

    or-int/lit8 v0, v0, 0x0

    .line 20011
    iget-boolean v1, p0, LX/07R;->b:Z

    if-eqz v1, :cond_0

    .line 20012
    or-int/lit8 v0, v0, 0x8

    .line 20013
    :cond_0
    iget v1, p0, LX/07R;->c:I

    shl-int/lit8 v1, v1, 0x1

    or-int/2addr v0, v1

    .line 20014
    iget-boolean v1, p0, LX/07R;->e:Z

    move v1, v1

    .line 20015
    if-eqz v1, :cond_1

    .line 20016
    or-int/lit8 v0, v0, 0x1

    .line 20017
    :cond_1
    return v0
.end method

.method public static a(LX/07U;)I
    .locals 2

    .prologue
    .line 20018
    const/4 v0, 0x0

    .line 20019
    iget-boolean v1, p0, LX/07U;->b:Z

    if-eqz v1, :cond_0

    .line 20020
    const/16 v0, 0x80

    .line 20021
    :cond_0
    iget-boolean v1, p0, LX/07U;->c:Z

    if-eqz v1, :cond_1

    .line 20022
    or-int/lit8 v0, v0, 0x40

    .line 20023
    :cond_1
    iget-boolean v1, p0, LX/07U;->e:Z

    if-eqz v1, :cond_2

    .line 20024
    or-int/lit8 v0, v0, 0x20

    .line 20025
    :cond_2
    iget v1, p0, LX/07U;->f:I

    and-int/lit8 v1, v1, 0x3

    shl-int/lit8 v1, v1, 0x3

    or-int/2addr v0, v1

    .line 20026
    iget-boolean v1, p0, LX/07U;->d:Z

    if-eqz v1, :cond_3

    .line 20027
    or-int/lit8 v0, v0, 0x4

    .line 20028
    :cond_3
    iget-boolean v1, p0, LX/07U;->g:Z

    if-eqz v1, :cond_4

    .line 20029
    or-int/lit8 v0, v0, 0x2

    .line 20030
    :cond_4
    return v0
.end method

.method public static a(Ljava/io/DataOutputStream;I)I
    .locals 2

    .prologue
    .line 20031
    const/4 v0, 0x0

    .line 20032
    :cond_0
    rem-int/lit16 v1, p1, 0x80

    .line 20033
    div-int/lit16 p1, p1, 0x80

    .line 20034
    if-lez p1, :cond_1

    .line 20035
    or-int/lit16 v1, v1, 0x80

    .line 20036
    :cond_1
    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 20037
    add-int/lit8 v0, v0, 0x1

    .line 20038
    if-gtz p1, :cond_0

    .line 20039
    return v0
.end method

.method public static a(Ljava/lang/String;)[B
    .locals 2

    .prologue
    .line 20040
    :try_start_0
    const-string v0, "utf-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 20041
    :catch_0
    move-exception v0

    .line 20042
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
