.class public final enum LX/01T;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/01T;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/01T;

.field public static final enum ALOHA:LX/01T;

.field public static final enum APPMANAGER:LX/01T;

.field public static final enum FACEBOOK_SERVICES:LX/01T;

.field public static final enum FB4A:LX/01T;

.field public static final enum FBCAMERA:LX/01T;

.field public static final enum GAMESPORTAL:LX/01T;

.field public static final enum GETAPPS:LX/01T;

.field public static final enum GIFTS:LX/01T;

.field public static final enum GROUPS:LX/01T;

.field public static final enum HOME:LX/01T;

.field public static final enum ME:LX/01T;

.field public static final enum MESSENGER:LX/01T;

.field public static final enum MOMENTS:LX/01T;

.field public static final enum NATIVEMSITE:LX/01T;

.field public static final enum PAA:LX/01T;

.field public static final enum PETOPE:LX/01T;

.field public static final enum PHONE:LX/01T;

.field public static final enum SAMPLE:LX/01T;

.field public static final enum SPUTNIK:LX/01T;

.field public static final enum UNKNOWN:LX/01T;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4769
    new-instance v0, LX/01T;

    const-string v1, "MESSENGER"

    invoke-direct {v0, v1, v3}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->MESSENGER:LX/01T;

    .line 4770
    new-instance v0, LX/01T;

    const-string v1, "FB4A"

    invoke-direct {v0, v1, v4}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->FB4A:LX/01T;

    .line 4771
    new-instance v0, LX/01T;

    const-string v1, "PAA"

    invoke-direct {v0, v1, v5}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->PAA:LX/01T;

    .line 4772
    new-instance v0, LX/01T;

    const-string v1, "SAMPLE"

    invoke-direct {v0, v1, v6}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->SAMPLE:LX/01T;

    .line 4773
    new-instance v0, LX/01T;

    const-string v1, "GIFTS"

    invoke-direct {v0, v1, v7}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->GIFTS:LX/01T;

    .line 4774
    new-instance v0, LX/01T;

    const-string v1, "NATIVEMSITE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->NATIVEMSITE:LX/01T;

    .line 4775
    new-instance v0, LX/01T;

    const-string v1, "HOME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->HOME:LX/01T;

    .line 4776
    new-instance v0, LX/01T;

    const-string v1, "GAMESPORTAL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->GAMESPORTAL:LX/01T;

    .line 4777
    new-instance v0, LX/01T;

    const-string v1, "PHONE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->PHONE:LX/01T;

    .line 4778
    new-instance v0, LX/01T;

    const-string v1, "PETOPE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->PETOPE:LX/01T;

    .line 4779
    new-instance v0, LX/01T;

    const-string v1, "GROUPS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->GROUPS:LX/01T;

    .line 4780
    new-instance v0, LX/01T;

    const-string v1, "ME"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->ME:LX/01T;

    .line 4781
    new-instance v0, LX/01T;

    const-string v1, "MOMENTS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->MOMENTS:LX/01T;

    .line 4782
    new-instance v0, LX/01T;

    const-string v1, "APPMANAGER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->APPMANAGER:LX/01T;

    .line 4783
    new-instance v0, LX/01T;

    const-string v1, "FACEBOOK_SERVICES"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->FACEBOOK_SERVICES:LX/01T;

    .line 4784
    new-instance v0, LX/01T;

    const-string v1, "GETAPPS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->GETAPPS:LX/01T;

    .line 4785
    new-instance v0, LX/01T;

    const-string v1, "FBCAMERA"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->FBCAMERA:LX/01T;

    .line 4786
    new-instance v0, LX/01T;

    const-string v1, "SPUTNIK"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->SPUTNIK:LX/01T;

    .line 4787
    new-instance v0, LX/01T;

    const-string v1, "ALOHA"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->ALOHA:LX/01T;

    .line 4788
    new-instance v0, LX/01T;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/01T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01T;->UNKNOWN:LX/01T;

    .line 4789
    const/16 v0, 0x14

    new-array v0, v0, [LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    aput-object v1, v0, v3

    sget-object v1, LX/01T;->FB4A:LX/01T;

    aput-object v1, v0, v4

    sget-object v1, LX/01T;->PAA:LX/01T;

    aput-object v1, v0, v5

    sget-object v1, LX/01T;->SAMPLE:LX/01T;

    aput-object v1, v0, v6

    sget-object v1, LX/01T;->GIFTS:LX/01T;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/01T;->NATIVEMSITE:LX/01T;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/01T;->HOME:LX/01T;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/01T;->GAMESPORTAL:LX/01T;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/01T;->PHONE:LX/01T;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/01T;->PETOPE:LX/01T;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/01T;->GROUPS:LX/01T;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/01T;->ME:LX/01T;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/01T;->MOMENTS:LX/01T;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/01T;->APPMANAGER:LX/01T;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/01T;->FACEBOOK_SERVICES:LX/01T;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/01T;->GETAPPS:LX/01T;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/01T;->FBCAMERA:LX/01T;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/01T;->SPUTNIK:LX/01T;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/01T;->ALOHA:LX/01T;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/01T;->UNKNOWN:LX/01T;

    aput-object v2, v0, v1

    sput-object v0, LX/01T;->$VALUES:[LX/01T;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4790
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/01T;
    .locals 1

    .prologue
    .line 4791
    const-class v0, LX/01T;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/01T;

    return-object v0
.end method

.method public static values()[LX/01T;
    .locals 1

    .prologue
    .line 4792
    sget-object v0, LX/01T;->$VALUES:[LX/01T;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/01T;

    return-object v0
.end method
