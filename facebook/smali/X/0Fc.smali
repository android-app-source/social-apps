.class public final LX/0Fc;
.super LX/0Fb;
.source ""


# instance fields
.field private final lowMemoryThreshold:I

.field private final mApk:Ljava/io/File;

.field private final mDexes:[LX/02Z;

.field private final mIsComplete:Z


# direct methods
.method public constructor <init>([LX/02Z;Ljava/io/File;J)V
    .locals 5

    .prologue
    .line 33871
    const/4 v0, 0x5

    invoke-static {p1, p3, p4}, LX/0Fc;->makeExpectedFileList([LX/02Z;J)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/0Fb;-><init>(I[Ljava/lang/String;)V

    .line 33872
    const/high16 v0, 0x100000

    iput v0, p0, LX/0Fc;->lowMemoryThreshold:I

    .line 33873
    iput-object p1, p0, LX/0Fc;->mDexes:[LX/02Z;

    .line 33874
    iput-object p2, p0, LX/0Fc;->mApk:Ljava/io/File;

    .line 33875
    const-wide/16 v0, 0x10

    and-long/2addr v0, p3

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/0Fc;->mIsComplete:Z

    .line 33876
    return-void

    .line 33877
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTruncatedSize(LX/02U;)I
    .locals 1

    .prologue
    .line 33869
    invoke-virtual {p1}, LX/02U;->readConfig()LX/080;

    move-result-object v0

    .line 33870
    iget v0, v0, LX/080;->artTruncatedDexSize:I

    return v0
.end method

.method private static isOatFileStillValid(Ljava/io/File;JJ)Z
    .locals 5

    .prologue
    .line 33864
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 33865
    invoke-virtual {p0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 33866
    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    cmp-long v0, p3, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 33867
    :cond_0
    const/4 v0, 0x0

    .line 33868
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static makeExpectedFileList([LX/02Z;J)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 33858
    invoke-static {p0}, LX/0Fb;->makeExpectedFileList([LX/02Z;)[Ljava/lang/String;

    move-result-object v1

    .line 33859
    const-wide/16 v2, 0x10

    and-long/2addr v2, p1

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 33860
    array-length v0, v1

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 33861
    array-length v2, v1

    invoke-static {v1, v6, v0, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 33862
    array-length v1, v1

    const-string v2, "everything.oat"

    aput-object v2, v0, v1

    .line 33863
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private needsTruncation(Ljava/io/File;I)Z
    .locals 4

    .prologue
    .line 33857
    if-ltz p2, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    int-to-long v2, p2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private readMemInfoFromProc()J
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x3

    .line 33878
    const-wide/16 v4, -0x1

    .line 33879
    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    const-string v1, "/proc/meminfo"

    invoke-direct {v3, v1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33880
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-wide v0, v4

    .line 33881
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 33882
    const-string v5, "MemTotal: (.*)"

    invoke-virtual {v4, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 33883
    const-string v5, "\\s+"

    const/4 v6, 0x3

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    .line 33884
    array-length v5, v4

    if-ne v5, v7, :cond_0

    .line 33885
    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v0

    int-to-long v0, v0

    .line 33886
    goto :goto_0

    .line 33887
    :cond_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 33888
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 33889
    :goto_1
    return-wide v0

    :catch_0
    move-object v2, v0

    move-object v3, v0

    move-wide v0, v4

    .line 33890
    :goto_2
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 33891
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 33892
    :catch_1
    goto :goto_1

    .line 33893
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    .line 33894
    :goto_3
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 33895
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 33896
    :goto_4
    throw v0

    :catch_2
    goto :goto_4

    .line 33897
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_3

    :catchall_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-object v2, v0

    move-wide v0, v4

    goto :goto_2

    :catch_4
    goto :goto_2

    .line 33898
    :catch_5
    goto :goto_1
.end method

.method private truncateWithBackup(Ljava/io/File;Ljava/io/File;I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 33839
    if-ltz p3, :cond_0

    .line 33840
    const v0, 0x8000

    new-array v0, v0, [B

    .line 33841
    invoke-static {p1, p2}, LX/02Q;->renameOrThrow(Ljava/io/File;Ljava/io/File;)V

    .line 33842
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 33843
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 33844
    :try_start_1
    invoke-static {v3, v4, p3, v0}, LX/02Q;->copyBytes(Ljava/io/OutputStream;Ljava/io/InputStream;I[B)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 33845
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 33846
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 33847
    :cond_0
    return-void

    .line 33848
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 33849
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :goto_1
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 33850
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 33851
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_2

    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :goto_3
    throw v0

    .line 33852
    :catch_2
    move-exception v4

    :try_start_8
    invoke-static {v1, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 33853
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 33854
    :cond_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_1

    .line 33855
    :catch_3
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    goto :goto_3

    .line 33856
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final configureClassLoader(Ljava/io/File;LX/02f;)V
    .locals 22

    .prologue
    .line 33785
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/0Fc;->mIsComplete:Z

    if-nez v2, :cond_1

    .line 33786
    const-string v2, "isComplete so avoid loading secondary dexes"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33787
    invoke-super/range {p0 .. p2}, LX/0Fb;->configureClassLoader(Ljava/io/File;LX/02f;)V

    .line 33788
    :cond_0
    :goto_0
    return-void

    .line 33789
    :cond_1
    const-string v2, "We pass through this code when loading secondary dexes"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33790
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Fc;->mDexes:[LX/02Z;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, LX/02c;->expectedFiles:[Ljava/lang/String;

    array-length v3, v3

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    :goto_1
    const-string v3, "expect oat"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 33791
    const-string v2, "loading pre-built omni-oat"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33792
    new-instance v10, Ljava/io/File;

    const-string v2, "everything.oat"

    move-object/from16 v0, p1

    invoke-direct {v10, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33793
    invoke-static/range {p1 .. p1}, LX/02U;->findOpened(Ljava/io/File;)LX/02U;

    move-result-object v11

    .line 33794
    invoke-virtual {v11}, LX/02U;->reportStatus()J

    move-result-wide v4

    .line 33795
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, LX/0Fc;->getTruncatedSize(LX/02U;)I

    move-result v12

    .line 33796
    invoke-virtual {v10}, Ljava/io/File;->lastModified()J

    move-result-wide v14

    .line 33797
    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v16

    .line 33798
    const/4 v3, 0x1

    .line 33799
    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v6, v0, LX/02c;->expectedFiles:[Ljava/lang/String;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v2, v6, :cond_7

    .line 33800
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/02c;->expectedFiles:[Ljava/lang/String;

    aget-object v6, v6, v2

    move-object/from16 v0, p1

    invoke-direct {v13, v0, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33801
    new-instance v18, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, LX/02c;->expectedFiles:[Ljava/lang/String;

    aget-object v7, v7, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".backup"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33802
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "odexSchemeArtXDex.configureClassLoader() status="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, LX/02U;->reportStatus()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 33803
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_4

    .line 33804
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " expected dex file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " not found"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 33805
    :cond_2
    :goto_3
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v6, v7}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33806
    :try_start_0
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v12}, LX/0Fc;->needsTruncation(Ljava/io/File;I)Z

    move-result v7

    if-eqz v7, :cond_5

    if-eqz v3, :cond_5

    .line 33807
    const-wide/16 v8, 0x10

    add-int/lit8 v7, v2, 0x1

    shl-long v20, v8, v7

    .line 33808
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v13, v1, v12}, LX/0Fc;->truncateWithBackup(Ljava/io/File;Ljava/io/File;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 33809
    const-wide/16 v8, -0x1

    xor-long v8, v8, v20

    and-long/2addr v8, v4

    .line 33810
    :try_start_1
    const-string v4, "attempting to truncate %s to %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v13, v5, v7

    const/4 v7, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v5, v7

    invoke-static {v4, v5}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33811
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v10}, LX/02f;->addDex(Ljava/io/File;Ljava/io/File;)V

    .line 33812
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "added truncated dex ok "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 33813
    or-long v4, v8, v20

    .line 33814
    :try_start_2
    invoke-static/range {v18 .. v18}, LX/02Q;->deleteRecursive(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 33815
    :goto_4
    move-wide/from16 v0, v16

    invoke-static {v10, v0, v1, v14, v15}, LX/0Fc;->isOatFileStillValid(Ljava/io/File;JJ)Z

    move-result v6

    and-int/2addr v3, v6

    .line 33816
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 33817
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 33818
    :cond_4
    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/16 v20, 0x0

    cmp-long v7, v8, v20

    if-nez v7, :cond_2

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 33819
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " attempting to load 0 length dex file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " when we seemed to have already compiled to everything.oat"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_3

    .line 33820
    :catch_0
    move-exception v4

    move-object v7, v4

    move-wide v4, v8

    .line 33821
    :goto_5
    :try_start_3
    move-object/from16 v0, v18

    invoke-static {v0, v13}, LX/02Q;->renameOrThrow(Ljava/io/File;Ljava/io/File;)V

    .line 33822
    const-string v8, "failed to load truncated dex"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, LX/02P;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33823
    invoke-virtual {v11}, LX/02U;->forceRegenerateOnNextLoad()V

    .line 33824
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v10}, LX/02f;->addDex(Ljava/io/File;Ljava/io/File;)V

    .line 33825
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "added full dex ok "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v7, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_4

    .line 33826
    :catch_1
    move-exception v2

    .line 33827
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "IOException adding dex "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " will rethrow"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, LX/02P;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33828
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3, v6, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 33829
    :cond_5
    if-eqz v3, :cond_6

    .line 33830
    :try_start_4
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v10}, LX/02f;->addDex(Ljava/io/File;Ljava/io/File;)V

    .line 33831
    :goto_6
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "added dex ok "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v7, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto/16 :goto_4

    .line 33832
    :cond_6
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, LX/02f;->addDex(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_6

    .line 33833
    :cond_7
    if-nez v3, :cond_8

    .line 33834
    invoke-virtual {v11}, LX/02U;->forceRegenerateOnNextLoad()V

    .line 33835
    :cond_8
    const-wide/16 v2, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, LX/02c;->expectedFiles:[Ljava/lang/String;

    array-length v6, v6

    add-int/lit8 v6, v6, 0x4

    shl-long/2addr v2, v6

    const-wide/16 v6, 0x1

    sub-long/2addr v2, v6

    .line 33836
    and-long v6, v2, v4

    cmp-long v2, v6, v2

    if-nez v2, :cond_0

    .line 33837
    invoke-virtual {v11, v4, v5}, LX/02U;->writeStatusLocked(J)V

    goto/16 :goto_0

    .line 33838
    :catch_2
    move-exception v7

    goto/16 :goto_5
.end method

.method public final getSchemeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33784
    const-string v0, "OdexSchemeArtXdex"

    return-object v0
.end method

.method public final needOptimization(J)Z
    .locals 5

    .prologue
    .line 33783
    const-wide/16 v0, 0x10

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final optimize(LX/02U;LX/08Z;LX/0FQ;)V
    .locals 17
    .param p3    # LX/0FQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 33662
    if-eqz p3, :cond_0

    .line 33663
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, LX/0FQ;->onProgress(IIZ)V

    .line 33664
    :cond_0
    invoke-direct/range {p0 .. p0}, LX/0Fc;->readMemInfoFromProc()J

    .line 33665
    const-string v2, "dexopt"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/02U;->makeTemporaryDirectory(Ljava/lang/String;)LX/087;

    move-result-object v8

    const/4 v7, 0x0

    .line 33666
    :try_start_0
    const-string v2, "[opt] opened tmpDir %s; starting job"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v8, LX/087;->directory:Ljava/io/File;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33667
    new-instance v9, LX/08T;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object/from16 v0, p2

    invoke-direct {v9, v0}, LX/08T;-><init>(LX/08Z;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    const/4 v6, 0x0

    .line 33668
    :try_start_1
    const-string v2, "[opt] opened job"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33669
    iget-wide v2, v9, LX/08T;->initialStatus:J

    const-wide/16 v4, 0x10

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 33670
    const-string v2, "[opt] nothing to do: ART xdex already complete"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 33671
    :try_start_2
    invoke-virtual {v9}, LX/08T;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    .line 33672
    if-eqz v8, :cond_1

    invoke-virtual {v8}, LX/087;->close()V

    .line 33673
    :cond_1
    :goto_0
    return-void

    .line 33674
    :cond_2
    :try_start_3
    move-object/from16 v0, p2

    iget-object v2, v0, LX/08Z;->config:LX/08F;

    iget v2, v2, LX/08F;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    move v4, v2

    .line 33675
    :goto_1
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p1

    iget-object v2, v0, LX/02U;->root:Ljava/io/File;

    const-string v3, "everything.oat"

    invoke-direct {v10, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33676
    new-instance v11, Ljava/io/File;

    iget-object v2, v8, LX/087;->directory:Ljava/io/File;

    const-string v3, "everything.oat"

    invoke-direct {v11, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33677
    const-string v2, "BOOTCLASSPATH"

    invoke-static {v2}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 33678
    invoke-virtual/range {p1 .. p1}, LX/02U;->getDependencyOdexFiles()[Ljava/io/File;

    move-result-object v5

    .line 33679
    const/4 v2, 0x0

    :goto_2
    array-length v12, v5

    if-ge v2, v12, :cond_4

    .line 33680
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v12, ":"

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v12, v5, v2

    invoke-virtual {v12}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v12

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 33681
    add-int/lit8 v2, v2, 0x2

    goto :goto_2

    .line 33682
    :cond_3
    const/4 v2, 0x0

    move v4, v2

    goto :goto_1

    .line 33683
    :cond_4
    new-instance v2, LX/086;

    const-string v5, "/system/bin/dex2oat"

    const/4 v12, 0x4

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "--oat-file="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "--oat-location="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    const-string v14, "--no-watch-dog"

    aput-object v14, v12, v13

    const/4 v13, 0x3

    const-string v14, "--dump-timing"

    aput-object v14, v12, v13

    invoke-direct {v2, v5, v12}, LX/086;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    const-string v5, "BOOTCLASSPATH"

    invoke-virtual {v2, v5, v3}, LX/086;->setenv(Ljava/lang/String;Ljava/lang/String;)LX/086;

    move-result-object v12

    .line 33684
    const-string v2, "dalvik.vm.dex2oat-Xms"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 33685
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 33686
    const-string v3, "--runtime-arg"

    invoke-virtual {v12, v3}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33687
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "-Xms"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33688
    :cond_5
    const-string v2, "dalvik.vm.dex2oat-Xmx"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 33689
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 33690
    const-string v3, "--runtime-arg"

    invoke-virtual {v12, v3}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33691
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "-Xmx"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33692
    :cond_6
    const-string v2, "dalvik.vm.dex2oat-filter"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 33693
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    .line 33694
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "--compiler-filter="

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33695
    :goto_3
    const-string v2, "dalvik.vm.dex2oat-flags"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 33696
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    .line 33697
    new-instance v3, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v5, 0x20

    invoke-direct {v3, v5}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 33698
    invoke-virtual {v3, v2}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 33699
    :goto_4
    invoke-virtual {v3}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 33700
    invoke-virtual {v3}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/086;->addArgument(Ljava/lang/String;)LX/086;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_4

    .line 33701
    :catch_0
    move-exception v2

    :try_start_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 33702
    :catchall_0
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    :goto_5
    if-eqz v3, :cond_1b

    :try_start_5
    invoke-virtual {v9}, LX/08T;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    :goto_6
    :try_start_6
    throw v2
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    .line 33703
    :catch_1
    move-exception v2

    :try_start_7
    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 33704
    :catchall_1
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    :goto_7
    if-eqz v8, :cond_7

    if-eqz v3, :cond_1c

    :try_start_8
    invoke-virtual {v8}, LX/087;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_5

    :cond_7
    :goto_8
    throw v2

    .line 33705
    :cond_8
    :try_start_9
    const-string v2, "--compiler-filter=interpret-only"

    invoke-virtual {v12, v2}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    goto :goto_3

    .line 33706
    :catchall_2
    move-exception v2

    move-object v3, v6

    goto :goto_5

    .line 33707
    :cond_9
    if-eqz v4, :cond_a

    .line 33708
    const-string v2, "-j1"

    invoke-virtual {v12, v2}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33709
    :cond_a
    move-object/from16 v0, p2

    iget-object v3, v0, LX/08Z;->dexStoreConfig:LX/080;

    .line 33710
    iget-byte v2, v3, LX/080;->artFilter:B

    if-eqz v2, :cond_b

    .line 33711
    const/4 v2, 0x0

    .line 33712
    iget-byte v4, v3, LX/080;->artFilter:B

    packed-switch v4, :pswitch_data_0

    .line 33713
    const-string v4, "ignoring unknown configured ART filter %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-byte v14, v3, LX/080;->artFilter:B

    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v14

    aput-object v14, v5, v13

    invoke-static {v4, v5}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33714
    :goto_9
    if-eqz v2, :cond_b

    .line 33715
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "--compiler-filter="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33716
    :cond_b
    iget v2, v3, LX/080;->artHugeMethodMax:I

    if-ltz v2, :cond_c

    .line 33717
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "--huge-method-max="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v3, LX/080;->artHugeMethodMax:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33718
    :cond_c
    iget v2, v3, LX/080;->artLargeMethodMax:I

    if-ltz v2, :cond_d

    .line 33719
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "--large-method-max="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v3, LX/080;->artLargeMethodMax:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33720
    :cond_d
    iget v2, v3, LX/080;->artSmallMethodMax:I

    if-ltz v2, :cond_e

    .line 33721
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "--small-method-max="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v3, LX/080;->artSmallMethodMax:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33722
    :cond_e
    iget v2, v3, LX/080;->artTinyMethodMax:I

    if-ltz v2, :cond_f

    .line 33723
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "--tiny-method-max="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v3, LX/080;->artTinyMethodMax:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33724
    :cond_f
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ne v2, v3, :cond_10

    .line 33725
    const-string v2, "arthook"

    invoke-static {v2}, LX/01L;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 33726
    const-string v3, "LD_PRELOAD"

    invoke-static {v3}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 33727
    const-string v4, "LD_PRELOAD"

    if-nez v3, :cond_11

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    :goto_a
    invoke-virtual {v12, v4, v2}, LX/086;->setenv(Ljava/lang/String;Ljava/lang/String;)LX/086;

    .line 33728
    const-string v2, "LD_LIBRARY_PATH"

    invoke-static {}, LX/01L;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, LX/086;->setenv(Ljava/lang/String;Ljava/lang/String;)LX/086;

    .line 33729
    const-string v2, "FB_ENABLE_MIRANDA_HACK"

    const-string v3, "1"

    invoke-virtual {v12, v2, v3}, LX/086;->setenv(Ljava/lang/String;Ljava/lang/String;)LX/086;

    .line 33730
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Fc;->mDexes:[LX/02Z;

    const-wide/16 v4, 0x0

    invoke-static {v2, v4, v5}, LX/0Fc;->makeExpectedFileList([LX/02Z;J)[Ljava/lang/String;

    move-result-object v3

    .line 33731
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "--dex-file="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Fc;->mApk:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33732
    const/4 v2, 0x0

    :goto_b
    array-length v4, v3

    if-ge v2, v4, :cond_12

    .line 33733
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/02U;->root:Ljava/io/File;

    aget-object v13, v3, v2

    invoke-direct {v4, v5, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33734
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v13, "--dex-file="

    invoke-direct {v5, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33735
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 33736
    :pswitch_0
    const-string v2, "verify-none"

    goto/16 :goto_9

    .line 33737
    :pswitch_1
    const-string v2, "interpret-only"

    goto/16 :goto_9

    .line 33738
    :pswitch_2
    const-string v2, "space"

    goto/16 :goto_9

    .line 33739
    :pswitch_3
    const-string v2, "balanced"

    goto/16 :goto_9

    .line 33740
    :pswitch_4
    const-string v2, "speed"

    goto/16 :goto_9

    .line 33741
    :pswitch_5
    const-string v2, "everything"

    goto/16 :goto_9

    .line 33742
    :pswitch_6
    const-string v2, "time"

    goto/16 :goto_9

    .line 33743
    :cond_11
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ":"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_a

    .line 33744
    :cond_12
    invoke-virtual {v9}, LX/08T;->startOptimizing()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 33745
    const/4 v3, 0x0

    .line 33746
    const/4 v4, 0x0

    .line 33747
    :try_start_a
    iget-object v2, v8, LX/087;->directory:Ljava/io/File;

    invoke-static {v2}, LX/02Q;->openUnlinkedTemporaryFile(Ljava/io/File;)Ljava/io/RandomAccessFile;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    move-result-object v5

    .line 33748
    const/4 v2, 0x1

    :try_start_b
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-static {v3}, LX/08B;->fileno(Ljava/io/FileDescriptor;)I

    move-result v3

    invoke-virtual {v12, v2, v3}, LX/086;->setStream(II)LX/086;

    .line 33749
    const/4 v2, 0x2

    const/4 v3, -0x5

    invoke-virtual {v12, v2, v3}, LX/086;->setStream(II)LX/086;

    .line 33750
    const-string v2, "starting dex2oat to build %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-string v14, "everything.oat"

    aput-object v14, v3, v13

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33751
    move-object/from16 v0, p2

    iget-object v2, v0, LX/08Z;->config:LX/08F;

    iget-object v2, v2, LX/08F;->prio:LX/08D;

    invoke-virtual {v2}, LX/08D;->with()LX/0Fk;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    move-result-object v13

    const/4 v3, 0x0

    .line 33752
    :try_start_c
    invoke-virtual {v12}, LX/086;->create()Lcom/facebook/forker/Process;
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    move-result-object v3

    .line 33753
    if-eqz v13, :cond_13

    :try_start_d
    invoke-virtual {v13}, LX/0Fk;->close()V

    .line 33754
    :cond_13
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, LX/08Z;->waitForAndManageProcess(Lcom/facebook/forker/Process;)I

    move-result v2

    .line 33755
    invoke-static {v5}, LX/02Q;->readProgramOutputFile(Ljava/io/RandomAccessFile;)Ljava/lang/String;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_7

    move-result-object v4

    .line 33756
    :try_start_e
    invoke-static {v5}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 33757
    if-eqz v3, :cond_14

    .line 33758
    invoke-virtual {v3}, Lcom/facebook/forker/Process;->destroy()V

    .line 33759
    :cond_14
    const-string v3, "dex2oat exited with status %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v5, v12

    invoke-static {v3, v5}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33760
    if-eqz v2, :cond_18

    .line 33761
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v5, "dex2oat failed: %s: %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2}, Lcom/facebook/forker/Process;->describeStatus(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v11

    const/4 v2, 0x1

    aput-object v4, v10, v2

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 33762
    :catch_2
    move-exception v3

    :try_start_f
    throw v3
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    .line 33763
    :catchall_3
    move-exception v2

    if-eqz v13, :cond_15

    if-eqz v3, :cond_17

    :try_start_10
    invoke-virtual {v13}, LX/0Fk;->close()V
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_3
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    :cond_15
    :goto_c
    :try_start_11
    throw v2
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    .line 33764
    :catchall_4
    move-exception v2

    move-object v3, v4

    move-object v4, v5

    :goto_d
    :try_start_12
    invoke-static {v4}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 33765
    if-eqz v3, :cond_16

    .line 33766
    invoke-virtual {v3}, Lcom/facebook/forker/Process;->destroy()V

    :cond_16
    throw v2
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_0
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    .line 33767
    :catch_3
    move-exception v10

    :try_start_13
    invoke-static {v3, v10}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_c

    :cond_17
    invoke-virtual {v13}, LX/0Fk;->close()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_4

    goto :goto_c

    .line 33768
    :cond_18
    :try_start_14
    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v12, 0x0

    cmp-long v2, v2, v12

    if-nez v2, :cond_19

    .line 33769
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "dex2oat produced impossibly short oat file:"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 33770
    :cond_19
    invoke-virtual {v9}, LX/08T;->startCommitting()J

    move-result-wide v2

    .line 33771
    invoke-static {v11, v10}, LX/02Q;->renameOrThrow(Ljava/io/File;Ljava/io/File;)V

    .line 33772
    const-wide/16 v4, 0x10

    or-long/2addr v2, v4

    invoke-virtual {v9, v2, v3}, LX/08T;->finishCommit(J)V

    .line 33773
    const-string v2, "ART xdex optimization complete"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_0
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    .line 33774
    :try_start_15
    invoke-virtual {v9}, LX/08T;->close()V
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_15 .. :try_end_15} :catch_1
    .catchall {:try_start_15 .. :try_end_15} :catchall_5

    .line 33775
    if-eqz v8, :cond_1a

    invoke-virtual {v8}, LX/087;->close()V

    .line 33776
    :cond_1a
    if-eqz p3, :cond_1

    .line 33777
    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, LX/0FQ;->onProgress(IIZ)V

    goto/16 :goto_0

    .line 33778
    :catch_4
    move-exception v4

    :try_start_16
    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto/16 :goto_6

    .line 33779
    :catchall_5
    move-exception v2

    move-object v3, v7

    goto/16 :goto_7

    .line 33780
    :cond_1b
    invoke-virtual {v9}, LX/08T;->close()V
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_1
    .catchall {:try_start_16 .. :try_end_16} :catchall_5

    goto/16 :goto_6

    .line 33781
    :catch_5
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    :cond_1c
    invoke-virtual {v8}, LX/087;->close()V

    goto/16 :goto_8

    .line 33782
    :catchall_6
    move-exception v2

    move-object/from16 v16, v4

    move-object v4, v3

    move-object/from16 v3, v16

    goto :goto_d

    :catchall_7
    move-exception v2

    move-object v4, v5

    goto :goto_d

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
