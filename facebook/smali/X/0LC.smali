.class public final LX/0LC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0LB;


# instance fields
.field private final a:J

.field private final b:J

.field private final c:J

.field private final d:J

.field private final e:LX/0OX;


# direct methods
.method public constructor <init>(JJJJLX/0OX;)V
    .locals 1

    .prologue
    .line 43061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43062
    iput-wide p1, p0, LX/0LC;->a:J

    .line 43063
    iput-wide p3, p0, LX/0LC;->b:J

    .line 43064
    iput-wide p5, p0, LX/0LC;->c:J

    .line 43065
    iput-wide p7, p0, LX/0LC;->d:J

    .line 43066
    iput-object p9, p0, LX/0LC;->e:LX/0OX;

    .line 43067
    return-void
.end method


# virtual methods
.method public final a([J)[J
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 43068
    invoke-virtual {p0, p1}, LX/0LC;->b([J)[J

    move-result-object v0

    .line 43069
    const/4 v1, 0x0

    aget-wide v2, v0, v1

    div-long/2addr v2, v4

    aput-wide v2, v0, v1

    .line 43070
    const/4 v1, 0x1

    aget-wide v2, v0, v1

    div-long/2addr v2, v4

    aput-wide v2, v0, v1

    .line 43071
    return-object v0
.end method

.method public final b([J)[J
    .locals 8

    .prologue
    const/4 v1, 0x2

    .line 43072
    if-eqz p1, :cond_0

    array-length v0, p1

    if-ge v0, v1, :cond_1

    .line 43073
    :cond_0
    new-array p1, v1, [J

    .line 43074
    :cond_1
    iget-wide v0, p0, LX/0LC;->b:J

    iget-object v2, p0, LX/0LC;->e:LX/0OX;

    invoke-interface {v2}, LX/0OX;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iget-wide v4, p0, LX/0LC;->c:J

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 43075
    iget-wide v0, p0, LX/0LC;->a:J

    .line 43076
    iget-wide v4, p0, LX/0LC;->d:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    .line 43077
    iget-wide v4, p0, LX/0LC;->d:J

    sub-long v4, v2, v4

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 43078
    :cond_2
    const/4 v4, 0x0

    aput-wide v0, p1, v4

    .line 43079
    const/4 v0, 0x1

    aput-wide v2, p1, v0

    .line 43080
    return-object p1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43081
    if-ne p1, p0, :cond_1

    .line 43082
    :cond_0
    :goto_0
    return v0

    .line 43083
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 43084
    goto :goto_0

    .line 43085
    :cond_3
    check-cast p1, LX/0LC;

    .line 43086
    iget-wide v2, p1, LX/0LC;->a:J

    iget-wide v4, p0, LX/0LC;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-wide v2, p1, LX/0LC;->b:J

    iget-wide v4, p0, LX/0LC;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-wide v2, p1, LX/0LC;->c:J

    iget-wide v4, p0, LX/0LC;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-wide v2, p1, LX/0LC;->d:J

    iget-wide v4, p0, LX/0LC;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 43087
    iget-wide v0, p0, LX/0LC;->a:J

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 43088
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, LX/0LC;->b:J

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 43089
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, LX/0LC;->c:J

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 43090
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, LX/0LC;->d:J

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 43091
    return v0
.end method
