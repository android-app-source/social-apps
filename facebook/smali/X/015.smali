.class public LX/015;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3717
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/015;->a:[I

    return-void

    :array_0
    .array-data 4
        0x20
        0x20
        0x20
        0x20
        0x20
        0x20
        0x20
        0x20
        0x20
        0x2020
        0x2020
        0x2020
        0x2020
        0x20
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a()[J
    .locals 1

    .prologue
    .line 3719
    const-string v0, "/proc/self/stat"

    invoke-static {v0}, LX/015;->a(Ljava/lang/String;)[J

    move-result-object v0

    return-object v0
.end method

.method private static final a(Ljava/lang/String;)[J
    .locals 3

    .prologue
    .line 3720
    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    .line 3721
    sget-object v1, LX/015;->a:[I

    const/4 v2, 0x0

    .line 3722
    invoke-static {p0, v1, v2, v0, v2}, LX/016;->a(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    .line 3723
    return-object v0

    .line 3724
    nop

    :array_0
    .array-data 8
        -0x1
        -0x1
        -0x1
        -0x1
    .end array-data
.end method

.method public static final b()[J
    .locals 3

    .prologue
    .line 3725
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    .line 3726
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/proc/self/task/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/stat"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 3727
    move-object v0, v0

    .line 3728
    invoke-static {v0}, LX/015;->a(Ljava/lang/String;)[J

    move-result-object v0

    return-object v0
.end method
