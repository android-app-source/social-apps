.class public final enum LX/0Jn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Jn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Jn;

.field public static final enum MP4:LX/0Jn;

.field public static final enum UNKNOWN:LX/0Jn;

.field public static final enum WEBM:LX/0Jn;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39747
    new-instance v0, LX/0Jn;

    const-string v1, "MP4"

    invoke-direct {v0, v1, v2}, LX/0Jn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Jn;->MP4:LX/0Jn;

    .line 39748
    new-instance v0, LX/0Jn;

    const-string v1, "WEBM"

    invoke-direct {v0, v1, v3}, LX/0Jn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Jn;->WEBM:LX/0Jn;

    .line 39749
    new-instance v0, LX/0Jn;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/0Jn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Jn;->UNKNOWN:LX/0Jn;

    .line 39750
    const/4 v0, 0x3

    new-array v0, v0, [LX/0Jn;

    sget-object v1, LX/0Jn;->MP4:LX/0Jn;

    aput-object v1, v0, v2

    sget-object v1, LX/0Jn;->WEBM:LX/0Jn;

    aput-object v1, v0, v3

    sget-object v1, LX/0Jn;->UNKNOWN:LX/0Jn;

    aput-object v1, v0, v4

    sput-object v0, LX/0Jn;->$VALUES:[LX/0Jn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39746
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Jn;
    .locals 1

    .prologue
    .line 39745
    const-class v0, LX/0Jn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Jn;

    return-object v0
.end method

.method public static values()[LX/0Jn;
    .locals 1

    .prologue
    .line 39744
    sget-object v0, LX/0Jn;->$VALUES:[LX/0Jn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Jn;

    return-object v0
.end method
