.class public LX/09G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/1fT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile k:LX/09G;


# instance fields
.field private final b:LX/1fU;

.field private final c:LX/0lp;

.field public final d:LX/2Hu;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field public final f:LX/0lC;

.field private final g:LX/0Yb;

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/0lF;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/0lF;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private j:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22209
    const-class v0, LX/09G;

    sput-object v0, LX/09G;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1fU;LX/0lp;LX/2Hu;LX/0lC;Ljava/util/concurrent/ExecutorService;LX/0Xl;)V
    .locals 3
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p6    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 22210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22211
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/09G;->h:Ljava/util/Map;

    .line 22212
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/09G;->i:Ljava/util/Map;

    .line 22213
    iput-object p1, p0, LX/09G;->b:LX/1fU;

    .line 22214
    iput-object p2, p0, LX/09G;->c:LX/0lp;

    .line 22215
    iput-object p3, p0, LX/09G;->d:LX/2Hu;

    .line 22216
    iput-object p4, p0, LX/09G;->f:LX/0lC;

    .line 22217
    iput-object p5, p0, LX/09G;->e:Ljava/util/concurrent/ExecutorService;

    .line 22218
    invoke-direct {p0}, LX/09G;->d()V

    .line 22219
    invoke-interface {p6}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    new-instance v2, LX/09H;

    invoke-direct {v2, p0}, LX/09H;-><init>(LX/09G;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v2, LX/09I;

    invoke-direct {v2, p0}, LX/09I;-><init>(LX/09G;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v2, LX/09J;

    invoke-direct {v2, p0}, LX/09J;-><init>(LX/09G;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/09G;->g:LX/0Yb;

    .line 22220
    iget-object v0, p0, LX/09G;->g:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 22221
    return-void
.end method

.method public static a(LX/0QB;)LX/09G;
    .locals 3

    .prologue
    .line 22222
    sget-object v0, LX/09G;->k:LX/09G;

    if-nez v0, :cond_1

    .line 22223
    const-class v1, LX/09G;

    monitor-enter v1

    .line 22224
    :try_start_0
    sget-object v0, LX/09G;->k:LX/09G;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 22225
    if-eqz v2, :cond_0

    .line 22226
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/09G;->b(LX/0QB;)LX/09G;

    move-result-object v0

    sput-object v0, LX/09G;->k:LX/09G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 22227
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 22228
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 22229
    :cond_1
    sget-object v0, LX/09G;->k:LX/09G;

    return-object v0

    .line 22230
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 22231
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static synthetic a(LX/09G;LX/162;LX/162;LX/0lF;)LX/0m9;
    .locals 1

    .prologue
    .line 22232
    invoke-static {p1, p2, p3}, LX/09G;->a(LX/162;LX/162;LX/0lF;)LX/0m9;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/162;LX/162;LX/0lF;)LX/0m9;
    .locals 3

    .prologue
    .line 22233
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 22234
    if-eqz p0, :cond_0

    .line 22235
    const-string v1, "sub"

    invoke-virtual {v0, v1, p0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 22236
    :cond_0
    if-eqz p1, :cond_1

    .line 22237
    const-string v1, "unsub"

    invoke-virtual {v0, v1, p1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 22238
    :cond_1
    if-eqz p2, :cond_2

    .line 22239
    const-string v1, "pub"

    invoke-virtual {v0, v1, p2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 22240
    :cond_2
    const-string v1, "version"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 22241
    return-object v0
.end method

.method public static a$redex0(LX/09G;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 22242
    const-string v0, "event"

    sget-object v1, LX/2EU;->UNKNOWN:LX/2EU;

    invoke-virtual {v1}, LX/2EU;->toValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, LX/2EU;->fromValue(I)LX/2EU;

    move-result-object v0

    .line 22243
    sget-object v1, LX/2EU;->CHANNEL_CONNECTED:LX/2EU;

    if-ne v0, v1, :cond_0

    .line 22244
    invoke-static {p0}, LX/09G;->e(LX/09G;)V

    .line 22245
    :goto_0
    return-void

    .line 22246
    :cond_0
    monitor-enter p0

    .line 22247
    :try_start_0
    iget-object v0, p0, LX/09G;->i:Ljava/util/Map;

    iget-object v1, p0, LX/09G;->h:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 22248
    iget-object v0, p0, LX/09G;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 22249
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a$redex0(LX/09G;LX/162;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 22250
    invoke-static {p1, v0, v0}, LX/09G;->a(LX/162;LX/162;LX/0lF;)LX/0m9;

    move-result-object v1

    .line 22251
    iget-object v0, p0, LX/09G;->d:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;

    move-result-object v2

    .line 22252
    const/4 v0, 0x0

    .line 22253
    :try_start_0
    const-string v3, "/pubsub"

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v3, v1, v4, v5}, LX/2gV;->a(Ljava/lang/String;LX/0lF;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 22254
    invoke-virtual {v2}, LX/2gV;->f()V

    .line 22255
    :goto_0
    return v0

    .line 22256
    :catch_0
    move-exception v1

    .line 22257
    :try_start_1
    sget-object v3, LX/09G;->a:Ljava/lang/Class;

    const-string v4, "Remote exception for subscribe"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 22258
    invoke-virtual {v2}, LX/2gV;->f()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/2gV;->f()V

    throw v0
.end method

.method private static b(LX/0QB;)LX/09G;
    .locals 7

    .prologue
    .line 22202
    new-instance v0, LX/09G;

    invoke-static {p0}, LX/1fU;->a(LX/0QB;)LX/1fU;

    move-result-object v1

    check-cast v1, LX/1fU;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v2

    check-cast v2, LX/0lp;

    invoke-static {p0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v3

    check-cast v3, LX/2Hu;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-direct/range {v0 .. v6}, LX/09G;-><init>(LX/1fU;LX/0lp;LX/2Hu;LX/0lC;Ljava/util/concurrent/ExecutorService;LX/0Xl;)V

    .line 22203
    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 22204
    new-instance v0, LX/1se;

    const-string v1, "/pubsub"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/1se;-><init>(Ljava/lang/String;I)V

    .line 22205
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 22206
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 22207
    iget-object v0, p0, LX/09G;->b:LX/1fU;

    invoke-static {}, LX/0Rf;->of()LX/0Rf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1fU;->a(Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 22208
    return-void
.end method

.method private static e(LX/09G;)V
    .locals 3

    .prologue
    .line 22151
    monitor-enter p0

    .line 22152
    :try_start_0
    iget-object v0, p0, LX/09G;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/09G;->j:Z

    if-nez v0, :cond_1

    .line 22153
    :cond_0
    monitor-exit p0

    .line 22154
    :goto_0
    return-void

    .line 22155
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22156
    iget-object v0, p0, LX/09G;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$7;

    invoke-direct {v1, p0}, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$7;-><init>(LX/09G;)V

    const v2, 0x77eeec0e

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 22157
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 22158
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/09G;->j:Z

    .line 22159
    invoke-static {p0}, LX/09G;->e(LX/09G;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22160
    monitor-exit p0

    return-void

    .line 22161
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 22162
    iget-object v0, p0, LX/09G;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;

    invoke-direct {v1, p0, p1}, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;-><init>(LX/09G;Ljava/lang/String;)V

    const v2, -0x4ce1f78d

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 22163
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0TF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/0lF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22164
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/09G;->a(Ljava/lang/String;LX/0TF;LX/09K;)V

    .line 22165
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0TF;LX/09K;)V
    .locals 3
    .param p3    # LX/09K;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/0lF;",
            ">;",
            "LX/09K;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22166
    iget-object v0, p0, LX/09G;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;-><init>(LX/09G;Ljava/lang/String;LX/0TF;LX/09K;)V

    const v2, -0x76444f3d

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 22167
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0lF;)V
    .locals 3

    .prologue
    .line 22168
    iget-object v0, p0, LX/09G;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$6;-><init>(LX/09G;Ljava/lang/String;LX/0lF;)V

    const v2, 0x1677d197

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 22169
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 22170
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/09G;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22171
    monitor-exit p0

    return-void

    .line 22172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 22173
    monitor-enter p0

    .line 22174
    :try_start_0
    iget-object v0, p0, LX/09G;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    .line 22175
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22176
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 22177
    invoke-virtual {p0, v0}, LX/09G;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 22178
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 22179
    :cond_0
    return-void
.end method

.method public final onMessage(Ljava/lang/String;[BJ)V
    .locals 6

    .prologue
    .line 22180
    const-string v0, "/pubsub"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22181
    :goto_0
    return-void

    .line 22182
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/09G;->c:LX/0lp;

    invoke-virtual {v0, p2}, LX/0lp;->a([B)LX/15w;

    move-result-object v0

    invoke-virtual {v0}, LX/15w;->J()LX/0lG;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 22183
    const-string v1, "raw"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 22184
    iget-object v1, p0, LX/09G;->c:LX/0lp;

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v0

    invoke-virtual {v0}, LX/15w;->J()LX/0lG;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 22185
    const-string v1, "topic"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    .line 22186
    const-string v2, "payload"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 22187
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22188
    sget-object v0, LX/09G;->a:Ljava/lang/Class;

    const-string v1, "Empty topic"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 22189
    :catch_0
    move-exception v0

    .line 22190
    sget-object v1, LX/09G;->a:Ljava/lang/Class;

    const-string v2, "JsonParseException in onMessage"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 22191
    :cond_1
    if-eqz v2, :cond_2

    :try_start_1
    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;

    .line 22192
    :cond_2
    monitor-enter p0
    :try_end_1
    .catch LX/2aQ; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 22193
    :try_start_2
    iget-object v0, p0, LX/09G;->h:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 22194
    iget-object v0, p0, LX/09G;->h:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TF;

    invoke-interface {v0, v2}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 22195
    :goto_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch LX/2aQ; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 22196
    :catch_1
    move-exception v0

    .line 22197
    sget-object v1, LX/09G;->a:Ljava/lang/Class;

    const-string v2, "IOException in onMessage"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 22198
    :cond_3
    :try_start_4
    iget-object v0, p0, LX/09G;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 22199
    sget-object v0, LX/09G;->a:Ljava/lang/Class;

    const-string v3, "No callback set for topic %s, fallback to pending topic map"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v0, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22200
    iget-object v0, p0, LX/09G;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TF;

    invoke-interface {v0, v2}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_1

    .line 22201
    :cond_4
    sget-object v0, LX/09G;->a:Ljava/lang/Class;

    const-string v2, "No callback set for topic %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method
