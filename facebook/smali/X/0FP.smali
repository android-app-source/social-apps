.class public final LX/0FP;
.super LX/08G;
.source ""


# instance fields
.field private mPausedConfig:LX/08F;

.field private mScreenOffConfig:LX/08F;

.field public final synthetic this$0:Lcom/facebook/common/dextricks/DexOptimization$Service;


# direct methods
.method public constructor <init>(Lcom/facebook/common/dextricks/DexOptimization$Service;)V
    .locals 4

    .prologue
    const/16 v2, 0x12c

    .line 33121
    iput-object p1, p0, LX/0FP;->this$0:Lcom/facebook/common/dextricks/DexOptimization$Service;

    .line 33122
    new-instance v0, LX/08E;

    invoke-direct {v0}, LX/08E;-><init>()V

    .line 33123
    new-instance v1, LX/08D;

    const/16 v3, 0x6000

    const/16 p1, 0x13

    invoke-direct {v1, v3, p1}, LX/08D;-><init>(II)V

    move-object v1, v1

    .line 33124
    iput-object v1, v0, LX/08E;->mPrio:LX/08D;

    .line 33125
    move-object v0, v0

    .line 33126
    const/4 v1, 0x1

    .line 33127
    if-eqz v1, :cond_0

    .line 33128
    iget v3, v0, LX/08E;->mFlags:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, LX/08E;->mFlags:I

    .line 33129
    :goto_0
    move-object v0, v0

    .line 33130
    iput v2, v0, LX/08E;->mOptTimeSliceMs:I

    .line 33131
    move-object v0, v0

    .line 33132
    const/16 v1, 0x3e8

    .line 33133
    iput v1, v0, LX/08E;->mYieldTimeSliceMs:I

    .line 33134
    move-object v0, v0

    .line 33135
    invoke-virtual {v0}, LX/08E;->build()LX/08F;

    move-result-object v0

    invoke-direct {p0, v0}, LX/08G;-><init>(LX/08F;)V

    .line 33136
    new-instance v0, LX/08E;

    iget-object v1, p0, LX/08G;->baseline:LX/08F;

    invoke-direct {v0, v1}, LX/08E;-><init>(LX/08F;)V

    const/16 v1, 0x384

    .line 33137
    iput v1, v0, LX/08E;->mOptTimeSliceMs:I

    .line 33138
    move-object v0, v0

    .line 33139
    iput v2, v0, LX/08E;->mYieldTimeSliceMs:I

    .line 33140
    move-object v0, v0

    .line 33141
    invoke-virtual {v0}, LX/08E;->build()LX/08F;

    move-result-object v0

    iput-object v0, p0, LX/0FP;->mScreenOffConfig:LX/08F;

    .line 33142
    new-instance v0, LX/08E;

    iget-object v1, p0, LX/08G;->baseline:LX/08F;

    invoke-direct {v0, v1}, LX/08E;-><init>(LX/08F;)V

    const/4 v1, 0x0

    .line 33143
    iput v1, v0, LX/08E;->mOptTimeSliceMs:I

    .line 33144
    move-object v0, v0

    .line 33145
    const/16 v1, 0x64

    .line 33146
    iput v1, v0, LX/08E;->mYieldTimeSliceMs:I

    .line 33147
    move-object v0, v0

    .line 33148
    invoke-virtual {v0}, LX/08E;->build()LX/08F;

    move-result-object v0

    iput-object v0, p0, LX/0FP;->mPausedConfig:LX/08F;

    .line 33149
    return-void

    .line 33150
    :cond_0
    iget v3, v0, LX/08E;->mFlags:I

    and-int/lit8 v3, v3, -0x2

    iput v3, v0, LX/08E;->mFlags:I

    goto :goto_0
.end method


# virtual methods
.method public final getInstantaneous()LX/08F;
    .locals 4

    .prologue
    .line 33151
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-object v2, p0, LX/0FP;->this$0:Lcom/facebook/common/dextricks/DexOptimization$Service;

    iget-wide v2, v2, Lcom/facebook/common/dextricks/DexOptimization$Service;->unpauseTime:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 33152
    iget-object v0, p0, LX/0FP;->mPausedConfig:LX/08F;

    .line 33153
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0FP;->this$0:Lcom/facebook/common/dextricks/DexOptimization$Service;

    iget-boolean v0, v0, Lcom/facebook/common/dextricks/DexOptimization$Service;->isScreenOn:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/08G;->baseline:LX/08F;

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/0FP;->mScreenOffConfig:LX/08F;

    goto :goto_0
.end method
