.class public LX/07M;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/07M;


# instance fields
.field public final b:Ljava/util/concurrent/atomic/AtomicLong;

.field public final c:Ljava/util/concurrent/atomic/AtomicLong;

.field public final d:Ljava/util/concurrent/atomic/AtomicLong;

.field public final e:Ljava/util/concurrent/atomic/AtomicLong;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/07N;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19630
    new-instance v0, LX/07M;

    invoke-direct {v0}, LX/07M;-><init>()V

    sput-object v0, LX/07M;->a:LX/07M;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 19623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19624
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/07M;->b:Ljava/util/concurrent/atomic/AtomicLong;

    .line 19625
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/07M;->c:Ljava/util/concurrent/atomic/AtomicLong;

    .line 19626
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/07M;->d:Ljava/util/concurrent/atomic/AtomicLong;

    .line 19627
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/07M;->e:Ljava/util/concurrent/atomic/AtomicLong;

    .line 19628
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/07M;->f:Ljava/util/Map;

    .line 19629
    return-void
.end method

.method private static c(LX/07M;Ljava/lang/String;Ljava/lang/String;)LX/07N;
    .locals 2

    .prologue
    .line 19631
    invoke-static {p2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19632
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 19633
    :cond_0
    iget-object v0, p0, LX/07M;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/07N;

    .line 19634
    if-nez v0, :cond_1

    .line 19635
    iget-object v0, p0, LX/07M;->f:Ljava/util/Map;

    new-instance v1, LX/07N;

    invoke-direct {v1}, LX/07N;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19636
    :cond_1
    iget-object v0, p0, LX/07M;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/07N;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 19618
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/07M;->c:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 19619
    invoke-static {p0, p1, p2}, LX/07M;->c(LX/07M;Ljava/lang/String;Ljava/lang/String;)LX/07N;

    move-result-object v0

    .line 19620
    iget-wide v4, v0, LX/07N;->b:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v0, LX/07N;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 19621
    monitor-exit p0

    return-void

    .line 19622
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 19613
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/07M;->b:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 19614
    invoke-static {p0, p1, p2}, LX/07M;->c(LX/07M;Ljava/lang/String;Ljava/lang/String;)LX/07N;

    move-result-object v0

    .line 19615
    iget-wide v4, v0, LX/07N;->a:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v0, LX/07N;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 19616
    monitor-exit p0

    return-void

    .line 19617
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/07N;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19609
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, LX/07M;->f:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 19610
    iget-object v1, p0, LX/07M;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 19611
    monitor-exit p0

    return-object v0

    .line 19612
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
