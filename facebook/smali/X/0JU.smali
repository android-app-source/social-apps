.class public final enum LX/0JU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JU;

.field public static final enum BADGE_FETCH:LX/0JU;

.field public static final enum DATA_STALE_FOREGROUNDING:LX/0JU;

.field public static final enum DATA_STALE_IN_APP:LX/0JU;

.field public static final enum EXIT_VIDEO_HOME:LX/0JU;

.field public static final enum FOREGROUNDING:LX/0JU;

.field public static final enum INITIAL:LX/0JU;

.field public static final enum MQTT:LX/0JU;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39492
    new-instance v0, LX/0JU;

    const-string v1, "BADGE_FETCH"

    const-string v2, "badge_fetch"

    invoke-direct {v0, v1, v4, v2}, LX/0JU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JU;->BADGE_FETCH:LX/0JU;

    .line 39493
    new-instance v0, LX/0JU;

    const-string v1, "DATA_STALE_FOREGROUNDING"

    const-string v2, "data_stale_foregrounding"

    invoke-direct {v0, v1, v5, v2}, LX/0JU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JU;->DATA_STALE_FOREGROUNDING:LX/0JU;

    .line 39494
    new-instance v0, LX/0JU;

    const-string v1, "DATA_STALE_IN_APP"

    const-string v2, "data_stale_in_app"

    invoke-direct {v0, v1, v6, v2}, LX/0JU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JU;->DATA_STALE_IN_APP:LX/0JU;

    .line 39495
    new-instance v0, LX/0JU;

    const-string v1, "EXIT_VIDEO_HOME"

    const-string v2, "exit_video_home"

    invoke-direct {v0, v1, v7, v2}, LX/0JU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JU;->EXIT_VIDEO_HOME:LX/0JU;

    .line 39496
    new-instance v0, LX/0JU;

    const-string v1, "FOREGROUNDING"

    const-string v2, "foregrounding"

    invoke-direct {v0, v1, v8, v2}, LX/0JU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JU;->FOREGROUNDING:LX/0JU;

    .line 39497
    new-instance v0, LX/0JU;

    const-string v1, "INITIAL"

    const/4 v2, 0x5

    const-string v3, "initial"

    invoke-direct {v0, v1, v2, v3}, LX/0JU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JU;->INITIAL:LX/0JU;

    .line 39498
    new-instance v0, LX/0JU;

    const-string v1, "MQTT"

    const/4 v2, 0x6

    const-string v3, "mqtt"

    invoke-direct {v0, v1, v2, v3}, LX/0JU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JU;->MQTT:LX/0JU;

    .line 39499
    const/4 v0, 0x7

    new-array v0, v0, [LX/0JU;

    sget-object v1, LX/0JU;->BADGE_FETCH:LX/0JU;

    aput-object v1, v0, v4

    sget-object v1, LX/0JU;->DATA_STALE_FOREGROUNDING:LX/0JU;

    aput-object v1, v0, v5

    sget-object v1, LX/0JU;->DATA_STALE_IN_APP:LX/0JU;

    aput-object v1, v0, v6

    sget-object v1, LX/0JU;->EXIT_VIDEO_HOME:LX/0JU;

    aput-object v1, v0, v7

    sget-object v1, LX/0JU;->FOREGROUNDING:LX/0JU;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0JU;->INITIAL:LX/0JU;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0JU;->MQTT:LX/0JU;

    aput-object v2, v0, v1

    sput-object v0, LX/0JU;->$VALUES:[LX/0JU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39500
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39501
    iput-object p3, p0, LX/0JU;->value:Ljava/lang/String;

    .line 39502
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JU;
    .locals 1

    .prologue
    .line 39503
    const-class v0, LX/0JU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JU;

    return-object v0
.end method

.method public static values()[LX/0JU;
    .locals 1

    .prologue
    .line 39504
    sget-object v0, LX/0JU;->$VALUES:[LX/0JU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JU;

    return-object v0
.end method
