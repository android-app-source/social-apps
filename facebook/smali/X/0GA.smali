.class public final LX/0GA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/0GB;

.field private final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/0G9;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/0G9;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0GB;)V
    .locals 1

    .prologue
    .line 34316
    iput-object p1, p0, LX/0GA;->a:LX/0GB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34317
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/0GA;->b:Ljava/util/Queue;

    .line 34318
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/0GA;->c:Ljava/util/Queue;

    .line 34319
    return-void
.end method

.method private static a(Ljava/util/Queue;Landroid/net/Uri;)LX/0G9;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "LX/0G9;",
            ">;",
            "Landroid/net/Uri;",
            ")",
            "LX/0G9;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 34320
    invoke-interface {p0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0G9;

    .line 34321
    iget-object v2, v0, LX/0G9;->b:Landroid/net/Uri;

    invoke-virtual {v2, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34322
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/net/Uri;)LX/0G9;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 34323
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0GA;->b:Ljava/util/Queue;

    invoke-static {v0, p1}, LX/0GA;->a(Ljava/util/Queue;Landroid/net/Uri;)LX/0G9;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 34324
    if-eqz v0, :cond_0

    .line 34325
    :goto_0
    monitor-exit p0

    return-object v0

    .line 34326
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0GA;->c:Ljava/util/Queue;

    invoke-static {v0, p1}, LX/0GA;->a(Ljava/util/Queue;Landroid/net/Uri;)LX/0G9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 34327
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34328
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0GA;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/net/Uri;[BI)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 34329
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "init.m4a"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "init.m4v"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 34330
    :cond_0
    :goto_0
    if-eqz v1, :cond_3

    iget-object v0, p0, LX/0GA;->c:Ljava/util/Queue;

    .line 34331
    :goto_1
    invoke-static {v0, p1}, LX/0GA;->a(Ljava/util/Queue;Landroid/net/Uri;)LX/0G9;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 34332
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34333
    :cond_1
    :goto_2
    monitor-exit p0

    return-void

    :cond_2
    move v1, v0

    .line 34334
    goto :goto_0

    .line 34335
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/0GA;->b:Ljava/util/Queue;

    goto :goto_1

    .line 34336
    :cond_4
    invoke-static {p2, p3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    .line 34337
    new-instance v3, LX/0G9;

    invoke-direct {v3, v2, p1}, LX/0G9;-><init>([BLandroid/net/Uri;)V

    .line 34338
    invoke-interface {v0, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 34339
    if-nez v1, :cond_1

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v1

    iget-object v2, p0, LX/0GA;->a:LX/0GB;

    iget-object v2, v2, LX/0GB;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 34340
    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 34341
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34342
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/0GA;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34343
    monitor-exit p0

    return-void

    .line 34344
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
