.class public final LX/0LD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0LB;


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 43106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43107
    iput-wide p1, p0, LX/0LD;->a:J

    .line 43108
    iput-wide p3, p0, LX/0LD;->b:J

    .line 43109
    return-void
.end method


# virtual methods
.method public final a([J)[J
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 43110
    invoke-virtual {p0, p1}, LX/0LD;->b([J)[J

    move-result-object v0

    .line 43111
    const/4 v1, 0x0

    aget-wide v2, v0, v1

    div-long/2addr v2, v4

    aput-wide v2, v0, v1

    .line 43112
    const/4 v1, 0x1

    aget-wide v2, v0, v1

    div-long/2addr v2, v4

    aput-wide v2, v0, v1

    .line 43113
    return-object v0
.end method

.method public final b([J)[J
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 43101
    if-eqz p1, :cond_0

    array-length v0, p1

    if-ge v0, v1, :cond_1

    .line 43102
    :cond_0
    new-array p1, v1, [J

    .line 43103
    :cond_1
    const/4 v0, 0x0

    iget-wide v2, p0, LX/0LD;->a:J

    aput-wide v2, p1, v0

    .line 43104
    const/4 v0, 0x1

    iget-wide v2, p0, LX/0LD;->b:J

    aput-wide v2, p1, v0

    .line 43105
    return-object p1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43095
    if-ne p1, p0, :cond_1

    .line 43096
    :cond_0
    :goto_0
    return v0

    .line 43097
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 43098
    goto :goto_0

    .line 43099
    :cond_3
    check-cast p1, LX/0LD;

    .line 43100
    iget-wide v2, p1, LX/0LD;->a:J

    iget-wide v4, p0, LX/0LD;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-wide v2, p1, LX/0LD;->b:J

    iget-wide v4, p0, LX/0LD;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 43092
    iget-wide v0, p0, LX/0LD;->a:J

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 43093
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, LX/0LD;->b:J

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 43094
    return v0
.end method
