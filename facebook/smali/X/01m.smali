.class public LX/01m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "StringFormatUse",
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation


# static fields
.field public static a:LX/03G;

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/debug/log/BLogLevelCallback;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/03H;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 5459
    sget-object v0, LX/03F;->a:LX/03F;

    move-object v0, v0

    .line 5460
    sput-object v0, LX/01m;->a:LX/03G;

    .line 5461
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/01m;->b:Ljava/util/List;

    .line 5462
    sget-object v0, LX/03H;->b:LX/03H;

    move-object v0, v0

    .line 5463
    sput-object v0, LX/01m;->c:LX/03H;

    .line 5464
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->a(I)V

    .line 5465
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-static {v0}, LX/03J;->a(LX/03G;)V

    .line 5466
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 5468
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0}, LX/03G;->b()I

    move-result v0

    return v0
.end method

.method public static declared-synchronized a(I)V
    .locals 3

    .prologue
    .line 5469
    const-class v1, LX/01m;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0}, LX/03G;->a(I)V

    .line 5470
    sget-object v0, LX/01m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03E;

    .line 5471
    invoke-static {p0}, Lcom/facebook/xplat/fbglog/FbGlog;->setLogLevel(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5472
    goto :goto_0

    .line 5473
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 5474
    :cond_0
    monitor-exit v1

    return-void
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 5475
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5476
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1, p2}, LX/03G;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 5477
    :cond_0
    return-void
.end method

.method public static declared-synchronized a(LX/03E;)V
    .locals 2

    .prologue
    .line 5478
    const-class v1, LX/01m;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/01m;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5479
    monitor-exit v1

    return-void

    .line 5480
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5481
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5482
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1}, LX/03H;->a(Ljava/lang/String;)V

    .line 5483
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5484
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5485
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5486
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1, p2}, LX/03H;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5487
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5488
    :cond_0
    return-void
.end method

.method public static varargs a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5489
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5490
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1}, LX/03H;->a(Ljava/lang/String;)V

    .line 5491
    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5492
    :cond_0
    return-void
.end method

.method public static varargs a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Throwable;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5493
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5494
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p2, p1}, LX/03H;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5495
    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, LX/01m;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5496
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5497
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5498
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1}, LX/03H;->a(Ljava/lang/String;)V

    .line 5499
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5500
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 5501
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5502
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1, p2}, LX/03H;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5503
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1, p2}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5504
    :cond_0
    return-void
.end method

.method public static varargs a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 5505
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5506
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1}, LX/03H;->a(Ljava/lang/String;)V

    .line 5507
    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/01m;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5508
    :cond_0
    return-void
.end method

.method public static varargs a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 5509
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5510
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p2, p1}, LX/03H;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5511
    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, LX/01m;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5512
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5513
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5514
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1}, LX/03H;->a(Ljava/lang/String;)V

    .line 5515
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5516
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5517
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5518
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1, p2}, LX/03H;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5519
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5520
    :cond_0
    return-void
.end method

.method public static varargs b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5521
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5522
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1}, LX/03H;->a(Ljava/lang/String;)V

    .line 5523
    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 5524
    :cond_0
    return-void
.end method

.method public static varargs b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Throwable;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5451
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5452
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p2, p1}, LX/03H;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5453
    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, LX/01m;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5454
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5455
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5456
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1}, LX/03H;->a(Ljava/lang/String;)V

    .line 5457
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5458
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 5388
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5389
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1, p2}, LX/03H;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5390
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1, p2}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5391
    :cond_0
    return-void
.end method

.method public static varargs b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 5392
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5393
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1}, LX/03H;->a(Ljava/lang/String;)V

    .line 5394
    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/01m;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 5395
    :cond_0
    return-void
.end method

.method public static varargs b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 5396
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5397
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p2, p1}, LX/03H;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5398
    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, LX/01m;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5399
    :cond_0
    return-void
.end method

.method public static b(I)Z
    .locals 1

    .prologue
    .line 5400
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0}, LX/03G;->b(I)Z

    move-result v0

    return v0
.end method

.method public static c(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5401
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5402
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1}, LX/03H;->a(Ljava/lang/String;)V

    .line 5403
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LX/03G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 5404
    :cond_0
    return-void
.end method

.method public static c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5405
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5406
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1, p2}, LX/03H;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5407
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, LX/03G;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5408
    :cond_0
    return-void
.end method

.method public static varargs c(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5409
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5410
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1}, LX/03H;->a(Ljava/lang/String;)V

    .line 5411
    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 5412
    :cond_0
    return-void
.end method

.method public static varargs c(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Throwable;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5413
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5414
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p2, p1}, LX/03H;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5415
    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, LX/01m;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5416
    :cond_0
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5417
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5418
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1}, LX/03H;->a(Ljava/lang/String;)V

    .line 5419
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1}, LX/03G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 5420
    :cond_0
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 5421
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5422
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1, p2}, LX/03H;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5423
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1, p2}, LX/03G;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5424
    :cond_0
    return-void
.end method

.method public static varargs c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 5425
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5426
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p1}, LX/03H;->a(Ljava/lang/String;)V

    .line 5427
    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/01m;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 5428
    :cond_0
    return-void
.end method

.method public static varargs c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 5429
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5430
    sget-object v0, LX/01m;->c:LX/03H;

    invoke-virtual {v0, p2, p1}, LX/03H;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5431
    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, LX/01m;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5432
    :cond_0
    return-void
.end method

.method private static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5433
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5434
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5435
    :cond_0
    return-void
.end method

.method private static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 5436
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5437
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1, p2}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5438
    :cond_0
    return-void
.end method

.method private static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5439
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5440
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5441
    :cond_0
    return-void
.end method

.method private static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 5442
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5443
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1, p2}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5444
    :cond_0
    return-void
.end method

.method private static f(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5445
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5446
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1}, LX/03G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 5447
    :cond_0
    return-void
.end method

.method private static f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 5448
    sget-object v0, LX/01m;->a:LX/03G;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5449
    sget-object v0, LX/01m;->a:LX/03G;

    invoke-interface {v0, p0, p1, p2}, LX/03G;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5450
    :cond_0
    return-void
.end method
