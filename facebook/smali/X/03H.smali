.class public LX/03H;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:LX/03H;


# instance fields
.field public a:[LX/03I;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9881
    new-instance v0, LX/03H;

    invoke-direct {v0}, LX/03H;-><init>()V

    sput-object v0, LX/03H;->b:LX/03H;

    return-void
.end method

.method private constructor <init>()V
    .locals 15

    .prologue
    const/4 v3, 0x3

    .line 9882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9883
    new-array v0, v3, [LX/03I;

    iput-object v0, p0, LX/03H;->a:[LX/03I;

    .line 9884
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 9885
    iget-object v1, p0, LX/03H;->a:[LX/03I;

    const/4 v11, 0x0

    const-wide/16 v6, 0x0

    .line 9886
    new-instance v5, LX/03I;

    const/4 v10, 0x0

    move-wide v8, v6

    move-object v12, v11

    move-wide v13, v6

    invoke-direct/range {v5 .. v14}, LX/03I;-><init>(JJILjava/lang/String;Ljava/lang/String;J)V

    move-object v2, v5

    .line 9887
    aput-object v2, v1, v0

    .line 9888
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9889
    :cond_0
    return-void
.end method

.method private declared-synchronized a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 19
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 9890
    monitor-enter p0

    const/4 v4, 0x0

    .line 9891
    const/4 v5, 0x0

    .line 9892
    const/4 v3, 0x0

    .line 9893
    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    const-wide/32 v8, 0xf4240

    div-long v8, v6, v8

    .line 9894
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 9895
    move-object/from16 v0, p0

    iget-object v7, v0, LX/03H;->a:[LX/03I;

    array-length v12, v7

    const/4 v2, 0x0

    move v6, v2

    move-object v2, v3

    :goto_0
    if-ge v6, v12, :cond_a

    aget-object v3, v7, v6

    .line 9896
    iget-wide v14, v3, LX/03I;->a:J

    cmp-long v13, p1, v14

    if-nez v13, :cond_1

    .line 9897
    :goto_1
    if-eqz v3, :cond_8

    .line 9898
    iget-wide v4, v3, LX/03I;->b:J

    sub-long v4, v8, v4

    const-wide/16 v6, 0x2710

    cmp-long v2, v4, v6

    if-gtz v2, :cond_7

    .line 9899
    iget v2, v3, LX/03I;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v3, LX/03I;->c:I

    .line 9900
    :goto_2
    iput-wide v8, v3, LX/03I;->b:J

    .line 9901
    iput-wide v10, v3, LX/03I;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9902
    :cond_0
    :goto_3
    monitor-exit p0

    return-void

    .line 9903
    :cond_1
    :try_start_1
    iget v13, v3, LX/03I;->c:I

    if-eqz v13, :cond_2

    iget-wide v14, v3, LX/03I;->b:J

    sub-long v14, v8, v14

    const-wide/16 v16, 0x2710

    cmp-long v13, v14, v16

    if-lez v13, :cond_4

    .line 9904
    :cond_2
    if-eqz v5, :cond_3

    iget-wide v14, v3, LX/03I;->b:J

    iget-wide v0, v5, LX/03I;->b:J

    move-wide/from16 v16, v0

    cmp-long v13, v14, v16

    if-gez v13, :cond_4

    :cond_3
    move-object v5, v3

    .line 9905
    :cond_4
    iget v13, v3, LX/03I;->c:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_6

    if-eqz v2, :cond_5

    iget-wide v14, v3, LX/03I;->b:J

    iget-wide v0, v2, LX/03I;->b:J

    move-wide/from16 v16, v0

    cmp-long v13, v14, v16

    if-gez v13, :cond_6

    :cond_5
    move-object v2, v3

    .line 9906
    :cond_6
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_0

    .line 9907
    :cond_7
    const/4 v2, 0x1

    iput v2, v3, LX/03I;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 9908
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 9909
    :cond_8
    if-eqz v5, :cond_9

    .line 9910
    :try_start_2
    move-wide/from16 v0, p1

    iput-wide v0, v5, LX/03I;->a:J

    .line 9911
    iput-wide v8, v5, LX/03I;->b:J

    .line 9912
    const/4 v2, 0x1

    iput v2, v5, LX/03I;->c:I

    .line 9913
    move-object/from16 v0, p3

    iput-object v0, v5, LX/03I;->d:Ljava/lang/String;

    .line 9914
    move-object/from16 v0, p4

    iput-object v0, v5, LX/03I;->e:Ljava/lang/String;

    .line 9915
    iput-wide v10, v5, LX/03I;->f:J

    goto :goto_3

    .line 9916
    :cond_9
    if-eqz v2, :cond_0

    .line 9917
    move-wide/from16 v0, p1

    iput-wide v0, v2, LX/03I;->a:J

    .line 9918
    iput-wide v8, v2, LX/03I;->b:J

    .line 9919
    const/4 v3, 0x1

    iput v3, v2, LX/03I;->c:I

    .line 9920
    move-object/from16 v0, p3

    iput-object v0, v2, LX/03I;->d:Ljava/lang/String;

    .line 9921
    move-object/from16 v0, p4

    iput-object v0, v2, LX/03I;->e:Ljava/lang/String;

    .line 9922
    iput-wide v10, v2, LX/03I;->f:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :cond_a
    move-object v3, v4

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 9923
    if-nez p1, :cond_0

    .line 9924
    :goto_0
    return-void

    .line 9925
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    .line 9926
    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p1, v2}, LX/03H;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 9927
    if-nez p1, :cond_0

    .line 9928
    :goto_0
    return-void

    .line 9929
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    .line 9930
    if-nez p2, :cond_2

    const/4 v2, 0x0

    .line 9931
    :goto_1
    if-eqz v2, :cond_1

    .line 9932
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    int-to-long v4, v3

    xor-long/2addr v0, v4

    .line 9933
    :cond_1
    invoke-direct {p0, v0, v1, p1, v2}, LX/03H;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 9934
    :cond_2
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method
