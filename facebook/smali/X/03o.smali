.class public final LX/03o;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/os/IBinder;

.field public b:Landroid/os/Message;

.field public final synthetic c:LX/000;


# direct methods
.method public constructor <init>(LX/000;Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 10788
    iput-object p1, p0, LX/03o;->c:LX/000;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10789
    :try_start_0
    iget-object v0, p1, LX/000;->H:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    iput-object v0, p0, LX/03o;->a:Landroid/os/IBinder;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10790
    iget-object v0, p0, LX/03o;->a:Landroid/os/IBinder;

    if-nez v0, :cond_0

    .line 10791
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "activity not bound"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10792
    :catch_0
    move-exception v0

    .line 10793
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 10794
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)I
    .locals 2

    .prologue
    .line 10795
    iget v0, p1, Landroid/os/Message;->what:I

    iget-object v1, p0, LX/03o;->c:LX/000;

    iget v1, v1, LX/000;->h:I

    if-ne v0, v1, :cond_0

    invoke-static {p1}, LX/01t;->c(Landroid/os/Message;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LX/03o;->c:LX/000;

    iget-object v1, v1, LX/000;->o:Landroid/os/Handler;

    if-ne v0, v1, :cond_0

    .line 10796
    :try_start_0
    iget-object v0, p0, LX/03o;->c:LX/000;

    iget-object v0, v0, LX/000;->F:Ljava/lang/reflect/Field;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10797
    iget-object v1, p0, LX/03o;->a:Landroid/os/IBinder;

    if-ne v0, v1, :cond_0

    .line 10798
    iput-object p1, p0, LX/03o;->b:Landroid/os/Message;

    .line 10799
    const/4 v0, 0x3

    .line 10800
    :goto_0
    return v0

    .line 10801
    :catch_0
    move-exception v0

    .line 10802
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 10803
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
