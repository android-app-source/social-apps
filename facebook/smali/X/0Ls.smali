.class public final LX/0Ls;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0BX;


# instance fields
.field private final a:LX/0M9;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0M9;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44789
    iput-object p1, p0, LX/0Ls;->a:LX/0M9;

    .line 44790
    iput-object p2, p0, LX/0Ls;->b:Ljava/lang/String;

    .line 44791
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 44787
    const/4 v0, 0x0

    return v0
.end method

.method public final a(J)I
    .locals 1

    .prologue
    .line 44780
    iget-object v0, p0, LX/0Ls;->a:LX/0M9;

    iget v0, v0, LX/0M9;->a:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final a(JJ)I
    .locals 1

    .prologue
    .line 44786
    iget-object v0, p0, LX/0Ls;->a:LX/0M9;

    invoke-virtual {v0, p1, p2}, LX/0M9;->a(J)I

    move-result v0

    return v0
.end method

.method public final a(I)J
    .locals 2

    .prologue
    .line 44785
    iget-object v0, p0, LX/0Ls;->a:LX/0M9;

    iget-object v0, v0, LX/0M9;->e:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public final a(IJ)J
    .locals 2

    .prologue
    .line 44784
    iget-object v0, p0, LX/0Ls;->a:LX/0M9;

    iget-object v0, v0, LX/0M9;->d:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public final b(I)LX/0Au;
    .locals 8

    .prologue
    .line 44783
    new-instance v1, LX/0Au;

    iget-object v2, p0, LX/0Ls;->b:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v0, p0, LX/0Ls;->a:LX/0M9;

    iget-object v0, v0, LX/0M9;->c:[J

    aget-wide v4, v0, p1

    iget-object v0, p0, LX/0Ls;->a:LX/0M9;

    iget-object v0, v0, LX/0M9;->b:[I

    aget v0, v0, p1

    int-to-long v6, v0

    invoke-direct/range {v1 .. v7}, LX/0Au;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    return-object v1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 44782
    const/4 v0, 0x1

    return v0
.end method

.method public final c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44781
    const/4 v0, 0x0

    return-object v0
.end method
