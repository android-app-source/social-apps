.class public final LX/0Lg;
.super LX/0LP;
.source ""

# interfaces
.implements LX/0LT;


# instance fields
.field private final a:LX/0LV;

.field public h:LX/0L4;

.field public i:LX/0Lz;

.field public j:LX/0M8;

.field private volatile k:I

.field private volatile l:Z


# direct methods
.method public constructor <init>(LX/0G6;LX/0OA;ILX/0AR;LX/0LV;I)V
    .locals 7

    .prologue
    .line 44080
    const/4 v3, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, LX/0LP;-><init>(LX/0G6;LX/0OA;IILX/0AR;I)V

    .line 44081
    iput-object p5, p0, LX/0Lg;->a:LX/0LV;

    .line 44082
    return-void
.end method


# virtual methods
.method public final a(LX/0MA;IZ)I
    .locals 2

    .prologue
    .line 44083
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected sample data in initialization chunk"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(JIII[B)V
    .locals 2

    .prologue
    .line 44084
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected sample data in initialization chunk"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/0L4;)V
    .locals 0

    .prologue
    .line 44085
    iput-object p1, p0, LX/0Lg;->h:LX/0L4;

    .line 44086
    return-void
.end method

.method public final a(LX/0Lz;)V
    .locals 0

    .prologue
    .line 44076
    iput-object p1, p0, LX/0Lg;->i:LX/0Lz;

    .line 44077
    return-void
.end method

.method public final a(LX/0M8;)V
    .locals 0

    .prologue
    .line 44078
    iput-object p1, p0, LX/0Lg;->j:LX/0M8;

    .line 44079
    return-void
.end method

.method public final a(LX/0Oj;I)V
    .locals 2

    .prologue
    .line 44060
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected sample data in initialization chunk"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 44059
    iget v0, p0, LX/0Lg;->k:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 44074
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Lg;->l:Z

    .line 44075
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 44061
    iget-boolean v0, p0, LX/0Lg;->l:Z

    return v0
.end method

.method public final h()V
    .locals 6

    .prologue
    .line 44062
    iget-object v0, p0, LX/0LP;->e:LX/0OA;

    iget v1, p0, LX/0Lg;->k:I

    invoke-static {v0, v1}, LX/08x;->a(LX/0OA;I)LX/0OA;

    move-result-object v4

    .line 44063
    :try_start_0
    new-instance v0, LX/0MB;

    iget-object v1, p0, LX/0LP;->g:LX/0G6;

    iget-wide v2, v4, LX/0OA;->c:J

    iget-object v5, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v5, v4}, LX/0G6;->a(LX/0OA;)J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, LX/0MB;-><init>(LX/0G6;JJ)V

    .line 44064
    iget v1, p0, LX/0Lg;->k:I

    if-nez v1, :cond_0

    .line 44065
    iget-object v1, p0, LX/0Lg;->a:LX/0LV;

    invoke-virtual {v1, p0}, LX/0LV;->a(LX/0LT;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 44066
    :cond_0
    const/4 v1, 0x0

    .line 44067
    :goto_0
    if-nez v1, :cond_1

    :try_start_1
    iget-boolean v1, p0, LX/0Lg;->l:Z

    if-nez v1, :cond_1

    .line 44068
    iget-object v1, p0, LX/0Lg;->a:LX/0LV;

    invoke-virtual {v1, v0}, LX/0LV;->a(LX/0MA;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_0

    .line 44069
    :cond_1
    :try_start_2
    invoke-interface {v0}, LX/0MA;->c()J

    move-result-wide v0

    iget-object v2, p0, LX/0LP;->e:LX/0OA;

    iget-wide v2, v2, LX/0OA;->c:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, LX/0Lg;->k:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 44070
    iget-object v0, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v0}, LX/0G6;->a()V

    .line 44071
    return-void

    .line 44072
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-interface {v0}, LX/0MA;->c()J

    move-result-wide v2

    iget-object v0, p0, LX/0LP;->e:LX/0OA;

    iget-wide v4, v0, LX/0OA;->c:J

    sub-long/2addr v2, v4

    long-to-int v0, v2

    iput v0, p0, LX/0Lg;->k:I

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 44073
    :catchall_1
    move-exception v0

    iget-object v1, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v1}, LX/0G6;->a()V

    throw v0
.end method
