.class public final LX/0FM;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic this$0:LX/0FN;


# direct methods
.method public constructor <init>(LX/0FN;)V
    .locals 0

    .prologue
    .line 33040
    iput-object p1, p0, LX/0FM;->this$0:LX/0FN;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/16 v6, 0x12c

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 33041
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 33042
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 33043
    :cond_0
    :goto_0
    return-void

    .line 33044
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    packed-switch v0, :pswitch_data_1

    .line 33045
    const-string v0, "unknown"

    .line 33046
    :goto_1
    const-string v1, "[c] received optimization-done message (result: %s)"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33047
    iget-object v0, p0, LX/0FM;->this$0:LX/0FN;

    iget-object v0, v0, LX/0FN;->mContext:Landroid/content/Context;

    iget-object v1, p0, LX/0FM;->this$0:LX/0FN;

    const v2, -0x4328ea6

    invoke-static {v0, v1, v2}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    .line 33048
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_0

    .line 33049
    :pswitch_1
    iget-object v0, p0, LX/0FM;->this$0:LX/0FN;

    .line 33050
    iget-object v1, v0, LX/0FN;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 33051
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_4

    .line 33052
    :try_start_0
    iget-object v2, v0, LX/0FN;->mRpi:Landroid/app/ActivityManager$RunningAppProcessInfo;

    if-nez v2, :cond_1

    .line 33053
    new-instance v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    invoke-direct {v2}, Landroid/app/ActivityManager$RunningAppProcessInfo;-><init>()V

    .line 33054
    iput-object v2, v0, LX/0FN;->mRpi:Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 33055
    :cond_1
    iget-object v2, v0, LX/0FN;->mRpi:Landroid/app/ActivityManager$RunningAppProcessInfo;

    invoke-static {v2}, Landroid/app/ActivityManager;->getMyMemoryState(Landroid/app/ActivityManager$RunningAppProcessInfo;)V

    .line 33056
    iget-object v2, v0, LX/0FN;->mRpi:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v2, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    move v1, v2
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33057
    :goto_2
    move v0, v1

    .line 33058
    const-string v1, "[c] current importance: %s threshold: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33059
    if-lt v0, v6, :cond_2

    .line 33060
    const-string v1, "[c] low importance: %s: restarting ourselves"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33061
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    const/16 v1, 0x9

    invoke-static {v0, v1}, Landroid/os/Process;->sendSignal(II)V

    .line 33062
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "somehow survived SIGKILL"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 33063
    :pswitch_2
    const-string v0, "success"

    goto :goto_1

    .line 33064
    :pswitch_3
    const-string v0, "failed"

    goto :goto_1

    .line 33065
    :pswitch_4
    const-string v0, "interrupted due to service shutdown"

    goto :goto_1

    .line 33066
    :cond_2
    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 33067
    const-string v0, "[c] importance unknown: not scheduling further checks"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 33068
    :cond_3
    const-string v0, "[c] scheduling another importance check in %sms"

    new-array v1, v5, [Ljava/lang/Object;

    const/16 v2, 0x3e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33069
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LX/0FM;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 33070
    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, LX/0FM;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 33071
    :catch_0
    move-exception v2

    .line 33072
    const-string v3, "getMyMemoryState failed: falling back to legacy process list API"

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {v2, v3, p1}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33073
    :cond_4
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    .line 33074
    if-eqz v1, :cond_6

    .line 33075
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    .line 33076
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 33077
    iget p1, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne p1, v2, :cond_5

    .line 33078
    iget v1, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    goto/16 :goto_2

    .line 33079
    :cond_6
    const/4 v1, -0x1

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
