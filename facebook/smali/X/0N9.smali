.class public final LX/0N9;
.super LX/0N4;
.source ""


# static fields
.field private static final b:[D


# instance fields
.field private c:Z

.field private d:J

.field private final e:[Z

.field private final f:LX/0N8;

.field private g:Z

.field private h:J

.field private i:J

.field private j:Z

.field private k:Z

.field private l:J

.field private m:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49126
    const/16 v0, 0x8

    new-array v0, v0, [D

    fill-array-data v0, :array_0

    sput-object v0, LX/0N9;->b:[D

    return-void

    :array_0
    .array-data 8
        0x4037f9dcb5112287L    # 23.976023976023978
        0x4038000000000000L    # 24.0
        0x4039000000000000L    # 25.0
        0x403df853e2556b28L    # 29.97002997002997
        0x403e000000000000L    # 30.0
        0x4049000000000000L    # 50.0
        0x404df853e2556b28L    # 59.94005994005994
        0x404e000000000000L    # 60.0
    .end array-data
.end method

.method public constructor <init>(LX/0LS;)V
    .locals 2

    .prologue
    .line 49127
    invoke-direct {p0, p1}, LX/0N4;-><init>(LX/0LS;)V

    .line 49128
    const/4 v0, 0x4

    new-array v0, v0, [Z

    iput-object v0, p0, LX/0N9;->e:[Z

    .line 49129
    new-instance v0, LX/0N8;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/0N8;-><init>(I)V

    iput-object v0, p0, LX/0N9;->f:LX/0N8;

    .line 49130
    return-void
.end method

.method private static a(LX/0N8;)Landroid/util/Pair;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0N8;",
            ")",
            "Landroid/util/Pair",
            "<",
            "LX/0L4;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x7

    const/4 v2, -0x1

    .line 49131
    iget-object v0, p0, LX/0N8;->c:[B

    iget v1, p0, LX/0N8;->a:I

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v11

    .line 49132
    const/4 v0, 0x4

    aget-byte v0, v11, v0

    and-int/lit16 v0, v0, 0xff

    .line 49133
    const/4 v1, 0x5

    aget-byte v1, v11, v1

    and-int/lit16 v1, v1, 0xff

    .line 49134
    const/4 v3, 0x6

    aget-byte v3, v11, v3

    and-int/lit16 v3, v3, 0xff

    .line 49135
    shl-int/lit8 v0, v0, 0x4

    shr-int/lit8 v4, v1, 0x4

    or-int v6, v0, v4

    .line 49136
    and-int/lit8 v0, v1, 0xf

    shl-int/lit8 v0, v0, 0x8

    or-int v7, v0, v3

    .line 49137
    const/high16 v10, 0x3f800000    # 1.0f

    .line 49138
    aget-byte v0, v11, v12

    and-int/lit16 v0, v0, 0xf0

    shr-int/lit8 v0, v0, 0x4

    .line 49139
    packed-switch v0, :pswitch_data_0

    .line 49140
    :goto_0
    const/4 v0, 0x0

    const-string v1, "video/mpeg2"

    const-wide/16 v4, -0x1

    invoke-static {v11}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    move v3, v2

    move v9, v2

    invoke-static/range {v0 .. v10}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)LX/0L4;

    move-result-object v2

    .line 49141
    const-wide/16 v0, 0x0

    .line 49142
    aget-byte v3, v11, v12

    and-int/lit8 v3, v3, 0xf

    add-int/lit8 v3, v3, -0x1

    .line 49143
    if-ltz v3, :cond_1

    sget-object v4, LX/0N9;->b:[D

    array-length v4, v4

    if-ge v3, v4, :cond_1

    .line 49144
    sget-object v0, LX/0N9;->b:[D

    aget-wide v0, v0, v3

    .line 49145
    iget v3, p0, LX/0N8;->b:I

    .line 49146
    add-int/lit8 v4, v3, 0x9

    aget-byte v4, v11, v4

    and-int/lit8 v4, v4, 0x60

    shr-int/lit8 v4, v4, 0x5

    .line 49147
    add-int/lit8 v3, v3, 0x9

    aget-byte v3, v11, v3

    and-int/lit8 v3, v3, 0x1f

    .line 49148
    if-eq v4, v3, :cond_0

    .line 49149
    int-to-double v4, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-double/2addr v4, v6

    add-int/lit8 v3, v3, 0x1

    int-to-double v6, v3

    div-double/2addr v4, v6

    mul-double/2addr v0, v4

    .line 49150
    :cond_0
    const-wide v4, 0x412e848000000000L    # 1000000.0

    div-double v0, v4, v0

    double-to-long v0, v0

    .line 49151
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 49152
    :pswitch_0
    mul-int/lit8 v0, v7, 0x4

    int-to-float v0, v0

    mul-int/lit8 v1, v6, 0x3

    int-to-float v1, v1

    div-float v10, v0, v1

    .line 49153
    goto :goto_0

    .line 49154
    :pswitch_1
    mul-int/lit8 v0, v7, 0x10

    int-to-float v0, v0

    mul-int/lit8 v1, v6, 0x9

    int-to-float v1, v1

    div-float v10, v0, v1

    .line 49155
    goto :goto_0

    .line 49156
    :pswitch_2
    mul-int/lit8 v0, v7, 0x79

    int-to-float v0, v0

    mul-int/lit8 v1, v6, 0x64

    int-to-float v1, v1

    div-float v10, v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 49157
    iget-object v0, p0, LX/0N9;->e:[Z

    invoke-static {v0}, LX/0Oh;->a([Z)V

    .line 49158
    iget-object v0, p0, LX/0N9;->f:LX/0N8;

    const/4 v2, 0x0

    .line 49159
    iput-boolean v2, v0, LX/0N8;->d:Z

    .line 49160
    iput v2, v0, LX/0N8;->a:I

    .line 49161
    iput v2, v0, LX/0N8;->b:I

    .line 49162
    iput-boolean v1, p0, LX/0N9;->j:Z

    .line 49163
    iput-boolean v1, p0, LX/0N9;->g:Z

    .line 49164
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0N9;->h:J

    .line 49165
    return-void
.end method

.method public final a(JZ)V
    .locals 3

    .prologue
    .line 49166
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/0N9;->j:Z

    .line 49167
    iget-boolean v0, p0, LX/0N9;->j:Z

    if-eqz v0, :cond_0

    .line 49168
    iput-wide p1, p0, LX/0N9;->i:J

    .line 49169
    :cond_0
    return-void

    .line 49170
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0Oj;)V
    .locals 12

    .prologue
    .line 49171
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 49172
    iget v0, p1, LX/0Oj;->b:I

    move v0, v0

    .line 49173
    iget v1, p1, LX/0Oj;->c:I

    move v9, v1

    .line 49174
    iget-object v10, p1, LX/0Oj;->a:[B

    .line 49175
    iget-wide v2, p0, LX/0N9;->h:J

    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0N9;->h:J

    .line 49176
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v2

    invoke-interface {v1, p1, v2}, LX/0LS;->a(LX/0Oj;I)V

    move v1, v0

    .line 49177
    :goto_0
    iget-object v2, p0, LX/0N9;->e:[Z

    invoke-static {v10, v0, v9, v2}, LX/0Oh;->a([BII[Z)I

    move-result v8

    .line 49178
    if-ne v8, v9, :cond_1

    .line 49179
    iget-boolean v0, p0, LX/0N9;->c:Z

    if-nez v0, :cond_0

    .line 49180
    iget-object v0, p0, LX/0N9;->f:LX/0N8;

    invoke-virtual {v0, v10, v1, v9}, LX/0N8;->a([BII)V

    .line 49181
    :cond_0
    return-void

    .line 49182
    :cond_1
    iget-object v0, p1, LX/0Oj;->a:[B

    add-int/lit8 v2, v8, 0x3

    aget-byte v0, v0, v2

    and-int/lit16 v11, v0, 0xff

    .line 49183
    iget-boolean v0, p0, LX/0N9;->c:Z

    if-nez v0, :cond_4

    .line 49184
    sub-int v0, v8, v1

    .line 49185
    if-lez v0, :cond_2

    .line 49186
    iget-object v2, p0, LX/0N9;->f:LX/0N8;

    invoke-virtual {v2, v10, v1, v8}, LX/0N8;->a([BII)V

    .line 49187
    :cond_2
    if-gez v0, :cond_8

    neg-int v0, v0

    .line 49188
    :goto_1
    iget-object v1, p0, LX/0N9;->f:LX/0N8;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 49189
    iget-boolean v4, v1, LX/0N8;->d:Z

    if-eqz v4, :cond_d

    .line 49190
    iget v4, v1, LX/0N8;->b:I

    if-nez v4, :cond_c

    const/16 v4, 0xb5

    if-ne v11, v4, :cond_c

    .line 49191
    iget v2, v1, LX/0N8;->a:I

    iput v2, v1, LX/0N8;->b:I

    :cond_3
    :goto_2
    move v2, v3

    .line 49192
    :goto_3
    move v0, v2

    .line 49193
    if-eqz v0, :cond_4

    .line 49194
    iget-object v0, p0, LX/0N9;->f:LX/0N8;

    invoke-static {v0}, LX/0N9;->a(LX/0N8;)Landroid/util/Pair;

    move-result-object v1

    .line 49195
    iget-object v2, p0, LX/0N4;->a:LX/0LS;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/0L4;

    invoke-interface {v2, v0}, LX/0LS;->a(LX/0L4;)V

    .line 49196
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/0N9;->d:J

    .line 49197
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0N9;->c:Z

    .line 49198
    :cond_4
    iget-boolean v0, p0, LX/0N9;->c:Z

    if-eqz v0, :cond_7

    const/16 v0, 0xb8

    if-eq v11, v0, :cond_5

    if-nez v11, :cond_7

    .line 49199
    :cond_5
    sub-int v6, v9, v8

    .line 49200
    iget-boolean v0, p0, LX/0N9;->g:Z

    if-eqz v0, :cond_6

    .line 49201
    iget-boolean v0, p0, LX/0N9;->k:Z

    if-eqz v0, :cond_9

    const/4 v4, 0x1

    .line 49202
    :goto_4
    iget-wide v0, p0, LX/0N9;->h:J

    iget-wide v2, p0, LX/0N9;->l:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    sub-int v5, v0, v6

    .line 49203
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    iget-wide v2, p0, LX/0N9;->m:J

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    .line 49204
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0N9;->k:Z

    .line 49205
    :cond_6
    const/16 v0, 0xb8

    if-ne v11, v0, :cond_a

    .line 49206
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0N9;->g:Z

    .line 49207
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0N9;->k:Z

    .line 49208
    :cond_7
    :goto_5
    add-int/lit8 v0, v8, 0x3

    move v1, v8

    .line 49209
    goto/16 :goto_0

    .line 49210
    :cond_8
    const/4 v0, 0x0

    goto :goto_1

    .line 49211
    :cond_9
    const/4 v4, 0x0

    goto :goto_4

    .line 49212
    :cond_a
    iget-boolean v0, p0, LX/0N9;->j:Z

    if-eqz v0, :cond_b

    iget-wide v0, p0, LX/0N9;->i:J

    :goto_6
    iput-wide v0, p0, LX/0N9;->m:J

    .line 49213
    iget-wide v0, p0, LX/0N9;->h:J

    int-to-long v2, v6

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/0N9;->l:J

    .line 49214
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0N9;->j:Z

    .line 49215
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0N9;->g:Z

    goto :goto_5

    .line 49216
    :cond_b
    iget-wide v0, p0, LX/0N9;->m:J

    iget-wide v2, p0, LX/0N9;->d:J

    add-long/2addr v0, v2

    goto :goto_6

    .line 49217
    :cond_c
    iget v4, v1, LX/0N8;->a:I

    sub-int/2addr v4, v0

    iput v4, v1, LX/0N8;->a:I

    .line 49218
    iput-boolean v3, v1, LX/0N8;->d:Z

    goto :goto_3

    .line 49219
    :cond_d
    const/16 v4, 0xb3

    if-ne v11, v4, :cond_3

    .line 49220
    iput-boolean v2, v1, LX/0N8;->d:Z

    goto/16 :goto_2
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 49221
    return-void
.end method
