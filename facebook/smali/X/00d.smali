.class public LX/00d;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/00d;


# instance fields
.field public b:Landroid/content/SharedPreferences;

.field public volatile c:Ljava/lang/String;

.field public volatile d:[B

.field public volatile e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2561
    new-instance v0, LX/00d;

    invoke-direct {v0}, LX/00d;-><init>()V

    sput-object v0, LX/00d;->a:LX/00d;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 2562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2563
    const-string v0, ""

    iput-object v0, p0, LX/00d;->c:Ljava/lang/String;

    .line 2564
    const/4 v0, 0x0

    iput-object v0, p0, LX/00d;->d:[B

    .line 2565
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/00d;->e:J

    .line 2566
    return-void
.end method

.method public static a(JJ)Z
    .locals 2

    .prologue
    .line 2567
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    cmp-long v0, v0, p0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized d()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2568
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/00d;->b:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 2569
    :goto_0
    monitor-exit p0

    return v0

    .line 2570
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/00d;->b:Landroid/content/SharedPreferences;

    const-string v1, "UDP_PRIMING_DNS/HOST_NAME"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/00d;->c:Ljava/lang/String;

    .line 2571
    iget-object v0, p0, LX/00d;->b:Landroid/content/SharedPreferences;

    const-string v1, "UDP_PRIMING_DNS/HOST_IP"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2572
    if-eqz v0, :cond_1

    .line 2573
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    iput-object v0, p0, LX/00d;->d:[B

    .line 2574
    :cond_1
    iget-object v0, p0, LX/00d;->b:Landroid/content/SharedPreferences;

    const-string v1, "UDP_PRIMING_DNS/HOST_TIMESTAMP"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, LX/00d;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2575
    const/4 v0, 0x1

    goto :goto_0

    .line 2576
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 2577
    iget-object v0, p0, LX/00d;->b:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 2578
    const-string v0, "UDP_PRIMING_DNS_INFO_STORAGE"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, LX/00d;->b:Landroid/content/SharedPreferences;

    .line 2579
    invoke-direct {p0}, LX/00d;->d()Z

    move-result v0

    .line 2580
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
