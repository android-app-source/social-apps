.class public final LX/0DW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Intent;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30748
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    .line 30749
    iput-object v2, p0, LX/0DW;->b:Ljava/util/ArrayList;

    .line 30750
    iput-object v2, p0, LX/0DW;->c:Ljava/util/ArrayList;

    .line 30751
    iput-object v2, p0, LX/0DW;->d:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a(D)LX/0DW;
    .locals 3

    .prologue
    .line 30745
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.DISPLAY_HEIGHT_RATIO"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 30746
    return-object p0
.end method

.method public final a(IIII)LX/0DW;
    .locals 4

    .prologue
    .line 30743
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_ANIMATION"

    const/4 v2, 0x4

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput p1, v2, v3

    const/4 v3, 0x1

    aput p2, v2, v3

    const/4 v3, 0x2

    aput p3, v2, v3

    const/4 v3, 0x3

    aput p4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 30744
    return-object p0
.end method

.method public final a(Landroid/os/Bundle;)LX/0DW;
    .locals 2

    .prologue
    .line 30738
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_TRACKING"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 30739
    if-nez v0, :cond_0

    .line 30740
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_TRACKING"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 30741
    :goto_0
    return-object p0

    .line 30742
    :cond_0
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;)LX/0DW;
    .locals 2

    .prologue
    .line 30736
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.JS_BRIDGE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 30737
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/0DW;
    .locals 2

    .prologue
    .line 30734
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 30735
    return-object p0
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;)LX/0DW;
    .locals 2

    .prologue
    .line 30684
    iget-object v0, p0, LX/0DW;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 30685
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0DW;->c:Ljava/util/ArrayList;

    .line 30686
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 30687
    const-string v1, "KEY_LABEL"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 30688
    const-string v1, "KEY_ICON_RES"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 30689
    const-string v1, "action"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 30690
    iget-object v1, p0, LX/0DW;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30691
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0DW;
    .locals 2

    .prologue
    .line 30730
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_PREVIEW_TITLE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30731
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_PREVIEW_BODY"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30732
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_PREVIEW_SUBTITLE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30733
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/util/ArrayList;)LX/0DW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0DW;"
        }
    .end annotation

    .prologue
    .line 30722
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 30723
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 30724
    const-string v1, "KEY_URL"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 30725
    const-string v1, "KEY_STRING_ARRAY"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 30726
    iget-object v1, p0, LX/0DW;->d:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 30727
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/0DW;->d:Ljava/util/ArrayList;

    .line 30728
    :cond_0
    iget-object v1, p0, LX/0DW;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30729
    :cond_1
    return-object p0
.end method

.method public final a(Ljava/util/Locale;)LX/0DW;
    .locals 2

    .prologue
    .line 30720
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_LOCALE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 30721
    return-object p0
.end method

.method public final a(Z)LX/0DW;
    .locals 2

    .prologue
    .line 30718
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_RAGE_SHAKE_AVAILABLE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 30719
    return-object p0
.end method

.method public final a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 30675
    iget-object v0, p0, LX/0DW;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 30676
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_MENU_ITEMS"

    iget-object v2, p0, LX/0DW;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 30677
    :cond_0
    iget-object v0, p0, LX/0DW;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 30678
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_ACTION_MENU_ITEMS"

    iget-object v2, p0, LX/0DW;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 30679
    :cond_1
    iget-object v0, p0, LX/0DW;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 30680
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_COOKIES"

    iget-object v2, p0, LX/0DW;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 30681
    :cond_2
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "splash_throbber"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "splash_icon_url"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 30682
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You can\'t specify both enable splash throbber and set a url for splash screen icon!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30683
    :cond_3
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public final b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;
    .locals 2

    .prologue
    .line 30692
    iget-object v0, p0, LX/0DW;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 30693
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0DW;->b:Ljava/util/ArrayList;

    .line 30694
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 30695
    const-string v1, "KEY_LABEL"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 30696
    const-string v1, "KEY_ICON_RES"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 30697
    const-string v1, "action"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 30698
    iget-object v1, p0, LX/0DW;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30699
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)LX/0DW;
    .locals 3

    .prologue
    .line 30700
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_BURD_BLUE_ENABLED"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 30701
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_CLOSE_BUTTON_ICON"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30702
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_URL_TEXT_COLOR"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30703
    return-object p0
.end method

.method public final b(Z)LX/0DW;
    .locals 2

    .prologue
    .line 30704
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_SHOW_DOMAIN_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 30705
    return-object p0
.end method

.method public final c(Z)LX/0DW;
    .locals 2

    .prologue
    .line 30706
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_LOGCAT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 30707
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/0DW;
    .locals 2

    .prologue
    .line 30708
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_THEME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30709
    return-object p0
.end method

.method public final d(Z)LX/0DW;
    .locals 2

    .prologue
    .line 30710
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "splash_throbber"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 30711
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/0DW;
    .locals 2

    .prologue
    .line 30712
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_UA"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30713
    return-object p0
.end method

.method public final f(I)LX/0DW;
    .locals 2

    .prologue
    .line 30714
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "extra_menu_button_icon"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 30715
    return-object p0
.end method

.method public final i(Z)LX/0DW;
    .locals 2

    .prologue
    .line 30716
    iget-object v0, p0, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_REFRESH_BUTTON_ENABLED"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 30717
    return-object p0
.end method
