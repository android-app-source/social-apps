.class public LX/08s;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x9
.end annotation


# instance fields
.field private final a:Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

.field private final b:LX/05w;

.field private final c:LX/063;

.field private final d:LX/064;


# direct methods
.method public constructor <init>(Ljavax/net/ssl/SSLSocketFactory;LX/05w;LX/060;LX/062;LX/063;LX/064;I)V
    .locals 1

    .prologue
    .line 21652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21653
    iput-object p2, p0, LX/08s;->b:LX/05w;

    .line 21654
    invoke-static {p1}, LX/060;->a(Ljavax/net/ssl/SSLSocketFactory;)Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

    move-result-object v0

    iput-object v0, p0, LX/08s;->a:Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

    .line 21655
    iget-object v0, p0, LX/08s;->a:Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

    invoke-static {v0, p7}, LX/062;->a(Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;I)V

    .line 21656
    iput-object p5, p0, LX/08s;->c:LX/063;

    .line 21657
    iput-object p6, p0, LX/08s;->d:LX/064;

    .line 21658
    return-void
.end method


# virtual methods
.method public final a(Ljava/net/Socket;Ljava/lang/String;I)Ljava/net/Socket;
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 21642
    iget-object v0, p0, LX/08s;->a:Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

    .line 21643
    new-instance v2, Lcom/facebook/rti/mqtt/common/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, v1

    move-object v7, v0

    invoke-direct/range {v2 .. v7}, Lcom/facebook/rti/mqtt/common/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;-><init>(Ljava/net/Socket;Ljava/lang/String;IZLorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;)V

    move-object v0, v2

    .line 21644
    :try_start_0
    invoke-virtual {v0, p2}, Lcom/facebook/rti/mqtt/common/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;->setHostname(Ljava/lang/String;)V

    .line 21645
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/rti/mqtt/common/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;->setUseSessionTickets(Z)V

    .line 21646
    invoke-virtual {p1}, Ljava/net/Socket;->getSoTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/rti/mqtt/common/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;->setHandshakeTimeout(I)V

    .line 21647
    invoke-virtual {p1}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    invoke-static {v0, v1, p2, p3}, LX/063;->a(Ljava/net/Socket;[BLjava/lang/String;I)V

    .line 21648
    iget-object v1, p0, LX/08s;->b:LX/05w;

    invoke-virtual {v1, v0, p2}, LX/05w;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V
    :try_end_0
    .catch LX/061; {:try_start_0 .. :try_end_0} :catch_0

    .line 21649
    return-object v0

    .line 21650
    :catch_0
    move-exception v0

    .line 21651
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
