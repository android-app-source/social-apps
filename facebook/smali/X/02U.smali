.class public final LX/02U;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static sAttemptedCrossDexHookInstallation:Z

.field private static sCrossDexHookInstallationError:Ljava/lang/Throwable;

.field private static sListHead:LX/02U;

.field private static sMergedDexConfig:LX/02f;


# instance fields
.field private id:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mApk:Ljava/io/File;

.field public final mChildStores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/02U;",
            ">;"
        }
    .end annotation
.end field

.field private mLastDeri:LX/02X;

.field public mLoadedManifest:LX/02Y;

.field public final mLockFile:LX/02V;

.field private mManifest:LX/02Y;

.field private final mParentStores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/02U;",
            ">;"
        }
    .end annotation
.end field

.field private final mResProvider:LX/02S;

.field public final next:LX/02U;

.field public final root:Ljava/io/File;


# direct methods
.method private constructor <init>(Ljava/io/File;Ljava/io/File;LX/02S;)V
    .locals 2

    .prologue
    .line 7964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7965
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/02U;->mParentStores:Ljava/util/List;

    .line 7966
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/02U;->mChildStores:Ljava/util/List;

    .line 7967
    sget-object v0, LX/02U;->sListHead:LX/02U;

    iput-object v0, p0, LX/02U;->next:LX/02U;

    .line 7968
    iput-object p2, p0, LX/02U;->mApk:Ljava/io/File;

    .line 7969
    iput-object p1, p0, LX/02U;->root:Ljava/io/File;

    .line 7970
    invoke-static {p1}, LX/02Q;->mkdirOrThrow(Ljava/io/File;)V

    .line 7971
    new-instance v0, Ljava/io/File;

    const-string v1, "mdex_lock"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, LX/02V;->open(Ljava/io/File;)LX/02V;

    move-result-object v0

    iput-object v0, p0, LX/02U;->mLockFile:LX/02V;

    .line 7972
    iput-object p3, p0, LX/02U;->mResProvider:LX/02S;

    .line 7973
    return-void
.end method

.method private adjustDesiredStateForConfig(BLX/080;)B
    .locals 5

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 7974
    iget-byte v2, p2, LX/080;->mode:B

    if-eqz v2, :cond_0

    .line 7975
    iget-byte v2, p2, LX/080;->mode:B

    if-ne v2, v4, :cond_1

    .line 7976
    const-string v1, "using fallback mode due to request in config file"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move p1, v0

    .line 7977
    :cond_0
    :goto_0
    return p1

    .line 7978
    :cond_1
    iget-byte v2, p2, LX/080;->mode:B

    if-ne v2, v0, :cond_2

    .line 7979
    packed-switch p1, :pswitch_data_0

    .line 7980
    :pswitch_0
    const-string v0, "ignoring configured turbo mode: state not whitelisted: %s"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 7981
    :pswitch_1
    const-string v0, "ignoring configured turbo mode: no dex loading to do"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 7982
    :pswitch_2
    const-string v0, "ignoring configured turbo mode: already forced to fallback mode"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 7983
    :pswitch_3
    const-string v0, "config file wants turbo mode: already using it"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 7984
    :pswitch_4
    const-string v0, "using ART turbo as requested in config file"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7985
    const/4 p1, 0x7

    .line 7986
    goto :goto_0

    .line 7987
    :pswitch_5
    const-string v0, "using Dalvik turbo as requested in config file"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7988
    const/4 p1, 0x4

    .line 7989
    goto :goto_0

    .line 7990
    :cond_2
    iget-byte v0, p2, LX/080;->mode:B

    if-ne v0, v1, :cond_3

    .line 7991
    packed-switch p1, :pswitch_data_1

    .line 7992
    :pswitch_6
    const-string v0, "ignoring configured xdex mode: state not whitelisted: %s"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 7993
    :pswitch_7
    const-string v0, "ignoring configured xdex mode: no dex loading to do"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 7994
    :pswitch_8
    const-string v0, "ignoring configured xdex mode: already forced to fallback"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 7995
    :pswitch_9
    const-string v0, "config file wants xdex mode: already using it"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 7996
    :pswitch_a
    const-string v0, "using ART xdex as requested in config file"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7997
    const/16 p1, 0x8

    .line 7998
    goto :goto_0

    .line 7999
    :pswitch_b
    const-string v0, "using Dalvik xdex as requested in config"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move p1, v1

    .line 8000
    goto :goto_0

    .line 8001
    :cond_3
    const-string v0, "ignoring unknown configured dex mode %s"

    new-array v1, v4, [Ljava/lang/Object;

    iget-byte v2, p2, LX/080;->mode:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_6
        :pswitch_6
        :pswitch_a
        :pswitch_9
        :pswitch_7
    .end packed-switch
.end method

.method private static assertLockHeld(LX/02U;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8002
    iget-object v0, p0, LX/02U;->mLockFile:LX/02V;

    .line 8003
    iget-object v2, v0, LX/02V;->mLockOwner:Ljava/lang/Thread;

    move-object v0, v2

    .line 8004
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "lock req"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 8005
    return-void

    :cond_0
    move v0, v1

    .line 8006
    goto :goto_0
.end method

.method private static checkDeps(LX/02U;)Z
    .locals 2

    .prologue
    .line 8007
    invoke-direct {p0}, LX/02U;->readCurrentDepBlock()[B

    move-result-object v0

    .line 8008
    invoke-direct {p0}, LX/02U;->readSavedDepBlock()[B

    move-result-object v1

    .line 8009
    if-eqz v1, :cond_0

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkDirty(LX/02c;[Ljava/lang/String;)I
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 8010
    iget-object v7, p1, LX/02c;->expectedFiles:[Ljava/lang/String;

    .line 8011
    iget v0, p1, LX/02c;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_7

    move v1, v2

    .line 8012
    :goto_0
    invoke-virtual {p2}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 8013
    invoke-static {p0, v0, v7}, LX/02U;->setDifference(LX/02U;[Ljava/lang/String;[Ljava/lang/String;)V

    move v4, v3

    move v5, v3

    .line 8014
    :goto_1
    array-length v8, v0

    if-ge v4, v8, :cond_1

    .line 8015
    aget-object v8, v0, v4

    if-eqz v8, :cond_0

    .line 8016
    const-string v5, "deleting unknown file %s in dex store %s with schema %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    aget-object v9, v0, v4

    aput-object v9, v8, v3

    iget-object v9, p0, LX/02U;->root:Ljava/io/File;

    aput-object v9, v8, v6

    invoke-virtual {p1}, LX/02c;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v5, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8017
    new-instance v5, Ljava/io/File;

    iget-object v8, p0, LX/02U;->root:Ljava/io/File;

    aget-object v9, v0, v4

    invoke-direct {v5, v8, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v5}, LX/02Q;->deleteRecursive(Ljava/io/File;)V

    move v5, v6

    .line 8018
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 8019
    :cond_1
    if-eqz v5, :cond_2

    iget v0, p1, LX/02c;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 8020
    const-string v0, "dex store %s had excess files and is non-incremental: regenerating"

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v4, p0, LX/02U;->root:Ljava/io/File;

    aput-object v4, v1, v3

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v1, v2

    .line 8021
    :cond_2
    invoke-static {p0}, LX/02U;->checkDeps(LX/02U;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 8022
    const-string v0, "dex store %s dependencies have changed: regenerating all"

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v4, p0, LX/02U;->root:Ljava/io/File;

    aput-object v4, v1, v3

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v1, v2

    .line 8023
    :cond_3
    if-gtz v1, :cond_5

    .line 8024
    invoke-virtual {v7}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 8025
    invoke-static {p0, v0, p2}, LX/02U;->setDifference(LX/02U;[Ljava/lang/String;[Ljava/lang/String;)V

    move v4, v1

    move v1, v3

    .line 8026
    :goto_2
    array-length v5, v0

    if-ge v1, v5, :cond_6

    .line 8027
    aget-object v5, v0, v1

    if-eqz v5, :cond_4

    .line 8028
    const-string v4, "missing file %s in dex store %s"

    new-array v5, v2, [Ljava/lang/Object;

    aget-object v7, v0, v1

    aput-object v7, v5, v3

    iget-object v7, p0, LX/02U;->root:Ljava/io/File;

    aput-object v7, v5, v6

    invoke-static {v4, v5}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v4, v6

    .line 8029
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    move v4, v1

    .line 8030
    :cond_6
    return v4

    :cond_7
    move v1, v3

    goto/16 :goto_0
.end method

.method private deleteFiles([Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 8031
    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 8032
    aget-object v2, p1, v0

    if-eqz v2, :cond_0

    .line 8033
    const-string v2, "deleting existing file %s/%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/02U;->root:Ljava/io/File;

    aput-object v4, v3, v1

    const/4 v4, 0x1

    aget-object v5, p1, v0

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8034
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, LX/02U;->root:Ljava/io/File;

    aget-object v4, p1, v0

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v2}, LX/02Q;->deleteRecursive(Ljava/io/File;)V

    .line 8035
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8036
    :cond_1
    return-void
.end method

.method private determineDesiredState(BLX/02Y;)B
    .locals 7

    .prologue
    const/16 v0, 0x9

    const/4 v1, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 8037
    iget-object v2, p2, LX/02Y;->dexes:[LX/02Z;

    array-length v2, v2

    if-nez v2, :cond_0

    .line 8038
    const-string v1, "no secondary dexes listed: using noop configuration"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8039
    :goto_0
    return v0

    .line 8040
    :cond_0
    const/4 v2, 0x5

    if-ne p1, v2, :cond_1

    .line 8041
    const-string v0, "recovering from bad class gen: using fallback"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v0, v1

    .line 8042
    goto :goto_0

    .line 8043
    :cond_1
    const-string v2, "Amazon"

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-boolean v2, LX/00g;->yes:Z

    if-nez v2, :cond_2

    .line 8044
    const-string v0, "avoiding optimizations on non-standard VM"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v0, v1

    .line 8045
    goto :goto_0

    .line 8046
    :cond_2
    sget-boolean v2, LX/00g;->yes:Z

    if-eqz v2, :cond_4

    .line 8047
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_3

    .line 8048
    const-string v0, "avoiding optimizations on pre-L VM"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v0, v1

    .line 8049
    goto :goto_0

    .line 8050
    :cond_3
    iget-object v1, p2, LX/02Y;->dexes:[LX/02Z;

    aget-object v1, v1, v5

    iget-object v1, v1, LX/02Z;->canaryClass:Ljava/lang/String;

    .line 8051
    :try_start_0
    const-string v2, "attempting to detect built-in ART multidex by classloading %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8052
    iget-object v2, p2, LX/02Y;->dexes:[LX/02Z;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v2, v2, LX/02Z;->canaryClass:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 8053
    const-string v2, "ART native multi-dex in use: found %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 8054
    :catch_0
    const-string v0, "ART multi-dex not in use: cannot load %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-static {v0, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8055
    const/16 v0, 0x8

    goto :goto_0

    .line 8056
    :cond_4
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private static determineOdexCacheName(Ljava/io/File;)Ljava/io/File;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 8057
    sget-boolean v0, LX/00g;->yes:Z

    if-eqz v0, :cond_1

    .line 8058
    const/4 v0, 0x0

    .line 8059
    :cond_0
    :goto_0
    return-object v0

    .line 8060
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 8061
    const-string v0, ".apk"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 8062
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".odex"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 8063
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 8064
    :cond_2
    invoke-static {}, LX/02Q;->findSystemDalvikCache()Ljava/io/File;

    move-result-object v0

    const-string v1, "classes.dex"

    invoke-static {v0, p0, v1}, LX/02Q;->dexOptGenerateCacheFileName(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public static declared-synchronized dexStoreListHead()LX/02U;
    .locals 2

    .prologue
    .line 8065
    const-class v0, LX/02U;

    monitor-enter v0

    :try_start_0
    sget-object v1, LX/02U;->sListHead:LX/02U;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private findInArray([Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 8066
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 8067
    aget-object v1, p1, v0

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8068
    :goto_1
    return v0

    .line 8069
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8070
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static declared-synchronized findOpened(Ljava/io/File;)LX/02U;
    .locals 4

    .prologue
    .line 8071
    const-class v1, LX/02U;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    .line 8072
    invoke-static {}, LX/02U;->dexStoreListHead()LX/02U;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    .line 8073
    iget-object v3, v0, LX/02U;->root:Ljava/io/File;

    invoke-virtual {v3, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 8074
    :goto_1
    monitor-exit v1

    return-object v0

    .line 8075
    :cond_0
    :try_start_1
    iget-object v0, v0, LX/02U;->next:LX/02U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 8076
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 8077
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getParents()[LX/02U;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 8078
    invoke-virtual {p0}, LX/02U;->loadManifest()LX/02Y;

    .line 8079
    iget-object v0, p0, LX/02U;->mParentStores:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "dex"

    iget-object v2, p0, LX/02U;->mManifest:LX/02Y;

    iget-object v2, v2, LX/02Y;->id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 8080
    iget-object v0, p0, LX/02U;->mManifest:LX/02Y;

    iget-object v3, v0, LX/02Y;->requires:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 8081
    const-string v0, "dex"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 8082
    invoke-static {}, LX/02U;->dexStoreListHead()LX/02U;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    .line 8083
    iget-object v6, v0, LX/02U;->id:Ljava/lang/String;

    if-eqz v6, :cond_1

    iget-object v6, v0, LX/02U;->id:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 8084
    iget-object v6, p0, LX/02U;->mParentStores:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 8085
    iget-object v6, v0, LX/02U;->mChildStores:Ljava/util/List;

    invoke-interface {v6, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 8086
    iget-object v6, v0, LX/02U;->mChildStores:Ljava/util/List;

    invoke-interface {v6, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 8087
    :cond_0
    const/4 v0, 0x1

    .line 8088
    :goto_2
    if-nez v0, :cond_2

    .line 8089
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to find required store "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of store "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/02U;->mManifest:LX/02Y;

    iget-object v2, v2, LX/02Y;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8090
    :cond_1
    iget-object v0, v0, LX/02U;->next:LX/02U;

    goto :goto_1

    .line 8091
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 8092
    :cond_3
    iget-object v0, p0, LX/02U;->mParentStores:Ljava/util/List;

    iget-object v1, p0, LX/02U;->mParentStores:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [LX/02U;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/02U;

    return-object v0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public static getStatusDescription(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 8507
    const-wide/16 v0, 0xf

    and-long/2addr v0, p0

    long-to-int v0, v0

    .line 8508
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8509
    packed-switch v0, :pswitch_data_0

    .line 8510
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BAD STATE "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8511
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 8512
    :pswitch_0
    const-string v0, "STATE_INVALID"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 8513
    :pswitch_1
    const-string v0, "STATE_TX_FAILED"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 8514
    :pswitch_2
    const-string v0, "STATE_FALLBACK"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 8515
    :pswitch_3
    const-string v0, "STATE_XDEX"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 8516
    :pswitch_4
    const-string v0, "STATE_TURBO"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 8517
    :pswitch_5
    const-string v0, "STATE_BAD_GEN"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 8518
    :pswitch_6
    const-string v0, "STATE_REGEN_FORCED"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 8519
    :pswitch_7
    const-string v0, "STATE_ART_TURBO"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 8520
    :pswitch_8
    const-string v0, "STATE_ART_XDEX"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 8521
    :pswitch_9
    const-string v0, "STATE_NOOP"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private static declared-synchronized installCrossDexHooks()V
    .locals 3

    .prologue
    .line 8093
    const-class v1, LX/02U;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, LX/02U;->sAttemptedCrossDexHookInstallation:Z

    if-eqz v0, :cond_0

    .line 8094
    sget-object v0, LX/02U;->sCrossDexHookInstallationError:Ljava/lang/Throwable;

    if-eqz v0, :cond_1

    .line 8095
    sget-object v0, LX/02U;->sCrossDexHookInstallationError:Ljava/lang/Throwable;

    invoke-static {v0}, LX/02Q;->runtimeExFrom(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8096
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 8097
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, LX/02U;->sAttemptedCrossDexHookInstallation:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8098
    :try_start_2
    invoke-static {}, Lcom/facebook/common/dextricks/DalvikInternals;->fixDvmForCrossDexHack()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 8099
    :try_start_3
    const-string v0, "cross-dex hook installation succeeded"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 8100
    :cond_1
    monitor-exit v1

    return-void

    .line 8101
    :catch_0
    move-exception v0

    .line 8102
    sput-object v0, LX/02U;->sCrossDexHookInstallationError:Ljava/lang/Throwable;

    .line 8103
    throw v0
.end method

.method private listAndPruneRootFiles()[Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 8494
    iget-object v0, p0, LX/02U;->root:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 8495
    if-nez v1, :cond_0

    .line 8496
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to list directory "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/02U;->root:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8497
    :cond_0
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_4

    .line 8498
    aget-object v2, v1, v0

    .line 8499
    const-string v3, "mdex_lock"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "mdex_status2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "odex_lock"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "deps"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "regen_stamp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "optimization_log"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "config"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 8500
    :cond_1
    aput-object v5, v1, v0

    .line 8501
    :cond_2
    const-string v3, "config.tmp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 8502
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, LX/02U;->root:Ljava/io/File;

    invoke-direct {v3, v4, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v3}, LX/02Q;->deleteRecursive(Ljava/io/File;)V

    .line 8503
    aput-object v5, v1, v0

    .line 8504
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8505
    :cond_4
    invoke-direct {p0, v1}, LX/02U;->pruneTemporaryDirectoriesLocked([Ljava/lang/String;)V

    .line 8506
    return-object v1
.end method

.method private loadAllImpl(ILX/006;Landroid/content/Context;)LX/02X;
    .locals 22

    .prologue
    .line 8298
    new-instance v11, LX/02X;

    invoke-direct {v11}, LX/02X;-><init>()V

    .line 8299
    invoke-virtual/range {p0 .. p0}, LX/02U;->isLoaded()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 8300
    move-object/from16 v0, p0

    iget-object v4, v0, LX/02U;->mLastDeri:LX/02X;

    if-nez v4, :cond_0

    .line 8301
    const-string v4, "dex store %s has already been loaded, but did not save recovery info"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/02U;->root:Ljava/io/File;

    aput-object v7, v5, v6

    invoke-static {v4, v5}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8302
    move-object/from16 v0, p0

    iput-object v11, v0, LX/02U;->mLastDeri:LX/02X;

    .line 8303
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/02U;->mLastDeri:LX/02X;

    .line 8304
    :goto_0
    return-object v4

    .line 8305
    :cond_1
    const-string v4, "DLL2_manifest_load"

    const v5, 0x880004

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, LX/006;->a(Ljava/lang/String;I)LX/00X;

    move-result-object v6

    const/4 v5, 0x0

    .line 8306
    :try_start_0
    invoke-virtual/range {p0 .. p0}, LX/02U;->loadManifest()LX/02Y;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 8307
    if-eqz v6, :cond_2

    invoke-virtual {v6}, LX/00X;->close()V

    .line 8308
    :cond_2
    invoke-direct/range {p0 .. p0}, LX/02U;->getParents()[LX/02U;

    move-result-object v5

    array-length v6, v5

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v6, :cond_6

    aget-object v8, v5, v4

    .line 8309
    invoke-virtual {v8}, LX/02U;->isLoaded()Z

    move-result v9

    if-nez v9, :cond_3

    .line 8310
    move/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v8, v0, v1, v2}, LX/02U;->loadAll(ILX/006;Landroid/content/Context;)LX/02X;

    move-result-object v9

    .line 8311
    const-string v10, "parent dex store %s loaded with result: %x"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v8, v8, LX/02U;->id:Ljava/lang/String;

    aput-object v8, v12, v13

    const/4 v8, 0x1

    iget v9, v9, LX/02X;->loadResult:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v12, v8

    invoke-static {v10, v12}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8312
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 8313
    :catch_0
    move-exception v5

    :try_start_1
    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8314
    :catchall_0
    move-exception v4

    if-eqz v6, :cond_4

    if-eqz v5, :cond_5

    :try_start_2
    invoke-virtual {v6}, LX/00X;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_4
    :goto_2
    throw v4

    :catch_1
    move-exception v6

    invoke-static {v5, v6}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_5
    invoke-virtual {v6}, LX/00X;->close()V

    goto :goto_2

    .line 8315
    :cond_6
    const/4 v13, 0x0

    .line 8316
    move-object/from16 v0, p0

    iget-object v4, v0, LX/02U;->mLockFile:LX/02V;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/02V;->acquire(I)LX/02W;

    move-result-object v12

    .line 8317
    :try_start_3
    invoke-static/range {p0 .. p0}, LX/02U;->readStatusLocked(LX/02U;)J

    move-result-wide v14

    .line 8318
    const-wide/16 v4, 0xf

    and-long/2addr v4, v14

    long-to-int v4, v4

    int-to-byte v10, v4

    .line 8319
    const/16 v4, 0xa

    if-lt v10, v4, :cond_7

    .line 8320
    const-string v4, "found invalid state %s: nuking dex store %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v10}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, LX/02U;->root:Ljava/io/File;

    aput-object v8, v5, v6

    invoke-static {v4, v5}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8321
    :cond_7
    const/4 v4, 0x1

    if-ne v10, v4, :cond_8

    .line 8322
    const-string v4, "found abandoned transaction (prev stateno %s status %x) on dex store %s: nuking store"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v8, 0x4

    shr-long v8, v14, v8

    const-wide/16 v16, 0xf

    and-long v8, v8, v16

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x1

    const/4 v8, 0x4

    shr-long v8, v14, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v8, v0, LX/02U;->root:Ljava/io/File;

    aput-object v8, v5, v6

    invoke-static {v4, v5}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8323
    const/16 v13, 0x10

    .line 8324
    :cond_8
    const/4 v4, 0x5

    if-ne v10, v4, :cond_9

    .line 8325
    const-string v4, "crashed last time while loading generated files; trying fallback"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8326
    or-int/lit8 v13, v13, 0x40

    .line 8327
    :cond_9
    const/4 v4, 0x6

    if-ne v10, v4, :cond_a

    .line 8328
    const-string v4, "force dex regeneration requested"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8329
    or-int/lit8 v13, v13, 0x20

    .line 8330
    :cond_a
    move-object/from16 v0, p0

    invoke-static {v0, v7, v14, v15}, LX/02U;->schemeForState(LX/02U;LX/02Y;J)LX/02c;

    move-result-object v4

    .line 8331
    const-string v5, "DLL2_check_dirty"

    const v6, 0x880005

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, LX/006;->a(Ljava/lang/String;I)LX/00X;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v6

    const/4 v5, 0x0

    .line 8332
    :try_start_4
    invoke-direct/range {p0 .. p0}, LX/02U;->listAndPruneRootFiles()[Ljava/lang/String;

    move-result-object v9

    .line 8333
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v9}, LX/02U;->checkDirty(LX/02c;[Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_b

    move-result v8

    .line 8334
    if-eqz v6, :cond_b

    :try_start_5
    invoke-virtual {v6}, LX/00X;->close()V

    .line 8335
    :cond_b
    if-nez v8, :cond_11

    .line 8336
    const-string v5, "LA_LOAD_EXISTING"

    .line 8337
    :goto_3
    const-string v6, "current scheme: %s next step: %s"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v4, v16, v17

    const/16 v17, 0x1

    aput-object v5, v16, v17

    move-object/from16 v0, v16

    invoke-static {v6, v0}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8338
    const/4 v5, 0x1

    if-ne v8, v5, :cond_c

    iget v5, v4, LX/02c;->flags:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_c

    .line 8339
    const-string v5, "scheme %s is non-incremental: regenerating everything"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v6, v8

    invoke-static {v5, v6}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 8340
    const/4 v8, 0x2

    .line 8341
    :cond_c
    if-nez v8, :cond_d

    const/4 v5, 0x3

    if-ne v10, v5, :cond_d

    .line 8342
    :try_start_6
    invoke-static {}, LX/02U;->installCrossDexHooks()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 8343
    :cond_d
    :goto_4
    const/4 v5, 0x0

    .line 8344
    if-lez v8, :cond_15

    .line 8345
    :try_start_7
    iget-object v5, v7, LX/02Y;->dexes:[LX/02Z;

    array-length v5, v5

    const/16 v6, 0x3a

    if-le v5, v6, :cond_13

    .line 8346
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "too many dexes: have %s but maximum per dex store is %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v7, v7, LX/02Y;->dexes:[LX/02Z;

    array-length v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x1

    const/16 v8, 0x3a

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 8347
    :catchall_1
    move-exception v4

    move-object v5, v12

    :goto_5
    if-eqz v5, :cond_e

    .line 8348
    invoke-virtual {v5}, LX/02W;->close()V

    :cond_e
    throw v4

    .line 8349
    :catch_2
    move-exception v4

    :try_start_8
    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 8350
    :catchall_2
    move-exception v5

    move-object/from16 v21, v5

    move-object v5, v4

    move-object/from16 v4, v21

    :goto_6
    if-eqz v6, :cond_f

    if-eqz v5, :cond_10

    :try_start_9
    invoke-virtual {v6}, LX/00X;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :cond_f
    :goto_7
    :try_start_a
    throw v4

    :catch_3
    move-exception v6

    invoke-static {v5, v6}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_7

    :cond_10
    invoke-virtual {v6}, LX/00X;->close()V

    goto :goto_7

    .line 8351
    :cond_11
    const/4 v5, 0x1

    if-ne v8, v5, :cond_12

    .line 8352
    const-string v5, "LA_REGEN_MISSING"

    goto/16 :goto_3

    .line 8353
    :cond_12
    const-string v5, "LA_REGEN_ALL"

    goto/16 :goto_3

    .line 8354
    :catch_4
    move-exception v5

    .line 8355
    const-string v6, "dex store %s needs xdex hooks, but we can\'t do it: regenerating"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/02U;->root:Ljava/io/File;

    move-object/from16 v17, v0

    aput-object v17, v8, v16

    invoke-static {v5, v6, v8}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8356
    const/4 v8, 0x2

    goto :goto_4

    .line 8357
    :cond_13
    move-object/from16 v0, p0

    invoke-static {v0, v14, v15}, LX/02U;->writeTxFailedStatusLocked(LX/02U;J)V

    .line 8358
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/02U;->root:Ljava/io/File;

    const-string v16, "regen_stamp"

    move-object/from16 v0, v16

    invoke-direct {v5, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8359
    invoke-static {v5}, LX/02Q;->deleteRecursive(Ljava/io/File;)V

    .line 8360
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 8361
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/02U;->root:Ljava/io/File;

    const-string v16, "odex_lock"

    move-object/from16 v0, v16

    invoke-direct {v5, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8362
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_14

    .line 8363
    invoke-static {v5}, LX/02V;->open(Ljava/io/File;)LX/02V;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result-object v6

    const/4 v5, 0x0

    .line 8364
    const/16 v16, 0x0

    :try_start_b
    move/from16 v0, v16

    invoke-virtual {v6, v0}, LX/02V;->acquire(I)LX/02W;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, LX/02W;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_a

    .line 8365
    if-eqz v6, :cond_14

    :try_start_c
    invoke-virtual {v6}, LX/02V;->close()V

    .line 8366
    :cond_14
    const/4 v5, 0x1

    .line 8367
    :cond_15
    const/4 v6, 0x1

    if-ne v8, v6, :cond_16

    .line 8368
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "DLL2_regen_missing_"

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const v8, 0x880006

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v8}, LX/006;->a(Ljava/lang/String;I)LX/00X;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    move-result-object v16

    const/4 v6, 0x0

    .line 8369
    const/4 v8, 0x1

    :try_start_d
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v4, v8}, LX/02U;->runCompiler(LX/02Y;LX/02c;I)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_8
    .catchall {:try_start_d .. :try_end_d} :catchall_9

    .line 8370
    const/4 v8, 0x0

    .line 8371
    :goto_8
    if-eqz v16, :cond_16

    :try_start_e
    invoke-virtual/range {v16 .. v16}, LX/00X;->close()V

    .line 8372
    :cond_16
    if-eqz v8, :cond_31

    .line 8373
    invoke-static/range {p0 .. p0}, LX/02U;->saveDeps(LX/02U;)V

    .line 8374
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v7}, LX/02U;->determineDesiredState(BLX/02Y;)B

    move-result v6

    .line 8375
    and-int/lit8 v10, p1, 0x1

    if-eqz v10, :cond_17

    .line 8376
    sparse-switch v6, :sswitch_data_0

    :cond_17
    move v14, v6

    .line 8377
    :goto_9
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/02U;->root:Ljava/io/File;

    const-string v15, "config"

    invoke-direct {v10, v6, v15}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 8378
    :try_start_f
    invoke-static {v10}, LX/080;->read(Ljava/io/File;)LX/080;

    move-result-object v6

    .line 8379
    const-string v15, "loaded config file %s"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v10, v16, v17

    invoke-static/range {v15 .. v16}, LX/02P;->v(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_f
    .catch Ljava/io/FileNotFoundException; {:try_start_f .. :try_end_f} :catch_a
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 8380
    :goto_a
    :try_start_10
    iget-byte v10, v6, LX/080;->sync:B

    packed-switch v10, :pswitch_data_0

    .line 8381
    const-string v10, "config file has unknown sync control mode %s: ignoring"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    iget-byte v0, v6, LX/080;->sync:B

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v10, v15}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move/from16 v10, p1

    .line 8382
    :goto_b
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v6}, LX/02U;->adjustDesiredStateForConfig(BLX/080;)B
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    move-result v6

    .line 8383
    const/4 v14, 0x3

    if-ne v6, v14, :cond_18

    .line 8384
    :try_start_11
    invoke-static {}, LX/02U;->installCrossDexHooks()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_b
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 8385
    :cond_18
    :goto_c
    :try_start_12
    const-string v14, "desiredStateNo:%s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-object v14, v9

    move v9, v6

    move-object v6, v4

    .line 8386
    :goto_d
    if-eqz v8, :cond_23

    .line 8387
    const/4 v4, 0x2

    if-lt v8, v4, :cond_1d

    const/4 v4, 0x1

    :goto_e
    const-string v6, "incremental regen already handled"

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    invoke-static {v4, v6, v15}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 8388
    int-to-long v0, v9

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-static {v0, v7, v1, v2}, LX/02U;->schemeForState(LX/02U;LX/02Y;J)LX/02c;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    move-result-object v15

    .line 8389
    :try_start_13
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "DLL2_regen_all_"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const v6, 0x880007

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v6}, LX/006;->a(Ljava/lang/String;I)LX/00X;
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_d
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    move-result-object v16

    const/4 v6, 0x0

    .line 8390
    :try_start_14
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, LX/02U;->deleteFiles([Ljava/lang/String;)V

    .line 8391
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/02U;->root:Ljava/io/File;

    move-object/from16 v17, v0

    const-string v18, "optimization_log"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v4, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8392
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 8393
    iget v4, v15, LX/02c;->flags:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_1e

    .line 8394
    const-string v4, "not running dex compiler: scheme says there is nothing to do"

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v4, v0}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_c
    .catchall {:try_start_14 .. :try_end_14} :catchall_8

    .line 8395
    :goto_f
    const/4 v6, 0x0

    .line 8396
    if-eqz v16, :cond_21

    :try_start_15
    invoke-virtual/range {v16 .. v16}, LX/00X;->close()V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_f
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    move v8, v6

    move-object v6, v15

    goto :goto_d

    .line 8397
    :catch_5
    move-exception v4

    :try_start_16
    throw v4
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_3

    .line 8398
    :catchall_3
    move-exception v5

    move-object/from16 v21, v5

    move-object v5, v4

    move-object/from16 v4, v21

    :goto_10
    if-eqz v6, :cond_19

    if-eqz v5, :cond_1a

    :try_start_17
    invoke-virtual {v6}, LX/02V;->close()V
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_6
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    :cond_19
    :goto_11
    :try_start_18
    throw v4

    :catch_6
    move-exception v6

    invoke-static {v5, v6}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_11

    :cond_1a
    invoke-virtual {v6}, LX/02V;->close()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    goto :goto_11

    .line 8399
    :catch_7
    move-exception v8

    .line 8400
    :try_start_19
    const-string v17, "incremental regeneration error in dex store %s: regenerating"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/02U;->root:Ljava/io/File;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v8, v0, v1}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_19} :catch_8
    .catchall {:try_start_19 .. :try_end_19} :catchall_9

    .line 8401
    const/4 v8, 0x2

    goto/16 :goto_8

    .line 8402
    :catch_8
    move-exception v4

    :try_start_1a
    throw v4
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_4

    .line 8403
    :catchall_4
    move-exception v5

    move-object/from16 v21, v5

    move-object v5, v4

    move-object/from16 v4, v21

    :goto_12
    if-eqz v16, :cond_1b

    if-eqz v5, :cond_1c

    :try_start_1b
    invoke-virtual/range {v16 .. v16}, LX/00X;->close()V
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_1b} :catch_9
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    :cond_1b
    :goto_13
    :try_start_1c
    throw v4

    :catch_9
    move-exception v6

    invoke-static {v5, v6}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_13

    :cond_1c
    invoke-virtual/range {v16 .. v16}, LX/00X;->close()V

    goto :goto_13

    .line 8404
    :sswitch_0
    const-string v6, "using ART turbo instead of ART xdex: DS_DO_NOT_OPTIMIZE"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v6, v10}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8405
    const/4 v6, 0x7

    move v14, v6

    .line 8406
    goto/16 :goto_9

    .line 8407
    :sswitch_1
    const-string v6, "using Dalvik turbo instead of xdex: DS_DO_NOT_OPTIMIZE"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v6, v10}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8408
    const/4 v6, 0x4

    move v14, v6

    goto/16 :goto_9

    .line 8409
    :catch_a
    const-string v6, "no config file for repository %s found: using all-default configuration"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/02U;->root:Ljava/io/File;

    move-object/from16 v16, v0

    aput-object v16, v10, v15

    invoke-static {v6, v10}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8410
    new-instance v6, LX/081;

    invoke-direct {v6}, LX/081;-><init>()V

    invoke-virtual {v6}, LX/081;->build()LX/080;

    move-result-object v6

    goto/16 :goto_a

    :pswitch_0
    move/from16 v10, p1

    .line 8411
    goto/16 :goto_b

    .line 8412
    :pswitch_1
    const-string v10, "forcing async optimization mode from config file: dangerous!"

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    invoke-static {v10, v15}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8413
    or-int/lit8 p1, p1, 0x4

    move/from16 v10, p1

    .line 8414
    goto/16 :goto_b

    .line 8415
    :pswitch_2
    const-string v10, "forcing synchronous optimization from config file"

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    invoke-static {v10, v15}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8416
    and-int/lit8 v10, p1, -0x5

    .line 8417
    or-int/lit8 p1, v10, 0x8

    move/from16 v10, p1

    .line 8418
    goto/16 :goto_b

    .line 8419
    :catch_b
    move-exception v6

    .line 8420
    const-string v14, "disabling cross-dex optimization: cannot install hooks"

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    invoke-static {v6, v14, v15}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8421
    iput-object v6, v11, LX/02X;->xdexFailureCause:Ljava/lang/Throwable;
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    .line 8422
    const/4 v6, 0x4

    goto/16 :goto_c

    .line 8423
    :cond_1d
    const/4 v4, 0x0

    goto/16 :goto_e

    .line 8424
    :cond_1e
    const/4 v4, 0x0

    :try_start_1d
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v15, v4}, LX/02U;->runCompiler(LX/02Y;LX/02c;I)V
    :try_end_1d
    .catch Ljava/lang/Throwable; {:try_start_1d .. :try_end_1d} :catch_c
    .catchall {:try_start_1d .. :try_end_1d} :catchall_8

    goto/16 :goto_f

    .line 8425
    :catch_c
    move-exception v4

    :try_start_1e
    throw v4
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_5

    .line 8426
    :catchall_5
    move-exception v6

    move-object/from16 v21, v6

    move-object v6, v4

    move-object/from16 v4, v21

    :goto_14
    if-eqz v16, :cond_1f

    if-eqz v6, :cond_20

    :try_start_1f
    invoke-virtual/range {v16 .. v16}, LX/00X;->close()V
    :try_end_1f
    .catch Ljava/lang/Throwable; {:try_start_1f .. :try_end_1f} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_d
    .catchall {:try_start_1f .. :try_end_1f} :catchall_1

    :cond_1f
    :goto_15
    :try_start_20
    throw v4
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_d
    .catchall {:try_start_20 .. :try_end_20} :catchall_1

    :catch_d
    move-exception v4

    move v6, v8

    .line 8427
    :goto_16
    const/4 v8, 0x2

    if-ne v9, v8, :cond_22

    .line 8428
    :try_start_21
    throw v4
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_1

    .line 8429
    :catch_e
    move-exception v14

    :try_start_22
    invoke-static {v6, v14}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_15

    :cond_20
    invoke-virtual/range {v16 .. v16}, LX/00X;->close()V
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_d
    .catchall {:try_start_22 .. :try_end_22} :catchall_1

    goto :goto_15

    :cond_21
    move v8, v6

    move-object v6, v15

    .line 8430
    goto/16 :goto_d

    .line 8431
    :cond_22
    :try_start_23
    const-string v8, "dex store %s: failed turbodex: using fallback"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/02U;->root:Ljava/io/File;

    move-object/from16 v16, v0

    aput-object v16, v9, v14

    invoke-static {v4, v8, v9}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8432
    iput-object v4, v11, LX/02X;->fallbackCause:Ljava/lang/Throwable;

    .line 8433
    invoke-direct/range {p0 .. p0}, LX/02U;->listAndPruneRootFiles()[Ljava/lang/String;

    move-result-object v8

    .line 8434
    const/4 v4, 0x2

    move v9, v4

    move-object v14, v8

    move v8, v6

    move-object v6, v15

    .line 8435
    goto/16 :goto_d

    .line 8436
    :cond_23
    int-to-long v14, v9

    .line 8437
    :goto_17
    if-nez v8, :cond_2a

    const/4 v4, 0x1

    :goto_18
    const-string v8, "next step should be LA_LOAD_EXISTING"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v8, v9}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 8438
    and-int/lit8 v4, v10, 0x4

    if-eqz v4, :cond_2b

    const/4 v4, 0x1

    move/from16 v16, v4

    .line 8439
    :goto_19
    if-eqz v16, :cond_24

    move-object/from16 v4, p0

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    .line 8440
    invoke-direct/range {v4 .. v10}, LX/02U;->loadDexFiles(ZLX/02c;LX/02Y;LX/006;Landroid/content/Context;I)V

    .line 8441
    :cond_24
    if-eqz v5, :cond_25

    .line 8442
    if-eqz v16, :cond_2c

    .line 8443
    const-string v4, "about to start syncer thread"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8444
    new-instance v4, Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v12, v14, v15}, Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;-><init>(LX/02U;LX/02W;J)V

    .line 8445
    move-object/from16 v0, p0

    iget-object v8, v0, LX/02U;->mLockFile:LX/02V;

    invoke-virtual {v8, v4}, LX/02V;->donateLock(Ljava/lang/Thread;)V
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_1

    .line 8446
    :try_start_24
    invoke-virtual {v4}, Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;->start()V
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_6

    .line 8447
    const/4 v8, 0x0

    .line 8448
    :try_start_25
    const-string v4, "started syncer thread"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v9}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_7

    move-object v12, v8

    .line 8449
    :cond_25
    :goto_1a
    if-nez v16, :cond_26

    move-object/from16 v4, p0

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    .line 8450
    :try_start_26
    invoke-direct/range {v4 .. v10}, LX/02U;->loadDexFiles(ZLX/02c;LX/02Y;LX/006;Landroid/content/Context;I)V

    .line 8451
    :cond_26
    invoke-virtual {v6}, LX/02c;->getSchemeName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v11, LX/02X;->odexSchemeName:Ljava/lang/String;

    .line 8452
    if-eqz v5, :cond_30

    .line 8453
    or-int/lit8 v4, v13, 0x1

    .line 8454
    :goto_1b
    iget v5, v6, LX/02c;->flags:I

    and-int/lit8 v5, v5, 0x8

    if-eqz v5, :cond_27

    .line 8455
    or-int/lit8 v4, v4, 0x8

    .line 8456
    :cond_27
    invoke-virtual {v6, v14, v15}, LX/02c;->needOptimization(J)Z

    move-result v5

    if-eqz v5, :cond_2f

    .line 8457
    or-int/lit8 v4, v4, 0xa

    .line 8458
    const-string v5, "optimization needed: yes"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v7}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8459
    iget v5, v6, LX/02c;->flags:I

    and-int/lit8 v5, v5, 0x4

    if-eqz v5, :cond_28

    .line 8460
    const-string v5, "... but optimization is very expensive"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8461
    or-int/lit8 v4, v4, 0x4

    .line 8462
    :cond_28
    :goto_1c
    iput v4, v11, LX/02X;->loadResult:I

    .line 8463
    move-object/from16 v0, p0

    iput-object v11, v0, LX/02U;->mLastDeri:LX/02X;
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_1

    .line 8464
    if-eqz v12, :cond_29

    .line 8465
    invoke-virtual {v12}, LX/02W;->close()V

    :cond_29
    move-object v4, v11

    goto/16 :goto_0

    .line 8466
    :cond_2a
    const/4 v4, 0x0

    goto/16 :goto_18

    .line 8467
    :cond_2b
    const/4 v4, 0x0

    move/from16 v16, v4

    goto/16 :goto_19

    .line 8468
    :catchall_6
    move-exception v4

    .line 8469
    :try_start_27
    const-string v5, "failed to start syncer thread"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8470
    move-object/from16 v0, p0

    iget-object v5, v0, LX/02U;->mLockFile:LX/02V;

    invoke-virtual {v5}, LX/02V;->stealLock()V

    throw v4

    .line 8471
    :cond_2c
    const-string v4, "fsyncing just-regenerated dex store %s in foreground as requested"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/02U;->root:Ljava/io/File;

    move-object/from16 v17, v0

    aput-object v17, v8, v9

    invoke-static {v4, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8472
    move-object/from16 v0, p0

    iget-object v4, v0, LX/02U;->root:Ljava/io/File;

    invoke-static {}, LX/08D;->unchanged()LX/08D;

    move-result-object v8

    invoke-static {v4, v8}, LX/02Q;->fsyncRecursive(Ljava/io/File;LX/08D;)V

    .line 8473
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, LX/02U;->writeStatusLocked(J)V

    .line 8474
    const-string v4, "dex store sync completed"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8475
    iget v4, v6, LX/02c;->flags:I

    and-int/lit8 v4, v4, 0x4

    if-nez v4, :cond_2d

    .line 8476
    const-string v4, "optimizing in foreground"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8477
    const/4 v4, 0x1

    .line 8478
    :goto_1d
    if-eqz v4, :cond_25

    .line 8479
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v14, v15}, LX/02U;->optimizeInForegroundLocked(LX/02Y;J)V

    .line 8480
    invoke-static/range {p0 .. p0}, LX/02U;->readStatusLocked(LX/02U;)J

    move-result-wide v8

    .line 8481
    move-object/from16 v0, p0

    invoke-static {v0, v7, v8, v9}, LX/02U;->schemeForState(LX/02U;LX/02Y;J)LX/02c;

    move-result-object v6

    .line 8482
    const-string v4, "done optimizing in foreground: new status %x scheme %s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v14, v15

    const/4 v15, 0x1

    aput-object v6, v14, v15

    invoke-static {v4, v14}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-wide v14, v8

    goto/16 :goto_1a

    .line 8483
    :cond_2d
    and-int/lit8 v4, v10, 0x8

    if-eqz v4, :cond_2e

    .line 8484
    const-string v4, "optimizing in foreground despite expense: forced"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8485
    const/4 v4, 0x1

    goto :goto_1d

    .line 8486
    :cond_2e
    const-string v4, "not optimizing in foreground: would be too expensive"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8487
    const/4 v4, 0x0

    goto :goto_1d

    .line 8488
    :cond_2f
    const-string v5, "optimization needed: no"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_1

    goto/16 :goto_1c

    .line 8489
    :catchall_7
    move-exception v4

    move-object v5, v8

    goto/16 :goto_5

    .line 8490
    :catch_f
    move-exception v4

    goto/16 :goto_16

    :catchall_8
    move-exception v4

    goto/16 :goto_14

    .line 8491
    :catchall_9
    move-exception v4

    move-object v5, v6

    goto/16 :goto_12

    .line 8492
    :catchall_a
    move-exception v4

    goto/16 :goto_10

    .line 8493
    :catchall_b
    move-exception v4

    goto/16 :goto_6

    :cond_30
    move v4, v13

    goto/16 :goto_1b

    :cond_31
    move-object v6, v4

    move/from16 v10, p1

    goto/16 :goto_17

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private loadDexFiles(ZLX/02c;LX/02Y;LX/006;Landroid/content/Context;I)V
    .locals 19

    .prologue
    .line 8253
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p1, :cond_5

    const-string v2, "DLL2_multidex_class_loader_first_"

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 8254
    if-eqz p1, :cond_6

    const v2, 0x880009

    .line 8255
    :goto_1
    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v2}, LX/006;->a(Ljava/lang/String;I)LX/00X;

    move-result-object v17

    const/16 v16, 0x0

    .line 8256
    const/4 v3, 0x0

    .line 8257
    :try_start_0
    move-object/from16 v0, p3

    iget-boolean v2, v0, LX/02Y;->locators:Z

    if-eqz v2, :cond_0

    .line 8258
    const/4 v3, 0x1

    .line 8259
    :cond_0
    and-int/lit8 v2, p6, 0x10

    if-eqz v2, :cond_1

    .line 8260
    or-int/lit8 v3, v3, 0x2

    .line 8261
    :cond_1
    invoke-static/range {p5 .. p5}, LX/02d;->readExperimentFromDisk(Landroid/content/Context;)LX/02d;

    move-result-object v8

    .line 8262
    const-string v2, "Read DexExperiment for MDCL Configure: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v8, v4, v5

    invoke-static {v2, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8263
    const-string v2, "fb4a_sequential_access_mode_for_oat_file_enabled"

    move-object/from16 v0, p5

    invoke-static {v0, v2}, LX/00f;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    .line 8264
    const-string v2, "fb4a_random_access_mode_for_oat_file_enabled"

    move-object/from16 v0, p5

    invoke-static {v0, v2}, LX/00f;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    .line 8265
    const-string v2, "fb4a_prot_exec_for_oat_file_enabled"

    move-object/from16 v0, p5

    invoke-static {v0, v2}, LX/00f;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    .line 8266
    const/high16 v2, 0x1e00000

    const/high16 v7, 0x700000

    sget v9, LX/02e;->FB_REDEX_COLD_START_SET_DEX_COUNT:I

    mul-int/2addr v7, v9

    const/high16 v9, 0x100000

    add-int/2addr v7, v9

    invoke-static {v2, v7}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 8267
    const/4 v7, 0x0

    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 8268
    new-instance v2, LX/02f;

    invoke-direct/range {v2 .. v8}, LX/02f;-><init>(IZZZILX/02d;)V

    .line 8269
    invoke-virtual {v2}, LX/02f;->getNumberConfiguredDexFiles()I

    move-result v3

    .line 8270
    move-object/from16 v0, p0

    iget-object v9, v0, LX/02U;->root:Ljava/io/File;

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v2}, LX/02c;->configureClassLoader(Ljava/io/File;LX/02f;)V

    .line 8271
    sget-object v9, LX/02U;->sMergedDexConfig:LX/02f;

    if-nez v9, :cond_2

    .line 8272
    new-instance v9, LX/02f;

    const/4 v10, 0x0

    move v11, v4

    move v12, v5

    move v13, v6

    move v14, v7

    move-object v15, v8

    invoke-direct/range {v9 .. v15}, LX/02f;-><init>(IZZZILX/02d;)V

    sput-object v9, LX/02U;->sMergedDexConfig:LX/02f;

    .line 8273
    :cond_2
    move-object/from16 v0, p3

    invoke-static {v2, v0}, LX/02U;->mergeConfiguration(LX/02f;LX/02Y;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 8274
    :try_start_1
    invoke-static/range {p5 .. p5}, Lcom/facebook/common/dextricks/MultiDexClassLoader;->install(Landroid/content/Context;)Lcom/facebook/common/dextricks/MultiDexClassLoader;

    move-result-object v2

    sget-object v4, LX/02U;->sMergedDexConfig:LX/02f;

    invoke-virtual {v2, v4}, Lcom/facebook/common/dextricks/MultiDexClassLoader;->configure(LX/02f;)V

    .line 8275
    if-eqz p1, :cond_3

    .line 8276
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, LX/02U;->verifyCanaryClasses(LX/02Y;)V

    .line 8277
    :cond_3
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, LX/02U;->mLoadedManifest:LX/02Y;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 8278
    if-eqz v17, :cond_4

    invoke-virtual/range {v17 .. v17}, LX/00X;->close()V

    .line 8279
    :cond_4
    return-void

    .line 8280
    :cond_5
    const-string v2, "DLL2_multidex_class_loader_subsequent_"

    goto/16 :goto_0

    .line 8281
    :cond_6
    const v2, 0x880008

    goto/16 :goto_1

    .line 8282
    :catch_0
    move-exception v2

    .line 8283
    and-int/lit8 v4, p6, 0x2

    if-nez v4, :cond_7

    :try_start_2
    sget-object v4, LX/02U;->sMergedDexConfig:LX/02f;

    invoke-virtual {v4}, LX/02f;->getNumberConfiguredDexFiles()I

    move-result v4

    if-eq v4, v3, :cond_9

    :cond_7
    const/4 v3, 0x1

    move v4, v3

    .line 8284
    :goto_2
    const-string v5, "%s error in store %s scheme %s regen %s"

    const/4 v3, 0x4

    new-array v6, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    if-eqz v4, :cond_a

    const-string v3, "fatal"

    :goto_3
    aput-object v3, v6, v7

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, LX/02U;->root:Ljava/io/File;

    aput-object v7, v6, v3

    const/4 v3, 0x2

    aput-object p2, v6, v3

    const/4 v3, 0x3

    invoke-static/range {p1 .. p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v2, v5, v6}, LX/02P;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8285
    if-eqz p1, :cond_b

    .line 8286
    const-wide/16 v6, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, LX/02U;->writeStatusLocked(J)V

    .line 8287
    :goto_4
    if-eqz v4, :cond_c

    .line 8288
    new-instance v3, LX/0FW;

    invoke-direct {v3, v2}, LX/0FW;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 8289
    :catch_1
    move-exception v2

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 8290
    :catchall_0
    move-exception v3

    move-object/from16 v18, v3

    move-object v3, v2

    move-object/from16 v2, v18

    :goto_5
    if-eqz v17, :cond_8

    if-eqz v3, :cond_d

    :try_start_4
    invoke-virtual/range {v17 .. v17}, LX/00X;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    :cond_8
    :goto_6
    throw v2

    .line 8291
    :cond_9
    const/4 v3, 0x0

    move v4, v3

    goto :goto_2

    .line 8292
    :cond_a
    :try_start_5
    const-string v3, "recoverable"

    goto :goto_3

    .line 8293
    :cond_b
    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, LX/02U;->writeStatusLocked(J)V

    goto :goto_4

    .line 8294
    :catchall_1
    move-exception v2

    move-object/from16 v3, v16

    goto :goto_5

    .line 8295
    :cond_c
    const-string v3, "retrying dex store load after reset"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8296
    new-instance v3, LX/0FV;

    invoke-direct {v3, v2}, LX/0FV;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 8297
    :catch_2
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_d
    invoke-virtual/range {v17 .. v17}, LX/00X;->close()V

    goto :goto_6
.end method

.method private static mergeConfiguration(LX/02f;LX/02Y;)V
    .locals 5

    .prologue
    .line 8234
    sget-object v0, LX/02U;->sMergedDexConfig:LX/02f;

    invoke-virtual {v0}, LX/02f;->getNumberConfiguredDexFiles()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 8235
    sget-object v1, LX/02U;->sMergedDexConfig:LX/02f;

    sget-object v2, LX/02U;->sMergedDexConfig:LX/02f;

    .line 8236
    iget v3, v2, LX/02f;->configFlags:I

    move v2, v3

    .line 8237
    iget v3, p0, LX/02f;->configFlags:I

    move v3, v3

    .line 8238
    or-int/2addr v2, v3

    .line 8239
    iput v2, v1, LX/02f;->configFlags:I

    .line 8240
    sget-object v1, LX/02U;->sMergedDexConfig:LX/02f;

    iget v2, p1, LX/02Y;->locator_id:I

    .line 8241
    if-lez v2, :cond_2

    .line 8242
    iget-object v3, v1, LX/02f;->storeLocators:[I

    if-nez v3, :cond_0

    .line 8243
    add-int/lit8 v3, v2, 0x1

    new-array v3, v3, [I

    iput-object v3, v1, LX/02f;->storeLocators:[I

    .line 8244
    :cond_0
    iget-object v3, v1, LX/02f;->storeLocators:[I

    array-length v3, v3

    if-ge v3, v2, :cond_1

    .line 8245
    iget-object v3, v1, LX/02f;->storeLocators:[I

    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v3

    iput-object v3, v1, LX/02f;->storeLocators:[I

    .line 8246
    :cond_1
    iget-object v3, v1, LX/02f;->storeLocators:[I

    aput v0, v3, v2

    .line 8247
    :cond_2
    iget-object v2, p0, LX/02f;->mDexFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldalvik/system/DexFile;

    .line 8248
    sget-object v4, LX/02U;->sMergedDexConfig:LX/02f;

    .line 8249
    iget-object p1, v4, LX/02f;->mDexFiles:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8250
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 8251
    :cond_3
    iget-object v0, p0, LX/02f;->mDexFiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 8252
    return-void
.end method

.method public static declared-synchronized open(Ljava/io/File;Ljava/io/File;LX/02S;)LX/02U;
    .locals 4

    .prologue
    .line 8226
    const-class v1, LX/02U;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    .line 8227
    invoke-static {}, LX/02U;->dexStoreListHead()LX/02U;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    .line 8228
    iget-object v3, v0, LX/02U;->root:Ljava/io/File;

    invoke-virtual {v3, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 8229
    :goto_1
    monitor-exit v1

    return-object v0

    .line 8230
    :cond_0
    :try_start_1
    iget-object v0, v0, LX/02U;->next:LX/02U;

    goto :goto_0

    .line 8231
    :cond_1
    new-instance v0, LX/02U;

    invoke-direct {v0, v2, p1, p2}, LX/02U;-><init>(Ljava/io/File;Ljava/io/File;LX/02S;)V

    .line 8232
    sput-object v0, LX/02U;->sListHead:LX/02U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 8233
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private openDexIterator(LX/02Y;)LX/089;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 8212
    :try_start_0
    iget-object v0, p0, LX/02U;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/02U;->id:Ljava/lang/String;

    const-string v2, "dex"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 8213
    :cond_0
    const-string v0, "secondary.dex.jar.xzs"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8214
    :goto_0
    :try_start_1
    iget-object v2, p0, LX/02U;->mResProvider:LX/02S;

    invoke-virtual {v2, v0}, LX/02S;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 8215
    :goto_1
    if-eqz v2, :cond_3

    .line 8216
    :try_start_2
    const-string v3, "using solid xz dex store at %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8217
    new-instance v0, LX/0Fn;

    iget-object v3, p0, LX/02U;->mResProvider:LX/02S;

    invoke-direct {v0, p1, v3, v2}, LX/0Fn;-><init>(LX/02Y;LX/02S;Ljava/io/InputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 8218
    :goto_2
    if-eqz v1, :cond_1

    .line 8219
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    :cond_1
    return-object v0

    .line 8220
    :cond_2
    :try_start_3
    iget-object v0, p0, LX/02U;->id:Ljava/lang/String;

    const-string v2, ".dex.jar.xzs"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-object v2, v1

    goto :goto_1

    .line 8221
    :cond_3
    :try_start_4
    const-string v1, "using discrete file inputs for store, no file at %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8222
    new-instance v0, LX/088;

    iget-object v1, p0, LX/02U;->mResProvider:LX/02S;

    invoke-direct {v0, p1, v1}, LX/088;-><init>(LX/02Y;LX/02S;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object v1, v2

    goto :goto_2

    .line 8223
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    if-eqz v2, :cond_4

    .line 8224
    invoke-static {v2}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    :cond_4
    throw v0

    .line 8225
    :catchall_1
    move-exception v0

    goto :goto_3
.end method

.method private optimizationNeeded(JLX/02Y;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 8203
    const-wide/16 v2, 0xf

    and-long/2addr v2, p1

    long-to-int v0, v2

    int-to-byte v0, v0

    .line 8204
    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 8205
    :goto_0
    iget-object v2, p3, LX/02Y;->dexes:[LX/02Z;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 8206
    const-wide/16 v2, 0x10

    shl-long/2addr v2, v0

    and-long/2addr v2, p1

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 8207
    const-string v0, "concluding optimization needed"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8208
    const/4 v1, 0x1

    .line 8209
    :goto_1
    return v1

    .line 8210
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8211
    :cond_1
    const-string v0, "concluding optimization not needed"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_1
.end method

.method private optimizeInForegroundLocked(LX/02Y;J)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 8183
    invoke-static {p0}, LX/02U;->assertLockHeld(LX/02U;)V

    .line 8184
    invoke-static {p0, p1, p2, p3}, LX/02U;->schemeForState(LX/02U;LX/02Y;J)LX/02c;

    move-result-object v0

    .line 8185
    new-instance v2, LX/08E;

    invoke-direct {v2}, LX/08E;-><init>()V

    invoke-virtual {v2}, LX/08E;->build()LX/08F;

    move-result-object v2

    .line 8186
    new-instance v3, LX/08G;

    invoke-direct {v3, v2}, LX/08G;-><init>(LX/08F;)V

    .line 8187
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {p0, v2}, LX/02U;->getNextRecommendedOptimizationAttemptTime(LX/08F;)J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-gez v2, :cond_0

    .line 8188
    const-string v0, "... actually, not optimizing in foreground, since we failed optimization too recently"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8189
    :goto_0
    return-void

    .line 8190
    :cond_0
    :try_start_0
    new-instance v2, LX/08Z;

    invoke-direct {v2, p0, v3}, LX/08Z;-><init>(LX/02U;LX/08G;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3

    .line 8191
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, p0, v2, v3}, LX/02c;->optimize(LX/02U;LX/08Z;LX/0FQ;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 8192
    :try_start_2
    invoke-virtual {v2}, LX/08Z;->noteOptimizationSuccess()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 8193
    :try_start_3
    invoke-virtual {v2}, LX/08Z;->close()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_0
    move-exception v0

    .line 8194
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 8195
    :catch_1
    move-exception v0

    .line 8196
    :try_start_4
    invoke-virtual {v2, v0}, LX/08Z;->copeWithOptimizationFailure(Ljava/lang/Throwable;)V

    .line 8197
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 8198
    :catch_2
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 8199
    :catchall_0
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_1
    if-eqz v1, :cond_1

    :try_start_6
    invoke-virtual {v2}, LX/08Z;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0

    :goto_2
    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    .line 8200
    :catch_3
    move-exception v0

    .line 8201
    const-string v1, "foreground optimization failed; proceeding"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 8202
    :catch_4
    move-exception v2

    :try_start_8
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_1
    invoke-virtual {v2}, LX/08Z;->close()V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private pruneTemporaryDirectoriesLocked([Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 8143
    invoke-static {p0}, LX/02U;->assertLockHeld(LX/02U;)V

    move v0, v1

    .line 8144
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_7

    .line 8145
    aget-object v4, p1, v0

    .line 8146
    if-eqz v4, :cond_0

    .line 8147
    const-string v2, ".tmpdir_lock"

    invoke-virtual {v4, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 8148
    aput-object v3, p1, v0

    .line 8149
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, LX/02Q;->stripLastExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ".tmpdir"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, LX/02U;->findInArray([Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 8150
    if-ltz v5, :cond_a

    .line 8151
    aget-object v2, p1, v5

    .line 8152
    aput-object v3, p1, v5

    .line 8153
    :goto_1
    if-eqz v4, :cond_5

    if-eqz v2, :cond_5

    .line 8154
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, LX/02U;->root:Ljava/io/File;

    invoke-direct {v5, v6, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8155
    invoke-static {v5}, LX/02V;->open(Ljava/io/File;)LX/02V;

    move-result-object v4

    .line 8156
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v4, v6}, LX/02V;->tryAcquire(I)LX/02W;

    move-result-object v6

    .line 8157
    if-nez v6, :cond_2

    .line 8158
    const-string v5, "tmpdir %s in use: not deleting"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-static {v5, v6}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 8159
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/02V;->close()V

    .line 8160
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8161
    :cond_1
    const-string v2, ".tmpdir"

    invoke-virtual {v4, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 8162
    aput-object v3, p1, v0

    .line 8163
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, LX/02Q;->stripLastExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ".tmpdir_lock"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, LX/02U;->findInArray([Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 8164
    if-ltz v5, :cond_8

    .line 8165
    aget-object v2, p1, v5

    .line 8166
    aput-object v3, p1, v5

    move-object v11, v4

    move-object v4, v2

    move-object v2, v11

    goto :goto_1

    .line 8167
    :cond_2
    :try_start_1
    const-string v7, "tmpdir %s (lockfile %s) is abandoned: deleting"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v9, 0x1

    aput-object v2, v8, v9

    invoke-static {v7, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8168
    invoke-static {v5}, LX/02Q;->deleteRecursive(Ljava/io/File;)V

    .line 8169
    new-instance v5, Ljava/io/File;

    iget-object v7, p0, LX/02U;->root:Ljava/io/File;

    invoke-direct {v5, v7, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v5}, LX/02Q;->deleteRecursive(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8170
    :try_start_2
    invoke-virtual {v6}, LX/02W;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 8171
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/02V;->close()V

    goto :goto_2

    .line 8172
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v6}, LX/02W;->close()V

    throw v0
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 8173
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 8174
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_3
    if-eqz v4, :cond_3

    if-eqz v3, :cond_4

    :try_start_5
    invoke-virtual {v4}, LX/02V;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    :cond_3
    :goto_4
    throw v0

    :catch_1
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_4
    invoke-virtual {v4}, LX/02V;->close()V

    goto :goto_4

    .line 8175
    :cond_5
    if-eqz v4, :cond_6

    .line 8176
    const-string v2, "tmpdir lockfile %s is orphaned: deleting"

    new-array v5, v10, [Ljava/lang/Object;

    aput-object v4, v5, v1

    invoke-static {v2, v5}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8177
    new-instance v2, Ljava/io/File;

    iget-object v5, p0, LX/02U;->root:Ljava/io/File;

    invoke-direct {v2, v5, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v2}, LX/02Q;->deleteRecursive(Ljava/io/File;)V

    goto/16 :goto_2

    .line 8178
    :cond_6
    if-eqz v2, :cond_0

    .line 8179
    const-string v4, "tmpdir %s is orphaned without its lockfile: deleting"

    new-array v5, v10, [Ljava/lang/Object;

    aput-object v2, v5, v1

    invoke-static {v4, v5}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8180
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, LX/02U;->root:Ljava/io/File;

    invoke-direct {v4, v5, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v4}, LX/02Q;->deleteRecursive(Ljava/io/File;)V

    goto/16 :goto_2

    .line 8181
    :cond_7
    return-void

    .line 8182
    :catchall_2
    move-exception v0

    goto :goto_3

    :cond_8
    move-object v2, v4

    move-object v4, v3

    goto/16 :goto_1

    :cond_9
    move-object v2, v3

    move-object v4, v3

    goto/16 :goto_1

    :cond_a
    move-object v2, v3

    goto/16 :goto_1
.end method

.method private readCurrentDepBlock()[B
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 8104
    :try_start_0
    iget-object v1, p0, LX/02U;->mApk:Ljava/io/File;

    invoke-static {v1}, LX/02U;->determineOdexCacheName(Ljava/io/File;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 8105
    :goto_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 8106
    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 8107
    :try_start_2
    invoke-direct {p0}, LX/02U;->getParents()[LX/02U;

    move-result-object v5

    array-length v6, v5

    move v1, v3

    :goto_1
    if-ge v1, v6, :cond_0

    aget-object v7, v5, v1

    .line 8108
    invoke-direct {v7}, LX/02U;->readCurrentDepBlock()[B

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/os/Parcel;->writeByteArray([B)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 8109
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 8110
    :catch_0
    move-exception v1

    .line 8111
    const-string v4, "error reading odex cache file %s"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v0, v5, v3

    invoke-static {v1, v4, v5}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 8112
    :cond_0
    :try_start_3
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/dextricks/DalvikInternals;->readOdexDepBlock(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/os/Parcel;->writeByteArray([B)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v2

    .line 8113
    :goto_2
    if-nez v0, :cond_3

    .line 8114
    :try_start_4
    iget-object v0, p0, LX/02U;->mApk:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    .line 8115
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 8116
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to get modtime of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/02U;->mApk:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 8117
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 8118
    :catch_1
    move-exception v0

    .line 8119
    :try_start_5
    const-string v1, "could not read odex dep block: using modtime: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v2, v5

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    :cond_1
    move v0, v3

    goto :goto_2

    .line 8120
    :cond_2
    iget-object v2, p0, LX/02U;->mApk:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 8121
    invoke-virtual {v4, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 8122
    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 8123
    :cond_3
    sget-boolean v0, LX/01A;->a:Z

    if-eqz v0, :cond_4

    .line 8124
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 8125
    :cond_4
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/02U;->root:Ljava/io/File;

    const-string v2, "config"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8126
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 8127
    invoke-static {p0}, LX/02U;->readDexStoreConfig(LX/02U;)LX/080;

    move-result-object v0

    .line 8128
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 8129
    iget-byte v2, v0, LX/080;->mode:B

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 8130
    iget-byte v2, v0, LX/080;->sync:B

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 8131
    iget-byte v2, v0, LX/080;->dalvikVerify:B

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 8132
    iget-byte v2, v0, LX/080;->dalvikOptimize:B

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 8133
    iget-byte v2, v0, LX/080;->dalvikRegisterMaps:B

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 8134
    iget-byte v2, v0, LX/080;->artFilter:B

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 8135
    iget v2, v0, LX/080;->artHugeMethodMax:I

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 8136
    iget v2, v0, LX/080;->artLargeMethodMax:I

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 8137
    iget v2, v0, LX/080;->artSmallMethodMax:I

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 8138
    iget v2, v0, LX/080;->artTinyMethodMax:I

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 8139
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v1

    move-object v0, v1

    .line 8140
    invoke-virtual {v4, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 8141
    :cond_5
    invoke-virtual {v4}, Landroid/os/Parcel;->marshall()[B
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 8142
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public static readDexStoreConfig(LX/02U;)LX/080;
    .locals 3

    .prologue
    .line 7943
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/02U;->root:Ljava/io/File;

    const-string v2, "config"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 7944
    :try_start_0
    invoke-static {v0}, LX/080;->read(Ljava/io/File;)LX/080;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 7945
    :goto_0
    return-object v0

    .line 7946
    :catch_0
    new-instance v0, LX/081;

    invoke-direct {v0}, LX/081;-><init>()V

    invoke-virtual {v0}, LX/081;->build()LX/080;

    move-result-object v0

    goto :goto_0
.end method

.method private readSavedDepBlock()[B
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 7927
    new-instance v2, Ljava/io/File;

    iget-object v1, p0, LX/02U;->root:Ljava/io/File;

    const-string v3, "deps"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 7928
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v1, "r"

    invoke-direct {v3, v2, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7929
    :try_start_1
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    .line 7930
    const-wide/32 v6, 0x1000000

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 7931
    const-string v1, "saved dep block file is way too big (%s bytes): considering invalid"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v6

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7932
    invoke-static {v3}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    :goto_0
    return-object v0

    .line 7933
    :catch_0
    const-string v3, "unable to open deps file %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    .line 7934
    invoke-static {v3, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7935
    goto :goto_0

    .line 7936
    :cond_0
    long-to-int v1, v4

    :try_start_2
    new-array v1, v1, [B

    .line 7937
    invoke-virtual {v3, v1}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v6

    .line 7938
    int-to-long v8, v6

    cmp-long v7, v8, v4

    if-gez v7, :cond_1

    .line 7939
    const-string v1, "short read of dep block %s: wanted %s bytes; got %s: considering invalid"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    const/4 v2, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v2

    const/4 v2, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v2

    invoke-static {v1, v7}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 7940
    invoke-static {v3}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    goto :goto_0

    .line 7941
    :cond_1
    :try_start_3
    const-string v0, "read saved dep file %s (%s bytes)"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v2, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v2

    invoke-static {v0, v6}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 7942
    invoke-static {v3}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v3}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    throw v0
.end method

.method public static readStatusLocked(LX/02U;)J
    .locals 14

    .prologue
    const-wide/16 v0, 0x0

    const/16 v8, 0x10

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 7947
    invoke-static {p0}, LX/02U;->assertLockHeld(LX/02U;)V

    .line 7948
    new-instance v4, Ljava/io/File;

    iget-object v2, p0, LX/02U;->root:Ljava/io/File;

    const-string v3, "mdex_status2"

    invoke-direct {v4, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 7949
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7950
    const/16 v2, 0x10

    :try_start_1
    new-array v2, v2, [B

    .line 7951
    const/4 v3, 0x0

    const/16 v6, 0x10

    invoke-virtual {v5, v2, v3, v6}, Ljava/io/FileInputStream;->read([BII)I

    move-result v3

    if-ge v3, v8, :cond_0

    .line 7952
    const-string v2, "status file %s too short: treating as zero"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v3, v6

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7953
    invoke-static {v4}, LX/02Q;->deleteRecursiveNoThrow(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7954
    invoke-static {v5}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    :goto_0
    return-wide v0

    .line 7955
    :catch_0
    const-string v2, "status file %s not found: treating as zero"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v4, v3, v6

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 7956
    :cond_0
    :try_start_2
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 7957
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v2

    .line 7958
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v6

    .line 7959
    const-string v8, "read status:%x check:%x str:%s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v2, v3}, LX/02U;->getStatusDescription(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7960
    const-wide v8, -0x5314ff805314ff9L

    xor-long/2addr v8, v2

    cmp-long v8, v8, v6

    if-eqz v8, :cond_1

    .line 7961
    const-string v8, "check mismatch: status:%x expected-check:%x actual-check:%x"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-wide v12, -0x5314ff805314ff9L

    xor-long/2addr v2, v12

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v2, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v9, v2

    invoke-static {v8, v9}, LX/02P;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7962
    invoke-static {v4}, LX/02Q;->deleteRecursiveNoThrow(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 7963
    invoke-static {v5}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_1
    invoke-static {v5}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    move-wide v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v5}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    throw v0
.end method

.method private runCompiler(LX/02Y;LX/02c;I)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 7754
    invoke-virtual {p2, p0, p3}, LX/02c;->makeCompiler(LX/02U;I)LX/084;

    move-result-object v3

    .line 7755
    :try_start_0
    invoke-direct {p0, p1}, LX/02U;->openDexIterator(LX/02Y;)LX/089;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    move-result-object v4

    .line 7756
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v4}, LX/089;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 7757
    invoke-virtual {v4}, LX/089;->next()LX/08A;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v5

    .line 7758
    :try_start_2
    const-string v0, "compiling %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v5, v1, v6

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7759
    invoke-virtual {v3, v5}, LX/084;->compile(LX/08A;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    .line 7760
    if-eqz v5, :cond_0

    :try_start_3
    invoke-virtual {v5}, LX/08A;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_0

    .line 7761
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 7762
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    if-eqz v4, :cond_1

    if-eqz v1, :cond_8

    :try_start_5
    invoke-virtual {v4}, LX/089;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    :cond_1
    :goto_2
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 7763
    :catch_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 7764
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v3, :cond_2

    if-eqz v2, :cond_9

    :try_start_8
    invoke-virtual {v3}, LX/084;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_5

    :cond_2
    :goto_4
    throw v0

    .line 7765
    :catch_2
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 7766
    :catchall_2
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_5
    if-eqz v5, :cond_3

    if-eqz v1, :cond_4

    :try_start_a
    invoke-virtual {v5}, LX/08A;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :cond_3
    :goto_6
    :try_start_b
    throw v0

    .line 7767
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_1

    .line 7768
    :catch_3
    move-exception v5

    invoke-static {v1, v5}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_4
    invoke-virtual {v5}, LX/08A;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_6

    .line 7769
    :cond_5
    if-eqz v4, :cond_6

    :try_start_c
    invoke-virtual {v4}, LX/089;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 7770
    :cond_6
    if-eqz v3, :cond_7

    invoke-virtual {v3}, LX/084;->close()V

    .line 7771
    :cond_7
    return-void

    .line 7772
    :catch_4
    move-exception v4

    :try_start_d
    invoke-static {v1, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 7773
    :catchall_4
    move-exception v0

    goto :goto_3

    .line 7774
    :cond_8
    invoke-virtual {v4}, LX/089;->close()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    goto :goto_2

    .line 7775
    :catch_5
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_9
    invoke-virtual {v3}, LX/084;->close()V

    goto :goto_4

    .line 7776
    :catchall_5
    move-exception v0

    move-object v1, v2

    goto :goto_5
.end method

.method private static saveDeps(LX/02U;)V
    .locals 7

    .prologue
    .line 7744
    invoke-direct {p0}, LX/02U;->readCurrentDepBlock()[B

    move-result-object v0

    .line 7745
    new-instance v2, Ljava/io/File;

    iget-object v1, p0, LX/02U;->root:Ljava/io/File;

    const-string v3, "deps"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 7746
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v1, "rw"

    invoke-direct {v3, v2, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 7747
    :try_start_0
    invoke-virtual {v3, v0}, Ljava/io/RandomAccessFile;->write([B)V

    .line 7748
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/io/RandomAccessFile;->setLength(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 7749
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V

    .line 7750
    const-string v0, "saved deps file %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7751
    return-void

    .line 7752
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7753
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_0
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private static schemeForState(LX/02U;LX/02Y;J)LX/02c;
    .locals 4

    .prologue
    .line 7735
    iget-object v1, p1, LX/02Y;->dexes:[LX/02Z;

    .line 7736
    const-wide/16 v2, 0xf

    and-long/2addr v2, p2

    long-to-int v0, v2

    int-to-byte v0, v0

    packed-switch v0, :pswitch_data_0

    .line 7737
    :pswitch_0
    new-instance v0, LX/07z;

    invoke-direct {v0}, LX/07z;-><init>()V

    :goto_0
    return-object v0

    .line 7738
    :pswitch_1
    new-instance v0, LX/0Fe;

    invoke-direct {v0, v1}, LX/0Fe;-><init>([LX/02Z;)V

    goto :goto_0

    .line 7739
    :pswitch_2
    new-instance v0, LX/02b;

    invoke-direct {v0, v1}, LX/02b;-><init>([LX/02Z;)V

    goto :goto_0

    .line 7740
    :pswitch_3
    new-instance v0, LX/02a;

    invoke-direct {v0, v1}, LX/02a;-><init>([LX/02Z;)V

    goto :goto_0

    .line 7741
    :pswitch_4
    new-instance v0, LX/0Fb;

    invoke-direct {v0, v1}, LX/0Fb;-><init>([LX/02Z;)V

    goto :goto_0

    .line 7742
    :pswitch_5
    new-instance v0, LX/0Fc;

    iget-object v2, p0, LX/02U;->mApk:Ljava/io/File;

    invoke-direct {v0, v1, v2, p2, p3}, LX/0Fc;-><init>([LX/02Z;Ljava/io/File;J)V

    goto :goto_0

    .line 7743
    :pswitch_6
    new-instance v0, LX/0Fg;

    invoke-direct {v0}, LX/0Fg;-><init>()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static setDifference(LX/02U;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 7725
    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 7726
    aget-object v3, p1, v0

    .line 7727
    if-eqz v3, :cond_0

    move v2, v1

    .line 7728
    :goto_1
    array-length v4, p2

    if-ge v2, v4, :cond_0

    .line 7729
    aget-object v4, p2, v2

    .line 7730
    if-eqz v4, :cond_1

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 7731
    const/4 v2, 0x0

    aput-object v2, p1, v0

    .line 7732
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7733
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 7734
    :cond_2
    return-void
.end method

.method private static touchRegenStamp(LX/02U;)V
    .locals 4

    .prologue
    .line 7720
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/02U;->root:Ljava/io/File;

    const-string v2, "regen_stamp"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 7721
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 7722
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 7723
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "could not set modtime of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7724
    :cond_0
    return-void
.end method

.method private verifyCanaryClasses(LX/02Y;)V
    .locals 2

    .prologue
    .line 7716
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, LX/02Y;->dexes:[LX/02Z;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 7717
    iget-object v1, p1, LX/02Y;->dexes:[LX/02Z;

    aget-object v1, v1, v0

    iget-object v1, v1, LX/02Z;->canaryClass:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 7718
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7719
    :cond_0
    return-void
.end method

.method public static writeTxFailedStatusLocked(LX/02U;J)V
    .locals 5

    .prologue
    .line 7714
    const-wide/16 v0, 0x1

    const/4 v2, 0x4

    shl-long v2, p1, v2

    or-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, LX/02U;->writeStatusLocked(J)V

    .line 7715
    return-void
.end method


# virtual methods
.method public final atomicReplaceConfig(LX/080;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 7684
    iget-object v1, p0, LX/02U;->mLockFile:LX/02V;

    invoke-virtual {v1, v0}, LX/02V;->acquire(I)LX/02W;

    move-result-object v4

    const/4 v1, 0x0

    .line 7685
    :try_start_0
    new-instance v5, Ljava/io/File;

    iget-object v2, p0, LX/02U;->root:Ljava/io/File;

    const-string v3, "config"

    invoke-direct {v5, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 7686
    invoke-virtual {p0}, LX/02U;->readConfig()LX/080;

    move-result-object v2

    .line 7687
    invoke-virtual {p1, v2}, LX/080;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 7688
    invoke-virtual {p0}, LX/02U;->attemptedOptimizationSinceRegeneration()Z

    move-result v6

    if-eqz v6, :cond_6

    move v2, v3

    .line 7689
    :goto_0
    if-eqz v2, :cond_1

    if-nez v3, :cond_1

    .line 7690
    invoke-static {p0}, LX/02U;->checkDeps(LX/02U;)Z

    move-result v6

    if-nez v6, :cond_0

    move v2, v0

    .line 7691
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, LX/02U;->attemptedOptimizationSinceRegeneration()Z

    move-result v6

    if-eqz v6, :cond_1

    move v2, v0

    .line 7692
    :cond_1
    new-instance v6, LX/081;

    invoke-direct {v6}, LX/081;-><init>()V

    invoke-virtual {v6}, LX/081;->build()LX/080;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/080;->equals(Ljava/lang/Object;)Z

    move-result v6

    move v6, v6

    .line 7693
    if-eqz v6, :cond_7

    .line 7694
    invoke-static {v5}, LX/02Q;->deleteRecursive(Ljava/io/File;)V

    .line 7695
    :goto_1
    if-nez v2, :cond_2

    .line 7696
    iget-object v5, p0, LX/02U;->root:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    const/4 v6, -0x1

    invoke-static {v5, v6}, Lcom/facebook/common/dextricks/DalvikInternals;->fsyncNamed(Ljava/lang/String;I)V

    .line 7697
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 7698
    :try_start_1
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, LX/02U;->root:Ljava/io/File;

    const-string v7, "regen_stamp"

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 7699
    invoke-static {p0}, LX/02U;->touchRegenStamp(LX/02U;)V

    .line 7700
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 7701
    :cond_2
    if-eqz v2, :cond_3

    if-nez v3, :cond_3

    .line 7702
    :try_start_2
    invoke-static {p0}, LX/02U;->saveDeps(LX/02U;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 7703
    :cond_3
    if-nez v2, :cond_4

    const/4 v0, 0x1

    .line 7704
    :cond_4
    if-eqz v4, :cond_5

    invoke-virtual {v4}, LX/02W;->close()V

    :cond_5
    return v0

    .line 7705
    :cond_6
    :try_start_3
    iget-byte v6, v2, LX/080;->mode:B

    iget-byte v7, p1, LX/080;->mode:B

    if-ne v6, v7, :cond_a

    iget-byte v6, v2, LX/080;->sync:B

    iget-byte v7, p1, LX/080;->sync:B

    if-ne v6, v7, :cond_a

    const/4 v6, 0x1

    :goto_2
    move v2, v6

    .line 7706
    goto :goto_0

    .line 7707
    :cond_7
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, LX/02U;->root:Ljava/io/File;

    const-string v8, "config.tmp"

    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 7708
    invoke-virtual {p1, v6}, LX/080;->writeAndSync(Ljava/io/File;)V

    .line 7709
    invoke-static {v6, v5}, LX/02Q;->renameOrThrow(Ljava/io/File;Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_1

    .line 7710
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 7711
    :catchall_0
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_3
    if-eqz v4, :cond_8

    if-eqz v1, :cond_9

    :try_start_5
    invoke-virtual {v4}, LX/02W;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    :cond_8
    :goto_4
    throw v0

    .line 7712
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 7713
    :catchall_2
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_9
    invoke-virtual {v4}, LX/02W;->close()V

    goto :goto_4

    :cond_a
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public final attemptedOptimizationSinceRegeneration()Z
    .locals 3

    .prologue
    .line 7683
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/02U;->root:Ljava/io/File;

    const-string v2, "optimization_log"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public final forceRegenerateOnNextLoad()V
    .locals 6

    .prologue
    .line 7677
    iget-object v0, p0, LX/02U;->mLockFile:LX/02V;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/02V;->acquire(I)LX/02W;

    move-result-object v2

    const/4 v1, 0x0

    .line 7678
    const-wide/16 v4, 0x6

    :try_start_0
    invoke-virtual {p0, v4, v5}, LX/02U;->writeStatusLocked(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7679
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/02W;->close()V

    .line 7680
    :cond_0
    return-void

    .line 7681
    :catch_0
    move-exception v1

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7682
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/02W;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_0
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, LX/02W;->close()V

    goto :goto_0
.end method

.method public final getAndClearCompletedOptimizationLog()LX/08a;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 7666
    new-instance v2, Ljava/io/File;

    iget-object v1, p0, LX/02U;->root:Ljava/io/File;

    const-string v3, "optimization_log"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 7667
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 7668
    :cond_0
    :goto_0
    return-object v0

    .line 7669
    :cond_1
    iget-object v1, p0, LX/02U;->mLockFile:LX/02V;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/02V;->acquire(I)LX/02W;

    move-result-object v3

    .line 7670
    :try_start_0
    invoke-static {v2}, LX/08a;->readOrMakeDefault(Ljava/io/File;)LX/08a;

    move-result-object v1

    .line 7671
    iget v4, v1, LX/08a;->flags:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    and-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_2

    .line 7672
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/02W;->close()V

    goto :goto_0

    .line 7673
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 7674
    if-eqz v3, :cond_3

    invoke-virtual {v3}, LX/02W;->close()V

    :cond_3
    move-object v0, v1

    goto :goto_0

    .line 7675
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 7676
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v3, :cond_4

    if-eqz v1, :cond_5

    :try_start_3
    invoke-virtual {v3}, LX/02W;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_4
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_5
    invoke-virtual {v3}, LX/02W;->close()V

    goto :goto_2

    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_1
.end method

.method public final getDependencyOdexFiles()[Ljava/io/File;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 7655
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 7656
    iget-object v0, p0, LX/02U;->mApk:Ljava/io/File;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7657
    iget-object v0, p0, LX/02U;->mApk:Ljava/io/File;

    invoke-static {v0}, LX/02U;->determineOdexCacheName(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7658
    invoke-direct {p0}, LX/02U;->getParents()[LX/02U;

    move-result-object v4

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v0, v4, v2

    .line 7659
    invoke-virtual {v0}, LX/02U;->getDependencyOdexFiles()[Ljava/io/File;

    move-result-object v6

    array-length v7, v6

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_0

    aget-object v8, v6, v0

    .line 7660
    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7661
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 7662
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 7663
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/io/File;

    .line 7664
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 7665
    return-object v0
.end method

.method public final declared-synchronized getLastRegenTime()J
    .locals 3

    .prologue
    .line 7654
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/02U;->root:Ljava/io/File;

    const-string v2, "regen_stamp"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getNextRecommendedOptimizationAttemptTime(LX/08F;)J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 7777
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, LX/02U;->root:Ljava/io/File;

    const-string v4, "optimization_log"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 7778
    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 7779
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 7780
    const-string v2, "ignoring optimization log file from the future"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move-wide v2, v0

    .line 7781
    :cond_0
    cmp-long v4, v2, v0

    if-lez v4, :cond_1

    iget v0, p1, LX/08F;->timeBetweenOptimizationAttemptsMs:I

    int-to-long v0, v0

    add-long/2addr v0, v2

    :cond_1
    return-wide v0
.end method

.method public final getOdexCachePath()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 7782
    iget-object v0, p0, LX/02U;->mApk:Ljava/io/File;

    invoke-static {v0}, LX/02U;->determineOdexCacheName(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7783
    iget-object v0, p0, LX/02U;->mApk:Ljava/io/File;

    invoke-static {v0}, LX/02U;->determineOdexCacheName(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 7784
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getParentNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 7785
    invoke-virtual {p0}, LX/02U;->loadManifest()LX/02Y;

    .line 7786
    iget-object v0, p0, LX/02U;->mManifest:LX/02Y;

    iget-object v0, v0, LX/02Y;->requires:[Ljava/lang/String;

    return-object v0
.end method

.method public final hasChildren()Z
    .locals 1

    .prologue
    .line 7787
    iget-object v0, p0, LX/02U;->mChildStores:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized isLoaded()Z
    .locals 1

    .prologue
    .line 7788
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/02U;->mLoadedManifest:LX/02Y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized loadAll(ILX/006;Landroid/content/Context;)LX/02X;
    .locals 5

    .prologue
    .line 7789
    monitor-enter p0

    :try_start_0
    const-string v0, "DLL2_dexstore_load_all"

    const v1, 0x880003

    invoke-virtual {p2, v0, v1}, LX/006;->a(Ljava/lang/String;I)LX/00X;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    const/4 v1, 0x0

    .line 7790
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, LX/02U;->loadAllImpl(ILX/006;Landroid/content/Context;)LX/02X;
    :try_end_1
    .catch LX/0FV; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v0

    .line 7791
    :goto_0
    if-eqz v2, :cond_0

    :try_start_2
    invoke-virtual {v2}, LX/00X;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    monitor-exit p0

    return-object v0

    .line 7792
    :catch_0
    move-exception v3

    .line 7793
    or-int/lit8 v0, p1, 0x2

    :try_start_3
    invoke-direct {p0, v0, p2, p3}, LX/02U;->loadAllImpl(ILX/006;Landroid/content/Context;)LX/02X;

    move-result-object v0

    .line 7794
    iput-object v3, v0, LX/02X;->regenRetryCause:Ljava/lang/Throwable;
    :try_end_3
    .catch LX/0FV; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_0

    .line 7795
    :catch_1
    move-exception v0

    .line 7796
    :try_start_4
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 7797
    :catch_2
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 7798
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_6
    invoke-virtual {v2}, LX/00X;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_1
    :goto_2
    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 7799
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 7800
    :catch_3
    move-exception v2

    :try_start_8
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, LX/00X;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_1
.end method

.method public final loadManifest()LX/02Y;
    .locals 4

    .prologue
    .line 7801
    iget-object v0, p0, LX/02U;->mManifest:LX/02Y;

    if-nez v0, :cond_3

    .line 7802
    monitor-enter p0

    .line 7803
    :try_start_0
    iget-object v0, p0, LX/02U;->mManifest:LX/02Y;

    if-nez v0, :cond_2

    .line 7804
    iget-object v0, p0, LX/02U;->mResProvider:LX/02S;

    const-string v1, "metadata.txt"

    invoke-virtual {v0, v1}, LX/02S;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    const/4 v1, 0x0

    .line 7805
    :try_start_1
    new-instance v0, LX/02Y;

    invoke-direct {v0, v2}, LX/02Y;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 7806
    if-eqz v2, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 7807
    :cond_0
    iget-boolean v1, v0, LX/02Y;->rootRelative:Z

    if-eqz v1, :cond_1

    .line 7808
    iget-object v1, p0, LX/02U;->mResProvider:LX/02S;

    invoke-virtual {v1}, LX/02S;->markRootRelative()V

    .line 7809
    :cond_1
    iget-object v1, v0, LX/02Y;->id:Ljava/lang/String;

    iput-object v1, p0, LX/02U;->id:Ljava/lang/String;

    .line 7810
    iput-object v0, p0, LX/02U;->mManifest:LX/02Y;

    .line 7811
    :cond_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 7812
    :cond_3
    iget-object v0, p0, LX/02U;->mManifest:LX/02Y;

    return-object v0

    .line 7813
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 7814
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v2, :cond_4

    if-eqz v1, :cond_5

    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_4
    :goto_1
    :try_start_5
    throw v0

    .line 7815
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 7816
    :catch_1
    move-exception v2

    :try_start_6
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    :catchall_2
    move-exception v0

    goto :goto_0
.end method

.method public final makeTemporaryDirectory(Ljava/lang/String;)LX/087;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 7817
    iget-object v0, p0, LX/02U;->mLockFile:LX/02V;

    invoke-virtual {v0, v1}, LX/02V;->acquire(I)LX/02W;

    move-result-object v6

    .line 7818
    :try_start_0
    const-string v0, ".tmpdir_lock"

    iget-object v1, p0, LX/02U;->root:Ljava/io/File;

    invoke-static {p1, v0, v1}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move-result-object v5

    .line 7819
    :try_start_1
    new-instance v4, Ljava/io/File;

    iget-object v0, p0, LX/02U;->root:Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/02Q;->stripLastExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ".tmpdir"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 7820
    invoke-static {v4}, LX/02Q;->mkdirOrThrow(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 7821
    :try_start_2
    invoke-static {v5}, LX/02V;->open(Ljava/io/File;)LX/02V;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    move-result-object v3

    .line 7822
    const/4 v0, 0x1

    :try_start_3
    invoke-virtual {v3, v0}, LX/02V;->tryAcquire(I)LX/02W;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    move-result-object v1

    .line 7823
    if-nez v1, :cond_1

    .line 7824
    :try_start_4
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v7, "should have been able to acquire tmpdir lock"

    invoke-direct {v0, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 7825
    :catchall_0
    move-exception v0

    :goto_0
    :try_start_5
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 7826
    invoke-static {v3}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 7827
    invoke-static {v5}, LX/02Q;->deleteRecursiveNoThrow(Ljava/io/File;)V

    .line 7828
    invoke-static {v4}, LX/02Q;->deleteRecursiveNoThrow(Ljava/io/File;)V

    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 7829
    :catch_0
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 7830
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_1
    if-eqz v6, :cond_0

    if-eqz v2, :cond_3

    :try_start_7
    invoke-virtual {v6}, LX/02W;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1

    :cond_0
    :goto_2
    throw v0

    .line 7831
    :cond_1
    :try_start_8
    new-instance v0, LX/087;

    invoke-direct {v0, p0, v1, v4}, LX/087;-><init>(LX/02U;LX/02W;Ljava/io/File;)V

    .line 7832
    const-string v7, "created tmpdir %s (lock file %s)"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, v0, LX/087;->directory:Ljava/io/File;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, v3, LX/02V;->lockFileName:Ljava/io/File;

    aput-object v10, v8, v9

    invoke-static {v7, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 7833
    const/4 v1, 0x0

    :try_start_9
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 7834
    const/4 v1, 0x0

    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 7835
    const/4 v1, 0x0

    invoke-static {v1}, LX/02Q;->deleteRecursiveNoThrow(Ljava/io/File;)V

    .line 7836
    const/4 v1, 0x0

    invoke-static {v1}, LX/02Q;->deleteRecursiveNoThrow(Ljava/io/File;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 7837
    if-eqz v6, :cond_2

    invoke-virtual {v6}, LX/02W;->close()V

    :cond_2
    return-object v0

    :catch_1
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v6}, LX/02W;->close()V

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_1

    .line 7838
    :catchall_3
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    goto :goto_0

    :catchall_4
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    move-object v4, v2

    goto :goto_0

    :catchall_5
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    goto :goto_0

    :catchall_6
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public final optimize(LX/08G;LX/0FQ;)V
    .locals 8
    .param p2    # LX/0FQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 7839
    iget-object v0, p0, LX/02U;->mLoadedManifest:LX/02Y;

    .line 7840
    if-nez v0, :cond_0

    .line 7841
    invoke-virtual {p0}, LX/02U;->loadManifest()LX/02Y;

    move-result-object v0

    .line 7842
    :cond_0
    const-string v2, "[opt] loaded manifets"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7843
    iget-object v2, p0, LX/02U;->mLockFile:LX/02V;

    invoke-virtual {v2, v6}, LX/02V;->acquireInterruptubly(I)LX/02W;

    move-result-object v2

    .line 7844
    const-string v3, "[opt] locked dex store %s"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LX/02U;->root:Ljava/io/File;

    aput-object v5, v4, v6

    invoke-static {v3, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7845
    :try_start_0
    invoke-static {p0}, LX/02U;->readStatusLocked(LX/02U;)J

    move-result-wide v4

    .line 7846
    invoke-static {p0, v0, v4, v5}, LX/02U;->schemeForState(LX/02U;LX/02Y;J)LX/02c;

    move-result-object v0

    .line 7847
    const-string v3, "[opt] found scheme %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7848
    invoke-static {p0}, LX/02U;->checkDeps(LX/02U;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 7849
    new-instance v0, LX/08c;

    const-string v1, "attempt to optimize stale repository"

    invoke-direct {v0, v1}, LX/08c;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7850
    :catch_0
    move-exception v0

    .line 7851
    :goto_0
    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7852
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v2, :cond_1

    .line 7853
    invoke-virtual {v2}, LX/02W;->close()V

    :cond_1
    throw v0

    .line 7854
    :cond_2
    :try_start_2
    new-instance v3, LX/08Z;

    invoke-direct {v3, p0, p1}, LX/08Z;-><init>(LX/02U;LX/08G;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 7855
    :try_start_3
    const-string v4, "[opt] opened optimization session"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7856
    invoke-virtual {v2}, LX/02W;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 7857
    :try_start_4
    invoke-virtual {v0, p0, v3, p2}, LX/02c;->optimize(LX/02U;LX/08Z;LX/0FQ;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 7858
    :try_start_5
    invoke-virtual {v3}, LX/08Z;->noteOptimizationSuccess()V

    .line 7859
    const-string v0, "[opt] finished optimization session"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 7860
    :try_start_6
    invoke-virtual {v3}, LX/08Z;->close()V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 7861
    return-void

    .line 7862
    :catch_1
    move-exception v0

    .line 7863
    :try_start_7
    invoke-virtual {v3, v0}, LX/08Z;->copeWithOptimizationFailure(Ljava/lang/Throwable;)V

    .line 7864
    throw v0
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 7865
    :catch_2
    move-exception v0

    :goto_2
    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 7866
    :catchall_1
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v7

    :goto_3
    if-eqz v1, :cond_3

    :try_start_9
    invoke-virtual {v3}, LX/08Z;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :goto_4
    :try_start_a
    throw v0
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 7867
    :catch_3
    move-exception v0

    .line 7868
    :goto_5
    :try_start_b
    const-string v1, "[opt] optimization failed!"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v3}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7869
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 7870
    :catch_4
    move-exception v3

    :try_start_c
    invoke-static {v1, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_3
    invoke-virtual {v3}, LX/08Z;->close()V
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_4

    .line 7871
    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_1

    .line 7872
    :catch_5
    move-exception v0

    move-object v2, v1

    goto :goto_5

    .line 7873
    :catch_6
    move-exception v0

    move-object v2, v1

    goto :goto_0

    .line 7874
    :catchall_3
    move-exception v0

    goto :goto_3

    :catchall_4
    move-exception v0

    move-object v2, v1

    goto :goto_3

    .line 7875
    :catch_7
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method public final pruneTemporaryDirectories()V
    .locals 6

    .prologue
    .line 7876
    iget-object v0, p0, LX/02U;->mLockFile:LX/02V;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/02V;->acquire(I)LX/02W;

    move-result-object v2

    const/4 v1, 0x0

    .line 7877
    :try_start_0
    iget-object v0, p0, LX/02U;->root:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 7878
    if-nez v0, :cond_1

    .line 7879
    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unable to list directory "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/02U;->root:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 7880
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7881
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    if-eqz v2, :cond_0

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v2}, LX/02W;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_1
    throw v0

    .line 7882
    :cond_1
    :try_start_3
    invoke-direct {p0, v0}, LX/02U;->pruneTemporaryDirectoriesLocked([Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 7883
    if-eqz v2, :cond_2

    invoke-virtual {v2}, LX/02W;->close()V

    .line 7884
    :cond_2
    return-void

    .line 7885
    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, LX/02W;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final readConfig()LX/080;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 7886
    iget-object v0, p0, LX/02U;->mLockFile:LX/02V;

    invoke-virtual {v0, v1}, LX/02V;->acquire(I)LX/02W;

    move-result-object v2

    const/4 v1, 0x0

    .line 7887
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, LX/02U;->root:Ljava/io/File;

    const-string v4, "config"

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 7888
    :try_start_1
    invoke-static {v0}, LX/080;->read(Ljava/io/File;)LX/080;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 7889
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/02W;->close()V

    :cond_0
    return-object v0

    .line 7890
    :catch_0
    :try_start_2
    new-instance v0, LX/081;

    invoke-direct {v0}, LX/081;-><init>()V

    invoke-virtual {v0}, LX/081;->build()LX/080;

    move-result-object v0

    goto :goto_0

    .line 7891
    :catch_1
    const-string v3, "unsupported dex store config file %s: ignoring and deleting"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7892
    invoke-static {v0}, LX/02Q;->deleteRecursive(Ljava/io/File;)V

    .line 7893
    new-instance v0, LX/081;

    invoke-direct {v0}, LX/081;-><init>()V

    invoke-virtual {v0}, LX/081;->build()LX/080;

    move-result-object v0

    goto :goto_0

    .line 7894
    :catch_2
    move-exception v3

    .line 7895
    const-string v4, "error reading dex store config file %s: deleting and proceeding"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7896
    invoke-static {v0}, LX/02Q;->deleteRecursive(Ljava/io/File;)V

    .line 7897
    new-instance v0, LX/081;

    invoke-direct {v0}, LX/081;-><init>()V

    invoke-virtual {v0}, LX/081;->build()LX/080;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    goto :goto_0

    .line 7898
    :catch_3
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 7899
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v2}, LX/02W;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4

    :cond_1
    :goto_2
    throw v0

    :catch_4
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, LX/02W;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final reportStatus()J
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 7900
    const-string v0, "DexStore::reportStatus()"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7901
    const-wide/16 v2, 0x0

    .line 7902
    :try_start_0
    iget-object v0, p0, LX/02U;->mLockFile:LX/02V;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/02V;->acquire(I)LX/02W;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    const/4 v1, 0x0

    .line 7903
    :try_start_1
    invoke-static {p0}, LX/02U;->readStatusLocked(LX/02U;)J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 7904
    if-eqz v4, :cond_0

    :try_start_2
    invoke-virtual {v4}, LX/02W;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3

    .line 7905
    :cond_0
    :goto_0
    return-wide v0

    .line 7906
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 7907
    :catchall_0
    move-exception v0

    if-eqz v4, :cond_1

    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v4}, LX/02W;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    :cond_1
    :goto_1
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    move-object v6, v0

    move-wide v0, v2

    move-object v2, v6

    .line 7908
    :goto_2
    const-string v3, "DexStore::reportStatus caught Throwable "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    invoke-static {v3, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 7909
    :catch_2
    move-exception v4

    :try_start_6
    invoke-static {v1, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, LX/02W;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    :catch_3
    move-exception v2

    goto :goto_2
.end method

.method public final writeStatusLocked(J)V
    .locals 11

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 7910
    invoke-static {p0}, LX/02U;->assertLockHeld(LX/02U;)V

    .line 7911
    const-wide/16 v0, 0xf

    and-long/2addr v0, p1

    long-to-int v0, v0

    int-to-byte v0, v0

    .line 7912
    if-eq v0, v7, :cond_0

    .line 7913
    iget-object v0, p0, LX/02U;->root:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/facebook/common/dextricks/DalvikInternals;->fsyncNamed(Ljava/lang/String;I)V

    .line 7914
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/02U;->root:Ljava/io/File;

    const-string v2, "mdex_status2"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 7915
    const-wide v2, -0x5314ff805314ff9L

    xor-long/2addr v2, p1

    .line 7916
    const-string v1, "writing status:%x check:%x str:%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    invoke-static {p1, p2}, LX/02U;->getStatusDescription(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 7917
    new-array v4, v8, [B

    .line 7918
    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 7919
    invoke-virtual {v1, p1, p2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 7920
    invoke-virtual {v1, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 7921
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v1, 0x0

    .line 7922
    const/4 v0, 0x0

    const/16 v3, 0x10

    :try_start_0
    invoke-virtual {v2, v4, v0, v3}, Ljava/io/FileOutputStream;->write([BII)V

    .line 7923
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 7924
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    return-void

    .line 7925
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7926
    :catchall_0
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_0
    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method
