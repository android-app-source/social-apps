.class public final enum LX/0GH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0GH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0GH;

.field public static final enum DISMISS:LX/0GH;

.field public static final enum FAILED:LX/0GH;

.field public static final enum PREPARED:LX/0GH;

.field public static final enum PREPARING:LX/0GH;

.field public static final enum UNKNOWN:LX/0GH;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34751
    new-instance v0, LX/0GH;

    const-string v1, "PREPARING"

    invoke-direct {v0, v1, v2}, LX/0GH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0GH;->PREPARING:LX/0GH;

    .line 34752
    new-instance v0, LX/0GH;

    const-string v1, "PREPARED"

    invoke-direct {v0, v1, v3}, LX/0GH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0GH;->PREPARED:LX/0GH;

    .line 34753
    new-instance v0, LX/0GH;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, LX/0GH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0GH;->FAILED:LX/0GH;

    .line 34754
    new-instance v0, LX/0GH;

    const-string v1, "DISMISS"

    invoke-direct {v0, v1, v5}, LX/0GH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0GH;->DISMISS:LX/0GH;

    .line 34755
    new-instance v0, LX/0GH;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/0GH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0GH;->UNKNOWN:LX/0GH;

    .line 34756
    const/4 v0, 0x5

    new-array v0, v0, [LX/0GH;

    sget-object v1, LX/0GH;->PREPARING:LX/0GH;

    aput-object v1, v0, v2

    sget-object v1, LX/0GH;->PREPARED:LX/0GH;

    aput-object v1, v0, v3

    sget-object v1, LX/0GH;->FAILED:LX/0GH;

    aput-object v1, v0, v4

    sget-object v1, LX/0GH;->DISMISS:LX/0GH;

    aput-object v1, v0, v5

    sget-object v1, LX/0GH;->UNKNOWN:LX/0GH;

    aput-object v1, v0, v6

    sput-object v0, LX/0GH;->$VALUES:[LX/0GH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34757
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0GH;
    .locals 1

    .prologue
    .line 34758
    const-class v0, LX/0GH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0GH;

    return-object v0
.end method

.method public static values()[LX/0GH;
    .locals 1

    .prologue
    .line 34759
    sget-object v0, LX/0GH;->$VALUES:[LX/0GH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0GH;

    return-object v0
.end method
