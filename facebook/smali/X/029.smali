.class public LX/029;
.super LX/025;
.source ""


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final f:Ljava/io/File;

.field private final g:Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6346
    invoke-direct {p0, p1, p2}, LX/025;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 6347
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/025;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/029;->f:Ljava/io/File;

    .line 6348
    iget-object v0, p0, LX/029;->f:Ljava/io/File;

    iput-object v0, p0, LX/029;->g:Ljava/io/File;

    .line 6349
    const-string v0, "assets/lib/libs.xzs"

    iput-object v0, p0, LX/029;->c:Ljava/lang/String;

    .line 6350
    const-string v0, "assets/lib/metadata.txt"

    iput-object v0, p0, LX/029;->d:Ljava/lang/String;

    .line 6351
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6352
    invoke-direct {p0, p1, p2}, LX/025;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 6353
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/025;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/029;->f:Ljava/io/File;

    .line 6354
    iput-object p3, p0, LX/029;->g:Ljava/io/File;

    .line 6355
    iput-object p4, p0, LX/029;->c:Ljava/lang/String;

    .line 6356
    iput-object p5, p0, LX/029;->d:Ljava/lang/String;

    .line 6357
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6358
    invoke-direct {p0, p1, p2}, LX/025;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 6359
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/025;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/029;->f:Ljava/io/File;

    .line 6360
    iget-object v0, p0, LX/029;->f:Ljava/io/File;

    iput-object v0, p0, LX/029;->g:Ljava/io/File;

    .line 6361
    iput-object p3, p0, LX/029;->c:Ljava/lang/String;

    .line 6362
    iput-object p4, p0, LX/029;->d:Ljava/lang/String;

    .line 6363
    return-void
.end method

.method public static synthetic a(LX/029;)Ljava/io/File;
    .locals 1

    .prologue
    .line 6364
    iget-object v0, p0, LX/029;->g:Ljava/io/File;

    return-object v0
.end method

.method public static synthetic b(LX/029;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 6365
    iget-object v0, p0, LX/029;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic c(LX/029;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 6366
    iget-object v0, p0, LX/029;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()LX/07u;
    .locals 1

    .prologue
    .line 6367
    new-instance v0, LX/0AP;

    invoke-direct {v0, p0, p0}, LX/0AP;-><init>(LX/029;LX/025;)V

    return-object v0
.end method

.method public final b()[B
    .locals 1

    .prologue
    .line 6368
    iget-object v0, p0, LX/029;->f:Ljava/io/File;

    invoke-static {v0}, LX/01z;->d(Ljava/io/File;)[B

    move-result-object v0

    return-object v0
.end method
