.class public final LX/0NC;
.super LX/0N4;
.source ""


# instance fields
.field private b:Z

.field private final c:LX/0NK;

.field private final d:[Z

.field private final e:LX/0NB;

.field private final f:LX/0NH;

.field private final g:LX/0NH;

.field private final h:LX/0NH;

.field private i:J

.field private j:J

.field private final k:LX/0Oj;


# direct methods
.method public constructor <init>(LX/0LS;LX/0NK;ZZ)V
    .locals 3

    .prologue
    const/16 v2, 0x80

    .line 49424
    invoke-direct {p0, p1}, LX/0N4;-><init>(LX/0LS;)V

    .line 49425
    iput-object p2, p0, LX/0NC;->c:LX/0NK;

    .line 49426
    const/4 v0, 0x3

    new-array v0, v0, [Z

    iput-object v0, p0, LX/0NC;->d:[Z

    .line 49427
    new-instance v0, LX/0NB;

    invoke-direct {v0, p1, p3, p4}, LX/0NB;-><init>(LX/0LS;ZZ)V

    iput-object v0, p0, LX/0NC;->e:LX/0NB;

    .line 49428
    new-instance v0, LX/0NH;

    const/4 v1, 0x7

    invoke-direct {v0, v1, v2}, LX/0NH;-><init>(II)V

    iput-object v0, p0, LX/0NC;->f:LX/0NH;

    .line 49429
    new-instance v0, LX/0NH;

    const/16 v1, 0x8

    invoke-direct {v0, v1, v2}, LX/0NH;-><init>(II)V

    iput-object v0, p0, LX/0NC;->g:LX/0NH;

    .line 49430
    new-instance v0, LX/0NH;

    const/4 v1, 0x6

    invoke-direct {v0, v1, v2}, LX/0NH;-><init>(II)V

    iput-object v0, p0, LX/0NC;->h:LX/0NH;

    .line 49431
    new-instance v0, LX/0Oj;

    invoke-direct {v0}, LX/0Oj;-><init>()V

    iput-object v0, p0, LX/0NC;->k:LX/0Oj;

    .line 49432
    return-void
.end method

.method private static a(LX/0NH;)LX/0Oi;
    .locals 3

    .prologue
    .line 49420
    iget-object v0, p0, LX/0NH;->a:[B

    iget v1, p0, LX/0NH;->b:I

    invoke-static {v0, v1}, LX/0Oh;->a([BI)I

    move-result v0

    .line 49421
    new-instance v1, LX/0Oi;

    iget-object v2, p0, LX/0NH;->a:[B

    invoke-direct {v1, v2, v0}, LX/0Oi;-><init>([BI)V

    .line 49422
    const/16 v0, 0x20

    invoke-virtual {v1, v0}, LX/0Oi;->b(I)V

    .line 49423
    return-object v1
.end method

.method private a(JIIJ)V
    .locals 19

    .prologue
    .line 49389
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0NC;->b:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->e:LX/0NB;

    invoke-virtual {v4}, LX/0NB;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 49390
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->f:LX/0NH;

    move/from16 v0, p4

    invoke-virtual {v4, v0}, LX/0NH;->b(I)Z

    .line 49391
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->g:LX/0NH;

    move/from16 v0, p4

    invoke-virtual {v4, v0}, LX/0NH;->b(I)Z

    .line 49392
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0NC;->b:Z

    if-nez v4, :cond_3

    .line 49393
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->f:LX/0NH;

    invoke-virtual {v4}, LX/0NH;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->g:LX/0NH;

    invoke-virtual {v4}, LX/0NH;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 49394
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 49395
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->f:LX/0NH;

    iget-object v4, v4, LX/0NH;->a:[B

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0NC;->f:LX/0NH;

    iget v5, v5, LX/0NH;->b:I

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v4

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49396
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->g:LX/0NH;

    iget-object v4, v4, LX/0NH;->a:[B

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0NC;->g:LX/0NH;

    iget v5, v5, LX/0NH;->b:I

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v4

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49397
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->f:LX/0NH;

    invoke-static {v4}, LX/0NC;->a(LX/0NH;)LX/0Oi;

    move-result-object v4

    invoke-static {v4}, LX/0Oh;->a(LX/0Oi;)LX/0Og;

    move-result-object v15

    .line 49398
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->g:LX/0NH;

    invoke-static {v4}, LX/0NC;->a(LX/0NH;)LX/0Oi;

    move-result-object v4

    invoke-static {v4}, LX/0Oh;->b(LX/0Oi;)LX/0Of;

    move-result-object v16

    .line 49399
    move-object/from16 v0, p0

    iget-object v0, v0, LX/0N4;->a:LX/0LS;

    move-object/from16 v17, v0

    const/4 v4, 0x0

    const-string v5, "video/avc"

    const/4 v6, -0x1

    const/4 v7, -0x1

    const-wide/16 v8, -0x1

    iget v10, v15, LX/0Og;->b:I

    iget v11, v15, LX/0Og;->c:I

    const/4 v13, -0x1

    iget v14, v15, LX/0Og;->d:F

    invoke-static/range {v4 .. v14}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)LX/0L4;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, LX/0LS;->a(LX/0L4;)V

    .line 49400
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/0NC;->b:Z

    .line 49401
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->e:LX/0NB;

    invoke-virtual {v4, v15}, LX/0NB;->a(LX/0Og;)V

    .line 49402
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->e:LX/0NB;

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, LX/0NB;->a(LX/0Of;)V

    .line 49403
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->f:LX/0NH;

    invoke-virtual {v4}, LX/0NH;->a()V

    .line 49404
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->g:LX/0NH;

    invoke-virtual {v4}, LX/0NH;->a()V

    .line 49405
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->h:LX/0NH;

    move/from16 v0, p4

    invoke-virtual {v4, v0}, LX/0NH;->b(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 49406
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->h:LX/0NH;

    iget-object v4, v4, LX/0NH;->a:[B

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0NC;->h:LX/0NH;

    iget v5, v5, LX/0NH;->b:I

    invoke-static {v4, v5}, LX/0Oh;->a([BI)I

    move-result v4

    .line 49407
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0NC;->k:LX/0Oj;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0NC;->h:LX/0NH;

    iget-object v6, v6, LX/0NH;->a:[B

    invoke-virtual {v5, v6, v4}, LX/0Oj;->a([BI)V

    .line 49408
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->k:LX/0Oj;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, LX/0Oj;->b(I)V

    .line 49409
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->c:LX/0NK;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0NC;->k:LX/0Oj;

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1, v5}, LX/0NK;->a(JLX/0Oj;)V

    .line 49410
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->e:LX/0NB;

    move-wide/from16 v0, p1

    move/from16 v2, p3

    invoke-virtual {v4, v0, v1, v2}, LX/0NB;->a(JI)V

    .line 49411
    return-void

    .line 49412
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->f:LX/0NH;

    invoke-virtual {v4}, LX/0NH;->b()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 49413
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->f:LX/0NH;

    invoke-static {v4}, LX/0NC;->a(LX/0NH;)LX/0Oi;

    move-result-object v4

    invoke-static {v4}, LX/0Oh;->a(LX/0Oi;)LX/0Og;

    move-result-object v4

    .line 49414
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0NC;->e:LX/0NB;

    invoke-virtual {v5, v4}, LX/0NB;->a(LX/0Og;)V

    .line 49415
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->f:LX/0NH;

    invoke-virtual {v4}, LX/0NH;->a()V

    goto :goto_0

    .line 49416
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->g:LX/0NH;

    invoke-virtual {v4}, LX/0NH;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 49417
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->g:LX/0NH;

    invoke-static {v4}, LX/0NC;->a(LX/0NH;)LX/0Oi;

    move-result-object v4

    invoke-static {v4}, LX/0Oh;->b(LX/0Oi;)LX/0Of;

    move-result-object v4

    .line 49418
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0NC;->e:LX/0NB;

    invoke-virtual {v5, v4}, LX/0NB;->a(LX/0Of;)V

    .line 49419
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0NC;->g:LX/0NH;

    invoke-virtual {v4}, LX/0NH;->a()V

    goto/16 :goto_0
.end method

.method private a(JIJ)V
    .locals 9

    .prologue
    .line 49433
    iget-boolean v0, p0, LX/0NC;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0NC;->e:LX/0NB;

    .line 49434
    iget-boolean v1, v0, LX/0NB;->c:Z

    move v0, v1

    .line 49435
    if-eqz v0, :cond_1

    .line 49436
    :cond_0
    iget-object v0, p0, LX/0NC;->f:LX/0NH;

    invoke-virtual {v0, p3}, LX/0NH;->a(I)V

    .line 49437
    iget-object v0, p0, LX/0NC;->g:LX/0NH;

    invoke-virtual {v0, p3}, LX/0NH;->a(I)V

    .line 49438
    :cond_1
    iget-object v0, p0, LX/0NC;->h:LX/0NH;

    invoke-virtual {v0, p3}, LX/0NH;->a(I)V

    .line 49439
    iget-object v0, p0, LX/0NC;->e:LX/0NB;

    move-wide v1, p1

    move v3, p3

    move-wide v4, p4

    const/4 v8, 0x1

    .line 49440
    iput v3, v0, LX/0NB;->i:I

    .line 49441
    iput-wide v4, v0, LX/0NB;->l:J

    .line 49442
    iput-wide v1, v0, LX/0NB;->j:J

    .line 49443
    iget-boolean v6, v0, LX/0NB;->b:Z

    if-eqz v6, :cond_2

    iget v6, v0, LX/0NB;->i:I

    if-eq v6, v8, :cond_3

    :cond_2
    iget-boolean v6, v0, LX/0NB;->c:Z

    if-eqz v6, :cond_4

    iget v6, v0, LX/0NB;->i:I

    const/4 v7, 0x5

    if-eq v6, v7, :cond_3

    iget v6, v0, LX/0NB;->i:I

    if-eq v6, v8, :cond_3

    iget v6, v0, LX/0NB;->i:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_4

    .line 49444
    :cond_3
    iget-object v6, v0, LX/0NB;->m:LX/0NA;

    .line 49445
    iget-object v7, v0, LX/0NB;->n:LX/0NA;

    iput-object v7, v0, LX/0NB;->m:LX/0NA;

    .line 49446
    iput-object v6, v0, LX/0NB;->n:LX/0NA;

    .line 49447
    iget-object v6, v0, LX/0NB;->n:LX/0NA;

    invoke-virtual {v6}, LX/0NA;->a()V

    .line 49448
    const/4 v6, 0x0

    iput v6, v0, LX/0NB;->h:I

    .line 49449
    iput-boolean v8, v0, LX/0NB;->k:Z

    .line 49450
    :cond_4
    return-void
.end method

.method private a([BII)V
    .locals 2

    .prologue
    .line 49381
    iget-boolean v0, p0, LX/0NC;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0NC;->e:LX/0NB;

    .line 49382
    iget-boolean v1, v0, LX/0NB;->c:Z

    move v0, v1

    .line 49383
    if-eqz v0, :cond_1

    .line 49384
    :cond_0
    iget-object v0, p0, LX/0NC;->f:LX/0NH;

    invoke-virtual {v0, p1, p2, p3}, LX/0NH;->a([BII)V

    .line 49385
    iget-object v0, p0, LX/0NC;->g:LX/0NH;

    invoke-virtual {v0, p1, p2, p3}, LX/0NH;->a([BII)V

    .line 49386
    :cond_1
    iget-object v0, p0, LX/0NC;->h:LX/0NH;

    invoke-virtual {v0, p1, p2, p3}, LX/0NH;->a([BII)V

    .line 49387
    iget-object v0, p0, LX/0NC;->e:LX/0NB;

    invoke-virtual {v0, p1, p2, p3}, LX/0NB;->a([BII)V

    .line 49388
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 49374
    iget-object v0, p0, LX/0NC;->d:[Z

    invoke-static {v0}, LX/0Oh;->a([Z)V

    .line 49375
    iget-object v0, p0, LX/0NC;->f:LX/0NH;

    invoke-virtual {v0}, LX/0NH;->a()V

    .line 49376
    iget-object v0, p0, LX/0NC;->g:LX/0NH;

    invoke-virtual {v0}, LX/0NH;->a()V

    .line 49377
    iget-object v0, p0, LX/0NC;->h:LX/0NH;

    invoke-virtual {v0}, LX/0NH;->a()V

    .line 49378
    iget-object v0, p0, LX/0NC;->e:LX/0NB;

    invoke-virtual {v0}, LX/0NB;->b()V

    .line 49379
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0NC;->i:J

    .line 49380
    return-void
.end method

.method public final a(JZ)V
    .locals 1

    .prologue
    .line 49372
    iput-wide p1, p0, LX/0NC;->j:J

    .line 49373
    return-void
.end method

.method public final a(LX/0Oj;)V
    .locals 12

    .prologue
    .line 49351
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 49352
    iget v0, p1, LX/0Oj;->b:I

    move v0, v0

    .line 49353
    iget v1, p1, LX/0Oj;->c:I

    move v8, v1

    .line 49354
    iget-object v9, p1, LX/0Oj;->a:[B

    .line 49355
    iget-wide v2, p0, LX/0NC;->i:J

    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0NC;->i:J

    .line 49356
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v2

    invoke-interface {v1, p1, v2}, LX/0LS;->a(LX/0Oj;I)V

    .line 49357
    :goto_0
    iget-object v1, p0, LX/0NC;->d:[Z

    invoke-static {v9, v0, v8, v1}, LX/0Oh;->a([BII[Z)I

    move-result v10

    .line 49358
    if-ne v10, v8, :cond_1

    .line 49359
    invoke-direct {p0, v9, v0, v8}, LX/0NC;->a([BII)V

    .line 49360
    :cond_0
    return-void

    .line 49361
    :cond_1
    add-int/lit8 v1, v10, 0x3

    aget-byte v1, v9, v1

    and-int/lit8 v1, v1, 0x1f

    move v11, v1

    .line 49362
    sub-int v1, v10, v0

    .line 49363
    if-lez v1, :cond_2

    .line 49364
    invoke-direct {p0, v9, v0, v10}, LX/0NC;->a([BII)V

    .line 49365
    :cond_2
    sub-int v4, v8, v10

    .line 49366
    iget-wide v2, p0, LX/0NC;->i:J

    int-to-long v6, v4

    sub-long/2addr v2, v6

    .line 49367
    if-gez v1, :cond_3

    neg-int v5, v1

    :goto_1
    iget-wide v6, p0, LX/0NC;->j:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, LX/0NC;->a(JIIJ)V

    .line 49368
    iget-wide v5, p0, LX/0NC;->j:J

    move-object v1, p0

    move v4, v11

    invoke-direct/range {v1 .. v6}, LX/0NC;->a(JIJ)V

    .line 49369
    add-int/lit8 v0, v10, 0x3

    .line 49370
    goto :goto_0

    .line 49371
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 49350
    return-void
.end method
