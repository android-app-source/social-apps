.class public final enum LX/0AA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0AA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0AA;

.field public static final enum VIDEO_MANIFEST_MISALIGN:LX/0AA;

.field public static final enum VIDEO_PLAYER_SERVICE_DISCONNECTED:LX/0AA;

.field public static final enum VIDEO_PLAYER_SERVICE_RECONNECTED:LX/0AA;

.field public static final enum VIDEO_PLAYER_SERVICE_UNAVAILABLE:LX/0AA;

.field public static final enum VIDEO_VPS_HTTP_TRANSFER:LX/0AA;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23568
    new-instance v0, LX/0AA;

    const-string v1, "VIDEO_PLAYER_SERVICE_RECONNECTED"

    const-string v2, "video_player_service_reconnected"

    invoke-direct {v0, v1, v3, v2}, LX/0AA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AA;->VIDEO_PLAYER_SERVICE_RECONNECTED:LX/0AA;

    .line 23569
    new-instance v0, LX/0AA;

    const-string v1, "VIDEO_PLAYER_SERVICE_DISCONNECTED"

    const-string v2, "video_player_service_disconnected"

    invoke-direct {v0, v1, v4, v2}, LX/0AA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AA;->VIDEO_PLAYER_SERVICE_DISCONNECTED:LX/0AA;

    .line 23570
    new-instance v0, LX/0AA;

    const-string v1, "VIDEO_PLAYER_SERVICE_UNAVAILABLE"

    const-string v2, "video_player_service_unavailable"

    invoke-direct {v0, v1, v5, v2}, LX/0AA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AA;->VIDEO_PLAYER_SERVICE_UNAVAILABLE:LX/0AA;

    .line 23571
    new-instance v0, LX/0AA;

    const-string v1, "VIDEO_VPS_HTTP_TRANSFER"

    const-string v2, "vps_http_transfer"

    invoke-direct {v0, v1, v6, v2}, LX/0AA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AA;->VIDEO_VPS_HTTP_TRANSFER:LX/0AA;

    .line 23572
    new-instance v0, LX/0AA;

    const-string v1, "VIDEO_MANIFEST_MISALIGN"

    const-string v2, "manifest_misalign"

    invoke-direct {v0, v1, v7, v2}, LX/0AA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AA;->VIDEO_MANIFEST_MISALIGN:LX/0AA;

    .line 23573
    const/4 v0, 0x5

    new-array v0, v0, [LX/0AA;

    sget-object v1, LX/0AA;->VIDEO_PLAYER_SERVICE_RECONNECTED:LX/0AA;

    aput-object v1, v0, v3

    sget-object v1, LX/0AA;->VIDEO_PLAYER_SERVICE_DISCONNECTED:LX/0AA;

    aput-object v1, v0, v4

    sget-object v1, LX/0AA;->VIDEO_PLAYER_SERVICE_UNAVAILABLE:LX/0AA;

    aput-object v1, v0, v5

    sget-object v1, LX/0AA;->VIDEO_VPS_HTTP_TRANSFER:LX/0AA;

    aput-object v1, v0, v6

    sget-object v1, LX/0AA;->VIDEO_MANIFEST_MISALIGN:LX/0AA;

    aput-object v1, v0, v7

    sput-object v0, LX/0AA;->$VALUES:[LX/0AA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23574
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23575
    iput-object p3, p0, LX/0AA;->value:Ljava/lang/String;

    .line 23576
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0AA;
    .locals 1

    .prologue
    .line 23577
    const-class v0, LX/0AA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0AA;

    return-object v0
.end method

.method public static values()[LX/0AA;
    .locals 1

    .prologue
    .line 23578
    sget-object v0, LX/0AA;->$VALUES:[LX/0AA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0AA;

    return-object v0
.end method
