.class public LX/0Am;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Ak;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List",
            "<",
            "LX/0Ak;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25005
    iput-object p1, p0, LX/0Am;->a:Ljava/lang/String;

    .line 25006
    iput-wide p2, p0, LX/0Am;->b:J

    .line 25007
    invoke-static {p4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0Am;->c:Ljava/util/List;

    .line 25008
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    .prologue
    .line 24998
    iget-object v0, p0, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 24999
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 25000
    iget-object v0, p0, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    iget v0, v0, LX/0Ak;->b:I

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 25001
    :goto_1
    return v0

    .line 25002
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 25003
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
