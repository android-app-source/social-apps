.class public LX/0An;
.super LX/0Ab;
.source ""

# interfaces
.implements LX/2WC;


# instance fields
.field private b:J

.field private c:J

.field private d:LX/0Ae;

.field private final e:J

.field public final f:Ljava/util/regex/Pattern;

.field public final g:LX/19l;


# direct methods
.method public constructor <init>(LX/0Ad;LX/0Zb;LX/0oz;LX/0kb;Ljava/lang/String;LX/0Ae;JLjava/util/regex/Pattern;LX/19l;)V
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 25036
    invoke-direct/range {p0 .. p5}, LX/0Ab;-><init>(LX/0Ad;LX/0Zb;LX/0oz;LX/0kb;Ljava/lang/String;)V

    .line 25037
    iput-wide v0, p0, LX/0An;->b:J

    .line 25038
    iput-wide v0, p0, LX/0An;->c:J

    .line 25039
    iput-object p6, p0, LX/0An;->d:LX/0Ae;

    .line 25040
    iput-wide p7, p0, LX/0An;->e:J

    .line 25041
    iput-object p9, p0, LX/0An;->f:Ljava/util/regex/Pattern;

    .line 25042
    iput-object p10, p0, LX/0An;->g:LX/19l;

    .line 25043
    iget-object v0, p0, LX/0An;->g:LX/19l;

    iget-object v0, v0, LX/19l;->a:LX/16V;

    if-eqz v0, :cond_0

    .line 25044
    iget-object v0, p0, LX/0An;->g:LX/19l;

    iget-object v0, v0, LX/19l;->a:LX/16V;

    const-class v1, LX/2WD;

    invoke-virtual {v0, v1, p0}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 25045
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2WD;)V
    .locals 12

    .prologue
    .line 25046
    iget-object v0, p1, LX/2WD;->a:Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;

    .line 25047
    iget-object v1, p0, LX/0Ab;->a:LX/0Ad;

    .line 25048
    iget-boolean v2, v1, LX/0Ad;->a:Z

    move v1, v2

    .line 25049
    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->a:Ljava/lang/String;

    .line 25050
    iget-object v2, p0, LX/0Ab;->e:Ljava/lang/String;

    move-object v2, v2

    .line 25051
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 25052
    iget-object v1, v0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->b:Ljava/lang/String;

    const-wide/16 v8, -0x1

    .line 25053
    if-eqz v1, :cond_0

    iget-object v10, p0, LX/0An;->f:Ljava/util/regex/Pattern;

    if-nez v10, :cond_2

    .line 25054
    :cond_0
    :goto_0
    move-wide v2, v8

    .line 25055
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 25056
    iget-wide v4, p0, LX/0An;->e:J

    add-long/2addr v4, v2

    iget-wide v6, v0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->g:J

    iget-wide v0, v0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->f:J

    sub-long/2addr v6, v0

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, LX/0Ab;->a(JJJ)V

    .line 25057
    :cond_1
    return-void

    .line 25058
    :cond_2
    iget-object v10, p0, LX/0An;->f:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v10

    .line 25059
    invoke-virtual {v10}, Ljava/util/regex/Matcher;->find()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 25060
    const/4 v11, 0x1

    :try_start_0
    invoke-virtual {v10, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    goto :goto_0

    :catch_0
    goto :goto_0
.end method

.method public final c()V
    .locals 15

    .prologue
    const-wide/16 v6, 0x28

    .line 25025
    iget-object v0, p0, LX/0Ab;->a:LX/0Ad;

    .line 25026
    iget-boolean v1, v0, LX/0Ad;->a:Z

    move v0, v1

    .line 25027
    if-eqz v0, :cond_2

    iget-wide v0, p0, LX/0An;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Ab;->a:LX/0Ad;

    iget-wide v2, p0, LX/0An;->b:J

    iget-wide v4, p0, LX/0An;->c:J

    add-long/2addr v4, v6

    invoke-virtual {v0, v2, v3, v4, v5}, LX/0Ad;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 25028
    :cond_0
    iget-object v0, p0, LX/0An;->d:LX/0Ae;

    invoke-interface {v0}, LX/0Ae;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0An;->b:J

    .line 25029
    iget-wide v0, p0, LX/0An;->b:J

    iput-wide v0, p0, LX/0An;->c:J

    .line 25030
    iget-wide v0, p0, LX/0An;->c:J

    .line 25031
    iget-object v8, p0, LX/0Ab;->a:LX/0Ad;

    iget-wide v10, p0, LX/0Ab;->h:J

    invoke-virtual {v8, v10, v11, v0, v1}, LX/0Ad;->a(JJ)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 25032
    iput-wide v0, p0, LX/0Ab;->h:J

    .line 25033
    const-string v9, "live_video_frame_viewed"

    const-wide/16 v12, 0x0

    move-object v8, p0

    move-wide v10, v0

    invoke-static/range {v8 .. v13}, LX/0Ab;->a(LX/0Ab;Ljava/lang/String;JJ)V

    .line 25034
    :cond_1
    :goto_0
    return-void

    .line 25035
    :cond_2
    iget-wide v0, p0, LX/0An;->c:J

    add-long/2addr v0, v6

    iput-wide v0, p0, LX/0An;->c:J

    goto :goto_0
.end method
