.class public LX/094;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/094;


# instance fields
.field public a:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 21973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21974
    iput-object p1, p0, LX/094;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 21975
    return-void
.end method

.method public static a(LX/0QB;)LX/094;
    .locals 3

    .prologue
    .line 21976
    sget-object v0, LX/094;->b:LX/094;

    if-nez v0, :cond_1

    .line 21977
    const-class v1, LX/094;

    monitor-enter v1

    .line 21978
    :try_start_0
    sget-object v0, LX/094;->b:LX/094;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 21979
    if-eqz v2, :cond_0

    .line 21980
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/094;->b(LX/0QB;)LX/094;

    move-result-object v0

    sput-object v0, LX/094;->b:LX/094;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 21981
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 21982
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 21983
    :cond_1
    sget-object v0, LX/094;->b:LX/094;

    return-object v0

    .line 21984
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 21985
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 21986
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 21987
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 21988
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/094;
    .locals 2

    .prologue
    .line 21989
    new-instance v1, LX/094;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {v1, v0}, LX/094;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 21990
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 21991
    iget-object v0, p0, LX/094;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1d0003

    invoke-static {p1}, LX/094;->b(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 21992
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 21993
    iget-object v0, p0, LX/094;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1d0003

    invoke-static {p1}, LX/094;->b(Ljava/lang/String;)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "was_playing_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 21994
    return-void
.end method
