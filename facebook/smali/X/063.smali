.class public LX/063;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Ljava/lang/reflect/Field;

.field public static b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17202
    const/4 v0, 0x0

    sput-boolean v0, LX/063;->b:Z

    .line 17203
    :try_start_0
    const-class v0, Ljava/net/Socket;

    const-string v1, "impl"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 17204
    sput-object v0, LX/063;->a:Ljava/lang/reflect/Field;

    invoke-static {v0}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17205
    sget-object v0, LX/063;->a:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 17206
    const/4 v0, 0x1

    sput-boolean v0, LX/063;->b:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 17207
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17209
    return-void
.end method

.method public static a(Ljava/net/Socket;[BLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 17210
    :try_start_0
    sget-object v0, LX/063;->a:Ljava/lang/reflect/Field;

    new-instance v1, LX/08r;

    invoke-direct {v1, p1, p2, p3}, LX/08r;-><init>([BLjava/lang/String;I)V

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 17211
    return-void

    .line 17212
    :catch_0
    move-exception v0

    .line 17213
    new-instance v1, LX/061;

    invoke-direct {v1, v0}, LX/061;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method
