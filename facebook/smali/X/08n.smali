.class public LX/08n;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/08e;

.field public final c:LX/08o;

.field public final d:LX/09m;

.field public final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/08e;LX/09m;I)V
    .locals 2

    .prologue
    .line 21588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21589
    iput-object p1, p0, LX/08n;->a:Landroid/content/Context;

    .line 21590
    iput-object p2, p0, LX/08n;->b:LX/08e;

    .line 21591
    new-instance v0, LX/08o;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    invoke-direct {v0, p1, v1, p4}, LX/08o;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;I)V

    iput-object v0, p0, LX/08n;->c:LX/08o;

    .line 21592
    iput-object p3, p0, LX/08n;->d:LX/09m;

    .line 21593
    iput p4, p0, LX/08n;->e:I

    .line 21594
    return-void
.end method

.method public static a$redex0(LX/08n;I)V
    .locals 2

    .prologue
    .line 21568
    iget-object v0, p0, LX/08n;->a:Landroid/content/Context;

    .line 21569
    invoke-static {v0}, LX/04u;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 21570
    if-eqz v0, :cond_0

    .line 21571
    iget-object v0, p0, LX/08n;->b:LX/08e;

    invoke-virtual {v0}, LX/08e;->a()V

    .line 21572
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/08n;ILjava/lang/String;)V
    .locals 12

    .prologue
    const/4 v2, -0x1

    .line 21573
    invoke-static {p0}, LX/08n;->d(LX/08n;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "shared_qe_config"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "shared_status"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 21574
    if-eq p1, v2, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    iget-object v0, p0, LX/08n;->a:Landroid/content/Context;

    invoke-static {v0}, LX/04u;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 21575
    :cond_0
    iget-object v0, p0, LX/08n;->d:LX/09m;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/09m;->a(Z)V

    .line 21576
    iget-object v0, p0, LX/08n;->b:LX/08e;

    invoke-virtual {v0}, LX/08e;->e()V

    .line 21577
    :cond_1
    if-eq p1, v2, :cond_2

    .line 21578
    iget-object v0, p0, LX/08n;->d:LX/09m;

    const-string v1, "onInit"

    .line 21579
    invoke-static {v0, p1}, LX/09m;->a(LX/09m;I)Ljava/lang/String;

    move-result-object v3

    .line 21580
    invoke-static {v0}, LX/09m;->e(LX/09m;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "shared_flag"

    invoke-interface {v4, v5, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "leader_package"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 21581
    invoke-static {v4}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 21582
    iget-object v4, v0, LX/09m;->a:Landroid/content/Context;

    .line 21583
    if-nez v3, :cond_3

    .line 21584
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    .line 21585
    :goto_0
    invoke-static {v10}, Lcom/facebook/rti/push/service/FbnsService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x1

    const-string v11, "Orca.START"

    move-object v6, v4

    move-object v8, v1

    invoke-static/range {v6 .. v11}, LX/09o;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 21586
    iget-object v0, p0, LX/08n;->b:LX/08e;

    invoke-virtual {v0}, LX/08e;->d()V

    .line 21587
    :cond_2
    return-void

    :cond_3
    move-object v10, v3

    goto :goto_0
.end method

.method public static d(LX/08n;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 21567
    iget-object v0, p0, LX/08n;->a:Landroid/content/Context;

    sget-object v1, LX/01p;->f:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method
