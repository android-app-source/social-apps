.class public LX/09d;
.super LX/09e;
.source ""


# static fields
.field private static final a:Ljava/nio/charset/Charset;

.field private static final b:I


# instance fields
.field public final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field public final e:Ljava/lang/StringBuilder;

.field private f:[B

.field private final g:[J

.field private h:I

.field private i:J

.field private j:I

.field private k:I

.field private l:I

.field private m:J

.field private n:Z

.field private o:Ljava/io/OutputStream;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22759
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, LX/09d;->a:Ljava/nio/charset/Charset;

    .line 22760
    const/16 v0, 0x3e8

    sput v0, LX/09d;->b:I

    .line 22761
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/16 v1, 0x80

    .line 22751
    invoke-direct {p0}, LX/09e;-><init>()V

    .line 22752
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, LX/09d;->e:Ljava/lang/StringBuilder;

    .line 22753
    new-array v0, v1, [B

    iput-object v0, p0, LX/09d;->f:[B

    .line 22754
    const/16 v0, 0x10

    new-array v0, v0, [J

    iput-object v0, p0, LX/09d;->g:[J

    .line 22755
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/09d;->n:Z

    .line 22756
    iput-object p1, p0, LX/09d;->c:Ljava/lang/String;

    .line 22757
    iput-object p2, p0, LX/09d;->d:Ljava/lang/String;

    .line 22758
    return-void
.end method

.method private static a(J)J
    .locals 4

    .prologue
    .line 22750
    sget v0, LX/09d;->b:I

    div-int/lit8 v0, v0, 0x2

    int-to-long v0, v0

    add-long/2addr v0, p0

    sget v2, LX/09d;->b:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private a([BI)V
    .locals 3

    .prologue
    .line 22744
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    .line 22745
    aget-byte v1, p1, v0

    const/16 v2, 0x80

    if-ge v1, v2, :cond_0

    .line 22746
    iget-object v1, p0, LX/09d;->e:Ljava/lang/StringBuilder;

    aget-byte v2, p1, v0

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 22747
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22748
    :cond_0
    iget-object v1, p0, LX/09d;->e:Ljava/lang/StringBuilder;

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 22749
    :cond_1
    return-void
.end method

.method private b()I
    .locals 6

    .prologue
    .line 22647
    iget-object v1, p0, LX/09d;->e:Ljava/lang/StringBuilder;

    .line 22648
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    .line 22649
    iget-object v0, p0, LX/09d;->f:[B

    array-length v0, v0

    if-le v2, v0, :cond_0

    .line 22650
    new-array v0, v2, [B

    iput-object v0, p0, LX/09d;->f:[B

    .line 22651
    :cond_0
    iget-object v3, p0, LX/09d;->f:[B

    .line 22652
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    .line 22653
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    .line 22654
    const/16 v5, 0x80

    if-ge v4, v5, :cond_1

    .line 22655
    int-to-byte v4, v4

    aput-byte v4, v3, v0

    .line 22656
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22657
    :cond_1
    const/16 v4, 0x2e

    aput-byte v4, v3, v0

    goto :goto_1

    .line 22658
    :cond_2
    return v2
.end method

.method private b(Lcom/facebook/loom/logger/LogEntry;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 22722
    iget v0, p1, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    move v0, v0

    .line 22723
    if-eqz v0, :cond_1

    .line 22724
    :cond_0
    :goto_0
    return-void

    .line 22725
    :cond_1
    iget-boolean v0, p0, LX/09d;->n:Z

    if-eqz v0, :cond_0

    .line 22726
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/loom/logger/LogEntry;->e()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/09d;->p:J

    .line 22727
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, LX/09d;->d:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 22728
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 22729
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 22730
    :cond_2
    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v0, p0, LX/09d;->d:Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22731
    :try_start_1
    new-instance v0, Ljava/io/BufferedOutputStream;

    const/high16 v3, 0x80000

    invoke-direct {v0, v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 22732
    :try_start_2
    new-instance v2, Ljava/util/zip/GZIPOutputStream;

    const/16 v3, 0x2000

    invoke-direct {v2, v0, v3}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v2, p0, LX/09d;->o:Ljava/io/OutputStream;

    .line 22733
    iget-object v2, p0, LX/09d;->o:Ljava/io/OutputStream;

    const/16 v5, 0xa

    .line 22734
    iget-object v3, p0, LX/09d;->e:Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 22735
    iget-object v3, p0, LX/09d;->e:Ljava/lang/StringBuilder;

    const-string v4, "dt\nver"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x7c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "id|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LX/09d;->c:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "cmap|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LX/09i;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "prec|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "pid|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "arch|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "os.arch"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "os|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Android"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 22736
    iget-object v3, p0, LX/09d;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 22737
    iget-object v3, p0, LX/09d;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 22738
    sget-object v4, LX/09d;->a:Ljava/nio/charset/Charset;

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 22739
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/09d;->n:Z

    goto/16 :goto_0

    .line 22740
    :catch_0
    move-object v0, v1

    :goto_2
    invoke-static {v0}, LX/09f;->a(Ljava/io/Closeable;)V

    .line 22741
    iget-object v0, p0, LX/09d;->o:Ljava/io/OutputStream;

    invoke-static {v0}, LX/09f;->a(Ljava/io/Closeable;)V

    .line 22742
    iput-object v1, p0, LX/09d;->o:Ljava/io/OutputStream;

    goto :goto_1

    .line 22743
    :catch_1
    move-object v0, v2

    goto :goto_2

    :catch_2
    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/facebook/loom/logger/LogEntry;)J
    .locals 18

    .prologue
    .line 22662
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/09d;->n:Z

    if-eqz v2, :cond_0

    .line 22663
    invoke-direct/range {p0 .. p1}, LX/09d;->b(Lcom/facebook/loom/logger/LogEntry;)V

    .line 22664
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/09d;->o:Ljava/io/OutputStream;

    if-nez v2, :cond_1

    .line 22665
    const-wide/16 v2, -0x1

    .line 22666
    :goto_0
    return-wide v2

    .line 22667
    :cond_1
    const-wide/16 v4, 0x0

    .line 22668
    const-wide/16 v2, 0x0

    .line 22669
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->c()I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_2

    .line 22670
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/09d;->p:J

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->e()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 22671
    invoke-static {v2, v3}, LX/09d;->a(J)J

    move-result-wide v4

    .line 22672
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/09d;->i:J

    sub-long v2, v4, v2

    .line 22673
    :cond_2
    move-object/from16 v0, p0

    iget-object v6, v0, LX/09d;->e:Ljava/lang/StringBuilder;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 22674
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->c()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 22675
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Entry content type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->c()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is undefined."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 22676
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->d()I

    move-result v6

    .line 22677
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->h()I

    move-result v7

    .line 22678
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->g()I

    move-result v8

    .line 22679
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->f()J

    move-result-wide v10

    .line 22680
    move-object/from16 v0, p0

    iget-object v9, v0, LX/09d;->e:Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->a()I

    move-result v12

    move-object/from16 v0, p0

    iget v13, v0, LX/09d;->h:I

    sub-int/2addr v12, v13

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v12, 0x7c

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v12, Lcom/facebook/loom/logger/LogEntry$EntryType;->a:[Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->b()I

    move-result v13

    aget-object v12, v12, v13

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v12, 0x7c

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x7c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, LX/09d;->j:I

    sub-int v3, v6, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x7c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, LX/09d;->k:I

    sub-int v3, v7, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x7c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, LX/09d;->l:I

    sub-int v3, v8, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x7c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v12, v0, LX/09d;->m:J

    sub-long v12, v10, v12

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 22681
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->a()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/09d;->h:I

    .line 22682
    move-object/from16 v0, p0

    iput-wide v4, v0, LX/09d;->i:J

    .line 22683
    move-object/from16 v0, p0

    iput v6, v0, LX/09d;->j:I

    .line 22684
    move-object/from16 v0, p0

    iput v7, v0, LX/09d;->k:I

    .line 22685
    move-object/from16 v0, p0

    iput v8, v0, LX/09d;->l:I

    .line 22686
    move-object/from16 v0, p0

    iput-wide v10, v0, LX/09d;->m:J

    .line 22687
    :goto_1
    invoke-direct/range {p0 .. p0}, LX/09d;->b()I

    move-result v2

    .line 22688
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, LX/09d;->o:Ljava/io/OutputStream;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/09d;->f:[B

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22689
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->a()I

    move-result v2

    int-to-long v2, v2

    goto/16 :goto_0

    .line 22690
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->g()I

    move-result v2

    .line 22691
    move-object/from16 v0, p0

    iget-object v3, v0, LX/09d;->e:Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x7c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/facebook/loom/logger/LogEntry$EntryType;->a:[Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->b()I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x7c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x7c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 22692
    move-object/from16 v0, p0

    iget-object v2, v0, LX/09d;->f:[B

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/facebook/loom/logger/LogEntry;->a([B)V

    .line 22693
    move-object/from16 v0, p0

    iget-object v2, v0, LX/09d;->f:[B

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->i()I

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, LX/09d;->a([BI)V

    .line 22694
    move-object/from16 v0, p0

    iget-object v2, v0, LX/09d;->e:Ljava/lang/StringBuilder;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 22695
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->i()I

    move-result v6

    .line 22696
    if-nez v6, :cond_3

    .line 22697
    move-object/from16 v0, p0

    iget v2, v0, LX/09d;->h:I

    int-to-long v2, v2

    goto/16 :goto_0

    .line 22698
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->a()I

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, LX/09d;->h:I

    sub-int/2addr v6, v7

    int-to-long v12, v6

    .line 22699
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->d()I

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, LX/09d;->j:I

    sub-int v11, v6, v7

    .line 22700
    move-object/from16 v0, p0

    iget v6, v0, LX/09d;->l:I

    rsub-int/lit8 v10, v6, 0x0

    .line 22701
    move-object/from16 v0, p0

    iget v6, v0, LX/09d;->k:I

    rsub-int/lit8 v7, v6, 0x0

    .line 22702
    move-object/from16 v0, p0

    iget-wide v8, v0, LX/09d;->m:J

    .line 22703
    move-object/from16 v0, p0

    iget-object v6, v0, LX/09d;->g:[J

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/facebook/loom/logger/LogEntry;->a([J)I

    move-result v16

    .line 22704
    const/4 v6, 0x0

    move-wide v14, v2

    move v3, v7

    move v2, v6

    move-wide v6, v8

    :goto_3
    move/from16 v0, v16

    if-ge v2, v0, :cond_4

    .line 22705
    move-object/from16 v0, p0

    iget-object v8, v0, LX/09d;->g:[J

    aget-wide v8, v8, v2

    .line 22706
    move-object/from16 v0, p0

    iget-object v0, v0, LX/09d;->e:Ljava/lang/StringBuilder;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const/16 v13, 0x7c

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/facebook/loom/logger/LogEntry$EntryType;->a:[Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->b()I

    move-result v17

    aget-object v13, v13, v17

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/16 v13, 0x7c

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const/16 v13, 0x7c

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const/16 v12, 0x7c

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v11, 0x7c

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v10, 0x7c

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v6, v8, v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v6, 0xa

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 22707
    const-wide/16 v12, 0x0

    .line 22708
    const-wide/16 v10, 0x1

    .line 22709
    const/4 v7, 0x0

    .line 22710
    const/4 v6, 0x0

    .line 22711
    const/4 v3, 0x0

    .line 22712
    add-int/lit8 v2, v2, 0x1

    move-wide v14, v12

    move-wide v12, v10

    move v11, v7

    move v10, v6

    move-wide v6, v8

    goto :goto_3

    .line 22713
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->a()I

    move-result v2

    add-int v2, v2, v16

    add-int/lit8 v2, v2, -0x1

    .line 22714
    move-object/from16 v0, p0

    iput v2, v0, LX/09d;->h:I

    .line 22715
    move-object/from16 v0, p0

    iput-wide v4, v0, LX/09d;->i:J

    .line 22716
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/loom/logger/LogEntry;->d()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/09d;->j:I

    .line 22717
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/09d;->k:I

    .line 22718
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/09d;->l:I

    .line 22719
    move-object/from16 v0, p0

    iput-wide v6, v0, LX/09d;->m:J

    goto/16 :goto_1

    .line 22720
    :catch_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/09d;->o:Ljava/io/OutputStream;

    invoke-static {v2}, LX/09f;->a(Ljava/io/Closeable;)V

    .line 22721
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, LX/09d;->o:Ljava/io/OutputStream;

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 22659
    iget-object v0, p0, LX/09d;->o:Ljava/io/OutputStream;

    invoke-static {v0}, LX/09f;->a(Ljava/io/Closeable;)V

    .line 22660
    const/4 v0, 0x0

    iput-object v0, p0, LX/09d;->o:Ljava/io/OutputStream;

    .line 22661
    return-void
.end method
