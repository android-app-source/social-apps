.class public final LX/06j;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:LX/05e;


# direct methods
.method public constructor <init>(LX/05e;)V
    .locals 0

    .prologue
    .line 18115
    iput-object p1, p0, LX/06j;->a:LX/05e;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/16 v0, 0x26

    const v1, -0x59a691cb

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 18116
    if-eqz p2, :cond_1

    const-string v1, "com.facebook.rti.mqtt.ACTION_ZR_SWITCH"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 18117
    const-string v1, "extra_mqtt_endpoint"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 18118
    const-string v2, "extra_analytics_endpoint"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 18119
    const-string v3, "ZeroRatingConnectionConfigOverrides"

    const-string v4, "broadcast received %s, %s"

    new-array v5, v7, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18120
    iget-object v3, p0, LX/06j;->a:LX/05e;

    iget-object v3, v3, LX/05e;->d:Ljava/lang/String;

    invoke-static {v3, v1}, LX/05V;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/06j;->a:LX/05e;

    iget-object v3, v3, LX/05e;->e:Ljava/lang/String;

    invoke-static {v3, v2}, LX/05V;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 18121
    :cond_0
    iget-object v3, p0, LX/06j;->a:LX/05e;

    .line 18122
    iput-object v1, v3, LX/05e;->d:Ljava/lang/String;

    .line 18123
    iget-object v1, p0, LX/06j;->a:LX/05e;

    .line 18124
    iput-object v2, v1, LX/05e;->e:Ljava/lang/String;

    .line 18125
    iget-object v1, p0, LX/06j;->a:LX/05e;

    iget-object v1, v1, LX/05e;->b:LX/04p;

    invoke-virtual {v1}, LX/04p;->c()V

    .line 18126
    :cond_1
    const/16 v1, 0x27

    const v2, 0x2ffac15b

    invoke-static {p2, v7, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void
.end method
