.class public LX/030;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02z;
.implements LX/02w;


# instance fields
.field private final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Random;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9752
    new-instance v0, LX/03L;

    invoke-direct {v0, p0}, LX/03L;-><init>(LX/030;)V

    iput-object v0, p0, LX/030;->a:Ljava/lang/ThreadLocal;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/0Pk;)I
    .locals 2

    .prologue
    .line 9747
    check-cast p2, LX/0Pp;

    .line 9748
    iget-object v0, p0, LX/030;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    iget v1, p2, LX/0Pp;->a:I

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 9749
    iget v0, p2, LX/0Pp;->b:I

    .line 9750
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(II)Z
    .locals 1

    .prologue
    .line 9760
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;ILjava/lang/Object;)Z
    .locals 3
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9754
    if-nez p2, :cond_2

    if-nez p3, :cond_2

    instance-of v2, p4, LX/0Pn;

    if-eqz v2, :cond_2

    .line 9755
    check-cast p4, LX/0Pn;

    invoke-virtual {p4}, LX/0Pn;->k()I

    move-result v2

    .line 9756
    if-ne p1, v2, :cond_1

    .line 9757
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 9758
    goto :goto_0

    .line 9759
    :cond_2
    if-ne p2, p4, :cond_3

    if-eq p1, p3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;LX/0Pk;)I
    .locals 1

    .prologue
    .line 9753
    check-cast p2, LX/0Pp;

    iget v0, p2, LX/0Pp;->d:I

    return v0
.end method
