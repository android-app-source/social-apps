.class public final enum LX/04y;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/04y;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/04y;

.field public static final enum HMAC_MD5:LX/04y;

.field public static final enum HMAC_SHA1:LX/04y;

.field public static final enum HMAC_SHA256:LX/04y;


# instance fields
.field private final mAlgorithm:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 14940
    new-instance v0, LX/04y;

    const-string v1, "HMAC_MD5"

    const-string v2, "HmacMD5"

    invoke-direct {v0, v1, v3, v2}, LX/04y;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04y;->HMAC_MD5:LX/04y;

    .line 14941
    new-instance v0, LX/04y;

    const-string v1, "HMAC_SHA1"

    const-string v2, "HmacSHA1"

    invoke-direct {v0, v1, v4, v2}, LX/04y;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04y;->HMAC_SHA1:LX/04y;

    .line 14942
    new-instance v0, LX/04y;

    const-string v1, "HMAC_SHA256"

    const-string v2, "HmacSHA256"

    invoke-direct {v0, v1, v5, v2}, LX/04y;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04y;->HMAC_SHA256:LX/04y;

    .line 14943
    const/4 v0, 0x3

    new-array v0, v0, [LX/04y;

    sget-object v1, LX/04y;->HMAC_MD5:LX/04y;

    aput-object v1, v0, v3

    sget-object v1, LX/04y;->HMAC_SHA1:LX/04y;

    aput-object v1, v0, v4

    sget-object v1, LX/04y;->HMAC_SHA256:LX/04y;

    aput-object v1, v0, v5

    sput-object v0, LX/04y;->$VALUES:[LX/04y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 14934
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14935
    iput-object p3, p0, LX/04y;->mAlgorithm:Ljava/lang/String;

    .line 14936
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/04y;
    .locals 1

    .prologue
    .line 14939
    const-class v0, LX/04y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/04y;

    return-object v0
.end method

.method public static values()[LX/04y;
    .locals 1

    .prologue
    .line 14938
    sget-object v0, LX/04y;->$VALUES:[LX/04y;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/04y;

    return-object v0
.end method


# virtual methods
.method public final getAlgorithmName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14937
    iget-object v0, p0, LX/04y;->mAlgorithm:Ljava/lang/String;

    return-object v0
.end method
