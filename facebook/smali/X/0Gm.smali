.class public final LX/0Gm;
.super Landroid/util/LruCache;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "LX/0Gn;",
        "LX/0G6;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Go;


# direct methods
.method public constructor <init>(LX/0Go;I)V
    .locals 0

    .prologue
    .line 36411
    iput-object p1, p0, LX/0Gm;->a:LX/0Go;

    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 36412
    check-cast p2, LX/0Gn;

    check-cast p3, LX/0G6;

    .line 36413
    if-eqz p1, :cond_1

    .line 36414
    invoke-static {p3}, LX/0Go;->b(LX/0G6;)V

    .line 36415
    iget-object v0, p0, LX/0Gm;->a:LX/0Go;

    iget-object v0, v0, LX/0Go;->e:Landroid/util/LruCache;

    invoke-virtual {v0, p2}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36416
    :cond_0
    :goto_0
    return-void

    .line 36417
    :cond_1
    invoke-virtual {p0}, LX/0Gm;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 36418
    iget-object v0, p0, LX/0Gm;->a:LX/0Go;

    iget-object v0, v0, LX/0Go;->d:Ljava/util/HashMap;

    iget-object v1, p2, LX/0Gn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
