.class public LX/049;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;ZLandroid/os/Bundle;)V
    .locals 2
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 12585
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 12586
    const-string v1, "BrowserLiteIntent.EXTRA_PREFETCH_INFO"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 12587
    if-eqz p3, :cond_0

    .line 12588
    invoke-virtual {v0, p3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 12589
    :cond_0
    const-string v1, "ACTION_EXTRACT_HTML_RESOURCE"

    invoke-static {p0, v1, v0, p2}, LX/049;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 12590
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 12576
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 12577
    :goto_0
    return-void

    .line 12578
    :cond_0
    new-instance v1, Landroid/content/Intent;

    if-eqz p3, :cond_2

    const-class v0, Lcom/facebook/browser/lite/BrowserLiteFallbackIntentService;

    :goto_1
    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 12579
    const-string v0, "EXTRA_ACTION"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 12580
    if-eqz p2, :cond_1

    .line 12581
    invoke-virtual {v1, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 12582
    :cond_1
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 12583
    :catch_0
    goto :goto_0

    .line 12584
    :cond_2
    const-class v0, Lcom/facebook/browser/lite/BrowserLiteIntentService;

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;ZLandroid/os/Bundle;)V
    .locals 1
    .param p4    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12572
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 12573
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12574
    invoke-static {p0, v0, p3, p4}, LX/049;->a(Landroid/content/Context;Ljava/util/Map;ZLandroid/os/Bundle;)V

    .line 12575
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/Map;ZLandroid/os/Bundle;)V
    .locals 2
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;Z",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12563
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 12564
    instance-of v0, p1, Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 12565
    check-cast p1, Ljava/util/HashMap;

    .line 12566
    :goto_0
    const-string v0, "BrowserLiteIntent.EXTRA_COOKIES"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 12567
    if-eqz p3, :cond_0

    .line 12568
    invoke-virtual {v1, p3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 12569
    :cond_0
    const-string v0, "ACTION_INJECT_COOKIES"

    invoke-static {p0, v0, v1, p2}, LX/049;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 12570
    return-void

    .line 12571
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ZLandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 12561
    const-string v0, "ACTION_HANDLE_AUTO_FILL"

    invoke-static {p0, v0, p2, p1}, LX/049;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 12562
    return-void
.end method

.method public static b(Landroid/content/Context;ZLandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 12592
    const-string v0, "ACTION_HANDLE_AUTOFILL_SAVE"

    invoke-static {p0, v0, p2, p1}, LX/049;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 12593
    return-void
.end method

.method public static c(Landroid/content/Context;ZLandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 12559
    const-string v0, "ACTION_HANDLE_OFFER_CODE"

    invoke-static {p0, v0, p2, p1}, LX/049;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 12560
    return-void
.end method

.method public static d(Landroid/content/Context;ZLandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 12557
    const-string v0, "ACTION_HANDLE_IX_UPDATE_PRODUCT_HISTORY"

    invoke-static {p0, v0, p2, p1}, LX/049;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 12558
    return-void
.end method

.method public static e(Landroid/content/Context;ZLandroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 12554
    if-nez p1, :cond_0

    .line 12555
    const-string v0, "ACTION_WARM_UP"

    invoke-static {p0, v0, p2, p1}, LX/049;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 12556
    :cond_0
    return-void
.end method

.method public static f(Landroid/content/Context;ZLandroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 12552
    const-string v0, "ACTION_CLEAR_DATA"

    invoke-static {p0, v0, p2, p1}, LX/049;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 12553
    return-void
.end method

.method public static g(Landroid/content/Context;ZLandroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 12546
    const-string v0, "ACTION_SHOW_QUOTE_SHARE_NUX"

    invoke-static {p0, v0, p2, p1}, LX/049;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 12547
    return-void
.end method

.method public static h(Landroid/content/Context;ZLandroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 12550
    const-string v0, "ACTION_COMPLETE_WEB_SHARE_DIALOG"

    invoke-static {p0, v0, p2, p1}, LX/049;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 12551
    return-void
.end method

.method public static i(Landroid/content/Context;ZLandroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 12548
    const-string v0, "ACTION_CLOSE_BROWSER"

    invoke-static {p0, v0, p2, p1}, LX/049;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 12549
    return-void
.end method
