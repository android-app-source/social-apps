.class public final LX/0M1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 1

    .prologue
    .line 44952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44953
    invoke-static {p1}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/0M1;->a:Ljava/lang/String;

    .line 44954
    invoke-static {p2}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, LX/0M1;->b:[B

    .line 44955
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 44956
    instance-of v2, p1, LX/0M1;

    if-nez v2, :cond_1

    .line 44957
    :cond_0
    :goto_0
    return v0

    .line 44958
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    .line 44959
    goto :goto_0

    .line 44960
    :cond_2
    check-cast p1, LX/0M1;

    .line 44961
    iget-object v2, p0, LX/0M1;->a:Ljava/lang/String;

    iget-object v3, p1, LX/0M1;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/0M1;->b:[B

    iget-object v3, p1, LX/0M1;->b:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 44962
    iget-object v0, p0, LX/0M1;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, LX/0M1;->b:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    return v0
.end method
