.class public final LX/0NZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:[B

.field public g:[B

.field public h:[B

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:J

.field public r:J

.field public s:LX/0LS;

.field public t:I

.field public u:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, -0x1

    .line 50713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50714
    iput v1, p0, LX/0NZ;->i:I

    .line 50715
    iput v1, p0, LX/0NZ;->j:I

    .line 50716
    iput v1, p0, LX/0NZ;->k:I

    .line 50717
    iput v1, p0, LX/0NZ;->l:I

    .line 50718
    const/4 v0, 0x0

    iput v0, p0, LX/0NZ;->m:I

    .line 50719
    const/4 v0, 0x1

    iput v0, p0, LX/0NZ;->n:I

    .line 50720
    iput v1, p0, LX/0NZ;->o:I

    .line 50721
    const/16 v0, 0x1f40

    iput v0, p0, LX/0NZ;->p:I

    .line 50722
    iput-wide v2, p0, LX/0NZ;->q:J

    .line 50723
    iput-wide v2, p0, LX/0NZ;->r:J

    .line 50724
    const-string v0, "eng"

    iput-object v0, p0, LX/0NZ;->u:Ljava/lang/String;

    return-void
.end method

.method private static a(LX/0Oj;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Oj;",
            ")",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 50699
    const/16 v0, 0x10

    :try_start_0
    invoke-virtual {p0, v0}, LX/0Oj;->c(I)V

    .line 50700
    invoke-virtual {p0}, LX/0Oj;->l()J

    move-result-wide v0

    .line 50701
    const-wide/32 v2, 0x31435657

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 50702
    new-instance v2, LX/0L6;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported FourCC compression type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50703
    :catch_0
    new-instance v0, LX/0L6;

    const-string v1, "Error parsing FourCC VC1 codec private"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50704
    :cond_0
    iget v0, p0, LX/0Oj;->b:I

    move v0, v0

    .line 50705
    add-int/lit8 v0, v0, 0x14

    .line 50706
    iget-object v1, p0, LX/0Oj;->a:[B

    .line 50707
    :goto_0
    array-length v2, v1

    add-int/lit8 v2, v2, -0x4

    if-ge v0, v2, :cond_2

    .line 50708
    aget-byte v2, v1, v0

    if-nez v2, :cond_1

    add-int/lit8 v2, v0, 0x1

    aget-byte v2, v1, v2

    if-nez v2, :cond_1

    add-int/lit8 v2, v0, 0x2

    aget-byte v2, v1, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    add-int/lit8 v2, v0, 0x3

    aget-byte v2, v1, v2

    const/16 v3, 0xf

    if-ne v2, v3, :cond_1

    .line 50709
    array-length v2, v1

    invoke-static {v1, v0, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 50710
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 50711
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50712
    :cond_2
    new-instance v0, LX/0L6;

    const-string v1, "Failed to find FourCC VC1 initialization data"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a([B)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v0, 0x0

    .line 50672
    const/4 v1, 0x0

    :try_start_0
    aget-byte v1, p0, v1

    if-eq v1, v2, :cond_0

    .line 50673
    new-instance v0, LX/0L6;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50674
    :catch_0
    new-instance v0, LX/0L6;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v0

    move v3, v4

    .line 50675
    :goto_0
    :try_start_1
    aget-byte v1, p0, v3

    if-ne v1, v5, :cond_1

    .line 50676
    add-int/lit16 v1, v2, 0xff

    .line 50677
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    .line 50678
    :cond_1
    add-int/lit8 v1, v3, 0x1

    aget-byte v3, p0, v3

    add-int/2addr v2, v3

    .line 50679
    :goto_1
    aget-byte v3, p0, v1

    if-ne v3, v5, :cond_2

    .line 50680
    add-int/lit16 v0, v0, 0xff

    .line 50681
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 50682
    :cond_2
    add-int/lit8 v3, v1, 0x1

    aget-byte v1, p0, v1

    add-int/2addr v0, v1

    .line 50683
    aget-byte v1, p0, v3

    if-eq v1, v4, :cond_3

    .line 50684
    new-instance v0, LX/0L6;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50685
    :cond_3
    new-array v1, v2, [B

    .line 50686
    const/4 v4, 0x0

    invoke-static {p0, v3, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 50687
    add-int/2addr v2, v3

    .line 50688
    aget-byte v3, p0, v2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_4

    .line 50689
    new-instance v0, LX/0L6;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50690
    :cond_4
    add-int/2addr v0, v2

    .line 50691
    aget-byte v2, p0, v0

    const/4 v3, 0x5

    if-eq v2, v3, :cond_5

    .line 50692
    new-instance v0, LX/0L6;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50693
    :cond_5
    array-length v2, p0

    sub-int/2addr v2, v0

    new-array v2, v2, [B

    .line 50694
    const/4 v3, 0x0

    array-length v4, p0

    sub-int/2addr v4, v0

    invoke-static {p0, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 50695
    new-instance v0, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 50696
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50697
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    .line 50698
    return-object v0
.end method

.method private static b(LX/0Oj;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Oj;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 50725
    const/4 v1, 0x4

    :try_start_0
    invoke-virtual {p0, v1}, LX/0Oj;->b(I)V

    .line 50726
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v1

    and-int/lit8 v1, v1, 0x3

    add-int/lit8 v2, v1, 0x1

    .line 50727
    const/4 v1, 0x3

    if-ne v2, v1, :cond_0

    .line 50728
    new-instance v0, LX/0L6;

    invoke-direct {v0}, LX/0L6;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50729
    :catch_0
    new-instance v0, LX/0L6;

    const-string v1, "Error parsing AVC codec private"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50730
    :cond_0
    :try_start_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 50731
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v1

    and-int/lit8 v4, v1, 0x1f

    move v1, v0

    .line 50732
    :goto_0
    if-ge v1, v4, :cond_1

    .line 50733
    invoke-static {p0}, LX/0Oh;->a(LX/0Oj;)[B

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50734
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 50735
    :cond_1
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v1

    .line 50736
    :goto_1
    if-ge v0, v1, :cond_2

    .line 50737
    invoke-static {p0}, LX/0Oh;->a(LX/0Oj;)[B

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50738
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 50739
    :cond_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    return-object v0
.end method

.method private static c(LX/0Oj;)Landroid/util/Pair;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Oj;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 50540
    const/16 v0, 0x15

    :try_start_0
    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 50541
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    and-int/lit8 v5, v0, 0x3

    .line 50542
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v6

    .line 50543
    iget v0, p0, LX/0Oj;->b:I

    move v7, v0

    .line 50544
    move v3, v1

    move v4, v1

    .line 50545
    :goto_0
    if-ge v3, v6, :cond_1

    .line 50546
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/0Oj;->c(I)V

    .line 50547
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v8

    move v0, v1

    move v2, v4

    .line 50548
    :goto_1
    if-ge v0, v8, :cond_0

    .line 50549
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v4

    .line 50550
    add-int/lit8 v9, v4, 0x4

    add-int/2addr v2, v9

    .line 50551
    invoke-virtual {p0, v4}, LX/0Oj;->c(I)V

    .line 50552
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 50553
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v2

    goto :goto_0

    .line 50554
    :cond_1
    invoke-virtual {p0, v7}, LX/0Oj;->b(I)V

    .line 50555
    new-array v7, v4, [B

    move v3, v1

    move v0, v1

    .line 50556
    :goto_2
    if-ge v3, v6, :cond_3

    .line 50557
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/0Oj;->c(I)V

    .line 50558
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v8

    move v2, v0

    move v0, v1

    .line 50559
    :goto_3
    if-ge v0, v8, :cond_2

    .line 50560
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v9

    .line 50561
    sget-object v10, LX/0Oh;->a:[B

    const/4 v11, 0x0

    sget-object v12, LX/0Oh;->a:[B

    array-length v12, v12

    invoke-static {v10, v11, v7, v2, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 50562
    sget-object v10, LX/0Oh;->a:[B

    array-length v10, v10

    add-int/2addr v2, v10

    .line 50563
    iget-object v10, p0, LX/0Oj;->a:[B

    .line 50564
    iget v11, p0, LX/0Oj;->b:I

    move v11, v11

    .line 50565
    invoke-static {v10, v11, v7, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 50566
    add-int/2addr v2, v9

    .line 50567
    invoke-virtual {p0, v9}, LX/0Oj;->c(I)V

    .line 50568
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 50569
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    .line 50570
    :cond_3
    if-nez v4, :cond_4

    const/4 v0, 0x0

    .line 50571
    :goto_4
    add-int/lit8 v1, v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 50572
    :cond_4
    invoke-static {v7}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_4

    .line 50573
    :catch_0
    new-instance v0, LX/0L6;

    const-string v1, "Error parsing HEVC codec private"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static d(LX/0Oj;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 50664
    :try_start_0
    invoke-virtual {p0}, LX/0Oj;->h()I

    move-result v2

    .line 50665
    if-ne v2, v0, :cond_1

    .line 50666
    :cond_0
    :goto_0
    return v0

    .line 50667
    :cond_1
    const v3, 0xfffe

    if-ne v2, v3, :cond_3

    .line 50668
    const/16 v2, 0x18

    invoke-virtual {p0, v2}, LX/0Oj;->b(I)V

    .line 50669
    invoke-virtual {p0}, LX/0Oj;->o()J

    move-result-wide v2

    sget-object v4, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->c:Ljava/util/UUID;

    invoke-virtual {v4}, Ljava/util/UUID;->getMostSignificantBits()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    invoke-virtual {p0}, LX/0Oj;->o()J

    move-result-wide v2

    sget-object v4, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->c:Ljava/util/UUID;

    invoke-virtual {v4}, Ljava/util/UUID;->getLeastSignificantBits()J
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 50670
    goto :goto_0

    .line 50671
    :catch_0
    new-instance v0, LX/0L6;

    const-string v1, "Error parsing MS/ACM codec private"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(LX/0LU;IJ)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x3

    const/16 v3, 0x10

    const/16 v5, 0x8

    const/4 v2, -0x1

    .line 50574
    iget-object v1, p0, LX/0NZ;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_0
    move v1, v2

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 50575
    new-instance v0, LX/0L6;

    const-string v1, "Unrecognized codec identifier."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50576
    :sswitch_0
    const-string v6, "V_VP8"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v6, "V_VP9"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v6, "V_MPEG2"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v6, "V_MPEG4/ISO/SP"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v4

    goto :goto_0

    :sswitch_4
    const-string v6, "V_MPEG4/ISO/ASP"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v6, "V_MPEG4/ISO/AP"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v6, "V_MPEG4/ISO/AVC"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v6, "V_MPEGH/ISO/HEVC"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v6, "V_MS/VFW/FOURCC"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v5

    goto :goto_0

    :sswitch_9
    const-string v6, "A_VORBIS"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x9

    goto :goto_0

    :sswitch_a
    const-string v6, "A_OPUS"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xa

    goto :goto_0

    :sswitch_b
    const-string v6, "A_AAC"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v6, "A_MPEG/L3"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v6, "A_AC3"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v6, "A_EAC3"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v6, "A_TRUEHD"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v6, "A_DTS"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v3

    goto/16 :goto_0

    :sswitch_11
    const-string v6, "A_DTS/EXPRESS"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v6, "A_DTS/LOSSLESS"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v6, "A_FLAC"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v6, "A_MS/ACM"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v6, "A_PCM/INT/LIT"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v6, "S_TEXT/UTF8"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v6, "S_VOBSUB"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v6, "S_HDMV/PGS"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x18

    goto/16 :goto_0

    .line 50577
    :pswitch_0
    const-string v1, "video/x-vnd.on2.vp8"

    move-object v8, v0

    move v3, v2

    .line 50578
    :goto_1
    invoke-static {v1}, LX/0Al;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 50579
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget v6, p0, LX/0NZ;->n:I

    iget v7, p0, LX/0NZ;->p:I

    iget-object v9, p0, LX/0NZ;->u:Ljava/lang/String;

    move-wide v4, p3

    invoke-static/range {v0 .. v9}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v0

    .line 50580
    :goto_2
    iget v1, p0, LX/0NZ;->b:I

    invoke-interface {p1, v1}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v1

    iput-object v1, p0, LX/0NZ;->s:LX/0LS;

    .line 50581
    iget-object v1, p0, LX/0NZ;->s:LX/0LS;

    invoke-interface {v1, v0}, LX/0LS;->a(LX/0L4;)V

    .line 50582
    return-void

    .line 50583
    :pswitch_1
    const-string v1, "video/x-vnd.on2.vp9"

    move-object v8, v0

    move v3, v2

    .line 50584
    goto :goto_1

    .line 50585
    :pswitch_2
    const-string v1, "video/mpeg2"

    move-object v8, v0

    move v3, v2

    .line 50586
    goto :goto_1

    .line 50587
    :pswitch_3
    const-string v1, "video/mp4v-es"

    .line 50588
    iget-object v3, p0, LX/0NZ;->h:[B

    if-nez v3, :cond_1

    :goto_3
    move-object v8, v0

    move v3, v2

    .line 50589
    goto :goto_1

    .line 50590
    :cond_1
    iget-object v0, p0, LX/0NZ;->h:[B

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_3

    .line 50591
    :pswitch_4
    const-string v3, "video/avc"

    .line 50592
    new-instance v0, LX/0Oj;

    iget-object v1, p0, LX/0NZ;->h:[B

    invoke-direct {v0, v1}, LX/0Oj;-><init>([B)V

    invoke-static {v0}, LX/0NZ;->b(LX/0Oj;)Landroid/util/Pair;

    move-result-object v1

    .line 50593
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 50594
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, LX/0NZ;->t:I

    move-object v1, v3

    move-object v8, v0

    move v3, v2

    .line 50595
    goto :goto_1

    .line 50596
    :pswitch_5
    const-string v3, "video/hevc"

    .line 50597
    new-instance v0, LX/0Oj;

    iget-object v1, p0, LX/0NZ;->h:[B

    invoke-direct {v0, v1}, LX/0Oj;-><init>([B)V

    invoke-static {v0}, LX/0NZ;->c(LX/0Oj;)Landroid/util/Pair;

    move-result-object v1

    .line 50598
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 50599
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, LX/0NZ;->t:I

    move-object v1, v3

    move-object v8, v0

    move v3, v2

    .line 50600
    goto :goto_1

    .line 50601
    :pswitch_6
    const-string v1, "video/wvc1"

    .line 50602
    new-instance v0, LX/0Oj;

    iget-object v3, p0, LX/0NZ;->h:[B

    invoke-direct {v0, v3}, LX/0Oj;-><init>([B)V

    invoke-static {v0}, LX/0NZ;->a(LX/0Oj;)Ljava/util/List;

    move-result-object v0

    move-object v8, v0

    move v3, v2

    .line 50603
    goto/16 :goto_1

    .line 50604
    :pswitch_7
    const-string v1, "audio/vorbis"

    .line 50605
    const/16 v3, 0x2000

    .line 50606
    iget-object v0, p0, LX/0NZ;->h:[B

    invoke-static {v0}, LX/0NZ;->a([B)Ljava/util/List;

    move-result-object v0

    move-object v8, v0

    .line 50607
    goto/16 :goto_1

    .line 50608
    :pswitch_8
    const-string v1, "audio/opus"

    .line 50609
    const/16 v3, 0x1680

    .line 50610
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 50611
    iget-object v4, p0, LX/0NZ;->h:[B

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50612
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v4

    iget-wide v6, p0, LX/0NZ;->q:J

    invoke-virtual {v4, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50613
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v4

    iget-wide v6, p0, LX/0NZ;->r:J

    invoke-virtual {v4, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v8, v0

    .line 50614
    goto/16 :goto_1

    .line 50615
    :pswitch_9
    const-string v1, "audio/mp4a-latm"

    .line 50616
    iget-object v0, p0, LX/0NZ;->h:[B

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    move-object v8, v0

    move v3, v2

    .line 50617
    goto/16 :goto_1

    .line 50618
    :pswitch_a
    const-string v1, "audio/mpeg"

    .line 50619
    const/16 v3, 0x1000

    move-object v8, v0

    .line 50620
    goto/16 :goto_1

    .line 50621
    :pswitch_b
    const-string v1, "audio/ac3"

    move-object v8, v0

    move v3, v2

    .line 50622
    goto/16 :goto_1

    .line 50623
    :pswitch_c
    const-string v1, "audio/eac3"

    move-object v8, v0

    move v3, v2

    .line 50624
    goto/16 :goto_1

    .line 50625
    :pswitch_d
    const-string v1, "audio/true-hd"

    move-object v8, v0

    move v3, v2

    .line 50626
    goto/16 :goto_1

    .line 50627
    :pswitch_e
    const-string v1, "audio/vnd.dts"

    move-object v8, v0

    move v3, v2

    .line 50628
    goto/16 :goto_1

    .line 50629
    :pswitch_f
    const-string v1, "audio/vnd.dts.hd"

    move-object v8, v0

    move v3, v2

    .line 50630
    goto/16 :goto_1

    .line 50631
    :pswitch_10
    const-string v1, "audio/x-flac"

    .line 50632
    iget-object v0, p0, LX/0NZ;->h:[B

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    move-object v8, v0

    move v3, v2

    .line 50633
    goto/16 :goto_1

    .line 50634
    :pswitch_11
    const-string v1, "audio/raw"

    .line 50635
    new-instance v4, LX/0Oj;

    iget-object v5, p0, LX/0NZ;->h:[B

    invoke-direct {v4, v5}, LX/0Oj;-><init>([B)V

    invoke-static {v4}, LX/0NZ;->d(LX/0Oj;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 50636
    new-instance v0, LX/0L6;

    const-string v1, "Non-PCM MS/ACM is unsupported"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50637
    :cond_2
    iget v4, p0, LX/0NZ;->o:I

    if-eq v4, v3, :cond_c

    .line 50638
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported PCM bit depth: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/0NZ;->o:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50639
    :pswitch_12
    const-string v1, "audio/raw"

    .line 50640
    iget v4, p0, LX/0NZ;->o:I

    if-eq v4, v3, :cond_c

    .line 50641
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported PCM bit depth: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/0NZ;->o:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50642
    :pswitch_13
    const-string v1, "application/x-subrip"

    move-object v8, v0

    move v3, v2

    .line 50643
    goto/16 :goto_1

    .line 50644
    :pswitch_14
    const-string v1, "application/vobsub"

    .line 50645
    iget-object v0, p0, LX/0NZ;->h:[B

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    move-object v8, v0

    move v3, v2

    .line 50646
    goto/16 :goto_1

    .line 50647
    :pswitch_15
    const-string v1, "application/pgs"

    move-object v8, v0

    move v3, v2

    .line 50648
    goto/16 :goto_1

    .line 50649
    :cond_3
    invoke-static {v1}, LX/0Al;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 50650
    iget v0, p0, LX/0NZ;->m:I

    if-nez v0, :cond_4

    .line 50651
    iget v0, p0, LX/0NZ;->k:I

    if-ne v0, v2, :cond_6

    iget v0, p0, LX/0NZ;->i:I

    :goto_4
    iput v0, p0, LX/0NZ;->k:I

    .line 50652
    iget v0, p0, LX/0NZ;->l:I

    if-ne v0, v2, :cond_7

    iget v0, p0, LX/0NZ;->j:I

    :goto_5
    iput v0, p0, LX/0NZ;->l:I

    .line 50653
    :cond_4
    const/high16 v10, -0x40800000    # -1.0f

    .line 50654
    iget v0, p0, LX/0NZ;->k:I

    if-eq v0, v2, :cond_5

    iget v0, p0, LX/0NZ;->l:I

    if-eq v0, v2, :cond_5

    .line 50655
    iget v0, p0, LX/0NZ;->j:I

    iget v4, p0, LX/0NZ;->k:I

    mul-int/2addr v0, v4

    int-to-float v0, v0

    iget v4, p0, LX/0NZ;->i:I

    iget v5, p0, LX/0NZ;->l:I

    mul-int/2addr v4, v5

    int-to-float v4, v4

    div-float v10, v0, v4

    .line 50656
    :cond_5
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget v6, p0, LX/0NZ;->i:I

    iget v7, p0, LX/0NZ;->j:I

    move-wide v4, p3

    move v9, v2

    invoke-static/range {v0 .. v10}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)LX/0L4;

    move-result-object v0

    goto/16 :goto_2

    .line 50657
    :cond_6
    iget v0, p0, LX/0NZ;->k:I

    goto :goto_4

    .line 50658
    :cond_7
    iget v0, p0, LX/0NZ;->l:I

    goto :goto_5

    .line 50659
    :cond_8
    const-string v0, "application/x-subrip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 50660
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, LX/0NZ;->u:Ljava/lang/String;

    move-wide v3, p3

    invoke-static/range {v0 .. v5}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)LX/0L4;

    move-result-object v0

    goto/16 :goto_2

    .line 50661
    :cond_9
    const-string v0, "application/vobsub"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "application/pgs"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 50662
    :cond_a
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, LX/0NZ;->u:Ljava/lang/String;

    move-wide v3, p3

    move-object v5, v8

    invoke-static/range {v0 .. v6}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v0

    goto/16 :goto_2

    .line 50663
    :cond_b
    new-instance v0, LX/0L6;

    const-string v1, "Unexpected MIME type."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    move-object v8, v0

    move v3, v2

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7ce7f5de -> :sswitch_5
        -0x7ce7f3b0 -> :sswitch_3
        -0x76567dc0 -> :sswitch_14
        -0x6a615338 -> :sswitch_f
        -0x672350af -> :sswitch_9
        -0x585f4fcd -> :sswitch_c
        -0x51dc40b2 -> :sswitch_8
        -0x2016c535 -> :sswitch_4
        -0x2016c4e5 -> :sswitch_6
        -0x19552dbd -> :sswitch_17
        -0x1538b2ba -> :sswitch_12
        0x3c02325 -> :sswitch_b
        0x3c02353 -> :sswitch_d
        0x3c030c5 -> :sswitch_10
        0x4e86155 -> :sswitch_0
        0x4e86156 -> :sswitch_1
        0x5e8da3e -> :sswitch_18
        0x2056f406 -> :sswitch_11
        0x2b453ce4 -> :sswitch_15
        0x32fdf009 -> :sswitch_7
        0x54c61e47 -> :sswitch_16
        0x6bd6c624 -> :sswitch_2
        0x7446132a -> :sswitch_e
        0x7446b0a6 -> :sswitch_13
        0x744ad97d -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method
