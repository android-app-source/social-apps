.class public LX/08Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private final a:LX/08P;

.field private final b:LX/0DN;

.field public c:Landroid/hardware/SensorManager;

.field public d:Landroid/hardware/Sensor;


# direct methods
.method public constructor <init>(LX/0DN;)V
    .locals 1

    .prologue
    .line 21061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21062
    new-instance v0, LX/08P;

    invoke-direct {v0}, LX/08P;-><init>()V

    iput-object v0, p0, LX/08Q;->a:LX/08P;

    .line 21063
    iput-object p1, p0, LX/08Q;->b:LX/0DN;

    .line 21064
    return-void
.end method


# virtual methods
.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 21065
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 12

    .prologue
    .line 21066
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 21067
    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v5

    .line 21068
    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v7, v7, v4

    .line 21069
    iget-object v8, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v9, 0x2

    aget v8, v8, v9

    .line 21070
    mul-float/2addr v6, v6

    mul-float/2addr v7, v7

    add-float/2addr v6, v7

    mul-float v7, v8, v8

    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    .line 21071
    const-wide/high16 v8, 0x402a000000000000L    # 13.0

    cmpl-double v6, v6, v8

    if-lez v6, :cond_8

    :goto_0
    move v0, v4

    .line 21072
    iget-wide v2, p1, Landroid/hardware/SensorEvent;->timestamp:J

    .line 21073
    iget-object v1, p0, LX/08Q;->a:LX/08P;

    .line 21074
    const-wide/32 v4, 0x1dcd6500

    sub-long v4, v2, v4

    .line 21075
    :goto_1
    iget v7, v1, LX/08P;->d:I

    const/4 v8, 0x4

    if-lt v7, v8, :cond_2

    iget-object v7, v1, LX/08P;->b:LX/08N;

    if-eqz v7, :cond_2

    iget-object v7, v1, LX/08P;->b:LX/08N;

    iget-wide v7, v7, LX/08N;->a:J

    sub-long v7, v4, v7

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-lez v7, :cond_2

    .line 21076
    iget-object v7, v1, LX/08P;->b:LX/08N;

    .line 21077
    iget-boolean v8, v7, LX/08N;->b:Z

    if-eqz v8, :cond_0

    .line 21078
    iget v8, v1, LX/08P;->e:I

    add-int/lit8 v8, v8, -0x1

    iput v8, v1, LX/08P;->e:I

    .line 21079
    :cond_0
    iget v8, v1, LX/08P;->d:I

    add-int/lit8 v8, v8, -0x1

    iput v8, v1, LX/08P;->d:I

    .line 21080
    iget-object v8, v7, LX/08N;->c:LX/08N;

    iput-object v8, v1, LX/08P;->b:LX/08N;

    .line 21081
    iget-object v8, v1, LX/08P;->b:LX/08N;

    if-nez v8, :cond_1

    .line 21082
    const/4 v8, 0x0

    iput-object v8, v1, LX/08P;->c:LX/08N;

    .line 21083
    :cond_1
    iget-object v8, v1, LX/08P;->a:LX/08O;

    invoke-virtual {v8, v7}, LX/08O;->a(LX/08N;)V

    goto :goto_1

    .line 21084
    :cond_2
    iget-object v4, v1, LX/08P;->a:LX/08O;

    .line 21085
    iget-object v5, v4, LX/08O;->a:LX/08N;

    .line 21086
    if-nez v5, :cond_9

    .line 21087
    new-instance v5, LX/08N;

    invoke-direct {v5}, LX/08N;-><init>()V

    .line 21088
    :goto_2
    move-object v4, v5

    .line 21089
    iput-wide v2, v4, LX/08N;->a:J

    .line 21090
    iput-boolean v0, v4, LX/08N;->b:Z

    .line 21091
    const/4 v5, 0x0

    iput-object v5, v4, LX/08N;->c:LX/08N;

    .line 21092
    iget-object v5, v1, LX/08P;->c:LX/08N;

    if-eqz v5, :cond_3

    .line 21093
    iget-object v5, v1, LX/08P;->c:LX/08N;

    iput-object v4, v5, LX/08N;->c:LX/08N;

    .line 21094
    :cond_3
    iput-object v4, v1, LX/08P;->c:LX/08N;

    .line 21095
    iget-object v5, v1, LX/08P;->b:LX/08N;

    if-nez v5, :cond_4

    .line 21096
    iput-object v4, v1, LX/08P;->b:LX/08N;

    .line 21097
    :cond_4
    iget v4, v1, LX/08P;->d:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, LX/08P;->d:I

    .line 21098
    if-eqz v0, :cond_5

    .line 21099
    iget v4, v1, LX/08P;->e:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, LX/08P;->e:I

    .line 21100
    :cond_5
    iget-object v0, p0, LX/08Q;->a:LX/08P;

    .line 21101
    iget-object v4, v0, LX/08P;->c:LX/08N;

    if-eqz v4, :cond_a

    iget-object v4, v0, LX/08P;->b:LX/08N;

    if-eqz v4, :cond_a

    iget-object v4, v0, LX/08P;->c:LX/08N;

    iget-wide v4, v4, LX/08N;->a:J

    iget-object v6, v0, LX/08P;->b:LX/08N;

    iget-wide v6, v6, LX/08N;->a:J

    sub-long/2addr v4, v6

    const-wide/32 v6, 0xee6b280

    cmp-long v4, v4, v6

    if-ltz v4, :cond_a

    iget v4, v0, LX/08P;->e:I

    iget v5, v0, LX/08P;->d:I

    shr-int/lit8 v5, v5, 0x1

    iget v6, v0, LX/08P;->d:I

    shr-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    if-lt v4, v5, :cond_a

    const/4 v4, 0x1

    :goto_3
    move v0, v4

    .line 21102
    if-eqz v0, :cond_7

    .line 21103
    iget-object v0, p0, LX/08Q;->a:LX/08P;

    const/4 v3, 0x0

    .line 21104
    :goto_4
    iget-object v1, v0, LX/08P;->b:LX/08N;

    if-eqz v1, :cond_6

    .line 21105
    iget-object v1, v0, LX/08P;->b:LX/08N;

    .line 21106
    iget-object v2, v1, LX/08N;->c:LX/08N;

    iput-object v2, v0, LX/08P;->b:LX/08N;

    .line 21107
    iget-object v2, v0, LX/08P;->a:LX/08O;

    invoke-virtual {v2, v1}, LX/08O;->a(LX/08N;)V

    goto :goto_4

    .line 21108
    :cond_6
    const/4 v1, 0x0

    iput-object v1, v0, LX/08P;->c:LX/08N;

    .line 21109
    iput v3, v0, LX/08P;->d:I

    .line 21110
    iput v3, v0, LX/08P;->e:I

    .line 21111
    iget-object v0, p0, LX/08Q;->b:LX/0DN;

    invoke-interface {v0}, LX/0DN;->a()V

    .line 21112
    :cond_7
    return-void

    :cond_8
    move v4, v5

    goto/16 :goto_0

    .line 21113
    :cond_9
    iget-object v6, v5, LX/08N;->c:LX/08N;

    iput-object v6, v4, LX/08O;->a:LX/08N;

    goto :goto_2

    :cond_a
    const/4 v4, 0x0

    goto :goto_3
.end method
