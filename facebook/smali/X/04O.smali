.class public LX/04O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field private static final d:LX/03b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/03b",
            "<",
            "Landroid/content/ServiceConnection;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Landroid/content/ServiceConnection;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private a:Landroid/content/ServiceConnection;

.field private b:I

.field private c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13167
    invoke-static {}, LX/03b;->a()LX/03b;

    move-result-object v0

    sput-object v0, LX/04O;->d:LX/03b;

    .line 13168
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    sput-object v0, LX/04O;->e:Ljava/lang/ref/ReferenceQueue;

    .line 13169
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/04O;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 13165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13166
    return-void
.end method

.method private static a(Landroid/content/ServiceConnection;Z)LX/04O;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 13145
    invoke-static {}, LX/04O;->a()V

    .line 13146
    sget-object v1, LX/04O;->d:LX/03b;

    monitor-enter v1

    .line 13147
    :try_start_0
    sget-object v0, LX/04O;->d:LX/03b;

    invoke-static {v0, p0}, LX/03b;->a(LX/03b;Ljava/lang/Object;)LX/03b;

    move-result-object v0

    .line 13148
    sget-object v2, LX/04O;->d:LX/03b;

    if-eq v0, v2, :cond_1

    .line 13149
    iget-object v2, v0, LX/03b;->b:LX/03b;

    .line 13150
    if-eq v2, v0, :cond_0

    invoke-virtual {v2}, LX/03b;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 13151
    invoke-virtual {v2}, LX/03b;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/04O;

    monitor-exit v1

    .line 13152
    :goto_0
    return-object v0

    .line 13153
    :cond_0
    invoke-virtual {v2}, LX/03b;->b()V

    .line 13154
    invoke-virtual {v0}, LX/03b;->b()V

    .line 13155
    invoke-static {p0, p1}, LX/04O;->a(Landroid/content/ServiceConnection;Z)LX/04O;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 13156
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 13157
    :cond_1
    if-eqz p1, :cond_2

    .line 13158
    :try_start_1
    new-instance v2, LX/03b;

    sget-object v0, LX/04O;->e:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v2, p0, v0}, LX/03b;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 13159
    new-instance v0, LX/04O;

    invoke-direct {v0}, LX/04O;-><init>()V

    .line 13160
    new-instance v3, LX/03b;

    invoke-direct {v3, v0}, LX/03b;-><init>(Ljava/lang/Object;)V

    .line 13161
    invoke-virtual {v2, v3}, LX/03b;->a(LX/03b;)V

    .line 13162
    sget-object v3, LX/04O;->d:LX/03b;

    invoke-virtual {v3, v2}, LX/03b;->b(LX/03b;)V

    .line 13163
    monitor-exit v1

    goto :goto_0

    .line 13164
    :cond_2
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private static a()V
    .locals 4

    .prologue
    .line 13137
    sget-object v0, LX/04O;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 13138
    :cond_0
    :goto_0
    return-void

    .line 13139
    :cond_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 13140
    sget-object v1, LX/04O;->d:LX/03b;

    monitor-enter v1

    .line 13141
    :goto_1
    :try_start_0
    sget-object v0, LX/04O;->e:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 13142
    check-cast v0, LX/03b;

    invoke-virtual {v0}, LX/03b;->b()V

    goto :goto_1

    .line 13143
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 13144
    sget-object v0, LX/04O;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V
    .locals 3

    .prologue
    .line 13170
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/04O;->a(Landroid/content/ServiceConnection;Z)LX/04O;

    move-result-object v0

    .line 13171
    if-nez v0, :cond_0

    .line 13172
    invoke-virtual {p0, p1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 13173
    :goto_0
    return-void

    .line 13174
    :cond_0
    const/4 v1, 0x1

    const/16 v2, 0xe

    invoke-static {v1, v2, p2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 13175
    invoke-direct {v0, p1, v1, p2}, LX/04O;->a(Landroid/content/ServiceConnection;II)V

    .line 13176
    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0
.end method

.method private a(Landroid/content/ServiceConnection;II)V
    .locals 0

    .prologue
    .line 13133
    iput-object p1, p0, LX/04O;->a:Landroid/content/ServiceConnection;

    .line 13134
    iput p2, p0, LX/04O;->c:I

    .line 13135
    iput p3, p0, LX/04O;->b:I

    .line 13136
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 13126
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 13127
    invoke-virtual {p0, p1, p2, p3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 13128
    :goto_0
    return v0

    .line 13129
    :cond_0
    const/16 v0, 0xe

    invoke-static {v1, v0, p4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 13130
    invoke-static {p2, v1}, LX/04O;->a(Landroid/content/ServiceConnection;Z)LX/04O;

    move-result-object v1

    .line 13131
    invoke-direct {v1, p2, v0, p4}, LX/04O;->a(Landroid/content/ServiceConnection;II)V

    .line 13132
    invoke-virtual {p0, p1, v1, p3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 13122
    const/16 v0, 0xf

    iget v1, p0, LX/04O;->b:I

    iget v2, p0, LX/04O;->c:I

    invoke-static {v3, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move-result v0

    .line 13123
    iget-object v1, p0, LX/04O;->a:Landroid/content/ServiceConnection;

    invoke-interface {v1, p1, p2}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 13124
    const/16 v1, 0x11

    iget v2, p0, LX/04O;->b:I

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 13125
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 13118
    const/16 v0, 0x10

    iget v1, p0, LX/04O;->b:I

    iget v2, p0, LX/04O;->c:I

    invoke-static {v3, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move-result v0

    .line 13119
    iget-object v1, p0, LX/04O;->a:Landroid/content/ServiceConnection;

    invoke-interface {v1, p1}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    .line 13120
    const/16 v1, 0x11

    iget v2, p0, LX/04O;->b:I

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 13121
    return-void
.end method
