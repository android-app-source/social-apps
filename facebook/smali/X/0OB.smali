.class public final LX/0OB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0O1;


# instance fields
.field private final a:I

.field private final b:[B

.field private c:I

.field private d:I

.field private e:[LX/0O0;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 52565
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/0OB;-><init>(II)V

    .line 52566
    return-void
.end method

.method private constructor <init>(II)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52550
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->a(Z)V

    .line 52551
    if-ltz p2, :cond_1

    :goto_1
    invoke-static {v1}, LX/0Av;->a(Z)V

    .line 52552
    iput p1, p0, LX/0OB;->a:I

    .line 52553
    iput p2, p0, LX/0OB;->d:I

    .line 52554
    add-int/lit8 v0, p2, 0x64

    new-array v0, v0, [LX/0O0;

    iput-object v0, p0, LX/0OB;->e:[LX/0O0;

    .line 52555
    if-lez p2, :cond_2

    .line 52556
    mul-int v0, p2, p1

    new-array v0, v0, [B

    iput-object v0, p0, LX/0OB;->b:[B

    .line 52557
    :goto_2
    if-ge v2, p2, :cond_3

    .line 52558
    mul-int v0, v2, p1

    .line 52559
    iget-object v1, p0, LX/0OB;->e:[LX/0O0;

    new-instance v3, LX/0O0;

    iget-object v4, p0, LX/0OB;->b:[B

    invoke-direct {v3, v4, v0}, LX/0O0;-><init>([BI)V

    aput-object v3, v1, v2

    .line 52560
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_0
    move v0, v2

    .line 52561
    goto :goto_0

    :cond_1
    move v1, v2

    .line 52562
    goto :goto_1

    .line 52563
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, LX/0OB;->b:[B

    .line 52564
    :cond_3
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/0O0;
    .locals 4

    .prologue
    .line 52542
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0OB;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0OB;->c:I

    .line 52543
    iget v0, p0, LX/0OB;->d:I

    if-lez v0, :cond_0

    .line 52544
    iget-object v0, p0, LX/0OB;->e:[LX/0O0;

    iget v1, p0, LX/0OB;->d:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/0OB;->d:I

    aget-object v0, v0, v1

    .line 52545
    iget-object v1, p0, LX/0OB;->e:[LX/0O0;

    iget v2, p0, LX/0OB;->d:I

    const/4 v3, 0x0

    aput-object v3, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52546
    :goto_0
    monitor-exit p0

    return-object v0

    .line 52547
    :cond_0
    :try_start_1
    new-instance v0, LX/0O0;

    iget v1, p0, LX/0OB;->a:I

    new-array v1, v1, [B

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/0O0;-><init>([BI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 52548
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 52567
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0OB;->a:I

    invoke-static {p1, v0}, LX/08x;->a(II)I

    move-result v0

    .line 52568
    const/4 v1, 0x0

    iget v3, p0, LX/0OB;->c:I

    sub-int/2addr v0, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 52569
    iget v1, p0, LX/0OB;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt v0, v1, :cond_1

    .line 52570
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 52571
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/0OB;->b:[B

    if-eqz v1, :cond_5

    .line 52572
    iget v1, p0, LX/0OB;->d:I

    add-int/lit8 v1, v1, -0x1

    .line 52573
    :goto_1
    if-gt v2, v1, :cond_4

    .line 52574
    iget-object v3, p0, LX/0OB;->e:[LX/0O0;

    aget-object v4, v3, v2

    .line 52575
    iget-object v3, v4, LX/0O0;->a:[B

    iget-object v5, p0, LX/0OB;->b:[B

    if-ne v3, v5, :cond_2

    .line 52576
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 52577
    :cond_2
    iget-object v3, p0, LX/0OB;->e:[LX/0O0;

    aget-object v5, v3, v2

    .line 52578
    iget-object v3, v5, LX/0O0;->a:[B

    iget-object v6, p0, LX/0OB;->b:[B

    if-eq v3, v6, :cond_3

    .line 52579
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 52580
    :cond_3
    iget-object v6, p0, LX/0OB;->e:[LX/0O0;

    add-int/lit8 v3, v2, 0x1

    aput-object v5, v6, v2

    .line 52581
    iget-object v5, p0, LX/0OB;->e:[LX/0O0;

    add-int/lit8 v2, v1, -0x1

    aput-object v4, v5, v1

    move v1, v2

    move v2, v3

    .line 52582
    goto :goto_1

    .line 52583
    :cond_4
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 52584
    iget v1, p0, LX/0OB;->d:I

    if-ge v0, v1, :cond_0

    .line 52585
    :cond_5
    iget-object v1, p0, LX/0OB;->e:[LX/0O0;

    iget v2, p0, LX/0OB;->d:I

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 52586
    iput v0, p0, LX/0OB;->d:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 52587
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0O0;)V
    .locals 3

    .prologue
    .line 52534
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, LX/0O0;->a:[B

    iget-object v1, p0, LX/0OB;->b:[B

    if-eq v0, v1, :cond_0

    iget-object v0, p1, LX/0O0;->a:[B

    array-length v0, v0

    iget v1, p0, LX/0OB;->a:I

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->a(Z)V

    .line 52535
    iget v0, p0, LX/0OB;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0OB;->c:I

    .line 52536
    iget v0, p0, LX/0OB;->d:I

    iget-object v1, p0, LX/0OB;->e:[LX/0O0;

    array-length v1, v1

    if-ne v0, v1, :cond_1

    .line 52537
    iget-object v0, p0, LX/0OB;->e:[LX/0O0;

    iget-object v1, p0, LX/0OB;->e:[LX/0O0;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0O0;

    iput-object v0, p0, LX/0OB;->e:[LX/0O0;

    .line 52538
    :cond_1
    iget-object v0, p0, LX/0OB;->e:[LX/0O0;

    iget v1, p0, LX/0OB;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0OB;->d:I

    aput-object p1, v0, v1

    .line 52539
    const v0, 0xbdad561

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52540
    monitor-exit p0

    return-void

    .line 52541
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()I
    .locals 2

    .prologue
    .line 52533
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0OB;->c:I

    iget v1, p0, LX/0OB;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    mul-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)V
    .locals 1

    .prologue
    .line 52529
    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, LX/0OB;->b()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 52530
    const v0, 0x37dd48dd

    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 52531
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 52532
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 52528
    iget v0, p0, LX/0OB;->a:I

    return v0
.end method
