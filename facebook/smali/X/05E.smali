.class public LX/05E;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/052;

.field private volatile b:Z


# direct methods
.method public constructor <init>(LX/052;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 15888
    iput-object p1, p0, LX/05E;->a:LX/052;

    .line 15889
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 15890
    return-void
.end method

.method private declared-synchronized c()V
    .locals 1

    .prologue
    .line 15891
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/05E;->b:Z

    .line 15892
    const v0, 0x7f40655b

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15893
    monitor-exit p0

    return-void

    .line 15894
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()Z
    .locals 1

    .prologue
    .line 15895
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v0, p0, LX/05E;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 15896
    const v0, 0x5b5610b9

    :try_start_1
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 15897
    :catch_0
    goto :goto_0

    .line 15898
    :cond_0
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 15899
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 15900
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/05E;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/05E;->sendMessage(Landroid/os/Message;)Z

    .line 15901
    return-void
.end method

.method public a(Landroid/content/Intent;II)V
    .locals 1

    .prologue
    .line 15902
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p2, p3, p1}, LX/05E;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/05E;->sendMessage(Landroid/os/Message;)Z

    .line 15903
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 15904
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LX/05E;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/05E;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15905
    :goto_0
    return-void

    .line 15906
    :cond_0
    invoke-direct {p0}, LX/05E;->d()Z

    goto :goto_0
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 15907
    const-string v0, "MqttBackgroundService"

    const-string v1, "handleMessage %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15908
    if-nez p1, :cond_0

    .line 15909
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Message is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 15910
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 15911
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported message"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 15912
    :pswitch_0
    iget-object v0, p0, LX/05E;->a:LX/052;

    invoke-virtual {v0}, LX/052;->a()V

    .line 15913
    :goto_0
    return-void

    .line 15914
    :pswitch_1
    iget-object v1, p0, LX/05E;->a:LX/052;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v0, v2, v3}, LX/052;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 15915
    :pswitch_2
    iget-object v0, p0, LX/05E;->a:LX/052;

    invoke-virtual {v0}, LX/052;->d()V

    .line 15916
    invoke-direct {p0}, LX/05E;->c()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
