.class public final LX/0GO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/04o;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35273
    iput-object p1, p0, LX/0GO;->a:Ljava/lang/String;

    .line 35274
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;J[LX/0AR;LX/0Ln;ZLX/0Lq;)LX/0AR;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/0LQ;",
            ">;J[",
            "LX/0AR;",
            "LX/0Ln;",
            "Z",
            "LX/0Lq;",
            ")",
            "LX/0AR;"
        }
    .end annotation

    .prologue
    .line 35275
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 35271
    return-void
.end method

.method public final a(Ljava/util/List;J[LX/0AR;LX/0Ld;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/0LQ;",
            ">;J[",
            "LX/0AR;",
            "LX/0Ld;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35258
    const/4 v1, 0x0

    .line 35259
    iget-object v0, p0, LX/0GO;->a:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 35260
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    array-length v0, p4

    if-ge v2, v0, :cond_0

    .line 35261
    aget-object v0, p4, v2

    .line 35262
    iget-object v3, v0, LX/0AR;->a:Ljava/lang/String;

    iget-object v4, p0, LX/0GO;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 35263
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 35264
    :goto_2
    if-nez v0, :cond_1

    .line 35265
    array-length v0, p4

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p4, v0

    .line 35266
    :cond_1
    iget-object v1, p5, LX/0Ld;->c:LX/0AR;

    if-eq v1, v0, :cond_2

    .line 35267
    iget-object v1, p5, LX/0Ld;->c:LX/0AR;

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_3
    iput v1, p5, LX/0Ld;->b:I

    .line 35268
    iput-object v0, p5, LX/0Ld;->c:LX/0AR;

    .line 35269
    :cond_2
    return-void

    .line 35270
    :cond_3
    const/4 v1, 0x3

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 35257
    return-void
.end method
