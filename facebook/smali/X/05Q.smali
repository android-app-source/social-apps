.class public LX/05Q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:LX/05g;

.field public B:LX/04v;

.field public C:LX/05A;

.field public D:LX/05k;

.field public E:Z

.field public a:LX/05T;

.field public b:LX/05G;

.field public c:LX/059;

.field public d:LX/06Q;

.field public e:LX/05O;

.field public f:LX/05h;

.field public g:LX/05i;

.field public h:LX/05b;

.field public i:LX/01o;

.field public j:LX/06F;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/rti/common/util/NonInjectProvider1",
            "<",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;",
            "LX/072;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/06I;

.field public l:LX/06J;

.field public m:LX/06K;

.field public n:LX/06T;

.field public o:LX/056;

.field public p:Ljava/util/concurrent/atomic/AtomicInteger;

.field public q:LX/05m;

.field public r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field public s:LX/05a;

.field public t:Landroid/app/AlarmManager;

.field public u:LX/04p;

.field public v:Ljava/util/concurrent/ScheduledExecutorService;

.field public w:LX/05Z;

.field public x:Landroid/os/PowerManager;

.field public y:Landroid/os/Handler;

.field public z:LX/05e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16057
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/05Q;->E:Z

    return-void
.end method

.method public static synthetic a(ZLX/05N;)I
    .locals 1

    .prologue
    .line 16058
    if-eqz p0, :cond_1

    .line 16059
    invoke-interface {p1}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    .line 16060
    :goto_0
    move v0, v0

    .line 16061
    return v0

    .line 16062
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 16063
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/05S;Ljava/util/List;)V
    .locals 34
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05S;",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16064
    move-object/from16 v0, p1

    iget-object v3, v0, LX/05S;->a:Landroid/content/Context;

    .line 16065
    move-object/from16 v0, p1

    iget-object v4, v0, LX/05S;->b:Ljava/lang/String;

    .line 16066
    invoke-static {v3}, LX/05T;->a(Landroid/content/Context;)LX/05T;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->a:LX/05T;

    .line 16067
    move-object/from16 v0, p0

    iget-object v2, v0, LX/05Q;->a:LX/05T;

    invoke-virtual {v2}, LX/05T;->c()Z

    move-result v2

    if-nez v2, :cond_0

    .line 16068
    const/4 v2, 0x2

    invoke-static {v2}, LX/05D;->a(I)V

    .line 16069
    :cond_0
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 16070
    invoke-static {v2}, LX/04u;->a(Ljava/lang/String;)Z

    move-result v13

    .line 16071
    invoke-static {v2}, LX/05D;->a(Ljava/lang/String;)V

    .line 16072
    move-object/from16 v0, p0

    iget-object v2, v0, LX/05Q;->a:LX/05T;

    invoke-virtual {v2}, LX/05T;->c()Z

    move-result v2

    invoke-static {v2}, LX/05Y;->a(Z)V

    .line 16073
    new-instance v5, LX/05Z;

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/05Q;->a:LX/05T;

    invoke-virtual {v2}, LX/05T;->c()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-direct {v5, v3, v4, v2}, LX/05Z;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->w:LX/05Z;

    .line 16074
    const-string v2, "connectivity"

    invoke-virtual {v3, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 16075
    const-string v5, "alarm"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/AlarmManager;

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->t:Landroid/app/AlarmManager;

    .line 16076
    const-string v5, "power"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/PowerManager;

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->x:Landroid/os/PowerManager;

    .line 16077
    const-string v5, "phone"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    move-object v11, v5

    check-cast v11, Landroid/telephony/TelephonyManager;

    .line 16078
    invoke-static {}, LX/01o;->b()LX/01o;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->i:LX/01o;

    .line 16079
    invoke-static {}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->get()Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 16080
    new-instance v5, LX/05a;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->a:LX/05T;

    move-object/from16 v0, p1

    iget-object v7, v0, LX/05S;->e:LX/05G;

    invoke-interface {v7}, LX/05G;->b()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v3, v6, v7}, LX/05a;-><init>(Landroid/content/Context;LX/05T;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->s:LX/05a;

    .line 16081
    new-instance v5, LX/05b;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->x:Landroid/os/PowerManager;

    move-object/from16 v0, p1

    iget-object v7, v0, LX/05S;->n:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-direct {v5, v3, v6, v7, v8}, LX/05b;-><init>(Landroid/content/Context;Landroid/os/PowerManager;Landroid/os/Handler;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;)V

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->h:LX/05b;

    .line 16082
    new-instance v5, LX/059;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    move-object/from16 v0, p1

    iget-object v7, v0, LX/05S;->n:Landroid/os/Handler;

    invoke-direct {v5, v2, v3, v6, v7}, LX/059;-><init>(Landroid/net/ConnectivityManager;Landroid/content/Context;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->c:LX/059;

    .line 16083
    new-instance v2, LX/05A;

    invoke-direct {v2, v3}, LX/05A;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->C:LX/05A;

    .line 16084
    move-object/from16 v0, p1

    iget-object v2, v0, LX/05S;->e:LX/05G;

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->b:LX/05G;

    .line 16085
    move-object/from16 v0, p1

    iget-object v2, v0, LX/05S;->k:LX/04p;

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->u:LX/04p;

    .line 16086
    new-instance v2, LX/05e;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/05Q;->u:LX/04p;

    invoke-direct {v2, v3, v5}, LX/05e;-><init>(Landroid/content/Context;LX/04p;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->z:LX/05e;

    .line 16087
    new-instance v2, LX/05g;

    invoke-direct {v2, v3}, LX/05g;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->A:LX/05g;

    .line 16088
    move-object/from16 v0, p0

    iget-object v2, v0, LX/05Q;->u:LX/04p;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/05Q;->z:LX/05e;

    invoke-virtual {v2, v5}, LX/04p;->a(LX/05f;)V

    .line 16089
    move-object/from16 v0, p0

    iget-object v2, v0, LX/05Q;->u:LX/04p;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/05Q;->A:LX/05g;

    invoke-virtual {v2, v5}, LX/04p;->a(LX/05f;)V

    .line 16090
    move-object/from16 v0, p0

    iget-object v2, v0, LX/05Q;->u:LX/04p;

    invoke-virtual {v2}, LX/04p;->a()V

    .line 16091
    move-object/from16 v0, p1

    iget-object v2, v0, LX/05S;->p:LX/05O;

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->e:LX/05O;

    .line 16092
    new-instance v2, LX/05h;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/05S;->q:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->c:LX/059;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/05Q;->C:LX/05A;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/05Q;->e:LX/05O;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-direct/range {v2 .. v9}, LX/05h;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/059;LX/05A;LX/05O;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->f:LX/05h;

    .line 16093
    new-instance v2, LX/05i;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->c:LX/059;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/05Q;->h:LX/05b;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/05Q;->i:LX/01o;

    move-object/from16 v0, p1

    iget-object v10, v0, LX/05S;->l:LX/05N;

    move-object v5, v11

    invoke-direct/range {v2 .. v10}, LX/05i;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/telephony/TelephonyManager;LX/059;LX/05b;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;LX/01o;LX/05N;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->g:LX/05i;

    .line 16094
    move-object/from16 v0, p1

    iget-object v2, v0, LX/05S;->z:LX/05M;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/05Q;->c:LX/059;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->i:LX/01o;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/05Q;->f:LX/05h;

    invoke-virtual {v2, v5, v6, v7}, LX/05M;->a(LX/059;LX/01o;LX/05h;)V

    .line 16095
    new-instance v5, LX/05k;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->f:LX/05h;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    move-object/from16 v0, p1

    iget-object v2, v0, LX/05S;->C:LX/05N;

    invoke-interface {v2}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {v5, v3, v6, v7, v2}, LX/05k;-><init>(Landroid/content/Context;LX/05h;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Z)V

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->D:LX/05k;

    .line 16096
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v12

    .line 16097
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    .line 16098
    const/4 v5, 0x2

    invoke-static {v5}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v11

    .line 16099
    const/4 v5, 0x2

    invoke-static {v5}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v14

    .line 16100
    move-object/from16 v0, p1

    iget-object v5, v0, LX/05S;->n:Landroid/os/Handler;

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->y:Landroid/os/Handler;

    .line 16101
    new-instance v5, LX/05l;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->y:Landroid/os/Handler;

    invoke-direct {v5, v6}, LX/05l;-><init>(Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->q:LX/05m;

    .line 16102
    new-instance v5, LX/05q;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/05Q;->t:Landroid/app/AlarmManager;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/05Q;->y:Landroid/os/Handler;

    move-object v6, v4

    move-object v7, v3

    invoke-direct/range {v5 .. v10}, LX/05q;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Landroid/app/AlarmManager;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->v:Ljava/util/concurrent/ScheduledExecutorService;

    .line 16103
    new-instance v5, Ljava/util/concurrent/atomic/AtomicInteger;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->u:LX/04p;

    invoke-virtual {v6}, LX/04p;->b()LX/04q;

    move-result-object v6

    iget v6, v6, LX/04q;->p:I

    invoke-direct {v5, v6}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 16104
    new-instance v5, LX/05w;

    new-instance v6, LX/05x;

    invoke-direct {v6}, LX/05x;-><init>()V

    invoke-direct {v5, v6}, LX/05w;-><init>(Ljavax/net/ssl/HostnameVerifier;)V

    .line 16105
    new-instance v9, LX/05y;

    new-instance v6, LX/05z;

    invoke-direct {v6, v5}, LX/05z;-><init>(LX/05w;)V

    invoke-direct {v9, v11, v6, v5}, LX/05y;-><init>(Ljava/util/concurrent/ExecutorService;LX/05z;LX/05w;)V

    .line 16106
    new-instance v11, LX/06B;

    new-instance v5, LX/06C;

    invoke-direct {v5}, LX/06C;-><init>()V

    invoke-direct {v11, v3, v2, v5}, LX/06B;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/06C;)V

    .line 16107
    new-instance v5, LX/06F;

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move v8, v13

    move-object v10, v14

    invoke-direct/range {v5 .. v12}, LX/06F;-><init>(LX/05Q;LX/05S;ZLX/05y;Ljava/util/concurrent/ScheduledExecutorService;LX/06B;Ljava/util/concurrent/ExecutorService;)V

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->j:LX/06F;

    .line 16108
    move-object/from16 v0, p0

    iget-object v2, v0, LX/05Q;->u:LX/04p;

    invoke-virtual {v2}, LX/04p;->b()LX/04q;

    move-result-object v2

    iget v2, v2, LX/04q;->B:I

    .line 16109
    move-object/from16 v0, p0

    iget-object v5, v0, LX/05Q;->u:LX/04p;

    invoke-virtual {v5}, LX/04p;->b()LX/04q;

    move-result-object v5

    iget v5, v5, LX/04q;->C:I

    .line 16110
    if-lez v2, :cond_2

    if-lez v5, :cond_2

    .line 16111
    new-instance v12, LX/06G;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    int-to-long v8, v5

    invoke-direct {v12, v6, v2, v8, v9}, LX/06G;-><init>(Lcom/facebook/rti/common/time/RealtimeSinceBootClock;IJ)V

    .line 16112
    :goto_1
    new-instance v5, LX/06I;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    move-object/from16 v0, p1

    iget-object v2, v0, LX/05S;->l:LX/05N;

    if-eqz v2, :cond_3

    move-object/from16 v0, p1

    iget-object v7, v0, LX/05S;->l:LX/05N;

    :goto_2
    move-object/from16 v0, p0

    iget-object v8, v0, LX/05Q;->q:LX/05m;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/05Q;->v:Ljava/util/concurrent/ScheduledExecutorService;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/05Q;->y:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/05Q;->u:LX/04p;

    invoke-direct/range {v5 .. v12}, LX/06I;-><init>(Lcom/facebook/rti/common/time/RealtimeSinceBootClock;LX/05N;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;Landroid/os/Handler;LX/04p;LX/06H;)V

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->k:LX/06I;

    .line 16113
    new-instance v5, LX/06J;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->q:LX/05m;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/05Q;->v:Ljava/util/concurrent/ScheduledExecutorService;

    check-cast v7, LX/05q;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/05Q;->f:LX/05h;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/05Q;->g:LX/05i;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/05Q;->c:LX/059;

    invoke-direct/range {v5 .. v11}, LX/06J;-><init>(LX/05m;LX/05q;LX/05h;LX/05i;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;LX/059;)V

    move-object/from16 v0, p0

    iput-object v5, v0, LX/05Q;->l:LX/06J;

    .line 16114
    new-instance v2, LX/06K;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/05Q;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/05Q;->t:Landroid/app/AlarmManager;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/05Q;->y:Landroid/os/Handler;

    invoke-direct/range {v2 .. v8}, LX/06K;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Landroid/app/AlarmManager;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->m:LX/06K;

    .line 16115
    new-instance v2, LX/06Q;

    invoke-direct {v2}, LX/06Q;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->d:LX/06Q;

    .line 16116
    move-object/from16 v0, p0

    iget-object v2, v0, LX/05Q;->d:LX/06Q;

    iget-object v2, v2, LX/06Q;->a:Ljava/util/Set;

    new-instance v5, LX/06S;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->c:LX/059;

    invoke-direct {v5, v6}, LX/06S;-><init>(LX/059;)V

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 16117
    new-instance v2, LX/06T;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->t:Landroid/app/AlarmManager;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/05Q;->y:Landroid/os/Handler;

    move-object/from16 v0, p1

    iget-object v8, v0, LX/05S;->y:LX/05H;

    invoke-direct/range {v2 .. v8}, LX/06T;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Landroid/app/AlarmManager;Landroid/os/Handler;LX/05H;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->n:LX/06T;

    .line 16118
    move-object/from16 v0, p1

    iget-object v2, v0, LX/05S;->c:LX/056;

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->o:LX/056;

    .line 16119
    move-object/from16 v0, p0

    iget-object v2, v0, LX/05Q;->o:LX/056;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/05S;->d:LX/055;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05Q;->j:LX/06F;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/05Q;->k:LX/06I;

    move-object/from16 v0, p1

    iget-object v8, v0, LX/05S;->f:LX/05F;

    move-object/from16 v0, p1

    iget-object v9, v0, LX/05S;->e:LX/05G;

    move-object/from16 v0, p1

    iget-object v10, v0, LX/05S;->h:LX/05F;

    move-object/from16 v0, p1

    iget-object v11, v0, LX/05S;->g:LX/05G;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/05Q;->l:LX/06J;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/05Q;->m:LX/06K;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/05Q;->n:LX/06T;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/05Q;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05Q;->f:LX/05h;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05Q;->g:LX/05i;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05Q;->y:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05Q;->u:LX/04p;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/05S;->y:LX/05H;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/05S;->i:LX/05L;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05Q;->x:Landroid/os/PowerManager;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05Q;->c:LX/059;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05Q;->h:LX/05b;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05Q;->z:LX/05e;

    move-object/from16 v26, v0

    new-instance v27, LX/06V;

    invoke-direct/range {v27 .. v27}, LX/06V;-><init>()V

    move-object/from16 v0, p1

    iget v0, v0, LX/05S;->D:I

    move/from16 v29, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/05S;->E:Z

    move/from16 v30, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/05S;->G:Z

    move/from16 v31, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/05S;->I:Z

    move/from16 v32, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/05S;->J:Z

    move/from16 v33, v0

    move-object/from16 v28, p2

    invoke-virtual/range {v2 .. v33}, LX/056;->a(Landroid/content/Context;LX/055;Ljava/lang/String;LX/06F;LX/06I;LX/05F;LX/05G;LX/05F;LX/05G;LX/06J;LX/06K;LX/06T;Ljava/util/concurrent/atomic/AtomicInteger;LX/05h;LX/05i;Landroid/os/Handler;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;LX/04p;LX/05H;LX/05L;Landroid/os/PowerManager;LX/059;LX/05b;LX/05e;Ljava/util/concurrent/ExecutorService;Ljava/util/List;IZZZZ)V

    .line 16120
    move-object/from16 v0, p1

    iget-object v2, v0, LX/05S;->r:LX/04v;

    move-object/from16 v0, p0

    iput-object v2, v0, LX/05Q;->B:LX/04v;

    .line 16121
    return-void

    .line 16122
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 16123
    :cond_2
    new-instance v12, LX/0HY;

    invoke-direct {v12}, LX/0HY;-><init>()V

    goto/16 :goto_1

    .line 16124
    :cond_3
    new-instance v7, LX/0Hm;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, LX/0Hm;-><init>(LX/05Q;)V

    goto/16 :goto_2
.end method
