.class public LX/078;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final y:Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/Long;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/Long;

.field public final d:Ljava/lang/Long;

.field public final e:Ljava/lang/Integer;

.field public final f:Ljava/lang/Integer;

.field public final g:Ljava/lang/Boolean;

.field public final h:Ljava/lang/Boolean;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/Boolean;

.field public final l:Ljava/lang/Long;

.field public final m:I

.field public final n:Ljava/lang/String;

.field public final o:Ljava/lang/String;

.field public final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Ljava/lang/String;

.field public final r:Ljava/lang/String;

.field public final s:Ljava/lang/Long;

.field public final t:Ljava/lang/String;

.field public final u:Ljava/lang/String;

.field public final v:Ljava/lang/String;

.field public final w:Ljava/lang/Byte;

.field public final x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19290
    const-class v0, LX/078;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/078;->y:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Byte;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Byte;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19265
    iput-object p1, p0, LX/078;->a:Ljava/lang/Long;

    .line 19266
    iput-object p2, p0, LX/078;->b:Ljava/lang/String;

    .line 19267
    iput-object p3, p0, LX/078;->c:Ljava/lang/Long;

    .line 19268
    iput-object p4, p0, LX/078;->d:Ljava/lang/Long;

    .line 19269
    iput-object p5, p0, LX/078;->e:Ljava/lang/Integer;

    .line 19270
    iput-object p6, p0, LX/078;->f:Ljava/lang/Integer;

    .line 19271
    iput-object p7, p0, LX/078;->g:Ljava/lang/Boolean;

    .line 19272
    iput-object p8, p0, LX/078;->h:Ljava/lang/Boolean;

    .line 19273
    iput-object p9, p0, LX/078;->i:Ljava/lang/String;

    .line 19274
    iput-object p10, p0, LX/078;->j:Ljava/lang/String;

    .line 19275
    iput-object p11, p0, LX/078;->k:Ljava/lang/Boolean;

    .line 19276
    iput-object p12, p0, LX/078;->l:Ljava/lang/Long;

    .line 19277
    iput p13, p0, LX/078;->m:I

    .line 19278
    iput-object p14, p0, LX/078;->n:Ljava/lang/String;

    .line 19279
    move-object/from16 v0, p15

    iput-object v0, p0, LX/078;->o:Ljava/lang/String;

    .line 19280
    move-object/from16 v0, p16

    iput-object v0, p0, LX/078;->p:Ljava/util/List;

    .line 19281
    move-object/from16 v0, p17

    iput-object v0, p0, LX/078;->q:Ljava/lang/String;

    .line 19282
    move-object/from16 v0, p18

    iput-object v0, p0, LX/078;->r:Ljava/lang/String;

    .line 19283
    move-object/from16 v0, p19

    iput-object v0, p0, LX/078;->s:Ljava/lang/Long;

    .line 19284
    move-object/from16 v0, p20

    iput-object v0, p0, LX/078;->t:Ljava/lang/String;

    .line 19285
    move-object/from16 v0, p21

    iput-object v0, p0, LX/078;->u:Ljava/lang/String;

    .line 19286
    move-object/from16 v0, p22

    iput-object v0, p0, LX/078;->v:Ljava/lang/String;

    .line 19287
    move-object/from16 v0, p23

    iput-object v0, p0, LX/078;->w:Ljava/lang/Byte;

    .line 19288
    move-object/from16 v0, p24

    iput-object v0, p0, LX/078;->x:Ljava/util/Map;

    .line 19289
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/078;
    .locals 28

    .prologue
    .line 19246
    :try_start_0
    new-instance v25, Lorg/json/JSONObject;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 19247
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 19248
    sget-object v2, LX/0Hv;->SUBSCRIBE_TOPICS:LX/0Hv;

    invoke-virtual {v2}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 19249
    const/4 v2, 0x0

    :goto_0
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 19250
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19251
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 19252
    :cond_0
    new-instance v26, Ljava/util/HashMap;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashMap;-><init>()V

    .line 19253
    sget-object v2, LX/0Hv;->APP_SPECIFIC_INFO:LX/0Hv;

    invoke-virtual {v2}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 19254
    if-eqz v3, :cond_1

    .line 19255
    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 19256
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 19257
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 19258
    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 19259
    move-object/from16 v0, v26

    invoke-interface {v0, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 19260
    :catch_0
    move-exception v2

    .line 19261
    sget-object v3, LX/078;->y:Ljava/lang/String;

    const-string v4, "Failed to deserialize ConnectPayloadUserName"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19262
    const/4 v2, 0x0

    :goto_2
    return-object v2

    .line 19263
    :cond_1
    :try_start_1
    new-instance v2, LX/078;

    sget-object v3, LX/0Hv;->USER_ID:LX/0Hv;

    move-object/from16 v0, v25

    invoke-static {v0, v3}, LX/078;->a(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Long;

    move-result-object v3

    sget-object v4, LX/0Hv;->AGENT:LX/0Hv;

    move-object/from16 v0, v25

    invoke-static {v0, v4}, LX/078;->b(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/0Hv;->CAPABILITIES:LX/0Hv;

    move-object/from16 v0, v25

    invoke-static {v0, v5}, LX/078;->a(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Long;

    move-result-object v5

    sget-object v6, LX/0Hv;->CLIENT_MQTT_SESSION_ID:LX/0Hv;

    move-object/from16 v0, v25

    invoke-static {v0, v6}, LX/078;->a(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Long;

    move-result-object v6

    sget-object v7, LX/0Hv;->NETWORK_TYPE:LX/0Hv;

    invoke-virtual {v7}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v7

    const/4 v8, -0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    sget-object v8, LX/0Hv;->NETWORK_SUBTYPE:LX/0Hv;

    move-object/from16 v0, v25

    invoke-static {v0, v8}, LX/078;->d(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Integer;

    move-result-object v8

    sget-object v9, LX/0Hv;->MAKE_USER_AVAILABLE_IN_FOREGROUND:LX/0Hv;

    move-object/from16 v0, v25

    invoke-static {v0, v9}, LX/078;->c(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Boolean;

    move-result-object v9

    sget-object v10, LX/0Hv;->NO_AUTOMATIC_FOREGROUND:LX/0Hv;

    move-object/from16 v0, v25

    invoke-static {v0, v10}, LX/078;->c(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Boolean;

    move-result-object v10

    sget-object v11, LX/0Hv;->DEVICE_ID:LX/0Hv;

    move-object/from16 v0, v25

    invoke-static {v0, v11}, LX/078;->b(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/String;

    move-result-object v11

    sget-object v12, LX/0Hv;->DEVICE_SECRET:LX/0Hv;

    move-object/from16 v0, v25

    invoke-static {v0, v12}, LX/078;->b(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/String;

    move-result-object v12

    sget-object v13, LX/0Hv;->INITIAL_FOREGROUND_STATE:LX/0Hv;

    move-object/from16 v0, v25

    invoke-static {v0, v13}, LX/078;->c(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Boolean;

    move-result-object v13

    sget-object v14, LX/0Hv;->ENDPOINT_CAPABILITIES:LX/0Hv;

    move-object/from16 v0, v25

    invoke-static {v0, v14}, LX/078;->a(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Long;

    move-result-object v14

    sget-object v15, LX/0Hv;->PUBLISH_FORMAT:LX/0Hv;

    move-object/from16 v0, v25

    invoke-static {v0, v15}, LX/078;->b(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, LX/0Hw;->a(Ljava/lang/String;)I

    move-result v15

    sget-object v16, LX/0Hv;->CLIENT_TYPE:LX/0Hv;

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/078;->b(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/String;

    move-result-object v16

    sget-object v17, LX/0Hv;->APP_ID:LX/0Hv;

    move-object/from16 v0, v25

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/078;->b(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/String;

    move-result-object v17

    sget-object v19, LX/0Hv;->CONNECT_HASH:LX/0Hv;

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/078;->b(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/String;

    move-result-object v19

    sget-object v20, LX/0Hv;->DATACENTER_PREFERENCE:LX/0Hv;

    move-object/from16 v0, v25

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/078;->b(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/String;

    move-result-object v20

    sget-object v21, LX/0Hv;->FBNS_CONNECTION_KEY:LX/0Hv;

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/078;->a(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Long;

    move-result-object v21

    sget-object v22, LX/0Hv;->FBNS_CONNECTION_SECRET:LX/0Hv;

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/078;->b(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/String;

    move-result-object v22

    sget-object v23, LX/0Hv;->FBNS_DEVICE_ID:LX/0Hv;

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/078;->b(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/String;

    move-result-object v23

    sget-object v24, LX/0Hv;->FBNS_DEVICE_SECRET:LX/0Hv;

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/078;->b(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/String;

    move-result-object v24

    sget-object v27, LX/0Hv;->CLIENT_STACK:LX/0Hv;

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/078;->e(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Byte;

    move-result-object v25

    invoke-direct/range {v2 .. v26}, LX/078;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Byte;Ljava/util/Map;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method

.method private static a(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 19243
    invoke-virtual {p1}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19244
    const/4 v0, 0x0

    .line 19245
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 19291
    invoke-virtual {p1}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19292
    const/4 v0, 0x0

    .line 19293
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static c(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 19240
    invoke-virtual {p1}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19241
    const/4 v0, 0x0

    .line 19242
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method private static d(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 19237
    invoke-virtual {p1}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19238
    const/4 v0, 0x0

    .line 19239
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method private static e(Lorg/json/JSONObject;LX/0Hv;)Ljava/lang/Byte;
    .locals 1

    .prologue
    .line 19235
    :try_start_0
    invoke-virtual {p1}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 19236
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 19189
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 19190
    sget-object v0, LX/0Hv;->USER_ID:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->a:Ljava/lang/Long;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19191
    sget-object v0, LX/0Hv;->AGENT:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19192
    sget-object v0, LX/0Hv;->CAPABILITIES:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->c:Ljava/lang/Long;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19193
    sget-object v0, LX/0Hv;->CLIENT_MQTT_SESSION_ID:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->d:Ljava/lang/Long;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19194
    sget-object v0, LX/0Hv;->NETWORK_TYPE:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->e:Ljava/lang/Integer;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19195
    sget-object v0, LX/0Hv;->NETWORK_SUBTYPE:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->f:Ljava/lang/Integer;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19196
    sget-object v0, LX/0Hv;->MAKE_USER_AVAILABLE_IN_FOREGROUND:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->g:Ljava/lang/Boolean;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19197
    sget-object v0, LX/0Hv;->NO_AUTOMATIC_FOREGROUND:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->h:Ljava/lang/Boolean;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19198
    sget-object v0, LX/0Hv;->DEVICE_ID:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->i:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19199
    sget-object v0, LX/0Hv;->DEVICE_SECRET:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->j:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19200
    sget-object v0, LX/0Hv;->INITIAL_FOREGROUND_STATE:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->k:Ljava/lang/Boolean;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19201
    sget-object v0, LX/0Hv;->ENDPOINT_CAPABILITIES:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->l:Ljava/lang/Long;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19202
    sget-object v0, LX/0Hv;->PUBLISH_FORMAT:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, LX/078;->m:I

    .line 19203
    const/4 v3, 0x1

    if-ne v3, v1, :cond_4

    .line 19204
    const-string v3, "jz"

    .line 19205
    :goto_0
    move-object v1, v3

    .line 19206
    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19207
    sget-object v0, LX/0Hv;->CLIENT_TYPE:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->n:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19208
    sget-object v0, LX/0Hv;->APP_ID:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->o:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19209
    iget-object v0, p0, LX/078;->p:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 19210
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 19211
    iget-object v0, p0, LX/078;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 19212
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 19213
    :catch_0
    move-exception v0

    .line 19214
    sget-object v1, LX/078;->y:Ljava/lang/String;

    const-string v2, "Failed to serialize ConnectPayloadUserName"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19215
    const/4 v0, 0x0

    :goto_2
    return-object v0

    .line 19216
    :cond_0
    :try_start_1
    sget-object v0, LX/0Hv;->SUBSCRIBE_TOPICS:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19217
    :cond_1
    sget-object v0, LX/0Hv;->CONNECT_HASH:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->q:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19218
    sget-object v0, LX/0Hv;->DATACENTER_PREFERENCE:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->r:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19219
    sget-object v0, LX/0Hv;->FBNS_CONNECTION_KEY:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->s:Ljava/lang/Long;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19220
    sget-object v0, LX/0Hv;->FBNS_CONNECTION_SECRET:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->t:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19221
    sget-object v0, LX/0Hv;->FBNS_DEVICE_ID:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->u:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19222
    sget-object v0, LX/0Hv;->FBNS_DEVICE_SECRET:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->v:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19223
    sget-object v0, LX/0Hv;->CLIENT_STACK:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/078;->w:Ljava/lang/Byte;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19224
    iget-object v0, p0, LX/078;->x:Ljava/util/Map;

    if-eqz v0, :cond_3

    .line 19225
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 19226
    iget-object v0, p0, LX/078;->x:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 19227
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_3

    .line 19228
    :cond_2
    sget-object v0, LX/0Hv;->APP_SPECIFIC_INFO:LX/0Hv;

    invoke-virtual {v0}, LX/0Hv;->getJsonKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19229
    :cond_3
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto/16 :goto_2

    .line 19230
    :cond_4
    const/4 v3, 0x2

    if-ne v3, v1, :cond_5

    .line 19231
    const-string v3, "jzo"

    goto/16 :goto_0

    .line 19232
    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19233
    invoke-virtual {p0}, LX/078;->a()Ljava/lang/String;

    move-result-object v0

    .line 19234
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
