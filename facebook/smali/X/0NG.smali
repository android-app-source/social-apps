.class public final LX/0NG;
.super LX/0N4;
.source ""


# instance fields
.field public final b:LX/0Oj;

.field public final c:LX/0Oe;

.field public d:I

.field public e:I

.field public f:Z

.field public g:Z

.field public h:J

.field public i:I

.field public j:J


# direct methods
.method public constructor <init>(LX/0LS;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49720
    invoke-direct {p0, p1}, LX/0N4;-><init>(LX/0LS;)V

    .line 49721
    iput v2, p0, LX/0NG;->d:I

    .line 49722
    new-instance v0, LX/0Oj;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, LX/0NG;->b:LX/0Oj;

    .line 49723
    iget-object v0, p0, LX/0NG;->b:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    const/4 v1, -0x1

    aput-byte v1, v0, v2

    .line 49724
    new-instance v0, LX/0Oe;

    invoke-direct {v0}, LX/0Oe;-><init>()V

    iput-object v0, p0, LX/0NG;->c:LX/0Oe;

    .line 49725
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49726
    iput v0, p0, LX/0NG;->d:I

    .line 49727
    iput v0, p0, LX/0NG;->e:I

    .line 49728
    iput-boolean v0, p0, LX/0NG;->g:Z

    .line 49729
    return-void
.end method

.method public final a(JZ)V
    .locals 1

    .prologue
    .line 49730
    iput-wide p1, p0, LX/0NG;->j:J

    .line 49731
    return-void
.end method

.method public final a(LX/0Oj;)V
    .locals 14

    .prologue
    .line 49732
    :goto_0
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 49733
    iget v0, p0, LX/0NG;->d:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 49734
    :pswitch_0
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 49735
    iget-object v5, p1, LX/0Oj;->a:[B

    .line 49736
    iget v0, p1, LX/0Oj;->b:I

    move v0, v0

    .line 49737
    iget v3, p1, LX/0Oj;->c:I

    move v6, v3

    .line 49738
    move v4, v0

    .line 49739
    :goto_1
    if-ge v4, v6, :cond_4

    .line 49740
    aget-byte v0, v5, v4

    and-int/lit16 v0, v0, 0xff

    const/16 v3, 0xff

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 49741
    :goto_2
    iget-boolean v3, p0, LX/0NG;->g:Z

    if-eqz v3, :cond_2

    aget-byte v3, v5, v4

    and-int/lit16 v3, v3, 0xe0

    const/16 v7, 0xe0

    if-ne v3, v7, :cond_2

    move v3, v1

    .line 49742
    :goto_3
    iput-boolean v0, p0, LX/0NG;->g:Z

    .line 49743
    if-eqz v3, :cond_3

    .line 49744
    add-int/lit8 v0, v4, 0x1

    invoke-virtual {p1, v0}, LX/0Oj;->b(I)V

    .line 49745
    iput-boolean v2, p0, LX/0NG;->g:Z

    .line 49746
    iget-object v0, p0, LX/0NG;->b:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    aget-byte v2, v5, v4

    aput-byte v2, v0, v1

    .line 49747
    const/4 v0, 0x2

    iput v0, p0, LX/0NG;->e:I

    .line 49748
    iput v1, p0, LX/0NG;->d:I

    .line 49749
    :goto_4
    goto :goto_0

    .line 49750
    :pswitch_1
    const/4 v13, 0x4

    const/4 v12, 0x1

    const/4 v1, 0x0

    const/4 v11, 0x0

    .line 49751
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v2

    iget v3, p0, LX/0NG;->e:I

    rsub-int/lit8 v3, v3, 0x4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 49752
    iget-object v3, p0, LX/0NG;->b:LX/0Oj;

    iget-object v3, v3, LX/0Oj;->a:[B

    iget v4, p0, LX/0NG;->e:I

    invoke-virtual {p1, v3, v4, v2}, LX/0Oj;->a([BII)V

    .line 49753
    iget v3, p0, LX/0NG;->e:I

    add-int/2addr v2, v3

    iput v2, p0, LX/0NG;->e:I

    .line 49754
    iget v2, p0, LX/0NG;->e:I

    if-ge v2, v13, :cond_5

    .line 49755
    :goto_5
    goto :goto_0

    .line 49756
    :pswitch_2
    const/4 v7, 0x0

    .line 49757
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v1

    iget v2, p0, LX/0NG;->i:I

    iget v3, p0, LX/0NG;->e:I

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 49758
    iget-object v2, p0, LX/0N4;->a:LX/0LS;

    invoke-interface {v2, p1, v1}, LX/0LS;->a(LX/0Oj;I)V

    .line 49759
    iget v2, p0, LX/0NG;->e:I

    add-int/2addr v1, v2

    iput v1, p0, LX/0NG;->e:I

    .line 49760
    iget v1, p0, LX/0NG;->e:I

    iget v2, p0, LX/0NG;->i:I

    if-ge v1, v2, :cond_8

    .line 49761
    :goto_6
    goto/16 :goto_0

    .line 49762
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 49763
    goto :goto_2

    :cond_2
    move v3, v2

    .line 49764
    goto :goto_3

    .line 49765
    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 49766
    :cond_4
    invoke-virtual {p1, v6}, LX/0Oj;->b(I)V

    goto :goto_4

    .line 49767
    :cond_5
    iget-object v2, p0, LX/0NG;->b:LX/0Oj;

    invoke-virtual {v2, v11}, LX/0Oj;->b(I)V

    .line 49768
    iget-object v2, p0, LX/0NG;->b:LX/0Oj;

    invoke-virtual {v2}, LX/0Oj;->m()I

    move-result v2

    iget-object v3, p0, LX/0NG;->c:LX/0Oe;

    invoke-static {v2, v3}, LX/0Oe;->a(ILX/0Oe;)Z

    move-result v2

    .line 49769
    if-nez v2, :cond_6

    .line 49770
    iput v11, p0, LX/0NG;->e:I

    .line 49771
    iput v12, p0, LX/0NG;->d:I

    goto :goto_5

    .line 49772
    :cond_6
    iget-object v2, p0, LX/0NG;->c:LX/0Oe;

    iget v2, v2, LX/0Oe;->c:I

    iput v2, p0, LX/0NG;->i:I

    .line 49773
    iget-boolean v2, p0, LX/0NG;->f:Z

    if-nez v2, :cond_7

    .line 49774
    const-wide/32 v3, 0xf4240

    iget-object v2, p0, LX/0NG;->c:LX/0Oe;

    iget v2, v2, LX/0Oe;->g:I

    int-to-long v5, v2

    mul-long/2addr v3, v5

    iget-object v2, p0, LX/0NG;->c:LX/0Oe;

    iget v2, v2, LX/0Oe;->d:I

    int-to-long v5, v2

    div-long/2addr v3, v5

    iput-wide v3, p0, LX/0NG;->h:J

    .line 49775
    iget-object v2, p0, LX/0NG;->c:LX/0Oe;

    iget-object v2, v2, LX/0Oe;->b:Ljava/lang/String;

    const/4 v3, -0x1

    const/16 v4, 0x1000

    const-wide/16 v5, -0x1

    iget-object v7, p0, LX/0NG;->c:LX/0Oe;

    iget v7, v7, LX/0Oe;->e:I

    iget-object v8, p0, LX/0NG;->c:LX/0Oe;

    iget v8, v8, LX/0Oe;->d:I

    move-object v9, v1

    move-object v10, v1

    invoke-static/range {v1 .. v10}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v1

    .line 49776
    iget-object v2, p0, LX/0N4;->a:LX/0LS;

    invoke-interface {v2, v1}, LX/0LS;->a(LX/0L4;)V

    .line 49777
    iput-boolean v12, p0, LX/0NG;->f:Z

    .line 49778
    :cond_7
    iget-object v1, p0, LX/0NG;->b:LX/0Oj;

    invoke-virtual {v1, v11}, LX/0Oj;->b(I)V

    .line 49779
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    iget-object v2, p0, LX/0NG;->b:LX/0Oj;

    invoke-interface {v1, v2, v13}, LX/0LS;->a(LX/0Oj;I)V

    .line 49780
    const/4 v1, 0x2

    iput v1, p0, LX/0NG;->d:I

    goto/16 :goto_5

    .line 49781
    :cond_8
    iget-object v2, p0, LX/0N4;->a:LX/0LS;

    iget-wide v3, p0, LX/0NG;->j:J

    const/4 v5, 0x1

    iget v6, p0, LX/0NG;->i:I

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, LX/0LS;->a(JIII[B)V

    .line 49782
    iget-wide v1, p0, LX/0NG;->j:J

    iget-wide v3, p0, LX/0NG;->h:J

    add-long/2addr v1, v3

    iput-wide v1, p0, LX/0NG;->j:J

    .line 49783
    iput v7, p0, LX/0NG;->e:I

    .line 49784
    iput v7, p0, LX/0NG;->d:I

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 49785
    return-void
.end method
