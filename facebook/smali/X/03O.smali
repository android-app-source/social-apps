.class public final LX/03O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/00j;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    .line 10220
    invoke-static {}, LX/00m;->c()V

    .line 10221
    const-wide/16 v1, 0x1

    invoke-static {v1, v2}, LX/00k;->a(J)Z

    move-result v1

    if-nez v1, :cond_1

    .line 10222
    :goto_0
    const/4 v5, 0x0

    const-wide/16 v3, 0x40

    .line 10223
    invoke-static {v3, v4}, LX/00k;->a(J)Z

    move-result v1

    if-nez v1, :cond_2

    .line 10224
    :goto_1
    sget-object v0, LX/00m;->a:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 10225
    if-nez v0, :cond_0

    .line 10226
    :goto_3
    return-void

    .line 10227
    :cond_0
    invoke-static {}, LX/00m;->k()V

    goto :goto_3

    .line 10228
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x7f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 10229
    const-string v2, "Android trace tags: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10230
    const-string v6, "debug.atrace.tags.enableflags"

    const-wide/16 v8, 0x0

    invoke-static {v6, v8, v9}, LX/011;->a(Ljava/lang/String;J)J

    move-result-wide v6

    move-wide v3, v6

    .line 10231
    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 10232
    const-string v2, ", Facebook trace tags: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10233
    sget-wide v6, LX/00k;->b:J

    move-wide v3, v6

    .line 10234
    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 10235
    const-wide/16 v3, 0x40

    const-string v2, "process_labels"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    invoke-static {v3, v4, v2, v1, v5}, LX/018;->a(JLjava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 10236
    :cond_2
    const-string v1, "process_name"

    invoke-static {}, LX/00z;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v1, v2, v5}, LX/018;->a(JLjava/lang/String;Ljava/lang/String;I)V

    .line 10237
    const-string v1, "process_labels"

    .line 10238
    const-string v2, "dalvik.vm.heapgrowthlimit"

    invoke-static {v2}, LX/011;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 10239
    const-string v6, "dalvik.vm.heapmaxfree"

    invoke-static {v6}, LX/011;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 10240
    const-string v7, "dalvik.vm.heapminfree"

    invoke-static {v7}, LX/011;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 10241
    const-string v8, "dalvik.vm.heapstartsize"

    invoke-static {v8}, LX/011;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 10242
    const-string v9, "dalvik.vm.heaptargetutilization"

    invoke-static {v9}, LX/011;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 10243
    sget-object v10, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 10244
    const-string v11, "device=%s,heapgrowthlimit=%s,heapstartsize=%s,heapminfree=%s,heapmaxfree=%s,heaptargetutilization=%s"

    const/4 v12, 0x6

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v10, v12, v13

    const/4 v10, 0x1

    aput-object v2, v12, v10

    const/4 v2, 0x2

    aput-object v8, v12, v2

    const/4 v2, 0x3

    aput-object v6, v12, v2

    const/4 v2, 0x4

    aput-object v7, v12, v2

    const/4 v2, 0x5

    aput-object v9, v12, v2

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 10245
    invoke-static {v3, v4, v1, v2, v5}, LX/018;->a(JLjava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_1

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 10246
    return-void
.end method
