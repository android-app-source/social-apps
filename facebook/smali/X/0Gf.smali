.class public LX/0Gf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ge;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.util.Log.e",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.v"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field private d:LX/0Ge;

.field public final e:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:I

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36181
    const-class v0, LX/0Gf;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0Gf;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;LX/0Ge;I)V
    .locals 1
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 36182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36183
    iput-object p2, p0, LX/0Gf;->b:Ljava/lang/String;

    .line 36184
    iput-object p1, p0, LX/0Gf;->e:Landroid/net/Uri;

    .line 36185
    iput-object p3, p0, LX/0Gf;->d:LX/0Ge;

    .line 36186
    iput p4, p0, LX/0Gf;->f:I

    .line 36187
    const/4 v0, 0x0

    iput v0, p0, LX/0Gf;->g:I

    .line 36188
    return-void
.end method


# virtual methods
.method public final declared-synchronized a([BII)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 36172
    monitor-enter p0

    :try_start_0
    iget v1, p0, LX/0Gf;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 36173
    :goto_0
    monitor-exit p0

    return v0

    .line 36174
    :cond_0
    :try_start_1
    iget v1, p0, LX/0Gf;->g:I

    if-ne v1, v0, :cond_2

    .line 36175
    :goto_1
    iget-object v1, p0, LX/0Gf;->d:LX/0Ge;

    invoke-interface {v1, p1, p2, p3}, LX/0Ge;->a([BII)I

    move-result v1

    .line 36176
    if-eq v1, v0, :cond_1

    .line 36177
    iget v0, p0, LX/0Gf;->g:I

    sub-int/2addr v0, v1

    iput v0, p0, LX/0Gf;->g:I

    :cond_1
    move v0, v1

    .line 36178
    goto :goto_0

    .line 36179
    :cond_2
    iget v1, p0, LX/0Gf;->g:I

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result p3

    goto :goto_1

    .line 36180
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0OA;)J
    .locals 14

    .prologue
    const-wide/16 v12, -0x1

    .line 36145
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Gf;->e:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 36146
    new-instance v1, LX/0OA;

    .line 36147
    iget-object v0, p0, LX/0Gf;->e:Landroid/net/Uri;

    if-nez v0, :cond_4

    .line 36148
    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    .line 36149
    :goto_0
    move-object v2, v0

    .line 36150
    iget-object v3, p1, LX/0OA;->b:[B

    iget-wide v4, p1, LX/0OA;->c:J

    iget-wide v6, p1, LX/0OA;->d:J

    iget-wide v8, p1, LX/0OA;->e:J

    iget-object v10, p1, LX/0OA;->f:Ljava/lang/String;

    iget v11, p1, LX/0OA;->g:I

    invoke-direct/range {v1 .. v11}, LX/0OA;-><init>(Landroid/net/Uri;[BJJJLjava/lang/String;I)V

    move-object p1, v1

    .line 36151
    :cond_0
    iget-object v0, p0, LX/0Gf;->d:LX/0Ge;

    invoke-interface {v0, p1}, LX/0Ge;->a(LX/0OA;)J

    move-result-wide v2

    .line 36152
    invoke-virtual {p0}, LX/0Gf;->c()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, LX/0Gf;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Gj;->a(Ljava/util/Map;Ljava/lang/String;)J

    move-result-wide v0

    .line 36153
    const-wide/16 v4, 0x0

    iget-wide v6, p1, LX/0OA;->d:J

    sub-long/2addr v0, v6

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 36154
    cmp-long v4, v2, v12

    if-eqz v4, :cond_1

    cmp-long v4, v2, v0

    if-lez v4, :cond_2

    .line 36155
    :cond_1
    long-to-int v4, v0

    iput v4, p0, LX/0Gf;->g:I

    .line 36156
    :goto_1
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, p1, LX/0OA;->d:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-wide v8, p1, LX/0OA;->e:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    iget-object v8, p0, LX/0Gf;->b:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x4

    iget-object v8, p1, LX/0OA;->f:Ljava/lang/String;

    aput-object v8, v6, v7

    .line 36157
    iget-wide v4, p1, LX/0OA;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v4, v4, v12

    if-nez v4, :cond_3

    :goto_2
    monitor-exit p0

    return-wide v0

    .line 36158
    :cond_2
    long-to-int v4, v2

    :try_start_1
    iput v4, p0, LX/0Gf;->g:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 36159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 36160
    :cond_3
    :try_start_2
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v0

    goto :goto_2

    .line 36161
    :cond_4
    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v0, "127.0.0.1"

    iget-object v2, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 36162
    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 36163
    :goto_3
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 36164
    const-string v3, "disable-cache"

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "network-priority"

    iget v4, p0, LX/0Gf;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 36165
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 36166
    :cond_5
    iget-object v0, p0, LX/0Gf;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "remote-uri"

    iget-object v3, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "vid"

    iget-object v3, p0, LX/0Gf;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    goto :goto_3
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 36169
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Gf;->d:LX/0Ge;

    invoke-interface {v0}, LX/0Ge;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36170
    monitor-exit p0

    return-void

    .line 36171
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36168
    iget-object v0, p0, LX/0Gf;->d:LX/0Ge;

    invoke-interface {v0}, LX/0G7;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 36167
    iget-object v0, p0, LX/0Gf;->d:LX/0Ge;

    invoke-interface {v0}, LX/0Ge;->c()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
