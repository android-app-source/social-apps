.class public LX/0Ab;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ad;

.field private final b:LX/0Zb;

.field private final c:LX/0oz;

.field private final d:LX/0kb;

.field public final e:Ljava/lang/String;

.field public f:J

.field private g:J

.field public h:J


# direct methods
.method public constructor <init>(LX/0Ad;LX/0Zb;LX/0oz;LX/0kb;Ljava/lang/String;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 24833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24834
    iput-wide v0, p0, LX/0Ab;->f:J

    .line 24835
    iput-wide v0, p0, LX/0Ab;->g:J

    .line 24836
    iput-wide v0, p0, LX/0Ab;->h:J

    .line 24837
    iput-object p1, p0, LX/0Ab;->a:LX/0Ad;

    .line 24838
    iput-object p2, p0, LX/0Ab;->b:LX/0Zb;

    .line 24839
    iput-object p3, p0, LX/0Ab;->c:LX/0oz;

    .line 24840
    iput-object p4, p0, LX/0Ab;->d:LX/0kb;

    .line 24841
    iput-object p5, p0, LX/0Ab;->e:Ljava/lang/String;

    .line 24842
    return-void
.end method

.method public static a(LX/0Ab;Ljava/lang/String;JJ)V
    .locals 4

    .prologue
    .line 24843
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 24844
    const-string v1, "live_video"

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 24845
    const-string v1, "video_id"

    iget-object v2, p0, LX/0Ab;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 24846
    const-string v1, "frame_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 24847
    const-wide/16 v2, 0x0

    cmp-long v1, p4, v2

    if-eqz v1, :cond_0

    .line 24848
    const-string v1, "duration"

    invoke-virtual {v0, v1, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 24849
    :cond_0
    iget-object v1, p0, LX/0Ab;->d:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 24850
    if-eqz v1, :cond_1

    .line 24851
    const-string v2, "connection_type"

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 24852
    const-string v2, "connection_subtype"

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 24853
    :cond_1
    const-string v1, "bandwidth"

    iget-object v2, p0, LX/0Ab;->c:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->f()D

    move-result-wide v2

    double-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 24854
    const-string v1, "bandwidth_quality"

    iget-object v2, p0, LX/0Ab;->c:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    invoke-virtual {v2}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 24855
    const-string v1, "latency"

    iget-object v2, p0, LX/0Ab;->c:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->k()D

    move-result-wide v2

    double-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 24856
    const-string v1, "latency_quality"

    iget-object v2, p0, LX/0Ab;->c:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->e()LX/0p3;

    move-result-object v2

    invoke-virtual {v2}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 24857
    iget-object v1, p0, LX/0Ab;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 24858
    return-void
.end method


# virtual methods
.method public final a(JJJ)V
    .locals 7

    .prologue
    .line 24859
    iget-object v0, p0, LX/0Ab;->a:LX/0Ad;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/0Ad;->b(JJ)J

    move-result-wide v2

    .line 24860
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    .line 24861
    const-string v1, "live_video_frame_received"

    move-object v0, p0

    move-wide v4, p5

    invoke-static/range {v0 .. v5}, LX/0Ab;->a(LX/0Ab;Ljava/lang/String;JJ)V

    .line 24862
    :cond_0
    return-void
.end method

.method public final b(J)V
    .locals 7

    .prologue
    .line 24863
    iget-object v0, p0, LX/0Ab;->a:LX/0Ad;

    iget-wide v2, p0, LX/0Ab;->g:J

    invoke-virtual {v0, v2, v3, p1, p2}, LX/0Ad;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24864
    iput-wide p1, p0, LX/0Ab;->g:J

    .line 24865
    const-string v1, "live_video_frame_encoded"

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, LX/0Ab;->a(LX/0Ab;Ljava/lang/String;JJ)V

    .line 24866
    :cond_0
    return-void
.end method
