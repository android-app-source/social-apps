.class public LX/059;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/net/ConnectivityManager;

.field private final b:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field public final c:Landroid/content/Context;

.field public final d:Landroid/os/Handler;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/058;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Landroid/content/BroadcastReceiver;

.field private g:J

.field private h:J

.field private i:J

.field private j:J


# direct methods
.method public constructor <init>(Landroid/net/ConnectivityManager;Landroid/content/Context;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Landroid/os/Handler;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 15801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15802
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/059;->e:Ljava/util/Set;

    .line 15803
    iput-wide v2, p0, LX/059;->h:J

    .line 15804
    iput-wide v2, p0, LX/059;->i:J

    .line 15805
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/059;->j:J

    .line 15806
    iput-object p1, p0, LX/059;->a:Landroid/net/ConnectivityManager;

    .line 15807
    iput-object p2, p0, LX/059;->c:Landroid/content/Context;

    .line 15808
    iput-object p3, p0, LX/059;->b:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 15809
    iput-object p4, p0, LX/059;->d:Landroid/os/Handler;

    .line 15810
    new-instance v0, LX/05d;

    invoke-direct {v0, p0}, LX/05d;-><init>(LX/059;)V

    iput-object v0, p0, LX/059;->f:Landroid/content/BroadcastReceiver;

    .line 15811
    invoke-static {p0}, LX/059;->p(LX/059;)Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-static {p0, v0}, LX/059;->a$redex0(LX/059;Landroid/net/NetworkInfo;)V

    .line 15812
    iget-object v0, p0, LX/059;->c:Landroid/content/Context;

    iget-object v1, p0, LX/059;->f:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    iget-object p1, p0, LX/059;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 15813
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/059;Landroid/net/NetworkInfo;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const-wide/16 v2, 0x0

    .line 15736
    monitor-enter p0

    if-eqz p1, :cond_1

    :try_start_0
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15737
    iget-wide v0, p0, LX/059;->g:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 15738
    iget-object v0, p0, LX/059;->b:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/059;->g:J

    .line 15739
    iget-wide v0, p0, LX/059;->h:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 15740
    iget-wide v0, p0, LX/059;->g:J

    iget-wide v2, p0, LX/059;->h:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/059;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15741
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 15742
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/059;->b:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/059;->h:J

    .line 15743
    iget-wide v0, p0, LX/059;->g:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 15744
    iget-wide v0, p0, LX/059;->j:J

    iget-wide v2, p0, LX/059;->h:J

    iget-wide v4, p0, LX/059;->g:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/059;->j:J

    .line 15745
    :cond_2
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/059;->i:J

    .line 15746
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/059;->g:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 15747
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized n(LX/059;)V
    .locals 8

    .prologue
    .line 15788
    monitor-enter p0

    .line 15789
    :try_start_0
    invoke-static {p0}, LX/059;->p(LX/059;)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 15790
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 15791
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    .line 15792
    :goto_0
    move v0, v0

    .line 15793
    const-string v1, "MqttNetworkManager"

    const-string v2, "Connectivity changed: networkType=%d, networkCategory=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, LX/059;->b()LX/0HW;

    move-result-object v5

    invoke-virtual {v5}, LX/0HW;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15794
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 15795
    const-string v2, "com.facebook.mqtt.EXTRA_NETWORK_TYPE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 15796
    iget-object v0, p0, LX/059;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/058;

    .line 15797
    const-string v3, "MqttNetworkManager"

    const-string v4, "notify %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15798
    invoke-interface {v0, v1}, LX/058;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 15799
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 15800
    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static p(LX/059;)Landroid/net/NetworkInfo;
    .locals 1

    .prologue
    .line 15786
    :try_start_0
    iget-object v0, p0, LX/059;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 15787
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 15781
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/059;->c:Landroid/content/Context;

    iget-object v1, p0, LX/059;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15782
    :goto_0
    monitor-exit p0

    return-void

    .line 15783
    :catch_0
    move-exception v0

    .line 15784
    :try_start_1
    const-string v1, "MqttNetworkManager"

    const-string v2, "Failed to unregister broadcast receiver"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 15785
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/058;)V
    .locals 1

    .prologue
    .line 15778
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/059;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15779
    monitor-exit p0

    return-void

    .line 15780
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()LX/0HW;
    .locals 2

    .prologue
    .line 15774
    invoke-static {p0}, LX/059;->p(LX/059;)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 15775
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 15776
    :cond_0
    sget-object v0, LX/0HW;->NoNetwork:LX/0HW;

    .line 15777
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v0}, LX/0HX;->a(Landroid/net/NetworkInfo;)LX/0HW;

    move-result-object v0

    goto :goto_0
.end method

.method public final declared-synchronized b(LX/058;)V
    .locals 1

    .prologue
    .line 15771
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/059;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15772
    monitor-exit p0

    return-void

    .line 15773
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 15814
    invoke-static {p0}, LX/059;->p(LX/059;)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 15815
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Landroid/net/NetworkInfo;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 15767
    invoke-static {p0}, LX/059;->p(LX/059;)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 15768
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 15769
    :cond_0
    const/4 v0, 0x0

    .line 15770
    :cond_1
    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 15762
    const-string v0, "none"

    .line 15763
    invoke-virtual {p0}, LX/059;->d()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 15764
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 15765
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    .line 15766
    :cond_0
    return-object v0
.end method

.method public final g()J
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 15752
    invoke-virtual {p0}, LX/059;->d()Landroid/net/NetworkInfo;

    move-result-object v4

    .line 15753
    sget-object v0, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    .line 15754
    if-eqz v4, :cond_0

    .line 15755
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    .line 15756
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v1

    .line 15757
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    .line 15758
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v5

    .line 15759
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v6

    .line 15760
    const-string v7, "MqttNetworkManager"

    const-string v8, "typeName=%s, subtypeName=%s, networkInfo State=%s."

    new-array v9, v12, [Ljava/lang/Object;

    aput-object v5, v9, v2

    aput-object v6, v9, v10

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v4

    aput-object v4, v9, v11

    invoke-static {v7, v8, v9}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15761
    :goto_0
    new-array v4, v12, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v10

    aput-object v0, v4, v11

    invoke-static {v4}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    :cond_0
    move v1, v2

    move v3, v2

    goto :goto_0
.end method

.method public final declared-synchronized h()J
    .locals 2

    .prologue
    .line 15751
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/059;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()J
    .locals 2

    .prologue
    .line 15750
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/059;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j()J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 15749
    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, LX/059;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/059;->b:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/059;->g:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-long/2addr v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()J
    .locals 4

    .prologue
    .line 15748
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/059;->j:J

    invoke-virtual {p0}, LX/059;->j()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    add-long/2addr v0, v2

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
