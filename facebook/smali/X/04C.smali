.class public final enum LX/04C;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/04C;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/04C;

.field public static final enum VIDEO_CHAINING_IMPRESSION:LX/04C;

.field public static final enum VIDEO_CHAINING_REQUEST_RESULT:LX/04C;

.field public static final enum VIDEO_CHAINING_TAP:LX/04C;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12725
    new-instance v0, LX/04C;

    const-string v1, "VIDEO_CHAINING_TAP"

    const-string v2, "video_chaining_tap"

    invoke-direct {v0, v1, v3, v2}, LX/04C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04C;->VIDEO_CHAINING_TAP:LX/04C;

    .line 12726
    new-instance v0, LX/04C;

    const-string v1, "VIDEO_CHAINING_IMPRESSION"

    const-string v2, "video_chaining_impression"

    invoke-direct {v0, v1, v4, v2}, LX/04C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04C;->VIDEO_CHAINING_IMPRESSION:LX/04C;

    .line 12727
    new-instance v0, LX/04C;

    const-string v1, "VIDEO_CHAINING_REQUEST_RESULT"

    const-string v2, "video_chaining_request_result"

    invoke-direct {v0, v1, v5, v2}, LX/04C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04C;->VIDEO_CHAINING_REQUEST_RESULT:LX/04C;

    .line 12728
    const/4 v0, 0x3

    new-array v0, v0, [LX/04C;

    sget-object v1, LX/04C;->VIDEO_CHAINING_TAP:LX/04C;

    aput-object v1, v0, v3

    sget-object v1, LX/04C;->VIDEO_CHAINING_IMPRESSION:LX/04C;

    aput-object v1, v0, v4

    sget-object v1, LX/04C;->VIDEO_CHAINING_REQUEST_RESULT:LX/04C;

    aput-object v1, v0, v5

    sput-object v0, LX/04C;->$VALUES:[LX/04C;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12729
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12730
    iput-object p3, p0, LX/04C;->value:Ljava/lang/String;

    .line 12731
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/04C;
    .locals 1

    .prologue
    .line 12732
    const-class v0, LX/04C;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/04C;

    return-object v0
.end method

.method public static values()[LX/04C;
    .locals 1

    .prologue
    .line 12733
    sget-object v0, LX/04C;->$VALUES:[LX/04C;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/04C;

    return-object v0
.end method
