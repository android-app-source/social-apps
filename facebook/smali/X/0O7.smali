.class public final LX/0O7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0G7;


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private final b:LX/04n;

.field private c:Ljava/io/InputStream;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/04n;)V
    .locals 1

    .prologue
    .line 52454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52455
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, LX/0O7;->a:Landroid/content/ContentResolver;

    .line 52456
    iput-object p2, p0, LX/0O7;->b:LX/04n;

    .line 52457
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 52425
    iget-wide v0, p0, LX/0O7;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 52426
    const/4 v0, -0x1

    .line 52427
    :cond_0
    :goto_0
    return v0

    .line 52428
    :cond_1
    :try_start_0
    iget-wide v0, p0, LX/0O7;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    .line 52429
    :goto_1
    iget-object v0, p0, LX/0O7;->c:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 52430
    if-lez v0, :cond_0

    .line 52431
    iget-wide v2, p0, LX/0O7;->e:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 52432
    iget-wide v2, p0, LX/0O7;->e:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/0O7;->e:J

    .line 52433
    :cond_2
    iget-object v1, p0, LX/0O7;->b:LX/04n;

    if-eqz v1, :cond_0

    .line 52434
    iget-object v1, p0, LX/0O7;->b:LX/04n;

    invoke-interface {v1, v0}, LX/04n;->a(I)V

    goto :goto_0

    .line 52435
    :cond_3
    :try_start_1
    iget-wide v0, p0, LX/0O7;->e:J

    int-to-long v2, p3

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v0

    long-to-int p3, v0

    goto :goto_1

    .line 52436
    :catch_0
    move-exception v0

    .line 52437
    new-instance v1, LX/0O6;

    invoke-direct {v1, v0}, LX/0O6;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public final a(LX/0OA;)J
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 52458
    :try_start_0
    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0O7;->d:Ljava/lang/String;

    .line 52459
    iget-object v0, p0, LX/0O7;->a:Landroid/content/ContentResolver;

    iget-object v1, p1, LX/0OA;->a:Landroid/net/Uri;

    const-string v2, "r"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 52460
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    iput-object v1, p0, LX/0O7;->c:Ljava/io/InputStream;

    .line 52461
    iget-object v0, p0, LX/0O7;->c:Ljava/io/InputStream;

    iget-wide v2, p1, LX/0OA;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 52462
    iget-wide v2, p1, LX/0OA;->d:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 52463
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52464
    :catch_0
    move-exception v0

    .line 52465
    new-instance v1, LX/0O6;

    invoke-direct {v1, v0}, LX/0O6;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 52466
    :cond_0
    :try_start_1
    iget-wide v0, p1, LX/0OA;->e:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 52467
    iget-wide v0, p1, LX/0OA;->e:J

    iput-wide v0, p0, LX/0O7;->e:J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 52468
    :cond_1
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0O7;->f:Z

    .line 52469
    iget-object v0, p0, LX/0O7;->b:LX/04n;

    if-eqz v0, :cond_2

    .line 52470
    iget-object v0, p0, LX/0O7;->b:LX/04n;

    invoke-interface {v0}, LX/04n;->b()V

    .line 52471
    :cond_2
    iget-wide v0, p0, LX/0O7;->e:J

    return-wide v0

    .line 52472
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/0O7;->c:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/0O7;->e:J

    .line 52473
    iget-wide v0, p0, LX/0O7;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 52474
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0O7;->e:J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 52438
    iput-object v2, p0, LX/0O7;->d:Ljava/lang/String;

    .line 52439
    iget-object v0, p0, LX/0O7;->c:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 52440
    :try_start_0
    iget-object v0, p0, LX/0O7;->c:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52441
    iput-object v2, p0, LX/0O7;->c:Ljava/io/InputStream;

    .line 52442
    iget-boolean v0, p0, LX/0O7;->f:Z

    if-eqz v0, :cond_0

    .line 52443
    iput-boolean v3, p0, LX/0O7;->f:Z

    .line 52444
    iget-object v0, p0, LX/0O7;->b:LX/04n;

    if-eqz v0, :cond_0

    .line 52445
    iget-object v0, p0, LX/0O7;->b:LX/04n;

    invoke-interface {v0}, LX/04n;->c()V

    .line 52446
    :cond_0
    return-void

    .line 52447
    :catch_0
    move-exception v0

    .line 52448
    :try_start_1
    new-instance v1, LX/0O6;

    invoke-direct {v1, v0}, LX/0O6;-><init>(Ljava/io/IOException;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52449
    :catchall_0
    move-exception v0

    iput-object v2, p0, LX/0O7;->c:Ljava/io/InputStream;

    .line 52450
    iget-boolean v1, p0, LX/0O7;->f:Z

    if-eqz v1, :cond_1

    .line 52451
    iput-boolean v3, p0, LX/0O7;->f:Z

    .line 52452
    iget-object v1, p0, LX/0O7;->b:LX/04n;

    if-eqz v1, :cond_1

    .line 52453
    iget-object v1, p0, LX/0O7;->b:LX/04n;

    invoke-interface {v1}, LX/04n;->c()V

    :cond_1
    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52424
    iget-object v0, p0, LX/0O7;->d:Ljava/lang/String;

    return-object v0
.end method
