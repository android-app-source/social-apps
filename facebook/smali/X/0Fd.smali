.class public final LX/0Fd;
.super LX/084;
.source ""


# instance fields
.field private final mDexStore:LX/02U;

.field private final mFlags:I

.field private final mTmpDir:LX/087;


# direct methods
.method public constructor <init>(LX/02U;I)V
    .locals 1

    .prologue
    .line 33899
    invoke-direct {p0}, LX/084;-><init>()V

    .line 33900
    iput-object p1, p0, LX/0Fd;->mDexStore:LX/02U;

    .line 33901
    iput p2, p0, LX/0Fd;->mFlags:I

    .line 33902
    const-string v0, "boring-compiler"

    invoke-virtual {p1, v0}, LX/02U;->makeTemporaryDirectory(Ljava/lang/String;)LX/087;

    move-result-object v0

    iput-object v0, p0, LX/0Fd;->mTmpDir:LX/087;

    .line 33903
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 33904
    iget-object v0, p0, LX/0Fd;->mTmpDir:LX/087;

    invoke-virtual {v0}, LX/087;->close()V

    .line 33905
    return-void
.end method

.method public final compile(LX/08A;)V
    .locals 7

    .prologue
    .line 33906
    iget-object v0, p1, LX/08A;->dex:LX/02Z;

    invoke-static {v0}, LX/0Fe;->makeDexName(LX/02Z;)Ljava/lang/String;

    move-result-object v0

    .line 33907
    invoke-static {v0}, LX/0Fe;->makeOdexName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 33908
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, LX/0Fd;->mDexStore:LX/02U;

    iget-object v3, v3, LX/02U;->root:Ljava/io/File;

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33909
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, LX/0Fd;->mDexStore:LX/02U;

    iget-object v4, v4, LX/02U;->root:Ljava/io/File;

    invoke-direct {v3, v4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33910
    iget v4, p0, LX/0Fd;->mFlags:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 33911
    :goto_0
    return-void

    .line 33912
    :cond_0
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, LX/0Fd;->mTmpDir:LX/087;

    iget-object v5, v5, LX/087;->directory:Ljava/io/File;

    invoke-direct {v4, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33913
    new-instance v0, Ljava/io/File;

    iget-object v5, p0, LX/0Fd;->mTmpDir:LX/087;

    iget-object v5, v5, LX/087;->directory:Ljava/io/File;

    invoke-direct {v0, v5, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33914
    invoke-virtual {p1, v4}, LX/08A;->extract(Ljava/io/File;)V

    .line 33915
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v1, v5, v6}, Ldalvik/system/DexFile;->loadDex(Ljava/lang/String;Ljava/lang/String;I)Ldalvik/system/DexFile;

    .line 33916
    invoke-static {v4, v2}, LX/02Q;->renameOrThrow(Ljava/io/File;Ljava/io/File;)V

    .line 33917
    invoke-static {v0, v3}, LX/02Q;->renameOrThrow(Ljava/io/File;Ljava/io/File;)V

    goto :goto_0
.end method
