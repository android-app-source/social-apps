.class public LX/0MC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0LS;


# instance fields
.field public final a:LX/0MP;

.field public final b:LX/0L7;

.field private c:Z

.field public d:J

.field public e:J

.field public volatile f:J

.field public volatile g:LX/0L4;


# direct methods
.method public constructor <init>(LX/0O1;)V
    .locals 4

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 45376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45377
    new-instance v0, LX/0MP;

    invoke-direct {v0, p1}, LX/0MP;-><init>(LX/0O1;)V

    iput-object v0, p0, LX/0MC;->a:LX/0MP;

    .line 45378
    new-instance v0, LX/0L7;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/0L7;-><init>(I)V

    iput-object v0, p0, LX/0MC;->b:LX/0L7;

    .line 45379
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0MC;->c:Z

    .line 45380
    iput-wide v2, p0, LX/0MC;->d:J

    .line 45381
    iput-wide v2, p0, LX/0MC;->e:J

    .line 45382
    iput-wide v2, p0, LX/0MC;->f:J

    .line 45383
    return-void
.end method

.method private h()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 45367
    iget-object v1, p0, LX/0MC;->a:LX/0MP;

    iget-object v2, p0, LX/0MC;->b:LX/0L7;

    invoke-virtual {v1, v2}, LX/0MP;->a(LX/0L7;)Z

    move-result v1

    .line 45368
    iget-boolean v2, p0, LX/0MC;->c:Z

    if-eqz v2, :cond_0

    .line 45369
    :goto_0
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/0MC;->b:LX/0L7;

    invoke-virtual {v2}, LX/0L7;->c()Z

    move-result v2

    if-nez v2, :cond_0

    .line 45370
    iget-object v1, p0, LX/0MC;->a:LX/0MP;

    invoke-virtual {v1}, LX/0MP;->d()V

    .line 45371
    iget-object v1, p0, LX/0MC;->a:LX/0MP;

    iget-object v2, p0, LX/0MC;->b:LX/0L7;

    invoke-virtual {v1, v2}, LX/0MP;->a(LX/0L7;)Z

    move-result v1

    goto :goto_0

    .line 45372
    :cond_0
    if-nez v1, :cond_2

    .line 45373
    :cond_1
    :goto_1
    return v0

    .line 45374
    :cond_2
    iget-wide v2, p0, LX/0MC;->e:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/0MC;->b:LX/0L7;

    iget-wide v2, v1, LX/0L7;->e:J

    iget-wide v4, p0, LX/0MC;->e:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 45375
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0MA;IZ)I
    .locals 1

    .prologue
    .line 45366
    iget-object v0, p0, LX/0MC;->a:LX/0MP;

    invoke-virtual {v0, p1, p2, p3}, LX/0MP;->a(LX/0MA;IZ)I

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 8

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 45349
    iget-object v0, p0, LX/0MC;->a:LX/0MP;

    const-wide/16 v6, 0x0

    .line 45350
    iget-object v4, v0, LX/0MP;->c:LX/0MN;

    const/4 v5, 0x0

    .line 45351
    iput v5, v4, LX/0MN;->h:I

    .line 45352
    iput v5, v4, LX/0MN;->i:I

    .line 45353
    iput v5, v4, LX/0MN;->j:I

    .line 45354
    iput v5, v4, LX/0MN;->g:I

    .line 45355
    :goto_0
    iget-object v4, v0, LX/0MP;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingDeque;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 45356
    iget-object v5, v0, LX/0MP;->a:LX/0O1;

    iget-object v4, v0, LX/0MP;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingDeque;->remove()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0O0;

    invoke-interface {v5, v4}, LX/0O1;->a(LX/0O0;)V

    goto :goto_0

    .line 45357
    :cond_0
    iput-wide v6, v0, LX/0MP;->g:J

    .line 45358
    iput-wide v6, v0, LX/0MP;->h:J

    .line 45359
    const/4 v4, 0x0

    iput-object v4, v0, LX/0MP;->i:LX/0O0;

    .line 45360
    iget v4, v0, LX/0MP;->b:I

    iput v4, v0, LX/0MP;->j:I

    .line 45361
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0MC;->c:Z

    .line 45362
    iput-wide v2, p0, LX/0MC;->d:J

    .line 45363
    iput-wide v2, p0, LX/0MC;->e:J

    .line 45364
    iput-wide v2, p0, LX/0MC;->f:J

    .line 45365
    return-void
.end method

.method public final a(I)V
    .locals 9

    .prologue
    .line 45317
    iget-object v0, p0, LX/0MC;->a:LX/0MP;

    .line 45318
    iget-object v2, v0, LX/0MP;->c:LX/0MN;

    .line 45319
    invoke-virtual {v2}, LX/0MN;->b()I

    move-result v4

    sub-int v5, v4, p1

    .line 45320
    if-ltz v5, :cond_2

    iget v4, v2, LX/0MN;->g:I

    if-gt v5, v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, LX/0Av;->a(Z)V

    .line 45321
    if-nez v5, :cond_5

    .line 45322
    iget v4, v2, LX/0MN;->h:I

    if-nez v4, :cond_3

    .line 45323
    const-wide/16 v4, 0x0

    .line 45324
    :goto_1
    move-wide v2, v4

    .line 45325
    iput-wide v2, v0, LX/0MP;->h:J

    .line 45326
    iget-wide v2, v0, LX/0MP;->h:J

    .line 45327
    iget-wide v4, v0, LX/0MP;->g:J

    sub-long v4, v2, v4

    long-to-int v4, v4

    .line 45328
    iget v5, v0, LX/0MP;->b:I

    div-int v5, v4, v5

    .line 45329
    iget v6, v0, LX/0MP;->b:I

    rem-int v6, v4, v6

    .line 45330
    iget-object v4, v0, LX/0MP;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v4

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    .line 45331
    if-nez v6, :cond_7

    .line 45332
    add-int/lit8 v4, v4, 0x1

    move v5, v4

    .line 45333
    :goto_2
    const/4 v4, 0x0

    move v7, v4

    :goto_3
    if-ge v7, v5, :cond_0

    .line 45334
    iget-object v8, v0, LX/0MP;->a:LX/0O1;

    iget-object v4, v0, LX/0MP;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingDeque;->removeLast()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0O0;

    invoke-interface {v8, v4}, LX/0O1;->a(LX/0O0;)V

    .line 45335
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_3

    .line 45336
    :cond_0
    iget-object v4, v0, LX/0MP;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingDeque;->peekLast()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0O0;

    iput-object v4, v0, LX/0MP;->i:LX/0O0;

    .line 45337
    if-nez v6, :cond_6

    iget v4, v0, LX/0MP;->b:I

    :goto_4
    iput v4, v0, LX/0MP;->j:I

    .line 45338
    iget-object v0, p0, LX/0MC;->a:LX/0MP;

    iget-object v1, p0, LX/0MC;->b:LX/0L7;

    invoke-virtual {v0, v1}, LX/0MP;->a(LX/0L7;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0MC;->b:LX/0L7;

    iget-wide v0, v0, LX/0L7;->e:J

    :goto_5
    iput-wide v0, p0, LX/0MC;->f:J

    .line 45339
    return-void

    .line 45340
    :cond_1
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_5

    .line 45341
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 45342
    :cond_3
    iget v4, v2, LX/0MN;->j:I

    if-nez v4, :cond_4

    iget v4, v2, LX/0MN;->a:I

    :goto_6
    add-int/lit8 v4, v4, -0x1

    .line 45343
    iget-object v5, v2, LX/0MN;->b:[J

    aget-wide v6, v5, v4

    iget-object v5, v2, LX/0MN;->c:[I

    aget v4, v5, v4

    int-to-long v4, v4

    add-long/2addr v4, v6

    goto :goto_1

    .line 45344
    :cond_4
    iget v4, v2, LX/0MN;->j:I

    goto :goto_6

    .line 45345
    :cond_5
    iget v4, v2, LX/0MN;->g:I

    sub-int/2addr v4, v5

    iput v4, v2, LX/0MN;->g:I

    .line 45346
    iget v4, v2, LX/0MN;->j:I

    iget v6, v2, LX/0MN;->a:I

    add-int/2addr v4, v6

    sub-int/2addr v4, v5

    iget v5, v2, LX/0MN;->a:I

    rem-int/2addr v4, v5

    iput v4, v2, LX/0MN;->j:I

    .line 45347
    iget-object v4, v2, LX/0MN;->b:[J

    iget v5, v2, LX/0MN;->j:I

    aget-wide v4, v4, v5

    goto/16 :goto_1

    :cond_6
    move v4, v6

    .line 45348
    goto :goto_4

    :cond_7
    move v5, v4

    goto :goto_2
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 45274
    :goto_0
    iget-object v0, p0, LX/0MC;->a:LX/0MP;

    iget-object v1, p0, LX/0MC;->b:LX/0L7;

    invoke-virtual {v0, v1}, LX/0MP;->a(LX/0L7;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0MC;->b:LX/0L7;

    iget-wide v0, v0, LX/0L7;->e:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_0

    .line 45275
    iget-object v0, p0, LX/0MC;->a:LX/0MP;

    invoke-virtual {v0}, LX/0MP;->d()V

    .line 45276
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0MC;->c:Z

    goto :goto_0

    .line 45277
    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/0MC;->d:J

    .line 45278
    return-void
.end method

.method public a(JIII[B)V
    .locals 9

    .prologue
    .line 45314
    iget-wide v0, p0, LX/0MC;->f:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/0MC;->f:J

    .line 45315
    iget-object v0, p0, LX/0MC;->a:LX/0MP;

    iget-object v1, p0, LX/0MC;->a:LX/0MP;

    invoke-virtual {v1}, LX/0MP;->e()J

    move-result-wide v2

    int-to-long v4, p4

    sub-long/2addr v2, v4

    int-to-long v4, p5

    sub-long v4, v2, v4

    move-wide v1, p1

    move v3, p3

    move v6, p4

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, LX/0MP;->a(JIJI[B)V

    .line 45316
    return-void
.end method

.method public final a(LX/0L4;)V
    .locals 0

    .prologue
    .line 45312
    iput-object p1, p0, LX/0MC;->g:LX/0L4;

    .line 45313
    return-void
.end method

.method public final a(LX/0Oj;I)V
    .locals 1

    .prologue
    .line 45310
    iget-object v0, p0, LX/0MC;->a:LX/0MP;

    invoke-virtual {v0, p1, p2}, LX/0MP;->a(LX/0Oj;I)V

    .line 45311
    return-void
.end method

.method public final a(LX/0L7;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 45294
    invoke-direct {p0}, LX/0MC;->h()Z

    move-result v1

    .line 45295
    if-nez v1, :cond_0

    .line 45296
    :goto_0
    return v0

    .line 45297
    :cond_0
    iget-object v1, p0, LX/0MC;->a:LX/0MP;

    .line 45298
    iget-object v2, v1, LX/0MP;->c:LX/0MN;

    iget-object v3, v1, LX/0MP;->e:LX/0MO;

    invoke-virtual {v2, p1, v3}, LX/0MN;->a(LX/0L7;LX/0MO;)Z

    move-result v2

    .line 45299
    if-nez v2, :cond_1

    .line 45300
    :goto_1
    iput-boolean v0, p0, LX/0MC;->c:Z

    .line 45301
    iget-wide v0, p1, LX/0L7;->e:J

    iput-wide v0, p0, LX/0MC;->d:J

    .line 45302
    const/4 v0, 0x1

    goto :goto_0

    .line 45303
    :cond_1
    invoke-virtual {p1}, LX/0L7;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 45304
    iget-object v2, v1, LX/0MP;->e:LX/0MO;

    invoke-static {v1, p1, v2}, LX/0MP;->a(LX/0MP;LX/0L7;LX/0MO;)V

    .line 45305
    :cond_2
    iget v2, p1, LX/0L7;->c:I

    invoke-virtual {p1, v2}, LX/0L7;->a(I)V

    .line 45306
    iget-object v2, v1, LX/0MP;->e:LX/0MO;

    iget-wide v2, v2, LX/0MO;->a:J

    iget-object v4, p1, LX/0L7;->b:Ljava/nio/ByteBuffer;

    iget v5, p1, LX/0L7;->c:I

    invoke-static {v1, v2, v3, v4, v5}, LX/0MP;->a(LX/0MP;JLjava/nio/ByteBuffer;I)V

    .line 45307
    iget-object v2, v1, LX/0MP;->c:LX/0MN;

    invoke-virtual {v2}, LX/0MN;->d()J

    move-result-wide v2

    .line 45308
    invoke-static {v1, v2, v3}, LX/0MP;->c(LX/0MP;J)V

    .line 45309
    goto :goto_1
.end method

.method public final b(J)Z
    .locals 6

    .prologue
    .line 45286
    iget-object v0, p0, LX/0MC;->a:LX/0MP;

    .line 45287
    iget-object v1, v0, LX/0MP;->c:LX/0MN;

    invoke-virtual {v1, p1, p2}, LX/0MN;->a(J)J

    move-result-wide v1

    .line 45288
    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-nez v3, :cond_0

    .line 45289
    const/4 v1, 0x0

    .line 45290
    :goto_0
    move v0, v1

    .line 45291
    return v0

    .line 45292
    :cond_0
    invoke-static {v0, v1, v2}, LX/0MP;->c(LX/0MP;J)V

    .line 45293
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 45281
    iget-object v0, p0, LX/0MC;->a:LX/0MP;

    .line 45282
    iget-object p0, v0, LX/0MP;->c:LX/0MN;

    .line 45283
    iget v0, p0, LX/0MN;->h:I

    move p0, v0

    .line 45284
    move v0, p0

    .line 45285
    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 45280
    iget-object v0, p0, LX/0MC;->g:LX/0L4;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 45279
    invoke-direct {p0}, LX/0MC;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
