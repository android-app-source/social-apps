.class public final LX/0KV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0KU;


# instance fields
.field public final synthetic a:Lcom/facebook/video/vps/VideoPlayerService;

.field private b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;


# direct methods
.method public constructor <init>(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 0

    .prologue
    .line 41416
    iput-object p1, p0, LX/0KV;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41417
    iput-object p2, p0, LX/0KV;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 41418
    return-void
.end method


# virtual methods
.method public final a(LX/0LB;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 41410
    if-nez p1, :cond_0

    .line 41411
    :goto_0
    return-void

    .line 41412
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, LX/0LB;->a([J)[J

    move-result-object v0

    .line 41413
    iget-object v1, p0, LX/0KV;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, p0, LX/0KV;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    const-string v3, "DashLive seek range changed: startMs=%d, endMs=%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aget-wide v6, v0, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    aget-wide v6, v0, v9

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v9

    .line 41414
    invoke-static {v1, v2, v3, v4}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41415
    goto :goto_0
.end method
