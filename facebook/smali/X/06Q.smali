.class public LX/06Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/06R;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/06R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17872
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/06Q;->a:Ljava/util/Set;

    .line 17873
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 17874
    const/4 v0, 0x1

    .line 17875
    iget-object v1, p0, LX/06Q;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/06R;

    .line 17876
    invoke-interface {v0, p1}, LX/06R;->a(Ljava/util/Map;)Z

    move-result v0

    and-int/2addr v0, v1

    move v1, v0

    .line 17877
    goto :goto_0

    .line 17878
    :cond_0
    return v1
.end method
