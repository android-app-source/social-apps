.class public LX/0AR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:I

.field public final g:I

.field public final h:F

.field public final i:I

.field public final j:I

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIFIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 23905
    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, LX/0AR;-><init>(Ljava/lang/String;Ljava/lang/String;IIFIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 23906
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIFIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 23891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23892
    invoke-static {p1}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/0AR;->a:Ljava/lang/String;

    .line 23893
    iput-object p2, p0, LX/0AR;->b:Ljava/lang/String;

    .line 23894
    iput p3, p0, LX/0AR;->f:I

    .line 23895
    iput p4, p0, LX/0AR;->g:I

    .line 23896
    iput p5, p0, LX/0AR;->h:F

    .line 23897
    iput p6, p0, LX/0AR;->i:I

    .line 23898
    iput p7, p0, LX/0AR;->j:I

    .line 23899
    iput p8, p0, LX/0AR;->c:I

    .line 23900
    iput-object p9, p0, LX/0AR;->l:Ljava/lang/String;

    .line 23901
    iput-object p10, p0, LX/0AR;->k:Ljava/lang/String;

    .line 23902
    iput-object p11, p0, LX/0AR;->d:Ljava/lang/String;

    .line 23903
    iput-boolean p12, p0, LX/0AR;->e:Z

    .line 23904
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 23883
    if-ne p0, p1, :cond_0

    .line 23884
    const/4 v0, 0x1

    .line 23885
    :goto_0
    return v0

    .line 23886
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 23887
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 23888
    :cond_2
    check-cast p1, LX/0AR;

    .line 23889
    iget-object v0, p1, LX/0AR;->a:Ljava/lang/String;

    iget-object v1, p0, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 23890
    iget-object v0, p0, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
