.class public LX/0A1;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/09y;

.field private final c:LX/0Zb;

.field private final d:LX/0kb;

.field private final e:LX/0A2;

.field public final f:LX/0A4;

.field public final g:LX/0A4;

.field private h:J

.field private i:J

.field public j:J

.field public k:J

.field public l:J

.field private m:LX/19s;

.field private n:J

.field private o:J

.field private final p:LX/19l;

.field private final q:LX/0A3;

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23363
    const-class v0, LX/0A1;

    sput-object v0, LX/0A1;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/09y;LX/0Zb;LX/0kb;LX/0A2;LX/19s;LX/19l;)V
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 23345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23346
    iput-wide v0, p0, LX/0A1;->n:J

    .line 23347
    iput-wide v0, p0, LX/0A1;->o:J

    .line 23348
    new-instance v0, LX/0A3;

    invoke-direct {v0, p0}, LX/0A3;-><init>(LX/0A1;)V

    iput-object v0, p0, LX/0A1;->q:LX/0A3;

    .line 23349
    iput-object p1, p0, LX/0A1;->b:LX/09y;

    .line 23350
    iput-object p2, p0, LX/0A1;->c:LX/0Zb;

    .line 23351
    iput-object p3, p0, LX/0A1;->d:LX/0kb;

    .line 23352
    iput-object p4, p0, LX/0A1;->e:LX/0A2;

    .line 23353
    new-instance v0, LX/0A4;

    invoke-direct {v0}, LX/0A4;-><init>()V

    iput-object v0, p0, LX/0A1;->f:LX/0A4;

    .line 23354
    new-instance v0, LX/0A4;

    invoke-direct {v0}, LX/0A4;-><init>()V

    iput-object v0, p0, LX/0A1;->g:LX/0A4;

    .line 23355
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0A1;->r:Ljava/util/List;

    .line 23356
    iput-object p5, p0, LX/0A1;->m:LX/19s;

    .line 23357
    iput-object p6, p0, LX/0A1;->p:LX/19l;

    .line 23358
    iget-object v0, p0, LX/0A1;->p:LX/19l;

    iget-object v0, v0, LX/19l;->a:LX/16V;

    if-eqz v0, :cond_0

    .line 23359
    iget-object v0, p0, LX/0A1;->p:LX/19l;

    iget-object v0, v0, LX/19l;->a:LX/16V;

    const-class v1, LX/2WD;

    iget-object v2, p0, LX/0A1;->q:LX/0A3;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 23360
    iget-object v0, p0, LX/0A1;->p:LX/19l;

    iget-object v0, v0, LX/19l;->a:LX/16V;

    const-class v1, LX/2WE;

    new-instance v2, LX/0A5;

    invoke-direct {v2, p0}, LX/0A5;-><init>(LX/0A1;)V

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 23361
    :cond_0
    new-instance v0, LX/0A6;

    invoke-direct {v0, p0}, LX/0A6;-><init>(LX/0A1;)V

    invoke-virtual {p5, v0}, LX/19s;->a(LX/0A6;)V

    .line 23362
    return-void
.end method

.method public static synthetic a(LX/0A1;J)J
    .locals 3

    .prologue
    .line 23344
    iget-wide v0, p0, LX/0A1;->h:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/0A1;->h:J

    return-wide v0
.end method

.method private static a(LX/0A1;JJ)V
    .locals 3

    .prologue
    .line 23341
    iget-object v0, p0, LX/0A1;->b:LX/09y;

    const-string v1, "downloaded"

    invoke-virtual {v0, v1, p1, p2}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 23342
    iget-object v0, p0, LX/0A1;->b:LX/09y;

    const-string v1, "served"

    invoke-virtual {v0, v1, p3, p4}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 23343
    return-void
.end method

.method private static a(LX/0A1;JZ)V
    .locals 1

    .prologue
    .line 23339
    new-instance v0, Lcom/facebook/video/analytics/VideoPerformanceTracking$6;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/video/analytics/VideoPerformanceTracking$6;-><init>(LX/0A1;JZ)V

    invoke-static {p0, v0}, LX/0A1;->a(LX/0A1;Ljava/lang/Runnable;)V

    .line 23340
    return-void
.end method

.method private static declared-synchronized a(LX/0A1;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 23332
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0A1;->r:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 23333
    :goto_0
    if-nez v0, :cond_1

    .line 23334
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23335
    :goto_1
    monitor-exit p0

    return-void

    .line 23336
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 23337
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0A1;->r:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 23338
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Landroid/net/Uri;IZLX/2WF;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 23322
    const-string v0, "video_id"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23323
    const-string v0, "url"

    invoke-virtual {p0, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23324
    const-string v0, "session_id"

    invoke-virtual {p0, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23325
    const-string v0, "connection_type"

    invoke-virtual {p0, v0, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23326
    const-string v0, "is_partial"

    invoke-virtual {p0, v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23327
    const-string v2, "range_from"

    if-eqz p5, :cond_0

    iget-wide v0, p5, LX/2WF;->a:J

    :goto_0
    invoke-virtual {p0, v2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23328
    const-string v2, "range_to"

    if-eqz p5, :cond_1

    iget-wide v0, p5, LX/2WF;->b:J

    :goto_1
    invoke-virtual {p0, v2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23329
    return-void

    .line 23330
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 23331
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_1
.end method

.method public static declared-synchronized a$redex0(LX/0A1;Ljava/io/DataOutputStream;)V
    .locals 2

    .prologue
    .line 23310
    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 23311
    iget-wide v0, p0, LX/0A1;->h:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 23312
    iget-wide v0, p0, LX/0A1;->i:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 23313
    iget-wide v0, p0, LX/0A1;->j:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 23314
    iget-wide v0, p0, LX/0A1;->k:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 23315
    iget-wide v0, p0, LX/0A1;->l:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 23316
    iget-object v0, p0, LX/0A1;->f:LX/0A4;

    invoke-virtual {v0, p1}, LX/0A4;->a(Ljava/io/DataOutputStream;)V

    .line 23317
    iget-object v0, p0, LX/0A1;->g:LX/0A4;

    invoke-virtual {v0, p1}, LX/0A4;->a(Ljava/io/DataOutputStream;)V

    .line 23318
    iget-wide v0, p0, LX/0A1;->n:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 23319
    iget-wide v0, p0, LX/0A1;->o:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23320
    monitor-exit p0

    return-void

    .line 23321
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a$redex0(LX/0A1;[B)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 23291
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 23292
    :try_start_0
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 23293
    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 23294
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, LX/0A1;->h:J

    .line 23295
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, LX/0A1;->i:J

    .line 23296
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, LX/0A1;->j:J

    .line 23297
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, LX/0A1;->k:J

    .line 23298
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, LX/0A1;->l:J

    .line 23299
    iget-object v0, p0, LX/0A1;->f:LX/0A4;

    invoke-virtual {v0, v2}, LX/0A4;->a(Ljava/io/DataInputStream;)V

    .line 23300
    iget-object v0, p0, LX/0A1;->g:LX/0A4;

    invoke-virtual {v0, v2}, LX/0A4;->a(Ljava/io/DataInputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 23301
    :try_start_1
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, LX/0A1;->n:J

    .line 23302
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, LX/0A1;->o:J
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 23303
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 23304
    goto :goto_0

    .line 23305
    :catch_0
    move-exception v0

    .line 23306
    :try_start_2
    sget-object v2, LX/0A1;->a:Ljava/lang/Class;

    const-string v3, "Error reading previous exo cache counts. Legit when upgrading"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 23307
    :catch_1
    move-exception v0

    .line 23308
    sget-object v2, LX/0A1;->a:Ljava/lang/Class;

    const-string v3, "Error reading from storage, clearing accumulated vard"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23309
    invoke-static {p0}, LX/0A1;->f(LX/0A1;)V

    goto :goto_1
.end method

.method public static synthetic b(LX/0A1;J)J
    .locals 3

    .prologue
    .line 23194
    iget-wide v0, p0, LX/0A1;->i:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/0A1;->i:J

    return-wide v0
.end method

.method private b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23289
    iget-object v0, p0, LX/0A1;->d:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 23290
    if-nez v0, :cond_0

    const-string v0, "not_available"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(JZ)V
    .locals 1

    .prologue
    .line 23287
    new-instance v0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;-><init>(LX/0A1;JZ)V

    invoke-static {p0, v0}, LX/0A1;->a(LX/0A1;Ljava/lang/Runnable;)V

    .line 23288
    return-void
.end method

.method private static b(LX/0A1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 23284
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 23285
    iget-object v0, p0, LX/0A1;->c:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 23286
    return-void
.end method

.method private c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 23192
    iget-object v1, p0, LX/0A1;->d:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 23193
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/0A1;)V
    .locals 2

    .prologue
    .line 23195
    iget-object v0, p0, LX/0A1;->e:LX/0A2;

    new-instance v1, LX/0A7;

    invoke-direct {v1, p0}, LX/0A7;-><init>(LX/0A1;)V

    invoke-virtual {v0, v1}, LX/0A2;->a(LX/0A7;)V

    .line 23196
    return-void
.end method

.method private static declared-synchronized f(LX/0A1;)V
    .locals 2

    .prologue
    .line 23197
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, LX/0A1;->h:J

    .line 23198
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0A1;->i:J

    .line 23199
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0A1;->j:J

    .line 23200
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0A1;->k:J

    .line 23201
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0A1;->l:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23202
    monitor-exit p0

    return-void

    .line 23203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 23204
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0A1;->e:LX/0A2;

    new-instance v1, LX/0A8;

    invoke-direct {v1, p0}, LX/0A8;-><init>(LX/0A1;)V

    invoke-virtual {v0, v1}, LX/0A2;->a(LX/0A8;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23205
    monitor-exit p0

    return-void

    .line 23206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/2WG;JJIZ)V
    .locals 10

    .prologue
    .line 23207
    new-instance v1, Lcom/facebook/video/analytics/VideoPerformanceTracking$3;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lcom/facebook/video/analytics/VideoPerformanceTracking$3;-><init>(LX/0A1;LX/2WG;JJIZ)V

    invoke-static {p0, v1}, LX/0A1;->a(LX/0A1;Ljava/lang/Runnable;)V

    .line 23208
    return-void
.end method

.method public final a(Landroid/net/Uri;JLjava/util/List;JLjava/lang/String;Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 12
    .param p8    # Ljava/lang/Exception;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "J",
            "Ljava/util/List",
            "<",
            "LX/2WF;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/lang/Exception;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23209
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/0AB;->VIDEO_PREFETCH:LX/0AB;

    iget-object v3, v3, LX/0AB;->value:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "bytes_downloaded"

    move-wide/from16 v0, p5

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 23210
    const/4 v3, 0x0

    .line 23211
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2WF;

    .line 23212
    int-to-long v6, v4

    iget-wide v8, v3, LX/2WF;->b:J

    iget-wide v10, v3, LX/2WF;->a:J

    sub-long/2addr v8, v10

    add-long/2addr v6, v8

    long-to-int v3, v6

    move v4, v3

    .line 23213
    goto :goto_0

    .line 23214
    :cond_0
    const-string v3, "bytes_in_cache"

    int-to-long v4, v4

    sub-long v4, p2, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23215
    const-string v3, "prefetch_location"

    move-object/from16 v0, p9

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23216
    if-eqz p8, :cond_1

    .line 23217
    const-string v3, "exception"

    invoke-virtual/range {p8 .. p8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23218
    :cond_1
    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, LX/2WF;

    const-wide/16 v8, 0x0

    invoke-direct {v7, v8, v9, p2, p3}, LX/2WF;-><init>(JJ)V

    invoke-direct {p0}, LX/0A1;->b()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v3, p7

    move-object v4, p1

    invoke-static/range {v2 .. v8}, LX/0A1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Landroid/net/Uri;IZLX/2WF;Ljava/lang/String;)V

    .line 23219
    invoke-static {p0, v2}, LX/0A1;->b(LX/0A1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 23220
    invoke-direct {p0}, LX/0A1;->c()Z

    move-result v2

    move-wide/from16 v0, p5

    invoke-direct {p0, v0, v1, v2}, LX/0A1;->b(JZ)V

    .line 23221
    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 8

    .prologue
    const-wide v6, 0x408f400000000000L    # 1000.0

    .line 23222
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0A1;->r:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 23223
    :goto_0
    if-nez v0, :cond_1

    .line 23224
    :goto_1
    monitor-exit p0

    return-void

    .line 23225
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 23226
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0A1;->m:LX/19s;

    invoke-virtual {v0}, LX/19s;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 23227
    iget-object v0, p0, LX/0A1;->m:LX/19s;

    invoke-virtual {v0}, LX/19s;->g()Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;

    move-result-object v0

    .line 23228
    if-eqz v0, :cond_2

    .line 23229
    const-string v1, "exo_cache_used_space"

    iget-wide v2, v0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->a:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23230
    const-string v1, "exo_cache_chunks_reused"

    iget-wide v2, v0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->b:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23231
    const-string v1, "exo_total_bytes_read"

    iget-wide v2, v0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->c:J

    iget-wide v4, p0, LX/0A1;->o:J

    sub-long/2addr v2, v4

    invoke-virtual {p1, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23232
    iget-wide v2, v0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->c:J

    iput-wide v2, p0, LX/0A1;->o:J

    .line 23233
    const-string v1, "exo_bytes_read_from_cache"

    iget-wide v2, v0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->d:J

    iget-wide v4, p0, LX/0A1;->n:J

    sub-long/2addr v2, v4

    invoke-virtual {p1, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23234
    iget-wide v0, v0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->d:J

    iput-wide v0, p0, LX/0A1;->n:J

    .line 23235
    :cond_2
    const-string v0, "bytes_downloaded"

    iget-wide v2, p0, LX/0A1;->h:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23236
    const-string v0, "bytes_downloaded_cell"

    iget-wide v2, p0, LX/0A1;->i:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23237
    const-string v0, "bytes_prefetched"

    iget-wide v2, p0, LX/0A1;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23238
    const-string v0, "bytes_prefetched_wifi"

    iget-wide v2, p0, LX/0A1;->k:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23239
    const-string v0, "bytes_prefetched_cell"

    iget-wide v2, p0, LX/0A1;->l:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23240
    invoke-static {p0}, LX/0A1;->f(LX/0A1;)V

    .line 23241
    invoke-static {p0}, LX/0A1;->e(LX/0A1;)V

    .line 23242
    iget-object v0, p0, LX/0A1;->f:LX/0A4;

    invoke-virtual {v0}, LX/0A4;->b()J

    move-result-wide v0

    long-to-double v0, v0

    div-double/2addr v0, v6

    .line 23243
    const-string v2, "time_spent"

    invoke-virtual {p1, v2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23244
    iget-object v0, p0, LX/0A1;->g:LX/0A4;

    invoke-virtual {v0}, LX/0A4;->b()J

    move-result-wide v0

    long-to-double v0, v0

    div-double/2addr v0, v6

    .line 23245
    const-string v2, "time_spent_in_cell"

    invoke-virtual {p1, v2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23246
    const-string v0, "bytes_watched"

    iget-object v1, p0, LX/0A1;->f:LX/0A4;

    invoke-virtual {v1}, LX/0A4;->a()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23247
    const-string v0, "bytes_watched_in_cell"

    iget-object v1, p0, LX/0A1;->g:LX/0A4;

    invoke-virtual {v1}, LX/0A4;->a()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 23248
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;)V
    .locals 3

    .prologue
    .line 23249
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/0AA;->VIDEO_MANIFEST_MISALIGN:LX/0AA;

    iget-object v1, v1, LX/0AA;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "video_id"

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "url"

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "expected_segment_info"

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "actual_segment_info"

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/04F;->IS_LIVE_STREAM:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "video"

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 23250
    invoke-static {p0, v0}, LX/0A1;->b(LX/0A1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 23251
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;IJJZLX/2WF;Ljava/lang/String;ZJJ)V
    .locals 10

    .prologue
    .line 23252
    move-wide/from16 v0, p6

    invoke-static {p0, v0, v1, p4, p5}, LX/0A1;->a(LX/0A1;JJ)V

    .line 23253
    move-wide/from16 v0, p6

    move/from16 v2, p11

    invoke-static {p0, v0, v1, v2}, LX/0A1;->a(LX/0A1;JZ)V

    .line 23254
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/0AB;->VIDEO_CACHE_REQUEST_FINISHED:LX/0AB;

    iget-object v4, v4, LX/0AB;->value:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    move/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    .line 23255
    invoke-static/range {v3 .. v9}, LX/0A1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Landroid/net/Uri;IZLX/2WF;Ljava/lang/String;)V

    .line 23256
    const-string v4, "bytes_served"

    invoke-virtual {v3, v4, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23257
    const-string v4, "bytes_downloaded"

    move-wide/from16 v0, p6

    invoke-virtual {v3, v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23258
    const-string v4, "first_byte_served_time_ms"

    move-wide/from16 v0, p14

    invoke-virtual {v3, v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23259
    const-string v4, "serving_time_ms"

    move-wide/from16 v0, p12

    invoke-virtual {v3, v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23260
    invoke-static {p0, v3}, LX/0A1;->b(LX/0A1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 23261
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;ILjava/lang/Throwable;JJZLX/2WF;Ljava/lang/String;ZJJ)V
    .locals 11

    .prologue
    .line 23262
    move-wide/from16 v0, p7

    move-wide/from16 v2, p5

    invoke-static {p0, v0, v1, v2, v3}, LX/0A1;->a(LX/0A1;JJ)V

    .line 23263
    move-wide/from16 v0, p7

    move/from16 v2, p12

    invoke-static {p0, v0, v1, v2}, LX/0A1;->a(LX/0A1;JZ)V

    .line 23264
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/0AB;->VIDEO_CACHE_REQUEST_ABORTED:LX/0AB;

    iget-object v5, v5, LX/0AB;->value:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    move-object v5, p1

    move-object v6, p2

    move v7, p3

    move/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    .line 23265
    invoke-static/range {v4 .. v10}, LX/0A1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Landroid/net/Uri;IZLX/2WF;Ljava/lang/String;)V

    .line 23266
    const-string v5, "first_byte_served_time_ms"

    move-wide/from16 v0, p15

    invoke-virtual {v4, v5, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23267
    const-string v5, "serving_time_ms"

    move-wide/from16 v0, p13

    invoke-virtual {v4, v5, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23268
    const-string v5, "bytes_served"

    move-wide/from16 v0, p5

    invoke-virtual {v4, v5, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23269
    const-string v5, "bytes_downloaded"

    move-wide/from16 v0, p7

    invoke-virtual {v4, v5, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23270
    const-string v5, "exception"

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23271
    const-string v5, "reason"

    invoke-virtual {p4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23272
    invoke-static {p0, v4}, LX/0A1;->b(LX/0A1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 23273
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;ILjava/lang/Throwable;ZLX/2WF;Ljava/lang/String;J)V
    .locals 10

    .prologue
    .line 23274
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/0AB;->VIDEO_CACHE_REQUEST_FAILED:LX/0AB;

    iget-object v3, v3, LX/0AB;->value:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    .line 23275
    invoke-static/range {v2 .. v8}, LX/0A1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Landroid/net/Uri;IZLX/2WF;Ljava/lang/String;)V

    .line 23276
    const-string v3, "serving_time_ms"

    move-wide/from16 v0, p8

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23277
    const-string v3, "exception"

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23278
    const-string v3, "reason"

    invoke-virtual {p4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23279
    invoke-static {p0, v2}, LX/0A1;->b(LX/0A1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 23280
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIJJILX/0AC;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 23281
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, LX/0AA;->VIDEO_VPS_HTTP_TRANSFER:LX/0AA;

    iget-object v2, v2, LX/0AA;->value:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "video_id"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "url"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "is_prefetch"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bytes_length"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "transfer_start_duration_ms"

    invoke-virtual {v1, v2, p6, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "transfer_end_duration_ms"

    invoke-virtual {v1, v2, p8, p9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "seq_num"

    invoke-virtual {v1, v2, p10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "cache_type"

    invoke-virtual {p11}, LX/0AC;->getValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "first_time_play"

    move/from16 v0, p12

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "play_origin"

    move-object/from16 v0, p13

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "debug_info"

    move-object/from16 v0, p14

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "video"

    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 23282
    invoke-static {p0, v1}, LX/0A1;->b(LX/0A1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 23283
    return-void
.end method
