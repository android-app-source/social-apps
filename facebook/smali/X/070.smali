.class public LX/070;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/071;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/05y;

.field private final b:LX/05h;

.field private final c:LX/06z;

.field private final d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field private final e:LX/01o;

.field private final f:LX/06B;

.field private final g:LX/05K;

.field private final h:Ljava/util/concurrent/ScheduledExecutorService;

.field private final i:LX/05L;

.field private j:Ljava/net/InetAddress;

.field private k:Ljava/net/InetAddress;

.field public l:Ljava/net/Socket;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "member reference guarded by this"
    .end annotation
.end field

.field public m:LX/07O;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "member reference guarded by this"
    .end annotation
.end field

.field public n:LX/07Q;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "member reference guarded by this"
    .end annotation
.end field

.field private volatile o:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public volatile p:LX/076;

.field private volatile q:LX/075;


# direct methods
.method public constructor <init>(LX/05y;LX/05h;LX/06z;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;LX/01o;Ljava/util/concurrent/ScheduledExecutorService;LX/06B;LX/05K;LX/05L;)V
    .locals 1

    .prologue
    .line 18530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18531
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/070;->o:Z

    .line 18532
    iput-object p1, p0, LX/070;->a:LX/05y;

    .line 18533
    iput-object p2, p0, LX/070;->b:LX/05h;

    .line 18534
    iput-object p3, p0, LX/070;->c:LX/06z;

    .line 18535
    iput-object p4, p0, LX/070;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 18536
    iput-object p5, p0, LX/070;->e:LX/01o;

    .line 18537
    iput-object p6, p0, LX/070;->h:Ljava/util/concurrent/ScheduledExecutorService;

    .line 18538
    iput-object p7, p0, LX/070;->f:LX/06B;

    .line 18539
    iput-object p9, p0, LX/070;->i:LX/05L;

    .line 18540
    iput-object p8, p0, LX/070;->g:LX/05K;

    .line 18541
    return-void
.end method

.method private static a(LX/07W;)LX/05B;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/07W;",
            ")",
            "LX/05B",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18475
    if-eqz p0, :cond_0

    .line 18476
    instance-of v0, p0, LX/07X;

    if-eqz v0, :cond_0

    .line 18477
    check-cast p0, LX/07X;

    invoke-virtual {p0}, LX/07X;->a()LX/0B5;

    move-result-object v0

    iget-object v0, v0, LX/0B5;->a:Ljava/lang/String;

    invoke-static {v0}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v0

    .line 18478
    :goto_0
    return-object v0

    .line 18479
    :cond_0
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v0, v0

    .line 18480
    goto :goto_0
.end method

.method private static a(LX/070;Ljava/lang/String;)LX/07D;
    .locals 13

    .prologue
    .line 18481
    iget-object v0, p0, LX/070;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    .line 18482
    iget-object v0, p0, LX/070;->f:LX/06B;

    iget-object v1, p0, LX/070;->c:LX/06z;

    .line 18483
    iget v9, v1, LX/06z;->i:I

    int-to-long v9, v9

    const-wide/16 v11, 0x3e8

    mul-long/2addr v9, v11

    move-wide v4, v9

    .line 18484
    invoke-virtual {v0, p1, v4, v5}, LX/06B;->a(Ljava/lang/String;J)LX/07D;

    move-result-object v0

    .line 18485
    iget-object v1, p0, LX/070;->b:LX/05h;

    iget-object v4, p0, LX/070;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v4}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    sub-long v2, v4, v2

    iget-object v4, p0, LX/070;->p:LX/076;

    invoke-virtual {v4}, LX/076;->e()J

    move-result-wide v4

    iget-object v6, p0, LX/070;->p:LX/076;

    invoke-virtual {v6}, LX/076;->d()J

    move-result-wide v6

    iget-object v8, p0, LX/070;->p:LX/076;

    invoke-virtual {v8}, LX/076;->b()Landroid/net/NetworkInfo;

    move-result-object v8

    invoke-virtual/range {v1 .. v8}, LX/05h;->a(JJJLandroid/net/NetworkInfo;)V

    .line 18486
    return-object v0
.end method

.method private a(LX/07Q;LX/07O;ZLX/079;I)LX/07k;
    .locals 10

    .prologue
    .line 18487
    iget-object v0, p0, LX/070;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    .line 18488
    :try_start_0
    invoke-static {p0, p1, p3, p4, p5}, LX/070;->a(LX/070;LX/07Q;ZLX/079;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 18489
    :try_start_1
    invoke-virtual {p2}, LX/07O;->a()LX/07W;
    :try_end_1
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/util/zip/DataFormatException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v9

    .line 18490
    invoke-virtual {v9}, LX/07W;->e()LX/07S;

    move-result-object v0

    sget-object v1, LX/07S;->CONNACK:LX/07S;

    if-eq v0, v1, :cond_0

    .line 18491
    const-string v0, "DefaultMqttClientCore"

    const-string v1, "receive/unexpected; type=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v9}, LX/07W;->e()LX/07S;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18492
    new-instance v0, LX/07k;

    sget-object v1, LX/0Hs;->FAILED_INVALID_CONACK:LX/0Hs;

    invoke-direct {v0, v1}, LX/07k;-><init>(LX/0Hs;)V

    .line 18493
    :goto_0
    return-object v0

    .line 18494
    :catch_0
    move-exception v1

    .line 18495
    const-string v0, "DefaultMqttClientCore"

    const-string v2, "exception/send_connect_failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18496
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_CONNECT_MESSAGE:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V

    goto :goto_0

    .line 18497
    :catch_1
    move-exception v1

    .line 18498
    const-string v0, "DefaultMqttClientCore"

    const-string v2, "exception/read_conack_timeout"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18499
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_MQTT_CONACK_TIMEOUT:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V

    goto :goto_0

    .line 18500
    :catch_2
    move-exception v1

    .line 18501
    const-string v0, "DefaultMqttClientCore"

    const-string v2, "exception/read_conack_failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18502
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_CONNACK_READ:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V

    goto :goto_0

    .line 18503
    :catch_3
    move-exception v1

    .line 18504
    const-string v0, "DefaultMqttClientCore"

    const-string v2, "exception/deserialize_failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18505
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_CONNACK_READ:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V

    goto :goto_0

    .line 18506
    :catch_4
    move-exception v1

    .line 18507
    const-string v0, "DefaultMqttClientCore"

    const-string v2, "exception/compression_error"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, LX/05D;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18508
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_CONNACK_READ:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V

    goto :goto_0

    .line 18509
    :cond_0
    iget-object v0, p0, LX/070;->b:LX/05h;

    sget-object v1, LX/07S;->CONNECT:LX/07S;

    invoke-virtual {v1}, LX/07S;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, LX/070;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v4}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    sub-long v2, v4, v2

    iget-object v4, p0, LX/070;->p:LX/076;

    invoke-virtual {v4}, LX/076;->e()J

    move-result-wide v4

    iget-object v6, p0, LX/070;->p:LX/076;

    invoke-virtual {v6}, LX/076;->d()J

    move-result-wide v6

    iget-object v8, p0, LX/070;->p:LX/076;

    invoke-virtual {v8}, LX/076;->b()Landroid/net/NetworkInfo;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, LX/05h;->a(Ljava/lang/String;JJJLandroid/net/NetworkInfo;)V

    move-object v0, v9

    .line 18510
    check-cast v0, LX/07j;

    .line 18511
    invoke-virtual {v0}, LX/07j;->a()LX/07e;

    move-result-object v1

    iget-byte v1, v1, LX/07e;->a:B

    .line 18512
    if-eqz v1, :cond_5

    .line 18513
    const-string v0, "DefaultMqttClientCore"

    const-string v2, "connection/refused; rc=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18514
    const/16 v0, 0x11

    if-ne v1, v0, :cond_1

    .line 18515
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_CONNECTION_REFUSED_SERVER_SHEDDING_LOAD:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;B)V

    goto/16 :goto_0

    .line 18516
    :cond_1
    const/4 v0, 0x5

    if-ne v1, v0, :cond_2

    .line 18517
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_CONNECTION_REFUSED_NOT_AUTHORIZED:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;B)V

    goto/16 :goto_0

    .line 18518
    :cond_2
    const/4 v0, 0x4

    if-ne v1, v0, :cond_3

    .line 18519
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;B)V

    goto/16 :goto_0

    .line 18520
    :cond_3
    const/16 v0, 0x13

    if-ne v1, v0, :cond_4

    .line 18521
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_CONNECTION_UNKNOWN_CONNECT_HASH:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;B)V

    goto/16 :goto_0

    .line 18522
    :cond_4
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_CONNECTION_REFUSED:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;B)V

    goto/16 :goto_0

    .line 18523
    :cond_5
    invoke-virtual {v0}, LX/07j;->b()LX/07h;

    move-result-object v2

    .line 18524
    const-string v0, "DefaultMqttClientCore"

    const-string v1, "connection/conack; payload=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v0, v1, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18525
    iget-object v0, v2, LX/07h;->c:Ljava/lang/String;

    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v2, LX/07h;->d:Ljava/lang/String;

    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 18526
    :cond_6
    sget-object v0, LX/06y;->a:LX/06y;

    .line 18527
    :goto_1
    iget-object v1, p0, LX/070;->p:LX/076;

    iget-object v3, v2, LX/07h;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/076;->a(Ljava/lang/String;)V

    .line 18528
    new-instance v1, LX/07k;

    iget-object v3, v2, LX/07h;->a:Ljava/lang/String;

    invoke-static {v3}, LX/05V;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, v2, LX/07h;->b:Ljava/lang/String;

    invoke-static {v2}, LX/05V;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, LX/06k;->a(Ljava/lang/String;Ljava/lang/String;)LX/06k;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/07k;-><init>(LX/06k;LX/06y;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 18529
    :cond_7
    new-instance v0, LX/06y;

    iget-object v1, v2, LX/07h;->c:Ljava/lang/String;

    iget-object v3, v2, LX/07h;->d:Ljava/lang/String;

    iget-object v4, p0, LX/070;->e:LX/01o;

    invoke-virtual {v4}, LX/01o;->a()J

    move-result-wide v4

    invoke-direct {v0, v1, v3, v4, v5}, LX/06y;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_1
.end method

.method private static a(LX/070;IILjava/net/InetAddress;Ljava/net/InetAddress;LX/07G;)Ljava/net/Socket;
    .locals 8

    .prologue
    .line 18542
    new-instance v0, LX/09Z;

    iget-object v6, p0, LX/070;->h:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/070;->c:LX/06z;

    .line 18543
    iget v2, v1, LX/06z;->j:I

    move v7, v2

    .line 18544
    move-object v1, p3

    move-object v2, p4

    move v3, p1

    move v4, p2

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, LX/09Z;-><init>(Ljava/net/InetAddress;Ljava/net/InetAddress;IILX/07G;Ljava/util/concurrent/ScheduledExecutorService;I)V

    .line 18545
    invoke-virtual {v0}, LX/09Z;->a()Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/070;ZLjava/lang/String;LX/07D;I)Ljava/net/Socket;
    .locals 19

    .prologue
    .line 18546
    const/4 v9, 0x0

    .line 18547
    move-object/from16 v0, p0

    iget-object v2, v0, LX/070;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v16

    .line 18548
    const/4 v13, 0x0

    .line 18549
    sget-object v8, LX/07E;->UNKNOWN:LX/07E;

    .line 18550
    :try_start_0
    invoke-virtual/range {p3 .. p3}, LX/07D;->f()Ljava/net/InetAddress;

    move-result-object v5

    .line 18551
    invoke-virtual/range {p3 .. p3}, LX/07D;->g()Ljava/net/InetAddress;

    move-result-object v6

    .line 18552
    if-eqz p1, :cond_5

    .line 18553
    move-object/from16 v0, p0

    iget-object v2, v0, LX/070;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v10

    .line 18554
    move-object/from16 v0, p0

    iget-object v2, v0, LX/070;->a:LX/05y;

    invoke-virtual {v2}, LX/05y;->a()LX/07G;

    move-result-object v7

    .line 18555
    if-eqz v6, :cond_2

    .line 18556
    move-object/from16 v0, p0

    iget-object v2, v0, LX/070;->c:LX/06z;

    invoke-virtual {v2}, LX/06z;->l()I

    move-result v4

    move-object/from16 v2, p0

    move/from16 v3, p4

    invoke-static/range {v2 .. v7}, LX/070;->a(LX/070;IILjava/net/InetAddress;Ljava/net/InetAddress;LX/07G;)Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 18557
    :try_start_1
    invoke-virtual {v3}, Ljava/net/Socket;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 18558
    invoke-virtual {v3}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 18559
    sget-object v8, LX/07E;->HE_PREFERRED:LX/07E;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-object v2, v8

    move-object v8, v3

    .line 18560
    :goto_0
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, LX/070;->c:LX/06z;

    invoke-virtual {v3}, LX/06z;->l()I

    move-result v3

    int-to-long v4, v3

    move-object/from16 v0, p0

    iget-object v3, v0, LX/070;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v3}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v14

    sub-long v10, v14, v10

    sub-long v11, v4, v10

    .line 18561
    const-wide/16 v4, 0x0

    cmp-long v3, v11, v4

    if-gtz v3, :cond_3

    .line 18562
    new-instance v3, Ljava/net/SocketTimeoutException;

    const-string v4, "connectSocket already timeout"

    invoke-direct {v3, v4}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 18563
    :catch_0
    move-exception v3

    move-object/from16 v18, v3

    move-object v3, v8

    move-object v8, v2

    move-object/from16 v2, v18

    .line 18564
    :goto_1
    :try_start_3
    const-string v4, "DefaultMqttClientCore"

    const-string v5, "connection/close/IOException"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v2, v5, v6}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18565
    invoke-static {v3}, LX/07H;->b(Ljava/net/Socket;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    .line 18566
    :try_start_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 18567
    :catchall_0
    move-exception v4

    move-object v13, v4

    move-object v5, v2

    move-object v14, v3

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/070;->b:LX/05h;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/070;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v3}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v6

    sub-long v3, v6, v16

    invoke-virtual {v8}, LX/07E;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5}, LX/05B;->b(Ljava/lang/Object;)LX/05B;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v5, v0, LX/070;->p:LX/076;

    invoke-virtual {v5}, LX/076;->e()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v5, v0, LX/070;->p:LX/076;

    invoke-virtual {v5}, LX/076;->d()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v5, v0, LX/070;->p:LX/076;

    invoke-virtual {v5}, LX/076;->b()Landroid/net/NetworkInfo;

    move-result-object v12

    move/from16 v5, p4

    invoke-virtual/range {v2 .. v12}, LX/05h;->a(JILjava/lang/String;LX/05B;JJLandroid/net/NetworkInfo;)V

    .line 18568
    if-eqz v14, :cond_0

    .line 18569
    invoke-virtual {v14}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/070;->j:Ljava/net/InetAddress;

    .line 18570
    invoke-virtual {v14}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/070;->k:Ljava/net/InetAddress;

    :cond_0
    throw v13

    .line 18571
    :cond_1
    :try_start_5
    invoke-virtual {v3}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 18572
    sget-object v8, LX/07E;->HE_NONPREFERRED:LX/07E;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-object v2, v8

    move-object v8, v3

    goto/16 :goto_0

    .line 18573
    :cond_2
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/070;->c:LX/06z;

    invoke-virtual {v2}, LX/06z;->l()I

    move-result v2

    move/from16 v0, p4

    invoke-static {v5, v0, v2, v7}, LX/07H;->a(Ljava/net/InetAddress;IILX/07G;)Ljava/net/Socket;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v3

    .line 18574
    :try_start_7
    invoke-virtual {v3}, Ljava/net/Socket;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 18575
    sget-object v8, LX/07E;->DEFAULT:LX/07E;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-object v2, v8

    move-object v8, v3

    goto/16 :goto_0

    :cond_3
    move-object/from16 v9, p2

    move/from16 v10, p4

    .line 18576
    :try_start_8
    invoke-virtual/range {v7 .. v12}, LX/07G;->a(Ljava/net/Socket;Ljava/lang/String;IJ)Ljava/net/Socket;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    move-result-object v3

    move-object v5, v2

    move-object v13, v3

    .line 18577
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/070;->b:LX/05h;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/070;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v3}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v6

    sub-long v3, v6, v16

    invoke-virtual {v5}, LX/07E;->name()Ljava/lang/String;

    move-result-object v6

    const/4 v5, 0x0

    invoke-static {v5}, LX/05B;->b(Ljava/lang/Object;)LX/05B;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v5, v0, LX/070;->p:LX/076;

    invoke-virtual {v5}, LX/076;->e()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v5, v0, LX/070;->p:LX/076;

    invoke-virtual {v5}, LX/076;->d()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v5, v0, LX/070;->p:LX/076;

    invoke-virtual {v5}, LX/076;->b()Landroid/net/NetworkInfo;

    move-result-object v12

    move/from16 v5, p4

    invoke-virtual/range {v2 .. v12}, LX/05h;->a(JILjava/lang/String;LX/05B;JJLandroid/net/NetworkInfo;)V

    .line 18578
    if-eqz v13, :cond_4

    .line 18579
    invoke-virtual {v13}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/070;->j:Ljava/net/InetAddress;

    .line 18580
    invoke-virtual {v13}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/070;->k:Ljava/net/InetAddress;

    .line 18581
    :cond_4
    return-object v13

    .line 18582
    :cond_5
    :try_start_9
    new-instance v2, Ljava/net/Socket;

    invoke-direct {v2}, Ljava/net/Socket;-><init>()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 18583
    :try_start_a
    invoke-static {v2}, LX/07H;->a(Ljava/net/Socket;)V

    .line 18584
    new-instance v3, Ljava/net/InetSocketAddress;

    move/from16 v0, p4

    invoke-direct {v3, v5, v0}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    move-object/from16 v0, p0

    iget-object v4, v0, LX/070;->c:LX/06z;

    invoke-virtual {v4}, LX/06z;->l()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    move-object v5, v8

    move-object v13, v2

    goto :goto_3

    .line 18585
    :catchall_1
    move-exception v2

    move-object v5, v13

    move-object v14, v9

    move-object v13, v2

    goto/16 :goto_2

    :catchall_2
    move-exception v2

    move-object v5, v13

    move-object v14, v3

    move-object v13, v2

    goto/16 :goto_2

    :catchall_3
    move-exception v3

    move-object v5, v13

    move-object v14, v8

    move-object v13, v3

    move-object v8, v2

    goto/16 :goto_2

    :catchall_4
    move-exception v3

    move-object v5, v13

    move-object v14, v2

    move-object v13, v3

    goto/16 :goto_2

    :catchall_5
    move-exception v2

    move-object v5, v13

    move-object v14, v3

    move-object v13, v2

    goto/16 :goto_2

    .line 18586
    :catch_1
    move-exception v2

    move-object v3, v9

    goto/16 :goto_1

    :catch_2
    move-exception v2

    goto/16 :goto_1

    :catch_3
    move-exception v3

    move-object/from16 v18, v3

    move-object v3, v2

    move-object/from16 v2, v18

    goto/16 :goto_1

    :cond_6
    move-object v2, v8

    move-object v8, v3

    goto/16 :goto_0
.end method

.method private static a(LX/070;LX/07Q;LX/07W;)V
    .locals 5

    .prologue
    .line 18587
    if-nez p1, :cond_0

    .line 18588
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No message encoder"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18589
    :cond_0
    invoke-static {p2}, LX/070;->a(LX/07W;)LX/05B;

    move-result-object v1

    .line 18590
    const-string v0, ""

    .line 18591
    invoke-virtual {v1}, LX/05B;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 18592
    invoke-virtual {v1}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0AJ;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 18593
    if-nez v0, :cond_1

    .line 18594
    invoke-virtual {v1}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 18595
    :cond_1
    :try_start_0
    invoke-virtual {p1, p2}, LX/07Q;->a(LX/07W;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 18596
    iget-object v1, p0, LX/070;->p:LX/076;

    invoke-virtual {p2}, LX/07W;->e()LX/07S;

    move-result-object v2

    invoke-virtual {v2}, LX/07S;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/076;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 18597
    return-void

    .line 18598
    :catch_0
    move-exception v1

    .line 18599
    iget-object v2, p0, LX/070;->p:LX/076;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, LX/07W;->e()LX/07S;

    move-result-object v4

    invoke-virtual {v4}, LX/07S;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LX/076;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 18600
    throw v1
.end method

.method private static a(LX/070;LX/07Q;ZLX/079;I)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 18601
    new-instance v6, LX/07R;

    sget-object v0, LX/07S;->CONNECT:LX/07S;

    invoke-direct {v6, v0}, LX/07R;-><init>(LX/07S;)V

    .line 18602
    new-instance v0, LX/07U;

    const/4 v1, 0x3

    if-nez p2, :cond_0

    move v3, v2

    :goto_0
    move v4, v2

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/07U;-><init>(IZZZI)V

    .line 18603
    new-instance v1, LX/07V;

    invoke-direct {v1, v6, v0, p3}, LX/07V;-><init>(LX/07R;LX/07U;LX/079;)V

    .line 18604
    invoke-static {p0, p1, v1}, LX/070;->a(LX/070;LX/07Q;LX/07W;)V

    .line 18605
    return-void

    .line 18606
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 18607
    iget-object v0, p0, LX/070;->p:LX/076;

    invoke-static {p1}, LX/0AX;->getFromReadException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v1

    sget-object v2, LX/0BJ;->NETWORK_THREAD_LOOP:LX/0BJ;

    invoke-virtual {v0, v1, v2, p1}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 18608
    return-void
.end method

.method public static b(LX/070;Ljava/lang/String;IZLX/079;IZ)LX/07k;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 18609
    const-string v0, "DefaultMqttClientCore"

    const-string v1, "connection/connecting"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18610
    :try_start_0
    invoke-static {p0, p1}, LX/070;->a(LX/070;Ljava/lang/String;)LX/07D;
    :try_end_0
    .catch LX/0Hj; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 18611
    :try_start_1
    invoke-static {p0, p6, p1, v7, p2}, LX/070;->a(LX/070;ZLjava/lang/String;LX/07D;I)Ljava/net/Socket;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 18612
    if-nez v8, :cond_0

    .line 18613
    const-string v0, "DefaultMqttClientCore"

    const-string v1, "connection/socket/failed"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18614
    iget-object v0, p0, LX/070;->f:LX/06B;

    invoke-virtual {v0, v7}, LX/06B;->a(LX/07D;)V

    .line 18615
    :cond_0
    iget-object v1, p0, LX/070;->p:LX/076;

    if-eqz v8, :cond_2

    const-string v0, "SSL"

    :goto_0
    const-string v2, ""

    invoke-virtual {v1, v0, v2}, LX/076;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 18616
    invoke-static {v8}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 18617
    monitor-enter p0

    .line 18618
    :try_start_2
    iget-boolean v0, p0, LX/070;->o:Z

    if-eqz v0, :cond_6

    .line 18619
    const-string v0, "DefaultMqttClientCore"

    const-string v1, "connection/connecting/aborted before sending connect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18620
    new-instance v0, LX/07k;

    sget-object v1, LX/0Hs;->FAILED_STOPPED_BEFORE_SSL:LX/0Hs;

    invoke-direct {v0, v1}, LX/07k;-><init>(LX/0Hs;)V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 18621
    :goto_1
    return-object v0

    .line 18622
    :catch_0
    move-exception v1

    .line 18623
    const-string v0, "DefaultMqttClientCore"

    const-string v2, "connection/dns/unresolved; status=%s"

    new-array v3, v3, [Ljava/lang/Object;

    .line 18624
    iget-object v4, v1, LX/0Hj;->mDNSResolveStatus:LX/0Hi;

    move-object v4, v4

    .line 18625
    aput-object v4, v3, v5

    invoke-static {v0, v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18626
    sget-object v0, LX/0Hi;->TimedOut:LX/0Hi;

    .line 18627
    iget-object v2, v1, LX/0Hj;->mDNSResolveStatus:LX/0Hi;

    move-object v2, v2

    .line 18628
    invoke-virtual {v0, v2}, LX/0Hi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18629
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_DNS_RESOLVE_TIMEOUT:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V

    goto :goto_1

    .line 18630
    :cond_1
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_DNS_UNRESOLVED:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V

    goto :goto_1

    .line 18631
    :cond_2
    const-string v0, "SSL-failed"

    goto :goto_0

    .line 18632
    :catch_1
    move-exception v1

    .line 18633
    :try_start_3
    instance-of v0, v1, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_3

    .line 18634
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_SOCKET_CONNECT_TIMEOUT:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 18635
    :goto_2
    const-string v1, "DefaultMqttClientCore"

    const-string v2, "connection/socket/failed"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18636
    iget-object v1, p0, LX/070;->f:LX/06B;

    invoke-virtual {v1, v7}, LX/06B;->a(LX/07D;)V

    .line 18637
    iget-object v1, p0, LX/070;->p:LX/076;

    const-string v2, "SSL-failed"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, LX/076;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 18638
    :cond_3
    :try_start_4
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    .line 18639
    if-nez v0, :cond_9

    .line 18640
    :cond_4
    :goto_3
    move v0, v2

    .line 18641
    if-eqz v0, :cond_5

    .line 18642
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_SOCKET_CONNECT_ERROR_SSL_CLOCK_SKEW:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 18643
    :catchall_0
    move-exception v0

    .line 18644
    const-string v1, "DefaultMqttClientCore"

    const-string v2, "connection/socket/failed"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18645
    iget-object v1, p0, LX/070;->f:LX/06B;

    invoke-virtual {v1, v7}, LX/06B;->a(LX/07D;)V

    .line 18646
    iget-object v1, p0, LX/070;->p:LX/076;

    const-string v2, "SSL-failed"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, LX/076;->a(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 18647
    :cond_5
    :try_start_5
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_SOCKET_CONNECT_ERROR:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 18648
    :cond_6
    const/4 v0, 0x1

    :try_start_6
    iput-boolean v0, p0, LX/070;->o:Z

    .line 18649
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 18650
    :try_start_7
    new-instance v0, LX/07O;

    new-instance v1, LX/07P;

    invoke-direct {v1}, LX/07P;-><init>()V

    iget-object v2, p0, LX/070;->b:LX/05h;

    iget-object v3, p0, LX/070;->c:LX/06z;

    .line 18651
    iget v4, v3, LX/06z;->z:I

    move v3, v4

    .line 18652
    iget-object v4, p0, LX/070;->g:LX/05K;

    iget-object v5, p0, LX/070;->q:LX/075;

    invoke-direct/range {v0 .. v5}, LX/07O;-><init>(LX/07P;LX/05h;ILX/05K;LX/075;)V

    .line 18653
    new-instance v2, LX/07Q;

    iget-object v1, p0, LX/070;->c:LX/06z;

    .line 18654
    iget v3, v1, LX/06z;->z:I

    move v1, v3

    .line 18655
    iget-object v3, p0, LX/070;->g:LX/05K;

    iget-object v4, p0, LX/070;->i:LX/05L;

    iget-object v5, p0, LX/070;->q:LX/075;

    invoke-direct {v2, v1, v3, v4, v5}, LX/07Q;-><init>(ILX/05K;LX/05L;LX/075;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 18656
    :try_start_8
    new-instance v1, Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 18657
    iput-object v1, v0, LX/07O;->f:Ljava/io/DataInputStream;

    .line 18658
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/BufferedOutputStream;

    invoke-virtual {v8}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 18659
    iput-object v1, v2, LX/07Q;->e:Ljava/io/DataOutputStream;

    .line 18660
    iget-object v1, p0, LX/070;->c:LX/06z;

    invoke-virtual {v1}, LX/06z;->k()I

    move-result v1

    invoke-virtual {v8, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    move-object v1, p0

    move-object v3, v0

    move v4, p3

    move-object v5, p4

    move v6, p5

    .line 18661
    invoke-direct/range {v1 .. v6}, LX/070;->a(LX/07Q;LX/07O;ZLX/079;I)LX/07k;

    move-result-object v1

    .line 18662
    const/4 v3, 0x0

    invoke-virtual {v8, v3}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 18663
    :try_start_9
    iget-boolean v3, v1, LX/07k;->a:Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    if-nez v3, :cond_7

    .line 18664
    invoke-static {v8}, LX/07H;->b(Ljava/net/Socket;)V

    .line 18665
    iget-object v0, p0, LX/070;->f:LX/06B;

    invoke-virtual {v0, v7}, LX/06B;->a(LX/07D;)V

    move-object v0, v1

    .line 18666
    goto/16 :goto_1

    .line 18667
    :catchall_1
    move-exception v0

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    throw v0

    .line 18668
    :catch_2
    move-exception v0

    move-object v1, v0

    .line 18669
    :try_start_b
    const-string v0, "DefaultMqttClientCore"

    const-string v2, "connection/connecting/failed_io"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18670
    new-instance v0, LX/07k;

    sget-object v2, LX/0Hs;->FAILED_CREATE_IOSTREAM:LX/0Hs;

    invoke-direct {v0, v2, v1}, LX/07k;-><init>(LX/0Hs;Ljava/lang/Exception;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 18671
    invoke-static {v8}, LX/07H;->b(Ljava/net/Socket;)V

    .line 18672
    iget-object v1, p0, LX/070;->f:LX/06B;

    invoke-virtual {v1, v7}, LX/06B;->a(LX/07D;)V

    goto/16 :goto_1

    .line 18673
    :cond_7
    :try_start_c
    monitor-enter p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 18674
    :try_start_d
    iget-object v3, p0, LX/070;->p:LX/076;

    sget-object v4, LX/074;->DISCONNECTED:LX/074;

    invoke-virtual {v3, v4}, LX/076;->a(LX/074;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 18675
    const-string v0, "DefaultMqttClientCore"

    const-string v1, "connection/connecting/unexpected_disconnect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18676
    new-instance v0, LX/07k;

    sget-object v1, LX/0Hs;->FAILED_UNEXPECTED_DISCONNECT:LX/0Hs;

    invoke-direct {v0, v1}, LX/07k;-><init>(LX/0Hs;)V

    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 18677
    invoke-static {v8}, LX/07H;->b(Ljava/net/Socket;)V

    .line 18678
    iget-object v1, p0, LX/070;->f:LX/06B;

    invoke-virtual {v1, v7}, LX/06B;->a(LX/07D;)V

    goto/16 :goto_1

    .line 18679
    :cond_8
    :try_start_e
    iput-object v8, p0, LX/070;->l:Ljava/net/Socket;

    .line 18680
    iput-object v2, p0, LX/070;->n:LX/07Q;

    .line 18681
    iput-object v0, p0, LX/070;->m:LX/07O;

    .line 18682
    iget-object v3, p0, LX/070;->p:LX/076;

    sget-object v4, LX/074;->CONNECTED:LX/074;

    invoke-virtual {v3, v4}, LX/076;->b(LX/074;)V

    .line 18683
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 18684
    :try_start_f
    iget-object v0, p0, LX/070;->p:LX/076;

    invoke-virtual {v0}, LX/076;->c()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    .line 18685
    iget-object v0, p0, LX/070;->f:LX/06B;

    invoke-virtual {v0, v7}, LX/06B;->b(LX/07D;)V

    move-object v0, v1

    .line 18686
    goto/16 :goto_1

    .line 18687
    :catchall_2
    move-exception v0

    :try_start_10
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    :try_start_11
    throw v0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    .line 18688
    :catchall_3
    move-exception v0

    .line 18689
    invoke-static {v8}, LX/07H;->b(Ljava/net/Socket;)V

    .line 18690
    iget-object v1, p0, LX/070;->f:LX/06B;

    invoke-virtual {v1, v7}, LX/06B;->a(LX/07D;)V

    .line 18691
    throw v0

    .line 18692
    :cond_9
    const-string v3, "Could not validate certificate: current time"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 18693
    const-string v3, "validation time: Thu Aug 28"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "validation time: Wed Aug 27"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 18694
    :cond_a
    const/4 v2, 0x1

    goto/16 :goto_3
.end method

.method public static h(LX/070;)V
    .locals 3

    .prologue
    .line 18695
    :goto_0
    monitor-enter p0

    .line 18696
    :try_start_0
    iget-object v0, p0, LX/070;->p:LX/076;

    sget-object v1, LX/074;->CONNECTED:LX/074;

    invoke-virtual {v0, v1}, LX/076;->a(LX/074;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18697
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18698
    :goto_1
    iget-object v0, p0, LX/070;->p:LX/076;

    sget-object v1, LX/074;->DISCONNECTED:LX/074;

    invoke-virtual {v0, v1}, LX/076;->b(LX/074;)V

    .line 18699
    iget-object v0, p0, LX/070;->p:LX/076;

    invoke-virtual {v0}, LX/076;->c()V

    .line 18700
    const-string v0, "DefaultMqttClientCore"

    const-string v1, "thread/exiting"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18701
    return-void

    .line 18702
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/070;->m:LX/07O;

    .line 18703
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 18704
    :try_start_2
    invoke-virtual {v0}, LX/07O;->a()LX/07W;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    .line 18705
    iget-object v1, p0, LX/070;->p:LX/076;

    invoke-virtual {v1, v0}, LX/076;->a(LX/07W;)V

    goto :goto_0

    .line 18706
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 18707
    :catch_0
    move-exception v0

    .line 18708
    invoke-direct {p0, v0}, LX/070;->a(Ljava/lang/Exception;)V

    goto :goto_1

    .line 18709
    :catch_1
    move-exception v0

    .line 18710
    invoke-direct {p0, v0}, LX/070;->a(Ljava/lang/Exception;)V

    goto :goto_1

    .line 18711
    :catch_2
    move-exception v0

    .line 18712
    invoke-direct {p0, v0}, LX/070;->a(Ljava/lang/Exception;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/07X;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 18459
    invoke-virtual {p1}, LX/07X;->a()LX/0B5;

    move-result-object v0

    iget-object v0, v0, LX/0B5;->a:Ljava/lang/String;

    .line 18460
    invoke-static {v0}, LX/0AJ;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 18461
    if-nez v1, :cond_0

    .line 18462
    iget-object v1, p0, LX/070;->p:LX/076;

    const/4 v2, 0x0

    const-string v3, "mqtt_enum_topic"

    const-string v4, "Failed to decode topic %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, LX/05V;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/076;->a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    .line 18463
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 18464
    const-string v0, "DefaultMqttClientCore"

    const-string v1, "connection/cleanup_failure"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18465
    iget-object v0, p0, LX/070;->l:Ljava/net/Socket;

    invoke-static {v0}, LX/07H;->b(Ljava/net/Socket;)V

    .line 18466
    monitor-enter p0

    .line 18467
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/070;->l:Ljava/net/Socket;

    .line 18468
    const/4 v0, 0x0

    iput-object v0, p0, LX/070;->m:LX/07O;

    .line 18469
    const/4 v0, 0x0

    iput-object v0, p0, LX/070;->n:LX/07Q;

    .line 18470
    iget-object v0, p0, LX/070;->p:LX/076;

    sget-object v1, LX/074;->DISCONNECTED:LX/074;

    invoke-virtual {v0, v1}, LX/076;->b(LX/074;)V

    .line 18471
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18472
    iget-object v0, p0, LX/070;->p:LX/076;

    invoke-virtual {v0}, LX/076;->c()V

    .line 18473
    return-void

    .line 18474
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 18370
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 18371
    :try_start_1
    iget-object v0, p0, LX/070;->p:LX/076;

    sget-object v1, LX/074;->CONNECTED:LX/074;

    invoke-virtual {v0, v1}, LX/076;->a(LX/074;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18372
    iget-object v0, p0, LX/070;->p:LX/076;

    const/4 v1, 0x0

    const-string v2, "not_connected"

    invoke-virtual {v0, v1, v2, p2}, LX/076;->a(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 18373
    monitor-exit p0

    .line 18374
    :goto_0
    return-void

    .line 18375
    :cond_0
    const-string v0, "DefaultMqttClientCore"

    const-string v1, "send/puback; id=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18376
    new-instance v0, LX/07R;

    sget-object v1, LX/07S;->PUBACK:LX/07S;

    invoke-direct {v0, v1}, LX/07R;-><init>(LX/07S;)V

    .line 18377
    new-instance v1, LX/0B6;

    invoke-direct {v1, p1}, LX/0B6;-><init>(I)V

    .line 18378
    new-instance v2, LX/0B7;

    invoke-direct {v2, v0, v1}, LX/0B7;-><init>(LX/07R;LX/0B6;)V

    .line 18379
    iget-object v0, p0, LX/070;->n:LX/07Q;

    invoke-static {p0, v0, v2}, LX/070;->a(LX/070;LX/07Q;LX/07W;)V

    .line 18380
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 18381
    :try_start_2
    iget-object v0, p0, LX/070;->p:LX/076;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p2}, LX/076;->a(ZLjava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 18382
    :catch_0
    move-exception v0

    .line 18383
    const-string v1, "DefaultMqttClientCore"

    const-string v2, "exception/puback"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18384
    iget-object v1, p0, LX/070;->p:LX/076;

    invoke-static {v0}, LX/0AX;->getFromWriteException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v2

    sget-object v3, LX/0BJ;->PUBACK:LX/0BJ;

    invoke-virtual {v1, v2, v3, v0}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 18385
    iget-object v1, p0, LX/070;->p:LX/076;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "puback_exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v5, v0, p2}, LX/076;->a(ZLjava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 18386
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

.method public final a(LX/076;LX/075;)V
    .locals 0

    .prologue
    .line 18387
    iput-object p1, p0, LX/070;->p:LX/076;

    .line 18388
    iput-object p2, p0, LX/070;->q:LX/075;

    .line 18389
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;IZLX/079;IZ)V
    .locals 8

    .prologue
    .line 18390
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;-><init>(LX/070;Ljava/lang/String;IZLX/079;IZ)V

    const-string v1, "MqttClient-Network-Thread"

    const v2, -0x1aacef9e

    invoke-static {v0, v1, v2}, LX/00l;->a(Ljava/lang/Runnable;Ljava/lang/String;I)Ljava/lang/Thread;

    move-result-object v0

    .line 18391
    const-string v1, "DefaultMqttClientCore"

    const-string v2, "thread/set_priority; priority=%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/070;->c:LX/06z;

    .line 18392
    iget v6, v5, LX/06z;->k:I

    move v5, v6

    .line 18393
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18394
    iget-object v1, p0, LX/070;->c:LX/06z;

    .line 18395
    iget v2, v1, LX/06z;->k:I

    move v1, v2

    .line 18396
    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 18397
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18398
    monitor-exit p0

    return-void

    .line 18399
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;[BII)V
    .locals 6

    .prologue
    .line 18400
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0AJ;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 18401
    if-eqz v0, :cond_0

    .line 18402
    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object p1

    .line 18403
    :goto_0
    new-instance v0, LX/07R;

    sget-object v1, LX/07S;->PUBLISH:LX/07S;

    invoke-direct {v0, v1, p3}, LX/07R;-><init>(LX/07S;I)V

    .line 18404
    new-instance v1, LX/0B5;

    invoke-direct {v1, p1, p4}, LX/0B5;-><init>(Ljava/lang/String;I)V

    .line 18405
    new-instance v2, LX/07X;

    invoke-direct {v2, v0, v1, p2}, LX/07X;-><init>(LX/07R;LX/0B5;[B)V

    .line 18406
    iget-object v0, p0, LX/070;->n:LX/07Q;

    invoke-static {p0, v0, v2}, LX/070;->a(LX/070;LX/07Q;LX/07W;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18407
    monitor-exit p0

    return-void

    .line 18408
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/070;->p:LX/076;

    const/4 v1, 0x0

    const-string v2, "mqtt_enum_topic"

    const-string v3, "Failed to encode topic %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v3, v4}, LX/05V;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/076;->a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 18409
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/List;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 18410
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/07R;

    sget-object v1, LX/07S;->SUBSCRIBE:LX/07S;

    invoke-direct {v0, v1}, LX/07R;-><init>(LX/07S;)V

    .line 18411
    new-instance v1, LX/0B6;

    invoke-direct {v1, p2}, LX/0B6;-><init>(I)V

    .line 18412
    new-instance v2, LX/0I1;

    invoke-direct {v2, p1}, LX/0I1;-><init>(Ljava/util/List;)V

    .line 18413
    new-instance v3, LX/0I0;

    invoke-direct {v3, v0, v1, v2}, LX/0I0;-><init>(LX/07R;LX/0B6;LX/0I1;)V

    .line 18414
    iget-object v0, p0, LX/070;->n:LX/07Q;

    invoke-static {p0, v0, v3}, LX/070;->a(LX/070;LX/07Q;LX/07W;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18415
    monitor-exit p0

    return-void

    .line 18416
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 18417
    const/4 v1, 0x0

    .line 18418
    monitor-enter p0

    .line 18419
    :try_start_0
    iget-boolean v2, p0, LX/070;->o:Z

    if-nez v2, :cond_1

    .line 18420
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/070;->o:Z

    .line 18421
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18422
    if-eqz v0, :cond_0

    .line 18423
    iget-object v0, p0, LX/070;->p:LX/076;

    sget-object v1, LX/0AX;->ABORTED_PREEMPTIVE_RECONNECT:LX/0AX;

    sget-object v2, LX/0BJ;->NETWORK_THREAD_LOOP:LX/0BJ;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 18424
    :cond_0
    return-void

    .line 18425
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final declared-synchronized b(Ljava/util/List;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 18426
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/07R;

    sget-object v1, LX/07S;->UNSUBSCRIBE:LX/07S;

    invoke-direct {v0, v1}, LX/07R;-><init>(LX/07S;)V

    .line 18427
    new-instance v1, LX/0B6;

    invoke-direct {v1, p2}, LX/0B6;-><init>(I)V

    .line 18428
    new-instance v2, LX/0I4;

    invoke-direct {v2, p1}, LX/0I4;-><init>(Ljava/util/List;)V

    .line 18429
    new-instance v3, LX/0I3;

    invoke-direct {v3, v0, v1, v2}, LX/0I3;-><init>(LX/07R;LX/0B6;LX/0I4;)V

    .line 18430
    iget-object v0, p0, LX/070;->n:LX/07Q;

    invoke-static {p0, v0, v3}, LX/070;->a(LX/070;LX/07Q;LX/07W;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18431
    monitor-exit p0

    return-void

    .line 18432
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 4

    .prologue
    .line 18433
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/07R;

    sget-object v1, LX/07S;->PINGREQ:LX/07S;

    invoke-direct {v0, v1}, LX/07R;-><init>(LX/07S;)V

    .line 18434
    new-instance v1, LX/07W;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, LX/07W;-><init>(LX/07R;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 18435
    iget-object v0, p0, LX/070;->n:LX/07Q;

    invoke-static {p0, v0, v1}, LX/070;->a(LX/070;LX/07Q;LX/07W;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18436
    monitor-exit p0

    return-void

    .line 18437
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 4

    .prologue
    .line 18454
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/07R;

    sget-object v1, LX/07S;->PINGRESP:LX/07S;

    invoke-direct {v0, v1}, LX/07R;-><init>(LX/07S;)V

    .line 18455
    new-instance v1, LX/07W;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, LX/07W;-><init>(LX/07R;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 18456
    iget-object v0, p0, LX/070;->n:LX/07Q;

    invoke-static {p0, v0, v1}, LX/070;->a(LX/070;LX/07Q;LX/07W;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18457
    monitor-exit p0

    return-void

    .line 18458
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18438
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/070;->l:Ljava/net/Socket;

    if-eqz v0, :cond_0

    .line 18439
    iget-object v0, p0, LX/070;->l:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    move-result-object v0

    .line 18440
    if-eqz v0, :cond_0

    .line 18441
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 18442
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    const-string v0, "N/A"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 18443
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 18444
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18445
    iget-object v1, p0, LX/070;->k:Ljava/net/InetAddress;

    if-eqz v1, :cond_0

    .line 18446
    const-string v1, "Remote:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/070;->k:Ljava/net/InetAddress;

    invoke-virtual {v2}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18447
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 18448
    :cond_0
    iget-object v1, p0, LX/070;->j:Ljava/net/InetAddress;

    if-eqz v1, :cond_1

    .line 18449
    const-string v1, "Local:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/070;->j:Ljava/net/InetAddress;

    invoke-virtual {v2}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18450
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 18451
    :cond_1
    iget-object v1, p0, LX/070;->f:LX/06B;

    invoke-virtual {v1}, LX/06B;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18452
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()B
    .locals 1

    .prologue
    .line 18453
    const/4 v0, 0x3

    return v0
.end method
