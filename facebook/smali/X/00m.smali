.class public final LX/00m;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static volatile a:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Thread;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2921
    new-instance v0, LX/03O;

    invoke-direct {v0}, LX/03O;-><init>()V

    invoke-static {v0}, LX/00k;->a(LX/00j;)V

    .line 2922
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2920
    return-void
.end method

.method public static c()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x40

    .line 2905
    invoke-static {v4, v5}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2906
    :cond_0
    :goto_0
    return-void

    .line 2907
    :cond_1
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    .line 2908
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 2909
    const-string v2, "thread_name"

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v5, v2, v3, v0}, LX/018;->a(JLjava/lang/String;Ljava/lang/String;I)V

    .line 2910
    sget-object v2, LX/00m;->a:Ljava/util/WeakHashMap;

    if-eqz v2, :cond_0

    .line 2911
    sget-object v2, LX/00m;->a:Ljava/util/WeakHashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static k()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x40

    .line 2912
    invoke-static {v6, v7}, LX/00k;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2913
    :goto_0
    return-void

    .line 2914
    :cond_0
    const-string v0, "TraceExistingThreadsMetadata"

    invoke-static {v6, v7, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 2915
    :try_start_0
    sget-object v0, LX/00m;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2916
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    .line 2917
    const-wide/16 v4, 0x40

    const-string v3, "thread_name"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v5, v3, v1, v0}, LX/018;->a(JLjava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2918
    :catchall_0
    move-exception v0

    invoke-static {v6, v7}, LX/018;->a(J)V

    throw v0

    :cond_1
    invoke-static {v6, v7}, LX/018;->a(J)V

    goto :goto_0
.end method
