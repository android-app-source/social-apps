.class public LX/05i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/telephony/TelephonyManager;

.field private final d:LX/059;

.field private final e:LX/05b;

.field public final f:LX/05j;

.field private final g:Ljava/lang/String;

.field private final h:LX/01o;

.field private final i:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field private final j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/06a;",
            "Ljava/util/concurrent/atomic/AtomicLong;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/06r;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/atomic/AtomicLong;",
            ">;"
        }
    .end annotation
.end field

.field public volatile n:LX/06f;

.field private volatile o:Ljava/lang/String;

.field public volatile p:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/telephony/TelephonyManager;LX/059;LX/05b;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;LX/01o;LX/05N;)V
    .locals 2
    .param p8    # LX/05N;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/telephony/TelephonyManager;",
            "LX/059;",
            "LX/05b;",
            "Lcom/facebook/rti/common/time/MonotonicClock;",
            "Lcom/facebook/rti/common/time/Clock;",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16694
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16695
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LX/05i;->m:Ljava/util/concurrent/ConcurrentMap;

    .line 16696
    const-string v0, ""

    iput-object v0, p0, LX/05i;->o:Ljava/lang/String;

    .line 16697
    const-string v0, ""

    iput-object v0, p0, LX/05i;->p:Ljava/lang/String;

    .line 16698
    iput-object p1, p0, LX/05i;->a:Landroid/content/Context;

    .line 16699
    iput-object p2, p0, LX/05i;->b:Ljava/lang/String;

    .line 16700
    iput-object p3, p0, LX/05i;->c:Landroid/telephony/TelephonyManager;

    .line 16701
    iput-object p4, p0, LX/05i;->d:LX/059;

    .line 16702
    iput-object p5, p0, LX/05i;->e:LX/05b;

    .line 16703
    new-instance v0, LX/05j;

    invoke-direct {v0, p1, p6, p7}, LX/05j;-><init>(Landroid/content/Context;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;LX/01o;)V

    iput-object v0, p0, LX/05i;->f:LX/05j;

    .line 16704
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/05i;->g:Ljava/lang/String;

    .line 16705
    iput-object p7, p0, LX/05i;->h:LX/01o;

    .line 16706
    iput-object p6, p0, LX/05i;->i:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 16707
    iput-object p8, p0, LX/05i;->l:LX/05N;

    .line 16708
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/05i;->j:Ljava/util/HashMap;

    .line 16709
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/05i;->k:Ljava/util/HashMap;

    .line 16710
    return-void
.end method

.method private static a(Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 16711
    invoke-interface {p0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v1

    .line 16712
    const/4 v0, 0x1

    .line 16713
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 16714
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 16715
    if-eqz v1, :cond_0

    .line 16716
    const/4 v2, 0x0

    .line 16717
    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "|"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v2

    .line 16718
    goto :goto_0

    .line 16719
    :cond_0
    const-string v2, ";"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v2, v1

    goto :goto_1

    .line 16720
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a(LX/05i;LX/06a;)Ljava/util/concurrent/atomic/AtomicLong;
    .locals 2

    .prologue
    .line 16721
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/05i;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16722
    iget-object v0, p0, LX/05i;->j:Ljava/util/HashMap;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16723
    :cond_0
    iget-object v0, p0, LX/05i;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 16724
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private d(J)LX/0BF;
    .locals 7

    .prologue
    .line 16725
    const-class v0, LX/0BF;

    invoke-virtual {p0, v0}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/0BF;

    .line 16726
    sget-object v1, LX/0BG;->MqttDurationMs:LX/0BG;

    invoke-virtual {v0, v1}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 16727
    sget-object v1, LX/0BG;->NetworkDurationMs:LX/0BG;

    invoke-virtual {v0, v1}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v2, p0, LX/05i;->d:LX/059;

    invoke-virtual {v2}, LX/059;->j()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 16728
    sget-object v1, LX/0BG;->NetworkTotalDurationMs:LX/0BG;

    invoke-virtual {v0, v1}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v2, p0, LX/05i;->d:LX/059;

    invoke-virtual {v2}, LX/059;->k()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 16729
    iget-object v1, p0, LX/05i;->i:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    sget-object v1, LX/06a;->ServiceCreatedTimestamp:LX/06a;

    invoke-static {p0, v1}, LX/05i;->a(LX/05i;LX/06a;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 16730
    sget-object v1, LX/0BG;->ServiceDurationMs:LX/0BG;

    invoke-virtual {v0, v1}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 16731
    return-object v0
.end method

.method private static g(LX/05i;)LX/06s;
    .locals 1

    .prologue
    .line 16732
    const-class v0, LX/06s;

    invoke-virtual {p0, v0}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06s;

    return-object v0
.end method

.method private static h(LX/05i;)LX/06u;
    .locals 1

    .prologue
    .line 16733
    const-class v0, LX/06u;

    invoke-virtual {p0, v0}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06u;

    return-object v0
.end method

.method private static k(LX/05i;)LX/06p;
    .locals 1

    .prologue
    .line 16734
    const-class v0, LX/06p;

    invoke-virtual {p0, v0}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06p;

    return-object v0
.end method

.method private static l(LX/05i;)LX/0Hd;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 16667
    const-class v0, LX/0Hd;

    invoke-virtual {p0, v0}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/0Hd;

    .line 16668
    sget-object v1, LX/0Hc;->ServiceName:LX/0Hc;

    iget-object v2, p0, LX/05i;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16669
    sget-object v1, LX/0Hc;->ClientCoreName:LX/0Hc;

    iget-object v2, p0, LX/05i;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16670
    sget-object v1, LX/0Hc;->NotificationStoreName:LX/0Hc;

    iget-object v2, p0, LX/05i;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16671
    sget-object v1, LX/0Hc;->AndroidId:LX/0Hc;

    iget-object v2, p0, LX/05i;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16672
    iget-object v1, p0, LX/05i;->a:Landroid/content/Context;

    sget-object v2, LX/01p;->b:LX/01q;

    invoke-static {v1, v2}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 16673
    sget-object v1, LX/0Hc;->YearClass:LX/0Hc;

    const-string v3, "year_class"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16674
    iget-object v1, p0, LX/05i;->a:Landroid/content/Context;

    sget-object v3, LX/01p;->m:LX/01q;

    invoke-static {v1, v3}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 16675
    sget-object v3, LX/0Hc;->MqttGKs:LX/0Hc;

    invoke-static {v1}, LX/05i;->a(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16676
    iget-object v1, p0, LX/05i;->a:Landroid/content/Context;

    sget-object v3, LX/01p;->f:LX/01q;

    invoke-static {v1, v3}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 16677
    sget-object v3, LX/0Hc;->MqttFlags:LX/0Hc;

    invoke-static {v1}, LX/05i;->a(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16678
    iget-object v1, p0, LX/05i;->l:LX/05N;

    if-eqz v1, :cond_0

    .line 16679
    sget-object v3, LX/0Hc;->AppState:LX/0Hc;

    iget-object v1, p0, LX/05i;->l:LX/05N;

    invoke-interface {v1}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "fg"

    :goto_0
    invoke-virtual {v0, v3, v1}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16680
    :cond_0
    sget-object v3, LX/0Hc;->ScreenState:LX/0Hc;

    iget-object v1, p0, LX/05i;->e:LX/05b;

    invoke-virtual {v1}, LX/05b;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "1"

    :goto_1
    invoke-virtual {v0, v3, v1}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16681
    sget-object v1, LX/0Hc;->Country:LX/0Hc;

    iget-object v3, p0, LX/05i;->c:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/05V;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16682
    sget-object v1, LX/0Hc;->NetworkType:LX/0Hc;

    iget-object v3, p0, LX/05i;->d:LX/059;

    invoke-virtual {v3}, LX/059;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/05V;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16683
    sget-object v1, LX/0Hc;->NetworkSubtype:LX/0Hc;

    iget-object v3, p0, LX/05i;->d:LX/059;

    .line 16684
    const-string v5, "none"

    .line 16685
    invoke-virtual {v3}, LX/059;->d()Landroid/net/NetworkInfo;

    move-result-object v6

    .line 16686
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 16687
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v5

    .line 16688
    :cond_1
    move-object v3, v5

    .line 16689
    invoke-static {v3}, LX/05V;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16690
    sget-object v1, LX/0Hc;->IsEmployee:LX/0Hc;

    const-string v3, "is_employee"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 16691
    return-object v0

    .line 16692
    :cond_2
    const-string v1, "bg"

    goto :goto_0

    .line 16693
    :cond_3
    const-string v1, "0"

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/Class;)LX/06r;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/06r;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 16735
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 16736
    iget-object v0, p0, LX/05i;->k:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16737
    const-class v0, LX/06s;

    if-ne p1, v0, :cond_1

    .line 16738
    new-instance v0, LX/06s;

    iget-object v2, p0, LX/05i;->a:Landroid/content/Context;

    iget-object v3, p0, LX/05i;->b:Ljava/lang/String;

    iget-object v4, p0, LX/05i;->h:LX/01o;

    iget-object v5, p0, LX/05i;->i:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-direct {v0, v2, v3, v4, v5}, LX/06s;-><init>(Landroid/content/Context;Ljava/lang/String;LX/01o;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;)V

    .line 16739
    :goto_0
    iget-object v2, p0, LX/05i;->k:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16740
    :cond_0
    iget-object v0, p0, LX/05i;->k:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/06r;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 16741
    :cond_1
    :try_start_1
    const-class v0, LX/06u;

    if-ne p1, v0, :cond_2

    .line 16742
    new-instance v0, LX/06u;

    iget-object v2, p0, LX/05i;->a:Landroid/content/Context;

    iget-object v3, p0, LX/05i;->b:Ljava/lang/String;

    iget-object v4, p0, LX/05i;->h:LX/01o;

    iget-object v5, p0, LX/05i;->i:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-direct {v0, v2, v3, v4, v5}, LX/06u;-><init>(Landroid/content/Context;Ljava/lang/String;LX/01o;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 16743
    :catch_0
    move-exception v0

    .line 16744
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Incorrect stat category used:"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 16745
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 16746
    :cond_2
    :try_start_3
    const-class v0, LX/06v;

    if-ne p1, v0, :cond_3

    .line 16747
    new-instance v0, LX/06v;

    iget-object v2, p0, LX/05i;->a:Landroid/content/Context;

    iget-object v3, p0, LX/05i;->b:Ljava/lang/String;

    iget-object v4, p0, LX/05i;->h:LX/01o;

    iget-object v5, p0, LX/05i;->i:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-direct {v0, v2, v3, v4, v5}, LX/06v;-><init>(Landroid/content/Context;Ljava/lang/String;LX/01o;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;)V

    goto :goto_0

    .line 16748
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/06r;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final a()LX/0Ha;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 16666
    new-instance v0, LX/0Ha;

    invoke-static {p0}, LX/05i;->l(LX/05i;)LX/0Hd;

    move-result-object v1

    invoke-static {p0}, LX/05i;->k(LX/05i;)LX/06p;

    move-result-object v3

    iget-object v4, p0, LX/05i;->f:LX/05j;

    invoke-virtual {v4}, LX/05j;->a()LX/0Hb;

    move-result-object v5

    invoke-static {p0}, LX/05i;->g(LX/05i;)LX/06s;

    move-result-object v6

    invoke-static {p0}, LX/05i;->h(LX/05i;)LX/06u;

    move-result-object v7

    const/4 v8, 0x0

    move-object v4, v2

    invoke-direct/range {v0 .. v8}, LX/0Ha;-><init>(LX/0Hd;LX/0BF;LX/06p;LX/0BB;LX/0Hb;LX/06s;LX/06u;Z)V

    return-object v0
.end method

.method public final a(J)LX/0Ha;
    .locals 11

    .prologue
    .line 16663
    new-instance v0, LX/0Ha;

    invoke-static {p0}, LX/05i;->l(LX/05i;)LX/0Hd;

    move-result-object v1

    invoke-direct {p0, p1, p2}, LX/05i;->d(J)LX/0BF;

    move-result-object v2

    invoke-static {p0}, LX/05i;->k(LX/05i;)LX/06p;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, LX/05i;->f:LX/05j;

    invoke-virtual {v5}, LX/05j;->a()LX/0Hb;

    move-result-object v5

    invoke-static {p0}, LX/05i;->g(LX/05i;)LX/06s;

    move-result-object v6

    invoke-static {p0}, LX/05i;->h(LX/05i;)LX/06u;

    move-result-object v7

    .line 16664
    const-class v8, LX/06v;

    invoke-virtual {p0, v8}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v8

    check-cast v8, LX/06v;

    move-object v8, v8

    .line 16665
    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, LX/0Ha;-><init>(LX/0Hd;LX/0BF;LX/06p;LX/0BB;LX/0Hb;LX/06s;LX/06u;LX/06v;ZZ)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16610
    iput-object p1, p0, LX/05i;->o:Ljava/lang/String;

    .line 16611
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 10

    .prologue
    .line 16630
    iget-object v0, p0, LX/05i;->l:LX/05N;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    move v1, v0

    .line 16631
    :goto_0
    sget-object v0, LX/07L;->a:LX/07L;

    move-object v0, v0

    .line 16632
    invoke-virtual {v0}, LX/07L;->c()Z

    move-result v5

    .line 16633
    sget-object v0, LX/07L;->a:LX/07L;

    move-object v0, v0

    .line 16634
    iget-object v2, v0, LX/07L;->c:Ljava/lang/String;

    move-object v0, v2

    .line 16635
    if-eqz v0, :cond_2

    if-nez p4, :cond_0

    sget-object v2, LX/07S;->PINGREQ:LX/07S;

    invoke-virtual {v2}, LX/07S;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    if-eqz p4, :cond_2

    sget-object v2, LX/07S;->PINGRESP:LX/07S;

    invoke-virtual {v2}, LX/07S;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 16636
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 16637
    :cond_2
    if-eqz v1, :cond_4

    .line 16638
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_FG"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 16639
    :goto_1
    if-eqz v5, :cond_6

    .line 16640
    if-eqz v1, :cond_5

    .line 16641
    const-class v0, LX/06u;

    invoke-virtual {p0, v0}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06u;

    const-wide/16 v2, 0x1

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "tc"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "fg"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "rw"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object p3, v6, v7

    invoke-virtual {v0, v2, v3, v6}, LX/06t;->a(J[Ljava/lang/String;)LX/06t;

    .line 16642
    :goto_2
    sget-object v0, LX/07M;->a:LX/07M;

    move-object v0, v0

    .line 16643
    invoke-virtual {v0, v4, p2}, LX/07M;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 16644
    :goto_3
    if-eqz v1, :cond_8

    const-string v0, "fg"

    move-object v2, v0

    .line 16645
    :goto_4
    invoke-static {p2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    move-object v3, v4

    .line 16646
    :goto_5
    const-class v0, LX/06v;

    invoke-virtual {p0, v0}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06v;

    const-wide/16 v6, 0x1

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v3, 0x1

    aput-object v2, v8, v3

    invoke-virtual {v0, v6, v7, v8}, LX/06t;->a(J[Ljava/lang/String;)LX/06t;

    .line 16647
    sget-object v0, LX/07M;->a:LX/07M;

    move-object v0, v0

    .line 16648
    invoke-virtual {v0, v4, p2}, LX/07M;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 16649
    sget-object v0, LX/07L;->a:LX/07L;

    move-object v0, v0

    .line 16650
    invoke-virtual {v0}, LX/07L;->b()V

    .line 16651
    const-string v0, "MqttHealthStatsHelper"

    const-string v2, "logged mqtt traffic, isRadioWakeup:%b, type:%s, topic:%s, isMqttForeground:%b, network:%s"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v6

    const/4 v5, 0x1

    aput-object v4, v3, v5

    const/4 v4, 0x2

    aput-object p2, v3, v4

    const/4 v4, 0x3

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x4

    aput-object p3, v3, v1

    invoke-static {v0, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16652
    return-void

    .line 16653
    :cond_3
    iget-object v0, p0, LX/05i;->l:LX/05N;

    invoke-interface {v0}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v1, v0

    goto/16 :goto_0

    .line 16654
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_BG"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 16655
    :cond_5
    const-class v0, LX/06u;

    invoke-virtual {p0, v0}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06u;

    const-wide/16 v2, 0x1

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "tc"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "bg"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "rw"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object p3, v6, v7

    invoke-virtual {v0, v2, v3, v6}, LX/06t;->a(J[Ljava/lang/String;)LX/06t;

    goto/16 :goto_2

    .line 16656
    :cond_6
    if-eqz v1, :cond_7

    .line 16657
    const-class v0, LX/06u;

    invoke-virtual {p0, v0}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06u;

    const-wide/16 v2, 0x1

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "tc"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "fg"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "nw"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object p3, v6, v7

    invoke-virtual {v0, v2, v3, v6}, LX/06t;->a(J[Ljava/lang/String;)LX/06t;

    goto/16 :goto_3

    .line 16658
    :cond_7
    const-class v0, LX/06u;

    invoke-virtual {p0, v0}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06u;

    const-wide/16 v2, 0x1

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "tc"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "bg"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "nw"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object p3, v6, v7

    invoke-virtual {v0, v2, v3, v6}, LX/06t;->a(J[Ljava/lang/String;)LX/06t;

    goto/16 :goto_3

    .line 16659
    :cond_8
    const-string v0, "bg"

    move-object v2, v0

    goto/16 :goto_4

    .line 16660
    :cond_9
    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 16661
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_5

    :cond_a
    move-object v3, p2

    .line 16662
    goto/16 :goto_5
.end method

.method public final b(J)LX/0Ha;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 16627
    new-instance v0, LX/0Ha;

    invoke-static {p0}, LX/05i;->l(LX/05i;)LX/0Hd;

    move-result-object v1

    invoke-direct {p0, p1, p2}, LX/05i;->d(J)LX/0BF;

    move-result-object v2

    .line 16628
    const-class v4, LX/0BB;

    invoke-virtual {p0, v4}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v4

    check-cast v4, LX/0BB;

    move-object v4, v4

    .line 16629
    const/4 v8, 0x1

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v8}, LX/0Ha;-><init>(LX/0Hd;LX/0BF;LX/06p;LX/0BB;LX/0Hb;LX/06s;LX/06u;Z)V

    return-object v0
.end method

.method public final c(J)V
    .locals 7

    .prologue
    .line 16621
    const-class v0, LX/06p;

    invoke-virtual {p0, v0}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06p;

    .line 16622
    sget-object v1, LX/06w;->CountSuccessfulConnection:LX/06w;

    invoke-virtual {v0, v1}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 16623
    sget-object v1, LX/06w;->ConnectingMs:LX/06w;

    invoke-virtual {v0, v1}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 16624
    iget-object v0, p0, LX/05i;->f:LX/05j;

    .line 16625
    iget-object v3, v0, LX/05j;->i:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v4, v0, LX/05j;->b:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v4}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 16626
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 16619
    iget-object v0, p0, LX/05i;->f:LX/05j;

    invoke-virtual {v0}, LX/05j;->e()V

    .line 16620
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 16617
    iget-object v0, p0, LX/05i;->f:LX/05j;

    invoke-virtual {v0}, LX/05j;->b()V

    .line 16618
    return-void
.end method

.method public final f()V
    .locals 7

    .prologue
    .line 16612
    iget-object v0, p0, LX/05i;->f:LX/05j;

    const-wide/16 v5, 0x0

    .line 16613
    iget-object v1, v0, LX/05j;->b:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v1

    .line 16614
    iget-object v3, v0, LX/05j;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3, v5, v6, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    .line 16615
    iget-object v3, v0, LX/05j;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3, v5, v6, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    .line 16616
    return-void
.end method
