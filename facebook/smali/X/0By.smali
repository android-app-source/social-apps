.class public LX/0By;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Bw;


# instance fields
.field public final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/concurrent/atomic/AtomicLong;

.field public final d:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/16 v1, 0x8

    .line 27021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27022
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, LX/0By;->a:Landroid/util/SparseArray;

    .line 27023
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, LX/0By;->b:Landroid/util/SparseArray;

    .line 27024
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/0By;->c:Ljava/util/concurrent/atomic/AtomicLong;

    .line 27025
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/0By;->d:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public static a(Landroid/util/SparseArray;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 27026
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 27027
    const-wide/16 v2, 0x0

    .line 27028
    monitor-enter p0

    .line 27029
    :try_start_0
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v6

    .line 27030
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_0

    .line 27031
    invoke-virtual {p0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v8, v4, v8

    add-long/2addr v2, v8

    .line 27032
    invoke-virtual {p0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p0, v0, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 27033
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 27034
    :cond_0
    monitor-exit p0

    .line 27035
    return-wide v2

    .line 27036
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(ILandroid/util/SparseArray;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    .line 27037
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 27038
    monitor-enter p1

    .line 27039
    const-wide/16 v0, -0x1

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, p0, v0}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 27040
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, p0, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 27041
    :cond_0
    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(ILandroid/util/SparseArray;)J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    .line 27042
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 27043
    const-wide/16 v2, 0x0

    .line 27044
    monitor-enter p1

    .line 27045
    const-wide/16 v0, -0x1

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, p0, v0}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 27046
    cmp-long v6, v0, v6

    if-eqz v6, :cond_0

    .line 27047
    sub-long v0, v4, v0

    .line 27048
    invoke-virtual {p1, p0}, Landroid/util/SparseArray;->delete(I)V

    .line 27049
    :goto_0
    monitor-exit p1

    .line 27050
    return-wide v0

    .line 27051
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 27052
    iget-object v0, p0, LX/0By;->a:Landroid/util/SparseArray;

    invoke-static {p1, v0}, LX/0By;->a(ILandroid/util/SparseArray;)V

    .line 27053
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 27054
    iget-object v0, p0, LX/0By;->c:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v1, p0, LX/0By;->a:Landroid/util/SparseArray;

    invoke-static {p1, v1}, LX/0By;->b(ILandroid/util/SparseArray;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 27055
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 27056
    iget-object v0, p0, LX/0By;->b:Landroid/util/SparseArray;

    invoke-static {p1, v0}, LX/0By;->a(ILandroid/util/SparseArray;)V

    .line 27057
    return-void
.end method

.method public final d(I)V
    .locals 4

    .prologue
    .line 27058
    iget-object v0, p0, LX/0By;->d:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v1, p0, LX/0By;->b:Landroid/util/SparseArray;

    invoke-static {p1, v1}, LX/0By;->b(ILandroid/util/SparseArray;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 27059
    return-void
.end method
