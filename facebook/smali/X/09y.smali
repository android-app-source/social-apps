.class public LX/09y;
.super LX/0Vx;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/09y;


# direct methods
.method public constructor <init>(LX/2WA;)V
    .locals 0
    .param p1    # LX/2WA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 23158
    invoke-direct {p0, p1}, LX/0Vx;-><init>(LX/2WA;)V

    .line 23159
    return-void
.end method

.method public static a(LX/0QB;)LX/09y;
    .locals 3

    .prologue
    .line 23160
    sget-object v0, LX/09y;->b:LX/09y;

    if-nez v0, :cond_1

    .line 23161
    const-class v1, LX/09y;

    monitor-enter v1

    .line 23162
    :try_start_0
    sget-object v0, LX/09y;->b:LX/09y;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 23163
    if-eqz v2, :cond_0

    .line 23164
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/09y;->b(LX/0QB;)LX/09y;

    move-result-object v0

    sput-object v0, LX/09y;->b:LX/09y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23165
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 23166
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 23167
    :cond_1
    sget-object v0, LX/09y;->b:LX/09y;

    return-object v0

    .line 23168
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 23169
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/09y;
    .locals 2

    .prologue
    .line 23156
    new-instance v1, LX/09y;

    invoke-static {p0}, LX/0Ws;->a(LX/0QB;)LX/2WA;

    move-result-object v0

    check-cast v0, LX/2WA;

    invoke-direct {v1, v0}, LX/09y;-><init>(LX/2WA;)V

    .line 23157
    return-object v1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23155
    const-string v0, "video_cachecounters"

    return-object v0
.end method
