.class public LX/01a;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static NO_LIMIT:I = 0x0


# instance fields
.field private mTrace:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/03u;",
            ">;"
        }
    .end annotation
.end field

.field public final mTraceCountLimit:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4898
    const/4 v0, 0x0

    sput v0, LX/01a;->NO_LIMIT:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 4899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4900
    iput p1, p0, LX/01a;->mTraceCountLimit:I

    .line 4901
    invoke-virtual {p0}, LX/01a;->clear()V

    .line 4902
    return-void
.end method


# virtual methods
.method public append(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 4903
    monitor-enter p0

    .line 4904
    :try_start_0
    iget v0, p0, LX/01a;->mTraceCountLimit:I

    sget v1, LX/01a;->NO_LIMIT:I

    if-le v0, v1, :cond_0

    .line 4905
    iget-object v0, p0, LX/01a;->mTrace:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    iget v1, p0, LX/01a;->mTraceCountLimit:I

    if-ne v0, v1, :cond_0

    .line 4906
    iget-object v0, p0, LX/01a;->mTrace:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 4907
    :cond_0
    iget-object v0, p0, LX/01a;->mTrace:Ljava/util/Queue;

    new-instance v1, LX/03u;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-direct {v1, p1, v2, v3}, LX/03u;-><init>(Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 4908
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public varargs append(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 4909
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/01a;->append(Ljava/lang/String;)V

    .line 4910
    return-void
.end method

.method public declared-synchronized clear()V
    .locals 1

    .prologue
    .line 4911
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/01a;->mTrace:Ljava/util/Queue;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4912
    monitor-exit p0

    return-void

    .line 4913
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4914
    monitor-enter p0

    :try_start_0
    sget v0, LX/01a;->NO_LIMIT:I

    invoke-virtual {p0, v0}, LX/01a;->toString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized toString(I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 4915
    monitor-enter p0

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 4916
    sget v1, LX/01a;->NO_LIMIT:I

    if-gt p1, v1, :cond_1

    move v2, v0

    .line 4917
    :goto_0
    iget-object v1, p0, LX/01a;->mTrace:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03u;

    .line 4918
    if-lt v1, v2, :cond_0

    .line 4919
    invoke-virtual {v0}, LX/03u;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v5, 0xa

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 4920
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 4921
    goto :goto_1

    .line 4922
    :cond_1
    iget-object v1, p0, LX/01a;->mTrace:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    sub-int/2addr v1, p1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v2, v1

    goto :goto_0

    .line 4923
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 4924
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
