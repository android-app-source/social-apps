.class public LX/05h;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x3
.end annotation


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/059;

.field private final g:LX/05O;

.field private final h:J

.field private final i:J

.field private final j:J

.field private final k:LX/05A;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 16468
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, LX/05h;->a:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/059;LX/05A;LX/05O;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 16567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16568
    iput-object p1, p0, LX/05h;->b:Landroid/content/Context;

    .line 16569
    iput-object p2, p0, LX/05h;->c:Ljava/lang/String;

    .line 16570
    iput-object p4, p0, LX/05h;->f:LX/059;

    .line 16571
    iput-object p5, p0, LX/05h;->k:LX/05A;

    .line 16572
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/05h;->d:Ljava/lang/String;

    .line 16573
    iput-object p3, p0, LX/05h;->e:Ljava/lang/String;

    .line 16574
    iput-object p6, p0, LX/05h;->g:LX/05O;

    .line 16575
    invoke-virtual {p7}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/05h;->h:J

    .line 16576
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/05h;->i:J

    .line 16577
    sget-object v0, LX/05h;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iput-wide v0, p0, LX/05h;->j:J

    .line 16578
    return-void
.end method

.method private static a(Ljava/util/Map;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 16579
    const-string v0, "mqtt_session_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16580
    return-void
.end method

.method public static a(Ljava/util/Map;Landroid/net/NetworkInfo;)V
    .locals 4
    .param p1    # Landroid/net/NetworkInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/net/NetworkInfo;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 16581
    if-eqz p1, :cond_0

    .line 16582
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    .line 16583
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v1

    .line 16584
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    move-result-object v0

    .line 16585
    :goto_0
    invoke-static {v2}, LX/05V;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 16586
    invoke-static {v1}, LX/05V;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 16587
    invoke-static {v0}, LX/05V;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 16588
    const-string v3, "network_type"

    invoke-interface {p0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16589
    const-string v2, "network_subtype"

    invoke-interface {p0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16590
    const-string v1, "network_extra_info"

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16591
    return-void

    :cond_0
    move-object v1, v0

    move-object v2, v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 16605
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v2, v3, :cond_2

    .line 16606
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    .line 16607
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 16608
    goto :goto_0

    .line 16609
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private static b(LX/05h;Ljava/lang/String;JJJLandroid/net/NetworkInfo;)V
    .locals 4

    .prologue
    .line 16592
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "timespan_ms"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 16593
    invoke-static {v0, p4, p5}, LX/05h;->a(Ljava/util/Map;J)V

    .line 16594
    invoke-static {v0, p6, p7}, LX/05h;->b(Ljava/util/Map;J)V

    .line 16595
    invoke-static {v0, p8}, LX/05h;->a(Ljava/util/Map;Landroid/net/NetworkInfo;)V

    .line 16596
    invoke-virtual {p0, p1, v0}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 16597
    return-void
.end method

.method private static b(LX/05h;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16598
    iget-object v0, p0, LX/05h;->k:LX/05A;

    invoke-virtual {v0}, LX/05A;->a()LX/05B;

    move-result-object v1

    .line 16599
    invoke-virtual {v1}, LX/05B;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 16600
    :cond_0
    :goto_0
    return-void

    .line 16601
    :cond_1
    invoke-virtual {v1}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/05C;

    iget-boolean v0, v0, LX/05C;->a:Z

    if-nez v0, :cond_2

    invoke-virtual {v1}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/05C;

    iget-boolean v0, v0, LX/05C;->b:Z

    if-eqz v0, :cond_3

    .line 16602
    :cond_2
    const-string v0, "bat"

    const-string v1, "crg"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 16603
    :cond_3
    invoke-virtual {v1}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/05C;

    iget-object v0, v0, LX/05C;->c:LX/05B;

    invoke-virtual {v0}, LX/05B;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16604
    const-string v2, "bat"

    invoke-virtual {v1}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/05C;

    iget-object v0, v0, LX/05C;->c:LX/05B;

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static b(Ljava/util/Map;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 16554
    const-string v0, "network_session_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16555
    return-void
.end method


# virtual methods
.method public final a(JILjava/lang/String;LX/05B;JJLandroid/net/NetworkInfo;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/lang/String;",
            "LX/05B",
            "<",
            "Ljava/lang/Throwable;",
            ">;JJ",
            "Landroid/net/NetworkInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 16556
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "timespan_ms"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "port"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "he_state"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    aput-object p4, v0, v1

    invoke-static {v0}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 16557
    invoke-virtual {p5}, LX/05B;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16558
    invoke-virtual {p5}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 16559
    invoke-virtual {p5}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 16560
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Caused by: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p5}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 16561
    :goto_0
    const-string v1, "error_message"

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16562
    :cond_0
    invoke-static {v2, p6, p7}, LX/05h;->a(Ljava/util/Map;J)V

    .line 16563
    invoke-static {v2, p8, p9}, LX/05h;->b(Ljava/util/Map;J)V

    .line 16564
    invoke-static {v2, p10}, LX/05h;->a(Ljava/util/Map;Landroid/net/NetworkInfo;)V

    .line 16565
    const-string v0, "mqtt_socket_connect"

    invoke-virtual {p0, v0, v2}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 16566
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(JJJLandroid/net/NetworkInfo;)V
    .locals 9

    .prologue
    .line 16552
    const-string v1, "mqtt_dns_lookup_duration"

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    move-object/from16 v8, p7

    invoke-static/range {v0 .. v8}, LX/05h;->b(LX/05h;Ljava/lang/String;JJJLandroid/net/NetworkInfo;)V

    .line 16553
    return-void
.end method

.method public final a(JLandroid/net/NetworkInfo;)V
    .locals 7

    .prologue
    .line 16544
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {v0}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 16545
    invoke-static {v0, p1, p2}, LX/05h;->b(Ljava/util/Map;J)V

    .line 16546
    invoke-static {v0, p3}, LX/05h;->a(Ljava/util/Map;Landroid/net/NetworkInfo;)V

    .line 16547
    iget-object v1, p0, LX/05h;->f:LX/059;

    invoke-virtual {v1}, LX/059;->i()J

    move-result-wide v2

    .line 16548
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 16549
    const-string v1, "dc_ms_ago"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16550
    :cond_0
    const-string v1, "mqtt_network_changed"

    invoke-virtual {p0, v1, v0}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 16551
    return-void
.end method

.method public final a(LX/05B;LX/05B;LX/05B;LX/05B;LX/05B;LX/05B;LX/05B;JJLandroid/net/NetworkInfo;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05B",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "LX/05B",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "LX/05B",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "LX/05B",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "LX/05B",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/05B",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/05B",
            "<",
            "Ljava/lang/Throwable;",
            ">;JJ",
            "Landroid/net/NetworkInfo;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 16519
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 16520
    iget-object v2, p0, LX/05h;->b:Landroid/content/Context;

    invoke-static {v2}, LX/05h;->a(Landroid/content/Context;)Z

    move-result v2

    .line 16521
    const-string v4, "is_airplane_mode_on"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16522
    invoke-static {p0, v3}, LX/05h;->b(LX/05h;Ljava/util/Map;)V

    .line 16523
    invoke-virtual {p1}, LX/05B;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 16524
    const-string v4, "connected_duration_ms"

    invoke-virtual {p1}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16525
    :cond_0
    invoke-virtual {p2}, LX/05B;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 16526
    const-string v4, "last_ping_ms_ago"

    invoke-virtual {p2}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16527
    :cond_1
    invoke-virtual {p3}, LX/05B;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 16528
    const-string v4, "last_sent_ms_ago"

    invoke-virtual {p3}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16529
    :cond_2
    invoke-virtual {p4}, LX/05B;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 16530
    const-string v4, "last_received_ms_ago"

    invoke-virtual {p4}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16531
    :cond_3
    invoke-virtual {p5}, LX/05B;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 16532
    const-string v2, "reason"

    invoke-virtual {p5}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16533
    :cond_4
    invoke-virtual {p6}, LX/05B;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 16534
    const-string v2, "operation"

    invoke-virtual {p6}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16535
    :cond_5
    invoke-virtual {p7}, LX/05B;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 16536
    const-string v4, "exception"

    invoke-virtual {p7}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16537
    const-string v4, "error_message"

    invoke-virtual {p7}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16538
    :cond_6
    const-string v2, "fs"

    invoke-static/range {p13 .. p13}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16539
    invoke-static {v3, p8, p9}, LX/05h;->a(Ljava/util/Map;J)V

    .line 16540
    move-wide/from16 v0, p10

    invoke-static {v3, v0, v1}, LX/05h;->b(Ljava/util/Map;J)V

    .line 16541
    move-object/from16 v0, p12

    invoke-static {v3, v0}, LX/05h;->a(Ljava/util/Map;Landroid/net/NetworkInfo;)V

    .line 16542
    const-string v2, "mqtt_disconnection_on_failure"

    invoke-virtual {p0, v2, v3}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 16543
    return-void
.end method

.method public final a(Ljava/lang/String;IJJJLandroid/net/NetworkInfo;)V
    .locals 3

    .prologue
    .line 16513
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "operation"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, "msg_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "timespan_ms"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 16514
    invoke-static {v0, p5, p6}, LX/05h;->a(Ljava/util/Map;J)V

    .line 16515
    invoke-static {v0, p7, p8}, LX/05h;->b(Ljava/util/Map;J)V

    .line 16516
    invoke-static {v0, p9}, LX/05h;->a(Ljava/util/Map;Landroid/net/NetworkInfo;)V

    .line 16517
    const-string v1, "mqtt_operation_timeout"

    invoke-virtual {p0, v1, v0}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 16518
    return-void
.end method

.method public final a(Ljava/lang/String;JJJLandroid/net/NetworkInfo;)V
    .locals 4

    .prologue
    .line 16507
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "operation"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, "timespan_ms"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 16508
    invoke-static {v0, p4, p5}, LX/05h;->a(Ljava/util/Map;J)V

    .line 16509
    invoke-static {v0, p6, p7}, LX/05h;->b(Ljava/util/Map;J)V

    .line 16510
    invoke-static {v0, p8}, LX/05h;->a(Ljava/util/Map;Landroid/net/NetworkInfo;)V

    .line 16511
    const-string v1, "mqtt_response_time"

    invoke-virtual {p0, v1, v0}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 16512
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/05B;LX/05B;ZIJLandroid/net/NetworkInfo;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Landroid/net/NetworkInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/05B",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/05B",
            "<",
            "Ljava/lang/Integer;",
            ">;ZIJ",
            "Landroid/net/NetworkInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 16494
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "act"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, "running"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 16495
    invoke-static {v0, p7, p8}, LX/05h;->b(Ljava/util/Map;J)V

    .line 16496
    invoke-static {v0, p9}, LX/05h;->a(Ljava/util/Map;Landroid/net/NetworkInfo;)V

    .line 16497
    if-ltz p6, :cond_0

    .line 16498
    const-string v1, "fflg"

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16499
    :cond_0
    invoke-static {p2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 16500
    const-string v1, "calr"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16501
    :cond_1
    invoke-virtual {p3}, LX/05B;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 16502
    const-string v1, "flg"

    invoke-virtual {p3}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16503
    :cond_2
    invoke-virtual {p4}, LX/05B;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 16504
    const-string v1, "sta_id"

    invoke-virtual {p4}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16505
    :cond_3
    const-string v1, "mqtt_service_state"

    invoke-virtual {p0, v1, v0}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 16506
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16482
    const-string v0, "service_name"

    iget-object v1, p0, LX/05h;->c:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16483
    const-string v0, "service_session_id"

    iget-wide v2, p0, LX/05h;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16484
    const-string v0, "process_id"

    iget-wide v2, p0, LX/05h;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16485
    const-string v0, "logger_object_id"

    iget-wide v2, p0, LX/05h;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16486
    const-string v4, "network_session_id"

    invoke-interface {p2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 16487
    const-string v4, "network_session_id"

    iget-object v5, p0, LX/05h;->f:LX/059;

    invoke-virtual {v5}, LX/059;->h()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16488
    :cond_0
    new-instance v0, LX/06d;

    .line 16489
    iget-object v1, p0, LX/05h;->e:Ljava/lang/String;

    if-nez v1, :cond_1

    :goto_0
    move-object v1, p1

    .line 16490
    iget-object v2, p0, LX/05h;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/06d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 16491
    invoke-virtual {v0, p2}, LX/06d;->a(Ljava/util/Map;)LX/06d;

    .line 16492
    iget-object v1, p0, LX/05h;->g:LX/05O;

    invoke-interface {v1, v0}, LX/05O;->a(LX/06d;)V

    .line 16493
    return-void

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/05h;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(ZJLjava/lang/String;LX/05B;LX/05B;JJLandroid/net/NetworkInfo;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZJ",
            "Ljava/lang/String;",
            "LX/05B",
            "<",
            "Ljava/lang/Exception;",
            ">;",
            "LX/05B",
            "<",
            "Ljava/lang/Byte;",
            ">;JJ",
            "Landroid/net/NetworkInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 16469
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "connect_result"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "connect_duration_ms"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 16470
    if-eqz p4, :cond_0

    .line 16471
    const-string v0, "failure_reason"

    invoke-interface {v1, v0, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16472
    :cond_0
    invoke-virtual {p5}, LX/05B;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16473
    const-string v2, "exception"

    invoke-virtual {p5}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16474
    const-string v2, "error_message"

    invoke-virtual {p5}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16475
    :cond_1
    invoke-virtual {p6}, LX/05B;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16476
    const-string v2, "conack_rc"

    invoke-virtual {p6}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16477
    :cond_2
    invoke-static {v1, p7, p8}, LX/05h;->a(Ljava/util/Map;J)V

    .line 16478
    invoke-static {v1, p9, p10}, LX/05h;->b(Ljava/util/Map;J)V

    .line 16479
    invoke-static {v1, p11}, LX/05h;->a(Ljava/util/Map;Landroid/net/NetworkInfo;)V

    .line 16480
    const-string v0, "mqtt_connect_attempt"

    invoke-virtual {p0, v0, v1}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 16481
    return-void
.end method
