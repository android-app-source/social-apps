.class public final LX/0KQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/vps/VideoPlayerService;


# direct methods
.method public constructor <init>(Lcom/facebook/video/vps/VideoPlayerService;)V
    .locals 0

    .prologue
    .line 41383
    iput-object p1, p0, LX/0KQ;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0H9;Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V
    .locals 5

    .prologue
    .line 41377
    iget-object v0, p0, LX/0KQ;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->q:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Kb;

    .line 41378
    if-eqz v0, :cond_0

    .line 41379
    :try_start_0
    iget v1, p1, LX/0H9;->mValue:I

    invoke-virtual {v0, v1, p2}, LX/0Kb;->a(ILcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41380
    :cond_0
    :goto_0
    return-void

    .line 41381
    :catch_0
    move-exception v0

    .line 41382
    sget-object v1, Lcom/facebook/video/vps/VideoPlayerService;->a:Ljava/lang/String;

    const-string v2, "Exception in VPS listener callback event %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
