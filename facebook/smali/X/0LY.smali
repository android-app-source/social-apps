.class public LX/0LY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0L9;
.implements LX/0L8;
.implements LX/0LX;


# instance fields
.field public final a:I

.field private final b:LX/0Gb;

.field private final c:LX/0LZ;

.field public final d:LX/0LW;

.field public final e:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/0LR;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0LR;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0MC;

.field private final h:I

.field public final i:Landroid/os/Handler;

.field public final j:LX/0KZ;

.field private final k:I

.field private l:I

.field private m:J

.field private n:J

.field private o:J

.field private p:J

.field private q:Z

.field public r:LX/0ON;

.field private s:Z

.field public t:Ljava/io/IOException;

.field private u:I

.field private v:I

.field private w:J

.field private x:J

.field private y:LX/0L4;

.field private z:LX/0AR;


# direct methods
.method public constructor <init>(LX/0LZ;LX/0Gb;I)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 43837
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, LX/0LY;-><init>(LX/0LZ;LX/0Gb;ILandroid/os/Handler;LX/0KZ;I)V

    .line 43838
    return-void
.end method

.method public constructor <init>(LX/0LZ;LX/0Gb;ILandroid/os/Handler;LX/0KZ;I)V
    .locals 8

    .prologue
    .line 43839
    const/4 v7, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, LX/0LY;-><init>(LX/0LZ;LX/0Gb;ILandroid/os/Handler;LX/0KZ;II)V

    .line 43840
    return-void
.end method

.method private constructor <init>(LX/0LZ;LX/0Gb;ILandroid/os/Handler;LX/0KZ;II)V
    .locals 2

    .prologue
    .line 43841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43842
    iput-object p1, p0, LX/0LY;->c:LX/0LZ;

    .line 43843
    iput-object p2, p0, LX/0LY;->b:LX/0Gb;

    .line 43844
    iput p3, p0, LX/0LY;->h:I

    .line 43845
    iput-object p4, p0, LX/0LY;->i:Landroid/os/Handler;

    .line 43846
    iput-object p5, p0, LX/0LY;->j:LX/0KZ;

    .line 43847
    iput p6, p0, LX/0LY;->a:I

    .line 43848
    iput p7, p0, LX/0LY;->k:I

    .line 43849
    new-instance v0, LX/0LW;

    invoke-direct {v0}, LX/0LW;-><init>()V

    iput-object v0, p0, LX/0LY;->d:LX/0LW;

    .line 43850
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    .line 43851
    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0LY;->f:Ljava/util/List;

    .line 43852
    new-instance v0, LX/0MC;

    invoke-interface {p2}, LX/0Gb;->b()LX/0O1;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0MC;-><init>(LX/0O1;)V

    iput-object v0, p0, LX/0LY;->g:LX/0MC;

    .line 43853
    const/4 v0, 0x0

    iput v0, p0, LX/0LY;->l:I

    .line 43854
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/0LY;->o:J

    .line 43855
    return-void
.end method

.method private a(JIILX/0AR;JJ)V
    .locals 12

    .prologue
    .line 43856
    iget-object v0, p0, LX/0LY;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0LY;->j:LX/0KZ;

    if-eqz v0, :cond_0

    .line 43857
    iget-object v0, p0, LX/0LY;->i:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/chunk/ChunkSampleSource$1;

    move-object v2, p0

    move-wide v3, p1

    move v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    invoke-direct/range {v1 .. v11}, Lcom/google/android/exoplayer/chunk/ChunkSampleSource$1;-><init>(LX/0LY;JIILX/0AR;JJ)V

    const v2, -0x1a0382c4

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 43858
    :cond_0
    return-void
.end method

.method private a(JIILX/0AR;JJJJ)V
    .locals 18

    .prologue
    .line 43827
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0LY;->i:Landroid/os/Handler;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0LY;->j:LX/0KZ;

    if-eqz v2, :cond_0

    .line 43828
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0LY;->i:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/exoplayer/chunk/ChunkSampleSource$2;

    move-object/from16 v4, p0

    move-wide/from16 v5, p1

    move/from16 v7, p3

    move/from16 v8, p4

    move-object/from16 v9, p5

    move-wide/from16 v10, p6

    move-wide/from16 v12, p8

    move-wide/from16 v14, p10

    move-wide/from16 v16, p12

    invoke-direct/range {v3 .. v17}, Lcom/google/android/exoplayer/chunk/ChunkSampleSource$2;-><init>(LX/0LY;JIILX/0AR;JJJJ)V

    const v4, 0x6a95e97

    invoke-static {v2, v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 43829
    :cond_0
    return-void
.end method

.method private a(LX/0AR;IJ)V
    .locals 7

    .prologue
    .line 43859
    iget-object v0, p0, LX/0LY;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0LY;->j:LX/0KZ;

    if-eqz v0, :cond_0

    .line 43860
    iget-object v6, p0, LX/0LY;->i:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/exoplayer/chunk/ChunkSampleSource$6;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer/chunk/ChunkSampleSource$6;-><init>(LX/0LY;LX/0AR;IJ)V

    const v1, -0x4e14efa8

    invoke-static {v6, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 43861
    :cond_0
    return-void
.end method

.method public static c(J)J
    .locals 2

    .prologue
    .line 43862
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    return-wide v0
.end method

.method private d(J)V
    .locals 1

    .prologue
    .line 43863
    iput-wide p1, p0, LX/0LY;->o:J

    .line 43864
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0LY;->s:Z

    .line 43865
    iget-object v0, p0, LX/0LY;->r:LX/0ON;

    .line 43866
    iget-boolean p1, v0, LX/0ON;->c:Z

    move v0, p1

    .line 43867
    if-eqz v0, :cond_0

    .line 43868
    iget-object v0, p0, LX/0LY;->r:LX/0ON;

    invoke-virtual {v0}, LX/0ON;->b()V

    .line 43869
    :goto_0
    return-void

    .line 43870
    :cond_0
    iget-object v0, p0, LX/0LY;->g:LX/0MC;

    invoke-virtual {v0}, LX/0MC;->a()V

    .line 43871
    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 43872
    invoke-direct {p0}, LX/0LY;->g()V

    .line 43873
    invoke-direct {p0}, LX/0LY;->i()V

    goto :goto_0
.end method

.method public static d(LX/0LY;I)Z
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 43953
    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-gt v0, p1, :cond_0

    move v0, v1

    .line 43954
    :goto_0
    return v0

    .line 43955
    :cond_0
    const-wide/16 v2, 0x0

    .line 43956
    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0LR;

    iget-wide v4, v0, LX/0LQ;->i:J

    .line 43957
    const/4 v0, 0x0

    .line 43958
    :goto_1
    iget-object v6, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    if-le v6, p1, :cond_1

    .line 43959
    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0LR;

    .line 43960
    iget-wide v2, v0, LX/0LQ;->h:J

    .line 43961
    iput-boolean v1, p0, LX/0LY;->s:Z

    goto :goto_1

    .line 43962
    :cond_1
    iget-object v1, p0, LX/0LY;->g:LX/0MC;

    .line 43963
    iget v6, v0, LX/0LR;->l:I

    move v0, v6

    .line 43964
    invoke-virtual {v1, v0}, LX/0MC;->a(I)V

    .line 43965
    iget-object v7, p0, LX/0LY;->i:Landroid/os/Handler;

    if-eqz v7, :cond_2

    iget-object v7, p0, LX/0LY;->j:LX/0KZ;

    if-eqz v7, :cond_2

    .line 43966
    iget-object v13, p0, LX/0LY;->i:Landroid/os/Handler;

    new-instance v7, Lcom/google/android/exoplayer/chunk/ChunkSampleSource$5;

    move-object v8, p0

    move-wide v9, v2

    move-wide v11, v4

    invoke-direct/range {v7 .. v12}, Lcom/google/android/exoplayer/chunk/ChunkSampleSource$5;-><init>(LX/0LY;JJ)V

    const v8, -0x3b3fe343

    invoke-static {v13, v7, v8}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 43967
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static e(J)J
    .locals 4

    .prologue
    .line 43874
    const-wide/16 v0, 0x1

    sub-long v0, p0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x1388

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static f(LX/0LY;J)V
    .locals 3

    .prologue
    .line 43875
    iget-object v0, p0, LX/0LY;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0LY;->j:LX/0KZ;

    if-eqz v0, :cond_0

    .line 43876
    iget-object v0, p0, LX/0LY;->i:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/chunk/ChunkSampleSource$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/exoplayer/chunk/ChunkSampleSource$3;-><init>(LX/0LY;J)V

    const v2, -0x1705f2a7

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 43877
    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 43878
    iget-object v0, p0, LX/0LY;->d:LX/0LW;

    const/4 v1, 0x0

    iput-object v1, v0, LX/0LW;->b:LX/0LP;

    .line 43879
    invoke-static {p0}, LX/0LY;->h(LX/0LY;)V

    .line 43880
    return-void
.end method

.method public static h(LX/0LY;)V
    .locals 1

    .prologue
    .line 43881
    const/4 v0, 0x0

    iput-object v0, p0, LX/0LY;->t:Ljava/io/IOException;

    .line 43882
    const/4 v0, 0x0

    iput v0, p0, LX/0LY;->v:I

    .line 43883
    return-void
.end method

.method private i()V
    .locals 15

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43884
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 43885
    invoke-direct {p0}, LX/0LY;->j()J

    move-result-wide v4

    .line 43886
    iget-object v6, p0, LX/0LY;->t:Ljava/io/IOException;

    if-eqz v6, :cond_5

    move v7, v0

    .line 43887
    :goto_0
    iget-object v6, p0, LX/0LY;->r:LX/0ON;

    .line 43888
    iget-boolean v10, v6, LX/0ON;->c:Z

    move v6, v10

    .line 43889
    if-nez v6, :cond_0

    if-eqz v7, :cond_6

    :cond_0
    move v6, v0

    .line 43890
    :goto_1
    if-nez v6, :cond_3

    iget-object v0, p0, LX/0LY;->d:LX/0LW;

    iget-object v0, v0, LX/0LW;->b:LX/0LP;

    if-nez v0, :cond_1

    cmp-long v0, v4, v2

    if-nez v0, :cond_2

    :cond_1
    iget-wide v0, p0, LX/0LY;->p:J

    sub-long v0, v8, v0

    const-wide/16 v10, 0x7d0

    cmp-long v0, v0, v10

    if-lez v0, :cond_3

    .line 43891
    :cond_2
    iput-wide v8, p0, LX/0LY;->p:J

    .line 43892
    invoke-static {p0}, LX/0LY;->m(LX/0LY;)V

    .line 43893
    iget-object v0, p0, LX/0LY;->d:LX/0LW;

    iget v0, v0, LX/0LW;->a:I

    invoke-static {p0, v0}, LX/0LY;->d(LX/0LY;I)Z

    move-result v0

    .line 43894
    iget-object v1, p0, LX/0LY;->d:LX/0LW;

    iget-object v1, v1, LX/0LW;->b:LX/0LP;

    if-nez v1, :cond_7

    move-wide v4, v2

    .line 43895
    :cond_3
    :goto_2
    iget-object v0, p0, LX/0LY;->b:LX/0Gb;

    iget-wide v2, p0, LX/0LY;->m:J

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, LX/0Gb;->a(Ljava/lang/Object;JJZ)Z

    move-result v0

    .line 43896
    if-eqz v7, :cond_8

    .line 43897
    iget-wide v0, p0, LX/0LY;->w:J

    sub-long v0, v8, v0

    .line 43898
    iget v2, p0, LX/0LY;->v:I

    int-to-long v2, v2

    invoke-static {v2, v3}, LX/0LY;->e(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_4

    .line 43899
    const/4 v12, 0x0

    iput-object v12, p0, LX/0LY;->t:Ljava/io/IOException;

    .line 43900
    iget-object v12, p0, LX/0LY;->d:LX/0LW;

    iget-object v14, v12, LX/0LW;->b:LX/0LP;

    .line 43901
    instance-of v12, v14, LX/0LR;

    move v12, v12

    .line 43902
    if-nez v12, :cond_a

    .line 43903
    invoke-static {p0}, LX/0LY;->m(LX/0LY;)V

    .line 43904
    iget-object v12, p0, LX/0LY;->d:LX/0LW;

    iget v12, v12, LX/0LW;->a:I

    invoke-static {p0, v12}, LX/0LY;->d(LX/0LY;I)Z

    .line 43905
    iget-object v12, p0, LX/0LY;->d:LX/0LW;

    iget-object v12, v12, LX/0LW;->b:LX/0LP;

    if-ne v12, v14, :cond_9

    .line 43906
    iget-object v12, p0, LX/0LY;->r:LX/0ON;

    invoke-virtual {v12, v14, p0}, LX/0ON;->a(LX/0LO;LX/0LX;)V

    .line 43907
    :cond_4
    :goto_3
    return-void

    :cond_5
    move v7, v1

    .line 43908
    goto :goto_0

    :cond_6
    move v6, v1

    .line 43909
    goto :goto_1

    .line 43910
    :cond_7
    if-eqz v0, :cond_3

    .line 43911
    invoke-direct {p0}, LX/0LY;->j()J

    move-result-wide v4

    goto :goto_2

    .line 43912
    :cond_8
    iget-object v1, p0, LX/0LY;->r:LX/0ON;

    .line 43913
    iget-boolean v2, v1, LX/0ON;->c:Z

    move v1, v2

    .line 43914
    if-nez v1, :cond_4

    if-eqz v0, :cond_4

    .line 43915
    invoke-static {p0}, LX/0LY;->l(LX/0LY;)V

    goto :goto_3

    .line 43916
    :cond_9
    invoke-virtual {v14}, LX/0LP;->e()J

    move-result-wide v12

    invoke-static {p0, v12, v13}, LX/0LY;->f(LX/0LY;J)V

    .line 43917
    invoke-static {p0}, LX/0LY;->l(LX/0LY;)V

    goto :goto_3

    .line 43918
    :cond_a
    iget-object v12, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v12}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v12

    if-ne v14, v12, :cond_b

    .line 43919
    iget-object v12, p0, LX/0LY;->r:LX/0ON;

    invoke-virtual {v12, v14, p0}, LX/0ON;->a(LX/0LO;LX/0LX;)V

    goto :goto_3

    .line 43920
    :cond_b
    iget-object v12, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v12}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/0LR;

    .line 43921
    if-ne v14, v12, :cond_c

    const/4 v13, 0x1

    :goto_4
    invoke-static {v13}, LX/0Av;->b(Z)V

    .line 43922
    invoke-static {p0}, LX/0LY;->m(LX/0LY;)V

    .line 43923
    iget-object v13, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v13, v12}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 43924
    iget-object v12, p0, LX/0LY;->d:LX/0LW;

    iget-object v12, v12, LX/0LW;->b:LX/0LP;

    if-ne v12, v14, :cond_d

    .line 43925
    iget-object v12, p0, LX/0LY;->r:LX/0ON;

    invoke-virtual {v12, v14, p0}, LX/0ON;->a(LX/0LO;LX/0LX;)V

    goto :goto_3

    .line 43926
    :cond_c
    const/4 v13, 0x0

    goto :goto_4

    .line 43927
    :cond_d
    invoke-virtual {v14}, LX/0LP;->e()J

    move-result-wide v12

    invoke-static {p0, v12, v13}, LX/0LY;->f(LX/0LY;J)V

    .line 43928
    iget-object v12, p0, LX/0LY;->d:LX/0LW;

    iget v12, v12, LX/0LW;->a:I

    invoke-static {p0, v12}, LX/0LY;->d(LX/0LY;I)Z

    .line 43929
    invoke-static {p0}, LX/0LY;->h(LX/0LY;)V

    .line 43930
    invoke-static {p0}, LX/0LY;->l(LX/0LY;)V

    goto :goto_3
.end method

.method private j()J
    .locals 2

    .prologue
    .line 43931
    invoke-direct {p0}, LX/0LY;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43932
    iget-wide v0, p0, LX/0LY;->o:J

    .line 43933
    :goto_0
    return-wide v0

    :cond_0
    iget-boolean v0, p0, LX/0LY;->s:Z

    if-eqz v0, :cond_1

    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0LR;

    iget-wide v0, v0, LX/0LQ;->i:J

    goto :goto_0
.end method

.method public static l(LX/0LY;)V
    .locals 11

    .prologue
    const-wide/16 v6, -0x1

    .line 43934
    iget-object v0, p0, LX/0LY;->d:LX/0LW;

    iget-object v10, v0, LX/0LW;->b:LX/0LP;

    .line 43935
    if-nez v10, :cond_0

    .line 43936
    :goto_0
    return-void

    .line 43937
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0LY;->x:J

    .line 43938
    instance-of v0, v10, LX/0LR;

    move v0, v0

    .line 43939
    if-eqz v0, :cond_2

    move-object v0, v10

    .line 43940
    check-cast v0, LX/0LR;

    .line 43941
    iget-object v1, p0, LX/0LY;->g:LX/0MC;

    .line 43942
    iput-object v1, v0, LX/0LR;->k:LX/0MC;

    .line 43943
    iget-object v2, v1, LX/0MC;->a:LX/0MP;

    .line 43944
    iget-object v1, v2, LX/0MP;->c:LX/0MN;

    invoke-virtual {v1}, LX/0MN;->b()I

    move-result v1

    move v2, v1

    .line 43945
    move v2, v2

    .line 43946
    iput v2, v0, LX/0LR;->l:I

    .line 43947
    iget-object v1, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 43948
    invoke-direct {p0}, LX/0LY;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43949
    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, LX/0LY;->o:J

    .line 43950
    :cond_1
    iget-object v1, v0, LX/0LP;->e:LX/0OA;

    iget-wide v1, v1, LX/0OA;->e:J

    iget v3, v0, LX/0LP;->b:I

    iget v4, v0, LX/0LP;->c:I

    iget-object v5, v0, LX/0LP;->d:LX/0AR;

    iget-wide v6, v0, LX/0LQ;->h:J

    iget-wide v8, v0, LX/0LQ;->i:J

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, LX/0LY;->a(JIILX/0AR;JJ)V

    .line 43951
    :goto_1
    iget-object v0, p0, LX/0LY;->r:LX/0ON;

    invoke-virtual {v0, v10, p0}, LX/0ON;->a(LX/0LO;LX/0LX;)V

    goto :goto_0

    .line 43952
    :cond_2
    iget-object v0, v10, LX/0LP;->e:LX/0OA;

    iget-wide v1, v0, LX/0OA;->e:J

    iget v3, v10, LX/0LP;->b:I

    iget v4, v10, LX/0LP;->c:I

    iget-object v5, v10, LX/0LP;->d:LX/0AR;

    move-object v0, p0

    move-wide v8, v6

    invoke-direct/range {v0 .. v9}, LX/0LY;->a(JIILX/0AR;JJ)V

    goto :goto_1
.end method

.method public static m(LX/0LY;)V
    .locals 6

    .prologue
    .line 43830
    iget-object v0, p0, LX/0LY;->d:LX/0LW;

    const/4 v1, 0x0

    iput-boolean v1, v0, LX/0LW;->c:Z

    .line 43831
    iget-object v0, p0, LX/0LY;->d:LX/0LW;

    iget-object v1, p0, LX/0LY;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, LX/0LW;->a:I

    .line 43832
    iget-object v2, p0, LX/0LY;->c:LX/0LZ;

    iget-object v3, p0, LX/0LY;->f:Ljava/util/List;

    iget-wide v0, p0, LX/0LY;->o:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/0LY;->o:J

    :goto_0
    iget-object v4, p0, LX/0LY;->d:LX/0LW;

    invoke-interface {v2, v3, v0, v1, v4}, LX/0LZ;->a(Ljava/util/List;JLX/0LW;)V

    .line 43833
    iget-object v0, p0, LX/0LY;->d:LX/0LW;

    iget-boolean v0, v0, LX/0LW;->c:Z

    iput-boolean v0, p0, LX/0LY;->s:Z

    .line 43834
    return-void

    .line 43835
    :cond_0
    iget-wide v0, p0, LX/0LY;->m:J

    goto :goto_0
.end method

.method private n()Z
    .locals 4

    .prologue
    .line 43836
    iget-wide v0, p0, LX/0LY;->o:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final Z_()LX/0L8;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 43738
    iget v0, p0, LX/0LY;->l:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43739
    iput v1, p0, LX/0LY;->l:I

    .line 43740
    return-object p0

    .line 43741
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IJLX/0L5;LX/0L7;)I
    .locals 10

    .prologue
    const/4 v4, -0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 43705
    iget v0, p0, LX/0LY;->l:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43706
    iput-wide p2, p0, LX/0LY;->m:J

    .line 43707
    iget-boolean v0, p0, LX/0LY;->q:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/0LY;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v4

    .line 43708
    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 43709
    goto :goto_0

    .line 43710
    :cond_2
    iget-object v0, p0, LX/0LY;->g:LX/0MC;

    invoke-virtual {v0}, LX/0MC;->g()Z

    move-result v0

    if-nez v0, :cond_3

    move v3, v1

    .line 43711
    :goto_2
    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0LR;

    move-object v5, v0

    .line 43712
    :goto_3
    if-eqz v3, :cond_4

    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v1, :cond_4

    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0LR;

    .line 43713
    iget v6, v0, LX/0LR;->l:I

    move v0, v6

    .line 43714
    iget-object v6, p0, LX/0LY;->g:LX/0MC;

    invoke-virtual {v6}, LX/0MC;->c()I

    move-result v6

    if-gt v0, v6, :cond_4

    .line 43715
    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 43716
    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0LR;

    move-object v5, v0

    goto :goto_3

    :cond_3
    move v3, v2

    .line 43717
    goto :goto_2

    .line 43718
    :cond_4
    iget-object v0, p0, LX/0LY;->z:LX/0AR;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/0LY;->z:LX/0AR;

    iget-object v6, v5, LX/0LP;->d:LX/0AR;

    invoke-virtual {v0, v6}, LX/0AR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 43719
    :cond_5
    iget-object v0, v5, LX/0LP;->d:LX/0AR;

    iget v6, v5, LX/0LP;->c:I

    iget-wide v8, v5, LX/0LQ;->h:J

    invoke-direct {p0, v0, v6, v8, v9}, LX/0LY;->a(LX/0AR;IJ)V

    .line 43720
    iget-object v0, v5, LX/0LP;->d:LX/0AR;

    iput-object v0, p0, LX/0LY;->z:LX/0AR;

    .line 43721
    :cond_6
    if-nez v3, :cond_7

    iget-boolean v0, v5, LX/0LR;->a:Z

    if-eqz v0, :cond_8

    .line 43722
    :cond_7
    invoke-virtual {v5}, LX/0LR;->b()LX/0L4;

    move-result-object v0

    .line 43723
    iget-object v6, p0, LX/0LY;->y:LX/0L4;

    invoke-virtual {v0, v6}, LX/0L4;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 43724
    iput-object v0, p4, LX/0L5;->a:LX/0L4;

    .line 43725
    invoke-virtual {v5}, LX/0LR;->c()LX/0Lz;

    move-result-object v1

    iput-object v1, p4, LX/0L5;->b:LX/0Lz;

    .line 43726
    iput-object v0, p0, LX/0LY;->y:LX/0L4;

    .line 43727
    const/4 v0, -0x4

    goto :goto_1

    .line 43728
    :cond_8
    if-nez v3, :cond_a

    .line 43729
    iget-boolean v0, p0, LX/0LY;->s:Z

    if-eqz v0, :cond_9

    .line 43730
    const/4 v0, -0x1

    goto/16 :goto_1

    :cond_9
    move v0, v4

    .line 43731
    goto/16 :goto_1

    .line 43732
    :cond_a
    iget-object v0, p0, LX/0LY;->g:LX/0MC;

    invoke-virtual {v0, p5}, LX/0MC;->a(LX/0L7;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 43733
    iget-wide v4, p5, LX/0L7;->e:J

    iget-wide v6, p0, LX/0LY;->n:J

    cmp-long v0, v4, v6

    if-gez v0, :cond_c

    .line 43734
    :goto_4
    iget v0, p5, LX/0L7;->d:I

    if-eqz v1, :cond_b

    const/high16 v2, 0x8000000

    :cond_b
    or-int/2addr v0, v2

    iput v0, p5, LX/0L7;->d:I

    .line 43735
    const/4 v0, -0x3

    goto/16 :goto_1

    :cond_c
    move v1, v2

    .line 43736
    goto :goto_4

    :cond_d
    move v0, v4

    .line 43737
    goto/16 :goto_1
.end method

.method public final a(I)LX/0L4;
    .locals 2

    .prologue
    .line 43702
    iget v0, p0, LX/0LY;->l:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/0LY;->l:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43703
    iget-object v0, p0, LX/0LY;->c:LX/0LZ;

    invoke-interface {v0, p1}, LX/0LZ;->a(I)LX/0L4;

    move-result-object v0

    return-object v0

    .line 43704
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43688
    iget v0, p0, LX/0LY;->l:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43689
    iget v0, p0, LX/0LY;->u:I

    add-int/lit8 v3, v0, 0x1

    iput v3, p0, LX/0LY;->u:I

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0Av;->b(Z)V

    .line 43690
    const/4 v0, 0x3

    iput v0, p0, LX/0LY;->l:I

    .line 43691
    iget-object v0, p0, LX/0LY;->c:LX/0LZ;

    invoke-interface {v0, p1}, LX/0LZ;->b(I)V

    .line 43692
    iget-object v0, p0, LX/0LY;->b:LX/0Gb;

    iget v1, p0, LX/0LY;->h:I

    invoke-interface {v0, p0, v1}, LX/0Gb;->a(Ljava/lang/Object;I)V

    .line 43693
    iput-object v4, p0, LX/0LY;->z:LX/0AR;

    .line 43694
    iput-object v4, p0, LX/0LY;->y:LX/0L4;

    .line 43695
    iput-wide p2, p0, LX/0LY;->m:J

    .line 43696
    iput-wide p2, p0, LX/0LY;->n:J

    .line 43697
    iput-boolean v2, p0, LX/0LY;->q:Z

    .line 43698
    invoke-direct {p0, p2, p3}, LX/0LY;->d(J)V

    .line 43699
    return-void

    :cond_0
    move v0, v2

    .line 43700
    goto :goto_0

    :cond_1
    move v1, v2

    .line 43701
    goto :goto_1
.end method

.method public final a(LX/0LO;)V
    .locals 14

    .prologue
    const-wide/16 v6, -0x1

    .line 43676
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 43677
    iget-wide v0, p0, LX/0LY;->x:J

    sub-long v12, v10, v0

    .line 43678
    iget-object v0, p0, LX/0LY;->d:LX/0LW;

    iget-object v5, v0, LX/0LW;->b:LX/0LP;

    .line 43679
    iget-object v0, p0, LX/0LY;->c:LX/0LZ;

    invoke-interface {v0, v5}, LX/0LZ;->a(LX/0LP;)V

    .line 43680
    instance-of v0, v5, LX/0LR;

    move v0, v0

    .line 43681
    if-eqz v0, :cond_0

    move-object v0, v5

    .line 43682
    check-cast v0, LX/0LR;

    .line 43683
    invoke-virtual {v5}, LX/0LP;->e()J

    move-result-wide v1

    iget v3, v0, LX/0LP;->b:I

    iget v4, v0, LX/0LP;->c:I

    iget-object v5, v0, LX/0LP;->d:LX/0AR;

    iget-wide v6, v0, LX/0LQ;->h:J

    iget-wide v8, v0, LX/0LQ;->i:J

    move-object v0, p0

    invoke-direct/range {v0 .. v13}, LX/0LY;->a(JIILX/0AR;JJJJ)V

    .line 43684
    :goto_0
    invoke-direct {p0}, LX/0LY;->g()V

    .line 43685
    invoke-direct {p0}, LX/0LY;->i()V

    .line 43686
    return-void

    .line 43687
    :cond_0
    invoke-virtual {v5}, LX/0LP;->e()J

    move-result-wide v1

    iget v3, v5, LX/0LP;->b:I

    iget v4, v5, LX/0LP;->c:I

    iget-object v5, v5, LX/0LP;->d:LX/0AR;

    move-object v0, p0

    move-wide v8, v6

    invoke-direct/range {v0 .. v13}, LX/0LY;->a(JIILX/0AR;JJJJ)V

    goto :goto_0
.end method

.method public final a(LX/0LO;Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 43665
    iput-object p2, p0, LX/0LY;->t:Ljava/io/IOException;

    .line 43666
    iget v0, p0, LX/0LY;->v:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0LY;->v:I

    .line 43667
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0LY;->w:J

    .line 43668
    iget-object v0, p0, LX/0LY;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0LY;->j:LX/0KZ;

    if-eqz v0, :cond_0

    .line 43669
    iget-object v0, p0, LX/0LY;->i:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/chunk/ChunkSampleSource$4;

    invoke-direct {v1, p0, p2}, Lcom/google/android/exoplayer/chunk/ChunkSampleSource$4;-><init>(LX/0LY;Ljava/io/IOException;)V

    const p1, 0x1aa72c7c

    invoke-static {v0, v1, p1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 43670
    :cond_0
    invoke-direct {p0}, LX/0LY;->i()V

    .line 43671
    return-void
.end method

.method public final a(J)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 43656
    iget v0, p0, LX/0LY;->l:I

    if-eq v0, v2, :cond_0

    iget v0, p0, LX/0LY;->l:I

    if-ne v0, v5, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43657
    iget v0, p0, LX/0LY;->l:I

    if-ne v0, v5, :cond_2

    .line 43658
    :goto_1
    return v2

    :cond_1
    move v0, v1

    .line 43659
    goto :goto_0

    .line 43660
    :cond_2
    iget-object v0, p0, LX/0LY;->c:LX/0LZ;

    invoke-interface {v0}, LX/0LZ;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v2, v1

    .line 43661
    goto :goto_1

    .line 43662
    :cond_3
    iget-object v0, p0, LX/0LY;->c:LX/0LZ;

    invoke-interface {v0}, LX/0LZ;->c()I

    move-result v0

    if-lez v0, :cond_4

    .line 43663
    new-instance v0, LX/0ON;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Loader:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/0LY;->c:LX/0LZ;

    invoke-interface {v4, v1}, LX/0LZ;->a(I)LX/0L4;

    move-result-object v1

    iget-object v1, v1, LX/0L4;->b:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ON;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0LY;->r:LX/0ON;

    .line 43664
    :cond_4
    iput v5, p0, LX/0LY;->l:I

    goto :goto_1
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 43672
    iget-boolean v0, p0, LX/0LY;->q:Z

    if-eqz v0, :cond_0

    .line 43673
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0LY;->q:Z

    .line 43674
    iget-wide v0, p0, LX/0LY;->n:J

    .line 43675
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 43742
    iget-object v0, p0, LX/0LY;->t:Ljava/io/IOException;

    if-eqz v0, :cond_0

    iget v0, p0, LX/0LY;->v:I

    iget v1, p0, LX/0LY;->k:I

    if-le v0, v1, :cond_0

    .line 43743
    iget-object v0, p0, LX/0LY;->t:Ljava/io/IOException;

    throw v0

    .line 43744
    :cond_0
    iget-object v0, p0, LX/0LY;->d:LX/0LW;

    iget-object v0, v0, LX/0LW;->b:LX/0LP;

    if-nez v0, :cond_1

    .line 43745
    iget-object v0, p0, LX/0LY;->c:LX/0LZ;

    invoke-interface {v0}, LX/0LZ;->a()V

    .line 43746
    :cond_1
    return-void
.end method

.method public final b(J)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 43747
    iget v0, p0, LX/0LY;->l:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43748
    invoke-direct {p0}, LX/0LY;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v4, p0, LX/0LY;->o:J

    .line 43749
    :goto_1
    iput-wide p1, p0, LX/0LY;->m:J

    .line 43750
    iput-wide p1, p0, LX/0LY;->n:J

    .line 43751
    cmp-long v0, v4, p1

    if-nez v0, :cond_2

    .line 43752
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 43753
    goto :goto_0

    .line 43754
    :cond_1
    iget-wide v4, p0, LX/0LY;->m:J

    goto :goto_1

    .line 43755
    :cond_2
    invoke-direct {p0}, LX/0LY;->n()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, LX/0LY;->g:LX/0MC;

    invoke-virtual {v0, p1, p2}, LX/0MC;->b(J)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 43756
    :goto_3
    if-eqz v0, :cond_5

    .line 43757
    iget-object v0, p0, LX/0LY;->g:LX/0MC;

    invoke-virtual {v0}, LX/0MC;->g()Z

    move-result v0

    if-nez v0, :cond_3

    move v2, v1

    .line 43758
    :cond_3
    :goto_4
    if-eqz v2, :cond_6

    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v1, :cond_6

    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0LR;

    .line 43759
    iget v3, v0, LX/0LR;->l:I

    move v0, v3

    .line 43760
    iget-object v3, p0, LX/0LY;->g:LX/0MC;

    invoke-virtual {v3}, LX/0MC;->c()I

    move-result v3

    if-gt v0, v3, :cond_6

    .line 43761
    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    goto :goto_4

    :cond_4
    move v0, v2

    .line 43762
    goto :goto_3

    .line 43763
    :cond_5
    invoke-direct {p0, p1, p2}, LX/0LY;->d(J)V

    .line 43764
    :cond_6
    iput-boolean v1, p0, LX/0LY;->q:Z

    goto :goto_2
.end method

.method public final b(IJ)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43765
    iget v0, p0, LX/0LY;->l:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43766
    iput-wide p2, p0, LX/0LY;->m:J

    .line 43767
    iget-object v0, p0, LX/0LY;->c:LX/0LZ;

    invoke-interface {v0, p2, p3}, LX/0LZ;->a(J)V

    .line 43768
    invoke-direct {p0}, LX/0LY;->i()V

    .line 43769
    iget-boolean v0, p0, LX/0LY;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0LY;->g:LX/0MC;

    invoke-virtual {v0}, LX/0MC;->g()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 43770
    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 43771
    iget v0, p0, LX/0LY;->l:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/0LY;->l:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43772
    iget-object v0, p0, LX/0LY;->c:LX/0LZ;

    invoke-interface {v0}, LX/0LZ;->c()I

    move-result v0

    return v0

    .line 43773
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43774
    iget v0, p0, LX/0LY;->l:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43775
    iget v0, p0, LX/0LY;->u:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0LY;->u:I

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0Av;->b(Z)V

    .line 43776
    const/4 v0, 0x2

    iput v0, p0, LX/0LY;->l:I

    .line 43777
    :try_start_0
    iget-object v0, p0, LX/0LY;->c:LX/0LZ;

    invoke-interface {v0}, LX/0LZ;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43778
    iget-object v0, p0, LX/0LY;->b:LX/0Gb;

    invoke-interface {v0, p0}, LX/0Gb;->a(Ljava/lang/Object;)V

    .line 43779
    iget-object v0, p0, LX/0LY;->r:LX/0ON;

    .line 43780
    iget-boolean v1, v0, LX/0ON;->c:Z

    move v0, v1

    .line 43781
    if-eqz v0, :cond_2

    .line 43782
    iget-object v0, p0, LX/0LY;->r:LX/0ON;

    invoke-virtual {v0}, LX/0ON;->b()V

    .line 43783
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 43784
    goto :goto_0

    :cond_1
    move v1, v2

    .line 43785
    goto :goto_1

    .line 43786
    :cond_2
    iget-object v0, p0, LX/0LY;->g:LX/0MC;

    invoke-virtual {v0}, LX/0MC;->a()V

    .line 43787
    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 43788
    invoke-direct {p0}, LX/0LY;->g()V

    .line 43789
    iget-object v0, p0, LX/0LY;->b:LX/0Gb;

    invoke-interface {v0}, LX/0Gb;->a()V

    goto :goto_2

    .line 43790
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0LY;->b:LX/0Gb;

    invoke-interface {v1, p0}, LX/0Gb;->a(Ljava/lang/Object;)V

    .line 43791
    iget-object v1, p0, LX/0LY;->r:LX/0ON;

    .line 43792
    iget-boolean v2, v1, LX/0ON;->c:Z

    move v1, v2

    .line 43793
    if-eqz v1, :cond_3

    .line 43794
    iget-object v1, p0, LX/0LY;->r:LX/0ON;

    invoke-virtual {v1}, LX/0ON;->b()V

    .line 43795
    :goto_3
    throw v0

    .line 43796
    :cond_3
    iget-object v1, p0, LX/0LY;->g:LX/0MC;

    invoke-virtual {v1}, LX/0MC;->a()V

    .line 43797
    iget-object v1, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 43798
    invoke-direct {p0}, LX/0LY;->g()V

    .line 43799
    iget-object v1, p0, LX/0LY;->b:LX/0Gb;

    invoke-interface {v1}, LX/0Gb;->a()V

    goto :goto_3
.end method

.method public final d()J
    .locals 6

    .prologue
    .line 43800
    iget v0, p0, LX/0LY;->l:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43801
    invoke-direct {p0}, LX/0LY;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 43802
    iget-wide v0, p0, LX/0LY;->o:J

    .line 43803
    :cond_0
    :goto_1
    return-wide v0

    .line 43804
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 43805
    :cond_2
    iget-boolean v0, p0, LX/0LY;->s:Z

    if-eqz v0, :cond_3

    .line 43806
    const-wide/16 v0, -0x3

    goto :goto_1

    .line 43807
    :cond_3
    iget-object v0, p0, LX/0LY;->g:LX/0MC;

    .line 43808
    iget-wide v4, v0, LX/0MC;->f:J

    move-wide v0, v4

    .line 43809
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-wide v0, p0, LX/0LY;->m:J

    goto :goto_1
.end method

.method public final e()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 43810
    iget v0, p0, LX/0LY;->l:I

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43811
    iget-object v0, p0, LX/0LY;->r:LX/0ON;

    if-eqz v0, :cond_0

    .line 43812
    iget-object v0, p0, LX/0LY;->r:LX/0ON;

    invoke-virtual {v0}, LX/0ON;->c()V

    .line 43813
    const/4 v0, 0x0

    iput-object v0, p0, LX/0LY;->r:LX/0ON;

    .line 43814
    :cond_0
    iput v1, p0, LX/0LY;->l:I

    .line 43815
    return-void

    :cond_1
    move v0, v1

    .line 43816
    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 43817
    iget-object v0, p0, LX/0LY;->d:LX/0LW;

    iget-object v0, v0, LX/0LW;->b:LX/0LP;

    .line 43818
    invoke-virtual {v0}, LX/0LP;->e()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, LX/0LY;->f(LX/0LY;J)V

    .line 43819
    invoke-direct {p0}, LX/0LY;->g()V

    .line 43820
    iget v0, p0, LX/0LY;->l:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 43821
    iget-wide v0, p0, LX/0LY;->o:J

    invoke-direct {p0, v0, v1}, LX/0LY;->d(J)V

    .line 43822
    :goto_0
    return-void

    .line 43823
    :cond_0
    iget-object v0, p0, LX/0LY;->g:LX/0MC;

    invoke-virtual {v0}, LX/0MC;->a()V

    .line 43824
    iget-object v0, p0, LX/0LY;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 43825
    invoke-direct {p0}, LX/0LY;->g()V

    .line 43826
    iget-object v0, p0, LX/0LY;->b:LX/0Gb;

    invoke-interface {v0}, LX/0Gb;->a()V

    goto :goto_0
.end method
