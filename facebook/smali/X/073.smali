.class public final enum LX/073;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/073;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/073;

.field public static final enum ACKNOWLEDGED_DELIVERY:LX/073;

.field public static final enum DATA_SAVING_MODE:LX/073;

.field public static final enum DELTA_SENT_MESSAGE_ENABLED:LX/073;

.field public static final enum EXACT_KEEPALIVE:LX/073;

.field public static final enum PROCESSING_LASTACTIVE_PRESENCEINFO:LX/073;

.field public static final enum REQUIRES_JSON_UNICODE_ESCAPES:LX/073;

.field public static final enum REQUIRE_REPLAY_PROTECTION:LX/073;

.field public static final enum SUPPRESS_GETDIFF_IN_CONNECT:LX/073;

.field public static final enum TYPING_OFF_WHEN_SENDING_MESSAGE:LX/073;

.field public static final enum USE_ENUM_TOPIC:LX/073;

.field public static final enum USE_SEND_PINGRESP:LX/073;

.field public static final enum USE_THRIFT_FOR_INBOX:LX/073;


# instance fields
.field private final mPosition:B


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 19001
    new-instance v0, LX/073;

    const-string v1, "ACKNOWLEDGED_DELIVERY"

    invoke-direct {v0, v1, v4, v4}, LX/073;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/073;->ACKNOWLEDGED_DELIVERY:LX/073;

    .line 19002
    new-instance v0, LX/073;

    const-string v1, "PROCESSING_LASTACTIVE_PRESENCEINFO"

    invoke-direct {v0, v1, v5, v5}, LX/073;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/073;->PROCESSING_LASTACTIVE_PRESENCEINFO:LX/073;

    .line 19003
    new-instance v0, LX/073;

    const-string v1, "EXACT_KEEPALIVE"

    invoke-direct {v0, v1, v6, v6}, LX/073;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/073;->EXACT_KEEPALIVE:LX/073;

    .line 19004
    new-instance v0, LX/073;

    const-string v1, "REQUIRES_JSON_UNICODE_ESCAPES"

    invoke-direct {v0, v1, v7, v7}, LX/073;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/073;->REQUIRES_JSON_UNICODE_ESCAPES:LX/073;

    .line 19005
    new-instance v0, LX/073;

    const-string v1, "DELTA_SENT_MESSAGE_ENABLED"

    invoke-direct {v0, v1, v8, v8}, LX/073;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/073;->DELTA_SENT_MESSAGE_ENABLED:LX/073;

    .line 19006
    new-instance v0, LX/073;

    const-string v1, "USE_ENUM_TOPIC"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/073;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/073;->USE_ENUM_TOPIC:LX/073;

    .line 19007
    new-instance v0, LX/073;

    const-string v1, "SUPPRESS_GETDIFF_IN_CONNECT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/073;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/073;->SUPPRESS_GETDIFF_IN_CONNECT:LX/073;

    .line 19008
    new-instance v0, LX/073;

    const-string v1, "USE_THRIFT_FOR_INBOX"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/073;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/073;->USE_THRIFT_FOR_INBOX:LX/073;

    .line 19009
    new-instance v0, LX/073;

    const-string v1, "USE_SEND_PINGRESP"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/073;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/073;->USE_SEND_PINGRESP:LX/073;

    .line 19010
    new-instance v0, LX/073;

    const-string v1, "REQUIRE_REPLAY_PROTECTION"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, LX/073;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/073;->REQUIRE_REPLAY_PROTECTION:LX/073;

    .line 19011
    new-instance v0, LX/073;

    const-string v1, "DATA_SAVING_MODE"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/073;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/073;->DATA_SAVING_MODE:LX/073;

    .line 19012
    new-instance v0, LX/073;

    const-string v1, "TYPING_OFF_WHEN_SENDING_MESSAGE"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/073;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/073;->TYPING_OFF_WHEN_SENDING_MESSAGE:LX/073;

    .line 19013
    const/16 v0, 0xc

    new-array v0, v0, [LX/073;

    sget-object v1, LX/073;->ACKNOWLEDGED_DELIVERY:LX/073;

    aput-object v1, v0, v4

    sget-object v1, LX/073;->PROCESSING_LASTACTIVE_PRESENCEINFO:LX/073;

    aput-object v1, v0, v5

    sget-object v1, LX/073;->EXACT_KEEPALIVE:LX/073;

    aput-object v1, v0, v6

    sget-object v1, LX/073;->REQUIRES_JSON_UNICODE_ESCAPES:LX/073;

    aput-object v1, v0, v7

    sget-object v1, LX/073;->DELTA_SENT_MESSAGE_ENABLED:LX/073;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/073;->USE_ENUM_TOPIC:LX/073;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/073;->SUPPRESS_GETDIFF_IN_CONNECT:LX/073;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/073;->USE_THRIFT_FOR_INBOX:LX/073;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/073;->USE_SEND_PINGRESP:LX/073;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/073;->REQUIRE_REPLAY_PROTECTION:LX/073;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/073;->DATA_SAVING_MODE:LX/073;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/073;->TYPING_OFF_WHEN_SENDING_MESSAGE:LX/073;

    aput-object v2, v0, v1

    sput-object v0, LX/073;->$VALUES:[LX/073;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 18991
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18992
    if-ltz p3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/01n;->a(Z)V

    .line 18993
    const/16 v0, 0x40

    if-ge p3, v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/01n;->a(Z)V

    .line 18994
    int-to-byte v0, p3

    iput-byte v0, p0, LX/073;->mPosition:B

    .line 18995
    return-void

    :cond_0
    move v0, v2

    .line 18996
    goto :goto_0

    :cond_1
    move v1, v2

    .line 18997
    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/073;
    .locals 1

    .prologue
    .line 19000
    const-class v0, LX/073;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/073;

    return-object v0
.end method

.method public static values()[LX/073;
    .locals 1

    .prologue
    .line 18999
    sget-object v0, LX/073;->$VALUES:[LX/073;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/073;

    return-object v0
.end method


# virtual methods
.method public final getMask()J
    .locals 2

    .prologue
    .line 18998
    const/4 v0, 0x1

    iget-byte v1, p0, LX/073;->mPosition:B

    shl-int/2addr v0, v1

    int-to-long v0, v0

    return-wide v0
.end method
