.class public final LX/01e;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final mDirectoryName:Ljava/io/File;

.field public final mHazardList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 4952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4953
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/01e;->mHazardList:Ljava/util/HashSet;

    .line 4954
    iput-object p1, p0, LX/01e;->mDirectoryName:Ljava/io/File;

    .line 4955
    return-void
.end method


# virtual methods
.method public final closeSilently(Ljava/io/Closeable;)V
    .locals 1
    .param p1    # Ljava/io/Closeable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 4956
    if-eqz p1, :cond_0

    .line 4957
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4958
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final produceWithDonorFile(Ljava/lang/String;Ljava/io/File;)LX/04Q;
    .locals 9
    .param p2    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 4959
    const-string v3, "/"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "."

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, ".."

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4960
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "illegal spool file name: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4961
    :cond_1
    :try_start_0
    new-instance v4, Ljava/io/File;

    iget-object v3, p0, LX/01e;->mDirectoryName:Ljava/io/File;

    invoke-direct {v4, v3, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 4962
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 4963
    :try_start_1
    iget-object v3, p0, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 4964
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4965
    invoke-virtual {p0, v2}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    move-object v0, v2

    .line 4966
    :goto_0
    return-object v0

    .line 4967
    :cond_2
    :try_start_2
    iget-object v3, p0, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 4968
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4969
    :try_start_3
    invoke-virtual {v4}, Ljava/io/File;->exists()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    move-result v3

    if-eqz v3, :cond_5

    .line 4970
    invoke-virtual {p0, v2}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    .line 4971
    monitor-enter p0

    .line 4972
    :try_start_4
    iget-object v0, p0, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 4973
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object v0, v2

    goto :goto_0

    .line 4974
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 4975
    :catchall_1
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    :goto_1
    if-eqz v4, :cond_3

    .line 4976
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 4977
    :cond_3
    invoke-virtual {p0, v3}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    .line 4978
    if-eqz v2, :cond_4

    .line 4979
    monitor-enter p0

    .line 4980
    :try_start_7
    iget-object v1, p0, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 4981
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    :cond_4
    throw v0

    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0

    .line 4982
    :cond_5
    if-eqz p2, :cond_c

    :try_start_9
    invoke-virtual {p2}, Ljava/io/File;->exists()Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    move-result v3

    if-eqz v3, :cond_c

    .line 4983
    :try_start_a
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v5, "rw"

    invoke-direct {v3, p2, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 4984
    :try_start_b
    invoke-virtual {p0, v3}, LX/01e;->tryLock(Ljava/io/RandomAccessFile;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {p2, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_c

    move-result v5

    if-eqz v5, :cond_6

    .line 4985
    :goto_2
    if-nez v0, :cond_b

    .line 4986
    :try_start_c
    invoke-virtual {p0, v3}, LX/01e;->closeSilently(Ljava/io/Closeable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_8

    move-object v3, v2

    move-object v1, v2

    .line 4987
    :goto_3
    if-nez v3, :cond_a

    .line 4988
    :try_start_d
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v0, "rw"

    invoke-direct {v3, v4, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_9

    .line 4989
    :try_start_e
    invoke-virtual {p0, v3}, LX/01e;->tryLock(Ljava/io/RandomAccessFile;)Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_a

    move-result v0

    if-nez v0, :cond_7

    .line 4990
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 4991
    invoke-virtual {p0, v3}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    .line 4992
    monitor-enter p0

    .line 4993
    :try_start_f
    iget-object v0, p0, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 4994
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    move-object v0, v2

    goto :goto_0

    :cond_6
    move v0, v1

    .line 4995
    goto :goto_2

    .line 4996
    :catch_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    .line 4997
    :goto_4
    :try_start_10
    const-string v5, "error using donor file %s; falling back to regular path"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 4998
    const-string v6, "Spool"

    invoke-static {v6, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_d

    .line 4999
    :try_start_11
    invoke-virtual {p0, v1}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    move-object v3, v2

    move-object v1, v2

    .line 5000
    goto :goto_3

    .line 5001
    :catchall_3
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    .line 5002
    :goto_5
    invoke-virtual {p0, v1}, LX/01e;->closeSilently(Ljava/io/Closeable;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_8

    .line 5003
    :try_start_12
    throw v0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    .line 5004
    :catchall_4
    move-exception v0

    move-object v3, v2

    move-object v8, v4

    move-object v4, v2

    move-object v2, v8

    goto :goto_1

    .line 5005
    :catchall_5
    move-exception v0

    :try_start_13
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_5

    throw v0

    :cond_7
    move-object v5, v3

    move-object v1, v4

    .line 5006
    :goto_6
    :try_start_14
    invoke-virtual {v4}, Ljava/io/File;->exists()Z
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_b

    move-result v0

    if-nez v0, :cond_9

    .line 5007
    if-eqz v1, :cond_8

    .line 5008
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 5009
    :cond_8
    invoke-virtual {p0, v3}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    .line 5010
    monitor-enter p0

    .line 5011
    :try_start_15
    iget-object v0, p0, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 5012
    monitor-exit p0

    move-object v0, v2

    goto/16 :goto_0

    :catchall_6
    move-exception v0

    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_6

    throw v0

    .line 5013
    :cond_9
    :try_start_16
    new-instance v0, LX/04Q;

    invoke-direct {v0, p0, v4, v5}, LX/04Q;-><init>(LX/01e;Ljava/io/File;Ljava/io/RandomAccessFile;)V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_b

    .line 5014
    invoke-virtual {p0, v2}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 5015
    :catchall_7
    move-exception v0

    :try_start_17
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_7

    throw v0

    .line 5016
    :catchall_8
    move-exception v0

    move-object v8, v4

    move-object v4, v2

    move-object v2, v8

    goto/16 :goto_1

    :catchall_9
    move-exception v0

    move-object v3, v1

    move-object v8, v4

    move-object v4, v2

    move-object v2, v8

    goto/16 :goto_1

    :catchall_a
    move-exception v0

    move-object v2, v4

    goto/16 :goto_1

    :catchall_b
    move-exception v0

    move-object v2, v4

    move-object v4, v1

    goto/16 :goto_1

    .line 5017
    :catchall_c
    move-exception v0

    move-object v1, v3

    goto :goto_5

    :catchall_d
    move-exception v0

    goto :goto_5

    .line 5018
    :catch_1
    move-exception v0

    move-object v1, v3

    goto :goto_4

    :cond_a
    move-object v5, v3

    move-object v3, v1

    move-object v1, v2

    goto :goto_6

    :cond_b
    move-object v1, v3

    goto/16 :goto_3

    :cond_c
    move-object v3, v2

    move-object v1, v2

    goto/16 :goto_3
.end method

.method public final tryLock(Ljava/io/RandomAccessFile;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 5019
    :try_start_0
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide v4, 0x7fffffffffffffffL

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->tryLock(JJZ)Ljava/nio/channels/FileLock;
    :try_end_0
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5020
    const/4 v0, 0x1

    .line 5021
    :cond_0
    :goto_0
    return v0

    .line 5022
    :catch_0
    move-exception v1

    .line 5023
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 5024
    if-eqz v2, :cond_1

    const-string v3, ": EAGAIN ("

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, ": errno 11 ("

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5025
    :cond_1
    throw v1

    .line 5026
    :catch_1
    goto :goto_0
.end method
