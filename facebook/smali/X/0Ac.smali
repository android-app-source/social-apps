.class public LX/0Ac;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0oz;

.field private final c:LX/0kb;

.field private final d:LX/0ad;

.field private final e:Z

.field private final f:Z

.field private final g:J

.field private final h:J

.field private final i:J

.field private final j:Ljava/util/regex/Pattern;

.field private final k:LX/19l;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0oz;LX/0kb;LX/0ad;LX/19l;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 24867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24868
    iput-object p1, p0, LX/0Ac;->a:LX/0Zb;

    .line 24869
    iput-object p2, p0, LX/0Ac;->b:LX/0oz;

    .line 24870
    iput-object p3, p0, LX/0Ac;->c:LX/0kb;

    .line 24871
    iput-object p4, p0, LX/0Ac;->d:LX/0ad;

    .line 24872
    iput-object p5, p0, LX/0Ac;->k:LX/19l;

    .line 24873
    iget-object v0, p0, LX/0Ac;->d:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/0ws;->j:S

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0Ac;->e:Z

    .line 24874
    iget-object v0, p0, LX/0Ac;->d:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/0ws;->k:S

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0Ac;->f:Z

    .line 24875
    iget-object v1, p0, LX/0Ac;->d:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-wide v4, LX/0ws;->i:J

    const-wide/16 v6, 0x1388

    invoke-interface/range {v1 .. v7}, LX/0ad;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/0Ac;->g:J

    .line 24876
    iget-object v1, p0, LX/0Ac;->d:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-wide v4, LX/0ws;->l:J

    const-wide/16 v6, 0x64

    invoke-interface/range {v1 .. v7}, LX/0ad;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/0Ac;->h:J

    .line 24877
    iget-object v1, p0, LX/0Ac;->d:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-wide v4, LX/0ws;->g:J

    const-wide/16 v6, 0x3e8

    invoke-interface/range {v1 .. v7}, LX/0ad;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/0Ac;->i:J

    .line 24878
    iget-object v0, p0, LX/0Ac;->d:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-char v3, LX/0ws;->h:C

    const-string v4, "\\d+-(\\d+).m4v"

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24879
    :try_start_0
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 24880
    :goto_0
    iput-object v0, p0, LX/0Ac;->j:Ljava/util/regex/Pattern;

    .line 24881
    return-void

    .line 24882
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/0Ac;
    .locals 6

    .prologue
    .line 24883
    new-instance v0, LX/0Ac;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v2

    check-cast v2, LX/0oz;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/19l;->a(LX/0QB;)LX/19l;

    move-result-object v5

    check-cast v5, LX/19l;

    invoke-direct/range {v0 .. v5}, LX/0Ac;-><init>(LX/0Zb;LX/0oz;LX/0kb;LX/0ad;LX/19l;)V

    .line 24884
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Ab;
    .locals 6

    .prologue
    .line 24885
    new-instance v0, LX/0Ab;

    new-instance v1, LX/0Ad;

    iget-boolean v2, p0, LX/0Ac;->e:Z

    iget-wide v4, p0, LX/0Ac;->g:J

    invoke-direct {v1, v2, v4, v5}, LX/0Ad;-><init>(ZJ)V

    iget-object v2, p0, LX/0Ac;->a:LX/0Zb;

    iget-object v3, p0, LX/0Ac;->b:LX/0oz;

    iget-object v4, p0, LX/0Ac;->c:LX/0kb;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/0Ab;-><init>(LX/0Ad;LX/0Zb;LX/0oz;LX/0kb;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;JLX/0Ae;)LX/0An;
    .locals 14

    .prologue
    .line 24886
    new-instance v12, LX/0An;

    new-instance v0, LX/0Ad;

    iget-boolean v1, p0, LX/0Ac;->f:Z

    iget-wide v2, p0, LX/0Ac;->g:J

    iget-wide v4, p0, LX/0Ac;->h:J

    move-wide/from16 v6, p2

    invoke-direct/range {v0 .. v7}, LX/0Ad;-><init>(ZJJJ)V

    iget-object v3, p0, LX/0Ac;->a:LX/0Zb;

    iget-object v4, p0, LX/0Ac;->b:LX/0oz;

    iget-object v5, p0, LX/0Ac;->c:LX/0kb;

    iget-wide v8, p0, LX/0Ac;->i:J

    iget-object v10, p0, LX/0Ac;->j:Ljava/util/regex/Pattern;

    iget-object v11, p0, LX/0Ac;->k:LX/19l;

    move-object v1, v12

    move-object v2, v0

    move-object v6, p1

    move-object/from16 v7, p4

    invoke-direct/range {v1 .. v11}, LX/0An;-><init>(LX/0Ad;LX/0Zb;LX/0oz;LX/0kb;Ljava/lang/String;LX/0Ae;JLjava/util/regex/Pattern;LX/19l;)V

    return-object v12
.end method
