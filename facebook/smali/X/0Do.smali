.class public LX/0Do;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/0Do;


# instance fields
.field private final b:Landroid/os/Handler;

.field public final c:Landroid/content/Context;

.field public d:Landroid/webkit/WebView;

.field public e:Ljava/lang/String;

.field public f:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:LX/0CQ;

.field public k:J


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 31089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31090
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/0Do;->h:Ljava/util/LinkedList;

    .line 31091
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Do;->i:Z

    .line 31092
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/0Do;->c:Landroid/content/Context;

    .line 31093
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/0Do;->b:Landroid/os/Handler;

    .line 31094
    invoke-static {}, LX/0CQ;->a()LX/0CQ;

    move-result-object v0

    iput-object v0, p0, LX/0Do;->j:LX/0CQ;

    .line 31095
    iget-object v0, p0, LX/0Do;->j:LX/0CQ;

    iget-object v1, p0, LX/0Do;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/0CQ;->a(Landroid/content/Context;)V

    .line 31096
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0Do;->g:Ljava/util/List;

    .line 31097
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)LX/0Do;
    .locals 2

    .prologue
    .line 31098
    const-class v1, LX/0Do;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0Do;->a:LX/0Do;

    if-nez v0, :cond_0

    .line 31099
    new-instance v0, LX/0Do;

    invoke-direct {v0, p0}, LX/0Do;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/0Do;->a:LX/0Do;

    .line 31100
    :cond_0
    sget-object v0, LX/0Do;->a:LX/0Do;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 31101
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(LX/0Do;)V
    .locals 5

    .prologue
    .line 31102
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Do;->d:Landroid/webkit/WebView;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 31103
    :goto_0
    monitor-exit p0

    return-void

    .line 31104
    :cond_0
    :try_start_1
    iget-boolean v0, p0, LX/0Do;->i:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/0Do;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 31105
    :cond_1
    const-string v0, "BrowserHtmlResourceExtractor"

    const-string v1, "HtmlResourceExtractor is still working when browser opened, current url %s, in queue %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/0Do;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/0Do;->h:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31106
    :cond_2
    iget-object v0, p0, LX/0Do;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 31107
    iget-object v0, p0, LX/0Do;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 31108
    iget-object v0, p0, LX/0Do;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 31109
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Do;->d:Landroid/webkit/WebView;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 31110
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized d(LX/0Do;)V
    .locals 6

    .prologue
    .line 31111
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/0Do;->i:Z

    .line 31112
    iget-object v0, p0, LX/0Do;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 31113
    iget-object v0, p0, LX/0Do;->j:LX/0CQ;

    iget-object v1, p0, LX/0Do;->e:Ljava/lang/String;

    iget-object v2, p0, LX/0Do;->g:Ljava/util/List;

    .line 31114
    new-instance v3, LX/0CI;

    invoke-direct {v3, v0, v1, v2}, LX/0CI;-><init>(LX/0CQ;Ljava/lang/String;Ljava/util/List;)V

    invoke-static {v0, v3}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 31115
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, LX/0Do;->k:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/0Do;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/0Do;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 31116
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Do;->e:Ljava/lang/String;

    .line 31117
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0Do;->g:Ljava/util/List;

    .line 31118
    iget-object v0, p0, LX/0Do;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    .line 31119
    if-eqz v0, :cond_1

    .line 31120
    invoke-virtual {p0, v0}, LX/0Do;->a(Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31121
    :cond_1
    monitor-exit p0

    return-void

    .line 31122
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;)V
    .locals 3

    .prologue
    .line 31123
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0Do;->i:Z

    if-eqz v0, :cond_2

    .line 31124
    iget-object v0, p0, LX/0Do;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 31125
    iget-object v0, p0, LX/0Do;->h:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31126
    :goto_0
    monitor-exit p0

    return-void

    .line 31127
    :cond_0
    :try_start_1
    const-string v0, "BrowserHtmlResourceExtractor"

    const-string v1, "Too many extract resource requests, dropping current one"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 31128
    sget-boolean p1, LX/0Dg;->a:Z

    if-eqz p1, :cond_1

    .line 31129
    invoke-static {v1, v2}, LX/0Dg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 31130
    :cond_1
    goto :goto_0

    .line 31131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 31132
    :cond_2
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LX/0Do;->i:Z

    .line 31133
    iget-object v0, p0, LX/0Do;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;-><init>(LX/0Do;Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;)V

    const v2, 0x6e0097da

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
