.class public final LX/01O;
.super Ljava/io/ByteArrayOutputStream;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4647
    invoke-direct {p0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 4648
    return-void
.end method


# virtual methods
.method public final flush()V
    .locals 2

    .prologue
    .line 4640
    iget v0, p0, Ljava/io/ByteArrayOutputStream;->count:I

    iget-object v1, p0, Ljava/io/ByteArrayOutputStream;->buf:[B

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 4641
    iget-object v0, p0, Ljava/io/ByteArrayOutputStream;->buf:[B

    .line 4642
    invoke-static {v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->setBreakpadStreamData([B)V

    .line 4643
    :goto_0
    return-void

    .line 4644
    :cond_0
    invoke-virtual {p0}, LX/01O;->toByteArray()[B

    move-result-object v0

    .line 4645
    invoke-static {v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->setBreakpadStreamData([B)V

    .line 4646
    goto :goto_0
.end method
