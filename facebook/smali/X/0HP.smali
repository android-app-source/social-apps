.class public LX/0HP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/05O;


# static fields
.field public static final a:LX/06Z;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field public final d:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public h:LX/0HK;

.field public final i:Landroid/os/Handler;

.field public final j:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final l:Ljava/lang/Runnable;

.field private final m:LX/0HL;

.field public final n:LX/0HM;

.field private final o:LX/0IY;

.field public final p:Landroid/content/SharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37687
    invoke-static {}, LX/06W;->a()LX/06W;

    move-result-object v0

    invoke-virtual {v0}, LX/06W;->b()LX/06Z;

    move-result-object v0

    sput-object v0, LX/0HP;->a:LX/06Z;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;LX/05N;LX/0IY;Landroid/content/SharedPreferences;LX/05N;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "LX/05N",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/rti/common/analytics/defaultlogger/AnalyticsSamplePolicy;",
            "Landroid/content/SharedPreferences;",
            "LX/05N",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37688
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37689
    new-instance v1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v1, p0, LX/0HP;->j:Ljava/util/Queue;

    .line 37690
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, LX/0HP;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 37691
    iput-object p1, p0, LX/0HP;->b:Landroid/content/Context;

    .line 37692
    iput-object p2, p0, LX/0HP;->c:Ljava/lang/String;

    .line 37693
    iput-object p5, p0, LX/0HP;->p:Landroid/content/SharedPreferences;

    .line 37694
    iput-object p6, p0, LX/0HP;->d:LX/05N;

    .line 37695
    move-object/from16 v0, p9

    iput-object v0, p0, LX/0HP;->f:Ljava/lang/String;

    .line 37696
    move-object/from16 v0, p8

    iput-object v0, p0, LX/0HP;->e:Ljava/lang/String;

    .line 37697
    move-object/from16 v0, p12

    iput-object v0, p0, LX/0HP;->g:Ljava/lang/String;

    .line 37698
    new-instance v1, LX/0HN;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/0HN;-><init>(LX/0HP;Landroid/os/Looper;)V

    iput-object v1, p0, LX/0HP;->i:Landroid/os/Handler;

    .line 37699
    new-instance v1, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$AnalyticsBackgroundWorker;

    invoke-direct {v1, p0}, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$AnalyticsBackgroundWorker;-><init>(LX/0HP;)V

    iput-object v1, p0, LX/0HP;->l:Ljava/lang/Runnable;

    .line 37700
    new-instance v1, LX/0HL;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/0HP;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, LX/0HL;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, LX/0HP;->m:LX/0HL;

    .line 37701
    new-instance v1, LX/0HM;

    iget-object v3, p0, LX/0HP;->c:Ljava/lang/String;

    move-object v2, p1

    move-object v4, p3

    move-object v5, p7

    move-object/from16 v6, p10

    move-object/from16 v7, p11

    invoke-direct/range {v1 .. v7}, LX/0HM;-><init>(Landroid/content/Context;Ljava/lang/String;LX/05N;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, LX/0HP;->n:LX/0HM;

    .line 37702
    iput-object p4, p0, LX/0HP;->o:LX/0IY;

    .line 37703
    invoke-direct {p0}, LX/0HP;->c()V

    .line 37704
    return-void
.end method

.method public static a(Landroid/content/Context;LX/0IY;Landroid/content/SharedPreferences;Ljava/lang/String;LX/05N;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0HP;
    .locals 14
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/rti/common/analytics/defaultlogger/AnalyticsSamplePolicy;",
            "Landroid/content/SharedPreferences;",
            "Ljava/lang/String;",
            "LX/05N",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/0HP;"
        }
    .end annotation

    .prologue
    .line 37705
    invoke-static {p0}, LX/05T;->a(Landroid/content/Context;)LX/05T;

    move-result-object v2

    .line 37706
    new-instance v3, LX/05a;

    move-object/from16 v0, p7

    invoke-direct {v3, p0, v2, v0}, LX/05a;-><init>(Landroid/content/Context;LX/05T;Ljava/lang/String;)V

    .line 37707
    new-instance v7, LX/0HO;

    move-object/from16 v0, p5

    invoke-direct {v7, v0}, LX/0HO;-><init>(Ljava/lang/String;)V

    .line 37708
    new-instance v1, LX/0HP;

    invoke-virtual {v3}, LX/05a;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2}, LX/05T;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2}, LX/05T;->b()Ljava/lang/String;

    move-result-object v10

    const-string v11, "725056107548211"

    const-string v12, "0e20c3123a90c76c02c901b7415ff67f"

    move-object v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object v5, p1

    move-object/from16 v6, p2

    move-object/from16 v13, p6

    invoke-direct/range {v1 .. v13}, LX/0HP;-><init>(Landroid/content/Context;Ljava/lang/String;LX/05N;LX/0IY;Landroid/content/SharedPreferences;LX/05N;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 37709
    invoke-static {p0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "0"

    :cond_0
    return-object p0
.end method

.method public static a$redex0(LX/0HP;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 37710
    iget-object v0, p0, LX/0HP;->j:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 37711
    iget-object v0, p0, LX/0HP;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 p1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37712
    sget-object v0, LX/0HP;->a:LX/06Z;

    iget-object v1, p0, LX/0HP;->l:Ljava/lang/Runnable;

    const p1, -0x69fdf782

    invoke-static {v0, v1, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 37713
    :cond_0
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 37714
    iget-object v0, p0, LX/0HP;->h:LX/0HK;

    if-eqz v0, :cond_0

    .line 37715
    invoke-static {p0}, LX/0HP;->e$redex0(LX/0HP;)V

    .line 37716
    :cond_0
    new-instance v0, LX/0HK;

    invoke-direct {v0}, LX/0HK;-><init>()V

    .line 37717
    iget-object v1, p0, LX/0HP;->e:Ljava/lang/String;

    .line 37718
    iput-object v1, v0, LX/0HK;->e:Ljava/lang/String;

    .line 37719
    iget-object v1, p0, LX/0HP;->f:Ljava/lang/String;

    .line 37720
    iput-object v1, v0, LX/0HK;->f:Ljava/lang/String;

    .line 37721
    iget-object v1, p0, LX/0HP;->p:Landroid/content/SharedPreferences;

    const-string v2, "fb_uid"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0HP;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 37722
    iput-object v1, v0, LX/0HK;->h:Ljava/lang/String;

    .line 37723
    iget-object v1, p0, LX/0HP;->g:Ljava/lang/String;

    .line 37724
    iput-object v1, v0, LX/0HK;->g:Ljava/lang/String;

    .line 37725
    iget-object v1, p0, LX/0HP;->d:LX/05N;

    .line 37726
    iput-object v1, v0, LX/0HK;->d:LX/05N;

    .line 37727
    move-object v0, v0

    .line 37728
    iput-object v0, p0, LX/0HP;->h:LX/0HK;

    .line 37729
    return-void
.end method

.method public static e$redex0(LX/0HP;)V
    .locals 2

    .prologue
    .line 37730
    iget-object v0, p0, LX/0HP;->h:LX/0HK;

    .line 37731
    iget-object v1, v0, LX/0HK;->j:Ljava/util/List;

    move-object v0, v1

    .line 37732
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37733
    iget-object v0, p0, LX/0HP;->m:LX/0HL;

    iget-object v1, p0, LX/0HP;->h:LX/0HK;

    invoke-virtual {v0, v1}, LX/0HL;->a(LX/0HK;)V

    .line 37734
    iget-object v0, p0, LX/0HP;->h:LX/0HK;

    .line 37735
    iget-object v1, v0, LX/0HK;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 37736
    iget v1, v0, LX/0HK;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0HK;->c:I

    .line 37737
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/06d;)V
    .locals 2

    .prologue
    .line 37738
    invoke-static {}, LX/06o;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0HP;->o:LX/0IY;

    .line 37739
    iget-boolean v1, v0, LX/0IY;->a:Z

    move v0, v1

    .line 37740
    if-eqz v0, :cond_0

    .line 37741
    new-instance v0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;

    invoke-direct {v0, p0, p1}, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;-><init>(LX/0HP;LX/06d;)V

    invoke-static {p0, v0}, LX/0HP;->a$redex0(LX/0HP;Ljava/lang/Runnable;)V

    .line 37742
    :cond_0
    return-void
.end method
