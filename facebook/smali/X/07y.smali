.class public LX/07y;
.super LX/16B;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile q:LX/07y;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Uh;

.field public final d:LX/09k;

.field public final e:LX/0Xl;

.field public final f:Landroid/content/Context;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0dC;

.field public final i:Ljava/util/concurrent/ExecutorService;

.field public final j:LX/08X;

.field private final k:LX/09m;

.field public final l:LX/09n;

.field public final m:Ljava/lang/Runnable;

.field private n:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field public o:I

.field private final p:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20427
    const-class v0, LX/07y;

    sput-object v0, LX/07y;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Uh;LX/09k;LX/0Xl;LX/0Or;LX/0dC;Ljava/util/concurrent/ExecutorService;Landroid/content/Context;LX/08X;LX/0ad;LX/1qm;)V
    .locals 4
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/09k;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "Ljava/util/concurrent/ExecutorService;",
            "Landroid/content/Context;",
            "LX/08X;",
            "LX/0ad;",
            "LX/1qm;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 20428
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 20429
    new-instance v0, Lcom/facebook/rti/orca/MqttLiteInitializer$1;

    invoke-direct {v0, p0}, Lcom/facebook/rti/orca/MqttLiteInitializer$1;-><init>(LX/07y;)V

    iput-object v0, p0, LX/07y;->m:Ljava/lang/Runnable;

    .line 20430
    iput-object p1, p0, LX/07y;->b:LX/0Or;

    .line 20431
    iput-object p2, p0, LX/07y;->c:LX/0Uh;

    .line 20432
    iput-object p3, p0, LX/07y;->d:LX/09k;

    .line 20433
    iput-object p4, p0, LX/07y;->e:LX/0Xl;

    .line 20434
    iput-object p5, p0, LX/07y;->g:LX/0Or;

    .line 20435
    iput-object p6, p0, LX/07y;->h:LX/0dC;

    .line 20436
    iput-object p7, p0, LX/07y;->i:Ljava/util/concurrent/ExecutorService;

    .line 20437
    iput-object p8, p0, LX/07y;->f:Landroid/content/Context;

    .line 20438
    iput-object p9, p0, LX/07y;->j:LX/08X;

    .line 20439
    new-instance v0, LX/09m;

    new-instance v1, LX/08k;

    invoke-direct {v1, p0, p10}, LX/08k;-><init>(LX/07y;LX/0ad;)V

    const/4 v2, -0x1

    invoke-direct {v0, p8, v1, v2}, LX/09m;-><init>(Landroid/content/Context;LX/08e;I)V

    iput-object v0, p0, LX/07y;->k:LX/09m;

    .line 20440
    new-instance v0, LX/09n;

    iget-object v1, p0, LX/07y;->f:Landroid/content/Context;

    iget-object v2, p0, LX/07y;->k:LX/09m;

    invoke-virtual {p11}, LX/1qm;->b()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/09n;-><init>(Landroid/content/Context;LX/09m;I)V

    iput-object v0, p0, LX/07y;->l:LX/09n;

    .line 20441
    iput-object p10, p0, LX/07y;->p:LX/0ad;

    .line 20442
    return-void
.end method

.method public static a(LX/0QB;)LX/07y;
    .locals 3

    .prologue
    .line 20447
    sget-object v0, LX/07y;->q:LX/07y;

    if-nez v0, :cond_1

    .line 20448
    const-class v1, LX/07y;

    monitor-enter v1

    .line 20449
    :try_start_0
    sget-object v0, LX/07y;->q:LX/07y;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 20450
    if-eqz v2, :cond_0

    .line 20451
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/07y;->b(LX/0QB;)LX/07y;

    move-result-object v0

    sput-object v0, LX/07y;->q:LX/07y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 20452
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 20453
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 20454
    :cond_1
    sget-object v0, LX/07y;->q:LX/07y;

    return-object v0

    .line 20455
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 20456
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/07y;Z)V
    .locals 3

    .prologue
    .line 20443
    iget-object v0, p0, LX/07y;->n:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 20444
    iget-object v0, p0, LX/07y;->n:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 20445
    :cond_0
    iget-object v0, p0, LX/07y;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/orca/MqttLiteInitializer$5;

    invoke-direct {v1, p0, p1}, Lcom/facebook/rti/orca/MqttLiteInitializer$5;-><init>(LX/07y;Z)V

    const v2, 0x7a52bafc

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, LX/07y;->n:Ljava/util/concurrent/Future;

    .line 20446
    return-void
.end method

.method private static b(LX/0QB;)LX/07y;
    .locals 12

    .prologue
    .line 20457
    new-instance v0, LX/07y;

    const/16 v1, 0x2fd

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static {p0}, LX/09k;->a(LX/0QB;)LX/09k;

    move-result-object v3

    check-cast v3, LX/09k;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    const/16 v5, 0x15e7

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v6

    check-cast v6, LX/0dC;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    const-class v8, Landroid/content/Context;

    invoke-interface {p0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {p0}, LX/08X;->a(LX/0QB;)LX/08X;

    move-result-object v9

    check-cast v9, LX/08X;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {p0}, LX/1qm;->a(LX/0QB;)LX/1qm;

    move-result-object v11

    check-cast v11, LX/1qm;

    invoke-direct/range {v0 .. v11}, LX/07y;-><init>(LX/0Or;LX/0Uh;LX/09k;LX/0Xl;LX/0Or;LX/0dC;Ljava/util/concurrent/ExecutorService;Landroid/content/Context;LX/08X;LX/0ad;LX/1qm;)V

    .line 20458
    return-object v0
.end method

.method public static k(LX/07y;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 20408
    iget-object v0, p0, LX/07y;->d:LX/09k;

    invoke-virtual {v0}, LX/09k;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 20409
    :cond_0
    :goto_0
    return-void

    .line 20410
    :cond_1
    iget-object v0, p0, LX/07y;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 20411
    invoke-direct {p0}, LX/07y;->n()V

    .line 20412
    invoke-direct {p0}, LX/07y;->o()Z

    move-result v0

    if-nez v0, :cond_2

    .line 20413
    iget-object v0, p0, LX/07y;->k:LX/09m;

    invoke-virtual {v0}, LX/09m;->b()V

    goto :goto_0

    .line 20414
    :cond_2
    iget-object v0, p0, LX/07y;->d:LX/09k;

    invoke-virtual {v0}, LX/09k;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 20415
    iget-object v0, p0, LX/07y;->k:LX/09m;

    invoke-virtual {v0}, LX/09m;->a()V

    goto :goto_0

    .line 20416
    :cond_3
    iget-object v0, p0, LX/07y;->d:LX/09k;

    .line 20417
    iget-boolean v1, v0, LX/09k;->f:Z

    move v0, v1

    .line 20418
    if-nez v0, :cond_0

    .line 20419
    iget-object v0, p0, LX/07y;->c:LX/0Uh;

    const/16 v1, 0x7d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 20420
    iget-object v0, p0, LX/07y;->k:LX/09m;

    invoke-virtual {v0}, LX/09m;->a()V

    goto :goto_0

    .line 20421
    :cond_4
    iget-object v0, p0, LX/07y;->k:LX/09m;

    invoke-virtual {v0}, LX/09m;->b()V

    goto :goto_0
.end method

.method public static l(LX/07y;)V
    .locals 7

    .prologue
    .line 20422
    iget-object v0, p0, LX/07y;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/07y;->h:LX/0dC;

    invoke-virtual {v0}, LX/0dC;->a()Ljava/lang/String;

    iget-object v0, p0, LX/07y;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    .line 20423
    iget-object v0, p0, LX/07y;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const/4 v2, 0x1

    .line 20424
    :goto_0
    iget-object v0, p0, LX/07y;->f:Landroid/content/Context;

    iget-object v1, p0, LX/07y;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, LX/07y;->h:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, LX/07y;->o:I

    iget-object v5, p0, LX/07y;->d:LX/09k;

    invoke-virtual {v5}, LX/09k;->j()I

    move-result v5

    iget-object v6, p0, LX/07y;->d:LX/09k;

    invoke-virtual {v6}, LX/09k;->k()I

    move-result v6

    invoke-static/range {v0 .. v6}, LX/09o;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;III)V

    .line 20425
    return-void

    .line 20426
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 20402
    iget-object v0, p0, LX/07y;->p:LX/0ad;

    sget v1, LX/08m;->a:I

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v0

    .line 20403
    iget-object v1, p0, LX/07y;->f:Landroid/content/Context;

    sget-object v2, LX/01p;->c:LX/01q;

    invoke-static {v1, v2}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 20404
    const-string v2, "notification_store_class"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 20405
    if-eq v0, v2, :cond_0

    .line 20406
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "notification_store_class"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 20407
    :cond_0
    return-void
.end method

.method private o()Z
    .locals 9

    .prologue
    .line 20394
    iget-object v0, p0, LX/07y;->c:LX/0Uh;

    const/16 v1, 0x7a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20395
    iget-object v0, p0, LX/07y;->f:Landroid/content/Context;

    const/4 v6, 0x1

    .line 20396
    invoke-static {v0}, LX/04u;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 20397
    invoke-static {v7}, LX/04u;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 20398
    invoke-static {v7}, Lcom/facebook/rti/push/service/FbnsService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "FbnsSuspendSwitch"

    const-string v8, "com.facebook.rti.intent.ACTION_FBNS_KILL_SWITCH_DISABLE_SERVICE"

    move-object v3, v0

    invoke-static/range {v3 .. v8}, LX/09o;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 20399
    const/4 v6, 0x0

    .line 20400
    :cond_0
    move v0, v6

    .line 20401
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 20392
    iget-object v0, p0, LX/07y;->i:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/07y;->m:Ljava/lang/Runnable;

    const v2, 0xdb10dba

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 20393
    return-void
.end method

.method public final a(Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 3
    .param p1    # Lcom/facebook/auth/component/AuthenticationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 20388
    iget-object v0, p0, LX/07y;->d:LX/09k;

    invoke-virtual {v0}, LX/09k;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 20389
    :goto_0
    return-void

    .line 20390
    :cond_0
    invoke-static {p0}, LX/07y;->l(LX/07y;)V

    .line 20391
    iget-object v0, p0, LX/07y;->i:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/07y;->m:Ljava/lang/Runnable;

    const v2, -0x54dcfde

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 20375
    iget-object v0, p0, LX/07y;->d:LX/09k;

    invoke-virtual {v0}, LX/09k;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 20376
    :goto_0
    return-void

    .line 20377
    :cond_0
    iget-object v0, p0, LX/07y;->l:LX/09n;

    invoke-virtual {v0}, LX/09n;->b()V

    .line 20378
    iget-object v0, p0, LX/07y;->k:LX/09m;

    .line 20379
    iget-object v1, v0, LX/09m;->c:LX/08n;

    .line 20380
    iget-object v2, v1, LX/08n;->d:LX/09m;

    invoke-virtual {v2}, LX/09m;->d()V

    .line 20381
    iget-object v2, v1, LX/08n;->d:LX/09m;

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, LX/09m;->a(Z)V

    .line 20382
    iget-object v2, v1, LX/08n;->a:Landroid/content/Context;

    .line 20383
    sget-object v0, LX/01p;->p:LX/01q;

    invoke-static {v2, v0}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 20384
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 20385
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/07y;->a$redex0(LX/07y;Z)V

    .line 20386
    iget-object v0, p0, LX/07y;->f:Landroid/content/Context;

    invoke-static {v0}, LX/09o;->b(Landroid/content/Context;)V

    .line 20387
    goto :goto_0
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 20372
    iget-object v0, p0, LX/07y;->d:LX/09k;

    invoke-virtual {v0}, LX/09k;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 20373
    :goto_0
    return-void

    .line 20374
    :cond_0
    iget-object v0, p0, LX/07y;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/orca/MqttLiteInitializer$3;

    invoke-direct {v1, p0}, Lcom/facebook/rti/orca/MqttLiteInitializer$3;-><init>(LX/07y;)V

    const v2, 0x8971b26

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
