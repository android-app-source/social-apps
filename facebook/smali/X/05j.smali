.class public LX/05j;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field public final c:LX/01o;

.field public volatile d:Ljava/lang/String;

.field public final e:Ljava/util/concurrent/atomic/AtomicLong;

.field public final f:Ljava/util/concurrent/atomic/AtomicLong;

.field public final g:Ljava/util/concurrent/atomic/AtomicLong;

.field public final h:Ljava/util/concurrent/atomic/AtomicLong;

.field public final i:Ljava/util/concurrent/atomic/AtomicLong;

.field private final j:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;LX/01o;)V
    .locals 1

    .prologue
    .line 16749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16750
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, LX/05j;->e:Ljava/util/concurrent/atomic/AtomicLong;

    .line 16751
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, LX/05j;->f:Ljava/util/concurrent/atomic/AtomicLong;

    .line 16752
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, LX/05j;->g:Ljava/util/concurrent/atomic/AtomicLong;

    .line 16753
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, LX/05j;->h:Ljava/util/concurrent/atomic/AtomicLong;

    .line 16754
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, LX/05j;->i:Ljava/util/concurrent/atomic/AtomicLong;

    .line 16755
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, LX/05j;->j:Ljava/util/concurrent/atomic/AtomicLong;

    .line 16756
    iput-object p1, p0, LX/05j;->a:Landroid/content/Context;

    .line 16757
    iput-object p2, p0, LX/05j;->b:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 16758
    iput-object p3, p0, LX/05j;->c:LX/01o;

    .line 16759
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 16760
    iget-object v0, p0, LX/05j;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 16761
    iget-object v0, p0, LX/05j;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 16762
    if-nez p1, :cond_0

    .line 16763
    invoke-static {p0}, LX/05j;->g(LX/05j;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "snapshot_reported"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 16764
    :cond_0
    return-void
.end method

.method public static f(LX/05j;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 16765
    iget-object v0, p0, LX/05j;->a:Landroid/content/Context;

    sget-object v1, LX/01p;->n:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private static g(LX/05j;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 16766
    iget-object v0, p0, LX/05j;->a:Landroid/content/Context;

    sget-object v1, LX/01p;->h:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/0Hb;
    .locals 24

    .prologue
    .line 16767
    invoke-static/range {p0 .. p0}, LX/05j;->g(LX/05j;)Landroid/content/SharedPreferences;

    move-result-object v15

    .line 16768
    const-string v2, "snapshot_reported"

    const/4 v3, 0x0

    invoke-interface {v15, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v20

    .line 16769
    if-nez v20, :cond_2

    .line 16770
    const-string v2, "snapshot_service_state"

    const/4 v3, 0x0

    invoke-interface {v15, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    .line 16771
    const-string v2, "snapshot_connection_state"

    const/4 v3, 0x0

    invoke-interface {v15, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 16772
    const-string v2, "snapshot_service_gap"

    const-wide/16 v4, 0x0

    invoke-interface {v15, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v16

    .line 16773
    if-nez v12, :cond_0

    .line 16774
    move-object/from16 v0, p0

    iget-object v2, v0, LX/05j;->f:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    sub-long v16, v2, v16

    .line 16775
    :cond_0
    const-string v2, "snapshot_connection_gap"

    const-wide/16 v4, 0x0

    invoke-interface {v15, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v18

    .line 16776
    const-string v2, "CONNECTED"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 16777
    move-object/from16 v0, p0

    iget-object v2, v0, LX/05j;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    sub-long v18, v2, v18

    .line 16778
    :cond_1
    new-instance v2, LX/0Hb;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/05j;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/05j;->e:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05j;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, LX/05j;->f:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v8

    sub-long/2addr v6, v8

    move-object/from16 v0, p0

    iget-object v8, v0, LX/05j;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v10, v0, LX/05j;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v10

    sub-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-object v10, v0, LX/05j;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v14, v0, LX/05j;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v14}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v22

    sub-long v10, v10, v22

    invoke-static {v12}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v12

    const-string v14, "snapshot_network_type"

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-interface {v15, v14, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v21, "snapshot_mqtt_network_type"

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-interface {v15, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-direct/range {v2 .. v19}, LX/0Hb;-><init>(Ljava/lang/String;JJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 16779
    :goto_0
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, LX/05j;->a(Z)V

    .line 16780
    return-object v2

    .line 16781
    :cond_2
    new-instance v2, LX/0Hb;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/05j;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/05j;->e:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05j;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, LX/05j;->f:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v8

    sub-long/2addr v6, v8

    move-object/from16 v0, p0

    iget-object v8, v0, LX/05j;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v10, v0, LX/05j;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v10

    sub-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-object v10, v0, LX/05j;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, LX/05j;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v12}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v12

    sub-long/2addr v10, v12

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const-wide/16 v16, 0x0

    const-wide/16 v18, 0x0

    invoke-direct/range {v2 .. v19}, LX/0Hb;-><init>(Ljava/lang/String;JJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 16782
    invoke-static {p0}, LX/05j;->f(LX/05j;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_seen"

    iget-object v2, p0, LX/05j;->c:LX/01o;

    invoke-virtual {v2}, LX/01o;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 16783
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 16784
    iget-object v0, p0, LX/05j;->j:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v1, p0, LX/05j;->b:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 16785
    return-void
.end method
