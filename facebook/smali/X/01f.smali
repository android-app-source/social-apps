.class public final LX/01f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/01g;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 5

    .prologue
    .line 5028
    check-cast p1, LX/01g;

    check-cast p2, LX/01g;

    .line 5029
    iget-wide v1, p1, LX/01g;->lastModifiedTime:J

    iget-wide v3, p2, LX/01g;->lastModifiedTime:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 5030
    const/4 v1, 0x0

    .line 5031
    :goto_0
    move v0, v1

    .line 5032
    return v0

    :cond_0
    iget-wide v1, p1, LX/01g;->lastModifiedTime:J

    iget-wide v3, p2, LX/01g;->lastModifiedTime:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    const/4 v1, -0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
