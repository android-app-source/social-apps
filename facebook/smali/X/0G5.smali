.class public LX/0G5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-java.util.ArrayList._Constructor",
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[Ljava/lang/String;


# instance fields
.field private c:LX/0G4;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34256
    const-class v0, LX/0G5;

    sput-object v0, LX/0G5;->a:Ljava/lang/Class;

    .line 34257
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "-d"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "-v"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "threadtime"

    aput-object v2, v0, v1

    sput-object v0, LX/0G5;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34259
    new-instance v0, LX/0G4;

    sget-object v1, LX/0G5;->b:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0G4;-><init>(Ljava/util/List;)V

    iput-object v0, p0, LX/0G5;->c:LX/0G4;

    .line 34260
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34261
    iget-object v0, p0, LX/0G5;->c:LX/0G4;

    invoke-virtual {v0}, LX/0G4;->a()V

    .line 34262
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 34263
    const/4 v3, 0x0

    .line 34264
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    iget-object v4, p0, LX/0G5;->c:LX/0G4;

    .line 34265
    iget-object v5, v4, LX/0G4;->b:Ljava/lang/Process;

    invoke-virtual {v5}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    move-object v4, v5

    .line 34266
    const-string v5, "US-ASCII"

    invoke-direct {v1, v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34267
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 34268
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 34269
    :catch_0
    move-exception v1

    .line 34270
    :try_start_2
    sget-object v3, LX/0G5;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "unexpected error"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 34271
    :cond_0
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 34272
    :goto_1
    iget-object v1, p0, LX/0G5;->c:LX/0G4;

    invoke-virtual {v1}, LX/0G4;->c()V

    .line 34273
    :goto_2
    return-object v0

    .line 34274
    :catch_1
    move-exception v1

    .line 34275
    :try_start_4
    sget-object v2, LX/0G5;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "unexpected error"

    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 34276
    iget-object v1, p0, LX/0G5;->c:LX/0G4;

    invoke-virtual {v1}, LX/0G4;->c()V

    goto :goto_2

    .line 34277
    :catchall_0
    move-exception v0

    move-object v1, v3

    :goto_3
    if-eqz v1, :cond_1

    .line 34278
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 34279
    :cond_1
    :goto_4
    iget-object v1, p0, LX/0G5;->c:LX/0G4;

    invoke-virtual {v1}, LX/0G4;->c()V

    throw v0

    :catch_2
    goto :goto_1

    :catch_3
    goto :goto_4

    .line 34280
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3
.end method
