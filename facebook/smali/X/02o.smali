.class public final LX/02o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Pc;
.implements LX/02p;
.implements LX/02q;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-java.lang.Thread.start"
    }
.end annotation


# static fields
.field public static final c:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/02o;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Pb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private b:Z

.field private d:LX/0Pb;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public e:LX/0Pd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private f:LX/02s;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:LX/02t;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:LX/02u;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private i:LX/036;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final j:Lcom/facebook/loom/provider/SystemCounterThread;

.field private final k:LX/0Pe;

.field private final l:Lcom/facebook/loom/yarn/PerfEventsSession;

.field private final m:Lcom/facebook/loom/provider/NetworkProvider;

.field private final n:Ljava/util/Random;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9485
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, LX/02o;->c:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;LX/0Pb;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9473
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/02o;->b:Z

    .line 9474
    iput-object p2, p0, LX/02o;->d:LX/0Pb;

    .line 9475
    iput-object v1, p0, LX/02o;->e:LX/0Pd;

    .line 9476
    new-instance v0, LX/02s;

    invoke-direct {v0, p1}, LX/02s;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/02o;->f:LX/02s;

    .line 9477
    iput-object v1, p0, LX/02o;->g:LX/02t;

    .line 9478
    iput-object v1, p0, LX/02o;->h:LX/02u;

    .line 9479
    new-instance v0, Lcom/facebook/loom/provider/SystemCounterThread;

    invoke-direct {v0}, Lcom/facebook/loom/provider/SystemCounterThread;-><init>()V

    iput-object v0, p0, LX/02o;->j:Lcom/facebook/loom/provider/SystemCounterThread;

    .line 9480
    new-instance v0, LX/0Pe;

    invoke-direct {v0, p1}, LX/0Pe;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/02o;->k:LX/0Pe;

    .line 9481
    new-instance v0, Lcom/facebook/loom/provider/NetworkProvider;

    invoke-direct {v0}, Lcom/facebook/loom/provider/NetworkProvider;-><init>()V

    iput-object v0, p0, LX/02o;->m:Lcom/facebook/loom/provider/NetworkProvider;

    .line 9482
    new-instance v0, Lcom/facebook/loom/yarn/PerfEventsSession;

    invoke-direct {v0}, Lcom/facebook/loom/yarn/PerfEventsSession;-><init>()V

    iput-object v0, p0, LX/02o;->l:Lcom/facebook/loom/yarn/PerfEventsSession;

    .line 9483
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/02o;->n:Ljava/util/Random;

    .line 9484
    return-void
.end method

.method private static declared-synchronized a(LX/02o;LX/0Pb;)V
    .locals 1

    .prologue
    .line 9454
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/02o;->d:LX/0Pb;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 9455
    :goto_0
    monitor-exit p0

    return-void

    .line 9456
    :cond_0
    :try_start_1
    sget-object v0, LX/02r;->b:LX/02r;

    move-object v0, v0

    .line 9457
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/02r;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9458
    iput-object p1, p0, LX/02o;->a:LX/0Pb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 9459
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 9460
    :cond_1
    :try_start_2
    invoke-direct {p0, p1}, LX/02o;->b(LX/0Pb;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized a(LX/02u;)V
    .locals 2

    .prologue
    .line 9486
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/02o;->h:LX/02u;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    .line 9487
    :goto_0
    monitor-exit p0

    return-void

    .line 9488
    :cond_0
    :try_start_1
    iput-object p1, p0, LX/02o;->h:LX/02u;

    .line 9489
    iget-object v0, p0, LX/02o;->h:LX/02u;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/02o;->e:LX/0Pd;

    if-eqz v0, :cond_1

    .line 9490
    iget-object v0, p0, LX/02o;->h:LX/02u;

    iget-object v1, p0, LX/02o;->e:LX/0Pd;

    invoke-interface {v1}, LX/0Pd;->b()Lcom/facebook/loom/config/SystemControlConfiguration;

    move-result-object v1

    invoke-interface {v0, v1}, LX/02u;->a(Lcom/facebook/loom/config/SystemControlConfiguration;)V

    .line 9491
    :cond_1
    invoke-direct {p0}, LX/02o;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 9492
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(Landroid/content/Context;LX/0Pb;)V
    .locals 3
    .param p1    # LX/0Pb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 9493
    if-nez p1, :cond_0

    .line 9494
    new-instance p1, LX/0Pf;

    invoke-direct {p1}, LX/0Pf;-><init>()V

    .line 9495
    :cond_0
    new-instance v0, LX/02o;

    invoke-direct {v0, p0, p1}, LX/02o;-><init>(Landroid/content/Context;LX/0Pb;)V

    .line 9496
    sget-object v1, LX/02o;->c:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 9497
    invoke-direct {v0}, LX/02o;->g()V

    return-void

    .line 9498
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Orchestrator already initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b()LX/02o;
    .locals 2

    .prologue
    .line 9499
    sget-object v0, LX/02o;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/02o;

    .line 9500
    if-nez v0, :cond_0

    .line 9501
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "TraceOrchestrator has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9502
    :cond_0
    return-object v0
.end method

.method private b(LX/0Pb;)V
    .locals 2
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 9571
    iget-object v0, p0, LX/02o;->d:LX/0Pb;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0Pb;->a(LX/0Pc;)V

    .line 9572
    invoke-interface {p1, p0}, LX/0Pb;->a(LX/0Pc;)V

    .line 9573
    iput-object p1, p0, LX/02o;->d:LX/0Pb;

    .line 9574
    invoke-interface {p1}, LX/0Pb;->b()LX/0Pd;

    move-result-object v0

    invoke-direct {p0, v0}, LX/02o;->b(LX/0Pd;)V

    .line 9575
    return-void
.end method

.method private b(LX/0Pd;)V
    .locals 2
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 9503
    iget-object v0, p0, LX/02o;->e:LX/0Pd;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9504
    :cond_0
    :goto_0
    return-void

    .line 9505
    :cond_1
    const/4 v0, -0x1

    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->disableProviders(I)V

    .line 9506
    iput-object p1, p0, LX/02o;->e:LX/0Pd;

    .line 9507
    sget-object v0, LX/02r;->b:LX/02r;

    move-object v0, v0

    .line 9508
    if-nez v0, :cond_2

    .line 9509
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Performing config change before TraceControl has been initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9510
    :cond_2
    invoke-interface {p1}, LX/0Pd;->a()LX/0Pg;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/02r;->a(LX/0Pg;)V

    .line 9511
    invoke-interface {p1}, LX/0Pd;->c()I

    move-result v0

    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->enableProviders(I)V

    .line 9512
    invoke-direct {p0}, LX/02o;->j()LX/02u;

    move-result-object v0

    .line 9513
    if-eqz v0, :cond_0

    .line 9514
    invoke-interface {p1}, LX/0Pd;->b()Lcom/facebook/loom/config/SystemControlConfiguration;

    move-result-object v1

    invoke-interface {v0, v1}, LX/02u;->a(Lcom/facebook/loom/config/SystemControlConfiguration;)V

    goto :goto_0
.end method

.method private g()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 9515
    monitor-enter p0

    .line 9516
    :try_start_0
    iget-object v0, p0, LX/02o;->d:LX/0Pb;

    invoke-interface {v0, p0}, LX/0Pb;->a(LX/0Pc;)V

    .line 9517
    iget-object v0, p0, LX/02o;->d:LX/0Pb;

    invoke-interface {v0}, LX/0Pb;->b()LX/0Pd;

    move-result-object v0

    .line 9518
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9519
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1, v4}, Landroid/util/SparseArray;-><init>(I)V

    .line 9520
    const/4 v2, 0x4

    new-instance v3, LX/02v;

    invoke-direct {v3}, LX/02v;-><init>()V

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 9521
    const/4 v2, 0x2

    new-instance v3, LX/02x;

    invoke-direct {v3}, LX/02x;-><init>()V

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 9522
    new-instance v2, LX/02y;

    invoke-direct {v2}, LX/02y;-><init>()V

    invoke-virtual {v1, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 9523
    const/16 v2, 0x8

    new-instance v3, LX/030;

    invoke-direct {v3}, LX/030;-><init>()V

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 9524
    const/16 v2, 0x10

    new-instance v3, LX/031;

    invoke-direct {v3}, LX/031;-><init>()V

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 9525
    invoke-interface {v0}, LX/0Pd;->a()LX/0Pg;

    move-result-object v2

    invoke-static {v1, p0, v2}, LX/02r;->a(Landroid/util/SparseArray;LX/02q;LX/0Pg;)V

    .line 9526
    monitor-enter p0

    .line 9527
    :try_start_1
    iget-object v1, p0, LX/02o;->f:LX/02s;

    .line 9528
    iget-object v2, v1, LX/02s;->f:Ljava/io/File;

    move-object v1, v2

    .line 9529
    const/16 v2, 0x1388

    invoke-static {v2, v1, p0}, Lcom/facebook/loom/logger/Logger;->a(ILjava/io/File;LX/02o;)V

    .line 9530
    const/4 v1, 0x1

    sput-boolean v1, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    .line 9531
    const/4 v1, 0x1

    sput-boolean v1, LX/032;->a:Z

    .line 9532
    invoke-direct {p0, v0}, LX/02o;->b(LX/0Pd;)V

    .line 9533
    iget-object v0, p0, LX/02o;->f:LX/02s;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    .line 9534
    const-wide/16 v5, 0x3e8

    mul-long/2addr v5, v2

    iput-wide v5, v0, LX/02s;->c:J

    .line 9535
    iget-object v0, p0, LX/02o;->f:LX/02s;

    const/16 v1, 0xa

    .line 9536
    iput v1, v0, LX/02s;->b:I

    .line 9537
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 9538
    new-instance v0, LX/033;

    invoke-direct {v0}, LX/033;-><init>()V

    invoke-static {v0}, LX/00k;->a(LX/00j;)V

    .line 9539
    return-void

    .line 9540
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 9541
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized h()V
    .locals 1

    .prologue
    .line 9542
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/02o;->a:LX/0Pb;

    if-eqz v0, :cond_1

    .line 9543
    iget-object v0, p0, LX/02o;->a:LX/0Pb;

    invoke-direct {p0, v0}, LX/02o;->b(LX/0Pb;)V

    .line 9544
    const/4 v0, 0x0

    iput-object v0, p0, LX/02o;->a:LX/0Pb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9545
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 9546
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/02o;->e:LX/0Pd;

    if-eqz v0, :cond_0

    .line 9547
    const/4 v0, -0x1

    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->disableProviders(I)V

    .line 9548
    iget-object v0, p0, LX/02o;->e:LX/0Pd;

    invoke-interface {v0}, LX/0Pd;->c()I

    move-result v0

    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->enableProviders(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 9549
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private i()V
    .locals 6
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 9550
    invoke-direct {p0}, LX/02o;->j()LX/02u;

    move-result-object v0

    .line 9551
    if-nez v0, :cond_0

    .line 9552
    :goto_0
    return-void

    .line 9553
    :cond_0
    iget-object v1, p0, LX/02o;->f:LX/02s;

    .line 9554
    invoke-static {v1}, LX/02s;->g(LX/02s;)Ljava/io/File;

    move-result-object v2

    .line 9555
    iget-object v3, v1, LX/02s;->f:Ljava/io/File;

    iget-wide v4, v1, LX/02s;->c:J

    invoke-static {v1, v2, v3, v4, v5}, LX/02s;->a(LX/02s;Ljava/io/File;Ljava/io/File;J)V

    .line 9556
    sget-object v3, LX/02s;->d:Ljava/io/FilenameFilter;

    invoke-static {v2, v3}, LX/02s;->a(Ljava/io/File;Ljava/io/FilenameFilter;)Ljava/util/List;

    move-result-object v2

    .line 9557
    new-instance v3, LX/034;

    invoke-direct {v3, v1}, LX/034;-><init>(LX/02s;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 9558
    move-object v1, v2

    .line 9559
    invoke-interface {v0, v1, p0}, LX/02u;->a(Ljava/util/List;LX/02p;)V

    .line 9560
    iget-object v1, p0, LX/02o;->f:LX/02s;

    .line 9561
    invoke-static {v1}, LX/02s;->g(LX/02s;)Ljava/io/File;

    move-result-object v2

    sget-object v3, LX/02s;->e:Ljava/io/FilenameFilter;

    invoke-static {v2, v3}, LX/02s;->a(Ljava/io/File;Ljava/io/FilenameFilter;)Ljava/util/List;

    move-result-object v2

    .line 9562
    new-instance v3, LX/035;

    invoke-direct {v3, v1}, LX/035;-><init>(LX/02s;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 9563
    move-object v1, v2

    .line 9564
    invoke-interface {v0, v1, p0}, LX/02u;->b(Ljava/util/List;LX/02p;)V

    goto :goto_0
.end method

.method private declared-synchronized j()LX/02u;
    .locals 1

    .prologue
    .line 9565
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/02o;->h:LX/02u;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/02o;->i:LX/036;

    if-eqz v0, :cond_0

    .line 9566
    iget-object v0, p0, LX/02o;->i:LX/036;

    invoke-interface {v0}, LX/036;->a()LX/02u;

    move-result-object v0

    .line 9567
    if-eqz v0, :cond_0

    .line 9568
    invoke-direct {p0, v0}, LX/02o;->a(LX/02u;)V

    .line 9569
    :cond_0
    iget-object v0, p0, LX/02o;->h:LX/02u;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 9570
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()LX/02t;
    .locals 1

    .prologue
    .line 9461
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/02o;->g:LX/02t;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/02o;->i:LX/036;

    if-eqz v0, :cond_0

    .line 9462
    iget-object v0, p0, LX/02o;->i:LX/036;

    invoke-interface {v0}, LX/036;->c()LX/02t;

    move-result-object v0

    iput-object v0, p0, LX/02o;->g:LX/02t;

    .line 9463
    :cond_0
    iget-object v0, p0, LX/02o;->g:LX/02t;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 9464
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized l(LX/02o;)LX/0Pb;
    .locals 2

    .prologue
    .line 9465
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/02o;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/02o;->i:LX/036;

    if-eqz v0, :cond_0

    .line 9466
    iget-object v0, p0, LX/02o;->i:LX/036;

    invoke-interface {v0}, LX/036;->b()LX/0Pb;

    move-result-object v0

    .line 9467
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/02o;->b:Z

    .line 9468
    if-eqz v0, :cond_0

    .line 9469
    invoke-static {p0, v0}, LX/02o;->a(LX/02o;LX/0Pb;)V

    .line 9470
    :cond_0
    iget-object v0, p0, LX/02o;->d:LX/0Pb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 9471
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/036;)V
    .locals 1

    .prologue
    .line 9301
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/02o;->i:LX/036;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9302
    monitor-exit p0

    return-void

    .line 9303
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/037;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 9304
    monitor-enter p0

    .line 9305
    :try_start_0
    iget-object v0, p0, LX/02o;->e:LX/0Pd;

    .line 9306
    invoke-direct {p0}, LX/02o;->k()LX/02t;

    move-result-object v1

    .line 9307
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9308
    if-eqz v0, :cond_0

    .line 9309
    invoke-static {v6}, Lcom/facebook/loom/core/TraceEvents;->disableProviders(I)V

    .line 9310
    iget v2, p1, LX/037;->e:I

    invoke-static {v2}, Lcom/facebook/loom/core/TraceEvents;->enableProviders(I)V

    .line 9311
    const/16 v2, 0x3b

    const v3, 0x7c0006

    invoke-interface {v0}, LX/0Pd;->d()J

    move-result-wide v4

    invoke-static {v6, v2, v3, v4, v5}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 9312
    :cond_0
    if-eqz v1, :cond_1

    .line 9313
    invoke-interface {v1, p1}, LX/02q;->a(LX/037;)V

    .line 9314
    :cond_1
    iget-object v0, p0, LX/02o;->j:Lcom/facebook/loom/provider/SystemCounterThread;

    invoke-virtual {v0}, Lcom/facebook/loom/provider/SystemCounterThread;->a()V

    .line 9315
    iget-object v0, p0, LX/02o;->k:LX/0Pe;

    iget v1, p1, LX/037;->h:I

    iget v2, p1, LX/037;->e:I

    invoke-virtual {v0, v1, v2}, LX/0Pe;->a(II)V

    .line 9316
    iget-object v0, p0, LX/02o;->m:Lcom/facebook/loom/provider/NetworkProvider;

    invoke-virtual {v0}, Lcom/facebook/loom/provider/NetworkProvider;->a()V

    .line 9317
    iget-object v0, p0, LX/02o;->l:Lcom/facebook/loom/yarn/PerfEventsSession;

    iget v1, p1, LX/037;->e:I

    invoke-virtual {v0, v1}, Lcom/facebook/loom/yarn/PerfEventsSession;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 9318
    iget-object v0, p0, LX/02o;->l:Lcom/facebook/loom/yarn/PerfEventsSession;

    invoke-virtual {v0}, Lcom/facebook/loom/yarn/PerfEventsSession;->b()V

    .line 9319
    :cond_2
    return-void

    .line 9320
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/038;)V
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 9321
    iget-object v0, p1, LX/038;->b:Ljava/io/File;

    move-object v3, v0

    .line 9322
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 9323
    :cond_0
    :goto_0
    return-void

    .line 9324
    :cond_1
    invoke-virtual {p1}, LX/038;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 9325
    monitor-enter p0

    .line 9326
    :try_start_0
    iget-object v0, p0, LX/02o;->e:LX/0Pd;

    .line 9327
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9328
    if-eqz v0, :cond_a

    invoke-virtual {p1}, LX/038;->f()S

    move-result v4

    const/16 v5, 0x71

    if-ne v4, v5, :cond_a

    .line 9329
    invoke-interface {v0}, LX/0Pd;->a()LX/0Pg;

    move-result-object v0

    invoke-interface {v0}, LX/0Pg;->c()I

    move-result v0

    .line 9330
    if-eqz v0, :cond_3

    iget-object v4, p0, LX/02o;->n:Ljava/util/Random;

    invoke-virtual {v4, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 9331
    :goto_1
    if-nez v0, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v4

    if-nez v4, :cond_2

    .line 9332
    const-string v4, "TraceOrchestrator"

    const-string v5, "Could not delete aborted trace"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 9333
    :cond_2
    sget-object v4, LX/02r;->b:LX/02r;

    move-object v4, v4

    .line 9334
    if-nez v4, :cond_4

    .line 9335
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No TraceControl when cleaning up aborted trace"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9336
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move v0, v2

    .line 9337
    goto :goto_1

    .line 9338
    :cond_4
    iget-wide v8, p1, LX/038;->a:J

    move-wide v6, v8

    .line 9339
    invoke-virtual {p1}, LX/038;->f()S

    move-result v5

    invoke-virtual {v4, v6, v7, v5}, LX/02r;->a(JS)V

    .line 9340
    if-eqz v0, :cond_0

    .line 9341
    :cond_5
    monitor-enter p0

    .line 9342
    :try_start_2
    iget v0, p1, LX/038;->c:I

    move v0, v0

    .line 9343
    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_9

    .line 9344
    :goto_2
    iget-object v0, p0, LX/02o;->f:LX/02s;

    const/4 v12, 0x0

    .line 9345
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    .line 9346
    const/16 v9, 0x2e

    invoke-virtual {v8, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v9

    .line 9347
    const/4 v10, -0x1

    if-eq v9, v10, :cond_6

    .line 9348
    invoke-virtual {v8, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 9349
    :cond_6
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".log"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 9350
    if-nez v1, :cond_7

    .line 9351
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "override-"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 9352
    :cond_7
    invoke-static {v0}, LX/02s;->g(LX/02s;)Ljava/io/File;

    move-result-object v9

    .line 9353
    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-nez v10, :cond_8

    invoke-virtual {v9}, Ljava/io/File;->mkdirs()Z

    move-result v10

    if-eqz v10, :cond_c

    .line 9354
    :cond_8
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v9, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 9355
    invoke-virtual {v3, v10}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 9356
    iget-object v8, v0, LX/02s;->a:LX/039;

    iget v10, v8, LX/039;->g:I

    add-int/lit8 v10, v10, 0x1

    iput v10, v8, LX/039;->g:I

    .line 9357
    :goto_3
    iget-object v8, v0, LX/02s;->f:Ljava/io/File;

    iget-wide v10, v0, LX/02s;->c:J

    invoke-static {v0, v9, v8, v10, v11}, LX/02s;->a(LX/02s;Ljava/io/File;Ljava/io/File;J)V

    .line 9358
    iget-object v8, v0, LX/02s;->f:Ljava/io/File;

    iget v9, v0, LX/02s;->b:I

    invoke-static {v0, v8, v9}, LX/02s;->a(LX/02s;Ljava/io/File;I)V

    .line 9359
    :goto_4
    invoke-direct {p0}, LX/02o;->i()V

    .line 9360
    iget-object v0, p0, LX/02o;->f:LX/02s;

    .line 9361
    iget-object v1, v0, LX/02s;->a:LX/039;

    .line 9362
    new-instance v2, LX/039;

    invoke-direct {v2}, LX/039;-><init>()V

    iput-object v2, v0, LX/02s;->a:LX/039;

    .line 9363
    move-object v0, v1

    .line 9364
    invoke-direct {p0}, LX/02o;->k()LX/02t;

    move-result-object v1

    .line 9365
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 9366
    if-eqz v1, :cond_0

    .line 9367
    invoke-interface {v1}, LX/02t;->a()V

    .line 9368
    iget v2, v0, LX/039;->a:I

    iget v3, v0, LX/039;->b:I

    add-int/2addr v2, v3

    iget v3, v0, LX/039;->c:I

    add-int/2addr v2, v3

    iget v3, v0, LX/039;->d:I

    add-int/2addr v2, v3

    move v2, v2

    .line 9369
    iget v3, v0, LX/039;->e:I

    move v3, v3

    .line 9370
    iget v4, v0, LX/039;->f:I

    move v4, v4

    .line 9371
    iget v5, v0, LX/039;->g:I

    move v0, v5

    .line 9372
    invoke-interface {v1, v2, v3, v4, v0}, LX/02t;->a(IIII)V

    goto/16 :goto_0

    :cond_9
    move v1, v2

    .line 9373
    goto/16 :goto_2

    .line 9374
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_a
    move v0, v2

    goto/16 :goto_1

    .line 9375
    :cond_b
    iget-object v8, v0, LX/02s;->a:LX/039;

    iget v10, v8, LX/039;->b:I

    add-int/lit8 v10, v10, 0x1

    iput v10, v8, LX/039;->b:I

    .line 9376
    const-string v8, "FileManager"

    const-string v10, "Can\'t move file to upload folder: %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    aput-object v3, v11, v12

    invoke-static {v8, v10, v11}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 9377
    :cond_c
    iget-object v8, v0, LX/02s;->a:LX/039;

    iget v9, v8, LX/039;->c:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v8, LX/039;->c:I

    .line 9378
    const-string v8, "FileManager"

    const-string v9, "Couldn\'t create upload directory"

    invoke-static {v8, v9}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method public final declared-synchronized a(LX/0Pd;)V
    .locals 1

    .prologue
    .line 9379
    monitor-enter p0

    .line 9380
    :try_start_0
    sget-object v0, LX/02r;->b:LX/02r;

    move-object v0, v0

    .line 9381
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/02r;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9382
    iget-object v0, p0, LX/02o;->d:LX/0Pb;

    iput-object v0, p0, LX/02o;->a:LX/0Pb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9383
    :goto_0
    monitor-exit p0

    return-void

    .line 9384
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, LX/02o;->b(LX/0Pd;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 9385
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 9386
    monitor-enter p0

    .line 9387
    :try_start_0
    iget-object v0, p0, LX/02o;->f:LX/02s;

    .line 9388
    new-instance v1, Ljava/io/File;

    iget-object v2, v0, LX/02s;->f:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 9389
    invoke-static {v0, p1, v1}, LX/02s;->a(LX/02s;Ljava/io/File;Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9390
    iget-object v1, v0, LX/02s;->f:Ljava/io/File;

    iget v2, v0, LX/02s;->b:I

    invoke-static {v0, v1, v2}, LX/02s;->a(LX/02s;Ljava/io/File;I)V

    .line 9391
    :cond_0
    invoke-direct {p0}, LX/02o;->k()LX/02t;

    move-result-object v0

    .line 9392
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9393
    if-eqz v0, :cond_1

    .line 9394
    invoke-interface {v0, p1}, LX/02p;->a(Ljava/io/File;)V

    .line 9395
    :cond_1
    return-void

    .line 9396
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 9397
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/0Pa;

    invoke-direct {v0, p1}, LX/0Pa;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0}, LX/02o;->a(LX/02o;LX/0Pb;)V

    .line 9398
    iget-object v0, p0, LX/02o;->f:LX/02s;

    .line 9399
    const/4 v1, 0x1

    .line 9400
    invoke-virtual {v0}, LX/02s;->d()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 9401
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 9402
    const/4 v2, 0x0

    .line 9403
    iget-object v1, v0, LX/02s;->a:LX/039;

    iget p1, v1, LX/039;->a:I

    add-int/lit8 p1, p1, 0x1

    iput p1, v1, LX/039;->a:I

    :cond_0
    move v1, v2

    move v2, v1

    .line 9404
    goto :goto_0

    .line 9405
    :cond_1
    move v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9406
    monitor-exit p0

    return v0

    .line 9407
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(LX/037;)V
    .locals 1

    .prologue
    .line 9408
    iget-object v0, p0, LX/02o;->j:Lcom/facebook/loom/provider/SystemCounterThread;

    invoke-virtual {v0}, Lcom/facebook/loom/provider/SystemCounterThread;->b()V

    .line 9409
    iget-object v0, p0, LX/02o;->k:LX/0Pe;

    invoke-virtual {v0}, LX/0Pe;->a()V

    .line 9410
    iget-object v0, p0, LX/02o;->m:Lcom/facebook/loom/provider/NetworkProvider;

    invoke-virtual {v0}, Lcom/facebook/loom/provider/NetworkProvider;->b()V

    .line 9411
    iget-object v0, p0, LX/02o;->l:Lcom/facebook/loom/yarn/PerfEventsSession;

    invoke-virtual {v0}, Lcom/facebook/loom/yarn/PerfEventsSession;->c()V

    .line 9412
    iget-object v0, p0, LX/02o;->l:Lcom/facebook/loom/yarn/PerfEventsSession;

    invoke-virtual {v0}, Lcom/facebook/loom/yarn/PerfEventsSession;->a()V

    .line 9413
    monitor-enter p0

    .line 9414
    :try_start_0
    invoke-direct {p0}, LX/02o;->k()LX/02t;

    move-result-object v0

    .line 9415
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9416
    if-eqz v0, :cond_0

    .line 9417
    invoke-interface {v0, p1}, LX/02q;->b(LX/037;)V

    .line 9418
    :cond_0
    return-void

    .line 9419
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 9420
    monitor-enter p0

    .line 9421
    :try_start_0
    invoke-direct {p0}, LX/02o;->k()LX/02t;

    move-result-object v0

    .line 9422
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9423
    if-eqz v0, :cond_0

    .line 9424
    invoke-interface {v0, p1}, LX/02p;->b(Ljava/io/File;)V

    .line 9425
    :cond_0
    return-void

    .line 9426
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final c(LX/037;)V
    .locals 1

    .prologue
    .line 9427
    invoke-direct {p0}, LX/02o;->h()V

    .line 9428
    monitor-enter p0

    .line 9429
    :try_start_0
    iget-object v0, p0, LX/02o;->g:LX/02t;

    .line 9430
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9431
    if-eqz v0, :cond_0

    .line 9432
    invoke-interface {v0, p1}, LX/02q;->c(LX/037;)V

    .line 9433
    :cond_0
    return-void

    .line 9434
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 2

    .prologue
    .line 9435
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/02o;->i:LX/036;

    if-nez v0, :cond_0

    .line 9436
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Loom Bridge still not set after cold start initialization"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9437
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 9438
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/02o;->m:Lcom/facebook/loom/provider/NetworkProvider;

    iget-object v1, p0, LX/02o;->i:LX/036;

    invoke-interface {v1}, LX/036;->d()LX/03A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/loom/provider/NetworkProvider;->a(LX/03A;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 9439
    monitor-exit p0

    return-void
.end method

.method public final d(LX/037;)V
    .locals 1

    .prologue
    .line 9440
    invoke-direct {p0}, LX/02o;->h()V

    .line 9441
    monitor-enter p0

    .line 9442
    :try_start_0
    invoke-direct {p0}, LX/02o;->k()LX/02t;

    move-result-object v0

    .line 9443
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9444
    if-eqz v0, :cond_0

    .line 9445
    invoke-interface {v0, p1}, LX/02q;->d(LX/037;)V

    .line 9446
    :cond_0
    iget-object v0, p0, LX/02o;->j:Lcom/facebook/loom/provider/SystemCounterThread;

    invoke-virtual {v0}, Lcom/facebook/loom/provider/SystemCounterThread;->b()V

    .line 9447
    iget-object v0, p0, LX/02o;->k:LX/0Pe;

    invoke-virtual {v0}, LX/0Pe;->a()V

    .line 9448
    iget-object v0, p0, LX/02o;->m:Lcom/facebook/loom/provider/NetworkProvider;

    invoke-virtual {v0}, Lcom/facebook/loom/provider/NetworkProvider;->b()V

    .line 9449
    iget-object v0, p0, LX/02o;->l:Lcom/facebook/loom/yarn/PerfEventsSession;

    invoke-virtual {v0}, Lcom/facebook/loom/yarn/PerfEventsSession;->c()V

    .line 9450
    iget-object v0, p0, LX/02o;->l:Lcom/facebook/loom/yarn/PerfEventsSession;

    invoke-virtual {v0}, Lcom/facebook/loom/yarn/PerfEventsSession;->a()V

    .line 9451
    return-void

    .line 9452
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized f()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9453
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/02o;->f:LX/02s;

    invoke-virtual {v0}, LX/02s;->d()Ljava/lang/Iterable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
