.class public final enum LX/0H5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0H5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0H5;

.field public static final enum DASH_LIVE:LX/0H5;

.field public static final enum DASH_VOD:LX/0H5;

.field public static final enum HLS_LIVE:LX/0H5;

.field public static final enum PROGRESSIVE:LX/0H5;

.field public static final enum UNKNOWN:LX/0H5;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36929
    new-instance v0, LX/0H5;

    const-string v1, "DASH_VOD"

    invoke-direct {v0, v1, v2}, LX/0H5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0H5;->DASH_VOD:LX/0H5;

    .line 36930
    new-instance v0, LX/0H5;

    const-string v1, "DASH_LIVE"

    invoke-direct {v0, v1, v3}, LX/0H5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0H5;->DASH_LIVE:LX/0H5;

    .line 36931
    new-instance v0, LX/0H5;

    const-string v1, "HLS_LIVE"

    invoke-direct {v0, v1, v4}, LX/0H5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0H5;->HLS_LIVE:LX/0H5;

    .line 36932
    new-instance v0, LX/0H5;

    const-string v1, "PROGRESSIVE"

    invoke-direct {v0, v1, v5}, LX/0H5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0H5;->PROGRESSIVE:LX/0H5;

    .line 36933
    new-instance v0, LX/0H5;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/0H5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0H5;->UNKNOWN:LX/0H5;

    .line 36934
    const/4 v0, 0x5

    new-array v0, v0, [LX/0H5;

    sget-object v1, LX/0H5;->DASH_VOD:LX/0H5;

    aput-object v1, v0, v2

    sget-object v1, LX/0H5;->DASH_LIVE:LX/0H5;

    aput-object v1, v0, v3

    sget-object v1, LX/0H5;->HLS_LIVE:LX/0H5;

    aput-object v1, v0, v4

    sget-object v1, LX/0H5;->PROGRESSIVE:LX/0H5;

    aput-object v1, v0, v5

    sget-object v1, LX/0H5;->UNKNOWN:LX/0H5;

    aput-object v1, v0, v6

    sput-object v0, LX/0H5;->$VALUES:[LX/0H5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36935
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static isLive(LX/0H5;)Z
    .locals 1

    .prologue
    .line 36936
    sget-object v0, LX/0H5;->DASH_LIVE:LX/0H5;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0H5;->HLS_LIVE:LX/0H5;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0H5;
    .locals 1

    .prologue
    .line 36937
    const-class v0, LX/0H5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0H5;

    return-object v0
.end method

.method public static values()[LX/0H5;
    .locals 1

    .prologue
    .line 36938
    sget-object v0, LX/0H5;->$VALUES:[LX/0H5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0H5;

    return-object v0
.end method
