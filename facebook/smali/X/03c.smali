.class public final LX/03c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/00j;


# instance fields
.field public final synthetic a:LX/03S;

.field private b:LX/03T;


# direct methods
.method public constructor <init>(LX/03S;)V
    .locals 0

    .prologue
    .line 10575
    iput-object p1, p0, LX/03c;->a:LX/03S;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 10576
    monitor-enter p0

    const-wide/16 v0, 0x1000

    :try_start_0
    invoke-static {v0, v1}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10577
    invoke-virtual {p0}, LX/03c;->b()V

    .line 10578
    iget-object v0, p0, LX/03c;->a:LX/03S;

    invoke-virtual {v0}, LX/03S;->a()LX/03T;

    move-result-object v0

    iput-object v0, p0, LX/03c;->b:LX/03T;

    .line 10579
    iget-object v0, p0, LX/03c;->b:LX/03T;

    invoke-virtual {v0}, LX/03T;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10580
    :cond_0
    monitor-exit p0

    return-void

    .line 10581
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 10582
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/03c;->b:LX/03T;

    if-eqz v0, :cond_0

    .line 10583
    iget-object v0, p0, LX/03c;->b:LX/03T;

    invoke-virtual {v0}, LX/03T;->d()V

    .line 10584
    const/4 v0, 0x0

    iput-object v0, p0, LX/03c;->b:LX/03T;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10585
    :cond_0
    monitor-exit p0

    return-void

    .line 10586
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
