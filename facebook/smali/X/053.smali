.class public final enum LX/053;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/053;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/053;

.field public static final enum CONNECTED:LX/053;

.field public static final enum CONNECTING:LX/053;

.field public static final enum CONNECT_SENT:LX/053;

.field public static final enum DISCONNECTED:LX/053;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15293
    new-instance v0, LX/053;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v2}, LX/053;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/053;->CONNECTING:LX/053;

    .line 15294
    new-instance v0, LX/053;

    const-string v1, "CONNECT_SENT"

    invoke-direct {v0, v1, v3}, LX/053;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/053;->CONNECT_SENT:LX/053;

    .line 15295
    new-instance v0, LX/053;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v4}, LX/053;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/053;->CONNECTED:LX/053;

    .line 15296
    new-instance v0, LX/053;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v5}, LX/053;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/053;->DISCONNECTED:LX/053;

    .line 15297
    const/4 v0, 0x4

    new-array v0, v0, [LX/053;

    sget-object v1, LX/053;->CONNECTING:LX/053;

    aput-object v1, v0, v2

    sget-object v1, LX/053;->CONNECT_SENT:LX/053;

    aput-object v1, v0, v3

    sget-object v1, LX/053;->CONNECTED:LX/053;

    aput-object v1, v0, v4

    sget-object v1, LX/053;->DISCONNECTED:LX/053;

    aput-object v1, v0, v5

    sput-object v0, LX/053;->$VALUES:[LX/053;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15292
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/053;
    .locals 1

    .prologue
    .line 15298
    const-class v0, LX/053;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/053;

    return-object v0
.end method

.method public static values()[LX/053;
    .locals 1

    .prologue
    .line 15291
    sget-object v0, LX/053;->$VALUES:[LX/053;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/053;

    return-object v0
.end method
