.class public LX/0Dm;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/0Dm;


# instance fields
.field public b:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

.field private c:LX/0CQ;

.field private d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31016
    invoke-static {}, LX/0CQ;->a()LX/0CQ;

    move-result-object v0

    iput-object v0, p0, LX/0Dm;->c:LX/0CQ;

    .line 31017
    return-void
.end method

.method public static a()LX/0Dm;
    .locals 1

    .prologue
    .line 31018
    sget-object v0, LX/0Dm;->a:LX/0Dm;

    if-nez v0, :cond_0

    .line 31019
    new-instance v0, LX/0Dm;

    invoke-direct {v0}, LX/0Dm;-><init>()V

    sput-object v0, LX/0Dm;->a:LX/0Dm;

    .line 31020
    :cond_0
    sget-object v0, LX/0Dm;->a:LX/0Dm;

    return-object v0
.end method

.method public static b(Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;)Landroid/webkit/WebResourceResponse;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 31021
    iget-object v1, p0, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;->b:Ljava/lang/String;

    move-object v1, v1

    .line 31022
    if-nez v1, :cond_0

    .line 31023
    :goto_0
    return-object v0

    .line 31024
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v2, Ljava/io/FileInputStream;

    .line 31025
    iget-object v3, p0, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;->b:Ljava/lang/String;

    move-object v3, v3

    .line 31026
    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 31027
    new-instance v0, Landroid/webkit/WebResourceResponse;

    .line 31028
    iget-object v2, p0, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;->c:Ljava/lang/String;

    move-object v2, v2

    .line 31029
    iget-object v3, p0, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;->d:Ljava/lang/String;

    move-object v3, v3

    .line 31030
    invoke-direct {v0, v2, v3, v1}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    goto :goto_0

    .line 31031
    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 31032
    iget-object v1, p0, LX/0Dm;->b:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    .line 31033
    if-eqz v1, :cond_1

    .line 31034
    iget-object v2, v1, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;->a:Ljava/lang/String;

    move-object v2, v2

    .line 31035
    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 31036
    iput-object v0, p0, LX/0Dm;->b:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    .line 31037
    :goto_0
    if-eqz v1, :cond_0

    .line 31038
    iget-object v2, v1, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;->b:Ljava/lang/String;

    move-object v2, v2

    .line 31039
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 31040
    :cond_0
    :goto_1
    return-object v0

    .line 31041
    :cond_1
    iget-object v1, p0, LX/0Dm;->d:Ljava/util/HashSet;

    if-eqz v1, :cond_0

    .line 31042
    invoke-static {p1}, LX/047;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 31043
    monitor-enter p0

    .line 31044
    :try_start_0
    iget-object v2, p0, LX/0Dm;->d:Ljava/util/HashSet;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/0Dm;->d:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 31045
    :cond_2
    monitor-exit p0

    goto :goto_1

    .line 31046
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 31047
    iget-object v1, p0, LX/0Dm;->c:LX/0CQ;

    .line 31048
    const/4 v2, 0x0

    .line 31049
    iget-object p0, v1, LX/0CQ;->d:LX/0DT;

    if-eqz p0, :cond_4

    .line 31050
    :try_start_2
    iget-object p0, v1, LX/0CQ;->d:LX/0DT;

    invoke-interface {p0, p1}, LX/0DT;->c(Ljava/lang/String;)Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v2

    .line 31051
    :cond_4
    :goto_2
    move-object v1, v2

    .line 31052
    goto :goto_0

    .line 31053
    :cond_5
    invoke-static {v1}, LX/0Dm;->b(Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    goto :goto_1

    :catch_0
    goto :goto_2
.end method

.method public final declared-synchronized a(Ljava/util/HashSet;)V
    .locals 1
    .param p1    # Ljava/util/HashSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31054
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31055
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Dm;->d:Ljava/util/HashSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31056
    :goto_0
    monitor-exit p0

    return-void

    .line 31057
    :cond_1
    :try_start_1
    iput-object p1, p0, LX/0Dm;->d:Ljava/util/HashSet;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 31058
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
