.class public final LX/06O;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:LX/06K;


# direct methods
.method public constructor <init>(LX/06K;)V
    .locals 0

    .prologue
    .line 17848
    iput-object p1, p0, LX/06O;->a:LX/06K;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x629ef998

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 17849
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/06O;->a:LX/06K;

    iget-object v2, v2, LX/06K;->d:Ljava/lang/String;

    invoke-static {v0, v2}, LX/06P;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17850
    const/16 v0, 0x27

    const v2, -0x149e9ce3

    invoke-static {p2, v3, v0, v2, v1}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 17851
    :goto_0
    return-void

    .line 17852
    :cond_0
    const-string v0, "KeepaliveManager"

    const-string v2, "receiver/keepalive; action=%s delay=%d"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, LX/06O;->a:LX/06K;

    iget-object v5, v5, LX/06K;->k:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v5}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v6

    iget-object v5, p0, LX/06O;->a:LX/06K;

    iget-wide v8, v5, LX/06K;->x:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17853
    iget-object v2, p0, LX/06O;->a:LX/06K;

    monitor-enter v2

    .line 17854
    :try_start_0
    iget-object v0, p0, LX/06O;->a:LX/06K;

    iget-wide v4, v0, LX/06K;->w:J

    const-wide/32 v6, 0xdbba0

    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 17855
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v0, 0x56aa93c0

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 17856
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/06O;->a:LX/06K;

    iget-object v0, v0, LX/06K;->k:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    .line 17857
    iget-object v0, p0, LX/06O;->a:LX/06K;

    iget-wide v6, v0, LX/06K;->x:J

    cmp-long v0, v4, v6

    if-gez v0, :cond_2

    .line 17858
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const v0, -0x517f611d

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 17859
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/06O;->a:LX/06K;

    iget-object v3, p0, LX/06O;->a:LX/06K;

    iget-wide v6, v3, LX/06K;->w:J

    add-long/2addr v4, v6

    .line 17860
    iput-wide v4, v0, LX/06K;->x:J

    .line 17861
    iget-object v0, p0, LX/06O;->a:LX/06K;

    iget-object v0, v0, LX/06K;->j:Landroid/app/AlarmManager;

    iget-object v3, p0, LX/06O;->a:LX/06K;

    iget-object v3, v3, LX/06K;->r:Landroid/app/PendingIntent;

    invoke-virtual {v0, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 17862
    iget-object v0, p0, LX/06O;->a:LX/06K;

    iget-boolean v0, v0, LX/06K;->v:Z

    if-eqz v0, :cond_3

    .line 17863
    iget-object v0, p0, LX/06O;->a:LX/06K;

    iget-object v3, p0, LX/06O;->a:LX/06K;

    iget-wide v4, v3, LX/06K;->x:J

    iget-object v3, p0, LX/06O;->a:LX/06K;

    iget-wide v6, v3, LX/06K;->w:J

    invoke-static {v0, v4, v5, v6, v7}, LX/06K;->b(LX/06K;JJ)V

    .line 17864
    iget-object v0, p0, LX/06O;->a:LX/06K;

    iget-object v3, p0, LX/06O;->a:LX/06K;

    iget-wide v4, v3, LX/06K;->x:J

    const-wide/16 v6, 0x4e20

    add-long/2addr v4, v6

    invoke-static {v0, v4, v5}, LX/06K;->b$redex0(LX/06K;J)V

    .line 17865
    :cond_3
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 17866
    iget-object v0, p0, LX/06O;->a:LX/06K;

    iget-object v0, v0, LX/06K;->u:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 17867
    const v0, 0x2b062444

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto/16 :goto_0

    .line 17868
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const v2, 0x57f6d5f0    # 5.4279743E14f

    invoke-static {p2, v2, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    throw v0
.end method
