.class public final LX/02G;
.super Lcom/facebook/common/asyncview/SpriteView;
.source ""


# instance fields
.field private d:Landroid/graphics/Bitmap;

.field private e:Lcom/facebook/common/asyncview/SpriteView$Sprite;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 6645
    invoke-direct {p0, p1}, Lcom/facebook/common/asyncview/SpriteView;-><init>(Landroid/content/Context;)V

    .line 6646
    invoke-virtual {p0}, LX/02G;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 6647
    iput-object p2, p0, LX/02G;->d:Landroid/graphics/Bitmap;

    .line 6648
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 6649
    invoke-super {p0}, Lcom/facebook/common/asyncview/SpriteView;->b()V

    .line 6650
    iget-object v0, p0, LX/02G;->d:Landroid/graphics/Bitmap;

    .line 6651
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    .line 6652
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    .line 6653
    new-instance v3, Lcom/facebook/common/asyncview/SpriteView$Sprite;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/common/asyncview/SpriteView$Sprite;-><init>(Landroid/graphics/Bitmap;FF)V

    iput-object v3, p0, LX/02G;->e:Lcom/facebook/common/asyncview/SpriteView$Sprite;

    .line 6654
    iget-object v0, p0, LX/02G;->e:Lcom/facebook/common/asyncview/SpriteView$Sprite;

    invoke-virtual {p0, v0}, Lcom/facebook/common/asyncview/SpriteView;->a(Lcom/facebook/common/asyncview/SpriteView$Sprite;)V

    .line 6655
    iget-object v0, p0, LX/02G;->e:Lcom/facebook/common/asyncview/SpriteView$Sprite;

    const-string v1, "alpha"

    new-array v2, v5, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 6656
    const-wide/16 v0, 0x258

    invoke-virtual {v6, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 6657
    invoke-virtual {v6, v4}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 6658
    invoke-virtual {v6, v5}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 6659
    invoke-virtual {v6}, Landroid/animation/ObjectAnimator;->start()V

    .line 6660
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    .line 6661
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    .line 6662
    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    const-wide/16 v4, 0x3e8

    .line 6663
    :goto_0
    new-instance v3, Lcom/facebook/base/init/GenericLogoSplashScreenActivity$LogoView$1;

    invoke-direct {v3, p0, v6}, Lcom/facebook/base/init/GenericLogoSplashScreenActivity$LogoView$1;-><init>(LX/02G;Landroid/animation/ObjectAnimator;)V

    .line 6664
    new-instance v0, LX/02g;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/02g;-><init>(LX/02G;Landroid/os/Handler;Ljava/lang/Runnable;J)V

    invoke-virtual {v6, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 6665
    return-void

    .line 6666
    :cond_0
    if-le v0, v4, :cond_1

    const-wide/16 v4, 0x7d0

    goto :goto_0

    :cond_1
    const-wide/16 v4, 0x1388

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x3e4ccccd    # 0.2f
        0x3f800000    # 1.0f
    .end array-data
.end method
