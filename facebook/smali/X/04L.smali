.class public abstract LX/04L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1FG;


# static fields
.field public static final a:[B


# instance fields
.field private final b:LX/1FI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13080
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/04L;->a:[B

    return-void

    nop

    :array_0
    .array-data 1
        -0x1t
        -0x27t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13078
    invoke-static {}, LX/1FH;->a()LX/1FI;

    move-result-object v0

    iput-object v0, p0, LX/04L;->b:LX/1FI;

    .line 13079
    return-void
.end method

.method private static a(ILandroid/graphics/Bitmap$Config;)Landroid/graphics/BitmapFactory$Options;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 13068
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 13069
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 13070
    iput-object p1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 13071
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 13072
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 13073
    iput p0, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 13074
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 13075
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 13076
    :cond_0
    return-object v0
.end method

.method public static a(LX/1FJ;I)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 13066
    invoke-virtual {p0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    .line 13067
    const/4 v1, 0x2

    if-lt p1, v1, :cond_0

    add-int/lit8 v1, p1, -0x2

    invoke-virtual {v0, v1}, LX/1FK;->a(I)B

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/1FK;->a(I)B

    move-result v0

    const/16 v1, -0x27

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LX/1FL;Landroid/graphics/Bitmap$Config;)LX/1FJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FL;",
            "Landroid/graphics/Bitmap$Config;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13081
    invoke-virtual {p1}, LX/1FL;->g()I

    move-result v0

    invoke-static {v0, p2}, LX/04L;->a(ILandroid/graphics/Bitmap$Config;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    .line 13082
    invoke-virtual {p1}, LX/1FL;->a()LX/1FJ;

    move-result-object v1

    .line 13083
    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 13084
    :try_start_0
    invoke-virtual {p0, v1, v0}, LX/04L;->a(LX/1FJ;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 13085
    invoke-virtual {p0, v0}, LX/04L;->a(Landroid/graphics/Bitmap;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 13086
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public a(LX/1FL;Landroid/graphics/Bitmap$Config;I)LX/1FJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FL;",
            "Landroid/graphics/Bitmap$Config;",
            "I)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13060
    invoke-virtual {p1}, LX/1FL;->g()I

    move-result v0

    invoke-static {v0, p2}, LX/04L;->a(ILandroid/graphics/Bitmap$Config;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    .line 13061
    invoke-virtual {p1}, LX/1FL;->a()LX/1FJ;

    move-result-object v1

    .line 13062
    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 13063
    :try_start_0
    invoke-virtual {p0, v1, p3, v0}, LX/04L;->a(LX/1FJ;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 13064
    invoke-virtual {p0, v0}, LX/04L;->a(Landroid/graphics/Bitmap;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 13065
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public a(Landroid/graphics/Bitmap;)LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13052
    :try_start_0
    invoke-static {p1}, Lcom/facebook/imagepipeline/nativecode/Bitmaps;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 13053
    iget-object v0, p0, LX/04L;->b:LX/1FI;

    invoke-virtual {v0, p1}, LX/1FI;->a(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 13054
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 13055
    new-instance v0, LX/1FM;

    invoke-direct {v0}, LX/1FM;-><init>()V

    throw v0

    .line 13056
    :catch_0
    move-exception v0

    .line 13057
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 13058
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 13059
    :cond_0
    iget-object v0, p0, LX/04L;->b:LX/1FI;

    invoke-virtual {v0}, LX/1FI;->a()LX/1FN;

    move-result-object v0

    invoke-static {p1, v0}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(LX/1FJ;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;I",
            "Landroid/graphics/BitmapFactory$Options;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation
.end method

.method public abstract a(LX/1FJ;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;",
            "Landroid/graphics/BitmapFactory$Options;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation
.end method
