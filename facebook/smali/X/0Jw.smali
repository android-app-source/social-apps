.class public LX/0Jw;
.super LX/0GV;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final c:LX/0LF;

.field public final d:LX/0Ju;

.field private final e:J

.field private final f:I

.field private final g:I

.field public h:Landroid/view/Surface;

.field public i:Z

.field private j:Z

.field private k:J

.field private l:J

.field private m:I

.field private n:I

.field private o:I

.field private p:F

.field private q:I

.field private r:I

.field private s:I

.field private t:F

.field private u:I

.field private v:I

.field private w:I

.field private x:F


# direct methods
.method private constructor <init>(Landroid/content/Context;LX/0L9;LX/0L1;IJLX/0M3;ZLandroid/os/Handler;LX/0Ju;I)V
    .locals 9

    .prologue
    .line 39965
    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p7

    move/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v2 .. v8}, LX/0GV;-><init>(LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jq;)V

    .line 39966
    new-instance v2, LX/0LF;

    invoke-direct {v2, p1}, LX/0LF;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/0Jw;->c:LX/0LF;

    .line 39967
    iput p4, p0, LX/0Jw;->f:I

    .line 39968
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p5

    iput-wide v2, p0, LX/0Jw;->e:J

    .line 39969
    move-object/from16 v0, p10

    iput-object v0, p0, LX/0Jw;->d:LX/0Ju;

    .line 39970
    move/from16 v0, p11

    iput v0, p0, LX/0Jw;->g:I

    .line 39971
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/0Jw;->k:J

    .line 39972
    const/4 v2, -0x1

    iput v2, p0, LX/0Jw;->q:I

    .line 39973
    const/4 v2, -0x1

    iput v2, p0, LX/0Jw;->r:I

    .line 39974
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, LX/0Jw;->t:F

    .line 39975
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, LX/0Jw;->p:F

    .line 39976
    const/4 v2, -0x1

    iput v2, p0, LX/0Jw;->u:I

    .line 39977
    const/4 v2, -0x1

    iput v2, p0, LX/0Jw;->v:I

    .line 39978
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, LX/0Jw;->x:F

    .line 39979
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0L9;LX/0L1;IJLandroid/os/Handler;LX/0Ju;I)V
    .locals 13

    .prologue
    .line 39932
    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-wide/from16 v6, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move/from16 v12, p9

    invoke-direct/range {v1 .. v12}, LX/0Jw;-><init>(Landroid/content/Context;LX/0L9;LX/0L1;IJLX/0M3;ZLandroid/os/Handler;LX/0Ju;I)V

    .line 39933
    return-void
.end method

.method private a(Landroid/media/MediaCodec;I)V
    .locals 2

    .prologue
    .line 39934
    const-string v0, "skipVideoBuffer"

    invoke-static {v0}, LX/08K;->a(Ljava/lang/String;)V

    .line 39935
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 39936
    invoke-static {}, LX/08K;->a()V

    .line 39937
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->g:I

    .line 39938
    return-void
.end method

.method private a(Landroid/media/MediaCodec;IJ)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 39939
    invoke-direct {p0}, LX/0Jw;->w()V

    .line 39940
    const-string v0, "releaseOutputBuffer"

    invoke-static {v0}, LX/08K;->a(Ljava/lang/String;)V

    .line 39941
    invoke-virtual {p1, p2, p3, p4}, Landroid/media/MediaCodec;->releaseOutputBuffer(IJ)V

    .line 39942
    invoke-static {}, LX/08K;->a()V

    .line 39943
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->f:I

    .line 39944
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Jw;->j:Z

    .line 39945
    invoke-direct {p0}, LX/0Jw;->x()V

    .line 39946
    return-void
.end method

.method private b(Landroid/media/MediaCodec;I)V
    .locals 3

    .prologue
    .line 39947
    const-string v0, "dropVideoBuffer"

    invoke-static {v0}, LX/08K;->a(Ljava/lang/String;)V

    .line 39948
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 39949
    invoke-static {}, LX/08K;->a()V

    .line 39950
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->h:I

    .line 39951
    iget v0, p0, LX/0Jw;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Jw;->m:I

    .line 39952
    iget v0, p0, LX/0Jw;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Jw;->n:I

    .line 39953
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    iget v1, p0, LX/0Jw;->n:I

    iget-object v2, p0, LX/0GV;->a:LX/0Kp;

    iget v2, v2, LX/0Kp;->i:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, LX/0Kp;->i:I

    .line 39954
    iget v0, p0, LX/0Jw;->m:I

    iget v1, p0, LX/0Jw;->g:I

    if-ne v0, v1, :cond_0

    .line 39955
    invoke-static {p0}, LX/0Jw;->y(LX/0Jw;)V

    .line 39956
    :cond_0
    return-void
.end method

.method private c(Landroid/media/MediaCodec;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 39957
    invoke-direct {p0}, LX/0Jw;->w()V

    .line 39958
    const-string v0, "releaseOutputBuffer"

    invoke-static {v0}, LX/08K;->a(Ljava/lang/String;)V

    .line 39959
    invoke-virtual {p1, p2, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 39960
    invoke-static {}, LX/08K;->a()V

    .line 39961
    iget-object v0, p0, LX/0GV;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->f:I

    .line 39962
    iput-boolean v2, p0, LX/0Jw;->j:Z

    .line 39963
    invoke-direct {p0}, LX/0Jw;->x()V

    .line 39964
    return-void
.end method

.method private w()V
    .locals 7

    .prologue
    .line 39904
    iget-object v0, p0, LX/0GV;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Jw;->d:LX/0Ju;

    if-eqz v0, :cond_0

    iget v0, p0, LX/0Jw;->u:I

    iget v1, p0, LX/0Jw;->q:I

    if-ne v0, v1, :cond_1

    iget v0, p0, LX/0Jw;->v:I

    iget v1, p0, LX/0Jw;->r:I

    if-ne v0, v1, :cond_1

    iget v0, p0, LX/0Jw;->w:I

    iget v1, p0, LX/0Jw;->s:I

    if-ne v0, v1, :cond_1

    iget v0, p0, LX/0Jw;->x:F

    iget v1, p0, LX/0Jw;->t:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 39905
    :cond_0
    :goto_0
    return-void

    .line 39906
    :cond_1
    iget v2, p0, LX/0Jw;->q:I

    .line 39907
    iget v3, p0, LX/0Jw;->r:I

    .line 39908
    iget v4, p0, LX/0Jw;->s:I

    .line 39909
    iget v5, p0, LX/0Jw;->t:F

    .line 39910
    iget-object v6, p0, LX/0GV;->b:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$1;-><init>(LX/0Jw;IIIF)V

    const v1, 0x2508eb03

    invoke-static {v6, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 39911
    iput v2, p0, LX/0Jw;->u:I

    .line 39912
    iput v3, p0, LX/0Jw;->v:I

    .line 39913
    iput v4, p0, LX/0Jw;->w:I

    .line 39914
    iput v5, p0, LX/0Jw;->x:F

    goto :goto_0
.end method

.method private x()V
    .locals 3

    .prologue
    .line 39980
    iget-object v0, p0, LX/0GV;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Jw;->d:LX/0Ju;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/0Jw;->i:Z

    if-eqz v0, :cond_1

    .line 39981
    :cond_0
    :goto_0
    return-void

    .line 39982
    :cond_1
    iget-object v0, p0, LX/0Jw;->h:Landroid/view/Surface;

    .line 39983
    iget-object v1, p0, LX/0GV;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$2;

    invoke-direct {v2, p0, v0}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$2;-><init>(LX/0Jw;Landroid/view/Surface;)V

    const v0, 0x5f7bc7a1

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 39984
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Jw;->i:Z

    goto :goto_0
.end method

.method private static y(LX/0Jw;)V
    .locals 7

    .prologue
    .line 39985
    iget-object v0, p0, LX/0GV;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Jw;->d:LX/0Ju;

    if-eqz v0, :cond_0

    iget v0, p0, LX/0Jw;->m:I

    if-nez v0, :cond_1

    .line 39986
    :cond_0
    :goto_0
    return-void

    .line 39987
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 39988
    iget v2, p0, LX/0Jw;->m:I

    .line 39989
    iget-wide v4, p0, LX/0Jw;->l:J

    sub-long v4, v0, v4

    .line 39990
    iget-object v3, p0, LX/0GV;->b:Landroid/os/Handler;

    new-instance v6, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$3;

    invoke-direct {v6, p0, v2, v4, v5}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$3;-><init>(LX/0Jw;IJ)V

    const v2, 0x93ae1f

    invoke-static {v3, v6, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 39991
    const/4 v2, 0x0

    iput v2, p0, LX/0Jw;->m:I

    .line 39992
    iput-wide v0, p0, LX/0Jw;->l:J

    goto :goto_0
.end method


# virtual methods
.method public final a(IJZ)V
    .locals 4

    .prologue
    .line 39993
    invoke-super {p0, p1, p2, p3, p4}, LX/0GV;->a(IJZ)V

    .line 39994
    if-eqz p4, :cond_0

    iget-wide v0, p0, LX/0Jw;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 39995
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-wide v2, p0, LX/0Jw;->e:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0Jw;->k:J

    .line 39996
    :cond_0
    iget-object v0, p0, LX/0Jw;->c:LX/0LF;

    .line 39997
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/0LF;->h:Z

    .line 39998
    iget-boolean v1, v0, LX/0LF;->b:Z

    if-eqz v1, :cond_1

    .line 39999
    iget-object v1, v0, LX/0LF;->a:LX/0LE;

    .line 40000
    iget-object v2, v1, LX/0LE;->c:Landroid/os/Handler;

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 40001
    :cond_1
    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 39915
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 39916
    check-cast p2, Landroid/view/Surface;

    .line 39917
    iget-object v0, p0, LX/0Jw;->h:Landroid/view/Surface;

    if-ne v0, p2, :cond_2

    .line 39918
    :cond_0
    :goto_0
    return-void

    .line 39919
    :cond_1
    invoke-super {p0, p1, p2}, LX/0GV;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 39920
    :cond_2
    iput-object p2, p0, LX/0Jw;->h:Landroid/view/Surface;

    .line 39921
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Jw;->i:Z

    .line 39922
    iget v0, p0, LX/0GT;->a:I

    move v0, v0

    .line 39923
    const/4 p1, 0x2

    if-eq v0, p1, :cond_3

    const/4 p1, 0x3

    if-ne v0, p1, :cond_0

    .line 39924
    :cond_3
    invoke-virtual {p0}, LX/0GV;->o()V

    .line 39925
    invoke-virtual {p0}, LX/0GV;->l()V

    goto :goto_0
.end method

.method public a(LX/0L5;)V
    .locals 2

    .prologue
    .line 39926
    invoke-super {p0, p1}, LX/0GV;->a(LX/0L5;)V

    .line 39927
    iget-object v0, p1, LX/0L5;->a:LX/0L4;

    iget v0, v0, LX/0L4;->m:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    iput v0, p0, LX/0Jw;->p:F

    .line 39928
    iget-object v0, p1, LX/0L5;->a:LX/0L4;

    iget v0, v0, LX/0L4;->l:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput v0, p0, LX/0Jw;->o:I

    .line 39929
    return-void

    .line 39930
    :cond_0
    iget-object v0, p1, LX/0L5;->a:LX/0L4;

    iget v0, v0, LX/0L4;->m:F

    goto :goto_0

    .line 39931
    :cond_1
    iget-object v0, p1, LX/0L5;->a:LX/0L4;

    iget v0, v0, LX/0L4;->l:I

    goto :goto_1
.end method

.method public final a(Landroid/media/MediaCodec;ZLandroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
    .locals 7

    .prologue
    .line 39789
    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 39790
    const-string v0, "max-input-size"

    invoke-virtual {p3, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 39791
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0Jw;->h:Landroid/view/Surface;

    const/4 v1, 0x0

    invoke-virtual {p1, p3, v0, p4, v1}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 39792
    iget v0, p0, LX/0Jw;->f:I

    invoke-virtual {p1, v0}, Landroid/media/MediaCodec;->setVideoScalingMode(I)V

    .line 39793
    return-void

    .line 39794
    :cond_1
    const-string v0, "height"

    invoke-virtual {p3, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 39795
    if-eqz p2, :cond_2

    const-string v1, "max-height"

    invoke-virtual {p3, v1}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 39796
    const-string v1, "max-height"

    invoke-virtual {p3, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 39797
    :cond_2
    const-string v1, "width"

    invoke-virtual {p3, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    .line 39798
    if-eqz p2, :cond_3

    const-string v4, "max-width"

    invoke-virtual {p3, v4}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 39799
    const-string v1, "max-width"

    invoke-virtual {p3, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 39800
    :cond_3
    const-string v4, "mime"

    invoke-virtual {p3, v4}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v4, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_4
    :goto_1
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 39801
    :pswitch_0
    mul-int/2addr v0, v1

    move v1, v0

    move v0, v2

    .line 39802
    :goto_2
    mul-int/lit8 v1, v1, 0x3

    mul-int/lit8 v0, v0, 0x2

    div-int v0, v1, v0

    .line 39803
    const-string v1, "max-input-size"

    invoke-virtual {p3, v1, v0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    goto :goto_0

    .line 39804
    :sswitch_0
    const-string v6, "video/3gpp"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v4, 0x0

    goto :goto_1

    :sswitch_1
    const-string v6, "video/mp4v-es"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v4, 0x1

    goto :goto_1

    :sswitch_2
    const-string v6, "video/avc"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v4, v2

    goto :goto_1

    :sswitch_3
    const-string v6, "video/x-vnd.on2.vp8"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v4, 0x3

    goto :goto_1

    :sswitch_4
    const-string v6, "video/hevc"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v4, v3

    goto :goto_1

    :sswitch_5
    const-string v6, "video/x-vnd.on2.vp9"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v4, 0x5

    goto :goto_1

    .line 39805
    :pswitch_1
    const-string v3, "BRAVIA 4K 2015"

    sget-object v4, LX/08x;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 39806
    add-int/lit8 v1, v1, 0xf

    div-int/lit8 v1, v1, 0x10

    add-int/lit8 v0, v0, 0xf

    div-int/lit8 v0, v0, 0x10

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x10

    mul-int/lit8 v0, v0, 0x10

    move v1, v0

    move v0, v2

    .line 39807
    goto :goto_2

    .line 39808
    :pswitch_2
    mul-int/2addr v0, v1

    move v1, v0

    move v0, v2

    .line 39809
    goto :goto_2

    .line 39810
    :pswitch_3
    mul-int/2addr v0, v1

    move v1, v0

    move v0, v3

    .line 39811
    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x63306f58 -> :sswitch_0
        -0x63185e82 -> :sswitch_4
        0x46cdc642 -> :sswitch_1
        0x4f62373a -> :sswitch_2
        0x5f50bed8 -> :sswitch_3
        0x5f50bed9 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/media/MediaFormat;)V
    .locals 3

    .prologue
    .line 39812
    const-string v0, "crop-right"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-left"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-bottom"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-top"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 39813
    :goto_0
    if-eqz v1, :cond_3

    const-string v0, "crop-right"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    const-string v2, "crop-left"

    invoke-virtual {p1, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    :goto_1
    iput v0, p0, LX/0Jw;->q:I

    .line 39814
    if-eqz v1, :cond_4

    const-string v0, "crop-bottom"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    const-string v1, "crop-top"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    :goto_2
    iput v0, p0, LX/0Jw;->r:I

    .line 39815
    iget v0, p0, LX/0Jw;->p:F

    iput v0, p0, LX/0Jw;->t:F

    .line 39816
    sget v0, LX/08x;->a:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_5

    .line 39817
    iget v0, p0, LX/0Jw;->o:I

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/0Jw;->o:I

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_1

    .line 39818
    :cond_0
    iget v0, p0, LX/0Jw;->q:I

    .line 39819
    iget v1, p0, LX/0Jw;->r:I

    iput v1, p0, LX/0Jw;->q:I

    .line 39820
    iput v0, p0, LX/0Jw;->r:I

    .line 39821
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, LX/0Jw;->t:F

    div-float/2addr v0, v1

    iput v0, p0, LX/0Jw;->t:F

    .line 39822
    :cond_1
    :goto_3
    return-void

    .line 39823
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 39824
    :cond_3
    const-string v0, "width"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 39825
    :cond_4
    const-string v0, "height"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    goto :goto_2

    .line 39826
    :cond_5
    iget v0, p0, LX/0Jw;->o:I

    iput v0, p0, LX/0Jw;->s:I

    goto :goto_3
.end method

.method public final a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
    .locals 11

    .prologue
    .line 39827
    if-eqz p9, :cond_0

    .line 39828
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-direct {p0, v0, v1}, LX/0Jw;->a(Landroid/media/MediaCodec;I)V

    .line 39829
    const/4 v2, 0x0

    iput v2, p0, LX/0Jw;->n:I

    .line 39830
    const/4 v2, 0x1

    .line 39831
    :goto_0
    return v2

    .line 39832
    :cond_0
    iget-boolean v2, p0, LX/0Jw;->j:Z

    if-nez v2, :cond_2

    .line 39833
    sget v2, LX/08x;->a:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    .line 39834
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-direct {p0, v0, v1, v2, v3}, LX/0Jw;->a(Landroid/media/MediaCodec;IJ)V

    .line 39835
    :goto_1
    const/4 v2, 0x0

    iput v2, p0, LX/0Jw;->n:I

    .line 39836
    const/4 v2, 0x1

    goto :goto_0

    .line 39837
    :cond_1
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-direct {p0, v0, v1}, LX/0Jw;->c(Landroid/media/MediaCodec;I)V

    goto :goto_1

    .line 39838
    :cond_2
    invoke-virtual {p0}, LX/0GT;->r()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    .line 39839
    const/4 v2, 0x0

    goto :goto_0

    .line 39840
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    sub-long/2addr v2, p3

    .line 39841
    move-object/from16 v0, p7

    iget-wide v4, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    sub-long/2addr v4, p1

    sub-long v2, v4, v2

    .line 39842
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 39843
    const-wide/16 v6, 0x3e8

    mul-long/2addr v2, v6

    add-long/2addr v2, v4

    .line 39844
    iget-object v6, p0, LX/0Jw;->c:LX/0LF;

    move-object/from16 v0, p7

    iget-wide v8, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-virtual {v6, v8, v9, v2, v3}, LX/0LF;->a(JJ)J

    move-result-wide v2

    .line 39845
    sub-long v4, v2, v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 39846
    const-wide/16 v6, -0x7530

    cmp-long v6, v4, v6

    if-gez v6, :cond_4

    .line 39847
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-direct {p0, v0, v1}, LX/0Jw;->b(Landroid/media/MediaCodec;I)V

    .line 39848
    const/4 v2, 0x1

    goto :goto_0

    .line 39849
    :cond_4
    sget v6, LX/08x;->a:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_5

    .line 39850
    const-wide/32 v6, 0xc350

    cmp-long v4, v4, v6

    if-gez v4, :cond_7

    .line 39851
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-direct {p0, v0, v1, v2, v3}, LX/0Jw;->a(Landroid/media/MediaCodec;IJ)V

    .line 39852
    const/4 v2, 0x0

    iput v2, p0, LX/0Jw;->n:I

    .line 39853
    const/4 v2, 0x1

    goto :goto_0

    .line 39854
    :cond_5
    const-wide/16 v2, 0x7530

    cmp-long v2, v4, v2

    if-gez v2, :cond_7

    .line 39855
    const-wide/16 v2, 0x2af8

    cmp-long v2, v4, v2

    if-lez v2, :cond_6

    .line 39856
    const-wide/16 v2, 0x2710

    sub-long v2, v4, v2

    const-wide/16 v4, 0x3e8

    :try_start_0
    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 39857
    :cond_6
    :goto_2
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-direct {p0, v0, v1}, LX/0Jw;->c(Landroid/media/MediaCodec;I)V

    .line 39858
    const/4 v2, 0x0

    iput v2, p0, LX/0Jw;->n:I

    .line 39859
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 39860
    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_2

    .line 39861
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/0L1;LX/0L4;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 39862
    iget-object v1, p2, LX/0L4;->b:Ljava/lang/String;

    .line 39863
    invoke-static {v1}, LX/0Al;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "video/x-unknown"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {p1, v1, v0}, LX/0L1;->a(Ljava/lang/String;Z)LX/08w;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final a(ZLX/0L4;LX/0L4;)Z
    .locals 2

    .prologue
    .line 39864
    iget-object v0, p3, LX/0L4;->b:Ljava/lang/String;

    iget-object v1, p2, LX/0L4;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    iget v0, p2, LX/0L4;->h:I

    iget v1, p3, LX/0L4;->h:I

    if-ne v0, v1, :cond_1

    iget v0, p2, LX/0L4;->i:I

    iget v1, p3, LX/0L4;->i:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(J)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 39865
    invoke-super {p0, p1, p2}, LX/0GV;->c(J)V

    .line 39866
    iput-boolean v0, p0, LX/0Jw;->j:Z

    .line 39867
    iput v0, p0, LX/0Jw;->n:I

    .line 39868
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0Jw;->k:J

    .line 39869
    return-void
.end method

.method public final c()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v6, -0x1

    .line 39870
    invoke-super {p0}, LX/0GV;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, LX/0Jw;->j:Z

    if-nez v2, :cond_0

    .line 39871
    iget-object v2, p0, LX/0GV;->n:Landroid/media/MediaCodec;

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 39872
    if-eqz v2, :cond_0

    .line 39873
    iget v2, p0, LX/0GV;->F:I

    move v2, v2

    .line 39874
    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 39875
    :cond_0
    iput-wide v6, p0, LX/0Jw;->k:J

    .line 39876
    :cond_1
    :goto_1
    return v0

    .line 39877
    :cond_2
    iget-wide v2, p0, LX/0Jw;->k:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_3

    move v0, v1

    .line 39878
    goto :goto_1

    .line 39879
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iget-wide v4, p0, LX/0Jw;->k:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 39880
    iput-wide v6, p0, LX/0Jw;->k:J

    move v0, v1

    .line 39881
    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 39882
    invoke-super {p0}, LX/0GV;->h()V

    .line 39883
    const/4 v0, 0x0

    iput v0, p0, LX/0Jw;->m:I

    .line 39884
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0Jw;->l:J

    .line 39885
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 39886
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0Jw;->k:J

    .line 39887
    invoke-static {p0}, LX/0Jw;->y(LX/0Jw;)V

    .line 39888
    invoke-super {p0}, LX/0GV;->i()V

    .line 39889
    return-void
.end method

.method public final j()V
    .locals 3

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    const/4 v0, -0x1

    .line 39890
    iput v0, p0, LX/0Jw;->q:I

    .line 39891
    iput v0, p0, LX/0Jw;->r:I

    .line 39892
    iput v1, p0, LX/0Jw;->t:F

    .line 39893
    iput v1, p0, LX/0Jw;->p:F

    .line 39894
    iput v0, p0, LX/0Jw;->u:I

    .line 39895
    iput v0, p0, LX/0Jw;->v:I

    .line 39896
    iput v1, p0, LX/0Jw;->x:F

    .line 39897
    iget-object v0, p0, LX/0Jw;->c:LX/0LF;

    .line 39898
    iget-boolean v1, v0, LX/0LF;->b:Z

    if-eqz v1, :cond_0

    .line 39899
    iget-object v1, v0, LX/0LF;->a:LX/0LE;

    .line 39900
    iget-object v2, v1, LX/0LE;->c:Landroid/os/Handler;

    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 39901
    :cond_0
    invoke-super {p0}, LX/0GV;->j()V

    .line 39902
    return-void
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 39903
    invoke-super {p0}, LX/0GV;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Jw;->h:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Jw;->h:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
