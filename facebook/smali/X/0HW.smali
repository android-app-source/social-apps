.class public final enum LX/0HW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0HW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0HW;

.field public static final enum MOBILE_2G:LX/0HW;

.field public static final enum MOBILE_3G:LX/0HW;

.field public static final enum MOBILE_4G:LX/0HW;

.field public static final enum MOBILE_OTHER:LX/0HW;

.field public static final enum NoNetwork:LX/0HW;

.field public static final enum Other:LX/0HW;

.field public static final enum UNKNOWN:LX/0HW;

.field public static final enum WIFI:LX/0HW;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 37797
    new-instance v0, LX/0HW;

    const-string v1, "UNKNOWN"

    const-string v2, "Unknown"

    invoke-direct {v0, v1, v4, v2}, LX/0HW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0HW;->UNKNOWN:LX/0HW;

    .line 37798
    new-instance v0, LX/0HW;

    const-string v1, "NoNetwork"

    const-string v2, "NoNetwork"

    invoke-direct {v0, v1, v5, v2}, LX/0HW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0HW;->NoNetwork:LX/0HW;

    .line 37799
    new-instance v0, LX/0HW;

    const-string v1, "WIFI"

    const-string v2, "Wifi"

    invoke-direct {v0, v1, v6, v2}, LX/0HW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0HW;->WIFI:LX/0HW;

    .line 37800
    new-instance v0, LX/0HW;

    const-string v1, "MOBILE_2G"

    const-string v2, "Cell_2G"

    invoke-direct {v0, v1, v7, v2}, LX/0HW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0HW;->MOBILE_2G:LX/0HW;

    .line 37801
    new-instance v0, LX/0HW;

    const-string v1, "MOBILE_3G"

    const-string v2, "Cell_3G"

    invoke-direct {v0, v1, v8, v2}, LX/0HW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0HW;->MOBILE_3G:LX/0HW;

    .line 37802
    new-instance v0, LX/0HW;

    const-string v1, "MOBILE_4G"

    const/4 v2, 0x5

    const-string v3, "Cell_4G"

    invoke-direct {v0, v1, v2, v3}, LX/0HW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0HW;->MOBILE_4G:LX/0HW;

    .line 37803
    new-instance v0, LX/0HW;

    const-string v1, "MOBILE_OTHER"

    const/4 v2, 0x6

    const-string v3, "Cell_other"

    invoke-direct {v0, v1, v2, v3}, LX/0HW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0HW;->MOBILE_OTHER:LX/0HW;

    .line 37804
    new-instance v0, LX/0HW;

    const-string v1, "Other"

    const/4 v2, 0x7

    const-string v3, "Other"

    invoke-direct {v0, v1, v2, v3}, LX/0HW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0HW;->Other:LX/0HW;

    .line 37805
    const/16 v0, 0x8

    new-array v0, v0, [LX/0HW;

    sget-object v1, LX/0HW;->UNKNOWN:LX/0HW;

    aput-object v1, v0, v4

    sget-object v1, LX/0HW;->NoNetwork:LX/0HW;

    aput-object v1, v0, v5

    sget-object v1, LX/0HW;->WIFI:LX/0HW;

    aput-object v1, v0, v6

    sget-object v1, LX/0HW;->MOBILE_2G:LX/0HW;

    aput-object v1, v0, v7

    sget-object v1, LX/0HW;->MOBILE_3G:LX/0HW;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0HW;->MOBILE_4G:LX/0HW;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0HW;->MOBILE_OTHER:LX/0HW;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0HW;->Other:LX/0HW;

    aput-object v2, v0, v1

    sput-object v0, LX/0HW;->$VALUES:[LX/0HW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37806
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37807
    iput-object p3, p0, LX/0HW;->name:Ljava/lang/String;

    .line 37808
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0HW;
    .locals 1

    .prologue
    .line 37796
    const-class v0, LX/0HW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0HW;

    return-object v0
.end method

.method public static values()[LX/0HW;
    .locals 1

    .prologue
    .line 37794
    sget-object v0, LX/0HW;->$VALUES:[LX/0HW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0HW;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37795
    iget-object v0, p0, LX/0HW;->name:Ljava/lang/String;

    return-object v0
.end method
