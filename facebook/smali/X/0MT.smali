.class public final LX/0MT;
.super LX/0MR;
.source ""


# direct methods
.method public constructor <init>(LX/0LS;)V
    .locals 0

    .prologue
    .line 46148
    invoke-direct {p0, p1}, LX/0MR;-><init>(LX/0LS;)V

    .line 46149
    return-void
.end method

.method public static a(LX/0Oj;I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 46121
    packed-switch p1, :pswitch_data_0

    .line 46122
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 46123
    :pswitch_1
    invoke-static {p0}, LX/0MT;->d(LX/0Oj;)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 46124
    :pswitch_2
    const/4 v0, 0x1

    .line 46125
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result p1

    if-ne p1, v0, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    move-object v0, v0

    .line 46126
    goto :goto_0

    .line 46127
    :pswitch_3
    invoke-static {p0}, LX/0MT;->e(LX/0Oj;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 46128
    :pswitch_4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 46129
    :goto_2
    invoke-static {p0}, LX/0MT;->e(LX/0Oj;)Ljava/lang/String;

    move-result-object v1

    .line 46130
    invoke-static {p0}, LX/0MT;->b(LX/0Oj;)I

    move-result v2

    .line 46131
    const/16 p1, 0x9

    if-eq v2, p1, :cond_0

    .line 46132
    invoke-static {p0, v2}, LX/0MT;->a(LX/0Oj;I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 46133
    :cond_0
    move-object v0, v0

    .line 46134
    goto :goto_0

    .line 46135
    :pswitch_5
    invoke-static {p0}, LX/0MT;->h(LX/0Oj;)Ljava/util/HashMap;

    move-result-object v0

    goto :goto_0

    .line 46136
    :pswitch_6
    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v1

    .line 46137
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 46138
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_1

    .line 46139
    invoke-static {p0}, LX/0MT;->b(LX/0Oj;)I

    move-result p1

    .line 46140
    invoke-static {p0, p1}, LX/0MT;->a(LX/0Oj;I)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46141
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 46142
    :cond_1
    move-object v0, v2

    .line 46143
    goto :goto_0

    .line 46144
    :pswitch_7
    new-instance v1, Ljava/util/Date;

    invoke-static {p0}, LX/0MT;->d(LX/0Oj;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    double-to-long v3, v3

    invoke-direct {v1, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 46145
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, LX/0Oj;->c(I)V

    .line 46146
    move-object v0, v1

    .line 46147
    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static b(LX/0Oj;)I
    .locals 1

    .prologue
    .line 46094
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    return v0
.end method

.method public static d(LX/0Oj;)Ljava/lang/Double;
    .locals 2

    .prologue
    .line 46120
    invoke-virtual {p0}, LX/0Oj;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/0Oj;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 46150
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v0

    .line 46151
    iget v1, p0, LX/0Oj;->b:I

    move v1, v1

    .line 46152
    invoke-virtual {p0, v0}, LX/0Oj;->c(I)V

    .line 46153
    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, LX/0Oj;->a:[B

    invoke-direct {v2, v3, v1, v0}, Ljava/lang/String;-><init>([BII)V

    return-object v2
.end method

.method private static h(LX/0Oj;)Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Oj;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46112
    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v1

    .line 46113
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 46114
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 46115
    invoke-static {p0}, LX/0MT;->e(LX/0Oj;)Ljava/lang/String;

    move-result-object v3

    .line 46116
    invoke-static {p0}, LX/0MT;->b(LX/0Oj;)I

    move-result v4

    .line 46117
    invoke-static {p0, v4}, LX/0MT;->a(LX/0Oj;I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46119
    :cond_0
    return-object v2
.end method


# virtual methods
.method public final a(LX/0Oj;J)V
    .locals 5

    .prologue
    .line 46096
    invoke-static {p1}, LX/0MT;->b(LX/0Oj;)I

    move-result v0

    .line 46097
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 46098
    new-instance v0, LX/0L6;

    invoke-direct {v0}, LX/0L6;-><init>()V

    throw v0

    .line 46099
    :cond_0
    invoke-static {p1}, LX/0MT;->e(LX/0Oj;)Ljava/lang/String;

    move-result-object v0

    .line 46100
    const-string v1, "onMetaData"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 46101
    :cond_1
    :goto_0
    return-void

    .line 46102
    :cond_2
    invoke-static {p1}, LX/0MT;->b(LX/0Oj;)I

    move-result v0

    .line 46103
    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    .line 46104
    new-instance v0, LX/0L6;

    invoke-direct {v0}, LX/0L6;-><init>()V

    throw v0

    .line 46105
    :cond_3
    invoke-static {p1}, LX/0MT;->h(LX/0Oj;)Ljava/util/HashMap;

    move-result-object v0

    .line 46106
    const-string v1, "duration"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 46107
    const-string v1, "duration"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 46108
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    .line 46109
    const-wide v2, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    .line 46110
    iput-wide v0, p0, LX/0MR;->b:J

    .line 46111
    goto :goto_0
.end method

.method public final a(LX/0Oj;)Z
    .locals 1

    .prologue
    .line 46095
    const/4 v0, 0x1

    return v0
.end method
