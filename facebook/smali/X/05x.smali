.class public final LX/05x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljavax/net/ssl/HostnameVerifier;


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17096
    const-string v0, "([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/05x;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17123
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)LX/07K;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/07K;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 17112
    new-instance v1, LX/07K;

    invoke-direct {v1, p0}, LX/07K;-><init>(Ljava/lang/String;)V

    .line 17113
    iput-boolean v4, v1, LX/07K;->f:Z

    .line 17114
    iput-object p0, v1, LX/07K;->i:Ljava/lang/String;

    .line 17115
    iput-object p1, v1, LX/07K;->j:Ljava/util/List;

    .line 17116
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 17117
    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 17118
    iput-boolean v4, v1, LX/07K;->e:Z

    .line 17119
    iput-object v0, v1, LX/07K;->g:Ljava/lang/String;

    .line 17120
    invoke-virtual {v1}, LX/07K;->b()LX/07K;

    move-result-object v0

    .line 17121
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, LX/07K;->c()LX/07K;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/security/cert/X509Certificate;I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/security/cert/X509Certificate;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17097
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 17098
    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getSubjectAlternativeNames()Ljava/util/Collection;

    move-result-object v0

    .line 17099
    if-nez v0, :cond_0

    .line 17100
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 17101
    :goto_0
    return-object v0

    .line 17102
    :cond_0
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 17103
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v4, 0x2

    if-lt v1, v4, :cond_1

    .line 17104
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 17105
    if-eqz v1, :cond_1

    .line 17106
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 17107
    if-ne v1, p1, :cond_1

    .line 17108
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 17109
    if-eqz v0, :cond_1

    .line 17110
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 17111
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 17124
    sget-object v0, LX/05x;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/16 v6, 0x2e

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 17034
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 17035
    :cond_1
    :goto_0
    return v0

    .line 17036
    :cond_2
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 17037
    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 17038
    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 17039
    :cond_3
    const-string v3, "*."

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {p0, v1, v2, v3, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-nez v3, :cond_1

    .line 17040
    :cond_4
    const/16 v3, 0x2a

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 17041
    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 17042
    if-le v3, v4, :cond_5

    move v0, v1

    .line 17043
    goto :goto_0

    .line 17044
    :cond_5
    invoke-virtual {p0, v1, v2, v1, v3}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v4

    if-nez v4, :cond_6

    move v0, v1

    .line 17045
    goto :goto_0

    .line 17046
    :cond_6
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v5, v3, 0x1

    sub-int/2addr v4, v5

    .line 17047
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v5, v4

    .line 17048
    invoke-virtual {p0, v6, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v6

    if-ge v6, v5, :cond_7

    .line 17049
    const-string v6, ".clients.google.com"

    invoke-virtual {p0, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_7

    move v0, v1

    .line 17050
    goto :goto_0

    .line 17051
    :cond_7
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0, v5, v2, v3, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 17052
    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)LX/07K;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/07K;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 17053
    new-instance v2, LX/07K;

    invoke-direct {v2, p0}, LX/07K;-><init>(Ljava/lang/String;)V

    .line 17054
    iput-object p1, v2, LX/07K;->i:Ljava/lang/String;

    .line 17055
    iput-object p2, v2, LX/07K;->j:Ljava/util/List;

    .line 17056
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 17057
    const/4 v0, 0x0

    .line 17058
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 17059
    invoke-static {v3, v0}, LX/05x;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 17060
    iput-boolean v1, v2, LX/07K;->d:Z

    .line 17061
    iput-object v0, v2, LX/07K;->g:Ljava/lang/String;

    .line 17062
    invoke-virtual {v2}, LX/07K;->b()LX/07K;

    move-result-object v0

    .line 17063
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 17064
    goto :goto_0

    .line 17065
    :cond_1
    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    .line 17066
    invoke-static {v3, p1}, LX/05x;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 17067
    iput-boolean v1, v2, LX/07K;->c:Z

    .line 17068
    iput-object p1, v2, LX/07K;->g:Ljava/lang/String;

    .line 17069
    invoke-virtual {v2}, LX/07K;->b()LX/07K;

    move-result-object v0

    goto :goto_1

    .line 17070
    :cond_2
    invoke-virtual {v2}, LX/07K;->c()LX/07K;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/security/cert/X509Certificate;)LX/07K;
    .locals 5

    .prologue
    .line 17071
    invoke-static {p1}, LX/05x;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17072
    const-string v0, ""

    .line 17073
    const/4 v1, 0x7

    :try_start_0
    invoke-static {p2, v1}, LX/05x;->a(Ljava/security/cert/X509Certificate;I)Ljava/util/List;
    :try_end_0
    .catch Ljava/security/cert/CertificateParsingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 17074
    :goto_0
    invoke-static {p1, v1}, LX/05x;->a(Ljava/lang/String;Ljava/util/List;)LX/07K;

    move-result-object v1

    .line 17075
    iput-object v0, v1, LX/07K;->h:Ljava/lang/String;

    .line 17076
    move-object v0, v1

    .line 17077
    :goto_1
    return-object v0

    .line 17078
    :cond_0
    invoke-virtual {p2}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v0

    .line 17079
    new-instance v1, LX/07J;

    invoke-direct {v1, v0}, LX/07J;-><init>(Ljavax/security/auth/x500/X500Principal;)V

    const-string v0, "cn"

    invoke-virtual {v1, v0}, LX/07J;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 17080
    const-string v0, ""

    .line 17081
    const/4 v1, 0x2

    :try_start_1
    invoke-static {p2, v1}, LX/05x;->a(Ljava/security/cert/X509Certificate;I)Ljava/util/List;
    :try_end_1
    .catch Ljava/security/cert/CertificateParsingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 17082
    :goto_2
    invoke-static {p1, v2, v1}, LX/05x;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)LX/07K;

    move-result-object v1

    .line 17083
    iput-object v0, v1, LX/07K;->h:Ljava/lang/String;

    .line 17084
    move-object v0, v1

    .line 17085
    goto :goto_1

    .line 17086
    :catch_0
    move-exception v0

    .line 17087
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 17088
    new-instance v2, Ljava/lang/StringBuilder;

    const-string p0, "Failed parsing subjectAltName: "

    invoke-direct {v2, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/cert/CertificateParsingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 17089
    :catch_1
    move-exception v0

    .line 17090
    new-instance v1, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 17091
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed parsing subjectAltName: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/cert/CertificateParsingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public final verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 17092
    :try_start_0
    invoke-interface {p2}, Ljavax/net/ssl/SSLSession;->getPeerCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0

    .line 17093
    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-virtual {p0, p1, v0}, LX/05x;->a(Ljava/lang/String;Ljava/security/cert/X509Certificate;)LX/07K;

    move-result-object v0

    .line 17094
    iget-boolean v2, v0, LX/07K;->a:Z

    move v0, v2
    :try_end_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 17095
    :goto_0
    return v0

    :catch_0
    move v0, v1

    goto :goto_0
.end method
