.class public final enum LX/0AX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0AX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0AX;

.field public static final enum ABORTED_PREEMPTIVE_RECONNECT:LX/0AX;

.field public static final enum AUTH_CREDENTIALS_CHANGE:LX/0AX;

.field public static final enum DISCONNECT_FROM_SERVER:LX/0AX;

.field public static final enum EXPIRE_CONNECTION:LX/0AX;

.field public static final enum KEEPALIVE_SHOULD_NOT_CONNECT:LX/0AX;

.field public static final enum KICK_CONFIG_CHANGED:LX/0AX;

.field public static final enum KICK_SHOULD_NOT_CONNECT:LX/0AX;

.field public static final enum OPERATION_TIMEOUT:LX/0AX;

.field public static final enum PING_UNRECEIVED:LX/0AX;

.field public static final enum PREEMPTIVE_RECONNECT_SUCCESS:LX/0AX;

.field public static final enum READ_EOF:LX/0AX;

.field public static final enum READ_FAILURE_UNCLASSIFIED:LX/0AX;

.field public static final enum READ_FORMAT:LX/0AX;

.field public static final enum READ_IO:LX/0AX;

.field public static final enum READ_SOCKET:LX/0AX;

.field public static final enum READ_SSL:LX/0AX;

.field public static final enum READ_TIMEOUT:LX/0AX;

.field public static final enum SEND_FAILURE:LX/0AX;

.field public static final enum SERIALIZER_FAILURE:LX/0AX;

.field public static final enum SERVICE_DESTROY:LX/0AX;

.field public static final enum SERVICE_STOP:LX/0AX;

.field public static final enum UNKNOWN_RUNTIME:LX/0AX;

.field public static final enum WRITE_EOF:LX/0AX;

.field public static final enum WRITE_FAILURE_UNCLASSIFIED:LX/0AX;

.field public static final enum WRITE_IO:LX/0AX;

.field public static final enum WRITE_SOCKET:LX/0AX;

.field public static final enum WRITE_SSL:LX/0AX;

.field public static final enum WRITE_TIMEOUT:LX/0AX;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24406
    new-instance v0, LX/0AX;

    const-string v1, "SERVICE_DESTROY"

    invoke-direct {v0, v1, v3}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->SERVICE_DESTROY:LX/0AX;

    .line 24407
    new-instance v0, LX/0AX;

    const-string v1, "SERVICE_STOP"

    invoke-direct {v0, v1, v4}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->SERVICE_STOP:LX/0AX;

    .line 24408
    new-instance v0, LX/0AX;

    const-string v1, "KICK_SHOULD_NOT_CONNECT"

    invoke-direct {v0, v1, v5}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->KICK_SHOULD_NOT_CONNECT:LX/0AX;

    .line 24409
    new-instance v0, LX/0AX;

    const-string v1, "KICK_CONFIG_CHANGED"

    invoke-direct {v0, v1, v6}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->KICK_CONFIG_CHANGED:LX/0AX;

    .line 24410
    new-instance v0, LX/0AX;

    const-string v1, "KEEPALIVE_SHOULD_NOT_CONNECT"

    invoke-direct {v0, v1, v7}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->KEEPALIVE_SHOULD_NOT_CONNECT:LX/0AX;

    .line 24411
    new-instance v0, LX/0AX;

    const-string v1, "EXPIRE_CONNECTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->EXPIRE_CONNECTION:LX/0AX;

    .line 24412
    new-instance v0, LX/0AX;

    const-string v1, "OPERATION_TIMEOUT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->OPERATION_TIMEOUT:LX/0AX;

    .line 24413
    new-instance v0, LX/0AX;

    const-string v1, "PING_UNRECEIVED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->PING_UNRECEIVED:LX/0AX;

    .line 24414
    new-instance v0, LX/0AX;

    const-string v1, "READ_TIMEOUT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->READ_TIMEOUT:LX/0AX;

    .line 24415
    new-instance v0, LX/0AX;

    const-string v1, "READ_EOF"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->READ_EOF:LX/0AX;

    .line 24416
    new-instance v0, LX/0AX;

    const-string v1, "READ_SOCKET"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->READ_SOCKET:LX/0AX;

    .line 24417
    new-instance v0, LX/0AX;

    const-string v1, "READ_SSL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->READ_SSL:LX/0AX;

    .line 24418
    new-instance v0, LX/0AX;

    const-string v1, "READ_IO"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->READ_IO:LX/0AX;

    .line 24419
    new-instance v0, LX/0AX;

    const-string v1, "READ_FORMAT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->READ_FORMAT:LX/0AX;

    .line 24420
    new-instance v0, LX/0AX;

    const-string v1, "READ_FAILURE_UNCLASSIFIED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->READ_FAILURE_UNCLASSIFIED:LX/0AX;

    .line 24421
    new-instance v0, LX/0AX;

    const-string v1, "WRITE_TIMEOUT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->WRITE_TIMEOUT:LX/0AX;

    .line 24422
    new-instance v0, LX/0AX;

    const-string v1, "WRITE_EOF"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->WRITE_EOF:LX/0AX;

    .line 24423
    new-instance v0, LX/0AX;

    const-string v1, "WRITE_SOCKET"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->WRITE_SOCKET:LX/0AX;

    .line 24424
    new-instance v0, LX/0AX;

    const-string v1, "WRITE_SSL"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->WRITE_SSL:LX/0AX;

    .line 24425
    new-instance v0, LX/0AX;

    const-string v1, "WRITE_IO"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->WRITE_IO:LX/0AX;

    .line 24426
    new-instance v0, LX/0AX;

    const-string v1, "WRITE_FAILURE_UNCLASSIFIED"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->WRITE_FAILURE_UNCLASSIFIED:LX/0AX;

    .line 24427
    new-instance v0, LX/0AX;

    const-string v1, "UNKNOWN_RUNTIME"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->UNKNOWN_RUNTIME:LX/0AX;

    .line 24428
    new-instance v0, LX/0AX;

    const-string v1, "SEND_FAILURE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->SEND_FAILURE:LX/0AX;

    .line 24429
    new-instance v0, LX/0AX;

    const-string v1, "DISCONNECT_FROM_SERVER"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->DISCONNECT_FROM_SERVER:LX/0AX;

    .line 24430
    new-instance v0, LX/0AX;

    const-string v1, "SERIALIZER_FAILURE"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->SERIALIZER_FAILURE:LX/0AX;

    .line 24431
    new-instance v0, LX/0AX;

    const-string v1, "PREEMPTIVE_RECONNECT_SUCCESS"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->PREEMPTIVE_RECONNECT_SUCCESS:LX/0AX;

    .line 24432
    new-instance v0, LX/0AX;

    const-string v1, "ABORTED_PREEMPTIVE_RECONNECT"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->ABORTED_PREEMPTIVE_RECONNECT:LX/0AX;

    .line 24433
    new-instance v0, LX/0AX;

    const-string v1, "AUTH_CREDENTIALS_CHANGE"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, LX/0AX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0AX;->AUTH_CREDENTIALS_CHANGE:LX/0AX;

    .line 24434
    const/16 v0, 0x1c

    new-array v0, v0, [LX/0AX;

    sget-object v1, LX/0AX;->SERVICE_DESTROY:LX/0AX;

    aput-object v1, v0, v3

    sget-object v1, LX/0AX;->SERVICE_STOP:LX/0AX;

    aput-object v1, v0, v4

    sget-object v1, LX/0AX;->KICK_SHOULD_NOT_CONNECT:LX/0AX;

    aput-object v1, v0, v5

    sget-object v1, LX/0AX;->KICK_CONFIG_CHANGED:LX/0AX;

    aput-object v1, v0, v6

    sget-object v1, LX/0AX;->KEEPALIVE_SHOULD_NOT_CONNECT:LX/0AX;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0AX;->EXPIRE_CONNECTION:LX/0AX;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0AX;->OPERATION_TIMEOUT:LX/0AX;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0AX;->PING_UNRECEIVED:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0AX;->READ_TIMEOUT:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0AX;->READ_EOF:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0AX;->READ_SOCKET:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0AX;->READ_SSL:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0AX;->READ_IO:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0AX;->READ_FORMAT:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0AX;->READ_FAILURE_UNCLASSIFIED:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0AX;->WRITE_TIMEOUT:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0AX;->WRITE_EOF:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0AX;->WRITE_SOCKET:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/0AX;->WRITE_SSL:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/0AX;->WRITE_IO:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/0AX;->WRITE_FAILURE_UNCLASSIFIED:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/0AX;->UNKNOWN_RUNTIME:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/0AX;->SEND_FAILURE:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/0AX;->DISCONNECT_FROM_SERVER:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/0AX;->SERIALIZER_FAILURE:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/0AX;->PREEMPTIVE_RECONNECT_SUCCESS:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/0AX;->ABORTED_PREEMPTIVE_RECONNECT:LX/0AX;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/0AX;->AUTH_CREDENTIALS_CHANGE:LX/0AX;

    aput-object v2, v0, v1

    sput-object v0, LX/0AX;->$VALUES:[LX/0AX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24435
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getFromReadException(Ljava/lang/Throwable;)LX/0AX;
    .locals 1

    .prologue
    .line 24436
    instance-of v0, p0, Ljava/util/concurrent/TimeoutException;

    if-nez v0, :cond_0

    instance-of v0, p0, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_1

    .line 24437
    :cond_0
    sget-object v0, LX/0AX;->READ_TIMEOUT:LX/0AX;

    .line 24438
    :goto_0
    return-object v0

    .line 24439
    :cond_1
    instance-of v0, p0, Ljava/io/EOFException;

    if-eqz v0, :cond_2

    .line 24440
    sget-object v0, LX/0AX;->READ_EOF:LX/0AX;

    goto :goto_0

    .line 24441
    :cond_2
    instance-of v0, p0, Ljava/net/SocketException;

    if-eqz v0, :cond_3

    .line 24442
    sget-object v0, LX/0AX;->READ_SOCKET:LX/0AX;

    goto :goto_0

    .line 24443
    :cond_3
    instance-of v0, p0, Ljavax/net/ssl/SSLException;

    if-eqz v0, :cond_4

    .line 24444
    sget-object v0, LX/0AX;->READ_SSL:LX/0AX;

    goto :goto_0

    .line 24445
    :cond_4
    instance-of v0, p0, Ljava/io/IOException;

    if-eqz v0, :cond_5

    .line 24446
    sget-object v0, LX/0AX;->READ_IO:LX/0AX;

    goto :goto_0

    .line 24447
    :cond_5
    instance-of v0, p0, Ljava/util/zip/DataFormatException;

    if-eqz v0, :cond_6

    .line 24448
    sget-object v0, LX/0AX;->READ_FORMAT:LX/0AX;

    goto :goto_0

    .line 24449
    :cond_6
    sget-object v0, LX/0AX;->READ_FAILURE_UNCLASSIFIED:LX/0AX;

    goto :goto_0
.end method

.method public static getFromWriteException(Ljava/lang/Throwable;)LX/0AX;
    .locals 1

    .prologue
    .line 24450
    instance-of v0, p0, Ljava/util/concurrent/TimeoutException;

    if-nez v0, :cond_0

    instance-of v0, p0, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_1

    .line 24451
    :cond_0
    sget-object v0, LX/0AX;->WRITE_TIMEOUT:LX/0AX;

    .line 24452
    :goto_0
    return-object v0

    .line 24453
    :cond_1
    instance-of v0, p0, Ljava/io/EOFException;

    if-eqz v0, :cond_2

    .line 24454
    sget-object v0, LX/0AX;->WRITE_EOF:LX/0AX;

    goto :goto_0

    .line 24455
    :cond_2
    instance-of v0, p0, Ljava/net/SocketException;

    if-eqz v0, :cond_3

    .line 24456
    sget-object v0, LX/0AX;->WRITE_SOCKET:LX/0AX;

    goto :goto_0

    .line 24457
    :cond_3
    instance-of v0, p0, Ljavax/net/ssl/SSLException;

    if-eqz v0, :cond_4

    .line 24458
    sget-object v0, LX/0AX;->WRITE_SSL:LX/0AX;

    goto :goto_0

    .line 24459
    :cond_4
    instance-of v0, p0, Ljava/io/IOException;

    if-eqz v0, :cond_5

    .line 24460
    sget-object v0, LX/0AX;->WRITE_IO:LX/0AX;

    goto :goto_0

    .line 24461
    :cond_5
    sget-object v0, LX/0AX;->WRITE_FAILURE_UNCLASSIFIED:LX/0AX;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0AX;
    .locals 1

    .prologue
    .line 24462
    const-class v0, LX/0AX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0AX;

    return-object v0
.end method

.method public static values()[LX/0AX;
    .locals 1

    .prologue
    .line 24463
    sget-object v0, LX/0AX;->$VALUES:[LX/0AX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0AX;

    return-object v0
.end method
