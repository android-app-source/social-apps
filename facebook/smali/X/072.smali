.class public LX/072;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LX/073;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public volatile A:Ljava/lang/String;

.field public volatile B:Landroid/net/NetworkInfo;

.field public volatile C:J

.field public volatile D:J

.field public volatile E:LX/077;

.field private F:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private G:I

.field private final H:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0AF;",
            ">;"
        }
    .end annotation
.end field

.field private final I:LX/075;

.field public final J:LX/076;

.field private final K:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final b:LX/059;

.field public final c:LX/05h;

.field public final d:LX/05i;

.field public final e:LX/06z;

.field public final f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field public final g:Ljava/util/concurrent/ExecutorService;

.field public final h:LX/05Z;

.field public final i:LX/05R;

.field public final j:LX/05F;

.field public final k:LX/05k;

.field private final l:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final m:LX/071;

.field private final n:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Z

.field public final p:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final q:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Z

.field public final t:Z

.field public volatile u:J

.field public volatile v:J

.field public volatile w:J

.field public volatile x:J

.field public volatile y:J

.field public volatile z:LX/074;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 18962
    sget-object v0, LX/073;->ACKNOWLEDGED_DELIVERY:LX/073;

    const/4 v1, 0x5

    new-array v1, v1, [LX/073;

    const/4 v2, 0x0

    sget-object v3, LX/073;->PROCESSING_LASTACTIVE_PRESENCEINFO:LX/073;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, LX/073;->EXACT_KEEPALIVE:LX/073;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, LX/073;->DELTA_SENT_MESSAGE_ENABLED:LX/073;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, LX/073;->USE_THRIFT_FOR_INBOX:LX/073;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, LX/073;->USE_ENUM_TOPIC:LX/073;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, LX/072;->a:Ljava/util/EnumSet;

    return-void
.end method

.method public constructor <init>(LX/059;LX/05h;LX/05i;LX/06z;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Ljava/util/concurrent/ExecutorService;LX/05Z;LX/05R;LX/05F;LX/05k;LX/071;LX/05N;Ljava/util/concurrent/atomic/AtomicReference;LX/05N;LX/05N;ZZ)V
    .locals 5
    .param p13    # Ljava/util/concurrent/atomic/AtomicReference;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/059;",
            "LX/05h;",
            "LX/05i;",
            "LX/06z;",
            "Lcom/facebook/rti/common/time/MonotonicClock;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/05Z;",
            "LX/05R;",
            "LX/05F;",
            "LX/05k;",
            "LX/071;",
            "LX/05N",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 18848
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18849
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v2, p0, LX/072;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 18850
    const-wide v2, 0x7fffffffffffffffL

    iput-wide v2, p0, LX/072;->u:J

    .line 18851
    const-wide v2, 0x7fffffffffffffffL

    iput-wide v2, p0, LX/072;->v:J

    .line 18852
    const-wide v2, 0x7fffffffffffffffL

    iput-wide v2, p0, LX/072;->w:J

    .line 18853
    const-wide v2, 0x7fffffffffffffffL

    iput-wide v2, p0, LX/072;->x:J

    .line 18854
    const-wide v2, 0x7fffffffffffffffL

    iput-wide v2, p0, LX/072;->y:J

    .line 18855
    sget-object v2, LX/074;->INIT:LX/074;

    iput-object v2, p0, LX/072;->z:LX/074;

    .line 18856
    const-string v2, "none"

    iput-object v2, p0, LX/072;->A:Ljava/lang/String;

    .line 18857
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, LX/072;->H:Ljava/util/Map;

    .line 18858
    new-instance v2, LX/075;

    invoke-direct {v2, p0}, LX/075;-><init>(LX/072;)V

    iput-object v2, p0, LX/072;->I:LX/075;

    .line 18859
    new-instance v2, LX/076;

    invoke-direct {v2, p0}, LX/076;-><init>(LX/072;)V

    iput-object v2, p0, LX/072;->J:LX/076;

    .line 18860
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v2, p0, LX/072;->K:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 18861
    iput-object p1, p0, LX/072;->b:LX/059;

    .line 18862
    iput-object p2, p0, LX/072;->c:LX/05h;

    .line 18863
    iput-object p3, p0, LX/072;->d:LX/05i;

    .line 18864
    iput-object p4, p0, LX/072;->e:LX/06z;

    .line 18865
    iput-object p5, p0, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 18866
    iput-object p6, p0, LX/072;->g:Ljava/util/concurrent/ExecutorService;

    .line 18867
    iput-object p7, p0, LX/072;->h:LX/05Z;

    .line 18868
    iput-object p8, p0, LX/072;->i:LX/05R;

    .line 18869
    iput-object p9, p0, LX/072;->j:LX/05F;

    .line 18870
    iput-object p10, p0, LX/072;->k:LX/05k;

    .line 18871
    move-object/from16 v0, p11

    iput-object v0, p0, LX/072;->m:LX/071;

    .line 18872
    move-object/from16 v0, p12

    iput-object v0, p0, LX/072;->n:LX/05N;

    .line 18873
    move-object/from16 v0, p13

    iput-object v0, p0, LX/072;->p:Ljava/util/concurrent/atomic/AtomicReference;

    .line 18874
    move-object/from16 v0, p14

    iput-object v0, p0, LX/072;->q:LX/05N;

    .line 18875
    iget-object v2, p0, LX/072;->J:LX/076;

    iget-object v3, p0, LX/072;->I:LX/075;

    move-object/from16 v0, p11

    invoke-interface {v0, v2, v3}, LX/071;->a(LX/076;LX/075;)V

    .line 18876
    iget-object v2, p0, LX/072;->j:LX/05F;

    invoke-interface {v2}, LX/05F;->d()Ljava/lang/String;

    move-result-object v2

    .line 18877
    const-string v3, ""

    iget-object v4, p0, LX/072;->j:LX/05F;

    invoke-interface {v4}, LX/05F;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/072;->e:LX/06z;

    invoke-virtual {v3}, LX/06z;->y()Ljava/lang/Long;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v3, p0, LX/072;->e:LX/06z;

    invoke-virtual {v3}, LX/06z;->u()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/072;->e:LX/06z;

    invoke-virtual {v3}, LX/06z;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, LX/072;->o:Z

    .line 18878
    move-object/from16 v0, p15

    iput-object v0, p0, LX/072;->r:LX/05N;

    .line 18879
    move/from16 v0, p16

    iput-boolean v0, p0, LX/072;->s:Z

    .line 18880
    move/from16 v0, p17

    iput-boolean v0, p0, LX/072;->t:Z

    .line 18881
    return-void

    .line 18882
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static synthetic a(LX/072;J)J
    .locals 1

    .prologue
    .line 18883
    iput-wide p1, p0, LX/072;->x:J

    return-wide p1
.end method

.method private static declared-synchronized a(LX/072;LX/0AX;LX/0BJ;Ljava/lang/Throwable;)Ljava/util/concurrent/Future;
    .locals 3
    .param p2    # LX/0BJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0AX;",
            "LX/0BJ;",
            "Ljava/lang/Throwable;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 18884
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/072;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18885
    iget-object v0, p0, LX/072;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/mqtt/protocol/MqttClient$8;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/rti/mqtt/protocol/MqttClient$8;-><init>(LX/072;LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    const v2, -0x2116a319

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 18886
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, LX/07C;->a:LX/07C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 18887
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a$redex0(LX/072;Ljava/lang/String;[BIILX/0AM;JLjava/lang/String;)V
    .locals 10
    .param p4    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 18888
    iget-object v2, p0, LX/072;->i:LX/05R;

    invoke-interface {v2, p1, p2}, LX/05R;->b(Ljava/lang/String;[B)Ljava/lang/Object;

    move-result-object v4

    .line 18889
    :try_start_0
    invoke-static {p0}, LX/072;->p(LX/072;)V

    .line 18890
    invoke-virtual {p0}, LX/072;->d()Z

    move-result v2

    if-nez v2, :cond_1

    .line 18891
    if-eqz p5, :cond_0

    .line 18892
    invoke-interface {p5}, LX/0AM;->a()V

    .line 18893
    :cond_0
    iget-object v2, p0, LX/072;->i:LX/05R;

    const/4 v3, 0x0

    const-string v5, "not_connected"

    invoke-interface {v2, v4, v3, v5}, LX/05R;->b(Ljava/lang/Object;ZLjava/lang/String;)V

    .line 18894
    :goto_0
    return-void

    .line 18895
    :cond_1
    const-string v2, "/t_sm"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/072;->p:Ljava/util/concurrent/atomic/AtomicReference;

    if-eqz v2, :cond_8

    .line 18896
    iget-object v2, p0, LX/072;->p:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 18897
    if-eqz v2, :cond_7

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 18898
    :goto_1
    if-nez v2, :cond_2

    if-nez p8, :cond_3

    :cond_2
    if-eqz v2, :cond_8

    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 18899
    :cond_3
    iget-object v2, p0, LX/072;->E:LX/077;

    .line 18900
    if-eqz v2, :cond_4

    .line 18901
    new-instance v3, LX/0BK;

    sget-object v5, LX/0Ht;->REF_CODE_EXPIRED:LX/0Ht;

    invoke-direct {v3, v5}, LX/0BK;-><init>(LX/0Ht;)V

    invoke-virtual {v2, p4, v3}, LX/077;->a(ILjava/lang/Throwable;)V

    .line 18902
    :cond_4
    if-eqz p5, :cond_5

    .line 18903
    invoke-interface {p5}, LX/0AM;->a()V

    .line 18904
    :cond_5
    iget-object v2, p0, LX/072;->i:LX/05R;

    const/4 v3, 0x0

    const-string v5, "ref_code_expired"

    invoke-interface {v2, v4, v3, v5}, LX/05R;->b(Ljava/lang/Object;ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 18905
    :catch_0
    move-exception v2

    .line 18906
    const-string v3, "MqttClient"

    const-string v5, "exception/publish"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v3, v2, v5, v6}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18907
    invoke-static {v2}, LX/0AX;->getFromWriteException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v3

    sget-object v5, LX/0BJ;->PUBLISH:LX/0BJ;

    invoke-static {p0, v3, v5, v2}, LX/072;->b(LX/072;LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 18908
    if-eqz p5, :cond_6

    .line 18909
    invoke-interface {p5}, LX/0AM;->a()V

    .line 18910
    :cond_6
    iget-object v3, p0, LX/072;->i:LX/05R;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "publish_exception:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v8, v2}, LX/05R;->b(Ljava/lang/Object;ZLjava/lang/String;)V

    goto :goto_0

    :cond_7
    move-object v2, v3

    .line 18911
    goto :goto_1

    .line 18912
    :cond_8
    :try_start_1
    iget-object v2, p0, LX/072;->m:LX/071;

    invoke-interface {v2, p1, p2, p3, p4}, LX/071;->a(Ljava/lang/String;[BII)V

    .line 18913
    const-wide/16 v2, 0x0

    cmp-long v2, p6, v2

    if-lez v2, :cond_9

    .line 18914
    iget-object v2, p0, LX/072;->d:LX/05i;

    const-class v3, LX/0BB;

    invoke-virtual {v2, v3}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v2

    check-cast v2, LX/0BB;

    sget-object v3, LX/0BC;->StackSendingLatencyMs:LX/0BC;

    iget-object v5, p0, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v5}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v6

    sub-long v6, v6, p6

    invoke-virtual {v2, v3, v6, v7}, LX/0BB;->a(LX/0BC;J)V

    .line 18915
    :cond_9
    iget-object v2, p0, LX/072;->E:LX/077;

    .line 18916
    if-eqz v2, :cond_a

    .line 18917
    sget-object v3, LX/07S;->PUBLISH:LX/07S;

    invoke-virtual {v3}, LX/07S;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p4}, LX/077;->a(Ljava/lang/String;I)V

    .line 18918
    :cond_a
    if-eqz p5, :cond_b

    .line 18919
    iget-object v2, p0, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    invoke-interface {p5, v2, v3}, LX/0AM;->a(J)V

    .line 18920
    :cond_b
    iget-object v2, p0, LX/072;->i:LX/05R;

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-interface {v2, v4, v3, v5}, LX/05R;->b(Ljava/lang/Object;ZLjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public static synthetic b(LX/072;J)J
    .locals 1

    .prologue
    .line 18921
    iput-wide p1, p0, LX/072;->y:J

    return-wide p1
.end method

.method private b(J)LX/05B;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/05B",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18922
    iget-object v0, p0, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    .line 18923
    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    .line 18924
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v0, v0

    .line 18925
    :goto_0
    return-object v0

    .line 18926
    :cond_0
    sub-long/2addr v0, p1

    .line 18927
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/072;LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V
    .locals 16
    .param p2    # LX/0BJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 18928
    const-string v2, "MqttClient"

    const-string v3, "connection/disconnecting; operation=%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-static {v2, v3, v4}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18929
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->d:LX/05i;

    invoke-virtual {v2}, LX/05i;->e()V

    .line 18930
    monitor-enter p0

    .line 18931
    :try_start_0
    invoke-virtual/range {p0 .. p0}, LX/072;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 18932
    monitor-exit p0

    .line 18933
    :goto_0
    return-void

    .line 18934
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, LX/072;->E:LX/077;

    .line 18935
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->m:LX/071;

    invoke-interface {v2}, LX/071;->a()V

    .line 18936
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18937
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->d:LX/05i;

    const-class v4, LX/06p;

    invoke-virtual {v2, v4}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v2

    check-cast v2, LX/06p;

    sget-object v4, LX/06w;->LastDisconnectReason:LX/06w;

    invoke-virtual/range {p1 .. p1}, LX/0AX;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 18938
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->d:LX/05i;

    invoke-virtual {v2}, LX/05i;->d()V

    .line 18939
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->d:LX/05i;

    const-class v4, LX/0BF;

    invoke-virtual {v2, v4}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v2

    check-cast v2, LX/0BF;

    sget-object v4, LX/0BG;->MqttTotalDurationMs:LX/0BG;

    invoke-virtual {v2, v4}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v4}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, LX/072;->i()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 18940
    if-eqz v3, :cond_2

    .line 18941
    invoke-virtual {v3}, LX/077;->b()V

    .line 18942
    sget-object v2, LX/0AX;->READ_FAILURE_UNCLASSIFIED:LX/0AX;

    move-object/from16 v0, p1

    if-eq v0, v2, :cond_1

    sget-object v2, LX/0AX;->WRITE_FAILURE_UNCLASSIFIED:LX/0AX;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_2

    .line 18943
    :cond_1
    const-string v2, "Mqtt Unknown Exception"

    invoke-virtual/range {p1 .. p1}, LX/0AX;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v3, v2, v4, v0}, LX/077;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 18944
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->c:LX/05h;

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/072;->u:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, LX/072;->b(J)LX/05B;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/072;->v:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, LX/072;->b(J)LX/05B;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/072;->w:J

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, LX/072;->b(J)LX/05B;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/072;->x:J

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, LX/072;->b(J)LX/05B;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, LX/0AX;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, LX/0BJ;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v8

    invoke-static/range {p3 .. p3}, LX/05B;->b(Ljava/lang/Object;)LX/05B;

    move-result-object v9

    move-object/from16 v0, p0

    iget-wide v10, v0, LX/072;->C:J

    move-object/from16 v0, p0

    iget-object v12, v0, LX/072;->b:LX/059;

    invoke-virtual {v12}, LX/059;->h()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v14, v0, LX/072;->B:Landroid/net/NetworkInfo;

    invoke-direct/range {p0 .. p0}, LX/072;->s()Z

    move-result v15

    invoke-virtual/range {v2 .. v15}, LX/05h;->a(LX/05B;LX/05B;LX/05B;LX/05B;LX/05B;LX/05B;LX/05B;JJLandroid/net/NetworkInfo;Z)V

    .line 18945
    const-wide v2, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/072;->u:J

    .line 18946
    const-wide v2, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/072;->v:J

    .line 18947
    const-wide v2, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/072;->w:J

    .line 18948
    const-wide v2, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/072;->x:J

    .line 18949
    const-wide v2, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/072;->y:J

    goto/16 :goto_0

    .line 18950
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private static c(LX/072;J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 18951
    invoke-direct {p0, p1, p2}, LX/072;->b(J)LX/05B;

    move-result-object v0

    .line 18952
    invoke-virtual {v0}, LX/05B;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18953
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    .line 18954
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    .line 18955
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "N/A"

    goto :goto_0
.end method

.method public static synthetic i(LX/072;)J
    .locals 2

    .prologue
    .line 18956
    iget-wide v0, p0, LX/072;->x:J

    return-wide v0
.end method

.method public static declared-synchronized n(LX/072;)V
    .locals 1

    .prologue
    .line 18957
    monitor-enter p0

    const v0, 0x55bcdf19

    :try_start_0
    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18958
    monitor-exit p0

    return-void

    .line 18959
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static p(LX/072;)V
    .locals 2

    .prologue
    .line 18960
    iget-object v0, p0, LX/072;->e:LX/06z;

    invoke-virtual {v0}, LX/06z;->k()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, LX/072;->a(J)V

    .line 18961
    return-void
.end method

.method private r()J
    .locals 5

    .prologue
    .line 18963
    const-wide/16 v0, 0x0

    .line 18964
    sget-object v2, LX/072;->a:Ljava/util/EnumSet;

    invoke-virtual {v2}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/073;

    .line 18965
    invoke-virtual {v0}, LX/073;->getMask()J

    move-result-wide v0

    or-long/2addr v0, v2

    move-wide v2, v0

    .line 18966
    goto :goto_0

    .line 18967
    :cond_0
    sget-object v0, LX/073;->USE_SEND_PINGRESP:LX/073;

    invoke-virtual {v0}, LX/073;->getMask()J

    move-result-wide v0

    or-long/2addr v2, v0

    .line 18968
    iget-object v0, p0, LX/072;->q:LX/05N;

    invoke-interface {v0}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/072;->b:LX/059;

    invoke-virtual {v0}, LX/059;->b()LX/0HW;

    move-result-object v0

    sget-object v1, LX/0HW;->MOBILE_2G:LX/0HW;

    if-ne v0, v1, :cond_3

    .line 18969
    sget-object v0, LX/073;->DATA_SAVING_MODE:LX/073;

    invoke-virtual {v0}, LX/073;->getMask()J

    move-result-wide v0

    or-long/2addr v0, v2

    .line 18970
    :goto_1
    iget-object v2, p0, LX/072;->e:LX/06z;

    .line 18971
    iget-boolean v3, v2, LX/06z;->q:Z

    move v2, v3

    .line 18972
    if-eqz v2, :cond_1

    .line 18973
    sget-object v2, LX/073;->REQUIRE_REPLAY_PROTECTION:LX/073;

    invoke-virtual {v2}, LX/073;->getMask()J

    move-result-wide v2

    or-long/2addr v0, v2

    .line 18974
    :cond_1
    iget-object v2, p0, LX/072;->e:LX/06z;

    .line 18975
    iget-boolean v3, v2, LX/06z;->A:Z

    move v2, v3

    .line 18976
    if-eqz v2, :cond_2

    .line 18977
    sget-object v2, LX/073;->TYPING_OFF_WHEN_SENDING_MESSAGE:LX/073;

    invoke-virtual {v2}, LX/073;->getMask()J

    move-result-wide v2

    or-long/2addr v0, v2

    .line 18978
    :cond_2
    return-wide v0

    :cond_3
    move-wide v0, v2

    goto :goto_1
.end method

.method private s()Z
    .locals 1

    .prologue
    .line 18979
    iget-object v0, p0, LX/072;->r:LX/05N;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/072;->r:LX/05N;

    invoke-interface {v0}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(ILjava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 18980
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/072;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 18981
    new-instance v0, LX/0BK;

    sget-object v1, LX/0Ht;->NOT_CONNECTED:LX/0Ht;

    invoke-direct {v0, v1}, LX/0BK;-><init>(LX/0Ht;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18982
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 18983
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/072;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/mqtt/protocol/MqttClient$4;

    invoke-direct {v1, p0, p2, p1}, Lcom/facebook/rti/mqtt/protocol/MqttClient$4;-><init>(LX/072;Ljava/util/List;I)V

    const v2, 0x59259113

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 18984
    monitor-exit p0

    return p1
.end method

.method public final declared-synchronized a(Ljava/lang/String;[BLX/0AL;ILX/0AM;JLjava/lang/String;)I
    .locals 12
    .param p5    # LX/0AM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 18985
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/072;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 18986
    new-instance v0, LX/0BK;

    sget-object v1, LX/0Ht;->NOT_CONNECTED:LX/0Ht;

    invoke-direct {v0, v1}, LX/0BK;-><init>(LX/0Ht;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18987
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 18988
    :cond_0
    :try_start_1
    const-string v0, "MqttClient"

    const-string v1, "send/publish; topic=%s, qos=%d, id=%d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget v4, p3, LX/0AL;->mValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18989
    iget-object v0, p0, LX/072;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/mqtt/protocol/MqttClient$6;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-wide/from16 v8, p6

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, Lcom/facebook/rti/mqtt/protocol/MqttClient$6;-><init>(LX/072;Ljava/lang/String;[BLX/0AL;ILX/0AM;JLjava/lang/String;)V

    const v2, -0xcbe8a68

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 18990
    monitor-exit p0

    return p4
.end method

.method public final a(Ljava/util/Map;)Landroid/util/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0AF;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 18822
    iget-object v6, p0, LX/072;->H:Ljava/util/Map;

    monitor-enter v6

    .line 18823
    :try_start_0
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v3

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0AF;

    .line 18824
    iget-object v7, p0, LX/072;->H:Ljava/util/Map;

    iget-object v8, v0, LX/0AF;->a:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 18825
    iget-object v7, p0, LX/072;->H:Ljava/util/Map;

    iget-object v8, v0, LX/0AF;->a:Ljava/lang/String;

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18826
    if-nez v1, :cond_0

    .line 18827
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 18828
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object v0, v1

    move-object v1, v0

    .line 18829
    goto :goto_0

    .line 18830
    :cond_2
    iget-object v0, p0, LX/072;->H:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v2, v3

    .line 18831
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 18832
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 18833
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    invoke-interface {p1, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 18834
    if-nez v2, :cond_3

    .line 18835
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 18836
    :cond_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18837
    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    :cond_4
    move-object v0, v2

    move-object v2, v0

    .line 18838
    goto :goto_1

    .line 18839
    :cond_5
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v7, p0, LX/072;->H:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    if-ne v0, v7, :cond_6

    move v0, v4

    :goto_2
    invoke-static {v0}, LX/01n;->b(Z)V

    .line 18840
    monitor-exit v6

    .line 18841
    if-nez v1, :cond_7

    if-nez v2, :cond_7

    move-object v0, v3

    .line 18842
    :goto_3
    return-object v0

    :cond_6
    move v0, v5

    .line 18843
    goto :goto_2

    .line 18844
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 18845
    :cond_7
    const-string v0, "MqttClient"

    const-string v3, "topic diff %s %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v5

    aput-object v2, v6, v4

    invoke-static {v0, v3, v6}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18846
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_3
.end method

.method public final declared-synchronized a(LX/0AX;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0AX;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 18847
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/0BJ;->DISCONNECT:LX/0BJ;

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, LX/072;->a(LX/072;LX/0AX;LX/0BJ;Ljava/lang/Throwable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Exception;LX/0BJ;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Exception;",
            "LX/0BJ;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 18713
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/0AX;->OPERATION_TIMEOUT:LX/0AX;

    invoke-static {p0, v0, p2, p1}, LX/072;->a(LX/072;LX/0AX;LX/0BJ;Ljava/lang/Throwable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)V
    .locals 7

    .prologue
    .line 18714
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    .line 18715
    :goto_0
    invoke-virtual {p0}, LX/072;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 18716
    iget-object v2, p0, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    sub-long/2addr v2, v0

    .line 18717
    sub-long v2, p1, v2

    .line 18718
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 18719
    const v4, 0x4b9ff671    # 2.0966626E7f

    invoke-static {p0, v2, v3, v4}, LX/02L;->a(Ljava/lang/Object;JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 18720
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 18721
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Ljava/io/PrintWriter;)V
    .locals 4

    .prologue
    .line 18722
    monitor-enter p0

    :try_start_0
    const-string v0, "[ MqttClient ]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 18723
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "state="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/072;->z:LX/074;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 18724
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "lastMessageSent="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/072;->w:J

    invoke-static {p0, v2, v3}, LX/072;->c(LX/072;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 18725
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "lastMessageReceived="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/072;->x:J

    invoke-static {p0, v2, v3}, LX/072;->c(LX/072;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 18726
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "connectionEstablished="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/072;->u:J

    invoke-static {p0, v2, v3}, LX/072;->c(LX/072;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 18727
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "lastPing="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/072;->v:J

    invoke-static {p0, v2, v3}, LX/072;->c(LX/072;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 18728
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "peer="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/072;->m:LX/071;

    invoke-interface {v1}, LX/071;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18729
    monitor-exit p0

    return-void

    .line 18730
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/List;Z)V
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 18731
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->z:LX/074;

    sget-object v3, LX/074;->INIT:LX/074;

    if-eq v2, v3, :cond_0

    .line 18732
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Tried to connect on used client"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18733
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 18734
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/072;->y:J

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/072;->D:J

    .line 18735
    invoke-static/range {p1 .. p1}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    move-object/from16 v0, p0

    iput-object v2, v0, LX/072;->F:Ljava/util/List;

    .line 18736
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->e:LX/06z;

    invoke-virtual {v2}, LX/06z;->j()I

    move-result v27

    .line 18737
    const-string v2, "MqttClient"

    const-string v3, "send/connect; keepaliveSec=%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18738
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 18739
    move-object/from16 v0, p0

    iget-object v3, v0, LX/072;->H:Ljava/util/Map;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 18740
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->e:LX/06z;

    invoke-virtual {v2}, LX/06z;->r()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0AF;

    .line 18741
    iget-object v5, v2, LX/0AF;->a:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18742
    move-object/from16 v0, p0

    iget-object v5, v0, LX/072;->H:Ljava/util/Map;

    iget-object v6, v2, LX/0AF;->a:Ljava/lang/String;

    invoke-interface {v5, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 18743
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 18744
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/072;->o:Z

    if-eqz v2, :cond_2

    .line 18745
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->e:LX/06z;

    invoke-virtual {v2}, LX/06z;->i()LX/05a;

    move-result-object v2

    invoke-virtual {v2}, LX/05a;->b()Ljava/lang/String;

    move-result-object v4

    .line 18746
    const/4 v11, 0x0

    .line 18747
    :goto_1
    const/4 v3, 0x0

    .line 18748
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->n:LX/05N;

    invoke-interface {v2}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 18749
    if-eqz v2, :cond_9

    .line 18750
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    .line 18751
    const-string v2, ""

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 18752
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->b:LX/059;

    invoke-virtual {v2}, LX/059;->d()Landroid/net/NetworkInfo;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/072;->B:Landroid/net/NetworkInfo;

    .line 18753
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->b:LX/059;

    invoke-virtual {v2}, LX/059;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/072;->A:Ljava/lang/String;

    .line 18754
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/072;->C:J

    .line 18755
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v3

    .line 18756
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->e:LX/06z;

    invoke-virtual {v2}, LX/06z;->e()LX/06k;

    move-result-object v2

    invoke-virtual {v2}, LX/06k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v3

    .line 18757
    :goto_3
    :try_start_7
    new-instance v2, LX/078;

    invoke-direct/range {p0 .. p0}, LX/072;->r()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/072;->C:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, LX/072;->B:Landroid/net/NetworkInfo;

    if-eqz v7, :cond_3

    move-object/from16 v0, p0

    iget-object v7, v0, LX/072;->B:Landroid/net/NetworkInfo;

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getType()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    :goto_4
    move-object/from16 v0, p0

    iget-object v8, v0, LX/072;->B:Landroid/net/NetworkInfo;

    if-eqz v8, :cond_4

    move-object/from16 v0, p0

    iget-object v8, v0, LX/072;->B:Landroid/net/NetworkInfo;

    invoke-virtual {v8}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    :goto_5
    move-object/from16 v0, p0

    iget-object v9, v0, LX/072;->e:LX/06z;

    invoke-virtual {v9}, LX/06z;->s()Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v12, v0, LX/072;->e:LX/06z;

    invoke-virtual {v12}, LX/06z;->g()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, LX/072;->e:LX/06z;

    invoke-virtual {v13}, LX/06z;->t()Z

    move-result v13

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, LX/072;->e:LX/06z;

    invoke-virtual {v14}, LX/06z;->q()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, LX/072;->e:LX/06z;

    invoke-virtual {v15}, LX/06z;->n()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, LX/072;->j:LX/05F;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, LX/05F;->b()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, LX/072;->e:LX/06z;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, LX/06z;->h()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/072;->o:Z

    move/from16 v19, v0

    if-eqz v19, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, LX/072;->e:LX/06z;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, LX/06z;->u()Ljava/lang/String;

    move-result-object v19

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, LX/072;->e:LX/06z;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, LX/06z;->y()Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, LX/072;->e:LX/06z;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, LX/06z;->z()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, LX/072;->e:LX/06z;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, LX/06z;->A()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, LX/072;->e:LX/06z;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, LX/06z;->B()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, LX/072;->m:LX/071;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, LX/071;->g()B

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, LX/072;->e:LX/06z;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, LX/06z;->D()Ljava/util/Map;

    move-result-object v26

    invoke-direct/range {v2 .. v26}, LX/078;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Byte;Ljava/util/Map;)V

    .line 18758
    const-string v3, "MqttClient"

    const-string v4, "connection/connecting; username=%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18759
    move-object/from16 v0, p0

    iget-object v3, v0, LX/072;->e:LX/06z;

    invoke-virtual {v3}, LX/06z;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x14

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 18760
    :goto_7
    new-instance v6, LX/079;

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/072;->o:Z

    if-eqz v3, :cond_7

    const/4 v3, 0x0

    :goto_8
    move-object/from16 v0, p1

    invoke-direct {v6, v4, v2, v3, v0}, LX/079;-><init>(Ljava/lang/String;LX/078;Ljava/lang/String;Ljava/util/List;)V

    .line 18761
    sget-object v2, LX/074;->CONNECTING:LX/074;

    move-object/from16 v0, p0

    iput-object v2, v0, LX/072;->z:LX/074;

    .line 18762
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->d:LX/05i;

    invoke-virtual {v2}, LX/05i;->f()V

    .line 18763
    if-eqz p2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->e:LX/06z;

    invoke-virtual {v2}, LX/06z;->c()I

    move-result v2

    :goto_9
    move-object/from16 v0, p0

    iput v2, v0, LX/072;->G:I

    .line 18764
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->m:LX/071;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/072;->e:LX/06z;

    invoke-virtual {v3}, LX/06z;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, LX/072;->G:I

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/072;->o:Z

    move-object/from16 v0, p0

    iget-object v7, v0, LX/072;->e:LX/06z;

    invoke-virtual {v7}, LX/06z;->d()Z

    move-result v8

    move/from16 v7, v27

    invoke-interface/range {v2 .. v8}, LX/071;->a(Ljava/lang/String;IZLX/079;IZ)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 18765
    monitor-exit p0

    return-void

    .line 18766
    :cond_2
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->e:LX/06z;

    invoke-virtual {v2}, LX/06z;->i()LX/05a;

    move-result-object v2

    invoke-virtual {v2}, LX/05a;->a()Ljava/lang/String;

    move-result-object v4

    .line 18767
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->e:LX/06z;

    invoke-virtual {v2}, LX/06z;->f()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    .line 18768
    :cond_3
    const/4 v7, 0x0

    goto/16 :goto_4

    :cond_4
    const/4 v8, 0x0

    goto/16 :goto_5

    :cond_5
    const/16 v19, 0x0

    goto/16 :goto_6

    .line 18769
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, LX/072;->e:LX/06z;

    invoke-virtual {v3}, LX/06z;->f()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x14

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    goto :goto_7

    .line 18770
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, LX/072;->e:LX/06z;

    invoke-virtual {v3}, LX/06z;->e()LX/06k;

    move-result-object v3

    invoke-virtual {v3}, LX/06k;->b()Ljava/lang/String;

    move-result-object v3

    goto :goto_8

    .line 18771
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, LX/072;->e:LX/06z;

    invoke-virtual {v2}, LX/06z;->b()I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v2

    goto :goto_9

    :catch_0
    goto/16 :goto_3

    :cond_9
    move-object/from16 v20, v3

    goto/16 :goto_2
.end method

.method public final declared-synchronized b(ILjava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 18772
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/072;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 18773
    new-instance v0, LX/0BK;

    sget-object v1, LX/0Ht;->NOT_CONNECTED:LX/0Ht;

    invoke-direct {v0, v1}, LX/0BK;-><init>(LX/0Ht;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18774
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 18775
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/072;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/mqtt/protocol/MqttClient$5;

    invoke-direct {v1, p0, p2, p1}, Lcom/facebook/rti/mqtt/protocol/MqttClient$5;-><init>(LX/072;Ljava/util/List;I)V

    const v2, 0x9fc862f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 18776
    monitor-exit p0

    return p1
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 18820
    iget-object v0, p0, LX/072;->z:LX/074;

    .line 18821
    sget-object v1, LX/074;->CONNECTED:LX/074;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/074;->CONNECTING:LX/074;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 18777
    iget-object v0, p0, LX/072;->z:LX/074;

    sget-object v1, LX/074;->CONNECTING:LX/074;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 18778
    iget-object v0, p0, LX/072;->z:LX/074;

    sget-object v1, LX/074;->CONNECTED:LX/074;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 18779
    iget-object v0, p0, LX/072;->z:LX/074;

    sget-object v1, LX/074;->DISCONNECTED:LX/074;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized g()J
    .locals 2

    .prologue
    .line 18780
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/072;->y:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()Landroid/net/NetworkInfo;
    .locals 1

    .prologue
    .line 18781
    iget-object v0, p0, LX/072;->B:Landroid/net/NetworkInfo;

    return-object v0
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 18782
    iget-wide v0, p0, LX/072;->C:J

    return-wide v0
.end method

.method public final declared-synchronized j()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18783
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/072;->F:Ljava/util/List;

    .line 18784
    iget-object v1, p0, LX/072;->K:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    .line 18785
    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    .line 18786
    iget-object v1, p0, LX/072;->E:LX/077;

    const-string v2, "Mqtt Unknown Exception"

    const-string v3, "getAndResetConnectMessage being called twice"

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v1, v2, v3, v4}, LX/077;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 18787
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, LX/072;->F:Ljava/util/List;

    .line 18788
    if-nez v0, :cond_1

    .line 18789
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 18790
    :cond_1
    monitor-exit p0

    return-object v0

    .line 18791
    :cond_2
    if-nez v0, :cond_0

    .line 18792
    :try_start_1
    iget-object v1, p0, LX/072;->E:LX/077;

    const-string v2, "Mqtt Unknown Exception"

    const-string v3, "connectMessage is null"

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v1, v2, v3, v4}, LX/077;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 18793
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18794
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 18795
    iget-object v1, p0, LX/072;->F:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18796
    monitor-exit p0

    return-object v0

    .line 18797
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized l()V
    .locals 3

    .prologue
    .line 18798
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/072;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 18799
    new-instance v0, LX/0BK;

    sget-object v1, LX/0Ht;->NOT_CONNECTED:LX/0Ht;

    invoke-direct {v0, v1}, LX/0BK;-><init>(LX/0Ht;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18800
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 18801
    :cond_0
    :try_start_1
    const-string v0, "MqttClient"

    const-string v1, "send/ping"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18802
    iget-object v0, p0, LX/072;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/mqtt/protocol/MqttClient$7;

    invoke-direct {v1, p0}, Lcom/facebook/rti/mqtt/protocol/MqttClient$7;-><init>(LX/072;)V

    const v2, 0x535f92ef

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 18803
    monitor-exit p0

    return-void
.end method

.method public final m()I
    .locals 2

    .prologue
    .line 18804
    iget-object v0, p0, LX/072;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 18805
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18806
    const-string v1, "[MqttClient ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18807
    iget-object v1, p0, LX/072;->e:LX/06z;

    .line 18808
    iget-object v2, v1, LX/06z;->D:Ljava/lang/String;

    move-object v1, v2

    .line 18809
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18810
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18811
    iget v1, p0, LX/072;->G:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18812
    iget-object v1, p0, LX/072;->e:LX/06z;

    .line 18813
    iget-boolean v2, v1, LX/06z;->y:Z

    move v1, v2

    .line 18814
    if-eqz v1, :cond_0

    .line 18815
    const-string v1, " +ssl"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18816
    :cond_0
    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18817
    iget-object v1, p0, LX/072;->z:LX/074;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18818
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18819
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
