.class public final LX/08k;
.super LX/08e;
.source ""


# instance fields
.field public final synthetic a:LX/0ad;

.field public final synthetic b:LX/07y;


# direct methods
.method public constructor <init>(LX/07y;LX/0ad;)V
    .locals 0

    .prologue
    .line 21551
    iput-object p1, p0, LX/08k;->b:LX/07y;

    iput-object p2, p0, LX/08k;->a:LX/0ad;

    invoke-direct {p0}, LX/08e;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 21552
    iget-object v0, p0, LX/08k;->a:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget v2, LX/08j;->c:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(LX/0c0;I)V

    .line 21553
    return-void
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 21554
    iget-object v1, p0, LX/08k;->a:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget v4, LX/08j;->c:I

    invoke-interface {v1, v2, v3, v4, v0}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    .line 21555
    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    :cond_0
    move v0, v1

    .line 21556
    :cond_1
    return v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 21557
    iget-object v0, p0, LX/08k;->b:LX/07y;

    iget-object v0, v0, LX/07y;->d:LX/09k;

    invoke-virtual {v0}, LX/09k;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 21558
    iget-object v0, p0, LX/08k;->b:LX/07y;

    .line 21559
    iget-object v1, v0, LX/07y;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/rti/orca/MqttLiteInitializer$4;

    invoke-direct {v2, v0}, Lcom/facebook/rti/orca/MqttLiteInitializer$4;-><init>(LX/07y;)V

    const v3, 0x36672868

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 21560
    iget-object v0, p0, LX/08k;->b:LX/07y;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/07y;->a$redex0(LX/07y;Z)V

    .line 21561
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 21562
    iget-object v0, p0, LX/08k;->b:LX/07y;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/07y;->a$redex0(LX/07y;Z)V

    .line 21563
    return-void
.end method
