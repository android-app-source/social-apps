.class public LX/0CQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "EmptyCatchBlock"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:LX/0CQ;


# instance fields
.field public c:Landroid/content/ServiceConnection;

.field public d:LX/0DT;

.field public e:Landroid/os/HandlerThread;

.field public f:Landroid/os/Handler;

.field public g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27249
    const-class v0, LX/0CQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0CQ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27248
    return-void
.end method

.method public static declared-synchronized a()LX/0CQ;
    .locals 2

    .prologue
    .line 27243
    const-class v1, LX/0CQ;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0CQ;->b:LX/0CQ;

    if-nez v0, :cond_0

    .line 27244
    new-instance v0, LX/0CQ;

    invoke-direct {v0}, LX/0CQ;-><init>()V

    sput-object v0, LX/0CQ;->b:LX/0CQ;

    .line 27245
    :cond_0
    sget-object v0, LX/0CQ;->b:LX/0CQ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 27246
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(LX/0CQ;LX/0C7;)V
    .locals 3

    .prologue
    .line 27239
    iget-object v0, p0, LX/0CQ;->c:Landroid/content/ServiceConnection;

    if-nez v0, :cond_0

    .line 27240
    sget-object v0, LX/0CQ;->a:Ljava/lang/String;

    const-string v1, "Callback service is not available."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27241
    :goto_0
    return-void

    .line 27242
    :cond_0
    iget-object v0, p0, LX/0CQ;->f:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/browser/lite/BrowserLiteCallbacker$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/browser/lite/BrowserLiteCallbacker$3;-><init>(LX/0CQ;LX/0C7;)V

    const v2, 0x23bfdddf

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 27221
    iget v0, p0, LX/0CQ;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0CQ;->g:I

    .line 27222
    iget-object v0, p0, LX/0CQ;->c:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_1

    .line 27223
    invoke-static {}, LX/0Dm;->a()LX/0Dm;

    move-result-object v0

    invoke-virtual {p0}, LX/0CQ;->d()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Dm;->a(Ljava/util/HashSet;)V

    .line 27224
    :cond_0
    :goto_0
    return-void

    .line 27225
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.browser.lite.BrowserLiteCallback"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 27226
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 27227
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 27228
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 27229
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_0

    .line 27230
    new-instance v2, Landroid/os/HandlerThread;

    sget-object v3, LX/0CQ;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, LX/0CQ;->e:Landroid/os/HandlerThread;

    .line 27231
    iget-object v2, p0, LX/0CQ;->e:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 27232
    new-instance v2, Landroid/os/Handler;

    iget-object v3, p0, LX/0CQ;->e:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, LX/0CQ;->f:Landroid/os/Handler;

    .line 27233
    new-instance v2, LX/0CH;

    invoke-direct {v2, p0}, LX/0CH;-><init>(LX/0CQ;)V

    iput-object v2, p0, LX/0CQ;->c:Landroid/content/ServiceConnection;

    .line 27234
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 27235
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 27236
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v3, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 27237
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 27238
    iget-object v0, p0, LX/0CQ;->c:Landroid/content/ServiceConnection;

    const/16 v1, 0x9

    const v3, 0x1e146148

    invoke-static {p1, v2, v0, v1, v3}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;JJJJJIZZZLjava/util/HashMap;Z)V
    .locals 23
    .param p17    # Ljava/util/HashMap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "JJJJJIZZZ",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 27219
    new-instance v3, LX/0C8;

    move-object/from16 v4, p0

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    move-wide/from16 v14, p11

    move/from16 v16, p13

    move/from16 v17, p14

    move/from16 v18, p15

    move/from16 v19, p16

    move-object/from16 v20, p17

    move/from16 v21, p18

    move-object/from16 v22, p1

    invoke-direct/range {v3 .. v22}, LX/0C8;-><init>(LX/0CQ;Ljava/lang/String;JJJJJIZZZLjava/util/HashMap;ZLandroid/content/Context;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 27220
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;III)V
    .locals 7

    .prologue
    .line 27217
    new-instance v0, LX/0CA;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, LX/0CA;-><init>(LX/0CQ;Ljava/lang/String;Ljava/lang/String;III)V

    invoke-static {p0, v0}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 27218
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 27202
    new-instance v0, LX/0CM;

    invoke-direct {v0, p0, p1, p2}, LX/0CM;-><init>(LX/0CQ;Ljava/lang/String;Z)V

    invoke-static {p0, v0}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 27203
    return-void
.end method

.method public final a(Ljava/util/Map;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 27215
    new-instance v0, LX/0CD;

    invoke-direct {v0, p0, p1, p2}, LX/0CD;-><init>(LX/0CQ;Ljava/util/Map;Landroid/os/Bundle;)V

    invoke-static {p0, v0}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 27216
    return-void
.end method

.method public final b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 27212
    iget-object v0, p0, LX/0CQ;->c:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 27213
    iget-object v0, p0, LX/0CQ;->f:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/browser/lite/BrowserLiteCallbacker$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/browser/lite/BrowserLiteCallbacker$2;-><init>(LX/0CQ;Landroid/content/Context;)V

    const v2, 0x5f75f489

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 27214
    :cond_0
    return-void
.end method

.method public final c(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27210
    new-instance v0, LX/0CG;

    invoke-direct {v0, p0, p1}, LX/0CG;-><init>(LX/0CQ;Landroid/content/Context;)V

    invoke-static {p0, v0}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 27211
    return-void
.end method

.method public final d()Ljava/util/HashSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 27204
    const/4 v1, 0x0

    .line 27205
    iget-object v0, p0, LX/0CQ;->d:LX/0DT;

    if-eqz v0, :cond_0

    .line 27206
    :try_start_0
    iget-object v0, p0, LX/0CQ;->d:LX/0DT;

    invoke-interface {v0}, LX/0DT;->a()Ljava/util/List;

    move-result-object v2

    .line 27207
    if-eqz v2, :cond_1

    .line 27208
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v1, v0

    .line 27209
    :cond_0
    :goto_1
    return-object v1

    :catch_0
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
