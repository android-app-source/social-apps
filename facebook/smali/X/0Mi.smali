.class public final LX/0Mi;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 46849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46850
    return-void
.end method

.method private static a(LX/0Oj;I)LX/0Mf;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 47244
    add-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 47245
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    add-int/lit8 v3, v0, 0x1

    .line 47246
    const/4 v0, 0x3

    if-ne v3, v0, :cond_0

    .line 47247
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 47248
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 47249
    const/high16 v0, 0x3f800000    # 1.0f

    .line 47250
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v1

    and-int/lit8 v5, v1, 0x1f

    move v1, v2

    .line 47251
    :goto_0
    if-ge v1, v5, :cond_1

    .line 47252
    invoke-static {p0}, LX/0Oh;->a(LX/0Oj;)[B

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47253
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47254
    :cond_1
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v6

    move v1, v2

    .line 47255
    :goto_1
    if-ge v1, v6, :cond_2

    .line 47256
    invoke-static {p0}, LX/0Oh;->a(LX/0Oj;)[B

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47257
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 47258
    :cond_2
    if-lez v5, :cond_3

    .line 47259
    new-instance v1, LX/0Oi;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {v1, v0}, LX/0Oi;-><init>([B)V

    .line 47260
    add-int/lit8 v0, v3, 0x1

    mul-int/lit8 v0, v0, 0x8

    invoke-virtual {v1, v0}, LX/0Oi;->a(I)V

    .line 47261
    invoke-static {v1}, LX/0Oh;->a(LX/0Oi;)LX/0Og;

    move-result-object v0

    iget v0, v0, LX/0Og;->d:F

    .line 47262
    :cond_3
    new-instance v1, LX/0Mf;

    invoke-direct {v1, v4, v3, v0}, LX/0Mf;-><init>(Ljava/util/List;IF)V

    return-object v1
.end method

.method private static a(LX/0Oj;IJILjava/lang/String;Z)LX/0Mg;
    .locals 24

    .prologue
    .line 47220
    const/16 v2, 0xc

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0Oj;->b(I)V

    .line 47221
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->m()I

    move-result v22

    .line 47222
    new-instance v9, LX/0Mg;

    move/from16 v0, v22

    invoke-direct {v9, v0}, LX/0Mg;-><init>(I)V

    .line 47223
    const/4 v10, 0x0

    :goto_0
    move/from16 v0, v22

    if-ge v10, v0, :cond_9

    .line 47224
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->d()I

    move-result v3

    .line 47225
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->m()I

    move-result v4

    .line 47226
    if-lez v4, :cond_2

    const/4 v2, 0x1

    :goto_1
    const-string v5, "childAtomSize should be positive"

    invoke-static {v2, v5}, LX/0Av;->a(ZLjava/lang/Object;)V

    .line 47227
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->m()I

    move-result v12

    .line 47228
    sget v2, LX/0Mc;->b:I

    if-eq v12, v2, :cond_0

    sget v2, LX/0Mc;->c:I

    if-eq v12, v2, :cond_0

    sget v2, LX/0Mc;->W:I

    if-eq v12, v2, :cond_0

    sget v2, LX/0Mc;->ag:I

    if-eq v12, v2, :cond_0

    sget v2, LX/0Mc;->d:I

    if-eq v12, v2, :cond_0

    sget v2, LX/0Mc;->e:I

    if-eq v12, v2, :cond_0

    sget v2, LX/0Mc;->f:I

    if-ne v12, v2, :cond_3

    :cond_0
    move-object/from16 v2, p0

    move/from16 v5, p1

    move-wide/from16 v6, p2

    move/from16 v8, p4

    .line 47229
    invoke-static/range {v2 .. v10}, LX/0Mi;->a(LX/0Oj;IIIJILX/0Mg;I)V

    .line 47230
    :cond_1
    :goto_2
    add-int v2, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0Oj;->b(I)V

    .line 47231
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 47232
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 47233
    :cond_3
    sget v2, LX/0Mc;->i:I

    if-eq v12, v2, :cond_4

    sget v2, LX/0Mc;->X:I

    if-eq v12, v2, :cond_4

    sget v2, LX/0Mc;->k:I

    if-eq v12, v2, :cond_4

    sget v2, LX/0Mc;->m:I

    if-eq v12, v2, :cond_4

    sget v2, LX/0Mc;->o:I

    if-eq v12, v2, :cond_4

    sget v2, LX/0Mc;->r:I

    if-eq v12, v2, :cond_4

    sget v2, LX/0Mc;->p:I

    if-eq v12, v2, :cond_4

    sget v2, LX/0Mc;->q:I

    if-eq v12, v2, :cond_4

    sget v2, LX/0Mc;->ar:I

    if-eq v12, v2, :cond_4

    sget v2, LX/0Mc;->as:I

    if-ne v12, v2, :cond_5

    :cond_4
    move-object/from16 v11, p0

    move v13, v3

    move v14, v4

    move/from16 v15, p1

    move-wide/from16 v16, p2

    move-object/from16 v18, p5

    move/from16 v19, p6

    move-object/from16 v20, v9

    move/from16 v21, v10

    .line 47234
    invoke-static/range {v11 .. v21}, LX/0Mi;->a(LX/0Oj;IIIIJLjava/lang/String;ZLX/0Mg;I)V

    goto :goto_2

    .line 47235
    :cond_5
    sget v2, LX/0Mc;->ae:I

    if-ne v12, v2, :cond_6

    .line 47236
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "application/ttml+xml"

    const/4 v13, -0x1

    move-wide/from16 v14, p2

    move-object/from16 v16, p5

    invoke-static/range {v11 .. v16}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)LX/0L4;

    move-result-object v2

    iput-object v2, v9, LX/0Mg;->b:LX/0L4;

    goto :goto_2

    .line 47237
    :cond_6
    sget v2, LX/0Mc;->ao:I

    if-ne v12, v2, :cond_7

    .line 47238
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "application/x-quicktime-tx3g"

    const/4 v13, -0x1

    move-wide/from16 v14, p2

    move-object/from16 v16, p5

    invoke-static/range {v11 .. v16}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)LX/0L4;

    move-result-object v2

    iput-object v2, v9, LX/0Mg;->b:LX/0L4;

    goto :goto_2

    .line 47239
    :cond_7
    sget v2, LX/0Mc;->ap:I

    if-ne v12, v2, :cond_8

    .line 47240
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "application/x-mp4vtt"

    const/4 v13, -0x1

    move-wide/from16 v14, p2

    move-object/from16 v16, p5

    invoke-static/range {v11 .. v16}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)LX/0L4;

    move-result-object v2

    iput-object v2, v9, LX/0Mg;->b:LX/0L4;

    goto/16 :goto_2

    .line 47241
    :cond_8
    sget v2, LX/0Mc;->aq:I

    if-ne v12, v2, :cond_1

    .line 47242
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, "application/ttml+xml"

    const/4 v14, -0x1

    const-wide/16 v18, 0x0

    move-wide/from16 v15, p2

    move-object/from16 v17, p5

    invoke-static/range {v12 .. v19}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;J)LX/0L4;

    move-result-object v2

    iput-object v2, v9, LX/0Mg;->b:LX/0L4;

    goto/16 :goto_2

    .line 47243
    :cond_9
    return-object v9
.end method

.method public static a(LX/0Md;LX/0Me;JZ)LX/0Mo;
    .locals 28

    .prologue
    .line 47202
    sget v2, LX/0Mc;->B:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0Md;->e(I)LX/0Md;

    move-result-object v8

    .line 47203
    sget v2, LX/0Mc;->P:I

    invoke-virtual {v8, v2}, LX/0Md;->d(I)LX/0Me;

    move-result-object v2

    iget-object v2, v2, LX/0Me;->aB:LX/0Oj;

    invoke-static {v2}, LX/0Mi;->e(LX/0Oj;)I

    move-result v15

    .line 47204
    sget v2, LX/0Mo;->b:I

    if-eq v15, v2, :cond_0

    sget v2, LX/0Mo;->a:I

    if-eq v15, v2, :cond_0

    sget v2, LX/0Mo;->c:I

    if-eq v15, v2, :cond_0

    sget v2, LX/0Mo;->d:I

    if-eq v15, v2, :cond_0

    sget v2, LX/0Mo;->e:I

    if-eq v15, v2, :cond_0

    .line 47205
    const/4 v13, 0x0

    .line 47206
    :goto_0
    return-object v13

    .line 47207
    :cond_0
    sget v2, LX/0Mc;->L:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0Md;->d(I)LX/0Me;

    move-result-object v2

    iget-object v2, v2, LX/0Me;->aB:LX/0Oj;

    invoke-static {v2}, LX/0Mi;->d(LX/0Oj;)LX/0Mh;

    move-result-object v16

    .line 47208
    const-wide/16 v2, -0x1

    cmp-long v2, p2, v2

    if-nez v2, :cond_3

    .line 47209
    invoke-static/range {v16 .. v16}, LX/0Mh;->a(LX/0Mh;)J

    move-result-wide v2

    .line 47210
    :goto_1
    move-object/from16 v0, p1

    iget-object v4, v0, LX/0Me;->aB:LX/0Oj;

    invoke-static {v4}, LX/0Mi;->c(LX/0Oj;)J

    move-result-wide v6

    .line 47211
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_1

    .line 47212
    const-wide/16 v10, -0x1

    .line 47213
    :goto_2
    sget v2, LX/0Mc;->C:I

    invoke-virtual {v8, v2}, LX/0Md;->e(I)LX/0Md;

    move-result-object v2

    sget v3, LX/0Mc;->D:I

    invoke-virtual {v2, v3}, LX/0Md;->e(I)LX/0Md;

    move-result-object v2

    .line 47214
    sget v3, LX/0Mc;->O:I

    invoke-virtual {v8, v3}, LX/0Md;->d(I)LX/0Me;

    move-result-object v3

    iget-object v3, v3, LX/0Me;->aB:LX/0Oj;

    invoke-static {v3}, LX/0Mi;->f(LX/0Oj;)Landroid/util/Pair;

    move-result-object v3

    .line 47215
    sget v4, LX/0Mc;->Q:I

    invoke-virtual {v2, v4}, LX/0Md;->d(I)LX/0Me;

    move-result-object v2

    iget-object v8, v2, LX/0Me;->aB:LX/0Oj;

    invoke-static/range {v16 .. v16}, LX/0Mh;->b(LX/0Mh;)I

    move-result v9

    invoke-static/range {v16 .. v16}, LX/0Mh;->c(LX/0Mh;)I

    move-result v12

    iget-object v13, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v13, Ljava/lang/String;

    move/from16 v14, p4

    invoke-static/range {v8 .. v14}, LX/0Mi;->a(LX/0Oj;IJILjava/lang/String;Z)LX/0Mg;

    move-result-object v4

    .line 47216
    sget v2, LX/0Mc;->M:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0Md;->e(I)LX/0Md;

    move-result-object v2

    invoke-static {v2}, LX/0Mi;->a(LX/0Md;)Landroid/util/Pair;

    move-result-object v5

    .line 47217
    iget-object v2, v4, LX/0Mg;->b:LX/0L4;

    if-nez v2, :cond_2

    const/4 v13, 0x0

    goto :goto_0

    .line 47218
    :cond_1
    const-wide/32 v4, 0xf4240

    invoke-static/range {v2 .. v7}, LX/08x;->a(JJJ)J

    move-result-wide v10

    goto :goto_2

    .line 47219
    :cond_2
    new-instance v13, LX/0Mo;

    invoke-static/range {v16 .. v16}, LX/0Mh;->b(LX/0Mh;)I

    move-result v14

    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    iget-object v0, v4, LX/0Mg;->b:LX/0L4;

    move-object/from16 v22, v0

    iget-object v0, v4, LX/0Mg;->a:[LX/0Mp;

    move-object/from16 v23, v0

    iget v0, v4, LX/0Mg;->c:I

    move/from16 v24, v0

    iget-object v0, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v25, v0

    check-cast v25, [J

    iget-object v0, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v26, v0

    check-cast v26, [J

    move-wide/from16 v18, v6

    move-wide/from16 v20, v10

    invoke-direct/range {v13 .. v26}, LX/0Mo;-><init>(IIJJJLX/0L4;[LX/0Mp;I[J[J)V

    goto/16 :goto_0

    :cond_3
    move-wide/from16 v2, p2

    goto/16 :goto_1
.end method

.method private static a(LX/0Oj;II)LX/0Mp;
    .locals 9

    .prologue
    .line 47166
    add-int/lit8 v1, p1, 0x8

    .line 47167
    const/4 v0, 0x0

    .line 47168
    :goto_0
    sub-int v2, v1, p1

    if-ge v2, p2, :cond_3

    .line 47169
    invoke-virtual {p0, v1}, LX/0Oj;->b(I)V

    .line 47170
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v2

    .line 47171
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v3

    .line 47172
    sget v4, LX/0Mc;->Y:I

    if-ne v3, v4, :cond_1

    .line 47173
    invoke-virtual {p0}, LX/0Oj;->m()I

    .line 47174
    :cond_0
    :goto_1
    add-int/2addr v1, v2

    .line 47175
    goto :goto_0

    .line 47176
    :cond_1
    sget v4, LX/0Mc;->T:I

    if-ne v3, v4, :cond_2

    .line 47177
    const/4 v3, 0x4

    invoke-virtual {p0, v3}, LX/0Oj;->c(I)V

    .line 47178
    invoke-virtual {p0}, LX/0Oj;->m()I

    .line 47179
    invoke-virtual {p0}, LX/0Oj;->m()I

    goto :goto_1

    .line 47180
    :cond_2
    sget v4, LX/0Mc;->U:I

    if-ne v3, v4, :cond_0

    .line 47181
    const/16 v8, 0x10

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 47182
    add-int/lit8 v4, v1, 0x8

    .line 47183
    :goto_2
    sub-int v5, v4, v1

    if-ge v5, v2, :cond_6

    .line 47184
    invoke-virtual {p0, v4}, LX/0Oj;->b(I)V

    .line 47185
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v5

    .line 47186
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v6

    .line 47187
    sget v7, LX/0Mc;->V:I

    if-ne v6, v7, :cond_5

    .line 47188
    const/4 v4, 0x4

    invoke-virtual {p0, v4}, LX/0Oj;->c(I)V

    .line 47189
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v4

    .line 47190
    shr-int/lit8 v5, v4, 0x8

    if-ne v5, v0, :cond_4

    .line 47191
    :goto_3
    and-int/lit16 v4, v4, 0xff

    .line 47192
    new-array v5, v8, [B

    .line 47193
    invoke-virtual {p0, v5, v3, v8}, LX/0Oj;->a([BII)V

    .line 47194
    new-instance v3, LX/0Mp;

    invoke-direct {v3, v0, v4, v5}, LX/0Mp;-><init>(ZI[B)V

    move-object v0, v3

    .line 47195
    :goto_4
    move-object v0, v0

    .line 47196
    goto :goto_1

    .line 47197
    :cond_3
    return-object v0

    :cond_4
    move v0, v3

    .line 47198
    goto :goto_3

    .line 47199
    :cond_5
    add-int/2addr v4, v5

    .line 47200
    goto :goto_2

    .line 47201
    :cond_6
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public static a(LX/0Mo;LX/0Md;)LX/0Mr;
    .locals 41

    .prologue
    .line 46994
    sget v2, LX/0Mc;->al:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0Md;->d(I)LX/0Me;

    move-result-object v2

    iget-object v0, v2, LX/0Me;->aB:LX/0Oj;

    move-object/from16 v32, v0

    .line 46995
    sget v2, LX/0Mc;->am:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0Md;->d(I)LX/0Me;

    move-result-object v2

    .line 46996
    if-nez v2, :cond_0

    .line 46997
    sget v2, LX/0Mc;->an:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0Md;->d(I)LX/0Me;

    move-result-object v2

    .line 46998
    :cond_0
    iget-object v0, v2, LX/0Me;->aB:LX/0Oj;

    move-object/from16 v33, v0

    .line 46999
    sget v3, LX/0Mc;->ak:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/0Md;->d(I)LX/0Me;

    move-result-object v3

    iget-object v0, v3, LX/0Me;->aB:LX/0Oj;

    move-object/from16 v34, v0

    .line 47000
    sget v3, LX/0Mc;->ah:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/0Md;->d(I)LX/0Me;

    move-result-object v3

    iget-object v0, v3, LX/0Me;->aB:LX/0Oj;

    move-object/from16 v35, v0

    .line 47001
    sget v3, LX/0Mc;->ai:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/0Md;->d(I)LX/0Me;

    move-result-object v3

    .line 47002
    if-eqz v3, :cond_1

    iget-object v3, v3, LX/0Me;->aB:LX/0Oj;

    move-object v8, v3

    .line 47003
    :goto_0
    sget v3, LX/0Mc;->aj:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/0Md;->d(I)LX/0Me;

    move-result-object v3

    .line 47004
    if-eqz v3, :cond_2

    iget-object v3, v3, LX/0Me;->aB:LX/0Oj;

    move-object v9, v3

    .line 47005
    :goto_1
    const/16 v3, 0xc

    move-object/from16 v0, v32

    invoke-virtual {v0, v3}, LX/0Oj;->b(I)V

    .line 47006
    invoke-virtual/range {v32 .. v32}, LX/0Oj;->s()I

    move-result v14

    .line 47007
    invoke-virtual/range {v32 .. v32}, LX/0Oj;->s()I

    move-result v36

    .line 47008
    move/from16 v0, v36

    new-array v3, v0, [J

    .line 47009
    move/from16 v0, v36

    new-array v4, v0, [I

    .line 47010
    const/16 v27, 0x0

    .line 47011
    move/from16 v0, v36

    new-array v6, v0, [J

    .line 47012
    move/from16 v0, v36

    new-array v7, v0, [I

    .line 47013
    if-nez v36, :cond_3

    .line 47014
    new-instance v2, LX/0Mr;

    const/4 v5, 0x0

    invoke-direct/range {v2 .. v7}, LX/0Mr;-><init>([J[II[J[I)V

    .line 47015
    :goto_2
    return-object v2

    .line 47016
    :cond_1
    const/4 v3, 0x0

    move-object v8, v3

    goto :goto_0

    .line 47017
    :cond_2
    const/4 v3, 0x0

    move-object v9, v3

    goto :goto_1

    .line 47018
    :cond_3
    const/16 v5, 0xc

    move-object/from16 v0, v33

    invoke-virtual {v0, v5}, LX/0Oj;->b(I)V

    .line 47019
    invoke-virtual/range {v33 .. v33}, LX/0Oj;->s()I

    move-result v37

    .line 47020
    const/16 v5, 0xc

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, LX/0Oj;->b(I)V

    .line 47021
    invoke-virtual/range {v34 .. v34}, LX/0Oj;->s()I

    move-result v5

    add-int/lit8 v26, v5, -0x1

    .line 47022
    invoke-virtual/range {v34 .. v34}, LX/0Oj;->m()I

    move-result v5

    const/4 v10, 0x1

    if-ne v5, v10, :cond_7

    const/4 v5, 0x1

    :goto_3
    const-string v10, "stsc first chunk must be 1"

    invoke-static {v5, v10}, LX/0Av;->b(ZLjava/lang/Object;)V

    .line 47023
    invoke-virtual/range {v34 .. v34}, LX/0Oj;->s()I

    move-result v21

    .line 47024
    const/4 v5, 0x4

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, LX/0Oj;->c(I)V

    .line 47025
    const/4 v5, -0x1

    .line 47026
    if-lez v26, :cond_4

    .line 47027
    invoke-virtual/range {v34 .. v34}, LX/0Oj;->s()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    .line 47028
    :cond_4
    const/16 v25, 0x0

    .line 47029
    const/16 v10, 0xc

    move-object/from16 v0, v35

    invoke-virtual {v0, v10}, LX/0Oj;->b(I)V

    .line 47030
    invoke-virtual/range {v35 .. v35}, LX/0Oj;->s()I

    move-result v10

    add-int/lit8 v24, v10, -0x1

    .line 47031
    invoke-virtual/range {v35 .. v35}, LX/0Oj;->s()I

    move-result v23

    .line 47032
    invoke-virtual/range {v35 .. v35}, LX/0Oj;->s()I

    move-result v22

    .line 47033
    const/16 v20, 0x0

    .line 47034
    const/16 v19, 0x0

    .line 47035
    const/16 v18, 0x0

    .line 47036
    if-eqz v9, :cond_5

    .line 47037
    const/16 v10, 0xc

    invoke-virtual {v9, v10}, LX/0Oj;->b(I)V

    .line 47038
    invoke-virtual {v9}, LX/0Oj;->s()I

    move-result v19

    .line 47039
    :cond_5
    const/4 v11, -0x1

    .line 47040
    const/4 v10, 0x0

    .line 47041
    if-eqz v8, :cond_6

    .line 47042
    const/16 v10, 0xc

    invoke-virtual {v8, v10}, LX/0Oj;->b(I)V

    .line 47043
    invoke-virtual {v8}, LX/0Oj;->s()I

    move-result v10

    .line 47044
    invoke-virtual {v8}, LX/0Oj;->s()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    .line 47045
    :cond_6
    iget v12, v2, LX/0Mc;->aA:I

    sget v13, LX/0Mc;->am:I

    if-ne v12, v13, :cond_8

    .line 47046
    invoke-virtual/range {v33 .. v33}, LX/0Oj;->k()J

    move-result-wide v12

    .line 47047
    :goto_4
    const-wide/16 v16, 0x0

    .line 47048
    const/4 v15, 0x0

    move-wide/from16 v28, v16

    move/from16 v30, v23

    move/from16 v31, v21

    move/from16 v23, v10

    move-wide/from16 v16, v12

    move/from16 v10, v18

    move/from16 v12, v20

    move/from16 v18, v25

    move/from16 v20, v21

    move/from16 v21, v26

    move/from16 v40, v24

    move/from16 v24, v11

    move/from16 v11, v19

    move/from16 v19, v5

    move/from16 v5, v27

    move/from16 v27, v15

    move/from16 v15, v22

    move/from16 v22, v40

    :goto_5
    move/from16 v0, v27

    move/from16 v1, v36

    if-ge v0, v1, :cond_12

    .line 47049
    if-eqz v9, :cond_a

    .line 47050
    :goto_6
    if-nez v12, :cond_9

    if-lez v11, :cond_9

    .line 47051
    invoke-virtual {v9}, LX/0Oj;->s()I

    move-result v12

    .line 47052
    invoke-virtual {v9}, LX/0Oj;->m()I

    move-result v10

    .line 47053
    add-int/lit8 v11, v11, -0x1

    goto :goto_6

    .line 47054
    :cond_7
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 47055
    :cond_8
    invoke-virtual/range {v33 .. v33}, LX/0Oj;->u()J

    move-result-wide v12

    goto :goto_4

    .line 47056
    :cond_9
    add-int/lit8 v12, v12, -0x1

    .line 47057
    :cond_a
    aput-wide v16, v3, v27

    .line 47058
    if-nez v14, :cond_e

    invoke-virtual/range {v32 .. v32}, LX/0Oj;->s()I

    move-result v13

    :goto_7
    aput v13, v4, v27

    .line 47059
    aget v13, v4, v27

    if-le v13, v5, :cond_b

    .line 47060
    aget v5, v4, v27

    .line 47061
    :cond_b
    int-to-long v0, v10

    move-wide/from16 v38, v0

    add-long v38, v38, v28

    aput-wide v38, v6, v27

    .line 47062
    if-nez v8, :cond_f

    const/4 v13, 0x1

    :goto_8
    aput v13, v7, v27

    .line 47063
    move/from16 v0, v27

    move/from16 v1, v24

    if-ne v0, v1, :cond_2f

    .line 47064
    const/4 v13, 0x1

    aput v13, v7, v27

    .line 47065
    add-int/lit8 v13, v23, -0x1

    .line 47066
    if-lez v13, :cond_2e

    .line 47067
    invoke-virtual {v8}, LX/0Oj;->s()I

    move-result v23

    add-int/lit8 v23, v23, -0x1

    move/from16 v25, v13

    move/from16 v26, v23

    .line 47068
    :goto_9
    int-to-long v0, v15

    move-wide/from16 v38, v0

    add-long v28, v28, v38

    .line 47069
    add-int/lit8 v13, v30, -0x1

    .line 47070
    if-nez v13, :cond_2d

    if-lez v22, :cond_2d

    .line 47071
    invoke-virtual/range {v35 .. v35}, LX/0Oj;->s()I

    move-result v15

    .line 47072
    invoke-virtual/range {v35 .. v35}, LX/0Oj;->s()I

    move-result v13

    .line 47073
    add-int/lit8 v22, v22, -0x1

    move/from16 v23, v15

    move/from16 v24, v22

    move/from16 v22, v13

    .line 47074
    :goto_a
    add-int/lit8 v15, v31, -0x1

    .line 47075
    if-nez v15, :cond_11

    .line 47076
    add-int/lit8 v18, v18, 0x1

    .line 47077
    move/from16 v0, v18

    move/from16 v1, v37

    if-ge v0, v1, :cond_c

    .line 47078
    iget v13, v2, LX/0Mc;->aA:I

    sget v16, LX/0Mc;->am:I

    move/from16 v0, v16

    if-ne v13, v0, :cond_10

    .line 47079
    invoke-virtual/range {v33 .. v33}, LX/0Oj;->k()J

    move-result-wide v16

    .line 47080
    :cond_c
    :goto_b
    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2c

    .line 47081
    invoke-virtual/range {v34 .. v34}, LX/0Oj;->s()I

    move-result v13

    .line 47082
    const/16 v20, 0x4

    move-object/from16 v0, v34

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/0Oj;->c(I)V

    .line 47083
    add-int/lit8 v20, v21, -0x1

    .line 47084
    if-lez v20, :cond_d

    .line 47085
    invoke-virtual/range {v34 .. v34}, LX/0Oj;->s()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    .line 47086
    :cond_d
    :goto_c
    move/from16 v0, v18

    move/from16 v1, v37

    if-ge v0, v1, :cond_2b

    move/from16 v15, v18

    move/from16 v18, v19

    move/from16 v19, v13

    .line 47087
    :goto_d
    add-int/lit8 v21, v27, 0x1

    move/from16 v27, v21

    move/from16 v30, v23

    move/from16 v31, v13

    move/from16 v23, v25

    move/from16 v21, v20

    move/from16 v20, v19

    move/from16 v19, v18

    move/from16 v18, v15

    move/from16 v15, v22

    move/from16 v22, v24

    move/from16 v24, v26

    goto/16 :goto_5

    :cond_e
    move v13, v14

    .line 47088
    goto/16 :goto_7

    .line 47089
    :cond_f
    const/4 v13, 0x0

    goto/16 :goto_8

    .line 47090
    :cond_10
    invoke-virtual/range {v33 .. v33}, LX/0Oj;->u()J

    move-result-wide v16

    goto :goto_b

    .line 47091
    :cond_11
    aget v13, v4, v27

    int-to-long v0, v13

    move-wide/from16 v30, v0

    add-long v16, v16, v30

    move v13, v15

    move/from16 v15, v18

    move/from16 v18, v19

    move/from16 v19, v20

    move/from16 v20, v21

    goto :goto_d

    .line 47092
    :cond_12
    if-nez v23, :cond_13

    const/4 v2, 0x1

    :goto_e
    invoke-static {v2}, LX/0Av;->a(Z)V

    .line 47093
    if-nez v30, :cond_14

    const/4 v2, 0x1

    :goto_f
    invoke-static {v2}, LX/0Av;->a(Z)V

    .line 47094
    if-nez v31, :cond_15

    const/4 v2, 0x1

    :goto_10
    invoke-static {v2}, LX/0Av;->a(Z)V

    .line 47095
    if-nez v22, :cond_16

    const/4 v2, 0x1

    :goto_11
    invoke-static {v2}, LX/0Av;->a(Z)V

    .line 47096
    if-nez v11, :cond_17

    const/4 v2, 0x1

    :goto_12
    invoke-static {v2}, LX/0Av;->a(Z)V

    .line 47097
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Mo;->m:[J

    if-nez v2, :cond_18

    .line 47098
    const-wide/32 v8, 0xf4240

    move-object/from16 v0, p0

    iget-wide v10, v0, LX/0Mo;->h:J

    invoke-static {v6, v8, v9, v10, v11}, LX/08x;->a([JJJ)V

    .line 47099
    new-instance v2, LX/0Mr;

    invoke-direct/range {v2 .. v7}, LX/0Mr;-><init>([J[II[J[I)V

    goto/16 :goto_2

    .line 47100
    :cond_13
    const/4 v2, 0x0

    goto :goto_e

    .line 47101
    :cond_14
    const/4 v2, 0x0

    goto :goto_f

    .line 47102
    :cond_15
    const/4 v2, 0x0

    goto :goto_10

    .line 47103
    :cond_16
    const/4 v2, 0x0

    goto :goto_11

    .line 47104
    :cond_17
    const/4 v2, 0x0

    goto :goto_12

    .line 47105
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Mo;->m:[J

    array-length v2, v2

    const/4 v8, 0x1

    if-ne v2, v8, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Mo;->m:[J

    const/4 v8, 0x0

    aget-wide v8, v2, v8

    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-nez v2, :cond_1a

    .line 47106
    const/4 v2, 0x0

    :goto_13
    array-length v8, v6

    if-ge v2, v8, :cond_19

    .line 47107
    aget-wide v8, v6, v2

    move-object/from16 v0, p0

    iget-object v10, v0, LX/0Mo;->n:[J

    const/4 v11, 0x0

    aget-wide v10, v10, v11

    sub-long/2addr v8, v10

    const-wide/32 v10, 0xf4240

    move-object/from16 v0, p0

    iget-wide v12, v0, LX/0Mo;->h:J

    invoke-static/range {v8 .. v13}, LX/08x;->a(JJJ)J

    move-result-wide v8

    aput-wide v8, v6, v2

    .line 47108
    add-int/lit8 v2, v2, 0x1

    goto :goto_13

    .line 47109
    :cond_19
    new-instance v2, LX/0Mr;

    invoke-direct/range {v2 .. v7}, LX/0Mr;-><init>([J[II[J[I)V

    goto/16 :goto_2

    .line 47110
    :cond_1a
    const/4 v10, 0x0

    .line 47111
    const/4 v9, 0x0

    .line 47112
    const/4 v8, 0x0

    .line 47113
    const/4 v2, 0x0

    move v14, v8

    move v15, v9

    move/from16 v16, v10

    :goto_14
    move-object/from16 v0, p0

    iget-object v8, v0, LX/0Mo;->m:[J

    array-length v8, v8

    if-ge v2, v8, :cond_1c

    .line 47114
    move-object/from16 v0, p0

    iget-object v8, v0, LX/0Mo;->n:[J

    aget-wide v18, v8, v2

    .line 47115
    const-wide/16 v8, -0x1

    cmp-long v8, v18, v8

    if-eqz v8, :cond_2a

    .line 47116
    move-object/from16 v0, p0

    iget-object v8, v0, LX/0Mo;->m:[J

    aget-wide v8, v8, v2

    move-object/from16 v0, p0

    iget-wide v10, v0, LX/0Mo;->h:J

    move-object/from16 v0, p0

    iget-wide v12, v0, LX/0Mo;->i:J

    invoke-static/range {v8 .. v13}, LX/08x;->a(JJJ)J

    move-result-wide v8

    .line 47117
    const/4 v10, 0x1

    const/4 v11, 0x1

    move-wide/from16 v0, v18

    invoke-static {v6, v0, v1, v10, v11}, LX/08x;->b([JJZZ)I

    move-result v11

    .line 47118
    add-long v8, v8, v18

    const/4 v10, 0x1

    const/4 v12, 0x0

    invoke-static {v6, v8, v9, v10, v12}, LX/08x;->b([JJZZ)I

    move-result v9

    .line 47119
    sub-int v8, v9, v11

    add-int v10, v16, v8

    .line 47120
    if-eq v15, v11, :cond_1b

    const/4 v8, 0x1

    :goto_15
    or-int/2addr v8, v14

    .line 47121
    :goto_16
    add-int/lit8 v2, v2, 0x1

    move v14, v8

    move v15, v9

    move/from16 v16, v10

    goto :goto_14

    .line 47122
    :cond_1b
    const/4 v8, 0x0

    goto :goto_15

    .line 47123
    :cond_1c
    move/from16 v0, v16

    move/from16 v1, v36

    if-eq v0, v1, :cond_1f

    const/4 v2, 0x1

    :goto_17
    or-int v23, v14, v2

    .line 47124
    if-eqz v23, :cond_20

    move/from16 v0, v16

    new-array v2, v0, [J

    move-object/from16 v22, v2

    .line 47125
    :goto_18
    if-eqz v23, :cond_21

    move/from16 v0, v16

    new-array v2, v0, [I

    move-object/from16 v21, v2

    .line 47126
    :goto_19
    if-eqz v23, :cond_22

    const/4 v10, 0x0

    .line 47127
    :goto_1a
    if-eqz v23, :cond_23

    move/from16 v0, v16

    new-array v2, v0, [I

    move-object/from16 v17, v2

    .line 47128
    :goto_1b
    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v24, v0

    .line 47129
    const-wide/16 v8, 0x0

    .line 47130
    const/4 v5, 0x0

    .line 47131
    const/4 v2, 0x0

    move v14, v5

    move-wide/from16 v18, v8

    move v5, v10

    :goto_1c
    move-object/from16 v0, p0

    iget-object v8, v0, LX/0Mo;->m:[J

    array-length v8, v8

    if-ge v2, v8, :cond_25

    .line 47132
    move-object/from16 v0, p0

    iget-object v8, v0, LX/0Mo;->n:[J

    aget-wide v26, v8, v2

    .line 47133
    move-object/from16 v0, p0

    iget-object v8, v0, LX/0Mo;->m:[J

    aget-wide v8, v8, v2

    .line 47134
    const-wide/16 v10, -0x1

    cmp-long v10, v26, v10

    if-eqz v10, :cond_29

    .line 47135
    move-object/from16 v0, p0

    iget-wide v10, v0, LX/0Mo;->h:J

    move-object/from16 v0, p0

    iget-wide v12, v0, LX/0Mo;->i:J

    invoke-static/range {v8 .. v13}, LX/08x;->a(JJJ)J

    move-result-wide v10

    add-long v12, v26, v10

    .line 47136
    const/4 v10, 0x1

    const/4 v11, 0x1

    move-wide/from16 v0, v26

    invoke-static {v6, v0, v1, v10, v11}, LX/08x;->b([JJZZ)I

    move-result v10

    .line 47137
    const/4 v11, 0x1

    const/4 v15, 0x0

    invoke-static {v6, v12, v13, v11, v15}, LX/08x;->b([JJZZ)I

    move-result v25

    .line 47138
    if-eqz v23, :cond_1d

    .line 47139
    sub-int v11, v25, v10

    .line 47140
    move-object/from16 v0, v22

    invoke-static {v3, v10, v0, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47141
    move-object/from16 v0, v21

    invoke-static {v4, v10, v0, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47142
    move-object/from16 v0, v17

    invoke-static {v7, v10, v0, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1d
    move/from16 v20, v10

    move/from16 v16, v14

    .line 47143
    :goto_1d
    move/from16 v0, v20

    move/from16 v1, v25

    if-ge v0, v1, :cond_24

    .line 47144
    const-wide/32 v12, 0xf4240

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/0Mo;->i:J

    move-wide/from16 v10, v18

    invoke-static/range {v10 .. v15}, LX/08x;->a(JJJ)J

    move-result-wide v28

    .line 47145
    aget-wide v10, v6, v20

    sub-long v10, v10, v26

    const-wide/32 v12, 0xf4240

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/0Mo;->h:J

    invoke-static/range {v10 .. v15}, LX/08x;->a(JJJ)J

    move-result-wide v10

    .line 47146
    add-long v10, v10, v28

    aput-wide v10, v24, v16

    .line 47147
    if-eqz v23, :cond_1e

    aget v10, v21, v16

    if-le v10, v5, :cond_1e

    .line 47148
    aget v5, v4, v20

    .line 47149
    :cond_1e
    add-int/lit8 v16, v16, 0x1

    .line 47150
    add-int/lit8 v10, v20, 0x1

    move/from16 v20, v10

    goto :goto_1d

    .line 47151
    :cond_1f
    const/4 v2, 0x0

    goto/16 :goto_17

    :cond_20
    move-object/from16 v22, v3

    .line 47152
    goto/16 :goto_18

    :cond_21
    move-object/from16 v21, v4

    .line 47153
    goto/16 :goto_19

    :cond_22
    move v10, v5

    .line 47154
    goto/16 :goto_1a

    :cond_23
    move-object/from16 v17, v7

    .line 47155
    goto/16 :goto_1b

    :cond_24
    move v10, v5

    move/from16 v5, v16

    .line 47156
    :goto_1e
    add-long v8, v8, v18

    .line 47157
    add-int/lit8 v2, v2, 0x1

    move v14, v5

    move-wide/from16 v18, v8

    move v5, v10

    goto/16 :goto_1c

    .line 47158
    :cond_25
    const/4 v3, 0x0

    .line 47159
    const/4 v2, 0x0

    :goto_1f
    move-object/from16 v0, v17

    array-length v4, v0

    if-ge v2, v4, :cond_27

    if-nez v3, :cond_27

    .line 47160
    aget v4, v17, v2

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_26

    const/4 v4, 0x1

    :goto_20
    or-int/2addr v3, v4

    .line 47161
    add-int/lit8 v2, v2, 0x1

    goto :goto_1f

    .line 47162
    :cond_26
    const/4 v4, 0x0

    goto :goto_20

    .line 47163
    :cond_27
    if-nez v3, :cond_28

    .line 47164
    new-instance v2, LX/0L6;

    const-string v3, "The edited sample sequence does not contain a sync sample."

    invoke-direct {v2, v3}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v2

    .line 47165
    :cond_28
    new-instance v2, LX/0Mr;

    move-object/from16 v3, v22

    move-object/from16 v4, v21

    move-object/from16 v6, v24

    move-object/from16 v7, v17

    invoke-direct/range {v2 .. v7}, LX/0Mr;-><init>([J[II[J[I)V

    goto/16 :goto_2

    :cond_29
    move v10, v5

    move v5, v14

    goto :goto_1e

    :cond_2a
    move v8, v14

    move v9, v15

    move/from16 v10, v16

    goto/16 :goto_16

    :cond_2b
    move/from16 v40, v15

    move/from16 v15, v18

    move/from16 v18, v19

    move/from16 v19, v13

    move/from16 v13, v40

    goto/16 :goto_d

    :cond_2c
    move/from16 v13, v20

    move/from16 v20, v21

    goto/16 :goto_c

    :cond_2d
    move/from16 v23, v13

    move/from16 v24, v22

    move/from16 v22, v15

    goto/16 :goto_a

    :cond_2e
    move/from16 v25, v13

    move/from16 v26, v24

    goto/16 :goto_9

    :cond_2f
    move/from16 v25, v23

    move/from16 v26, v24

    goto/16 :goto_9
.end method

.method private static a(LX/0Md;)Landroid/util/Pair;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Md;",
            ")",
            "Landroid/util/Pair",
            "<[J[J>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 46973
    if-eqz p0, :cond_0

    sget v0, LX/0Mc;->N:I

    invoke-virtual {p0, v0}, LX/0Md;->d(I)LX/0Me;

    move-result-object v0

    if-nez v0, :cond_1

    .line 46974
    :cond_0
    invoke-static {v1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 46975
    :goto_0
    return-object v0

    .line 46976
    :cond_1
    iget-object v3, v0, LX/0Me;->aB:LX/0Oj;

    .line 46977
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, LX/0Oj;->b(I)V

    .line 46978
    invoke-virtual {v3}, LX/0Oj;->m()I

    move-result v0

    .line 46979
    invoke-static {v0}, LX/0Mc;->a(I)I

    move-result v4

    .line 46980
    invoke-virtual {v3}, LX/0Oj;->s()I

    move-result v5

    .line 46981
    new-array v6, v5, [J

    .line 46982
    new-array v7, v5, [J

    .line 46983
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_5

    .line 46984
    if-ne v4, v8, :cond_2

    invoke-virtual {v3}, LX/0Oj;->u()J

    move-result-wide v0

    :goto_2
    aput-wide v0, v6, v2

    .line 46985
    if-ne v4, v8, :cond_3

    invoke-virtual {v3}, LX/0Oj;->o()J

    move-result-wide v0

    :goto_3
    aput-wide v0, v7, v2

    .line 46986
    iget-object v0, v3, LX/0Oj;->a:[B

    iget v1, v3, LX/0Oj;->b:I

    add-int/lit8 v9, v1, 0x1

    iput v9, v3, LX/0Oj;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, v3, LX/0Oj;->a:[B

    iget v9, v3, LX/0Oj;->b:I

    add-int/lit8 p0, v9, 0x1

    iput p0, v3, LX/0Oj;->b:I

    aget-byte v1, v1, v9

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    move v0, v0

    .line 46987
    if-eq v0, v8, :cond_4

    .line 46988
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported media rate."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46989
    :cond_2
    invoke-virtual {v3}, LX/0Oj;->k()J

    move-result-wide v0

    goto :goto_2

    .line 46990
    :cond_3
    invoke-virtual {v3}, LX/0Oj;->m()I

    move-result v0

    int-to-long v0, v0

    goto :goto_3

    .line 46991
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, LX/0Oj;->c(I)V

    .line 46992
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 46993
    :cond_5
    invoke-static {v6, v7}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/0Oj;IIIIJLjava/lang/String;ZLX/0Mg;I)V
    .locals 15

    .prologue
    .line 46907
    add-int/lit8 v4, p2, 0x8

    invoke-virtual {p0, v4}, LX/0Oj;->b(I)V

    .line 46908
    const/4 v4, 0x0

    .line 46909
    if-eqz p8, :cond_5

    .line 46910
    const/16 v4, 0x8

    invoke-virtual {p0, v4}, LX/0Oj;->c(I)V

    .line 46911
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v4

    .line 46912
    const/4 v5, 0x6

    invoke-virtual {p0, v5}, LX/0Oj;->c(I)V

    .line 46913
    :goto_0
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v10

    .line 46914
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v7

    .line 46915
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/0Oj;->c(I)V

    .line 46916
    invoke-virtual {p0}, LX/0Oj;->q()I

    move-result v11

    .line 46917
    if-lez v4, :cond_0

    .line 46918
    const/16 v5, 0x10

    invoke-virtual {p0, v5}, LX/0Oj;->c(I)V

    .line 46919
    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 46920
    const/16 v4, 0x14

    invoke-virtual {p0, v4}, LX/0Oj;->c(I)V

    .line 46921
    :cond_0
    const/4 v4, 0x0

    .line 46922
    sget v5, LX/0Mc;->k:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_6

    .line 46923
    const-string v4, "audio/ac3"

    .line 46924
    :cond_1
    :goto_1
    const/4 v8, 0x0

    .line 46925
    invoke-virtual {p0}, LX/0Oj;->d()I

    move-result v9

    move-object v5, v4

    .line 46926
    :goto_2
    sub-int v4, v9, p2

    move/from16 v0, p3

    if-ge v4, v0, :cond_16

    .line 46927
    invoke-virtual {p0, v9}, LX/0Oj;->b(I)V

    .line 46928
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v12

    .line 46929
    if-lez v12, :cond_d

    const/4 v4, 0x1

    :goto_3
    const-string v6, "childAtomSize should be positive"

    invoke-static {v4, v6}, LX/0Av;->a(ZLjava/lang/Object;)V

    .line 46930
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v6

    .line 46931
    sget v4, LX/0Mc;->i:I

    move/from16 v0, p1

    if-eq v0, v4, :cond_2

    sget v4, LX/0Mc;->X:I

    move/from16 v0, p1

    if-ne v0, v4, :cond_11

    .line 46932
    :cond_2
    const/4 v4, -0x1

    .line 46933
    sget v13, LX/0Mc;->G:I

    if-ne v6, v13, :cond_e

    move v4, v9

    .line 46934
    :cond_3
    :goto_4
    const/4 v13, -0x1

    if-eq v4, v13, :cond_f

    .line 46935
    invoke-static {p0, v4}, LX/0Mi;->d(LX/0Oj;I)Landroid/util/Pair;

    move-result-object v6

    .line 46936
    iget-object v4, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v5, v4

    check-cast v5, Ljava/lang/String;

    .line 46937
    iget-object v4, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object v6, v4

    check-cast v6, [B

    .line 46938
    const-string v4, "audio/mp4a-latm"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 46939
    invoke-static {v6}, LX/0OY;->a([B)Landroid/util/Pair;

    move-result-object v8

    .line 46940
    iget-object v4, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 46941
    iget-object v4, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 46942
    :cond_4
    :goto_5
    add-int/2addr v9, v12

    move-object v8, v6

    .line 46943
    goto :goto_2

    .line 46944
    :cond_5
    const/16 v5, 0x10

    invoke-virtual {p0, v5}, LX/0Oj;->c(I)V

    goto/16 :goto_0

    .line 46945
    :cond_6
    sget v5, LX/0Mc;->m:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_7

    .line 46946
    const-string v4, "audio/eac3"

    goto :goto_1

    .line 46947
    :cond_7
    sget v5, LX/0Mc;->o:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_8

    .line 46948
    const-string v4, "audio/vnd.dts"

    goto :goto_1

    .line 46949
    :cond_8
    sget v5, LX/0Mc;->p:I

    move/from16 v0, p1

    if-eq v0, v5, :cond_9

    sget v5, LX/0Mc;->q:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_a

    .line 46950
    :cond_9
    const-string v4, "audio/vnd.dts.hd"

    goto/16 :goto_1

    .line 46951
    :cond_a
    sget v5, LX/0Mc;->r:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_b

    .line 46952
    const-string v4, "audio/vnd.dts.hd;profile=lbr"

    goto/16 :goto_1

    .line 46953
    :cond_b
    sget v5, LX/0Mc;->ar:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_c

    .line 46954
    const-string v4, "audio/3gpp"

    goto/16 :goto_1

    .line 46955
    :cond_c
    sget v5, LX/0Mc;->as:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_1

    .line 46956
    const-string v4, "audio/amr-wb"

    goto/16 :goto_1

    .line 46957
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 46958
    :cond_e
    if-eqz p8, :cond_3

    sget v13, LX/0Mc;->j:I

    if-ne v6, v13, :cond_3

    .line 46959
    invoke-static {p0, v9, v12}, LX/0Mi;->c(LX/0Oj;II)I

    move-result v4

    goto/16 :goto_4

    .line 46960
    :cond_f
    sget v4, LX/0Mc;->S:I

    if-ne v6, v4, :cond_10

    .line 46961
    move-object/from16 v0, p9

    iget-object v4, v0, LX/0Mg;->a:[LX/0Mp;

    invoke-static {p0, v9, v12}, LX/0Mi;->a(LX/0Oj;II)LX/0Mp;

    move-result-object v6

    aput-object v6, v4, p10

    :cond_10
    move-object v6, v8

    .line 46962
    goto :goto_5

    :cond_11
    sget v4, LX/0Mc;->k:I

    move/from16 v0, p1

    if-ne v0, v4, :cond_13

    sget v4, LX/0Mc;->l:I

    if-ne v6, v4, :cond_13

    .line 46963
    add-int/lit8 v4, v9, 0x8

    invoke-virtual {p0, v4}, LX/0Oj;->b(I)V

    .line 46964
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-wide/from16 v0, p5

    move-object/from16 v2, p7

    invoke-static {p0, v4, v0, v1, v2}, LX/0OW;->a(LX/0Oj;Ljava/lang/String;JLjava/lang/String;)LX/0L4;

    move-result-object v4

    move-object/from16 v0, p9

    iput-object v4, v0, LX/0Mg;->b:LX/0L4;

    .line 46965
    :cond_12
    :goto_6
    return-void

    .line 46966
    :cond_13
    sget v4, LX/0Mc;->m:I

    move/from16 v0, p1

    if-ne v0, v4, :cond_14

    sget v4, LX/0Mc;->n:I

    if-ne v6, v4, :cond_14

    .line 46967
    add-int/lit8 v4, v9, 0x8

    invoke-virtual {p0, v4}, LX/0Oj;->b(I)V

    .line 46968
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-wide/from16 v0, p5

    move-object/from16 v2, p7

    invoke-static {p0, v4, v0, v1, v2}, LX/0OW;->b(LX/0Oj;Ljava/lang/String;JLjava/lang/String;)LX/0L4;

    move-result-object v4

    move-object/from16 v0, p9

    iput-object v4, v0, LX/0Mg;->b:LX/0L4;

    goto :goto_6

    .line 46969
    :cond_14
    sget v4, LX/0Mc;->o:I

    move/from16 v0, p1

    if-eq v0, v4, :cond_15

    sget v4, LX/0Mc;->r:I

    move/from16 v0, p1

    if-eq v0, v4, :cond_15

    sget v4, LX/0Mc;->p:I

    move/from16 v0, p1

    if-eq v0, v4, :cond_15

    sget v4, LX/0Mc;->q:I

    move/from16 v0, p1

    if-ne v0, v4, :cond_18

    :cond_15
    sget v4, LX/0Mc;->s:I

    if-ne v6, v4, :cond_18

    .line 46970
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, -0x1

    const/4 v7, -0x1

    const/4 v12, 0x0

    move-wide/from16 v8, p5

    move-object/from16 v13, p7

    invoke-static/range {v4 .. v13}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v4

    move-object/from16 v0, p9

    iput-object v4, v0, LX/0Mg;->b:LX/0L4;

    goto :goto_6

    .line 46971
    :cond_16
    if-eqz v5, :cond_12

    .line 46972
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, -0x1

    if-nez v8, :cond_17

    const/4 v12, 0x0

    :goto_7
    move-wide/from16 v8, p5

    move-object/from16 v13, p7

    invoke-static/range {v4 .. v13}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v4

    move-object/from16 v0, p9

    iput-object v4, v0, LX/0Mg;->b:LX/0L4;

    goto :goto_6

    :cond_17
    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v12

    goto :goto_7

    :cond_18
    move-object v6, v8

    goto/16 :goto_5
.end method

.method private static a(LX/0Oj;IIIJILX/0Mg;I)V
    .locals 14

    .prologue
    .line 46851
    add-int/lit8 v2, p1, 0x8

    invoke-virtual {p0, v2}, LX/0Oj;->b(I)V

    .line 46852
    const/16 v2, 0x18

    invoke-virtual {p0, v2}, LX/0Oj;->c(I)V

    .line 46853
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v8

    .line 46854
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v9

    .line 46855
    const/4 v5, 0x0

    .line 46856
    const/high16 v12, 0x3f800000    # 1.0f

    .line 46857
    const/16 v2, 0x32

    invoke-virtual {p0, v2}, LX/0Oj;->c(I)V

    .line 46858
    const/4 v10, 0x0

    .line 46859
    invoke-virtual {p0}, LX/0Oj;->d()I

    move-result v2

    .line 46860
    const/4 v3, 0x0

    move v6, v2

    .line 46861
    :goto_0
    sub-int v2, v6, p1

    move/from16 v0, p2

    if-ge v2, v0, :cond_c

    .line 46862
    invoke-virtual {p0, v6}, LX/0Oj;->b(I)V

    .line 46863
    invoke-virtual {p0}, LX/0Oj;->d()I

    move-result v7

    .line 46864
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v11

    .line 46865
    if-nez v11, :cond_0

    invoke-virtual {p0}, LX/0Oj;->d()I

    move-result v2

    sub-int/2addr v2, p1

    move/from16 v0, p2

    if-eq v2, v0, :cond_c

    .line 46866
    :cond_0
    if-lez v11, :cond_2

    const/4 v2, 0x1

    :goto_1
    const-string v4, "childAtomSize should be positive"

    invoke-static {v2, v4}, LX/0Av;->a(ZLjava/lang/Object;)V

    .line 46867
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v2

    .line 46868
    sget v4, LX/0Mc;->E:I

    if-ne v2, v4, :cond_4

    .line 46869
    if-nez v3, :cond_3

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, LX/0Av;->b(Z)V

    .line 46870
    const-string v3, "video/avc"

    .line 46871
    invoke-static {p0, v7}, LX/0Mi;->a(LX/0Oj;I)LX/0Mf;

    move-result-object v2

    .line 46872
    iget-object v10, v2, LX/0Mf;->a:Ljava/util/List;

    .line 46873
    iget v4, v2, LX/0Mf;->b:I

    move-object/from16 v0, p7

    iput v4, v0, LX/0Mg;->c:I

    .line 46874
    if-nez v5, :cond_1

    .line 46875
    iget v12, v2, LX/0Mf;->c:F

    :cond_1
    move v2, v5

    .line 46876
    :goto_3
    add-int v4, v6, v11

    move v6, v4

    move v5, v2

    .line 46877
    goto :goto_0

    .line 46878
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 46879
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 46880
    :cond_4
    sget v4, LX/0Mc;->F:I

    if-ne v2, v4, :cond_6

    .line 46881
    if-nez v3, :cond_5

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, LX/0Av;->b(Z)V

    .line 46882
    const-string v4, "video/hevc"

    .line 46883
    invoke-static {p0, v7}, LX/0Mi;->b(LX/0Oj;I)Landroid/util/Pair;

    move-result-object v7

    .line 46884
    iget-object v2, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v3, v2

    check-cast v3, Ljava/util/List;

    .line 46885
    iget-object v2, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p7

    iput v2, v0, LX/0Mg;->c:I

    move-object v10, v3

    move v2, v5

    move-object v3, v4

    .line 46886
    goto :goto_3

    .line 46887
    :cond_5
    const/4 v2, 0x0

    goto :goto_4

    .line 46888
    :cond_6
    sget v4, LX/0Mc;->g:I

    if-ne v2, v4, :cond_8

    .line 46889
    if-nez v3, :cond_7

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, LX/0Av;->b(Z)V

    .line 46890
    const-string v3, "video/3gpp"

    move v2, v5

    goto :goto_3

    .line 46891
    :cond_7
    const/4 v2, 0x0

    goto :goto_5

    .line 46892
    :cond_8
    sget v4, LX/0Mc;->G:I

    if-ne v2, v4, :cond_a

    .line 46893
    if-nez v3, :cond_9

    const/4 v2, 0x1

    :goto_6
    invoke-static {v2}, LX/0Av;->b(Z)V

    .line 46894
    invoke-static {p0, v7}, LX/0Mi;->d(LX/0Oj;I)Landroid/util/Pair;

    move-result-object v3

    .line 46895
    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 46896
    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    move-object v3, v2

    move v2, v5

    .line 46897
    goto :goto_3

    .line 46898
    :cond_9
    const/4 v2, 0x0

    goto :goto_6

    .line 46899
    :cond_a
    sget v4, LX/0Mc;->S:I

    if-ne v2, v4, :cond_b

    .line 46900
    move-object/from16 v0, p7

    iget-object v2, v0, LX/0Mg;->a:[LX/0Mp;

    invoke-static {p0, v7, v11}, LX/0Mi;->a(LX/0Oj;II)LX/0Mp;

    move-result-object v4

    aput-object v4, v2, p8

    move v2, v5

    goto :goto_3

    .line 46901
    :cond_b
    sget v4, LX/0Mc;->ad:I

    if-ne v2, v4, :cond_e

    .line 46902
    invoke-static {p0, v7}, LX/0Mi;->c(LX/0Oj;I)F

    move-result v12

    .line 46903
    const/4 v2, 0x1

    goto :goto_3

    .line 46904
    :cond_c
    if-nez v3, :cond_d

    .line 46905
    :goto_7
    return-void

    .line 46906
    :cond_d
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, -0x1

    const/4 v5, -0x1

    move-wide/from16 v6, p4

    move/from16 v11, p6

    invoke-static/range {v2 .. v12}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)LX/0L4;

    move-result-object v2

    move-object/from16 v0, p7

    iput-object v2, v0, LX/0Mg;->b:LX/0L4;

    goto :goto_7

    :cond_e
    move v2, v5

    goto/16 :goto_3
.end method

.method private static b(LX/0Oj;I)Landroid/util/Pair;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Oj;",
            "I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    const/4 v1, 0x0

    .line 47263
    add-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x15

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 47264
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    and-int/lit8 v5, v0, 0x3

    .line 47265
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v6

    .line 47266
    iget v0, p0, LX/0Oj;->b:I

    move v7, v0

    .line 47267
    move v3, v1

    move v4, v1

    .line 47268
    :goto_0
    if-ge v3, v6, :cond_1

    .line 47269
    invoke-virtual {p0, v12}, LX/0Oj;->c(I)V

    .line 47270
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v8

    move v0, v1

    move v2, v4

    .line 47271
    :goto_1
    if-ge v0, v8, :cond_0

    .line 47272
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v4

    .line 47273
    add-int/lit8 v9, v4, 0x4

    add-int/2addr v2, v9

    .line 47274
    invoke-virtual {p0, v4}, LX/0Oj;->c(I)V

    .line 47275
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 47276
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v2

    goto :goto_0

    .line 47277
    :cond_1
    invoke-virtual {p0, v7}, LX/0Oj;->b(I)V

    .line 47278
    new-array v7, v4, [B

    move v3, v1

    move v0, v1

    .line 47279
    :goto_2
    if-ge v3, v6, :cond_3

    .line 47280
    invoke-virtual {p0, v12}, LX/0Oj;->c(I)V

    .line 47281
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v8

    move v2, v0

    move v0, v1

    .line 47282
    :goto_3
    if-ge v0, v8, :cond_2

    .line 47283
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v9

    .line 47284
    sget-object v10, LX/0Oh;->a:[B

    sget-object v11, LX/0Oh;->a:[B

    array-length v11, v11

    invoke-static {v10, v1, v7, v2, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47285
    sget-object v10, LX/0Oh;->a:[B

    array-length v10, v10

    add-int/2addr v2, v10

    .line 47286
    iget-object v10, p0, LX/0Oj;->a:[B

    .line 47287
    iget v11, p0, LX/0Oj;->b:I

    move v11, v11

    .line 47288
    invoke-static {v10, v11, v7, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47289
    add-int/2addr v2, v9

    .line 47290
    invoke-virtual {p0, v9}, LX/0Oj;->c(I)V

    .line 47291
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 47292
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    .line 47293
    :cond_3
    if-nez v4, :cond_4

    const/4 v0, 0x0

    .line 47294
    :goto_4
    add-int/lit8 v1, v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 47295
    :cond_4
    invoke-static {v7}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_4
.end method

.method private static c(LX/0Oj;I)F
    .locals 2

    .prologue
    .line 46742
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 46743
    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v0

    .line 46744
    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v1

    .line 46745
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method private static c(LX/0Oj;II)I
    .locals 4

    .prologue
    .line 46746
    iget v0, p0, LX/0Oj;->b:I

    move v1, v0

    .line 46747
    :goto_0
    sub-int v0, v1, p1

    if-ge v0, p2, :cond_2

    .line 46748
    invoke-virtual {p0, v1}, LX/0Oj;->b(I)V

    .line 46749
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v2

    .line 46750
    if-lez v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    const-string v3, "childAtomSize should be positive"

    invoke-static {v0, v3}, LX/0Av;->a(ZLjava/lang/Object;)V

    .line 46751
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    .line 46752
    sget v3, LX/0Mc;->G:I

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 46753
    :goto_2
    return v0

    .line 46754
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 46755
    :cond_1
    add-int/2addr v1, v2

    .line 46756
    goto :goto_0

    .line 46757
    :cond_2
    const/4 v0, -0x1

    goto :goto_2
.end method

.method private static c(LX/0Oj;)J
    .locals 2

    .prologue
    const/16 v0, 0x8

    .line 46758
    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 46759
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v1

    .line 46760
    invoke-static {v1}, LX/0Mc;->a(I)I

    move-result v1

    .line 46761
    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, LX/0Oj;->c(I)V

    .line 46762
    invoke-virtual {p0}, LX/0Oj;->k()J

    move-result-wide v0

    return-wide v0

    .line 46763
    :cond_0
    const/16 v0, 0x10

    goto :goto_0
.end method

.method private static d(LX/0Oj;)LX/0Mh;
    .locals 12

    .prologue
    const/16 v2, 0x10

    const/16 v1, 0x8

    const/4 v3, 0x4

    const/4 v4, 0x0

    const/high16 v11, -0x10000

    .line 46764
    invoke-virtual {p0, v1}, LX/0Oj;->b(I)V

    .line 46765
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    .line 46766
    invoke-static {v0}, LX/0Mc;->a(I)I

    move-result v6

    .line 46767
    if-nez v6, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, LX/0Oj;->c(I)V

    .line 46768
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v7

    .line 46769
    invoke-virtual {p0, v3}, LX/0Oj;->c(I)V

    .line 46770
    const/4 v0, 0x1

    .line 46771
    iget v5, p0, LX/0Oj;->b:I

    move v8, v5

    .line 46772
    if-nez v6, :cond_0

    move v1, v3

    :cond_0
    move v5, v4

    .line 46773
    :goto_1
    if-ge v5, v1, :cond_1

    .line 46774
    iget-object v9, p0, LX/0Oj;->a:[B

    add-int v10, v8, v5

    aget-byte v9, v9, v10

    const/4 v10, -0x1

    if-eq v9, v10, :cond_4

    move v0, v4

    .line 46775
    :cond_1
    if-eqz v0, :cond_5

    .line 46776
    invoke-virtual {p0, v1}, LX/0Oj;->c(I)V

    .line 46777
    const-wide/16 v0, -0x1

    .line 46778
    :cond_2
    :goto_2
    invoke-virtual {p0, v2}, LX/0Oj;->c(I)V

    .line 46779
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v2

    .line 46780
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v5

    .line 46781
    invoke-virtual {p0, v3}, LX/0Oj;->c(I)V

    .line 46782
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v3

    .line 46783
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v6

    .line 46784
    if-nez v2, :cond_7

    const/high16 v8, 0x10000

    if-ne v5, v8, :cond_7

    if-ne v3, v11, :cond_7

    if-nez v6, :cond_7

    .line 46785
    const/16 v2, 0x5a

    .line 46786
    :goto_3
    new-instance v3, LX/0Mh;

    invoke-direct {v3, v7, v0, v1, v2}, LX/0Mh;-><init>(IJI)V

    return-object v3

    :cond_3
    move v0, v2

    .line 46787
    goto :goto_0

    .line 46788
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 46789
    :cond_5
    if-nez v6, :cond_6

    invoke-virtual {p0}, LX/0Oj;->k()J

    move-result-wide v0

    .line 46790
    :goto_4
    const-wide/16 v8, 0x0

    cmp-long v5, v0, v8

    if-nez v5, :cond_2

    .line 46791
    const-wide/16 v0, -0x1

    goto :goto_2

    .line 46792
    :cond_6
    invoke-virtual {p0}, LX/0Oj;->u()J

    move-result-wide v0

    goto :goto_4

    .line 46793
    :cond_7
    if-nez v2, :cond_8

    if-ne v5, v11, :cond_8

    const/high16 v8, 0x10000

    if-ne v3, v8, :cond_8

    if-nez v6, :cond_8

    .line 46794
    const/16 v2, 0x10e

    goto :goto_3

    .line 46795
    :cond_8
    if-ne v2, v11, :cond_9

    if-nez v5, :cond_9

    if-nez v3, :cond_9

    if-ne v6, v11, :cond_9

    .line 46796
    const/16 v2, 0xb4

    goto :goto_3

    :cond_9
    move v2, v4

    .line 46797
    goto :goto_3
.end method

.method private static d(LX/0Oj;I)Landroid/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Oj;",
            "I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 46798
    add-int/lit8 v1, p1, 0x8

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {p0, v1}, LX/0Oj;->b(I)V

    .line 46799
    invoke-virtual {p0, v3}, LX/0Oj;->c(I)V

    .line 46800
    invoke-static {p0}, LX/0Mi;->g(LX/0Oj;)I

    .line 46801
    invoke-virtual {p0, v4}, LX/0Oj;->c(I)V

    .line 46802
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v1

    .line 46803
    and-int/lit16 v2, v1, 0x80

    if-eqz v2, :cond_0

    .line 46804
    invoke-virtual {p0, v4}, LX/0Oj;->c(I)V

    .line 46805
    :cond_0
    and-int/lit8 v2, v1, 0x40

    if-eqz v2, :cond_1

    .line 46806
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v2

    invoke-virtual {p0, v2}, LX/0Oj;->c(I)V

    .line 46807
    :cond_1
    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_2

    .line 46808
    invoke-virtual {p0, v4}, LX/0Oj;->c(I)V

    .line 46809
    :cond_2
    invoke-virtual {p0, v3}, LX/0Oj;->c(I)V

    .line 46810
    invoke-static {p0}, LX/0Mi;->g(LX/0Oj;)I

    .line 46811
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v1

    .line 46812
    sparse-switch v1, :sswitch_data_0

    .line 46813
    :goto_0
    const/16 v1, 0xc

    invoke-virtual {p0, v1}, LX/0Oj;->c(I)V

    .line 46814
    invoke-virtual {p0, v3}, LX/0Oj;->c(I)V

    .line 46815
    invoke-static {p0}, LX/0Mi;->g(LX/0Oj;)I

    move-result v1

    .line 46816
    new-array v2, v1, [B

    .line 46817
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v1}, LX/0Oj;->a([BII)V

    .line 46818
    invoke-static {v0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_1
    return-object v0

    .line 46819
    :sswitch_0
    const-string v1, "audio/mpeg"

    .line 46820
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 46821
    :sswitch_1
    const-string v0, "video/mp4v-es"

    goto :goto_0

    .line 46822
    :sswitch_2
    const-string v0, "video/avc"

    goto :goto_0

    .line 46823
    :sswitch_3
    const-string v0, "video/hevc"

    goto :goto_0

    .line 46824
    :sswitch_4
    const-string v0, "audio/mp4a-latm"

    goto :goto_0

    .line 46825
    :sswitch_5
    const-string v0, "audio/ac3"

    goto :goto_0

    .line 46826
    :sswitch_6
    const-string v0, "audio/eac3"

    goto :goto_0

    .line 46827
    :sswitch_7
    const-string v1, "audio/vnd.dts"

    .line 46828
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 46829
    :sswitch_8
    const-string v1, "audio/vnd.dts.hd"

    .line 46830
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x21 -> :sswitch_2
        0x23 -> :sswitch_3
        0x40 -> :sswitch_4
        0x66 -> :sswitch_4
        0x67 -> :sswitch_4
        0x68 -> :sswitch_4
        0x6b -> :sswitch_0
        0xa5 -> :sswitch_5
        0xa6 -> :sswitch_6
        0xa9 -> :sswitch_7
        0xaa -> :sswitch_8
        0xab -> :sswitch_8
        0xac -> :sswitch_7
    .end sparse-switch
.end method

.method private static e(LX/0Oj;)I
    .locals 1

    .prologue
    .line 46831
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 46832
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    return v0
.end method

.method private static f(LX/0Oj;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Oj;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x8

    .line 46833
    invoke-virtual {p0, v1}, LX/0Oj;->b(I)V

    .line 46834
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    .line 46835
    invoke-static {v0}, LX/0Mc;->a(I)I

    move-result v2

    .line 46836
    if-nez v2, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, LX/0Oj;->c(I)V

    .line 46837
    invoke-virtual {p0}, LX/0Oj;->k()J

    move-result-wide v4

    .line 46838
    if-nez v2, :cond_0

    const/4 v1, 0x4

    :cond_0
    invoke-virtual {p0, v1}, LX/0Oj;->c(I)V

    .line 46839
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v0

    .line 46840
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    shr-int/lit8 v2, v0, 0xa

    and-int/lit8 v2, v2, 0x1f

    add-int/lit8 v2, v2, 0x60

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    shr-int/lit8 v2, v0, 0x5

    and-int/lit8 v2, v2, 0x1f

    add-int/lit8 v2, v2, 0x60

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    and-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x60

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 46841
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 46842
    :cond_1
    const/16 v0, 0x10

    goto :goto_0
.end method

.method private static g(LX/0Oj;)I
    .locals 3

    .prologue
    .line 46843
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v1

    .line 46844
    and-int/lit8 v0, v1, 0x7f

    .line 46845
    :goto_0
    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_0

    .line 46846
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v1

    .line 46847
    shl-int/lit8 v0, v0, 0x7

    and-int/lit8 v2, v1, 0x7f

    or-int/2addr v0, v2

    goto :goto_0

    .line 46848
    :cond_0
    return v0
.end method
