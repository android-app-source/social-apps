.class public LX/0AY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ax;


# instance fields
.field public final a:J

.field public final b:J

.field public final c:J

.field public final d:J

.field public final e:Z

.field public final f:J

.field public final g:J

.field public final h:LX/0Ly;

.field public final i:Ljava/lang/String;

.field public final j:J

.field private final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Am;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JJJJZJJLX/0Ly;Ljava/lang/String;JLjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJJZJJ",
            "LX/0Ly;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List",
            "<",
            "LX/0Am;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24469
    iput-wide p1, p0, LX/0AY;->a:J

    .line 24470
    iput-wide p3, p0, LX/0AY;->b:J

    .line 24471
    iput-wide p5, p0, LX/0AY;->c:J

    .line 24472
    iput-wide p7, p0, LX/0AY;->d:J

    .line 24473
    iput-boolean p9, p0, LX/0AY;->e:Z

    .line 24474
    iput-wide p10, p0, LX/0AY;->f:J

    .line 24475
    iput-wide p12, p0, LX/0AY;->g:J

    .line 24476
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0AY;->h:LX/0Ly;

    .line 24477
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0AY;->i:Ljava/lang/String;

    .line 24478
    move-wide/from16 v0, p16

    iput-wide v0, p0, LX/0AY;->j:J

    .line 24479
    if-nez p18, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p18

    :cond_0
    move-object/from16 v0, p18

    iput-object v0, p0, LX/0AY;->k:Ljava/util/List;

    .line 24480
    return-void
.end method


# virtual methods
.method public final a(I)LX/0Am;
    .locals 1

    .prologue
    .line 24467
    iget-object v0, p0, LX/0AY;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Am;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24466
    iget-object v0, p0, LX/0AY;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 24464
    iget-object v0, p0, LX/0AY;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b(I)J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 24465
    iget-object v2, p0, LX/0AY;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_1

    iget-wide v2, p0, LX/0AY;->c:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v2, p0, LX/0AY;->c:J

    iget-object v0, p0, LX/0AY;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Am;

    iget-wide v0, v0, LX/0Am;->b:J

    sub-long v0, v2, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/0AY;->k:Ljava/util/List;

    add-int/lit8 v1, p1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Am;

    iget-wide v2, v0, LX/0Am;->b:J

    iget-object v0, p0, LX/0AY;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Am;

    iget-wide v0, v0, LX/0Am;->b:J

    sub-long v0, v2, v0

    goto :goto_0
.end method
