.class public LX/093;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Z

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field public e:LX/03R;

.field public f:J

.field public g:J

.field public h:J

.field public i:J

.field public j:Ljava/lang/String;

.field public k:I

.field public l:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21948
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/093;->a:Z

    .line 21949
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/093;->b:Ljava/util/Set;

    .line 21950
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/093;->e:LX/03R;

    .line 21951
    return-void
.end method


# virtual methods
.method public final a(JJJJ)V
    .locals 1

    .prologue
    .line 21942
    iput-wide p1, p0, LX/093;->f:J

    .line 21943
    iput-wide p3, p0, LX/093;->g:J

    .line 21944
    iput-wide p5, p0, LX/093;->h:J

    .line 21945
    iput-wide p7, p0, LX/093;->i:J

    .line 21946
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 21965
    iput-object p1, p0, LX/093;->j:Ljava/lang/String;

    .line 21966
    iput p2, p0, LX/093;->k:I

    .line 21967
    return-void
.end method

.method public final a(Ljava/util/Set;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21968
    if-eqz p1, :cond_0

    .line 21969
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/046;

    .line 21970
    iget-object v2, p0, LX/093;->b:Ljava/util/Set;

    iget-object v0, v0, LX/046;->value:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 21971
    :cond_0
    iput-object p2, p0, LX/093;->c:Ljava/lang/String;

    .line 21972
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 21961
    iput-boolean p1, p0, LX/093;->l:Z

    .line 21962
    if-eqz p1, :cond_0

    .line 21963
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/093;->a:Z

    .line 21964
    :cond_0
    return-void
.end method

.method public final c()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21952
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 21953
    const-string v1, "initial_event"

    iget-boolean v2, p0, LX/093;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21954
    const-string v1, "autoplay_failure_reasons"

    iget-object v2, p0, LX/093;->b:Ljava/util/Set;

    .line 21955
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v2}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    move-object v2, v3

    .line 21956
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21957
    const-string v1, "autoplay_setting"

    iget-object v2, p0, LX/093;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21958
    iget-object v1, p0, LX/093;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 21959
    const-string v1, "projection"

    iget-object v2, p0, LX/093;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21960
    :cond_0
    return-object v0
.end method
