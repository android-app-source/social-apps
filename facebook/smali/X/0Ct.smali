.class public final LX/0Ct;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/webkit/DownloadListener;


# instance fields
.field public final synthetic a:LX/0D5;

.field public final synthetic b:Lcom/facebook/browser/lite/BrowserLiteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;)V
    .locals 0

    .prologue
    .line 28088
    iput-object p1, p0, LX/0Ct;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iput-object p2, p0, LX/0Ct;->a:LX/0D5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 28089
    iget-object v0, p0, LX/0Ct;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {v0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d(Lcom/facebook/browser/lite/BrowserLiteFragment;Ljava/lang/String;)Z

    .line 28090
    iget-object v0, p0, LX/0Ct;->a:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28091
    iget-object v0, p0, LX/0Ct;->a:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->goBack()V

    .line 28092
    :goto_0
    return-void

    .line 28093
    :cond_0
    iget-object v0, p0, LX/0Ct;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->e()V

    goto :goto_0
.end method
