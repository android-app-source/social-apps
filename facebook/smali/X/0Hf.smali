.class public final LX/0Hf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/Runnable;

.field public final b:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 37941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37942
    iput-object p1, p0, LX/0Hf;->a:Ljava/lang/Runnable;

    .line 37943
    iput-object p2, p0, LX/0Hf;->b:Ljava/util/concurrent/Executor;

    .line 37944
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 37945
    :try_start_0
    iget-object v0, p0, LX/0Hf;->b:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/0Hf;->a:Ljava/lang/Runnable;

    const v2, 0x1600168c

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37946
    :goto_0
    return-void

    .line 37947
    :catch_0
    move-exception v0

    .line 37948
    sget-object v1, LX/0B4;->a:Ljava/lang/String;

    const-string v2, "RuntimeException while executing runnable=%s with executor=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/0Hf;->a:Ljava/lang/Runnable;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, LX/0Hf;->b:Ljava/util/concurrent/Executor;

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/05D;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
