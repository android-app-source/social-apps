.class public final LX/0Ln;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:J

.field public final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0Lo;",
            ">;"
        }
    .end annotation
.end field

.field private final d:[I

.field public e:LX/0Lz;

.field public f:Z

.field public g:Z

.field public h:J

.field private i:J


# direct methods
.method public constructor <init>(ILX/0AY;ILX/0Lk;ZZ)V
    .locals 10

    .prologue
    .line 44270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44271
    iput p1, p0, LX/0Ln;->a:I

    .line 44272
    invoke-virtual {p2, p3}, LX/0AY;->a(I)LX/0Am;

    move-result-object v1

    .line 44273
    invoke-static {p2, p3}, LX/0Ln;->a(LX/0AY;I)J

    move-result-wide v4

    .line 44274
    iget-object v0, v1, LX/0Am;->c:Ljava/util/List;

    iget v2, p4, LX/0Lk;->d:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    .line 44275
    iget-object v9, v0, LX/0Ak;->c:Ljava/util/List;

    .line 44276
    iget-wide v2, v1, LX/0Am;->b:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v2, v6

    iput-wide v2, p0, LX/0Ln;->b:J

    .line 44277
    invoke-static {v0}, LX/0Ln;->a(LX/0Ak;)LX/0Lz;

    move-result-object v0

    iput-object v0, p0, LX/0Ln;->e:LX/0Lz;

    .line 44278
    invoke-virtual {p4}, LX/0Lk;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 44279
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget-object v2, p4, LX/0Lk;->e:LX/0AR;

    iget-object v2, v2, LX/0AR;->a:Ljava/lang/String;

    invoke-static {v9, v2}, LX/0Ln;->a(Ljava/util/List;Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, LX/0Ln;->d:[I

    .line 44280
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0Ln;->c:Ljava/util/HashMap;

    .line 44281
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/0Ln;->d:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 44282
    iget-object v1, p0, LX/0Ln;->d:[I

    aget v1, v1, v0

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0Ah;

    .line 44283
    new-instance v1, LX/0Lo;

    iget-wide v2, p0, LX/0Ln;->b:J

    move v7, p5

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, LX/0Lo;-><init>(JJLX/0Ah;ZZ)V

    .line 44284
    iget-object v2, p0, LX/0Ln;->c:Ljava/util/HashMap;

    iget-object v3, v6, LX/0Ah;->c:LX/0AR;

    iget-object v3, v3, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44285
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44286
    :cond_1
    iget-object v0, p4, LX/0Lk;->f:[LX/0AR;

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/0Ln;->d:[I

    .line 44287
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p4, LX/0Lk;->f:[LX/0AR;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 44288
    iget-object v1, p0, LX/0Ln;->d:[I

    iget-object v2, p4, LX/0Lk;->f:[LX/0AR;

    aget-object v2, v2, v0

    iget-object v2, v2, LX/0AR;->a:Ljava/lang/String;

    invoke-static {v9, v2}, LX/0Ln;->a(Ljava/util/List;Ljava/lang/String;)I

    move-result v2

    aput v2, v1, v0

    .line 44289
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 44290
    :cond_2
    iget-object v0, p0, LX/0Ln;->d:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    invoke-direct {p0, v4, v5, v0}, LX/0Ln;->a(JLX/0Ah;)V

    .line 44291
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0Ah;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 44292
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 44293
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 44294
    iget-object v0, v0, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44295
    return v1

    .line 44296
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 44297
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Missing format id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(LX/0AY;I)J
    .locals 5

    .prologue
    const-wide/16 v0, -0x1

    .line 44298
    invoke-virtual {p0, p1}, LX/0AY;->b(I)J

    move-result-wide v2

    .line 44299
    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    .line 44300
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, v2

    goto :goto_0
.end method

.method private static a(LX/0Ak;)LX/0Lz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 44244
    iget-object v0, p0, LX/0Ak;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44245
    :cond_0
    return-object v1

    .line 44246
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/0Ak;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 44247
    iget-object v0, p0, LX/0Ak;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Lu;

    .line 44248
    iget-object v3, v0, LX/0Lu;->b:Ljava/util/UUID;

    if-eqz v3, :cond_3

    iget-object v3, v0, LX/0Lu;->c:LX/0M1;

    if-eqz v3, :cond_3

    .line 44249
    if-nez v1, :cond_2

    .line 44250
    new-instance v1, LX/0M0;

    invoke-direct {v1}, LX/0M0;-><init>()V

    .line 44251
    :cond_2
    iget-object v3, v0, LX/0Lu;->b:Ljava/util/UUID;

    iget-object v0, v0, LX/0Lu;->c:LX/0M1;

    invoke-virtual {v1, v3, v0}, LX/0M0;->a(Ljava/util/UUID;LX/0M1;)V

    .line 44252
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public static synthetic a(LX/0Ln;)LX/0Lz;
    .locals 1

    .prologue
    .line 44254
    iget-object v0, p0, LX/0Ln;->e:LX/0Lz;

    return-object v0
.end method

.method private a(JLX/0Ah;)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 44255
    invoke-virtual {p3}, LX/0Ah;->f()LX/0BX;

    move-result-object v2

    .line 44256
    if-eqz v2, :cond_2

    .line 44257
    invoke-interface {v2}, LX/0BX;->a()I

    move-result v3

    .line 44258
    invoke-interface {v2, p1, p2}, LX/0BX;->a(J)I

    move-result v4

    .line 44259
    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    :goto_0
    iput-boolean v0, p0, LX/0Ln;->f:Z

    .line 44260
    invoke-interface {v2}, LX/0BX;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/0Ln;->g:Z

    .line 44261
    iget-wide v0, p0, LX/0Ln;->b:J

    invoke-interface {v2, v3}, LX/0BX;->a(I)J

    move-result-wide v6

    add-long/2addr v0, v6

    iput-wide v0, p0, LX/0Ln;->h:J

    .line 44262
    iget-boolean v0, p0, LX/0Ln;->f:Z

    if-nez v0, :cond_0

    .line 44263
    iget-wide v0, p0, LX/0Ln;->b:J

    invoke-interface {v2, v4}, LX/0BX;->a(I)J

    move-result-wide v6

    add-long/2addr v0, v6

    invoke-interface {v2, v4, p1, p2}, LX/0BX;->a(IJ)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0Ln;->i:J

    .line 44264
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 44265
    goto :goto_0

    .line 44266
    :cond_2
    iput-boolean v1, p0, LX/0Ln;->f:Z

    .line 44267
    iput-boolean v0, p0, LX/0Ln;->g:Z

    .line 44268
    iget-wide v0, p0, LX/0Ln;->b:J

    iput-wide v0, p0, LX/0Ln;->h:J

    .line 44269
    iget-wide v0, p0, LX/0Ln;->b:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/0Ln;->i:J

    goto :goto_1
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 44253
    iget-wide v0, p0, LX/0Ln;->h:J

    return-wide v0
.end method

.method public final a(LX/0AY;ILX/0Lk;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 44235
    invoke-virtual {p1, p2}, LX/0AY;->a(I)LX/0Am;

    move-result-object v0

    .line 44236
    invoke-static {p1, p2}, LX/0Ln;->a(LX/0AY;I)J

    move-result-wide v4

    .line 44237
    iget-object v0, v0, LX/0Am;->c:Ljava/util/List;

    iget v1, p3, LX/0Lk;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    iget-object v6, v0, LX/0Ak;->c:Ljava/util/List;

    move v2, v3

    .line 44238
    :goto_0
    iget-object v0, p0, LX/0Ln;->d:[I

    array-length v0, v0

    if-ge v2, v0, :cond_0

    .line 44239
    iget-object v0, p0, LX/0Ln;->d:[I

    aget v0, v0, v2

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 44240
    iget-object v1, p0, LX/0Ln;->c:Ljava/util/HashMap;

    iget-object v7, v0, LX/0Ah;->c:LX/0AR;

    iget-object v7, v7, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Lo;

    invoke-virtual {v1, v4, v5, v0}, LX/0Lo;->a(JLX/0Ah;)V

    .line 44241
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 44242
    :cond_0
    iget-object v0, p0, LX/0Ln;->d:[I

    aget v0, v0, v3

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    invoke-direct {p0, v4, v5, v0}, LX/0Ln;->a(JLX/0Ah;)V

    .line 44243
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 44231
    iget-boolean v0, p0, LX/0Ln;->f:Z

    move v0, v0

    .line 44232
    if-eqz v0, :cond_0

    .line 44233
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Period has unbounded index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44234
    :cond_0
    iget-wide v0, p0, LX/0Ln;->i:J

    return-wide v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 44230
    iget-boolean v0, p0, LX/0Ln;->f:Z

    return v0
.end method
