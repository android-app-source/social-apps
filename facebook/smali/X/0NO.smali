.class public final LX/0NO;
.super LX/0NL;
.source ""


# instance fields
.field public final synthetic a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

.field private final b:LX/0Oi;

.field private final c:LX/0Oj;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/extractor/ts/TsExtractor;)V
    .locals 2

    .prologue
    .line 50103
    iput-object p1, p0, LX/0NO;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    invoke-direct {p0}, LX/0NL;-><init>()V

    .line 50104
    new-instance v0, LX/0Oi;

    const/4 v1, 0x5

    new-array v1, v1, [B

    invoke-direct {v0, v1}, LX/0Oi;-><init>([B)V

    iput-object v0, p0, LX/0NO;->b:LX/0Oi;

    .line 50105
    new-instance v0, LX/0Oj;

    invoke-direct {v0}, LX/0Oj;-><init>()V

    iput-object v0, p0, LX/0NO;->c:LX/0Oj;

    .line 50106
    return-void
.end method

.method private static a(LX/0Oj;I)I
    .locals 8

    .prologue
    const/16 v2, 0x87

    const/16 v1, 0x81

    .line 50107
    const/4 v0, -0x1

    .line 50108
    iget v3, p0, LX/0Oj;->b:I

    move v3, v3

    .line 50109
    add-int/2addr v3, p1

    .line 50110
    :goto_0
    iget v4, p0, LX/0Oj;->b:I

    move v4, v4

    .line 50111
    if-ge v4, v3, :cond_0

    .line 50112
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v4

    .line 50113
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v5

    .line 50114
    const/4 v6, 0x5

    if-ne v4, v6, :cond_3

    .line 50115
    invoke-virtual {p0}, LX/0Oj;->k()J

    move-result-wide v4

    .line 50116
    sget-wide v6, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->d:J

    cmp-long v6, v4, v6

    if-nez v6, :cond_1

    move v0, v1

    .line 50117
    :cond_0
    :goto_1
    invoke-virtual {p0, v3}, LX/0Oj;->b(I)V

    .line 50118
    return v0

    .line 50119
    :cond_1
    sget-wide v6, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->e:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_2

    move v0, v2

    .line 50120
    goto :goto_1

    .line 50121
    :cond_2
    sget-wide v6, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->f:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_0

    .line 50122
    const/16 v0, 0x24

    goto :goto_1

    .line 50123
    :cond_3
    const/16 v6, 0x6a

    if-ne v4, v6, :cond_5

    move v0, v1

    .line 50124
    :cond_4
    :goto_2
    invoke-virtual {p0, v5}, LX/0Oj;->c(I)V

    goto :goto_0

    .line 50125
    :cond_5
    const/16 v6, 0x7a

    if-ne v4, v6, :cond_6

    move v0, v2

    .line 50126
    goto :goto_2

    .line 50127
    :cond_6
    const/16 v6, 0x7b

    if-ne v4, v6, :cond_4

    .line 50128
    const/16 v0, 0x8a

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 50129
    return-void
.end method

.method public final a(LX/0Oj;ZLX/0LU;)V
    .locals 8

    .prologue
    .line 50130
    if-eqz p2, :cond_0

    .line 50131
    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v0

    .line 50132
    invoke-virtual {p1, v0}, LX/0Oj;->c(I)V

    .line 50133
    iget-object v0, p0, LX/0NO;->b:LX/0Oi;

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, LX/0Oj;->a(LX/0Oi;I)V

    .line 50134
    iget-object v0, p0, LX/0NO;->b:LX/0Oi;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, LX/0Oi;->b(I)V

    .line 50135
    iget-object v0, p0, LX/0NO;->b:LX/0Oi;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, LX/0Oi;->c(I)I

    move-result v0

    iput v0, p0, LX/0NO;->d:I

    .line 50136
    iget-object v0, p0, LX/0NO;->c:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->e()I

    move-result v0

    iget v1, p0, LX/0NO;->d:I

    if-ge v0, v1, :cond_1

    .line 50137
    iget-object v0, p0, LX/0NO;->c:LX/0Oj;

    iget v1, p0, LX/0NO;->d:I

    new-array v1, v1, [B

    iget v2, p0, LX/0NO;->d:I

    invoke-virtual {v0, v1, v2}, LX/0Oj;->a([BI)V

    .line 50138
    :cond_0
    :goto_0
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    iget v1, p0, LX/0NO;->d:I

    iget v2, p0, LX/0NO;->e:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 50139
    iget-object v1, p0, LX/0NO;->c:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    iget v2, p0, LX/0NO;->e:I

    invoke-virtual {p1, v1, v2, v0}, LX/0Oj;->a([BII)V

    .line 50140
    iget v1, p0, LX/0NO;->e:I

    add-int/2addr v0, v1

    iput v0, p0, LX/0NO;->e:I

    .line 50141
    iget v0, p0, LX/0NO;->e:I

    iget v1, p0, LX/0NO;->d:I

    if-ge v0, v1, :cond_2

    .line 50142
    :goto_1
    return-void

    .line 50143
    :cond_1
    iget-object v0, p0, LX/0NO;->c:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->a()V

    .line 50144
    iget-object v0, p0, LX/0NO;->c:LX/0Oj;

    iget v1, p0, LX/0NO;->d:I

    invoke-virtual {v0, v1}, LX/0Oj;->a(I)V

    goto :goto_0

    .line 50145
    :cond_2
    iget-object v0, p0, LX/0NO;->c:LX/0Oj;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, LX/0Oj;->c(I)V

    .line 50146
    iget-object v0, p0, LX/0NO;->c:LX/0Oj;

    iget-object v1, p0, LX/0NO;->b:LX/0Oi;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, LX/0Oj;->a(LX/0Oi;I)V

    .line 50147
    iget-object v0, p0, LX/0NO;->b:LX/0Oi;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LX/0Oi;->b(I)V

    .line 50148
    iget-object v0, p0, LX/0NO;->b:LX/0Oi;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, LX/0Oi;->c(I)I

    move-result v0

    .line 50149
    iget-object v1, p0, LX/0NO;->c:LX/0Oj;

    invoke-virtual {v1, v0}, LX/0Oj;->c(I)V

    .line 50150
    iget-object v1, p0, LX/0NO;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    iget-object v1, v1, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->c:LX/0NF;

    if-nez v1, :cond_3

    .line 50151
    iget-object v1, p0, LX/0NO;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    new-instance v2, LX/0NF;

    const/16 v3, 0x15

    invoke-interface {p3, v3}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0NF;-><init>(LX/0LS;)V

    iput-object v2, v1, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->c:LX/0NF;

    .line 50152
    :cond_3
    iget v1, p0, LX/0NO;->d:I

    add-int/lit8 v1, v1, -0x9

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, -0x4

    move v1, v0

    .line 50153
    :goto_2
    if-lez v1, :cond_a

    .line 50154
    iget-object v0, p0, LX/0NO;->c:LX/0Oj;

    iget-object v2, p0, LX/0NO;->b:LX/0Oi;

    const/4 v3, 0x5

    invoke-virtual {v0, v2, v3}, LX/0Oj;->a(LX/0Oi;I)V

    .line 50155
    iget-object v0, p0, LX/0NO;->b:LX/0Oi;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, LX/0Oi;->c(I)I

    move-result v0

    .line 50156
    iget-object v2, p0, LX/0NO;->b:LX/0Oi;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, LX/0Oi;->b(I)V

    .line 50157
    iget-object v2, p0, LX/0NO;->b:LX/0Oi;

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, LX/0Oi;->c(I)I

    move-result v5

    .line 50158
    iget-object v2, p0, LX/0NO;->b:LX/0Oi;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, LX/0Oi;->b(I)V

    .line 50159
    iget-object v2, p0, LX/0NO;->b:LX/0Oi;

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, LX/0Oi;->c(I)I

    move-result v2

    .line 50160
    const/4 v3, 0x6

    if-ne v0, v3, :cond_5

    .line 50161
    iget-object v0, p0, LX/0NO;->c:LX/0Oj;

    invoke-static {v0, v2}, LX/0NO;->a(LX/0Oj;I)I

    move-result v0

    .line 50162
    :goto_3
    add-int/lit8 v2, v2, 0x5

    sub-int v4, v1, v2

    .line 50163
    iget-object v1, p0, LX/0NO;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    iget-object v1, v1, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-nez v1, :cond_b

    .line 50164
    sparse-switch v0, :sswitch_data_0

    .line 50165
    const/4 v1, 0x0

    .line 50166
    :goto_4
    if-eqz v1, :cond_4

    .line 50167
    iget-object v2, p0, LX/0NO;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    iget-object v2, v2, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->b:Landroid/util/SparseBooleanArray;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 50168
    iget-object v0, p0, LX/0NO;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    iget-object v0, v0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->a:Landroid/util/SparseArray;

    new-instance v2, LX/0NN;

    iget-object v3, p0, LX/0NO;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    iget-object v3, v3, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->g:LX/0NJ;

    invoke-direct {v2, v1, v3}, LX/0NN;-><init>(LX/0N4;LX/0NJ;)V

    invoke-virtual {v0, v5, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_4
    move v1, v4

    .line 50169
    goto :goto_2

    .line 50170
    :cond_5
    iget-object v3, p0, LX/0NO;->c:LX/0Oj;

    invoke-virtual {v3, v2}, LX/0Oj;->c(I)V

    goto :goto_3

    .line 50171
    :sswitch_0
    new-instance v1, LX/0NG;

    const/4 v2, 0x3

    invoke-interface {p3, v2}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0NG;-><init>(LX/0LS;)V

    goto :goto_4

    .line 50172
    :sswitch_1
    new-instance v1, LX/0NG;

    const/4 v2, 0x4

    invoke-interface {p3, v2}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0NG;-><init>(LX/0LS;)V

    goto :goto_4

    .line 50173
    :sswitch_2
    iget-object v1, p0, LX/0NO;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    iget v1, v1, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->h:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_6

    const/4 v1, 0x0

    goto :goto_4

    :cond_6
    new-instance v1, LX/0N6;

    const/16 v2, 0xf

    invoke-interface {p3, v2}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v2

    new-instance v3, LX/0MD;

    invoke-direct {v3}, LX/0MD;-><init>()V

    invoke-direct {v1, v2, v3}, LX/0N6;-><init>(LX/0LS;LX/0LS;)V

    goto :goto_4

    .line 50174
    :sswitch_3
    new-instance v1, LX/0N5;

    const/16 v2, 0x81

    invoke-interface {p3, v2}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LX/0N5;-><init>(LX/0LS;Z)V

    goto :goto_4

    .line 50175
    :sswitch_4
    new-instance v1, LX/0N5;

    const/16 v2, 0x87

    invoke-interface {p3, v2}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, LX/0N5;-><init>(LX/0LS;Z)V

    goto :goto_4

    .line 50176
    :sswitch_5
    new-instance v1, LX/0N7;

    const/16 v2, 0x8a

    invoke-interface {p3, v2}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0N7;-><init>(LX/0LS;)V

    goto :goto_4

    .line 50177
    :sswitch_6
    new-instance v1, LX/0N9;

    const/4 v2, 0x2

    invoke-interface {p3, v2}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0N9;-><init>(LX/0LS;)V

    goto/16 :goto_4

    .line 50178
    :sswitch_7
    iget-object v1, p0, LX/0NO;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    iget v1, v1, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->h:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_7
    new-instance v3, LX/0NC;

    const/16 v1, 0x1b

    invoke-interface {p3, v1}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v6

    new-instance v7, LX/0NK;

    const/16 v1, 0x100

    invoke-interface {p3, v1}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v1

    invoke-direct {v7, v1}, LX/0NK;-><init>(LX/0LS;)V

    iget-object v1, p0, LX/0NO;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    iget v1, v1, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->h:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_5
    iget-object v2, p0, LX/0NO;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    iget v2, v2, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->h:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_6
    invoke-direct {v3, v6, v7, v1, v2}, LX/0NC;-><init>(LX/0LS;LX/0NK;ZZ)V

    move-object v1, v3

    goto/16 :goto_4

    :cond_8
    const/4 v1, 0x0

    goto :goto_5

    :cond_9
    const/4 v2, 0x0

    goto :goto_6

    .line 50179
    :sswitch_8
    new-instance v1, LX/0NE;

    const/16 v2, 0x24

    invoke-interface {p3, v2}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v2

    new-instance v3, LX/0NK;

    const/16 v6, 0x100

    invoke-interface {p3, v6}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v6

    invoke-direct {v3, v6}, LX/0NK;-><init>(LX/0LS;)V

    invoke-direct {v1, v2, v3}, LX/0NE;-><init>(LX/0LS;LX/0NK;)V

    goto/16 :goto_4

    .line 50180
    :sswitch_9
    iget-object v1, p0, LX/0NO;->a:Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    iget-object v1, v1, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->c:LX/0NF;

    goto/16 :goto_4

    .line 50181
    :cond_a
    invoke-interface {p3}, LX/0LU;->a()V

    goto/16 :goto_1

    :cond_b
    move v1, v4

    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_6
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0xf -> :sswitch_2
        0x15 -> :sswitch_9
        0x1b -> :sswitch_7
        0x24 -> :sswitch_8
        0x81 -> :sswitch_3
        0x82 -> :sswitch_5
        0x87 -> :sswitch_4
        0x8a -> :sswitch_5
    .end sparse-switch
.end method
