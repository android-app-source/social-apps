.class public final LX/0GD;
.super LX/0GC;
.source ""


# instance fields
.field public final b:LX/0GE;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Gk;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

.field private f:Landroid/os/Handler;

.field private g:Landroid/net/Uri;

.field private h:I

.field private i:J

.field private j:I


# direct methods
.method public constructor <init>(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;LX/0GE;Landroid/os/Handler;Landroid/net/Uri;Ljava/util/Map;LX/0Gk;II)V
    .locals 0
    .param p6    # LX/0Gk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;",
            "LX/0GE;",
            "Landroid/os/Handler;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Gk;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 34405
    invoke-direct {p0, p1}, LX/0GC;-><init>(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)V

    .line 34406
    iput-object p2, p0, LX/0GD;->b:LX/0GE;

    .line 34407
    iput-object p5, p0, LX/0GD;->c:Ljava/util/Map;

    .line 34408
    iput-object p1, p0, LX/0GD;->e:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    .line 34409
    iput-object p3, p0, LX/0GD;->f:Landroid/os/Handler;

    .line 34410
    iput-object p4, p0, LX/0GD;->g:Landroid/net/Uri;

    .line 34411
    iput-object p6, p0, LX/0GD;->d:LX/0Gk;

    .line 34412
    iput p7, p0, LX/0GD;->h:I

    .line 34413
    iput p8, p0, LX/0GD;->j:I

    .line 34414
    return-void
.end method

.method private d()Z
    .locals 5

    .prologue
    .line 34415
    iget-wide v0, p0, LX/0GD;->i:J

    .line 34416
    iget-object v2, p0, LX/0GD;->c:Ljava/util/Map;

    .line 34417
    sget-object v3, LX/040;->Q:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 34418
    sget-object v3, LX/040;->Q:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 34419
    :goto_0
    move v3, v3

    .line 34420
    iget-object v2, p0, LX/0GD;->b:LX/0GE;

    iget-object v2, v2, LX/0GE;->f:Landroid/util/LruCache;

    iget-object v4, p0, LX/0GD;->e:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v4, v4, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0GK;

    .line 34421
    if-eqz v2, :cond_1

    .line 34422
    iget-object v4, v2, LX/0GK;->x:Ljava/util/Collection;

    if-nez v4, :cond_3

    const/4 v4, 0x0

    :goto_1
    move v2, v4

    .line 34423
    add-int/2addr v2, v3

    .line 34424
    :goto_2
    move v2, v2

    .line 34425
    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 34426
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_3

    :cond_1
    move v2, v3

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    iget-object v4, v2, LX/0GK;->x:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v4

    iget p0, v2, LX/0GK;->p:I

    mul-int/2addr v4, p0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    .line 34427
    iget-object v0, p0, LX/0GD;->b:LX/0GE;

    iget-object v1, p0, LX/0GD;->e:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v1, v1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    iget-object v2, p0, LX/0GD;->f:Landroid/os/Handler;

    iget-object v3, p0, LX/0GD;->e:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v3, v3, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->b:Ljava/lang/String;

    iget-object v4, p0, LX/0GD;->g:Landroid/net/Uri;

    iget-object v5, p0, LX/0GD;->e:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v5, v5, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->e:Ljava/lang/String;

    iget-object v6, p0, LX/0GD;->c:Ljava/util/Map;

    iget-object v7, p0, LX/0GD;->d:LX/0Gk;

    iget-object v8, p0, LX/0GD;->e:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v8, v8, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->h:Ljava/lang/String;

    iget v9, p0, LX/0GD;->h:I

    iget v10, p0, LX/0GD;->j:I

    invoke-virtual/range {v0 .. v10}, LX/0GE;->a(Landroid/net/Uri;Landroid/os/Handler;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;LX/0Gk;Ljava/lang/String;II)V

    .line 34428
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0GD;->i:J

    .line 34429
    return-void
.end method

.method public final b()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 34430
    iget-object v0, p0, LX/0GD;->b:LX/0GE;

    iget-object v0, v0, LX/0GE;->f:Landroid/util/LruCache;

    iget-object v3, p0, LX/0GD;->e:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v3, v3, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GK;

    .line 34431
    if-eqz v0, :cond_0

    move v0, v1

    .line 34432
    :goto_0
    return v0

    .line 34433
    :cond_0
    invoke-virtual {v0}, LX/0GK;->h()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 34434
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "execution completed: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0GD;->e:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v1, v1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v0, v2

    .line 34435
    goto :goto_0

    .line 34436
    :cond_1
    const/4 v4, 0x1

    .line 34437
    iget-object v3, v0, LX/0GK;->c:LX/0GH;

    sget-object v5, LX/0GH;->FAILED:LX/0GH;

    if-eq v3, v5, :cond_2

    iget-object v3, v0, LX/0GK;->c:LX/0GH;

    sget-object v5, LX/0GH;->DISMISS:LX/0GH;

    if-ne v3, v5, :cond_5

    :cond_2
    move v3, v4

    .line 34438
    :goto_1
    move v0, v3

    .line 34439
    if-eqz v0, :cond_3

    .line 34440
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "execution error: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0GD;->e:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v1, v1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v0, v2

    .line 34441
    goto :goto_0

    .line 34442
    :cond_3
    invoke-direct {p0}, LX/0GD;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 34443
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "execution timeout: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0GD;->e:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v1, v1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v0, v2

    .line 34444
    goto :goto_0

    :cond_4
    move v0, v1

    .line 34445
    goto :goto_0

    .line 34446
    :cond_5
    iget-object v3, v0, LX/0GK;->x:Ljava/util/Collection;

    if-eqz v3, :cond_7

    .line 34447
    iget-object v3, v0, LX/0GK;->x:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0GM;

    .line 34448
    iget-object v6, v3, LX/0GM;->l:LX/0GL;

    sget-object v0, LX/0GL;->FAILED:LX/0GL;

    if-ne v6, v0, :cond_8

    const/4 v6, 0x1

    :goto_2
    move v3, v6

    .line 34449
    if-eqz v3, :cond_6

    move v3, v4

    .line 34450
    goto :goto_1

    .line 34451
    :cond_7
    const/4 v3, 0x0

    goto :goto_1

    :cond_8
    const/4 v6, 0x0

    goto :goto_2
.end method
