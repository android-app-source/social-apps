.class public final enum LX/097;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/097;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/097;

.field public static final enum FROM_CACHE:LX/097;

.field public static final enum FROM_LOCAL_STORAGE:LX/097;

.field public static final enum FROM_SAVED_OFFLINE_LOCAL_FILE:LX/097;

.field public static final enum FROM_STREAM:LX/097;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22074
    new-instance v0, LX/097;

    const-string v1, "FROM_STREAM"

    const-string v2, "stream"

    invoke-direct {v0, v1, v3, v2}, LX/097;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/097;->FROM_STREAM:LX/097;

    .line 22075
    new-instance v0, LX/097;

    const-string v1, "FROM_CACHE"

    const-string v2, "from_cache"

    invoke-direct {v0, v1, v4, v2}, LX/097;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/097;->FROM_CACHE:LX/097;

    .line 22076
    new-instance v0, LX/097;

    const-string v1, "FROM_LOCAL_STORAGE"

    const-string v2, "local_storage"

    invoke-direct {v0, v1, v5, v2}, LX/097;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    .line 22077
    new-instance v0, LX/097;

    const-string v1, "FROM_SAVED_OFFLINE_LOCAL_FILE"

    const-string v2, "saved_offline_local_storage"

    invoke-direct {v0, v1, v6, v2}, LX/097;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/097;->FROM_SAVED_OFFLINE_LOCAL_FILE:LX/097;

    .line 22078
    const/4 v0, 0x4

    new-array v0, v0, [LX/097;

    sget-object v1, LX/097;->FROM_STREAM:LX/097;

    aput-object v1, v0, v3

    sget-object v1, LX/097;->FROM_CACHE:LX/097;

    aput-object v1, v0, v4

    sget-object v1, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    aput-object v1, v0, v5

    sget-object v1, LX/097;->FROM_SAVED_OFFLINE_LOCAL_FILE:LX/097;

    aput-object v1, v0, v6

    sput-object v0, LX/097;->$VALUES:[LX/097;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22079
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22080
    iput-object p3, p0, LX/097;->value:Ljava/lang/String;

    .line 22081
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/097;
    .locals 1

    .prologue
    .line 22082
    const-class v0, LX/097;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/097;

    return-object v0
.end method

.method public static values()[LX/097;
    .locals 1

    .prologue
    .line 22083
    sget-object v0, LX/097;->$VALUES:[LX/097;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/097;

    return-object v0
.end method
