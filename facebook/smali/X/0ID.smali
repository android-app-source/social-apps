.class public final LX/0ID;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/08n;


# direct methods
.method public constructor <init>(LX/08n;)V
    .locals 0

    .prologue
    .line 38473
    iput-object p1, p0, LX/0ID;->a:LX/08n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 13

    .prologue
    .line 38455
    iget-object v0, p0, LX/0ID;->a:LX/08n;

    invoke-static {v0}, LX/08n;->d(LX/08n;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 38456
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "cached_qe_flag"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 38457
    iget-object v0, p0, LX/0ID;->a:LX/08n;

    invoke-static {v0, p1}, LX/08n;->a$redex0(LX/08n;I)V

    .line 38458
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 38459
    iget-object v0, p0, LX/0ID;->a:LX/08n;

    iget-object v1, p0, LX/0ID;->a:LX/08n;

    iget v1, v1, LX/08n;->e:I

    const-string v2, "NOT_IN_EXPERIMENT"

    invoke-static {v0, v1, v2}, LX/08n;->a$redex0(LX/08n;ILjava/lang/String;)V

    .line 38460
    :goto_0
    return-void

    .line 38461
    :cond_0
    iget-object v0, p0, LX/0ID;->a:LX/08n;

    iget-object v0, v0, LX/08n;->c:LX/08o;

    new-instance v1, LX/0IE;

    iget-object v2, p0, LX/0ID;->a:LX/08n;

    invoke-direct {v1, v2}, LX/0IE;-><init>(LX/08n;)V

    const/4 v7, 0x0

    .line 38462
    iget-object v4, v0, LX/08o;->a:Landroid/content/Context;

    invoke-static {v4}, LX/04u;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 38463
    iget-object v5, v0, LX/08o;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 38464
    iget-object v4, v0, LX/08o;->a:Landroid/content/Context;

    invoke-static {v4}, LX/0Ic;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0IE;->a(Ljava/lang/String;)V

    .line 38465
    :goto_1
    goto :goto_0

    .line 38466
    :cond_1
    new-instance v5, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 38467
    iget-object v6, v0, LX/08o;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v8, Lcom/facebook/rti/push/client/SharedConfigProvider$3;

    invoke-direct {v8, v0, v5, v1}, Lcom/facebook/rti/push/client/SharedConfigProvider$3;-><init>(LX/08o;Ljava/util/concurrent/atomic/AtomicBoolean;LX/0IE;)V

    const-wide/16 v10, 0x7530

    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v6, v8, v10, v11, v9}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v6

    .line 38468
    new-instance v8, LX/0IG;

    invoke-direct {v8, v0, v5, v6, v1}, LX/0IG;-><init>(LX/08o;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/concurrent/ScheduledFuture;LX/0IE;)V

    .line 38469
    new-instance v6, Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 38470
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38471
    new-instance v5, Landroid/content/Intent;

    const-string v4, "com.facebook.rti.fbns.intent.SHARE_IDS"

    invoke-direct {v5, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 38472
    iget-object v4, v0, LX/08o;->d:LX/04v;

    const/4 v10, -0x1

    move-object v9, v7

    move-object v11, v7

    move-object v12, v7

    invoke-virtual/range {v4 .. v12}, LX/04v;->a(Landroid/content/Intent;Ljava/util/List;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)I

    goto :goto_1
.end method
