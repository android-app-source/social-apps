.class public final LX/0Lo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:LX/0LV;

.field public c:LX/0Ah;

.field public d:LX/0BX;

.field public e:LX/0L4;

.field public f:I

.field private final g:J

.field private final h:Z

.field public final i:Z

.field private j:J


# direct methods
.method public constructor <init>(JJLX/0Ah;ZZ)V
    .locals 3

    .prologue
    .line 44301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44302
    iput-wide p1, p0, LX/0Lo;->g:J

    .line 44303
    iput-wide p3, p0, LX/0Lo;->j:J

    .line 44304
    iput-object p5, p0, LX/0Lo;->c:LX/0Ah;

    .line 44305
    iput-boolean p6, p0, LX/0Lo;->h:Z

    .line 44306
    iget-object v0, p5, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    .line 44307
    invoke-static {v0}, LX/0Lq;->b(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, LX/0Lo;->a:Z

    .line 44308
    iget-boolean v1, p0, LX/0Lo;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/0Lo;->b:LX/0LV;

    .line 44309
    invoke-virtual {p5}, LX/0Ah;->f()LX/0BX;

    move-result-object v0

    iput-object v0, p0, LX/0Lo;->d:LX/0BX;

    .line 44310
    iput-boolean p7, p0, LX/0Lo;->i:Z

    .line 44311
    return-void

    .line 44312
    :cond_0
    new-instance v1, LX/0LV;

    .line 44313
    const-string v2, "video/webm"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "audio/webm"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "application/webm"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 44314
    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;

    invoke-direct {v0}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;-><init>()V

    :goto_2
    invoke-direct {v1, v0}, LX/0LV;-><init>(LX/0ME;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;

    invoke-direct {v0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;-><init>()V

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private c()I
    .locals 4

    .prologue
    .line 44315
    iget-object v0, p0, LX/0Lo;->d:LX/0BX;

    iget-wide v2, p0, LX/0Lo;->j:J

    invoke-interface {v0, v2, v3}, LX/0BX;->a(J)I

    move-result v0

    return v0
.end method

.method private static f(LX/0Lo;I)I
    .locals 2

    .prologue
    .line 44316
    iget v0, p0, LX/0Lo;->f:I

    sub-int v0, p1, v0

    .line 44317
    iget-object v1, p0, LX/0Lo;->d:LX/0BX;

    invoke-interface {v1}, LX/0BX;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 44318
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Segment number without shift number is behind, segmentNum="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", segmentNumShift="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0Lo;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",firstSegmentNum="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0Lo;->d:LX/0BX;

    invoke-interface {v1}, LX/0BX;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 44319
    new-instance v1, LX/0Ll;

    invoke-direct {v1, v0}, LX/0Ll;-><init>(Ljava/lang/String;)V

    throw v1

    .line 44320
    :cond_0
    return v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 44321
    iget-object v0, p0, LX/0Lo;->d:LX/0BX;

    invoke-interface {v0}, LX/0BX;->a()I

    move-result v0

    iget v1, p0, LX/0Lo;->f:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final a(J)I
    .locals 7

    .prologue
    .line 44322
    iget-object v0, p0, LX/0Lo;->d:LX/0BX;

    iget-wide v2, p0, LX/0Lo;->g:J

    sub-long v2, p1, v2

    iget-wide v4, p0, LX/0Lo;->j:J

    invoke-interface {v0, v2, v3, v4, v5}, LX/0BX;->a(JJ)I

    move-result v0

    iget v1, p0, LX/0Lo;->f:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final a(I)J
    .locals 4

    .prologue
    .line 44323
    iget-object v0, p0, LX/0Lo;->d:LX/0BX;

    invoke-static {p0, p1}, LX/0Lo;->f(LX/0Lo;I)I

    move-result v1

    invoke-interface {v0, v1}, LX/0BX;->a(I)J

    move-result-wide v0

    iget-wide v2, p0, LX/0Lo;->g:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final a(JLX/0Ah;)V
    .locals 11

    .prologue
    .line 44324
    iget-object v0, p0, LX/0Lo;->c:LX/0Ah;

    invoke-virtual {v0}, LX/0Ah;->f()LX/0BX;

    move-result-object v0

    .line 44325
    invoke-virtual {p3}, LX/0Ah;->f()LX/0BX;

    move-result-object v1

    .line 44326
    iput-wide p1, p0, LX/0Lo;->j:J

    .line 44327
    iput-object p3, p0, LX/0Lo;->c:LX/0Ah;

    .line 44328
    if-nez v0, :cond_1

    .line 44329
    :cond_0
    :goto_0
    return-void

    .line 44330
    :cond_1
    iput-object v1, p0, LX/0Lo;->d:LX/0BX;

    .line 44331
    invoke-interface {v0}, LX/0BX;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 44332
    iget-wide v2, p0, LX/0Lo;->j:J

    invoke-interface {v0, v2, v3}, LX/0BX;->a(J)I

    move-result v2

    .line 44333
    invoke-interface {v0, v2}, LX/0BX;->a(I)J

    move-result-wide v4

    iget-wide v6, p0, LX/0Lo;->j:J

    invoke-interface {v0, v2, v6, v7}, LX/0BX;->a(IJ)J

    move-result-wide v2

    add-long/2addr v2, v4

    .line 44334
    invoke-interface {v1}, LX/0BX;->a()I

    move-result v4

    .line 44335
    invoke-interface {v1, v4}, LX/0BX;->a(I)J

    move-result-wide v6

    .line 44336
    cmp-long v1, v2, v6

    if-nez v1, :cond_2

    .line 44337
    iget v1, p0, LX/0Lo;->f:I

    iget-wide v2, p0, LX/0Lo;->j:J

    invoke-interface {v0, v2, v3}, LX/0BX;->a(J)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    sub-int/2addr v0, v4

    add-int/2addr v0, v1

    iput v0, p0, LX/0Lo;->f:I

    goto :goto_0

    .line 44338
    :cond_2
    cmp-long v1, v2, v6

    if-gez v1, :cond_4

    .line 44339
    iget-boolean v1, p0, LX/0Lo;->h:Z

    if-eqz v1, :cond_3

    .line 44340
    iget v1, p0, LX/0Lo;->f:I

    iget-wide v8, p0, LX/0Lo;->j:J

    invoke-interface {v0, v8, v9}, LX/0BX;->a(J)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    sub-int/2addr v0, v4

    add-int/2addr v0, v1

    iput v0, p0, LX/0Lo;->f:I

    .line 44341
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Discontinuity detected for live, oldIndexEndTimeUs is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newIndexStartTimeUs is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", segmentNumberShift is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0Lo;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", representation id is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0Lo;->c:LX/0Ah;

    iget-object v1, v1, LX/0Ah;->c:LX/0AR;

    iget-object v1, v1, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 44342
    :cond_3
    new-instance v0, LX/0Kn;

    invoke-direct {v0}, LX/0Kn;-><init>()V

    throw v0

    .line 44343
    :cond_4
    iget v1, p0, LX/0Lo;->f:I

    iget-wide v2, p0, LX/0Lo;->j:J

    invoke-interface {v0, v6, v7, v2, v3}, LX/0BX;->a(JJ)I

    move-result v0

    sub-int/2addr v0, v4

    add-int/2addr v0, v1

    iput v0, p0, LX/0Lo;->f:I

    goto/16 :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 44344
    invoke-direct {p0}, LX/0Lo;->c()I

    move-result v0

    iget v1, p0, LX/0Lo;->f:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final b(I)J
    .locals 6

    .prologue
    .line 44345
    invoke-virtual {p0, p1}, LX/0Lo;->a(I)J

    move-result-wide v0

    iget-object v2, p0, LX/0Lo;->d:LX/0BX;

    invoke-static {p0, p1}, LX/0Lo;->f(LX/0Lo;I)I

    move-result v3

    iget-wide v4, p0, LX/0Lo;->j:J

    invoke-interface {v2, v3, v4, v5}, LX/0BX;->a(IJ)J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final c(I)Z
    .locals 2

    .prologue
    .line 44346
    invoke-direct {p0}, LX/0Lo;->c()I

    move-result v0

    .line 44347
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v1, p0, LX/0Lo;->f:I

    add-int/2addr v0, v1

    if-le p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(I)LX/0Au;
    .locals 2

    .prologue
    .line 44348
    iget-object v0, p0, LX/0Lo;->d:LX/0BX;

    invoke-static {p0, p1}, LX/0Lo;->f(LX/0Lo;I)I

    move-result v1

    invoke-interface {v0, v1}, LX/0BX;->b(I)LX/0Au;

    move-result-object v0

    return-object v0
.end method
