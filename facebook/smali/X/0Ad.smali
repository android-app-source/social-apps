.class public final LX/0Ad;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:J


# direct methods
.method public constructor <init>(ZJ)V
    .locals 0

    .prologue
    .line 24887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24888
    iput-boolean p1, p0, LX/0Ad;->a:Z

    .line 24889
    iput-wide p2, p0, LX/0Ad;->b:J

    .line 24890
    return-void
.end method

.method public constructor <init>(ZJJJ)V
    .locals 6

    .prologue
    .line 24891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24892
    if-eqz p1, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, p6, v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    long-to-double v2, p4

    long-to-double v4, p6

    div-double/2addr v2, v4

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/0Ad;->a:Z

    .line 24893
    iput-wide p2, p0, LX/0Ad;->b:J

    .line 24894
    return-void

    .line 24895
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JJ)Z
    .locals 5

    .prologue
    .line 24896
    iget-boolean v0, p0, LX/0Ad;->a:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/0Ad;->b:J

    rem-long v0, p3, v0

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-wide v0, p0, LX/0Ad;->b:J

    div-long v0, p1, v0

    iget-wide v2, p0, LX/0Ad;->b:J

    div-long v2, p3, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(JJ)J
    .locals 7

    .prologue
    const-wide/16 v0, 0x0

    .line 24897
    iget-boolean v2, p0, LX/0Ad;->a:Z

    if-eqz v2, :cond_1

    cmp-long v2, p1, v0

    if-lez v2, :cond_1

    iget-wide v2, p0, LX/0Ad;->b:J

    rem-long v2, p1, v2

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    iget-wide v2, p0, LX/0Ad;->b:J

    div-long v2, p1, v2

    iget-wide v4, p0, LX/0Ad;->b:J

    div-long v4, p3, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 24898
    :cond_0
    iget-wide v0, p0, LX/0Ad;->b:J

    div-long v0, p3, v0

    iget-wide v2, p0, LX/0Ad;->b:J

    mul-long/2addr v0, v2

    .line 24899
    :cond_1
    return-wide v0
.end method
