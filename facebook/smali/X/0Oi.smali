.class public final LX/0Oi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:[B

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 53836
    array-length v0, p1

    invoke-direct {p0, p1, v0}, LX/0Oi;-><init>([BI)V

    .line 53837
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0

    .prologue
    .line 53832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53833
    iput-object p1, p0, LX/0Oi;->a:[B

    .line 53834
    iput p2, p0, LX/0Oi;->d:I

    .line 53835
    return-void
.end method

.method private f()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 53828
    move v0, v1

    .line 53829
    :goto_0
    invoke-virtual {p0}, LX/0Oi;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 53830
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53831
    :cond_0
    const/4 v2, 0x1

    shl-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x1

    if-lez v0, :cond_1

    invoke-virtual {p0, v0}, LX/0Oi;->c(I)I

    move-result v1

    :cond_1
    add-int v0, v2, v1

    return v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 53825
    iget v0, p0, LX/0Oi;->b:I

    if-ltz v0, :cond_1

    iget v0, p0, LX/0Oi;->c:I

    if-ltz v0, :cond_1

    iget v0, p0, LX/0Oi;->c:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_1

    iget v0, p0, LX/0Oi;->b:I

    iget v1, p0, LX/0Oi;->d:I

    if-lt v0, v1, :cond_0

    iget v0, p0, LX/0Oi;->b:I

    iget v1, p0, LX/0Oi;->d:I

    if-ne v0, v1, :cond_1

    iget v0, p0, LX/0Oi;->c:I

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 53826
    return-void

    .line 53827
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 53824
    iget v0, p0, LX/0Oi;->d:I

    iget v1, p0, LX/0Oi;->b:I

    sub-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x8

    iget v1, p0, LX/0Oi;->c:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 53839
    div-int/lit8 v0, p1, 0x8

    iput v0, p0, LX/0Oi;->b:I

    .line 53840
    iget v0, p0, LX/0Oi;->b:I

    mul-int/lit8 v0, v0, 0x8

    sub-int v0, p1, v0

    iput v0, p0, LX/0Oi;->c:I

    .line 53841
    invoke-direct {p0}, LX/0Oi;->g()V

    .line 53842
    return-void
.end method

.method public final a([BI)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53819
    iput-object p1, p0, LX/0Oi;->a:[B

    .line 53820
    iput v0, p0, LX/0Oi;->b:I

    .line 53821
    iput v0, p0, LX/0Oi;->c:I

    .line 53822
    iput p2, p0, LX/0Oi;->d:I

    .line 53823
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 53812
    iget v0, p0, LX/0Oi;->b:I

    div-int/lit8 v1, p1, 0x8

    add-int/2addr v0, v1

    iput v0, p0, LX/0Oi;->b:I

    .line 53813
    iget v0, p0, LX/0Oi;->c:I

    rem-int/lit8 v1, p1, 0x8

    add-int/2addr v0, v1

    iput v0, p0, LX/0Oi;->c:I

    .line 53814
    iget v0, p0, LX/0Oi;->c:I

    const/4 v1, 0x7

    if-le v0, v1, :cond_0

    .line 53815
    iget v0, p0, LX/0Oi;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Oi;->b:I

    .line 53816
    iget v0, p0, LX/0Oi;->c:I

    add-int/lit8 v0, v0, -0x8

    iput v0, p0, LX/0Oi;->c:I

    .line 53817
    :cond_0
    invoke-direct {p0}, LX/0Oi;->g()V

    .line 53818
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 53811
    invoke-virtual {p0, v0}, LX/0Oi;->c(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v0, 0x0

    .line 53789
    if-nez p1, :cond_0

    .line 53790
    :goto_0
    return v0

    .line 53791
    :cond_0
    div-int/lit8 v3, p1, 0x8

    move v2, v0

    move v1, v0

    .line 53792
    :goto_1
    if-ge v2, v3, :cond_2

    .line 53793
    iget v0, p0, LX/0Oi;->c:I

    if-eqz v0, :cond_1

    .line 53794
    iget-object v0, p0, LX/0Oi;->a:[B

    iget v4, p0, LX/0Oi;->b:I

    aget-byte v0, v0, v4

    and-int/lit16 v0, v0, 0xff

    iget v4, p0, LX/0Oi;->c:I

    shl-int/2addr v0, v4

    iget-object v4, p0, LX/0Oi;->a:[B

    iget v5, p0, LX/0Oi;->b:I

    add-int/lit8 v5, v5, 0x1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    iget v5, p0, LX/0Oi;->c:I

    rsub-int/lit8 v5, v5, 0x8

    ushr-int/2addr v4, v5

    or-int/2addr v0, v4

    .line 53795
    :goto_2
    add-int/lit8 p1, p1, -0x8

    .line 53796
    and-int/lit16 v0, v0, 0xff

    shl-int/2addr v0, p1

    or-int/2addr v1, v0

    .line 53797
    iget v0, p0, LX/0Oi;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Oi;->b:I

    .line 53798
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 53799
    :cond_1
    iget-object v0, p0, LX/0Oi;->a:[B

    iget v4, p0, LX/0Oi;->b:I

    aget-byte v0, v0, v4

    goto :goto_2

    .line 53800
    :cond_2
    if-lez p1, :cond_5

    .line 53801
    iget v0, p0, LX/0Oi;->c:I

    add-int v2, v0, p1

    .line 53802
    const/16 v0, 0xff

    rsub-int/lit8 v3, p1, 0x8

    shr-int/2addr v0, v3

    int-to-byte v0, v0

    .line 53803
    if-le v2, v6, :cond_4

    .line 53804
    iget-object v3, p0, LX/0Oi;->a:[B

    iget v4, p0, LX/0Oi;->b:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    add-int/lit8 v4, v2, -0x8

    shl-int/2addr v3, v4

    iget-object v4, p0, LX/0Oi;->a:[B

    iget v5, p0, LX/0Oi;->b:I

    add-int/lit8 v5, v5, 0x1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    rsub-int/lit8 v5, v2, 0x10

    shr-int/2addr v4, v5

    or-int/2addr v3, v4

    and-int/2addr v0, v3

    or-int/2addr v0, v1

    .line 53805
    iget v1, p0, LX/0Oi;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0Oi;->b:I

    .line 53806
    :cond_3
    :goto_3
    rem-int/lit8 v1, v2, 0x8

    iput v1, p0, LX/0Oi;->c:I

    .line 53807
    :goto_4
    invoke-direct {p0}, LX/0Oi;->g()V

    goto :goto_0

    .line 53808
    :cond_4
    iget-object v3, p0, LX/0Oi;->a:[B

    iget v4, p0, LX/0Oi;->b:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    rsub-int/lit8 v4, v2, 0x8

    shr-int/2addr v3, v4

    and-int/2addr v0, v3

    or-int/2addr v0, v1

    .line 53809
    if-ne v2, v6, :cond_3

    .line 53810
    iget v1, p0, LX/0Oi;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0Oi;->b:I

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4
.end method

.method public final c()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 53779
    iget v4, p0, LX/0Oi;->b:I

    .line 53780
    iget v5, p0, LX/0Oi;->c:I

    move v0, v1

    .line 53781
    :goto_0
    iget v2, p0, LX/0Oi;->b:I

    iget v6, p0, LX/0Oi;->d:I

    if-ge v2, v6, :cond_0

    invoke-virtual {p0}, LX/0Oi;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 53782
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53783
    :cond_0
    iget v2, p0, LX/0Oi;->b:I

    iget v6, p0, LX/0Oi;->d:I

    if-ne v2, v6, :cond_1

    move v2, v3

    .line 53784
    :goto_1
    iput v4, p0, LX/0Oi;->b:I

    .line 53785
    iput v5, p0, LX/0Oi;->c:I

    .line 53786
    if-nez v2, :cond_2

    invoke-virtual {p0}, LX/0Oi;->a()I

    move-result v2

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    if-lt v2, v0, :cond_2

    :goto_2
    return v3

    :cond_1
    move v2, v1

    .line 53787
    goto :goto_1

    :cond_2
    move v3, v1

    .line 53788
    goto :goto_2
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 53778
    invoke-direct {p0}, LX/0Oi;->f()I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 53776
    invoke-direct {p0}, LX/0Oi;->f()I

    move-result v1

    .line 53777
    rem-int/lit8 v0, v1, 0x2

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x2

    mul-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
