.class public LX/0KB;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/io/File;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40388
    const-class v0, LX/0KB;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0KB;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 40389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40390
    iput-object p1, p0, LX/0KB;->b:Ljava/io/File;

    .line 40391
    iget-object v0, p0, LX/0KB;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40392
    iget-object v0, p0, LX/0KB;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 40393
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0KA;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 40394
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/0KB;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 40395
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40396
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 40397
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 40398
    :try_start_0
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    .line 40399
    new-instance v0, LX/0KA;

    invoke-direct {v0, v4, v5}, LX/0KA;-><init>(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40400
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    .line 40401
    :goto_0
    return-object v0

    .line 40402
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    throw v0

    .line 40403
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/0KA;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 40404
    invoke-virtual {p0, p1}, LX/0KB;->a(Ljava/lang/String;)LX/0KA;

    move-result-object v0

    .line 40405
    if-eqz v0, :cond_1

    .line 40406
    iget-wide v2, v0, LX/0KA;->a:J

    iget-wide v4, p2, LX/0KA;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 40407
    sget-object v1, LX/0KB;->a:Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Resource metadata for key %s mismatched old: %d, new %d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v6

    iget-wide v6, v0, LX/0KA;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v8

    const/4 v0, 0x2

    iget-wide v6, p2, LX/0KA;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v1, v2, v3, v4}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40408
    :goto_0
    return-void

    .line 40409
    :cond_0
    new-array v2, v8, [Ljava/lang/Object;

    aput-object p1, v2, v6

    .line 40410
    goto :goto_0

    .line 40411
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/0KB;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 40412
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 40413
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 40414
    :cond_2
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 40415
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 40416
    :try_start_0
    iget-wide v0, p2, LX/0KA;->a:J

    invoke-virtual {v2, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40417
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    throw v0
.end method
