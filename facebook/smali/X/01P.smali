.class public LX/01P;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/OutputStream;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/OutputStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4649
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 4650
    iput-object p1, p0, LX/01P;->a:Ljava/util/ArrayList;

    .line 4651
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 5

    .prologue
    .line 4652
    const/4 v1, 0x0

    .line 4653
    iget-object v0, p0, LX/01P;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, LX/01P;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 4654
    :try_start_0
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4655
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 4656
    :catch_0
    move-exception v4

    .line 4657
    if-nez v1, :cond_2

    .line 4658
    new-instance v0, LX/0Bt;

    const-string v1, "Exception closing the stream"

    invoke-direct {v0, v1}, LX/0Bt;-><init>(Ljava/lang/String;)V

    .line 4659
    :goto_2
    invoke-virtual {v0, v4}, LX/0Bt;->a(Ljava/io/IOException;)V

    move-object v1, v0

    goto :goto_1

    .line 4660
    :cond_0
    if-eqz v1, :cond_1

    .line 4661
    throw v1

    .line 4662
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method public final flush()V
    .locals 5

    .prologue
    .line 4663
    const/4 v1, 0x0

    .line 4664
    iget-object v0, p0, LX/01P;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, LX/01P;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 4665
    :try_start_0
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4666
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 4667
    :catch_0
    move-exception v4

    .line 4668
    if-nez v1, :cond_2

    .line 4669
    new-instance v0, LX/0Bt;

    const-string v1, "Exception flushing the stream"

    invoke-direct {v0, v1}, LX/0Bt;-><init>(Ljava/lang/String;)V

    .line 4670
    :goto_2
    invoke-virtual {v0, v4}, LX/0Bt;->a(Ljava/io/IOException;)V

    move-object v1, v0

    goto :goto_1

    .line 4671
    :cond_0
    if-eqz v1, :cond_1

    .line 4672
    throw v1

    .line 4673
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method public final write(I)V
    .locals 5

    .prologue
    .line 4674
    const/4 v1, 0x0

    .line 4675
    iget-object v0, p0, LX/01P;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, LX/01P;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 4676
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4677
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 4678
    :catch_0
    move-exception v4

    .line 4679
    if-nez v1, :cond_2

    .line 4680
    new-instance v0, LX/0Bt;

    const-string v1, "Exception writing one byte to the stream"

    invoke-direct {v0, v1}, LX/0Bt;-><init>(Ljava/lang/String;)V

    .line 4681
    :goto_2
    invoke-virtual {v0, v4}, LX/0Bt;->a(Ljava/io/IOException;)V

    move-object v1, v0

    goto :goto_1

    .line 4682
    :cond_0
    if-eqz v1, :cond_1

    .line 4683
    throw v1

    .line 4684
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method public final write([B)V
    .locals 5

    .prologue
    .line 4685
    const/4 v1, 0x0

    .line 4686
    iget-object v0, p0, LX/01P;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, LX/01P;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 4687
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4688
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 4689
    :catch_0
    move-exception v4

    .line 4690
    if-nez v1, :cond_2

    .line 4691
    new-instance v0, LX/0Bt;

    const-string v1, "Exception writing to the stream"

    invoke-direct {v0, v1}, LX/0Bt;-><init>(Ljava/lang/String;)V

    .line 4692
    :goto_2
    invoke-virtual {v0, v4}, LX/0Bt;->a(Ljava/io/IOException;)V

    move-object v1, v0

    goto :goto_1

    .line 4693
    :cond_0
    if-eqz v1, :cond_1

    .line 4694
    throw v1

    .line 4695
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method public final write([BII)V
    .locals 5

    .prologue
    .line 4696
    const/4 v1, 0x0

    .line 4697
    iget-object v0, p0, LX/01P;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, LX/01P;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 4698
    :try_start_0
    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4699
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 4700
    :catch_0
    move-exception v4

    .line 4701
    if-nez v1, :cond_2

    .line 4702
    new-instance v0, LX/0Bt;

    const-string v1, "Exception writing to the stream"

    invoke-direct {v0, v1}, LX/0Bt;-><init>(Ljava/lang/String;)V

    .line 4703
    :goto_2
    invoke-virtual {v0, v4}, LX/0Bt;->a(Ljava/io/IOException;)V

    move-object v1, v0

    goto :goto_1

    .line 4704
    :cond_0
    if-eqz v1, :cond_1

    .line 4705
    throw v1

    .line 4706
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method
