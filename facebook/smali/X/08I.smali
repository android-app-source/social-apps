.class public final LX/08I;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/08H;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/08H;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:I

.field public final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/08H;",
            ">;"
        }
    .end annotation
.end field

.field private final e:[LX/08H;

.field public f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20995
    new-instance v0, LX/0Ok;

    invoke-direct {v0}, LX/0Ok;-><init>()V

    sput-object v0, LX/08I;->a:Ljava/util/Comparator;

    .line 20996
    new-instance v0, LX/0Ol;

    invoke-direct {v0}, LX/0Ol;-><init>()V

    sput-object v0, LX/08I;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 20997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20998
    iput p1, p0, LX/08I;->c:I

    .line 20999
    const/4 v0, 0x5

    new-array v0, v0, [LX/08H;

    iput-object v0, p0, LX/08I;->e:[LX/08H;

    .line 21000
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/08I;->d:Ljava/util/ArrayList;

    .line 21001
    const/4 v0, -0x1

    iput v0, p0, LX/08I;->f:I

    .line 21002
    return-void
.end method


# virtual methods
.method public final a(F)F
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 21003
    iget v1, p0, LX/08I;->f:I

    if-eqz v1, :cond_0

    .line 21004
    iget-object v1, p0, LX/08I;->d:Ljava/util/ArrayList;

    sget-object v2, LX/08I;->b:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 21005
    const/4 v1, 0x0

    iput v1, p0, LX/08I;->f:I

    .line 21006
    :cond_0
    iget v1, p0, LX/08I;->h:I

    int-to-float v1, v1

    mul-float v3, p1, v1

    move v1, v0

    move v2, v0

    .line 21007
    :goto_0
    iget-object v0, p0, LX/08I;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 21008
    iget-object v0, p0, LX/08I;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/08H;

    .line 21009
    iget v4, v0, LX/08H;->b:I

    add-int/2addr v2, v4

    .line 21010
    int-to-float v4, v2

    cmpl-float v4, v4, v3

    if-ltz v4, :cond_1

    .line 21011
    iget v0, v0, LX/08H;->c:F

    .line 21012
    :goto_1
    return v0

    .line 21013
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 21014
    :cond_2
    iget-object v0, p0, LX/08I;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const/high16 v0, 0x7fc00000    # NaNf

    goto :goto_1

    :cond_3
    iget-object v0, p0, LX/08I;->d:Ljava/util/ArrayList;

    iget-object v1, p0, LX/08I;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/08H;

    iget v0, v0, LX/08H;->c:F

    goto :goto_1
.end method

.method public final a(IF)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 21015
    const/4 v2, 0x1

    .line 21016
    iget v0, p0, LX/08I;->f:I

    if-eq v0, v2, :cond_0

    .line 21017
    iget-object v0, p0, LX/08I;->d:Ljava/util/ArrayList;

    sget-object v1, LX/08I;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 21018
    iput v2, p0, LX/08I;->f:I

    .line 21019
    :cond_0
    iget v0, p0, LX/08I;->i:I

    if-lez v0, :cond_2

    iget-object v0, p0, LX/08I;->e:[LX/08H;

    iget v1, p0, LX/08I;->i:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/08I;->i:I

    aget-object v0, v0, v1

    .line 21020
    :goto_0
    iget v1, p0, LX/08I;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/08I;->g:I

    iput v1, v0, LX/08H;->a:I

    .line 21021
    iput p1, v0, LX/08H;->b:I

    .line 21022
    iput p2, v0, LX/08H;->c:F

    .line 21023
    iget-object v1, p0, LX/08I;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21024
    iget v0, p0, LX/08I;->h:I

    add-int/2addr v0, p1

    iput v0, p0, LX/08I;->h:I

    .line 21025
    :cond_1
    :goto_1
    iget v0, p0, LX/08I;->h:I

    iget v1, p0, LX/08I;->c:I

    if-le v0, v1, :cond_4

    .line 21026
    iget v0, p0, LX/08I;->h:I

    iget v1, p0, LX/08I;->c:I

    sub-int v1, v0, v1

    .line 21027
    iget-object v0, p0, LX/08I;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/08H;

    .line 21028
    iget v2, v0, LX/08H;->b:I

    if-gt v2, v1, :cond_3

    .line 21029
    iget v1, p0, LX/08I;->h:I

    iget v2, v0, LX/08H;->b:I

    sub-int/2addr v1, v2

    iput v1, p0, LX/08I;->h:I

    .line 21030
    iget-object v1, p0, LX/08I;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 21031
    iget v1, p0, LX/08I;->i:I

    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 21032
    iget-object v1, p0, LX/08I;->e:[LX/08H;

    iget v2, p0, LX/08I;->i:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/08I;->i:I

    aput-object v0, v1, v2

    goto :goto_1

    .line 21033
    :cond_2
    new-instance v0, LX/08H;

    invoke-direct {v0}, LX/08H;-><init>()V

    goto :goto_0

    .line 21034
    :cond_3
    iget v2, v0, LX/08H;->b:I

    sub-int/2addr v2, v1

    iput v2, v0, LX/08H;->b:I

    .line 21035
    iget v0, p0, LX/08I;->h:I

    sub-int/2addr v0, v1

    iput v0, p0, LX/08I;->h:I

    goto :goto_1

    .line 21036
    :cond_4
    return-void
.end method
