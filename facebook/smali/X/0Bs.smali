.class public LX/0Bs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/04b;


# instance fields
.field private mProxy:Ljava/net/Proxy;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mSocketTimeout:I


# direct methods
.method public constructor <init>(ILjava/net/Proxy;)V
    .locals 0
    .param p2    # Ljava/net/Proxy;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 26935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26936
    iput p1, p0, LX/0Bs;->mSocketTimeout:I

    .line 26937
    iput-object p2, p0, LX/0Bs;->mProxy:Ljava/net/Proxy;

    .line 26938
    return-void
.end method


# virtual methods
.method public getConnection(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 6

    .prologue
    .line 26939
    iget-object v1, p0, LX/0Bs;->mProxy:Ljava/net/Proxy;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0Bs;->mProxy:Ljava/net/Proxy;

    invoke-virtual {p1, v1}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v1

    :goto_0
    check-cast v1, Ljava/net/HttpURLConnection;

    check-cast v1, Ljava/net/HttpURLConnection;

    .line 26940
    instance-of v2, v1, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v2, :cond_0

    .line 26941
    :try_start_0
    const-string v2, "TLS"

    invoke-static {v2}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v2

    .line 26942
    const/4 v3, 0x1

    new-array v3, v3, [Ljavax/net/ssl/TrustManager;

    const/4 v4, 0x0

    new-instance v5, LX/0Bq;

    invoke-direct {v5}, LX/0Bq;-><init>()V

    aput-object v5, v3, v4

    .line 26943
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v3, v5}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 26944
    invoke-virtual {v2}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v3

    .line 26945
    move-object v0, v1

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    move-object v2, v0

    .line 26946
    invoke-virtual {v2, v3}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 26947
    new-instance v3, LX/0Br;

    invoke-direct {v3, p0}, LX/0Br;-><init>(LX/0Bs;)V

    invoke-virtual {v2, v3}, Ljavax/net/ssl/HttpsURLConnection;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/KeyManagementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26948
    :cond_0
    :goto_1
    iget v0, p0, LX/0Bs;->mSocketTimeout:I

    invoke-virtual {v1, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 26949
    iget v0, p0, LX/0Bs;->mSocketTimeout:I

    invoke-virtual {v1, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 26950
    move-object v1, v1

    .line 26951
    return-object v1

    .line 26952
    :cond_1
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    goto :goto_0

    :catch_0
    goto :goto_1

    .line 26953
    :catch_1
    goto :goto_1
.end method
