.class public final LX/0LE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/Choreographer$FrameCallback;


# static fields
.field public static final b:LX/0LE;


# instance fields
.field public volatile a:J

.field public final c:Landroid/os/Handler;

.field private final d:Landroid/os/HandlerThread;

.field public e:Landroid/view/Choreographer;

.field public f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43114
    new-instance v0, LX/0LE;

    invoke-direct {v0}, LX/0LE;-><init>()V

    sput-object v0, LX/0LE;->b:LX/0LE;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 43115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43116
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ChoreographerOwner:Handler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0LE;->d:Landroid/os/HandlerThread;

    .line 43117
    iget-object v0, p0, LX/0LE;->d:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 43118
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, LX/0LE;->d:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, LX/0LE;->c:Landroid/os/Handler;

    .line 43119
    iget-object v0, p0, LX/0LE;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 43120
    return-void
.end method


# virtual methods
.method public final doFrame(J)V
    .locals 5

    .prologue
    .line 43121
    iput-wide p1, p0, LX/0LE;->a:J

    .line 43122
    iget-object v0, p0, LX/0LE;->e:Landroid/view/Choreographer;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, p0, v2, v3}, Landroid/view/Choreographer;->postFrameCallbackDelayed(Landroid/view/Choreographer$FrameCallback;J)V

    .line 43123
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 43124
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 43125
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 43126
    :pswitch_0
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v1

    iput-object v1, p0, LX/0LE;->e:Landroid/view/Choreographer;

    .line 43127
    goto :goto_0

    .line 43128
    :pswitch_1
    iget v1, p0, LX/0LE;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0LE;->f:I

    .line 43129
    iget v1, p0, LX/0LE;->f:I

    const/4 p1, 0x1

    if-ne v1, p1, :cond_0

    .line 43130
    iget-object v1, p0, LX/0LE;->e:Landroid/view/Choreographer;

    invoke-virtual {v1, p0}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 43131
    :cond_0
    goto :goto_0

    .line 43132
    :pswitch_2
    iget v2, p0, LX/0LE;->f:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, LX/0LE;->f:I

    .line 43133
    iget v2, p0, LX/0LE;->f:I

    if-nez v2, :cond_1

    .line 43134
    iget-object v2, p0, LX/0LE;->e:Landroid/view/Choreographer;

    invoke-virtual {v2, p0}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 43135
    const-wide/16 v2, 0x0

    iput-wide v2, p0, LX/0LE;->a:J

    .line 43136
    :cond_1
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
