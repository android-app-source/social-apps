.class public LX/00q;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static d:Landroid/os/PowerManager;


# instance fields
.field public b:Z

.field public c:Z

.field public e:I

.field public f:I

.field public g:I

.field public h:J

.field public i:J

.field public j:J

.field public k:J

.field private l:Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

.field public m:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3115
    const-class v0, LX/00q;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/00q;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3041
    invoke-direct {p0}, LX/00q;->q()V

    .line 3042
    return-void
.end method

.method public constructor <init>(LX/00q;)V
    .locals 2

    .prologue
    .line 3102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3103
    invoke-direct {p0}, LX/00q;->q()V

    .line 3104
    if-eqz p1, :cond_0

    .line 3105
    iget v0, p1, LX/00q;->e:I

    iput v0, p0, LX/00q;->e:I

    .line 3106
    iget v0, p1, LX/00q;->f:I

    iput v0, p0, LX/00q;->f:I

    .line 3107
    iget-wide v0, p1, LX/00q;->h:J

    iput-wide v0, p0, LX/00q;->h:J

    .line 3108
    iget-wide v0, p1, LX/00q;->i:J

    iput-wide v0, p0, LX/00q;->i:J

    .line 3109
    iget-wide v0, p1, LX/00q;->j:J

    iput-wide v0, p0, LX/00q;->j:J

    .line 3110
    iget-wide v0, p1, LX/00q;->k:J

    iput-wide v0, p0, LX/00q;->k:J

    .line 3111
    iget-object v0, p1, LX/00q;->l:Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

    iput-object v0, p0, LX/00q;->l:Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

    .line 3112
    iget-object v0, p1, LX/00q;->m:Ljava/lang/String;

    iput-object v0, p0, LX/00q;->m:Ljava/lang/String;

    .line 3113
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/00q;->b:Z

    .line 3114
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/PowerManager;)V
    .locals 0

    .prologue
    .line 3099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3100
    sput-object p1, LX/00q;->d:Landroid/os/PowerManager;

    .line 3101
    return-void
.end method

.method public static a()LX/00q;
    .locals 1

    .prologue
    .line 3096
    new-instance v0, LX/00q;

    invoke-direct {v0}, LX/00q;-><init>()V

    .line 3097
    invoke-virtual {v0}, LX/00q;->n()V

    .line 3098
    return-object v0
.end method

.method private q()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    const-wide/16 v2, -0x1

    .line 3116
    iput-boolean v1, p0, LX/00q;->b:Z

    .line 3117
    iput-boolean v1, p0, LX/00q;->c:Z

    .line 3118
    iput v0, p0, LX/00q;->e:I

    .line 3119
    iput v0, p0, LX/00q;->f:I

    .line 3120
    iput v0, p0, LX/00q;->g:I

    .line 3121
    const-string v0, "not set"

    iput-object v0, p0, LX/00q;->m:Ljava/lang/String;

    .line 3122
    iput-wide v2, p0, LX/00q;->h:J

    .line 3123
    iput-wide v2, p0, LX/00q;->i:J

    .line 3124
    iput-wide v2, p0, LX/00q;->j:J

    .line 3125
    iput-wide v2, p0, LX/00q;->k:J

    .line 3126
    const/4 v0, 0x0

    iput-object v0, p0, LX/00q;->l:Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

    .line 3127
    return-void
.end method


# virtual methods
.method public final j()I
    .locals 1

    .prologue
    .line 3093
    sget-boolean v0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->sIsIntialized:Z

    if-eqz v0, :cond_0

    .line 3094
    invoke-static {}, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->getNumClassLoadAttempts()I

    move-result v0

    .line 3095
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/00q;->l:Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

    iget v0, v0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;->a:I

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 3090
    sget-boolean v0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->sIsIntialized:Z

    if-eqz v0, :cond_0

    .line 3091
    invoke-static {}, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->getNumDexQueries()I

    move-result v0

    .line 3092
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/00q;->l:Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

    iget v0, v0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;->b:I

    goto :goto_0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 3087
    iget-boolean v0, p0, LX/00q;->c:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/00q;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3088
    const/4 v0, 0x1

    .line 3089
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 3066
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    iput v0, p0, LX/00q;->e:I

    .line 3067
    iget v0, p0, LX/00q;->e:I

    invoke-static {v0}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v0

    iput v0, p0, LX/00q;->f:I

    .line 3068
    sget-object v0, LX/00q;->d:Landroid/os/PowerManager;

    if-eqz v0, :cond_1

    .line 3069
    const-string v0, "unknown"

    .line 3070
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 3071
    sget-object v0, LX/00q;->d:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isPowerSaveMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3072
    const-string v0, "true"

    .line 3073
    :cond_0
    :goto_0
    iput-object v0, p0, LX/00q;->m:Ljava/lang/String;

    .line 3074
    :cond_1
    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/00q;->h:J

    .line 3075
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/00q;->i:J

    .line 3076
    invoke-static {}, LX/015;->a()[J

    move-result-object v0

    .line 3077
    aget-wide v0, v0, v3

    iput-wide v0, p0, LX/00q;->j:J

    .line 3078
    invoke-static {}, LX/015;->b()[J

    move-result-object v0

    .line 3079
    aget-wide v0, v0, v3

    iput-wide v0, p0, LX/00q;->k:J

    .line 3080
    sget-object v0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    move-object v0, v0

    .line 3081
    invoke-virtual {v0}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->d()Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

    move-result-object v0

    iput-object v0, p0, LX/00q;->l:Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

    .line 3082
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/00q;->b:Z

    .line 3083
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/00q;->c:Z

    .line 3084
    const/4 v0, -0x1

    iput v0, p0, LX/00q;->g:I

    .line 3085
    return-void

    .line 3086
    :cond_2
    const-string v0, "false"

    goto :goto_0
.end method

.method public final o()V
    .locals 13

    .prologue
    const/4 v8, 0x2

    const-wide/16 v6, 0x0

    .line 3043
    iget-boolean v0, p0, LX/00q;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/00q;->c:Z

    if-eqz v0, :cond_1

    .line 3044
    :cond_0
    :goto_0
    return-void

    .line 3045
    :cond_1
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    .line 3046
    invoke-static {v0}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v1

    iput v1, p0, LX/00q;->g:I

    .line 3047
    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    move-result-wide v2

    iget-wide v4, p0, LX/00q;->h:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/00q;->h:J

    .line 3048
    invoke-static {}, LX/015;->a()[J

    move-result-object v1

    .line 3049
    aget-wide v2, v1, v8

    iget-wide v4, p0, LX/00q;->j:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/00q;->j:J

    .line 3050
    iget v1, p0, LX/00q;->e:I

    if-ne v0, v1, :cond_4

    .line 3051
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LX/00q;->i:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/00q;->i:J

    .line 3052
    invoke-static {}, LX/015;->b()[J

    move-result-object v0

    .line 3053
    aget-wide v0, v0, v8

    iget-wide v2, p0, LX/00q;->k:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/00q;->k:J

    .line 3054
    :goto_1
    sget-object v0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    move-object v0, v0

    .line 3055
    iget-object v1, p0, LX/00q;->l:Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

    invoke-virtual {v0, v1}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a(Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;)Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

    move-result-object v0

    iput-object v0, p0, LX/00q;->l:Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

    .line 3056
    iget-wide v0, p0, LX/00q;->h:J

    cmp-long v0, v0, v6

    if-ltz v0, :cond_2

    iget-wide v0, p0, LX/00q;->j:J

    cmp-long v0, v0, v6

    if-ltz v0, :cond_2

    invoke-virtual {p0}, LX/00q;->m()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-wide v0, p0, LX/00q;->i:J

    cmp-long v0, v0, v6

    if-ltz v0, :cond_2

    iget-wide v0, p0, LX/00q;->k:J

    cmp-long v0, v0, v6

    if-gez v0, :cond_5

    .line 3057
    :cond_2
    sget-object v0, LX/00q;->a:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3058
    sget-object v0, LX/00q;->a:Ljava/lang/String;

    const-string v1, "Negative values detected for PerfStats, discarding stats."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3059
    :cond_3
    invoke-direct {p0}, LX/00q;->q()V

    goto :goto_0

    .line 3060
    :cond_4
    const-wide/16 v11, -0x1

    .line 3061
    const/4 v9, -0x1

    iput v9, p0, LX/00q;->e:I

    .line 3062
    iput-wide v11, p0, LX/00q;->i:J

    .line 3063
    iput-wide v11, p0, LX/00q;->k:J

    .line 3064
    goto :goto_1

    .line 3065
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/00q;->c:Z

    goto/16 :goto_0
.end method
