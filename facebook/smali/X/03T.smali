.class public abstract LX/03T;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:I


# instance fields
.field private b:Z

.field public volatile c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10311
    const/16 v0, 0x15

    sput v0, LX/03T;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract b()V
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 10313
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/03T;->b:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x1000

    invoke-static {v0, v1}, LX/00k;->a(J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 10314
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 10315
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, LX/03T;->c:Z

    .line 10316
    invoke-virtual {p0}, LX/03T;->a()V

    .line 10317
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/03T;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 10318
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 2

    .prologue
    .line 10319
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/03T;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 10320
    :goto_0
    monitor-exit p0

    return-void

    .line 10321
    :cond_0
    :try_start_1
    iget-boolean v0, p0, LX/03T;->c:Z

    if-eqz v0, :cond_1

    .line 10322
    const-wide/16 v0, 0x1000

    invoke-static {v0, v1}, LX/018;->a(J)V

    .line 10323
    :cond_1
    invoke-virtual {p0}, LX/03T;->b()V

    .line 10324
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/03T;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 10325
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
