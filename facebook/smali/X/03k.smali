.class public LX/03k;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 10703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10704
    return-void
.end method

.method public static a(LX/0QB;)LX/03k;
    .locals 1

    .prologue
    .line 10705
    new-instance v0, LX/03k;

    invoke-direct {v0}, LX/03k;-><init>()V

    .line 10706
    move-object v0, v0

    .line 10707
    return-object v0
.end method

.method public static a()Landroid/app/Application;
    .locals 1

    .prologue
    .line 10708
    invoke-static {}, LX/00y;->a()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getApplication()Landroid/app/Application;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.util.Log.w",
            "SharedPreferencesUse"
        }
    .end annotation

    .prologue
    .line 10709
    invoke-static {}, LX/03k;->a()Landroid/app/Application;

    move-result-object v0

    .line 10710
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    .line 10711
    :goto_0
    if-nez v0, :cond_1

    .line 10712
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Context not available"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 10713
    :goto_1
    return-void

    .line 10714
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 10715
    :cond_1
    const-string v1, "breakpad_flags_store"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 10716
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "android_crash_breakpad_collect_stack_for_vps"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1
.end method
