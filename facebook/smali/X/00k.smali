.class public final LX/00k;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/013;

.field public static volatile b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2887
    new-instance v0, LX/013;

    invoke-direct {v0}, LX/013;-><init>()V

    sput-object v0, LX/00k;->a:LX/013;

    .line 2888
    const-wide/16 v0, 0x0

    sput-wide v0, LX/00k;->b:J

    .line 2889
    const/4 v0, 0x0

    invoke-static {v0}, LX/00k;->c(Z)V

    .line 2890
    new-instance v0, Lcom/facebook/systrace/TraceConfig$1;

    invoke-direct {v0}, Lcom/facebook/systrace/TraceConfig$1;-><init>()V

    invoke-static {v0}, LX/011;->a(Ljava/lang/Runnable;)V

    .line 2891
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2886
    return-void
.end method

.method public static a()J
    .locals 4

    .prologue
    .line 2884
    const-string v0, "debug.fbsystrace.tags"

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, LX/011;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(LX/00j;)V
    .locals 1

    .prologue
    .line 2864
    sget-object v0, LX/00k;->a:LX/013;

    invoke-virtual {v0, p0}, LX/013;->a(LX/00j;)V

    .line 2865
    return-void
.end method

.method public static a(J)Z
    .locals 4

    .prologue
    .line 2883
    sget-wide v0, LX/00k;->b:J

    and-long/2addr v0, p0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Z)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 2866
    invoke-static {}, LX/00z;->a()Z

    move-result v0

    .line 2867
    invoke-static {}, LX/00k;->a()J

    move-result-wide v4

    .line 2868
    if-eqz v0, :cond_0

    cmp-long v0, v4, v2

    if-nez v0, :cond_4

    :cond_0
    move-wide v0, v2

    .line 2869
    :goto_0
    sget-wide v4, LX/00k;->b:J

    cmp-long v4, v4, v2

    if-nez v4, :cond_1

    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    :cond_1
    cmp-long v4, v0, v2

    if-nez v4, :cond_5

    sget-wide v4, LX/00k;->b:J

    cmp-long v2, v4, v2

    if-eqz v2, :cond_5

    :cond_2
    const/4 v2, 0x1

    .line 2870
    :goto_1
    sput-wide v0, LX/00k;->b:J

    .line 2871
    if-eqz v2, :cond_3

    .line 2872
    invoke-static {v0, v1}, Lcom/facebook/systrace/TraceDirect;->a(J)V

    .line 2873
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-lez v6, :cond_6

    const/4 v6, 0x1

    .line 2874
    :goto_2
    if-eqz v6, :cond_8

    .line 2875
    if-nez p0, :cond_7

    .line 2876
    sget-object v6, LX/00k;->a:LX/013;

    invoke-virtual {v6}, LX/013;->b()V

    .line 2877
    :cond_3
    :goto_3
    return-void

    .line 2878
    :cond_4
    const-wide/16 v0, 0x1

    or-long/2addr v0, v4

    goto :goto_0

    .line 2879
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 2880
    :cond_6
    const/4 v6, 0x0

    goto :goto_2

    .line 2881
    :cond_7
    sget-object v6, LX/00k;->a:LX/013;

    invoke-virtual {v6}, LX/013;->a()V

    goto :goto_3

    .line 2882
    :cond_8
    sget-object v6, LX/00k;->a:LX/013;

    invoke-virtual {v6}, LX/013;->c()V

    goto :goto_3
.end method
