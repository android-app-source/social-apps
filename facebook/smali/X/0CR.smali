.class public final LX/0CR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/BrowserLiteChrome;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V
    .locals 0

    .prologue
    .line 27250
    iput-object p1, p0, LX/0CR;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDismiss()V
    .locals 3

    .prologue
    .line 27251
    iget-object v0, p0, LX/0CR;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->b()Z

    .line 27252
    iget-object v0, p0, LX/0CR;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-boolean v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->z:Z

    if-eqz v0, :cond_0

    .line 27253
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 27254
    const-string v1, "action"

    const-string v2, "zoom"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27255
    const-string v1, "text_zoom_level"

    iget-object v2, p0, LX/0CR;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget v2, v2, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27256
    const-string v1, "url"

    iget-object v2, p0, LX/0CR;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v2}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27257
    iget-object v1, p0, LX/0CR;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->t:LX/0CQ;

    iget-object v2, p0, LX/0CR;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteChrome;->C:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2}, LX/0CQ;->a(Ljava/util/Map;Landroid/os/Bundle;)V

    .line 27258
    iget-object v0, p0, LX/0CR;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    const/4 v1, 0x0

    .line 27259
    iput-boolean v1, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->z:Z

    .line 27260
    :cond_0
    return-void
.end method
