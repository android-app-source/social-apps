.class public LX/01N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "StringFormatUse"
    }
.end annotation


# static fields
.field public static final e:[B


# instance fields
.field private final a:Ljava/nio/channels/FileLock;

.field public final b:Ljava/io/RandomAccessFile;

.field public final c:Ljava/security/MessageDigest;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4606
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/01N;->e:[B

    return-void

    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
    .end array-data
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4608
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "rw"

    invoke-direct {v0, p1, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/01N;->b:Ljava/io/RandomAccessFile;

    .line 4609
    iget-object v0, p0, LX/01N;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 4610
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->tryLock()Ljava/nio/channels/FileLock;

    move-result-object v0

    iput-object v0, p0, LX/01N;->a:Ljava/nio/channels/FileLock;

    .line 4611
    iget-object v0, p0, LX/01N;->a:Ljava/nio/channels/FileLock;

    if-nez v0, :cond_0

    .line 4612
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to acquire lock for app state log file: %s"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4613
    :cond_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    iput-object v0, p0, LX/01N;->c:Ljava/security/MessageDigest;

    .line 4614
    iget-object v0, p0, LX/01N;->c:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->getDigestLength()I

    move-result v0

    .line 4615
    mul-int/lit8 v0, v0, 0x2

    .line 4616
    if-eq v0, v6, :cond_1

    .line 4617
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Expected digest to have length %d; found %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 4618
    :cond_1
    return-void
.end method

.method private static g(LX/01N;)V
    .locals 2

    .prologue
    .line 4619
    iget-boolean v0, p0, LX/01N;->d:Z

    if-eqz v0, :cond_0

    .line 4620
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot modify log file while content output stream is open"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4621
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/io/OutputStream;
    .locals 7

    .prologue
    .line 4622
    invoke-static {p0}, LX/01N;->g(LX/01N;)V

    .line 4623
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/01N;->d:Z

    .line 4624
    iget-object v3, p0, LX/01N;->b:Ljava/io/RandomAccessFile;

    const-wide/16 v5, 0x1

    invoke-virtual {v3, v5, v6}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 4625
    iget-object v3, p0, LX/01N;->b:Ljava/io/RandomAccessFile;

    const-wide/16 v5, 0x21

    invoke-virtual {v3, v5, v6}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 4626
    iget-object v0, p0, LX/01N;->c:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->reset()V

    .line 4627
    new-instance v0, LX/028;

    invoke-direct {v0, p0}, LX/028;-><init>(LX/01N;)V

    .line 4628
    new-instance v1, Ljava/security/DigestOutputStream;

    iget-object v2, p0, LX/01N;->c:Ljava/security/MessageDigest;

    invoke-direct {v1, v0, v2}, Ljava/security/DigestOutputStream;-><init>(Ljava/io/OutputStream;Ljava/security/MessageDigest;)V

    .line 4629
    return-object v1
.end method

.method public final a(I)V
    .locals 6

    .prologue
    .line 4630
    invoke-static {p0}, LX/01N;->g(LX/01N;)V

    .line 4631
    invoke-static {p1}, LX/01R;->a(I)C

    move-result v0

    .line 4632
    if-ltz v0, :cond_0

    const/16 v1, 0x7f

    if-le v0, v1, :cond_1

    .line 4633
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Status byte should be ASCII"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4634
    :cond_1
    iget-object v2, p0, LX/01N;->b:Ljava/io/RandomAccessFile;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 4635
    iget-object v1, p0, LX/01N;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v0}, Ljava/io/RandomAccessFile;->write(I)V

    .line 4636
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 4637
    iget-object v0, p0, LX/01N;->b:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_0

    .line 4638
    iget-object v0, p0, LX/01N;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 4639
    :cond_0
    return-void
.end method
