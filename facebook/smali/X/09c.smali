.class public final LX/09c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field public final synthetic a:Lcom/facebook/loom/logger/LoggerWorkerThread;


# direct methods
.method public constructor <init>(Lcom/facebook/loom/logger/LoggerWorkerThread;)V
    .locals 0

    .prologue
    .line 22642
    iput-object p1, p0, LX/09c;->a:Lcom/facebook/loom/logger/LoggerWorkerThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 22643
    const-string v0, "LoggerWorkerThread"

    const-string v1, "Unhandled exception -- Dextr disabled"

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 22644
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/009;->handleException(Ljava/lang/Throwable;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;

    .line 22645
    iget-object v0, p0, LX/09c;->a:Lcom/facebook/loom/logger/LoggerWorkerThread;

    invoke-virtual {v0}, Lcom/facebook/loom/logger/LoggerWorkerThread;->a()V

    .line 22646
    return-void
.end method
