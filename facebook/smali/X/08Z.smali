.class public final LX/08Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public accumulatedRunNs:J

.field public accumulatedYieldNs:J

.field public final config:LX/08F;

.field public final configProvider:LX/08G;

.field public final dexStoreConfig:LX/080;

.field public final mOptLockFile:LX/02V;

.field private final mRegenStampFile:Ljava/io/FileInputStream;

.field public final startRealtimeMs:J

.field public final startUptimeMs:J

.field public final synthetic this$0:LX/02U;


# direct methods
.method public constructor <init>(LX/02U;LX/08G;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 21270
    iput-object p1, p0, LX/08Z;->this$0:LX/02U;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21271
    new-instance v0, Ljava/io/File;

    iget-object v1, p1, LX/02U;->root:Ljava/io/File;

    const-string v3, "optimization_log"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 21272
    invoke-static {v0}, LX/08a;->readOrMakeDefault(Ljava/io/File;)LX/08a;

    move-result-object v1

    .line 21273
    iget v3, v1, LX/08a;->nrOptimizationsAttempted:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, LX/08a;->nrOptimizationsAttempted:I

    .line 21274
    invoke-static {p1}, LX/02U;->readStatusLocked(LX/02U;)J

    move-result-wide v4

    .line 21275
    invoke-static {p1, v4, v5}, LX/02U;->writeTxFailedStatusLocked(LX/02U;J)V

    .line 21276
    invoke-virtual {v1, v0}, LX/08a;->write(Ljava/io/File;)V

    .line 21277
    invoke-virtual {p1, v4, v5}, LX/02U;->writeStatusLocked(J)V

    .line 21278
    invoke-static {p1}, LX/02U;->readDexStoreConfig(LX/02U;)LX/080;

    move-result-object v0

    .line 21279
    iput-object v0, p0, LX/08Z;->dexStoreConfig:LX/080;

    .line 21280
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/08Z;->startUptimeMs:J

    .line 21281
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/08Z;->startRealtimeMs:J

    .line 21282
    :try_start_0
    iput-object p2, p0, LX/08Z;->configProvider:LX/08G;

    .line 21283
    iget-object v0, p2, LX/08G;->baseline:LX/08F;

    iput-object v0, p0, LX/08Z;->config:LX/08F;

    .line 21284
    new-instance v3, Ljava/io/FileInputStream;

    new-instance v0, Ljava/io/File;

    iget-object v1, p1, LX/02U;->root:Ljava/io/File;

    const-string v4, "regen_stamp"

    invoke-direct {v0, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 21285
    :try_start_1
    new-instance v0, Ljava/io/File;

    iget-object v1, p1, LX/02U;->root:Ljava/io/File;

    const-string v4, "odex_lock"

    invoke-direct {v0, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, LX/02V;->open(Ljava/io/File;)LX/02V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 21286
    :try_start_2
    iput-object v3, p0, LX/08Z;->mRegenStampFile:Ljava/io/FileInputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 21287
    :try_start_3
    iput-object v1, p0, LX/08Z;->mOptLockFile:LX/02V;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 21288
    invoke-static {v2}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 21289
    invoke-static {v2}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 21290
    return-void

    .line 21291
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    invoke-static {v2}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 21292
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    throw v0

    .line 21293
    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_0

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_0

    :catchall_3
    move-exception v0

    goto :goto_0
.end method

.method private resumeProcess(Lcom/facebook/forker/Process;)I
    .locals 2

    .prologue
    .line 21397
    const/16 v0, 0x12

    invoke-virtual {p1, v0}, Lcom/facebook/forker/Process;->kill(I)V

    .line 21398
    const/4 v0, -0x1

    const/4 v1, 0x5

    invoke-virtual {p1, v0, v1}, Lcom/facebook/forker/Process;->waitFor(II)I

    move-result v0

    return v0
.end method

.method private stopProcess(Lcom/facebook/forker/Process;)I
    .locals 2

    .prologue
    .line 21395
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lcom/facebook/forker/Process;->kill(I)V

    .line 21396
    const/4 v0, -0x1

    const/4 v1, 0x6

    invoke-virtual {p1, v0, v1}, Lcom/facebook/forker/Process;->waitFor(II)I

    move-result v0

    return v0
.end method

.method private updateOptimizationLogCounters(LX/08a;)V
    .locals 10

    .prologue
    const-wide/32 v8, 0xf4240

    const/4 v0, 0x0

    .line 21387
    iget-object v1, p1, LX/08a;->lastAttemptCounters:[J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, LX/08Z;->startUptimeMs:J

    sub-long/2addr v2, v4

    aput-wide v2, v1, v0

    .line 21388
    iget-object v1, p1, LX/08a;->lastAttemptCounters:[J

    const/4 v2, 0x1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, LX/08Z;->startRealtimeMs:J

    sub-long/2addr v4, v6

    aput-wide v4, v1, v2

    .line 21389
    iget-object v1, p1, LX/08a;->lastAttemptCounters:[J

    const/4 v2, 0x3

    iget-wide v4, p0, LX/08Z;->accumulatedRunNs:J

    div-long/2addr v4, v8

    aput-wide v4, v1, v2

    .line 21390
    iget-object v1, p1, LX/08a;->lastAttemptCounters:[J

    const/4 v2, 0x2

    iget-wide v4, p0, LX/08Z;->accumulatedYieldNs:J

    div-long/2addr v4, v8

    aput-wide v4, v1, v2

    .line 21391
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 21392
    iget-object v1, p1, LX/08a;->counters:[J

    aget-wide v2, v1, v0

    iget-object v4, p1, LX/08a;->lastAttemptCounters:[J

    aget-wide v4, v4, v0

    add-long/2addr v2, v4

    aput-wide v2, v1, v0

    .line 21393
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 21394
    :cond_0
    return-void
.end method


# virtual methods
.method public final checkShouldStop()V
    .locals 2

    .prologue
    .line 21383
    iget-object v0, p0, LX/08Z;->mRegenStampFile:Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-static {v0}, LX/08B;->fileno(Ljava/io/FileDescriptor;)I

    move-result v0

    invoke-static {v0}, Lcom/facebook/common/dextricks/DalvikInternals;->getOpenFileLinkCount(I)I

    move-result v0

    .line 21384
    if-nez v0, :cond_0

    .line 21385
    new-instance v0, LX/08c;

    const-string v1, "obsolete optimization: regeneration pending"

    invoke-direct {v0, v1}, LX/08c;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21386
    :cond_0
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 21380
    iget-object v0, p0, LX/08Z;->mOptLockFile:LX/02V;

    invoke-static {v0}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 21381
    iget-object v0, p0, LX/08Z;->mRegenStampFile:Ljava/io/FileInputStream;

    invoke-static {v0}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 21382
    return-void
.end method

.method public final copeWithOptimizationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 21371
    :try_start_0
    iget-object v0, p0, LX/08Z;->this$0:LX/02U;

    iget-object v0, v0, LX/02U;->mLockFile:LX/02V;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/02V;->acquire(I)LX/02W;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    const/4 v1, 0x0

    .line 21372
    :try_start_1
    invoke-virtual {p0}, LX/08Z;->checkShouldStop()V

    .line 21373
    invoke-virtual {p0, p1}, LX/08Z;->copeWithOptimizationFailureImpl(Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 21374
    if-eqz v2, :cond_0

    :try_start_2
    invoke-virtual {v2}, LX/02W;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 21375
    :cond_0
    :goto_0
    return-void

    .line 21376
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 21377
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v2}, LX/02W;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    :cond_1
    :goto_2
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    .line 21378
    const-string v1, "recording optimization failure itself failed"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 21379
    :catch_2
    move-exception v2

    :try_start_6
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, LX/02W;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final copeWithOptimizationFailureImpl(Ljava/lang/Throwable;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 21349
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, LX/08Z;->this$0:LX/02U;

    iget-object v0, v0, LX/02U;->root:Ljava/io/File;

    const-string v1, "optimization_log"

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 21350
    invoke-static {v2}, LX/08a;->readOrMakeDefault(Ljava/io/File;)LX/08a;

    move-result-object v3

    .line 21351
    const-string v0, "optimization failed (%s failures already)"

    new-array v1, v8, [Ljava/lang/Object;

    iget v4, v3, LX/08a;->nrOptimizationsFailed:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v7

    invoke-static {p1, v0, v1}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21352
    iget-object v0, p0, LX/08Z;->this$0:LX/02U;

    invoke-static {v0}, LX/02U;->readStatusLocked(LX/02U;)J

    move-result-wide v0

    .line 21353
    const-wide/16 v4, 0xf

    and-long/2addr v4, v0

    long-to-int v4, v4

    int-to-byte v4, v4

    .line 21354
    invoke-direct {p0, v3}, LX/08Z;->updateOptimizationLogCounters(LX/08a;)V

    .line 21355
    iget v5, v3, LX/08a;->nrOptimizationsFailed:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v3, LX/08a;->nrOptimizationsFailed:I

    .line 21356
    invoke-static {p1}, LX/23D;->b(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, LX/08a;->lastFailureExceptionJson:Ljava/lang/String;

    .line 21357
    iget v5, v3, LX/08a;->nrOptimizationsFailed:I

    iget-object v6, p0, LX/08Z;->config:LX/08F;

    iget v6, v6, LX/08F;->maximumOptimizationAttempts:I

    if-lt v5, v6, :cond_0

    .line 21358
    const-string v0, "too many optimization failures (threshold is %s): will not keep trying"

    new-array v1, v8, [Ljava/lang/Object;

    iget-object v5, p0, LX/08Z;->config:LX/08F;

    iget v5, v5, LX/08F;->maximumOptimizationAttempts:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v7

    invoke-static {v0, v1}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21359
    const/16 v0, 0x8

    if-ne v4, v0, :cond_1

    .line 21360
    const/4 v0, 0x7

    .line 21361
    :goto_0
    move v0, v0

    .line 21362
    int-to-long v0, v0

    .line 21363
    iget v4, v3, LX/08a;->flags:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v3, LX/08a;->flags:I

    .line 21364
    :cond_0
    iget-object v4, p0, LX/08Z;->this$0:LX/02U;

    invoke-static {v4, v0, v1}, LX/02U;->writeTxFailedStatusLocked(LX/02U;J)V

    .line 21365
    invoke-virtual {v3, v2}, LX/08a;->write(Ljava/io/File;)V

    .line 21366
    iget-object v2, p0, LX/08Z;->this$0:LX/02U;

    invoke-virtual {v2, v0, v1}, LX/02U;->writeStatusLocked(J)V

    .line 21367
    return-void

    .line 21368
    :cond_1
    const/4 v0, 0x3

    if-ne v4, v0, :cond_2

    .line 21369
    const/4 v0, 0x4

    goto :goto_0

    .line 21370
    :cond_2
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public final noteOptimizationSuccess()V
    .locals 8

    .prologue
    .line 21335
    iget-object v0, p0, LX/08Z;->this$0:LX/02U;

    iget-object v0, v0, LX/02U;->mLockFile:LX/02V;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/02V;->acquire(I)LX/02W;

    move-result-object v2

    const/4 v1, 0x0

    .line 21336
    :try_start_0
    invoke-virtual {p0}, LX/08Z;->checkShouldStop()V

    .line 21337
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, LX/08Z;->this$0:LX/02U;

    iget-object v3, v3, LX/02U;->root:Ljava/io/File;

    const-string v4, "optimization_log"

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 21338
    invoke-static {v0}, LX/08a;->readOrMakeDefault(Ljava/io/File;)LX/08a;

    move-result-object v3

    .line 21339
    iget v4, v3, LX/08a;->flags:I

    or-int/lit8 v4, v4, 0x3

    iput v4, v3, LX/08a;->flags:I

    .line 21340
    invoke-direct {p0, v3}, LX/08Z;->updateOptimizationLogCounters(LX/08a;)V

    .line 21341
    iget-object v4, p0, LX/08Z;->this$0:LX/02U;

    invoke-static {v4}, LX/02U;->readStatusLocked(LX/02U;)J

    move-result-wide v4

    .line 21342
    iget-object v6, p0, LX/08Z;->this$0:LX/02U;

    invoke-static {v6, v4, v5}, LX/02U;->writeTxFailedStatusLocked(LX/02U;J)V

    .line 21343
    invoke-virtual {v3, v0}, LX/08a;->write(Ljava/io/File;)V

    .line 21344
    iget-object v0, p0, LX/08Z;->this$0:LX/02U;

    invoke-virtual {v0, v4, v5}, LX/02U;->writeStatusLocked(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 21345
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/02W;->close()V

    .line 21346
    :cond_0
    return-void

    .line 21347
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 21348
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/02W;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/02W;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final waitForAndManageProcess(Lcom/facebook/forker/Process;)I
    .locals 20

    .prologue
    .line 21294
    const/high16 v7, -0x80000000

    .line 21295
    const/4 v6, 0x0

    .line 21296
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 21297
    const-wide/32 v4, 0xf4240

    div-long v4, v2, v4

    move-wide v8, v4

    move v10, v6

    move v11, v7

    move-wide v6, v4

    move-wide v4, v2

    .line 21298
    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, LX/08Z;->configProvider:LX/08G;

    invoke-virtual {v12}, LX/08G;->getInstantaneous()LX/08F;

    move-result-object v14

    .line 21299
    sub-long v16, v8, v6

    .line 21300
    if-nez v10, :cond_4

    .line 21301
    move-object/from16 v0, p0

    iget-wide v12, v0, LX/08Z;->accumulatedRunNs:J

    sub-long v2, v4, v2

    add-long/2addr v2, v12

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/08Z;->accumulatedRunNs:J

    .line 21302
    iget v2, v14, LX/08F;->optTimeSliceMs:I

    int-to-long v12, v2

    .line 21303
    cmp-long v2, v16, v12

    if-ltz v2, :cond_9

    .line 21304
    iget v2, v14, LX/08F;->yieldTimeSliceMs:I

    if-lez v2, :cond_a

    .line 21305
    const-string v2, "beginning yield"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 21306
    invoke-direct/range {p0 .. p1}, LX/08Z;->stopProcess(Lcom/facebook/forker/Process;)I

    move-result v2

    .line 21307
    const v3, -0x7fffffff

    if-ne v2, v3, :cond_0

    .line 21308
    const/high16 v2, -0x80000000

    .line 21309
    :cond_0
    const/4 v10, 0x1

    :goto_1
    move v11, v2

    move-wide v2, v8

    move-wide v8, v12

    .line 21310
    :goto_2
    const/high16 v6, -0x80000000

    if-ne v11, v6, :cond_3

    .line 21311
    iget v6, v14, LX/08F;->processPollMs:I

    .line 21312
    sub-long v12, v8, v16

    int-to-long v14, v6

    cmp-long v7, v12, v14

    if-gez v7, :cond_1

    .line 21313
    sub-long v6, v8, v16

    long-to-int v6, v6

    .line 21314
    :cond_1
    if-gez v6, :cond_2

    .line 21315
    const/4 v6, 0x0

    .line 21316
    :cond_2
    const/4 v7, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Lcom/facebook/forker/Process;->waitFor(II)I

    move-result v11

    .line 21317
    :cond_3
    invoke-virtual/range {p0 .. p0}, LX/08Z;->checkShouldStop()V

    .line 21318
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 21319
    const-wide/32 v6, 0xf4240

    div-long v6, v8, v6

    .line 21320
    const/high16 v12, -0x80000000

    if-eq v11, v12, :cond_7

    .line 21321
    const-string v2, "process exited with status %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 21322
    return v11

    .line 21323
    :cond_4
    const/4 v12, 0x1

    if-ne v10, v12, :cond_6

    .line 21324
    move-object/from16 v0, p0

    iget-wide v12, v0, LX/08Z;->accumulatedYieldNs:J

    sub-long v2, v4, v2

    add-long/2addr v2, v12

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/08Z;->accumulatedYieldNs:J

    .line 21325
    iget v2, v14, LX/08F;->yieldTimeSliceMs:I

    int-to-long v12, v2

    .line 21326
    cmp-long v2, v16, v12

    if-ltz v2, :cond_9

    .line 21327
    iget v2, v14, LX/08F;->optTimeSliceMs:I

    if-lez v2, :cond_8

    .line 21328
    const-string v2, "ending yield"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 21329
    invoke-direct/range {p0 .. p1}, LX/08Z;->resumeProcess(Lcom/facebook/forker/Process;)I

    move-result v2

    .line 21330
    const v3, -0x7ffffffe

    if-ne v2, v3, :cond_5

    .line 21331
    const/high16 v2, -0x80000000

    .line 21332
    :cond_5
    const/4 v10, 0x0

    :goto_3
    move v11, v2

    move-wide v2, v8

    move-wide v8, v12

    .line 21333
    goto :goto_2

    .line 21334
    :cond_6
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_7
    move-wide/from16 v18, v4

    move-wide v4, v8

    move-wide v8, v6

    move-wide v6, v2

    move-wide/from16 v2, v18

    goto/16 :goto_0

    :cond_8
    move v2, v11

    goto :goto_3

    :cond_9
    move-wide v8, v12

    move-wide v2, v6

    goto :goto_2

    :cond_a
    move v2, v11

    goto/16 :goto_1
.end method
