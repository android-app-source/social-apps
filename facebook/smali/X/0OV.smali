.class public final LX/0OV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0OQ;


# instance fields
.field public final a:Ljava/io/File;

.field public final b:LX/0K7;

.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0OT;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/TreeSet",
            "<",
            "LX/0OT;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "LX/0K5;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:J


# direct methods
.method public constructor <init>(Ljava/io/File;LX/0K7;)V
    .locals 2

    .prologue
    .line 53235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53236
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0OV;->f:J

    .line 53237
    iput-object p1, p0, LX/0OV;->a:Ljava/io/File;

    .line 53238
    iput-object p2, p0, LX/0OV;->b:LX/0K7;

    .line 53239
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0OV;->c:Ljava/util/HashMap;

    .line 53240
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0OV;->d:Ljava/util/HashMap;

    .line 53241
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0OV;->e:Ljava/util/HashMap;

    .line 53242
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    .line 53243
    new-instance v1, Lcom/google/android/exoplayer/upstream/cache/SimpleCache$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/exoplayer/upstream/cache/SimpleCache$1;-><init>(LX/0OV;Landroid/os/ConditionVariable;)V

    invoke-virtual {v1}, Lcom/google/android/exoplayer/upstream/cache/SimpleCache$1;->start()V

    .line 53244
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 53245
    return-void
.end method

.method private declared-synchronized c(LX/0OT;)LX/0OT;
    .locals 10

    .prologue
    .line 53246
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/0OV;->d(LX/0OV;LX/0OT;)LX/0OT;

    move-result-object v2

    .line 53247
    iget-boolean v0, v2, LX/0OT;->d:Z

    if-eqz v0, :cond_1

    .line 53248
    iget-object v0, p0, LX/0OV;->d:Ljava/util/HashMap;

    iget-object v1, v2, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    .line 53249
    invoke-virtual {v0, v2}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0Av;->b(Z)V

    .line 53250
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 53251
    iget-object v3, v2, LX/0OT;->e:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    iget-object v4, v2, LX/0OT;->a:Ljava/lang/String;

    iget-wide v5, v2, LX/0OT;->b:J

    invoke-static/range {v3 .. v8}, LX/0OT;->a(Ljava/io/File;Ljava/lang/String;JJ)Ljava/io/File;

    move-result-object v9

    .line 53252
    iget-object v3, v2, LX/0OT;->e:Ljava/io/File;

    invoke-virtual {v3, v9}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 53253
    iget-object v4, v2, LX/0OT;->a:Ljava/lang/String;

    iget-wide v5, v2, LX/0OT;->b:J

    invoke-static/range {v4 .. v9}, LX/0OT;->a(Ljava/lang/String;JJLjava/io/File;)LX/0OT;

    move-result-object v3

    move-object v1, v3

    .line 53254
    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 53255
    iget-object v0, p0, LX/0OV;->e:Ljava/util/HashMap;

    iget-object v3, v2, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 53256
    if-eqz v0, :cond_0

    .line 53257
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move v4, v3

    :goto_0
    if-ltz v4, :cond_0

    .line 53258
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0K5;

    invoke-interface {v3, p0, v2, v1}, LX/0K5;->a(LX/0OQ;LX/0OT;LX/0OT;)V

    .line 53259
    add-int/lit8 v3, v4, -0x1

    move v4, v3

    goto :goto_0

    .line 53260
    :cond_0
    iget-object v0, p0, LX/0OV;->b:LX/0K7;

    invoke-interface {v0, p0, v2, v1}, LX/0K5;->a(LX/0OQ;LX/0OT;LX/0OT;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53261
    move-object v0, v1

    .line 53262
    :goto_1
    monitor-exit p0

    return-object v0

    .line 53263
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0OV;->c:Ljava/util/HashMap;

    iget-object v1, p1, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 53264
    iget-object v0, p0, LX/0OV;->c:Ljava/util/HashMap;

    iget-object v1, p1, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    .line 53265
    goto :goto_1

    .line 53266
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 53267
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static d(LX/0OV;LX/0OT;)LX/0OT;
    .locals 10

    .prologue
    .line 53268
    iget-object v2, p1, LX/0OT;->a:Ljava/lang/String;

    .line 53269
    iget-wide v4, p1, LX/0OT;->b:J

    .line 53270
    iget-object v0, p0, LX/0OV;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    .line 53271
    if-nez v0, :cond_1

    .line 53272
    iget-wide v0, p1, LX/0OT;->b:J

    invoke-static {v2, v0, v1}, LX/0OT;->b(Ljava/lang/String;J)LX/0OT;

    move-result-object v1

    .line 53273
    :cond_0
    :goto_0
    return-object v1

    .line 53274
    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0OT;

    .line 53275
    if-eqz v1, :cond_2

    iget-wide v6, v1, LX/0OT;->b:J

    cmp-long v3, v6, v4

    if-gtz v3, :cond_2

    iget-wide v6, v1, LX/0OT;->b:J

    iget-wide v8, v1, LX/0OT;->c:J

    add-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    .line 53276
    iget-object v0, v1, LX/0OT;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53277
    invoke-direct {p0}, LX/0OV;->d()V

    .line 53278
    invoke-static {p0, p1}, LX/0OV;->d(LX/0OV;LX/0OT;)LX/0OT;

    move-result-object v1

    goto :goto_0

    .line 53279
    :cond_2
    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0OT;

    .line 53280
    if-nez v0, :cond_3

    iget-wide v0, p1, LX/0OT;->b:J

    invoke-static {v2, v0, v1}, LX/0OT;->b(Ljava/lang/String;J)LX/0OT;

    move-result-object v1

    goto :goto_0

    :cond_3
    iget-wide v4, p1, LX/0OT;->b:J

    iget-wide v0, v0, LX/0OT;->b:J

    iget-wide v6, p1, LX/0OT;->b:J

    sub-long/2addr v0, v6

    invoke-static {v2, v4, v5, v0, v1}, LX/0OT;->a(Ljava/lang/String;JJ)LX/0OT;

    move-result-object v1

    goto :goto_0
.end method

.method private d()V
    .locals 8

    .prologue
    .line 53281
    iget-object v0, p0, LX/0OV;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 53282
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 53283
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 53284
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 53285
    const/4 v0, 0x1

    move v1, v0

    .line 53286
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 53287
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0OT;

    .line 53288
    iget-object v4, v0, LX/0OT;->e:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 53289
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 53290
    iget-boolean v4, v0, LX/0OT;->d:Z

    if-eqz v4, :cond_1

    .line 53291
    iget-wide v4, p0, LX/0OV;->f:J

    iget-wide v6, v0, LX/0OT;->c:J

    sub-long/2addr v4, v6

    iput-wide v4, p0, LX/0OV;->f:J

    .line 53292
    :cond_1
    invoke-direct {p0, v0}, LX/0OV;->f(LX/0OT;)V

    goto :goto_1

    .line 53293
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    .line 53294
    goto :goto_1

    .line 53295
    :cond_3
    if-eqz v1, :cond_0

    .line 53296
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 53297
    :cond_4
    return-void
.end method

.method public static e(LX/0OV;LX/0OT;)V
    .locals 4

    .prologue
    .line 53298
    iget-object v0, p0, LX/0OV;->d:Ljava/util/HashMap;

    iget-object v1, p1, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    .line 53299
    if-nez v0, :cond_0

    .line 53300
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 53301
    iget-object v1, p0, LX/0OV;->d:Ljava/util/HashMap;

    iget-object v2, p1, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53302
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 53303
    iget-wide v0, p0, LX/0OV;->f:J

    iget-wide v2, p1, LX/0OT;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0OV;->f:J

    .line 53304
    iget-object v0, p0, LX/0OV;->e:Ljava/util/HashMap;

    iget-object v1, p1, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 53305
    if-eqz v0, :cond_1

    .line 53306
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_0
    if-ltz v2, :cond_1

    .line 53307
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0K5;

    invoke-interface {v1, p0, p1}, LX/0K5;->a(LX/0OQ;LX/0OT;)V

    .line 53308
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_0

    .line 53309
    :cond_1
    iget-object v0, p0, LX/0OV;->b:LX/0K7;

    invoke-interface {v0, p0, p1}, LX/0K5;->a(LX/0OQ;LX/0OT;)V

    .line 53310
    return-void
.end method

.method private f(LX/0OT;)V
    .locals 3

    .prologue
    .line 53311
    iget-object v0, p0, LX/0OV;->e:Ljava/util/HashMap;

    iget-object v1, p1, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 53312
    if-eqz v0, :cond_0

    .line 53313
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_0
    if-ltz v2, :cond_0

    .line 53314
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0K5;

    invoke-interface {v1, p1}, LX/0K5;->a(LX/0OT;)V

    .line 53315
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_0

    .line 53316
    :cond_0
    iget-object v0, p0, LX/0OV;->b:LX/0K7;

    invoke-interface {v0, p1}, LX/0K5;->a(LX/0OT;)V

    .line 53317
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;J)LX/0OT;
    .locals 2

    .prologue
    .line 53227
    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2, p3}, LX/0OT;->a(Ljava/lang/String;J)LX/0OT;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0OV;->c(LX/0OT;)LX/0OT;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;JJ)Ljava/io/File;
    .locals 6

    .prologue
    .line 53228
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0OV;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 53229
    iget-object v0, p0, LX/0OV;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53230
    invoke-direct {p0}, LX/0OV;->d()V

    .line 53231
    iget-object v0, p0, LX/0OV;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 53232
    :cond_0
    iget-object v0, p0, LX/0OV;->b:LX/0K7;

    invoke-interface {v0, p0, p4, p5}, LX/0K7;->a(LX/0OQ;J)V

    .line 53233
    iget-object v0, p0, LX/0OV;->a:Ljava/io/File;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object v1, p1

    move-wide v2, p2

    invoke-static/range {v0 .. v5}, LX/0OT;->a(Ljava/io/File;Ljava/lang/String;JJ)Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 53234
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Ljava/util/NavigableSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/NavigableSet",
            "<",
            "LX/0OT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53224
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0OV;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53225
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/SortedSet;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0

    .line 53226
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;LX/0K5;)Ljava/util/NavigableSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0K5;",
            ")",
            "Ljava/util/NavigableSet",
            "<",
            "LX/0OT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53217
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0OV;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 53218
    if-nez v0, :cond_0

    .line 53219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 53220
    iget-object v1, p0, LX/0OV;->e:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53221
    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53222
    invoke-virtual {p0, p1}, LX/0OV;->a(Ljava/lang/String;)Ljava/util/NavigableSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 53223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53216
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, LX/0OV;->d:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0OT;)V
    .locals 2

    .prologue
    .line 53212
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0OV;->c:Ljava/util/HashMap;

    iget-object v1, p1, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 53213
    const v0, -0xd169951

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53214
    monitor-exit p0

    return-void

    .line 53215
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/io/File;)V
    .locals 6

    .prologue
    .line 53200
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0OT;->a(Ljava/io/File;)LX/0OT;

    move-result-object v1

    .line 53201
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 53202
    iget-object v0, p0, LX/0OV;->c:Ljava/util/HashMap;

    iget-object v2, v1, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 53203
    invoke-virtual {p1}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 53204
    :goto_1
    monitor-exit p0

    return-void

    .line 53205
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 53206
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 53207
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    .line 53208
    invoke-virtual {p1}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 53209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 53210
    :cond_2
    :try_start_2
    invoke-static {p0, v1}, LX/0OV;->e(LX/0OV;LX/0OT;)V

    .line 53211
    const v0, -0x33a11444

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized b()J
    .locals 2

    .prologue
    .line 53199
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/0OV;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/0OT;)V
    .locals 6

    .prologue
    .line 53190
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0OV;->d:Ljava/util/HashMap;

    iget-object v1, p1, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    .line 53191
    iget-wide v2, p0, LX/0OV;->f:J

    iget-wide v4, p1, LX/0OT;->c:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/0OV;->f:J

    .line 53192
    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0Av;->b(Z)V

    .line 53193
    iget-object v1, p1, LX/0OT;->e:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 53194
    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53195
    iget-object v0, p0, LX/0OV;->d:Ljava/util/HashMap;

    iget-object v1, p1, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53196
    :cond_0
    invoke-direct {p0, p1}, LX/0OV;->f(LX/0OT;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53197
    monitor-exit p0

    return-void

    .line 53198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;LX/0K5;)V
    .locals 1

    .prologue
    .line 53183
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0OV;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 53184
    if-eqz v0, :cond_0

    .line 53185
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 53186
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53187
    iget-object v0, p0, LX/0OV;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53188
    :cond_0
    monitor-exit p0

    return-void

    .line 53189
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
