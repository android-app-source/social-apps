.class public final LX/00Q;
.super LX/00B;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2345
    invoke-direct {p0}, LX/00B;-><init>()V

    return-void
.end method


# virtual methods
.method public final translate(Ljava/lang/Throwable;Ljava/util/Map;)Ljava/lang/Throwable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Throwable;"
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 2346
    move-object v0, p1

    move v1, v2

    .line 2347
    :goto_0
    if-ne v1, v2, :cond_0

    if-eqz v0, :cond_0

    .line 2348
    invoke-static {v0}, LX/0Ft;->a(Ljava/lang/Throwable;)I

    move-result v1

    .line 2349
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0

    .line 2350
    :cond_0
    if-eq v1, v2, :cond_2

    .line 2351
    invoke-static {v1}, LX/0Ft;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 2352
    const-string v1, "EIO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "EROFS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2353
    :cond_1
    new-instance v0, LX/0Fp;

    invoke-direct {v0}, LX/0Fp;-><init>()V

    invoke-static {v0, p1}, LX/00B;->staplePreviousException(Ljava/lang/Throwable;Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    .line 2354
    :cond_2
    :goto_1
    return-object p1

    .line 2355
    :cond_3
    const-string v1, "ENOSPC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2356
    new-instance v0, LX/0Fq;

    invoke-direct {v0}, LX/0Fq;-><init>()V

    invoke-static {v0, p1}, LX/00B;->staplePreviousException(Ljava/lang/Throwable;Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    goto :goto_1
.end method
