.class public LX/0If;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 38794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38795
    const-string v0, ""

    iput-object v0, p0, LX/0If;->a:Ljava/lang/String;

    .line 38796
    const-string v0, ""

    iput-object v0, p0, LX/0If;->b:Ljava/lang/String;

    .line 38797
    const-string v0, ""

    iput-object v0, p0, LX/0If;->c:Ljava/lang/String;

    .line 38798
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/0If;->d:Ljava/lang/Long;

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0If;
    .locals 4

    .prologue
    .line 38805
    new-instance v0, LX/0If;

    invoke-direct {v0}, LX/0If;-><init>()V

    .line 38806
    if-nez p0, :cond_0

    .line 38807
    :goto_0
    return-object v0

    .line 38808
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 38809
    const-string v2, "app_id"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/0If;->a:Ljava/lang/String;

    .line 38810
    const-string v2, "pkg_name"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/0If;->b:Ljava/lang/String;

    .line 38811
    const-string v2, "token"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/0If;->c:Ljava/lang/String;

    .line 38812
    const-string v2, "time"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, LX/0If;->d:Ljava/lang/Long;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 38799
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 38800
    const-string v1, "app_id"

    iget-object v2, p0, LX/0If;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 38801
    const-string v1, "pkg_name"

    iget-object v2, p0, LX/0If;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 38802
    const-string v1, "token"

    iget-object v2, p0, LX/0If;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 38803
    const-string v1, "time"

    iget-object v2, p0, LX/0If;->d:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 38804
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
