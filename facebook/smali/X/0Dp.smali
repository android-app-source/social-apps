.class public abstract LX/0Dp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Landroid/view/View;

.field private d:I

.field public e:LX/0Cl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31135
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 31155
    iget-object v0, p0, LX/0Dp;->c:Landroid/view/View;

    if-nez v0, :cond_1

    .line 31156
    :cond_0
    :goto_0
    return-void

    .line 31157
    :cond_1
    iget-object v0, p0, LX/0Dp;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 31158
    iput-boolean v2, p0, LX/0Dp;->a:Z

    .line 31159
    iput-boolean v2, p0, LX/0Dp;->b:Z

    .line 31160
    iget-object v0, p0, LX/0Dp;->e:LX/0Cl;

    if-eqz v0, :cond_0

    .line 31161
    iget-object v0, p0, LX/0Dp;->e:LX/0Cl;

    invoke-interface {v0}, LX/0Cl;->a()V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31144
    iget v0, p0, LX/0Dp;->d:I

    const/16 v1, 0x32

    if-le v0, v1, :cond_0

    iget-boolean v0, p0, LX/0Dp;->a:Z

    if-eqz v0, :cond_0

    .line 31145
    iput-boolean v2, p0, LX/0Dp;->a:Z

    .line 31146
    invoke-virtual {p0, v2}, LX/0Dp;->a(Z)V

    .line 31147
    iput v2, p0, LX/0Dp;->d:I

    .line 31148
    :cond_0
    iget v0, p0, LX/0Dp;->d:I

    const/16 v1, -0x32

    if-ge v0, v1, :cond_1

    iget-boolean v0, p0, LX/0Dp;->a:Z

    if-nez v0, :cond_1

    .line 31149
    iput-boolean v3, p0, LX/0Dp;->a:Z

    .line 31150
    invoke-virtual {p0, v3}, LX/0Dp;->a(Z)V

    .line 31151
    iput v2, p0, LX/0Dp;->d:I

    .line 31152
    :cond_1
    iget-boolean v0, p0, LX/0Dp;->a:Z

    if-eqz v0, :cond_2

    if-gtz p1, :cond_3

    :cond_2
    iget-boolean v0, p0, LX/0Dp;->a:Z

    if-nez v0, :cond_4

    if-gez p1, :cond_4

    .line 31153
    :cond_3
    iget v0, p0, LX/0Dp;->d:I

    add-int/2addr v0, p1

    iput v0, p0, LX/0Dp;->d:I

    .line 31154
    :cond_4
    return-void
.end method

.method public abstract a(Z)V
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 31138
    iget-object v0, p0, LX/0Dp;->c:Landroid/view/View;

    if-nez v0, :cond_1

    .line 31139
    :cond_0
    :goto_0
    return-void

    .line 31140
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Dp;->b:Z

    .line 31141
    iget-object v0, p0, LX/0Dp;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 31142
    iget-object v0, p0, LX/0Dp;->e:LX/0Cl;

    if-eqz v0, :cond_0

    .line 31143
    iget-object v0, p0, LX/0Dp;->e:LX/0Cl;

    invoke-interface {v0}, LX/0Cl;->b()V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 31136
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Dp;->b:Z

    .line 31137
    return-void
.end method
