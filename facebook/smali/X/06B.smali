.class public LX/06B;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field private final b:LX/06C;

.field private final c:LX/06D;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/06C;)V
    .locals 4

    .prologue
    .line 17338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17339
    iput-object p2, p0, LX/06B;->a:Ljava/util/concurrent/ExecutorService;

    .line 17340
    iput-object p3, p0, LX/06B;->b:LX/06C;

    .line 17341
    new-instance v0, LX/06D;

    const/16 v1, 0xa

    sget-object v2, LX/01p;->a:LX/01q;

    invoke-static {p1, v2}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "/settings/mqtt/address"

    invoke-direct {v0, v1, v2, v3}, LX/06D;-><init>(ILandroid/content/SharedPreferences;Ljava/lang/String;)V

    iput-object v0, p0, LX/06B;->c:LX/06D;

    .line 17342
    return-void
.end method

.method private static declared-synchronized a(LX/06B;Ljava/lang/String;)Ljava/util/concurrent/Future;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/Future",
            "<",
            "LX/07D;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 17323
    monitor-enter p0

    .line 17324
    :try_start_0
    const-string v0, "AddressResolver"

    const-string v1, "resolveAsync scheduled"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17325
    iget-object v0, p0, LX/06B;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, LX/07A;

    invoke-direct {v1, p0, p1}, LX/07A;-><init>(LX/06B;Ljava/lang/String;)V

    const v2, 0x17586c3c

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    move-object v1, v0

    .line 17326
    iget-object v0, p0, LX/06B;->c:LX/06D;

    invoke-virtual {v0}, LX/06D;->a()Ljava/util/TreeSet;

    move-result-object v0

    .line 17327
    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 17328
    :goto_0
    monitor-exit p0

    return-object v0

    .line 17329
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/07D;

    .line 17330
    iget-object v2, v0, LX/07D;->a:Ljava/lang/String;

    move-object v2, v2

    .line 17331
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    .line 17332
    goto :goto_0

    .line 17333
    :cond_1
    iget v2, v0, LX/07D;->c:I

    move v2, v2

    .line 17334
    const/4 v3, 0x3

    if-le v2, v3, :cond_2

    move-object v0, v1

    .line 17335
    goto :goto_0

    .line 17336
    :cond_2
    new-instance v1, LX/07C;

    invoke-direct {v1, v0}, LX/07C;-><init>(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0

    .line 17337
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static c(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 17319
    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    move-object v0, v0
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 17320
    return-object v0

    .line 17321
    :catch_0
    new-instance v0, LX/0Hj;

    sget-object v1, LX/0Hi;->UnknownHost:LX/0Hi;

    invoke-direct {v0, v1}, LX/0Hj;-><init>(LX/0Hi;)V

    throw v0

    .line 17322
    :catch_1
    new-instance v0, LX/0Hj;

    sget-object v1, LX/0Hi;->SecurityException:LX/0Hi;

    invoke-direct {v0, v1}, LX/0Hj;-><init>(LX/0Hi;)V

    throw v0
.end method

.method private static declared-synchronized d(LX/06B;LX/07D;)V
    .locals 7

    .prologue
    .line 17302
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06B;->c:LX/06D;

    invoke-virtual {v0}, LX/06D;->a()Ljava/util/TreeSet;

    move-result-object v0

    .line 17303
    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .line 17304
    :goto_0
    iget-object v1, p0, LX/06B;->c:LX/06D;

    invoke-virtual {v1, p1}, LX/06D;->b(LX/07D;)LX/07D;

    move-result-object v1

    .line 17305
    if-nez v1, :cond_1

    .line 17306
    iget-object v1, p0, LX/06B;->c:LX/06D;

    new-instance v2, LX/07D;

    .line 17307
    iget-object v3, p1, LX/07D;->a:Ljava/lang/String;

    move-object v3, v3

    .line 17308
    invoke-virtual {p1}, LX/07D;->e()Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, LX/07D;-><init>(Ljava/lang/String;Ljava/util/List;I)V

    invoke-virtual {v1, v2}, LX/06D;->a(LX/07D;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17309
    :goto_1
    monitor-exit p0

    return-void

    .line 17310
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/07D;

    .line 17311
    iget v1, v0, LX/07D;->b:I

    move v0, v1

    .line 17312
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 17313
    :cond_1
    iget-object v2, p0, LX/06B;->c:LX/06D;

    new-instance v3, LX/07D;

    .line 17314
    iget-object v4, p1, LX/07D;->a:Ljava/lang/String;

    move-object v4, v4

    .line 17315
    invoke-virtual {p1}, LX/07D;->e()Ljava/util/List;

    move-result-object v5

    .line 17316
    iget v6, v1, LX/07D;->c:I

    move v6, v6

    .line 17317
    invoke-direct {v3, v4, v5, v0, v6}, LX/07D;-><init>(Ljava/lang/String;Ljava/util/List;II)V

    invoke-virtual {v2, v1, v3}, LX/06D;->a(LX/07D;LX/07D;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 17318
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)LX/07D;
    .locals 4
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 17295
    :try_start_0
    invoke-static {p0, p1}, LX/06B;->a(LX/06B;Ljava/lang/String;)Ljava/util/concurrent/Future;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v2, 0x42793aa

    invoke-static {v0, p2, p3, v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/07D;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    return-object v0

    .line 17296
    :catch_0
    new-instance v0, LX/0Hj;

    sget-object v1, LX/0Hi;->ExecutionException:LX/0Hi;

    invoke-direct {v0, v1}, LX/0Hj;-><init>(LX/0Hi;)V

    throw v0

    .line 17297
    :catch_1
    move-exception v0

    .line 17298
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, LX/0Hj;

    if-eqz v1, :cond_0

    .line 17299
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/0Hj;

    throw v0

    .line 17300
    :cond_0
    new-instance v0, LX/0Hj;

    sget-object v1, LX/0Hi;->ExecutionException:LX/0Hi;

    invoke-direct {v0, v1}, LX/0Hj;-><init>(LX/0Hi;)V

    throw v0

    .line 17301
    :catch_2
    new-instance v0, LX/0Hj;

    sget-object v1, LX/0Hi;->TimedOut:LX/0Hi;

    invoke-direct {v0, v1}, LX/0Hj;-><init>(LX/0Hi;)V

    throw v0
.end method

.method public final declared-synchronized a()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 17261
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17262
    const-string v0, "Cache{"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17263
    iget-object v0, p0, LX/06B;->c:LX/06D;

    invoke-virtual {v0}, LX/06D;->a()Ljava/util/TreeSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/07D;

    .line 17264
    invoke-virtual {v0}, LX/07D;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17265
    const/16 v0, 0x2c

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 17266
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 17267
    :cond_0
    :try_start_1
    const-string v0, "}\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17268
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a(LX/07D;)V
    .locals 7
    .param p1    # LX/07D;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 17283
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06B;->c:LX/06D;

    invoke-virtual {v0, p1}, LX/06D;->b(LX/07D;)LX/07D;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 17284
    if-nez v0, :cond_0

    .line 17285
    :goto_0
    monitor-exit p0

    return-void

    .line 17286
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/06B;->c:LX/06D;

    new-instance v2, LX/07D;

    .line 17287
    iget-object v3, v0, LX/07D;->a:Ljava/lang/String;

    move-object v3, v3

    .line 17288
    invoke-virtual {v0}, LX/07D;->e()Ljava/util/List;

    move-result-object v4

    .line 17289
    iget v5, v0, LX/07D;->b:I

    move v5, v5

    .line 17290
    add-int/lit8 v5, v5, -0xa

    .line 17291
    iget v6, v0, LX/07D;->c:I

    move v6, v6

    .line 17292
    add-int/lit8 v6, v6, 0x1

    invoke-direct {v2, v3, v4, v5, v6}, LX/07D;-><init>(Ljava/lang/String;Ljava/util/List;II)V

    invoke-virtual {v1, v0, v2}, LX/06D;->a(LX/07D;LX/07D;)V

    .line 17293
    iget-object v0, p0, LX/06B;->c:LX/06D;

    invoke-virtual {v0}, LX/06D;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 17294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/07D;)V
    .locals 7
    .param p1    # LX/07D;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 17273
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06B;->c:LX/06D;

    invoke-virtual {v0, p1}, LX/06D;->b(LX/07D;)LX/07D;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 17274
    if-nez v0, :cond_0

    .line 17275
    :goto_0
    monitor-exit p0

    return-void

    .line 17276
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/06B;->c:LX/06D;

    new-instance v2, LX/07D;

    .line 17277
    iget-object v3, v0, LX/07D;->a:Ljava/lang/String;

    move-object v3, v3

    .line 17278
    invoke-virtual {v0}, LX/07D;->e()Ljava/util/List;

    move-result-object v4

    .line 17279
    iget v5, v0, LX/07D;->b:I

    move v5, v5

    .line 17280
    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, LX/07D;-><init>(Ljava/lang/String;Ljava/util/List;II)V

    invoke-virtual {v1, v0, v2}, LX/06D;->a(LX/07D;LX/07D;)V

    .line 17281
    iget-object v0, p0, LX/06B;->c:LX/06D;

    invoke-virtual {v0}, LX/06D;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 17282
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(LX/07D;)V
    .locals 1
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 17269
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/06B;->d(LX/06B;LX/07D;)V

    .line 17270
    iget-object v0, p0, LX/06B;->c:LX/06D;

    invoke-virtual {v0}, LX/06D;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17271
    monitor-exit p0

    return-void

    .line 17272
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
