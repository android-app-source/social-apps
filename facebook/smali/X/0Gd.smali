.class public LX/0Gd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0G6;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Ge;

.field private final c:LX/0GB;

.field private final d:Ljava/lang/String;

.field private final e:Landroid/net/Uri;

.field private final f:Z

.field private g:LX/0O5;

.field private h:Landroid/net/Uri;

.field private i:[B

.field private j:I

.field private k:Z

.field private final l:Z

.field private final m:LX/0Gg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36143
    const-class v0, LX/0Gd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0Gd;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;LX/0Ge;LX/0GB;ZZLX/0Gg;)V
    .locals 1

    .prologue
    .line 36132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36133
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Gd;->k:Z

    .line 36134
    invoke-static {p7}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36135
    iput-object p1, p0, LX/0Gd;->d:Ljava/lang/String;

    .line 36136
    iput-object p2, p0, LX/0Gd;->e:Landroid/net/Uri;

    .line 36137
    iput-object p3, p0, LX/0Gd;->b:LX/0Ge;

    .line 36138
    iput-object p4, p0, LX/0Gd;->c:LX/0GB;

    .line 36139
    iput-boolean p5, p0, LX/0Gd;->f:Z

    .line 36140
    iput-boolean p6, p0, LX/0Gd;->l:Z

    .line 36141
    iput-object p7, p0, LX/0Gd;->m:LX/0Gg;

    .line 36142
    return-void
.end method

.method private b(LX/0OA;)LX/0OA;
    .locals 12

    .prologue
    .line 36127
    iget-object v0, p0, LX/0Gd;->e:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 36128
    :goto_0
    return-object p1

    .line 36129
    :cond_0
    iget-object v0, p0, LX/0Gd;->e:Landroid/net/Uri;

    iget-object v1, p1, LX/0OA;->a:Landroid/net/Uri;

    iget-object v2, p0, LX/0Gd;->d:Ljava/lang/String;

    iget-boolean v3, p0, LX/0Gd;->f:Z

    invoke-static {v0, v1, v2, v3}, LX/0Gj;->a(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v2

    .line 36130
    new-instance v1, LX/0OA;

    iget-object v3, p1, LX/0OA;->b:[B

    iget-wide v4, p1, LX/0OA;->c:J

    iget-wide v6, p1, LX/0OA;->d:J

    iget-wide v8, p1, LX/0OA;->e:J

    iget-object v10, p1, LX/0OA;->f:Ljava/lang/String;

    iget v11, p1, LX/0OA;->g:I

    invoke-direct/range {v1 .. v11}, LX/0OA;-><init>(Landroid/net/Uri;[BJJJLjava/lang/String;I)V

    move-object p1, v1

    .line 36131
    goto :goto_0
.end method


# virtual methods
.method public final a([BII)I
    .locals 3

    .prologue
    .line 36110
    :try_start_0
    iget-object v0, p0, LX/0Gd;->g:LX/0O5;

    if-eqz v0, :cond_1

    .line 36111
    iget-object v0, p0, LX/0Gd;->g:LX/0O5;

    invoke-virtual {v0, p1, p2, p3}, LX/0O5;->a([BII)I

    move-result v0

    .line 36112
    iget-object v1, p0, LX/0Gd;->m:LX/0Gg;

    invoke-virtual {v1, v0}, LX/0Gg;->a(I)V

    .line 36113
    :cond_0
    :goto_0
    return v0

    .line 36114
    :cond_1
    iget-object v0, p0, LX/0Gd;->b:LX/0Ge;

    invoke-interface {v0, p1, p2, p3}, LX/0Ge;->a([BII)I

    move-result v0

    .line 36115
    if-lez v0, :cond_2

    iget-boolean v1, p0, LX/0Gd;->l:Z

    if-nez v1, :cond_2

    .line 36116
    iget v1, p0, LX/0Gd;->j:I

    sget v2, LX/0GB;->a:I

    sub-int/2addr v2, v0

    if-le v1, v2, :cond_3

    .line 36117
    const v1, 0x7fffffff

    iput v1, p0, LX/0Gd;->j:I

    .line 36118
    :cond_2
    :goto_1
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 36119
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/0Gd;->k:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 36120
    :catch_0
    move-exception v0

    .line 36121
    iget-object v1, p0, LX/0Gd;->m:LX/0Gg;

    invoke-virtual {v1, v0}, LX/0Gg;->a(Ljava/io/IOException;)V

    .line 36122
    throw v0

    .line 36123
    :cond_3
    :try_start_1
    iget v1, p0, LX/0Gd;->j:I

    add-int/2addr v1, v0

    iget-object v2, p0, LX/0Gd;->i:[B

    array-length v2, v2

    if-le v1, v2, :cond_4

    .line 36124
    iget-object v1, p0, LX/0Gd;->i:[B

    iget-object v2, p0, LX/0Gd;->i:[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iput-object v1, p0, LX/0Gd;->i:[B

    .line 36125
    :cond_4
    iget-object v1, p0, LX/0Gd;->i:[B

    iget v2, p0, LX/0Gd;->j:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 36126
    iget v1, p0, LX/0Gd;->j:I

    add-int/2addr v1, v0

    iput v1, p0, LX/0Gd;->j:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public final a(LX/0OA;)J
    .locals 6

    .prologue
    .line 36091
    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    iput-object v0, p0, LX/0Gd;->h:Landroid/net/Uri;

    .line 36092
    :try_start_0
    iget-object v0, p0, LX/0Gd;->c:LX/0GB;

    iget-object v1, p0, LX/0Gd;->d:Ljava/lang/String;

    iget-object v2, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/0GB;->a(Ljava/lang/String;Landroid/net/Uri;)[B

    move-result-object v0

    .line 36093
    if-eqz v0, :cond_0

    .line 36094
    iget-object v1, p0, LX/0Gd;->m:LX/0Gg;

    iget-object v2, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/0AC;->CACHED:LX/0AC;

    invoke-virtual {v1, v2, v3}, LX/0Gg;->a(Ljava/lang/String;LX/0AC;)V

    .line 36095
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, LX/0OA;->a:Landroid/net/Uri;

    aput-object v5, v3, v4

    .line 36096
    new-instance v1, LX/0O5;

    invoke-direct {v1, v0}, LX/0O5;-><init>([B)V

    iput-object v1, p0, LX/0Gd;->g:LX/0O5;

    .line 36097
    iget-object v0, p0, LX/0Gd;->m:LX/0Gg;

    invoke-virtual {v0}, LX/0Gg;->b()V

    .line 36098
    iget-object v0, p0, LX/0Gd;->g:LX/0O5;

    invoke-virtual {v0, p1}, LX/0O5;->a(LX/0OA;)J

    move-result-wide v0

    .line 36099
    :goto_0
    return-wide v0

    .line 36100
    :cond_0
    iget-object v0, p0, LX/0Gd;->m:LX/0Gg;

    iget-object v1, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0AC;->NOT_CACHED:LX/0AC;

    invoke-virtual {v0, v1, v2}, LX/0Gg;->a(Ljava/lang/String;LX/0AC;)V

    .line 36101
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, LX/0OA;->a:Landroid/net/Uri;

    aput-object v4, v2, v3

    .line 36102
    iget-boolean v0, p0, LX/0Gd;->l:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, LX/0Gd;->i:[B

    .line 36103
    const/4 v0, 0x0

    iput v0, p0, LX/0Gd;->j:I

    .line 36104
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Gd;->k:Z

    .line 36105
    iget-object v0, p0, LX/0Gd;->b:LX/0Ge;

    invoke-direct {p0, p1}, LX/0Gd;->b(LX/0OA;)LX/0OA;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Ge;->a(LX/0OA;)J

    move-result-wide v0

    goto :goto_0

    .line 36106
    :cond_1
    const/high16 v0, 0x10000

    new-array v0, v0, [B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 36107
    :catch_0
    move-exception v0

    .line 36108
    iget-object v1, p0, LX/0Gd;->m:LX/0Gg;

    invoke-virtual {v1, v0}, LX/0Gg;->a(Ljava/io/IOException;)V

    .line 36109
    throw v0
.end method

.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 36079
    iget-object v0, p0, LX/0Gd;->g:LX/0O5;

    if-eqz v0, :cond_0

    .line 36080
    iget-object v0, p0, LX/0Gd;->g:LX/0O5;

    invoke-virtual {v0}, LX/0O5;->a()V

    .line 36081
    iput-object v5, p0, LX/0Gd;->g:LX/0O5;

    .line 36082
    iget-object v0, p0, LX/0Gd;->m:LX/0Gg;

    invoke-virtual {v0}, LX/0Gg;->c()V

    .line 36083
    :goto_0
    iput-object v5, p0, LX/0Gd;->h:Landroid/net/Uri;

    .line 36084
    iput-object v5, p0, LX/0Gd;->i:[B

    .line 36085
    iput v6, p0, LX/0Gd;->j:I

    .line 36086
    iput-boolean v6, p0, LX/0Gd;->k:Z

    .line 36087
    return-void

    .line 36088
    :cond_0
    iget-object v0, p0, LX/0Gd;->i:[B

    if-eqz v0, :cond_1

    iget v0, p0, LX/0Gd;->j:I

    if-lez v0, :cond_1

    iget-boolean v0, p0, LX/0Gd;->k:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/0Gd;->j:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_1

    .line 36089
    iget-object v0, p0, LX/0Gd;->c:LX/0GB;

    iget-object v1, p0, LX/0Gd;->d:Ljava/lang/String;

    iget-object v2, p0, LX/0Gd;->h:Landroid/net/Uri;

    iget-object v3, p0, LX/0Gd;->i:[B

    iget v4, p0, LX/0Gd;->j:I

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0GB;->a(Ljava/lang/String;Landroid/net/Uri;[BI)V

    .line 36090
    :cond_1
    iget-object v0, p0, LX/0Gd;->b:LX/0Ge;

    invoke-interface {v0}, LX/0Ge;->a()V

    goto :goto_0
.end method
