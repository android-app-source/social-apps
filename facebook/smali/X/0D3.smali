.class public LX/0D3;
.super Landroid/webkit/WebChromeClient;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/webkit/WebChromeClient$CustomViewCallback;

.field private c:Landroid/widget/VideoView;

.field private d:LX/0D5;

.field private e:Lcom/facebook/browser/lite/BrowserLiteFragment;

.field private f:Landroid/widget/FrameLayout;

.field private g:Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;

.field private h:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

.field private i:I

.field private j:LX/0Dj;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Landroid/webkit/ValueCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Landroid/webkit/ValueCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/webkit/ValueCallback",
            "<[",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29289
    const-class v0, LX/0D3;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0D3;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0D5;Lcom/facebook/browser/lite/BrowserLiteFragment;Ljava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 29332
    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    .line 29333
    iput v1, p0, LX/0D3;->i:I

    .line 29334
    iput-object p1, p0, LX/0D3;->d:LX/0D5;

    .line 29335
    iput-object p2, p0, LX/0D3;->e:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 29336
    const v0, 0x7f0d07c7

    invoke-direct {p0, v0}, LX/0D3;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/0D3;->f:Landroid/widget/FrameLayout;

    .line 29337
    const v0, 0x7f0d04de

    invoke-direct {p0, v0}, LX/0D3;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;

    iput-object v0, p0, LX/0D3;->g:Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;

    .line 29338
    iget-object v0, p0, LX/0D3;->g:Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;

    if-nez v0, :cond_2

    .line 29339
    if-eqz p3, :cond_1

    const-string v0, "THEME_INSTANT_EXPERIENCE"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29340
    const v0, 0x7f0d07b8

    invoke-direct {p0, v0}, LX/0D3;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 29341
    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;

    iput-object v0, p0, LX/0D3;->g:Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;

    .line 29342
    :goto_1
    iget-object v0, p0, LX/0D3;->g:Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;->setProgress(I)V

    .line 29343
    const v0, 0x7f0d07af

    invoke-direct {p0, v0}, LX/0D3;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    iput-object v0, p0, LX/0D3;->h:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    .line 29344
    iget-object v0, p0, LX/0D3;->h:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setProgress(I)V

    .line 29345
    sget v0, LX/0Dj;->a:I

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 29346
    if-eqz v0, :cond_0

    .line 29347
    invoke-static {}, LX/0Dj;->a()LX/0Dj;

    move-result-object v0

    iput-object v0, p0, LX/0D3;->j:LX/0Dj;

    .line 29348
    :cond_0
    return-void

    .line 29349
    :cond_1
    const v0, 0x7f0d07c5

    invoke-direct {p0, v0}, LX/0D3;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    goto :goto_0

    .line 29350
    :cond_2
    iget-object v0, p0, LX/0D3;->g:Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;->setVisibility(I)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 29331
    iget-object v0, p0, LX/0D3;->e:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 29323
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 29324
    if-eqz p1, :cond_0

    .line 29325
    iget-object v0, p0, LX/0D3;->e:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 29326
    :goto_0
    return-void

    .line 29327
    :cond_0
    iget-object v0, p0, LX/0D3;->e:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    goto :goto_0

    .line 29328
    :cond_1
    if-eqz p1, :cond_2

    .line 29329
    iget-object v0, p0, LX/0D3;->e:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0

    .line 29330
    :cond_2
    iget-object v0, p0, LX/0D3;->e:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 29308
    iget-object v0, p0, LX/0D3;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 29309
    :goto_0
    return-void

    .line 29310
    :cond_0
    iget-object v0, p0, LX/0D3;->j:LX/0Dj;

    if-eqz v0, :cond_1

    .line 29311
    iget-object v0, p0, LX/0D3;->j:LX/0Dj;

    invoke-virtual {v0}, LX/0Dj;->d()V

    .line 29312
    :cond_1
    iget-object v0, p0, LX/0D3;->c:Landroid/widget/VideoView;

    if-eqz v0, :cond_2

    .line 29313
    iget-object v0, p0, LX/0D3;->c:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 29314
    iput-object v2, p0, LX/0D3;->c:Landroid/widget/VideoView;

    .line 29315
    :cond_2
    iget-object v0, p0, LX/0D3;->b:Landroid/webkit/WebChromeClient$CustomViewCallback;

    if-eqz v0, :cond_3

    .line 29316
    :try_start_0
    iget-object v0, p0, LX/0D3;->b:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-interface {v0}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 29317
    :goto_1
    iput-object v2, p0, LX/0D3;->b:Landroid/webkit/WebChromeClient$CustomViewCallback;

    .line 29318
    :cond_3
    iget-object v0, p0, LX/0D3;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 29319
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/0D3;->a(Z)V

    .line 29320
    :try_start_1
    iget-object v0, p0, LX/0D3;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 29321
    :catch_0
    :try_start_2
    iget-object v0, p0, LX/0D3;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 29322
    :catch_1
    goto :goto_0

    :catch_2
    goto :goto_1
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 29307
    iget-object v0, p0, LX/0D3;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 29302
    iget-object v0, p0, LX/0D3;->j:LX/0Dj;

    if-eqz v0, :cond_0

    .line 29303
    iget-object v0, p0, LX/0D3;->j:LX/0Dj;

    invoke-virtual {v0}, LX/0Dj;->d()V

    .line 29304
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-gt v0, v1, :cond_1

    .line 29305
    invoke-virtual {p0}, LX/0D3;->onHideCustomView()V

    .line 29306
    :cond_1
    return-void
.end method

.method public final a(IILandroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 29290
    iget-object v0, p0, LX/0D3;->k:Landroid/webkit/ValueCallback;

    if-eqz v0, :cond_1

    if-ne p1, v2, :cond_1

    .line 29291
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 29292
    :goto_0
    iget-object v3, p0, LX/0D3;->k:Landroid/webkit/ValueCallback;

    invoke-interface {v3, v0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 29293
    iput-object v1, p0, LX/0D3;->k:Landroid/webkit/ValueCallback;

    move v0, v2

    .line 29294
    :goto_1
    return v0

    :cond_0
    move-object v0, v1

    .line 29295
    goto :goto_0

    .line 29296
    :cond_1
    iget-object v0, p0, LX/0D3;->l:Landroid/webkit/ValueCallback;

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 29297
    invoke-static {p2, p3}, Landroid/webkit/WebChromeClient$FileChooserParams;->parseResult(ILandroid/content/Intent;)[Landroid/net/Uri;

    move-result-object v0

    .line 29298
    iget-object v3, p0, LX/0D3;->l:Landroid/webkit/ValueCallback;

    invoke-interface {v3, v0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 29299
    iput-object v1, p0, LX/0D3;->l:Landroid/webkit/ValueCallback;

    move v0, v2

    .line 29300
    goto :goto_1

    .line 29301
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 29286
    iget-object v0, p0, LX/0D3;->j:LX/0Dj;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0D3;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 29287
    iget-object v0, p0, LX/0D3;->j:LX/0Dj;

    invoke-virtual {v0}, LX/0Dj;->c()V

    .line 29288
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 29283
    iget-object v0, p0, LX/0D3;->g:Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;

    iget v1, p0, LX/0D3;->i:I

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;->setProgress(I)V

    .line 29284
    iget-object v0, p0, LX/0D3;->h:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    iget v1, p0, LX/0D3;->i:I

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setProgress(I)V

    .line 29285
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 29279
    invoke-direct {p0}, LX/0D3;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29280
    invoke-virtual {p0}, LX/0D3;->onHideCustomView()V

    .line 29281
    const/4 v0, 0x1

    .line 29282
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCloseWindow(Landroid/webkit/WebView;)V
    .locals 1

    .prologue
    .line 29351
    iget-object v0, p0, LX/0D3;->e:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Landroid/webkit/WebView;)V

    .line 29352
    return-void
.end method

.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 29216
    :try_start_0
    invoke-virtual {p0}, LX/0D3;->onHideCustomView()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 29217
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final onConsoleMessage(Landroid/webkit/ConsoleMessage;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 29220
    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->message()Ljava/lang/String;

    move-result-object v0

    .line 29221
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29222
    :goto_0
    return v3

    .line 29223
    :cond_0
    sget-object v1, LX/0D2;->a:[I

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->messageLevel()Landroid/webkit/ConsoleMessage$MessageLevel;

    move-result-object v2

    invoke-virtual {v2}, Landroid/webkit/ConsoleMessage$MessageLevel;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 29224
    :pswitch_0
    iget-object v1, p0, LX/0D3;->d:LX/0D5;

    invoke-virtual {v1, v0}, LX/0D5;->b(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateWindow(Landroid/webkit/WebView;ZZLandroid/os/Message;)Z
    .locals 1

    .prologue
    .line 29225
    iget-object v0, p0, LX/0D3;->e:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0, p1, p3, p4}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Landroid/webkit/WebView;ZLandroid/os/Message;)Z

    move-result v0

    return v0
.end method

.method public final onError(Landroid/media/MediaPlayer;II)Z
    .locals 1

    .prologue
    .line 29226
    :try_start_0
    invoke-virtual {p0}, LX/0D3;->onHideCustomView()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 29227
    :goto_0
    const/4 v0, 0x0

    return v0

    :catch_0
    goto :goto_0
.end method

.method public final onHideCustomView()V
    .locals 1

    .prologue
    .line 29218
    :try_start_0
    invoke-direct {p0}, LX/0D3;->e()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 29219
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 2

    .prologue
    .line 29228
    iput p2, p0, LX/0D3;->i:I

    .line 29229
    invoke-virtual {p1}, Landroid/webkit/WebView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 29230
    :goto_0
    return-void

    .line 29231
    :cond_0
    iget-object v0, p0, LX/0D3;->g:Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;

    invoke-virtual {v0, p2}, Lcom/facebook/browser/lite/widget/BrowserLiteProgressBar;->setProgress(I)V

    .line 29232
    iget-object v0, p0, LX/0D3;->d:LX/0D5;

    .line 29233
    iget-object v1, v0, LX/0D5;->c:LX/0Dh;

    .line 29234
    iget-boolean p1, v1, LX/0Dh;->b:Z

    if-eqz p1, :cond_1

    .line 29235
    iget-object p1, v1, LX/0Dh;->a:LX/0D5;

    const-string v0, "void((function() {try {  if (!window.performance || !window.performance.timing || !document || !document.body       || document.body.scrollHeight <= 0 || !document.body.children ||       document.body.children.length < 1) {    return;  }  var nvtiming__fb_t = window.performance.timing;  if (nvtiming__fb_t.responseEnd > 0) {    console.log(\'FBNavResponseEnd:\'+nvtiming__fb_t.responseEnd);  }  if (nvtiming__fb_t.domContentLoadedEventStart > 0) {    console.log(\'FBNavDomContentLoaded:\'+nvtiming__fb_t.domContentLoadedEventStart);  }  if (nvtiming__fb_t.loadEventEnd > 0) {    console.log(\'FBNavLoadEventEnd:\'+nvtiming__fb_t.loadEventEnd);  }  var nvtiming__fb_html = document.getElementsByTagName(\'html\')[0];  if (nvtiming__fb_html) {    var nvtiming__fb_html_amp = nvtiming__fb_html.hasAttribute(\'amp\') ||        nvtiming__fb_html.hasAttribute(\"\\u26A1\");    console.log(\'FBNavAmpDetect:\'+nvtiming__fb_html_amp);  }}catch(err){  console.log(\'fb_navigation_timing_error:\'+err.message);}})());"

    invoke-virtual {p1, v0}, LX/0D5;->a(Ljava/lang/String;)V

    .line 29236
    :cond_1
    iget-object v0, p0, LX/0D3;->h:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    iget v1, p0, LX/0D3;->i:I

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setProgress(I)V

    goto :goto_0
.end method

.method public final onReceivedTitle(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 29237
    if-eqz p2, :cond_0

    const-string v0, "about:blank"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    .line 29238
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ge v0, v2, :cond_1

    move-object v0, p1

    .line 29239
    check-cast v0, LX/0D5;

    .line 29240
    iput-object v1, v0, LX/0D5;->g:Ljava/lang/String;

    .line 29241
    :cond_1
    invoke-virtual {p1}, Landroid/webkit/WebView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 29242
    iget-object v0, p0, LX/0D3;->e:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Ljava/lang/String;)V

    .line 29243
    :cond_2
    return-void

    .line 29244
    :cond_3
    invoke-static {p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method public final onShowCustomView(Landroid/view/View;ILandroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 0

    .prologue
    .line 29245
    invoke-virtual {p0, p1, p3}, LX/0D3;->onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V

    .line 29246
    return-void
.end method

.method public final onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 29247
    :try_start_0
    instance-of v0, p1, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 29248
    iget-object v0, p0, LX/0D3;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 29249
    iget-object v0, p0, LX/0D3;->f:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 29250
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0D3;->a(Z)V

    .line 29251
    iput-object p2, p0, LX/0D3;->b:Landroid/webkit/WebChromeClient$CustomViewCallback;

    .line 29252
    check-cast p1, Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    .line 29253
    instance-of v1, v0, Landroid/widget/VideoView;

    if-eqz v1, :cond_0

    .line 29254
    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, LX/0D3;->c:Landroid/widget/VideoView;

    .line 29255
    iget-object v0, p0, LX/0D3;->c:Landroid/widget/VideoView;

    invoke-virtual {v0, p0}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 29256
    iget-object v0, p0, LX/0D3;->c:Landroid/widget/VideoView;

    invoke-virtual {v0, p0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 29257
    :cond_0
    iget-object v0, p0, LX/0D3;->j:LX/0Dj;

    if-eqz v0, :cond_1

    .line 29258
    iget-object v0, p0, LX/0D3;->j:LX/0Dj;

    invoke-virtual {v0}, LX/0Dj;->c()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 29259
    :cond_1
    :goto_0
    return-void

    .line 29260
    :catch_0
    move-exception v0

    .line 29261
    sget-object v1, LX/0D3;->a:Ljava/lang/String;

    const-string v2, "Failed enter fullscreen %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onShowFileChooser(Landroid/webkit/WebView;Landroid/webkit/ValueCallback;Landroid/webkit/WebChromeClient$FileChooserParams;)Z
    .locals 4
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/WebView;",
            "Landroid/webkit/ValueCallback",
            "<[",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/webkit/WebChromeClient$FileChooserParams;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 29262
    iget-object v0, p0, LX/0D3;->l:Landroid/webkit/ValueCallback;

    if-eqz v0, :cond_0

    .line 29263
    iget-object v0, p0, LX/0D3;->l:Landroid/webkit/ValueCallback;

    invoke-interface {v0, v3}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 29264
    iput-object v3, p0, LX/0D3;->l:Landroid/webkit/ValueCallback;

    .line 29265
    :cond_0
    iput-object p2, p0, LX/0D3;->l:Landroid/webkit/ValueCallback;

    .line 29266
    invoke-virtual {p3}, Landroid/webkit/WebChromeClient$FileChooserParams;->createIntent()Landroid/content/Intent;

    move-result-object v0

    .line 29267
    :try_start_0
    iget-object v1, p0, LX/0D3;->e:Lcom/facebook/browser/lite/BrowserLiteFragment;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29268
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 29269
    :catch_0
    iput-object v3, p0, LX/0D3;->l:Landroid/webkit/ValueCallback;

    .line 29270
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29271
    const-string v0, ""

    invoke-virtual {p0, p1, p2, v0}, LX/0D3;->openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;Ljava/lang/String;)V

    .line 29272
    return-void
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29273
    iput-object p1, p0, LX/0D3;->k:Landroid/webkit/ValueCallback;

    .line 29274
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 29275
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 29276
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 29277
    :try_start_0
    iget-object v1, p0, LX/0D3;->e:Lcom/facebook/browser/lite/BrowserLiteFragment;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29278
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
