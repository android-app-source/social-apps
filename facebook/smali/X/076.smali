.class public final LX/076;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/072;


# direct methods
.method public constructor <init>(LX/072;)V
    .locals 0

    .prologue
    .line 19160
    iput-object p1, p0, LX/076;->a:LX/072;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/07W;)V
    .locals 14

    .prologue
    .line 19111
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    .line 19112
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v1, v0, LX/072;->E:LX/077;

    .line 19113
    invoke-virtual {p1}, LX/07W;->e()LX/07S;

    move-result-object v4

    .line 19114
    sget-object v0, LX/0B8;->a:[I

    invoke-virtual {v4}, LX/07S;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    .line 19115
    const-string v0, "MqttClient"

    const-string v2, "receive; type=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v0, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19116
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 19117
    iget-object v0, p0, LX/076;->a:LX/072;

    invoke-static {v0}, LX/072;->i(LX/072;)J

    .line 19118
    iget-object v0, v1, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->y:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;

    invoke-direct {v2, v1, p1}, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;-><init>(LX/077;LX/07W;)V

    const v3, 0x34a2637a

    invoke-static {v0, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 19119
    :cond_1
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v1, p0, LX/076;->a:LX/072;

    iget-object v1, v1, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/072;->a(LX/072;J)J

    .line 19120
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->d:LX/05i;

    invoke-virtual {v0}, LX/05i;->e()V

    .line 19121
    if-eqz p1, :cond_9

    .line 19122
    instance-of v1, p1, LX/07X;

    if-eqz v1, :cond_9

    .line 19123
    check-cast p1, LX/07X;

    invoke-virtual {p1}, LX/07X;->a()LX/0B5;

    move-result-object v1

    iget-object v1, v1, LX/0B5;->a:Ljava/lang/String;

    invoke-static {v1}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v1

    .line 19124
    :goto_1
    move-object v1, v1

    .line 19125
    move-object v0, v1

    .line 19126
    iget-object v1, p0, LX/076;->a:LX/072;

    iget-object v1, v1, LX/072;->h:LX/05Z;

    const-string v2, "I %s%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v4}, LX/07S;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    invoke-virtual {v0}, LX/05B;->a()Z

    move-result v5

    if-eqz v5, :cond_8

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/05Z;->b(Ljava/lang/String;)V

    .line 19127
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v1, p0, LX/076;->a:LX/072;

    iget-wide v2, v1, LX/072;->x:J

    invoke-static {v0, v2, v3}, LX/072;->b(LX/072;J)J

    .line 19128
    return-void

    :pswitch_0
    move-object v0, p1

    .line 19129
    check-cast v0, LX/07X;

    .line 19130
    iget-object v5, p0, LX/076;->a:LX/072;

    iget-object v5, v5, LX/072;->m:LX/071;

    invoke-interface {v5, v0}, LX/071;->a(LX/07X;)Ljava/lang/String;

    move-result-object v5

    .line 19131
    invoke-virtual {v0}, LX/07X;->a()LX/0B5;

    move-result-object v6

    iget v6, v6, LX/0B5;->b:I

    .line 19132
    iget-object v7, v0, LX/07W;->a:LX/07R;

    move-object v7, v7

    .line 19133
    iget v7, v7, LX/07R;->c:I

    .line 19134
    iget-object v8, p0, LX/076;->a:LX/072;

    iget-object v8, v8, LX/072;->i:LX/05R;

    invoke-virtual {v0}, LX/07X;->b()[B

    move-result-object v9

    invoke-interface {v8, v5, v9}, LX/05R;->a(Ljava/lang/String;[B)Ljava/lang/Object;

    move-result-object v8

    .line 19135
    const-string v9, "MqttClient"

    const-string v10, "receive/publish; type=%s, id=%d, qos=%d, topic=%s"

    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    const/4 v12, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x3

    aput-object v5, v11, v12

    invoke-static {v9, v10, v11}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19136
    if-eqz v1, :cond_7

    .line 19137
    invoke-virtual {v0}, LX/07X;->b()[B

    move-result-object v0

    .line 19138
    const-string v9, "/send_message_response"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "/t_sm_rp"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 19139
    :cond_2
    iget-object v9, v1, LX/077;->a:LX/056;

    iget-object v9, v9, LX/056;->j:LX/05i;

    const-class v10, LX/0BF;

    invoke-virtual {v9, v10}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v9

    check-cast v9, LX/0BF;

    sget-object v10, LX/0BG;->MessageSendSuccess:LX/0BG;

    invoke-virtual {v9, v10}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 19140
    :cond_3
    const-string v9, "/push_notification"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    const-string v9, "/t_push"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 19141
    :cond_4
    iget-object v9, v1, LX/077;->a:LX/056;

    iget-object v9, v9, LX/056;->j:LX/05i;

    const-class v10, LX/0BF;

    invoke-virtual {v9, v10}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v9

    check-cast v9, LX/0BF;

    sget-object v10, LX/0BG;->FbnsNotificationReceived:LX/0BG;

    invoke-virtual {v9, v10}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 19142
    :cond_5
    const-string v9, "/fbns_msg"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 19143
    iget-object v9, v1, LX/077;->a:LX/056;

    iget-object v9, v9, LX/056;->j:LX/05i;

    const-class v10, LX/0BF;

    invoke-virtual {v9, v10}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v9

    check-cast v9, LX/0BF;

    sget-object v10, LX/0BG;->FbnsLiteNotificationReceived:LX/0BG;

    invoke-virtual {v9, v10}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 19144
    :cond_6
    iget-object v9, v1, LX/077;->a:LX/056;

    iget-object v9, v9, LX/056;->j:LX/05i;

    const-class v10, LX/0BF;

    invoke-virtual {v9, v10}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v9

    check-cast v9, LX/0BF;

    sget-object v10, LX/0BG;->PublishReceived:LX/0BG;

    invoke-virtual {v9, v10}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 19145
    iget-object v9, v1, LX/077;->a:LX/056;

    iget-object v9, v9, LX/056;->D:LX/055;

    .line 19146
    iget-object v10, v9, LX/055;->a:LX/051;

    invoke-virtual {v10, v5, v0, v2, v3}, LX/051;->a(Ljava/lang/String;[BJ)V

    .line 19147
    :cond_7
    sget-object v0, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    iget v0, v0, LX/0AL;->mValue:I

    if-ne v7, v0, :cond_0

    .line 19148
    iget-object v0, p0, LX/076;->a:LX/072;

    .line 19149
    iget-object v2, v0, LX/072;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/rti/mqtt/protocol/MqttClient$9;

    invoke-direct {v3, v0, v6, v8}, Lcom/facebook/rti/mqtt/protocol/MqttClient$9;-><init>(LX/072;ILjava/lang/Object;)V

    const v5, 0x47b90b1c

    invoke-static {v2, v3, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 19150
    goto/16 :goto_0

    :pswitch_1
    move-object v0, p1

    .line 19151
    check-cast v0, LX/0B7;

    .line 19152
    const-string v2, "MqttClient"

    const-string v3, "receive/puback; type=%s, id=%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v0}, LX/0B7;->a()LX/0B6;

    move-result-object v0

    iget v0, v0, LX/0B6;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v2, v3, v5}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19153
    if-eqz v1, :cond_0

    goto/16 :goto_0

    .line 19154
    :pswitch_2
    iget-object v0, p0, LX/076;->a:LX/072;

    .line 19155
    iget-object v2, v0, LX/072;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/rti/mqtt/protocol/MqttClient$10;

    invoke-direct {v3, v0}, Lcom/facebook/rti/mqtt/protocol/MqttClient$10;-><init>(LX/072;)V

    const v5, -0x58dfe64f

    invoke-static {v2, v3, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 19156
    goto/16 :goto_0

    .line 19157
    :cond_8
    const-string v0, ""

    goto/16 :goto_2

    .line 19158
    :cond_9
    sget-object v1, LX/06b;->a:LX/06b;

    move-object v1, v1

    .line 19159
    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/07k;)V
    .locals 14

    .prologue
    .line 19089
    iget-boolean v0, p1, LX/07k;->a:Z

    if-eqz v0, :cond_3

    .line 19090
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-boolean v0, v0, LX/072;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->e:LX/06z;

    .line 19091
    iget-object v1, v0, LX/06z;->p:Ljava/lang/String;

    move-object v0, v1

    .line 19092
    if-eqz v0, :cond_0

    .line 19093
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->j:LX/05F;

    iget-object v1, p0, LX/076;->a:LX/072;

    iget-object v1, v1, LX/072;->e:LX/06z;

    .line 19094
    iget-object v2, v1, LX/06z;->p:Ljava/lang/String;

    move-object v1, v2

    .line 19095
    invoke-interface {v0, v1}, LX/05F;->a(Ljava/lang/String;)V

    .line 19096
    :cond_0
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v1, p0, LX/076;->a:LX/072;

    iget-object v1, v1, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    .line 19097
    iput-wide v2, v0, LX/072;->u:J

    .line 19098
    :cond_1
    :goto_0
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v1, v0, LX/072;->c:LX/05h;

    iget-boolean v2, p1, LX/07k;->a:Z

    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    iget-object v0, p0, LX/076;->a:LX/072;

    iget-wide v6, v0, LX/072;->D:J

    sub-long v3, v4, v6

    iget-object v0, p1, LX/07k;->b:LX/05B;

    invoke-virtual {v0}, LX/05B;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p1, LX/07k;->b:LX/05B;

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Hs;

    invoke-virtual {v0}, LX/0Hs;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    iget-object v6, p1, LX/07k;->c:LX/05B;

    iget-object v7, p1, LX/07k;->d:LX/05B;

    iget-object v0, p0, LX/076;->a:LX/072;

    iget-wide v8, v0, LX/072;->C:J

    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->b:LX/059;

    invoke-virtual {v0}, LX/059;->h()J

    move-result-wide v10

    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v12, v0, LX/072;->B:Landroid/net/NetworkInfo;

    invoke-virtual/range {v1 .. v12}, LX/05h;->a(ZJLjava/lang/String;LX/05B;LX/05B;JJLandroid/net/NetworkInfo;)V

    .line 19099
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->E:LX/077;

    .line 19100
    if-eqz v0, :cond_2

    .line 19101
    iget-boolean v1, p1, LX/07k;->a:Z

    if-eqz v1, :cond_6

    .line 19102
    const-string v1, "FbnsConnectionManager"

    const-string v2, "connection/established"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19103
    iget-object v1, v0, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->y:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;

    invoke-direct {v2, v0, p1}, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;-><init>(LX/077;LX/07k;)V

    const v3, -0x2e5168

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 19104
    :cond_2
    :goto_2
    return-void

    .line 19105
    :cond_3
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-boolean v0, v0, LX/072;->o:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/07k;->b:LX/05B;

    invoke-virtual {v0}, LX/05B;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/07k;->b:LX/05B;

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/0Hs;->FAILED_CONNECTION_UNKNOWN_CONNECT_HASH:LX/0Hs;

    if-eq v0, v1, :cond_4

    iget-object v0, p1, LX/07k;->b:LX/05B;

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/0Hs;->FAILED_CONNECTION_REFUSED:LX/0Hs;

    if-ne v0, v1, :cond_1

    .line 19106
    :cond_4
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->j:LX/05F;

    invoke-interface {v0}, LX/05F;->e()V

    goto/16 :goto_0

    .line 19107
    :cond_5
    const/4 v5, 0x0

    goto :goto_1

    .line 19108
    :cond_6
    const-string v1, "FbnsConnectionManager"

    const-string v2, "connection/failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19109
    iget-object v1, v0, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->y:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;

    invoke-direct {v2, v0, p1}, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;-><init>(LX/077;LX/07k;)V

    const v3, -0x4d7fedd7

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 19110
    goto :goto_2
.end method

.method public final a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 19086
    iget-object v0, p0, LX/076;->a:LX/072;

    invoke-virtual {v0}, LX/072;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 19087
    iget-object v0, p0, LX/076;->a:LX/072;

    invoke-static {v0, p1, p2, p3}, LX/072;->b(LX/072;LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 19088
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 19071
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->p:Ljava/util/concurrent/atomic/AtomicReference;

    if-eqz v0, :cond_0

    .line 19072
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->p:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p0, LX/076;->a:LX/072;

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 19073
    invoke-static {p1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v2, v3

    .line 19074
    :goto_0
    move-object v1, v2

    .line 19075
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 19076
    :cond_0
    return-void

    .line 19077
    :cond_1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 19078
    iget-object v3, v1, LX/072;->e:LX/06z;

    .line 19079
    iget-object v5, v3, LX/06z;->b:LX/06k;

    move-object v3, v5

    .line 19080
    invoke-virtual {v3}, LX/06k;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    move v3, v2

    .line 19081
    :goto_1
    array-length v6, v5

    if-ge v2, v6, :cond_2

    const/16 v6, 0xa

    if-ge v2, v6, :cond_2

    .line 19082
    shl-int/lit8 v3, v3, 0x1

    aget-byte v6, v5, v2

    add-int/2addr v3, v6

    .line 19083
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 19084
    :catch_0
    move-object v2, v3

    goto :goto_0

    .line 19085
    :cond_2
    mul-int v2, v4, v3

    add-int/2addr v2, v3

    iget-wide v4, v1, LX/072;->C:J

    long-to-int v3, v4

    xor-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 19062
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v1, p0, LX/076;->a:LX/072;

    iget-object v1, v1, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    .line 19063
    iput-wide v2, v0, LX/072;->w:J

    .line 19064
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->h:LX/05Z;

    const-string v1, "O %s%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/05Z;->b(Ljava/lang/String;)V

    .line 19065
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v1, p0, LX/076;->a:LX/072;

    iget-wide v2, v1, LX/072;->w:J

    .line 19066
    iput-wide v2, v0, LX/072;->y:J

    .line 19067
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->d:LX/05i;

    invoke-virtual {v0}, LX/05i;->e()V

    .line 19068
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->d:LX/05i;

    iget-object v1, p0, LX/076;->a:LX/072;

    iget-object v1, v1, LX/072;->A:Ljava/lang/String;

    invoke-virtual {v0, p1, p2, v1, v4}, LX/05i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 19069
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->k:LX/05k;

    invoke-virtual {v0}, LX/05k;->a()V

    .line 19070
    return-void
.end method

.method public final a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 19161
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->E:LX/077;

    .line 19162
    if-eqz v0, :cond_0

    .line 19163
    invoke-virtual {v0, p2, p3, p1}, LX/077;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 19164
    :cond_0
    return-void
.end method

.method public final a(ZLjava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 19049
    if-nez p1, :cond_0

    .line 19050
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->i:LX/05R;

    const/4 v1, 0x0

    invoke-interface {v0, p3, v1, p2}, LX/05R;->a(Ljava/lang/Object;ZLjava/lang/String;)V

    .line 19051
    :goto_0
    return-void

    .line 19052
    :cond_0
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->i:LX/05R;

    const/4 v1, 0x1

    const-string v2, ""

    invoke-interface {v0, p3, v1, v2}, LX/05R;->a(Ljava/lang/Object;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public final a(LX/074;)Z
    .locals 1

    .prologue
    .line 19053
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->z:LX/074;

    invoke-virtual {v0, p1}, LX/074;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Landroid/net/NetworkInfo;
    .locals 1

    .prologue
    .line 19061
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->B:Landroid/net/NetworkInfo;

    return-object v0
.end method

.method public final b(LX/074;)V
    .locals 1

    .prologue
    .line 19054
    iget-object v0, p0, LX/076;->a:LX/072;

    .line 19055
    iput-object p1, v0, LX/072;->z:LX/074;

    .line 19056
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 19057
    iget-object v0, p0, LX/076;->a:LX/072;

    invoke-static {v0}, LX/072;->n(LX/072;)V

    .line 19058
    return-void
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 19059
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-object v0, v0, LX/072;->b:LX/059;

    invoke-virtual {v0}, LX/059;->h()J

    move-result-wide v0

    return-wide v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 19060
    iget-object v0, p0, LX/076;->a:LX/072;

    iget-wide v0, v0, LX/072;->C:J

    return-wide v0
.end method
