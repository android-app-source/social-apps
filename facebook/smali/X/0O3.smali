.class public final LX/0O3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0G7;


# instance fields
.field private final a:Landroid/content/res/AssetManager;

.field private final b:LX/04n;

.field private c:Ljava/lang/String;

.field private d:Ljava/io/InputStream;

.field private e:J

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/04n;)V
    .locals 1

    .prologue
    .line 52398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52399
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, LX/0O3;->a:Landroid/content/res/AssetManager;

    .line 52400
    iput-object p2, p0, LX/0O3;->b:LX/04n;

    .line 52401
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 52385
    iget-wide v0, p0, LX/0O3;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 52386
    const/4 v0, -0x1

    .line 52387
    :cond_0
    :goto_0
    return v0

    .line 52388
    :cond_1
    :try_start_0
    iget-wide v0, p0, LX/0O3;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    .line 52389
    :goto_1
    iget-object v0, p0, LX/0O3;->d:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 52390
    if-lez v0, :cond_0

    .line 52391
    iget-wide v2, p0, LX/0O3;->e:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 52392
    iget-wide v2, p0, LX/0O3;->e:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/0O3;->e:J

    .line 52393
    :cond_2
    iget-object v1, p0, LX/0O3;->b:LX/04n;

    if-eqz v1, :cond_0

    .line 52394
    iget-object v1, p0, LX/0O3;->b:LX/04n;

    invoke-interface {v1, v0}, LX/04n;->a(I)V

    goto :goto_0

    .line 52395
    :cond_3
    :try_start_1
    iget-wide v0, p0, LX/0O3;->e:J

    int-to-long v2, p3

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v0

    long-to-int p3, v0

    goto :goto_1

    .line 52396
    :catch_0
    move-exception v0

    .line 52397
    new-instance v1, LX/0O2;

    invoke-direct {v1, v0}, LX/0O2;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public final a(LX/0OA;)J
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x1

    .line 52363
    :try_start_0
    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0O3;->c:Ljava/lang/String;

    .line 52364
    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 52365
    const-string v1, "/android_asset/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52366
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 52367
    :cond_0
    :goto_0
    iget-object v1, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/0O3;->c:Ljava/lang/String;

    .line 52368
    iget-object v1, p0, LX/0O3;->a:Landroid/content/res/AssetManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, LX/0O3;->d:Ljava/io/InputStream;

    .line 52369
    iget-object v0, p0, LX/0O3;->d:Ljava/io/InputStream;

    iget-wide v2, p1, LX/0OA;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 52370
    iget-wide v2, p1, LX/0OA;->d:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 52371
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52372
    :catch_0
    move-exception v0

    .line 52373
    new-instance v1, LX/0O2;

    invoke-direct {v1, v0}, LX/0O2;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 52374
    :cond_1
    :try_start_1
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52375
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 52376
    :cond_2
    iget-wide v0, p1, LX/0OA;->e:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_5

    .line 52377
    iget-wide v0, p1, LX/0OA;->e:J

    iput-wide v0, p0, LX/0O3;->e:J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 52378
    :cond_3
    :goto_1
    iput-boolean v4, p0, LX/0O3;->f:Z

    .line 52379
    iget-object v0, p0, LX/0O3;->b:LX/04n;

    if-eqz v0, :cond_4

    .line 52380
    iget-object v0, p0, LX/0O3;->b:LX/04n;

    invoke-interface {v0}, LX/04n;->b()V

    .line 52381
    :cond_4
    iget-wide v0, p0, LX/0O3;->e:J

    return-wide v0

    .line 52382
    :cond_5
    :try_start_2
    iget-object v0, p0, LX/0O3;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/0O3;->e:J

    .line 52383
    iget-wide v0, p0, LX/0O3;->e:J

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 52384
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0O3;->e:J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 52347
    iput-object v2, p0, LX/0O3;->c:Ljava/lang/String;

    .line 52348
    iget-object v0, p0, LX/0O3;->d:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 52349
    :try_start_0
    iget-object v0, p0, LX/0O3;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52350
    iput-object v2, p0, LX/0O3;->d:Ljava/io/InputStream;

    .line 52351
    iget-boolean v0, p0, LX/0O3;->f:Z

    if-eqz v0, :cond_0

    .line 52352
    iput-boolean v3, p0, LX/0O3;->f:Z

    .line 52353
    iget-object v0, p0, LX/0O3;->b:LX/04n;

    if-eqz v0, :cond_0

    .line 52354
    iget-object v0, p0, LX/0O3;->b:LX/04n;

    invoke-interface {v0}, LX/04n;->c()V

    .line 52355
    :cond_0
    return-void

    .line 52356
    :catch_0
    move-exception v0

    .line 52357
    :try_start_1
    new-instance v1, LX/0O2;

    invoke-direct {v1, v0}, LX/0O2;-><init>(Ljava/io/IOException;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52358
    :catchall_0
    move-exception v0

    iput-object v2, p0, LX/0O3;->d:Ljava/io/InputStream;

    .line 52359
    iget-boolean v1, p0, LX/0O3;->f:Z

    if-eqz v1, :cond_1

    .line 52360
    iput-boolean v3, p0, LX/0O3;->f:Z

    .line 52361
    iget-object v1, p0, LX/0O3;->b:LX/04n;

    if-eqz v1, :cond_1

    .line 52362
    iget-object v1, p0, LX/0O3;->b:LX/04n;

    invoke-interface {v1}, LX/04n;->c()V

    :cond_1
    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52346
    iget-object v0, p0, LX/0O3;->c:Ljava/lang/String;

    return-object v0
.end method
