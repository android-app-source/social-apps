.class public LX/0KL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0G6;


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:LX/0G6;

.field private c:LX/0G6;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0G6;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0G6;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40677
    iput-object p1, p0, LX/0KL;->a:Ljava/lang/String;

    .line 40678
    iput-object p2, p0, LX/0KL;->b:LX/0G6;

    .line 40679
    iput-object p3, p0, LX/0KL;->d:Ljava/util/Map;

    .line 40680
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 1

    .prologue
    .line 40681
    iget-object v0, p0, LX/0KL;->c:LX/0G6;

    if-nez v0, :cond_0

    .line 40682
    const/4 v0, -0x1

    .line 40683
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0KL;->c:LX/0G6;

    invoke-interface {v0, p1, p2, p3}, LX/0G6;->a([BII)I

    move-result v0

    goto :goto_0
.end method

.method public final a(LX/0OA;)J
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 40684
    iget-object v0, p0, LX/0KL;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 40685
    iget-object v0, p0, LX/0KL;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move-object v0, v3

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 40686
    :cond_0
    iget-object v0, p0, LX/0KL;->d:Ljava/util/Map;

    iget-object v1, p1, LX/0OA;->a:Landroid/net/Uri;

    .line 40687
    const-string v2, "remote-uri"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 40688
    if-nez v2, :cond_3

    .line 40689
    :goto_1
    move-object v1, v1

    .line 40690
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 40691
    :cond_1
    if-eqz v0, :cond_2

    .line 40692
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 40693
    new-instance v1, LX/0OA;

    iget-wide v4, p1, LX/0OA;->c:J

    iget-wide v6, p1, LX/0OA;->d:J

    iget-wide v8, p1, LX/0OA;->e:J

    iget-object v10, p1, LX/0OA;->f:Ljava/lang/String;

    iget v11, p1, LX/0OA;->g:I

    invoke-direct/range {v1 .. v11}, LX/0OA;-><init>(Landroid/net/Uri;[BJJJLjava/lang/String;I)V

    .line 40694
    new-instance v0, LX/0OG;

    invoke-direct {v0}, LX/0OG;-><init>()V

    iput-object v0, p0, LX/0KL;->c:LX/0G6;

    .line 40695
    :goto_2
    iget-object v0, p0, LX/0KL;->c:LX/0G6;

    invoke-interface {v0, v1}, LX/0G6;->a(LX/0OA;)J

    move-result-wide v0

    return-wide v0

    .line 40696
    :cond_2
    iget-object v0, p0, LX/0KL;->b:LX/0G6;

    iput-object v0, p0, LX/0KL;->c:LX/0G6;

    move-object v1, p1

    goto :goto_2

    :cond_3
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 40697
    iget-object v0, p0, LX/0KL;->c:LX/0G6;

    if-nez v0, :cond_0

    .line 40698
    :goto_0
    return-void

    .line 40699
    :cond_0
    iget-object v0, p0, LX/0KL;->c:LX/0G6;

    invoke-interface {v0}, LX/0G6;->a()V

    .line 40700
    const/4 v0, 0x0

    iput-object v0, p0, LX/0KL;->c:LX/0G6;

    goto :goto_0
.end method
