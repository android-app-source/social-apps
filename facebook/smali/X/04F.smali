.class public final enum LX/04F;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/04F;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/04F;

.field public static final enum AUDIO_CHANNEL_LAYOUT:LX/04F;

.field public static final enum AVAILABLE_QUALITIES:LX/04F;

.field public static final enum AVERAGE_STALL_TIME:LX/04F;

.field public static final enum AVG_UP_TIME:LX/04F;

.field public static final enum BATTERY_LEVEL:LX/04F;

.field public static final enum BATTERY_STATE:LX/04F;

.field public static final enum BOARD:LX/04F;

.field public static final enum BRAND:LX/04F;

.field public static final enum BROADCAST_STATUS:LX/04F;

.field public static final enum BUFFER_UNDERRUN_COUNT:LX/04F;

.field public static final enum CHANNEL_ELIGIBILITY:LX/04F;

.field public static final enum CPU_TEMPERATURE:LX/04F;

.field public static final enum CURRENT_VOLUME:LX/04F;

.field public static final enum DASH_MANIFEST_AVAILABLE:LX/04F;

.field public static final enum DATA_CONNECTION_QUALITY:LX/04F;

.field public static final enum DATA_LATENCY_QUALITY:LX/04F;

.field public static final enum DEVICE:LX/04F;

.field public static final enum EXTERNAL_LOG_ID:LX/04F;

.field public static final enum EXTERNAL_LOG_TYPE:LX/04F;

.field public static final enum FB_BANDWIDTH_ESTIMATE:LX/04F;

.field public static final enum FB_LATENCY_ESTIMATE:LX/04F;

.field public static final enum FIRST_STALL_START_POSITION:LX/04F;

.field public static final enum FIRST_STALL_TIME:LX/04F;

.field public static final enum FRAME_DROP_COUNT:LX/04F;

.field public static final enum FRAME_DROP_ELAPSED_TIME:LX/04F;

.field public static final enum HEADSET_STATE:LX/04F;

.field public static final enum IS_ABR_ENABLED:LX/04F;

.field public static final enum IS_BROADCAST:LX/04F;

.field public static final enum IS_LIVE_STREAM:LX/04F;

.field public static final enum IS_LONG_CLICK:LX/04F;

.field public static final enum IS_SPHERICAL_FALLBACK:LX/04F;

.field public static final enum IS_STALLING:LX/04F;

.field public static final enum IS_WATCH_AND_SHOP:LX/04F;

.field public static final enum LAST_STALL_START_POSITION:LX/04F;

.field public static final enum LAST_STALL_TIME:LX/04F;

.field public static final enum LAST_START_POSITION_PARAM:LX/04F;

.field public static final enum MANUFACTURER:LX/04F;

.field public static final enum MAX_STALL_START_POSITION:LX/04F;

.field public static final enum MAX_STALL_TIME:LX/04F;

.field public static final enum MODEL:LX/04F;

.field public static final enum MONETIZATION_IDS:LX/04F;

.field public static final enum OLD_BROADCAST_STATUS:LX/04F;

.field public static final enum PERCENT_BUFFERED:LX/04F;

.field public static final enum PLAYER_ALLOCATED:LX/04F;

.field public static final enum PLAYER_ORIGIN:LX/04F;

.field public static final enum PLAYER_SUBORIGIN:LX/04F;

.field public static final enum PLAYER_VERSION:LX/04F;

.field public static final enum PRESELECTED_QUALITY:LX/04F;

.field public static final enum PREVIOUS_PLAYER_TYPE:LX/04F;

.field public static final enum PREVIOUS_SELECTED_QUALITY:LX/04F;

.field public static final enum PREVIOUS_VIDEO_ID:LX/04F;

.field public static final enum PRODUCT:LX/04F;

.field public static final enum PROJECTION:LX/04F;

.field public static final enum QUALITY_SELECTOR_SURFACE:LX/04F;

.field public static final enum RECENT_STALLS_DETAIL:LX/04F;

.field public static final enum RECENT_STALLS_TOTAL_COUNT:LX/04F;

.field public static final enum RECENT_STALLS_TOTAL_DURATION:LX/04F;

.field public static final enum RECENT_STALLS_TOTAL_DURATION_TRIMMED:LX/04F;

.field public static final enum RELEASE_FROM:LX/04F;

.field public static final enum REQUESTED_STATE:LX/04F;

.field public static final enum RESTART_COUNT:LX/04F;

.field public static final enum SEEK_SOURCE_POSITION_PARAM:LX/04F;

.field public static final enum SELECTED_QUALITY:LX/04F;

.field public static final enum SEQUENCE_NUMBER:LX/04F;

.field public static final enum SOURCE_INDEX:LX/04F;

.field public static final enum STACK_TRACE:LX/04F;

.field public static final enum STALL_COUNT:LX/04F;

.field public static final enum STALL_INFO_JSON:LX/04F;

.field public static final enum STALL_RECORD_TIME:LX/04F;

.field public static final enum STALL_TIME:LX/04F;

.field public static final enum STORY_POSITION:LX/04F;

.field public static final enum STREAMING_FORMAT:LX/04F;

.field public static final enum STREAM_BITRATE:LX/04F;

.field public static final enum STREAM_MIME_TYPE:LX/04F;

.field public static final enum STREAM_NEXT_REPRESENTATION_ID:LX/04F;

.field public static final enum STREAM_REPRESENTATION_EVENT_SOURCE:LX/04F;

.field public static final enum STREAM_REPRESENTATION_ID:LX/04F;

.field public static final enum STREAM_TYPE:LX/04F;

.field public static final enum STREAM_VIDEO_HEIGHT:LX/04F;

.field public static final enum STREAM_VIDEO_WIDTH:LX/04F;

.field public static final enum SUBTITLE_ERROR:LX/04F;

.field public static final enum TARGET_INDEX:LX/04F;

.field public static final enum TEXTUREVIEW_ALLOCREASON:LX/04F;

.field public static final enum TEXTURE_ALLOCATED:LX/04F;

.field public static final enum TIME_UNTIL_INTERRUPT:LX/04F;

.field public static final enum TRACKING_PARAM:LX/04F;

.field public static final enum UP_TIME:LX/04F;

.field public static final enum URL:LX/04F;

.field public static final enum VIDEOSTATE_METADATA:LX/04F;

.field public static final enum VIDEO_BANDWIDTH_ESTIMATE:LX/04F;

.field public static final enum VIDEO_CHANGE_REASON:LX/04F;

.field public static final enum VIDEO_ENCODE:LX/04F;

.field public static final enum VIDEO_EXCEPTION_TAG:LX/04F;

.field public static final enum VIDEO_ID:LX/04F;

.field public static final enum VIDEO_IS_PLAYING:LX/04F;

.field public static final enum VIDEO_IS_PLAY_COMPLETED:LX/04F;

.field public static final enum VIDEO_PLAYER_TYPE:LX/04F;

.field public static final enum VIDEO_PLAY_REASON:LX/04F;

.field public static final enum VIDEO_TIME_POSITION_PARAM:LX/04F;

.field public static final enum VR_CAST_BUTTON_DISPLAYED:LX/04F;

.field public static final enum WATCH_AND_GO_SESSION_ID:LX/04F;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 12841
    new-instance v0, LX/04F;

    const-string v1, "REQUESTED_STATE"

    const-string v2, "state"

    invoke-direct {v0, v1, v4, v2}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->REQUESTED_STATE:LX/04F;

    .line 12842
    new-instance v0, LX/04F;

    const-string v1, "PLAYER_VERSION"

    const-string v2, "player_version"

    invoke-direct {v0, v1, v5, v2}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->PLAYER_VERSION:LX/04F;

    .line 12843
    new-instance v0, LX/04F;

    const-string v1, "STALL_RECORD_TIME"

    const-string v2, "stall_record_time"

    invoke-direct {v0, v1, v6, v2}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STALL_RECORD_TIME:LX/04F;

    .line 12844
    new-instance v0, LX/04F;

    const-string v1, "UP_TIME"

    const-string v2, "up_time"

    invoke-direct {v0, v1, v7, v2}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->UP_TIME:LX/04F;

    .line 12845
    new-instance v0, LX/04F;

    const-string v1, "AVG_UP_TIME"

    const-string v2, "avg_up_time"

    invoke-direct {v0, v1, v8, v2}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->AVG_UP_TIME:LX/04F;

    .line 12846
    new-instance v0, LX/04F;

    const-string v1, "RESTART_COUNT"

    const/4 v2, 0x5

    const-string v3, "restart_count"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->RESTART_COUNT:LX/04F;

    .line 12847
    new-instance v0, LX/04F;

    const-string v1, "STALL_TIME"

    const/4 v2, 0x6

    const-string v3, "stall_time"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STALL_TIME:LX/04F;

    .line 12848
    new-instance v0, LX/04F;

    const-string v1, "STALL_COUNT"

    const/4 v2, 0x7

    const-string v3, "stall_count"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STALL_COUNT:LX/04F;

    .line 12849
    new-instance v0, LX/04F;

    const-string v1, "AVERAGE_STALL_TIME"

    const/16 v2, 0x8

    const-string v3, "average_stall_time"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->AVERAGE_STALL_TIME:LX/04F;

    .line 12850
    new-instance v0, LX/04F;

    const-string v1, "FIRST_STALL_START_POSITION"

    const/16 v2, 0x9

    const-string v3, "first_stall_start_position"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->FIRST_STALL_START_POSITION:LX/04F;

    .line 12851
    new-instance v0, LX/04F;

    const-string v1, "FIRST_STALL_TIME"

    const/16 v2, 0xa

    const-string v3, "first_stall_time"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->FIRST_STALL_TIME:LX/04F;

    .line 12852
    new-instance v0, LX/04F;

    const-string v1, "LAST_STALL_START_POSITION"

    const/16 v2, 0xb

    const-string v3, "last_stall_start_position"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->LAST_STALL_START_POSITION:LX/04F;

    .line 12853
    new-instance v0, LX/04F;

    const-string v1, "LAST_STALL_TIME"

    const/16 v2, 0xc

    const-string v3, "last_stall_time"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->LAST_STALL_TIME:LX/04F;

    .line 12854
    new-instance v0, LX/04F;

    const-string v1, "MAX_STALL_START_POSITION"

    const/16 v2, 0xd

    const-string v3, "max_stall_start_position"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->MAX_STALL_START_POSITION:LX/04F;

    .line 12855
    new-instance v0, LX/04F;

    const-string v1, "MAX_STALL_TIME"

    const/16 v2, 0xe

    const-string v3, "max_stall_time"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->MAX_STALL_TIME:LX/04F;

    .line 12856
    new-instance v0, LX/04F;

    const-string v1, "RECENT_STALLS_TOTAL_COUNT"

    const/16 v2, 0xf

    const-string v3, "recent_stalls_total_count"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->RECENT_STALLS_TOTAL_COUNT:LX/04F;

    .line 12857
    new-instance v0, LX/04F;

    const-string v1, "RECENT_STALLS_TOTAL_DURATION"

    const/16 v2, 0x10

    const-string v3, "recent_stalls_total_duration"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->RECENT_STALLS_TOTAL_DURATION:LX/04F;

    .line 12858
    new-instance v0, LX/04F;

    const-string v1, "RECENT_STALLS_TOTAL_DURATION_TRIMMED"

    const/16 v2, 0x11

    const-string v3, "recent_stalls_total_duration_trimmed"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->RECENT_STALLS_TOTAL_DURATION_TRIMMED:LX/04F;

    .line 12859
    new-instance v0, LX/04F;

    const-string v1, "RECENT_STALLS_DETAIL"

    const/16 v2, 0x12

    const-string v3, "recent_stalls_detail"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->RECENT_STALLS_DETAIL:LX/04F;

    .line 12860
    new-instance v0, LX/04F;

    const-string v1, "IS_STALLING"

    const/16 v2, 0x13

    const-string v3, "is_stalling"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->IS_STALLING:LX/04F;

    .line 12861
    new-instance v0, LX/04F;

    const-string v1, "IS_WATCH_AND_SHOP"

    const/16 v2, 0x14

    const-string v3, "is_watch_and_shop"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->IS_WATCH_AND_SHOP:LX/04F;

    .line 12862
    new-instance v0, LX/04F;

    const-string v1, "STALL_INFO_JSON"

    const/16 v2, 0x15

    const-string v3, "stall_info_json"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STALL_INFO_JSON:LX/04F;

    .line 12863
    new-instance v0, LX/04F;

    const-string v1, "VIDEO_PLAYER_TYPE"

    const/16 v2, 0x16

    const-string v3, "player"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->VIDEO_PLAYER_TYPE:LX/04F;

    .line 12864
    new-instance v0, LX/04F;

    const-string v1, "PREVIOUS_PLAYER_TYPE"

    const/16 v2, 0x17

    const-string v3, "previous_player_format"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->PREVIOUS_PLAYER_TYPE:LX/04F;

    .line 12865
    new-instance v0, LX/04F;

    const-string v1, "PRODUCT"

    const/16 v2, 0x18

    const-string v3, "product"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->PRODUCT:LX/04F;

    .line 12866
    new-instance v0, LX/04F;

    const-string v1, "DEVICE"

    const/16 v2, 0x19

    const-string v3, "device"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->DEVICE:LX/04F;

    .line 12867
    new-instance v0, LX/04F;

    const-string v1, "BOARD"

    const/16 v2, 0x1a

    const-string v3, "board"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->BOARD:LX/04F;

    .line 12868
    new-instance v0, LX/04F;

    const-string v1, "MANUFACTURER"

    const/16 v2, 0x1b

    const-string v3, "manufacturer"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->MANUFACTURER:LX/04F;

    .line 12869
    new-instance v0, LX/04F;

    const-string v1, "BRAND"

    const/16 v2, 0x1c

    const-string v3, "brand"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->BRAND:LX/04F;

    .line 12870
    new-instance v0, LX/04F;

    const-string v1, "MODEL"

    const/16 v2, 0x1d

    const-string v3, "model"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->MODEL:LX/04F;

    .line 12871
    new-instance v0, LX/04F;

    const-string v1, "STREAM_TYPE"

    const/16 v2, 0x1e

    const-string v3, "stream_type"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STREAM_TYPE:LX/04F;

    .line 12872
    new-instance v0, LX/04F;

    const-string v1, "STREAMING_FORMAT"

    const/16 v2, 0x1f

    const-string v3, "streaming_format"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STREAMING_FORMAT:LX/04F;

    .line 12873
    new-instance v0, LX/04F;

    const-string v1, "AVAILABLE_QUALITIES"

    const/16 v2, 0x20

    const-string v3, "available_qualities"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->AVAILABLE_QUALITIES:LX/04F;

    .line 12874
    new-instance v0, LX/04F;

    const-string v1, "IS_ABR_ENABLED"

    const/16 v2, 0x21

    const-string v3, "is_abr_enabled"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->IS_ABR_ENABLED:LX/04F;

    .line 12875
    new-instance v0, LX/04F;

    const-string v1, "VIDEO_ID"

    const/16 v2, 0x22

    const-string v3, "video_id"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->VIDEO_ID:LX/04F;

    .line 12876
    new-instance v0, LX/04F;

    const-string v1, "VIDEO_ENCODE"

    const/16 v2, 0x23

    const-string v3, "vencode"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->VIDEO_ENCODE:LX/04F;

    .line 12877
    new-instance v0, LX/04F;

    const-string v1, "SUBTITLE_ERROR"

    const/16 v2, 0x24

    const-string v3, "subt_err"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->SUBTITLE_ERROR:LX/04F;

    .line 12878
    new-instance v0, LX/04F;

    const-string v1, "CURRENT_VOLUME"

    const/16 v2, 0x25

    const-string v3, "current_volume"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->CURRENT_VOLUME:LX/04F;

    .line 12879
    new-instance v0, LX/04F;

    const-string v1, "URL"

    const/16 v2, 0x26

    const-string v3, "url"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->URL:LX/04F;

    .line 12880
    new-instance v0, LX/04F;

    const-string v1, "PERCENT_BUFFERED"

    const/16 v2, 0x27

    const-string v3, "percent_buffered"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->PERCENT_BUFFERED:LX/04F;

    .line 12881
    new-instance v0, LX/04F;

    const-string v1, "PLAYER_ALLOCATED"

    const/16 v2, 0x28

    const-string v3, "player_allocated"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->PLAYER_ALLOCATED:LX/04F;

    .line 12882
    new-instance v0, LX/04F;

    const-string v1, "RELEASE_FROM"

    const/16 v2, 0x29

    const-string v3, "release_from"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->RELEASE_FROM:LX/04F;

    .line 12883
    new-instance v0, LX/04F;

    const-string v1, "SOURCE_INDEX"

    const/16 v2, 0x2a

    const-string v3, "source_index"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->SOURCE_INDEX:LX/04F;

    .line 12884
    new-instance v0, LX/04F;

    const-string v1, "TARGET_INDEX"

    const/16 v2, 0x2b

    const-string v3, "target_index"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->TARGET_INDEX:LX/04F;

    .line 12885
    new-instance v0, LX/04F;

    const-string v1, "TEXTUREVIEW_ALLOCREASON"

    const/16 v2, 0x2c

    const-string v3, "alloc_reason"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->TEXTUREVIEW_ALLOCREASON:LX/04F;

    .line 12886
    new-instance v0, LX/04F;

    const-string v1, "TEXTURE_ALLOCATED"

    const/16 v2, 0x2d

    const-string v3, "tex_allocated"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->TEXTURE_ALLOCATED:LX/04F;

    .line 12887
    new-instance v0, LX/04F;

    const-string v1, "VIDEOSTATE_METADATA"

    const/16 v2, 0x2e

    const-string v3, "vid_state_metadata"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->VIDEOSTATE_METADATA:LX/04F;

    .line 12888
    new-instance v0, LX/04F;

    const-string v1, "VIDEO_EXCEPTION_TAG"

    const/16 v2, 0x2f

    const-string v3, "reason"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->VIDEO_EXCEPTION_TAG:LX/04F;

    .line 12889
    new-instance v0, LX/04F;

    const-string v1, "TRACKING_PARAM"

    const/16 v2, 0x30

    const-string v3, "tracking"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->TRACKING_PARAM:LX/04F;

    .line 12890
    new-instance v0, LX/04F;

    const-string v1, "FRAME_DROP_COUNT"

    const/16 v2, 0x31

    const-string v3, "frame_drop_count"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->FRAME_DROP_COUNT:LX/04F;

    .line 12891
    new-instance v0, LX/04F;

    const-string v1, "FRAME_DROP_ELAPSED_TIME"

    const/16 v2, 0x32

    const-string v3, "frame_drop_elapsed_time"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->FRAME_DROP_ELAPSED_TIME:LX/04F;

    .line 12892
    new-instance v0, LX/04F;

    const-string v1, "VIDEO_TIME_POSITION_PARAM"

    const/16 v2, 0x33

    const-string v3, "video_time_position"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    .line 12893
    new-instance v0, LX/04F;

    const-string v1, "SEEK_SOURCE_POSITION_PARAM"

    const/16 v2, 0x34

    const-string v3, "video_seek_source_time_position"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->SEEK_SOURCE_POSITION_PARAM:LX/04F;

    .line 12894
    new-instance v0, LX/04F;

    const-string v1, "LAST_START_POSITION_PARAM"

    const/16 v2, 0x35

    const-string v3, "video_last_start_time_position"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->LAST_START_POSITION_PARAM:LX/04F;

    .line 12895
    new-instance v0, LX/04F;

    const-string v1, "VIDEO_CHANGE_REASON"

    const/16 v2, 0x36

    const-string v3, "debug_reason"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    .line 12896
    new-instance v0, LX/04F;

    const-string v1, "PLAYER_ORIGIN"

    const/16 v2, 0x37

    const-string v3, "player_origin"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->PLAYER_ORIGIN:LX/04F;

    .line 12897
    new-instance v0, LX/04F;

    const-string v1, "PLAYER_SUBORIGIN"

    const/16 v2, 0x38

    const-string v3, "player_suborigin"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->PLAYER_SUBORIGIN:LX/04F;

    .line 12898
    new-instance v0, LX/04F;

    const-string v1, "PREVIOUS_VIDEO_ID"

    const/16 v2, 0x39

    const-string v3, "video_chaining_previous_video"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->PREVIOUS_VIDEO_ID:LX/04F;

    .line 12899
    new-instance v0, LX/04F;

    const-string v1, "VIDEO_PLAY_REASON"

    const/16 v2, 0x3a

    const-string v3, "video_play_reason"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    .line 12900
    new-instance v0, LX/04F;

    const-string v1, "IS_LONG_CLICK"

    const/16 v2, 0x3b

    const-string v3, "long_click_on_video"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->IS_LONG_CLICK:LX/04F;

    .line 12901
    new-instance v0, LX/04F;

    const-string v1, "CHANNEL_ELIGIBILITY"

    const/16 v2, 0x3c

    const-string v3, "channel_eligibility"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->CHANNEL_ELIGIBILITY:LX/04F;

    .line 12902
    new-instance v0, LX/04F;

    const-string v1, "SEQUENCE_NUMBER"

    const/16 v2, 0x3d

    const-string v3, "seq_num"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->SEQUENCE_NUMBER:LX/04F;

    .line 12903
    new-instance v0, LX/04F;

    const-string v1, "MONETIZATION_IDS"

    const/16 v2, 0x3e

    const-string v3, "monetization_ids"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->MONETIZATION_IDS:LX/04F;

    .line 12904
    new-instance v0, LX/04F;

    const-string v1, "VR_CAST_BUTTON_DISPLAYED"

    const/16 v2, 0x3f

    const-string v3, "vr_cast_button_displayed"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->VR_CAST_BUTTON_DISPLAYED:LX/04F;

    .line 12905
    new-instance v0, LX/04F;

    const-string v1, "STREAM_REPRESENTATION_ID"

    const/16 v2, 0x40

    const-string v3, "representation_id"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STREAM_REPRESENTATION_ID:LX/04F;

    .line 12906
    new-instance v0, LX/04F;

    const-string v1, "STREAM_NEXT_REPRESENTATION_ID"

    const/16 v2, 0x41

    const-string v3, "next_representation_id"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STREAM_NEXT_REPRESENTATION_ID:LX/04F;

    .line 12907
    new-instance v0, LX/04F;

    const-string v1, "STREAM_VIDEO_WIDTH"

    const/16 v2, 0x42

    const-string v3, "video_width"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STREAM_VIDEO_WIDTH:LX/04F;

    .line 12908
    new-instance v0, LX/04F;

    const-string v1, "STREAM_VIDEO_HEIGHT"

    const/16 v2, 0x43

    const-string v3, "video_height"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STREAM_VIDEO_HEIGHT:LX/04F;

    .line 12909
    new-instance v0, LX/04F;

    const-string v1, "STREAM_BITRATE"

    const/16 v2, 0x44

    const-string v3, "bitrate"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STREAM_BITRATE:LX/04F;

    .line 12910
    new-instance v0, LX/04F;

    const-string v1, "STREAM_MIME_TYPE"

    const/16 v2, 0x45

    const-string v3, "mime"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STREAM_MIME_TYPE:LX/04F;

    .line 12911
    new-instance v0, LX/04F;

    const-string v1, "STREAM_REPRESENTATION_EVENT_SOURCE"

    const/16 v2, 0x46

    const-string v3, "representation_event_source"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STREAM_REPRESENTATION_EVENT_SOURCE:LX/04F;

    .line 12912
    new-instance v0, LX/04F;

    const-string v1, "VIDEO_BANDWIDTH_ESTIMATE"

    const/16 v2, 0x47

    const-string v3, "video_bandwidth"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->VIDEO_BANDWIDTH_ESTIMATE:LX/04F;

    .line 12913
    new-instance v0, LX/04F;

    const-string v1, "VIDEO_IS_PLAYING"

    const/16 v2, 0x48

    const-string v3, "video_is_playing"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->VIDEO_IS_PLAYING:LX/04F;

    .line 12914
    new-instance v0, LX/04F;

    const-string v1, "VIDEO_IS_PLAY_COMPLETED"

    const/16 v2, 0x49

    const-string v3, "video_is_play_completed"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->VIDEO_IS_PLAY_COMPLETED:LX/04F;

    .line 12915
    new-instance v0, LX/04F;

    const-string v1, "FB_BANDWIDTH_ESTIMATE"

    const/16 v2, 0x4a

    const-string v3, "fb_bandwidth"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->FB_BANDWIDTH_ESTIMATE:LX/04F;

    .line 12916
    new-instance v0, LX/04F;

    const-string v1, "FB_LATENCY_ESTIMATE"

    const/16 v2, 0x4b

    const-string v3, "fb_latency"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->FB_LATENCY_ESTIMATE:LX/04F;

    .line 12917
    new-instance v0, LX/04F;

    const-string v1, "IS_LIVE_STREAM"

    const/16 v2, 0x4c

    const-string v3, "playback_is_live_streaming"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->IS_LIVE_STREAM:LX/04F;

    .line 12918
    new-instance v0, LX/04F;

    const-string v1, "IS_BROADCAST"

    const/16 v2, 0x4d

    const-string v3, "playback_is_broadcast"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->IS_BROADCAST:LX/04F;

    .line 12919
    new-instance v0, LX/04F;

    const-string v1, "TIME_UNTIL_INTERRUPT"

    const/16 v2, 0x4e

    const-string v3, "time_until_interrupt"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->TIME_UNTIL_INTERRUPT:LX/04F;

    .line 12920
    new-instance v0, LX/04F;

    const-string v1, "EXTERNAL_LOG_TYPE"

    const/16 v2, 0x4f

    const-string v3, "external_log_type"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->EXTERNAL_LOG_TYPE:LX/04F;

    .line 12921
    new-instance v0, LX/04F;

    const-string v1, "EXTERNAL_LOG_ID"

    const/16 v2, 0x50

    const-string v3, "external_log_id"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->EXTERNAL_LOG_ID:LX/04F;

    .line 12922
    new-instance v0, LX/04F;

    const-string v1, "PROJECTION"

    const/16 v2, 0x51

    const-string v3, "projection"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->PROJECTION:LX/04F;

    .line 12923
    new-instance v0, LX/04F;

    const-string v1, "AUDIO_CHANNEL_LAYOUT"

    const/16 v2, 0x52

    const-string v3, "audio_ch_conf"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->AUDIO_CHANNEL_LAYOUT:LX/04F;

    .line 12924
    new-instance v0, LX/04F;

    const-string v1, "DATA_CONNECTION_QUALITY"

    const/16 v2, 0x53

    const-string v3, "data_connection_quality"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->DATA_CONNECTION_QUALITY:LX/04F;

    .line 12925
    new-instance v0, LX/04F;

    const-string v1, "DATA_LATENCY_QUALITY"

    const/16 v2, 0x54

    const-string v3, "data_latency_quality"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->DATA_LATENCY_QUALITY:LX/04F;

    .line 12926
    new-instance v0, LX/04F;

    const-string v1, "BROADCAST_STATUS"

    const/16 v2, 0x55

    const-string v3, "broadcast_status"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->BROADCAST_STATUS:LX/04F;

    .line 12927
    new-instance v0, LX/04F;

    const-string v1, "OLD_BROADCAST_STATUS"

    const/16 v2, 0x56

    const-string v3, "old_broadcast_status"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->OLD_BROADCAST_STATUS:LX/04F;

    .line 12928
    new-instance v0, LX/04F;

    const-string v1, "BATTERY_LEVEL"

    const/16 v2, 0x57

    const-string v3, "battery_level"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->BATTERY_LEVEL:LX/04F;

    .line 12929
    new-instance v0, LX/04F;

    const-string v1, "BATTERY_STATE"

    const/16 v2, 0x58

    const-string v3, "battery_state"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->BATTERY_STATE:LX/04F;

    .line 12930
    new-instance v0, LX/04F;

    const-string v1, "HEADSET_STATE"

    const/16 v2, 0x59

    const-string v3, "headset_state"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->HEADSET_STATE:LX/04F;

    .line 12931
    new-instance v0, LX/04F;

    const-string v1, "CPU_TEMPERATURE"

    const/16 v2, 0x5a

    const-string v3, "cpu_temperature"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->CPU_TEMPERATURE:LX/04F;

    .line 12932
    new-instance v0, LX/04F;

    const-string v1, "DASH_MANIFEST_AVAILABLE"

    const/16 v2, 0x5b

    const-string v3, "dash_manifest_available"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->DASH_MANIFEST_AVAILABLE:LX/04F;

    .line 12933
    new-instance v0, LX/04F;

    const-string v1, "BUFFER_UNDERRUN_COUNT"

    const/16 v2, 0x5c

    const-string v3, "buffer_underrun_count"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->BUFFER_UNDERRUN_COUNT:LX/04F;

    .line 12934
    new-instance v0, LX/04F;

    const-string v1, "STORY_POSITION"

    const/16 v2, 0x5d

    const-string v3, "story_position"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STORY_POSITION:LX/04F;

    .line 12935
    new-instance v0, LX/04F;

    const-string v1, "STACK_TRACE"

    const/16 v2, 0x5e

    const-string v3, "stack_trace"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->STACK_TRACE:LX/04F;

    .line 12936
    new-instance v0, LX/04F;

    const-string v1, "IS_SPHERICAL_FALLBACK"

    const/16 v2, 0x5f

    const-string v3, "is_spherical_fallback"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->IS_SPHERICAL_FALLBACK:LX/04F;

    .line 12937
    new-instance v0, LX/04F;

    const-string v1, "SELECTED_QUALITY"

    const/16 v2, 0x60

    const-string v3, "selected_quality"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->SELECTED_QUALITY:LX/04F;

    .line 12938
    new-instance v0, LX/04F;

    const-string v1, "PREVIOUS_SELECTED_QUALITY"

    const/16 v2, 0x61

    const-string v3, "previous_selected_quality"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->PREVIOUS_SELECTED_QUALITY:LX/04F;

    .line 12939
    new-instance v0, LX/04F;

    const-string v1, "QUALITY_SELECTOR_SURFACE"

    const/16 v2, 0x62

    const-string v3, "quality_selector_surface"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->QUALITY_SELECTOR_SURFACE:LX/04F;

    .line 12940
    new-instance v0, LX/04F;

    const-string v1, "PRESELECTED_QUALITY"

    const/16 v2, 0x63

    const-string v3, "preselected_quality"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->PRESELECTED_QUALITY:LX/04F;

    .line 12941
    new-instance v0, LX/04F;

    const-string v1, "WATCH_AND_GO_SESSION_ID"

    const/16 v2, 0x64

    const-string v3, "watch_and_go_session_id"

    invoke-direct {v0, v1, v2, v3}, LX/04F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04F;->WATCH_AND_GO_SESSION_ID:LX/04F;

    .line 12942
    const/16 v0, 0x65

    new-array v0, v0, [LX/04F;

    sget-object v1, LX/04F;->REQUESTED_STATE:LX/04F;

    aput-object v1, v0, v4

    sget-object v1, LX/04F;->PLAYER_VERSION:LX/04F;

    aput-object v1, v0, v5

    sget-object v1, LX/04F;->STALL_RECORD_TIME:LX/04F;

    aput-object v1, v0, v6

    sget-object v1, LX/04F;->UP_TIME:LX/04F;

    aput-object v1, v0, v7

    sget-object v1, LX/04F;->AVG_UP_TIME:LX/04F;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/04F;->RESTART_COUNT:LX/04F;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/04F;->STALL_TIME:LX/04F;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/04F;->STALL_COUNT:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/04F;->AVERAGE_STALL_TIME:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/04F;->FIRST_STALL_START_POSITION:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/04F;->FIRST_STALL_TIME:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/04F;->LAST_STALL_START_POSITION:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/04F;->LAST_STALL_TIME:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/04F;->MAX_STALL_START_POSITION:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/04F;->MAX_STALL_TIME:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/04F;->RECENT_STALLS_TOTAL_COUNT:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/04F;->RECENT_STALLS_TOTAL_DURATION:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/04F;->RECENT_STALLS_TOTAL_DURATION_TRIMMED:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/04F;->RECENT_STALLS_DETAIL:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/04F;->IS_STALLING:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/04F;->IS_WATCH_AND_SHOP:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/04F;->STALL_INFO_JSON:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/04F;->VIDEO_PLAYER_TYPE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/04F;->PREVIOUS_PLAYER_TYPE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/04F;->PRODUCT:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/04F;->DEVICE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/04F;->BOARD:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/04F;->MANUFACTURER:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/04F;->BRAND:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/04F;->MODEL:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/04F;->STREAM_TYPE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/04F;->STREAMING_FORMAT:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/04F;->AVAILABLE_QUALITIES:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/04F;->IS_ABR_ENABLED:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/04F;->VIDEO_ID:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/04F;->VIDEO_ENCODE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/04F;->SUBTITLE_ERROR:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/04F;->CURRENT_VOLUME:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/04F;->URL:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/04F;->PERCENT_BUFFERED:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/04F;->PLAYER_ALLOCATED:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/04F;->RELEASE_FROM:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/04F;->SOURCE_INDEX:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/04F;->TARGET_INDEX:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/04F;->TEXTUREVIEW_ALLOCREASON:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/04F;->TEXTURE_ALLOCATED:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/04F;->VIDEOSTATE_METADATA:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/04F;->VIDEO_EXCEPTION_TAG:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, LX/04F;->TRACKING_PARAM:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, LX/04F;->FRAME_DROP_COUNT:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, LX/04F;->FRAME_DROP_ELAPSED_TIME:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, LX/04F;->SEEK_SOURCE_POSITION_PARAM:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, LX/04F;->LAST_START_POSITION_PARAM:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, LX/04F;->VIDEO_CHANGE_REASON:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, LX/04F;->PLAYER_ORIGIN:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, LX/04F;->PLAYER_SUBORIGIN:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, LX/04F;->PREVIOUS_VIDEO_ID:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, LX/04F;->IS_LONG_CLICK:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, LX/04F;->CHANNEL_ELIGIBILITY:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, LX/04F;->SEQUENCE_NUMBER:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, LX/04F;->MONETIZATION_IDS:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, LX/04F;->VR_CAST_BUTTON_DISPLAYED:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, LX/04F;->STREAM_REPRESENTATION_ID:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, LX/04F;->STREAM_NEXT_REPRESENTATION_ID:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, LX/04F;->STREAM_VIDEO_WIDTH:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, LX/04F;->STREAM_VIDEO_HEIGHT:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, LX/04F;->STREAM_BITRATE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, LX/04F;->STREAM_MIME_TYPE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, LX/04F;->STREAM_REPRESENTATION_EVENT_SOURCE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, LX/04F;->VIDEO_BANDWIDTH_ESTIMATE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, LX/04F;->VIDEO_IS_PLAYING:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, LX/04F;->VIDEO_IS_PLAY_COMPLETED:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, LX/04F;->FB_BANDWIDTH_ESTIMATE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, LX/04F;->FB_LATENCY_ESTIMATE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, LX/04F;->IS_LIVE_STREAM:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, LX/04F;->IS_BROADCAST:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, LX/04F;->TIME_UNTIL_INTERRUPT:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, LX/04F;->EXTERNAL_LOG_TYPE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, LX/04F;->EXTERNAL_LOG_ID:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, LX/04F;->PROJECTION:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, LX/04F;->AUDIO_CHANNEL_LAYOUT:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, LX/04F;->DATA_CONNECTION_QUALITY:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, LX/04F;->DATA_LATENCY_QUALITY:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, LX/04F;->BROADCAST_STATUS:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, LX/04F;->OLD_BROADCAST_STATUS:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, LX/04F;->BATTERY_LEVEL:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, LX/04F;->BATTERY_STATE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, LX/04F;->HEADSET_STATE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, LX/04F;->CPU_TEMPERATURE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, LX/04F;->DASH_MANIFEST_AVAILABLE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, LX/04F;->BUFFER_UNDERRUN_COUNT:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, LX/04F;->STORY_POSITION:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, LX/04F;->STACK_TRACE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, LX/04F;->IS_SPHERICAL_FALLBACK:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, LX/04F;->SELECTED_QUALITY:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, LX/04F;->PREVIOUS_SELECTED_QUALITY:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, LX/04F;->QUALITY_SELECTOR_SURFACE:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, LX/04F;->PRESELECTED_QUALITY:LX/04F;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, LX/04F;->WATCH_AND_GO_SESSION_ID:LX/04F;

    aput-object v2, v0, v1

    sput-object v0, LX/04F;->$VALUES:[LX/04F;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12838
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12839
    iput-object p3, p0, LX/04F;->value:Ljava/lang/String;

    .line 12840
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/04F;
    .locals 1

    .prologue
    .line 12943
    const-class v0, LX/04F;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/04F;

    return-object v0
.end method

.method public static values()[LX/04F;
    .locals 1

    .prologue
    .line 12837
    sget-object v0, LX/04F;->$VALUES:[LX/04F;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/04F;

    return-object v0
.end method
