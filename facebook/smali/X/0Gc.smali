.class public final LX/0Gc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Gb;


# instance fields
.field private final a:LX/0O1;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "LX/0Ga;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/os/Handler;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/0GZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:J

.field private final g:J

.field public final h:F

.field public final i:F

.field public j:I

.field private k:J

.field private l:I

.field private m:Z

.field private n:Z


# direct methods
.method public constructor <init>(LX/0O1;Landroid/os/Handler;LX/0GZ;IIFF)V
    .locals 4
    .param p2    # Landroid/os/Handler;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0GZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v2, 0x3e8

    .line 36002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36003
    iput-object p1, p0, LX/0Gc;->a:LX/0O1;

    .line 36004
    iput-object p2, p0, LX/0Gc;->d:Landroid/os/Handler;

    .line 36005
    iput-object p3, p0, LX/0Gc;->e:LX/0GZ;

    .line 36006
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0Gc;->b:Ljava/util/List;

    .line 36007
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0Gc;->c:Ljava/util/HashMap;

    .line 36008
    int-to-long v0, p4

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/0Gc;->f:J

    .line 36009
    int-to-long v0, p5

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/0Gc;->g:J

    .line 36010
    iput p6, p0, LX/0Gc;->h:F

    .line 36011
    iput p7, p0, LX/0Gc;->i:F

    .line 36012
    return-void
.end method

.method private a(JJ)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 36073
    const-wide/16 v2, -0x1

    cmp-long v1, p3, v2

    if-nez v1, :cond_1

    .line 36074
    :cond_0
    :goto_0
    return v0

    .line 36075
    :cond_1
    sub-long v2, p3, p1

    .line 36076
    iget-wide v4, p0, LX/0Gc;->g:J

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v0, p0, LX/0Gc;->f:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 36070
    iget-object v0, p0, LX/0Gc;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Gc;->e:LX/0GZ;

    if-eqz v0, :cond_0

    .line 36071
    iget-object v0, p0, LX/0Gc;->d:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/exoplayer/FBVideoLoadControl$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/exoplayer/FBVideoLoadControl$1;-><init>(LX/0Gc;Z)V

    const v2, 0x93414de

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 36072
    :cond_0
    return-void
.end method

.method private c()V
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 36042
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    .line 36043
    :goto_0
    iget-object v0, p0, LX/0Gc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 36044
    iget-object v0, p0, LX/0Gc;->c:Ljava/util/HashMap;

    iget-object v6, p0, LX/0Gc;->b:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ga;

    .line 36045
    iget-boolean v6, v0, LX/0Ga;->c:Z

    or-int/2addr v5, v6

    .line 36046
    iget-wide v8, v0, LX/0Ga;->d:J

    cmp-long v6, v8, v10

    if-eqz v6, :cond_0

    move v6, v7

    :goto_1
    or-int/2addr v4, v6

    .line 36047
    iget v0, v0, LX/0Ga;->b:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 36048
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v6, v2

    .line 36049
    goto :goto_1

    .line 36050
    :cond_1
    iget-object v0, p0, LX/0Gc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    if-nez v5, :cond_2

    if-eqz v4, :cond_7

    :cond_2
    const/4 v0, 0x2

    if-eq v3, v0, :cond_3

    if-ne v3, v7, :cond_7

    iget-boolean v0, p0, LX/0Gc;->m:Z

    if-eqz v0, :cond_7

    :cond_3
    move v0, v7

    :goto_2
    iput-boolean v0, p0, LX/0Gc;->m:Z

    .line 36051
    iget-boolean v0, p0, LX/0Gc;->m:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, LX/0Gc;->n:Z

    if-nez v0, :cond_8

    .line 36052
    sget-object v0, LX/0OO;->a:LX/0OO;

    invoke-virtual {v0, v2}, LX/0OO;->a(I)V

    .line 36053
    iput-boolean v7, p0, LX/0Gc;->n:Z

    .line 36054
    invoke-direct {p0, v7}, LX/0Gc;->a(Z)V

    .line 36055
    :cond_4
    :goto_3
    iput-wide v10, p0, LX/0Gc;->k:J

    .line 36056
    iget-boolean v0, p0, LX/0Gc;->m:Z

    if-eqz v0, :cond_9

    .line 36057
    :goto_4
    iget-object v0, p0, LX/0Gc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 36058
    iget-object v0, p0, LX/0Gc;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 36059
    iget-object v1, p0, LX/0Gc;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ga;

    .line 36060
    iget-wide v0, v0, LX/0Ga;->d:J

    .line 36061
    cmp-long v3, v0, v10

    if-eqz v3, :cond_6

    iget-wide v4, p0, LX/0Gc;->k:J

    cmp-long v3, v4, v10

    if-eqz v3, :cond_5

    iget-wide v4, p0, LX/0Gc;->k:J

    cmp-long v3, v0, v4

    if-gez v3, :cond_6

    .line 36062
    :cond_5
    iput-wide v0, p0, LX/0Gc;->k:J

    .line 36063
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    move v0, v2

    .line 36064
    goto :goto_2

    .line 36065
    :cond_8
    iget-boolean v0, p0, LX/0Gc;->m:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, LX/0Gc;->n:Z

    if-eqz v0, :cond_4

    if-nez v5, :cond_4

    .line 36066
    sget-object v0, LX/0OO;->a:LX/0OO;

    invoke-virtual {v0, v2}, LX/0OO;->b(I)V

    .line 36067
    iput-boolean v2, p0, LX/0Gc;->n:Z

    .line 36068
    invoke-direct {p0, v2}, LX/0Gc;->a(Z)V

    goto :goto_3

    .line 36069
    :cond_9
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 36077
    iget-object v0, p0, LX/0Gc;->a:LX/0O1;

    iget v1, p0, LX/0Gc;->j:I

    invoke-interface {v0, v1}, LX/0O1;->a(I)V

    .line 36078
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 36037
    iget-object v0, p0, LX/0Gc;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 36038
    iget-object v0, p0, LX/0Gc;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ga;

    .line 36039
    iget v1, p0, LX/0Gc;->j:I

    iget v0, v0, LX/0Ga;->a:I

    sub-int v0, v1, v0

    iput v0, p0, LX/0Gc;->j:I

    .line 36040
    invoke-direct {p0}, LX/0Gc;->c()V

    .line 36041
    return-void
.end method

.method public final a(Ljava/lang/Object;I)V
    .locals 2

    .prologue
    .line 36033
    iget-object v0, p0, LX/0Gc;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36034
    iget-object v0, p0, LX/0Gc;->c:Ljava/util/HashMap;

    new-instance v1, LX/0Ga;

    invoke-direct {v1, p2}, LX/0Ga;-><init>(I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36035
    iget v0, p0, LX/0Gc;->j:I

    add-int/2addr v0, p2

    iput v0, p0, LX/0Gc;->j:I

    .line 36036
    return-void
.end method

.method public final a(Ljava/lang/Object;JJZ)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 36014
    invoke-direct {p0, p2, p3, p4, p5}, LX/0Gc;->a(JJ)I

    move-result v4

    .line 36015
    iget-object v0, p0, LX/0Gc;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ga;

    .line 36016
    iget v3, v0, LX/0Ga;->b:I

    if-ne v3, v4, :cond_0

    iget-wide v6, v0, LX/0Ga;->d:J

    cmp-long v3, v6, p4

    if-nez v3, :cond_0

    iget-boolean v3, v0, LX/0Ga;->c:Z

    if-eq v3, p6, :cond_5

    :cond_0
    move v3, v1

    .line 36017
    :goto_0
    if-eqz v3, :cond_1

    .line 36018
    iput v4, v0, LX/0Ga;->b:I

    .line 36019
    iput-wide p4, v0, LX/0Ga;->d:J

    .line 36020
    iput-boolean p6, v0, LX/0Ga;->c:Z

    .line 36021
    :cond_1
    iget-object v0, p0, LX/0Gc;->a:LX/0O1;

    invoke-interface {v0}, LX/0O1;->b()I

    move-result v4

    .line 36022
    int-to-float v0, v4

    iget v5, p0, LX/0Gc;->j:I

    int-to-float v5, v5

    div-float/2addr v0, v5

    .line 36023
    iget v5, p0, LX/0Gc;->i:F

    cmpl-float v5, v0, v5

    if-lez v5, :cond_8

    const/4 v0, 0x0

    :goto_1
    move v5, v0

    .line 36024
    iget v0, p0, LX/0Gc;->l:I

    if-eq v0, v5, :cond_6

    move v0, v1

    .line 36025
    :goto_2
    if-eqz v0, :cond_2

    .line 36026
    iput v5, p0, LX/0Gc;->l:I

    .line 36027
    :cond_2
    if-nez v3, :cond_3

    if-eqz v0, :cond_4

    .line 36028
    :cond_3
    invoke-direct {p0}, LX/0Gc;->c()V

    .line 36029
    :cond_4
    iget v0, p0, LX/0Gc;->j:I

    if-ge v4, v0, :cond_7

    const-wide/16 v4, -0x1

    cmp-long v0, p4, v4

    if-eqz v0, :cond_7

    iget-wide v4, p0, LX/0Gc;->k:J

    cmp-long v0, p4, v4

    if-gtz v0, :cond_7

    move v0, v1

    :goto_3
    return v0

    :cond_5
    move v3, v2

    .line 36030
    goto :goto_0

    :cond_6
    move v0, v2

    .line 36031
    goto :goto_2

    :cond_7
    move v0, v2

    .line 36032
    goto :goto_3

    :cond_8
    iget v5, p0, LX/0Gc;->h:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_9

    const/4 v0, 0x2

    goto :goto_1

    :cond_9
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final b()LX/0O1;
    .locals 1

    .prologue
    .line 36013
    iget-object v0, p0, LX/0Gc;->a:LX/0O1;

    return-object v0
.end method
