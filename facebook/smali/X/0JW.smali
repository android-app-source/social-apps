.class public final enum LX/0JW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JW;

.field public static final enum DISABLED:LX/0JW;

.field public static final enum KEYWORDS:LX/0JW;

.field public static final enum KEYWORDS_LITE:LX/0JW;

.field public static final enum SIMPLE:LX/0JW;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39513
    new-instance v0, LX/0JW;

    const-string v1, "KEYWORDS"

    const-string v2, "video_search_keywords"

    invoke-direct {v0, v1, v3, v2}, LX/0JW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JW;->KEYWORDS:LX/0JW;

    .line 39514
    new-instance v0, LX/0JW;

    const-string v1, "KEYWORDS_LITE"

    const-string v2, "video_search_keywords_lite"

    invoke-direct {v0, v1, v4, v2}, LX/0JW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JW;->KEYWORDS_LITE:LX/0JW;

    .line 39515
    new-instance v0, LX/0JW;

    const-string v1, "SIMPLE"

    const-string v2, "video_search_simple"

    invoke-direct {v0, v1, v5, v2}, LX/0JW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JW;->SIMPLE:LX/0JW;

    .line 39516
    new-instance v0, LX/0JW;

    const-string v1, "DISABLED"

    const-string v2, "video_search_disabled"

    invoke-direct {v0, v1, v6, v2}, LX/0JW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JW;->DISABLED:LX/0JW;

    .line 39517
    const/4 v0, 0x4

    new-array v0, v0, [LX/0JW;

    sget-object v1, LX/0JW;->KEYWORDS:LX/0JW;

    aput-object v1, v0, v3

    sget-object v1, LX/0JW;->KEYWORDS_LITE:LX/0JW;

    aput-object v1, v0, v4

    sget-object v1, LX/0JW;->SIMPLE:LX/0JW;

    aput-object v1, v0, v5

    sget-object v1, LX/0JW;->DISABLED:LX/0JW;

    aput-object v1, v0, v6

    sput-object v0, LX/0JW;->$VALUES:[LX/0JW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39518
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39519
    iput-object p3, p0, LX/0JW;->value:Ljava/lang/String;

    .line 39520
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JW;
    .locals 1

    .prologue
    .line 39521
    const-class v0, LX/0JW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JW;

    return-object v0
.end method

.method public static values()[LX/0JW;
    .locals 1

    .prologue
    .line 39522
    sget-object v0, LX/0JW;->$VALUES:[LX/0JW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JW;

    return-object v0
.end method
