.class public LX/04a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/04b;


# instance fields
.field private final mProxy:Ljava/net/Proxy;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mSocketTimeout:I


# direct methods
.method public constructor <init>(ILjava/net/Proxy;)V
    .locals 0
    .param p2    # Ljava/net/Proxy;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 13913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13914
    iput p1, p0, LX/04a;->mSocketTimeout:I

    .line 13915
    iput-object p2, p0, LX/04a;->mProxy:Ljava/net/Proxy;

    .line 13916
    return-void
.end method


# virtual methods
.method public getConnection(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 1

    .prologue
    .line 13917
    iget-object v0, p0, LX/04a;->mProxy:Ljava/net/Proxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/04a;->mProxy:Ljava/net/Proxy;

    invoke-virtual {p1, v0}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    :goto_0
    check-cast v0, Ljava/net/HttpURLConnection;

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 13918
    iget p1, p0, LX/04a;->mSocketTimeout:I

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 13919
    iget p1, p0, LX/04a;->mSocketTimeout:I

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 13920
    move-object v0, v0

    .line 13921
    return-object v0

    .line 13922
    :cond_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    goto :goto_0
.end method
