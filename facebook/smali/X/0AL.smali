.class public final enum LX/0AL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0AL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0AL;

.field public static final enum ACKNOWLEDGED_DELIVERY:LX/0AL;

.field public static final enum ASSURED_DELIVERY:LX/0AL;

.field public static final enum FIRE_AND_FORGET:LX/0AL;


# instance fields
.field public final mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23791
    new-instance v0, LX/0AL;

    const-string v1, "FIRE_AND_FORGET"

    invoke-direct {v0, v1, v2, v2}, LX/0AL;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0AL;->FIRE_AND_FORGET:LX/0AL;

    .line 23792
    new-instance v0, LX/0AL;

    const-string v1, "ACKNOWLEDGED_DELIVERY"

    invoke-direct {v0, v1, v3, v3}, LX/0AL;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    .line 23793
    new-instance v0, LX/0AL;

    const-string v1, "ASSURED_DELIVERY"

    invoke-direct {v0, v1, v4, v4}, LX/0AL;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0AL;->ASSURED_DELIVERY:LX/0AL;

    .line 23794
    const/4 v0, 0x3

    new-array v0, v0, [LX/0AL;

    sget-object v1, LX/0AL;->FIRE_AND_FORGET:LX/0AL;

    aput-object v1, v0, v2

    sget-object v1, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    aput-object v1, v0, v3

    sget-object v1, LX/0AL;->ASSURED_DELIVERY:LX/0AL;

    aput-object v1, v0, v4

    sput-object v0, LX/0AL;->$VALUES:[LX/0AL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 23781
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23782
    iput p3, p0, LX/0AL;->mValue:I

    .line 23783
    return-void
.end method

.method public static fromInt(I)LX/0AL;
    .locals 3

    .prologue
    .line 23785
    packed-switch p0, :pswitch_data_0

    .line 23786
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown QOS level "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23787
    :pswitch_0
    sget-object v0, LX/0AL;->FIRE_AND_FORGET:LX/0AL;

    .line 23788
    :goto_0
    return-object v0

    .line 23789
    :pswitch_1
    sget-object v0, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    goto :goto_0

    .line 23790
    :pswitch_2
    sget-object v0, LX/0AL;->ASSURED_DELIVERY:LX/0AL;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/0AL;
    .locals 1

    .prologue
    .line 23784
    const-class v0, LX/0AL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0AL;

    return-object v0
.end method

.method public static values()[LX/0AL;
    .locals 1

    .prologue
    .line 23780
    sget-object v0, LX/0AL;->$VALUES:[LX/0AL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0AL;

    return-object v0
.end method
