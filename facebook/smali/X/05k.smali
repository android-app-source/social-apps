.class public LX/05k;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/05h;

.field public final c:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field private final d:Z

.field public e:J

.field public f:J

.field public g:J


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/05h;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Z)V
    .locals 8

    .prologue
    const-wide/16 v2, -0x1

    .line 16813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16814
    iput-wide v2, p0, LX/05k;->e:J

    .line 16815
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/05k;->f:J

    .line 16816
    iput-wide v2, p0, LX/05k;->g:J

    .line 16817
    iput-object p1, p0, LX/05k;->a:Landroid/content/Context;

    .line 16818
    iput-object p2, p0, LX/05k;->b:LX/05h;

    .line 16819
    iput-object p3, p0, LX/05k;->c:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 16820
    iput-boolean p4, p0, LX/05k;->d:Z

    .line 16821
    iget-object v4, p0, LX/05k;->c:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v4}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    .line 16822
    invoke-static {p0}, LX/05k;->f(LX/05k;)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "last_log_ms"

    invoke-interface {v6, v7, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 16823
    cmp-long v6, v6, v4

    if-ltz v6, :cond_0

    .line 16824
    invoke-static {p0}, LX/05k;->f(LX/05k;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "last_log_ms"

    invoke-interface {v6, v7, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-static {v4}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 16825
    :cond_0
    return-void
.end method

.method public static f(LX/05k;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 16826
    iget-object v0, p0, LX/05k;->a:Landroid/content/Context;

    sget-object v1, LX/01p;->o:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 15

    .prologue
    .line 16786
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/05k;->d:Z

    if-eqz v0, :cond_0

    .line 16787
    const-wide/16 v7, 0x2710

    .line 16788
    iget-object v1, p0, LX/05k;->c:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v1

    .line 16789
    iget-wide v3, p0, LX/05k;->e:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-gez v3, :cond_1

    .line 16790
    iput-wide v1, p0, LX/05k;->e:J

    .line 16791
    iput-wide v1, p0, LX/05k;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 16792
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 16793
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 16794
    :cond_1
    iget-wide v3, p0, LX/05k;->e:J

    sub-long v3, v1, v3

    .line 16795
    iput-wide v1, p0, LX/05k;->e:J

    .line 16796
    cmp-long v5, v3, v7

    if-lez v5, :cond_3

    .line 16797
    iget-wide v3, p0, LX/05k;->f:J

    add-long/2addr v3, v7

    iput-wide v3, p0, LX/05k;->f:J

    .line 16798
    :goto_1
    iget-wide v3, p0, LX/05k;->g:J

    sub-long v3, v1, v3

    .line 16799
    const-wide/16 v5, 0x4e20

    cmp-long v3, v3, v5

    if-lez v3, :cond_2

    .line 16800
    const-wide/16 v13, 0x0

    .line 16801
    iget-wide v9, p0, LX/05k;->f:J

    invoke-static {p0}, LX/05k;->f(LX/05k;)Landroid/content/SharedPreferences;

    move-result-object v11

    const-string v12, "total_wake_ms"

    invoke-interface {v11, v12, v13, v14}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v11

    add-long/2addr v9, v11

    iput-wide v9, p0, LX/05k;->f:J

    .line 16802
    invoke-static {p0}, LX/05k;->f(LX/05k;)Landroid/content/SharedPreferences;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    const-string v10, "total_wake_ms"

    iget-wide v11, p0, LX/05k;->f:J

    invoke-interface {v9, v10, v11, v12}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    invoke-static {v9}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 16803
    iput-wide v13, p0, LX/05k;->f:J

    .line 16804
    iput-wide v1, p0, LX/05k;->g:J

    .line 16805
    :cond_2
    invoke-static {p0}, LX/05k;->f(LX/05k;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "last_log_ms"

    invoke-interface {v3, v4, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    sub-long v3, v1, v3

    .line 16806
    const-wide/32 v5, 0x36ee80

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    .line 16807
    iget-object v9, p0, LX/05k;->b:LX/05h;

    invoke-static {p0}, LX/05k;->f(LX/05k;)Landroid/content/SharedPreferences;

    move-result-object v10

    const-string v11, "total_wake_ms"

    const-wide/16 v13, 0x0

    invoke-interface {v10, v11, v13, v14}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v11

    .line 16808
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "total_wake_ms"

    aput-object v14, v10, v13

    const/4 v13, 0x1

    invoke-static {v11, v12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v10, v13

    invoke-static {v10}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v10

    .line 16809
    const-string v13, "mqtt_radio_active_time"

    invoke-virtual {v9, v13, v10}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 16810
    invoke-static {p0}, LX/05k;->f(LX/05k;)Landroid/content/SharedPreferences;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    invoke-static {v9}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 16811
    invoke-static {p0}, LX/05k;->f(LX/05k;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "last_log_ms"

    invoke-interface {v3, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_0

    .line 16812
    :cond_3
    iget-wide v5, p0, LX/05k;->f:J

    add-long/2addr v3, v5

    iput-wide v3, p0, LX/05k;->f:J

    goto/16 :goto_1
.end method
