.class public LX/0Ih;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:LX/0Ij;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38878
    const-class v0, LX/0Ih;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0Ih;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ij;)V
    .locals 0

    .prologue
    .line 38879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38880
    iput-object p1, p0, LX/0Ih;->b:Landroid/content/Context;

    .line 38881
    iput-object p2, p0, LX/0Ih;->c:LX/0Ij;

    .line 38882
    return-void
.end method


# virtual methods
.method public final a()LX/06y;
    .locals 8

    .prologue
    .line 38883
    iget-object v0, p0, LX/0Ih;->b:Landroid/content/Context;

    sget-object v1, LX/01p;->d:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 38884
    new-instance v1, LX/06y;

    const-string v2, "fbns_shared_id"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "fbns_shared_secret"

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "fbns_shared_timestamp"

    const-wide v6, 0x7fffffffffffffffL

    invoke-interface {v0, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, LX/06y;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    return-object v1
.end method
