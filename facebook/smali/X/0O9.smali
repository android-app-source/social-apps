.class public final LX/0O9;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field private final a:LX/0G6;

.field private final b:LX/0OA;

.field private final c:[B

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(LX/0G6;LX/0OA;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52495
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 52496
    iput-boolean v0, p0, LX/0O9;->d:Z

    .line 52497
    iput-boolean v0, p0, LX/0O9;->e:Z

    .line 52498
    iput-object p1, p0, LX/0O9;->a:LX/0G6;

    .line 52499
    iput-object p2, p0, LX/0O9;->b:LX/0OA;

    .line 52500
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, LX/0O9;->c:[B

    .line 52501
    return-void
.end method

.method public static b(LX/0O9;)V
    .locals 2

    .prologue
    .line 52475
    iget-boolean v0, p0, LX/0O9;->d:Z

    if-nez v0, :cond_0

    .line 52476
    iget-object v0, p0, LX/0O9;->a:LX/0G6;

    iget-object v1, p0, LX/0O9;->b:LX/0OA;

    invoke-interface {v0, v1}, LX/0G6;->a(LX/0OA;)J

    .line 52477
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0O9;->d:Z

    .line 52478
    :cond_0
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 52479
    iget-boolean v0, p0, LX/0O9;->e:Z

    if-nez v0, :cond_0

    .line 52480
    iget-object v0, p0, LX/0O9;->a:LX/0G6;

    invoke-interface {v0}, LX/0G6;->a()V

    .line 52481
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0O9;->e:Z

    .line 52482
    :cond_0
    return-void
.end method

.method public final read()I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 52483
    iget-object v1, p0, LX/0O9;->c:[B

    invoke-virtual {p0, v1}, LX/0O9;->read([B)I

    move-result v1

    .line 52484
    if-ne v1, v0, :cond_0

    .line 52485
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0O9;->c:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public final read([B)I
    .locals 2

    .prologue
    .line 52486
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LX/0O9;->read([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 52487
    iget-boolean v0, p0, LX/0O9;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 52488
    invoke-static {p0}, LX/0O9;->b(LX/0O9;)V

    .line 52489
    iget-object v0, p0, LX/0O9;->a:LX/0G6;

    invoke-interface {v0, p1, p2, p3}, LX/0G6;->a([BII)I

    move-result v0

    return v0

    .line 52490
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final skip(J)J
    .locals 3

    .prologue
    .line 52491
    iget-boolean v0, p0, LX/0O9;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 52492
    invoke-static {p0}, LX/0O9;->b(LX/0O9;)V

    .line 52493
    invoke-super {p0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    return-wide v0

    .line 52494
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
