.class public LX/0EO;
.super Landroid/widget/ListPopupWindow;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0EK;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Cf;

.field public d:LX/0EL;

.field public e:LX/0EK;

.field public f:LX/0EK;

.field public g:LX/0EN;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;LX/0Cf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "LX/0EK;",
            ">;",
            "LX/0Cf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31718
    invoke-direct {p0, p1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    .line 31719
    iput-object p2, p0, LX/0EO;->b:Ljava/util/ArrayList;

    .line 31720
    iput-object p1, p0, LX/0EO;->a:Landroid/content/Context;

    .line 31721
    iput-object p3, p0, LX/0EO;->c:LX/0Cf;

    .line 31722
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 31723
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/0EO;->setModal(Z)V

    .line 31724
    iget-object v0, p0, LX/0EO;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020172

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0EO;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 31725
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/0EO;->setInputMethodMode(I)V

    .line 31726
    new-instance v0, LX/0EN;

    invoke-direct {v0, p0}, LX/0EN;-><init>(LX/0EO;)V

    iput-object v0, p0, LX/0EO;->g:LX/0EN;

    .line 31727
    iget-object v0, p0, LX/0EO;->g:LX/0EN;

    invoke-virtual {p0, v0}, LX/0EO;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 31728
    iget-object v0, p0, LX/0EO;->g:LX/0EN;

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 31729
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 31730
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 31731
    invoke-virtual {v0}, LX/0EN;->getCount()I

    move-result v5

    move v2, v1

    .line 31732
    :goto_0
    if-ge v2, v5, :cond_1

    .line 31733
    invoke-virtual {v0, v2, v8, v8}, LX/0EN;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 31734
    invoke-virtual {v6, v3, v4}, Landroid/view/View;->measure(II)V

    .line 31735
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    if-le v7, v1, :cond_0

    .line 31736
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 31737
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 31738
    :cond_1
    iget-object v2, p0, LX/0EO;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b12e1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    .line 31739
    iget-object v3, p0, LX/0EO;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v3, v2

    .line 31740
    add-int/2addr v1, v2

    .line 31741
    iget-object v2, p0, LX/0EO;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b12e0

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 31742
    if-le v1, v3, :cond_6

    .line 31743
    :goto_1
    move v1, v3

    .line 31744
    invoke-virtual {p0, v1}, LX/0EO;->setContentWidth(I)V

    .line 31745
    iget-object v0, p0, LX/0EO;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0EK;

    .line 31746
    const-string v2, "zoom"

    .line 31747
    iget-object v3, v0, LX/0EK;->b:Ljava/lang/String;

    move-object v3, v3

    .line 31748
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 31749
    iget-object v2, v0, LX/0EK;->a:Ljava/util/ArrayList;

    move-object v0, v2

    .line 31750
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0EK;

    .line 31751
    const-string v3, "ZOOM_IN"

    .line 31752
    iget-object v4, v0, LX/0EK;->b:Ljava/lang/String;

    move-object v4, v4

    .line 31753
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 31754
    iput-object v0, p0, LX/0EO;->e:LX/0EK;

    goto :goto_2

    .line 31755
    :cond_4
    const-string v3, "ZOOM_OUT"

    .line 31756
    iget-object v4, v0, LX/0EK;->b:Ljava/lang/String;

    move-object v4, v4

    .line 31757
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 31758
    iput-object v0, p0, LX/0EO;->f:LX/0EK;

    goto :goto_2

    .line 31759
    :cond_5
    new-instance v0, LX/0EM;

    invoke-direct {v0, p0}, LX/0EM;-><init>(LX/0EO;)V

    iput-object v0, p0, LX/0EO;->d:LX/0EL;

    .line 31760
    return-void

    .line 31761
    :cond_6
    if-ge v1, v2, :cond_7

    move v3, v2

    .line 31762
    goto :goto_1

    :cond_7
    move v3, v1

    .line 31763
    goto :goto_1
.end method
