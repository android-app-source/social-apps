.class public LX/0BB;
.super LX/06q;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26210
    const-string v0, "lt"

    invoke-direct {p0, v0}, LX/06q;-><init>(Ljava/lang/String;)V

    .line 26211
    return-void
.end method


# virtual methods
.method public final a(LX/0BC;J)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 26212
    invoke-virtual {p0, p1}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    .line 26213
    cmp-long v1, p2, v10

    if-gtz v1, :cond_1

    .line 26214
    :cond_0
    return-void

    .line 26215
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    .line 26216
    cmp-long v1, v4, v10

    if-nez v1, :cond_2

    move-wide v2, p2

    .line 26217
    :goto_1
    invoke-virtual {v0, v4, v5, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 26218
    :cond_2
    long-to-double v2, v4

    const-wide v6, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v2, v6

    long-to-double v6, p2

    const-wide v8, 0x3fc999999999999aL    # 0.2

    mul-double/2addr v6, v8

    add-double/2addr v2, v6

    double-to-long v2, v2

    goto :goto_1
.end method
