.class public final LX/0Lt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Lr;


# instance fields
.field private final a:I

.field private final b:Landroid/content/Context;

.field private final c:Z

.field private final d:Z


# direct methods
.method private constructor <init>(ILandroid/content/Context;ZZ)V
    .locals 0

    .prologue
    .line 44792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44793
    iput p1, p0, LX/0Lt;->a:I

    .line 44794
    iput-object p2, p0, LX/0Lt;->b:Landroid/content/Context;

    .line 44795
    iput-boolean p3, p0, LX/0Lt;->c:Z

    .line 44796
    iput-boolean p4, p0, LX/0Lt;->d:Z

    .line 44797
    return-void
.end method

.method public static a()LX/0Lt;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 44798
    new-instance v0, LX/0Lt;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3, v3}, LX/0Lt;-><init>(ILandroid/content/Context;ZZ)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;ZZ)LX/0Lt;
    .locals 2

    .prologue
    .line 44799
    new-instance v0, LX/0Lt;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, p1, p2}, LX/0Lt;-><init>(ILandroid/content/Context;ZZ)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/0AY;ILX/0Lp;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44800
    invoke-virtual {p1, p2}, LX/0AY;->a(I)LX/0Am;

    move-result-object v5

    move v1, v2

    .line 44801
    :goto_0
    iget-object v0, v5, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 44802
    iget-object v0, v5, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    .line 44803
    iget v4, v0, LX/0Ak;->b:I

    iget v6, p0, LX/0Lt;->a:I

    if-ne v4, v6, :cond_5

    .line 44804
    iget v4, p0, LX/0Lt;->a:I

    if-nez v4, :cond_4

    .line 44805
    iget-boolean v4, p0, LX/0Lt;->c:Z

    if-eqz v4, :cond_2

    .line 44806
    iget-object v4, p0, LX/0Lt;->b:Landroid/content/Context;

    iget-object v6, v0, LX/0Ak;->c:Ljava/util/List;

    const/4 v7, 0x0

    iget-boolean v8, p0, LX/0Lt;->d:Z

    if-eqz v8, :cond_1

    .line 44807
    iget-object v8, v0, LX/0Ak;->d:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_7

    const/4 v8, 0x1

    :goto_1
    move v0, v8

    .line 44808
    if-eqz v0, :cond_1

    move v0, v3

    :goto_2
    invoke-static {v4, v6, v7, v0}, LX/0Li;->a(Landroid/content/Context;Ljava/util/List;[Ljava/lang/String;Z)[I

    move-result-object v0

    .line 44809
    :goto_3
    array-length v6, v0

    .line 44810
    if-le v6, v3, :cond_0

    .line 44811
    invoke-interface {p3, p1, p2, v1, v0}, LX/0Lp;->a(LX/0AY;II[I)V

    :cond_0
    move v4, v2

    .line 44812
    :goto_4
    if-ge v4, v6, :cond_5

    .line 44813
    aget v7, v0, v4

    invoke-interface {p3, p1, p2, v1, v7}, LX/0Lp;->a(LX/0AY;III)V

    .line 44814
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_1
    move v0, v2

    .line 44815
    goto :goto_2

    .line 44816
    :cond_2
    iget-object v0, v0, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 44817
    new-array v6, v0, [I

    .line 44818
    const/4 v4, 0x0

    :goto_5
    if-ge v4, v0, :cond_3

    .line 44819
    aput v4, v6, v4

    .line 44820
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 44821
    :cond_3
    move-object v0, v6

    .line 44822
    goto :goto_3

    :cond_4
    move v4, v2

    .line 44823
    :goto_6
    iget-object v6, v0, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_5

    .line 44824
    invoke-interface {p3, p1, p2, v1, v4}, LX/0Lp;->a(LX/0AY;III)V

    .line 44825
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 44826
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 44827
    :cond_6
    return-void

    :cond_7
    const/4 v8, 0x0

    goto :goto_1
.end method
