.class public final LX/083;
.super LX/084;
.source ""


# instance fields
.field private final mDexOptRunner:LX/085;

.field private final mDexStore:LX/02U;

.field private final mDummyZip:Ljava/io/File;

.field private final mFlags:I

.field private final mTmpDir:LX/087;


# direct methods
.method public constructor <init>(LX/02U;I)V
    .locals 3

    .prologue
    .line 20619
    invoke-direct {p0}, LX/084;-><init>()V

    .line 20620
    new-instance v0, LX/085;

    iget-object v1, p1, LX/02U;->root:Ljava/io/File;

    invoke-direct {v0, v1}, LX/085;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/083;->mDexOptRunner:LX/085;

    .line 20621
    iput-object p1, p0, LX/083;->mDexStore:LX/02U;

    .line 20622
    iput p2, p0, LX/083;->mFlags:I

    .line 20623
    const-string v0, "turbo-compiler"

    invoke-virtual {p1, v0}, LX/02U;->makeTemporaryDirectory(Ljava/lang/String;)LX/087;

    move-result-object v0

    iput-object v0, p0, LX/083;->mTmpDir:LX/087;

    .line 20624
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/083;->mTmpDir:LX/087;

    iget-object v1, v1, LX/087;->directory:Ljava/io/File;

    const-string v2, "dummy.zip"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/083;->mDummyZip:Ljava/io/File;

    .line 20625
    iget-object v0, p0, LX/083;->mDummyZip:Ljava/io/File;

    invoke-static {v0}, LX/02b;->makeDummyZip(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20626
    return-void

    :catchall_0
    move-exception v0

    .line 20627
    iget-object v1, p0, LX/083;->mTmpDir:LX/087;

    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    throw v0
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 20628
    iget-object v0, p0, LX/083;->mTmpDir:LX/087;

    invoke-virtual {v0}, LX/087;->close()V

    .line 20629
    return-void
.end method

.method public final compile(LX/08A;)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 20630
    iget-object v0, p1, LX/08A;->dex:LX/02Z;

    invoke-static {v0}, LX/02b;->makeDexName(LX/02Z;)Ljava/lang/String;

    move-result-object v3

    .line 20631
    invoke-static {v3}, LX/02b;->makeOdexName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 20632
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/083;->mDexStore:LX/02U;

    iget-object v1, v1, LX/02U;->root:Ljava/io/File;

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 20633
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/083;->mDexStore:LX/02U;

    iget-object v2, v2, LX/02U;->root:Ljava/io/File;

    invoke-direct {v1, v2, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 20634
    iget v2, p0, LX/083;->mFlags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20635
    :goto_0
    return-void

    .line 20636
    :cond_0
    new-instance v9, Ljava/io/File;

    iget-object v0, p0, LX/083;->mTmpDir:LX/087;

    iget-object v0, v0, LX/087;->directory:Ljava/io/File;

    invoke-direct {v9, v0, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 20637
    invoke-virtual {p1}, LX/08A;->getDexContents()Ljava/io/InputStream;

    move-result-object v1

    .line 20638
    :try_start_0
    invoke-virtual {p1, v1}, LX/08A;->getSizeHint(Ljava/io/InputStream;)I

    move-result v2

    .line 20639
    const-string v0, "size hint for %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 20640
    new-instance v4, Ljava/io/RandomAccessFile;

    const-string v0, "rw"

    invoke-direct {v4, v9, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 20641
    :try_start_1
    iget-object v0, p0, LX/083;->mDexOptRunner:LX/085;

    const-string v5, "quick"

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/082;->run(Ljava/io/InputStream;ILjava/lang/String;Ljava/io/RandomAccessFile;Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 20642
    :try_start_2
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 20643
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 20644
    :cond_1
    iget-object v0, p0, LX/083;->mDummyZip:Ljava/io/File;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/083;->mDexStore:LX/02U;

    iget-object v2, v2, LX/02U;->root:Ljava/io/File;

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 20645
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lcom/facebook/common/dextricks/DalvikInternals;->link(Ljava/lang/String;Ljava/lang/String;I)V

    .line 20646
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/083;->mDexStore:LX/02U;

    iget-object v1, v1, LX/02U;->root:Ljava/io/File;

    invoke-direct {v0, v1, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v9, v0}, LX/02Q;->renameOrThrow(Ljava/io/File;Ljava/io/File;)V

    goto :goto_0

    .line 20647
    :catch_0
    :try_start_3
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 20648
    :catch_1
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 20649
    :catchall_0
    move-exception v2

    move-object v10, v2

    move-object v2, v0

    move-object v0, v10

    :goto_1
    if-eqz v2, :cond_3

    :try_start_5
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :goto_2
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 20650
    :catch_2
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 20651
    :catchall_1
    move-exception v2

    move-object v7, v0

    move-object v0, v2

    :goto_3
    if-eqz v1, :cond_2

    if-eqz v7, :cond_4

    :try_start_8
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_4

    :cond_2
    :goto_4
    throw v0

    .line 20652
    :catch_3
    move-exception v3

    :try_start_9
    invoke-static {v2, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 20653
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 20654
    :cond_3
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_2

    .line 20655
    :catch_4
    move-exception v1

    invoke-static {v7, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto :goto_4

    .line 20656
    :catchall_3
    move-exception v0

    move-object v2, v7

    goto :goto_1
.end method
