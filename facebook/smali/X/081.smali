.class public final LX/081;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private mArtFilter:B

.field private mArtHugeMethodMax:I

.field private mArtLargeMethodMax:I

.field private mArtSmallMethodMax:I

.field private mArtTinyMethodMax:I

.field public mArtTruncatedDexSize:I

.field private mDalvikOptimize:B

.field private mDalvikRegisterMaps:B

.field private mDalvikVerify:B

.field private mMode:B

.field private mSync:B


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 20550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20551
    iput-byte v0, p0, LX/081;->mMode:B

    .line 20552
    iput-byte v0, p0, LX/081;->mSync:B

    .line 20553
    iput-byte v0, p0, LX/081;->mDalvikVerify:B

    .line 20554
    iput-byte v0, p0, LX/081;->mDalvikOptimize:B

    .line 20555
    iput-byte v0, p0, LX/081;->mDalvikRegisterMaps:B

    .line 20556
    iput-byte v0, p0, LX/081;->mArtFilter:B

    .line 20557
    iput v1, p0, LX/081;->mArtHugeMethodMax:I

    .line 20558
    iput v1, p0, LX/081;->mArtLargeMethodMax:I

    .line 20559
    iput v1, p0, LX/081;->mArtSmallMethodMax:I

    .line 20560
    iput v1, p0, LX/081;->mArtTinyMethodMax:I

    .line 20561
    iput v1, p0, LX/081;->mArtTruncatedDexSize:I

    return-void
.end method


# virtual methods
.method public final build()LX/080;
    .locals 13

    .prologue
    .line 20549
    new-instance v0, LX/080;

    iget-byte v1, p0, LX/081;->mMode:B

    iget-byte v2, p0, LX/081;->mSync:B

    iget-byte v3, p0, LX/081;->mDalvikVerify:B

    iget-byte v4, p0, LX/081;->mDalvikOptimize:B

    iget-byte v5, p0, LX/081;->mDalvikRegisterMaps:B

    iget-byte v6, p0, LX/081;->mArtFilter:B

    iget v7, p0, LX/081;->mArtHugeMethodMax:I

    iget v8, p0, LX/081;->mArtLargeMethodMax:I

    iget v9, p0, LX/081;->mArtSmallMethodMax:I

    iget v10, p0, LX/081;->mArtTinyMethodMax:I

    iget v11, p0, LX/081;->mArtTruncatedDexSize:I

    const/4 v12, 0x0

    invoke-direct/range {v0 .. v12}, LX/080;-><init>(BBBBBBIIIIILX/08b;)V

    return-object v0
.end method

.method public final setArtFilter(B)LX/081;
    .locals 0

    .prologue
    .line 20547
    iput-byte p1, p0, LX/081;->mArtFilter:B

    .line 20548
    return-object p0
.end method

.method public final setArtHugeMethodMax(I)LX/081;
    .locals 0

    .prologue
    .line 20545
    iput p1, p0, LX/081;->mArtHugeMethodMax:I

    .line 20546
    return-object p0
.end method

.method public final setArtLargeMethodMax(I)LX/081;
    .locals 0

    .prologue
    .line 20543
    iput p1, p0, LX/081;->mArtLargeMethodMax:I

    .line 20544
    return-object p0
.end method

.method public final setArtSmallMethodMax(I)LX/081;
    .locals 0

    .prologue
    .line 20562
    iput p1, p0, LX/081;->mArtSmallMethodMax:I

    .line 20563
    return-object p0
.end method

.method public final setArtTinyMethodMax(I)LX/081;
    .locals 0

    .prologue
    .line 20541
    iput p1, p0, LX/081;->mArtTinyMethodMax:I

    .line 20542
    return-object p0
.end method

.method public final setDalvikOptimize(B)LX/081;
    .locals 0

    .prologue
    .line 20539
    iput-byte p1, p0, LX/081;->mDalvikOptimize:B

    .line 20540
    return-object p0
.end method

.method public final setDalvikRegisterMaps(B)LX/081;
    .locals 0

    .prologue
    .line 20537
    iput-byte p1, p0, LX/081;->mDalvikRegisterMaps:B

    .line 20538
    return-object p0
.end method

.method public final setDalvikVerify(B)LX/081;
    .locals 0

    .prologue
    .line 20535
    iput-byte p1, p0, LX/081;->mDalvikVerify:B

    .line 20536
    return-object p0
.end method

.method public final setMode(B)LX/081;
    .locals 0

    .prologue
    .line 20533
    iput-byte p1, p0, LX/081;->mMode:B

    .line 20534
    return-object p0
.end method

.method public final setSync(B)LX/081;
    .locals 0

    .prologue
    .line 20531
    iput-byte p1, p0, LX/081;->mSync:B

    .line 20532
    return-object p0
.end method
