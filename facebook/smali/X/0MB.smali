.class public final LX/0MB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0MA;


# static fields
.field public static final a:[B


# instance fields
.field private final b:LX/0G6;

.field private final c:J

.field private d:J

.field public e:[B

.field public f:I

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45245
    const/16 v0, 0x1000

    new-array v0, v0, [B

    sput-object v0, LX/0MB;->a:[B

    return-void
.end method

.method public constructor <init>(LX/0G6;JJ)V
    .locals 2

    .prologue
    .line 45231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45232
    iput-object p1, p0, LX/0MB;->b:LX/0G6;

    .line 45233
    iput-wide p2, p0, LX/0MB;->d:J

    .line 45234
    iput-wide p4, p0, LX/0MB;->c:J

    .line 45235
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, LX/0MB;->e:[B

    .line 45236
    return-void
.end method

.method public static a(LX/0MB;[BIIIZ)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 45237
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45238
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 45239
    :cond_0
    iget-object v1, p0, LX/0MB;->b:LX/0G6;

    add-int v2, p2, p4

    sub-int v3, p3, p4

    invoke-interface {v1, p1, v2, v3}, LX/0G6;->a([BII)I

    move-result v1

    .line 45240
    if-ne v1, v0, :cond_2

    .line 45241
    if-nez p4, :cond_1

    if-eqz p5, :cond_1

    .line 45242
    :goto_0
    return v0

    .line 45243
    :cond_1
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 45244
    :cond_2
    add-int v0, p4, v1

    goto :goto_0
.end method

.method private b(IZ)Z
    .locals 6

    .prologue
    .line 45246
    iget v0, p0, LX/0MB;->f:I

    add-int/2addr v0, p1

    .line 45247
    iget-object v1, p0, LX/0MB;->e:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 45248
    iget-object v1, p0, LX/0MB;->e:[B

    iget-object v2, p0, LX/0MB;->e:[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, LX/0MB;->e:[B

    .line 45249
    :cond_0
    iget v0, p0, LX/0MB;->g:I

    iget v1, p0, LX/0MB;->f:I

    sub-int/2addr v0, v1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 45250
    iget v0, p0, LX/0MB;->g:I

    sub-int v1, p1, v4

    add-int/2addr v0, v1

    iput v0, p0, LX/0MB;->g:I

    .line 45251
    :cond_1
    if-ge v4, p1, :cond_2

    .line 45252
    iget-object v1, p0, LX/0MB;->e:[B

    iget v2, p0, LX/0MB;->f:I

    move-object v0, p0

    move v3, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, LX/0MB;->a(LX/0MB;[BIIIZ)I

    move-result v4

    .line 45253
    const/4 v0, -0x1

    if-ne v4, v0, :cond_1

    .line 45254
    const/4 v0, 0x0

    .line 45255
    :goto_0
    return v0

    .line 45256
    :cond_2
    iget v0, p0, LX/0MB;->f:I

    add-int/2addr v0, p1

    iput v0, p0, LX/0MB;->f:I

    .line 45257
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d([BII)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 45258
    iget v1, p0, LX/0MB;->g:I

    if-nez v1, :cond_0

    .line 45259
    :goto_0
    return v0

    .line 45260
    :cond_0
    iget v1, p0, LX/0MB;->g:I

    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 45261
    iget-object v2, p0, LX/0MB;->e:[B

    invoke-static {v2, v0, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45262
    invoke-direct {p0, v1}, LX/0MB;->f(I)V

    move v0, v1

    .line 45263
    goto :goto_0
.end method

.method public static e(LX/0MB;I)I
    .locals 1

    .prologue
    .line 45264
    iget v0, p0, LX/0MB;->g:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 45265
    invoke-direct {p0, v0}, LX/0MB;->f(I)V

    .line 45266
    return v0
.end method

.method private f(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 45267
    iget v0, p0, LX/0MB;->g:I

    sub-int/2addr v0, p1

    iput v0, p0, LX/0MB;->g:I

    .line 45268
    iput v3, p0, LX/0MB;->f:I

    .line 45269
    iget-object v0, p0, LX/0MB;->e:[B

    iget-object v1, p0, LX/0MB;->e:[B

    iget v2, p0, LX/0MB;->g:I

    invoke-static {v0, p1, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45270
    return-void
.end method

.method public static g(LX/0MB;I)V
    .locals 4

    .prologue
    .line 45271
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 45272
    iget-wide v0, p0, LX/0MB;->d:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0MB;->d:J

    .line 45273
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 45221
    invoke-static {p0, p1}, LX/0MB;->e(LX/0MB;I)I

    move-result v0

    .line 45222
    if-nez v0, :cond_0

    .line 45223
    sget-object v1, LX/0MB;->a:[B

    sget-object v0, LX/0MB;->a:[B

    array-length v0, v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    const/4 v5, 0x1

    move-object v0, p0

    move v4, v2

    invoke-static/range {v0 .. v5}, LX/0MB;->a(LX/0MB;[BIIIZ)I

    move-result v0

    .line 45224
    :cond_0
    invoke-static {p0, v0}, LX/0MB;->g(LX/0MB;I)V

    .line 45225
    return v0
.end method

.method public final a([BII)I
    .locals 6

    .prologue
    .line 45226
    invoke-direct {p0, p1, p2, p3}, LX/0MB;->d([BII)I

    move-result v0

    .line 45227
    if-nez v0, :cond_0

    .line 45228
    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, LX/0MB;->a(LX/0MB;[BIIIZ)I

    move-result v0

    .line 45229
    :cond_0
    invoke-static {p0, v0}, LX/0MB;->g(LX/0MB;I)V

    .line 45230
    return v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 45219
    const/4 v0, 0x0

    iput v0, p0, LX/0MB;->f:I

    .line 45220
    return-void
.end method

.method public final a([BIIZ)Z
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 45214
    invoke-direct {p0, p1, p2, p3}, LX/0MB;->d([BII)I

    move-result v4

    .line 45215
    :goto_0
    if-ge v4, p3, :cond_0

    if-eq v4, v6, :cond_0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    .line 45216
    invoke-static/range {v0 .. v5}, LX/0MB;->a(LX/0MB;[BIIIZ)I

    move-result v4

    goto :goto_0

    .line 45217
    :cond_0
    invoke-static {p0, v4}, LX/0MB;->g(LX/0MB;I)V

    .line 45218
    if-eq v4, v6, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 45213
    iget-wide v0, p0, LX/0MB;->d:J

    iget v2, p0, LX/0MB;->f:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final b(I)V
    .locals 8

    .prologue
    .line 45206
    const/4 v0, 0x0

    const/4 v7, -0x1

    .line 45207
    invoke-static {p0, p1}, LX/0MB;->e(LX/0MB;I)I

    move-result v5

    .line 45208
    :goto_0
    if-ge v5, p1, :cond_0

    if-eq v5, v7, :cond_0

    .line 45209
    sget-object v2, LX/0MB;->a:[B

    neg-int v3, v5

    sget-object v1, LX/0MB;->a:[B

    array-length v1, v1

    add-int/2addr v1, v5

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    move-object v1, p0

    move v6, v0

    invoke-static/range {v1 .. v6}, LX/0MB;->a(LX/0MB;[BIIIZ)I

    move-result v5

    goto :goto_0

    .line 45210
    :cond_0
    invoke-static {p0, v5}, LX/0MB;->g(LX/0MB;I)V

    .line 45211
    if-eq v5, v7, :cond_1

    .line 45212
    :goto_1
    return-void

    :cond_1
    goto :goto_1
.end method

.method public final b([BII)V
    .locals 1

    .prologue
    .line 45204
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/0MB;->a([BIIZ)Z

    .line 45205
    return-void
.end method

.method public final b([BIIZ)Z
    .locals 2

    .prologue
    .line 45199
    invoke-direct {p0, p3, p4}, LX/0MB;->b(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45200
    const/4 v0, 0x0

    .line 45201
    :goto_0
    return v0

    .line 45202
    :cond_0
    iget-object v0, p0, LX/0MB;->e:[B

    iget v1, p0, LX/0MB;->f:I

    sub-int/2addr v1, p3

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45203
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 45198
    iget-wide v0, p0, LX/0MB;->d:J

    return-wide v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 45193
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/0MB;->b(IZ)Z

    .line 45194
    return-void
.end method

.method public final c([BII)V
    .locals 1

    .prologue
    .line 45196
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/0MB;->b([BIIZ)Z

    .line 45197
    return-void
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 45195
    iget-wide v0, p0, LX/0MB;->c:J

    return-wide v0
.end method
