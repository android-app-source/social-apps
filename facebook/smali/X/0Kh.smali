.class public LX/0Kh;
.super LX/0Kg;
.source ""

# interfaces
.implements LX/0GW;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final b:LX/0Ki;

.field private c:J

.field private d:Z


# direct methods
.method public constructor <init>(LX/0L9;Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;Landroid/os/Handler;LX/0Ka;)V
    .locals 1

    .prologue
    .line 41773
    invoke-direct {p0, p1, p3, p4}, LX/0Kg;-><init>(LX/0L9;Landroid/os/Handler;LX/0Ka;)V

    .line 41774
    new-instance v0, LX/0Ki;

    invoke-direct {v0, p2}, LX/0Ki;-><init>(Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;)V

    iput-object v0, p0, LX/0Kh;->b:LX/0Ki;

    .line 41775
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 41776
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    invoke-virtual {v0}, LX/0Ki;->g()J

    move-result-wide v0

    .line 41777
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 41778
    iget-boolean v2, p0, LX/0Kh;->d:Z

    if-eqz v2, :cond_1

    :goto_0
    iput-wide v0, p0, LX/0Kh;->c:J

    .line 41779
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Kh;->d:Z

    .line 41780
    :cond_0
    iget-wide v0, p0, LX/0Kh;->c:J

    return-wide v0

    .line 41781
    :cond_1
    iget-wide v2, p0, LX/0Kh;->c:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 41782
    packed-switch p1, :pswitch_data_0

    .line 41783
    invoke-super {p0, p1, p2}, LX/0Kg;->a(ILjava/lang/Object;)V

    .line 41784
    :goto_0
    return-void

    .line 41785
    :pswitch_0
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 41786
    iget-object v2, v0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v2, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a(F)V

    .line 41787
    goto :goto_0

    .line 41788
    :pswitch_1
    check-cast p2, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;

    .line 41789
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    .line 41790
    iget-object v1, v0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    move-object v0, v1

    .line 41791
    iget-object v1, p2, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->d:[F

    invoke-virtual {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a([F)V

    goto :goto_0

    .line 41792
    :pswitch_2
    check-cast p2, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;

    .line 41793
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    .line 41794
    iget-object v1, v0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    move-object v0, v1

    .line 41795
    iget-boolean v1, p2, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->a:Z

    invoke-virtual {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a(Z)V

    .line 41796
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    .line 41797
    iget-object v1, v0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    move-object v0, v1

    .line 41798
    iget-wide v2, p2, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->b:D

    double-to-float v1, v2

    iget-wide v2, p2, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->c:D

    double-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a(FF)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/media/MediaFormat;)V
    .locals 3

    .prologue
    .line 41818
    :try_start_0
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    const/4 p0, 0x1

    .line 41819
    const-string v1, "sample-rate"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, LX/0Ki;->f:I

    .line 41820
    const-string v1, "channel-count"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, LX/0Ki;->g:I

    .line 41821
    iget-object v1, v0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    iget v2, v0, LX/0Ki;->f:I

    int-to-float v2, v2

    invoke-virtual {v1, v2, p0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a(FZ)V

    .line 41822
    iput-boolean p0, v0, LX/0Ki;->c:Z
    :try_end_0
    .catch LX/0Kd; {:try_start_0 .. :try_end_0} :catch_0

    .line 41823
    return-void

    .line 41824
    :catch_0
    move-exception v0

    .line 41825
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(LX/0M7;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 41799
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    invoke-virtual {v0}, LX/0Ki;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41800
    :try_start_0
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    .line 41801
    iget-object v1, v0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a()V

    .line 41802
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0Ki;->c:Z
    :try_end_0
    .catch LX/0Kd; {:try_start_0 .. :try_end_0} :catch_0

    .line 41803
    iget v0, p0, LX/0GT;->a:I

    move v0, v0

    .line 41804
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 41805
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    invoke-virtual {v0}, LX/0Ki;->d()V

    .line 41806
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    iget-object v1, p1, LX/0M7;->b:Ljava/nio/ByteBuffer;

    iget-object v2, p1, LX/0M7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    iget-object v3, p1, LX/0M7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    iget-wide v4, p1, LX/0M6;->a:J

    invoke-virtual/range {v0 .. v5}, LX/0Ki;->a(Ljava/nio/ByteBuffer;IIJ)I
    :try_end_1
    .catch LX/09Y; {:try_start_1 .. :try_end_1} :catch_2
    .catch LX/0Kf; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 41807
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_1

    .line 41808
    iput-boolean v6, p0, LX/0Kh;->d:Z

    .line 41809
    :cond_1
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 41810
    iget-object v0, p0, LX/0Kg;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->f:I

    move v0, v6

    .line 41811
    :goto_0
    return v0

    .line 41812
    :catch_0
    move-exception v0

    .line 41813
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 41814
    :catch_1
    move-exception v0

    .line 41815
    :goto_1
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 41816
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 41817
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 41741
    invoke-super {p0}, LX/0Kg;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    invoke-virtual {v0}, LX/0Ki;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(J)V
    .locals 5

    .prologue
    .line 41762
    invoke-super {p0, p1, p2}, LX/0Kg;->c(J)V

    .line 41763
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    const-wide/16 v3, 0x0

    const/4 v1, 0x0

    .line 41764
    iput-wide v3, v0, LX/0Ki;->j:J

    .line 41765
    iput v1, v0, LX/0Ki;->d:I

    .line 41766
    iput v1, v0, LX/0Ki;->h:I

    .line 41767
    iput-wide v3, v0, LX/0Ki;->i:J

    .line 41768
    invoke-static {v0}, LX/0Ki;->l(LX/0Ki;)V

    .line 41769
    iget-object v1, v0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->h()V

    .line 41770
    iput-wide p1, p0, LX/0Kh;->c:J

    .line 41771
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Kh;->d:Z

    .line 41772
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 41761
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    invoke-virtual {v0}, LX/0Ki;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, LX/0Kg;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/0GW;
    .locals 0

    .prologue
    .line 41760
    return-object p0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 41757
    invoke-super {p0}, LX/0Kg;->h()V

    .line 41758
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    invoke-virtual {v0}, LX/0Ki;->d()V

    .line 41759
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 41752
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    .line 41753
    invoke-static {v0}, LX/0Ki;->l(LX/0Ki;)V

    .line 41754
    iget-object v1, v0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->f()V

    .line 41755
    invoke-super {p0}, LX/0Kg;->i()V

    .line 41756
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 41746
    :try_start_0
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    .line 41747
    iget-object v1, v0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->i()V

    .line 41748
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/0Ki;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41749
    invoke-super {p0}, LX/0Kg;->j()V

    .line 41750
    return-void

    .line 41751
    :catchall_0
    move-exception v0

    invoke-super {p0}, LX/0Kg;->j()V

    throw v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 41742
    iget-object v0, p0, LX/0Kh;->b:LX/0Ki;

    .line 41743
    invoke-virtual {v0}, LX/0Ki;->b()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 41744
    iget-object p0, v0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {p0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->d()V

    .line 41745
    :cond_0
    return-void
.end method
