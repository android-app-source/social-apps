.class public LX/0B1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0B2;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:LX/072;

.field public final b:LX/07S;

.field public final c:I

.field public final d:J

.field private e:Ljava/lang/Throwable;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private volatile f:LX/05u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05u",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/072;LX/07S;IJ)V
    .locals 2

    .prologue
    .line 26077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26078
    const/4 v0, 0x0

    iput-object v0, p0, LX/0B1;->e:Ljava/lang/Throwable;

    .line 26079
    iput-object p1, p0, LX/0B1;->a:LX/072;

    .line 26080
    iput-object p2, p0, LX/0B1;->b:LX/07S;

    .line 26081
    iput p3, p0, LX/0B1;->c:I

    .line 26082
    iput-wide p4, p0, LX/0B1;->d:J

    .line 26083
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 26076
    iget v0, p0, LX/0B1;->c:I

    return v0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 26069
    iget-object v0, p0, LX/0B1;->f:LX/05u;

    if-nez v0, :cond_0

    .line 26070
    :goto_0
    return-void

    .line 26071
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/0B1;->f:LX/05u;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v2, -0x156d978c

    invoke-static {v0, p1, p2, v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26072
    :goto_1
    monitor-enter p0

    .line 26073
    :try_start_1
    iget-object v0, p0, LX/0B1;->e:Ljava/lang/Throwable;

    if-eqz v0, :cond_1

    .line 26074
    new-instance v0, Ljava/util/concurrent/ExecutionException;

    iget-object v1, p0, LX/0B1;->e:Ljava/lang/Throwable;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 26075
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public final a(LX/05u;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05u",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 26084
    invoke-static {p1}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26085
    iget-object v0, p0, LX/0B1;->f:LX/05u;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/01n;->b(Z)V

    .line 26086
    iput-object p1, p0, LX/0B1;->f:LX/05u;

    .line 26087
    return-void

    .line 26088
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 26063
    monitor-enter p0

    .line 26064
    :try_start_0
    iput-object p1, p0, LX/0B1;->e:Ljava/lang/Throwable;

    .line 26065
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26066
    invoke-virtual {p0}, LX/0B1;->b()V

    .line 26067
    return-void

    .line 26068
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 26060
    iget-object v0, p0, LX/0B1;->f:LX/05u;

    if-eqz v0, :cond_0

    .line 26061
    iget-object v0, p0, LX/0B1;->f:LX/05u;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/05u;->cancel(Z)Z

    .line 26062
    :cond_0
    return-void
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 26059
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0B1;->e:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 26058
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MqttOperation{mResponseType="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0B1;->b:LX/07S;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mOperationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0B1;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCreationTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/0B1;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
