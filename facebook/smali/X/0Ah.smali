.class public abstract LX/0Ah;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ai;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:LX/0AR;

.field public final d:J

.field public final e:Ljava/lang/String;

.field public final f:LX/0Au;

.field public final g:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;JLX/0AR;LX/0Ag;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 24910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24911
    iput-object p1, p0, LX/0Ah;->a:Ljava/lang/String;

    .line 24912
    iput-wide p2, p0, LX/0Ah;->b:J

    .line 24913
    iput-object p4, p0, LX/0Ah;->c:LX/0AR;

    .line 24914
    if-eqz p6, :cond_0

    :goto_0
    iput-object p6, p0, LX/0Ah;->e:Ljava/lang/String;

    .line 24915
    invoke-virtual {p5, p0}, LX/0Ag;->a(LX/0Ah;)LX/0Au;

    move-result-object v0

    iput-object v0, p0, LX/0Ah;->f:LX/0Au;

    .line 24916
    iget-wide v2, p5, LX/0Ag;->c:J

    const-wide/32 v4, 0xf4240

    iget-wide v6, p5, LX/0Ag;->b:J

    invoke-static/range {v2 .. v7}, LX/08x;->a(JJJ)J

    move-result-wide v2

    move-wide v0, v2

    .line 24917
    iput-wide v0, p0, LX/0Ah;->d:J

    .line 24918
    iput-object p7, p0, LX/0Ah;->g:Ljava/lang/String;

    .line 24919
    return-void

    .line 24920
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p4, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p6

    goto :goto_0
.end method

.method public synthetic constructor <init>(Ljava/lang/String;JLX/0AR;LX/0Ag;Ljava/lang/String;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 24921
    invoke-direct/range {p0 .. p7}, LX/0Ah;-><init>(Ljava/lang/String;JLX/0AR;LX/0Ag;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;JLX/0AR;LX/0Ag;Ljava/lang/String;)LX/0Ah;
    .locals 9

    .prologue
    .line 24922
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v7, p5

    invoke-static/range {v1 .. v7}, LX/0Ah;->a(Ljava/lang/String;JLX/0AR;LX/0Ag;Ljava/lang/String;Ljava/lang/String;)LX/0Ah;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;JLX/0AR;LX/0Ag;Ljava/lang/String;Ljava/lang/String;)LX/0Ah;
    .locals 11

    .prologue
    .line 24923
    instance-of v0, p4, LX/0Aj;

    if-eqz v0, :cond_0

    .line 24924
    new-instance v0, LX/0Aw;

    move-object v5, p4

    check-cast v5, LX/0Aj;

    const-wide/16 v7, -0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object/from16 v6, p5

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v9}, LX/0Aw;-><init>(Ljava/lang/String;JLX/0AR;LX/0Aj;Ljava/lang/String;JLjava/lang/String;)V

    .line 24925
    :goto_0
    return-object v0

    .line 24926
    :cond_0
    instance-of v0, p4, LX/0BV;

    if-eqz v0, :cond_1

    .line 24927
    new-instance v0, LX/0BW;

    move-object v5, p4

    check-cast v5, LX/0BV;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v7}, LX/0BW;-><init>(Ljava/lang/String;JLX/0AR;LX/0BV;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 24928
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "segmentBase must be of type SingleSegmentBase or MultiSegmentBase"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final Y_()LX/0AR;
    .locals 1

    .prologue
    .line 24929
    iget-object v0, p0, LX/0Ah;->c:LX/0AR;

    return-object v0
.end method

.method public final c()LX/0Au;
    .locals 1

    .prologue
    .line 24930
    iget-object v0, p0, LX/0Ah;->f:LX/0Au;

    return-object v0
.end method

.method public abstract e()LX/0Au;
.end method

.method public abstract f()LX/0BX;
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24931
    iget-object v0, p0, LX/0Ah;->e:Ljava/lang/String;

    return-object v0
.end method
