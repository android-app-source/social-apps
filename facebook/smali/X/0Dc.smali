.class public abstract LX/0Dc;
.super Landroid/os/Binder;
.source ""

# interfaces
.implements LX/0Da;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30822
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 30823
    const-string v0, "com.facebook.browser.lite.ipc.BrowserLiteJSBridgeCallback"

    invoke-virtual {p0, p0, v0}, LX/0Dc;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 30824
    return-void
.end method

.method public static a(Landroid/os/IBinder;)LX/0Da;
    .locals 2

    .prologue
    .line 30825
    if-nez p0, :cond_0

    .line 30826
    const/4 v0, 0x0

    .line 30827
    :goto_0
    return-object v0

    .line 30828
    :cond_0
    const-string v0, "com.facebook.browser.lite.ipc.BrowserLiteJSBridgeCallback"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30829
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/0Da;

    if-eqz v1, :cond_1

    .line 30830
    check-cast v0, LX/0Da;

    goto :goto_0

    .line 30831
    :cond_1
    new-instance v0, LX/0Db;

    invoke-direct {v0, p0}, LX/0Db;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public final asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 30832
    return-object p0
.end method

.method public final onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 30833
    sparse-switch p1, :sswitch_data_0

    .line 30834
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 30835
    :sswitch_0
    const-string v0, "com.facebook.browser.lite.ipc.BrowserLiteJSBridgeCallback"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    .line 30836
    goto :goto_0

    .line 30837
    :sswitch_1
    const-string v0, "com.facebook.browser.lite.ipc.BrowserLiteJSBridgeCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30838
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 30839
    sget-object v0, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    move-object v1, v0

    .line 30840
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 30841
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 30842
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 30843
    :goto_2
    invoke-virtual {p0, v1, v4, v0}, LX/0Dc;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;ILandroid/os/Bundle;)V

    .line 30844
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    .line 30845
    goto :goto_0

    :cond_0
    move-object v1, v2

    .line 30846
    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 30847
    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
