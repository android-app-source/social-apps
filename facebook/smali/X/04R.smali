.class public LX/04R;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static DEFAULT_TRACE_COUNT_LIMIT:I = 0x0

.field public static processNameByAms:Ljava/lang/String;

.field public static processNameByAmsReady:Z

.field private static final sDeviceSpecificFields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sPackageManagerWrapper:LX/01b;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13700
    const/4 v0, 0x5

    sput v0, LX/04R;->DEFAULT_TRACE_COUNT_LIMIT:I

    .line 13701
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    sput-object v0, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static varargs collectCommandOutput([Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 13682
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 13683
    new-instance v1, Ljava/lang/ProcessBuilder;

    invoke-direct {v1, p0}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/ProcessBuilder;->redirectErrorStream(Z)Ljava/lang/ProcessBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;

    move-result-object v2

    .line 13684
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 13685
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x0

    .line 13686
    const/16 v4, 0x1000

    :try_start_1
    new-array v4, v4, [C

    .line 13687
    :goto_0
    const/4 v5, 0x0

    const/16 v6, 0x1000

    invoke-virtual {v3, v4, v5, v6}, Ljava/io/InputStreamReader;->read([CII)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 13688
    const/4 v6, 0x0

    invoke-virtual {v0, v4, v6, v5}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    goto :goto_0

    .line 13689
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 13690
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    if-eqz v1, :cond_1

    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_2
    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 13691
    :catchall_1
    move-exception v0

    invoke-virtual {v2}, Ljava/lang/Process;->destroy()V

    throw v0

    .line 13692
    :cond_0
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 13693
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Process;->waitFor()I
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 13694
    :goto_3
    invoke-virtual {v2}, Ljava/lang/Process;->destroy()V

    .line 13695
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 13696
    :catch_1
    move-exception v3

    :try_start_7
    invoke-static {v1, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_1
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    goto :goto_2

    .line 13697
    :catch_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_3

    .line 13698
    :catchall_2
    move-exception v0

    goto :goto_1
.end method

.method public static gatherCrashData(LX/009;LX/00K;Ljava/lang/String;Ljava/lang/Throwable;LX/01l;Ljava/io/Writer;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/009;",
            "Lcom/facebook/acra/config/AcraReportingConfig;",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            "LX/01l;",
            "Ljava/io/Writer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 13628
    const/4 v1, 0x1

    move v0, v1

    .line 13629
    if-eqz v0, :cond_0

    .line 13630
    :try_start_0
    const-string v0, "UID"

    invoke-virtual {p0}, LX/009;->getUserId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p4, p5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 13631
    :cond_0
    :goto_0
    const/4 v1, 0x1

    move v0, v1

    .line 13632
    if-eqz v0, :cond_1

    .line 13633
    :try_start_1
    const-string v0, "STACK_TRACE"

    invoke-static {v0, p2, p4, p5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 13634
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/009;->getConstantFields()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 13635
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 13636
    const/4 v2, 0x1

    move v1, v2

    .line 13637
    if-eqz v1, :cond_2

    .line 13638
    :try_start_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v2, p4, p5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 13639
    :catch_0
    move-exception v1

    .line 13640
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p4, v0, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 13641
    :catch_1
    move-exception v0

    .line 13642
    const-string v1, "UID"

    invoke-static {p4, v1, v0}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 13643
    :catch_2
    move-exception v0

    .line 13644
    const-string v1, "STACK_TRACE"

    invoke-static {p4, v1, v0}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 13645
    :cond_3
    if-eqz p6, :cond_4

    .line 13646
    invoke-interface {p6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 13647
    :try_start_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v2, p4, p5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_3

    .line 13648
    :catch_3
    move-exception v1

    .line 13649
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p4, v0, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 13650
    :cond_4
    invoke-static {p0, p1, p3, p4, p5}, LX/04R;->populateCrashTimeData(LX/009;LX/00K;Ljava/lang/Throwable;LX/01l;Ljava/io/Writer;)V

    .line 13651
    invoke-static {p1}, LX/04R;->getConstantDeviceData(LX/00K;)Ljava/util/Map;

    move-result-object v0

    .line 13652
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 13653
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 13654
    const/4 v2, 0x1

    move v1, v2

    .line 13655
    if-eqz v1, :cond_5

    .line 13656
    :try_start_4
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v2, p4, p5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_4

    .line 13657
    :catch_4
    move-exception v1

    .line 13658
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p4, v0, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 13659
    :cond_6
    invoke-virtual {p0}, LX/009;->getCustomFieldsSnapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 13660
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 13661
    const/4 v2, 0x1

    move v1, v2

    .line 13662
    if-eqz v1, :cond_7

    .line 13663
    :try_start_5
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v2, p4, p5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_5

    .line 13664
    :catch_5
    move-exception v1

    .line 13665
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p4, v0, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 13666
    :cond_8
    invoke-virtual {p0}, LX/009;->getLazyCustomFieldsSnapshot()Ljava/util/Map;

    move-result-object v0

    .line 13667
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 13668
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 13669
    const/4 v2, 0x1

    move v1, v2

    .line 13670
    if-eqz v1, :cond_9

    .line 13671
    :try_start_6
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/00T;

    invoke-interface {v2, p3}, LX/00T;->getCustomData(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p4, p5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_6

    .line 13672
    :catch_6
    move-exception v1

    .line 13673
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p4, v0, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 13674
    :cond_a
    iget-object v0, p4, LX/01l;->fieldFailures:Ljava/util/ArrayList;

    if-eqz v0, :cond_c

    .line 13675
    const/4 v1, 0x1

    move v0, v1

    .line 13676
    if-eqz v0, :cond_b

    .line 13677
    :try_start_7
    const-string v0, "FIELD_FAILURES"

    const-string v1, "\n"

    iget-object v2, p4, LX/01l;->fieldFailures:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p4, p5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_7

    .line 13678
    :cond_b
    :goto_7
    const/4 v0, 0x0

    iput-object v0, p4, LX/01l;->fieldFailures:Ljava/util/ArrayList;

    .line 13679
    :cond_c
    return-void

    .line 13680
    :catch_7
    move-exception v0

    .line 13681
    :try_start_8
    sget-object v1, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v2, "error attaching field failures to report: continuing"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_8

    goto :goto_7

    :catch_8
    goto :goto_7
.end method

.method private static getAvailableInternalMemorySize()J
    .locals 4

    .prologue
    .line 13702
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    .line 13703
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 13704
    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v2, v0

    .line 13705
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    .line 13706
    mul-long/2addr v0, v2

    .line 13707
    :goto_0
    return-wide v0

    :catch_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static getConstantDeviceData(LX/00K;)Ljava/util/Map;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/acra/config/AcraReportingConfig;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13457
    iget-object v0, p0, LX/00K;->mApplicationContext:Landroid/content/Context;

    move-object v1, v0

    .line 13458
    sget-object v2, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    monitor-enter v2

    .line 13459
    :try_start_0
    sget-object v0, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 13460
    const/4 v3, 0x1

    move v0, v3

    .line 13461
    if-eqz v0, :cond_1

    .line 13462
    sget-object v0, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v3, "BUILD"

    const-class v4, Landroid/os/Build;

    .line 13463
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 13464
    invoke-virtual {v4}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v7

    .line 13465
    array-length v8, v7

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v8, :cond_0

    aget-object v9, v7, v5

    .line 13466
    invoke-virtual {v9}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13467
    const/4 v10, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v9, v10}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 13468
    :goto_1
    :try_start_2
    const-string v9, "\n"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13469
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 13470
    :catch_0
    const-string v9, "N/A"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 13471
    :cond_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 13472
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13473
    :cond_1
    const/4 v3, 0x1

    move v0, v3

    .line 13474
    if-eqz v0, :cond_2

    .line 13475
    sget-object v0, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v3, "JAIL_BROKEN"

    .line 13476
    sget-object v4, Landroid/os/Build;->TAGS:Ljava/lang/String;

    .line 13477
    if-eqz v4, :cond_18

    const-string v5, "test-keys"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 13478
    const-string v4, "yes"

    .line 13479
    :goto_2
    move-object v4, v4

    .line 13480
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13481
    :cond_2
    const/4 v3, 0x1

    move v0, v3

    .line 13482
    if-eqz v0, :cond_3

    .line 13483
    sget-object v0, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v3, "INSTALLATION_ID"

    invoke-static {v1}, LX/04S;->id(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13484
    :cond_3
    const/4 v3, 0x1

    move v0, v3

    .line 13485
    if-eqz v0, :cond_4

    .line 13486
    sget-object v0, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v3, "TOTAL_MEM_SIZE"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 13487
    :try_start_3
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v6

    .line 13488
    new-instance v7, Landroid/os/StatFs;

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 13489
    invoke-virtual {v7}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v8, v6

    .line 13490
    invoke-virtual {v7}, Landroid/os/StatFs;->getBlockCount()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    move-result v6

    int-to-long v6, v6

    .line 13491
    mul-long/2addr v6, v8

    .line 13492
    :goto_3
    move-wide v4, v6

    .line 13493
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13494
    :cond_4
    const/4 v3, 0x1

    move v0, v3

    .line 13495
    if-eqz v0, :cond_5

    invoke-static {v1}, LX/04R;->getPackageManagerWrapper(Landroid/content/Context;)LX/01b;

    move-result-object v0

    const-string v3, "android.permission.READ_PHONE_STATE"

    invoke-virtual {v0, v3}, LX/01b;->hasPermission(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 13496
    const-string v0, "phone"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 13497
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 13498
    if-eqz v0, :cond_5

    .line 13499
    sget-object v3, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v4, "DEVICE_ID"

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13500
    :cond_5
    const/4 v3, 0x1

    move v0, v3

    .line 13501
    if-eqz v0, :cond_6

    .line 13502
    const-string v0, "window"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 13503
    sget-object v3, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v4, "DISPLAY"

    const/16 v9, 0xa

    .line 13504
    if-nez v0, :cond_1c

    .line 13505
    const-string v5, ""

    .line 13506
    :goto_4
    move-object v0, v5

    .line 13507
    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13508
    :cond_6
    const/4 v3, 0x1

    move v0, v3

    .line 13509
    if-eqz v0, :cond_a

    .line 13510
    sget-object v0, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v3, "ENVIRONMENT"

    const-class v4, Landroid/os/Environment;

    .line 13511
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 13512
    invoke-virtual {v4}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v7

    .line 13513
    array-length v8, v7

    const/4 v5, 0x0

    :goto_5
    if-ge v5, v8, :cond_9

    aget-object v9, v7, v5

    .line 13514
    invoke-virtual {v9}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v10

    array-length v10, v10

    if-nez v10, :cond_8

    invoke-virtual {v9}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "get"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_7

    invoke-virtual {v9}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "is"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    :cond_7
    invoke-virtual {v9}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "getClass"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 13515
    :try_start_5
    invoke-virtual {v9}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v11, 0x3d

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v9, v11, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_9
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_8
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 13516
    :cond_8
    :goto_6
    :try_start_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 13517
    :cond_9
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 13518
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13519
    :cond_a
    const/4 v3, 0x1

    move v0, v3

    .line 13520
    if-eqz v0, :cond_d

    .line 13521
    sget-object v0, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v3, "DEVICE_FEATURES"

    const/4 v5, 0x0

    .line 13522
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 13523
    :try_start_7
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 13524
    const-class v6, Landroid/content/pm/PackageManager;

    const-string v8, "getSystemAvailableFeatures"

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 13525
    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v6, v4, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    .line 13526
    if-eqz v4, :cond_c

    .line 13527
    array-length v8, v4

    move v6, v5

    :goto_7
    if-ge v6, v8, :cond_c

    aget-object v9, v4, v6

    .line 13528
    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v10, "name"

    invoke-virtual {v5, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 13529
    if-eqz v5, :cond_b

    .line 13530
    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 13531
    :goto_8
    const-string v5, "\n"

    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 13532
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_7

    .line 13533
    :cond_b
    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v10, "getGlEsVersion"

    const/4 v11, 0x0

    invoke-virtual {v5, v10, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 13534
    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-virtual {v5, v9, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 13535
    const-string v9, "glEsVersion = "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 13536
    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    goto :goto_8

    .line 13537
    :catch_1
    move-exception v4

    .line 13538
    sget-object v5, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Couldn\'t retrieve device features for "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 13539
    const-string v5, "Could not retrieve data: "

    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 13540
    invoke-virtual {v4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 13541
    :cond_c
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 13542
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13543
    :cond_d
    const/4 v3, 0x1

    move v0, v3

    .line 13544
    if-eqz v0, :cond_10

    .line 13545
    sget-object v0, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v3, "SETTINGS_SYSTEM"

    .line 13546
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 13547
    const-class v4, Landroid/provider/Settings$System;

    invoke-virtual {v4}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v7

    .line 13548
    array-length v8, v7

    const/4 v4, 0x0

    move v5, v4

    :goto_9
    if-ge v5, v8, :cond_f

    aget-object v9, v7, v5

    .line 13549
    const-class v4, Ljava/lang/Deprecated;

    invoke-virtual {v9, v4}, Ljava/lang/reflect/Field;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_e

    invoke-virtual {v9}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v4

    const-class v10, Ljava/lang/String;

    if-ne v4, v10, :cond_e
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 13550
    :try_start_9
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const/4 v4, 0x0

    invoke-virtual {v9, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v10, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 13551
    if-eqz v4, :cond_e

    .line 13552
    invoke-virtual {v9}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "\n"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_b
    .catch Ljava/lang/IllegalAccessException; {:try_start_9 .. :try_end_9} :catch_a
    .catch Ljava/lang/SecurityException; {:try_start_9 .. :try_end_9} :catch_c
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 13553
    :cond_e
    :goto_a
    :try_start_a
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_9

    .line 13554
    :catch_2
    move-exception v4

    .line 13555
    :goto_b
    sget-object v9, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v10, "Error : "

    invoke-static {v9, v10, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_a

    .line 13556
    :cond_f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 13557
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13558
    :cond_10
    const/4 v3, 0x1

    move v0, v3

    .line 13559
    if-eqz v0, :cond_14

    .line 13560
    sget-object v0, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v3, "SETTINGS_SECURE"

    .line 13561
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 13562
    const-class v4, Landroid/provider/Settings$Secure;

    invoke-virtual {v4}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v7

    .line 13563
    array-length v8, v7

    const/4 v4, 0x0

    move v5, v4

    :goto_c
    if-ge v5, v8, :cond_13

    aget-object v9, v7, v5

    .line 13564
    const-class v4, Ljava/lang/Deprecated;

    invoke-virtual {v9, v4}, Ljava/lang/reflect/Field;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_12

    invoke-virtual {v9}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v4

    const-class v10, Ljava/lang/String;

    if-ne v4, v10, :cond_12

    .line 13565
    if-eqz v9, :cond_11

    invoke-virtual {v9}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v10, "WIFI_AP"

    invoke-virtual {v4, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 13566
    :cond_11
    const/4 v4, 0x0

    .line 13567
    :goto_d
    move v4, v4

    .line 13568
    if-eqz v4, :cond_12
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 13569
    :try_start_b
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const/4 v4, 0x0

    invoke-virtual {v9, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v10, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 13570
    if-eqz v4, :cond_12

    .line 13571
    invoke-virtual {v9}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "\n"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_e
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_b} :catch_d
    .catch Ljava/lang/SecurityException; {:try_start_b .. :try_end_b} :catch_f
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 13572
    :cond_12
    :goto_e
    :try_start_c
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_c

    .line 13573
    :catch_3
    move-exception v4

    .line 13574
    :goto_f
    sget-object v9, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v10, "Error : "

    invoke-static {v9, v10, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_e

    .line 13575
    :cond_13
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 13576
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13577
    :cond_14
    const/4 v3, 0x1

    move v0, v3

    .line 13578
    if-eqz v0, :cond_15

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v0, v3, :cond_15

    .line 13579
    const-string v0, "activity"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 13580
    sget-object v3, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v4, "IS_LOW_RAM_DEVICE"

    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13581
    :cond_15
    const/4 v3, 0x1

    move v0, v3

    .line 13582
    if-eqz v0, :cond_16

    .line 13583
    sget-object v0, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v3, "ANDROID_RUNTIME"

    .line 13584
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-ge v4, v5, :cond_1e

    .line 13585
    const-string v4, "DALVIK"

    .line 13586
    :goto_10
    move-object v4, v4

    .line 13587
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13588
    :cond_16
    const/4 v3, 0x1

    move v0, v3

    .line 13589
    if-eqz v0, :cond_17

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_17

    .line 13590
    invoke-static {v1}, LX/04R;->getPackageManagerWrapper(Landroid/content/Context;)LX/01b;

    move-result-object v0

    const-string v1, "com.google.android.webview"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, LX/01b;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 13591
    if-eqz v0, :cond_17

    .line 13592
    sget-object v1, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    const-string v3, "WEBVIEW_VERSION"

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13593
    :cond_17
    sget-object v0, LX/04R;->sDeviceSpecificFields:Ljava/util/Map;

    monitor-exit v2

    return-object v0

    .line 13594
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    throw v0

    .line 13595
    :cond_18
    :try_start_d
    new-instance v4, Ljava/io/File;

    const-string v5, "/system/app/Superuser.apk"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 13596
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_19

    .line 13597
    const-string v4, "yes"
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :try_start_e
    goto/16 :goto_2

    .line 13598
    :catch_4
    move-exception v4

    .line 13599
    const-string v5, "ACRA"

    const-string v6, "Failed to find Superuser.pak"

    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 13600
    :cond_19
    invoke-static {}, Ljava/lang/System;->getenv()Ljava/util/Map;

    move-result-object v4

    .line 13601
    if-eqz v4, :cond_1b

    .line 13602
    const-string v5, "PATH"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 13603
    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 13604
    array-length v7, v6

    const/4 v4, 0x0

    move v5, v4

    :goto_11
    if-ge v5, v7, :cond_1b

    aget-object v4, v6, v5

    .line 13605
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "/su"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 13606
    :try_start_f
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 13607
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 13608
    const-string v4, "yes"
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_5
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :try_start_10
    goto/16 :goto_2

    .line 13609
    :catch_5
    move-exception v4

    .line 13610
    const-string v8, "ACRA"

    const-string v9, "Failed to find su binary in the PATH"

    invoke-static {v8, v9, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 13611
    :cond_1a
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_11

    .line 13612
    :cond_1b
    const-string v4, "no"

    goto/16 :goto_2
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :catch_6
    :try_start_11
    const-wide/16 v6, -0x1

    goto/16 :goto_3
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 13613
    :cond_1c
    :try_start_12
    new-instance v5, Landroid/util/DisplayMetrics;

    invoke-direct {v5}, Landroid/util/DisplayMetrics;-><init>()V

    .line 13614
    invoke-virtual {v0, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 13615
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 13616
    const-string v7, "width="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "height="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "pixelFormat="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/view/Display;->getPixelFormat()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "refreshRate="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/view/Display;->getRefreshRate()F

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "fps\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "metrics.density=x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "metrics.scaledDensity=x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Landroid/util/DisplayMetrics;->scaledDensity:F

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "metrics.widthPixels="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "metrics.heightPixels="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "metrics.xdpi="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Landroid/util/DisplayMetrics;->xdpi:F

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "metrics.ydpi="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v5, v5, Landroid/util/DisplayMetrics;->ydpi:F

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 13617
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_4
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :catch_7
    :try_start_13
    goto/16 :goto_6

    .line 13618
    :catch_8
    goto/16 :goto_6

    :catch_9
    goto/16 :goto_6
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 13619
    :catch_a
    :try_start_14
    move-exception v4

    goto/16 :goto_b

    :catch_b
    move-exception v4

    goto/16 :goto_b

    :catch_c
    move-exception v4

    goto/16 :goto_b
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 13620
    :catch_d
    :try_start_15
    move-exception v4

    goto/16 :goto_f

    :catch_e
    move-exception v4

    goto/16 :goto_f

    :catch_f
    move-exception v4

    goto/16 :goto_f

    :cond_1d
    const/4 v4, 0x1

    goto/16 :goto_d
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 13621
    :cond_1e
    const-string v4, "java.boot.class.path"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 13622
    if-eqz v4, :cond_20

    .line 13623
    const-string v5, "/system/framework/core-libart.jar"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 13624
    const-string v4, "ART"

    goto/16 :goto_10

    .line 13625
    :cond_1f
    const-string v5, "/system/framework/core.jar"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 13626
    const-string v4, "DALVIK"

    goto/16 :goto_10

    .line 13627
    :cond_20
    const-string v4, "UNKNOWN"

    goto/16 :goto_10
.end method

.method private static getPackageManagerWrapper(Landroid/content/Context;)LX/01b;
    .locals 2

    .prologue
    .line 13454
    sget-object v0, LX/04R;->sPackageManagerWrapper:LX/01b;

    if-nez v0, :cond_0

    .line 13455
    new-instance v0, LX/01b;

    sget-object v1, LX/00L;->LOG_TAG:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, LX/01b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, LX/04R;->sPackageManagerWrapper:LX/01b;

    .line 13456
    :cond_0
    sget-object v0, LX/04R;->sPackageManagerWrapper:LX/01b;

    return-object v0
.end method

.method public static getProcessNameFromAms(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13450
    invoke-static {p0}, LX/04R;->getProcessNameFromAmsOrNull(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 13451
    if-nez v0, :cond_0

    .line 13452
    const-string v0, "n/a"

    .line 13453
    :cond_0
    return-object v0
.end method

.method public static getProcessNameFromAmsOrNull(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 13434
    sget-boolean v0, LX/04R;->processNameByAmsReady:Z

    if-eqz v0, :cond_0

    .line 13435
    sget-object v0, LX/04R;->processNameByAms:Ljava/lang/String;

    .line 13436
    :goto_0
    return-object v0

    .line 13437
    :cond_0
    const/4 v0, 0x0

    sput-object v0, LX/04R;->processNameByAms:Ljava/lang/String;

    .line 13438
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 13439
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 13440
    if-nez v0, :cond_1

    .line 13441
    sget-object v0, LX/04R;->processNameByAms:Ljava/lang/String;

    goto :goto_0

    .line 13442
    :cond_1
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 13443
    if-nez v0, :cond_2

    .line 13444
    sget-object v0, LX/04R;->processNameByAms:Ljava/lang/String;

    goto :goto_0

    .line 13445
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 13446
    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v3, v1, :cond_3

    .line 13447
    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    sput-object v0, LX/04R;->processNameByAms:Ljava/lang/String;

    .line 13448
    :cond_4
    const/4 v0, 0x1

    sput-boolean v0, LX/04R;->processNameByAmsReady:Z

    .line 13449
    sget-object v0, LX/04R;->processNameByAms:Ljava/lang/String;

    goto :goto_0
.end method

.method public static noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 13427
    :try_start_0
    iget-object v0, p0, LX/01l;->fieldFailures:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 13428
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/01l;->fieldFailures:Ljava/util/ArrayList;

    .line 13429
    :cond_0
    iget-object v0, p0, LX/01l;->fieldFailures:Ljava/util/ArrayList;

    const-string v1, "%s: [%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 13430
    :goto_0
    return-void

    .line 13431
    :catch_0
    move-exception v0

    .line 13432
    :try_start_1
    sget-object v1, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ignoring failing remembering failure for custom field: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 13433
    :catch_1
    goto :goto_0
.end method

.method private static populateCrashTimeData(LX/009;LX/00K;Ljava/lang/Throwable;LX/01l;Ljava/io/Writer;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 13246
    invoke-virtual {p0}, LX/009;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 13247
    const/4 v5, 0x1

    move v1, v5

    .line 13248
    if-eqz v1, :cond_3

    .line 13249
    :try_start_0
    const-string v1, "PROCESS_NAME"

    .line 13250
    invoke-static {v4}, LX/04R;->getProcessNameFromAmsOrNull(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 13251
    if-nez v8, :cond_1d

    .line 13252
    const/4 v6, 0x0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 13253
    :try_start_1
    new-instance v5, Ljava/io/FileReader;

    const-string v7, "/proc/self/cmdline"

    invoke-direct {v5, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    .line 13254
    new-instance v7, Ljava/io/BufferedReader;

    const/16 v9, 0x80

    invoke-direct {v7, v5, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_16
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 13255
    :try_start_2
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_18
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    move-result-object v5

    .line 13256
    if-eqz v5, :cond_0
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 13257
    :try_start_4
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_19
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :try_start_5
    move-result-object v5

    .line 13258
    :cond_0
    :goto_0
    if-eqz v7, :cond_1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    .line 13259
    :try_start_6
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_17
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    .line 13260
    :cond_1
    :goto_1
    :try_start_7
    if-nez v5, :cond_2

    .line 13261
    const-string v5, ""

    .line 13262
    :cond_2
    move-object v5, v5

    .line 13263
    invoke-static {v1, v5, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1

    .line 13264
    :cond_3
    :goto_2
    const/4 v5, 0x1

    move v1, v5

    .line 13265
    if-eqz v1, :cond_4

    .line 13266
    :try_start_8
    const-string v1, "USER_APP_START_DATE"

    invoke-virtual {p0}, LX/009;->getAppStartDate()Landroid/text/format/Time;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_2

    .line 13267
    :cond_4
    :goto_3
    const/4 v5, 0x1

    move v1, v5

    .line 13268
    if-eqz v1, :cond_5

    .line 13269
    :try_start_9
    const-string v1, "PROCESS_UPTIME"

    .line 13270
    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    move-result-wide v8

    move-wide v6, v8

    .line 13271
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_3

    .line 13272
    :cond_5
    :goto_4
    const/4 v5, 0x1

    move v1, v5

    .line 13273
    if-eqz v1, :cond_6

    .line 13274
    :try_start_a
    const-string v1, "DEVICE_UPTIME"

    .line 13275
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    move-wide v6, v8

    .line 13276
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_4

    .line 13277
    :cond_6
    :goto_5
    const/4 v5, 0x1

    move v1, v5

    .line 13278
    if-eqz v1, :cond_7

    .line 13279
    :try_start_b
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 13280
    if-eqz v1, :cond_7

    .line 13281
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 13282
    const-string v5, "CRASH_CONFIGURATION"

    invoke-static {v1}, LX/04T;->toString(Landroid/content/res/Configuration;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_5

    .line 13283
    :cond_7
    :goto_6
    const/4 v5, 0x1

    move v1, v5

    .line 13284
    if-eqz v1, :cond_8

    .line 13285
    :try_start_c
    invoke-static {}, LX/04R;->getAvailableInternalMemorySize()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 13286
    const-string v5, "AVAILABLE_MEM_SIZE"

    invoke-static {v5, v1, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_6

    .line 13287
    :cond_8
    :goto_7
    const/4 v5, 0x1

    move v1, v5

    .line 13288
    if-eqz v1, :cond_9

    .line 13289
    :try_start_d
    const-string v1, "DUMPSYS_MEMINFO"

    invoke-static {v4}, LX/04U;->collectMemInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_7

    .line 13290
    :cond_9
    :goto_8
    const/4 v5, 0x1

    move v1, v5

    .line 13291
    if-eqz v1, :cond_a

    .line 13292
    :try_start_e
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 13293
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 13294
    const-string v5, "USER_CRASH_DATE"

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_8

    .line 13295
    :cond_a
    :goto_9
    const/4 v5, 0x1

    move v1, v5

    .line 13296
    if-eqz v1, :cond_b

    .line 13297
    :try_start_f
    instance-of v1, p2, Ljava/lang/OutOfMemoryError;

    if-eqz v1, :cond_1b

    .line 13298
    invoke-virtual {p0}, LX/009;->getActivityLogger()LX/01a;

    move-result-object v1

    invoke-virtual {v1}, LX/01a;->toString()Ljava/lang/String;

    move-result-object v1

    .line 13299
    :goto_a
    const-string v5, "ACTIVITY_LOG"

    invoke-static {v5, v1, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_9

    .line 13300
    :cond_b
    :goto_b
    const/4 v5, 0x1

    move v1, v5

    .line 13301
    if-eqz v1, :cond_c

    .line 13302
    :try_start_10
    const-string v1, "PROCESS_NAME_BY_AMS"

    invoke-static {v4}, LX/04R;->getProcessNameFromAms(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V

    .line 13303
    const/4 v1, 0x0

    sput-object v1, LX/04R;->processNameByAms:Ljava/lang/String;

    .line 13304
    const/4 v1, 0x0

    sput-boolean v1, LX/04R;->processNameByAmsReady:Z
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_a

    .line 13305
    :cond_c
    :goto_c
    const/4 v5, 0x1

    move v1, v5

    .line 13306
    if-eqz v1, :cond_d

    .line 13307
    :try_start_11
    const-string v1, "OPEN_FD_COUNT"

    invoke-static {}, LX/04V;->getOpenFDCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_b

    .line 13308
    :cond_d
    :goto_d
    :try_start_12
    invoke-static {}, LX/04V;->getOpenFDLimits()LX/04W;
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_c

    move-result-object v0

    .line 13309
    :goto_e
    const/4 v5, 0x1

    move v1, v5

    .line 13310
    if-eqz v1, :cond_e

    .line 13311
    if-eqz v0, :cond_e

    .line 13312
    :try_start_13
    const-string v1, "OPEN_FD_SOFT_LIMIT"

    iget-object v5, v0, LX/04W;->softLimit:Ljava/lang/String;

    invoke-static {v1, v5, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_d

    .line 13313
    :cond_e
    :goto_f
    const/4 v5, 0x1

    move v1, v5

    .line 13314
    if-eqz v1, :cond_f

    .line 13315
    :try_start_14
    const-string v1, "OPEN_FD_HARD_LIMIT"

    iget-object v0, v0, LX/04W;->hardLimit:Ljava/lang/String;

    invoke-static {v1, v0, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_e

    .line 13316
    :cond_f
    :goto_10
    const/4 v1, 0x1

    move v0, v1

    .line 13317
    if-eqz v0, :cond_14

    .line 13318
    :try_start_15
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_14

    .line 13319
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 13320
    const/4 v0, 0x0

    :goto_11
    sget-object v1, LX/04X;->ALL_PERMISSIONS_SAMPLES:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_13

    .line 13321
    sget-object v1, LX/04X;->ALL_PERMISSIONS_SAMPLES:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 13322
    const/4 v6, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_10
    :goto_12
    packed-switch v6, :pswitch_data_0

    .line 13323
    const/4 v6, 0x0

    :goto_13
    move-object v1, v6

    .line 13324
    const/16 v6, 0x2e

    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    .line 13325
    if-ltz v6, :cond_11

    .line 13326
    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 13327
    :cond_11
    sget-object v6, LX/04X;->ALL_PERMISSIONS_SAMPLES:[Ljava/lang/String;

    aget-object v6, v6, v0

    const/4 v7, 0x0
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_15 .. :try_end_15} :catch_f

    .line 13328
    :try_start_16
    invoke-virtual {v4, v6}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
    :try_end_16
    .catch Ljava/lang/RuntimeException; {:try_start_16 .. :try_end_16} :catch_1a
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_f

    :try_start_17
    move-result v8

    if-nez v8, :cond_12

    const/4 v7, 0x1

    .line 13329
    :cond_12
    :goto_14
    move v6, v7
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_f

    .line 13330
    :try_start_18
    invoke-virtual {v5, v1, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_18
    .catch Lorg/json/JSONException; {:try_start_18 .. :try_end_18} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_18} :catch_f

    .line 13331
    :goto_15
    :try_start_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 13332
    :catch_0
    move-exception v1

    .line 13333
    const-string v6, "PermissionsReporter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Caught JSONException "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_15

    .line 13334
    :cond_13
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 13335
    const-string v1, "RUNTIME_PERMISSIONS"

    invoke-static {v1, v0, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_19} :catch_f

    .line 13336
    :cond_14
    :goto_16
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1c

    move v1, v2

    .line 13337
    :goto_17
    if-eqz v1, :cond_17

    .line 13338
    iget-boolean v0, p1, LX/00K;->mIsInternalBuild:Z

    move v0, v0

    .line 13339
    if-eqz v0, :cond_17

    .line 13340
    const/4 v2, 0x1

    move v0, v2

    .line 13341
    if-eqz v0, :cond_15

    .line 13342
    :try_start_1a
    const-string v0, "LOGCAT"

    const/4 v2, 0x0

    invoke-static {p1, v2}, LX/04Y;->collectLogCat(LX/00K;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_1a
    .catch Ljava/lang/Throwable; {:try_start_1a .. :try_end_1a} :catch_10

    .line 13343
    :cond_15
    :goto_18
    const/4 v2, 0x1

    move v0, v2

    .line 13344
    if-eqz v0, :cond_16

    .line 13345
    :try_start_1b
    const-string v0, "EVENTSLOG"

    const-string v2, "events"

    invoke-static {p1, v2}, LX/04Y;->collectLogCat(LX/00K;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_1b} :catch_11

    .line 13346
    :cond_16
    :goto_19
    const/4 v2, 0x1

    move v0, v2

    .line 13347
    if-eqz v0, :cond_17

    .line 13348
    :try_start_1c
    const-string v0, "RADIOLOG"

    const-string v2, "radio"

    invoke-static {p1, v2}, LX/04Y;->collectLogCat(LX/00K;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_1c
    .catch Ljava/lang/Throwable; {:try_start_1c .. :try_end_1c} :catch_12

    .line 13349
    :cond_17
    :goto_1a
    const/4 v2, 0x1

    move v0, v2

    .line 13350
    if-eqz v0, :cond_18

    .line 13351
    :try_start_1d
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_18

    .line 13352
    const-string v0, "LARGE_MEM_HEAP"

    .line 13353
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_1e

    .line 13354
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 13355
    const-string v2, "activity"

    invoke-virtual {v4, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    .line 13356
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Large heap size ="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/app/ActivityManager;->getLargeMemoryClass()I

    move-result v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13357
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 13358
    :goto_1b
    move-object v2, v2

    .line 13359
    invoke-static {v0, v2, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_1d
    .catch Ljava/lang/Throwable; {:try_start_1d .. :try_end_1d} :catch_13

    .line 13360
    :cond_18
    :goto_1c
    if-eqz v1, :cond_1a

    .line 13361
    const/4 v1, 0x1

    move v0, v1

    .line 13362
    if-eqz v0, :cond_19

    .line 13363
    :try_start_1e
    const-string v0, "OPEN_FILE_DESCRIPTORS"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "/system/bin/ls"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "-l"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "/proc/self/fd"

    aput-object v3, v1, v2

    invoke-static {v1}, LX/04R;->collectCommandOutput([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_1e} :catch_14

    .line 13364
    :cond_19
    :goto_1d
    const/4 v1, 0x1

    move v0, v1

    .line 13365
    if-eqz v0, :cond_1a

    .line 13366
    :try_start_1f
    const-string v0, "DATA_FILE_LS_LR"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "/system/bin/ls"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "-lR"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v1}, LX/04R;->collectCommandOutput([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p3, p4}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_1f
    .catch Ljava/lang/Throwable; {:try_start_1f .. :try_end_1f} :catch_15

    .line 13367
    :cond_1a
    :goto_1e
    return-void

    .line 13368
    :catch_1
    move-exception v1

    .line 13369
    const-string v5, "PROCESS_NAME"

    invoke-static {p3, v5, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 13370
    :catch_2
    move-exception v1

    .line 13371
    const-string v5, "USER_APP_START_DATE"

    invoke-static {p3, v5, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 13372
    :catch_3
    move-exception v1

    .line 13373
    const-string v5, "PROCESS_UPTIME"

    invoke-static {p3, v5, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4

    .line 13374
    :catch_4
    move-exception v1

    .line 13375
    const-string v5, "DEVICE_UPTIME"

    invoke-static {p3, v5, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5

    .line 13376
    :catch_5
    move-exception v1

    .line 13377
    const-string v5, "CRASH_CONFIGURATION"

    invoke-static {p3, v5, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_6

    .line 13378
    :catch_6
    move-exception v1

    .line 13379
    const-string v5, "AVAILABLE_MEM_SIZE"

    invoke-static {p3, v5, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 13380
    :catch_7
    move-exception v1

    .line 13381
    const-string v5, "DUMPSYS_MEMINFO"

    invoke-static {p3, v5, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    .line 13382
    :catch_8
    move-exception v1

    .line 13383
    const-string v5, "USER_CRASH_DATE"

    invoke-static {p3, v5, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_9

    .line 13384
    :cond_1b
    :try_start_20
    invoke-virtual {p0}, LX/009;->getActivityLogger()LX/01a;

    move-result-object v1

    sget v5, LX/04R;->DEFAULT_TRACE_COUNT_LIMIT:I

    invoke-virtual {v1, v5}, LX/01a;->toString(I)Ljava/lang/String;
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_20 .. :try_end_20} :catch_9

    move-result-object v1

    goto/16 :goto_a

    .line 13385
    :catch_9
    move-exception v1

    .line 13386
    const-string v5, "ACTIVITY_LOG"

    invoke-static {p3, v5, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_b

    .line 13387
    :catch_a
    move-exception v1

    .line 13388
    const-string v5, "PROCESS_NAME_BY_AMS"

    invoke-static {p3, v5, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_c

    .line 13389
    :catch_b
    move-exception v1

    .line 13390
    const-string v5, "OPEN_FD_COUNT"

    invoke-static {p3, v5, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_d

    .line 13391
    :catch_c
    move-exception v1

    .line 13392
    sget-object v5, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v6, "unable to retrieve open FD info: not logging FD fields"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_e

    .line 13393
    :catch_d
    move-exception v1

    .line 13394
    const-string v5, "OPEN_FD_SOFT_LIMIT"

    invoke-static {p3, v5, v1}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_f

    .line 13395
    :catch_e
    move-exception v0

    .line 13396
    const-string v1, "OPEN_FD_HARD_LIMIT"

    invoke-static {p3, v1, v0}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_10

    .line 13397
    :catch_f
    move-exception v0

    .line 13398
    const-string v1, "RUNTIME_PERMISSIONS"

    invoke-static {p3, v1, v0}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_16

    :cond_1c
    move v1, v3

    .line 13399
    goto/16 :goto_17

    .line 13400
    :catch_10
    move-exception v0

    .line 13401
    const-string v2, "LOGCAT"

    invoke-static {p3, v2, v0}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_18

    .line 13402
    :catch_11
    move-exception v0

    .line 13403
    const-string v2, "EVENTSLOG"

    invoke-static {p3, v2, v0}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_19

    .line 13404
    :catch_12
    move-exception v0

    .line 13405
    const-string v2, "RADIOLOG"

    invoke-static {p3, v2, v0}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1a

    .line 13406
    :catch_13
    move-exception v0

    .line 13407
    const-string v2, "LARGE_MEM_HEAP"

    invoke-static {p3, v2, v0}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1c

    .line 13408
    :catch_14
    move-exception v0

    .line 13409
    const-string v1, "OPEN_FILE_DESCRIPTORS"

    invoke-static {p3, v1, v0}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1d

    .line 13410
    :catch_15
    move-exception v0

    .line 13411
    const-string v1, "DATA_FILE_LS_LR"

    invoke-static {p3, v1, v0}, LX/04R;->noteReportFieldFailure(LX/01l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1e

    .line 13412
    :catch_16
    :try_start_21
    move-exception v5

    move-object v7, v8

    .line 13413
    :goto_1f
    sget-object v8, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v9, "Failed to get process name."

    invoke-static {v8, v9, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v5, v7

    move-object v7, v6

    goto/16 :goto_0

    .line 13414
    :catch_17
    move-exception v6

    .line 13415
    sget-object v7, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Failed to close file."

    invoke-static {v7, v8, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 13416
    :catch_18
    move-exception v5

    move-object v6, v7

    move-object v7, v8

    goto :goto_1f

    :catch_19
    move-exception v6

    move-object v10, v6

    move-object v6, v7

    move-object v7, v5

    move-object v5, v10

    goto :goto_1f

    :cond_1d
    move-object v5, v8

    goto/16 :goto_1
    :try_end_21
    .catch Ljava/lang/Throwable; {:try_start_21 .. :try_end_21} :catch_1

    .line 13417
    :sswitch_0
    :try_start_22
    const-string v7, "android.permission.READ_CALENDAR"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/4 v6, 0x0

    goto/16 :goto_12

    :sswitch_1
    const-string v7, "android.permission.WRITE_CALENDAR"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/4 v6, 0x1

    goto/16 :goto_12

    :sswitch_2
    const-string v7, "android.permission.CAMERA"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/4 v6, 0x2

    goto/16 :goto_12

    :sswitch_3
    const-string v7, "android.permission.READ_CONTACTS"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/4 v6, 0x3

    goto/16 :goto_12

    :sswitch_4
    const-string v7, "android.permission.WRITE_CONTACTS"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/4 v6, 0x4

    goto/16 :goto_12

    :sswitch_5
    const-string v7, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/4 v6, 0x5

    goto/16 :goto_12

    :sswitch_6
    const-string v7, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/4 v6, 0x6

    goto/16 :goto_12

    :sswitch_7
    const-string v7, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/4 v6, 0x7

    goto/16 :goto_12

    :sswitch_8
    const-string v7, "android.permission.RECORD_AUDIO"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0x8

    goto/16 :goto_12

    :sswitch_9
    const-string v7, "android.permission.READ_PHONE_STATE"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0x9

    goto/16 :goto_12

    :sswitch_a
    const-string v7, "android.permission.CALL_PHONE"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0xa

    goto/16 :goto_12

    :sswitch_b
    const-string v7, "android.permission.READ_CALL_LOG"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0xb

    goto/16 :goto_12

    :sswitch_c
    const-string v7, "android.permission.WRITE_CALL_LOG"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0xc

    goto/16 :goto_12

    :sswitch_d
    const-string v7, "com.android.voicemail.permission.ADD_VOICEMAIL"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0xd

    goto/16 :goto_12

    :sswitch_e
    const-string v7, "android.permission.USE_SIP"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0xe

    goto/16 :goto_12

    :sswitch_f
    const-string v7, "android.permission.PROCESS_OUTGOING_CALLS"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0xf

    goto/16 :goto_12

    :sswitch_10
    const-string v7, "android.permission.BODY_SENSORS"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0x10

    goto/16 :goto_12

    :sswitch_11
    const-string v7, "android.permission.SEND_SMS"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0x11

    goto/16 :goto_12

    :sswitch_12
    const-string v7, "android.permission.RECEIVE_SMS"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0x12

    goto/16 :goto_12

    :sswitch_13
    const-string v7, "android.permission.READ_SMS"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0x13

    goto/16 :goto_12

    :sswitch_14
    const-string v7, "android.permission.RECEIVE_WAP_PUSH"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0x14

    goto/16 :goto_12

    :sswitch_15
    const-string v7, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0x15

    goto/16 :goto_12

    :sswitch_16
    const-string v7, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    const/16 v6, 0x16

    goto/16 :goto_12

    .line 13418
    :pswitch_0
    const-string v6, "android.permission-group.CALENDAR"

    goto/16 :goto_13

    .line 13419
    :pswitch_1
    const-string v6, "android.permission-group.CAMERA"

    goto/16 :goto_13

    .line 13420
    :pswitch_2
    const-string v6, "android.permission-group.CONTACTS"

    goto/16 :goto_13

    .line 13421
    :pswitch_3
    const-string v6, "android.permission-group.LOCATION"

    goto/16 :goto_13

    .line 13422
    :pswitch_4
    const-string v6, "android.permission-group.MICROPHONE"

    goto/16 :goto_13

    .line 13423
    :pswitch_5
    const-string v6, "android.permission-group.PHONE"

    goto/16 :goto_13

    .line 13424
    :pswitch_6
    const-string v6, "android.permission-group.SENSORS"

    goto/16 :goto_13

    .line 13425
    :pswitch_7
    const-string v6, "android.permission-group.SMS"

    goto/16 :goto_13

    .line 13426
    :pswitch_8
    const-string v6, "android.permission-group.STORAGE"

    goto/16 :goto_13

    :catch_1a
    goto/16 :goto_14
    :try_end_22
    .catch Ljava/lang/Throwable; {:try_start_22 .. :try_end_22} :catch_f

    :cond_1e
    const-string v2, ""

    goto/16 :goto_1b

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7aed85b0 -> :sswitch_13
        -0x72f13779 -> :sswitch_0
        -0x7286b8f4 -> :sswitch_b
        -0x70918bc1 -> :sswitch_6
        -0x583351d1 -> :sswitch_14
        -0x49cb6684 -> :sswitch_10
        -0x3562e583 -> :sswitch_12
        -0x1833add0 -> :sswitch_15
        -0x3c1ac56 -> :sswitch_7
        -0x550ba9 -> :sswitch_9
        0x322a742 -> :sswitch_11
        0x6afff6d -> :sswitch_a
        0xcc96c13 -> :sswitch_4
        0x1b9efa65 -> :sswitch_2
        0x23fb06fe -> :sswitch_1
        0x24658583 -> :sswitch_c
        0x2ec2d2a2 -> :sswitch_e
        0x38cade52 -> :sswitch_f
        0x4bcdda0f -> :sswitch_5
        0x516a29a7 -> :sswitch_16
        0x6d24f988 -> :sswitch_8
        0x75dd2d9c -> :sswitch_3
        0x7f2f307d -> :sswitch_d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method
