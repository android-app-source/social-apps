.class public final LX/0GM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0GK;

.field private final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LX/0GM;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final d:Ljava/lang/String;

.field public final e:Landroid/net/Uri;

.field public final f:Landroid/net/Uri;

.field public final g:Ljava/lang/String;

.field public final h:I

.field public final i:LX/0Gk;

.field public final j:Ljava/lang/String;

.field public k:I

.field public l:LX/0GL;


# direct methods
.method private constructor <init>(LX/0GK;Ljava/util/Collection;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;ILX/0Gk;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0GK;",
            "Ljava/util/Collection",
            "<",
            "LX/0GM;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "I",
            "LX/0Gk;",
            "I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34951
    iput-object p1, p0, LX/0GM;->a:LX/0GK;

    .line 34952
    iput-object p2, p0, LX/0GM;->b:Ljava/util/Collection;

    .line 34953
    iput-object p3, p0, LX/0GM;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 34954
    iput-object p4, p0, LX/0GM;->d:Ljava/lang/String;

    .line 34955
    iput-object p5, p0, LX/0GM;->e:Landroid/net/Uri;

    .line 34956
    iput-object p6, p0, LX/0GM;->f:Landroid/net/Uri;

    .line 34957
    iput-object p7, p0, LX/0GM;->g:Ljava/lang/String;

    .line 34958
    iput p8, p0, LX/0GM;->h:I

    .line 34959
    iput-object p9, p0, LX/0GM;->i:LX/0Gk;

    .line 34960
    iput p10, p0, LX/0GM;->k:I

    .line 34961
    sget-object v0, LX/0GL;->PENDING:LX/0GL;

    iput-object v0, p0, LX/0GM;->l:LX/0GL;

    .line 34962
    iput-object p11, p0, LX/0GM;->j:Ljava/lang/String;

    .line 34963
    return-void
.end method

.method public synthetic constructor <init>(LX/0GK;Ljava/util/Collection;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;ILX/0Gk;ILjava/lang/String;B)V
    .locals 0

    .prologue
    .line 34964
    invoke-direct/range {p0 .. p11}, LX/0GM;-><init>(LX/0GK;Ljava/util/Collection;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;ILX/0Gk;ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0GL;)V
    .locals 3

    .prologue
    .line 34965
    iget-object v1, p0, LX/0GM;->l:LX/0GL;

    monitor-enter v1

    .line 34966
    :try_start_0
    iget-object v0, p0, LX/0GM;->l:LX/0GL;

    sget-object v2, LX/0GL;->PENDING:LX/0GL;

    if-ne v0, v2, :cond_0

    .line 34967
    iput-object p1, p0, LX/0GM;->l:LX/0GL;

    .line 34968
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 34969
    iget-object v1, p0, LX/0GM;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 34970
    :try_start_0
    iget-object v0, p0, LX/0GM;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 34971
    iget-object v0, p0, LX/0GM;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GM;

    .line 34972
    sget-object v3, LX/0GL;->CANCELED:LX/0GL;

    invoke-virtual {v0, v3}, LX/0GM;->a(LX/0GL;)V

    goto :goto_0

    .line 34973
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 34974
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0GM;->a:LX/0GK;

    invoke-virtual {v0}, LX/0GK;->e()V

    .line 34975
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
