.class public LX/0Lu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/UUID;

.field public final c:LX/0M1;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/UUID;LX/0M1;)V
    .locals 1

    .prologue
    .line 44828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44829
    invoke-static {p1}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/0Lu;->a:Ljava/lang/String;

    .line 44830
    iput-object p2, p0, LX/0Lu;->b:Ljava/util/UUID;

    .line 44831
    iput-object p3, p0, LX/0Lu;->c:LX/0M1;

    .line 44832
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 44833
    instance-of v2, p1, LX/0Lu;

    if-nez v2, :cond_1

    .line 44834
    :cond_0
    :goto_0
    return v0

    .line 44835
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    .line 44836
    goto :goto_0

    .line 44837
    :cond_2
    check-cast p1, LX/0Lu;

    .line 44838
    iget-object v2, p0, LX/0Lu;->a:Ljava/lang/String;

    iget-object v3, p1, LX/0Lu;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/0Lu;->b:Ljava/util/UUID;

    iget-object v3, p1, LX/0Lu;->b:Ljava/util/UUID;

    invoke-static {v2, v3}, LX/08x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/0Lu;->c:LX/0M1;

    iget-object v3, p1, LX/0Lu;->c:LX/0M1;

    invoke-static {v2, v3}, LX/08x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 44839
    iget-object v0, p0, LX/0Lu;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 44840
    mul-int/lit8 v2, v0, 0x25

    iget-object v0, p0, LX/0Lu;->b:Ljava/util/UUID;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0Lu;->b:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 44841
    mul-int/lit8 v0, v0, 0x25

    iget-object v2, p0, LX/0Lu;->c:LX/0M1;

    if-eqz v2, :cond_0

    iget-object v1, p0, LX/0Lu;->c:LX/0M1;

    invoke-virtual {v1}, LX/0M1;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 44842
    return v0

    :cond_1
    move v0, v1

    .line 44843
    goto :goto_0
.end method
