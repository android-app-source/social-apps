.class public LX/09j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<TV;>;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Callable;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;II)V"
        }
    .end annotation

    .prologue
    .line 22837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22838
    iput-object p1, p0, LX/09j;->a:Ljava/util/concurrent/Callable;

    .line 22839
    iput p2, p0, LX/09j;->c:I

    .line 22840
    iput p3, p0, LX/09j;->b:I

    .line 22841
    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    const/16 v4, 0xd

    const/4 v3, 0x1

    .line 22842
    const/16 v0, 0xc

    iget v1, p0, LX/09j;->b:I

    iget v2, p0, LX/09j;->c:I

    invoke-static {v3, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move-result v1

    .line 22843
    :try_start_0
    iget-object v0, p0, LX/09j;->a:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 22844
    iget v2, p0, LX/09j;->b:I

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0

    :catchall_0
    move-exception v0

    iget v2, p0, LX/09j;->b:I

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0
.end method
