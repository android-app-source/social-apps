.class public final LX/0Cm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Cl;


# instance fields
.field public final synthetic a:LX/0D5;

.field public final synthetic b:Lcom/facebook/browser/lite/BrowserLiteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;)V
    .locals 0

    .prologue
    .line 28016
    iput-object p1, p0, LX/0Cm;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iput-object p2, p0, LX/0Cm;->a:LX/0D5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 28001
    iget-object v0, p0, LX/0Cm;->a:LX/0D5;

    new-instance v1, LX/0Ck;

    invoke-direct {v1, p0}, LX/0Ck;-><init>(LX/0Cm;)V

    .line 28002
    iput-object v1, v0, LX/0D5;->f:LX/0Ck;

    .line 28003
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 28010
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 28011
    const-string v1, "action"

    const-string v2, "MESSENGER_CONTENT_SUBSCRIBE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28012
    const-string v1, "url"

    iget-object v2, p0, LX/0Cm;->a:LX/0D5;

    invoke-virtual {v2}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28013
    const-string v1, "id"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28014
    iget-object v1, p0, LX/0Cm;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    iget-object v2, p0, LX/0Cm;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_TRACKING"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0CQ;->a(Ljava/util/Map;Landroid/os/Bundle;)V

    .line 28015
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 28007
    iget-object v0, p0, LX/0Cm;->a:LX/0D5;

    const/4 v1, 0x0

    .line 28008
    iput-object v1, v0, LX/0D5;->f:LX/0Ck;

    .line 28009
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 28004
    iget-object v0, p0, LX/0Cm;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    iget-object v1, p0, LX/0Cm;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteFragment;->u:Ljava/lang/String;

    .line 28005
    new-instance p0, LX/0CE;

    invoke-direct {p0, v0, p1, v1}, LX/0CE;-><init>(LX/0CQ;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, p0}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 28006
    return-void
.end method
