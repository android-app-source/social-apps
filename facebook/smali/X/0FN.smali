.class public final LX/0FN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private final mClient:Landroid/os/Messenger;

.field public final mContext:Landroid/content/Context;

.field private final mDexStoreRoot:Ljava/lang/String;

.field public mRpi:Landroid/app/ActivityManager$RunningAppProcessInfo;

.field private mService:Landroid/os/Messenger;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 33080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33081
    iput-object p1, p0, LX/0FN;->mContext:Landroid/content/Context;

    .line 33082
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, LX/0FM;

    invoke-direct {v1, p0}, LX/0FM;-><init>(LX/0FN;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, LX/0FN;->mClient:Landroid/os/Messenger;

    .line 33083
    iput-object p2, p0, LX/0FN;->mDexStoreRoot:Ljava/lang/String;

    .line 33084
    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 33085
    const-string v0, "service connected"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33086
    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    iput-object v0, p0, LX/0FN;->mService:Landroid/os/Messenger;

    .line 33087
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 33088
    const-string v1, "dexStoreRoot"

    iget-object v2, p0, LX/0FN;->mDexStoreRoot:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 33089
    const/4 v1, 0x1

    invoke-static {v4, v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 33090
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 33091
    iget-object v0, p0, LX/0FN;->mClient:Landroid/os/Messenger;

    iput-object v0, v1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 33092
    :try_start_0
    iget-object v0, p0, LX/0FN;->mService:Landroid/os/Messenger;

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33093
    const-string v0, "sent MSG_OPT_START to service"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33094
    :goto_0
    return-void

    .line 33095
    :catch_0
    move-exception v0

    .line 33096
    const-string v1, "error sending MSG_OPT_START to service: will try later"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/02P;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 33097
    const-string v0, "service disconnected"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33098
    const/4 v0, 0x0

    iput-object v0, p0, LX/0FN;->mService:Landroid/os/Messenger;

    .line 33099
    return-void
.end method
