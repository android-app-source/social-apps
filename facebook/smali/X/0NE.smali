.class public final LX/0NE;
.super LX/0N4;
.source ""


# instance fields
.field private b:Z

.field private final c:LX/0NK;

.field private final d:[Z

.field private final e:LX/0NH;

.field private final f:LX/0NH;

.field private final g:LX/0NH;

.field private final h:LX/0NH;

.field private final i:LX/0NH;

.field private final j:LX/0ND;

.field private k:J

.field private l:J

.field private final m:LX/0Oj;


# direct methods
.method public constructor <init>(LX/0LS;LX/0NK;)V
    .locals 3

    .prologue
    const/16 v2, 0x80

    .line 49680
    invoke-direct {p0, p1}, LX/0N4;-><init>(LX/0LS;)V

    .line 49681
    iput-object p2, p0, LX/0NE;->c:LX/0NK;

    .line 49682
    const/4 v0, 0x3

    new-array v0, v0, [Z

    iput-object v0, p0, LX/0NE;->d:[Z

    .line 49683
    new-instance v0, LX/0NH;

    const/16 v1, 0x20

    invoke-direct {v0, v1, v2}, LX/0NH;-><init>(II)V

    iput-object v0, p0, LX/0NE;->e:LX/0NH;

    .line 49684
    new-instance v0, LX/0NH;

    const/16 v1, 0x21

    invoke-direct {v0, v1, v2}, LX/0NH;-><init>(II)V

    iput-object v0, p0, LX/0NE;->f:LX/0NH;

    .line 49685
    new-instance v0, LX/0NH;

    const/16 v1, 0x22

    invoke-direct {v0, v1, v2}, LX/0NH;-><init>(II)V

    iput-object v0, p0, LX/0NE;->g:LX/0NH;

    .line 49686
    new-instance v0, LX/0NH;

    const/16 v1, 0x27

    invoke-direct {v0, v1, v2}, LX/0NH;-><init>(II)V

    iput-object v0, p0, LX/0NE;->h:LX/0NH;

    .line 49687
    new-instance v0, LX/0NH;

    const/16 v1, 0x28

    invoke-direct {v0, v1, v2}, LX/0NH;-><init>(II)V

    iput-object v0, p0, LX/0NE;->i:LX/0NH;

    .line 49688
    new-instance v0, LX/0ND;

    invoke-direct {v0, p1}, LX/0ND;-><init>(LX/0LS;)V

    iput-object v0, p0, LX/0NE;->j:LX/0ND;

    .line 49689
    new-instance v0, LX/0Oj;

    invoke-direct {v0}, LX/0Oj;-><init>()V

    iput-object v0, p0, LX/0NE;->m:LX/0Oj;

    .line 49690
    return-void
.end method

.method private static a(LX/0NH;LX/0NH;LX/0NH;)LX/0L4;
    .locals 13

    .prologue
    .line 49563
    iget v0, p0, LX/0NH;->b:I

    iget v1, p1, LX/0NH;->b:I

    add-int/2addr v0, v1

    iget v1, p2, LX/0NH;->b:I

    add-int/2addr v0, v1

    new-array v8, v0, [B

    .line 49564
    iget-object v0, p0, LX/0NH;->a:[B

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, LX/0NH;->b:I

    invoke-static {v0, v1, v8, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49565
    iget-object v0, p1, LX/0NH;->a:[B

    const/4 v1, 0x0

    iget v2, p0, LX/0NH;->b:I

    iget v3, p1, LX/0NH;->b:I

    invoke-static {v0, v1, v8, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49566
    iget-object v0, p2, LX/0NH;->a:[B

    const/4 v1, 0x0

    iget v2, p0, LX/0NH;->b:I

    iget v3, p1, LX/0NH;->b:I

    add-int/2addr v2, v3

    iget v3, p2, LX/0NH;->b:I

    invoke-static {v0, v1, v8, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49567
    iget-object v0, p1, LX/0NH;->a:[B

    iget v1, p1, LX/0NH;->b:I

    invoke-static {v0, v1}, LX/0Oh;->a([BI)I

    .line 49568
    new-instance v3, LX/0Oi;

    iget-object v0, p1, LX/0NH;->a:[B

    invoke-direct {v3, v0}, LX/0Oi;-><init>([B)V

    .line 49569
    const/16 v0, 0x2c

    invoke-virtual {v3, v0}, LX/0Oi;->b(I)V

    .line 49570
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, LX/0Oi;->c(I)I

    move-result v1

    .line 49571
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, LX/0Oi;->b(I)V

    .line 49572
    const/16 v0, 0x58

    invoke-virtual {v3, v0}, LX/0Oi;->b(I)V

    .line 49573
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, LX/0Oi;->b(I)V

    .line 49574
    const/4 v2, 0x0

    .line 49575
    const/4 v0, 0x0

    move v12, v0

    move v0, v2

    move v2, v12

    :goto_0
    if-ge v2, v1, :cond_2

    .line 49576
    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 49577
    add-int/lit8 v0, v0, 0x59

    .line 49578
    :cond_0
    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 49579
    add-int/lit8 v0, v0, 0x8

    .line 49580
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 49581
    :cond_2
    invoke-virtual {v3, v0}, LX/0Oi;->b(I)V

    .line 49582
    if-lez v1, :cond_3

    .line 49583
    rsub-int/lit8 v0, v1, 0x8

    mul-int/lit8 v0, v0, 0x2

    invoke-virtual {v3, v0}, LX/0Oi;->b(I)V

    .line 49584
    :cond_3
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49585
    invoke-virtual {v3}, LX/0Oi;->d()I

    move-result v4

    .line 49586
    const/4 v0, 0x3

    if-ne v4, v0, :cond_4

    .line 49587
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, LX/0Oi;->b(I)V

    .line 49588
    :cond_4
    invoke-virtual {v3}, LX/0Oi;->d()I

    move-result v6

    .line 49589
    invoke-virtual {v3}, LX/0Oi;->d()I

    move-result v7

    .line 49590
    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 49591
    invoke-virtual {v3}, LX/0Oi;->d()I

    move-result v5

    .line 49592
    invoke-virtual {v3}, LX/0Oi;->d()I

    move-result v9

    .line 49593
    invoke-virtual {v3}, LX/0Oi;->d()I

    move-result v10

    .line 49594
    invoke-virtual {v3}, LX/0Oi;->d()I

    move-result v11

    .line 49595
    const/4 v0, 0x1

    if-eq v4, v0, :cond_5

    const/4 v0, 0x2

    if-ne v4, v0, :cond_7

    :cond_5
    const/4 v0, 0x2

    move v2, v0

    .line 49596
    :goto_1
    const/4 v0, 0x1

    if-ne v4, v0, :cond_8

    const/4 v0, 0x2

    .line 49597
    :goto_2
    add-int v4, v5, v9

    mul-int/2addr v2, v4

    sub-int/2addr v6, v2

    .line 49598
    add-int v2, v10, v11

    mul-int/2addr v0, v2

    sub-int/2addr v7, v0

    .line 49599
    :cond_6
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49600
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49601
    invoke-virtual {v3}, LX/0Oi;->d()I

    move-result v2

    .line 49602
    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x0

    :goto_3
    if-gt v0, v1, :cond_a

    .line 49603
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49604
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49605
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49606
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 49607
    :cond_7
    const/4 v0, 0x1

    move v2, v0

    goto :goto_1

    .line 49608
    :cond_8
    const/4 v0, 0x1

    goto :goto_2

    :cond_9
    move v0, v1

    .line 49609
    goto :goto_3

    .line 49610
    :cond_a
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49611
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49612
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49613
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49614
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49615
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49616
    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 49617
    const/4 v1, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 49618
    move v10, v5

    :goto_4
    const/4 v0, 0x4

    if-ge v10, v0, :cond_10

    move v9, v5

    .line 49619
    :goto_5
    const/4 v0, 0x6

    if-ge v9, v0, :cond_f

    .line 49620
    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v0

    if-nez v0, :cond_c

    .line 49621
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49622
    :cond_b
    if-ne v10, v1, :cond_e

    move v0, v1

    :goto_6
    add-int/2addr v0, v9

    move v9, v0

    goto :goto_5

    .line 49623
    :cond_c
    const/16 v0, 0x40

    shl-int/lit8 v11, v10, 0x1

    add-int/lit8 v11, v11, 0x4

    shl-int v11, v4, v11

    invoke-static {v0, v11}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 49624
    if-le v10, v4, :cond_d

    .line 49625
    invoke-virtual {v3}, LX/0Oi;->e()I

    :cond_d
    move v0, v5

    .line 49626
    :goto_7
    if-ge v0, v11, :cond_b

    .line 49627
    invoke-virtual {v3}, LX/0Oi;->e()I

    .line 49628
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_e
    move v0, v4

    .line 49629
    goto :goto_6

    .line 49630
    :cond_f
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_4

    .line 49631
    :cond_10
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, LX/0Oi;->b(I)V

    .line 49632
    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 49633
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, LX/0Oi;->b(I)V

    .line 49634
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49635
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49636
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, LX/0Oi;->b(I)V

    .line 49637
    :cond_11
    const/4 p0, 0x1

    const/4 v4, 0x0

    .line 49638
    invoke-virtual {v3}, LX/0Oi;->d()I

    move-result v10

    move v9, v4

    move v0, v4

    move v1, v4

    .line 49639
    :goto_8
    if-ge v9, v10, :cond_16

    .line 49640
    if-eqz v9, :cond_1c

    .line 49641
    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v1

    move v5, v1

    .line 49642
    :goto_9
    if-eqz v5, :cond_13

    .line 49643
    invoke-virtual {v3, p0}, LX/0Oi;->b(I)V

    .line 49644
    invoke-virtual {v3}, LX/0Oi;->d()I

    move v1, v4

    .line 49645
    :goto_a
    if-gt v1, v0, :cond_15

    .line 49646
    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v11

    if-eqz v11, :cond_12

    .line 49647
    invoke-virtual {v3, p0}, LX/0Oi;->b(I)V

    .line 49648
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 49649
    :cond_13
    invoke-virtual {v3}, LX/0Oi;->d()I

    move-result v11

    .line 49650
    invoke-virtual {v3}, LX/0Oi;->d()I

    move-result v12

    .line 49651
    add-int v0, v11, v12

    move v1, v4

    .line 49652
    :goto_b
    if-ge v1, v11, :cond_14

    .line 49653
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49654
    invoke-virtual {v3, p0}, LX/0Oi;->b(I)V

    .line 49655
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_14
    move v1, v4

    .line 49656
    :goto_c
    if-ge v1, v12, :cond_15

    .line 49657
    invoke-virtual {v3}, LX/0Oi;->d()I

    .line 49658
    invoke-virtual {v3, p0}, LX/0Oi;->b(I)V

    .line 49659
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 49660
    :cond_15
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    move v1, v5

    goto :goto_8

    .line 49661
    :cond_16
    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 49662
    const/4 v0, 0x0

    :goto_d
    invoke-virtual {v3}, LX/0Oi;->d()I

    move-result v1

    if-ge v0, v1, :cond_17

    .line 49663
    add-int/lit8 v1, v2, 0x4

    .line 49664
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3, v1}, LX/0Oi;->b(I)V

    .line 49665
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 49666
    :cond_17
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, LX/0Oi;->b(I)V

    .line 49667
    const/high16 v0, 0x3f800000    # 1.0f

    .line 49668
    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 49669
    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 49670
    const/16 v1, 0x8

    invoke-virtual {v3, v1}, LX/0Oi;->c(I)I

    move-result v1

    .line 49671
    const/16 v2, 0xff

    if-ne v1, v2, :cond_19

    .line 49672
    const/16 v1, 0x10

    invoke-virtual {v3, v1}, LX/0Oi;->c(I)I

    move-result v1

    .line 49673
    const/16 v2, 0x10

    invoke-virtual {v3, v2}, LX/0Oi;->c(I)I

    move-result v2

    .line 49674
    if-eqz v1, :cond_18

    if-eqz v2, :cond_18

    .line 49675
    int-to-float v0, v1

    int-to-float v1, v2

    div-float/2addr v0, v1

    :cond_18
    move v10, v0

    .line 49676
    :goto_e
    const/4 v0, 0x0

    const-string v1, "video/hevc"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-wide/16 v4, -0x1

    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    const/4 v9, -0x1

    invoke-static/range {v0 .. v10}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)LX/0L4;

    move-result-object v0

    return-object v0

    .line 49677
    :cond_19
    sget-object v2, LX/0Oh;->b:[F

    array-length v2, v2

    if-ge v1, v2, :cond_1a

    .line 49678
    sget-object v0, LX/0Oh;->b:[F

    aget v0, v0, v1

    move v10, v0

    goto :goto_e

    .line 49679
    :cond_1a
    const-string v2, "H265Reader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected aspect_ratio_idc value: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1b
    move v10, v0

    goto :goto_e

    :cond_1c
    move v5, v1

    goto/16 :goto_9
.end method

.method private a(JIIJ)V
    .locals 9

    .prologue
    .line 49470
    iget-boolean v0, p0, LX/0NE;->b:Z

    if-nez v0, :cond_0

    .line 49471
    iget-object v0, p0, LX/0NE;->e:LX/0NH;

    invoke-virtual {v0, p4}, LX/0NH;->a(I)V

    .line 49472
    iget-object v0, p0, LX/0NE;->f:LX/0NH;

    invoke-virtual {v0, p4}, LX/0NH;->a(I)V

    .line 49473
    iget-object v0, p0, LX/0NE;->g:LX/0NH;

    invoke-virtual {v0, p4}, LX/0NH;->a(I)V

    .line 49474
    :cond_0
    iget-object v0, p0, LX/0NE;->h:LX/0NH;

    invoke-virtual {v0, p4}, LX/0NH;->a(I)V

    .line 49475
    iget-object v0, p0, LX/0NE;->i:LX/0NH;

    invoke-virtual {v0, p4}, LX/0NH;->a(I)V

    .line 49476
    iget-object v1, p0, LX/0NE;->j:LX/0ND;

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move-wide v6, p5

    invoke-virtual/range {v1 .. v7}, LX/0ND;->a(JIIJ)V

    .line 49477
    return-void
.end method

.method private a([BII)V
    .locals 4

    .prologue
    .line 49548
    iget-boolean v0, p0, LX/0NE;->b:Z

    if-eqz v0, :cond_1

    .line 49549
    iget-object v0, p0, LX/0NE;->j:LX/0ND;

    const/4 v2, 0x0

    .line 49550
    iget-boolean v1, v0, LX/0ND;->f:Z

    if-eqz v1, :cond_0

    .line 49551
    add-int/lit8 v1, p2, 0x2

    iget v3, v0, LX/0ND;->d:I

    sub-int/2addr v1, v3

    .line 49552
    if-ge v1, p3, :cond_3

    .line 49553
    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, v0, LX/0ND;->g:Z

    .line 49554
    iput-boolean v2, v0, LX/0ND;->f:Z

    .line 49555
    :cond_0
    :goto_1
    iget-object v0, p0, LX/0NE;->h:LX/0NH;

    invoke-virtual {v0, p1, p2, p3}, LX/0NH;->a([BII)V

    .line 49556
    iget-object v0, p0, LX/0NE;->i:LX/0NH;

    invoke-virtual {v0, p1, p2, p3}, LX/0NH;->a([BII)V

    .line 49557
    return-void

    .line 49558
    :cond_1
    iget-object v0, p0, LX/0NE;->e:LX/0NH;

    invoke-virtual {v0, p1, p2, p3}, LX/0NH;->a([BII)V

    .line 49559
    iget-object v0, p0, LX/0NE;->f:LX/0NH;

    invoke-virtual {v0, p1, p2, p3}, LX/0NH;->a([BII)V

    .line 49560
    iget-object v0, p0, LX/0NE;->g:LX/0NH;

    invoke-virtual {v0, p1, p2, p3}, LX/0NH;->a([BII)V

    goto :goto_1

    :cond_2
    move v1, v2

    .line 49561
    goto :goto_0

    .line 49562
    :cond_3
    iget v1, v0, LX/0ND;->d:I

    sub-int v2, p3, p2

    add-int/2addr v1, v2

    iput v1, v0, LX/0ND;->d:I

    goto :goto_1
.end method

.method private b(JIIJ)V
    .locals 8

    .prologue
    const/4 v4, 0x5

    .line 49515
    iget-boolean v0, p0, LX/0NE;->b:Z

    if-eqz v0, :cond_4

    .line 49516
    iget-object v0, p0, LX/0NE;->j:LX/0ND;

    .line 49517
    iget-boolean v5, v0, LX/0ND;->g:Z

    if-eqz v5, :cond_1

    .line 49518
    iget-boolean v5, v0, LX/0ND;->h:Z

    if-eqz v5, :cond_0

    .line 49519
    iget-wide v5, v0, LX/0ND;->b:J

    sub-long v5, p1, v5

    long-to-int v5, v5

    .line 49520
    add-int/2addr v5, p3

    invoke-static {v0, v5}, LX/0ND;->a(LX/0ND;I)V

    .line 49521
    :cond_0
    iget-wide v5, v0, LX/0ND;->b:J

    iput-wide v5, v0, LX/0ND;->i:J

    .line 49522
    iget-wide v5, v0, LX/0ND;->e:J

    iput-wide v5, v0, LX/0ND;->j:J

    .line 49523
    const/4 v5, 0x1

    iput-boolean v5, v0, LX/0ND;->h:Z

    .line 49524
    iget-boolean v5, v0, LX/0ND;->c:Z

    iput-boolean v5, v0, LX/0ND;->k:Z

    .line 49525
    :cond_1
    :goto_0
    iget-object v0, p0, LX/0NE;->h:LX/0NH;

    invoke-virtual {v0, p4}, LX/0NH;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 49526
    iget-object v0, p0, LX/0NE;->h:LX/0NH;

    iget-object v0, v0, LX/0NH;->a:[B

    iget-object v1, p0, LX/0NE;->h:LX/0NH;

    iget v1, v1, LX/0NH;->b:I

    invoke-static {v0, v1}, LX/0Oh;->a([BI)I

    move-result v0

    .line 49527
    iget-object v1, p0, LX/0NE;->m:LX/0Oj;

    iget-object v2, p0, LX/0NE;->h:LX/0NH;

    iget-object v2, v2, LX/0NH;->a:[B

    invoke-virtual {v1, v2, v0}, LX/0Oj;->a([BI)V

    .line 49528
    iget-object v0, p0, LX/0NE;->m:LX/0Oj;

    invoke-virtual {v0, v4}, LX/0Oj;->c(I)V

    .line 49529
    iget-object v0, p0, LX/0NE;->c:LX/0NK;

    iget-object v1, p0, LX/0NE;->m:LX/0Oj;

    invoke-virtual {v0, p5, p6, v1}, LX/0NK;->a(JLX/0Oj;)V

    .line 49530
    :cond_2
    iget-object v0, p0, LX/0NE;->i:LX/0NH;

    invoke-virtual {v0, p4}, LX/0NH;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 49531
    iget-object v0, p0, LX/0NE;->i:LX/0NH;

    iget-object v0, v0, LX/0NH;->a:[B

    iget-object v1, p0, LX/0NE;->i:LX/0NH;

    iget v1, v1, LX/0NH;->b:I

    invoke-static {v0, v1}, LX/0Oh;->a([BI)I

    move-result v0

    .line 49532
    iget-object v1, p0, LX/0NE;->m:LX/0Oj;

    iget-object v2, p0, LX/0NE;->i:LX/0NH;

    iget-object v2, v2, LX/0NH;->a:[B

    invoke-virtual {v1, v2, v0}, LX/0Oj;->a([BI)V

    .line 49533
    iget-object v0, p0, LX/0NE;->m:LX/0Oj;

    invoke-virtual {v0, v4}, LX/0Oj;->c(I)V

    .line 49534
    iget-object v0, p0, LX/0NE;->c:LX/0NK;

    iget-object v1, p0, LX/0NE;->m:LX/0Oj;

    invoke-virtual {v0, p5, p6, v1}, LX/0NK;->a(JLX/0Oj;)V

    .line 49535
    :cond_3
    return-void

    .line 49536
    :cond_4
    iget-object v0, p0, LX/0NE;->e:LX/0NH;

    invoke-virtual {v0, p4}, LX/0NH;->b(I)Z

    .line 49537
    iget-object v0, p0, LX/0NE;->f:LX/0NH;

    invoke-virtual {v0, p4}, LX/0NH;->b(I)Z

    .line 49538
    iget-object v0, p0, LX/0NE;->g:LX/0NH;

    invoke-virtual {v0, p4}, LX/0NH;->b(I)Z

    .line 49539
    iget-object v0, p0, LX/0NE;->e:LX/0NH;

    .line 49540
    iget-boolean v1, v0, LX/0NH;->e:Z

    move v0, v1

    .line 49541
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0NE;->f:LX/0NH;

    .line 49542
    iget-boolean v1, v0, LX/0NH;->e:Z

    move v0, v1

    .line 49543
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0NE;->g:LX/0NH;

    .line 49544
    iget-boolean v1, v0, LX/0NH;->e:Z

    move v0, v1

    .line 49545
    if-eqz v0, :cond_1

    .line 49546
    iget-object v0, p0, LX/0N4;->a:LX/0LS;

    iget-object v1, p0, LX/0NE;->e:LX/0NH;

    iget-object v2, p0, LX/0NE;->f:LX/0NH;

    iget-object v3, p0, LX/0NE;->g:LX/0NH;

    invoke-static {v1, v2, v3}, LX/0NE;->a(LX/0NH;LX/0NH;LX/0NH;)LX/0L4;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0LS;->a(LX/0L4;)V

    .line 49547
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0NE;->b:Z

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 49503
    iget-object v0, p0, LX/0NE;->d:[Z

    invoke-static {v0}, LX/0Oh;->a([Z)V

    .line 49504
    iget-object v0, p0, LX/0NE;->e:LX/0NH;

    invoke-virtual {v0}, LX/0NH;->a()V

    .line 49505
    iget-object v0, p0, LX/0NE;->f:LX/0NH;

    invoke-virtual {v0}, LX/0NH;->a()V

    .line 49506
    iget-object v0, p0, LX/0NE;->g:LX/0NH;

    invoke-virtual {v0}, LX/0NH;->a()V

    .line 49507
    iget-object v0, p0, LX/0NE;->h:LX/0NH;

    invoke-virtual {v0}, LX/0NH;->a()V

    .line 49508
    iget-object v0, p0, LX/0NE;->i:LX/0NH;

    invoke-virtual {v0}, LX/0NH;->a()V

    .line 49509
    iget-object v0, p0, LX/0NE;->j:LX/0ND;

    const/4 v1, 0x0

    .line 49510
    iput-boolean v1, v0, LX/0ND;->f:Z

    .line 49511
    iput-boolean v1, v0, LX/0ND;->g:Z

    .line 49512
    iput-boolean v1, v0, LX/0ND;->h:Z

    .line 49513
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0NE;->k:J

    .line 49514
    return-void
.end method

.method public final a(JZ)V
    .locals 1

    .prologue
    .line 49501
    iput-wide p1, p0, LX/0NE;->l:J

    .line 49502
    return-void
.end method

.method public final a(LX/0Oj;)V
    .locals 12

    .prologue
    .line 49479
    :cond_0
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 49480
    iget v0, p1, LX/0Oj;->b:I

    move v0, v0

    .line 49481
    iget v1, p1, LX/0Oj;->c:I

    move v8, v1

    .line 49482
    iget-object v9, p1, LX/0Oj;->a:[B

    .line 49483
    iget-wide v2, p0, LX/0NE;->k:J

    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0NE;->k:J

    .line 49484
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v2

    invoke-interface {v1, p1, v2}, LX/0LS;->a(LX/0Oj;I)V

    .line 49485
    :goto_0
    if-ge v0, v8, :cond_0

    .line 49486
    iget-object v1, p0, LX/0NE;->d:[Z

    invoke-static {v9, v0, v8, v1}, LX/0Oh;->a([BII[Z)I

    move-result v10

    .line 49487
    if-ne v10, v8, :cond_2

    .line 49488
    invoke-direct {p0, v9, v0, v8}, LX/0NE;->a([BII)V

    .line 49489
    :cond_1
    return-void

    .line 49490
    :cond_2
    add-int/lit8 v1, v10, 0x3

    aget-byte v1, v9, v1

    and-int/lit8 v1, v1, 0x7e

    shr-int/lit8 v1, v1, 0x1

    move v11, v1

    .line 49491
    sub-int v1, v10, v0

    .line 49492
    if-lez v1, :cond_3

    .line 49493
    invoke-direct {p0, v9, v0, v10}, LX/0NE;->a([BII)V

    .line 49494
    :cond_3
    sub-int v4, v8, v10

    .line 49495
    iget-wide v2, p0, LX/0NE;->k:J

    int-to-long v6, v4

    sub-long/2addr v2, v6

    .line 49496
    if-gez v1, :cond_4

    neg-int v5, v1

    :goto_1
    iget-wide v6, p0, LX/0NE;->l:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, LX/0NE;->b(JIIJ)V

    .line 49497
    iget-wide v6, p0, LX/0NE;->l:J

    move-object v1, p0

    move v5, v11

    invoke-direct/range {v1 .. v7}, LX/0NE;->a(JIIJ)V

    .line 49498
    add-int/lit8 v0, v10, 0x3

    .line 49499
    goto :goto_0

    .line 49500
    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 49478
    return-void
.end method
