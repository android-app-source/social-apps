.class public final enum LX/04H;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/04H;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/04H;

.field public static final enum CALL_TO_ACTION_VIDEO:LX/04H;

.field public static final enum CHANNEL_DISABLED:LX/04H;

.field public static final enum ELIGIBLE:LX/04H;

.field public static final enum NO_INFO:LX/04H;

.field public static final enum NO_RELATED:LX/04H;

.field public static final enum SPONSORED_VIDEO:LX/04H;

.field public static final enum UNSUPPORTED_LOCATION:LX/04H;

.field public static final enum WRONG_STORY_TYPE:LX/04H;


# instance fields
.field public final eligibility:Ljava/lang/String;

.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 12971
    new-instance v0, LX/04H;

    const-string v1, "ELIGIBLE"

    const-string v2, "eligible"

    invoke-direct {v0, v1, v4, v2}, LX/04H;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04H;->ELIGIBLE:LX/04H;

    .line 12972
    new-instance v0, LX/04H;

    const-string v1, "CHANNEL_DISABLED"

    const-string v2, "channel_disabled"

    invoke-direct {v0, v1, v5, v2}, LX/04H;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04H;->CHANNEL_DISABLED:LX/04H;

    .line 12973
    new-instance v0, LX/04H;

    const-string v1, "NO_RELATED"

    const-string v2, "no_related"

    invoke-direct {v0, v1, v6, v2}, LX/04H;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04H;->NO_RELATED:LX/04H;

    .line 12974
    new-instance v0, LX/04H;

    const-string v1, "NO_INFO"

    const-string v2, "no_info"

    const-string v3, "no_related"

    invoke-direct {v0, v1, v7, v2, v3}, LX/04H;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04H;->NO_INFO:LX/04H;

    .line 12975
    new-instance v0, LX/04H;

    const-string v1, "SPONSORED_VIDEO"

    const-string v2, "sponsored_video"

    invoke-direct {v0, v1, v8, v2}, LX/04H;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04H;->SPONSORED_VIDEO:LX/04H;

    .line 12976
    new-instance v0, LX/04H;

    const-string v1, "CALL_TO_ACTION_VIDEO"

    const/4 v2, 0x5

    const-string v3, "call_to_action_video"

    invoke-direct {v0, v1, v2, v3}, LX/04H;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04H;->CALL_TO_ACTION_VIDEO:LX/04H;

    .line 12977
    new-instance v0, LX/04H;

    const-string v1, "WRONG_STORY_TYPE"

    const/4 v2, 0x6

    const-string v3, "wrong_story_type"

    invoke-direct {v0, v1, v2, v3}, LX/04H;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04H;->WRONG_STORY_TYPE:LX/04H;

    .line 12978
    new-instance v0, LX/04H;

    const-string v1, "UNSUPPORTED_LOCATION"

    const/4 v2, 0x7

    const-string v3, "unsupported_location"

    invoke-direct {v0, v1, v2, v3}, LX/04H;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04H;->UNSUPPORTED_LOCATION:LX/04H;

    .line 12979
    const/16 v0, 0x8

    new-array v0, v0, [LX/04H;

    sget-object v1, LX/04H;->ELIGIBLE:LX/04H;

    aput-object v1, v0, v4

    sget-object v1, LX/04H;->CHANNEL_DISABLED:LX/04H;

    aput-object v1, v0, v5

    sget-object v1, LX/04H;->NO_RELATED:LX/04H;

    aput-object v1, v0, v6

    sget-object v1, LX/04H;->NO_INFO:LX/04H;

    aput-object v1, v0, v7

    sget-object v1, LX/04H;->SPONSORED_VIDEO:LX/04H;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/04H;->CALL_TO_ACTION_VIDEO:LX/04H;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/04H;->WRONG_STORY_TYPE:LX/04H;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/04H;->UNSUPPORTED_LOCATION:LX/04H;

    aput-object v2, v0, v1

    sput-object v0, LX/04H;->$VALUES:[LX/04H;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12980
    invoke-direct {p0, p1, p2, p3, p3}, LX/04H;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 12981
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12982
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12983
    iput-object p3, p0, LX/04H;->value:Ljava/lang/String;

    .line 12984
    iput-object p4, p0, LX/04H;->eligibility:Ljava/lang/String;

    .line 12985
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/04H;
    .locals 1

    .prologue
    .line 12986
    const-class v0, LX/04H;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/04H;

    return-object v0
.end method

.method public static values()[LX/04H;
    .locals 1

    .prologue
    .line 12987
    sget-object v0, LX/04H;->$VALUES:[LX/04H;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/04H;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12988
    iget-object v0, p0, LX/04H;->value:Ljava/lang/String;

    return-object v0
.end method
