.class public LX/09z;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0A1;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0A1;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23170
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0A1;
    .locals 3

    .prologue
    .line 23171
    sget-object v0, LX/09z;->a:LX/0A1;

    if-nez v0, :cond_1

    .line 23172
    const-class v1, LX/09z;

    monitor-enter v1

    .line 23173
    :try_start_0
    sget-object v0, LX/09z;->a:LX/0A1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 23174
    if-eqz v2, :cond_0

    .line 23175
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/09z;->b(LX/0QB;)LX/0A1;

    move-result-object v0

    sput-object v0, LX/09z;->a:LX/0A1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23176
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 23177
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 23178
    :cond_1
    sget-object v0, LX/09z;->a:LX/0A1;

    return-object v0

    .line 23179
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 23180
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/0A1;
    .locals 8

    .prologue
    .line 23181
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/09y;->a(LX/0QB;)LX/09y;

    move-result-object v1

    check-cast v1, LX/09y;

    invoke-static {p0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/19i;->a(LX/0QB;)LX/19s;

    move-result-object v6

    check-cast v6, LX/19s;

    invoke-static {p0}, LX/19l;->a(LX/0QB;)LX/19l;

    move-result-object v7

    check-cast v7, LX/19l;

    invoke-static/range {v0 .. v7}, LX/0A0;->a(Landroid/content/Context;LX/09y;Ljava/util/concurrent/ScheduledExecutorService;LX/0Zb;LX/0kb;LX/03V;LX/19s;LX/19l;)LX/0A1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 23182
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/09y;->a(LX/0QB;)LX/09y;

    move-result-object v1

    check-cast v1, LX/09y;

    invoke-static {p0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/19i;->a(LX/0QB;)LX/19s;

    move-result-object v6

    check-cast v6, LX/19s;

    invoke-static {p0}, LX/19l;->a(LX/0QB;)LX/19l;

    move-result-object v7

    check-cast v7, LX/19l;

    invoke-static/range {v0 .. v7}, LX/0A0;->a(Landroid/content/Context;LX/09y;Ljava/util/concurrent/ScheduledExecutorService;LX/0Zb;LX/0kb;LX/03V;LX/19s;LX/19l;)LX/0A1;

    move-result-object v0

    return-object v0
.end method
