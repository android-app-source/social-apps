.class public final LX/0NH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:[B

.field public b:I

.field private final c:I

.field private d:Z

.field public e:Z


# direct methods
.method public constructor <init>(II)V
    .locals 3

    .prologue
    .line 49786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49787
    iput p1, p0, LX/0NH;->c:I

    .line 49788
    add-int/lit8 v0, p2, 0x3

    new-array v0, v0, [B

    iput-object v0, p0, LX/0NH;->a:[B

    .line 49789
    iget-object v0, p0, LX/0NH;->a:[B

    const/4 v1, 0x2

    const/4 v2, 0x1

    aput-byte v2, v0, v1

    .line 49790
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49791
    iput-boolean v0, p0, LX/0NH;->d:Z

    .line 49792
    iput-boolean v0, p0, LX/0NH;->e:Z

    .line 49793
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49794
    iget-boolean v0, p0, LX/0NH;->d:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 49795
    iget v0, p0, LX/0NH;->c:I

    if-ne p1, v0, :cond_2

    :goto_1
    iput-boolean v1, p0, LX/0NH;->d:Z

    .line 49796
    iget-boolean v0, p0, LX/0NH;->d:Z

    if-eqz v0, :cond_0

    .line 49797
    const/4 v0, 0x3

    iput v0, p0, LX/0NH;->b:I

    .line 49798
    iput-boolean v2, p0, LX/0NH;->e:Z

    .line 49799
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 49800
    goto :goto_0

    :cond_2
    move v1, v2

    .line 49801
    goto :goto_1
.end method

.method public final a([BII)V
    .locals 3

    .prologue
    .line 49802
    iget-boolean v0, p0, LX/0NH;->d:Z

    if-nez v0, :cond_0

    .line 49803
    :goto_0
    return-void

    .line 49804
    :cond_0
    sub-int v0, p3, p2

    .line 49805
    iget-object v1, p0, LX/0NH;->a:[B

    array-length v1, v1

    iget v2, p0, LX/0NH;->b:I

    add-int/2addr v2, v0

    if-ge v1, v2, :cond_1

    .line 49806
    iget-object v1, p0, LX/0NH;->a:[B

    iget v2, p0, LX/0NH;->b:I

    add-int/2addr v2, v0

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iput-object v1, p0, LX/0NH;->a:[B

    .line 49807
    :cond_1
    iget-object v1, p0, LX/0NH;->a:[B

    iget v2, p0, LX/0NH;->b:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49808
    iget v1, p0, LX/0NH;->b:I

    add-int/2addr v0, v1

    iput v0, p0, LX/0NH;->b:I

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 49809
    iget-boolean v0, p0, LX/0NH;->e:Z

    return v0
.end method

.method public final b(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 49810
    iget-boolean v2, p0, LX/0NH;->d:Z

    if-nez v2, :cond_0

    .line 49811
    :goto_0
    return v0

    .line 49812
    :cond_0
    iget v2, p0, LX/0NH;->b:I

    sub-int/2addr v2, p1

    iput v2, p0, LX/0NH;->b:I

    .line 49813
    iput-boolean v0, p0, LX/0NH;->d:Z

    .line 49814
    iput-boolean v1, p0, LX/0NH;->e:Z

    move v0, v1

    .line 49815
    goto :goto_0
.end method
