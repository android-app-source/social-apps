.class public final LX/02V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final sListHead:LX/02V;


# instance fields
.field public final lockFileName:Ljava/io/File;

.field private mChannel:Ljava/nio/channels/FileChannel;

.field private mLockFlags:I

.field private final mLockHandle:LX/02W;

.field private mLockInProgress:Z

.field public mLockOwner:Ljava/lang/Thread;

.field private mLockShareCount:I

.field private mNext:LX/02V;

.field private mPrev:LX/02V;

.field private mReferenceCount:I

.field private mTheLock:Ljava/nio/channels/FileLock;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8676
    new-instance v0, LX/02V;

    invoke-direct {v0}, LX/02V;-><init>()V

    sput-object v0, LX/02V;->sListHead:LX/02V;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8672
    iput-object v0, p0, LX/02V;->lockFileName:Ljava/io/File;

    .line 8673
    iput-object v0, p0, LX/02V;->mLockHandle:LX/02W;

    .line 8674
    iput-object p0, p0, LX/02V;->mNext:LX/02V;

    iput-object p0, p0, LX/02V;->mPrev:LX/02V;

    .line 8675
    return-void
.end method

.method private constructor <init>(Ljava/io/File;Ljava/nio/channels/FileChannel;)V
    .locals 1

    .prologue
    .line 8665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8666
    iput-object p1, p0, LX/02V;->lockFileName:Ljava/io/File;

    .line 8667
    iput-object p2, p0, LX/02V;->mChannel:Ljava/nio/channels/FileChannel;

    .line 8668
    const/4 v0, 0x1

    iput v0, p0, LX/02V;->mReferenceCount:I

    .line 8669
    new-instance v0, LX/02W;

    invoke-direct {v0, p0}, LX/02W;-><init>(LX/02V;)V

    iput-object v0, p0, LX/02V;->mLockHandle:LX/02W;

    .line 8670
    return-void
.end method

.method private addrefLocked()V
    .locals 2

    .prologue
    .line 8661
    iget-object v0, p0, LX/02V;->mChannel:Ljava/nio/channels/FileChannel;

    if-nez v0, :cond_0

    .line 8662
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cannot add reference to dead lock"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8663
    :cond_0
    iget v0, p0, LX/02V;->mReferenceCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/02V;->mReferenceCount:I

    .line 8664
    return-void
.end method

.method private static assertMonitorLockNotHeld(LX/02V;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8658
    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "lock order violation"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 8659
    return-void

    :cond_0
    move v0, v1

    .line 8660
    goto :goto_0
.end method

.method private claimLock(ILjava/nio/channels/FileLock;)V
    .locals 1

    .prologue
    .line 8652
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    .line 8653
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, LX/02V;->mLockOwner:Ljava/lang/Thread;

    .line 8654
    :cond_0
    iput-object p2, p0, LX/02V;->mTheLock:Ljava/nio/channels/FileLock;

    .line 8655
    iput p1, p0, LX/02V;->mLockFlags:I

    .line 8656
    const/4 v0, 0x1

    iput v0, p0, LX/02V;->mLockShareCount:I

    .line 8657
    return-void
.end method

.method public static declared-synchronized open(Ljava/io/File;)LX/02V;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 8677
    const-class v3, LX/02V;

    monitor-enter v3

    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v4

    .line 8678
    sget-object v0, LX/02V;->sListHead:LX/02V;

    iget-object v0, v0, LX/02V;->mNext:LX/02V;

    :goto_0
    sget-object v2, LX/02V;->sListHead:LX/02V;

    if-eq v0, v2, :cond_1

    .line 8679
    iget-object v2, v0, LX/02V;->lockFileName:Ljava/io/File;

    invoke-virtual {v4, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 8680
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 8681
    :try_start_1
    invoke-direct {v0}, LX/02V;->addrefLocked()V

    .line 8682
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8683
    :goto_1
    monitor-exit v3

    return-object v0

    .line 8684
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 8685
    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    .line 8686
    :cond_0
    :try_start_4
    iget-object v0, v0, LX/02V;->mNext:LX/02V;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 8687
    :cond_1
    :try_start_5
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v0, "rw"

    invoke-direct {v2, v4, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 8688
    :try_start_6
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-result-object v2

    .line 8689
    :try_start_7
    new-instance v0, LX/02V;

    invoke-direct {v0, v4, v2}, LX/02V;-><init>(Ljava/io/File;Ljava/nio/channels/FileChannel;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 8690
    :try_start_8
    sget-object v2, LX/02V;->sListHead:LX/02V;

    iput-object v2, v0, LX/02V;->mPrev:LX/02V;

    .line 8691
    sget-object v2, LX/02V;->sListHead:LX/02V;

    iget-object v2, v2, LX/02V;->mNext:LX/02V;

    iput-object v2, v0, LX/02V;->mNext:LX/02V;

    .line 8692
    iget-object v2, v0, LX/02V;->mPrev:LX/02V;

    iput-object v0, v2, LX/02V;->mNext:LX/02V;

    .line 8693
    iget-object v2, v0, LX/02V;->mNext:LX/02V;

    iput-object v0, v2, LX/02V;->mPrev:LX/02V;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 8694
    const/4 v1, 0x0

    :try_start_9
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 8695
    const/4 v1, 0x0

    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    goto :goto_1

    .line 8696
    :catchall_2
    move-exception v0

    move-object v2, v1

    :goto_2
    invoke-static {v2}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 8697
    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 8698
    :catchall_3
    move-exception v0

    goto :goto_2

    :catchall_4
    move-exception v0

    move-object v5, v2

    move-object v2, v1

    move-object v1, v5

    goto :goto_2
.end method


# virtual methods
.method public final acquire(I)LX/02W;
    .locals 2

    .prologue
    .line 8645
    const/4 v1, 0x0

    .line 8646
    const/4 v0, 0x0

    .line 8647
    :try_start_0
    invoke-virtual {p0, p1}, LX/02V;->acquireInterruptubly(I)LX/02W;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 8648
    :goto_0
    if-eqz v1, :cond_0

    .line 8649
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 8650
    :cond_0
    return-object v0

    .line 8651
    :catch_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final acquireInterruptubly(I)LX/02W;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v7, 0x0

    .line 8599
    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_1

    move v6, v0

    .line 8600
    :goto_0
    invoke-static {p0}, LX/02V;->assertMonitorLockNotHeld(LX/02V;)V

    .line 8601
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move v1, v7

    .line 8602
    :goto_1
    :try_start_1
    invoke-virtual {p0, p1}, LX/02V;->tryAcquire(I)LX/02W;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 8603
    iget-object v0, p0, LX/02V;->mLockHandle:LX/02W;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8604
    if-eqz v1, :cond_0

    .line 8605
    invoke-virtual {p0}, LX/02V;->close()V

    :cond_0
    :goto_2
    return-object v0

    :cond_1
    move v6, v7

    .line 8606
    goto :goto_0

    .line 8607
    :cond_2
    :try_start_2
    iget-boolean v2, p0, LX/02V;->mLockInProgress:Z

    if-nez v2, :cond_3

    iget v2, p0, LX/02V;->mLockShareCount:I

    if-eqz v2, :cond_6

    .line 8608
    :cond_3
    if-nez v1, :cond_4

    .line 8609
    invoke-direct {p0}, LX/02V;->addrefLocked()V

    move v1, v0

    .line 8610
    :cond_4
    const v2, 0x413882bc

    invoke-static {p0, v2}, LX/02L;->a(Ljava/lang/Object;I)V

    goto :goto_1

    .line 8611
    :catchall_0
    move-exception v0

    :goto_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 8612
    :catchall_1
    move-exception v0

    move v8, v1

    :goto_4
    if-eqz v8, :cond_5

    .line 8613
    invoke-virtual {p0}, LX/02V;->close()V

    :cond_5
    throw v0

    .line 8614
    :cond_6
    if-nez v1, :cond_9

    .line 8615
    :try_start_4
    invoke-direct {p0}, LX/02V;->addrefLocked()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v8, v0

    .line 8616
    :goto_5
    const/4 v0, 0x1

    :try_start_5
    iput-boolean v0, p0, LX/02V;->mLockInProgress:Z

    .line 8617
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_9

    .line 8618
    const/4 v9, 0x0

    .line 8619
    :try_start_6
    iget-object v1, p0, LX/02V;->mChannel:Ljava/nio/channels/FileChannel;

    const-wide/16 v2, 0x0

    const-wide v4, 0x7fffffffffffffffL

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->lock(JJZ)Ljava/nio/channels/FileLock;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    move-result-object v0

    .line 8620
    if-nez v0, :cond_7

    .line 8621
    :try_start_7
    monitor-enter p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    .line 8622
    const/4 v1, 0x0

    :try_start_8
    iput-boolean v1, p0, LX/02V;->mLockInProgress:Z

    .line 8623
    const v1, -0x26ddd7a5

    invoke-static {p0, v1}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 8624
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_6

    .line 8625
    :try_start_9
    invoke-virtual {p0}, LX/02V;->close()V

    .line 8626
    :cond_7
    monitor-enter p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 8627
    :try_start_a
    invoke-direct {p0, p1, v0}, LX/02V;->claimLock(ILjava/nio/channels/FileLock;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_8

    .line 8628
    const/4 v0, 0x0

    :try_start_b
    iput-boolean v0, p0, LX/02V;->mLockInProgress:Z

    .line 8629
    const v0, -0x1bc2ccb3

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 8630
    iget-object v0, p0, LX/02V;->mLockHandle:LX/02W;

    monitor-exit p0

    goto :goto_2

    .line 8631
    :catchall_2
    move-exception v0

    :goto_6
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :try_start_c
    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 8632
    :catchall_3
    move-exception v0

    move v8, v7

    goto :goto_4

    .line 8633
    :catch_0
    move-exception v0

    .line 8634
    :try_start_d
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    .line 8635
    :catchall_4
    move-exception v0

    if-nez v9, :cond_8

    .line 8636
    :try_start_e
    monitor-enter p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    .line 8637
    const/4 v1, 0x0

    :try_start_f
    iput-boolean v1, p0, LX/02V;->mLockInProgress:Z

    .line 8638
    const v1, -0x676cd223

    invoke-static {p0, v1}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 8639
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_7

    .line 8640
    :try_start_10
    invoke-virtual {p0}, LX/02V;->close()V

    :cond_8
    throw v0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    .line 8641
    :catchall_5
    move-exception v0

    goto :goto_4

    .line 8642
    :catchall_6
    move-exception v0

    :try_start_11
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    :try_start_12
    throw v0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    :catchall_7
    move-exception v0

    :try_start_13
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_7

    :try_start_14
    throw v0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_5

    .line 8643
    :catchall_8
    move-exception v0

    move v7, v8

    goto :goto_6

    .line 8644
    :catchall_9
    move-exception v0

    move v1, v8

    goto :goto_3

    :cond_9
    move v8, v1

    goto :goto_5
.end method

.method public final close()V
    .locals 3

    .prologue
    .line 8577
    invoke-static {p0}, LX/02V;->assertMonitorLockNotHeld(LX/02V;)V

    .line 8578
    monitor-enter p0

    .line 8579
    :try_start_0
    iget-object v0, p0, LX/02V;->mChannel:Ljava/nio/channels/FileChannel;

    if-nez v0, :cond_0

    .line 8580
    monitor-exit p0

    .line 8581
    :goto_0
    return-void

    .line 8582
    :cond_0
    iget v0, p0, LX/02V;->mReferenceCount:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 8583
    iget v0, p0, LX/02V;->mReferenceCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/02V;->mReferenceCount:I

    .line 8584
    monitor-exit p0

    goto :goto_0

    .line 8585
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8586
    const-class v1, LX/02V;

    monitor-enter v1

    .line 8587
    :try_start_2
    monitor-enter p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 8588
    :try_start_3
    iget v0, p0, LX/02V;->mReferenceCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/02V;->mReferenceCount:I

    .line 8589
    iget v0, p0, LX/02V;->mReferenceCount:I

    if-nez v0, :cond_2

    .line 8590
    iget-object v0, p0, LX/02V;->mPrev:LX/02V;

    iget-object v2, p0, LX/02V;->mNext:LX/02V;

    iput-object v2, v0, LX/02V;->mNext:LX/02V;

    .line 8591
    iget-object v0, p0, LX/02V;->mNext:LX/02V;

    iget-object v2, p0, LX/02V;->mPrev:LX/02V;

    iput-object v2, v0, LX/02V;->mPrev:LX/02V;

    .line 8592
    const/4 v0, 0x0

    iput-object v0, p0, LX/02V;->mPrev:LX/02V;

    .line 8593
    const/4 v0, 0x0

    iput-object v0, p0, LX/02V;->mNext:LX/02V;

    .line 8594
    iget-object v0, p0, LX/02V;->mChannel:Ljava/nio/channels/FileChannel;

    invoke-static {v0}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 8595
    const/4 v0, 0x0

    iput-object v0, p0, LX/02V;->mChannel:Ljava/nio/channels/FileChannel;

    .line 8596
    :cond_2
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 8597
    :try_start_4
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 8598
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method public final donateLock(Ljava/lang/Thread;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8572
    iget-object v0, p0, LX/02V;->mLockOwner:Ljava/lang/Thread;

    move-object v0, v0

    .line 8573
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "caller must own lock exclusively"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 8574
    iput-object p1, p0, LX/02V;->mLockOwner:Ljava/lang/Thread;

    .line 8575
    return-void

    :cond_0
    move v0, v1

    .line 8576
    goto :goto_0
.end method

.method public final release()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8550
    invoke-static {p0}, LX/02V;->assertMonitorLockNotHeld(LX/02V;)V

    .line 8551
    monitor-enter p0

    .line 8552
    :try_start_0
    iget v2, p0, LX/02V;->mLockShareCount:I

    if-lez v2, :cond_2

    move v2, v0

    :goto_0
    const-string v3, "lock release balance"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 8553
    iget v2, p0, LX/02V;->mLockFlags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    move v2, v0

    .line 8554
    :goto_1
    if-nez v2, :cond_0

    iget-object v2, p0, LX/02V;->mLockOwner:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v2, v3, :cond_4

    :cond_0
    move v2, v0

    :goto_2
    const-string v3, "lock thread affinity"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 8555
    iget v2, p0, LX/02V;->mLockShareCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, LX/02V;->mLockShareCount:I

    .line 8556
    iget v2, p0, LX/02V;->mLockShareCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_5

    .line 8557
    :try_start_1
    iget-object v1, p0, LX/02V;->mTheLock:Ljava/nio/channels/FileLock;

    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8558
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, LX/02V;->mLockOwner:Ljava/lang/Thread;

    .line 8559
    const/4 v1, 0x0

    iput-object v1, p0, LX/02V;->mTheLock:Ljava/nio/channels/FileLock;

    .line 8560
    const/4 v1, 0x0

    iput v1, p0, LX/02V;->mLockFlags:I

    .line 8561
    const v1, 0x64b579e7

    invoke-static {p0, v1}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 8562
    :goto_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 8563
    if-eqz v0, :cond_1

    .line 8564
    invoke-virtual {p0}, LX/02V;->close()V

    .line 8565
    :cond_1
    return-void

    :cond_2
    move v2, v1

    .line 8566
    goto :goto_0

    :cond_3
    move v2, v1

    .line 8567
    goto :goto_1

    :cond_4
    move v2, v1

    .line 8568
    goto :goto_2

    .line 8569
    :catch_0
    move-exception v0

    .line 8570
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 8571
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :cond_5
    move v0, v1

    goto :goto_3
.end method

.method public final stealLock()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8545
    iget-object v0, p0, LX/02V;->mLockOwner:Ljava/lang/Thread;

    move-object v0, v0

    .line 8546
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "cannot steal unowned lock"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 8547
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, LX/02V;->mLockOwner:Ljava/lang/Thread;

    .line 8548
    return-void

    :cond_0
    move v0, v1

    .line 8549
    goto :goto_0
.end method

.method public final declared-synchronized tryAcquire(I)LX/02W;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 8522
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, LX/02V;->mChannel:Ljava/nio/channels/FileChannel;

    if-nez v3, :cond_0

    .line 8523
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cannot acquire closed lock"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8524
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 8525
    :cond_0
    and-int/lit8 v3, p1, 0x1

    if-eqz v3, :cond_2

    move v6, v1

    .line 8526
    :goto_0
    :try_start_1
    iget-boolean v3, p0, LX/02V;->mLockInProgress:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v3, :cond_3

    .line 8527
    :cond_1
    :goto_1
    monitor-exit p0

    return-object v0

    :cond_2
    move v6, v2

    .line 8528
    goto :goto_0

    .line 8529
    :cond_3
    :try_start_2
    iget v3, p0, LX/02V;->mLockShareCount:I

    if-lez v3, :cond_7

    .line 8530
    iget v3, p0, LX/02V;->mLockFlags:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_6

    .line 8531
    :goto_2
    if-eqz v6, :cond_4

    if-nez v1, :cond_5

    :cond_4
    if-nez v1, :cond_1

    iget-object v1, p0, LX/02V;->mLockOwner:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v1, v2, :cond_1

    .line 8532
    :cond_5
    iget v0, p0, LX/02V;->mLockShareCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/02V;->mLockShareCount:I

    .line 8533
    iget-object v0, p0, LX/02V;->mLockHandle:LX/02W;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :cond_6
    move v1, v2

    .line 8534
    goto :goto_2

    .line 8535
    :cond_7
    :try_start_3
    iget-object v1, p0, LX/02V;->mChannel:Ljava/nio/channels/FileChannel;

    const-wide/16 v2, 0x0

    const-wide v4, 0x7fffffffffffffffL

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->tryLock(JJZ)Ljava/nio/channels/FileLock;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 8536
    :goto_3
    if-eqz v1, :cond_1

    .line 8537
    invoke-direct {p0}, LX/02V;->addrefLocked()V

    .line 8538
    invoke-direct {p0, p1, v1}, LX/02V;->claimLock(ILjava/nio/channels/FileLock;)V

    .line 8539
    iget-object v0, p0, LX/02V;->mLockHandle:LX/02W;

    goto :goto_1

    .line 8540
    :catch_0
    move-exception v1

    .line 8541
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 8542
    if-eqz v2, :cond_9

    const-string v3, ": EAGAIN ("

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, ": errno 11 ("

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_8
    move-object v1, v0

    .line 8543
    goto :goto_3

    .line 8544
    :cond_9
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
