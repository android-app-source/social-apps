.class public abstract LX/0DV;
.super Landroid/os/Binder;
.source ""

# interfaces
.implements LX/0DT;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30455
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 30456
    const-string v0, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    invoke-virtual {p0, p0, v0}, LX/0DV;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 30457
    return-void
.end method


# virtual methods
.method public final asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 30458
    return-object p0
.end method

.method public final onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 20

    .prologue
    .line 30459
    sparse-switch p1, :sswitch_data_0

    .line 30460
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    .line 30461
    :sswitch_0
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 30462
    const/4 v2, 0x1

    goto :goto_0

    .line 30463
    :sswitch_1
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30464
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 30465
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0DV;->a(Ljava/lang/String;)I

    move-result v2

    .line 30466
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30467
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 30468
    const/4 v2, 0x1

    goto :goto_0

    .line 30469
    :sswitch_2
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30470
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 30471
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 30472
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/0DV;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 30473
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30474
    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 30475
    const/4 v2, 0x1

    goto :goto_0

    .line 30476
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 30477
    :sswitch_3
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30478
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 30479
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 30480
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 30481
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/0DV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 30482
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30483
    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_2
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 30484
    const/4 v2, 0x1

    goto :goto_0

    .line 30485
    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    .line 30486
    :sswitch_4
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30487
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 30488
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    .line 30489
    sget-object v2, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 30490
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/0DV;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 30491
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30492
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30493
    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    .line 30494
    :sswitch_5
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30495
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 30496
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_3

    .line 30497
    sget-object v2, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 30498
    :goto_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/0DV;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 30499
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30500
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30501
    :cond_3
    const/4 v2, 0x0

    goto :goto_4

    .line 30502
    :sswitch_6
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30503
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 30504
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    .line 30505
    :goto_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/0DV;->a(Ljava/lang/String;Z)V

    .line 30506
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30507
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30508
    :cond_4
    const/4 v2, 0x0

    goto :goto_5

    .line 30509
    :sswitch_7
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30510
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 30511
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    .line 30512
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 30513
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    .line 30514
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v10

    .line 30515
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v12

    .line 30516
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v14

    .line 30517
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_5

    const/4 v15, 0x1

    .line 30518
    :goto_6
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_6

    const/16 v16, 0x1

    .line 30519
    :goto_7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_7

    const/16 v17, 0x1

    .line 30520
    :goto_8
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 30521
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v18

    .line 30522
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_8

    const/16 v19, 0x1

    :goto_9
    move-object/from16 v2, p0

    .line 30523
    invoke-virtual/range {v2 .. v19}, LX/0DV;->a(Ljava/lang/String;JJJJJIZZZLjava/util/Map;Z)V

    .line 30524
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30525
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30526
    :cond_5
    const/4 v15, 0x0

    goto :goto_6

    .line 30527
    :cond_6
    const/16 v16, 0x0

    goto :goto_7

    .line 30528
    :cond_7
    const/16 v17, 0x0

    goto :goto_8

    .line 30529
    :cond_8
    const/16 v19, 0x0

    goto :goto_9

    .line 30530
    :sswitch_8
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30531
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 30532
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 30533
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_9

    .line 30534
    sget-object v2, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 30535
    :goto_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, LX/0DV;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 30536
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30537
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30538
    :cond_9
    const/4 v2, 0x0

    goto :goto_a

    .line 30539
    :sswitch_9
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30540
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 30541
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_a

    .line 30542
    sget-object v2, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 30543
    :goto_b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 30544
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2, v4}, LX/0DV;->a(Ljava/lang/String;Landroid/os/Bundle;I)V

    .line 30545
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30546
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30547
    :cond_a
    const/4 v2, 0x0

    goto :goto_b

    .line 30548
    :sswitch_a
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30549
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 30550
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 30551
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/0DV;->a(Ljava/lang/String;I)V

    .line 30552
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30553
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30554
    :sswitch_b
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30555
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createLongArray()[J

    move-result-object v2

    .line 30556
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0DV;->a([J)V

    .line 30557
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30558
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30559
    :sswitch_c
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30560
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 30561
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 30562
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 30563
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 30564
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    move-object/from16 v2, p0

    .line 30565
    invoke-virtual/range {v2 .. v7}, LX/0DV;->a(Ljava/lang/String;Ljava/lang/String;III)V

    .line 30566
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30567
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30568
    :sswitch_d
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30569
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 30570
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v2

    .line 30571
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0DV;->a(Ljava/util/Map;)V

    .line 30572
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30573
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30574
    :sswitch_e
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30575
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 30576
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v3

    .line 30577
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_b

    .line 30578
    sget-object v2, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 30579
    :goto_c
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/0DV;->a(Ljava/util/Map;Landroid/os/Bundle;)V

    .line 30580
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30581
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30582
    :cond_b
    const/4 v2, 0x0

    goto :goto_c

    .line 30583
    :sswitch_f
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30584
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 30585
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 30586
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/0DV;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 30587
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30588
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30589
    :sswitch_10
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30590
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 30591
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 30592
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/0DV;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 30593
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30594
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30595
    :sswitch_11
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30596
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 30597
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0DV;->b(Ljava/lang/String;)V

    .line 30598
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30599
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30600
    :sswitch_12
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30601
    invoke-virtual/range {p0 .. p0}, LX/0DV;->a()Ljava/util/List;

    move-result-object v2

    .line 30602
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30603
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 30604
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30605
    :sswitch_13
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30606
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 30607
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0DV;->c(Ljava/lang/String;)Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    move-result-object v2

    .line 30608
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30609
    if-eqz v2, :cond_c

    .line 30610
    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 30611
    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;->writeToParcel(Landroid/os/Parcel;I)V

    .line 30612
    :goto_d
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30613
    :cond_c
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_d

    .line 30614
    :sswitch_14
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30615
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 30616
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0DV;->a(I)V

    .line 30617
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30618
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30619
    :sswitch_15
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30620
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_d

    .line 30621
    sget-object v2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    .line 30622
    :goto_e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, LX/0Dc;->a(Landroid/os/IBinder;)LX/0Da;

    move-result-object v3

    .line 30623
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/0DV;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;LX/0Da;)V

    .line 30624
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30625
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30626
    :cond_d
    const/4 v2, 0x0

    goto :goto_e

    .line 30627
    :sswitch_16
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30628
    invoke-virtual/range {p0 .. p0}, LX/0DV;->b()V

    .line 30629
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30630
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30631
    :sswitch_17
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30632
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 30633
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    .line 30634
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v3

    .line 30635
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/0DV;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 30636
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30637
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30638
    :sswitch_18
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30639
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 30640
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0DV;->d(Ljava/lang/String;)Z

    move-result v2

    .line 30641
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30642
    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_f
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 30643
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30644
    :cond_e
    const/4 v2, 0x0

    goto :goto_f

    .line 30645
    :sswitch_19
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30646
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_f

    .line 30647
    sget-object v2, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 30648
    :goto_10
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0DV;->a(Landroid/os/Bundle;)V

    .line 30649
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30650
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30651
    :cond_f
    const/4 v2, 0x0

    goto :goto_10

    .line 30652
    :sswitch_1a
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30653
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_10

    .line 30654
    sget-object v2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    .line 30655
    :goto_11
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0DV;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 30656
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30657
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30658
    :cond_10
    const/4 v2, 0x0

    goto :goto_11

    .line 30659
    :sswitch_1b
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30660
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 30661
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v3

    .line 30662
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_11

    .line 30663
    sget-object v2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    .line 30664
    :goto_12
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/0DV;->a(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 30665
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30666
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30667
    :cond_11
    const/4 v2, 0x0

    goto :goto_12

    .line 30668
    :sswitch_1c
    const-string v2, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 30669
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_12

    .line 30670
    sget-object v2, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 30671
    :goto_13
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0DV;->b(Landroid/os/Bundle;)V

    .line 30672
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 30673
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 30674
    :cond_12
    const/4 v2, 0x0

    goto :goto_13

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
