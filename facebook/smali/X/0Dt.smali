.class public LX/0Dt;
.super LX/0Dp;
.source ""


# instance fields
.field private d:Landroid/animation/ObjectAnimator;

.field public e:LX/0Cm;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31183
    invoke-direct {p0}, LX/0Dp;-><init>()V

    .line 31184
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 31174
    iget-object v0, p0, LX/0Dt;->d:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Dt;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31175
    iget-object v0, p0, LX/0Dt;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 31176
    :cond_0
    iget-object v0, p0, LX/0Dp;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 31177
    iget-object v0, p0, LX/0Dp;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 31178
    if-eqz p1, :cond_1

    iget-object v1, p0, LX/0Dp;->c:Landroid/view/View;

    const-string v2, "translationY"

    new-array v3, v3, [F

    int-to-float v0, v0

    aput v0, v3, v4

    aput v5, v3, v6

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/0Dt;->d:Landroid/animation/ObjectAnimator;

    .line 31179
    iget-object v0, p0, LX/0Dt;->d:Landroid/animation/ObjectAnimator;

    new-instance v1, LX/0Ds;

    invoke-direct {v1, p0}, LX/0Ds;-><init>(LX/0Dt;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 31180
    iget-object v0, p0, LX/0Dt;->d:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 31181
    return-void

    .line 31182
    :cond_1
    iget-object v1, p0, LX/0Dp;->c:Landroid/view/View;

    const-string v2, "translationY"

    new-array v3, v3, [F

    aput v5, v3, v4

    int-to-float v0, v0

    aput v0, v3, v6

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto :goto_0
.end method
