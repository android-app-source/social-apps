.class public LX/02y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02z;
.implements LX/02w;


# instance fields
.field private a:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9745
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/02y;->a:Ljava/util/Random;

    .line 9746
    return-void
.end method

.method public static b(ILX/0Pk;)I
    .locals 1

    .prologue
    .line 9740
    check-cast p1, LX/0Pl;

    .line 9741
    invoke-virtual {p1, p0}, LX/0Pl;->a(I)Lcom/facebook/loom/config/QPLTraceControlConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->d()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(ILX/0Pk;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 9731
    check-cast p2, LX/0Pl;

    .line 9732
    invoke-virtual {p2, p1}, LX/0Pl;->a(I)Lcom/facebook/loom/config/QPLTraceControlConfiguration;

    move-result-object v1

    .line 9733
    if-nez v1, :cond_1

    .line 9734
    :cond_0
    :goto_0
    return v0

    .line 9735
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->b()I

    move-result v2

    .line 9736
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    if-eqz v2, :cond_0

    .line 9737
    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    iget-object v3, p0, LX/02y;->a:Ljava/util/Random;

    invoke-virtual {v3, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    if-nez v2, :cond_0

    .line 9738
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->c()I

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;LX/0Pk;)I
    .locals 1

    .prologue
    .line 9742
    check-cast p1, LX/0Pn;

    invoke-virtual {p1}, LX/0Pn;->k()I

    move-result v0

    .line 9743
    invoke-virtual {p0, v0, p2}, LX/02y;->a(ILX/0Pk;)I

    move-result v0

    return v0
.end method

.method public final a(II)Z
    .locals 1

    .prologue
    .line 9739
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILjava/lang/Object;ILjava/lang/Object;)Z
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 9730
    if-ne p2, p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/Object;LX/0Pk;)I
    .locals 1

    .prologue
    .line 9728
    check-cast p1, LX/0Pn;

    invoke-virtual {p1}, LX/0Pn;->k()I

    move-result v0

    .line 9729
    invoke-static {v0, p2}, LX/02y;->b(ILX/0Pk;)I

    move-result v0

    return v0
.end method
