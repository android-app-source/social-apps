.class public LX/04w;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation


# static fields
.field private static a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14810
    const v0, 0x80e8

    sput v0, LX/04w;->a:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 14811
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14812
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)LX/00b;
    .locals 10

    .prologue
    .line 14813
    sget-object v0, LX/00d;->a:LX/00d;

    move-object v0, v0

    .line 14814
    iget-object v1, v0, LX/00d;->c:Ljava/lang/String;

    move-object v0, v1

    .line 14815
    sget-object v1, LX/00d;->a:LX/00d;

    move-object v1, v1

    .line 14816
    iget-wide v6, v1, LX/00d;->e:J

    const-wide/32 v8, 0x44aa200

    invoke-static {v6, v7, v8, v9}, LX/00d;->a(JJ)Z

    move-result v6

    if-nez v6, :cond_2

    .line 14817
    iget-object v6, v1, LX/00d;->d:[B

    .line 14818
    :goto_0
    move-object v1, v6

    .line 14819
    if-nez v1, :cond_0

    .line 14820
    sget-object v0, LX/00b;->ERROR_IP_ADDRESS:LX/00b;

    .line 14821
    :goto_1
    return-object v0

    .line 14822
    :cond_0
    invoke-static {v0, v1}, Ljava/net/InetAddress;->getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;

    move-result-object v1

    .line 14823
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "https"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "encryptedrequest"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "channel_id"

    invoke-virtual {v0, v2, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "encrypted_request"

    invoke-virtual {v0, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 14824
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 14825
    array-length v2, v0

    .line 14826
    const/16 v3, 0x5c0

    if-le v2, v3, :cond_1

    .line 14827
    sget-object v0, LX/00b;->ERROR_SIZE_LIMIT:LX/00b;

    goto :goto_1

    .line 14828
    :cond_1
    new-instance v3, Ljava/net/DatagramSocket;

    invoke-direct {v3}, Ljava/net/DatagramSocket;-><init>()V

    .line 14829
    new-instance v4, Ljava/net/DatagramPacket;

    sget v5, LX/04w;->a:I

    invoke-direct {v4, v0, v2, v1, v5}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    .line 14830
    invoke-virtual {v3, v4}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    .line 14831
    invoke-virtual {v3}, Ljava/net/DatagramSocket;->close()V

    .line 14832
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes sent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14833
    sget-object v0, LX/00b;->SENT:LX/00b;

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized a(Ljava/lang/String;Ljava/lang/String;J)LX/00b;
    .locals 8

    .prologue
    .line 14834
    const-class v1, LX/04w;

    monitor-enter v1

    .line 14835
    :try_start_0
    sget-object v0, LX/00c;->a:LX/00c;

    move-object v0, v0

    .line 14836
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, LX/00c;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 14837
    :cond_0
    sget-object v0, LX/00b;->ERROR_ENCRYPT_CHANNEL:LX/00b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 14838
    :goto_0
    monitor-exit v1

    return-object v0

    .line 14839
    :cond_1
    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 14840
    const-string v3, "url"

    invoke-virtual {v2, v3, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 14841
    const-string v3, "timestamp"

    invoke-virtual {v2, v3, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 14842
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 14843
    iget-object v3, v0, LX/00c;->d:Ljava/lang/String;

    move-object v3, v3

    .line 14844
    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    .line 14845
    iget-object v4, v0, LX/00c;->e:Ljava/lang/String;

    move-object v4, v4

    .line 14846
    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v4

    .line 14847
    iget-object v5, v0, LX/00c;->c:Ljava/lang/String;

    move-object v5, v5

    .line 14848
    const-string v6, "UTF-8"

    invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    .line 14849
    new-instance v6, LX/04x;

    new-instance v7, Ljava/security/SecureRandom;

    invoke-direct {v7}, Ljava/security/SecureRandom;-><init>()V

    invoke-direct {v6, v3, v4, v7}, LX/04x;-><init>([B[BLjava/security/SecureRandom;)V

    .line 14850
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 14851
    new-instance v4, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v4, v3}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 14852
    invoke-virtual {v4, v2}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    .line 14853
    invoke-virtual {v4}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 14854
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 14855
    array-length v3, v2

    rem-int/lit8 v3, v3, 0x10

    rsub-int/lit8 v3, v3, 0x10

    .line 14856
    array-length v4, v2

    add-int/2addr v4, v3

    .line 14857
    add-int/lit8 v4, v4, 0x31

    .line 14858
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 14859
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 14860
    const/4 p2, 0x0

    .line 14861
    new-array v7, v3, [B

    .line 14862
    iget-object p0, v6, LX/04x;->d:Ljava/security/SecureRandom;

    invoke-virtual {p0, v7}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 14863
    add-int/lit8 p0, v3, -0x1

    int-to-byte p1, v3

    aput-byte p1, v7, p0

    .line 14864
    array-length p0, v2

    add-int/2addr p0, v3

    new-array p0, p0, [B

    .line 14865
    array-length p1, v2

    invoke-static {v2, p2, p0, p2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 14866
    array-length p1, v2

    invoke-static {v7, p2, p0, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 14867
    move-object v3, p0

    .line 14868
    invoke-static {v6, v3}, LX/04x;->a(LX/04x;[B)[B

    move-result-object v3

    .line 14869
    iget-object v7, v6, LX/04x;->e:[B

    invoke-static {v6, v7, v3, v5}, LX/04x;->a(LX/04x;[B[B[B)[B

    move-result-object v7

    .line 14870
    if-nez v7, :cond_3

    .line 14871
    const/4 v3, 0x0

    .line 14872
    :goto_1
    move-object v2, v3

    .line 14873
    if-nez v2, :cond_2

    .line 14874
    const-string v0, "UDPPacketSender"

    const-string v2, "UDP Priming packet encryption failed, might because of the preconditioncheck failed in function encryptMacForInitializationVector"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 14875
    sget-object v0, LX/00b;->ERROR_ENCRYPTING:LX/00b;

    goto/16 :goto_0

    .line 14876
    :cond_2
    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 14877
    iget-object v3, v0, LX/00c;->c:Ljava/lang/String;

    move-object v0, v3

    .line 14878
    invoke-static {v0, v2}, LX/04w;->a(Ljava/lang/String;Ljava/lang/String;)LX/00b;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto/16 :goto_0

    .line 14879
    :catch_0
    move-exception v0

    .line 14880
    :try_start_2
    const-string v2, "UDPPacketSender"

    const-string v3, "Json exception when apply json encoding in UDPPacketSender"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 14881
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 14882
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 14883
    :catch_1
    move-exception v0

    .line 14884
    :try_start_3
    const-string v2, "UDPPacketSender"

    const-string v3, "Encryption exception in UDPPacketSender"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 14885
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 14886
    :catch_2
    move-exception v0

    .line 14887
    const-string v2, "UDPPacketSender"

    const-string v3, "IO exception in UDPPacketSender"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 14888
    sget-object v0, LX/00b;->ERROR_NETWORK_IO:LX/00b;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 14889
    :cond_3
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 14890
    iget-object v7, v6, LX/04x;->e:[B

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 14891
    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 14892
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    goto :goto_1
.end method
