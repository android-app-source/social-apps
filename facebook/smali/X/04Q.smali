.class public final LX/04Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final file:Ljava/io/RandomAccessFile;

.field public final fileName:Ljava/io/File;

.field public final synthetic this$0:LX/01e;


# direct methods
.method public constructor <init>(LX/01e;Ljava/io/File;Ljava/io/RandomAccessFile;)V
    .locals 0

    .prologue
    .line 13194
    iput-object p1, p0, LX/04Q;->this$0:LX/01e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13195
    iput-object p2, p0, LX/04Q;->fileName:Ljava/io/File;

    .line 13196
    iput-object p3, p0, LX/04Q;->file:Ljava/io/RandomAccessFile;

    .line 13197
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 3

    .prologue
    .line 13198
    iget-object v1, p0, LX/04Q;->this$0:LX/01e;

    monitor-enter v1

    .line 13199
    :try_start_0
    iget-object v0, p0, LX/04Q;->this$0:LX/01e;

    iget-object v0, v0, LX/01e;->mHazardList:Ljava/util/HashSet;

    iget-object v2, p0, LX/04Q;->fileName:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 13200
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13201
    iget-object v0, p0, LX/04Q;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 13202
    return-void

    .line 13203
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
