.class public final enum LX/06w;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/06x;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/06w;",
        ">;",
        "LX/06x;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/06w;

.field public static final enum ConnectTriggerReason:LX/06w;

.field public static final enum ConnectingMs:LX/06w;

.field public static final enum CountConnectAttempt:LX/06w;

.field public static final enum CountSuccessfulConnection:LX/06w;

.field public static final enum LastConnectFailureReason:LX/06w;

.field public static final enum LastDisconnectReason:LX/06w;


# instance fields
.field private final mJsonKey:Ljava/lang/String;

.field private final mType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 18283
    new-instance v0, LX/06w;

    const-string v1, "CountSuccessfulConnection"

    const-string v2, "sc"

    const-class v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v5, v2, v3}, LX/06w;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/06w;->CountSuccessfulConnection:LX/06w;

    .line 18284
    new-instance v0, LX/06w;

    const-string v1, "CountConnectAttempt"

    const-string v2, "ca"

    const-class v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v6, v2, v3}, LX/06w;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/06w;->CountConnectAttempt:LX/06w;

    .line 18285
    new-instance v0, LX/06w;

    const-string v1, "ConnectingMs"

    const-string v2, "ce"

    const-class v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v7, v2, v3}, LX/06w;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/06w;->ConnectingMs:LX/06w;

    .line 18286
    new-instance v0, LX/06w;

    const-string v1, "ConnectTriggerReason"

    const-string v2, "tr"

    const-class v3, Ljava/lang/String;

    invoke-direct {v0, v1, v8, v2, v3}, LX/06w;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/06w;->ConnectTriggerReason:LX/06w;

    .line 18287
    new-instance v0, LX/06w;

    const-string v1, "LastConnectFailureReason"

    const-string v2, "fr"

    const-class v3, Ljava/lang/String;

    invoke-direct {v0, v1, v9, v2, v3}, LX/06w;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/06w;->LastConnectFailureReason:LX/06w;

    .line 18288
    new-instance v0, LX/06w;

    const-string v1, "LastDisconnectReason"

    const/4 v2, 0x5

    const-string v3, "dr"

    const-class v4, Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, LX/06w;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/06w;->LastDisconnectReason:LX/06w;

    .line 18289
    const/4 v0, 0x6

    new-array v0, v0, [LX/06w;

    sget-object v1, LX/06w;->CountSuccessfulConnection:LX/06w;

    aput-object v1, v0, v5

    sget-object v1, LX/06w;->CountConnectAttempt:LX/06w;

    aput-object v1, v0, v6

    sget-object v1, LX/06w;->ConnectingMs:LX/06w;

    aput-object v1, v0, v7

    sget-object v1, LX/06w;->ConnectTriggerReason:LX/06w;

    aput-object v1, v0, v8

    sget-object v1, LX/06w;->LastConnectFailureReason:LX/06w;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/06w;->LastDisconnectReason:LX/06w;

    aput-object v2, v0, v1

    sput-object v0, LX/06w;->$VALUES:[LX/06w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 18275
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18276
    iput-object p3, p0, LX/06w;->mJsonKey:Ljava/lang/String;

    .line 18277
    iput-object p4, p0, LX/06w;->mType:Ljava/lang/Class;

    .line 18278
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/06w;
    .locals 1

    .prologue
    .line 18282
    const-class v0, LX/06w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/06w;

    return-object v0
.end method

.method public static values()[LX/06w;
    .locals 1

    .prologue
    .line 18281
    sget-object v0, LX/06w;->$VALUES:[LX/06w;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/06w;

    return-object v0
.end method


# virtual methods
.method public final getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18280
    iget-object v0, p0, LX/06w;->mJsonKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getValueType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 18279
    iget-object v0, p0, LX/06w;->mType:Ljava/lang/Class;

    return-object v0
.end method
