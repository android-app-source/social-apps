.class public final LX/0It;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field public final synthetic a:LX/0Iu;

.field private b:I


# direct methods
.method public constructor <init>(LX/0Iu;I)V
    .locals 0

    .prologue
    .line 39016
    iput-object p1, p0, LX/0It;->a:LX/0Iu;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 39017
    iput p2, p0, LX/0It;->b:I

    .line 39018
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 1

    .prologue
    .line 39015
    iget v0, p0, LX/0It;->b:I

    return v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 39012
    :goto_0
    invoke-virtual {p0}, LX/0It;->available()I

    move-result v0

    if-lez v0, :cond_0

    .line 39013
    invoke-virtual {p0}, LX/0It;->available()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, LX/0It;->skip(J)J

    goto :goto_0

    .line 39014
    :cond_0
    return-void
.end method

.method public final read()I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 38998
    iget v1, p0, LX/0It;->b:I

    if-nez v1, :cond_0

    .line 38999
    :goto_0
    return v0

    .line 39000
    :cond_0
    iget-object v1, p0, LX/0It;->a:LX/0Iu;

    iget-object v1, v1, LX/0Iu;->c:Lcom/facebook/xzdecoder/XzInputStream;

    invoke-virtual {v1}, Lcom/facebook/xzdecoder/XzInputStream;->read()I

    move-result v1

    .line 39001
    if-ne v1, v0, :cond_1

    .line 39002
    new-instance v0, Ljava/io/IOException;

    const-string v1, "xz stream terminated prematurely"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39003
    :cond_1
    iget v0, p0, LX/0It;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0It;->b:I

    move v0, v1

    .line 39004
    goto :goto_0
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    .line 39005
    if-lez p3, :cond_1

    iget v0, p0, LX/0It;->b:I

    if-nez v0, :cond_1

    .line 39006
    const/4 v0, -0x1

    .line 39007
    :cond_0
    :goto_0
    return v0

    .line 39008
    :cond_1
    iget v0, p0, LX/0It;->b:I

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 39009
    iget-object v1, p0, LX/0It;->a:LX/0Iu;

    iget-object v1, v1, LX/0Iu;->c:Lcom/facebook/xzdecoder/XzInputStream;

    invoke-virtual {v1, p1, p2, v0}, Lcom/facebook/xzdecoder/XzInputStream;->read([BII)I

    move-result v0

    .line 39010
    if-lez v0, :cond_0

    .line 39011
    iget v1, p0, LX/0It;->b:I

    sub-int/2addr v1, v0

    iput v1, p0, LX/0It;->b:I

    goto :goto_0
.end method
