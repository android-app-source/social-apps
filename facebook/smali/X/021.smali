.class public LX/021;
.super LX/022;
.source ""


# instance fields
.field public final a:Ljava/io/File;

.field public final b:I


# direct methods
.method public constructor <init>(Ljava/io/File;I)V
    .locals 0

    .prologue
    .line 5982
    invoke-direct {p0}, LX/022;-><init>()V

    .line 5983
    iput-object p1, p0, LX/021;->a:Ljava/io/File;

    .line 5984
    iput p2, p0, LX/021;->b:I

    .line 5985
    return-void
.end method

.method public static a(Ljava/io/File;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 5986
    sget-boolean v0, LX/01L;->a:Z

    if-eqz v0, :cond_0

    .line 5987
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SoLoader.getElfDependencies["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/04t;->a(Ljava/lang/String;)V

    .line 5988
    :cond_0
    :try_start_0
    invoke-static {p0}, LX/02E;->a(Ljava/io/File;)[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 5989
    sget-boolean v1, LX/01L;->a:Z

    if-eqz v1, :cond_1

    .line 5990
    invoke-static {}, LX/04t;->a()V

    :cond_1
    return-object v0

    .line 5991
    :catchall_0
    move-exception v0

    sget-boolean v1, LX/01L;->a:Z

    if-eqz v1, :cond_2

    .line 5992
    invoke-static {}, LX/04t;->a()V

    :cond_2
    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)I
    .locals 6

    .prologue
    .line 5993
    iget-object v0, p0, LX/021;->a:Ljava/io/File;

    const/4 v1, 0x0

    .line 5994
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 5995
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 5996
    :goto_0
    move v0, v1

    .line 5997
    return v0

    .line 5998
    :cond_0
    and-int/lit8 v3, p2, 0x1

    if-eqz v3, :cond_1

    iget v3, p0, LX/021;->b:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_1

    .line 5999
    const/4 v1, 0x2

    goto :goto_0

    .line 6000
    :cond_1
    iget v3, p0, LX/021;->b:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_3

    .line 6001
    invoke-static {v2}, LX/021;->a(Ljava/io/File;)[Ljava/lang/String;

    move-result-object v3

    .line 6002
    :goto_1
    array-length v4, v3

    if-ge v1, v4, :cond_3

    .line 6003
    aget-object v4, v3, v1

    .line 6004
    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 6005
    or-int/lit8 v5, p2, 0x1

    invoke-static {v4, v5}, LX/01L;->b(Ljava/lang/String;I)V

    .line 6006
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6007
    :cond_3
    sget-object v1, LX/01L;->b:LX/020;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, LX/020;->a(Ljava/lang/String;I)V

    .line 6008
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 6009
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/021;->a:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 6010
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6011
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 6012
    iget-object v0, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 6013
    return-void
.end method
