.class public final LX/085;
.super LX/082;
.source ""


# instance fields
.field private final mBuffer:[B

.field private final mDsConfig:LX/080;


# direct methods
.method public constructor <init>(LX/080;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 20659
    invoke-direct {p0, p2}, LX/082;-><init>(Ljava/io/File;)V

    .line 20660
    const/high16 v0, 0x10000

    new-array v0, v0, [B

    iput-object v0, p0, LX/085;->mBuffer:[B

    .line 20661
    iput-object p1, p0, LX/085;->mDsConfig:LX/080;

    .line 20662
    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 20663
    invoke-direct {p0, p1}, LX/082;-><init>(Ljava/io/File;)V

    .line 20664
    const/high16 v0, 0x10000

    new-array v0, v0, [B

    iput-object v0, p0, LX/085;->mBuffer:[B

    .line 20665
    const/4 v0, 0x0

    iput-object v0, p0, LX/085;->mDsConfig:LX/080;

    .line 20666
    return-void
.end method


# virtual methods
.method public final addDexOptOptions(LX/086;)V
    .locals 1

    .prologue
    .line 20667
    iget-object v0, p0, LX/085;->mDsConfig:LX/080;

    if-eqz v0, :cond_0

    .line 20668
    iget-object v0, p0, LX/085;->mDsConfig:LX/080;

    invoke-static {v0, p1}, LX/02b;->addConfiguredDexOptOptions(LX/080;LX/086;)V

    .line 20669
    :cond_0
    return-void
.end method

.method public final copyDexToOdex(Ljava/io/InputStream;ILjava/io/RandomAccessFile;)I
    .locals 2

    .prologue
    .line 20670
    const v0, 0x7fffffff

    iget-object v1, p0, LX/085;->mBuffer:[B

    invoke-static {p3, p1, v0, v1}, LX/02Q;->copyBytes(Ljava/io/RandomAccessFile;Ljava/io/InputStream;I[B)I

    move-result v0

    return v0
.end method
