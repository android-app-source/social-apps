.class public final LX/0KX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0KW;


# instance fields
.field public final synthetic a:Lcom/facebook/video/vps/VideoPlayerService;

.field private final b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;


# direct methods
.method public constructor <init>(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 0

    .prologue
    .line 41419
    iput-object p1, p0, LX/0KX;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41420
    iput-object p2, p0, LX/0KX;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 41421
    return-void
.end method


# virtual methods
.method public final a(LX/0Kv;)V
    .locals 4

    .prologue
    .line 41422
    const-string v1, "ERROR_IO"

    .line 41423
    invoke-virtual {p1}, LX/0Kv;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 41424
    iget-object v0, p0, LX/0KX;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    invoke-static {v0}, LX/040;->au(Ljava/util/Map;)Z

    move-result v2

    .line 41425
    invoke-virtual {p1}, LX/0Kv;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, LX/0OL;

    if-eqz v0, :cond_1

    .line 41426
    invoke-virtual {p1}, LX/0Kv;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/0OL;

    .line 41427
    iget v0, v0, LX/0OL;->responseCode:I

    const/16 v3, 0x19a

    if-ne v0, v3, :cond_1

    if-eqz v2, :cond_1

    .line 41428
    const-string v0, "DISMISS"

    .line 41429
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onPlayerError: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41430
    iget-object v1, p0, LX/0KX;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, p0, LX/0KX;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 41431
    invoke-static {v1, v0, p1, v2}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Ljava/lang/Throwable;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41432
    return-void

    .line 41433
    :cond_0
    const-string v0, "PLAYBACK_EXCEPTION"

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(ZI)V
    .locals 4

    .prologue
    .line 41434
    iget-object v0, p0, LX/0KX;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MSG_STATE_CHANGED to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and playWhenReady is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0KX;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1, v2}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41435
    const/4 v0, 0x5

    if-ne p2, v0, :cond_0

    .line 41436
    iget-object v0, p0, LX/0KX;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->f:LX/0Gh;

    invoke-virtual {v0}, LX/0Gh;->b()V

    .line 41437
    :cond_0
    iget-object v0, p0, LX/0KX;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0KX;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v1

    .line 41438
    if-nez v1, :cond_1

    .line 41439
    :goto_0
    return-void

    .line 41440
    :cond_1
    iget-object v0, p0, LX/0KX;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, p0, LX/0KX;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v2}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v0

    .line 41441
    if-nez v0, :cond_2

    .line 41442
    iget-object v0, p0, LX/0KX;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "Error: Cannot get exo internal player, but have a listener"

    iget-object v2, p0, LX/0KX;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1, v2}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0

    .line 41443
    :cond_2
    :try_start_0
    iget-object v2, p0, LX/0KX;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-interface {v0}, LX/0Kx;->i()I

    move-result v0

    invoke-virtual {v1, v2, p1, p2, v0}, LX/0Kb;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 41444
    :catch_0
    move-exception v0

    .line 41445
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error forward player state change to listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0KX;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1, v2}, Lcom/facebook/video/vps/VideoPlayerService;->b(Ljava/lang/Throwable;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0
.end method
