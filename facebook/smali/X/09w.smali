.class public LX/09w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Uh;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 23120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23121
    iput-object p1, p0, LX/09w;->a:Landroid/content/Context;

    .line 23122
    iput-object p2, p0, LX/09w;->b:LX/0Uh;

    .line 23123
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 4

    .prologue
    .line 23124
    iget-object v0, p0, LX/09w;->a:Landroid/content/Context;

    iget-object v1, p0, LX/09w;->b:LX/0Uh;

    const/16 v2, 0x38

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 23125
    invoke-static {v0}, LX/00n;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    .line 23126
    if-eqz v1, :cond_1

    .line 23127
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    move-result v2

    if-nez v2, :cond_0

    .line 23128
    const-string v2, "GcOptimizer"

    const-string v3, "Can\'t create file"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23129
    :cond_0
    :goto_0
    return-void

    .line 23130
    :catch_0
    move-exception v2

    .line 23131
    const-string v3, "GcOptimizer"

    const-string p0, "Can\'t create file"

    invoke-static {v3, p0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 23132
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    .line 23133
    const-string v2, "GcOptimizer"

    const-string v3, "Can\'t delete"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
