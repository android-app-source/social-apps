.class public final enum LX/04I;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/04I;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/04I;

.field public static final enum STORY_SET_ID:LX/04I;

.field public static final enum STORY_SET_VIDEO_POSITION:LX/04I;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12994
    new-instance v0, LX/04I;

    const-string v1, "STORY_SET_ID"

    const-string v2, "story_set_id"

    invoke-direct {v0, v1, v3, v2}, LX/04I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04I;->STORY_SET_ID:LX/04I;

    .line 12995
    new-instance v0, LX/04I;

    const-string v1, "STORY_SET_VIDEO_POSITION"

    const-string v2, "story_set_video_position"

    invoke-direct {v0, v1, v4, v2}, LX/04I;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04I;->STORY_SET_VIDEO_POSITION:LX/04I;

    .line 12996
    const/4 v0, 0x2

    new-array v0, v0, [LX/04I;

    sget-object v1, LX/04I;->STORY_SET_ID:LX/04I;

    aput-object v1, v0, v3

    sget-object v1, LX/04I;->STORY_SET_VIDEO_POSITION:LX/04I;

    aput-object v1, v0, v4

    sput-object v0, LX/04I;->$VALUES:[LX/04I;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12991
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12992
    iput-object p3, p0, LX/04I;->value:Ljava/lang/String;

    .line 12993
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/04I;
    .locals 1

    .prologue
    .line 12989
    const-class v0, LX/04I;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/04I;

    return-object v0
.end method

.method public static values()[LX/04I;
    .locals 1

    .prologue
    .line 12990
    sget-object v0, LX/04I;->$VALUES:[LX/04I;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/04I;

    return-object v0
.end method
