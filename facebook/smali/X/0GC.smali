.class public abstract LX/0GC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

.field public b:I


# direct methods
.method public constructor <init>(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)V
    .locals 2

    .prologue
    .line 34398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34399
    iput-object p1, p0, LX/0GC;->a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    .line 34400
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Creating prefetch "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0GC;->a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v1, v1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Origin "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0GC;->a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v1, v1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34401
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract b()Z
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 34402
    instance-of v0, p1, LX/0GC;

    if-eqz v0, :cond_0

    .line 34403
    iget v0, p0, LX/0GC;->b:I

    check-cast p1, LX/0GC;

    iget v1, p1, LX/0GC;->b:I

    sub-int/2addr v0, v1

    neg-int v0, v0

    .line 34404
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
