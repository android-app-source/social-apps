.class public final LX/04Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final file:Ljava/io/RandomAccessFile;

.field public final fileName:Ljava/io/File;

.field public final synthetic this$0:LX/01e;


# direct methods
.method public constructor <init>(LX/01e;Ljava/io/File;Ljava/io/RandomAccessFile;)V
    .locals 0

    .prologue
    .line 13903
    iput-object p1, p0, LX/04Z;->this$0:LX/01e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13904
    iput-object p2, p0, LX/04Z;->fileName:Ljava/io/File;

    .line 13905
    iput-object p3, p0, LX/04Z;->file:Ljava/io/RandomAccessFile;

    .line 13906
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 3

    .prologue
    .line 13907
    iget-object v1, p0, LX/04Z;->this$0:LX/01e;

    monitor-enter v1

    .line 13908
    :try_start_0
    iget-object v0, p0, LX/04Z;->this$0:LX/01e;

    iget-object v0, v0, LX/01e;->mHazardList:Ljava/util/HashSet;

    iget-object v2, p0, LX/04Z;->fileName:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 13909
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13910
    iget-object v0, p0, LX/04Z;->this$0:LX/01e;

    iget-object v1, p0, LX/04Z;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, v1}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    .line 13911
    return-void

    .line 13912
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
