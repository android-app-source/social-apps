.class public abstract LX/0Lb;
.super LX/0LP;
.source ""


# instance fields
.field public a:[B

.field public h:I

.field private volatile i:Z


# direct methods
.method public constructor <init>(LX/0G6;LX/0OA;IILX/0AR;I[B)V
    .locals 0

    .prologue
    .line 44014
    invoke-direct/range {p0 .. p6}, LX/0LP;-><init>(LX/0G6;LX/0OA;IILX/0AR;I)V

    .line 44015
    iput-object p7, p0, LX/0Lb;->a:[B

    .line 44016
    return-void
.end method


# virtual methods
.method public abstract a([BI)V
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 44017
    iget v0, p0, LX/0Lb;->h:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 44018
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Lb;->i:Z

    .line 44019
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 44020
    iget-boolean v0, p0, LX/0Lb;->i:Z

    return v0
.end method

.method public final h()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 44021
    :try_start_0
    iget-object v1, p0, LX/0LP;->g:LX/0G6;

    iget-object v2, p0, LX/0LP;->e:LX/0OA;

    invoke-interface {v1, v2}, LX/0G6;->a(LX/0OA;)J

    .line 44022
    const/4 v1, 0x0

    iput v1, p0, LX/0Lb;->h:I

    .line 44023
    :cond_0
    :goto_0
    if-eq v0, v4, :cond_2

    iget-boolean v0, p0, LX/0Lb;->i:Z

    if-nez v0, :cond_2

    .line 44024
    iget-object v0, p0, LX/0Lb;->a:[B

    if-nez v0, :cond_4

    .line 44025
    const/16 v0, 0x4000

    new-array v0, v0, [B

    iput-object v0, p0, LX/0Lb;->a:[B

    .line 44026
    :cond_1
    :goto_1
    iget-object v0, p0, LX/0LP;->g:LX/0G6;

    iget-object v1, p0, LX/0Lb;->a:[B

    iget v2, p0, LX/0Lb;->h:I

    const/16 v3, 0x4000

    invoke-interface {v0, v1, v2, v3}, LX/0G6;->a([BII)I

    move-result v0

    .line 44027
    if-eq v0, v4, :cond_0

    .line 44028
    iget v1, p0, LX/0Lb;->h:I

    add-int/2addr v1, v0

    iput v1, p0, LX/0Lb;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 44029
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v1}, LX/0G6;->a()V

    throw v0

    .line 44030
    :cond_2
    :try_start_1
    iget-boolean v0, p0, LX/0Lb;->i:Z

    if-nez v0, :cond_3

    .line 44031
    iget-object v0, p0, LX/0Lb;->a:[B

    iget v1, p0, LX/0Lb;->h:I

    invoke-virtual {p0, v0, v1}, LX/0Lb;->a([BI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 44032
    :cond_3
    iget-object v0, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v0}, LX/0G6;->a()V

    .line 44033
    return-void

    .line 44034
    :cond_4
    iget-object v0, p0, LX/0Lb;->a:[B

    array-length v0, v0

    iget v1, p0, LX/0Lb;->h:I

    add-int/lit16 v1, v1, 0x4000

    if-ge v0, v1, :cond_1

    .line 44035
    iget-object v0, p0, LX/0Lb;->a:[B

    iget-object v1, p0, LX/0Lb;->a:[B

    array-length v1, v1

    add-int/lit16 v1, v1, 0x4000

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, LX/0Lb;->a:[B

    goto :goto_1
.end method
