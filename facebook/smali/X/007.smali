.class public LX/007;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;

.field public static final h:Ljava/lang/String;

.field public static final i:Z

.field public static final j:Z

.field public static final k:Z

.field public static final l:Z

.field public static final m:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 849
    const/16 v0, 0x0

    sput-boolean v0, LX/007;->i:Z

    .line 850
    const/16 v0, 0x0

    sput-boolean v0, LX/007;->j:Z

    .line 851
    const/16 v0, 0x0

    sput-boolean v0, LX/007;->k:Z

    .line 852
    const/16 v0, 0x0

    sput-boolean v0, LX/007;->l:Z

    .line 853
    sget-object v0, LX/01Q;->e:Ljava/lang/String;

    sput-object v0, LX/007;->m:Ljava/lang/String;

    .line 854
    sget-object v0, LX/01Q;->l:Ljava/lang/String;

    sput-object v0, LX/007;->a:Ljava/lang/String;

    .line 855
    sget-object v0, LX/01Q;->m:Ljava/lang/String;

    sput-object v0, LX/007;->b:Ljava/lang/String;

    .line 856
    sget-object v0, LX/01Q;->n:Ljava/lang/String;

    sput-object v0, LX/007;->c:Ljava/lang/String;

    .line 857
    sget-object v0, LX/01Q;->o:Ljava/lang/String;

    sput-object v0, LX/007;->d:Ljava/lang/String;

    .line 858
    sget-boolean v0, LX/007;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "com.facebook.workchat"

    :goto_0
    sput-object v0, LX/007;->e:Ljava/lang/String;

    .line 859
    sget-boolean v0, LX/007;->i:Z

    if-eqz v0, :cond_1

    const-string v0, "com.facebook.workdev"

    :goto_1
    sput-object v0, LX/007;->f:Ljava/lang/String;

    .line 860
    sget-boolean v0, LX/007;->i:Z

    if-eqz v0, :cond_2

    const-string v0, "com.facebook.wakizashi"

    :goto_2
    sput-object v0, LX/007;->g:Ljava/lang/String;

    .line 861
    sget-object v0, LX/01Q;->p:Ljava/lang/String;

    sput-object v0, LX/007;->h:Ljava/lang/String;

    return-void

    .line 862
    :cond_0
    const-string v0, "com.facebook.orca"

    goto :goto_0

    .line 863
    :cond_1
    const-string v0, "com.facebook.work"

    goto :goto_1

    .line 864
    :cond_2
    const-string v0, "com.facebook.katana"

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final f()Z
    .locals 1

    .prologue
    .line 866
    sget-boolean v0, LX/007;->l:Z

    return v0
.end method

.method public static final g()Z
    .locals 1

    .prologue
    .line 867
    sget-boolean v0, LX/007;->i:Z

    return v0
.end method

.method public static final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 868
    sget-object v0, LX/007;->g:Ljava/lang/String;

    return-object v0
.end method

.method public static final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 869
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 870
    if-eqz v0, :cond_0

    .line 871
    sget-object v0, LX/007;->f:Ljava/lang/String;

    .line 872
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/007;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public static final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 873
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 874
    if-eqz v0, :cond_0

    .line 875
    const-string v0, "wakizashi"

    .line 876
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "katana"

    goto :goto_0
.end method

.method public static final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 877
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 878
    if-eqz v0, :cond_0

    const-string v0, "https://m.facebook.com/mobile_builds"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "https://market.android.com/details?id=com.facebook.katana"

    goto :goto_0
.end method
