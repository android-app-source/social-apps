.class public final LX/02Z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final assetName:Ljava/lang/String;

.field public final canaryClass:Ljava/lang/String;

.field public final hash:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 8760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8761
    iput-object p1, p0, LX/02Z;->assetName:Ljava/lang/String;

    .line 8762
    iput-object p2, p0, LX/02Z;->hash:Ljava/lang/String;

    .line 8763
    iput-object p3, p0, LX/02Z;->canaryClass:Ljava/lang/String;

    .line 8764
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 8765
    const-string v0, "<Dex assetName:[%s]>"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/02Z;->assetName:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
