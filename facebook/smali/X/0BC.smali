.class public final enum LX/0BC;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/06x;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0BC;",
        ">;",
        "LX/06x;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0BC;

.field public static final enum PublishAcknowledgementMs:LX/0BC;

.field public static final enum StackReceivingLatencyMs:LX/0BC;

.field public static final enum StackSendingLatencyMs:LX/0BC;


# instance fields
.field private final mJsonKey:Ljava/lang/String;

.field private final mType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 26227
    new-instance v0, LX/0BC;

    const-string v1, "PublishAcknowledgementMs"

    const-string v2, "pub"

    const-class v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v4, v2, v3}, LX/0BC;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BC;->PublishAcknowledgementMs:LX/0BC;

    .line 26228
    new-instance v0, LX/0BC;

    const-string v1, "StackSendingLatencyMs"

    const-string v2, "s"

    const-class v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v5, v2, v3}, LX/0BC;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BC;->StackSendingLatencyMs:LX/0BC;

    .line 26229
    new-instance v0, LX/0BC;

    const-string v1, "StackReceivingLatencyMs"

    const-string v2, "r"

    const-class v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v6, v2, v3}, LX/0BC;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BC;->StackReceivingLatencyMs:LX/0BC;

    .line 26230
    const/4 v0, 0x3

    new-array v0, v0, [LX/0BC;

    sget-object v1, LX/0BC;->PublishAcknowledgementMs:LX/0BC;

    aput-object v1, v0, v4

    sget-object v1, LX/0BC;->StackSendingLatencyMs:LX/0BC;

    aput-object v1, v0, v5

    sget-object v1, LX/0BC;->StackReceivingLatencyMs:LX/0BC;

    aput-object v1, v0, v6

    sput-object v0, LX/0BC;->$VALUES:[LX/0BC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 26223
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26224
    iput-object p3, p0, LX/0BC;->mJsonKey:Ljava/lang/String;

    .line 26225
    iput-object p4, p0, LX/0BC;->mType:Ljava/lang/Class;

    .line 26226
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0BC;
    .locals 1

    .prologue
    .line 26222
    const-class v0, LX/0BC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0BC;

    return-object v0
.end method

.method public static values()[LX/0BC;
    .locals 1

    .prologue
    .line 26221
    sget-object v0, LX/0BC;->$VALUES:[LX/0BC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0BC;

    return-object v0
.end method


# virtual methods
.method public final getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26220
    iget-object v0, p0, LX/0BC;->mJsonKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getValueType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 26219
    iget-object v0, p0, LX/0BC;->mType:Ljava/lang/Class;

    return-object v0
.end method
