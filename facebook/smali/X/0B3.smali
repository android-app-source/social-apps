.class public LX/0B3;
.super Ljava/util/concurrent/FutureTask;
.source ""

# interfaces
.implements LX/05u;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/FutureTask",
        "<TV;>;",
        "LX/05u",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0B4;


# direct methods
.method private constructor <init>(Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 26101
    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 26102
    new-instance v0, LX/0B4;

    invoke-direct {v0}, LX/0B4;-><init>()V

    iput-object v0, p0, LX/0B3;->a:LX/0B4;

    .line 26103
    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 26098
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 26099
    new-instance v0, LX/0B4;

    invoke-direct {v0}, LX/0B4;-><init>()V

    iput-object v0, p0, LX/0B3;->a:LX/0B4;

    .line 26100
    return-void
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0B3;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TV;)",
            "LX/0B3",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 26092
    new-instance v0, LX/0B3;

    invoke-direct {v0, p0, p1}, LX/0B3;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)LX/0B3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)",
            "LX/0B3",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 26097
    new-instance v0, LX/0B3;

    invoke-direct {v0, p0}, LX/0B3;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 26095
    iget-object v0, p0, LX/0B3;->a:LX/0B4;

    invoke-virtual {v0, p1, p2}, LX/0B4;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 26096
    return-void
.end method

.method public final done()V
    .locals 1

    .prologue
    .line 26093
    iget-object v0, p0, LX/0B3;->a:LX/0B4;

    invoke-virtual {v0}, LX/0B4;->a()V

    .line 26094
    return-void
.end method
