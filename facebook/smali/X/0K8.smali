.class public final LX/0K8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0K7;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0K7;",
        "Ljava/util/Comparator",
        "<",
        "LX/0OT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0K9;

.field private final b:J

.field private final c:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "LX/0OT;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0OQ;

.field private e:J


# direct methods
.method public constructor <init>(LX/0K9;JLX/0OQ;)V
    .locals 2

    .prologue
    .line 40158
    iput-object p1, p0, LX/0K8;->a:LX/0K9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40159
    iput-wide p2, p0, LX/0K8;->b:J

    .line 40160
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0, p0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, LX/0K8;->c:Ljava/util/TreeSet;

    .line 40161
    iput-object p4, p0, LX/0K8;->d:LX/0OQ;

    .line 40162
    return-void
.end method

.method private b(LX/0OQ;J)V
    .locals 6

    .prologue
    .line 40205
    :goto_0
    iget-wide v0, p0, LX/0K8;->e:J

    add-long/2addr v0, p2

    iget-wide v2, p0, LX/0K8;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 40206
    iget-object v0, p0, LX/0K8;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0OT;

    .line 40207
    iget-wide v2, v0, LX/0OT;->b:J

    const-wide/32 v4, 0x20000

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 40208
    invoke-direct {p0, v0}, LX/0K8;->b(LX/0OT;)V

    .line 40209
    :cond_0
    invoke-interface {p1, v0}, LX/0OQ;->b(LX/0OT;)V

    goto :goto_0

    .line 40210
    :cond_1
    return-void
.end method

.method private b(LX/0OT;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 40180
    new-instance v8, LX/0OS;

    iget-object v0, p0, LX/0K8;->d:LX/0OQ;

    const-wide v2, 0x7fffffffffffffffL

    invoke-direct {v8, v0, v2, v3}, LX/0OS;-><init>(LX/0OQ;J)V

    .line 40181
    new-instance v0, LX/0OA;

    iget-wide v2, p1, LX/0OT;->b:J

    const-wide/32 v4, 0x20000

    iget-wide v10, p1, LX/0OT;->b:J

    sub-long/2addr v4, v10

    iget-wide v10, p1, LX/0OT;->c:J

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    iget-object v6, p1, LX/0OT;->a:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, LX/0OA;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 40182
    :try_start_0
    iget-object v2, p0, LX/0K8;->d:LX/0OQ;

    iget-object v3, p1, LX/0OT;->a:Ljava/lang/String;

    iget-wide v4, p1, LX/0OT;->b:J

    invoke-interface {v2, v3, v4, v5}, LX/0OQ;->a(Ljava/lang/String;J)LX/0OT;

    move-result-object v1

    .line 40183
    if-eqz v1, :cond_0

    iget-boolean v2, v1, LX/0OT;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    .line 40184
    :cond_0
    if-eqz v1, :cond_1

    iget-boolean v0, v1, LX/0OT;->d:Z

    if-nez v0, :cond_1

    .line 40185
    iget-object v0, p0, LX/0K8;->d:LX/0OQ;

    invoke-interface {v0, v1}, LX/0OQ;->a(LX/0OT;)V

    .line 40186
    :cond_1
    :goto_0
    return-void

    .line 40187
    :cond_2
    :try_start_1
    invoke-virtual {v8, v0}, LX/0OS;->a(LX/0OA;)LX/0O8;

    .line 40188
    new-instance v3, Ljava/io/FileInputStream;

    iget-object v2, p1, LX/0OT;->e:Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 40189
    const/high16 v2, 0x20000

    new-array v4, v2, [B

    move v2, v7

    .line 40190
    :goto_1
    int-to-long v6, v2

    iget-wide v10, v0, LX/0OA;->e:J

    cmp-long v5, v6, v10

    if-gez v5, :cond_3

    .line 40191
    const/4 v5, 0x0

    iget-wide v6, v0, LX/0OA;->e:J

    int-to-long v10, v2

    sub-long/2addr v6, v10

    long-to-int v6, v6

    invoke-virtual {v3, v4, v5, v6}, Ljava/io/FileInputStream;->read([BII)I

    move-result v5

    .line 40192
    invoke-virtual {v8, v4, v2, v5}, LX/0OS;->a([BII)V

    .line 40193
    add-int/2addr v2, v5

    .line 40194
    goto :goto_1

    .line 40195
    :cond_3
    invoke-virtual {v8}, LX/0OS;->a()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 40196
    if-eqz v1, :cond_1

    iget-boolean v0, v1, LX/0OT;->d:Z

    if-nez v0, :cond_1

    .line 40197
    iget-object v0, p0, LX/0K8;->d:LX/0OQ;

    invoke-interface {v0, v1}, LX/0OQ;->a(LX/0OT;)V

    goto :goto_0

    .line 40198
    :catch_0
    move-exception v0

    .line 40199
    :try_start_2
    sget-object v2, LX/0K9;->a:Ljava/lang/String;

    const-string v3, "Failed to copy prefetch data from play cache %s, %d, %d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p1, LX/0OT;->e:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-wide v6, p1, LX/0OT;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-wide v6, p1, LX/0OT;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 40200
    if-eqz v1, :cond_1

    iget-boolean v0, v1, LX/0OT;->d:Z

    if-nez v0, :cond_1

    .line 40201
    iget-object v0, p0, LX/0K8;->d:LX/0OQ;

    invoke-interface {v0, v1}, LX/0OQ;->a(LX/0OT;)V

    goto :goto_0

    .line 40202
    :catchall_0
    move-exception v0

    .line 40203
    if-eqz v1, :cond_4

    iget-boolean v2, v1, LX/0OT;->d:Z

    if-nez v2, :cond_4

    .line 40204
    iget-object v2, p0, LX/0K8;->d:LX/0OQ;

    invoke-interface {v2, v1}, LX/0OQ;->a(LX/0OT;)V

    :cond_4
    throw v0
.end method


# virtual methods
.method public final a(LX/0OQ;J)V
    .locals 0

    .prologue
    .line 40178
    invoke-direct {p0, p1, p2, p3}, LX/0K8;->b(LX/0OQ;J)V

    .line 40179
    return-void
.end method

.method public final a(LX/0OQ;LX/0OT;)V
    .locals 4

    .prologue
    .line 40174
    iget-object v0, p0, LX/0K8;->c:Ljava/util/TreeSet;

    invoke-virtual {v0, p2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 40175
    iget-wide v0, p0, LX/0K8;->e:J

    iget-wide v2, p2, LX/0OT;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0K8;->e:J

    .line 40176
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/0K8;->b(LX/0OQ;J)V

    .line 40177
    return-void
.end method

.method public final a(LX/0OQ;LX/0OT;LX/0OT;)V
    .locals 0

    .prologue
    .line 40171
    invoke-virtual {p0, p2}, LX/0K8;->a(LX/0OT;)V

    .line 40172
    invoke-virtual {p0, p1, p3}, LX/0K8;->a(LX/0OQ;LX/0OT;)V

    .line 40173
    return-void
.end method

.method public final a(LX/0OT;)V
    .locals 4

    .prologue
    .line 40168
    iget-object v0, p0, LX/0K8;->c:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 40169
    iget-wide v0, p0, LX/0K8;->e:J

    iget-wide v2, p1, LX/0OT;->c:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/0K8;->e:J

    .line 40170
    return-void
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 40163
    check-cast p1, LX/0OT;

    check-cast p2, LX/0OT;

    .line 40164
    iget-wide v0, p1, LX/0OT;->f:J

    iget-wide v2, p2, LX/0OT;->f:J

    sub-long/2addr v0, v2

    .line 40165
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 40166
    invoke-virtual {p1, p2}, LX/0OT;->a(LX/0OT;)I

    move-result v0

    .line 40167
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p1, LX/0OT;->f:J

    iget-wide v2, p2, LX/0OT;->f:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
