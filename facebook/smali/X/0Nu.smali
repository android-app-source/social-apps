.class public final LX/0Nu;
.super LX/0LQ;
.source ""


# instance fields
.field public final a:I

.field public final k:LX/0Nj;

.field private final l:Z

.field private m:I

.field private volatile n:Z


# direct methods
.method public constructor <init>(LX/0G6;LX/0OA;ILX/0AR;JJIILX/0Nj;[B[B)V
    .locals 13

    .prologue
    .line 52240
    move-object/from16 v0, p12

    move-object/from16 v1, p13

    invoke-static {p1, v0, v1}, LX/0Nu;->a(LX/0G6;[B[B)LX/0G6;

    move-result-object v4

    move-object v3, p0

    move-object v5, p2

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move/from16 v12, p9

    invoke-direct/range {v3 .. v12}, LX/0LQ;-><init>(LX/0G6;LX/0OA;ILX/0AR;JJI)V

    .line 52241
    move/from16 v0, p10

    iput v0, p0, LX/0Nu;->a:I

    .line 52242
    move-object/from16 v0, p11

    iput-object v0, p0, LX/0Nu;->k:LX/0Nj;

    .line 52243
    iget-object v2, p0, LX/0LP;->g:LX/0G6;

    instance-of v2, v2, LX/0Na;

    iput-boolean v2, p0, LX/0Nu;->l:Z

    .line 52244
    return-void
.end method

.method private static a(LX/0G6;[B[B)LX/0G6;
    .locals 1

    .prologue
    .line 52213
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 52214
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    new-instance v0, LX/0Na;

    invoke-direct {v0, p0, p1, p2}, LX/0Na;-><init>(LX/0G6;[B[B)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final e()J
    .locals 2

    .prologue
    .line 52239
    iget v0, p0, LX/0Nu;->m:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 52237
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Nu;->n:Z

    .line 52238
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 52236
    iget-boolean v0, p0, LX/0Nu;->n:Z

    return v0
.end method

.method public final h()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 52215
    iget-boolean v0, p0, LX/0Nu;->l:Z

    if-eqz v0, :cond_2

    .line 52216
    iget-object v1, p0, LX/0LP;->e:LX/0OA;

    .line 52217
    iget v0, p0, LX/0Nu;->m:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v7, v0

    move-object v4, v1

    .line 52218
    :goto_1
    :try_start_0
    new-instance v0, LX/0MB;

    iget-object v1, p0, LX/0LP;->g:LX/0G6;

    iget-wide v2, v4, LX/0OA;->c:J

    iget-object v5, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v5, v4}, LX/0G6;->a(LX/0OA;)J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, LX/0MB;-><init>(LX/0G6;JJ)V

    .line 52219
    if-eqz v7, :cond_0

    .line 52220
    iget v1, p0, LX/0Nu;->m:I

    invoke-interface {v0, v1}, LX/0MA;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 52221
    :cond_0
    :goto_2
    if-nez v6, :cond_3

    :try_start_1
    iget-boolean v1, p0, LX/0Nu;->n:Z

    if-nez v1, :cond_3

    .line 52222
    iget-object v1, p0, LX/0Nu;->k:LX/0Nj;

    const/4 v2, 0x1

    .line 52223
    iget-object v3, v1, LX/0Nj;->d:LX/0ME;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, LX/0ME;->a(LX/0MA;LX/0MM;)I

    move-result v3

    .line 52224
    if-eq v3, v2, :cond_4

    :goto_3
    invoke-static {v2}, LX/0Av;->b(Z)V

    .line 52225
    move v6, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52226
    goto :goto_2

    :cond_1
    move v0, v6

    .line 52227
    goto :goto_0

    .line 52228
    :cond_2
    iget-object v0, p0, LX/0LP;->e:LX/0OA;

    iget v1, p0, LX/0Nu;->m:I

    invoke-static {v0, v1}, LX/08x;->a(LX/0OA;I)LX/0OA;

    move-result-object v0

    move v7, v6

    move-object v4, v0

    .line 52229
    goto :goto_1

    .line 52230
    :cond_3
    :try_start_2
    invoke-interface {v0}, LX/0MA;->c()J

    move-result-wide v0

    iget-object v2, p0, LX/0LP;->e:LX/0OA;

    iget-wide v2, v2, LX/0OA;->c:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, LX/0Nu;->m:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 52231
    iget-object v0, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v0}, LX/0G6;->a()V

    .line 52232
    return-void

    .line 52233
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-interface {v0}, LX/0MA;->c()J

    move-result-wide v2

    iget-object v0, p0, LX/0LP;->e:LX/0OA;

    iget-wide v4, v0, LX/0OA;->c:J

    sub-long/2addr v2, v4

    long-to-int v0, v2

    iput v0, p0, LX/0Nu;->m:I

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 52234
    :catchall_1
    move-exception v0

    iget-object v1, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v1}, LX/0G6;->a()V

    throw v0

    .line 52235
    :cond_4
    const/4 v2, 0x0

    goto :goto_3
.end method
