.class public LX/0OD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ge;


# static fields
.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<[B>;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Z

.field private final e:I

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:LX/0OH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0OH",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/04n;

.field private k:LX/0OA;

.field private l:Ljava/net/HttpURLConnection;

.field private m:Ljava/io/InputStream;

.field private n:Z

.field private o:J

.field public p:J

.field private q:J

.field public r:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52632
    const-string v0, "^bytes (\\d+)-(\\d+)/(\\d+)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0OD;->b:Ljava/util/regex/Pattern;

    .line 52633
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, LX/0OD;->c:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0OH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0OH",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52811
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/0OD;-><init>(Ljava/lang/String;LX/0OH;LX/04n;)V

    .line 52812
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0OH;LX/04n;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0OH",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/04n;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v4, 0x1f40

    .line 52809
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/0OD;-><init>(Ljava/lang/String;LX/0OH;LX/04n;II)V

    .line 52810
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0OH;LX/04n;II)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0OH",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/04n;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 52807
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/0OD;-><init>(Ljava/lang/String;LX/0OH;LX/04n;IIZ)V

    .line 52808
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0OH;LX/04n;IIZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0OH",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/04n;",
            "IIZ)V"
        }
    .end annotation

    .prologue
    .line 52798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52799
    invoke-static {p1}, LX/0Av;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0OD;->g:Ljava/lang/String;

    .line 52800
    iput-object p2, p0, LX/0OD;->h:LX/0OH;

    .line 52801
    iput-object p3, p0, LX/0OD;->j:LX/04n;

    .line 52802
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0OD;->i:Ljava/util/HashMap;

    .line 52803
    iput p4, p0, LX/0OD;->e:I

    .line 52804
    iput p5, p0, LX/0OD;->f:I

    .line 52805
    iput-boolean p6, p0, LX/0OD;->d:Z

    .line 52806
    return-void
.end method

.method private static a(Ljava/net/HttpURLConnection;)J
    .locals 9

    .prologue
    .line 52782
    const-wide/16 v0, -0x1

    .line 52783
    const-string v2, "Content-Length"

    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 52784
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 52785
    :try_start_0
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 52786
    :cond_0
    :goto_0
    const-string v2, "Content-Range"

    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 52787
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 52788
    sget-object v2, LX/0OD;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 52789
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 52790
    const/4 v3, 0x2

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v2

    sub-long v2, v6, v2

    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    .line 52791
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-gez v6, :cond_2

    move-wide v0, v2

    .line 52792
    :cond_1
    :goto_1
    return-wide v0

    .line 52793
    :catch_0
    const-string v2, "DefaultHttpDataSource"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Unexpected Content-Length ["

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 52794
    :cond_2
    cmp-long v6, v0, v2

    if-eqz v6, :cond_1

    .line 52795
    :try_start_2
    const-string v6, "DefaultHttpDataSource"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Inconsistent headers ["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "] ["

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "]"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 52796
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-wide v0

    goto :goto_1

    .line 52797
    :catch_1
    const-string v2, "DefaultHttpDataSource"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected Content-Range ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static a(LX/0OD;Ljava/net/URL;[BJJZZ)Ljava/net/HttpURLConnection;
    .locals 11

    .prologue
    .line 52753
    invoke-virtual {p1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52754
    sget-object v2, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-virtual {p1, v2}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    move-object v4, v2

    .line 52755
    :goto_0
    iget v2, p0, LX/0OD;->e:I

    invoke-virtual {v4, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 52756
    iget v2, p0, LX/0OD;->f:I

    invoke-virtual {v4, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 52757
    iget-object v5, p0, LX/0OD;->i:Ljava/util/HashMap;

    monitor-enter v5

    .line 52758
    :try_start_0
    iget-object v2, p0, LX/0OD;->i:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 52759
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v3, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 52760
    :catchall_0
    move-exception v2

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 52761
    :cond_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    move-object v4, v2

    goto :goto_0

    .line 52762
    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52763
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-nez v2, :cond_2

    const-wide/16 v2, -0x1

    cmp-long v2, p5, v2

    if-eqz v2, :cond_4

    .line 52764
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bytes="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 52765
    const-wide/16 v6, -0x1

    cmp-long v3, p5, v6

    if-eqz v3, :cond_3

    .line 52766
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-long v6, p3, p5

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 52767
    :cond_3
    const-string v3, "Range"

    invoke-virtual {v4, v3, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 52768
    :cond_4
    const-string v2, "User-Agent"

    iget-object v3, p0, LX/0OD;->g:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 52769
    if-nez p7, :cond_5

    .line 52770
    const-string v2, "Accept-Encoding"

    const-string v3, "identity"

    invoke-virtual {v4, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 52771
    :cond_5
    move/from16 v0, p8

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 52772
    if-eqz p2, :cond_6

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v4, v2}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 52773
    if-eqz p2, :cond_7

    .line 52774
    array-length v2, p2

    invoke-virtual {v4, v2}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 52775
    const v2, 0x47ecb806    # 121200.05f

    invoke-static {v4, v2}, LX/04e;->a(Ljava/net/URLConnection;I)V

    .line 52776
    const v2, -0x48964df3

    invoke-static {v4, v2}, LX/04e;->c(Ljava/net/URLConnection;I)Ljava/io/OutputStream;

    move-result-object v2

    .line 52777
    invoke-virtual {v2, p2}, Ljava/io/OutputStream;->write([B)V

    .line 52778
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 52779
    :goto_3
    return-object v4

    .line 52780
    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    .line 52781
    :cond_7
    const v2, 0x610d96e9

    invoke-static {v4, v2}, LX/04e;->a(Ljava/net/URLConnection;I)V

    goto :goto_3
.end method

.method private static a(Ljava/net/URL;Ljava/lang/String;)Ljava/net/URL;
    .locals 4

    .prologue
    .line 52746
    if-nez p1, :cond_0

    .line 52747
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Null location redirect"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52748
    :cond_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0, p1}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    .line 52749
    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    .line 52750
    const-string v2, "https"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "http"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 52751
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported protocol redirect: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52752
    :cond_1
    return-object v0
.end method

.method private b([BII)I
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v0, -0x1

    .line 52813
    iget-wide v2, p0, LX/0OD;->p:J

    cmp-long v1, v2, v8

    if-nez v1, :cond_1

    .line 52814
    :goto_0
    if-nez p3, :cond_2

    .line 52815
    :cond_0
    :goto_1
    return v0

    .line 52816
    :cond_1
    int-to-long v2, p3

    iget-wide v4, p0, LX/0OD;->p:J

    iget-wide v6, p0, LX/0OD;->r:J

    sub-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int p3, v2

    goto :goto_0

    .line 52817
    :cond_2
    iget-object v1, p0, LX/0OD;->m:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 52818
    if-ne v1, v0, :cond_3

    .line 52819
    iget-wide v2, p0, LX/0OD;->p:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_0

    iget-wide v2, p0, LX/0OD;->p:J

    iget-wide v4, p0, LX/0OD;->r:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 52820
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 52821
    :cond_3
    iget-wide v2, p0, LX/0OD;->r:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0OD;->r:J

    .line 52822
    iget-object v0, p0, LX/0OD;->j:LX/04n;

    if-eqz v0, :cond_4

    .line 52823
    iget-object v0, p0, LX/0OD;->j:LX/04n;

    invoke-interface {v0, v1}, LX/04n;->a(I)V

    :cond_4
    move v0, v1

    .line 52824
    goto :goto_1
.end method

.method private b(LX/0OA;)Ljava/net/HttpURLConnection;
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 52726
    new-instance v2, Ljava/net/URL;

    iget-object v1, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 52727
    iget-object v3, p1, LX/0OA;->b:[B

    .line 52728
    iget-wide v4, p1, LX/0OA;->d:J

    .line 52729
    iget-wide v6, p1, LX/0OA;->e:J

    .line 52730
    iget v1, p1, LX/0OA;->g:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    move v8, v9

    .line 52731
    :goto_0
    iget-boolean v1, p0, LX/0OD;->d:Z

    if-nez v1, :cond_1

    move-object v1, p0

    .line 52732
    invoke-static/range {v1 .. v9}, LX/0OD;->a(LX/0OD;Ljava/net/URL;[BJJZZ)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 52733
    :goto_1
    return-object v0

    :cond_0
    move v8, v0

    .line 52734
    goto :goto_0

    :cond_1
    move v1, v0

    .line 52735
    :goto_2
    add-int/lit8 v10, v1, 0x1

    const/16 v9, 0x14

    if-gt v1, v9, :cond_4

    move-object v1, p0

    move v9, v0

    .line 52736
    invoke-static/range {v1 .. v9}, LX/0OD;->a(LX/0OD;Ljava/net/URL;[BJJZZ)Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 52737
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v9

    .line 52738
    const/16 v11, 0x12c

    if-eq v9, v11, :cond_2

    const/16 v11, 0x12d

    if-eq v9, v11, :cond_2

    const/16 v11, 0x12e

    if-eq v9, v11, :cond_2

    const/16 v11, 0x12f

    if-eq v9, v11, :cond_2

    if-nez v3, :cond_3

    const/16 v3, 0x133

    if-eq v9, v3, :cond_2

    const/16 v3, 0x134

    if-ne v9, v3, :cond_3

    .line 52739
    :cond_2
    const/4 v3, 0x0

    .line 52740
    const-string v9, "Location"

    invoke-virtual {v1, v9}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 52741
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 52742
    invoke-static {v2, v9}, LX/0OD;->a(Ljava/net/URL;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v2

    move v1, v10

    .line 52743
    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 52744
    goto :goto_1

    .line 52745
    :cond_4
    new-instance v0, Ljava/net/NoRouteToHostException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Too many redirects: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/NoRouteToHostException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private e()V
    .locals 6

    .prologue
    .line 52710
    iget-wide v0, p0, LX/0OD;->q:J

    iget-wide v2, p0, LX/0OD;->o:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 52711
    :goto_0
    return-void

    .line 52712
    :cond_0
    sget-object v0, LX/0OD;->c:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 52713
    if-nez v0, :cond_1

    .line 52714
    const/16 v0, 0x1000

    new-array v0, v0, [B

    .line 52715
    :cond_1
    :goto_1
    iget-wide v2, p0, LX/0OD;->q:J

    iget-wide v4, p0, LX/0OD;->o:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 52716
    iget-wide v2, p0, LX/0OD;->o:J

    iget-wide v4, p0, LX/0OD;->q:J

    sub-long/2addr v2, v4

    array-length v1, v0

    int-to-long v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    .line 52717
    iget-object v2, p0, LX/0OD;->m:Ljava/io/InputStream;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 52718
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 52719
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 52720
    :cond_2
    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    .line 52721
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 52722
    :cond_3
    iget-wide v2, p0, LX/0OD;->q:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0OD;->q:J

    .line 52723
    iget-object v2, p0, LX/0OD;->j:LX/04n;

    if-eqz v2, :cond_1

    .line 52724
    iget-object v2, p0, LX/0OD;->j:LX/04n;

    invoke-interface {v2, v1}, LX/04n;->a(I)V

    goto :goto_1

    .line 52725
    :cond_4
    sget-object v1, LX/0OD;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 52704
    iget-object v0, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 52705
    :try_start_0
    iget-object v0, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 52706
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    .line 52707
    :cond_0
    return-void

    .line 52708
    :catch_0
    move-exception v0

    .line 52709
    const-string v1, "DefaultHttpDataSource"

    const-string v2, "Unexpected error while disconnecting"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final a([BII)I
    .locals 3

    .prologue
    .line 52700
    :try_start_0
    invoke-direct {p0}, LX/0OD;->e()V

    .line 52701
    invoke-direct {p0, p1, p2, p3}, LX/0OD;->b([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 52702
    :catch_0
    move-exception v0

    .line 52703
    new-instance v1, LX/0OJ;

    iget-object v2, p0, LX/0OD;->k:LX/0OA;

    invoke-direct {v1, v0, v2}, LX/0OJ;-><init>(Ljava/io/IOException;LX/0OA;)V

    throw v1
.end method

.method public final a(LX/0OA;)J
    .locals 8

    .prologue
    const/16 v7, 0xc8

    const-wide/16 v2, -0x1

    const-wide/16 v0, 0x0

    .line 52668
    iput-object p1, p0, LX/0OD;->k:LX/0OA;

    .line 52669
    iput-wide v0, p0, LX/0OD;->r:J

    .line 52670
    iput-wide v0, p0, LX/0OD;->q:J

    .line 52671
    :try_start_0
    invoke-direct {p0, p1}, LX/0OD;->b(LX/0OA;)Ljava/net/HttpURLConnection;

    move-result-object v4

    iput-object v4, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52672
    :try_start_1
    iget-object v4, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v4

    .line 52673
    if-lt v4, v7, :cond_0

    const/16 v5, 0x12b

    if-le v4, v5, :cond_1

    .line 52674
    :cond_0
    iget-object v0, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    .line 52675
    invoke-direct {p0}, LX/0OD;->f()V

    .line 52676
    new-instance v1, LX/0OL;

    invoke-direct {v1, v4, v0, p1}, LX/0OL;-><init>(ILjava/util/Map;LX/0OA;)V

    throw v1

    .line 52677
    :catch_0
    move-exception v0

    .line 52678
    new-instance v1, LX/0OJ;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to connect to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0, p1}, LX/0OJ;-><init>(Ljava/lang/String;Ljava/io/IOException;LX/0OA;)V

    throw v1

    .line 52679
    :catch_1
    move-exception v0

    .line 52680
    invoke-direct {p0}, LX/0OD;->f()V

    .line 52681
    new-instance v1, LX/0OJ;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to connect to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0, p1}, LX/0OJ;-><init>(Ljava/lang/String;Ljava/io/IOException;LX/0OA;)V

    throw v1

    .line 52682
    :cond_1
    iget-object v5, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v5

    .line 52683
    iget-object v6, p0, LX/0OD;->h:LX/0OH;

    if-eqz v6, :cond_2

    iget-object v6, p0, LX/0OD;->h:LX/0OH;

    invoke-interface {v6, v5}, LX/0OH;->a(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 52684
    invoke-direct {p0}, LX/0OD;->f()V

    .line 52685
    new-instance v0, LX/0OK;

    invoke-direct {v0, v5, p1}, LX/0OK;-><init>(Ljava/lang/String;LX/0OA;)V

    throw v0

    .line 52686
    :cond_2
    if-ne v4, v7, :cond_3

    iget-wide v4, p1, LX/0OA;->d:J

    cmp-long v4, v4, v0

    if-eqz v4, :cond_3

    iget-wide v0, p1, LX/0OA;->d:J

    :cond_3
    iput-wide v0, p0, LX/0OD;->o:J

    .line 52687
    iget v0, p1, LX/0OA;->g:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_7

    .line 52688
    iget-object v0, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    invoke-static {v0}, LX/0OD;->a(Ljava/net/HttpURLConnection;)J

    move-result-wide v0

    .line 52689
    iget-wide v4, p1, LX/0OA;->e:J

    cmp-long v4, v4, v2

    if-eqz v4, :cond_5

    iget-wide v0, p1, LX/0OA;->e:J

    :goto_0
    iput-wide v0, p0, LX/0OD;->p:J

    .line 52690
    :goto_1
    :try_start_2
    iget-object v0, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    const v1, 0x5fcad645

    invoke-static {v0, v1}, LX/04e;->b(Ljava/net/URLConnection;I)Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, LX/0OD;->m:Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 52691
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0OD;->n:Z

    .line 52692
    iget-object v0, p0, LX/0OD;->j:LX/04n;

    if-eqz v0, :cond_4

    .line 52693
    iget-object v0, p0, LX/0OD;->j:LX/04n;

    invoke-interface {v0}, LX/04n;->b()V

    .line 52694
    :cond_4
    iget-wide v0, p0, LX/0OD;->p:J

    return-wide v0

    .line 52695
    :cond_5
    cmp-long v4, v0, v2

    if-eqz v4, :cond_6

    iget-wide v2, p0, LX/0OD;->o:J

    sub-long/2addr v0, v2

    goto :goto_0

    :cond_6
    move-wide v0, v2

    goto :goto_0

    .line 52696
    :cond_7
    iget-wide v0, p1, LX/0OA;->e:J

    iput-wide v0, p0, LX/0OD;->p:J

    goto :goto_1

    .line 52697
    :catch_2
    move-exception v0

    .line 52698
    invoke-direct {p0}, LX/0OD;->f()V

    .line 52699
    new-instance v1, LX/0OJ;

    invoke-direct {v1, v0, p1}, LX/0OJ;-><init>(Ljava/io/IOException;LX/0OA;)V

    throw v1
.end method

.method public final a()V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 52636
    :try_start_0
    iget-object v0, p0, LX/0OD;->m:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 52637
    iget-object v0, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    .line 52638
    iget-wide v6, p0, LX/0OD;->p:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_4

    iget-wide v6, p0, LX/0OD;->p:J

    :goto_0
    move-wide v2, v6

    .line 52639
    sget v6, LX/08x;->a:I

    const/16 v7, 0x13

    if-eq v6, v7, :cond_5

    sget v6, LX/08x;->a:I

    const/16 v7, 0x14

    if-eq v6, v7, :cond_5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52640
    :cond_0
    :goto_1
    :try_start_1
    iget-object v0, p0, LX/0OD;->m:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52641
    :cond_1
    iput-object v5, p0, LX/0OD;->m:Ljava/io/InputStream;

    .line 52642
    invoke-direct {p0}, LX/0OD;->f()V

    .line 52643
    iget-boolean v0, p0, LX/0OD;->n:Z

    if-eqz v0, :cond_2

    .line 52644
    iput-boolean v4, p0, LX/0OD;->n:Z

    .line 52645
    iget-object v0, p0, LX/0OD;->j:LX/04n;

    if-eqz v0, :cond_2

    .line 52646
    iget-object v0, p0, LX/0OD;->j:LX/04n;

    invoke-interface {v0}, LX/04n;->c()V

    .line 52647
    :cond_2
    return-void

    .line 52648
    :catch_0
    move-exception v0

    .line 52649
    :try_start_2
    new-instance v1, LX/0OJ;

    iget-object v2, p0, LX/0OD;->k:LX/0OA;

    invoke-direct {v1, v0, v2}, LX/0OJ;-><init>(Ljava/io/IOException;LX/0OA;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 52650
    :catchall_0
    move-exception v0

    iput-object v5, p0, LX/0OD;->m:Ljava/io/InputStream;

    .line 52651
    invoke-direct {p0}, LX/0OD;->f()V

    .line 52652
    iget-boolean v1, p0, LX/0OD;->n:Z

    if-eqz v1, :cond_3

    .line 52653
    iput-boolean v4, p0, LX/0OD;->n:Z

    .line 52654
    iget-object v1, p0, LX/0OD;->j:LX/04n;

    if-eqz v1, :cond_3

    .line 52655
    iget-object v1, p0, LX/0OD;->j:LX/04n;

    invoke-interface {v1}, LX/04n;->c()V

    :cond_3
    throw v0

    :cond_4
    :try_start_3
    iget-wide v6, p0, LX/0OD;->p:J

    iget-wide v8, p0, LX/0OD;->r:J

    sub-long/2addr v6, v8

    goto :goto_0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 52656
    :cond_5
    :try_start_4
    const v6, 0x455acd1d
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-static {v0, v6}, LX/04e;->b(Ljava/net/URLConnection;I)Ljava/io/InputStream;

    move-result-object v6

    .line 52657
    const-wide/16 v8, -0x1

    cmp-long v7, v2, v8

    if-nez v7, :cond_8

    .line 52658
    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    .line 52659
    :cond_6
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    .line 52660
    const-string v8, "com.android.okhttp.internal.http.HttpTransport$ChunkedInputStream"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_7

    const-string v8, "com.android.okhttp.internal.http.HttpTransport$FixedLengthInputStream"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 52661
    :cond_7
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v7

    .line 52662
    const-string v8, "unexpectedEndOfInput"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v7

    .line 52663
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 52664
    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v7, v6, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 52665
    :catch_1
    goto/16 :goto_1

    .line 52666
    :cond_8
    const-wide/16 v8, 0x800

    cmp-long v7, v2, v8

    if-gtz v7, :cond_6

    goto/16 :goto_1

    .line 52667
    :catch_2
    goto/16 :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52635
    iget-object v0, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 52634
    iget-object v0, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0OD;->l:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method
