.class public LX/0Jj;
.super LX/0Ji;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:I

.field private final b:LX/0Gb;

.field private final h:LX/04m;

.field private final i:Landroid/net/Uri;

.field public final j:Ljava/lang/String;

.field private final k:LX/0Sh;

.field private final l:LX/1m0;

.field private final m:LX/1m6;

.field private final n:LX/0wp;

.field private final o:LX/0KZ;

.field private final p:Z

.field private final q:LX/0ka;

.field private final r:LX/0oz;

.field private final s:LX/0TD;

.field private final t:LX/0YR;

.field private final u:LX/1AA;

.field private final v:LX/0wq;

.field private final w:LX/19m;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1m6;Landroid/content/Context;Landroid/os/Handler;LX/0Jv;LX/0Js;LX/0Sh;LX/1m0;LX/0wp;LX/0KZ;LX/04m;ZLX/0ka;LX/0oz;LX/0TD;LX/0YR;LX/1AA;LX/0wq;LX/19m;)V
    .locals 7

    .prologue
    .line 39634
    move-object v1, p0

    move-object v2, p1

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    move-object v6, p8

    invoke-direct/range {v1 .. v6}, LX/0Ji;-><init>(Landroid/net/Uri;Landroid/content/Context;Landroid/os/Handler;LX/0Jv;LX/0Js;)V

    .line 39635
    iput-object p4, p0, LX/0Jj;->m:LX/1m6;

    .line 39636
    move-object/from16 v0, p11

    iput-object v0, p0, LX/0Jj;->n:LX/0wp;

    .line 39637
    iget-object v1, p0, LX/0Jj;->m:LX/1m6;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1m6;->a(Z)V

    .line 39638
    iput-object p2, p0, LX/0Jj;->i:Landroid/net/Uri;

    .line 39639
    iput-object p3, p0, LX/0Jj;->j:Ljava/lang/String;

    .line 39640
    move-object/from16 v0, p9

    iput-object v0, p0, LX/0Jj;->k:LX/0Sh;

    .line 39641
    move-object/from16 v0, p10

    iput-object v0, p0, LX/0Jj;->l:LX/1m0;

    .line 39642
    move-object/from16 v0, p12

    iput-object v0, p0, LX/0Jj;->o:LX/0KZ;

    .line 39643
    move-object/from16 v0, p13

    iput-object v0, p0, LX/0Jj;->h:LX/04m;

    .line 39644
    move/from16 v0, p14

    iput-boolean v0, p0, LX/0Jj;->p:Z

    .line 39645
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0Jj;->q:LX/0ka;

    .line 39646
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0Jj;->r:LX/0oz;

    .line 39647
    move-object/from16 v0, p17

    iput-object v0, p0, LX/0Jj;->s:LX/0TD;

    .line 39648
    move-object/from16 v0, p11

    iget v1, v0, LX/0wp;->B:I

    iput v1, p0, LX/0Jj;->a:I

    .line 39649
    new-instance v1, LX/0Kt;

    new-instance v2, LX/0OB;

    iget v3, p0, LX/0Jj;->a:I

    invoke-direct {v2, v3}, LX/0OB;-><init>(I)V

    invoke-direct {v1, v2}, LX/0Kt;-><init>(LX/0O1;)V

    iput-object v1, p0, LX/0Jj;->b:LX/0Gb;

    .line 39650
    move-object/from16 v0, p18

    iput-object v0, p0, LX/0Jj;->t:LX/0YR;

    .line 39651
    move-object/from16 v0, p19

    iput-object v0, p0, LX/0Jj;->u:LX/1AA;

    .line 39652
    move-object/from16 v0, p20

    iput-object v0, p0, LX/0Jj;->v:LX/0wq;

    .line 39653
    move-object/from16 v0, p21

    iput-object v0, p0, LX/0Jj;->w:LX/19m;

    .line 39654
    return-void
.end method

.method public static a$redex0(LX/0Jj;LX/0Ah;LX/03z;J)LX/0GT;
    .locals 9
    .param p0    # LX/0Jj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 39655
    if-nez p1, :cond_0

    move-object v0, v8

    .line 39656
    :goto_0
    return-object v0

    .line 39657
    :cond_0
    iget-object v0, p1, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    .line 39658
    const-string v1, "audio/webm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 39659
    new-instance v2, LX/0K2;

    new-instance v0, LX/0OE;

    iget-object v1, p0, LX/0Ji;->d:Landroid/content/Context;

    const-string v3, "ExoPlayer"

    invoke-direct {v0, v1, v3}, LX/0OE;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, LX/0Jj;->m:LX/1m6;

    invoke-virtual {v1}, LX/1m6;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, LX/0Jj;->l:LX/1m0;

    invoke-direct {v2, v0, v1, v3}, LX/0K2;-><init>(LX/0OE;Ljava/lang/String;LX/1m0;)V

    .line 39660
    new-instance v0, LX/0Lq;

    invoke-static {}, LX/0Lt;->a()LX/0Lt;

    move-result-object v1

    new-instance v3, LX/0Le;

    invoke-direct {v3}, LX/0Le;-><init>()V

    new-array v7, v6, [LX/0Ah;

    const/4 v4, 0x0

    aput-object p1, v7, v4

    move-wide v4, p3

    invoke-direct/range {v0 .. v7}, LX/0Lq;-><init>(LX/0Lr;LX/0G6;LX/04o;JI[LX/0Ah;)V

    .line 39661
    new-instance v1, LX/0LY;

    iget-object v2, p0, LX/0Jj;->b:LX/0Gb;

    iget-object v3, p0, LX/0Jj;->n:LX/0wp;

    iget v3, v3, LX/0wp;->A:I

    iget v4, p0, LX/0Jj;->a:I

    mul-int/2addr v3, v4

    invoke-direct {v1, v0, v2, v3}, LX/0LY;-><init>(LX/0LZ;LX/0Gb;I)V

    .line 39662
    new-instance v2, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    iget-object v0, p0, LX/0Jj;->w:LX/19m;

    invoke-virtual {v0}, LX/19m;->q()I

    move-result v0

    iget-object v3, p0, LX/0Jj;->w:LX/19m;

    invoke-virtual {v3}, LX/19m;->r()I

    move-result v3

    invoke-direct {v2, p2, v0, v3, v8}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;-><init>(LX/03z;IILX/0Ka;)V

    .line 39663
    iget-object v0, p0, LX/0Jj;->w:LX/19m;

    invoke-virtual {v0}, LX/19m;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 39664
    new-instance v0, LX/0Kh;

    iget-object v3, p0, LX/0Ji;->e:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3, v8}, LX/0Kh;-><init>(LX/0L9;Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;Landroid/os/Handler;LX/0Ka;)V

    goto :goto_0

    .line 39665
    :cond_1
    new-instance v0, LX/0Kj;

    iget-object v3, p0, LX/0Ji;->e:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3, v8}, LX/0Kj;-><init>(LX/0L9;Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;Landroid/os/Handler;LX/0Ka;)V

    goto :goto_0

    .line 39666
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected mime type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a$redex0(LX/0Jj;LX/0Ah;J)LX/0GX;
    .locals 8
    .param p0    # LX/0Jj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 39667
    if-nez p1, :cond_0

    .line 39668
    const/4 v0, 0x0

    .line 39669
    :goto_0
    return-object v0

    .line 39670
    :cond_0
    iget-object v0, p1, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    .line 39671
    const-string v1, "audio/mp4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "audio/mp4a-latm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 39672
    :cond_1
    new-instance v2, LX/0K2;

    new-instance v0, LX/0OE;

    iget-object v1, p0, LX/0Ji;->d:Landroid/content/Context;

    const-string v3, "ExoPlayer"

    invoke-direct {v0, v1, v3}, LX/0OE;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, LX/0Jj;->m:LX/1m6;

    invoke-virtual {v1}, LX/1m6;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, LX/0Jj;->l:LX/1m0;

    invoke-direct {v2, v0, v1, v3}, LX/0K2;-><init>(LX/0OE;Ljava/lang/String;LX/1m0;)V

    .line 39673
    new-instance v0, LX/0Lq;

    invoke-static {}, LX/0Lt;->a()LX/0Lt;

    move-result-object v1

    new-instance v3, LX/0Le;

    invoke-direct {v3}, LX/0Le;-><init>()V

    new-array v7, v6, [LX/0Ah;

    const/4 v4, 0x0

    aput-object p1, v7, v4

    move-wide v4, p2

    invoke-direct/range {v0 .. v7}, LX/0Lq;-><init>(LX/0Lr;LX/0G6;LX/04o;JI[LX/0Ah;)V

    .line 39674
    new-instance v1, LX/0LY;

    iget-object v2, p0, LX/0Jj;->b:LX/0Gb;

    iget-object v3, p0, LX/0Jj;->n:LX/0wp;

    iget v3, v3, LX/0wp;->A:I

    iget v4, p0, LX/0Jj;->a:I

    mul-int/2addr v3, v4

    invoke-direct {v1, v0, v2, v3}, LX/0LY;-><init>(LX/0LZ;LX/0Gb;I)V

    .line 39675
    new-instance v0, LX/0Jt;

    iget-object v2, p0, LX/0Ji;->e:Landroid/os/Handler;

    iget-object v3, p0, LX/0Ji;->g:LX/0Js;

    invoke-direct {v0, v1, v2, v3}, LX/0Jt;-><init>(LX/0L9;Landroid/os/Handler;LX/0Js;)V

    goto :goto_0

    .line 39676
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected mime type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a$redex0(LX/0Jj;LX/0AY;)LX/0Jw;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v10, -0x1

    const/4 v11, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 39677
    invoke-virtual {p1}, LX/0AY;->b()I

    move-result v0

    if-ne v0, v11, :cond_1

    move v0, v11

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 39678
    invoke-virtual {p1, v8}, LX/0AY;->a(I)LX/0Am;

    move-result-object v0

    .line 39679
    invoke-virtual {v0, v8}, LX/0Am;->a(I)I

    move-result v1

    .line 39680
    if-eq v1, v10, :cond_7

    .line 39681
    iget-object v0, v0, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    move-object v9, v0

    .line 39682
    :goto_1
    if-nez v9, :cond_2

    .line 39683
    :cond_0
    :goto_2
    return-object v2

    :cond_1
    move v0, v8

    .line 39684
    goto :goto_0

    .line 39685
    :cond_2
    :try_start_0
    iget-object v0, p0, LX/0Ji;->d:Landroid/content/Context;

    iget-object v1, v9, LX/0Ak;->c:Ljava/util/List;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v3, v4}, LX/0Li;->a(Landroid/content/Context;Ljava/util/List;[Ljava/lang/String;Z)[I
    :try_end_0
    .catch LX/090; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 39686
    iget-object v0, p0, LX/0Jj;->n:LX/0wp;

    invoke-virtual {v0}, LX/0wp;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v12, :cond_0

    array-length v0, v12

    if-eqz v0, :cond_0

    .line 39687
    :cond_3
    iget-object v0, v9, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    iget-object v0, v0, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    .line 39688
    const-string v1, "video/avc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "video/mp4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "video/x-vnd.on2.vp9"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "video/webm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 39689
    :cond_4
    new-instance v13, LX/0K2;

    new-instance v0, LX/0OE;

    iget-object v1, p0, LX/0Ji;->d:Landroid/content/Context;

    const-string v2, "ExoPlayer"

    invoke-direct {v0, v1, v2}, LX/0OE;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, LX/0Jj;->m:LX/1m6;

    invoke-virtual {v1}, LX/1m6;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0Jj;->l:LX/1m0;

    invoke-direct {v13, v0, v1, v2}, LX/0K2;-><init>(LX/0OE;Ljava/lang/String;LX/1m0;)V

    .line 39690
    iget-object v0, p0, LX/0Jj;->n:LX/0wp;

    iget-object v1, p0, LX/0Jj;->m:LX/1m6;

    iget-object v2, p0, LX/0Jj;->h:LX/04m;

    iget-boolean v3, p0, LX/0Jj;->p:Z

    iget-object v4, p0, LX/0Jj;->q:LX/0ka;

    iget-object v5, p0, LX/0Jj;->r:LX/0oz;

    iget-object v6, p0, LX/0Jj;->t:LX/0YR;

    iget-object v7, p0, LX/0Jj;->u:LX/1AA;

    invoke-static/range {v0 .. v7}, LX/1m7;->a(LX/0wp;LX/1m6;LX/04m;ZLX/0ka;LX/0oz;LX/0YR;LX/1AA;)LX/04o;

    move-result-object v3

    .line 39691
    array-length v2, v12

    .line 39692
    new-array v7, v2, [LX/0Ah;

    move v1, v8

    .line 39693
    :goto_3
    if-ge v1, v2, :cond_5

    .line 39694
    iget-object v0, v9, LX/0Ak;->c:Ljava/util/List;

    aget v4, v12, v1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    aput-object v0, v7, v1

    .line 39695
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 39696
    :cond_5
    new-instance v0, LX/0Lq;

    iget-object v1, p0, LX/0Ji;->d:Landroid/content/Context;

    invoke-static {v1, v8, v8}, LX/0Lt;->a(Landroid/content/Context;ZZ)LX/0Lt;

    move-result-object v1

    iget-wide v4, p1, LX/0AY;->c:J

    iget v6, v9, LX/0Ak;->b:I

    move-object v2, v13

    invoke-direct/range {v0 .. v7}, LX/0Lq;-><init>(LX/0Lr;LX/0G6;LX/04o;JI[LX/0Ah;)V

    .line 39697
    new-instance v1, LX/0LY;

    iget-object v3, p0, LX/0Jj;->b:LX/0Gb;

    iget-object v2, p0, LX/0Jj;->n:LX/0wp;

    iget v2, v2, LX/0wp;->z:I

    iget v4, p0, LX/0Jj;->a:I

    mul-int/2addr v4, v2

    iget-object v5, p0, LX/0Ji;->e:Landroid/os/Handler;

    iget-object v6, p0, LX/0Jj;->o:LX/0KZ;

    move-object v2, v0

    move v7, v11

    invoke-direct/range {v1 .. v7}, LX/0LY;-><init>(LX/0LZ;LX/0Gb;ILandroid/os/Handler;LX/0KZ;I)V

    .line 39698
    new-instance v2, LX/0Jx;

    iget-object v3, p0, LX/0Ji;->d:Landroid/content/Context;

    const-wide/16 v6, 0x0

    iget-object v8, p0, LX/0Ji;->e:Landroid/os/Handler;

    iget-object v9, p0, LX/0Ji;->f:LX/0Jv;

    move-object v4, v1

    move v5, v11

    invoke-direct/range {v2 .. v10}, LX/0Jx;-><init>(Landroid/content/Context;LX/0L9;IJLandroid/os/Handler;LX/0Jv;I)V

    goto/16 :goto_2

    .line 39699
    :cond_6
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected mime type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 39700
    :catch_0
    goto/16 :goto_2

    :cond_7
    move-object v9, v2

    goto/16 :goto_1
.end method


# virtual methods
.method public final a()LX/09L;
    .locals 1

    .prologue
    .line 39701
    sget-object v0, LX/09L;->DASH:LX/09L;

    return-object v0
.end method

.method public final a(LX/0Jp;)V
    .locals 6

    .prologue
    .line 39702
    const-string v0, "ExoPlayerDashStreamRenderBuilder.build"

    const v1, 0x354685f6

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 39703
    :try_start_0
    new-instance v1, LX/0AZ;

    invoke-direct {v1}, LX/0AZ;-><init>()V

    .line 39704
    iget-object v0, p0, LX/0Jj;->j:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39705
    new-instance v0, LX/0Od;

    iget-object v2, p0, LX/0Jj;->i:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/0OD;

    const-string v4, "ExoPlayer_DASH"

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, LX/0OD;-><init>(Ljava/lang/String;LX/0OH;)V

    invoke-direct {v0, v2, v3, v1}, LX/0Od;-><init>(Ljava/lang/String;LX/0G7;LX/0Aa;)V

    .line 39706
    iget-object v1, p0, LX/0Ji;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, LX/0Jh;

    invoke-direct {v2, p0, p1}, LX/0Jh;-><init>(LX/0Jj;LX/0Jp;)V

    invoke-virtual {v0, v1, v2}, LX/0Od;->a(Landroid/os/Looper;LX/0GJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39707
    :goto_0
    const v0, 0x4902b793

    invoke-static {v0}, LX/02m;->a(I)V

    .line 39708
    return-void

    .line 39709
    :cond_0
    :try_start_1
    new-instance v0, LX/0K1;

    iget-object v2, p0, LX/0Jj;->i:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/0Jj;->j:Ljava/lang/String;

    iget-object v4, p0, LX/0Jj;->s:LX/0TD;

    iget-object v5, p0, LX/0Jj;->k:LX/0Sh;

    invoke-direct/range {v0 .. v5}, LX/0K1;-><init>(LX/0AZ;Ljava/lang/String;Ljava/lang/String;LX/0TD;LX/0Sh;)V

    .line 39710
    new-instance v1, LX/0Jh;

    invoke-direct {v1, p0, p1}, LX/0Jh;-><init>(LX/0Jj;LX/0Jp;)V

    .line 39711
    invoke-virtual {v0, v1}, LX/0K1;->a(LX/0GJ;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 39712
    :catchall_0
    move-exception v0

    const v1, -0x6630af76

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
