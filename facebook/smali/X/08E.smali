.class public final LX/08E;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public mFlags:I

.field private mMaximumOptimizationAttempts:I

.field public mOptTimeSliceMs:I

.field public mPrio:LX/08D;

.field private mProcessPollMs:I

.field private mTimeBetweenOptimizationAttemptsMs:I

.field public mYieldTimeSliceMs:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20956
    invoke-static {}, LX/08D;->unchanged()LX/08D;

    move-result-object v0

    iput-object v0, p0, LX/08E;->mPrio:LX/08D;

    .line 20957
    iput v1, p0, LX/08E;->mFlags:I

    .line 20958
    const/16 v0, 0x3e8

    iput v0, p0, LX/08E;->mOptTimeSliceMs:I

    .line 20959
    iput v1, p0, LX/08E;->mYieldTimeSliceMs:I

    .line 20960
    const/16 v0, 0x64

    iput v0, p0, LX/08E;->mProcessPollMs:I

    .line 20961
    const v0, 0x36ee80

    iput v0, p0, LX/08E;->mTimeBetweenOptimizationAttemptsMs:I

    .line 20962
    const/16 v0, 0xa

    iput v0, p0, LX/08E;->mMaximumOptimizationAttempts:I

    .line 20963
    return-void
.end method

.method public constructor <init>(LX/08F;)V
    .locals 1

    .prologue
    .line 20964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20965
    iget-object v0, p1, LX/08F;->prio:LX/08D;

    iput-object v0, p0, LX/08E;->mPrio:LX/08D;

    .line 20966
    iget v0, p1, LX/08F;->flags:I

    iput v0, p0, LX/08E;->mFlags:I

    .line 20967
    iget v0, p1, LX/08F;->optTimeSliceMs:I

    iput v0, p0, LX/08E;->mOptTimeSliceMs:I

    .line 20968
    iget v0, p1, LX/08F;->yieldTimeSliceMs:I

    iput v0, p0, LX/08E;->mYieldTimeSliceMs:I

    .line 20969
    iget v0, p1, LX/08F;->processPollMs:I

    iput v0, p0, LX/08E;->mProcessPollMs:I

    .line 20970
    iget v0, p1, LX/08F;->timeBetweenOptimizationAttemptsMs:I

    iput v0, p0, LX/08E;->mTimeBetweenOptimizationAttemptsMs:I

    .line 20971
    iget v0, p1, LX/08F;->maximumOptimizationAttempts:I

    iput v0, p0, LX/08E;->mMaximumOptimizationAttempts:I

    .line 20972
    return-void
.end method


# virtual methods
.method public final build()LX/08F;
    .locals 9

    .prologue
    .line 20973
    new-instance v0, LX/08F;

    iget-object v1, p0, LX/08E;->mPrio:LX/08D;

    iget v2, p0, LX/08E;->mFlags:I

    iget v3, p0, LX/08E;->mOptTimeSliceMs:I

    iget v4, p0, LX/08E;->mYieldTimeSliceMs:I

    iget v5, p0, LX/08E;->mProcessPollMs:I

    iget v6, p0, LX/08E;->mTimeBetweenOptimizationAttemptsMs:I

    iget v7, p0, LX/08E;->mMaximumOptimizationAttempts:I

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, LX/08F;-><init>(LX/08D;IIIIIILX/0Fi;)V

    return-object v0
.end method

.method public final setMaximumOptimizationAttempts(I)LX/08E;
    .locals 0

    .prologue
    .line 20974
    iput p1, p0, LX/08E;->mMaximumOptimizationAttempts:I

    .line 20975
    return-object p0
.end method

.method public final setProcessPollMs(I)LX/08E;
    .locals 0

    .prologue
    .line 20976
    iput p1, p0, LX/08E;->mProcessPollMs:I

    .line 20977
    return-object p0
.end method

.method public final setTimeBetweenOptimizationAttemptsMs(I)LX/08E;
    .locals 0

    .prologue
    .line 20978
    iput p1, p0, LX/08E;->mTimeBetweenOptimizationAttemptsMs:I

    .line 20979
    return-object p0
.end method
