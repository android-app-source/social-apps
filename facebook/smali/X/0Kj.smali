.class public LX/0Kj;
.super LX/0Kg;
.source ""

# interfaces
.implements LX/0GW;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

.field public final c:Landroid/os/Handler;

.field public final d:LX/0Ka;

.field private final e:LX/0LN;

.field private f:J

.field private g:Z

.field private h:I

.field private i:I

.field private j:I

.field private k:Ljava/nio/ByteBuffer;

.field private l:I


# direct methods
.method public constructor <init>(LX/0L9;Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;Landroid/os/Handler;LX/0Ka;)V
    .locals 1

    .prologue
    .line 41900
    invoke-direct {p0, p1, p3, p4}, LX/0Kg;-><init>(LX/0L9;Landroid/os/Handler;LX/0Ka;)V

    .line 41901
    iput-object p2, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    .line 41902
    iput-object p3, p0, LX/0Kj;->c:Landroid/os/Handler;

    .line 41903
    iput-object p4, p0, LX/0Kj;->d:LX/0Ka;

    .line 41904
    const/4 v0, 0x0

    iput v0, p0, LX/0Kj;->h:I

    .line 41905
    new-instance v0, LX/0LN;

    invoke-direct {v0}, LX/0LN;-><init>()V

    iput-object v0, p0, LX/0Kj;->e:LX/0LN;

    .line 41906
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 42002
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    invoke-virtual {p0}, LX/0Kj;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/0LN;->a(Z)J

    move-result-wide v0

    .line 42003
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 42004
    iget-boolean v2, p0, LX/0Kj;->g:Z

    if-eqz v2, :cond_1

    :goto_0
    iput-wide v0, p0, LX/0Kj;->f:J

    .line 42005
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Kj;->g:Z

    .line 42006
    :cond_0
    iget-wide v0, p0, LX/0Kj;->f:J

    return-wide v0

    .line 42007
    :cond_1
    iget-wide v2, p0, LX/0Kj;->f:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 41993
    packed-switch p1, :pswitch_data_0

    .line 41994
    invoke-super {p0, p1, p2}, LX/0Kg;->a(ILjava/lang/Object;)V

    .line 41995
    :goto_0
    return-void

    .line 41996
    :pswitch_0
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, LX/0LN;->a(F)V

    goto :goto_0

    .line 41997
    :pswitch_1
    check-cast p2, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;

    .line 41998
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    iget-object v1, p2, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->d:[F

    invoke-virtual {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a([F)V

    goto :goto_0

    .line 41999
    :pswitch_2
    check-cast p2, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;

    .line 42000
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    iget-boolean v1, p2, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->a:Z

    invoke-virtual {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a(Z)V

    .line 42001
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    iget-wide v2, p2, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->b:D

    double-to-float v1, v2

    iget-wide v2, p2, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->c:D

    double-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a(FF)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/media/MediaFormat;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 41982
    const-string v0, "sample-rate"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 41983
    const-string v1, "mime"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41984
    new-instance v2, Landroid/media/MediaFormat;

    invoke-direct {v2}, Landroid/media/MediaFormat;-><init>()V

    .line 41985
    const-string v3, "channel-count"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 41986
    const-string v3, "sample-rate"

    invoke-virtual {v2, v3, v0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 41987
    const-string v3, "mime"

    invoke-virtual {v2, v3, v1}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 41988
    iget-object v1, p0, LX/0Kj;->e:LX/0LN;

    invoke-virtual {v1, v2, v5}, LX/0LN;->a(Landroid/media/MediaFormat;Z)V

    .line 41989
    :try_start_0
    iget-object v1, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    int-to-float v0, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a(FZ)V
    :try_end_0
    .catch LX/0Kd; {:try_start_0 .. :try_end_0} :catch_0

    .line 41990
    return-void

    .line 41991
    :catch_0
    move-exception v0

    .line 41992
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(LX/0M7;)Z
    .locals 9

    .prologue
    const/4 v2, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 41933
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41934
    :try_start_0
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a()V
    :try_end_0
    .catch LX/0Kd; {:try_start_0 .. :try_end_0} :catch_0

    .line 41935
    iget v0, p0, LX/0GT;->a:I

    move v0, v0

    .line 41936
    if-ne v0, v2, :cond_0

    .line 41937
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->e()V

    .line 41938
    :cond_0
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 41939
    :try_start_1
    iget v0, p0, LX/0Kj;->h:I

    if-eqz v0, :cond_5

    .line 41940
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    iget v1, p0, LX/0Kj;->h:I

    invoke-virtual {v0, v1}, LX/0LN;->a(I)I
    :try_end_1
    .catch LX/0LK; {:try_start_1 .. :try_end_1} :catch_1

    .line 41941
    :goto_0
    iget v0, p0, LX/0GT;->a:I

    move v0, v0

    .line 41942
    if-ne v0, v2, :cond_1

    .line 41943
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->e()V

    .line 41944
    :cond_1
    iget v0, p0, LX/0Kj;->i:I

    if-nez v0, :cond_2

    .line 41945
    iget-object v0, p1, LX/0M7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iput v0, p0, LX/0Kj;->i:I

    .line 41946
    iget-object v0, p1, LX/0M7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iput v0, p0, LX/0Kj;->j:I

    .line 41947
    :cond_2
    :try_start_2
    iget-object v0, p1, LX/0M7;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0Kj;->j:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 41948
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    iget-object v1, p1, LX/0M7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 41949
    iget v1, p0, LX/0Kj;->i:I

    sub-int/2addr v1, v0

    iput v1, p0, LX/0Kj;->i:I

    .line 41950
    iget v1, p0, LX/0Kj;->j:I

    add-int/2addr v0, v1

    iput v0, p0, LX/0Kj;->j:I
    :try_end_2
    .catch LX/09Y; {:try_start_2 .. :try_end_2} :catch_4
    .catch LX/0Kf; {:try_start_2 .. :try_end_2} :catch_2

    .line 41951
    iget-object v0, p0, LX/0Kj;->k:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_3

    .line 41952
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->j()I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/0Kj;->k:Ljava/nio/ByteBuffer;

    .line 41953
    iget-object v0, p0, LX/0Kj;->k:Ljava/nio/ByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    :cond_3
    move v0, v8

    .line 41954
    :goto_1
    if-eqz v0, :cond_9

    .line 41955
    iget v1, p0, LX/0Kj;->l:I

    if-nez v1, :cond_c

    .line 41956
    iget-object v1, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    iget-object v2, p0, LX/0Kj;->k:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v2}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->b(Ljava/nio/ByteBuffer;)I

    move-result v1

    iput v1, p0, LX/0Kj;->l:I

    .line 41957
    iget v1, p0, LX/0Kj;->l:I

    if-nez v1, :cond_c

    move v6, v7

    .line 41958
    :goto_2
    iget v0, p0, LX/0Kj;->l:I

    if-lez v0, :cond_b

    .line 41959
    :try_start_3
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    iget-object v1, p0, LX/0Kj;->k:Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    iget v3, p0, LX/0Kj;->l:I

    iget-wide v4, p1, LX/0M6;->a:J

    invoke-virtual/range {v0 .. v5}, LX/0LN;->a(Ljava/nio/ByteBuffer;IIJ)I
    :try_end_3
    .catch LX/0LM; {:try_start_3 .. :try_end_3} :catch_3

    move-result v0

    .line 41960
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_4

    .line 41961
    iput-boolean v8, p0, LX/0Kj;->g:Z

    .line 41962
    :cond_4
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    .line 41963
    iput v7, p0, LX/0Kj;->l:I

    move v0, v6

    goto :goto_1

    .line 41964
    :catch_0
    move-exception v0

    .line 41965
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 41966
    :cond_5
    :try_start_4
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->b()I

    move-result v0

    iput v0, p0, LX/0Kj;->h:I
    :try_end_4
    .catch LX/0LK; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 41967
    :catch_1
    move-exception v0

    .line 41968
    iget-object v1, p0, LX/0Kj;->c:Landroid/os/Handler;

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/0Kj;->d:LX/0Ka;

    if-eqz v1, :cond_6

    .line 41969
    iget-object v1, p0, LX/0Kj;->c:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/video/vps/spatialaudio/SpatialAudioTrackRenderer$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/video/vps/spatialaudio/SpatialAudioTrackRenderer$1;-><init>(LX/0Kj;LX/0LK;)V

    const v3, 0x40f38434

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 41970
    :cond_6
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 41971
    :catch_2
    move-exception v0

    .line 41972
    :goto_3
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 41973
    :catch_3
    move-exception v0

    .line 41974
    iget-object v1, p0, LX/0Kj;->c:Landroid/os/Handler;

    if-eqz v1, :cond_7

    iget-object v1, p0, LX/0Kj;->d:LX/0Ka;

    if-eqz v1, :cond_7

    .line 41975
    iget-object v1, p0, LX/0Kj;->c:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/video/vps/spatialaudio/SpatialAudioTrackRenderer$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/video/vps/spatialaudio/SpatialAudioTrackRenderer$2;-><init>(LX/0Kj;LX/0LM;)V

    const v3, -0x26b760ba

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 41976
    :cond_7
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_8
    move v0, v7

    .line 41977
    goto/16 :goto_1

    .line 41978
    :cond_9
    iget v0, p0, LX/0Kj;->i:I

    if-nez v0, :cond_a

    .line 41979
    iget-object v0, p0, LX/0Kg;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->f:I

    move v7, v8

    .line 41980
    :cond_a
    return v7

    .line 41981
    :catch_4
    move-exception v0

    goto :goto_3

    :cond_b
    move v0, v6

    goto/16 :goto_1

    :cond_c
    move v6, v0

    goto/16 :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 41932
    invoke-super {p0}, LX/0Kg;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(J)V
    .locals 1

    .prologue
    .line 41926
    invoke-super {p0, p1, p2}, LX/0Kg;->c(J)V

    .line 41927
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->j()V

    .line 41928
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->h()V

    .line 41929
    iput-wide p1, p0, LX/0Kj;->f:J

    .line 41930
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Kj;->g:Z

    .line 41931
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 41925
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, LX/0Kg;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/0GW;
    .locals 0

    .prologue
    .line 41924
    return-object p0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 41920
    invoke-super {p0}, LX/0Kg;->h()V

    .line 41921
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->e()V

    .line 41922
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->e()V

    .line 41923
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 41916
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->i()V

    .line 41917
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->f()V

    .line 41918
    invoke-super {p0}, LX/0Kg;->i()V

    .line 41919
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 41910
    const/4 v0, 0x0

    iput v0, p0, LX/0Kj;->h:I

    .line 41911
    :try_start_0
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->k()V

    .line 41912
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41913
    invoke-super {p0}, LX/0Kg;->j()V

    .line 41914
    return-void

    .line 41915
    :catchall_0
    move-exception v0

    invoke-super {p0}, LX/0Kg;->j()V

    throw v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 41907
    iget-object v0, p0, LX/0Kj;->b:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->d()V

    .line 41908
    iget-object v0, p0, LX/0Kj;->e:LX/0LN;

    invoke-virtual {v0}, LX/0LN;->g()V

    .line 41909
    return-void
.end method
