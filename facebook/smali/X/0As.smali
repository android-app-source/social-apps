.class public final LX/0As;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/0Lu;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0Lu;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0Lu;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0Lu;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/List;LX/0Lu;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0Lu;",
            ">;",
            "LX/0Lu;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 25110
    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v1, v2

    .line 25111
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 25112
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Lu;

    iget-object v0, v0, LX/0Lu;->a:Ljava/lang/String;

    iget-object v3, p1, LX/0Lu;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 25113
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 25114
    goto :goto_1

    .line 25115
    :cond_1
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25116
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 25084
    iget-boolean v1, p0, LX/0As;->d:Z

    if-nez v1, :cond_1

    .line 25085
    iget-object v1, p0, LX/0As;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 25086
    iget-object v1, p0, LX/0As;->c:Ljava/util/ArrayList;

    invoke-static {v1, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 25087
    :cond_0
    iget-object v1, p0, LX/0As;->c:Ljava/util/ArrayList;

    iput-object v1, p0, LX/0As;->b:Ljava/util/ArrayList;

    .line 25088
    iput-boolean v0, p0, LX/0As;->d:Z

    .line 25089
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0As;->c:Ljava/util/ArrayList;

    .line 25090
    return-void

    .line 25091
    :cond_1
    iget-object v1, p0, LX/0As;->c:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 25092
    iget-object v1, p0, LX/0As;->b:Ljava/util/ArrayList;

    if-nez v1, :cond_2

    :goto_1
    invoke-static {v0}, LX/0Av;->b(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 25093
    :cond_3
    iget-object v0, p0, LX/0As;->c:Ljava/util/ArrayList;

    invoke-static {v0, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 25094
    iget-object v0, p0, LX/0As;->c:Ljava/util/ArrayList;

    iget-object v1, p0, LX/0As;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0Av;->b(Z)V

    goto :goto_0
.end method

.method public final a(LX/0Lu;)V
    .locals 1

    .prologue
    .line 25106
    iget-object v0, p0, LX/0As;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 25107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0As;->a:Ljava/util/ArrayList;

    .line 25108
    :cond_0
    iget-object v0, p0, LX/0As;->a:Ljava/util/ArrayList;

    invoke-static {v0, p1}, LX/0As;->a(Ljava/util/List;LX/0Lu;)V

    .line 25109
    return-void
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/0Lu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25097
    iget-object v0, p0, LX/0As;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 25098
    iget-object v0, p0, LX/0As;->b:Ljava/util/ArrayList;

    .line 25099
    :goto_0
    return-object v0

    .line 25100
    :cond_0
    iget-object v0, p0, LX/0As;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 25101
    iget-object v0, p0, LX/0As;->a:Ljava/util/ArrayList;

    goto :goto_0

    .line 25102
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, LX/0As;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 25103
    iget-object v2, p0, LX/0As;->a:Ljava/util/ArrayList;

    iget-object v0, p0, LX/0As;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Lu;

    invoke-static {v2, v0}, LX/0As;->a(Ljava/util/List;LX/0Lu;)V

    .line 25104
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 25105
    :cond_2
    iget-object v0, p0, LX/0As;->a:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 25095
    check-cast p1, LX/0Lu;

    check-cast p2, LX/0Lu;

    .line 25096
    iget-object v0, p1, LX/0Lu;->a:Ljava/lang/String;

    iget-object v1, p2, LX/0Lu;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
