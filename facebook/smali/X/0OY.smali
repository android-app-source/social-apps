.class public final LX/0OY;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[B

.field private static final b:[I

.field private static final c:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53345
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/0OY;->a:[B

    .line 53346
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, LX/0OY;->b:[I

    .line 53347
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, LX/0OY;->c:[I

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 53348
    :array_1
    .array-data 4
        0x17700
        0x15888
        0xfa00
        0xbb80
        0xac44
        0x7d00
        0x5dc0
        0x5622
        0x3e80
        0x2ee0
        0x2b11
        0x1f40
        0x1cb6
    .end array-data

    .line 53349
    :array_2
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x8
        -0x1
        -0x1
        -0x1
        0x7
        0x8
        -0x1
        0x8
        -0x1
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 53350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([B)Landroid/util/Pair;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v9, 0xd

    const/4 v8, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x4

    .line 53351
    new-instance v4, LX/0Oi;

    invoke-direct {v4, p0}, LX/0Oi;-><init>([B)V

    .line 53352
    invoke-virtual {v4, v8}, LX/0Oi;->c(I)I

    move-result v5

    .line 53353
    invoke-virtual {v4, v7}, LX/0Oi;->c(I)I

    move-result v3

    .line 53354
    const/16 v0, 0xf

    if-ne v3, v0, :cond_1

    .line 53355
    const/16 v0, 0x18

    invoke-virtual {v4, v0}, LX/0Oi;->c(I)I

    move-result v0

    .line 53356
    :goto_0
    invoke-virtual {v4, v7}, LX/0Oi;->c(I)I

    move-result v3

    .line 53357
    if-eq v5, v8, :cond_0

    const/16 v6, 0x1d

    if-ne v5, v6, :cond_6

    .line 53358
    :cond_0
    invoke-virtual {v4, v7}, LX/0Oi;->c(I)I

    move-result v5

    .line 53359
    const/16 v0, 0xf

    if-ne v5, v0, :cond_3

    .line 53360
    const/16 v0, 0x18

    invoke-virtual {v4, v0}, LX/0Oi;->c(I)I

    move-result v0

    .line 53361
    :goto_1
    invoke-virtual {v4, v8}, LX/0Oi;->c(I)I

    move-result v5

    .line 53362
    const/16 v6, 0x16

    if-ne v5, v6, :cond_6

    .line 53363
    invoke-virtual {v4, v7}, LX/0Oi;->c(I)I

    move-result v3

    move v10, v3

    move v3, v0

    move v0, v10

    .line 53364
    :goto_2
    sget-object v4, LX/0OY;->c:[I

    aget v0, v4, v0

    .line 53365
    const/4 v4, -0x1

    if-eq v0, v4, :cond_5

    :goto_3
    invoke-static {v1}, LX/0Av;->a(Z)V

    .line 53366
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 53367
    :cond_1
    if-ge v3, v9, :cond_2

    move v0, v1

    :goto_4
    invoke-static {v0}, LX/0Av;->a(Z)V

    .line 53368
    sget-object v0, LX/0OY;->b:[I

    aget v0, v0, v3

    goto :goto_0

    :cond_2
    move v0, v2

    .line 53369
    goto :goto_4

    .line 53370
    :cond_3
    if-ge v5, v9, :cond_4

    move v0, v1

    :goto_5
    invoke-static {v0}, LX/0Av;->a(Z)V

    .line 53371
    sget-object v0, LX/0OY;->b:[I

    aget v0, v0, v5

    goto :goto_1

    :cond_4
    move v0, v2

    .line 53372
    goto :goto_5

    :cond_5
    move v1, v2

    .line 53373
    goto :goto_3

    :cond_6
    move v10, v3

    move v3, v0

    move v0, v10

    goto :goto_2
.end method
