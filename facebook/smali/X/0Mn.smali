.class public final LX/0Mn;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48182
    const/16 v0, 0x18

    new-array v0, v0, [I

    const/4 v1, 0x0

    const-string v2, "isom"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "iso2"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "iso3"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "iso4"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "iso5"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "iso6"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "avc1"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "hvc1"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "hev1"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "mp41"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "mp42"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "3g2a"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "3g2b"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "3gr6"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "3gs6"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "3ge6"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "3gg6"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "M4V "

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "M4A "

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "f4v "

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "kddi"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "M4VP"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "qt  "

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "MSNV"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    sput-object v0, LX/0Mn;->a:[I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 48133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48134
    return-void
.end method

.method private static a(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 48176
    ushr-int/lit8 v2, p0, 0x8

    const-string v3, "3gp"

    invoke-static {v3}, LX/08x;->e(Ljava/lang/String;)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 48177
    :cond_0
    :goto_0
    return v0

    .line 48178
    :cond_1
    sget-object v3, LX/0Mn;->a:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget v5, v3, v2

    .line 48179
    if-eq v5, p0, :cond_0

    .line 48180
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 48181
    goto :goto_0
.end method

.method public static a(LX/0MA;IZ)Z
    .locals 18

    .prologue
    .line 48135
    invoke-interface/range {p0 .. p0}, LX/0MA;->d()J

    move-result-wide v2

    .line 48136
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    move/from16 v0, p1

    int-to-long v4, v0

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    :cond_0
    move/from16 v0, p1

    int-to-long v2, v0

    :cond_1
    long-to-int v9, v2

    .line 48137
    new-instance v10, LX/0Oj;

    const/16 v2, 0x40

    invoke-direct {v10, v2}, LX/0Oj;-><init>(I)V

    .line 48138
    const/4 v4, 0x0

    .line 48139
    const/4 v2, 0x0

    .line 48140
    const/4 v3, 0x0

    move v8, v4

    .line 48141
    :goto_0
    if-ge v8, v9, :cond_7

    .line 48142
    const/16 v6, 0x8

    .line 48143
    iget-object v4, v10, LX/0Oj;->a:[B

    const/4 v5, 0x0

    const/16 v7, 0x8

    move-object/from16 v0, p0

    invoke-interface {v0, v4, v5, v7}, LX/0MA;->c([BII)V

    .line 48144
    const/4 v4, 0x0

    invoke-virtual {v10, v4}, LX/0Oj;->b(I)V

    .line 48145
    invoke-virtual {v10}, LX/0Oj;->k()J

    move-result-wide v4

    .line 48146
    invoke-virtual {v10}, LX/0Oj;->m()I

    move-result v11

    .line 48147
    const-wide/16 v12, 0x1

    cmp-long v7, v4, v12

    if-nez v7, :cond_b

    .line 48148
    iget-object v4, v10, LX/0Oj;->a:[B

    const/16 v5, 0x8

    const/16 v6, 0x8

    move-object/from16 v0, p0

    invoke-interface {v0, v4, v5, v6}, LX/0MA;->c([BII)V

    .line 48149
    const/16 v6, 0x10

    .line 48150
    invoke-virtual {v10}, LX/0Oj;->o()J

    move-result-wide v4

    move-wide/from16 v16, v4

    move v4, v6

    move-wide/from16 v6, v16

    .line 48151
    :goto_1
    int-to-long v12, v4

    cmp-long v5, v6, v12

    if-gez v5, :cond_2

    .line 48152
    const/4 v2, 0x0

    .line 48153
    :goto_2
    return v2

    .line 48154
    :cond_2
    long-to-int v5, v6

    sub-int v4, v5, v4

    .line 48155
    sget v5, LX/0Mc;->a:I

    if-ne v11, v5, :cond_6

    .line 48156
    const/16 v5, 0x8

    if-ge v4, v5, :cond_3

    .line 48157
    const/4 v2, 0x0

    goto :goto_2

    .line 48158
    :cond_3
    add-int/lit8 v4, v4, -0x8

    div-int/lit8 v5, v4, 0x4

    .line 48159
    iget-object v4, v10, LX/0Oj;->a:[B

    const/4 v11, 0x0

    add-int/lit8 v12, v5, 0x2

    mul-int/lit8 v12, v12, 0x4

    move-object/from16 v0, p0

    invoke-interface {v0, v4, v11, v12}, LX/0MA;->c([BII)V

    .line 48160
    const/4 v4, 0x0

    :goto_3
    add-int/lit8 v11, v5, 0x2

    if-ge v4, v11, :cond_4

    .line 48161
    const/4 v11, 0x1

    if-eq v4, v11, :cond_5

    .line 48162
    invoke-virtual {v10}, LX/0Oj;->m()I

    move-result v11

    invoke-static {v11}, LX/0Mn;->a(I)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 48163
    const/4 v2, 0x1

    .line 48164
    :cond_4
    if-nez v2, :cond_9

    .line 48165
    const/4 v2, 0x0

    goto :goto_2

    .line 48166
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 48167
    :cond_6
    sget v5, LX/0Mc;->H:I

    if-ne v11, v5, :cond_8

    .line 48168
    const/4 v3, 0x1

    .line 48169
    :cond_7
    if-eqz v2, :cond_a

    move/from16 v0, p2

    if-ne v0, v3, :cond_a

    const/4 v2, 0x1

    goto :goto_2

    .line 48170
    :cond_8
    if-eqz v4, :cond_9

    .line 48171
    int-to-long v12, v8

    add-long/2addr v12, v6

    int-to-long v14, v9

    cmp-long v5, v12, v14

    if-gez v5, :cond_7

    .line 48172
    move-object/from16 v0, p0

    invoke-interface {v0, v4}, LX/0MA;->c(I)V

    .line 48173
    :cond_9
    int-to-long v4, v8

    add-long/2addr v4, v6

    long-to-int v4, v4

    move v8, v4

    .line 48174
    goto/16 :goto_0

    .line 48175
    :cond_a
    const/4 v2, 0x0

    goto :goto_2

    :cond_b
    move-wide/from16 v16, v4

    move v4, v6

    move-wide/from16 v6, v16

    goto :goto_1
.end method
