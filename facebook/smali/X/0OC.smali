.class public final LX/0OC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/04m;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:LX/0O4;

.field private final c:LX/0OX;

.field private final d:LX/08I;

.field private e:J

.field private f:J

.field private g:J

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52630
    invoke-direct {p0, v0, v0}, LX/0OC;-><init>(Landroid/os/Handler;LX/0O4;)V

    .line 52631
    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;LX/0O4;)V
    .locals 1

    .prologue
    .line 52628
    new-instance v0, LX/08J;

    invoke-direct {v0}, LX/08J;-><init>()V

    invoke-direct {p0, p1, p2, v0}, LX/0OC;-><init>(Landroid/os/Handler;LX/0O4;LX/0OX;)V

    .line 52629
    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;LX/0O4;LX/0OX;)V
    .locals 1

    .prologue
    .line 52626
    const/16 v0, 0x7d0

    invoke-direct {p0, p1, p2, p3, v0}, LX/0OC;-><init>(Landroid/os/Handler;LX/0O4;LX/0OX;I)V

    .line 52627
    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;LX/0O4;LX/0OX;I)V
    .locals 2

    .prologue
    .line 52619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52620
    iput-object p1, p0, LX/0OC;->a:Landroid/os/Handler;

    .line 52621
    iput-object p2, p0, LX/0OC;->b:LX/0O4;

    .line 52622
    iput-object p3, p0, LX/0OC;->c:LX/0OX;

    .line 52623
    new-instance v0, LX/08I;

    invoke-direct {v0, p4}, LX/08I;-><init>(I)V

    iput-object v0, p0, LX/0OC;->d:LX/08I;

    .line 52624
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0OC;->g:J

    .line 52625
    return-void
.end method

.method private a(IJJ)V
    .locals 8

    .prologue
    .line 52616
    iget-object v0, p0, LX/0OC;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0OC;->b:LX/0O4;

    if-eqz v0, :cond_0

    .line 52617
    iget-object v0, p0, LX/0OC;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter$1;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter$1;-><init>(LX/0OC;IJJ)V

    const v2, 0x51b8ac04

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 52618
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()J
    .locals 2

    .prologue
    .line 52615
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/0OC;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 4

    .prologue
    .line 52590
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/0OC;->e:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0OC;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52591
    monitor-exit p0

    return-void

    .line 52592
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 52610
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0OC;->h:I

    if-nez v0, :cond_0

    .line 52611
    iget-object v0, p0, LX/0OC;->c:LX/0OX;

    invoke-interface {v0}, LX/0OX;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0OC;->f:J

    .line 52612
    :cond_0
    iget v0, p0, LX/0OC;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0OC;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52613
    monitor-exit p0

    return-void

    .line 52614
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 8

    .prologue
    .line 52593
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0OC;->h:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 52594
    iget-object v0, p0, LX/0OC;->c:LX/0OX;

    invoke-interface {v0}, LX/0OX;->a()J

    move-result-wide v6

    .line 52595
    iget-wide v0, p0, LX/0OC;->f:J

    sub-long v0, v6, v0

    long-to-int v1, v0

    .line 52596
    if-lez v1, :cond_0

    .line 52597
    iget-wide v2, p0, LX/0OC;->e:J

    const-wide/16 v4, 0x1f40

    mul-long/2addr v2, v4

    int-to-long v4, v1

    div-long/2addr v2, v4

    long-to-float v0, v2

    .line 52598
    iget-object v2, p0, LX/0OC;->d:LX/08I;

    iget-wide v4, p0, LX/0OC;->e:J

    long-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-virtual {v2, v3, v0}, LX/08I;->a(IF)V

    .line 52599
    iget-object v0, p0, LX/0OC;->d:LX/08I;

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v2}, LX/08I;->a(F)F

    move-result v0

    .line 52600
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_3

    const-wide/16 v2, -0x1

    :goto_1
    iput-wide v2, p0, LX/0OC;->g:J

    .line 52601
    iget-wide v2, p0, LX/0OC;->e:J

    iget-wide v4, p0, LX/0OC;->g:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/0OC;->a(IJJ)V

    .line 52602
    :cond_0
    iget v0, p0, LX/0OC;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0OC;->h:I

    .line 52603
    iget v0, p0, LX/0OC;->h:I

    if-lez v0, :cond_1

    .line 52604
    iput-wide v6, p0, LX/0OC;->f:J

    .line 52605
    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0OC;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52606
    monitor-exit p0

    return-void

    .line 52607
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 52608
    :cond_3
    float-to-long v2, v0

    goto :goto_1

    .line 52609
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
