.class public LX/038;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public a:J

.field public final b:Ljava/io/File;

.field public final c:I

.field private final d:LX/02o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/09e;

.field private f:S
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/io/File;LX/09e;ILX/02o;)V
    .locals 1
    .param p6    # LX/02o;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 9829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9830
    iput-wide p1, p0, LX/038;->a:J

    .line 9831
    iput-object p3, p0, LX/038;->b:Ljava/io/File;

    .line 9832
    iput-object p4, p0, LX/038;->e:LX/09e;

    .line 9833
    iput p5, p0, LX/038;->c:I

    .line 9834
    iput-object p6, p0, LX/038;->d:LX/02o;

    .line 9835
    const/4 v0, 0x2

    iput-short v0, p0, LX/038;->f:S

    .line 9836
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(S)V
    .locals 1

    .prologue
    .line 9837
    monitor-enter p0

    :try_start_0
    iput-short p1, p0, LX/038;->f:S
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9838
    monitor-exit p0

    return-void

    .line 9839
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 9840
    iget-object v0, p0, LX/038;->e:LX/09e;

    move-object v0, v0

    .line 9841
    invoke-static {v0}, LX/09f;->a(Ljava/io/Closeable;)V

    .line 9842
    iget-object v0, p0, LX/038;->d:LX/02o;

    if-eqz v0, :cond_0

    .line 9843
    iget-object v0, p0, LX/038;->d:LX/02o;

    invoke-virtual {v0, p0}, LX/02o;->a(LX/038;)V

    .line 9844
    :cond_0
    return-void
.end method

.method public final declared-synchronized e()Z
    .locals 2

    .prologue
    .line 9845
    monitor-enter p0

    :try_start_0
    iget-short v0, p0, LX/038;->f:S
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()S
    .locals 1

    .prologue
    .line 9846
    monitor-enter p0

    :try_start_0
    iget-short v0, p0, LX/038;->f:S
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
