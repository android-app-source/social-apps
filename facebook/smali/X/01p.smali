.class public LX/01p;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/01q;

.field public static final b:LX/01q;

.field public static final c:LX/01q;

.field public static final d:LX/01q;

.field public static final e:LX/01q;

.field public static final f:LX/01q;

.field public static final g:LX/01q;

.field public static final h:LX/01q;

.field public static final i:LX/01q;

.field public static final j:LX/01q;

.field public static final k:LX/01q;

.field public static final l:LX/01q;

.field public static final m:LX/01q;

.field public static final n:LX/01q;

.field public static final o:LX/01q;

.field public static final p:LX/01q;

.field public static final q:LX/01q;

.field public static volatile r:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 5540
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.addresses"

    invoke-direct {v0, v1, v3}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->a:LX/01q;

    .line 5541
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.analytics"

    invoke-direct {v0, v1, v2}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->b:LX/01q;

    .line 5542
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.fbns_notification_store"

    invoke-direct {v0, v1, v2}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->c:LX/01q;

    .line 5543
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.shared_ids"

    invoke-direct {v0, v1, v2}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->d:LX/01q;

    .line 5544
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.fbns_state"

    invoke-direct {v0, v1, v2}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->e:LX/01q;

    .line 5545
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.flags"

    invoke-direct {v0, v1, v2}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->f:LX/01q;

    .line 5546
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.ids"

    invoke-direct {v0, v1, v2}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->g:LX/01q;

    .line 5547
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.mqtt_config"

    invoke-direct {v0, v1, v2}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->h:LX/01q;

    .line 5548
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.oxygen_fbns_config"

    invoke-direct {v0, v1, v3}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->i:LX/01q;

    .line 5549
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.registrations"

    invoke-direct {v0, v1, v2}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->j:LX/01q;

    .line 5550
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.retry"

    invoke-direct {v0, v1, v3}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->k:LX/01q;

    .line 5551
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.stats"

    invoke-direct {v0, v1, v2}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->l:LX/01q;

    .line 5552
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.gk"

    invoke-direct {v0, v1, v3}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->m:LX/01q;

    .line 5553
    new-instance v0, LX/01q;

    const-string v1, "rti.mqtt.snapshot"

    invoke-direct {v0, v1, v3}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->n:LX/01q;

    .line 5554
    new-instance v0, LX/01q;

    const-string v1, "mqtt_radio_active_time"

    invoke-direct {v0, v1, v3}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->o:LX/01q;

    .line 5555
    new-instance v0, LX/01q;

    const-string v1, "token_store"

    invoke-direct {v0, v1, v3}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->p:LX/01q;

    .line 5556
    new-instance v0, LX/01q;

    const-string v1, "mqtt_debug"

    invoke-direct {v0, v1, v3}, LX/01q;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/01p;->q:LX/01q;

    .line 5557
    sput-boolean v2, LX/01p;->r:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5559
    return-void
.end method

.method public static a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 5560
    sget-object v0, LX/01r;->a:LX/01r;

    iget-object v1, p1, LX/01q;->a:Ljava/lang/String;

    .line 5561
    sget-boolean v2, LX/01p;->r:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p1, LX/01q;->b:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 5562
    invoke-virtual {v0, p0, v1, v2}, LX/01r;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
