.class public LX/0AF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 23612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23613
    iput-object p1, p0, LX/0AF;->a:Ljava/lang/String;

    .line 23614
    iput p2, p0, LX/0AF;->b:I

    .line 23615
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 23616
    if-ne p0, p1, :cond_1

    .line 23617
    :cond_0
    :goto_0
    return v0

    .line 23618
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 23619
    goto :goto_0

    .line 23620
    :cond_3
    check-cast p1, LX/0AF;

    .line 23621
    iget-object v2, p0, LX/0AF;->a:Ljava/lang/String;

    iget-object v3, p1, LX/0AF;->a:Ljava/lang/String;

    if-eq v2, v3, :cond_4

    iget-object v2, p0, LX/0AF;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/0AF;->a:Ljava/lang/String;

    iget-object v3, p1, LX/0AF;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    iget v2, p0, LX/0AF;->b:I

    iget v3, p1, LX/0AF;->b:I

    if-eq v2, v3, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 23622
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/0AF;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, LX/0AF;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 23623
    const-string v0, "{ name=\'%s\', qos=\'%s\'}"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/0AF;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, LX/0AF;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
