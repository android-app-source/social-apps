.class public LX/0Bx;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static volatile a:Z

.field private static final b:Ljava/util/concurrent/locks/ReadWriteLock;

.field public static final c:LX/0By;

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Bw;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "sReadWriteLock"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27007
    const/4 v0, 0x0

    sput-boolean v0, LX/0Bx;->a:Z

    .line 27008
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    sput-object v0, LX/0Bx;->b:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 27009
    new-instance v0, LX/0By;

    invoke-direct {v0}, LX/0By;-><init>()V

    sput-object v0, LX/0Bx;->c:LX/0By;

    .line 27010
    new-instance v0, LX/0Bv;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/0Bv;-><init>(I)V

    sput-object v0, LX/0Bx;->d:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27012
    return-void
.end method

.method public static a(I)V
    .locals 4

    .prologue
    .line 27013
    sget-object v0, LX/0Bx;->b:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    monitor-enter v2

    .line 27014
    const/4 v0, 0x0

    :try_start_0
    sget-object v1, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 27015
    sget-object v0, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Bw;

    invoke-interface {v0, p0}, LX/0Bw;->a(I)V

    .line 27016
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 27017
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(LX/0Bw;)V
    .locals 2

    .prologue
    .line 27018
    sget-object v0, LX/0Bx;->b:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    monitor-enter v1

    .line 27019
    :try_start_0
    sget-object v0, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27020
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/hardware/Camera;)V
    .locals 1

    .prologue
    .line 27000
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, LX/0Bx;->a(I)V

    .line 27001
    return-void
.end method

.method public static b(I)V
    .locals 4

    .prologue
    .line 27002
    sget-object v0, LX/0Bx;->b:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    monitor-enter v2

    .line 27003
    const/4 v0, 0x0

    :try_start_0
    sget-object v1, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 27004
    sget-object v0, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Bw;

    invoke-interface {v0, p0}, LX/0Bw;->b(I)V

    .line 27005
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 27006
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(LX/0Bw;)V
    .locals 2

    .prologue
    .line 26997
    sget-object v0, LX/0Bx;->b:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    monitor-enter v1

    .line 26998
    :try_start_0
    sget-object v0, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 26999
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c(I)V
    .locals 4

    .prologue
    .line 26992
    sget-object v0, LX/0Bx;->b:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    monitor-enter v2

    .line 26993
    const/4 v0, 0x0

    :try_start_0
    sget-object v1, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 26994
    sget-object v0, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Bw;

    invoke-interface {v0, p0}, LX/0Bw;->c(I)V

    .line 26995
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 26996
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c(Landroid/hardware/Camera;)V
    .locals 5

    .prologue
    .line 26986
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    .line 26987
    sget-object v0, LX/0Bx;->b:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    monitor-enter v3

    .line 26988
    const/4 v0, 0x0

    :try_start_0
    sget-object v1, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 26989
    sget-object v0, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Bw;

    invoke-interface {v0, v2}, LX/0Bw;->c(I)V

    .line 26990
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 26991
    :cond_0
    monitor-exit v3

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 26985
    sget-boolean v0, LX/0Bx;->a:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(I)V
    .locals 4

    .prologue
    .line 26980
    sget-object v0, LX/0Bx;->b:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    monitor-enter v2

    .line 26981
    const/4 v0, 0x0

    :try_start_0
    sget-object v1, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 26982
    sget-object v0, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Bw;

    invoke-interface {v0, p0}, LX/0Bw;->d(I)V

    .line 26983
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 26984
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static d(Landroid/hardware/Camera;)V
    .locals 5

    .prologue
    .line 26974
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    .line 26975
    sget-object v0, LX/0Bx;->b:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    monitor-enter v3

    .line 26976
    const/4 v0, 0x0

    :try_start_0
    sget-object v1, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 26977
    sget-object v0, LX/0Bx;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Bw;

    invoke-interface {v0, v2}, LX/0Bw;->d(I)V

    .line 26978
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 26979
    :cond_0
    monitor-exit v3

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
