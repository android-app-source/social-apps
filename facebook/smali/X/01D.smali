.class public final LX/01D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Ljava/io/FileOutputStream;

.field private final b:Ljava/nio/channels/FileLock;


# direct methods
.method private constructor <init>(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 3903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3904
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/01D;->a:Ljava/io/FileOutputStream;

    .line 3905
    :try_start_0
    iget-object v0, p0, LX/01D;->a:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3906
    if-nez v0, :cond_0

    .line 3907
    iget-object v1, p0, LX/01D;->a:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 3908
    :cond_0
    iput-object v0, p0, LX/01D;->b:Ljava/nio/channels/FileLock;

    .line 3909
    return-void

    .line 3910
    :catchall_0
    move-exception v0

    .line 3911
    iget-object v1, p0, LX/01D;->a:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
.end method

.method public static a(Ljava/io/File;)LX/01D;
    .locals 1

    .prologue
    .line 3902
    new-instance v0, LX/01D;

    invoke-direct {v0, p0}, LX/01D;-><init>(Ljava/io/File;)V

    return-object v0
.end method


# virtual methods
.method public final close()V
    .locals 2

    .prologue
    .line 3898
    :try_start_0
    iget-object v0, p0, LX/01D;->b:Ljava/nio/channels/FileLock;

    invoke-virtual {v0}, Ljava/nio/channels/FileLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3899
    iget-object v0, p0, LX/01D;->a:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 3900
    return-void

    .line 3901
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/01D;->a:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
.end method
