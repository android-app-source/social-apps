.class public final LX/016;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:[I

.field public static volatile c:Ljava/lang/reflect/Method;

.field public static volatile d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3742
    const-class v0, LX/016;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/016;->a:Ljava/lang/String;

    .line 3743
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/16 v1, 0x1000

    aput v1, v0, v2

    sput-object v0, LX/016;->b:[I

    .line 3744
    const/4 v0, 0x0

    sput-object v0, LX/016;->c:Ljava/lang/reflect/Method;

    .line 3745
    sput-boolean v2, LX/016;->d:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 3746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3747
    return-void
.end method

.method public static a(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z
    .locals 8

    .prologue
    const/4 v5, 0x6

    const/4 v1, 0x0

    .line 3748
    sget-boolean v0, LX/016;->d:Z

    if-nez v0, :cond_0

    .line 3749
    const/4 v7, 0x5

    .line 3750
    :try_start_0
    const-class v0, Landroid/os/Process;

    const-string v2, "readProcFile"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v3, v4

    const/4 v4, 0x1

    const-class v6, [I

    aput-object v6, v3, v4

    const/4 v4, 0x2

    const-class v6, [Ljava/lang/String;

    aput-object v6, v3, v4

    const/4 v4, 0x3

    const-class v6, [J

    aput-object v6, v3, v4

    const/4 v4, 0x4

    const-class v6, [F

    aput-object v6, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 3751
    :goto_0
    move-object v0, v0

    .line 3752
    sput-object v0, LX/016;->c:Ljava/lang/reflect/Method;

    .line 3753
    const/4 v0, 0x1

    sput-boolean v0, LX/016;->d:Z

    .line 3754
    :cond_0
    sget-object v0, LX/016;->c:Ljava/lang/reflect/Method;

    move-object v0, v0

    .line 3755
    if-nez v0, :cond_1

    move v0, v1

    .line 3756
    :goto_1
    return v0

    .line 3757
    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x5

    :try_start_1
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    const/4 v4, 0x2

    aput-object p2, v3, v4

    const/4 v4, 0x3

    aput-object p3, v3, v4

    const/4 v4, 0x4

    aput-object p4, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 3758
    if-nez v0, :cond_2

    move v0, v1

    .line 3759
    goto :goto_1

    .line 3760
    :cond_2
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    goto :goto_1

    .line 3761
    :catch_0
    move-exception v0

    .line 3762
    sget-object v2, LX/016;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3763
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error (IllegalAccessException - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") while accessing proc file - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 3764
    sget-object v3, LX/016;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    :goto_2
    move v0, v1

    .line 3765
    goto :goto_1

    .line 3766
    :catch_1
    move-exception v0

    .line 3767
    sget-object v2, LX/016;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3768
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error (InvocationTargetException - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") while accessing proc file - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 3769
    sget-object v3, LX/016;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 3770
    :catch_2
    move-exception v0

    .line 3771
    sget-object v2, LX/016;->a:Ljava/lang/String;

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3772
    sget-object v2, LX/016;->a:Ljava/lang/String;

    const-string v3, "Warning! Could not get access to JNI method - readProcFile"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3773
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
