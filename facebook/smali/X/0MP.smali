.class public final LX/0MP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0O1;

.field public final b:I

.field public final c:LX/0MN;

.field public final d:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "LX/0O0;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0MO;

.field private final f:LX/0Oj;

.field public g:J

.field public h:J

.field public i:LX/0O0;

.field public j:I


# direct methods
.method public constructor <init>(LX/0O1;)V
    .locals 2

    .prologue
    .line 45947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45948
    iput-object p1, p0, LX/0MP;->a:LX/0O1;

    .line 45949
    invoke-interface {p1}, LX/0O1;->c()I

    move-result v0

    iput v0, p0, LX/0MP;->b:I

    .line 45950
    new-instance v0, LX/0MN;

    invoke-direct {v0}, LX/0MN;-><init>()V

    iput-object v0, p0, LX/0MP;->c:LX/0MN;

    .line 45951
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, LX/0MP;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 45952
    new-instance v0, LX/0MO;

    invoke-direct {v0}, LX/0MO;-><init>()V

    iput-object v0, p0, LX/0MP;->e:LX/0MO;

    .line 45953
    new-instance v0, LX/0Oj;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, LX/0MP;->f:LX/0Oj;

    .line 45954
    iget v0, p0, LX/0MP;->b:I

    iput v0, p0, LX/0MP;->j:I

    .line 45955
    return-void
.end method

.method public static a(LX/0MP;JLjava/nio/ByteBuffer;I)V
    .locals 5

    .prologue
    .line 45937
    :goto_0
    if-lez p4, :cond_0

    .line 45938
    invoke-static {p0, p1, p2}, LX/0MP;->c(LX/0MP;J)V

    .line 45939
    iget-wide v0, p0, LX/0MP;->g:J

    sub-long v0, p1, v0

    long-to-int v1, v0

    .line 45940
    iget v0, p0, LX/0MP;->b:I

    sub-int/2addr v0, v1

    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 45941
    iget-object v0, p0, LX/0MP;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0O0;

    .line 45942
    iget-object v3, v0, LX/0O0;->a:[B

    invoke-virtual {v0, v1}, LX/0O0;->a(I)I

    move-result v0

    invoke-virtual {p3, v3, v0, v2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 45943
    int-to-long v0, v2

    add-long/2addr p1, v0

    .line 45944
    sub-int/2addr p4, v2

    .line 45945
    goto :goto_0

    .line 45946
    :cond_0
    return-void
.end method

.method private static a(LX/0MP;J[BI)V
    .locals 7

    .prologue
    .line 45926
    const/4 v0, 0x0

    move v1, v0

    .line 45927
    :goto_0
    if-ge v1, p4, :cond_0

    .line 45928
    invoke-static {p0, p1, p2}, LX/0MP;->c(LX/0MP;J)V

    .line 45929
    iget-wide v2, p0, LX/0MP;->g:J

    sub-long v2, p1, v2

    long-to-int v2, v2

    .line 45930
    sub-int v0, p4, v1

    iget v3, p0, LX/0MP;->b:I

    sub-int/2addr v3, v2

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 45931
    iget-object v0, p0, LX/0MP;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0O0;

    .line 45932
    iget-object v4, v0, LX/0O0;->a:[B

    invoke-virtual {v0, v2}, LX/0O0;->a(I)I

    move-result v0

    invoke-static {v4, v0, p3, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45933
    int-to-long v4, v3

    add-long/2addr p1, v4

    .line 45934
    add-int v0, v1, v3

    move v1, v0

    .line 45935
    goto :goto_0

    .line 45936
    :cond_0
    return-void
.end method

.method public static a(LX/0MP;LX/0L7;LX/0MO;)V
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 45883
    iget-wide v0, p2, LX/0MO;->a:J

    .line 45884
    iget-object v2, p0, LX/0MP;->f:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    invoke-static {p0, v0, v1, v2, v6}, LX/0MP;->a(LX/0MP;J[BI)V

    .line 45885
    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    .line 45886
    iget-object v0, p0, LX/0MP;->f:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    aget-byte v1, v0, v4

    .line 45887
    and-int/lit16 v0, v1, 0x80

    if-eqz v0, :cond_6

    move v0, v6

    .line 45888
    :goto_0
    and-int/lit8 v1, v1, 0x7f

    .line 45889
    iget-object v5, p1, LX/0L7;->a:LX/0Kq;

    iget-object v5, v5, LX/0Kq;->a:[B

    if-nez v5, :cond_0

    .line 45890
    iget-object v5, p1, LX/0L7;->a:LX/0Kq;

    const/16 v7, 0x10

    new-array v7, v7, [B

    iput-object v7, v5, LX/0Kq;->a:[B

    .line 45891
    :cond_0
    iget-object v5, p1, LX/0L7;->a:LX/0Kq;

    iget-object v5, v5, LX/0Kq;->a:[B

    invoke-static {p0, v2, v3, v5, v1}, LX/0MP;->a(LX/0MP;J[BI)V

    .line 45892
    int-to-long v8, v1

    add-long/2addr v2, v8

    .line 45893
    if-eqz v0, :cond_7

    .line 45894
    iget-object v1, p0, LX/0MP;->f:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    const/4 v5, 0x2

    invoke-static {p0, v2, v3, v1, v5}, LX/0MP;->a(LX/0MP;J[BI)V

    .line 45895
    const-wide/16 v8, 0x2

    add-long/2addr v2, v8

    .line 45896
    iget-object v1, p0, LX/0MP;->f:LX/0Oj;

    invoke-virtual {v1, v4}, LX/0Oj;->b(I)V

    .line 45897
    iget-object v1, p0, LX/0MP;->f:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->g()I

    move-result v1

    move-wide v8, v2

    .line 45898
    :goto_1
    iget-object v2, p1, LX/0L7;->a:LX/0Kq;

    iget-object v2, v2, LX/0Kq;->d:[I

    .line 45899
    if-eqz v2, :cond_1

    array-length v3, v2

    if-ge v3, v1, :cond_2

    .line 45900
    :cond_1
    new-array v2, v1, [I

    .line 45901
    :cond_2
    iget-object v3, p1, LX/0L7;->a:LX/0Kq;

    iget-object v3, v3, LX/0Kq;->e:[I

    .line 45902
    if-eqz v3, :cond_3

    array-length v5, v3

    if-ge v5, v1, :cond_4

    .line 45903
    :cond_3
    new-array v3, v1, [I

    .line 45904
    :cond_4
    if-eqz v0, :cond_8

    .line 45905
    mul-int/lit8 v0, v1, 0x6

    .line 45906
    iget-object v5, p0, LX/0MP;->f:LX/0Oj;

    .line 45907
    iget v7, v5, LX/0Oj;->c:I

    move v7, v7

    .line 45908
    if-ge v7, v0, :cond_5

    .line 45909
    new-array v7, v0, [B

    invoke-virtual {v5, v7, v0}, LX/0Oj;->a([BI)V

    .line 45910
    :cond_5
    iget-object v5, p0, LX/0MP;->f:LX/0Oj;

    iget-object v5, v5, LX/0Oj;->a:[B

    invoke-static {p0, v8, v9, v5, v0}, LX/0MP;->a(LX/0MP;J[BI)V

    .line 45911
    int-to-long v10, v0

    add-long/2addr v8, v10

    .line 45912
    iget-object v0, p0, LX/0MP;->f:LX/0Oj;

    invoke-virtual {v0, v4}, LX/0Oj;->b(I)V

    .line 45913
    :goto_2
    if-ge v4, v1, :cond_9

    .line 45914
    iget-object v0, p0, LX/0MP;->f:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->g()I

    move-result v0

    aput v0, v2, v4

    .line 45915
    iget-object v0, p0, LX/0MP;->f:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->s()I

    move-result v0

    aput v0, v3, v4

    .line 45916
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_6
    move v0, v4

    .line 45917
    goto :goto_0

    :cond_7
    move v1, v6

    move-wide v8, v2

    .line 45918
    goto :goto_1

    .line 45919
    :cond_8
    aput v4, v2, v4

    .line 45920
    iget v0, p1, LX/0L7;->c:I

    iget-wide v10, p2, LX/0MO;->a:J

    sub-long v10, v8, v10

    long-to-int v5, v10

    sub-int/2addr v0, v5

    aput v0, v3, v4

    .line 45921
    :cond_9
    iget-object v0, p1, LX/0L7;->a:LX/0Kq;

    iget-object v4, p2, LX/0MO;->b:[B

    iget-object v5, p1, LX/0L7;->a:LX/0Kq;

    iget-object v5, v5, LX/0Kq;->a:[B

    invoke-virtual/range {v0 .. v6}, LX/0Kq;->a(I[I[I[B[BI)V

    .line 45922
    iget-wide v0, p2, LX/0MO;->a:J

    sub-long v0, v8, v0

    long-to-int v0, v0

    .line 45923
    iget-wide v2, p2, LX/0MO;->a:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p2, LX/0MO;->a:J

    .line 45924
    iget v1, p1, LX/0L7;->c:I

    sub-int v0, v1, v0

    iput v0, p1, LX/0L7;->c:I

    .line 45925
    return-void
.end method

.method private static b(LX/0MP;I)I
    .locals 2

    .prologue
    .line 45878
    iget v0, p0, LX/0MP;->j:I

    iget v1, p0, LX/0MP;->b:I

    if-ne v0, v1, :cond_0

    .line 45879
    const/4 v0, 0x0

    iput v0, p0, LX/0MP;->j:I

    .line 45880
    iget-object v0, p0, LX/0MP;->a:LX/0O1;

    invoke-interface {v0}, LX/0O1;->a()LX/0O0;

    move-result-object v0

    iput-object v0, p0, LX/0MP;->i:LX/0O0;

    .line 45881
    iget-object v0, p0, LX/0MP;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    iget-object v1, p0, LX/0MP;->i:LX/0O0;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    .line 45882
    :cond_0
    iget v0, p0, LX/0MP;->b:I

    iget v1, p0, LX/0MP;->j:I

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public static c(LX/0MP;J)V
    .locals 9

    .prologue
    .line 45871
    iget-wide v0, p0, LX/0MP;->g:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    .line 45872
    iget v1, p0, LX/0MP;->b:I

    div-int v2, v0, v1

    .line 45873
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 45874
    iget-object v3, p0, LX/0MP;->a:LX/0O1;

    iget-object v0, p0, LX/0MP;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0O0;

    invoke-interface {v3, v0}, LX/0O1;->a(LX/0O0;)V

    .line 45875
    iget-wide v4, p0, LX/0MP;->g:J

    iget v0, p0, LX/0MP;->b:I

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/0MP;->g:J

    .line 45876
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 45877
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0G6;IZ)I
    .locals 6

    .prologue
    const/4 v0, -0x1

    .line 45862
    invoke-static {p0, p2}, LX/0MP;->b(LX/0MP;I)I

    move-result v1

    .line 45863
    iget-object v2, p0, LX/0MP;->i:LX/0O0;

    iget-object v2, v2, LX/0O0;->a:[B

    iget-object v3, p0, LX/0MP;->i:LX/0O0;

    iget v4, p0, LX/0MP;->j:I

    invoke-virtual {v3, v4}, LX/0O0;->a(I)I

    move-result v3

    invoke-interface {p1, v2, v3, v1}, LX/0G6;->a([BII)I

    move-result v1

    .line 45864
    if-ne v1, v0, :cond_1

    .line 45865
    if-eqz p3, :cond_0

    .line 45866
    :goto_0
    return v0

    .line 45867
    :cond_0
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 45868
    :cond_1
    iget v0, p0, LX/0MP;->j:I

    add-int/2addr v0, v1

    iput v0, p0, LX/0MP;->j:I

    .line 45869
    iget-wide v2, p0, LX/0MP;->h:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0MP;->h:J

    move v0, v1

    .line 45870
    goto :goto_0
.end method

.method public final a(LX/0MA;IZ)I
    .locals 6

    .prologue
    const/4 v0, -0x1

    .line 45838
    invoke-static {p0, p2}, LX/0MP;->b(LX/0MP;I)I

    move-result v1

    .line 45839
    iget-object v2, p0, LX/0MP;->i:LX/0O0;

    iget-object v2, v2, LX/0O0;->a:[B

    iget-object v3, p0, LX/0MP;->i:LX/0O0;

    iget v4, p0, LX/0MP;->j:I

    invoke-virtual {v3, v4}, LX/0O0;->a(I)I

    move-result v3

    invoke-interface {p1, v2, v3, v1}, LX/0MA;->a([BII)I

    move-result v1

    .line 45840
    if-ne v1, v0, :cond_1

    .line 45841
    if-eqz p3, :cond_0

    .line 45842
    :goto_0
    return v0

    .line 45843
    :cond_0
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 45844
    :cond_1
    iget v0, p0, LX/0MP;->j:I

    add-int/2addr v0, v1

    iput v0, p0, LX/0MP;->j:I

    .line 45845
    iget-wide v2, p0, LX/0MP;->h:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0MP;->h:J

    move v0, v1

    .line 45846
    goto :goto_0
.end method

.method public final a(JIJI[B)V
    .locals 8

    .prologue
    .line 45860
    iget-object v0, p0, LX/0MP;->c:LX/0MN;

    move-wide v1, p1

    move v3, p3

    move-wide v4, p4

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, LX/0MN;->a(JIJI[B)V

    .line 45861
    return-void
.end method

.method public final a(LX/0Oj;I)V
    .locals 6

    .prologue
    .line 45852
    :goto_0
    if-lez p2, :cond_0

    .line 45853
    invoke-static {p0, p2}, LX/0MP;->b(LX/0MP;I)I

    move-result v0

    .line 45854
    iget-object v1, p0, LX/0MP;->i:LX/0O0;

    iget-object v1, v1, LX/0O0;->a:[B

    iget-object v2, p0, LX/0MP;->i:LX/0O0;

    iget v3, p0, LX/0MP;->j:I

    invoke-virtual {v2, v3}, LX/0O0;->a(I)I

    move-result v2

    invoke-virtual {p1, v1, v2, v0}, LX/0Oj;->a([BII)V

    .line 45855
    iget v1, p0, LX/0MP;->j:I

    add-int/2addr v1, v0

    iput v1, p0, LX/0MP;->j:I

    .line 45856
    iget-wide v2, p0, LX/0MP;->h:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0MP;->h:J

    .line 45857
    sub-int/2addr p2, v0

    .line 45858
    goto :goto_0

    .line 45859
    :cond_0
    return-void
.end method

.method public final a(LX/0L7;)Z
    .locals 2

    .prologue
    .line 45851
    iget-object v0, p0, LX/0MP;->c:LX/0MN;

    iget-object v1, p0, LX/0MP;->e:LX/0MO;

    invoke-virtual {v0, p1, v1}, LX/0MN;->a(LX/0L7;LX/0MO;)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 45848
    iget-object v0, p0, LX/0MP;->c:LX/0MN;

    invoke-virtual {v0}, LX/0MN;->d()J

    move-result-wide v0

    .line 45849
    invoke-static {p0, v0, v1}, LX/0MP;->c(LX/0MP;J)V

    .line 45850
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 45847
    iget-wide v0, p0, LX/0MP;->h:J

    return-wide v0
.end method
