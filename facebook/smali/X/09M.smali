.class public LX/09M;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/19a;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0So;

.field public e:I

.field public f:J

.field public g:LX/09N;

.field public h:LX/09N;

.field public i:LX/09N;

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/09N;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z

.field private l:J

.field private m:J

.field private n:J

.field public o:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22413
    const-class v0, LX/09M;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/09M;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0So;LX/19a;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0So;",
            "LX/19a;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22407
    iput-object p1, p0, LX/09M;->d:LX/0So;

    .line 22408
    iput-object p2, p0, LX/09M;->b:LX/19a;

    .line 22409
    iput-object p3, p0, LX/09M;->c:LX/0Ot;

    .line 22410
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/09M;->j:Ljava/util/List;

    .line 22411
    invoke-virtual {p0}, LX/09M;->a()V

    .line 22412
    return-void
.end method

.method private b(J)J
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 22402
    iget-wide v0, p0, LX/09M;->l:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 22403
    iget-wide v0, p0, LX/09M;->l:J

    sub-long v0, p1, v0

    .line 22404
    iget-object v4, p0, LX/09M;->b:LX/19a;

    iget-wide v4, v4, LX/19a;->a:J

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    .line 22405
    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 22389
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, LX/09M;->e:I

    .line 22390
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/09M;->f:J

    .line 22391
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/09M;->l:J

    .line 22392
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/09M;->m:J

    .line 22393
    const/4 v0, 0x0

    iput-object v0, p0, LX/09M;->g:LX/09N;

    .line 22394
    const/4 v0, 0x0

    iput-object v0, p0, LX/09M;->h:LX/09N;

    .line 22395
    const/4 v0, 0x0

    iput-object v0, p0, LX/09M;->i:LX/09N;

    .line 22396
    iget-object v0, p0, LX/09M;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 22397
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/09M;->k:Z

    .line 22398
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/09M;->n:J

    .line 22399
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/09M;->o:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22400
    monitor-exit p0

    return-void

    .line 22401
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 22414
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/09M;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/09M;->l:J

    .line 22415
    iput-wide p1, p0, LX/09M;->m:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22416
    monitor-exit p0

    return-void

    .line 22417
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 22385
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/09M;->l:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 22386
    :goto_0
    monitor-exit p0

    return-void

    .line 22387
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/09M;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/09M;->l:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 22388
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 22365
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/09M;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LX/09M;->b(J)J

    move-result-wide v6

    .line 22366
    cmp-long v0, v6, v8

    if-lez v0, :cond_3

    .line 22367
    iget v0, p0, LX/09M;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/09M;->e:I

    .line 22368
    iget-wide v0, p0, LX/09M;->f:J

    add-long/2addr v0, v6

    iput-wide v0, p0, LX/09M;->f:J

    .line 22369
    new-instance v1, LX/09N;

    iget-wide v2, p0, LX/09M;->m:J

    iget-wide v4, p0, LX/09M;->l:J

    invoke-direct/range {v1 .. v7}, LX/09N;-><init>(JJJ)V

    .line 22370
    iget-object v0, p0, LX/09M;->g:LX/09N;

    if-nez v0, :cond_0

    .line 22371
    iput-object v1, p0, LX/09M;->g:LX/09N;

    .line 22372
    :cond_0
    iput-object v1, p0, LX/09M;->h:LX/09N;

    .line 22373
    iget-object v0, p0, LX/09M;->i:LX/09N;

    if-eqz v0, :cond_1

    long-to-float v0, v6

    iget-object v2, p0, LX/09M;->i:LX/09N;

    invoke-virtual {v2}, LX/09N;->b()F

    move-result v2

    const/high16 v3, 0x447a0000    # 1000.0f

    mul-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    .line 22374
    :cond_1
    iput-object v1, p0, LX/09M;->i:LX/09N;

    .line 22375
    :cond_2
    iget-object v0, p0, LX/09M;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22376
    :cond_3
    iget-wide v0, p0, LX/09M;->n:J

    cmp-long v0, v0, v8

    if-lez v0, :cond_4

    .line 22377
    iget-wide v0, p0, LX/09M;->n:J

    iget-wide v2, p0, LX/09M;->l:J

    sub-long/2addr v0, v2

    .line 22378
    cmp-long v2, v0, v8

    if-ltz v2, :cond_4

    .line 22379
    iput-wide v0, p0, LX/09M;->o:J

    .line 22380
    :cond_4
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/09M;->l:J

    .line 22381
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/09M;->m:J

    .line 22382
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/09M;->n:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22383
    monitor-exit p0

    return-void

    .line 22384
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 22360
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/09M;->l:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, LX/09M;->n:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 22361
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/09M;->k:Z

    .line 22362
    :cond_0
    invoke-virtual {p0}, LX/09M;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22363
    monitor-exit p0

    return-void

    .line 22364
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 2

    .prologue
    .line 22357
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/09M;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/09M;->n:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22358
    monitor-exit p0

    return-void

    .line 22359
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()LX/09O;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 22345
    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, LX/09M;->l:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "Must call .end() first"

    invoke-static {v0, v1}, LX/03g;->b(ZLjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22346
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/09M;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 22347
    iget-object v0, p0, LX/09M;->b:LX/19a;

    iget v0, v0, LX/19a;->b:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    sub-long v4, v2, v0

    .line 22348
    :goto_1
    iget-object v0, p0, LX/09M;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 22349
    iget-object v0, p0, LX/09M;->j:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/09N;

    .line 22350
    iget-wide v6, v0, LX/09N;->b:J

    iget-wide v8, v0, LX/09N;->c:J

    add-long/2addr v6, v8

    move-wide v0, v6

    .line 22351
    cmp-long v0, v0, v4

    if-gtz v0, :cond_1

    .line 22352
    iget-object v0, p0, LX/09M;->j:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 22353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 22354
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 22355
    :try_start_2
    iget-object v0, p0, LX/09M;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/09M;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 22356
    :cond_1
    new-instance v0, LX/09O;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/09O;-><init>(LX/09M;JJ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0
.end method
