.class public final LX/0IF;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic b:Ljava/util/concurrent/ScheduledFuture;

.field public final synthetic c:I

.field public final synthetic d:LX/0ID;

.field public final synthetic e:LX/08o;


# direct methods
.method public constructor <init>(LX/08o;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/concurrent/ScheduledFuture;ILX/0ID;)V
    .locals 0

    .prologue
    .line 38484
    iput-object p1, p0, LX/0IF;->e:LX/08o;

    iput-object p2, p0, LX/0IF;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p3, p0, LX/0IF;->b:Ljava/util/concurrent/ScheduledFuture;

    iput p4, p0, LX/0IF;->c:I

    iput-object p5, p0, LX/0IF;->d:LX/0ID;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v0, 0x26

    const v1, 0x3f20ebd2

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 38485
    invoke-virtual {p0}, LX/0IF;->getResultCode()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/0IF;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38486
    iget-object v1, p0, LX/0IF;->b:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v1, v3}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 38487
    invoke-virtual {p0, v3}, LX/0IF;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v1

    .line 38488
    const-string v2, "shared_qe_flag"

    iget v3, p0, LX/0IF;->c:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 38489
    iget-object v2, p0, LX/0IF;->d:LX/0ID;

    invoke-virtual {v2, v1}, LX/0ID;->a(I)V

    .line 38490
    :cond_0
    const/16 v1, 0x27

    const v2, -0x5d2fe742

    invoke-static {p2, v4, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void
.end method
