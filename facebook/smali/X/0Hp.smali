.class public LX/0Hp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0BE;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0Ho;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/01o;


# direct methods
.method public constructor <init>(LX/01o;)V
    .locals 1

    .prologue
    .line 38094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38095
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0Hp;->a:Ljava/util/Map;

    .line 38096
    iput-object p1, p0, LX/0Hp;->b:LX/01o;

    .line 38097
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38089
    const-string v0, "M"

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 38090
    monitor-enter p0

    :try_start_0
    const-string v0, "NotificationDeliveryStoreInMemory"

    const-string v1, "remove %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38091
    iget-object v0, p0, LX/0Hp;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38092
    monitor-exit p0

    return-void

    .line 38093
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 38085
    monitor-enter p0

    :try_start_0
    const-string v0, "NotificationDeliveryStoreInMemory"

    const-string v1, "add %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38086
    iget-object v0, p0, LX/0Hp;->a:Ljava/util/Map;

    new-instance v1, LX/0Ho;

    iget-object v2, p0, LX/0Hp;->b:LX/01o;

    invoke-virtual {v2}, LX/01o;->a()J

    move-result-wide v4

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, LX/0Ho;-><init>(Landroid/content/Intent;Ljava/lang/String;JI)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38087
    monitor-exit p0

    return-void

    .line 38088
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0Hn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38071
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 38072
    iget-object v0, p0, LX/0Hp;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 38073
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 38074
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 38075
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ho;

    .line 38076
    iget v3, v0, LX/0Ho;->b:I

    const/16 v4, 0xa

    if-ge v3, v4, :cond_2

    .line 38077
    iget-wide v4, v0, LX/0Ho;->a:J

    const-wide/32 v6, 0x493e0

    add-long/2addr v4, v6

    iget-object v3, p0, LX/0Hp;->b:LX/01o;

    invoke-virtual {v3}, LX/01o;->a()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-ltz v3, :cond_1

    iget-wide v4, v0, LX/0Ho;->a:J

    iget-object v3, p0, LX/0Hp;->b:LX/01o;

    invoke-virtual {v3}, LX/01o;->a()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 38078
    :cond_1
    iget v3, v0, LX/0Ho;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, LX/0Ho;->b:I

    .line 38079
    iget-object v3, p0, LX/0Hp;->b:LX/01o;

    invoke-virtual {v3}, LX/01o;->a()J

    move-result-wide v4

    iput-wide v4, v0, LX/0Ho;->a:J

    .line 38080
    const-string v3, "NotificationDeliveryStoreInMemory"

    const-string v4, "checkAndUpdateRetryList found %s %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, LX/0Hn;->d:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, v0, LX/0Ho;->b:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38081
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 38082
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 38083
    :cond_2
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 38084
    :cond_3
    monitor-exit p0

    return-object v1
.end method
