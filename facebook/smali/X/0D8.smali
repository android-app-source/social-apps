.class public final LX/0D8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

.field public final synthetic b:Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

.field public final synthetic c:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

.field public final synthetic d:LX/0D9;


# direct methods
.method public constructor <init>(LX/0D9;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;)V
    .locals 0

    .prologue
    .line 29623
    iput-object p1, p0, LX/0D8;->d:LX/0D9;

    iput-object p2, p0, LX/0D8;->a:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    iput-object p3, p0, LX/0D8;->b:Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    iput-object p4, p0, LX/0D8;->c:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x38fe0818

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 29624
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 29625
    const-string v0, "callbackID"

    iget-object v1, p0, LX/0D8;->a:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    invoke-virtual {v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29626
    iget-object v0, p0, LX/0D8;->b:Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a()Ljava/util/HashMap;

    move-result-object v4

    .line 29627
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 29628
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 29629
    :cond_0
    iget-object v0, p0, LX/0D8;->c:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    iget-object v1, p0, LX/0D8;->a:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    invoke-virtual {v0, v1, v3}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Landroid/os/Bundle;)V

    .line 29630
    iget-object v0, p0, LX/0D8;->d:LX/0D9;

    iget-object v0, v0, LX/0D9;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 29631
    iget-object v0, p0, LX/0D8;->d:LX/0D9;

    iget-object v0, v0, LX/0D9;->a:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 29632
    iget-object v1, p0, LX/0D8;->d:LX/0D9;

    iget-object v1, v1, LX/0D9;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 29633
    invoke-static {}, LX/0CQ;->a()LX/0CQ;

    move-result-object v0

    iget-object v1, p0, LX/0D8;->a:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    .line 29634
    iget-object v3, v0, LX/0CQ;->d:LX/0DT;

    if-eqz v3, :cond_1

    .line 29635
    :try_start_0
    iget-object v3, v0, LX/0CQ;->d:LX/0DT;

    invoke-interface {v3, v1}, LX/0DT;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29636
    :cond_1
    :goto_1
    const v0, 0x61e6b93a

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    :catch_0
    goto :goto_1
.end method
