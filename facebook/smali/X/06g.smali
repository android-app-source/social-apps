.class public final LX/06g;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:LX/05I;


# direct methods
.method public constructor <init>(LX/05I;)V
    .locals 0

    .prologue
    .line 18097
    iput-object p1, p0, LX/06g;->a:LX/05I;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x74e6bb35

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 18085
    if-nez p2, :cond_0

    .line 18086
    const/16 v1, 0x27

    const v2, -0x73800761

    invoke-static {p2, v3, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 18087
    :goto_0
    return-void

    .line 18088
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.facebook.rti.intent.ACTION_NOTIFICATION_ACK"

    invoke-static {v1, v2}, LX/06P;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 18089
    const v1, 0x2d984a76

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 18090
    :cond_1
    iget-object v1, p0, LX/06g;->a:LX/05I;

    iget-object v1, v1, LX/05I;->b:LX/04v;

    invoke-virtual {v1, p2}, LX/04v;->a(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 18091
    const v1, 0x65a2eafc

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 18092
    :cond_2
    const-string v1, "extra_notification_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 18093
    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 18094
    iget-object v2, p0, LX/06g;->a:LX/05I;

    invoke-virtual {v2}, LX/05I;->e()LX/0BE;

    move-result-object v2

    invoke-interface {v2, v1}, LX/0BE;->a(Ljava/lang/String;)V

    .line 18095
    const-string v2, "NotificationDeliveryHelper"

    const-string v3, "receiver/NotificationAckReceiver %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18096
    :cond_3
    const v1, 0x558e8d4b

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0
.end method
