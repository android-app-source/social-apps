.class public final LX/00R;
.super LX/00B;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2357
    invoke-direct {p0}, LX/00B;-><init>()V

    return-void
.end method


# virtual methods
.method public final translate(Ljava/lang/Throwable;Ljava/util/Map;)Ljava/lang/Throwable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Throwable;"
        }
    .end annotation

    .prologue
    .line 2358
    invoke-static {}, LX/00F;->a()J

    move-result-wide v0

    .line 2359
    invoke-static {}, LX/00F;->c()LX/01E;

    move-result-object v2

    .line 2360
    if-eqz v2, :cond_0

    .line 2361
    const-string v3, "CMIYK_remedyLog_timestamp_boot_relative"

    iget-wide v4, v2, LX/01E;->a:J

    sub-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2362
    const-string v0, "CMIYK_remedyLog_appliedThisBoot"

    invoke-static {}, LX/00F;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2363
    const-string v0, "CMIYK_recentCrashes_count"

    invoke-static {}, LX/00F;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2364
    const-string v0, "CMIYK_recentCrashes_interval"

    const/16 v1, 0x3840

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2365
    :cond_0
    return-object p1
.end method
