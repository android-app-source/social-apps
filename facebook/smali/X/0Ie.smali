.class public LX/0Ie;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/05F;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:LX/06k;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 38774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38775
    iput-object p1, p0, LX/0Ie;->a:Landroid/content/Context;

    .line 38776
    invoke-direct {p0}, LX/0Ie;->f()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 38777
    const-string v1, "/settings/mqtt/id/connection_key"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/settings/mqtt/id/connection_secret"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/06k;->a(Ljava/lang/String;Ljava/lang/String;)LX/06k;

    move-result-object v0

    iput-object v0, p0, LX/0Ie;->b:LX/06k;

    .line 38778
    return-void
.end method

.method private f()Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 38792
    iget-object v0, p0, LX/0Ie;->a:Landroid/content/Context;

    sget-object v1, LX/01p;->g:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/06k;
    .locals 1

    .prologue
    .line 38793
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ie;->b:LX/06k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38785
    return-void
.end method

.method public final declared-synchronized a(LX/06k;)Z
    .locals 3

    .prologue
    .line 38786
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ie;->b:LX/06k;

    invoke-virtual {v0, p1}, LX/06k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 38787
    invoke-direct {p0}, LX/0Ie;->f()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "/settings/mqtt/id/connection_key"

    invoke-virtual {p1}, LX/06k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "/settings/mqtt/id/connection_secret"

    invoke-virtual {p1}, LX/06k;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 38788
    iput-object p1, p0, LX/0Ie;->b:LX/06k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38789
    const/4 v0, 0x1

    .line 38790
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 38791
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38784
    const-string v0, "device_auth"

    return-object v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 38781
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/06k;->a:LX/06k;

    invoke-virtual {p0, v0}, LX/0Ie;->a(LX/06k;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38782
    monitor-exit p0

    return-void

    .line 38783
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38780
    const-string v0, ""

    return-object v0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 38779
    return-void
.end method
