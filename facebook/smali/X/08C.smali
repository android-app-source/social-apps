.class public final LX/08C;
.super Ljava/io/ByteArrayOutputStream;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20941
    invoke-direct {p0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 20942
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 20943
    invoke-direct {p0, p1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 20944
    return-void
.end method


# virtual methods
.method public final getRawBuffer()[B
    .locals 2

    .prologue
    .line 20945
    iget v0, p0, Ljava/io/ByteArrayOutputStream;->count:I

    iget-object v1, p0, Ljava/io/ByteArrayOutputStream;->buf:[B

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 20946
    iget-object v0, p0, Ljava/io/ByteArrayOutputStream;->buf:[B

    .line 20947
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/08C;->toByteArray()[B

    move-result-object v0

    goto :goto_0
.end method
