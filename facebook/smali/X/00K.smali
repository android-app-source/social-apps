.class public LX/00K;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final mApplicationContext:Landroid/content/Context;

.field public final mCrashReportUrl:Ljava/lang/String;

.field public final mExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

.field public final mIsInternalBuild:Z

.field private final mShouldStartANRDetector:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2115
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/00K;-><init>(Landroid/content/Context;Ljava/lang/String;ZZ)V

    .line 2116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 2118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2119
    if-nez p1, :cond_0

    .line 2120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Application context cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2121
    :cond_0
    if-nez p2, :cond_1

    .line 2122
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Crash report url cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2123
    :cond_1
    iput-object p1, p0, LX/00K;->mApplicationContext:Landroid/content/Context;

    .line 2124
    iput-object p2, p0, LX/00K;->mCrashReportUrl:Ljava/lang/String;

    .line 2125
    iput-boolean p3, p0, LX/00K;->mIsInternalBuild:Z

    .line 2126
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, LX/00K;->mExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 2127
    iput-boolean p4, p0, LX/00K;->mShouldStartANRDetector:Z

    .line 2128
    return-void
.end method


# virtual methods
.method public shouldStartANRDetector()Z
    .locals 1

    .prologue
    .line 2117
    iget-boolean v0, p0, LX/00K;->mShouldStartANRDetector:Z

    return v0
.end method
