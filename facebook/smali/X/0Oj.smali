.class public final LX/0Oj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:[B

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 53898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53899
    new-array v0, p1, [B

    iput-object v0, p0, LX/0Oj;->a:[B

    .line 53900
    iget-object v0, p0, LX/0Oj;->a:[B

    array-length v0, v0

    iput v0, p0, LX/0Oj;->c:I

    .line 53901
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 53902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53903
    iput-object p1, p0, LX/0Oj;->a:[B

    .line 53904
    array-length v0, p1

    iput v0, p0, LX/0Oj;->c:I

    .line 53905
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0

    .prologue
    .line 53892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53893
    iput-object p1, p0, LX/0Oj;->a:[B

    .line 53894
    iput p2, p0, LX/0Oj;->c:I

    .line 53895
    return-void
.end method


# virtual methods
.method public final a(ILjava/nio/charset/Charset;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 53906
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    invoke-direct {v0, v1, v2, p1, p2}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 53907
    iget v1, p0, LX/0Oj;->b:I

    add-int/2addr v1, p1

    iput v1, p0, LX/0Oj;->b:I

    .line 53908
    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53909
    iput v0, p0, LX/0Oj;->b:I

    .line 53910
    iput v0, p0, LX/0Oj;->c:I

    .line 53911
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 53912
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/0Oj;->a:[B

    array-length v0, v0

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->a(Z)V

    .line 53913
    iput p1, p0, LX/0Oj;->c:I

    .line 53914
    return-void

    .line 53915
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0Oi;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53916
    iget-object v0, p1, LX/0Oi;->a:[B

    invoke-virtual {p0, v0, v1, p2}, LX/0Oj;->a([BII)V

    .line 53917
    invoke-virtual {p1, v1}, LX/0Oi;->a(I)V

    .line 53918
    return-void
.end method

.method public final a([BI)V
    .locals 1

    .prologue
    .line 53932
    iput-object p1, p0, LX/0Oj;->a:[B

    .line 53933
    iput p2, p0, LX/0Oj;->c:I

    .line 53934
    const/4 v0, 0x0

    iput v0, p0, LX/0Oj;->b:I

    .line 53935
    return-void
.end method

.method public final a([BII)V
    .locals 2

    .prologue
    .line 53919
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v1, p0, LX/0Oj;->b:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 53920
    iget v0, p0, LX/0Oj;->b:I

    add-int/2addr v0, p3

    iput v0, p0, LX/0Oj;->b:I

    .line 53921
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 53931
    iget v0, p0, LX/0Oj;->c:I

    iget v1, p0, LX/0Oj;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 53927
    if-ltz p1, :cond_0

    iget v0, p0, LX/0Oj;->c:I

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->a(Z)V

    .line 53928
    iput p1, p0, LX/0Oj;->b:I

    .line 53929
    return-void

    .line 53930
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 53926
    iget v0, p0, LX/0Oj;->c:I

    return v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 53924
    iget v0, p0, LX/0Oj;->b:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 53925
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 53923
    iget v0, p0, LX/0Oj;->b:I

    return v0
.end method

.method public final d(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53922
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/0Oj;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 53896
    iget-object v0, p0, LX/0Oj;->a:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0Oj;->a:[B

    array-length v0, v0

    goto :goto_0
.end method

.method public final f()I
    .locals 3

    .prologue
    .line 53843
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public final g()I
    .locals 4

    .prologue
    .line 53848
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0Oj;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public final h()I
    .locals 4

    .prologue
    .line 53849
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0Oj;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method public final j()I
    .locals 4

    .prologue
    .line 53850
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0Oj;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0Oj;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public final k()J
    .locals 8

    .prologue
    const-wide/16 v6, 0xff

    .line 53851
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v1

    int-to-long v0, v0

    and-long/2addr v0, v6

    const/16 v2, 0x18

    shl-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public final l()J
    .locals 8

    .prologue
    const-wide/16 v6, 0xff

    .line 53852
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v1

    int-to-long v0, v0

    and-long/2addr v0, v6

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public final m()I
    .locals 4

    .prologue
    .line 53853
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0Oj;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0Oj;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0Oj;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public final n()I
    .locals 4

    .prologue
    .line 53854
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0Oj;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0Oj;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0Oj;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method public final o()J
    .locals 8

    .prologue
    const-wide/16 v6, 0xff

    .line 53855
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v1

    int-to-long v0, v0

    and-long/2addr v0, v6

    const/16 v2, 0x38

    shl-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public final p()J
    .locals 8

    .prologue
    const-wide/16 v6, 0xff

    .line 53856
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v1

    int-to-long v0, v0

    and-long/2addr v0, v6

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/0Oj;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public final q()I
    .locals 4

    .prologue
    .line 53857
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0Oj;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 53858
    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, LX/0Oj;->b:I

    .line 53859
    return v0
.end method

.method public final r()I
    .locals 4

    .prologue
    .line 53860
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    .line 53861
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v1

    .line 53862
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v2

    .line 53863
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v3

    .line 53864
    shl-int/lit8 v0, v0, 0x15

    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    shl-int/lit8 v1, v2, 0x7

    or-int/2addr v0, v1

    or-int/2addr v0, v3

    return v0
.end method

.method public final s()I
    .locals 4

    .prologue
    .line 53865
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    .line 53866
    if-gez v0, :cond_0

    .line 53867
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Top bit not zero: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 53868
    :cond_0
    return v0
.end method

.method public final t()I
    .locals 4

    .prologue
    .line 53844
    invoke-virtual {p0}, LX/0Oj;->n()I

    move-result v0

    .line 53845
    if-gez v0, :cond_0

    .line 53846
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Top bit not zero: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 53847
    :cond_0
    return v0
.end method

.method public final u()J
    .locals 5

    .prologue
    .line 53869
    invoke-virtual {p0}, LX/0Oj;->o()J

    move-result-wide v0

    .line 53870
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 53871
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Top bit not zero: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 53872
    :cond_0
    return-wide v0
.end method

.method public final v()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0xd

    const/16 v5, 0xa

    .line 53873
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v0

    if-nez v0, :cond_0

    .line 53874
    const/4 v0, 0x0

    .line 53875
    :goto_0
    return-object v0

    .line 53876
    :cond_0
    iget v0, p0, LX/0Oj;->b:I

    .line 53877
    :goto_1
    iget v1, p0, LX/0Oj;->c:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, LX/0Oj;->a:[B

    aget-byte v1, v1, v0

    if-eq v1, v5, :cond_1

    iget-object v1, p0, LX/0Oj;->a:[B

    aget-byte v1, v1, v0

    if-eq v1, v6, :cond_1

    .line 53878
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 53879
    :cond_1
    iget v1, p0, LX/0Oj;->b:I

    sub-int v1, v0, v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_2

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    aget-byte v1, v1, v2

    const/16 v2, -0x11

    if-ne v1, v2, :cond_2

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v2, 0x1

    aget-byte v1, v1, v2

    const/16 v2, -0x45

    if-ne v1, v2, :cond_2

    iget-object v1, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    add-int/lit8 v2, v2, 0x2

    aget-byte v1, v1, v2

    const/16 v2, -0x41

    if-ne v1, v2, :cond_2

    .line 53880
    iget v1, p0, LX/0Oj;->b:I

    add-int/lit8 v1, v1, 0x3

    iput v1, p0, LX/0Oj;->b:I

    .line 53881
    :cond_2
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, LX/0Oj;->a:[B

    iget v3, p0, LX/0Oj;->b:I

    iget v4, p0, LX/0Oj;->b:I

    sub-int v4, v0, v4

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([BII)V

    .line 53882
    iput v0, p0, LX/0Oj;->b:I

    .line 53883
    iget v0, p0, LX/0Oj;->b:I

    iget v2, p0, LX/0Oj;->c:I

    if-ne v0, v2, :cond_3

    move-object v0, v1

    .line 53884
    goto :goto_0

    .line 53885
    :cond_3
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v2

    if-ne v0, v6, :cond_4

    .line 53886
    iget v0, p0, LX/0Oj;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Oj;->b:I

    .line 53887
    iget v0, p0, LX/0Oj;->b:I

    iget v2, p0, LX/0Oj;->c:I

    if-ne v0, v2, :cond_4

    move-object v0, v1

    .line 53888
    goto :goto_0

    .line 53889
    :cond_4
    iget-object v0, p0, LX/0Oj;->a:[B

    iget v2, p0, LX/0Oj;->b:I

    aget-byte v0, v0, v2

    if-ne v0, v5, :cond_5

    .line 53890
    iget v0, p0, LX/0Oj;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Oj;->b:I

    :cond_5
    move-object v0, v1

    .line 53891
    goto/16 :goto_0
.end method
