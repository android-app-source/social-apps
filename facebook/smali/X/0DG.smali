.class public final LX/0DG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

.field public final synthetic b:LX/0DI;


# direct methods
.method public constructor <init>(LX/0DI;Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)V
    .locals 0

    .prologue
    .line 29742
    iput-object p1, p0, LX/0DG;->b:LX/0DI;

    iput-object p2, p0, LX/0DG;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0xbaaaea5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 29743
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 29744
    const-string v2, "action"

    const-string v3, "instant_experience_chrome_navigation_drawer_opened"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29745
    const-string v2, "url"

    iget-object v3, p0, LX/0DG;->b:LX/0DI;

    iget-object v3, v3, LX/0DI;->e:LX/0D5;

    invoke-virtual {v3}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29746
    iget-object v2, p0, LX/0DG;->b:LX/0DI;

    iget-object v2, v2, LX/0DI;->f:LX/0CQ;

    iget-object v3, p0, LX/0DG;->b:LX/0DI;

    iget-object v3, v3, LX/0DI;->g:Landroid/os/Bundle;

    invoke-virtual {v2, v1, v3}, LX/0CQ;->a(Ljava/util/Map;Landroid/os/Bundle;)V

    .line 29747
    iget-object v1, p0, LX/0DG;->a:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-virtual {v1}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a()V

    .line 29748
    const v1, -0x6be5129a

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
