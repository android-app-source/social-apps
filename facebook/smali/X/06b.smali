.class public final LX/06b;
.super LX/05B;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/05B",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/06b;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18015
    new-instance v0, LX/06b;

    invoke-direct {v0}, LX/06b;-><init>()V

    sput-object v0, LX/06b;->a:LX/06b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18008
    invoke-direct {p0}, LX/05B;-><init>()V

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18014
    sget-object v0, LX/06b;->a:LX/06b;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 18013
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 18012
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Optional.get() cannot be called on an absent value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 18011
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 18010
    const v0, 0x598df91c

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18009
    const-string v0, "Optional.absent()"

    return-object v0
.end method
