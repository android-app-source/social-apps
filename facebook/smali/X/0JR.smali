.class public final enum LX/0JR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JR;

.field public static final enum PAGE_VIDEO_HUB:LX/0JR;

.field public static final enum PAGE_VIDEO_PERMALINK:LX/0JR;

.field public static final enum PHOTO_ALBUM:LX/0JR;

.field public static final enum UNKNOWN:LX/0JR;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39416
    new-instance v0, LX/0JR;

    const-string v1, "PHOTO_ALBUM"

    const-string v2, "photo_album"

    invoke-direct {v0, v1, v3, v2}, LX/0JR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JR;->PHOTO_ALBUM:LX/0JR;

    .line 39417
    new-instance v0, LX/0JR;

    const-string v1, "PAGE_VIDEO_PERMALINK"

    const-string v2, "page_video_permalink"

    invoke-direct {v0, v1, v4, v2}, LX/0JR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JR;->PAGE_VIDEO_PERMALINK:LX/0JR;

    .line 39418
    new-instance v0, LX/0JR;

    const-string v1, "PAGE_VIDEO_HUB"

    const-string v2, "page_video_hub"

    invoke-direct {v0, v1, v5, v2}, LX/0JR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JR;->PAGE_VIDEO_HUB:LX/0JR;

    .line 39419
    new-instance v0, LX/0JR;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v6, v2}, LX/0JR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JR;->UNKNOWN:LX/0JR;

    .line 39420
    const/4 v0, 0x4

    new-array v0, v0, [LX/0JR;

    sget-object v1, LX/0JR;->PHOTO_ALBUM:LX/0JR;

    aput-object v1, v0, v3

    sget-object v1, LX/0JR;->PAGE_VIDEO_PERMALINK:LX/0JR;

    aput-object v1, v0, v4

    sget-object v1, LX/0JR;->PAGE_VIDEO_HUB:LX/0JR;

    aput-object v1, v0, v5

    sget-object v1, LX/0JR;->UNKNOWN:LX/0JR;

    aput-object v1, v0, v6

    sput-object v0, LX/0JR;->$VALUES:[LX/0JR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39421
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39422
    iput-object p3, p0, LX/0JR;->value:Ljava/lang/String;

    .line 39423
    return-void
.end method

.method public static asOriginType(Ljava/lang/String;)LX/0JR;
    .locals 5

    .prologue
    .line 39424
    if-nez p0, :cond_1

    .line 39425
    sget-object v0, LX/0JR;->UNKNOWN:LX/0JR;

    .line 39426
    :cond_0
    :goto_0
    return-object v0

    .line 39427
    :cond_1
    invoke-static {}, LX/0JR;->values()[LX/0JR;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 39428
    iget-object v4, v0, LX/0JR;->value:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 39429
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 39430
    :cond_2
    sget-object v0, LX/0JR;->UNKNOWN:LX/0JR;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JR;
    .locals 1

    .prologue
    .line 39431
    const-class v0, LX/0JR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JR;

    return-object v0
.end method

.method public static values()[LX/0JR;
    .locals 1

    .prologue
    .line 39432
    sget-object v0, LX/0JR;->$VALUES:[LX/0JR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JR;

    return-object v0
.end method
