.class public final LX/0OE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0G7;


# instance fields
.field private final a:LX/0G7;

.field private final b:LX/0G7;

.field private final c:LX/0G7;

.field private final d:LX/0G7;

.field private e:LX/0G7;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/04n;LX/0G7;)V
    .locals 1

    .prologue
    .line 52854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52855
    invoke-static {p3}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0G7;

    iput-object v0, p0, LX/0OE;->a:LX/0G7;

    .line 52856
    new-instance v0, LX/0OG;

    invoke-direct {v0, p2}, LX/0OG;-><init>(LX/04n;)V

    iput-object v0, p0, LX/0OE;->b:LX/0G7;

    .line 52857
    new-instance v0, LX/0O3;

    invoke-direct {v0, p1, p2}, LX/0O3;-><init>(Landroid/content/Context;LX/04n;)V

    iput-object v0, p0, LX/0OE;->c:LX/0G7;

    .line 52858
    new-instance v0, LX/0O7;

    invoke-direct {v0, p1, p2}, LX/0O7;-><init>(Landroid/content/Context;LX/04n;)V

    iput-object v0, p0, LX/0OE;->d:LX/0G7;

    .line 52859
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/04n;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 52852
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/0OE;-><init>(Landroid/content/Context;LX/04n;Ljava/lang/String;Z)V

    .line 52853
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;LX/04n;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/16 v4, 0x1f40

    .line 52850
    new-instance v0, LX/0OD;

    const/4 v2, 0x0

    move-object v1, p3

    move-object v3, p2

    move v5, v4

    move v6, p4

    invoke-direct/range {v0 .. v6}, LX/0OD;-><init>(Ljava/lang/String;LX/0OH;LX/04n;IIZ)V

    invoke-direct {p0, p1, p2, v0}, LX/0OE;-><init>(Landroid/content/Context;LX/04n;LX/0G7;)V

    .line 52851
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 52848
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, LX/0OE;-><init>(Landroid/content/Context;LX/04n;Ljava/lang/String;Z)V

    .line 52849
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 1

    .prologue
    .line 52825
    iget-object v0, p0, LX/0OE;->e:LX/0G7;

    invoke-interface {v0, p1, p2, p3}, LX/0G6;->a([BII)I

    move-result v0

    return v0
.end method

.method public final a(LX/0OA;)J
    .locals 4

    .prologue
    .line 52832
    iget-object v0, p0, LX/0OE;->e:LX/0G7;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 52833
    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 52834
    iget-object v1, p1, LX/0OA;->a:Landroid/net/Uri;

    .line 52835
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 52836
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "file"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_0
    const/4 v2, 0x1

    :goto_1
    move v1, v2

    .line 52837
    if-eqz v1, :cond_3

    .line 52838
    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/android_asset/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52839
    iget-object v0, p0, LX/0OE;->c:LX/0G7;

    iput-object v0, p0, LX/0OE;->e:LX/0G7;

    .line 52840
    :goto_2
    iget-object v0, p0, LX/0OE;->e:LX/0G7;

    invoke-interface {v0, p1}, LX/0G6;->a(LX/0OA;)J

    move-result-wide v0

    return-wide v0

    .line 52841
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 52842
    :cond_2
    iget-object v0, p0, LX/0OE;->b:LX/0G7;

    iput-object v0, p0, LX/0OE;->e:LX/0G7;

    goto :goto_2

    .line 52843
    :cond_3
    const-string v1, "asset"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 52844
    iget-object v0, p0, LX/0OE;->c:LX/0G7;

    iput-object v0, p0, LX/0OE;->e:LX/0G7;

    goto :goto_2

    .line 52845
    :cond_4
    const-string v1, "content"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 52846
    iget-object v0, p0, LX/0OE;->d:LX/0G7;

    iput-object v0, p0, LX/0OE;->e:LX/0G7;

    goto :goto_2

    .line 52847
    :cond_5
    iget-object v0, p0, LX/0OE;->a:LX/0G7;

    iput-object v0, p0, LX/0OE;->e:LX/0G7;

    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52827
    iget-object v0, p0, LX/0OE;->e:LX/0G7;

    if-eqz v0, :cond_0

    .line 52828
    :try_start_0
    iget-object v0, p0, LX/0OE;->e:LX/0G7;

    invoke-interface {v0}, LX/0G6;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52829
    iput-object v1, p0, LX/0OE;->e:LX/0G7;

    .line 52830
    :cond_0
    return-void

    .line 52831
    :catchall_0
    move-exception v0

    iput-object v1, p0, LX/0OE;->e:LX/0G7;

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52826
    iget-object v0, p0, LX/0OE;->e:LX/0G7;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0OE;->e:LX/0G7;

    invoke-interface {v0}, LX/0G7;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
