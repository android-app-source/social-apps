.class public final LX/07s;
.super LX/07t;
.source ""


# instance fields
.field public final synthetic a:LX/023;

.field private c:Ljava/io/File;

.field private final d:I


# direct methods
.method public constructor <init>(LX/023;LX/024;)V
    .locals 2

    .prologue
    .line 20287
    iput-object p1, p0, LX/07s;->a:LX/023;

    .line 20288
    invoke-direct {p0, p1, p2}, LX/07t;-><init>(LX/024;LX/025;)V

    .line 20289
    new-instance v0, Ljava/io/File;

    iget-object v1, p1, LX/025;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/07s;->c:Ljava/io/File;

    .line 20290
    iget v0, p1, LX/023;->f:I

    iput v0, p0, LX/07s;->d:I

    .line 20291
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/zip/ZipEntry;Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 20292
    invoke-virtual {p1}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v2

    .line 20293
    iget v3, p0, LX/07s;->d:I

    and-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_0

    .line 20294
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "allowing consideration of "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": self-extraction preferred"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20295
    :goto_0
    return v0

    .line 20296
    :cond_0
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, LX/07s;->c:Ljava/io/File;

    invoke-direct {v3, v4, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 20297
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v4

    if-nez v4, :cond_1

    .line 20298
    new-array v3, v9, [Ljava/lang/Object;

    aput-object v2, v3, v1

    aput-object p2, v3, v0

    goto :goto_0

    .line 20299
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 20300
    invoke-virtual {p1}, Ljava/util/zip/ZipEntry;->getSize()J

    move-result-wide v6

    .line 20301
    cmp-long v8, v4, v6

    if-eqz v8, :cond_2

    .line 20302
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v3, v2, v1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v2, v9

    goto :goto_0

    .line 20303
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "not allowing consideration of "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": deferring to libdir"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 20304
    goto :goto_0
.end method
