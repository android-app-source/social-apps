.class public final LX/0GN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e",
        "BadMethodUse-java.util.concurrent.Executors.newFixedThreadPool"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final t:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LX/0GM;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "LX/0GM;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/concurrent/ExecutorService;

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final e:LX/0GB;

.field private final f:Landroid/content/Context;

.field public g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:I

.field public i:Z

.field public j:Z

.field public k:I

.field public l:I

.field public m:I

.field public n:Z

.field public o:I

.field public p:I

.field public q:Z

.field public r:Z

.field public s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35255
    const-class v0, LX/0GN;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0GN;->a:Ljava/lang/String;

    .line 35256
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/0GN;->t:Ljava/util/Collection;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0GB;)V
    .locals 3

    .prologue
    const/16 v2, 0x2710

    const/4 v1, 0x0

    .line 35236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35237
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, LX/0GN;->b:Ljava/util/concurrent/BlockingQueue;

    .line 35238
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/0GN;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 35239
    iput-boolean v1, p0, LX/0GN;->i:Z

    .line 35240
    iput-boolean v1, p0, LX/0GN;->j:Z

    .line 35241
    iput v2, p0, LX/0GN;->k:I

    .line 35242
    iput v2, p0, LX/0GN;->l:I

    .line 35243
    const/4 v0, 0x3

    iput v0, p0, LX/0GN;->m:I

    .line 35244
    iput-boolean v1, p0, LX/0GN;->n:Z

    .line 35245
    iput v1, p0, LX/0GN;->o:I

    .line 35246
    iput v1, p0, LX/0GN;->p:I

    .line 35247
    iput-boolean v1, p0, LX/0GN;->q:Z

    .line 35248
    iput-boolean v1, p0, LX/0GN;->r:Z

    .line 35249
    iput-boolean v1, p0, LX/0GN;->s:Z

    .line 35250
    iput-object p1, p0, LX/0GN;->f:Landroid/content/Context;

    .line 35251
    iput-object p2, p0, LX/0GN;->e:LX/0GB;

    .line 35252
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/0GN;->g:Ljava/util/Set;

    .line 35253
    const/4 v0, 0x2

    iput v0, p0, LX/0GN;->h:I

    .line 35254
    return-void
.end method

.method private static a(LX/0AY;LX/0Ah;J)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 35225
    if-nez p1, :cond_1

    .line 35226
    :cond_0
    :goto_0
    return v0

    .line 35227
    :cond_1
    invoke-virtual {p1}, LX/0Ah;->f()LX/0BX;

    move-result-object v1

    .line 35228
    if-eqz v1, :cond_0

    .line 35229
    invoke-virtual {p0, v0}, LX/0AY;->b(I)J

    move-result-wide v2

    .line 35230
    invoke-interface {v1}, LX/0BX;->a()I

    move-result v0

    .line 35231
    invoke-interface {v1, v2, v3}, LX/0BX;->a(J)I

    move-result v4

    .line 35232
    invoke-interface {v1, v0}, LX/0BX;->a(I)J

    move-result-wide v6

    .line 35233
    invoke-interface {v1, v4}, LX/0BX;->a(I)J

    move-result-wide v8

    invoke-interface {v1, v4, v2, v3}, LX/0BX;->a(IJ)J

    move-result-wide v4

    add-long/2addr v4, v8

    .line 35234
    sub-long/2addr v4, p2

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 35235
    invoke-interface {v1, v4, v5, v2, v3}, LX/0BX;->a(JJ)I

    move-result v0

    goto :goto_0
.end method

.method private static a(LX/0GN;Ljava/lang/String;LX/0Ak;Z)LX/0Ah;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 35182
    if-nez p2, :cond_1

    .line 35183
    :cond_0
    :goto_0
    return-object v0

    .line 35184
    :cond_1
    iget-object v6, p2, LX/0Ak;->c:Ljava/util/List;

    .line 35185
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 35186
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 35187
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    goto :goto_0

    .line 35188
    :cond_2
    if-nez p3, :cond_4

    .line 35189
    iget-object v0, p0, LX/0GN;->e:LX/0GB;

    const/4 v4, 0x0

    .line 35190
    invoke-virtual {v0, p1}, LX/0GB;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 35191
    const/4 v1, 0x0

    move v3, v1

    move-object v2, v4

    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_d

    .line 35192
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Ah;

    .line 35193
    if-eqz v5, :cond_9

    iget-object p0, v1, LX/0Ah;->c:LX/0AR;

    iget-object p0, p0, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 35194
    :cond_3
    :goto_2
    if-nez v1, :cond_b

    .line 35195
    :goto_3
    iget-object v1, v2, LX/0Ah;->c:LX/0AR;

    iget-object v1, v1, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/0GB;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 35196
    move-object v0, v2

    .line 35197
    goto :goto_0

    .line 35198
    :cond_4
    new-instance v0, LX/0GF;

    iget-object v1, p0, LX/0GN;->f:Landroid/content/Context;

    iget-object v3, p0, LX/0GN;->e:LX/0GB;

    iget v4, p0, LX/0GN;->o:I

    iget v5, p0, LX/0GN;->p:I

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LX/0GF;-><init>(Landroid/content/Context;Ljava/lang/String;LX/0GB;II)V

    const/4 v2, 0x0

    const/4 p3, 0x1

    const/4 v4, 0x0

    .line 35199
    invoke-static {v0}, LX/0GF;->a(LX/0GF;)I

    move-result v7

    .line 35200
    iget-object v1, v0, LX/0GF;->b:LX/0GB;

    iget-object v3, v0, LX/0GF;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/0GB;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    move v3, v4

    move-object v5, v2

    .line 35201
    :goto_4
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_11

    .line 35202
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Ah;

    .line 35203
    if-lez v7, :cond_8

    .line 35204
    iget-object p1, v1, LX/0Ah;->c:LX/0AR;

    .line 35205
    iget p2, p1, LX/0AR;->f:I

    move p1, p2

    .line 35206
    if-gt p1, v7, :cond_6

    .line 35207
    if-eqz v5, :cond_5

    iget-object p1, v5, LX/0Ah;->c:LX/0AR;

    iget p1, p1, LX/0AR;->c:I

    iget-object p2, v1, LX/0Ah;->c:LX/0AR;

    iget p2, p2, LX/0AR;->c:I

    if-ge p1, p2, :cond_6

    .line 35208
    :cond_5
    new-array p1, p3, [Ljava/lang/Object;

    iget-object p2, v1, LX/0Ah;->c:LX/0AR;

    iget-object p2, p2, LX/0AR;->a:Ljava/lang/String;

    aput-object p2, p1, v4

    .line 35209
    move-object v5, v1

    .line 35210
    :cond_6
    if-eqz v2, :cond_7

    iget-object p1, v2, LX/0Ah;->c:LX/0AR;

    iget p1, p1, LX/0AR;->c:I

    iget-object p2, v1, LX/0Ah;->c:LX/0AR;

    iget p2, p2, LX/0AR;->c:I

    if-le p1, p2, :cond_10

    .line 35211
    :cond_7
    :goto_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_4

    .line 35212
    :cond_8
    if-eqz p0, :cond_e

    .line 35213
    iget-object p1, v1, LX/0Ah;->c:LX/0AR;

    iget-object p1, p1, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 35214
    :goto_6
    if-nez v1, :cond_f

    .line 35215
    new-array v3, p3, [Ljava/lang/Object;

    iget-object v5, v2, LX/0Ah;->c:LX/0AR;

    iget-object v5, v5, LX/0AR;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    .line 35216
    :goto_7
    iget-object v1, v0, LX/0GF;->b:LX/0GB;

    iget-object v3, v0, LX/0GF;->a:Ljava/lang/String;

    iget-object v4, v2, LX/0Ah;->c:LX/0AR;

    iget-object v4, v4, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, LX/0GB;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 35217
    move-object v0, v2

    .line 35218
    goto/16 :goto_0

    .line 35219
    :cond_9
    iget-object p0, v1, LX/0Ah;->c:LX/0AR;

    iget-boolean p0, p0, LX/0AR;->e:Z

    if-nez p0, :cond_3

    .line 35220
    if-eqz v2, :cond_a

    iget-object p0, v2, LX/0Ah;->c:LX/0AR;

    iget p0, p0, LX/0AR;->c:I

    iget-object p2, v1, LX/0Ah;->c:LX/0AR;

    iget p2, p2, LX/0AR;->c:I

    if-le p0, p2, :cond_c

    .line 35221
    :cond_a
    :goto_8
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto/16 :goto_1

    :cond_b
    move-object v2, v1

    goto/16 :goto_3

    :cond_c
    move-object v1, v2

    goto :goto_8

    :cond_d
    move-object v1, v4

    goto/16 :goto_2

    .line 35222
    :cond_e
    iget-object p1, v1, LX/0Ah;->c:LX/0AR;

    iget-boolean p1, p1, LX/0AR;->e:Z

    if-eqz p1, :cond_6

    .line 35223
    new-array v5, p3, [Ljava/lang/Object;

    iget-object v7, v1, LX/0Ah;->c:LX/0AR;

    iget-object v7, v7, LX/0AR;->a:Ljava/lang/String;

    aput-object v7, v5, v4

    .line 35224
    goto :goto_6

    :cond_f
    move-object v2, v1

    goto :goto_7

    :cond_10
    move-object v1, v2

    goto :goto_5

    :cond_11
    move-object v1, v5

    goto :goto_6
.end method

.method private static a(LX/0Ah;I)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 35174
    if-nez p0, :cond_1

    .line 35175
    :cond_0
    :goto_0
    return-object v0

    .line 35176
    :cond_1
    invoke-virtual {p0}, LX/0Ah;->f()LX/0BX;

    move-result-object v1

    .line 35177
    if-eqz v1, :cond_0

    .line 35178
    if-ltz p1, :cond_0

    invoke-interface {v1}, LX/0BX;->a()I

    move-result v2

    if-lt p1, v2, :cond_0

    const-wide/16 v2, 0x0

    invoke-interface {v1, v2, v3}, LX/0BX;->a(J)I

    move-result v2

    if-gt p1, v2, :cond_0

    .line 35179
    invoke-interface {v1, p1}, LX/0BX;->b(I)LX/0Au;

    move-result-object v1

    .line 35180
    if-eqz v1, :cond_0

    .line 35181
    invoke-virtual {v1}, LX/0Au;->a()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/0GN;IIZLX/0AY;Ljava/util/List;)Ljava/lang/String;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZ",
            "LX/0AY;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 35136
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/0GN;->n:Z

    if-nez v2, :cond_0

    .line 35137
    const-string v2, ""

    .line 35138
    :goto_0
    return-object v2

    .line 35139
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 35140
    const-string v2, "-- settings --\n"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35141
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dashLiveEdgeLatencyMs="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35142
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "totalSegmentsToPrefetch="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35143
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "enableLiveAbr="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35144
    const-string v2, "-- manifest --\n"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35145
    invoke-virtual/range {p4 .. p4}, LX/0AY;->b()I

    move-result v5

    .line 35146
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_5

    .line 35147
    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, LX/0AY;->a(I)LX/0Am;

    move-result-object v2

    .line 35148
    iget-object v2, v2, LX/0Am;->c:Ljava/util/List;

    .line 35149
    if-eqz v2, :cond_4

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 35150
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Period "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35151
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Ak;

    .line 35152
    if-eqz v2, :cond_1

    .line 35153
    iget-object v2, v2, LX/0Ak;->c:Ljava/util/List;

    .line 35154
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 35155
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Ah;

    .line 35156
    invoke-virtual {v2}, LX/0Ah;->c()LX/0Au;

    move-result-object v8

    .line 35157
    if-eqz v8, :cond_3

    .line 35158
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "init uri = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, LX/0Au;->a()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35159
    :cond_3
    invoke-virtual {v2}, LX/0Ah;->f()LX/0BX;

    move-result-object v8

    .line 35160
    if-eqz v8, :cond_2

    .line 35161
    invoke-interface {v8}, LX/0BX;->a()I

    move-result v2

    .line 35162
    const-wide/16 v10, 0x0

    invoke-interface {v8, v10, v11}, LX/0BX;->a(J)I

    move-result v9

    .line 35163
    :goto_2
    if-gt v2, v9, :cond_2

    .line 35164
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "seg "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35165
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "timeMs = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v8, v2}, LX/0BX;->a(I)J

    move-result-wide v12

    const-wide/16 v14, 0x3e8

    div-long/2addr v12, v14

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35166
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "durationMs = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v12, 0x0

    invoke-interface {v8, v2, v12, v13}, LX/0BX;->a(IJ)J

    move-result-wide v12

    const-wide/16 v14, 0x3e8

    div-long/2addr v12, v14

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35167
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v8, v2}, LX/0BX;->b(I)LX/0Au;

    move-result-object v11

    invoke-virtual {v11}, LX/0Au;->a()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35168
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 35169
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_1

    .line 35170
    :cond_5
    const-string v2, "-- prefetch list --\n"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35171
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 35172
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 35173
    :cond_6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0
.end method

.method private static a(LX/0GN;LX/0GK;Ljava/lang/String;Landroid/net/Uri;ILjava/util/List;LX/0Gk;Ljava/lang/String;)Ljava/util/Collection;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0GK;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;>;",
            "LX/0Gk;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "LX/0GM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35124
    new-instance v4, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v4, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 35125
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 35126
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move-object/from16 v12, p7

    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/util/Pair;

    .line 35127
    if-nez p4, :cond_1

    .line 35128
    invoke-virtual/range {p3 .. p3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "remote-uri"

    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v6, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v5, "vid"

    move-object/from16 v0, p2

    invoke-virtual {v1, v5, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 35129
    :goto_1
    new-instance v1, LX/0GM;

    iget-object v8, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    iget v11, p0, LX/0GN;->m:I

    const/4 v13, 0x0

    move-object/from16 v2, p1

    move-object/from16 v5, p2

    move-object/from16 v7, p3

    move/from16 v9, p4

    move-object/from16 v10, p6

    invoke-direct/range {v1 .. v13}, LX/0GM;-><init>(LX/0GK;Ljava/util/Collection;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;ILX/0Gk;ILjava/lang/String;B)V

    .line 35130
    const-string v12, ""

    .line 35131
    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 35132
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 35133
    :cond_1
    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/net/Uri;

    move-object v6, v1

    goto :goto_1

    .line 35134
    :cond_2
    iget-object v1, p0, LX/0GN;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v3}, Ljava/util/concurrent/BlockingQueue;->addAll(Ljava/util/Collection;)Z

    .line 35135
    return-object v3
.end method

.method private static a(LX/0GN;LX/0Ah;Ljava/lang/String;ILjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ah;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 35116
    invoke-static {p1, p3}, LX/0GN;->a(LX/0Ah;I)Landroid/net/Uri;

    move-result-object v0

    .line 35117
    if-nez v0, :cond_1

    .line 35118
    :cond_0
    :goto_0
    return-void

    .line 35119
    :cond_1
    iget-boolean v1, p0, LX/0GN;->q:Z

    if-nez v1, :cond_2

    iget-object v1, p0, LX/0GN;->e:LX/0GB;

    invoke-virtual {v1, p2, v0}, LX/0GB;->a(Ljava/lang/String;Landroid/net/Uri;)[B

    move-result-object v1

    if-nez v1, :cond_0

    .line 35120
    :cond_2
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 35121
    new-instance v1, Landroid/util/Pair;

    .line 35122
    iget-object v2, p1, LX/0Ah;->e:Ljava/lang/String;

    move-object v2, v2

    .line 35123
    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static a(LX/0GN;LX/0Ah;Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ah;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 35042
    if-nez p1, :cond_1

    .line 35043
    :cond_0
    :goto_0
    return-void

    .line 35044
    :cond_1
    iget-object v0, p1, LX/0Ah;->f:LX/0Au;

    move-object v0, v0

    .line 35045
    if-eqz v0, :cond_0

    .line 35046
    invoke-virtual {v0}, LX/0Au;->a()Landroid/net/Uri;

    move-result-object v0

    .line 35047
    iget-boolean v1, p0, LX/0GN;->q:Z

    if-nez v1, :cond_2

    iget-object v1, p0, LX/0GN;->e:LX/0GB;

    invoke-virtual {v1, p2, v0}, LX/0GB;->a(Ljava/lang/String;Landroid/net/Uri;)[B

    move-result-object v1

    if-nez v1, :cond_0

    .line 35048
    :cond_2
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 35049
    new-instance v1, Landroid/util/Pair;

    .line 35050
    iget-object v2, p1, LX/0Ah;->e:Ljava/lang/String;

    move-object v2, v2

    .line 35051
    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static a(LX/0GN;Ljava/lang/String;LX/0AY;ILX/0Ak;LX/0Ak;Ljava/util/List;IZ)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0AY;",
            "I",
            "LX/0Ak;",
            "LX/0Ak;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;>;IZ)V"
        }
    .end annotation

    .prologue
    .line 35103
    move/from16 v0, p8

    invoke-static {p0, p1, p4, v0}, LX/0GN;->a(LX/0GN;Ljava/lang/String;LX/0Ak;Z)LX/0Ah;

    move-result-object v3

    .line 35104
    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-static {p0, p1, v0, v2}, LX/0GN;->a(LX/0GN;Ljava/lang/String;LX/0Ak;Z)LX/0Ah;

    move-result-object v4

    .line 35105
    move-object/from16 v0, p6

    invoke-static {p0, v3, p1, v0}, LX/0GN;->a(LX/0GN;LX/0Ah;Ljava/lang/String;Ljava/util/List;)V

    .line 35106
    move-object/from16 v0, p6

    invoke-static {p0, v4, p1, v0}, LX/0GN;->a(LX/0GN;LX/0Ah;Ljava/lang/String;Ljava/util/List;)V

    .line 35107
    mul-int/lit16 v2, p3, 0x3e8

    int-to-long v6, v2

    invoke-static {p2, v3, v6, v7}, LX/0GN;->a(LX/0AY;LX/0Ah;J)I

    move-result v5

    .line 35108
    mul-int/lit16 v2, p3, 0x3e8

    int-to-long v6, v2

    invoke-static {p2, v4, v6, v7}, LX/0GN;->a(LX/0AY;LX/0Ah;J)I

    move-result v6

    .line 35109
    sget-object v2, LX/0GN;->a:Ljava/lang/String;

    const-string v7, "Using align prefetch: %d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v2, v7, v8}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35110
    const/4 v2, 0x0

    :goto_0
    move/from16 v0, p7

    if-ge v2, v0, :cond_2

    .line 35111
    iget-boolean v7, p0, LX/0GN;->s:Z

    if-eqz v7, :cond_0

    iget-object v7, p4, LX/0Ak;->c:Ljava/util/List;

    add-int v8, v5, v2

    invoke-static {p0, v7, p1, v8}, LX/0GN;->a(LX/0GN;Ljava/util/List;Ljava/lang/String;I)Z

    move-result v7

    if-nez v7, :cond_1

    .line 35112
    :cond_0
    add-int v7, v5, v2

    move-object/from16 v0, p6

    invoke-static {p0, v3, p1, v7, v0}, LX/0GN;->a(LX/0GN;LX/0Ah;Ljava/lang/String;ILjava/util/List;)V

    .line 35113
    :cond_1
    add-int v7, v6, v2

    move-object/from16 v0, p6

    invoke-static {p0, v4, p1, v7, v0}, LX/0GN;->a(LX/0GN;LX/0Ah;Ljava/lang/String;ILjava/util/List;)V

    .line 35114
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 35115
    :cond_2
    return-void
.end method

.method private static a(LX/0GN;Ljava/util/List;Ljava/lang/String;I)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0Ah;",
            ">;",
            "Ljava/lang/String;",
            "I)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 35091
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 35092
    invoke-static {v0, p3}, LX/0GN;->a(LX/0Ah;I)Landroid/net/Uri;

    move-result-object v0

    .line 35093
    if-eqz v0, :cond_0

    .line 35094
    invoke-static {p2, v0}, LX/0Gv;->a(Ljava/lang/String;Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 35095
    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    .line 35096
    move v0, v1

    .line 35097
    :goto_0
    return v0

    .line 35098
    :cond_1
    iget-object v4, p0, LX/0GN;->e:LX/0GB;

    invoke-virtual {v4, p2, v0}, LX/0GB;->a(Ljava/lang/String;Landroid/net/Uri;)[B

    move-result-object v4

    if-eqz v4, :cond_0

    .line 35099
    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    .line 35100
    move v0, v1

    .line 35101
    goto :goto_0

    :cond_2
    move v0, v2

    .line 35102
    goto :goto_0
.end method

.method private static b(LX/0GN;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 35083
    iget-object v1, p0, LX/0GN;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35084
    iget v1, p0, LX/0GN;->h:I

    .line 35085
    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    .line 35086
    invoke-static {v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    iput-object v2, p0, LX/0GN;->c:Ljava/util/concurrent/ExecutorService;

    .line 35087
    :goto_0
    if-ge v0, v1, :cond_0

    .line 35088
    iget-object v2, p0, LX/0GN;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;

    invoke-direct {v3, p0, v0}, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;-><init>(LX/0GN;I)V

    const v4, -0x6328d407

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 35089
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35090
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0GK;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/0AY;IIILX/0Gk;Z)Ljava/util/Collection;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0GK;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "LX/0AY;",
            "III",
            "LX/0Gk;",
            "Z)",
            "Ljava/util/Collection",
            "<",
            "LX/0GM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35056
    invoke-virtual {p0, p3}, LX/0GN;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35057
    sget-object v0, LX/0GN;->a:Ljava/lang/String;

    const-string v1, "Prefetch is disabled for origin: %s, videoId: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35058
    sget-object v0, LX/0GN;->t:Ljava/util/Collection;

    .line 35059
    :goto_0
    return-object v0

    .line 35060
    :cond_0
    invoke-virtual {p5}, LX/0AY;->b()I

    move-result v0

    if-gtz v0, :cond_1

    .line 35061
    sget-object v0, LX/0GN;->t:Ljava/util/Collection;

    goto :goto_0

    .line 35062
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p5, v0}, LX/0AY;->a(I)LX/0Am;

    move-result-object v0

    .line 35063
    iget-object v1, v0, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 35064
    sget-object v0, LX/0GN;->t:Ljava/util/Collection;

    goto :goto_0

    .line 35065
    :cond_2
    if-nez p8, :cond_3

    if-nez p4, :cond_3

    .line 35066
    sget-object v0, LX/0GN;->a:Ljava/lang/String;

    const-string v1, "VideoServerBaseUri is not set"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 35067
    sget-object v0, LX/0GN;->t:Ljava/util/Collection;

    goto :goto_0

    .line 35068
    :cond_3
    const/4 v4, 0x0

    .line 35069
    const/4 v5, 0x0

    .line 35070
    iget-object v0, v0, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    .line 35071
    iget-object v3, v0, LX/0Ak;->c:Ljava/util/List;

    .line 35072
    if-nez v4, :cond_5

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x0

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Ah;

    iget-object v1, v1, LX/0Ah;->c:LX/0AR;

    iget-object v1, v1, LX/0AR;->b:Ljava/lang/String;

    const-string v6, "video/"

    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    move-object v4, v0

    .line 35073
    goto :goto_1

    .line 35074
    :cond_5
    if-nez v5, :cond_6

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x0

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Ah;

    iget-object v1, v1, LX/0Ah;->c:LX/0AR;

    iget-object v1, v1, LX/0AR;->b:Ljava/lang/String;

    const-string v3, "audio/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object v5, v0

    .line 35075
    goto :goto_1

    .line 35076
    :cond_6
    if-eqz v5, :cond_4

    if-eqz v4, :cond_4

    .line 35077
    :cond_7
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p5

    move v3, p6

    move/from16 v7, p7

    move/from16 v8, p10

    .line 35078
    invoke-static/range {v0 .. v8}, LX/0GN;->a(LX/0GN;Ljava/lang/String;LX/0AY;ILX/0Ak;LX/0Ak;Ljava/util/List;IZ)V

    .line 35079
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 35080
    invoke-static {p0}, LX/0GN;->b(LX/0GN;)V

    :cond_8
    move-object v1, p0

    move v2, p6

    move/from16 v3, p7

    move/from16 v4, p10

    move-object v5, p5

    .line 35081
    invoke-static/range {v1 .. v6}, LX/0GN;->a(LX/0GN;IIZLX/0AY;Ljava/util/List;)Ljava/lang/String;

    move-result-object v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move/from16 v5, p8

    move-object/from16 v7, p9

    .line 35082
    invoke-static/range {v1 .. v8}, LX/0GN;->a(LX/0GN;LX/0GK;Ljava/lang/String;Landroid/net/Uri;ILjava/util/List;LX/0Gk;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 35053
    if-lez p1, :cond_0

    :goto_0
    iput p1, p0, LX/0GN;->h:I

    .line 35054
    return-void

    .line 35055
    :cond_0
    const/4 p1, 0x2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 35052
    iget-object v0, p0, LX/0GN;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
