.class public LX/06D;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/07D;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "LX/07D;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:Landroid/content/SharedPreferences;

.field private final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17428
    const-class v0, LX/06D;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/06D;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILandroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 17421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17422
    iput p1, p0, LX/06D;->d:I

    .line 17423
    iput-object p2, p0, LX/06D;->e:Landroid/content/SharedPreferences;

    .line 17424
    iput-object p3, p0, LX/06D;->f:Ljava/lang/String;

    .line 17425
    new-instance v0, Ljava/util/TreeSet;

    new-instance v1, LX/06E;

    invoke-direct {v1, p0}, LX/06E;-><init>(LX/06D;)V

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, LX/06D;->c:Ljava/util/TreeSet;

    .line 17426
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/06D;->a:Ljava/util/List;

    .line 17427
    return-void
.end method

.method private c()Ljava/lang/String;
    .locals 7

    .prologue
    .line 17404
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 17405
    iget-object v0, p0, LX/06D;->a:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 17406
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 17407
    iget-object v0, p0, LX/06D;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/07D;

    .line 17408
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 17409
    const-string v4, "host_name"

    iget-object v6, v0, LX/07D;->a:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 17410
    const-string v4, "priority"

    iget v6, v0, LX/07D;->b:I

    invoke-virtual {v5, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 17411
    const-string v4, "fail_count"

    iget v6, v0, LX/07D;->c:I

    invoke-virtual {v5, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 17412
    iget-object v4, v0, LX/07D;->d:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 17413
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 17414
    iget-object v4, v0, LX/07D;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 17415
    invoke-virtual {v6, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 17416
    :cond_0
    const-string v4, "address_list_data"

    invoke-virtual {v5, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 17417
    :cond_1
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 17418
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 17419
    :cond_2
    const-string v0, "address_entries"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 17420
    :cond_3
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized d()V
    .locals 3
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 17399
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06D;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 17400
    invoke-virtual {p0}, LX/06D;->a()Ljava/util/TreeSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/07D;

    .line 17401
    iget-object v2, p0, LX/06D;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 17402
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 17403
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/TreeSet;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/TreeSet",
            "<",
            "LX/07D;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 17345
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/06D;->c:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/06D;->e:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/06D;->e:Landroid/content/SharedPreferences;

    iget-object v2, p0, LX/06D;->f:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 17346
    iget-object v1, p0, LX/06D;->e:Landroid/content/SharedPreferences;

    iget-object v2, p0, LX/06D;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 17347
    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 17348
    const-string v1, "address_entries"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 17349
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 17350
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 17351
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 17352
    invoke-static {v2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 17353
    const/4 v3, 0x0

    .line 17354
    :goto_1
    move-object v2, v3

    .line 17355
    iget-object v3, v2, LX/07D;->d:Ljava/util/List;

    if-eqz v3, :cond_6

    iget-object v3, v2, LX/07D;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v2}, LX/07D;->e()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    const/4 v3, 0x1

    :goto_2
    move v3, v3

    .line 17356
    if-eqz v3, :cond_0

    .line 17357
    invoke-virtual {p0, v2}, LX/06D;->a(LX/07D;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17358
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 17359
    :catch_0
    move-exception v0

    .line 17360
    :try_start_2
    sget-object v1, LX/06D;->b:Ljava/lang/String;

    const-string v2, "Cannot create JSONObject from rawJson"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17361
    :cond_1
    iget-object v0, p0, LX/06D;->c:Ljava/util/TreeSet;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 17362
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 17363
    :cond_2
    :try_start_3
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 17364
    new-instance v4, LX/07D;

    invoke-direct {v4}, LX/07D;-><init>()V

    .line 17365
    const-string v5, "host_name"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/07D;->a:Ljava/lang/String;

    .line 17366
    const-string v5, "priority"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v4, LX/07D;->b:I

    .line 17367
    const-string v5, "fail_count"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v4, LX/07D;->c:I

    .line 17368
    const-string v5, "address_list_data"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 17369
    if-eqz v5, :cond_5

    .line 17370
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v3

    invoke-direct {v6, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 17371
    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_4

    .line 17372
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->isNull(I)Z

    move-result v7

    if-nez v7, :cond_3

    .line 17373
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17374
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 17375
    :cond_4
    iput-object v6, v4, LX/07D;->d:Ljava/util/List;

    :cond_5
    move-object v3, v4

    .line 17376
    goto :goto_1
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final declared-synchronized a(LX/07D;LX/07D;)V
    .locals 1

    .prologue
    .line 17395
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06D;->c:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 17396
    invoke-virtual {p0, p2}, LX/06D;->a(LX/07D;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17397
    monitor-exit p0

    return-void

    .line 17398
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/07D;)Z
    .locals 2

    .prologue
    .line 17388
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06D;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    iget v1, p0, LX/06D;->d:I

    if-lt v0, v1, :cond_0

    .line 17389
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    .line 17390
    iget-object v0, p0, LX/06D;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->pollLast()Ljava/lang/Object;

    .line 17391
    :cond_0
    :goto_0
    iget-object v0, p0, LX/06D;->c:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 17392
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/06D;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 17393
    iget-object v0, p0, LX/06D;->c:Ljava/util/TreeSet;

    iget-object v1, p0, LX/06D;->c:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 17394
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/07D;)LX/07D;
    .locals 3

    .prologue
    .line 17384
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/06D;->a()Ljava/util/TreeSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/07D;

    .line 17385
    invoke-virtual {v0, p1}, LX/07D;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 17386
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 17387
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 17377
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/06D;->d()V

    .line 17378
    iget-object v0, p0, LX/06D;->e:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 17379
    :try_start_1
    iget-object v0, p0, LX/06D;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, LX/06D;->f:Ljava/lang/String;

    invoke-direct {p0}, LX/06D;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17380
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 17381
    :catch_0
    move-exception v0

    .line 17382
    :try_start_2
    sget-object v1, LX/06D;->b:Ljava/lang/String;

    const-string v2, "Failed to save addressEntries"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 17383
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
