.class public LX/0Jc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0Jc;


# instance fields
.field public a:I

.field public b:I

.field public c:LX/0J9;

.field public d:LX/0JP;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 39567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39568
    sget-object v0, LX/0J9;->NOT_PREFETCHED:LX/0J9;

    iput-object v0, p0, LX/0Jc;->c:LX/0J9;

    .line 39569
    sget-object v0, LX/0JP;->INIT:LX/0JP;

    iput-object v0, p0, LX/0Jc;->d:LX/0JP;

    .line 39570
    return-void
.end method

.method public static a(LX/0QB;)LX/0Jc;
    .locals 3

    .prologue
    .line 39571
    sget-object v0, LX/0Jc;->e:LX/0Jc;

    if-nez v0, :cond_1

    .line 39572
    const-class v1, LX/0Jc;

    monitor-enter v1

    .line 39573
    :try_start_0
    sget-object v0, LX/0Jc;->e:LX/0Jc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 39574
    if-eqz v2, :cond_0

    .line 39575
    :try_start_1
    new-instance v0, LX/0Jc;

    invoke-direct {v0}, LX/0Jc;-><init>()V

    .line 39576
    move-object v0, v0

    .line 39577
    sput-object v0, LX/0Jc;->e:LX/0Jc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 39578
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 39579
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 39580
    :cond_1
    sget-object v0, LX/0Jc;->e:LX/0Jc;

    return-object v0

    .line 39581
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 39582
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
