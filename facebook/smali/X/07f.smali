.class public LX/07f;
.super LX/07c;
.source ""


# instance fields
.field public final c:LX/05K;

.field public final d:Ljava/lang/Object;

.field public final e:I


# direct methods
.method public constructor <init>(LX/07R;Ljava/lang/Object;IILX/05K;)V
    .locals 0

    .prologue
    .line 20087
    invoke-direct {p0, p1, p3}, LX/07c;-><init>(LX/07R;I)V

    .line 20088
    iput-object p2, p0, LX/07f;->d:Ljava/lang/Object;

    .line 20089
    iput p4, p0, LX/07f;->e:I

    .line 20090
    iput-object p5, p0, LX/07f;->c:LX/05K;

    .line 20091
    return-void
.end method


# virtual methods
.method public final c(Ljava/io/DataInputStream;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 20092
    sget-object v0, LX/07g;->a:[I

    iget-object v1, p0, LX/07c;->a:LX/07R;

    iget-object v1, v1, LX/07R;->a:LX/07S;

    invoke-virtual {v1}, LX/07S;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 20093
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 20094
    :pswitch_0
    const/4 v6, 0x0

    .line 20095
    iget-object v2, p0, LX/07f;->d:Ljava/lang/Object;

    check-cast v2, LX/07U;

    .line 20096
    invoke-virtual {p0, p1}, LX/07c;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v3

    .line 20097
    iget-boolean v4, v2, LX/07U;->d:Z

    if-eqz v4, :cond_a

    .line 20098
    invoke-virtual {p0, p1}, LX/07c;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v4

    .line 20099
    invoke-virtual {p0, p1}, LX/07c;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v5

    .line 20100
    :goto_1
    iget v7, p0, LX/07c;->b:I

    if-lez v7, :cond_9

    .line 20101
    iget-boolean v7, v2, LX/07U;->b:Z

    if-eqz v7, :cond_8

    .line 20102
    invoke-virtual {p0, p1}, LX/07c;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/078;->a(Ljava/lang/String;)LX/078;

    move-result-object v7

    .line 20103
    :goto_2
    iget-boolean v2, v2, LX/07U;->c:Z

    if-eqz v2, :cond_7

    .line 20104
    invoke-virtual {p0, p1}, LX/07c;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v6

    move-object v9, v6

    move-object v6, v7

    move-object v7, v9

    .line 20105
    :goto_3
    new-instance v2, LX/079;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, LX/079;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/078;Ljava/lang/String;Ljava/util/List;)V

    move-object v0, v2

    .line 20106
    goto :goto_0

    .line 20107
    :pswitch_1
    const/4 v0, 0x0

    .line 20108
    iget v1, p0, LX/07c;->b:I

    if-lez v1, :cond_0

    .line 20109
    invoke-virtual {p0, p1}, LX/07c;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v0

    .line 20110
    :cond_0
    invoke-static {v0}, LX/07h;->a(Ljava/lang/String;)LX/07h;

    move-result-object v0

    move-object v0, v0

    .line 20111
    goto :goto_0

    .line 20112
    :pswitch_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 20113
    :goto_4
    iget v1, p0, LX/07c;->b:I

    if-lez v1, :cond_1

    .line 20114
    invoke-virtual {p0, p1}, LX/07c;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v1

    .line 20115
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v2

    .line 20116
    iget v3, p0, LX/07c;->b:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, LX/07f;->b:I

    .line 20117
    new-instance v3, LX/0AF;

    invoke-direct {v3, v1, v2}, LX/0AF;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 20118
    :cond_1
    new-instance v1, LX/0I1;

    invoke-direct {v1, v0}, LX/0I1;-><init>(Ljava/util/List;)V

    move-object v0, v1

    .line 20119
    goto :goto_0

    .line 20120
    :pswitch_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 20121
    :goto_5
    iget v1, p0, LX/07c;->b:I

    if-lez v1, :cond_2

    .line 20122
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v1

    and-int/lit8 v1, v1, -0x4

    .line 20123
    iget v2, p0, LX/07c;->b:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, LX/07f;->b:I

    .line 20124
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 20125
    :cond_2
    new-instance v1, LX/0Hz;

    invoke-direct {v1, v0}, LX/0Hz;-><init>(Ljava/util/List;)V

    move-object v0, v1

    .line 20126
    goto/16 :goto_0

    .line 20127
    :pswitch_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 20128
    :goto_6
    iget v1, p0, LX/07c;->b:I

    if-lez v1, :cond_3

    .line 20129
    invoke-virtual {p0, p1}, LX/07c;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v1

    .line 20130
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 20131
    :cond_3
    new-instance v1, LX/0I4;

    invoke-direct {v1, v0}, LX/0I4;-><init>(Ljava/util/List;)V

    move-object v0, v1

    .line 20132
    goto/16 :goto_0

    .line 20133
    :pswitch_5
    iget v0, p0, LX/07c;->b:I

    new-array v0, v0, [B

    .line 20134
    invoke-virtual {p1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    .line 20135
    const/4 v1, 0x0

    iput v1, p0, LX/07f;->b:I

    .line 20136
    const/4 v1, 0x1

    iget v2, p0, LX/07f;->e:I

    if-eq v1, v2, :cond_4

    const/4 v1, 0x2

    iget v2, p0, LX/07f;->e:I

    if-ne v1, v2, :cond_6

    iget-object v1, p0, LX/07c;->a:LX/07R;

    .line 20137
    iget-boolean v2, v1, LX/07R;->e:Z

    move v1, v2

    .line 20138
    if-nez v1, :cond_6

    .line 20139
    :cond_4
    const/4 p1, 0x0

    .line 20140
    new-instance v1, Ljava/util/zip/Inflater;

    invoke-direct {v1}, Ljava/util/zip/Inflater;-><init>()V

    .line 20141
    array-length v2, v0

    invoke-virtual {v1, v0, p1, v2}, Ljava/util/zip/Inflater;->setInput([BII)V

    .line 20142
    array-length v2, v0

    mul-int/lit8 v2, v2, 0x2

    .line 20143
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 20144
    new-array v4, v2, [B

    .line 20145
    :goto_7
    invoke-virtual {v1}, Ljava/util/zip/Inflater;->finished()Z

    move-result p0

    if-nez p0, :cond_5

    .line 20146
    invoke-virtual {v1, v4, p1, v2}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result p0

    .line 20147
    invoke-virtual {v3, v4, p1, p0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_7

    .line 20148
    :cond_5
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 20149
    invoke-virtual {v1}, Ljava/util/zip/Inflater;->end()V

    .line 20150
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    move-object v0, v1

    .line 20151
    :cond_6
    move-object v0, v0

    .line 20152
    goto/16 :goto_0

    :cond_7
    move-object v9, v6

    move-object v6, v7

    move-object v7, v9

    goto/16 :goto_3

    :cond_8
    move-object v7, v6

    goto/16 :goto_2

    :cond_9
    move-object v7, v6

    goto/16 :goto_3

    :cond_a
    move-object v5, v6

    move-object v4, v6

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
