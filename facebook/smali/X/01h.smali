.class public final LX/01h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Closeable;",
        "Ljava/util/Iterator",
        "<",
        "LX/04Z;",
        ">;"
    }
.end annotation


# instance fields
.field private mCurrent:LX/04Z;

.field public final mDescriptors:[LX/01g;

.field private mPosition:I

.field public final synthetic this$0:LX/01e;


# direct methods
.method public constructor <init>(LX/01e;[LX/01g;)V
    .locals 1

    .prologue
    .line 5099
    iput-object p1, p0, LX/01h;->this$0:LX/01e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5100
    iput-object p2, p0, LX/01h;->mDescriptors:[LX/01g;

    .line 5101
    const/4 v0, 0x0

    iput v0, p0, LX/01h;->mPosition:I

    .line 5102
    const/4 v0, 0x0

    iput-object v0, p0, LX/01h;->mCurrent:LX/04Z;

    .line 5103
    return-void
.end method

.method private tryOpenFileForConsumption(LX/01g;)LX/04Z;
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 5051
    const/4 v2, 0x0

    .line 5052
    iget-object v4, p1, LX/01g;->fileName:Ljava/io/File;

    .line 5053
    :try_start_0
    iget-object v3, p0, LX/01h;->this$0:LX/01e;

    monitor-enter v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    .line 5054
    :try_start_1
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    iget-object v1, v1, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5055
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 5056
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    invoke-virtual {v1, v0}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    .line 5057
    :cond_0
    :goto_0
    return-object v0

    .line 5058
    :cond_1
    :try_start_2
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    iget-object v1, v1, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 5059
    const/4 v2, 0x1

    .line 5060
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 5061
    :try_start_3
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v1, "rw"

    invoke-direct {v3, v4, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    .line 5062
    :try_start_4
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    invoke-virtual {v1, v3}, LX/01e;->tryLock(Ljava/io/RandomAccessFile;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_7

    move-result v1

    if-nez v1, :cond_2

    .line 5063
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    invoke-virtual {v1, v3}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    .line 5064
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    monitor-enter v1

    .line 5065
    :try_start_5
    iget-object v2, p0, LX/01h;->this$0:LX/01e;

    iget-object v2, v2, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 5066
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    .line 5067
    :catchall_1
    move-exception v1

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v1
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    .line 5068
    :catch_0
    move-exception v1

    move v3, v2

    move-object v2, v0

    .line 5069
    :goto_1
    :try_start_8
    const-string v5, "Spool"

    const-string v6, "unexpected failure opening %s: deleting and continuing scan"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5070
    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    .line 5071
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    invoke-virtual {v1, v2}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    .line 5072
    if-eqz v3, :cond_0

    .line 5073
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    monitor-enter v1

    .line 5074
    :try_start_9
    iget-object v2, p0, LX/01h;->this$0:LX/01e;

    iget-object v2, v2, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 5075
    monitor-exit v1

    goto :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v0

    .line 5076
    :cond_2
    :try_start_a
    invoke-virtual {v4}, Ljava/io/File;->exists()Z
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_7

    move-result v1

    if-nez v1, :cond_3

    .line 5077
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    invoke-virtual {v1, v3}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    .line 5078
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    monitor-enter v1

    .line 5079
    :try_start_b
    iget-object v2, p0, LX/01h;->this$0:LX/01e;

    iget-object v2, v2, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 5080
    monitor-exit v1

    goto :goto_0

    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    throw v0

    .line 5081
    :cond_3
    :try_start_c
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-nez v1, :cond_4

    .line 5082
    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_7

    .line 5083
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    invoke-virtual {v1, v3}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    .line 5084
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    monitor-enter v1

    .line 5085
    :try_start_d
    iget-object v2, p0, LX/01h;->this$0:LX/01e;

    iget-object v2, v2, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 5086
    monitor-exit v1

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    throw v0

    .line 5087
    :cond_4
    :try_start_e
    new-instance v1, LX/04Z;

    iget-object v5, p0, LX/01h;->this$0:LX/01e;

    invoke-direct {v1, v5, v4, v3}, LX/04Z;-><init>(LX/01e;Ljava/io/File;Ljava/io/RandomAccessFile;)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    .line 5088
    iget-object v2, p0, LX/01h;->this$0:LX/01e;

    invoke-virtual {v2, v0}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    move-object v0, v1

    .line 5089
    goto/16 :goto_0

    .line 5090
    :catchall_5
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    invoke-virtual {v1, v3}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    .line 5091
    if-eqz v2, :cond_5

    .line 5092
    iget-object v1, p0, LX/01h;->this$0:LX/01e;

    monitor-enter v1

    .line 5093
    :try_start_f
    iget-object v2, p0, LX/01h;->this$0:LX/01e;

    iget-object v2, v2, LX/01e;->mHazardList:Ljava/util/HashSet;

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 5094
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    :cond_5
    throw v0

    :catchall_6
    move-exception v0

    :try_start_10
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_6

    throw v0

    .line 5095
    :catchall_7
    move-exception v0

    goto :goto_2

    :catchall_8
    move-exception v0

    move-object v10, v2

    move v2, v3

    move-object v3, v10

    goto :goto_2

    .line 5096
    :catch_1
    move-exception v1

    move-object v10, v3

    move v3, v2

    move-object v2, v10

    goto/16 :goto_1
.end method

.method private updateCurrent()V
    .locals 3

    .prologue
    .line 5048
    :goto_0
    iget-object v0, p0, LX/01h;->mCurrent:LX/04Z;

    if-nez v0, :cond_0

    iget v0, p0, LX/01h;->mPosition:I

    iget-object v1, p0, LX/01h;->mDescriptors:[LX/01g;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 5049
    iget-object v0, p0, LX/01h;->mDescriptors:[LX/01g;

    iget v1, p0, LX/01h;->mPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/01h;->mPosition:I

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, LX/01h;->tryOpenFileForConsumption(LX/01g;)LX/04Z;

    move-result-object v0

    iput-object v0, p0, LX/01h;->mCurrent:LX/04Z;

    goto :goto_0

    .line 5050
    :cond_0
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 2

    .prologue
    .line 5097
    iget-object v0, p0, LX/01h;->this$0:LX/01e;

    iget-object v1, p0, LX/01h;->mCurrent:LX/04Z;

    invoke-virtual {v0, v1}, LX/01e;->closeSilently(Ljava/io/Closeable;)V

    .line 5098
    return-void
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 5046
    invoke-direct {p0}, LX/01h;->updateCurrent()V

    .line 5047
    iget-object v0, p0, LX/01h;->mCurrent:LX/04Z;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()LX/04Z;
    .locals 2

    .prologue
    .line 5040
    invoke-direct {p0}, LX/01h;->updateCurrent()V

    .line 5041
    iget-object v0, p0, LX/01h;->mCurrent:LX/04Z;

    if-nez v0, :cond_0

    .line 5042
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 5043
    :cond_0
    iget-object v0, p0, LX/01h;->mCurrent:LX/04Z;

    .line 5044
    const/4 v1, 0x0

    iput-object v1, p0, LX/01h;->mCurrent:LX/04Z;

    .line 5045
    return-object v0
.end method

.method public final bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5039
    invoke-virtual {p0}, LX/01h;->next()LX/04Z;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 5038
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
