.class public final LX/0Nq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Aa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Aa",
        "<",
        "LX/0Nk;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;

.field private static final d:Ljava/util/regex/Pattern;

.field private static final e:Ljava/util/regex/Pattern;

.field private static final f:Ljava/util/regex/Pattern;

.field private static final g:Ljava/util/regex/Pattern;

.field private static final h:Ljava/util/regex/Pattern;

.field private static final i:Ljava/util/regex/Pattern;

.field private static final j:Ljava/util/regex/Pattern;

.field private static final k:Ljava/util/regex/Pattern;

.field private static final l:Ljava/util/regex/Pattern;

.field private static final m:Ljava/util/regex/Pattern;

.field private static final n:Ljava/util/regex/Pattern;

.field private static final o:Ljava/util/regex/Pattern;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51625
    const-string v0, "BANDWIDTH=(\\d+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->a:Ljava/util/regex/Pattern;

    .line 51626
    const-string v0, "CODECS=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->b:Ljava/util/regex/Pattern;

    .line 51627
    const-string v0, "RESOLUTION=(\\d+x\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->c:Ljava/util/regex/Pattern;

    .line 51628
    const-string v0, "#EXTINF:([\\d.]+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->d:Ljava/util/regex/Pattern;

    .line 51629
    const-string v0, "#EXT-X-MEDIA-SEQUENCE:(\\d+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->e:Ljava/util/regex/Pattern;

    .line 51630
    const-string v0, "#EXT-X-TARGETDURATION:(\\d+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->f:Ljava/util/regex/Pattern;

    .line 51631
    const-string v0, "#EXT-X-VERSION:(\\d+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->g:Ljava/util/regex/Pattern;

    .line 51632
    const-string v0, "#EXT-X-BYTERANGE:(\\d+(?:@\\d+)?)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->h:Ljava/util/regex/Pattern;

    .line 51633
    const-string v0, "METHOD=(NONE|AES-128)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->i:Ljava/util/regex/Pattern;

    .line 51634
    const-string v0, "URI=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->j:Ljava/util/regex/Pattern;

    .line 51635
    const-string v0, "IV=([^,.*]+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->k:Ljava/util/regex/Pattern;

    .line 51636
    const-string v0, "TYPE=(AUDIO|VIDEO|SUBTITLES|CLOSED-CAPTIONS)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->l:Ljava/util/regex/Pattern;

    .line 51637
    const-string v0, "LANGUAGE=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->m:Ljava/util/regex/Pattern;

    .line 51638
    const-string v0, "NAME=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->n:Ljava/util/regex/Pattern;

    .line 51639
    const-string v0, "INSTREAM-ID=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nq;->o:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51641
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/io/InputStream;)LX/0Nk;
    .locals 4

    .prologue
    .line 51642
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 51643
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 51644
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 51645
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 51646
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 51647
    const-string v3, "#EXT-X-STREAM-INF"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 51648
    invoke-interface {v0, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 51649
    new-instance v2, LX/0Np;

    invoke-direct {v2, v0, v1}, LX/0Np;-><init>(Ljava/util/Queue;Ljava/io/BufferedReader;)V

    invoke-static {v2, p0}, LX/0Nq;->a(LX/0Np;Ljava/lang/String;)LX/0Nl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 51650
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    :goto_1
    return-object v0

    .line 51651
    :cond_1
    :try_start_1
    const-string v3, "#EXT-X-TARGETDURATION"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXT-X-MEDIA-SEQUENCE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXTINF"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXT-X-KEY"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXT-X-BYTERANGE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXT-X-DISCONTINUITY"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXT-X-DISCONTINUITY-SEQUENCE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXT-X-ENDLIST"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 51652
    :cond_2
    invoke-interface {v0, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 51653
    new-instance v2, LX/0Np;

    invoke-direct {v2, v0, v1}, LX/0Np;-><init>(Ljava/util/Queue;Ljava/io/BufferedReader;)V

    invoke-static {v2, p0}, LX/0Nq;->b(LX/0Np;Ljava/lang/String;)LX/0Nn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 51654
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    goto :goto_1

    .line 51655
    :cond_3
    :try_start_2
    invoke-interface {v0, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 51656
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    throw v0

    :cond_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 51657
    new-instance v0, LX/0L6;

    const-string v1, "Failed to parse the playlist, could not identify any tags."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(LX/0Np;Ljava/lang/String;)LX/0Nl;
    .locals 25

    .prologue
    .line 51658
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 51659
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 51660
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 51661
    const/4 v7, 0x0

    .line 51662
    const/4 v11, 0x0

    .line 51663
    const/4 v6, -0x1

    .line 51664
    const/4 v5, -0x1

    .line 51665
    const/4 v4, 0x0

    .line 51666
    const/4 v3, 0x0

    .line 51667
    const/4 v2, 0x0

    .line 51668
    const/4 v1, 0x0

    move v13, v1

    move-object v14, v2

    move-object v15, v3

    move-object/from16 v16, v4

    move/from16 v17, v5

    move/from16 v18, v6

    move/from16 v19, v7

    .line 51669
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, LX/0Np;->a()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 51670
    invoke-virtual/range {p0 .. p0}, LX/0Np;->b()Ljava/lang/String;

    move-result-object v23

    .line 51671
    const-string v1, "#EXT-X-MEDIA"

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 51672
    sget-object v1, LX/0Nq;->l:Ljava/util/regex/Pattern;

    const-string v2, "TYPE"

    move-object/from16 v0, v23

    invoke-static {v0, v1, v2}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 51673
    const-string v2, "CLOSED-CAPTIONS"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 51674
    sget-object v1, LX/0Nq;->o:Ljava/util/regex/Pattern;

    const-string v2, "INSTREAM-ID"

    move-object/from16 v0, v23

    invoke-static {v0, v1, v2}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 51675
    const-string v2, "CC1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 51676
    sget-object v1, LX/0Nq;->m:Ljava/util/regex/Pattern;

    move-object/from16 v0, v23

    invoke-static {v0, v1}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object v14, v1

    .line 51677
    goto :goto_0

    :cond_1
    const-string v2, "SUBTITLES"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 51678
    sget-object v1, LX/0Nq;->n:Ljava/util/regex/Pattern;

    const-string v2, "NAME"

    move-object/from16 v0, v23

    invoke-static {v0, v1, v2}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 51679
    sget-object v1, LX/0Nq;->j:Ljava/util/regex/Pattern;

    const-string v3, "URI"

    move-object/from16 v0, v23

    invoke-static {v0, v1, v3}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 51680
    sget-object v1, LX/0Nq;->m:Ljava/util/regex/Pattern;

    move-object/from16 v0, v23

    invoke-static {v0, v1}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v10

    .line 51681
    new-instance v1, LX/0AR;

    const-string v3, "application/x-mpegURL"

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v7, -0x1

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/4 v12, 0x0

    invoke-direct/range {v1 .. v12}, LX/0AR;-><init>(Ljava/lang/String;Ljava/lang/String;IIFIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51682
    new-instance v2, LX/0Nv;

    move-object/from16 v0, v24

    invoke-direct {v2, v0, v1}, LX/0Nv;-><init>(Ljava/lang/String;LX/0AR;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 51683
    :cond_2
    const-string v2, "AUDIO"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 51684
    sget-object v1, LX/0Nq;->m:Ljava/util/regex/Pattern;

    move-object/from16 v0, v23

    invoke-static {v0, v1}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v10

    .line 51685
    sget-object v1, LX/0Nq;->j:Ljava/util/regex/Pattern;

    move-object/from16 v0, v23

    invoke-static {v0, v1}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v24

    .line 51686
    if-eqz v24, :cond_3

    .line 51687
    sget-object v1, LX/0Nq;->n:Ljava/util/regex/Pattern;

    const-string v2, "NAME"

    move-object/from16 v0, v23

    invoke-static {v0, v1, v2}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 51688
    new-instance v1, LX/0AR;

    const-string v3, "application/x-mpegURL"

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v7, -0x1

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/4 v12, 0x0

    invoke-direct/range {v1 .. v12}, LX/0AR;-><init>(Ljava/lang/String;Ljava/lang/String;IIFIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51689
    new-instance v2, LX/0Nv;

    move-object/from16 v0, v24

    invoke-direct {v2, v0, v1}, LX/0Nv;-><init>(Ljava/lang/String;LX/0AR;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    move-object v1, v10

    :goto_2
    move-object v15, v1

    .line 51690
    goto/16 :goto_0

    :cond_4
    const-string v1, "#EXT-X-STREAM-INF"

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 51691
    sget-object v1, LX/0Nq;->a:Ljava/util/regex/Pattern;

    const-string v2, "BANDWIDTH"

    move-object/from16 v0, v23

    invoke-static {v0, v1, v2}, LX/0No;->b(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)I

    move-result v5

    .line 51692
    sget-object v1, LX/0Nq;->b:Ljava/util/regex/Pattern;

    move-object/from16 v0, v23

    invoke-static {v0, v1}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v11

    .line 51693
    sget-object v1, LX/0Nq;->n:Ljava/util/regex/Pattern;

    move-object/from16 v0, v23

    invoke-static {v0, v1}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v4

    .line 51694
    sget-object v1, LX/0Nq;->c:Ljava/util/regex/Pattern;

    move-object/from16 v0, v23

    invoke-static {v0, v1}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v1

    .line 51695
    if-eqz v1, :cond_7

    .line 51696
    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 51697
    const/4 v1, 0x0

    aget-object v1, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 51698
    if-gtz v1, :cond_5

    .line 51699
    const/4 v1, -0x1

    .line 51700
    :cond_5
    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 51701
    if-gtz v2, :cond_6

    .line 51702
    const/4 v2, -0x1

    .line 51703
    :cond_6
    :goto_3
    const/4 v3, 0x1

    move v13, v3

    move-object/from16 v16, v4

    move/from16 v17, v2

    move/from16 v18, v1

    move/from16 v19, v5

    .line 51704
    goto/16 :goto_0

    .line 51705
    :cond_7
    const/4 v1, -0x1

    .line 51706
    const/4 v2, -0x1

    goto :goto_3

    .line 51707
    :cond_8
    const-string v1, "#"

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v13, :cond_0

    .line 51708
    if-nez v16, :cond_a

    .line 51709
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 51710
    :goto_4
    new-instance v1, LX/0AR;

    const-string v3, "application/x-mpegURL"

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v7, -0x1

    const/4 v8, -0x1

    const/4 v10, 0x0

    const/4 v12, 0x0

    move/from16 v4, v18

    move/from16 v5, v17

    move/from16 v9, v19

    invoke-direct/range {v1 .. v12}, LX/0AR;-><init>(Ljava/lang/String;Ljava/lang/String;IIFIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51711
    new-instance v2, LX/0Nv;

    move-object/from16 v0, v23

    invoke-direct {v2, v0, v1}, LX/0Nv;-><init>(Ljava/lang/String;LX/0AR;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51712
    const/4 v5, 0x0

    .line 51713
    const/4 v11, 0x0

    .line 51714
    const/4 v2, 0x0

    .line 51715
    const/4 v4, -0x1

    .line 51716
    const/4 v3, -0x1

    .line 51717
    const/4 v1, 0x0

    move v13, v1

    move-object/from16 v16, v2

    move/from16 v17, v3

    move/from16 v18, v4

    move/from16 v19, v5

    .line 51718
    goto/16 :goto_0

    .line 51719
    :cond_9
    new-instance v1, LX/0Nl;

    move-object/from16 v2, p1

    move-object/from16 v3, v20

    move-object/from16 v4, v21

    move-object/from16 v5, v22

    move-object v6, v15

    move-object v7, v14

    invoke-direct/range {v1 .. v7}, LX/0Nl;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_a
    move-object/from16 v2, v16

    goto :goto_4

    :cond_b
    move-object v1, v15

    goto/16 :goto_2

    :cond_c
    move-object v1, v14

    goto/16 :goto_1
.end method

.method private static b(LX/0Np;Ljava/lang/String;)LX/0Nn;
    .locals 25

    .prologue
    .line 51720
    const/16 v17, 0x0

    .line 51721
    const/16 v16, 0x0

    .line 51722
    const/4 v11, 0x1

    .line 51723
    const/16 v23, 0x1

    .line 51724
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 51725
    const-wide/16 v12, 0x0

    .line 51726
    const/4 v6, 0x0

    .line 51727
    const-wide/16 v7, 0x0

    .line 51728
    const-wide/16 v4, 0x0

    .line 51729
    const-wide/16 v14, -0x1

    .line 51730
    const/4 v3, 0x0

    .line 51731
    const/4 v9, 0x0

    .line 51732
    const/4 v10, 0x0

    .line 51733
    const/4 v2, 0x0

    move-wide/from16 v18, v4

    move/from16 v20, v11

    move/from16 v21, v16

    move/from16 v22, v17

    move-object/from16 v16, v2

    move-wide v4, v12

    move v12, v3

    .line 51734
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, LX/0Np;->a()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 51735
    invoke-virtual/range {p0 .. p0}, LX/0Np;->b()Ljava/lang/String;

    move-result-object v3

    .line 51736
    const-string v2, "#EXT-X-TARGETDURATION"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 51737
    sget-object v2, LX/0Nq;->f:Ljava/util/regex/Pattern;

    const-string v11, "#EXT-X-TARGETDURATION"

    invoke-static {v3, v2, v11}, LX/0No;->b(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto :goto_0

    .line 51738
    :cond_1
    const-string v2, "#EXT-X-MEDIA-SEQUENCE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 51739
    sget-object v2, LX/0Nq;->e:Ljava/util/regex/Pattern;

    const-string v11, "#EXT-X-MEDIA-SEQUENCE"

    invoke-static {v3, v2, v11}, LX/0No;->b(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)I

    move-result v2

    move v12, v2

    move/from16 v22, v2

    .line 51740
    goto :goto_0

    .line 51741
    :cond_2
    const-string v2, "#EXT-X-VERSION"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 51742
    sget-object v2, LX/0Nq;->g:Ljava/util/regex/Pattern;

    const-string v11, "#EXT-X-VERSION"

    invoke-static {v3, v2, v11}, LX/0No;->b(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto :goto_0

    .line 51743
    :cond_3
    const-string v2, "#EXTINF"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 51744
    sget-object v2, LX/0Nq;->d:Ljava/util/regex/Pattern;

    const-string v4, "#EXTINF"

    invoke-static {v3, v2, v4}, LX/0No;->c(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)D

    move-result-wide v4

    goto :goto_0

    .line 51745
    :cond_4
    const-string v2, "#EXT-X-KEY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 51746
    sget-object v2, LX/0Nq;->i:Ljava/util/regex/Pattern;

    const-string v9, "METHOD"

    invoke-static {v3, v2, v9}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 51747
    const-string v9, "AES-128"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 51748
    if-eqz v9, :cond_5

    .line 51749
    sget-object v2, LX/0Nq;->j:Ljava/util/regex/Pattern;

    const-string v10, "URI"

    invoke-static {v3, v2, v10}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 51750
    sget-object v2, LX/0Nq;->k:Ljava/util/regex/Pattern;

    invoke-static {v3, v2}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v16, v2

    goto :goto_0

    .line 51751
    :cond_5
    const/4 v10, 0x0

    .line 51752
    const/4 v2, 0x0

    move-object/from16 v16, v2

    .line 51753
    goto/16 :goto_0

    :cond_6
    const-string v2, "#EXT-X-BYTERANGE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 51754
    sget-object v2, LX/0Nq;->h:Ljava/util/regex/Pattern;

    const-string v11, "#EXT-X-BYTERANGE"

    invoke-static {v3, v2, v11}, LX/0No;->a(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 51755
    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 51756
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 51757
    array-length v3, v2

    const/4 v11, 0x1

    if-le v3, v11, :cond_f

    .line 51758
    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    :goto_1
    move-wide/from16 v18, v2

    .line 51759
    goto/16 :goto_0

    :cond_7
    const-string v2, "#EXT-X-DISCONTINUITY-SEQUENCE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 51760
    const/16 v2, 0x3a

    invoke-virtual {v3, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v3, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    .line 51761
    :cond_8
    const-string v2, "#EXT-X-DISCONTINUITY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 51762
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 51763
    :cond_9
    const-string v2, "#"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 51764
    if-nez v9, :cond_a

    .line 51765
    const/4 v11, 0x0

    .line 51766
    :goto_2
    add-int/lit8 v17, v12, 0x1

    .line 51767
    const-wide/16 v12, -0x1

    cmp-long v2, v14, v12

    if-nez v2, :cond_e

    .line 51768
    const-wide/16 v12, 0x0

    .line 51769
    :goto_3
    new-instance v2, LX/0Nm;

    invoke-direct/range {v2 .. v15}, LX/0Nm;-><init>(Ljava/lang/String;DIJZLjava/lang/String;Ljava/lang/String;JJ)V

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51770
    const-wide v2, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v2, v4

    double-to-long v2, v2

    add-long/2addr v7, v2

    .line 51771
    const-wide/16 v4, 0x0

    .line 51772
    const-wide/16 v2, -0x1

    cmp-long v2, v14, v2

    if-eqz v2, :cond_d

    .line 51773
    add-long v2, v12, v14

    .line 51774
    :goto_4
    const-wide/16 v14, -0x1

    move/from16 v12, v17

    move-wide/from16 v18, v2

    .line 51775
    goto/16 :goto_0

    .line 51776
    :cond_a
    if-eqz v16, :cond_b

    move-object/from16 v11, v16

    .line 51777
    goto :goto_2

    .line 51778
    :cond_b
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    goto :goto_2

    .line 51779
    :cond_c
    const-string v2, "#EXT-X-ENDLIST"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 51780
    const/4 v7, 0x0

    .line 51781
    :goto_5
    new-instance v2, LX/0Nn;

    invoke-static/range {v24 .. v24}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    move-object/from16 v3, p1

    move/from16 v4, v22

    move/from16 v5, v21

    move/from16 v6, v20

    invoke-direct/range {v2 .. v8}, LX/0Nn;-><init>(Ljava/lang/String;IIIZLjava/util/List;)V

    return-object v2

    :cond_d
    move-wide v2, v12

    goto :goto_4

    :cond_e
    move-wide/from16 v12, v18

    goto :goto_3

    :cond_f
    move-wide/from16 v2, v18

    goto/16 :goto_1

    :cond_10
    move/from16 v7, v23

    goto :goto_5
.end method


# virtual methods
.method public final synthetic b(Ljava/lang/String;Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51782
    invoke-static {p1, p2}, LX/0Nq;->a(Ljava/lang/String;Ljava/io/InputStream;)LX/0Nk;

    move-result-object v0

    return-object v0
.end method
