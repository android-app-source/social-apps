.class public LX/07H;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/net/InetAddress;IILX/07G;)Ljava/net/Socket;
    .locals 2

    .prologue
    .line 19420
    invoke-static {}, LX/07G;->a()Ljava/net/Socket;

    move-result-object v0

    .line 19421
    invoke-static {v0}, LX/07H;->a(Ljava/net/Socket;)V

    .line 19422
    new-instance v1, Ljava/net/InetSocketAddress;

    invoke-direct {v1, p0, p1}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v0, v1, p2}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 19423
    return-object v0
.end method

.method public static a(Ljava/net/Socket;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19427
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    .line 19428
    invoke-virtual {p0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 19429
    invoke-virtual {p0, v1}, Ljava/net/Socket;->setKeepAlive(Z)V

    .line 19430
    return-void
.end method

.method public static b(Ljava/net/Socket;)V
    .locals 1

    .prologue
    .line 19424
    if-eqz p0, :cond_0

    .line 19425
    :try_start_0
    invoke-virtual {p0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 19426
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
