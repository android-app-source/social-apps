.class public final LX/0Fm;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field private mBytesRead:I

.field private mBytesToRead:I

.field public final synthetic this$0:LX/0Fn;


# direct methods
.method public constructor <init>(LX/0Fn;I)V
    .locals 1

    .prologue
    .line 34011
    iput-object p1, p0, LX/0Fm;->this$0:LX/0Fn;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 34012
    const/4 v0, 0x0

    iput v0, p0, LX/0Fm;->mBytesRead:I

    .line 34013
    iput p2, p0, LX/0Fm;->mBytesToRead:I

    .line 34014
    const/4 v0, 0x1

    .line 34015
    iput-boolean v0, p1, LX/0Fn;->mConsumingStream:Z

    .line 34016
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 2

    .prologue
    .line 34017
    iget v0, p0, LX/0Fm;->mBytesToRead:I

    iget v1, p0, LX/0Fm;->mBytesRead:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 34018
    iget-object v0, p0, LX/0Fm;->this$0:LX/0Fn;

    const/4 v1, 0x0

    .line 34019
    iput-boolean v1, v0, LX/0Fn;->mConsumingStream:Z

    .line 34020
    return-void
.end method

.method public final read()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 34021
    iget v1, p0, LX/0Fm;->mBytesRead:I

    iget v2, p0, LX/0Fm;->mBytesToRead:I

    if-ne v1, v2, :cond_0

    .line 34022
    :goto_0
    return v0

    .line 34023
    :cond_0
    iget-object v1, p0, LX/0Fm;->this$0:LX/0Fn;

    iget-object v1, v1, LX/0Fn;->mXzs:Lcom/facebook/xzdecoder/XzInputStream;

    invoke-virtual {v1}, Lcom/facebook/xzdecoder/XzInputStream;->read()I

    move-result v1

    .line 34024
    if-ne v1, v0, :cond_1

    .line 34025
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "truncated xzs stream"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34026
    :cond_1
    iget v0, p0, LX/0Fm;->mBytesRead:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Fm;->mBytesRead:I

    move v0, v1

    .line 34027
    goto :goto_0
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    .line 34028
    if-lez p3, :cond_1

    iget v0, p0, LX/0Fm;->mBytesRead:I

    iget v1, p0, LX/0Fm;->mBytesToRead:I

    if-ne v0, v1, :cond_1

    .line 34029
    const/4 v0, -0x1

    .line 34030
    :cond_0
    :goto_0
    return v0

    .line 34031
    :cond_1
    iget v0, p0, LX/0Fm;->mBytesToRead:I

    iget v1, p0, LX/0Fm;->mBytesRead:I

    sub-int/2addr v0, v1

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 34032
    iget-object v1, p0, LX/0Fm;->this$0:LX/0Fn;

    iget-object v1, v1, LX/0Fn;->mXzs:Lcom/facebook/xzdecoder/XzInputStream;

    invoke-virtual {v1, p1, p2, v0}, Lcom/facebook/xzdecoder/XzInputStream;->read([BII)I

    move-result v0

    .line 34033
    if-lez v0, :cond_0

    .line 34034
    iget v1, p0, LX/0Fm;->mBytesRead:I

    add-int/2addr v1, v0

    iput v1, p0, LX/0Fm;->mBytesRead:I

    goto :goto_0
.end method
