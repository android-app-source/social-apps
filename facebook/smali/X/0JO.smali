.class public final enum LX/0JO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JO;

.field public static final enum SURFACE_TEXTURE_POOL_FAILED:LX/0JO;

.field public static final enum SURFACE_TEXTURE_POOL_HIT:LX/0JO;

.field public static final enum SURFACE_TEXTURE_POOL_MISSED:LX/0JO;

.field public static final enum SURFACE_TEXTURE_POOL_OVERFLOWED:LX/0JO;

.field public static final enum SURFACE_TEXTURE_POOL_POOLED:LX/0JO;

.field public static final enum SURFACE_TEXTURE_POOL_TRIMMED:LX/0JO;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39385
    new-instance v0, LX/0JO;

    const-string v1, "SURFACE_TEXTURE_POOL_HIT"

    const-string v2, "video_surface_texture_pool_hit"

    invoke-direct {v0, v1, v4, v2}, LX/0JO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JO;->SURFACE_TEXTURE_POOL_HIT:LX/0JO;

    .line 39386
    new-instance v0, LX/0JO;

    const-string v1, "SURFACE_TEXTURE_POOL_MISSED"

    const-string v2, "video_surface_texture_pool_missed"

    invoke-direct {v0, v1, v5, v2}, LX/0JO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JO;->SURFACE_TEXTURE_POOL_MISSED:LX/0JO;

    .line 39387
    new-instance v0, LX/0JO;

    const-string v1, "SURFACE_TEXTURE_POOL_FAILED"

    const-string v2, "video_surface_texture_pool_failed"

    invoke-direct {v0, v1, v6, v2}, LX/0JO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JO;->SURFACE_TEXTURE_POOL_FAILED:LX/0JO;

    .line 39388
    new-instance v0, LX/0JO;

    const-string v1, "SURFACE_TEXTURE_POOL_POOLED"

    const-string v2, "video_surface_texture_pool_pooled"

    invoke-direct {v0, v1, v7, v2}, LX/0JO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JO;->SURFACE_TEXTURE_POOL_POOLED:LX/0JO;

    .line 39389
    new-instance v0, LX/0JO;

    const-string v1, "SURFACE_TEXTURE_POOL_OVERFLOWED"

    const-string v2, "video_surface_texture_pool_overflowed"

    invoke-direct {v0, v1, v8, v2}, LX/0JO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JO;->SURFACE_TEXTURE_POOL_OVERFLOWED:LX/0JO;

    .line 39390
    new-instance v0, LX/0JO;

    const-string v1, "SURFACE_TEXTURE_POOL_TRIMMED"

    const/4 v2, 0x5

    const-string v3, "video_surface_texture_pool_trimmed"

    invoke-direct {v0, v1, v2, v3}, LX/0JO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JO;->SURFACE_TEXTURE_POOL_TRIMMED:LX/0JO;

    .line 39391
    const/4 v0, 0x6

    new-array v0, v0, [LX/0JO;

    sget-object v1, LX/0JO;->SURFACE_TEXTURE_POOL_HIT:LX/0JO;

    aput-object v1, v0, v4

    sget-object v1, LX/0JO;->SURFACE_TEXTURE_POOL_MISSED:LX/0JO;

    aput-object v1, v0, v5

    sget-object v1, LX/0JO;->SURFACE_TEXTURE_POOL_FAILED:LX/0JO;

    aput-object v1, v0, v6

    sget-object v1, LX/0JO;->SURFACE_TEXTURE_POOL_POOLED:LX/0JO;

    aput-object v1, v0, v7

    sget-object v1, LX/0JO;->SURFACE_TEXTURE_POOL_OVERFLOWED:LX/0JO;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0JO;->SURFACE_TEXTURE_POOL_TRIMMED:LX/0JO;

    aput-object v2, v0, v1

    sput-object v0, LX/0JO;->$VALUES:[LX/0JO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39392
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39393
    iput-object p3, p0, LX/0JO;->value:Ljava/lang/String;

    .line 39394
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JO;
    .locals 1

    .prologue
    .line 39395
    const-class v0, LX/0JO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JO;

    return-object v0
.end method

.method public static values()[LX/0JO;
    .locals 1

    .prologue
    .line 39396
    sget-object v0, LX/0JO;->$VALUES:[LX/0JO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JO;

    return-object v0
.end method
