.class public final LX/0IG;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic b:Ljava/util/concurrent/ScheduledFuture;

.field public final synthetic c:LX/0IE;

.field public final synthetic d:LX/08o;


# direct methods
.method public constructor <init>(LX/08o;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/concurrent/ScheduledFuture;LX/0IE;)V
    .locals 0

    .prologue
    .line 38495
    iput-object p1, p0, LX/0IG;->d:LX/08o;

    iput-object p2, p0, LX/0IG;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p3, p0, LX/0IG;->b:Ljava/util/concurrent/ScheduledFuture;

    iput-object p4, p0, LX/0IG;->c:LX/0IE;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v0, 0x26

    const v1, 0x4e62c216    # 9.5109261E8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 38496
    invoke-virtual {p0}, LX/0IG;->getResultCode()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/0IG;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38497
    iget-object v1, p0, LX/0IG;->b:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v1, v3}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 38498
    invoke-virtual {p0, v3}, LX/0IG;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v1

    .line 38499
    const-string v2, "/settings/mqtt/id/mqtt_device_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 38500
    iget-object v2, p0, LX/0IG;->c:LX/0IE;

    invoke-virtual {v2, v1}, LX/0IE;->a(Ljava/lang/String;)V

    .line 38501
    :cond_0
    const/16 v1, 0x27

    const v2, 0x326c459b

    invoke-static {p2, v4, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void
.end method
