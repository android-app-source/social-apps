.class public abstract LX/001;
.super Landroid/app/Application;
.source ""

# interfaces
.implements LX/002;


# static fields
.field private static a:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "DelegatingApplication.class"
    .end annotation
.end field


# instance fields
.field private b:Lcom/facebook/base/app/ApplicationLike;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 771
    const/4 v0, 0x0

    sput-boolean v0, LX/001;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 768
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 769
    invoke-static {}, LX/001;->h()V

    .line 770
    return-void
.end method

.method private static d(Ljava/lang/Throwable;)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 757
    move v1, v0

    .line 758
    :goto_0
    if-ne v1, v0, :cond_0

    if-eqz p0, :cond_0

    .line 759
    invoke-static {p0}, LX/0Ft;->a(Ljava/lang/Throwable;)I

    move-result v1

    .line 760
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0

    .line 761
    :cond_0
    if-eq v1, v0, :cond_1

    .line 762
    invoke-static {v1}, LX/0Ft;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 763
    const-string v2, "ENOSPC"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 764
    const/4 v0, 0x1

    .line 765
    :cond_1
    :goto_1
    return v0

    .line 766
    :cond_2
    const-string v2, "ENOENT"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 767
    const/4 v0, 0x2

    goto :goto_1
.end method

.method private static declared-synchronized h()V
    .locals 3

    .prologue
    .line 752
    const-class v1, LX/001;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, LX/001;->a:Z

    if-eqz v0, :cond_0

    .line 753
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Multiple instances of the Application object were created."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 754
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 755
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, LX/001;->a:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 756
    monitor-exit v1

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 751
    invoke-super {p0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a([Ljava/lang/Object;)Lcom/facebook/base/app/ApplicationLike;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 731
    :try_start_0
    array-length v1, p1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    .line 732
    array-length v1, p1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    new-array v3, v1, [Ljava/lang/Object;

    .line 733
    const/4 v1, 0x0

    const-class v4, Landroid/app/Application;

    aput-object v4, v2, v1

    .line 734
    const/4 v1, 0x0

    aput-object p0, v3, v1

    move v1, v0

    .line 735
    :goto_0
    array-length v0, p1

    div-int/lit8 v0, v0, 0x2

    if-ge v1, v0, :cond_0

    .line 736
    add-int/lit8 v4, v1, 0x1

    mul-int/lit8 v0, v1, 0x2

    add-int/lit8 v0, v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/Class;

    aput-object v0, v2, v4

    .line 737
    add-int/lit8 v0, v1, 0x1

    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v4, p1, v4

    aput-object v4, v3, v0

    .line 738
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 739
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Impl"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 740
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 741
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/app/ApplicationLike;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    return-object v0

    .line 742
    :catch_0
    move-exception v0

    .line 743
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 744
    if-nez v1, :cond_2

    .line 745
    :goto_1
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_1

    .line 746
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 747
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 748
    :catch_1
    move-exception v0

    .line 749
    :goto_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 750
    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(I)V
    .locals 9

    .prologue
    .line 700
    const/16 v0, 0x0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 701
    if-eqz v0, :cond_0

    .line 702
    or-int/lit8 p1, p1, 0x1

    .line 703
    :cond_0
    const/4 v0, 0x0

    .line 704
    :try_start_0
    invoke-static {p0, p1}, LX/01y;->a(Landroid/content/Context;I)V
    :try_end_0
    .catch LX/0Iq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 705
    :cond_1
    :goto_1
    if-nez v0, :cond_3

    const/4 v2, 0x0

    .line 706
    sget-object v6, LX/01L;->c:[LX/022;

    .line 707
    if-nez v6, :cond_7

    .line 708
    :cond_2
    :goto_2
    move v1, v2

    .line 709
    if-nez v1, :cond_4

    .line 710
    :cond_3
    invoke-virtual {p0, v0}, LX/001;->a(Ljava/lang/Throwable;)V

    .line 711
    :cond_4
    new-instance v0, LX/02A;

    invoke-direct {v0}, LX/02A;-><init>()V

    invoke-static {v0}, LX/02C;->a(LX/02B;)V

    .line 712
    return-void

    .line 713
    :catch_0
    move-exception v1

    .line 714
    invoke-static {v1}, LX/001;->d(Ljava/lang/Throwable;)I

    move-result v2

    .line 715
    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 716
    invoke-virtual {p0, v1}, LX/001;->b(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 717
    :cond_5
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 718
    invoke-virtual {p0, v1}, LX/001;->c(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 719
    :catch_1
    move-exception v0

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 720
    :cond_7
    invoke-static {}, LX/01z;->a()[Ljava/lang/String;

    move-result-object v7

    move v1, v2

    .line 721
    :goto_3
    array-length v3, v6

    if-ge v1, v3, :cond_a

    .line 722
    aget-object v3, v6, v1

    invoke-virtual {v3}, LX/022;->c()[Ljava/lang/String;

    move-result-object v8

    move v3, v2

    .line 723
    :goto_4
    array-length v4, v8

    if-ge v3, v4, :cond_9

    move v4, v2

    move v5, v2

    .line 724
    :goto_5
    array-length p1, v7

    if-ge v4, p1, :cond_8

    if-nez v5, :cond_8

    .line 725
    aget-object v5, v8, v3

    aget-object p1, v7, v4

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 726
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 727
    :cond_8
    if-eqz v5, :cond_2

    .line 728
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 729
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 730
    :cond_a
    const/4 v2, 0x1

    goto :goto_2
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 699
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unsupportedDsoAbiError"

    invoke-direct {v0, v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final attachBaseContext(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 692
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    .line 693
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    .line 694
    sput-object v0, LX/00y;->a:Landroid/app/ActivityThread;

    .line 695
    invoke-virtual {p0}, LX/001;->f()V

    .line 696
    invoke-virtual {p0}, LX/001;->d()V

    .line 697
    invoke-virtual {p0}, LX/001;->c()V

    .line 698
    return-void
.end method

.method public abstract b()Lcom/facebook/base/app/ApplicationLike;
.end method

.method public b(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 772
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "diskFullError"

    invoke-direct {v0, v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 688
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/001;->b:Lcom/facebook/base/app/ApplicationLike;

    if-nez v0, :cond_0

    .line 689
    invoke-virtual {p0}, LX/001;->b()Lcom/facebook/base/app/ApplicationLike;

    move-result-object v0

    iput-object v0, p0, LX/001;->b:Lcom/facebook/base/app/ApplicationLike;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 690
    :cond_0
    monitor-exit p0

    return-void

    .line 691
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 687
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "fileNotFoundError"

    invoke-direct {v0, v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 686
    return-void
.end method

.method public final e()Lcom/facebook/base/app/ApplicationLike;
    .locals 1

    .prologue
    .line 684
    invoke-virtual {p0}, LX/001;->c()V

    .line 685
    iget-object v0, p0, LX/001;->b:Lcom/facebook/base/app/ApplicationLike;

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 682
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/001;->a(I)V

    .line 683
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 680
    iget-object v0, p0, LX/001;->b:Lcom/facebook/base/app/ApplicationLike;

    invoke-virtual {v0}, Lcom/facebook/base/app/ApplicationLike;->b()V

    .line 681
    return-void
.end method

.method public final getResources()Landroid/content/res/Resources;
    .locals 3

    .prologue
    .line 674
    iget-object v0, p0, LX/001;->b:Lcom/facebook/base/app/ApplicationLike;

    instance-of v0, v0, LX/02l;

    if-eqz v0, :cond_1

    .line 675
    instance-of v0, p0, LX/004;

    if-nez v0, :cond_0

    .line 676
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " illegally implements HasOverridingResources without HasBaseResourcesAccess."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 677
    :cond_0
    iget-object v0, p0, LX/001;->b:Lcom/facebook/base/app/ApplicationLike;

    check-cast v0, LX/02l;

    invoke-interface {v0}, LX/02l;->l_()Landroid/content/res/Resources;

    move-result-object v0

    .line 678
    if-eqz v0, :cond_1

    .line 679
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    goto :goto_0
.end method

.method public final h_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 673
    invoke-virtual {p0}, LX/001;->e()Lcom/facebook/base/app/ApplicationLike;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate()V
    .locals 3

    .prologue
    const v0, 0x439d5d2c

    .line 666
    const/4 v1, 0x2

    const/16 v2, 0x20

    invoke-static {v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    move v0, v1

    .line 667
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 668
    invoke-virtual {p0}, LX/001;->c()V

    .line 669
    invoke-virtual {p0}, LX/001;->g()V

    .line 670
    const v1, -0x2caab9eb

    .line 671
    const/4 v2, 0x2

    const/16 p0, 0x21

    invoke-static {v2, p0, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 672
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 662
    invoke-super {p0}, Landroid/app/Application;->onLowMemory()V

    .line 663
    iget-object v0, p0, LX/001;->b:Lcom/facebook/base/app/ApplicationLike;

    if-eqz v0, :cond_0

    .line 664
    iget-object v0, p0, LX/001;->b:Lcom/facebook/base/app/ApplicationLike;

    invoke-virtual {v0}, Lcom/facebook/base/app/ApplicationLike;->c()V

    .line 665
    :cond_0
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    .prologue
    .line 658
    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    .line 659
    iget-object v0, p0, LX/001;->b:Lcom/facebook/base/app/ApplicationLike;

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, LX/001;->b:Lcom/facebook/base/app/ApplicationLike;

    invoke-virtual {v0, p1}, Lcom/facebook/base/app/ApplicationLike;->a(I)V

    .line 661
    :cond_0
    return-void
.end method
