.class public LX/0Gh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.w"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "LX/0GC;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0GC;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field private f:Ljava/util/TimerTask;

.field private g:Ljava/util/Timer;

.field private h:Ljava/util/concurrent/atomic/AtomicInteger;

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36315
    const-class v0, LX/0Gh;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0Gh;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 36307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36308
    iput p1, p0, LX/0Gh;->e:I

    .line 36309
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, LX/0Gh;->b:Ljava/util/PriorityQueue;

    .line 36310
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0Gh;->d:Ljava/util/List;

    .line 36311
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Gh;->g:Ljava/util/Timer;

    .line 36312
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/0Gh;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 36313
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/0Gh;->c:Ljava/util/Set;

    .line 36314
    return-void
.end method

.method private static declared-synchronized a(LX/0Gh;Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)V
    .locals 4

    .prologue
    .line 36297
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Gh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GC;

    .line 36298
    iget-object v2, v0, LX/0GC;->a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v2, v2, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 36299
    iget-object v1, p0, LX/0Gh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 36300
    iget v1, p1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->f:I

    if-nez v1, :cond_2

    iget-object v1, p0, LX/0Gh;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    .line 36301
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bumping prefetch priority "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, LX/0GC;->a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v3, v3, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Priority "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, LX/0GC;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Origin "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, LX/0GC;->a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v3, v3, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36302
    iput v1, v0, LX/0GC;->b:I

    .line 36303
    iget-object v1, p0, LX/0Gh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36304
    :cond_1
    monitor-exit p0

    return-void

    .line 36305
    :cond_2
    :try_start_1
    iget v1, p1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 36306
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized d$redex0(LX/0Gh;)V
    .locals 6

    .prologue
    .line 36291
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Gh;->g:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 36292
    new-instance v0, Lcom/facebook/exoplayer/PrefetchScheduler$StatusCheckTask;

    invoke-direct {v0, p0}, Lcom/facebook/exoplayer/PrefetchScheduler$StatusCheckTask;-><init>(LX/0Gh;)V

    iput-object v0, p0, LX/0Gh;->f:Ljava/util/TimerTask;

    .line 36293
    new-instance v0, Ljava/util/Timer;

    const-string v1, "PrefetchSchedulerTimer"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0Gh;->g:Ljava/util/Timer;

    .line 36294
    iget-object v0, p0, LX/0Gh;->g:Ljava/util/Timer;

    iget-object v1, p0, LX/0Gh;->f:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xfa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36295
    :cond_0
    monitor-exit p0

    return-void

    .line 36296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized e(LX/0Gh;)V
    .locals 1

    .prologue
    .line 36246
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Gh;->g:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Gh;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36247
    iget-object v0, p0, LX/0Gh;->g:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 36248
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Gh;->g:Ljava/util/Timer;

    .line 36249
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Gh;->f:Ljava/util/TimerTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36250
    :cond_0
    monitor-exit p0

    return-void

    .line 36251
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static f(LX/0Gh;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 36284
    iget-boolean v2, p0, LX/0Gh;->i:Z

    if-nez v2, :cond_2

    .line 36285
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No video player playing. Effective concurrency "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, LX/0Gh;->e:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 36286
    iget-object v2, p0, LX/0Gh;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, LX/0Gh;->e:I

    if-ge v2, v3, :cond_1

    .line 36287
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 36288
    goto :goto_0

    .line 36289
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Video player playing. Effective concurrency "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, LX/0Gh;->e:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 36290
    iget v2, p0, LX/0Gh;->e:I

    if-lez v2, :cond_3

    iget-object v2, p0, LX/0Gh;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, LX/0Gh;->e:I

    add-int/lit8 v3, v3, -0x1

    if-lt v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(LX/0GC;)V
    .locals 2

    .prologue
    .line 36275
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "In progress prefetch count "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0Gh;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Max "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0Gh;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 36276
    invoke-static {p0}, LX/0Gh;->f(LX/0Gh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36277
    iget-object v0, p1, LX/0GC;->a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    invoke-static {p0, v0}, LX/0Gh;->a(LX/0Gh;Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)V

    .line 36278
    iget-object v0, p0, LX/0Gh;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36279
    invoke-virtual {p1}, LX/0GC;->a()V

    .line 36280
    :goto_0
    invoke-static {p0}, LX/0Gh;->d$redex0(LX/0Gh;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36281
    monitor-exit p0

    return-void

    .line 36282
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0Gh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0, p1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 36283
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 36262
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Disabling prefetch for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36263
    iget-object v0, p0, LX/0Gh;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36264
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Prefetch already disabled for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36265
    :goto_0
    monitor-exit p0

    return-void

    .line 36266
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0Gh;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 36267
    iget-object v0, p0, LX/0Gh;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 36268
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 36269
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GC;

    .line 36270
    iget-object v2, v0, LX/0GC;->a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v2, v2, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->e:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 36271
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cancelling prefetch Uri: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, LX/0GC;->a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v0, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " Origin: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36272
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 36273
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Disabled prefetch for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 36274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 36257
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/0Gh;->i:Z

    .line 36258
    iget-object v0, p0, LX/0Gh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 36259
    invoke-static {p0}, LX/0Gh;->d$redex0(LX/0Gh;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36260
    :cond_0
    monitor-exit p0

    return-void

    .line 36261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 36252
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Enabling prefetch for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36253
    iget-object v0, p0, LX/0Gh;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 36254
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Enabled prefetch for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36255
    monitor-exit p0

    return-void

    .line 36256
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
