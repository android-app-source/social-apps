.class public final LX/01j;
.super Ljava/util/ArrayList;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5110
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 5111
    const-string v0, "com.facebook.acra.ErrorReporter.handleException"

    invoke-virtual {p0, v0}, LX/01j;->add(Ljava/lang/Object;)Z

    .line 5112
    const-string v0, "com.facebook.acra.ErrorReporter.uncaughtException"

    invoke-virtual {p0, v0}, LX/01j;->add(Ljava/lang/Object;)Z

    .line 5113
    const-string v0, "com.facebook.common.errorreporting.memory.MemoryDumpHandler.uncaughtException"

    invoke-virtual {p0, v0}, LX/01j;->add(Ljava/lang/Object;)Z

    .line 5114
    const-string v0, "com.facebook.nobreak.ExceptionHandlerToDispatchKnownExceptionRemedies.uncaughtException"

    invoke-virtual {p0, v0}, LX/01j;->add(Ljava/lang/Object;)Z

    .line 5115
    const-string v0, "com.facebook.nobreak.DefaultCatchMeIfYouCan.uncaughtException"

    invoke-virtual {p0, v0}, LX/01j;->add(Ljava/lang/Object;)Z

    .line 5116
    return-void
.end method
