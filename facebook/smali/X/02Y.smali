.class public final LX/02Y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final dexes:[LX/02Z;

.field public final id:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final locator_id:I

.field public final locators:Z

.field public final requires:[Ljava/lang/String;

.field public final rootRelative:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 14

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 8728
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8729
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 8730
    const-string v0, "dex"

    .line 8731
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 8732
    new-instance v8, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    const-string v3, "UTF-8"

    invoke-direct {v1, p1, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v8, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move v1, v2

    move v3, v2

    move v5, v2

    .line 8733
    :cond_0
    :goto_0
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 8734
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v10

    if-eqz v10, :cond_0

    .line 8735
    const-string v10, "Secondary program dex metadata: [%s]"

    new-array v11, v4, [Ljava/lang/Object;

    aput-object v9, v11, v2

    invoke-static {v10, v11}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8736
    const-string v10, ".root_relative"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    move v5, v4

    .line 8737
    goto :goto_0

    .line 8738
    :cond_1
    const-string v10, ".locators"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    move v3, v4

    .line 8739
    goto :goto_0

    .line 8740
    :cond_2
    const-string v10, ".locator_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 8741
    const-string v1, " "

    invoke-virtual {v9, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 8742
    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 8743
    :cond_3
    const-string v10, ".id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 8744
    const-string v0, " "

    invoke-virtual {v9, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 8745
    aget-object v0, v0, v4

    goto :goto_0

    .line 8746
    :cond_4
    const-string v10, ".requires"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 8747
    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 8748
    aget-object v9, v9, v4

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 8749
    :cond_5
    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 8750
    const-string v10, "ignoring dex metadata pragma [%s]"

    new-array v11, v4, [Ljava/lang/Object;

    aput-object v9, v11, v2

    invoke-static {v10, v11}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 8751
    :cond_6
    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 8752
    new-instance v10, LX/02Z;

    aget-object v11, v9, v2

    aget-object v12, v9, v4

    const/4 v13, 0x2

    aget-object v9, v9, v13

    invoke-direct {v10, v11, v12, v9}, LX/02Z;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 8753
    :cond_7
    iput-boolean v5, p0, LX/02Y;->rootRelative:Z

    .line 8754
    iput-boolean v3, p0, LX/02Y;->locators:Z

    .line 8755
    iput v1, p0, LX/02Y;->locator_id:I

    .line 8756
    iput-object v0, p0, LX/02Y;->id:Ljava/lang/String;

    .line 8757
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, LX/02Y;->requires:[Ljava/lang/String;

    .line 8758
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LX/02Z;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/02Z;

    iput-object v0, p0, LX/02Y;->dexes:[LX/02Z;

    .line 8759
    return-void
.end method
