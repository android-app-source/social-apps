.class public final enum LX/0II;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0II;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0II;

.field public static final enum AUTHFAIL_AUTO_REGISTER:LX/0II;

.field public static final enum CACHE_HIT:LX/0II;

.field public static final enum CREDENTIALS_UPDATED:LX/0II;

.field public static final enum FAILURE_CACHE_UPDATE:LX/0II;

.field public static final enum FAILURE_EMPTY_PACKAGE_NAME:LX/0II;

.field public static final enum FAILURE_MQTT_NOT_CONNECTED:LX/0II;

.field public static final enum FAILURE_PACKAGE_DOES_NOT_MATCH_INTENT:LX/0II;

.field public static final enum FAILURE_SERVER_RESPOND_WITH_ERROR:LX/0II;

.field public static final enum FAILURE_SERVER_RESPOND_WITH_INVALID_PACKAGE_NAME:LX/0II;

.field public static final enum FAILURE_SERVER_RESPOND_WITH_INVALID_TOKEN:LX/0II;

.field public static final enum FAILURE_SERVICE_NOT_STARTED:LX/0II;

.field public static final enum FAILURE_UNKNOWN_CLIENT_ERROR:LX/0II;

.field public static final enum REGISTER:LX/0II;

.field public static final enum REQUEST_SENT_FAIL:LX/0II;

.field public static final enum REQUEST_SENT_SUCCESS:LX/0II;

.field public static final enum RESPONSE_RECEIVED:LX/0II;

.field public static final enum UNREGISTER_CALLED:LX/0II;

.field public static final enum UNREGISTER_FAILURE_MQTT_NOT_CONNECTED:LX/0II;

.field public static final enum UNREGISTER_REQUEST_SENT_FAIL:LX/0II;

.field public static final enum UNREGISTER_REQUEST_SENT_SUCCESS:LX/0II;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38510
    new-instance v0, LX/0II;

    const-string v1, "CACHE_HIT"

    invoke-direct {v0, v1, v3}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->CACHE_HIT:LX/0II;

    .line 38511
    new-instance v0, LX/0II;

    const-string v1, "REQUEST_SENT_SUCCESS"

    invoke-direct {v0, v1, v4}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->REQUEST_SENT_SUCCESS:LX/0II;

    .line 38512
    new-instance v0, LX/0II;

    const-string v1, "REQUEST_SENT_FAIL"

    invoke-direct {v0, v1, v5}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->REQUEST_SENT_FAIL:LX/0II;

    .line 38513
    new-instance v0, LX/0II;

    const-string v1, "RESPONSE_RECEIVED"

    invoke-direct {v0, v1, v6}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->RESPONSE_RECEIVED:LX/0II;

    .line 38514
    new-instance v0, LX/0II;

    const-string v1, "FAILURE_CACHE_UPDATE"

    invoke-direct {v0, v1, v7}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->FAILURE_CACHE_UPDATE:LX/0II;

    .line 38515
    new-instance v0, LX/0II;

    const-string v1, "FAILURE_SERVICE_NOT_STARTED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->FAILURE_SERVICE_NOT_STARTED:LX/0II;

    .line 38516
    new-instance v0, LX/0II;

    const-string v1, "FAILURE_MQTT_NOT_CONNECTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->FAILURE_MQTT_NOT_CONNECTED:LX/0II;

    .line 38517
    new-instance v0, LX/0II;

    const-string v1, "FAILURE_UNKNOWN_CLIENT_ERROR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->FAILURE_UNKNOWN_CLIENT_ERROR:LX/0II;

    .line 38518
    new-instance v0, LX/0II;

    const-string v1, "FAILURE_SERVER_RESPOND_WITH_ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->FAILURE_SERVER_RESPOND_WITH_ERROR:LX/0II;

    .line 38519
    new-instance v0, LX/0II;

    const-string v1, "FAILURE_SERVER_RESPOND_WITH_INVALID_PACKAGE_NAME"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->FAILURE_SERVER_RESPOND_WITH_INVALID_PACKAGE_NAME:LX/0II;

    .line 38520
    new-instance v0, LX/0II;

    const-string v1, "FAILURE_SERVER_RESPOND_WITH_INVALID_TOKEN"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->FAILURE_SERVER_RESPOND_WITH_INVALID_TOKEN:LX/0II;

    .line 38521
    new-instance v0, LX/0II;

    const-string v1, "FAILURE_PACKAGE_DOES_NOT_MATCH_INTENT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->FAILURE_PACKAGE_DOES_NOT_MATCH_INTENT:LX/0II;

    .line 38522
    new-instance v0, LX/0II;

    const-string v1, "FAILURE_EMPTY_PACKAGE_NAME"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->FAILURE_EMPTY_PACKAGE_NAME:LX/0II;

    .line 38523
    new-instance v0, LX/0II;

    const-string v1, "UNREGISTER_CALLED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->UNREGISTER_CALLED:LX/0II;

    .line 38524
    new-instance v0, LX/0II;

    const-string v1, "AUTHFAIL_AUTO_REGISTER"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->AUTHFAIL_AUTO_REGISTER:LX/0II;

    .line 38525
    new-instance v0, LX/0II;

    const-string v1, "REGISTER"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->REGISTER:LX/0II;

    .line 38526
    new-instance v0, LX/0II;

    const-string v1, "UNREGISTER_FAILURE_MQTT_NOT_CONNECTED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->UNREGISTER_FAILURE_MQTT_NOT_CONNECTED:LX/0II;

    .line 38527
    new-instance v0, LX/0II;

    const-string v1, "UNREGISTER_REQUEST_SENT_SUCCESS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->UNREGISTER_REQUEST_SENT_SUCCESS:LX/0II;

    .line 38528
    new-instance v0, LX/0II;

    const-string v1, "UNREGISTER_REQUEST_SENT_FAIL"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->UNREGISTER_REQUEST_SENT_FAIL:LX/0II;

    .line 38529
    new-instance v0, LX/0II;

    const-string v1, "CREDENTIALS_UPDATED"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/0II;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0II;->CREDENTIALS_UPDATED:LX/0II;

    .line 38530
    const/16 v0, 0x14

    new-array v0, v0, [LX/0II;

    sget-object v1, LX/0II;->CACHE_HIT:LX/0II;

    aput-object v1, v0, v3

    sget-object v1, LX/0II;->REQUEST_SENT_SUCCESS:LX/0II;

    aput-object v1, v0, v4

    sget-object v1, LX/0II;->REQUEST_SENT_FAIL:LX/0II;

    aput-object v1, v0, v5

    sget-object v1, LX/0II;->RESPONSE_RECEIVED:LX/0II;

    aput-object v1, v0, v6

    sget-object v1, LX/0II;->FAILURE_CACHE_UPDATE:LX/0II;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0II;->FAILURE_SERVICE_NOT_STARTED:LX/0II;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0II;->FAILURE_MQTT_NOT_CONNECTED:LX/0II;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0II;->FAILURE_UNKNOWN_CLIENT_ERROR:LX/0II;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0II;->FAILURE_SERVER_RESPOND_WITH_ERROR:LX/0II;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0II;->FAILURE_SERVER_RESPOND_WITH_INVALID_PACKAGE_NAME:LX/0II;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0II;->FAILURE_SERVER_RESPOND_WITH_INVALID_TOKEN:LX/0II;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0II;->FAILURE_PACKAGE_DOES_NOT_MATCH_INTENT:LX/0II;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0II;->FAILURE_EMPTY_PACKAGE_NAME:LX/0II;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0II;->UNREGISTER_CALLED:LX/0II;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0II;->AUTHFAIL_AUTO_REGISTER:LX/0II;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0II;->REGISTER:LX/0II;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0II;->UNREGISTER_FAILURE_MQTT_NOT_CONNECTED:LX/0II;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0II;->UNREGISTER_REQUEST_SENT_SUCCESS:LX/0II;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/0II;->UNREGISTER_REQUEST_SENT_FAIL:LX/0II;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/0II;->CREDENTIALS_UPDATED:LX/0II;

    aput-object v2, v0, v1

    sput-object v0, LX/0II;->$VALUES:[LX/0II;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38509
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0II;
    .locals 1

    .prologue
    .line 38531
    const-class v0, LX/0II;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0II;

    return-object v0
.end method

.method public static values()[LX/0II;
    .locals 1

    .prologue
    .line 38508
    sget-object v0, LX/0II;->$VALUES:[LX/0II;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0II;

    return-object v0
.end method
