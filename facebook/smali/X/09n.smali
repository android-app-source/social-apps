.class public LX/09n;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private final c:Ljava/lang/Runnable;

.field public final d:Landroid/content/Context;

.field public final e:LX/09m;

.field private final f:I

.field private g:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22966
    const-class v0, LX/09n;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/09n;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/09m;I)V
    .locals 2

    .prologue
    .line 22967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22968
    iput-object p1, p0, LX/09n;->d:Landroid/content/Context;

    .line 22969
    iput-object p2, p0, LX/09n;->e:LX/09m;

    .line 22970
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, LX/09n;->b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 22971
    iput p3, p0, LX/09n;->f:I

    .line 22972
    new-instance v0, Lcom/facebook/rti/orca/FbnsForegroundPinger$1;

    invoke-direct {v0, p0}, Lcom/facebook/rti/orca/FbnsForegroundPinger$1;-><init>(LX/09n;)V

    iput-object v0, p0, LX/09n;->c:Ljava/lang/Runnable;

    .line 22973
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 7

    .prologue
    .line 22974
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/09n;->b()V

    .line 22975
    iget-object v0, p0, LX/09n;->b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    iget-object v1, p0, LX/09n;->c:Ljava/lang/Runnable;

    iget v2, p0, LX/09n;->f:I

    int-to-long v2, v2

    iget v4, p0, LX/09n;->f:I

    int-to-long v4, v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v0 .. v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/09n;->g:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22976
    monitor-exit p0

    return-void

    .line 22977
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 22978
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/09n;->g:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 22979
    iget-object v0, p0, LX/09n;->g:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 22980
    const/4 v0, 0x0

    iput-object v0, p0, LX/09n;->g:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22981
    :cond_0
    monitor-exit p0

    return-void

    .line 22982
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
