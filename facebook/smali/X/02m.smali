.class public LX/02m;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)V
    .locals 2

    .prologue
    .line 9168
    const/16 v0, 0x8

    const/16 v1, 0x1f

    invoke-static {v0, v1, p0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    .line 9169
    invoke-static {}, LX/0PR;->a()V

    .line 9170
    return-void
.end method

.method public static a(JI)V
    .locals 2

    .prologue
    .line 9171
    const/16 v0, 0x8

    const/16 v1, 0x1f

    invoke-static {v0, v1, p2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    .line 9172
    invoke-static {p0, p1}, LX/0PR;->a(J)V

    .line 9173
    return-void
.end method

.method public static a(Ljava/lang/String;I)V
    .locals 8
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 9174
    invoke-static {p0}, LX/0PR;->a(Ljava/lang/String;)V

    .line 9175
    const/16 v1, 0x8

    const/16 v2, 0x1e

    const-wide/16 v4, 0x0

    const-string v6, "__name"

    move v3, p1

    move-object v7, p0

    invoke-static/range {v1 .. v7}, Lcom/facebook/loom/logger/Logger;->a(IIIJLjava/lang/String;Ljava/lang/String;)I

    .line 9176
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Object;I)V
    .locals 8
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 9177
    invoke-static {p0, p1}, LX/0PR;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 9178
    const/16 v1, 0x8

    const/16 v2, 0x1e

    const-wide/16 v4, 0x0

    const-string v6, "__name"

    invoke-static {p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    move v3, p2

    invoke-static/range {v1 .. v7}, Lcom/facebook/loom/logger/Logger;->a(IIIJLjava/lang/String;Ljava/lang/String;)I

    .line 9179
    return-void
.end method

.method public static a(Ljava/lang/String;[Ljava/lang/Object;I)V
    .locals 8
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # [Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 9180
    invoke-static {p0, p1}, LX/0PR;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9181
    const/16 v1, 0x8

    const/16 v2, 0x1e

    const-wide/16 v4, 0x0

    const-string v6, "__name"

    invoke-static {p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    move v3, p2

    invoke-static/range {v1 .. v7}, Lcom/facebook/loom/logger/Logger;->a(IIIJLjava/lang/String;Ljava/lang/String;)I

    .line 9182
    return-void
.end method

.method public static b(I)J
    .locals 2

    .prologue
    .line 9183
    const/16 v0, 0x8

    const/16 v1, 0x1f

    invoke-static {v0, v1, p0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    .line 9184
    invoke-static {}, LX/0PR;->b()J

    move-result-wide v0

    return-wide v0
.end method
