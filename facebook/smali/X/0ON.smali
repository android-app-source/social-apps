.class public final LX/0ON;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field public b:Lcom/google/android/exoplayer/upstream/Loader$LoadTask;

.field public c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 52964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52965
    new-instance v0, LX/08L;

    invoke-direct {v0, p1}, LX/08L;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    move-object v0, v0

    .line 52966
    iput-object v0, p0, LX/0ON;->a:Ljava/util/concurrent/ExecutorService;

    .line 52967
    return-void
.end method


# virtual methods
.method public final a(LX/0LO;LX/0LX;)V
    .locals 2

    .prologue
    .line 52968
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    .line 52969
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 52970
    invoke-virtual {p0, v1, p1, p2}, LX/0ON;->a(Landroid/os/Looper;LX/0LO;LX/0LX;)V

    .line 52971
    return-void

    .line 52972
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Looper;LX/0LO;LX/0LX;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 52973
    iget-boolean v0, p0, LX/0ON;->c:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 52974
    iput-boolean v1, p0, LX/0ON;->c:Z

    .line 52975
    new-instance v0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;-><init>(LX/0ON;Landroid/os/Looper;LX/0LO;LX/0LX;)V

    iput-object v0, p0, LX/0ON;->b:Lcom/google/android/exoplayer/upstream/Loader$LoadTask;

    .line 52976
    iget-object v0, p0, LX/0ON;->a:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/0ON;->b:Lcom/google/android/exoplayer/upstream/Loader$LoadTask;

    const v2, -0x2b76aacb

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 52977
    return-void

    .line 52978
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 52979
    iget-boolean v0, p0, LX/0ON;->c:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 52980
    iget-object v0, p0, LX/0ON;->b:Lcom/google/android/exoplayer/upstream/Loader$LoadTask;

    .line 52981
    iget-object p0, v0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->b:LX/0LO;

    invoke-interface {p0}, LX/0LO;->f()V

    .line 52982
    iget-object p0, v0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->d:Ljava/lang/Thread;

    if-eqz p0, :cond_0

    .line 52983
    iget-object p0, v0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->d:Ljava/lang/Thread;

    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V

    .line 52984
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 52985
    iget-boolean v0, p0, LX/0ON;->c:Z

    if-eqz v0, :cond_0

    .line 52986
    invoke-virtual {p0}, LX/0ON;->b()V

    .line 52987
    :cond_0
    iget-object v0, p0, LX/0ON;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 52988
    return-void
.end method
