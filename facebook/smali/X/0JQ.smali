.class public final enum LX/0JQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JQ;

.field public static final enum ALBUM_ORIGIN:LX/0JQ;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 39409
    new-instance v0, LX/0JQ;

    const-string v1, "ALBUM_ORIGIN"

    const-string v2, "album_origin"

    invoke-direct {v0, v1, v3, v2}, LX/0JQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JQ;->ALBUM_ORIGIN:LX/0JQ;

    .line 39410
    const/4 v0, 0x1

    new-array v0, v0, [LX/0JQ;

    sget-object v1, LX/0JQ;->ALBUM_ORIGIN:LX/0JQ;

    aput-object v1, v0, v3

    sput-object v0, LX/0JQ;->$VALUES:[LX/0JQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39411
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39412
    iput-object p3, p0, LX/0JQ;->value:Ljava/lang/String;

    .line 39413
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JQ;
    .locals 1

    .prologue
    .line 39414
    const-class v0, LX/0JQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JQ;

    return-object v0
.end method

.method public static values()[LX/0JQ;
    .locals 1

    .prologue
    .line 39415
    sget-object v0, LX/0JQ;->$VALUES:[LX/0JQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JQ;

    return-object v0
.end method
