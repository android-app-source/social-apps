.class public final LX/0NR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50304
    return-void
.end method

.method public static a(LX/0MA;)LX/0NP;
    .locals 13

    .prologue
    const/4 v6, 0x1

    const/16 v12, 0x10

    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 50323
    invoke-static {p0}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50324
    new-instance v5, LX/0Oj;

    invoke-direct {v5, v12}, LX/0Oj;-><init>(I)V

    .line 50325
    invoke-static {p0, v5}, LX/0NQ;->a(LX/0MA;LX/0Oj;)LX/0NQ;

    move-result-object v0

    .line 50326
    iget v0, v0, LX/0NQ;->a:I

    const-string v2, "RIFF"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    if-eq v0, v2, :cond_0

    move-object v0, v7

    .line 50327
    :goto_0
    return-object v0

    .line 50328
    :cond_0
    iget-object v0, v5, LX/0Oj;->a:[B

    const/4 v2, 0x4

    invoke-interface {p0, v0, v1, v2}, LX/0MA;->c([BII)V

    .line 50329
    invoke-virtual {v5, v1}, LX/0Oj;->b(I)V

    .line 50330
    invoke-virtual {v5}, LX/0Oj;->m()I

    move-result v0

    .line 50331
    const-string v2, "WAVE"

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 50332
    const-string v1, "WavHeaderReader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported RIFF format: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 50333
    goto :goto_0

    .line 50334
    :cond_1
    invoke-static {p0, v5}, LX/0NQ;->a(LX/0MA;LX/0Oj;)LX/0NQ;

    move-result-object v8

    .line 50335
    iget v0, v8, LX/0NQ;->a:I

    const-string v2, "fmt "

    invoke-static {v2}, LX/08x;->e(Ljava/lang/String;)I

    move-result v2

    if-eq v0, v2, :cond_2

    .line 50336
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Second chunk in RIFF WAV should be format; got: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v8, LX/0NQ;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50337
    :cond_2
    iget-wide v2, v8, LX/0NQ;->b:J

    const-wide/16 v10, 0x10

    cmp-long v0, v2, v10

    if-ltz v0, :cond_3

    move v0, v6

    :goto_1
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 50338
    iget-object v0, v5, LX/0Oj;->a:[B

    invoke-interface {p0, v0, v1, v12}, LX/0MA;->c([BII)V

    .line 50339
    invoke-virtual {v5, v1}, LX/0Oj;->b(I)V

    .line 50340
    invoke-virtual {v5}, LX/0Oj;->h()I

    move-result v0

    .line 50341
    invoke-virtual {v5}, LX/0Oj;->h()I

    move-result v1

    .line 50342
    invoke-virtual {v5}, LX/0Oj;->t()I

    move-result v2

    .line 50343
    invoke-virtual {v5}, LX/0Oj;->t()I

    move-result v3

    .line 50344
    invoke-virtual {v5}, LX/0Oj;->h()I

    move-result v4

    .line 50345
    invoke-virtual {v5}, LX/0Oj;->h()I

    move-result v5

    .line 50346
    mul-int v9, v1, v5

    div-int/lit8 v9, v9, 0x8

    .line 50347
    if-eq v4, v9, :cond_4

    .line 50348
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected WAV block alignment of: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; got: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v1

    .line 50349
    goto :goto_1

    .line 50350
    :cond_4
    if-eq v5, v12, :cond_5

    .line 50351
    const-string v0, "WavHeaderReader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Only 16-bit WAVs are supported; got: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 50352
    goto/16 :goto_0

    .line 50353
    :cond_5
    if-eq v0, v6, :cond_6

    const v6, 0xfffe

    if-eq v0, v6, :cond_6

    .line 50354
    const-string v1, "WavHeaderReader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported WAV format type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 50355
    goto/16 :goto_0

    .line 50356
    :cond_6
    iget-wide v6, v8, LX/0NQ;->b:J

    long-to-int v0, v6

    add-int/lit8 v0, v0, -0x10

    invoke-interface {p0, v0}, LX/0MA;->c(I)V

    .line 50357
    new-instance v0, LX/0NP;

    invoke-direct/range {v0 .. v5}, LX/0NP;-><init>(IIIII)V

    goto/16 :goto_0
.end method

.method public static a(LX/0MA;LX/0NP;)V
    .locals 8

    .prologue
    const/16 v6, 0x8

    .line 50305
    invoke-static {p0}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50306
    invoke-static {p1}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50307
    new-instance v1, LX/0Oj;

    invoke-direct {v1, v6}, LX/0Oj;-><init>(I)V

    .line 50308
    invoke-static {p0, v1}, LX/0NQ;->a(LX/0MA;LX/0Oj;)LX/0NQ;

    move-result-object v0

    .line 50309
    :goto_0
    iget v2, v0, LX/0NQ;->a:I

    const-string v3, "data"

    invoke-static {v3}, LX/08x;->e(Ljava/lang/String;)I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 50310
    const-string v2, "WavHeaderReader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignoring unknown WAV chunk: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, LX/0NQ;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 50311
    const-wide/16 v2, 0x8

    iget-wide v4, v0, LX/0NQ;->b:J

    add-long/2addr v2, v4

    .line 50312
    iget v4, v0, LX/0NQ;->a:I

    const-string v5, "RIFF"

    invoke-static {v5}, LX/08x;->e(Ljava/lang/String;)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 50313
    const-wide/16 v2, 0xc

    .line 50314
    :cond_0
    const-wide/32 v4, 0x7fffffff

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    .line 50315
    new-instance v1, LX/0L6;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Chunk is too large (~2GB+) to skip; id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, LX/0NQ;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v1

    .line 50316
    :cond_1
    long-to-int v0, v2

    invoke-interface {p0, v0}, LX/0MA;->b(I)V

    .line 50317
    invoke-static {p0, v1}, LX/0NQ;->a(LX/0MA;LX/0Oj;)LX/0NQ;

    move-result-object v0

    goto :goto_0

    .line 50318
    :cond_2
    invoke-interface {p0, v6}, LX/0MA;->b(I)V

    .line 50319
    invoke-interface {p0}, LX/0MA;->c()J

    move-result-wide v2

    iget-wide v0, v0, LX/0NQ;->b:J

    .line 50320
    iput-wide v2, p1, LX/0NP;->f:J

    .line 50321
    iput-wide v0, p1, LX/0NP;->g:J

    .line 50322
    return-void
.end method
