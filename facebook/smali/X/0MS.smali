.class public final LX/0MS;
.super LX/0MR;
.source ""


# static fields
.field private static final b:[I


# instance fields
.field private c:Z

.field private d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45967
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/0MS;->b:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x157c
        0x2af8
        0x55f0
        0xabe0
    .end array-data
.end method

.method public constructor <init>(LX/0LS;)V
    .locals 0

    .prologue
    .line 45968
    invoke-direct {p0, p1}, LX/0MR;-><init>(LX/0LS;)V

    .line 45969
    return-void
.end method


# virtual methods
.method public final a(LX/0Oj;J)V
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v10, 0x1

    .line 45970
    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v1

    .line 45971
    if-nez v1, :cond_1

    iget-boolean v3, p0, LX/0MS;->d:Z

    if-nez v3, :cond_1

    .line 45972
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v1

    new-array v8, v1, [B

    .line 45973
    array-length v1, v8

    invoke-virtual {p1, v8, v6, v1}, LX/0Oj;->a([BII)V

    .line 45974
    invoke-static {v8}, LX/0OY;->a([B)Landroid/util/Pair;

    move-result-object v7

    .line 45975
    const-string v1, "audio/mp4a-latm"

    invoke-virtual {p0}, LX/0MR;->a()J

    move-result-wide v4

    iget-object v3, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v3, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    move v3, v2

    move-object v9, v0

    invoke-static/range {v0 .. v9}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v0

    .line 45976
    iget-object v1, p0, LX/0MR;->a:LX/0LS;

    invoke-interface {v1, v0}, LX/0LS;->a(LX/0L4;)V

    .line 45977
    iput-boolean v10, p0, LX/0MS;->d:Z

    .line 45978
    :cond_0
    :goto_0
    return-void

    .line 45979
    :cond_1
    if-ne v1, v10, :cond_0

    .line 45980
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v5

    .line 45981
    iget-object v1, p0, LX/0MR;->a:LX/0LS;

    invoke-interface {v1, p1, v5}, LX/0LS;->a(LX/0Oj;I)V

    .line 45982
    iget-object v1, p0, LX/0MR;->a:LX/0LS;

    move-wide v2, p2

    move v4, v10

    move-object v7, v0

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    goto :goto_0
.end method

.method public final a(LX/0Oj;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 45983
    iget-boolean v0, p0, LX/0MS;->c:Z

    if-nez v0, :cond_3

    .line 45984
    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v0

    .line 45985
    shr-int/lit8 v1, v0, 0x4

    and-int/lit8 v1, v1, 0xf

    .line 45986
    shr-int/lit8 v0, v0, 0x2

    and-int/lit8 v0, v0, 0x3

    .line 45987
    if-ltz v0, :cond_0

    sget-object v2, LX/0MS;->b:[I

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 45988
    :cond_0
    new-instance v1, LX/0MU;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid sample rate index: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0MU;-><init>(Ljava/lang/String;)V

    throw v1

    .line 45989
    :cond_1
    const/16 v0, 0xa

    if-eq v1, v0, :cond_2

    .line 45990
    new-instance v0, LX/0MU;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Audio format not supported: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0MU;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45991
    :cond_2
    iput-boolean v3, p0, LX/0MS;->c:Z

    .line 45992
    :goto_0
    return v3

    .line 45993
    :cond_3
    invoke-virtual {p1, v3}, LX/0Oj;->c(I)V

    goto :goto_0
.end method
