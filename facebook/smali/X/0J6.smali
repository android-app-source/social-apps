.class public LX/0J6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/098;


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Z

.field private final d:LX/19o;

.field private final e:LX/03z;

.field private final f:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field private final g:Z


# direct methods
.method public constructor <init>(ZZZLX/19o;LX/03z;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Z)V
    .locals 0
    .param p4    # LX/19o;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/03z;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 39189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39190
    iput-boolean p1, p0, LX/0J6;->a:Z

    .line 39191
    iput-boolean p2, p0, LX/0J6;->b:Z

    .line 39192
    iput-boolean p3, p0, LX/0J6;->c:Z

    .line 39193
    iput-object p4, p0, LX/0J6;->d:LX/19o;

    .line 39194
    iput-object p5, p0, LX/0J6;->e:LX/03z;

    .line 39195
    iput-object p6, p0, LX/0J6;->f:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 39196
    iput-boolean p7, p0, LX/0J6;->g:Z

    .line 39197
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 39198
    iget-boolean v0, p0, LX/0J6;->b:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 39199
    iget-boolean v0, p0, LX/0J6;->c:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 39200
    iget-boolean v0, p0, LX/0J6;->a:Z

    return v0
.end method

.method public final d()LX/19o;
    .locals 1

    .prologue
    .line 39201
    iget-object v0, p0, LX/0J6;->d:LX/19o;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 39202
    iget-boolean v0, p0, LX/0J6;->g:Z

    return v0
.end method

.method public final f()LX/03z;
    .locals 1

    .prologue
    .line 39203
    iget-object v0, p0, LX/0J6;->e:LX/03z;

    return-object v0
.end method

.method public final g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 1

    .prologue
    .line 39204
    iget-object v0, p0, LX/0J6;->f:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 39205
    const/4 v0, -0x1

    return v0
.end method
