.class public final LX/0Fh;
.super LX/082;
.source ""


# instance fields
.field private final mBuffer:[B

.field private final mOptimizationSession:LX/08Z;


# direct methods
.method public constructor <init>(LX/08Z;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 33949
    invoke-direct {p0, p2}, LX/082;-><init>(Ljava/io/File;)V

    .line 33950
    const/high16 v0, 0x40000

    new-array v0, v0, [B

    iput-object v0, p0, LX/0Fh;->mBuffer:[B

    .line 33951
    iput-object p1, p0, LX/0Fh;->mOptimizationSession:LX/08Z;

    .line 33952
    return-void
.end method


# virtual methods
.method public final addDexOptOptions(LX/086;)V
    .locals 1

    .prologue
    .line 33946
    const-string v0, "-n"

    invoke-virtual {p1, v0}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    .line 33947
    iget-object v0, p0, LX/0Fh;->mOptimizationSession:LX/08Z;

    iget-object v0, v0, LX/08Z;->dexStoreConfig:LX/080;

    invoke-static {v0, p1}, LX/02b;->addConfiguredDexOptOptions(LX/080;LX/086;)V

    .line 33948
    return-void
.end method

.method public final copyDexToOdex(Ljava/io/InputStream;ILjava/io/RandomAccessFile;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v7, -0x1

    .line 33960
    invoke-virtual {p3}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-static {v1}, LX/08B;->fileno(Ljava/io/FileDescriptor;)I

    move-result v2

    .line 33961
    iget-object v1, p0, LX/0Fh;->mOptimizationSession:LX/08Z;

    iget-object v1, v1, LX/08Z;->config:LX/08F;

    iget-object v1, v1, LX/08F;->prio:LX/08D;

    .line 33962
    new-instance v3, LX/08D;

    iget v4, v1, LX/08D;->ioPriority:I

    const/high16 v5, -0x80000000

    invoke-direct {v3, v4, v5}, LX/08D;-><init>(II)V

    move-object v1, v3

    .line 33963
    invoke-virtual {v1}, LX/08D;->with()LX/0Fk;

    move-result-object v3

    const/4 v1, 0x0

    .line 33964
    :goto_0
    :try_start_0
    iget-object v4, p0, LX/0Fh;->mBuffer:[B

    iget-object v5, p0, LX/0Fh;->mBuffer:[B

    array-length v5, v5

    invoke-static {p1, v4, v5}, LX/02Q;->slurp(Ljava/io/InputStream;[BI)I

    move-result v4

    if-eq v4, v7, :cond_1

    .line 33965
    iget-object v5, p0, LX/0Fh;->mBuffer:[B

    const/4 v6, 0x0

    invoke-virtual {p3, v5, v6, v4}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 33966
    add-int/2addr v0, v4

    .line 33967
    iget-object v4, p0, LX/0Fh;->mOptimizationSession:LX/08Z;

    invoke-virtual {v4}, LX/08Z;->checkShouldStop()V

    .line 33968
    const/4 v4, -0x1

    invoke-static {v2, v4}, Lcom/facebook/common/dextricks/DalvikInternals;->fdatasync(II)V

    .line 33969
    invoke-static {v2}, LX/02Q;->tryDiscardPageCache(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 33970
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 33971
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_1
    if-eqz v3, :cond_0

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v3}, LX/0Fk;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_2
    throw v0

    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, LX/0Fk;->close()V

    .line 33972
    :cond_2
    return v0

    .line 33973
    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v3}, LX/0Fk;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final startSubprocess(LX/086;)Lcom/facebook/forker/Process;
    .locals 3

    .prologue
    .line 33955
    iget-object v0, p0, LX/0Fh;->mOptimizationSession:LX/08Z;

    iget-object v0, v0, LX/08Z;->config:LX/08F;

    iget-object v0, v0, LX/08F;->prio:LX/08D;

    invoke-virtual {v0}, LX/08D;->with()LX/0Fk;

    move-result-object v2

    const/4 v1, 0x0

    .line 33956
    :try_start_0
    invoke-virtual {p1}, LX/086;->create()Lcom/facebook/forker/Process;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 33957
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/0Fk;->close()V

    :cond_0
    return-object v0

    .line 33958
    :catch_0
    move-exception v1

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 33959
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/0Fk;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_0
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, LX/0Fk;->close()V

    goto :goto_0
.end method

.method public final waitForDexOpt(Lcom/facebook/forker/Process;I)V
    .locals 1

    .prologue
    .line 33953
    iget-object v0, p0, LX/0Fh;->mOptimizationSession:LX/08Z;

    invoke-virtual {v0, p1}, LX/08Z;->waitForAndManageProcess(Lcom/facebook/forker/Process;)I

    .line 33954
    return-void
.end method
