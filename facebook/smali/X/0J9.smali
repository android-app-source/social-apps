.class public final enum LX/0J9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0J9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0J9;

.field public static final enum LOADED:LX/0J9;

.field public static final enum NOT_PREFETCHED:LX/0J9;

.field public static final enum PREFETCHED_LOADED:LX/0J9;

.field public static final enum PREFETCHED_NOT_LOADED:LX/0J9;

.field public static final enum PREFETCH_ONGOING:LX/0J9;

.field public static final enum STALED:LX/0J9;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39228
    new-instance v0, LX/0J9;

    const-string v1, "STALED"

    const-string v2, "staled"

    invoke-direct {v0, v1, v4, v2}, LX/0J9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0J9;->STALED:LX/0J9;

    .line 39229
    new-instance v0, LX/0J9;

    const-string v1, "NOT_PREFETCHED"

    const-string v2, "not_prefetched"

    invoke-direct {v0, v1, v5, v2}, LX/0J9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0J9;->NOT_PREFETCHED:LX/0J9;

    .line 39230
    new-instance v0, LX/0J9;

    const-string v1, "PREFETCH_ONGOING"

    const-string v2, "prefetch_ongoing"

    invoke-direct {v0, v1, v6, v2}, LX/0J9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0J9;->PREFETCH_ONGOING:LX/0J9;

    .line 39231
    new-instance v0, LX/0J9;

    const-string v1, "PREFETCHED_LOADED"

    const-string v2, "prefetched_loaded"

    invoke-direct {v0, v1, v7, v2}, LX/0J9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0J9;->PREFETCHED_LOADED:LX/0J9;

    .line 39232
    new-instance v0, LX/0J9;

    const-string v1, "LOADED"

    const-string v2, "loaded"

    invoke-direct {v0, v1, v8, v2}, LX/0J9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0J9;->LOADED:LX/0J9;

    .line 39233
    new-instance v0, LX/0J9;

    const-string v1, "PREFETCHED_NOT_LOADED"

    const/4 v2, 0x5

    const-string v3, "prefetched_not_loaded"

    invoke-direct {v0, v1, v2, v3}, LX/0J9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0J9;->PREFETCHED_NOT_LOADED:LX/0J9;

    .line 39234
    const/4 v0, 0x6

    new-array v0, v0, [LX/0J9;

    sget-object v1, LX/0J9;->STALED:LX/0J9;

    aput-object v1, v0, v4

    sget-object v1, LX/0J9;->NOT_PREFETCHED:LX/0J9;

    aput-object v1, v0, v5

    sget-object v1, LX/0J9;->PREFETCH_ONGOING:LX/0J9;

    aput-object v1, v0, v6

    sget-object v1, LX/0J9;->PREFETCHED_LOADED:LX/0J9;

    aput-object v1, v0, v7

    sget-object v1, LX/0J9;->LOADED:LX/0J9;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0J9;->PREFETCHED_NOT_LOADED:LX/0J9;

    aput-object v2, v0, v1

    sput-object v0, LX/0J9;->$VALUES:[LX/0J9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39235
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39236
    iput-object p3, p0, LX/0J9;->value:Ljava/lang/String;

    .line 39237
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0J9;
    .locals 1

    .prologue
    .line 39238
    const-class v0, LX/0J9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0J9;

    return-object v0
.end method

.method public static values()[LX/0J9;
    .locals 1

    .prologue
    .line 39239
    sget-object v0, LX/0J9;->$VALUES:[LX/0J9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0J9;

    return-object v0
.end method
