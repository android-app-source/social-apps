.class public LX/06I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/util/concurrent/ScheduledExecutorService;

.field private final c:Landroid/os/Handler;

.field private final d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field private final e:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/04p;

.field private g:LX/06n;

.field private h:Ljava/lang/Runnable;

.field private i:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field public j:I

.field public k:J

.field private l:Z

.field private m:LX/06H;


# direct methods
.method public constructor <init>(Lcom/facebook/rti/common/time/RealtimeSinceBootClock;LX/05N;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;Landroid/os/Handler;LX/04p;LX/06H;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/rti/common/time/MonotonicClock;",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Landroid/os/Handler;",
            "LX/04p;",
            "LX/06H;",
            ")V"
        }
    .end annotation

    .prologue
    .line 17569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17570
    iput-object p1, p0, LX/06I;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 17571
    iput-object p2, p0, LX/06I;->e:LX/05N;

    .line 17572
    iput-object p3, p0, LX/06I;->a:Ljava/util/concurrent/ExecutorService;

    .line 17573
    iput-object p4, p0, LX/06I;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 17574
    iput-object p5, p0, LX/06I;->c:Landroid/os/Handler;

    .line 17575
    iput-object p6, p0, LX/06I;->f:LX/04p;

    .line 17576
    const/4 v0, 0x0

    iput v0, p0, LX/06I;->j:I

    .line 17577
    iput-object p7, p0, LX/06I;->m:LX/06H;

    .line 17578
    return-void
.end method

.method private a(LX/06l;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 17558
    const-string v0, "ConnectionRetryManager"

    const-string v1, "set strategy to %s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17559
    invoke-direct {p0}, LX/06I;->m()V

    .line 17560
    iget-object v0, p0, LX/06I;->f:LX/04p;

    invoke-virtual {v0}, LX/04p;->b()LX/04q;

    move-result-object v0

    .line 17561
    sget-object v1, LX/06l;->BACK_TO_BACK:LX/06l;

    if-ne p1, v1, :cond_0

    .line 17562
    new-instance v1, LX/06m;

    iget v2, v0, LX/04q;->i:I

    iget v3, v0, LX/04q;->j:I

    iget v0, v0, LX/04q;->k:I

    invoke-direct {v1, v2, v3, v0}, LX/06m;-><init>(III)V

    iput-object v1, p0, LX/06I;->g:LX/06n;

    .line 17563
    :goto_0
    return-void

    .line 17564
    :cond_0
    sget-object v1, LX/06l;->BACK_OFF:LX/06l;

    if-ne p1, v1, :cond_1

    .line 17565
    new-instance v1, LX/0I6;

    iget v2, v0, LX/04q;->l:I

    iget v3, v0, LX/04q;->m:I

    iget v0, v0, LX/04q;->n:I

    invoke-direct {v1, v2, v3, v0}, LX/0I6;-><init>(III)V

    iput-object v1, p0, LX/06I;->g:LX/06n;

    goto :goto_0

    .line 17566
    :cond_1
    const-string v0, "Invalid strategy %s specified"

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {v0, v1}, LX/05V;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 17567
    const-string v1, "ConnectionRetryManager"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17568
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private declared-synchronized j()Z
    .locals 1

    .prologue
    .line 17484
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/06I;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()Z
    .locals 1

    .prologue
    .line 17557
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06I;->i:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/06I;->i:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 17553
    invoke-direct {p0}, LX/06I;->m()V

    .line 17554
    sget-object v0, LX/06l;->BACK_TO_BACK:LX/06l;

    invoke-direct {p0, v0}, LX/06I;->a(LX/06l;)V

    .line 17555
    const/4 v0, 0x0

    iput v0, p0, LX/06I;->j:I

    .line 17556
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 17549
    iget-object v0, p0, LX/06I;->i:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 17550
    iget-object v0, p0, LX/06I;->i:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 17551
    const/4 v0, 0x0

    iput-object v0, p0, LX/06I;->i:Ljava/util/concurrent/Future;

    .line 17552
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 17545
    monitor-enter p0

    :try_start_0
    const-string v0, "ConnectionRetryManager"

    const-string v1, "start"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17546
    invoke-direct {p0}, LX/06I;->l()V

    .line 17547
    invoke-virtual {p0}, LX/06I;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/06I;->i:Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 17548
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 17579
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06I;->h:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/01n;->b(Z)V

    .line 17580
    iput-object p1, p0, LX/06I;->h:Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17581
    monitor-exit p0

    return-void

    .line 17582
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 17539
    iget-object v0, p0, LX/06I;->c:Landroid/os/Handler;

    .line 17540
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 17541
    if-eqz v0, :cond_0

    .line 17542
    iget-object v0, p0, LX/06I;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 17543
    sget-object v0, LX/07C;->a:LX/07C;

    .line 17544
    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, LX/06I;->a:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/06I;->h:Ljava/lang/Runnable;

    const v2, 0x31885ce8

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized c()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17503
    monitor-enter p0

    :try_start_0
    const-string v0, "ConnectionRetryManager"

    const-string v3, "next"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17504
    invoke-static {}, LX/06o;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17505
    const-string v0, "ConnectionRetryManager"

    const-string v2, "next is called while in restricted mode."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 17506
    :goto_0
    monitor-exit p0

    return v0

    .line 17507
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/06I;->g:LX/06n;

    if-nez v0, :cond_1

    .line 17508
    const-string v0, "ConnectionRetryManager"

    const-string v2, "next is called before having a strategy."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 17509
    goto :goto_0

    .line 17510
    :cond_1
    iget-object v0, p0, LX/06I;->m:LX/06H;

    invoke-interface {v0}, LX/06H;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 17511
    goto :goto_0

    .line 17512
    :cond_2
    invoke-direct {p0}, LX/06I;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 17513
    const-string v0, "ConnectionRetryManager"

    const-string v1, "Retry attempt already scheduled."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 17514
    goto :goto_0

    .line 17515
    :cond_3
    iget v0, p0, LX/06I;->j:I

    if-nez v0, :cond_4

    .line 17516
    iget-object v0, p0, LX/06I;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    iput-wide v4, p0, LX/06I;->k:J

    .line 17517
    :cond_4
    iget-object v0, p0, LX/06I;->e:LX/05N;

    invoke-interface {v0}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, LX/06I;->j()Z

    move-result v0

    if-nez v0, :cond_6

    move v3, v2

    .line 17518
    :goto_1
    iget-object v0, p0, LX/06I;->g:LX/06n;

    invoke-interface {v0, v3}, LX/06n;->b(Z)Z

    move-result v0

    .line 17519
    if-nez v0, :cond_7

    .line 17520
    iget-object v4, p0, LX/06I;->g:LX/06n;

    invoke-interface {v4}, LX/06n;->a()LX/06l;

    move-result-object v4

    sget-object v5, LX/06l;->BACK_TO_BACK:LX/06l;

    if-ne v4, v5, :cond_5

    .line 17521
    const-string v0, "ConnectionRetryManager"

    const-string v4, "Auto switching from B2B to back off retry strategy."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17522
    sget-object v0, LX/06l;->BACK_OFF:LX/06l;

    invoke-direct {p0, v0}, LX/06I;->a(LX/06l;)V

    .line 17523
    iget-object v0, p0, LX/06I;->g:LX/06n;

    invoke-interface {v0, v3}, LX/06n;->b(Z)Z

    move-result v0

    .line 17524
    :cond_5
    if-nez v0, :cond_7

    .line 17525
    const-string v0, "ConnectionRetryManager"

    const-string v2, "No more retry!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 17526
    goto/16 :goto_0

    :cond_6
    move v3, v1

    .line 17527
    goto :goto_1

    .line 17528
    :cond_7
    iget-object v0, p0, LX/06I;->g:LX/06n;

    invoke-interface {v0, v3}, LX/06n;->a(Z)I

    move-result v0

    .line 17529
    const-string v1, "ConnectionRetryManager"

    const-string v3, "Strategy=%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/06I;->g:LX/06n;

    aput-object v6, v4, v5

    invoke-static {v1, v3, v4}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17530
    invoke-direct {p0}, LX/06I;->m()V

    .line 17531
    if-gtz v0, :cond_8

    .line 17532
    const-string v0, "ConnectionRetryManager"

    const-string v1, "Submitting immediate retry"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17533
    invoke-virtual {p0}, LX/06I;->b()Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, LX/06I;->i:Ljava/util/concurrent/Future;

    .line 17534
    :goto_2
    iget v0, p0, LX/06I;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/06I;->j:I

    move v0, v2

    .line 17535
    goto/16 :goto_0

    .line 17536
    :cond_8
    const-string v1, "ConnectionRetryManager"

    const-string v3, "Scheduling retry in %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17537
    iget-object v1, p0, LX/06I;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v3, p0, LX/06I;->h:Ljava/lang/Runnable;

    int-to-long v4, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v3, v4, v5, v0}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/06I;->i:Ljava/util/concurrent/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 17538
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 3

    .prologue
    .line 17499
    monitor-enter p0

    :try_start_0
    const-string v0, "ConnectionRetryManager"

    const-string v1, "stop retry"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17500
    invoke-direct {p0}, LX/06I;->l()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17501
    monitor-exit p0

    return-void

    .line 17502
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 17491
    monitor-enter p0

    :try_start_0
    const-string v1, "ConnectionRetryManager"

    const-string v2, "ensure scheduled"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17492
    invoke-direct {p0}, LX/06I;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 17493
    iget-object v0, p0, LX/06I;->g:LX/06n;

    if-nez v0, :cond_1

    .line 17494
    invoke-virtual {p0}, LX/06I;->a()Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17495
    :goto_0
    const/4 v0, 0x1

    .line 17496
    :cond_0
    monitor-exit p0

    return v0

    .line 17497
    :cond_1
    :try_start_1
    invoke-virtual {p0}, LX/06I;->c()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 17498
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()V
    .locals 1

    .prologue
    .line 17488
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/06I;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17489
    monitor-exit p0

    return-void

    .line 17490
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()V
    .locals 1

    .prologue
    .line 17485
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/06I;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17486
    monitor-exit p0

    return-void

    .line 17487
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
