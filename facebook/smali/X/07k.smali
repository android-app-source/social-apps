.class public LX/07k;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:LX/05B;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05B",
            "<",
            "LX/0Hs;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/05B;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05B",
            "<",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/05B;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05B",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/05B;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05B",
            "<",
            "LX/06k;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/05B;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05B",
            "<",
            "LX/06y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/06k;LX/06y;)V
    .locals 7

    .prologue
    .line 20222
    const/4 v1, 0x1

    .line 20223
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v2, v0

    .line 20224
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v3, v0

    .line 20225
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v4, v0

    .line 20226
    sget-object v0, LX/06k;->a:LX/06k;

    invoke-virtual {p1, v0}, LX/06k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20227
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v5, v0

    .line 20228
    :goto_0
    invoke-virtual {p2}, LX/06y;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20229
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v6, v0

    .line 20230
    :goto_1
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/07k;-><init>(ZLX/05B;LX/05B;LX/05B;LX/05B;LX/05B;)V

    .line 20231
    return-void

    .line 20232
    :cond_0
    invoke-static {p1}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v5

    goto :goto_0

    :cond_1
    invoke-static {p2}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v6

    goto :goto_1
.end method

.method public constructor <init>(LX/0Hs;)V
    .locals 7

    .prologue
    .line 20208
    const/4 v1, 0x0

    invoke-static {p1}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v2

    .line 20209
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v3, v0

    .line 20210
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v4, v0

    .line 20211
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v5, v0

    .line 20212
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v6, v0

    .line 20213
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/07k;-><init>(ZLX/05B;LX/05B;LX/05B;LX/05B;LX/05B;)V

    .line 20214
    return-void
.end method

.method public constructor <init>(LX/0Hs;B)V
    .locals 7

    .prologue
    .line 20215
    const/4 v1, 0x0

    invoke-static {p1}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v2

    .line 20216
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v3, v0

    .line 20217
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    invoke-static {v0}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v4

    .line 20218
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v5, v0

    .line 20219
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v6, v0

    .line 20220
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/07k;-><init>(ZLX/05B;LX/05B;LX/05B;LX/05B;LX/05B;)V

    .line 20221
    return-void
.end method

.method public constructor <init>(LX/0Hs;Ljava/lang/Exception;)V
    .locals 7

    .prologue
    .line 20202
    const/4 v1, 0x0

    invoke-static {p1}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v2

    invoke-static {p2}, LX/05B;->b(Ljava/lang/Object;)LX/05B;

    move-result-object v3

    .line 20203
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v4, v0

    .line 20204
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v5, v0

    .line 20205
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v6, v0

    .line 20206
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/07k;-><init>(ZLX/05B;LX/05B;LX/05B;LX/05B;LX/05B;)V

    .line 20207
    return-void
.end method

.method private constructor <init>(ZLX/05B;LX/05B;LX/05B;LX/05B;LX/05B;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/05B",
            "<",
            "LX/0Hs;",
            ">;",
            "LX/05B",
            "<",
            "Ljava/lang/Exception;",
            ">;",
            "LX/05B",
            "<",
            "Ljava/lang/Byte;",
            ">;",
            "LX/05B",
            "<",
            "LX/06k;",
            ">;",
            "LX/05B",
            "<",
            "LX/06y;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20195
    iput-boolean p1, p0, LX/07k;->a:Z

    .line 20196
    iput-object p2, p0, LX/07k;->b:LX/05B;

    .line 20197
    iput-object p3, p0, LX/07k;->c:LX/05B;

    .line 20198
    iput-object p4, p0, LX/07k;->d:LX/05B;

    .line 20199
    iput-object p5, p0, LX/07k;->e:LX/05B;

    .line 20200
    iput-object p6, p0, LX/07k;->f:LX/05B;

    .line 20201
    return-void
.end method
