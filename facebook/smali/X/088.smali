.class public final LX/088;
.super LX/089;
.source ""


# instance fields
.field private final mResProvider:LX/02S;


# direct methods
.method public constructor <init>(LX/02Y;LX/02S;)V
    .locals 0

    .prologue
    .line 20812
    invoke-direct {p0, p1}, LX/089;-><init>(LX/02Y;)V

    .line 20813
    iput-object p2, p0, LX/088;->mResProvider:LX/02S;

    .line 20814
    return-void
.end method


# virtual methods
.method public final nextImpl(LX/02Z;)LX/08A;
    .locals 2

    .prologue
    .line 20815
    iget-object v0, p0, LX/088;->mResProvider:LX/02S;

    iget-object v1, p1, LX/02Z;->assetName:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/02S;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 20816
    :try_start_0
    new-instance v0, LX/08A;

    invoke-direct {v0, p1, v1}, LX/08A;-><init>(LX/02Z;Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20817
    const/4 v1, 0x0

    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    throw v0
.end method
