.class public LX/08r;
.super Ljava/net/SocketImpl;
.source ""


# static fields
.field private static c:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<",
            "Ljava/net/Inet4Address;",
            ">;"
        }
    .end annotation
.end field

.field public static d:Z


# instance fields
.field private a:Ljava/net/InetAddress;

.field private b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 21636
    sput-boolean v0, LX/08r;->d:Z

    .line 21637
    :try_start_0
    const-class v0, Ljava/net/Inet4Address;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, [B

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 21638
    sput-object v0, LX/08r;->c:Ljava/lang/reflect/Constructor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 21639
    const/4 v0, 0x1

    sput-boolean v0, LX/08r;->d:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 21640
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public constructor <init>([BLjava/lang/String;I)V
    .locals 3

    .prologue
    .line 21610
    invoke-direct {p0}, Ljava/net/SocketImpl;-><init>()V

    .line 21611
    sget-object v0, LX/08r;->c:Ljava/lang/reflect/Constructor;

    if-nez v0, :cond_0

    .line 21612
    new-instance v0, LX/061;

    const-string v1, "getDeclaredConstructor failed"

    invoke-direct {v0, v1}, LX/061;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21613
    :cond_0
    :try_start_0
    sget-object v0, LX/08r;->c:Ljava/lang/reflect/Constructor;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    iput-object v0, p0, LX/08r;->a:Ljava/net/InetAddress;

    .line 21614
    iput p3, p0, LX/08r;->b:I
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 21615
    return-void

    .line 21616
    :catch_0
    move-exception v0

    .line 21617
    new-instance v1, LX/061;

    invoke-direct {v1, v0}, LX/061;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 21618
    :catch_1
    move-exception v0

    .line 21619
    new-instance v1, LX/061;

    invoke-direct {v1, v0}, LX/061;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 21620
    :catch_2
    move-exception v0

    .line 21621
    new-instance v1, LX/061;

    invoke-direct {v1, v0}, LX/061;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method


# virtual methods
.method public final accept(Ljava/net/SocketImpl;)V
    .locals 2

    .prologue
    .line 21635
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling accept() on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final available()I
    .locals 2

    .prologue
    .line 21634
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling available() on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final bind(Ljava/net/InetAddress;I)V
    .locals 2

    .prologue
    .line 21633
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling bind(InetAddress, int) on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 21632
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling close() on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final connect(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 21631
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling connect(String, int) on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final connect(Ljava/net/InetAddress;I)V
    .locals 2

    .prologue
    .line 21630
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling connect(InetAddress, int) on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final connect(Ljava/net/SocketAddress;I)V
    .locals 2

    .prologue
    .line 21629
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling connect(SocketAddress, int) on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final create(Z)V
    .locals 2

    .prologue
    .line 21641
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling create() on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getInetAddress()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 21628
    iget-object v0, p0, LX/08r;->a:Ljava/net/InetAddress;

    return-object v0
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 21627
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling getInputStream() on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getOption(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 21626
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling getOption(int) on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 2

    .prologue
    .line 21625
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling getOutputStream() on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getPort()I
    .locals 1

    .prologue
    .line 21624
    iget v0, p0, LX/08r;->b:I

    return v0
.end method

.method public final listen(I)V
    .locals 2

    .prologue
    .line 21623
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling listen(int) on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final sendUrgentData(I)V
    .locals 2

    .prologue
    .line 21622
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling sendUrgentData(int) on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setOption(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 21609
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling setOption(int, Object) on FakeSocketImpl"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
