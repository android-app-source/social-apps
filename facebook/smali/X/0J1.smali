.class public LX/0J1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39147
    const/4 v0, 0x1

    .line 39148
    sput-boolean v0, LX/0Bx;->a:Z

    .line 39149
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/hardware/camera2/CameraCaptureSession;I)V
    .locals 1

    .prologue
    .line 39151
    invoke-virtual {p0}, Landroid/hardware/camera2/CameraCaptureSession;->close()V

    .line 39152
    invoke-static {}, LX/0Bx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39153
    invoke-virtual {p0}, Landroid/hardware/camera2/CameraCaptureSession;->getDevice()Landroid/hardware/camera2/CameraDevice;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    .line 39154
    invoke-static {v0}, LX/0Bx;->d(I)V

    .line 39155
    :cond_0
    return-void
.end method
