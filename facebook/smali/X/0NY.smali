.class public final LX/0NY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0NV;


# instance fields
.field public final synthetic a:Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;)V
    .locals 0

    .prologue
    .line 50525
    iput-object p1, p0, LX/0NY;->a:Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 50526
    invoke-static {p1}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(I)I

    move-result v0

    return v0
.end method

.method public final a(ID)V
    .locals 2

    .prologue
    .line 50527
    iget-object v0, p0, LX/0NY;->a:Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(ID)V

    .line 50528
    return-void
.end method

.method public final a(IILX/0MA;)V
    .locals 1

    .prologue
    .line 50529
    iget-object v0, p0, LX/0NY;->a:Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(IILX/0MA;)V

    .line 50530
    return-void
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    .line 50531
    iget-object v0, p0, LX/0NY;->a:Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(IJ)V

    .line 50532
    return-void
.end method

.method public final a(IJJ)V
    .locals 6

    .prologue
    .line 50533
    iget-object v0, p0, LX/0NY;->a:Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(IJJ)V

    .line 50534
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 50535
    iget-object v0, p0, LX/0NY;->a:Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(ILjava/lang/String;)V

    .line 50536
    return-void
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 50537
    invoke-static {p1}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->b(I)Z

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 50538
    iget-object v0, p0, LX/0NY;->a:Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->c(I)V

    .line 50539
    return-void
.end method
