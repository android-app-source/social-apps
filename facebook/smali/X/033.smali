.class public final LX/033;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/00j;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const-wide/32 v6, 0x10000000

    .line 9775
    invoke-static {v6, v7}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9776
    sget-object v0, LX/02r;->b:LX/02r;

    move-object v1, v0

    .line 9777
    if-nez v1, :cond_1

    .line 9778
    :cond_0
    :goto_0
    return-void

    .line 9779
    :cond_1
    const-string v0, "Starting Loom"

    invoke-static {v6, v7, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 9780
    const/4 v0, 0x4

    const/4 v2, 0x1

    :try_start_0
    const-class v3, LX/08W;

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, LX/02r;->a(IILjava/lang/Object;I)Z

    move-result v0

    iput-boolean v0, p0, LX/033;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9781
    invoke-static {v6, v7}, LX/0BL;->a(J)LX/0BN;

    move-result-object v0

    .line 9782
    const-string v2, "Success"

    iget-boolean v3, p0, LX/033;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    .line 9783
    iget-boolean v2, p0, LX/033;->a:Z

    if-eqz v2, :cond_2

    .line 9784
    invoke-virtual {v1}, LX/02r;->d()Ljava/lang/String;

    move-result-object v1

    .line 9785
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "https"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "our.intern.facebook.com"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "intern/artillery2/waterfall"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "id"

    invoke-virtual {v2, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "pref_name"

    const-string v3, "Loom"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 9786
    const-string v2, "URL"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    .line 9787
    :cond_2
    invoke-virtual {v0}, LX/0BN;->a()V

    goto :goto_0

    .line 9788
    :catchall_0
    move-exception v0

    invoke-static {v6, v7}, LX/0BL;->a(J)LX/0BN;

    move-result-object v2

    .line 9789
    const-string v3, "Success"

    iget-boolean v4, p0, LX/033;->a:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    .line 9790
    iget-boolean v3, p0, LX/033;->a:Z

    if-eqz v3, :cond_3

    .line 9791
    invoke-virtual {v1}, LX/02r;->d()Ljava/lang/String;

    move-result-object v1

    .line 9792
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string v4, "https"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "our.intern.facebook.com"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "intern/artillery2/waterfall"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "id"

    invoke-virtual {v3, v4, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "pref_name"

    const-string v4, "Loom"

    invoke-virtual {v1, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 9793
    const-string v3, "URL"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    .line 9794
    :cond_3
    invoke-virtual {v2}, LX/0BN;->a()V

    .line 9795
    throw v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 9796
    iget-boolean v0, p0, LX/033;->a:Z

    if-eqz v0, :cond_0

    .line 9797
    sget-object v0, LX/02r;->b:LX/02r;

    move-object v0, v0

    .line 9798
    if-nez v0, :cond_1

    .line 9799
    :cond_0
    :goto_0
    return-void

    .line 9800
    :cond_1
    const/4 v1, 0x4

    const-class v2, LX/08W;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/02r;->a(ILjava/lang/Object;I)V

    goto :goto_0
.end method
