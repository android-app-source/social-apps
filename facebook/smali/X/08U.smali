.class public final LX/08U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final dexNr:I

.field public final signalLock:LX/02W;


# direct methods
.method public constructor <init>(ILX/02W;)V
    .locals 0

    .prologue
    .line 21170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21171
    iput p1, p0, LX/08U;->dexNr:I

    .line 21172
    iput-object p2, p0, LX/08U;->signalLock:LX/02W;

    .line 21173
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 21174
    iget-object v0, p0, LX/08U;->signalLock:LX/02W;

    invoke-virtual {v0}, LX/02W;->close()V

    .line 21175
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 21176
    const-string v0, "DexToOptimize(dexNr=%d)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, LX/08U;->dexNr:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
