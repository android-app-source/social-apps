.class public final LX/0Jh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0GJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0GJ",
        "<",
        "LX/0AY;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Jj;

.field private b:LX/0Jp;


# direct methods
.method public constructor <init>(LX/0Jj;LX/0Jp;)V
    .locals 0

    .prologue
    .line 39597
    iput-object p1, p0, LX/0Jh;->a:LX/0Jj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39598
    iput-object p2, p0, LX/0Jh;->b:LX/0Jp;

    .line 39599
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 39600
    check-cast p1, LX/0AY;

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 39601
    const-string v1, "ExoPlayerDashStreamRenderBuilder.onSingleManifest"

    const v2, 0x6e3564ac

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 39602
    :try_start_0
    invoke-virtual {p1}, LX/0AY;->b()I

    move-result v1

    if-ne v1, v0, :cond_2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 39603
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0AY;->a(I)LX/0Am;

    move-result-object v0

    .line 39604
    iget-object v0, v0, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v4

    move-object v2, v4

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    .line 39605
    iget v6, v0, LX/0Ak;->b:I

    packed-switch v6, :pswitch_data_0

    :cond_1
    move-object v0, v1

    move-object v1, v0

    .line 39606
    goto :goto_1

    :cond_2
    move v0, v3

    .line 39607
    goto :goto_0

    .line 39608
    :pswitch_0
    iget-object v0, v0, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 39609
    if-nez v2, :cond_9

    :goto_3
    move-object v2, v0

    .line 39610
    goto :goto_2

    .line 39611
    :pswitch_1
    if-nez v1, :cond_3

    move-object v1, v0

    .line 39612
    :cond_3
    iget-object v0, v0, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    .line 39613
    :catchall_0
    move-exception v0

    const v1, 0x22a5d346

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 39614
    :cond_4
    :try_start_1
    iget-object v0, p0, LX/0Jh;->a:LX/0Jj;

    invoke-static {v0, p1}, LX/0Jj;->a$redex0(LX/0Jj;LX/0AY;)LX/0Jw;

    move-result-object v5

    .line 39615
    iget-object v0, p0, LX/0Jh;->a:LX/0Jj;

    iget-object v0, v0, LX/0Jj;->j:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, LX/0K4;

    iget-object v4, p0, LX/0Jh;->a:LX/0Jj;

    iget-object v4, v4, LX/0Jj;->j:Ljava/lang/String;

    invoke-direct {v0, v4}, LX/0K4;-><init>(Ljava/lang/String;)V

    .line 39616
    :goto_5
    if-eqz v0, :cond_6

    invoke-virtual {v0}, LX/0K4;->a()LX/03z;

    move-result-object v0

    .line 39617
    :goto_6
    iget-boolean v4, v0, LX/03z;->isSpatial:Z

    if-eqz v4, :cond_7

    iget-object v4, p0, LX/0Jh;->a:LX/0Jj;

    iget-wide v6, p1, LX/0AY;->c:J

    invoke-static {v4, v2, v0, v6, v7}, LX/0Jj;->a$redex0(LX/0Jj;LX/0Ah;LX/03z;J)LX/0GT;

    move-result-object v0

    move-object v2, v0

    .line 39618
    :goto_7
    iget-object v4, p0, LX/0Jh;->b:LX/0Jp;

    if-eqz v1, :cond_8

    iget-object v0, v1, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_8
    const/4 v3, 0x1

    invoke-static {v1, v3}, LX/0Gj;->a(LX/0Ak;Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v5, v2, v0, v1}, LX/0Jp;->a(LX/0Jw;LX/0GT;ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 39619
    const v0, -0x599b5781

    invoke-static {v0}, LX/02m;->a(I)V

    .line 39620
    return-void

    :cond_5
    move-object v0, v4

    .line 39621
    goto :goto_5

    .line 39622
    :cond_6
    :try_start_2
    sget-object v0, LX/03z;->UNKNOWN:LX/03z;

    goto :goto_6

    .line 39623
    :cond_7
    iget-object v0, p0, LX/0Jh;->a:LX/0Jj;

    iget-wide v6, p1, LX/0AY;->c:J

    invoke-static {v0, v2, v6, v7}, LX/0Jj;->a$redex0(LX/0Jj;LX/0Ah;J)LX/0GX;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    move-object v2, v0

    goto :goto_7

    :cond_8
    move v0, v3

    .line 39624
    goto :goto_8

    :cond_9
    move-object v0, v2

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Ljava/io/IOException;)V
    .locals 1

    .prologue
    .line 39625
    iget-object v0, p0, LX/0Jh;->b:LX/0Jp;

    invoke-interface {v0, p1}, LX/0Jp;->a(Ljava/lang/Exception;)V

    .line 39626
    return-void
.end method
