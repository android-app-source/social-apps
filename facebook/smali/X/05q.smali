.class public LX/05q;
.super Ljava/util/concurrent/AbstractExecutorService;
.source ""

# interfaces
.implements Ljava/util/concurrent/ScheduledExecutorService;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field private final c:Landroid/content/Context;

.field public final d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field private final e:Landroid/app/AlarmManager;

.field private final f:Landroid/app/PendingIntent;

.field private final g:I

.field private final h:Landroid/content/BroadcastReceiver;

.field public final i:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "LX/05v",
            "<*>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16909
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/05q;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ACTION_ALARM."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/05q;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Landroid/app/AlarmManager;Landroid/os/Handler;)V
    .locals 4

    .prologue
    .line 16888
    invoke-direct {p0}, Ljava/util/concurrent/AbstractExecutorService;-><init>()V

    .line 16889
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, LX/05q;->i:Ljava/util/PriorityQueue;

    .line 16890
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, LX/05q;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 16891
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 16892
    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 16893
    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16894
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/05q;->b:Ljava/lang/String;

    .line 16895
    iput-object p2, p0, LX/05q;->c:Landroid/content/Context;

    .line 16896
    iput-object p3, p0, LX/05q;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 16897
    iput-object p4, p0, LX/05q;->e:Landroid/app/AlarmManager;

    .line 16898
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, LX/05q;->g:I

    .line 16899
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/05q;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 16900
    iget-object v1, p0, LX/05q;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 16901
    iget-object v1, p0, LX/05q;->c:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, LX/05q;->f:Landroid/app/PendingIntent;

    .line 16902
    new-instance v0, LX/05r;

    invoke-direct {v0, p0}, LX/05r;-><init>(LX/05q;)V

    iput-object v0, p0, LX/05q;->h:Landroid/content/BroadcastReceiver;

    .line 16903
    iget-object v0, p0, LX/05q;->c:Landroid/content/Context;

    iget-object v1, p0, LX/05q;->h:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    iget-object v3, p0, LX/05q;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, p5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 16904
    return-void
.end method

.method private a(Ljava/lang/Runnable;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 16905
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/05q;->a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 16906
    invoke-direct {p0, p1, p2}, LX/05q;->b(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    move-result-object v0

    .line 16907
    iget-object v1, p0, LX/05q;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, LX/05q;->a(Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;J)V

    .line 16908
    return-object v0
.end method

.method private a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 16910
    new-instance v0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    invoke-direct {v0, p0, p1}, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;-><init>(LX/05q;Ljava/util/concurrent/Callable;)V

    move-object v0, v0

    .line 16911
    iget-object v1, p0, LX/05q;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-direct {p0, v0, v2, v3}, LX/05q;->a(Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;J)V

    .line 16912
    return-object v0
.end method

.method private a(Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;J)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture",
            "<*>;J)V"
        }
    .end annotation

    .prologue
    .line 16929
    const-string v0, "WakingExecutorService"

    const-string v1, "Scheduling task for %d seconds from now"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/05q;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v4}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    sub-long v4, p2, v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16930
    monitor-enter p0

    .line 16931
    :try_start_0
    iget-object v0, p0, LX/05q;->i:Ljava/util/PriorityQueue;

    new-instance v1, LX/05v;

    invoke-direct {v1, p1, p2, p3}, LX/05v;-><init>(Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;J)V

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 16932
    invoke-static {p0}, LX/05q;->b(LX/05q;)V

    .line 16933
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a$redex0(LX/05q;)V
    .locals 10

    .prologue
    .line 16913
    const-string v0, "WakingExecutorService"

    const-string v1, "Alarm fired"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16914
    monitor-enter p0

    .line 16915
    :try_start_0
    const-string v3, "WakingExecutorService"

    const-string v4, "Removing expired tasks from the queue to be executed"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16916
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 16917
    :goto_0
    iget-object v6, p0, LX/05q;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v6}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, LX/05q;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v6}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/05v;

    iget-wide v6, v6, LX/05v;->b:J

    iget-object v8, p0, LX/05q;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v8}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-lez v6, :cond_3

    :cond_0
    const/4 v6, 0x1

    :goto_1
    move v3, v6

    .line 16918
    if-nez v3, :cond_1

    .line 16919
    iget-object v3, p0, LX/05q;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v3}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/05v;

    .line 16920
    iget-object v3, v3, LX/05v;->a:Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 16921
    :cond_1
    move-object v0, v4

    .line 16922
    invoke-static {p0}, LX/05q;->b(LX/05q;)V

    .line 16923
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 16924
    const-string v1, "WakingExecutorService"

    const-string v2, "Executing %d tasks"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16925
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    .line 16926
    invoke-virtual {v1}, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;->run()V

    goto :goto_2

    .line 16927
    :cond_2
    return-void

    .line 16928
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static a$redex0(LX/05q;Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;)V
    .locals 4

    .prologue
    .line 16952
    const/4 v1, 0x0

    .line 16953
    monitor-enter p0

    .line 16954
    :try_start_0
    iget-object v0, p0, LX/05q;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/05v;

    .line 16955
    iget-object v3, v0, LX/05v;->a:Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    if-ne v3, p1, :cond_0

    .line 16956
    :goto_0
    if-eqz v0, :cond_1

    .line 16957
    iget-object v1, p0, LX/05q;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 16958
    invoke-static {p0}, LX/05q;->b(LX/05q;)V

    .line 16959
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private b(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 16960
    new-instance v0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;-><init>(LX/05q;Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static b(LX/05q;)V
    .locals 12
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x2

    .line 16937
    iget-object v0, p0, LX/05q;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16938
    const-string v0, "WakingExecutorService"

    const-string v1, "No pending tasks, so not setting alarm and un-register the receiver"

    new-array v2, v11, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16939
    iget-object v0, p0, LX/05q;->e:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/05q;->f:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 16940
    :goto_0
    return-void

    .line 16941
    :cond_0
    iget-object v0, p0, LX/05q;->i:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/05v;

    iget-wide v0, v0, LX/05v;->b:J

    .line 16942
    const-string v2, "WakingExecutorService"

    const-string v3, "Next alarm in %d seconds"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LX/05q;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v5}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v6

    sub-long v6, v0, v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v11

    invoke-static {v2, v3, v4}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16943
    iget v2, p0, LX/05q;->g:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_1

    .line 16944
    iget-object v2, p0, LX/05q;->e:Landroid/app/AlarmManager;

    iget-object v3, p0, LX/05q;->f:Landroid/app/PendingIntent;

    .line 16945
    invoke-virtual {v2, v10, v0, v1, v3}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    .line 16946
    goto :goto_0

    .line 16947
    :cond_1
    iget v2, p0, LX/05q;->g:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_2

    .line 16948
    iget-object v2, p0, LX/05q;->e:Landroid/app/AlarmManager;

    iget-object v3, p0, LX/05q;->f:Landroid/app/PendingIntent;

    .line 16949
    invoke-virtual {v2, v10, v0, v1, v3}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 16950
    goto :goto_0

    .line 16951
    :cond_2
    iget-object v2, p0, LX/05q;->e:Landroid/app/AlarmManager;

    iget-object v3, p0, LX/05q;->f:Landroid/app/PendingIntent;

    invoke-virtual {v2, v10, v0, v1, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 16934
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/05q;->b(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    move-result-object v0

    .line 16935
    iget-object v1, p0, LX/05q;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-direct {p0, v0, v2, v3}, LX/05q;->a(Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;J)V

    .line 16936
    return-object v0
.end method

.method public final awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .prologue
    .line 16885
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final execute(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 16886
    invoke-direct {p0, p1}, LX/05q;->a(Ljava/lang/Runnable;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    .line 16887
    return-void
.end method

.method public final isShutdown()Z
    .locals 1

    .prologue
    .line 16868
    const/4 v0, 0x0

    return v0
.end method

.method public final isTerminated()Z
    .locals 1

    .prologue
    .line 16869
    const/4 v0, 0x0

    return v0
.end method

.method public final newTaskFor(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/RunnableFuture;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Ljava/util/concurrent/RunnableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 16870
    new-instance v0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledRunnableFuture;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledRunnableFuture;-><init>(LX/05q;Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final newTaskFor(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/RunnableFuture;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Ljava/util/concurrent/RunnableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 16871
    new-instance v0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledRunnableFuture;

    invoke-direct {v0, p0, p1}, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledRunnableFuture;-><init>(LX/05q;Ljava/util/concurrent/Callable;)V

    return-object v0
.end method

.method public final synthetic schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 16872
    invoke-virtual {p0, p1, p2, p3, p4}, LX/05q;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 16873
    invoke-direct {p0, p1, p2, p3, p4}, LX/05q;->a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "JJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 16874
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "JJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 16875
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final shutdown()V
    .locals 4

    .prologue
    .line 16876
    iget-object v0, p0, LX/05q;->e:Landroid/app/AlarmManager;

    iget-object v1, p0, LX/05q;->f:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 16877
    :try_start_0
    iget-object v0, p0, LX/05q;->c:Landroid/content/Context;

    iget-object v1, p0, LX/05q;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 16878
    :goto_0
    return-void

    .line 16879
    :catch_0
    move-exception v0

    .line 16880
    const-string v1, "WakingExecutorService"

    const-string v2, "Failed to unregister broadcast receiver"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16884
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 16881
    invoke-direct {p0, p1}, LX/05q;->a(Ljava/lang/Runnable;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 16882
    invoke-direct {p0, p1, p2}, LX/05q;->a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method

.method public final submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .locals 3

    .prologue
    .line 16883
    const-wide/16 v0, 0x0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {p0, p1, v0, v1, v2}, LX/05q;->a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;

    move-result-object v0

    return-object v0
.end method
