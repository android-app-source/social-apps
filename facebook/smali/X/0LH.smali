.class public LX/0LH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/media/AudioTrack;

.field private b:Z

.field private c:I

.field private d:J

.field private e:J

.field private f:J

.field public g:J

.field public h:J

.field public i:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/media/AudioTrack;Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 43240
    iput-object p1, p0, LX/0LH;->a:Landroid/media/AudioTrack;

    .line 43241
    iput-boolean p2, p0, LX/0LH;->b:Z

    .line 43242
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0LH;->g:J

    .line 43243
    iput-wide v2, p0, LX/0LH;->d:J

    .line 43244
    iput-wide v2, p0, LX/0LH;->e:J

    .line 43245
    iput-wide v2, p0, LX/0LH;->f:J

    .line 43246
    if-eqz p1, :cond_0

    .line 43247
    invoke-virtual {p1}, Landroid/media/AudioTrack;->getSampleRate()I

    move-result v0

    iput v0, p0, LX/0LH;->c:I

    .line 43248
    :cond_0
    return-void
.end method

.method public a(Landroid/media/PlaybackParams;)V
    .locals 1

    .prologue
    .line 43239
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()J
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 43222
    iget-wide v0, p0, LX/0LH;->g:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 43223
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-wide v2, p0, LX/0LH;->g:J

    sub-long/2addr v0, v2

    .line 43224
    iget v2, p0, LX/0LH;->c:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    .line 43225
    iget-wide v2, p0, LX/0LH;->i:J

    iget-wide v4, p0, LX/0LH;->h:J

    add-long/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 43226
    :goto_0
    return-wide v0

    .line 43227
    :cond_0
    iget-object v0, p0, LX/0LH;->a:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v4

    .line 43228
    const/4 v0, 0x1

    if-ne v4, v0, :cond_1

    move-wide v0, v2

    .line 43229
    goto :goto_0

    .line 43230
    :cond_1
    const-wide v0, 0xffffffffL

    iget-object v5, p0, LX/0LH;->a:Landroid/media/AudioTrack;

    invoke-virtual {v5}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v5

    int-to-long v6, v5

    and-long/2addr v0, v6

    .line 43231
    iget-boolean v5, p0, LX/0LH;->b:Z

    if-eqz v5, :cond_3

    .line 43232
    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 43233
    iget-wide v2, p0, LX/0LH;->d:J

    iput-wide v2, p0, LX/0LH;->f:J

    .line 43234
    :cond_2
    iget-wide v2, p0, LX/0LH;->f:J

    add-long/2addr v0, v2

    .line 43235
    :cond_3
    iget-wide v2, p0, LX/0LH;->d:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_4

    .line 43236
    iget-wide v2, p0, LX/0LH;->e:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0LH;->e:J

    .line 43237
    :cond_4
    iput-wide v0, p0, LX/0LH;->d:J

    .line 43238
    iget-wide v2, p0, LX/0LH;->e:J

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public final c()J
    .locals 4

    .prologue
    .line 43221
    invoke-virtual {p0}, LX/0LH;->b()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    iget v2, p0, LX/0LH;->c:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 43220
    const/4 v0, 0x0

    return v0
.end method

.method public e()J
    .locals 1

    .prologue
    .line 43219
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()J
    .locals 1

    .prologue
    .line 43218
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()F
    .locals 1

    .prologue
    .line 43217
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method
