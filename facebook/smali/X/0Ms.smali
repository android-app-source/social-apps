.class public final LX/0Ms;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Mv;

.field public final b:LX/0Oj;

.field private final c:LX/0Mu;

.field public d:I

.field private e:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48280
    new-instance v0, LX/0Mv;

    invoke-direct {v0}, LX/0Mv;-><init>()V

    iput-object v0, p0, LX/0Ms;->a:LX/0Mv;

    .line 48281
    new-instance v0, LX/0Oj;

    const/16 v1, 0x11a

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, LX/0Ms;->b:LX/0Oj;

    .line 48282
    new-instance v0, LX/0Mu;

    invoke-direct {v0}, LX/0Mu;-><init>()V

    iput-object v0, p0, LX/0Ms;->c:LX/0Mu;

    .line 48283
    const/4 v0, -0x1

    iput v0, p0, LX/0Ms;->d:I

    return-void
.end method


# virtual methods
.method public final a(LX/0MA;)J
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 48284
    invoke-interface {p1}, LX/0MA;->d()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->a(Z)V

    .line 48285
    invoke-static {p1}, LX/0Mw;->a(LX/0MA;)V

    .line 48286
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    invoke-virtual {v0}, LX/0Mv;->a()V

    .line 48287
    :goto_1
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget v0, v0, LX/0Mv;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    .line 48288
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget v0, v0, LX/0Mv;->i:I

    if-lez v0, :cond_0

    .line 48289
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget v0, v0, LX/0Mv;->i:I

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    .line 48290
    :cond_0
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget-object v2, p0, LX/0Ms;->b:LX/0Oj;

    invoke-static {p1, v0, v2, v1}, LX/0Mw;->a(LX/0MA;LX/0Mv;LX/0Oj;Z)Z

    .line 48291
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget v0, v0, LX/0Mv;->h:I

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    goto :goto_1

    :cond_1
    move v0, v1

    .line 48292
    goto :goto_0

    .line 48293
    :cond_2
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget-wide v0, v0, LX/0Mv;->c:J

    return-wide v0
.end method

.method public final a(LX/0MA;J)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 48266
    invoke-static {p1}, LX/0Mw;->a(LX/0MA;)V

    .line 48267
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget-object v1, p0, LX/0Ms;->b:LX/0Oj;

    invoke-static {p1, v0, v1, v2}, LX/0Mw;->a(LX/0MA;LX/0Mv;LX/0Oj;Z)Z

    .line 48268
    :goto_0
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget-wide v0, v0, LX/0Mv;->c:J

    cmp-long v0, v0, p2

    if-gez v0, :cond_0

    .line 48269
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget v0, v0, LX/0Mv;->h:I

    iget-object v1, p0, LX/0Ms;->a:LX/0Mv;

    iget v1, v1, LX/0Mv;->i:I

    add-int/2addr v0, v1

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    .line 48270
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget-wide v0, v0, LX/0Mv;->c:J

    iput-wide v0, p0, LX/0Ms;->e:J

    .line 48271
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget-object v1, p0, LX/0Ms;->b:LX/0Oj;

    invoke-static {p1, v0, v1, v2}, LX/0Mw;->a(LX/0MA;LX/0Mv;LX/0Oj;Z)Z

    goto :goto_0

    .line 48272
    :cond_0
    iget-wide v0, p0, LX/0Ms;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 48273
    new-instance v0, LX/0L6;

    invoke-direct {v0}, LX/0L6;-><init>()V

    throw v0

    .line 48274
    :cond_1
    invoke-interface {p1}, LX/0MA;->a()V

    .line 48275
    iget-wide v0, p0, LX/0Ms;->e:J

    .line 48276
    iput-wide v4, p0, LX/0Ms;->e:J

    .line 48277
    const/4 v2, -0x1

    iput v2, p0, LX/0Ms;->d:I

    .line 48278
    return-wide v0
.end method

.method public final a(LX/0MA;LX/0Oj;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48238
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    move v4, v2

    .line 48239
    :goto_1
    if-nez v4, :cond_5

    .line 48240
    iget v0, p0, LX/0Ms;->d:I

    if-gez v0, :cond_2

    .line 48241
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget-object v3, p0, LX/0Ms;->b:LX/0Oj;

    invoke-static {p1, v0, v3, v1}, LX/0Mw;->a(LX/0MA;LX/0Mv;LX/0Oj;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 48242
    :goto_2
    return v2

    :cond_0
    move v0, v2

    .line 48243
    goto :goto_0

    .line 48244
    :cond_1
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget v0, v0, LX/0Mv;->h:I

    .line 48245
    iget-object v3, p0, LX/0Ms;->a:LX/0Mv;

    iget v3, v3, LX/0Mv;->b:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_7

    .line 48246
    iget v3, p2, LX/0Oj;->c:I

    move v3, v3

    .line 48247
    if-nez v3, :cond_7

    .line 48248
    iget-object v3, p0, LX/0Ms;->a:LX/0Mv;

    iget-object v5, p0, LX/0Ms;->c:LX/0Mu;

    invoke-static {v3, v2, v5}, LX/0Mw;->a(LX/0Mv;ILX/0Mu;)V

    .line 48249
    iget-object v3, p0, LX/0Ms;->c:LX/0Mu;

    iget v3, v3, LX/0Mu;->b:I

    add-int/lit8 v3, v3, 0x0

    .line 48250
    iget-object v5, p0, LX/0Ms;->c:LX/0Mu;

    iget v5, v5, LX/0Mu;->a:I

    add-int/2addr v0, v5

    .line 48251
    :goto_3
    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    .line 48252
    iput v3, p0, LX/0Ms;->d:I

    .line 48253
    :cond_2
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget v3, p0, LX/0Ms;->d:I

    iget-object v5, p0, LX/0Ms;->c:LX/0Mu;

    invoke-static {v0, v3, v5}, LX/0Mw;->a(LX/0Mv;ILX/0Mu;)V

    .line 48254
    iget v0, p0, LX/0Ms;->d:I

    iget-object v3, p0, LX/0Ms;->c:LX/0Mu;

    iget v3, v3, LX/0Mu;->b:I

    add-int/2addr v3, v0

    .line 48255
    iget-object v0, p0, LX/0Ms;->c:LX/0Mu;

    iget v0, v0, LX/0Mu;->a:I

    if-lez v0, :cond_6

    .line 48256
    iget-object v0, p2, LX/0Oj;->a:[B

    .line 48257
    iget v4, p2, LX/0Oj;->c:I

    move v4, v4

    .line 48258
    iget-object v5, p0, LX/0Ms;->c:LX/0Mu;

    iget v5, v5, LX/0Mu;->a:I

    invoke-interface {p1, v0, v4, v5}, LX/0MA;->b([BII)V

    .line 48259
    iget v0, p2, LX/0Oj;->c:I

    move v0, v0

    .line 48260
    iget-object v4, p0, LX/0Ms;->c:LX/0Mu;

    iget v4, v4, LX/0Mu;->a:I

    add-int/2addr v0, v4

    invoke-virtual {p2, v0}, LX/0Oj;->a(I)V

    .line 48261
    iget-object v0, p0, LX/0Ms;->a:LX/0Mv;

    iget-object v0, v0, LX/0Mv;->j:[I

    add-int/lit8 v4, v3, -0x1

    aget v0, v0, v4

    const/16 v4, 0xff

    if-eq v0, v4, :cond_4

    move v0, v1

    .line 48262
    :goto_4
    iget-object v4, p0, LX/0Ms;->a:LX/0Mv;

    iget v4, v4, LX/0Mv;->g:I

    if-ne v3, v4, :cond_3

    const/4 v3, -0x1

    :cond_3
    iput v3, p0, LX/0Ms;->d:I

    move v4, v0

    .line 48263
    goto :goto_1

    :cond_4
    move v0, v2

    .line 48264
    goto :goto_4

    :cond_5
    move v2, v1

    .line 48265
    goto :goto_2

    :cond_6
    move v0, v4

    goto :goto_4

    :cond_7
    move v3, v2

    goto :goto_3
.end method
