.class public LX/0Od;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0LX;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0LX;"
    }
.end annotation


# instance fields
.field public volatile a:Ljava/lang/String;

.field private final b:LX/0Aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Aa",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:LX/0G7;

.field public final d:Landroid/os/Handler;

.field public final e:LX/0GI;

.field public f:I

.field public g:LX/0ON;

.field private h:LX/0OP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0OP",
            "<TT;>;"
        }
    .end annotation
.end field

.field private i:J

.field public j:I

.field private k:J

.field public l:LX/0Ob;

.field public volatile m:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public volatile n:J

.field public volatile o:J


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0G7;LX/0Aa;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0G7;",
            "LX/0Aa",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 53433
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, LX/0Od;-><init>(Ljava/lang/String;LX/0G7;LX/0Aa;Landroid/os/Handler;LX/0GI;)V

    .line 53434
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;LX/0G7;LX/0Aa;Landroid/os/Handler;LX/0GI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0G7;",
            "LX/0Aa",
            "<TT;>;",
            "Landroid/os/Handler;",
            "LX/0GI;",
            ")V"
        }
    .end annotation

    .prologue
    .line 53496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53497
    iput-object p3, p0, LX/0Od;->b:LX/0Aa;

    .line 53498
    iput-object p1, p0, LX/0Od;->a:Ljava/lang/String;

    .line 53499
    iput-object p2, p0, LX/0Od;->c:LX/0G7;

    .line 53500
    iput-object p4, p0, LX/0Od;->d:Landroid/os/Handler;

    .line 53501
    iput-object p5, p0, LX/0Od;->e:LX/0GI;

    .line 53502
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0G7;LX/0Aa;Landroid/os/Handler;LX/0GI;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0G7;",
            "LX/0Aa",
            "<TT;>;",
            "Landroid/os/Handler;",
            "LX/0GI;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 53488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53489
    iput-object p3, p0, LX/0Od;->b:LX/0Aa;

    .line 53490
    iput-object p1, p0, LX/0Od;->a:Ljava/lang/String;

    .line 53491
    iput-object p2, p0, LX/0Od;->c:LX/0G7;

    .line 53492
    iput-object p4, p0, LX/0Od;->d:Landroid/os/Handler;

    .line 53493
    iput-object p5, p0, LX/0Od;->e:LX/0GI;

    .line 53494
    iput-object p6, p0, LX/0Od;->m:Ljava/lang/Object;

    .line 53495
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 53487
    iget-object v0, p0, LX/0Od;->m:Ljava/lang/Object;

    return-object v0
.end method

.method public final a(LX/0LO;)V
    .locals 2

    .prologue
    .line 53470
    iget-object v0, p0, LX/0Od;->h:LX/0OP;

    if-eq v0, p1, :cond_0

    .line 53471
    :goto_0
    return-void

    .line 53472
    :cond_0
    iget-object v0, p0, LX/0Od;->h:LX/0OP;

    .line 53473
    iget-object v1, v0, LX/0OP;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 53474
    iput-object v0, p0, LX/0Od;->m:Ljava/lang/Object;

    .line 53475
    iget-wide v0, p0, LX/0Od;->i:J

    iput-wide v0, p0, LX/0Od;->n:J

    .line 53476
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0Od;->o:J

    .line 53477
    const/4 v0, 0x0

    iput v0, p0, LX/0Od;->j:I

    .line 53478
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Od;->l:LX/0Ob;

    .line 53479
    iget-object v0, p0, LX/0Od;->m:Ljava/lang/Object;

    instance-of v0, v0, LX/0Ax;

    if-eqz v0, :cond_1

    .line 53480
    iget-object v0, p0, LX/0Od;->m:Ljava/lang/Object;

    check-cast v0, LX/0Ax;

    .line 53481
    invoke-interface {v0}, LX/0Ax;->a()Ljava/lang/String;

    move-result-object v0

    .line 53482
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 53483
    iput-object v0, p0, LX/0Od;->a:Ljava/lang/String;

    .line 53484
    :cond_1
    iget-object v0, p0, LX/0Od;->d:Landroid/os/Handler;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0Od;->e:LX/0GI;

    if-eqz v0, :cond_2

    .line 53485
    iget-object v0, p0, LX/0Od;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/util/ManifestFetcher$3;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer/util/ManifestFetcher$3;-><init>(LX/0Od;)V

    const p1, -0x2448b97b

    invoke-static {v0, v1, p1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 53486
    :cond_2
    goto :goto_0
.end method

.method public final a(LX/0LO;Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 53461
    iget-object v0, p0, LX/0Od;->h:LX/0OP;

    if-eq v0, p1, :cond_0

    .line 53462
    :goto_0
    return-void

    .line 53463
    :cond_0
    iget v0, p0, LX/0Od;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Od;->j:I

    .line 53464
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0Od;->k:J

    .line 53465
    new-instance v0, LX/0Ob;

    invoke-direct {v0, p2}, LX/0Ob;-><init>(Ljava/lang/Throwable;)V

    iput-object v0, p0, LX/0Od;->l:LX/0Ob;

    .line 53466
    iget-object v0, p0, LX/0Od;->l:LX/0Ob;

    .line 53467
    iget-object v1, p0, LX/0Od;->d:Landroid/os/Handler;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0Od;->e:LX/0GI;

    if-eqz v1, :cond_1

    .line 53468
    iget-object v1, p0, LX/0Od;->d:Landroid/os/Handler;

    new-instance p1, Lcom/google/android/exoplayer/util/ManifestFetcher$4;

    invoke-direct {p1, p0, v0}, Lcom/google/android/exoplayer/util/ManifestFetcher$4;-><init>(LX/0Od;Ljava/io/IOException;)V

    const p2, 0xbaee929

    invoke-static {v1, p1, p2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 53469
    :cond_1
    goto :goto_0
.end method

.method public final a(Landroid/os/Looper;LX/0GJ;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Looper;",
            "LX/0GJ",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 53457
    new-instance v0, LX/0Oc;

    new-instance v1, LX/0OP;

    iget-object v2, p0, LX/0Od;->a:Ljava/lang/String;

    iget-object v3, p0, LX/0Od;->c:LX/0G7;

    iget-object v4, p0, LX/0Od;->b:LX/0Aa;

    invoke-direct {v1, v2, v3, v4}, LX/0OP;-><init>(Ljava/lang/String;LX/0G7;LX/0Aa;)V

    invoke-direct {v0, p0, v1, p1, p2}, LX/0Oc;-><init>(LX/0Od;LX/0OP;Landroid/os/Looper;LX/0GJ;)V

    .line 53458
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iput-wide v5, v0, LX/0Oc;->f:J

    .line 53459
    iget-object v5, v0, LX/0Oc;->e:LX/0ON;

    iget-object v6, v0, LX/0Oc;->c:Landroid/os/Looper;

    iget-object v7, v0, LX/0Oc;->b:LX/0OP;

    invoke-virtual {v5, v6, v7, v0}, LX/0ON;->a(Landroid/os/Looper;LX/0LO;LX/0LX;)V

    .line 53460
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 53454
    iget-object v0, p0, LX/0Od;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Od;->e:LX/0GI;

    if-eqz v0, :cond_0

    .line 53455
    iget-object v0, p0, LX/0Od;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/util/ManifestFetcher$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/exoplayer/util/ManifestFetcher$1;-><init>(LX/0Od;Ljava/lang/String;Ljava/lang/String;)V

    const v2, 0x10419bb3

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 53456
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 53451
    iget-object v0, p0, LX/0Od;->l:LX/0Ob;

    if-eqz v0, :cond_0

    iget v0, p0, LX/0Od;->j:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    .line 53452
    :cond_0
    return-void

    .line 53453
    :cond_1
    iget-object v0, p0, LX/0Od;->l:LX/0Ob;

    throw v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 53450
    return-void
.end method

.method public final g()V
    .locals 10

    .prologue
    .line 53435
    iget-object v0, p0, LX/0Od;->l:LX/0Ob;

    if-eqz v0, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, LX/0Od;->k:J

    iget v4, p0, LX/0Od;->j:I

    int-to-long v4, v4

    .line 53436
    const-wide/16 v6, 0x1

    sub-long v6, v4, v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x1388

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    move-wide v4, v6

    .line 53437
    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 53438
    :cond_0
    :goto_0
    return-void

    .line 53439
    :cond_1
    iget-object v0, p0, LX/0Od;->g:LX/0ON;

    if-nez v0, :cond_2

    .line 53440
    new-instance v0, LX/0ON;

    const-string v1, "manifestLoader"

    invoke-direct {v0, v1}, LX/0ON;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0Od;->g:LX/0ON;

    .line 53441
    :cond_2
    iget-object v0, p0, LX/0Od;->g:LX/0ON;

    .line 53442
    iget-boolean v1, v0, LX/0ON;->c:Z

    move v0, v1

    .line 53443
    if-nez v0, :cond_0

    .line 53444
    new-instance v0, LX/0OP;

    iget-object v1, p0, LX/0Od;->a:Ljava/lang/String;

    iget-object v2, p0, LX/0Od;->c:LX/0G7;

    iget-object v3, p0, LX/0Od;->b:LX/0Aa;

    invoke-direct {v0, v1, v2, v3}, LX/0OP;-><init>(Ljava/lang/String;LX/0G7;LX/0Aa;)V

    iput-object v0, p0, LX/0Od;->h:LX/0OP;

    .line 53445
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0Od;->i:J

    .line 53446
    iget-object v0, p0, LX/0Od;->g:LX/0ON;

    iget-object v1, p0, LX/0Od;->h:LX/0OP;

    invoke-virtual {v0, v1, p0}, LX/0ON;->a(LX/0LO;LX/0LX;)V

    .line 53447
    iget-object v0, p0, LX/0Od;->d:Landroid/os/Handler;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0Od;->e:LX/0GI;

    if-eqz v0, :cond_3

    .line 53448
    iget-object v0, p0, LX/0Od;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/util/ManifestFetcher$2;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer/util/ManifestFetcher$2;-><init>(LX/0Od;)V

    const v2, 0x699392cd

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 53449
    :cond_3
    goto :goto_0
.end method
