.class public final LX/080;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final artFilter:B

.field public final artHugeMethodMax:I

.field public final artLargeMethodMax:I

.field public final artSmallMethodMax:I

.field public final artTinyMethodMax:I

.field public final artTruncatedDexSize:I

.field public final dalvikOptimize:B

.field public final dalvikRegisterMaps:B

.field public final dalvikVerify:B

.field public final mode:B

.field public final sync:B


# direct methods
.method private constructor <init>(BBBBBBIIIII)V
    .locals 0

    .prologue
    .line 20464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20465
    iput-byte p1, p0, LX/080;->mode:B

    .line 20466
    iput-byte p2, p0, LX/080;->sync:B

    .line 20467
    iput-byte p3, p0, LX/080;->dalvikVerify:B

    .line 20468
    iput-byte p4, p0, LX/080;->dalvikOptimize:B

    .line 20469
    iput-byte p5, p0, LX/080;->dalvikRegisterMaps:B

    .line 20470
    iput-byte p6, p0, LX/080;->artFilter:B

    .line 20471
    iput p7, p0, LX/080;->artHugeMethodMax:I

    .line 20472
    iput p8, p0, LX/080;->artLargeMethodMax:I

    .line 20473
    iput p9, p0, LX/080;->artSmallMethodMax:I

    .line 20474
    iput p10, p0, LX/080;->artTinyMethodMax:I

    .line 20475
    iput p11, p0, LX/080;->artTruncatedDexSize:I

    .line 20476
    return-void
.end method

.method public synthetic constructor <init>(BBBBBBIIIIILX/08b;)V
    .locals 0

    .prologue
    .line 20477
    invoke-direct/range {p0 .. p11}, LX/080;-><init>(BBBBBBIIIII)V

    return-void
.end method

.method public static read(Ljava/io/File;)LX/080;
    .locals 15

    .prologue
    .line 20478
    new-instance v13, Ljava/io/RandomAccessFile;

    const-string v0, "r"

    invoke-direct {v13, p0, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v12, 0x0

    .line 20479
    :try_start_0
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v0

    .line 20480
    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 20481
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "unexpected version"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 20482
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 20483
    :catchall_0
    move-exception v1

    move-object v14, v1

    move-object v1, v0

    move-object v0, v14

    :goto_0
    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    .line 20484
    :cond_0
    :try_start_3
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v1

    .line 20485
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v2

    .line 20486
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v3

    .line 20487
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v4

    .line 20488
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v5

    .line 20489
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v6

    .line 20490
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v7

    .line 20491
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v8

    .line 20492
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v9

    .line 20493
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v10

    .line 20494
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v11

    .line 20495
    new-instance v0, LX/080;

    invoke-direct/range {v0 .. v11}, LX/080;-><init>(BBBBBBIIIII)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 20496
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->close()V

    return-object v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v1, v12

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 20497
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/080;

    if-nez v1, :cond_1

    .line 20498
    :cond_0
    :goto_0
    return v0

    .line 20499
    :cond_1
    check-cast p1, LX/080;

    .line 20500
    iget-byte v1, p0, LX/080;->mode:B

    iget-byte v2, p1, LX/080;->mode:B

    if-ne v1, v2, :cond_0

    iget-byte v1, p0, LX/080;->sync:B

    iget-byte v2, p1, LX/080;->sync:B

    if-ne v1, v2, :cond_0

    iget-byte v1, p0, LX/080;->dalvikVerify:B

    iget-byte v2, p1, LX/080;->dalvikVerify:B

    if-ne v1, v2, :cond_0

    iget-byte v1, p0, LX/080;->dalvikOptimize:B

    iget-byte v2, p1, LX/080;->dalvikOptimize:B

    if-ne v1, v2, :cond_0

    iget-byte v1, p0, LX/080;->dalvikRegisterMaps:B

    iget-byte v2, p1, LX/080;->dalvikRegisterMaps:B

    if-ne v1, v2, :cond_0

    iget-byte v1, p0, LX/080;->artFilter:B

    iget-byte v2, p1, LX/080;->artFilter:B

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/080;->artHugeMethodMax:I

    iget v2, p1, LX/080;->artHugeMethodMax:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/080;->artLargeMethodMax:I

    iget v2, p1, LX/080;->artLargeMethodMax:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/080;->artSmallMethodMax:I

    iget v2, p1, LX/080;->artSmallMethodMax:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/080;->artTinyMethodMax:I

    iget v2, p1, LX/080;->artTinyMethodMax:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/080;->artTruncatedDexSize:I

    iget v2, p1, LX/080;->artTruncatedDexSize:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 20501
    iget-byte v0, p0, LX/080;->mode:B

    add-int/lit16 v0, v0, 0x294b

    .line 20502
    mul-int/lit8 v0, v0, 0x1f

    iget-byte v1, p0, LX/080;->sync:B

    add-int/2addr v0, v1

    .line 20503
    mul-int/lit8 v0, v0, 0x1f

    iget-byte v1, p0, LX/080;->dalvikVerify:B

    add-int/2addr v0, v1

    .line 20504
    mul-int/lit8 v0, v0, 0x1f

    iget-byte v1, p0, LX/080;->dalvikOptimize:B

    add-int/2addr v0, v1

    .line 20505
    mul-int/lit8 v0, v0, 0x1f

    iget-byte v1, p0, LX/080;->dalvikRegisterMaps:B

    add-int/2addr v0, v1

    .line 20506
    mul-int/lit8 v0, v0, 0x1f

    iget-byte v1, p0, LX/080;->artFilter:B

    add-int/2addr v0, v1

    .line 20507
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/080;->artHugeMethodMax:I

    add-int/2addr v0, v1

    .line 20508
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/080;->artLargeMethodMax:I

    add-int/2addr v0, v1

    .line 20509
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/080;->artSmallMethodMax:I

    add-int/2addr v0, v1

    .line 20510
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/080;->artTinyMethodMax:I

    add-int/2addr v0, v1

    .line 20511
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/080;->artTruncatedDexSize:I

    add-int/2addr v0, v1

    .line 20512
    return v0
.end method

.method public final writeAndSync(Ljava/io/File;)V
    .locals 7

    .prologue
    .line 20513
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v0, "rw"

    invoke-direct {v2, p1, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 20514
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->writeByte(I)V

    .line 20515
    iget-byte v0, p0, LX/080;->mode:B

    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->writeByte(I)V

    .line 20516
    iget-byte v0, p0, LX/080;->sync:B

    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->writeByte(I)V

    .line 20517
    iget-byte v0, p0, LX/080;->dalvikVerify:B

    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->writeByte(I)V

    .line 20518
    iget-byte v0, p0, LX/080;->dalvikOptimize:B

    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->writeByte(I)V

    .line 20519
    iget-byte v0, p0, LX/080;->dalvikRegisterMaps:B

    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->writeByte(I)V

    .line 20520
    iget-byte v0, p0, LX/080;->artFilter:B

    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->writeByte(I)V

    .line 20521
    iget v0, p0, LX/080;->artHugeMethodMax:I

    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 20522
    iget v0, p0, LX/080;->artLargeMethodMax:I

    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 20523
    iget v0, p0, LX/080;->artSmallMethodMax:I

    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 20524
    iget v0, p0, LX/080;->artTinyMethodMax:I

    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 20525
    iget v0, p0, LX/080;->artTruncatedDexSize:I

    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 20526
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 20527
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 20528
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V

    return-void

    .line 20529
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 20530
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_0
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method
