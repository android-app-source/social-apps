.class public final enum LX/08R;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/08R;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/08R;

.field public static final enum APPLICATION:LX/08R;

.field public static final enum CONTAINER:LX/08R;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21117
    new-instance v0, LX/08R;

    const-string v1, "CONTAINER"

    invoke-direct {v0, v1, v2}, LX/08R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/08R;->CONTAINER:LX/08R;

    .line 21118
    new-instance v0, LX/08R;

    const-string v1, "APPLICATION"

    invoke-direct {v0, v1, v3}, LX/08R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/08R;->APPLICATION:LX/08R;

    .line 21119
    const/4 v0, 0x2

    new-array v0, v0, [LX/08R;

    sget-object v1, LX/08R;->CONTAINER:LX/08R;

    aput-object v1, v0, v2

    sget-object v1, LX/08R;->APPLICATION:LX/08R;

    aput-object v1, v0, v3

    sput-object v0, LX/08R;->$VALUES:[LX/08R;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21114
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/08R;
    .locals 1

    .prologue
    .line 21116
    const-class v0, LX/08R;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/08R;

    return-object v0
.end method

.method public static values()[LX/08R;
    .locals 1

    .prologue
    .line 21115
    sget-object v0, LX/08R;->$VALUES:[LX/08R;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/08R;

    return-object v0
.end method
