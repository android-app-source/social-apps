.class public final enum LX/09L;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/09L;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/09L;

.field public static final enum DASH:LX/09L;

.field public static final enum DASH_LIVE:LX/09L;

.field public static final enum HLS:LX/09L;

.field public static final enum PROGRESSIVE_DOWNLOAD:LX/09L;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22335
    new-instance v0, LX/09L;

    const-string v1, "DASH"

    const-string v2, "dash"

    invoke-direct {v0, v1, v3, v2}, LX/09L;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/09L;->DASH:LX/09L;

    .line 22336
    new-instance v0, LX/09L;

    const-string v1, "DASH_LIVE"

    const-string v2, "dash_live"

    invoke-direct {v0, v1, v4, v2}, LX/09L;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/09L;->DASH_LIVE:LX/09L;

    .line 22337
    new-instance v0, LX/09L;

    const-string v1, "HLS"

    const-string v2, "hls"

    invoke-direct {v0, v1, v5, v2}, LX/09L;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/09L;->HLS:LX/09L;

    .line 22338
    new-instance v0, LX/09L;

    const-string v1, "PROGRESSIVE_DOWNLOAD"

    const-string v2, "progressive"

    invoke-direct {v0, v1, v6, v2}, LX/09L;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    .line 22339
    const/4 v0, 0x4

    new-array v0, v0, [LX/09L;

    sget-object v1, LX/09L;->DASH:LX/09L;

    aput-object v1, v0, v3

    sget-object v1, LX/09L;->DASH_LIVE:LX/09L;

    aput-object v1, v0, v4

    sget-object v1, LX/09L;->HLS:LX/09L;

    aput-object v1, v0, v5

    sget-object v1, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    aput-object v1, v0, v6

    sput-object v0, LX/09L;->$VALUES:[LX/09L;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22340
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22341
    iput-object p3, p0, LX/09L;->value:Ljava/lang/String;

    .line 22342
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/09L;
    .locals 1

    .prologue
    .line 22343
    const-class v0, LX/09L;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/09L;

    return-object v0
.end method

.method public static values()[LX/09L;
    .locals 1

    .prologue
    .line 22344
    sget-object v0, LX/09L;->$VALUES:[LX/09L;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/09L;

    return-object v0
.end method
