.class public LX/03F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/03G;


# static fields
.field public static final a:LX/03F;


# instance fields
.field private b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9878
    new-instance v0, LX/03F;

    invoke-direct {v0}, LX/03F;-><init>()V

    sput-object v0, LX/03F;->a:LX/03F;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9877
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 9874
    iput p1, p0, LX/03F;->b:I

    .line 9875
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 9872
    invoke-static {p1, p2, p3}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 9873
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 9870
    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 9871
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 9879
    invoke-static {p1, p2, p3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 9880
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 9869
    iget v0, p0, LX/03F;->b:I

    return v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 9867
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 9868
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 9865
    invoke-static {p1, p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 9866
    return-void
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 9864
    iget v0, p0, LX/03F;->b:I

    if-gt v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 9862
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 9863
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 9860
    invoke-static {p1, p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 9861
    return-void
.end method
