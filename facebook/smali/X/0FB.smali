.class public final enum LX/0FB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0FB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0FB;

.field public static final enum PAYMENTS_CHECKOUT:LX/0FB;

.field public static final enum PAYMENTS_CHECKOUT_CHARGE_REQUEST_ERROR_RETURN:LX/0FB;

.field public static final enum PAYMENTS_CHECKOUT_CHARGE_REQUEST_SUCCESS_RETURN:LX/0FB;

.field public static final enum PAYMENTS_CHECKOUT_DUMMY_RETURN:LX/0FB;

.field public static final enum PAYMENTS_CHECKOUT_SHIPPING_ADDRESS_RETURN:LX/0FB;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32918
    new-instance v0, LX/0FB;

    const-string v1, "PAYMENTS_CHECKOUT"

    const-string v2, "paymentsCheckout"

    invoke-direct {v0, v1, v3, v2}, LX/0FB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0FB;->PAYMENTS_CHECKOUT:LX/0FB;

    .line 32919
    new-instance v0, LX/0FB;

    const-string v1, "PAYMENTS_CHECKOUT_CHARGE_REQUEST_SUCCESS_RETURN"

    const-string v2, "paymentsCheckoutChargeRequestSuccessReturn"

    invoke-direct {v0, v1, v4, v2}, LX/0FB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0FB;->PAYMENTS_CHECKOUT_CHARGE_REQUEST_SUCCESS_RETURN:LX/0FB;

    .line 32920
    new-instance v0, LX/0FB;

    const-string v1, "PAYMENTS_CHECKOUT_CHARGE_REQUEST_ERROR_RETURN"

    const-string v2, "paymentsCheckoutChargeRequestErrorReturn"

    invoke-direct {v0, v1, v5, v2}, LX/0FB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0FB;->PAYMENTS_CHECKOUT_CHARGE_REQUEST_ERROR_RETURN:LX/0FB;

    .line 32921
    new-instance v0, LX/0FB;

    const-string v1, "PAYMENTS_CHECKOUT_SHIPPING_ADDRESS_RETURN"

    const-string v2, "paymentsCheckoutShippingAddressReturn"

    invoke-direct {v0, v1, v6, v2}, LX/0FB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0FB;->PAYMENTS_CHECKOUT_SHIPPING_ADDRESS_RETURN:LX/0FB;

    .line 32922
    new-instance v0, LX/0FB;

    const-string v1, "PAYMENTS_CHECKOUT_DUMMY_RETURN"

    const-string v2, "paymentsCheckoutDummyReturn"

    invoke-direct {v0, v1, v7, v2}, LX/0FB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0FB;->PAYMENTS_CHECKOUT_DUMMY_RETURN:LX/0FB;

    .line 32923
    const/4 v0, 0x5

    new-array v0, v0, [LX/0FB;

    sget-object v1, LX/0FB;->PAYMENTS_CHECKOUT:LX/0FB;

    aput-object v1, v0, v3

    sget-object v1, LX/0FB;->PAYMENTS_CHECKOUT_CHARGE_REQUEST_SUCCESS_RETURN:LX/0FB;

    aput-object v1, v0, v4

    sget-object v1, LX/0FB;->PAYMENTS_CHECKOUT_CHARGE_REQUEST_ERROR_RETURN:LX/0FB;

    aput-object v1, v0, v5

    sget-object v1, LX/0FB;->PAYMENTS_CHECKOUT_SHIPPING_ADDRESS_RETURN:LX/0FB;

    aput-object v1, v0, v6

    sget-object v1, LX/0FB;->PAYMENTS_CHECKOUT_DUMMY_RETURN:LX/0FB;

    aput-object v1, v0, v7

    sput-object v0, LX/0FB;->$VALUES:[LX/0FB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32924
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32925
    iput-object p3, p0, LX/0FB;->value:Ljava/lang/String;

    .line 32926
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0FB;
    .locals 1

    .prologue
    .line 32927
    const-class v0, LX/0FB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0FB;

    return-object v0
.end method

.method public static values()[LX/0FB;
    .locals 1

    .prologue
    .line 32928
    sget-object v0, LX/0FB;->$VALUES:[LX/0FB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0FB;

    return-object v0
.end method
