.class public LX/09u;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/nio/charset/Charset;

.field private final b:Ljava/security/MessageDigest;

.field private final c:[B

.field private final d:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23086
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23087
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    iput-object v0, p0, LX/09u;->b:Ljava/security/MessageDigest;

    .line 23088
    iget-object v0, p0, LX/09u;->b:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->getDigestLength()I

    move-result v0

    .line 23089
    new-array v0, v0, [B

    iput-object v0, p0, LX/09u;->c:[B

    .line 23090
    const/16 v0, 0x20

    new-array v0, v0, [B

    iput-object v0, p0, LX/09u;->d:[B

    .line 23091
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    iput-object v0, p0, LX/09u;->a:Ljava/nio/charset/Charset;

    .line 23092
    return-void
.end method

.method private static a(LX/09u;Ljava/io/InputStream;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 23093
    :try_start_0
    iget-object v0, p0, LX/09u;->b:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->reset()V

    .line 23094
    new-instance v0, Ljava/security/DigestInputStream;

    iget-object v1, p0, LX/09u;->b:Ljava/security/MessageDigest;

    invoke-direct {v0, p1, v1}, Ljava/security/DigestInputStream;-><init>(Ljava/io/InputStream;Ljava/security/MessageDigest;)V

    .line 23095
    new-instance v1, Ljava/io/InputStreamReader;

    iget-object v2, p0, LX/09u;->a:Ljava/nio/charset/Charset;

    invoke-direct {v1, v0, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    .line 23096
    invoke-static {v1}, LX/2Eb;->a(Ljava/lang/Readable;)Ljava/lang/String;

    move-result-object v0

    .line 23097
    iget-object v1, p0, LX/09u;->b:Ljava/security/MessageDigest;

    iget-object v2, p0, LX/09u;->c:[B

    const/4 v3, 0x0

    iget-object v4, p0, LX/09u;->c:[B

    array-length v4, v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/security/MessageDigest;->digest([BII)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23098
    return-object v0

    .line 23099
    :catch_0
    move-exception v0

    .line 23100
    new-instance v1, LX/09v;

    const-string v2, "Error reading log contents"

    invoke-direct {v1, v2, v0}, LX/09v;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/io/DataInputStream;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 23101
    :try_start_0
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUnsignedByte()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 23102
    int-to-char v0, v0

    .line 23103
    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 23104
    :catch_0
    move-exception v0

    .line 23105
    new-instance v1, LX/09v;

    const-string v2, "Error reading status byte"

    invoke-direct {v1, v2, v0}, LX/09v;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static b(LX/09u;Ljava/io/DataInputStream;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 23106
    :try_start_0
    iget-object v0, p0, LX/09u;->d:[B

    invoke-virtual {p1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    .line 23107
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/09u;->d:[B

    iget-object v2, p0, LX/09u;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23108
    return-object v0

    .line 23109
    :catch_0
    move-exception v0

    .line 23110
    new-instance v1, LX/09v;

    const-string v2, "Error reading checksum"

    invoke-direct {v1, v2, v0}, LX/09v;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;Ljava/lang/String;J)Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;
    .locals 9

    .prologue
    .line 23111
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 23112
    invoke-static {v0}, LX/09u;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v2

    .line 23113
    invoke-static {p0, v0}, LX/09u;->b(LX/09u;Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v3

    .line 23114
    invoke-static {p0, p1}, LX/09u;->a(LX/09u;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    .line 23115
    new-instance v1, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;

    iget-object v8, p0, LX/09u;->c:[B

    move-object v5, p2

    move-wide v6, p3

    invoke-direct/range {v1 .. v8}, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J[B)V

    return-object v1
.end method
