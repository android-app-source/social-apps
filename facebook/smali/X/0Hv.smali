.class public final enum LX/0Hv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Hv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Hv;

.field public static final enum AGENT:LX/0Hv;

.field public static final enum APP_ID:LX/0Hv;

.field public static final enum APP_SPECIFIC_INFO:LX/0Hv;

.field public static final enum CAPABILITIES:LX/0Hv;

.field public static final enum CLIENT_MQTT_SESSION_ID:LX/0Hv;

.field public static final enum CLIENT_STACK:LX/0Hv;

.field public static final enum CLIENT_TYPE:LX/0Hv;

.field public static final enum CONNECT_HASH:LX/0Hv;

.field public static final enum DATACENTER_PREFERENCE:LX/0Hv;

.field public static final enum DEVICE_ID:LX/0Hv;

.field public static final enum DEVICE_SECRET:LX/0Hv;

.field public static final enum ENDPOINT_CAPABILITIES:LX/0Hv;

.field public static final enum FBNS_CONNECTION_KEY:LX/0Hv;

.field public static final enum FBNS_CONNECTION_SECRET:LX/0Hv;

.field public static final enum FBNS_DEVICE_ID:LX/0Hv;

.field public static final enum FBNS_DEVICE_SECRET:LX/0Hv;

.field public static final enum INITIAL_FOREGROUND_STATE:LX/0Hv;

.field public static final enum MAKE_USER_AVAILABLE_IN_FOREGROUND:LX/0Hv;

.field public static final enum NETWORK_SUBTYPE:LX/0Hv;

.field public static final enum NETWORK_TYPE:LX/0Hv;

.field public static final enum NO_AUTOMATIC_FOREGROUND:LX/0Hv;

.field public static final enum OVERRIDE_NECTAR_LOGGING:LX/0Hv;

.field public static final enum PUBLISH_FORMAT:LX/0Hv;

.field public static final enum SESSION_ID:LX/0Hv;

.field public static final enum SUBSCRIBE_TOPICS:LX/0Hv;

.field public static final enum USER_ID:LX/0Hv;


# instance fields
.field private final mJsonKey:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 38225
    new-instance v0, LX/0Hv;

    const-string v1, "USER_ID"

    const-string v2, "u"

    invoke-direct {v0, v1, v4, v2}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->USER_ID:LX/0Hv;

    .line 38226
    new-instance v0, LX/0Hv;

    const-string v1, "SESSION_ID"

    const-string v2, "s"

    invoke-direct {v0, v1, v5, v2}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->SESSION_ID:LX/0Hv;

    .line 38227
    new-instance v0, LX/0Hv;

    const-string v1, "AGENT"

    const-string v2, "a"

    invoke-direct {v0, v1, v6, v2}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->AGENT:LX/0Hv;

    .line 38228
    new-instance v0, LX/0Hv;

    const-string v1, "CAPABILITIES"

    const-string v2, "cp"

    invoke-direct {v0, v1, v7, v2}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->CAPABILITIES:LX/0Hv;

    .line 38229
    new-instance v0, LX/0Hv;

    const-string v1, "ENDPOINT_CAPABILITIES"

    const-string v2, "ecp"

    invoke-direct {v0, v1, v8, v2}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->ENDPOINT_CAPABILITIES:LX/0Hv;

    .line 38230
    new-instance v0, LX/0Hv;

    const-string v1, "PUBLISH_FORMAT"

    const/4 v2, 0x5

    const-string v3, "pf"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->PUBLISH_FORMAT:LX/0Hv;

    .line 38231
    new-instance v0, LX/0Hv;

    const-string v1, "NO_AUTOMATIC_FOREGROUND"

    const/4 v2, 0x6

    const-string v3, "no_auto_fg"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->NO_AUTOMATIC_FOREGROUND:LX/0Hv;

    .line 38232
    new-instance v0, LX/0Hv;

    const-string v1, "MAKE_USER_AVAILABLE_IN_FOREGROUND"

    const/4 v2, 0x7

    const-string v3, "chat_on"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->MAKE_USER_AVAILABLE_IN_FOREGROUND:LX/0Hv;

    .line 38233
    new-instance v0, LX/0Hv;

    const-string v1, "INITIAL_FOREGROUND_STATE"

    const/16 v2, 0x8

    const-string v3, "fg"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->INITIAL_FOREGROUND_STATE:LX/0Hv;

    .line 38234
    new-instance v0, LX/0Hv;

    const-string v1, "DEVICE_ID"

    const/16 v2, 0x9

    const-string v3, "d"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->DEVICE_ID:LX/0Hv;

    .line 38235
    new-instance v0, LX/0Hv;

    const-string v1, "DEVICE_SECRET"

    const/16 v2, 0xa

    const-string v3, "ds"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->DEVICE_SECRET:LX/0Hv;

    .line 38236
    new-instance v0, LX/0Hv;

    const-string v1, "NETWORK_TYPE"

    const/16 v2, 0xb

    const-string v3, "nwt"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->NETWORK_TYPE:LX/0Hv;

    .line 38237
    new-instance v0, LX/0Hv;

    const-string v1, "NETWORK_SUBTYPE"

    const/16 v2, 0xc

    const-string v3, "nwst"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->NETWORK_SUBTYPE:LX/0Hv;

    .line 38238
    new-instance v0, LX/0Hv;

    const-string v1, "CLIENT_MQTT_SESSION_ID"

    const/16 v2, 0xd

    const-string v3, "mqtt_sid"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->CLIENT_MQTT_SESSION_ID:LX/0Hv;

    .line 38239
    new-instance v0, LX/0Hv;

    const-string v1, "SUBSCRIBE_TOPICS"

    const/16 v2, 0xe

    const-string v3, "st"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->SUBSCRIBE_TOPICS:LX/0Hv;

    .line 38240
    new-instance v0, LX/0Hv;

    const-string v1, "CLIENT_TYPE"

    const/16 v2, 0xf

    const-string v3, "ct"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->CLIENT_TYPE:LX/0Hv;

    .line 38241
    new-instance v0, LX/0Hv;

    const-string v1, "APP_ID"

    const/16 v2, 0x10

    const-string v3, "aid"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->APP_ID:LX/0Hv;

    .line 38242
    new-instance v0, LX/0Hv;

    const-string v1, "OVERRIDE_NECTAR_LOGGING"

    const/16 v2, 0x11

    const-string v3, "log"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->OVERRIDE_NECTAR_LOGGING:LX/0Hv;

    .line 38243
    new-instance v0, LX/0Hv;

    const-string v1, "DATACENTER_PREFERENCE"

    const/16 v2, 0x12

    const-string v3, "dc"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->DATACENTER_PREFERENCE:LX/0Hv;

    .line 38244
    new-instance v0, LX/0Hv;

    const-string v1, "CONNECT_HASH"

    const/16 v2, 0x13

    const-string v3, "h"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->CONNECT_HASH:LX/0Hv;

    .line 38245
    new-instance v0, LX/0Hv;

    const-string v1, "FBNS_CONNECTION_KEY"

    const/16 v2, 0x14

    const-string v3, "fbnsck"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->FBNS_CONNECTION_KEY:LX/0Hv;

    .line 38246
    new-instance v0, LX/0Hv;

    const-string v1, "FBNS_CONNECTION_SECRET"

    const/16 v2, 0x15

    const-string v3, "fbnscs"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->FBNS_CONNECTION_SECRET:LX/0Hv;

    .line 38247
    new-instance v0, LX/0Hv;

    const-string v1, "FBNS_DEVICE_ID"

    const/16 v2, 0x16

    const-string v3, "fbnsdi"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->FBNS_DEVICE_ID:LX/0Hv;

    .line 38248
    new-instance v0, LX/0Hv;

    const-string v1, "FBNS_DEVICE_SECRET"

    const/16 v2, 0x17

    const-string v3, "fbnsds"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->FBNS_DEVICE_SECRET:LX/0Hv;

    .line 38249
    new-instance v0, LX/0Hv;

    const-string v1, "CLIENT_STACK"

    const/16 v2, 0x18

    const-string v3, "clientStack"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->CLIENT_STACK:LX/0Hv;

    .line 38250
    new-instance v0, LX/0Hv;

    const-string v1, "APP_SPECIFIC_INFO"

    const/16 v2, 0x19

    const-string v3, "app_specific_info"

    invoke-direct {v0, v1, v2, v3}, LX/0Hv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0Hv;->APP_SPECIFIC_INFO:LX/0Hv;

    .line 38251
    const/16 v0, 0x1a

    new-array v0, v0, [LX/0Hv;

    sget-object v1, LX/0Hv;->USER_ID:LX/0Hv;

    aput-object v1, v0, v4

    sget-object v1, LX/0Hv;->SESSION_ID:LX/0Hv;

    aput-object v1, v0, v5

    sget-object v1, LX/0Hv;->AGENT:LX/0Hv;

    aput-object v1, v0, v6

    sget-object v1, LX/0Hv;->CAPABILITIES:LX/0Hv;

    aput-object v1, v0, v7

    sget-object v1, LX/0Hv;->ENDPOINT_CAPABILITIES:LX/0Hv;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0Hv;->PUBLISH_FORMAT:LX/0Hv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0Hv;->NO_AUTOMATIC_FOREGROUND:LX/0Hv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0Hv;->MAKE_USER_AVAILABLE_IN_FOREGROUND:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0Hv;->INITIAL_FOREGROUND_STATE:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0Hv;->DEVICE_ID:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0Hv;->DEVICE_SECRET:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0Hv;->NETWORK_TYPE:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0Hv;->NETWORK_SUBTYPE:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0Hv;->CLIENT_MQTT_SESSION_ID:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0Hv;->SUBSCRIBE_TOPICS:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0Hv;->CLIENT_TYPE:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0Hv;->APP_ID:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0Hv;->OVERRIDE_NECTAR_LOGGING:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/0Hv;->DATACENTER_PREFERENCE:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/0Hv;->CONNECT_HASH:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/0Hv;->FBNS_CONNECTION_KEY:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/0Hv;->FBNS_CONNECTION_SECRET:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/0Hv;->FBNS_DEVICE_ID:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/0Hv;->FBNS_DEVICE_SECRET:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/0Hv;->CLIENT_STACK:LX/0Hv;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/0Hv;->APP_SPECIFIC_INFO:LX/0Hv;

    aput-object v2, v0, v1

    sput-object v0, LX/0Hv;->$VALUES:[LX/0Hv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38252
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38253
    iput-object p3, p0, LX/0Hv;->mJsonKey:Ljava/lang/String;

    .line 38254
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Hv;
    .locals 1

    .prologue
    .line 38255
    const-class v0, LX/0Hv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Hv;

    return-object v0
.end method

.method public static values()[LX/0Hv;
    .locals 1

    .prologue
    .line 38256
    sget-object v0, LX/0Hv;->$VALUES:[LX/0Hv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Hv;

    return-object v0
.end method


# virtual methods
.method public final getJsonKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38257
    iget-object v0, p0, LX/0Hv;->mJsonKey:Ljava/lang/String;

    return-object v0
.end method
