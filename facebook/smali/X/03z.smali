.class public final enum LX/03z;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/03z;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/03z;

.field public static final enum AMBIX_4:LX/03z;

.field public static final enum STEREO:LX/03z;

.field public static final enum TBE_4:LX/03z;

.field public static final enum TBE_4_2:LX/03z;

.field public static final enum TBE_6:LX/03z;

.field public static final enum TBE_6_2:LX/03z;

.field public static final enum TBE_8:LX/03z;

.field public static final enum TBE_8_2:LX/03z;

.field public static final enum UNKNOWN:LX/03z;


# instance fields
.field public final channelConfiguration:Ljava/lang/String;

.field public final isSpatial:Z

.field public final key:Ljava/lang/String;

.field public final priority:I


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v2, 0x0

    const/4 v10, 0x1

    .line 12020
    new-instance v0, LX/03z;

    const-string v1, "UNKNOWN"

    const-string v3, "unknown"

    const/4 v4, -0x1

    const-string v5, "unknown"

    move v6, v2

    invoke-direct/range {v0 .. v6}, LX/03z;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, LX/03z;->UNKNOWN:LX/03z;

    .line 12021
    new-instance v3, LX/03z;

    const-string v4, "STEREO"

    const-string v6, "stereo"

    const-string v8, "2"

    move v5, v10

    move v7, v2

    move v9, v2

    invoke-direct/range {v3 .. v9}, LX/03z;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Z)V

    sput-object v3, LX/03z;->STEREO:LX/03z;

    .line 12022
    new-instance v3, LX/03z;

    const-string v4, "AMBIX_4"

    const-string v6, "ambiX_4"

    const-string v8, "ambiX_4"

    move v5, v11

    move v7, v10

    move v9, v10

    invoke-direct/range {v3 .. v9}, LX/03z;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Z)V

    sput-object v3, LX/03z;->AMBIX_4:LX/03z;

    .line 12023
    new-instance v3, LX/03z;

    const-string v4, "TBE_4"

    const-string v6, "tbe_4"

    const-string v8, "tbe_4"

    move v5, v12

    move v7, v11

    move v9, v10

    invoke-direct/range {v3 .. v9}, LX/03z;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Z)V

    sput-object v3, LX/03z;->TBE_4:LX/03z;

    .line 12024
    new-instance v3, LX/03z;

    const-string v4, "TBE_4_2"

    const-string v6, "tbe_4.2"

    const-string v8, "tbe_4.2"

    move v5, v13

    move v7, v12

    move v9, v10

    invoke-direct/range {v3 .. v9}, LX/03z;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Z)V

    sput-object v3, LX/03z;->TBE_4_2:LX/03z;

    .line 12025
    new-instance v3, LX/03z;

    const-string v4, "TBE_6"

    const/4 v5, 0x5

    const-string v6, "tbe_6"

    const-string v8, "tbe_6"

    move v7, v13

    move v9, v10

    invoke-direct/range {v3 .. v9}, LX/03z;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Z)V

    sput-object v3, LX/03z;->TBE_6:LX/03z;

    .line 12026
    new-instance v3, LX/03z;

    const-string v4, "TBE_6_2"

    const/4 v5, 0x6

    const-string v6, "tbe_6.2"

    const/4 v7, 0x5

    const-string v8, "tbe_6.2"

    move v9, v10

    invoke-direct/range {v3 .. v9}, LX/03z;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Z)V

    sput-object v3, LX/03z;->TBE_6_2:LX/03z;

    .line 12027
    new-instance v3, LX/03z;

    const-string v4, "TBE_8"

    const/4 v5, 0x7

    const-string v6, "tbe_8"

    const/4 v7, 0x6

    const-string v8, "tbe_8"

    move v9, v10

    invoke-direct/range {v3 .. v9}, LX/03z;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Z)V

    sput-object v3, LX/03z;->TBE_8:LX/03z;

    .line 12028
    new-instance v3, LX/03z;

    const-string v4, "TBE_8_2"

    const/16 v5, 0x8

    const-string v6, "tbe_8.2"

    const/4 v7, 0x7

    const-string v8, "tbe_8.2"

    move v9, v10

    invoke-direct/range {v3 .. v9}, LX/03z;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Z)V

    sput-object v3, LX/03z;->TBE_8_2:LX/03z;

    .line 12029
    const/16 v0, 0x9

    new-array v0, v0, [LX/03z;

    sget-object v1, LX/03z;->UNKNOWN:LX/03z;

    aput-object v1, v0, v2

    sget-object v1, LX/03z;->STEREO:LX/03z;

    aput-object v1, v0, v10

    sget-object v1, LX/03z;->AMBIX_4:LX/03z;

    aput-object v1, v0, v11

    sget-object v1, LX/03z;->TBE_4:LX/03z;

    aput-object v1, v0, v12

    sget-object v1, LX/03z;->TBE_4_2:LX/03z;

    aput-object v1, v0, v13

    const/4 v1, 0x5

    sget-object v2, LX/03z;->TBE_6:LX/03z;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/03z;->TBE_6_2:LX/03z;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/03z;->TBE_8:LX/03z;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/03z;->TBE_8_2:LX/03z;

    aput-object v2, v0, v1

    sput-object v0, LX/03z;->$VALUES:[LX/03z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 12014
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12015
    iput-object p3, p0, LX/03z;->key:Ljava/lang/String;

    .line 12016
    iput p4, p0, LX/03z;->priority:I

    .line 12017
    iput-object p5, p0, LX/03z;->channelConfiguration:Ljava/lang/String;

    .line 12018
    iput-boolean p6, p0, LX/03z;->isSpatial:Z

    .line 12019
    return-void
.end method

.method public static fromChannelConfiguration(Ljava/lang/String;)LX/03z;
    .locals 5
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 12007
    if-nez p0, :cond_1

    .line 12008
    sget-object v0, LX/03z;->UNKNOWN:LX/03z;

    .line 12009
    :cond_0
    :goto_0
    return-object v0

    .line 12010
    :cond_1
    invoke-static {}, LX/03z;->values()[LX/03z;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 12011
    iget-object v4, v0, LX/03z;->channelConfiguration:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 12012
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 12013
    :cond_2
    sget-object v0, LX/03z;->UNKNOWN:LX/03z;

    goto :goto_0
.end method

.method public static fromString(Ljava/lang/String;)LX/03z;
    .locals 5
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 12000
    if-nez p0, :cond_1

    .line 12001
    sget-object v0, LX/03z;->UNKNOWN:LX/03z;

    .line 12002
    :cond_0
    :goto_0
    return-object v0

    .line 12003
    :cond_1
    invoke-static {}, LX/03z;->values()[LX/03z;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 12004
    iget-object v4, v0, LX/03z;->key:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 12005
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 12006
    :cond_2
    sget-object v0, LX/03z;->UNKNOWN:LX/03z;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/03z;
    .locals 1

    .prologue
    .line 11999
    const-class v0, LX/03z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/03z;

    return-object v0
.end method

.method public static values()[LX/03z;
    .locals 1

    .prologue
    .line 11997
    sget-object v0, LX/03z;->$VALUES:[LX/03z;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/03z;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11998
    iget-object v0, p0, LX/03z;->key:Ljava/lang/String;

    return-object v0
.end method
