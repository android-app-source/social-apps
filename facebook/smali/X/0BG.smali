.class public final enum LX/0BG;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/06x;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0BG;",
        ">;",
        "LX/06x;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0BG;

.field public static final enum BackgroundPing:LX/0BG;

.field public static final enum FbnsLiteNotificationDeliveryRetried:LX/0BG;

.field public static final enum FbnsLiteNotificationReceived:LX/0BG;

.field public static final enum FbnsNotificationDeliveryRetried:LX/0BG;

.field public static final enum FbnsNotificationReceived:LX/0BG;

.field public static final enum ForegroundPing:LX/0BG;

.field public static final enum MessageSendAttempt:LX/0BG;

.field public static final enum MessageSendSuccess:LX/0BG;

.field public static final enum MqttDurationMs:LX/0BG;

.field public static final enum MqttTotalDurationMs:LX/0BG;

.field public static final enum NetworkDurationMs:LX/0BG;

.field public static final enum NetworkTotalDurationMs:LX/0BG;

.field public static final enum PublishReceived:LX/0BG;

.field public static final enum ServiceDurationMs:LX/0BG;


# instance fields
.field private final mJsonKey:Ljava/lang/String;

.field private final mType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 26255
    new-instance v0, LX/0BG;

    const-string v1, "MqttDurationMs"

    const-string v2, "m"

    const-class v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v5, v2, v3}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->MqttDurationMs:LX/0BG;

    .line 26256
    new-instance v0, LX/0BG;

    const-string v1, "MqttTotalDurationMs"

    const-string v2, "mt"

    const-class v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v6, v2, v3}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->MqttTotalDurationMs:LX/0BG;

    .line 26257
    new-instance v0, LX/0BG;

    const-string v1, "NetworkDurationMs"

    const-string v2, "n"

    const-class v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v7, v2, v3}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->NetworkDurationMs:LX/0BG;

    .line 26258
    new-instance v0, LX/0BG;

    const-string v1, "NetworkTotalDurationMs"

    const-string v2, "nt"

    const-class v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v8, v2, v3}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->NetworkTotalDurationMs:LX/0BG;

    .line 26259
    new-instance v0, LX/0BG;

    const-string v1, "ServiceDurationMs"

    const-string v2, "s"

    const-class v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v9, v2, v3}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->ServiceDurationMs:LX/0BG;

    .line 26260
    new-instance v0, LX/0BG;

    const-string v1, "MessageSendAttempt"

    const/4 v2, 0x5

    const-string v3, "sa"

    const-class v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2, v3, v4}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->MessageSendAttempt:LX/0BG;

    .line 26261
    new-instance v0, LX/0BG;

    const-string v1, "MessageSendSuccess"

    const/4 v2, 0x6

    const-string v3, "ss"

    const-class v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2, v3, v4}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->MessageSendSuccess:LX/0BG;

    .line 26262
    new-instance v0, LX/0BG;

    const-string v1, "ForegroundPing"

    const/4 v2, 0x7

    const-string v3, "fp"

    const-class v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2, v3, v4}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->ForegroundPing:LX/0BG;

    .line 26263
    new-instance v0, LX/0BG;

    const-string v1, "BackgroundPing"

    const/16 v2, 0x8

    const-string v3, "bp"

    const-class v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2, v3, v4}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->BackgroundPing:LX/0BG;

    .line 26264
    new-instance v0, LX/0BG;

    const-string v1, "PublishReceived"

    const/16 v2, 0x9

    const-string v3, "pr"

    const-class v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2, v3, v4}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->PublishReceived:LX/0BG;

    .line 26265
    new-instance v0, LX/0BG;

    const-string v1, "FbnsNotificationReceived"

    const/16 v2, 0xa

    const-string v3, "fnr"

    const-class v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2, v3, v4}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->FbnsNotificationReceived:LX/0BG;

    .line 26266
    new-instance v0, LX/0BG;

    const-string v1, "FbnsLiteNotificationReceived"

    const/16 v2, 0xb

    const-string v3, "flnr"

    const-class v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2, v3, v4}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->FbnsLiteNotificationReceived:LX/0BG;

    .line 26267
    new-instance v0, LX/0BG;

    const-string v1, "FbnsNotificationDeliveryRetried"

    const/16 v2, 0xc

    const-string v3, "fdr"

    const-class v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2, v3, v4}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->FbnsNotificationDeliveryRetried:LX/0BG;

    .line 26268
    new-instance v0, LX/0BG;

    const-string v1, "FbnsLiteNotificationDeliveryRetried"

    const/16 v2, 0xd

    const-string v3, "fldr"

    const-class v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2, v3, v4}, LX/0BG;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V

    sput-object v0, LX/0BG;->FbnsLiteNotificationDeliveryRetried:LX/0BG;

    .line 26269
    const/16 v0, 0xe

    new-array v0, v0, [LX/0BG;

    sget-object v1, LX/0BG;->MqttDurationMs:LX/0BG;

    aput-object v1, v0, v5

    sget-object v1, LX/0BG;->MqttTotalDurationMs:LX/0BG;

    aput-object v1, v0, v6

    sget-object v1, LX/0BG;->NetworkDurationMs:LX/0BG;

    aput-object v1, v0, v7

    sget-object v1, LX/0BG;->NetworkTotalDurationMs:LX/0BG;

    aput-object v1, v0, v8

    sget-object v1, LX/0BG;->ServiceDurationMs:LX/0BG;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/0BG;->MessageSendAttempt:LX/0BG;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0BG;->MessageSendSuccess:LX/0BG;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0BG;->ForegroundPing:LX/0BG;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0BG;->BackgroundPing:LX/0BG;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0BG;->PublishReceived:LX/0BG;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0BG;->FbnsNotificationReceived:LX/0BG;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0BG;->FbnsLiteNotificationReceived:LX/0BG;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0BG;->FbnsNotificationDeliveryRetried:LX/0BG;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0BG;->FbnsLiteNotificationDeliveryRetried:LX/0BG;

    aput-object v2, v0, v1

    sput-object v0, LX/0BG;->$VALUES:[LX/0BG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 26270
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26271
    iput-object p3, p0, LX/0BG;->mJsonKey:Ljava/lang/String;

    .line 26272
    iput-object p4, p0, LX/0BG;->mType:Ljava/lang/Class;

    .line 26273
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0BG;
    .locals 1

    .prologue
    .line 26274
    const-class v0, LX/0BG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0BG;

    return-object v0
.end method

.method public static values()[LX/0BG;
    .locals 1

    .prologue
    .line 26275
    sget-object v0, LX/0BG;->$VALUES:[LX/0BG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0BG;

    return-object v0
.end method


# virtual methods
.method public final getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26276
    iget-object v0, p0, LX/0BG;->mJsonKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getValueType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 26277
    iget-object v0, p0, LX/0BG;->mType:Ljava/lang/Class;

    return-object v0
.end method
