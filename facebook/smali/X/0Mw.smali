.class public final LX/0Mw;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48379
    const-string v0, "OggS"

    invoke-static {v0}, LX/08x;->e(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/0Mw;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48378
    return-void
.end method

.method public static a(LX/0MA;)V
    .locals 9

    .prologue
    const/16 v0, 0x800

    const/16 v8, 0x67

    const/4 v2, 0x0

    .line 48329
    new-array v3, v0, [B

    .line 48330
    :goto_0
    invoke-interface {p0}, LX/0MA;->d()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    invoke-interface {p0}, LX/0MA;->c()J

    move-result-wide v4

    int-to-long v6, v0

    add-long/2addr v4, v6

    invoke-interface {p0}, LX/0MA;->d()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 48331
    invoke-interface {p0}, LX/0MA;->d()J

    move-result-wide v0

    invoke-interface {p0}, LX/0MA;->c()J

    move-result-wide v4

    sub-long/2addr v0, v4

    long-to-int v0, v0

    .line 48332
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 48333
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 48334
    :cond_0
    invoke-interface {p0, v3, v2, v0, v2}, LX/0MA;->b([BIIZ)Z

    move v1, v2

    .line 48335
    :goto_1
    add-int/lit8 v4, v0, -0x3

    if-ge v1, v4, :cond_2

    .line 48336
    aget-byte v4, v3, v1

    const/16 v5, 0x4f

    if-ne v4, v5, :cond_1

    add-int/lit8 v4, v1, 0x1

    aget-byte v4, v3, v4

    if-ne v4, v8, :cond_1

    add-int/lit8 v4, v1, 0x2

    aget-byte v4, v3, v4

    if-ne v4, v8, :cond_1

    add-int/lit8 v4, v1, 0x3

    aget-byte v4, v3, v4

    const/16 v5, 0x53

    if-ne v4, v5, :cond_1

    .line 48337
    invoke-interface {p0, v1}, LX/0MA;->b(I)V

    .line 48338
    return-void

    .line 48339
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 48340
    :cond_2
    add-int/lit8 v1, v0, -0x3

    invoke-interface {p0, v1}, LX/0MA;->b(I)V

    goto :goto_0
.end method

.method public static a(LX/0Mv;ILX/0Mu;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 48370
    iput v0, p2, LX/0Mu;->b:I

    .line 48371
    iput v0, p2, LX/0Mu;->a:I

    .line 48372
    :cond_0
    iget v0, p2, LX/0Mu;->b:I

    add-int/2addr v0, p1

    iget v1, p0, LX/0Mv;->g:I

    if-ge v0, v1, :cond_1

    .line 48373
    iget-object v0, p0, LX/0Mv;->j:[I

    iget v1, p2, LX/0Mu;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p2, LX/0Mu;->b:I

    add-int/2addr v1, p1

    aget v0, v0, v1

    .line 48374
    iget v1, p2, LX/0Mu;->a:I

    add-int/2addr v1, v0

    iput v1, p2, LX/0Mu;->a:I

    .line 48375
    const/16 v1, 0xff

    if-eq v0, v1, :cond_0

    .line 48376
    :cond_1
    return-void
.end method

.method public static a(LX/0MA;LX/0Mv;LX/0Oj;Z)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 48341
    invoke-virtual {p2}, LX/0Oj;->a()V

    .line 48342
    invoke-virtual {p1}, LX/0Mv;->a()V

    .line 48343
    invoke-interface {p0}, LX/0MA;->d()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    invoke-interface {p0}, LX/0MA;->d()J

    move-result-wide v2

    invoke-interface {p0}, LX/0MA;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1b

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    :cond_0
    move v2, v1

    .line 48344
    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p2, LX/0Oj;->a:[B

    const/16 v3, 0x1b

    invoke-interface {p0, v2, v0, v3, v1}, LX/0MA;->b([BIIZ)Z

    move-result v2

    if-nez v2, :cond_5

    .line 48345
    :cond_1
    if-eqz p3, :cond_4

    .line 48346
    :cond_2
    :goto_1
    return v0

    :cond_3
    move v2, v0

    .line 48347
    goto :goto_0

    .line 48348
    :cond_4
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 48349
    :cond_5
    invoke-virtual {p2}, LX/0Oj;->k()J

    move-result-wide v2

    sget v4, LX/0Mw;->a:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    .line 48350
    if-nez p3, :cond_2

    .line 48351
    new-instance v0, LX/0L6;

    const-string v1, "expected OggS capture pattern at begin of page"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48352
    :cond_6
    invoke-virtual {p2}, LX/0Oj;->f()I

    move-result v2

    iput v2, p1, LX/0Mv;->a:I

    .line 48353
    iget v2, p1, LX/0Mv;->a:I

    if-eqz v2, :cond_7

    .line 48354
    if-nez p3, :cond_2

    .line 48355
    new-instance v0, LX/0L6;

    const-string v1, "unsupported bit stream revision"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48356
    :cond_7
    invoke-virtual {p2}, LX/0Oj;->f()I

    move-result v2

    iput v2, p1, LX/0Mv;->b:I

    .line 48357
    invoke-virtual {p2}, LX/0Oj;->p()J

    move-result-wide v2

    iput-wide v2, p1, LX/0Mv;->c:J

    .line 48358
    invoke-virtual {p2}, LX/0Oj;->l()J

    move-result-wide v2

    iput-wide v2, p1, LX/0Mv;->d:J

    .line 48359
    invoke-virtual {p2}, LX/0Oj;->l()J

    move-result-wide v2

    iput-wide v2, p1, LX/0Mv;->e:J

    .line 48360
    invoke-virtual {p2}, LX/0Oj;->l()J

    move-result-wide v2

    iput-wide v2, p1, LX/0Mv;->f:J

    .line 48361
    invoke-virtual {p2}, LX/0Oj;->f()I

    move-result v2

    iput v2, p1, LX/0Mv;->g:I

    .line 48362
    invoke-virtual {p2}, LX/0Oj;->a()V

    .line 48363
    iget v2, p1, LX/0Mv;->g:I

    add-int/lit8 v2, v2, 0x1b

    iput v2, p1, LX/0Mv;->h:I

    .line 48364
    iget-object v2, p2, LX/0Oj;->a:[B

    iget v3, p1, LX/0Mv;->g:I

    invoke-interface {p0, v2, v0, v3}, LX/0MA;->c([BII)V

    .line 48365
    :goto_2
    iget v2, p1, LX/0Mv;->g:I

    if-ge v0, v2, :cond_8

    .line 48366
    iget-object v2, p1, LX/0Mv;->j:[I

    invoke-virtual {p2}, LX/0Oj;->f()I

    move-result v3

    aput v3, v2, v0

    .line 48367
    iget v2, p1, LX/0Mv;->i:I

    iget-object v3, p1, LX/0Mv;->j:[I

    aget v3, v3, v0

    add-int/2addr v2, v3

    iput v2, p1, LX/0Mv;->i:I

    .line 48368
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    move v0, v1

    .line 48369
    goto :goto_1
.end method
