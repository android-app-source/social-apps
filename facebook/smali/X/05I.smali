.class public abstract LX/05I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/04v;

.field public final c:LX/01o;

.field public final d:Ljava/lang/String;

.field public final e:LX/05J;

.field public f:LX/0BE;

.field public g:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/04v;LX/01o;Ljava/lang/String;LX/05J;)V
    .locals 0

    .prologue
    .line 15943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15944
    iput-object p1, p0, LX/05I;->a:Landroid/content/Context;

    .line 15945
    iput-object p2, p0, LX/05I;->b:LX/04v;

    .line 15946
    iput-object p3, p0, LX/05I;->c:LX/01o;

    .line 15947
    iput-object p4, p0, LX/05I;->d:Ljava/lang/String;

    .line 15948
    iput-object p5, p0, LX/05I;->e:LX/05J;

    .line 15949
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 15950
    invoke-static {p2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15951
    :cond_0
    :goto_0
    return-void

    .line 15952
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.rti.intent.ACTION_NOTIFICATION_ACK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 15953
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 15954
    const-string v1, "extra_notification_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 15955
    new-instance v1, LX/04v;

    invoke-direct {v1, p0}, LX/04v;-><init>(Landroid/content/Context;)V

    .line 15956
    invoke-virtual {v1, v0, p1}, LX/04v;->a(Landroid/content/Intent;Ljava/lang/String;)Z

    .line 15957
    const-string v0, "NotificationDeliveryHelper"

    const-string v1, "ackNotification %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 15939
    iget-object v0, p0, LX/05I;->g:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 15940
    new-instance v0, LX/06g;

    invoke-direct {v0, p0}, LX/06g;-><init>(LX/05I;)V

    iput-object v0, p0, LX/05I;->g:Landroid/content/BroadcastReceiver;

    .line 15941
    iget-object v0, p0, LX/05I;->a:Landroid/content/Context;

    iget-object v1, p0, LX/05I;->g:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.facebook.rti.intent.ACTION_NOTIFICATION_ACK"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 15942
    :cond_0
    return-void
.end method

.method public abstract a(Landroid/content/Intent;)Z
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 15933
    iget-object v0, p0, LX/05I;->g:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 15934
    :try_start_0
    iget-object v0, p0, LX/05I;->a:Landroid/content/Context;

    iget-object v1, p0, LX/05I;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 15935
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/05I;->g:Landroid/content/BroadcastReceiver;

    .line 15936
    :cond_0
    return-void

    .line 15937
    :catch_0
    move-exception v0

    .line 15938
    const-string v1, "NotificationDeliveryHelper"

    const-string v2, "Failed to unregister broadcast receiver"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 15926
    invoke-virtual {p0}, LX/05I;->e()LX/0BE;

    move-result-object v0

    invoke-interface {v0}, LX/0BE;->b()Ljava/util/List;

    move-result-object v0

    .line 15927
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Hn;

    .line 15928
    const-string v4, "NotificationDeliveryHelper"

    const-string v5, "redeliverAllNotifications send %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v0, LX/0Hn;->d:Ljava/lang/String;

    aput-object v7, v6, v2

    invoke-static {v4, v5, v6}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15929
    iget-object v0, v0, LX/0Hn;->c:Landroid/content/Intent;

    invoke-virtual {p0, v0}, LX/05I;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15930
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 15931
    goto :goto_0

    .line 15932
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final e()LX/0BE;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 15917
    iget-object v0, p0, LX/05I;->f:LX/0BE;

    if-nez v0, :cond_0

    .line 15918
    iget-object v0, p0, LX/05I;->a:Landroid/content/Context;

    sget-object v1, LX/01p;->c:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 15919
    const-string v1, "notification_store_class"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 15920
    const-string v1, "NotificationDeliveryHelper"

    const-string v2, "getNotificationDeliveryStore %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15921
    packed-switch v0, :pswitch_data_0

    .line 15922
    new-instance v0, LX/0BD;

    invoke-direct {v0}, LX/0BD;-><init>()V

    iput-object v0, p0, LX/05I;->f:LX/0BE;

    .line 15923
    :cond_0
    :goto_0
    iget-object v0, p0, LX/05I;->f:LX/0BE;

    return-object v0

    .line 15924
    :pswitch_0
    new-instance v0, LX/0Hp;

    iget-object v1, p0, LX/05I;->c:LX/01o;

    invoke-direct {v0, v1}, LX/0Hp;-><init>(LX/01o;)V

    iput-object v0, p0, LX/05I;->f:LX/0BE;

    goto :goto_0

    .line 15925
    :pswitch_1
    new-instance v0, LX/0Hr;

    iget-object v1, p0, LX/05I;->a:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/05I;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x5f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/05I;->e:LX/05J;

    invoke-virtual {v3}, LX/05J;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/05I;->c:LX/01o;

    invoke-direct {v0, v1, v2, v3}, LX/0Hr;-><init>(Landroid/content/Context;Ljava/lang/String;LX/01o;)V

    iput-object v0, p0, LX/05I;->f:LX/0BE;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
