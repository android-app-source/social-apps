.class public final LX/0Ex;
.super Ljava/util/HashSet;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashSet",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32682
    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    .line 32683
    const-string v0, "street-address"

    invoke-virtual {p0, v0}, LX/0Ex;->add(Ljava/lang/Object;)Z

    .line 32684
    const-string v0, "address-line1"

    invoke-virtual {p0, v0}, LX/0Ex;->add(Ljava/lang/Object;)Z

    .line 32685
    const-string v0, "address-line2"

    invoke-virtual {p0, v0}, LX/0Ex;->add(Ljava/lang/Object;)Z

    .line 32686
    const-string v0, "address-line3"

    invoke-virtual {p0, v0}, LX/0Ex;->add(Ljava/lang/Object;)Z

    .line 32687
    const-string v0, "address-level1"

    invoke-virtual {p0, v0}, LX/0Ex;->add(Ljava/lang/Object;)Z

    .line 32688
    const-string v0, "address-level2"

    invoke-virtual {p0, v0}, LX/0Ex;->add(Ljava/lang/Object;)Z

    .line 32689
    const-string v0, "address-level3"

    invoke-virtual {p0, v0}, LX/0Ex;->add(Ljava/lang/Object;)Z

    .line 32690
    const-string v0, "address-level4"

    invoke-virtual {p0, v0}, LX/0Ex;->add(Ljava/lang/Object;)Z

    .line 32691
    const-string v0, "country"

    invoke-virtual {p0, v0}, LX/0Ex;->add(Ljava/lang/Object;)Z

    .line 32692
    const-string v0, "country-name"

    invoke-virtual {p0, v0}, LX/0Ex;->add(Ljava/lang/Object;)Z

    .line 32693
    const-string v0, "postal-code"

    invoke-virtual {p0, v0}, LX/0Ex;->add(Ljava/lang/Object;)Z

    .line 32694
    return-void
.end method
