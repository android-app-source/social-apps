.class public LX/09U;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/pm/PackageManager;

.field private final d:Landroid/content/ComponentName;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22476
    const-class v0, LX/09U;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/09U;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 22477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22478
    iput-object p1, p0, LX/09U;->b:Landroid/content/Context;

    .line 22479
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, LX/09U;->c:Landroid/content/pm/PackageManager;

    .line 22480
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, LX/09U;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/nobreak/CrashLoop$LastState;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, LX/09U;->d:Landroid/content/ComponentName;

    .line 22481
    return-void
.end method

.method public static a(LX/09U;[Landroid/content/pm/ComponentInfo;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/content/pm/ComponentInfo;",
            ">([TT;)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ComponentInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 22482
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 22483
    if-nez p1, :cond_0

    move-object v0, v2

    .line 22484
    :goto_0
    return-object v0

    .line 22485
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/09U;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":nodex"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 22486
    array-length v5, p1

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_3

    aget-object v6, p1, v3

    .line 22487
    iget-object v0, v6, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    .line 22488
    iget-object v0, v6, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    const-string v7, "crash.loop.exclude"

    invoke-virtual {v0, v7, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 22489
    :goto_2
    iget-object v7, v6, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 22490
    const/4 v0, 0x1

    .line 22491
    :cond_1
    if-nez v0, :cond_2

    .line 22492
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22493
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 22494
    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private static a(LX/09U;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 22495
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    const-string v0, "disabled"

    move-object v1, v0

    .line 22496
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 22497
    const/16 v2, 0x282

    invoke-static {p0, v2}, LX/09U;->b(LX/09U;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 22498
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    invoke-static {p0, v2}, LX/09U;->a(LX/09U;[Landroid/content/pm/ComponentInfo;)Ljava/util/List;

    move-result-object v2

    move-object v2, v2

    .line 22499
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 22500
    const/16 v2, 0x281

    invoke-static {p0, v2}, LX/09U;->b(LX/09U;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 22501
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    invoke-static {p0, v2}, LX/09U;->a(LX/09U;[Landroid/content/pm/ComponentInfo;)Ljava/util/List;

    move-result-object v2

    move-object v2, v2

    .line 22502
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 22503
    const/16 v2, 0x288

    invoke-static {p0, v2}, LX/09U;->b(LX/09U;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 22504
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->providers:[Landroid/content/pm/ProviderInfo;

    invoke-static {p0, v2}, LX/09U;->a(LX/09U;[Landroid/content/pm/ComponentInfo;)Ljava/util/List;

    move-result-object v2

    move-object v2, v2

    .line 22505
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 22506
    const/16 v2, 0x284

    invoke-static {p0, v2}, LX/09U;->b(LX/09U;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 22507
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    invoke-static {p0, v2}, LX/09U;->a(LX/09U;[Landroid/content/pm/ComponentInfo;)Ljava/util/List;

    move-result-object v2

    move-object v2, v2

    .line 22508
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 22509
    new-instance v2, LX/09V;

    invoke-direct {v2}, LX/09V;-><init>()V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 22510
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 22511
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ComponentInfo;

    .line 22512
    new-instance v4, Landroid/content/ComponentName;

    iget-object v6, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v6, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 22513
    :cond_0
    move-object v0, v2

    .line 22514
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 22515
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Changing state for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22516
    iget-object v3, p0, LX/09U;->c:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v0, p1, v5}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto :goto_2

    .line 22517
    :cond_1
    const-string v0, "enabled"

    move-object v1, v0

    goto/16 :goto_0

    .line 22518
    :cond_2
    iget-object v0, p0, LX/09U;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, LX/09U;->d:Landroid/content/ComponentName;

    invoke-virtual {v0, v1, p1, v5}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 22519
    return-void
.end method

.method public static b(LX/09U;I)Landroid/content/pm/PackageInfo;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "GetPackageInfoFlag"
        }
    .end annotation

    .prologue
    .line 22520
    iget-object v0, p0, LX/09U;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, LX/09U;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 22521
    const/4 v0, 0x2

    :try_start_0
    invoke-static {p0, v0}, LX/09U;->a(LX/09U;I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22522
    return-void

    .line 22523
    :catch_0
    move-exception v0

    .line 22524
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 22525
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, v0}, LX/09U;->a(LX/09U;I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22526
    return-void

    .line 22527
    :catch_0
    move-exception v0

    .line 22528
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
