.class public abstract LX/0BV;
.super LX/0Ag;
.source ""


# instance fields
.field public final d:I

.field public final e:J

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0BT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Au;JJIJLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Au;",
            "JJIJ",
            "Ljava/util/List",
            "<",
            "LX/0BT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26640
    invoke-direct/range {p0 .. p5}, LX/0Ag;-><init>(LX/0Au;JJ)V

    .line 26641
    iput p6, p0, LX/0BV;->d:I

    .line 26642
    iput-wide p7, p0, LX/0BV;->e:J

    .line 26643
    iput-object p9, p0, LX/0BV;->f:Ljava/util/List;

    .line 26644
    return-void
.end method


# virtual methods
.method public abstract a(J)I
.end method

.method public final a(I)J
    .locals 6

    .prologue
    .line 26645
    iget-object v0, p0, LX/0BV;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 26646
    iget-object v0, p0, LX/0BV;->f:Ljava/util/List;

    iget v1, p0, LX/0BV;->d:I

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0BT;

    iget-wide v0, v0, LX/0BT;->a:J

    iget-wide v2, p0, LX/0Ag;->c:J

    sub-long/2addr v0, v2

    .line 26647
    :goto_0
    const-wide/32 v2, 0xf4240

    iget-wide v4, p0, LX/0Ag;->b:J

    invoke-static/range {v0 .. v5}, LX/08x;->a(JJJ)J

    move-result-wide v0

    return-wide v0

    .line 26648
    :cond_0
    iget v0, p0, LX/0BV;->d:I

    sub-int v0, p1, v0

    int-to-long v0, v0

    iget-wide v2, p0, LX/0BV;->e:J

    mul-long/2addr v0, v2

    goto :goto_0
.end method

.method public abstract a(LX/0Ah;I)LX/0Au;
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26649
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 26650
    iget-object v0, p0, LX/0BV;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26651
    const/4 v0, 0x0

    return-object v0
.end method
