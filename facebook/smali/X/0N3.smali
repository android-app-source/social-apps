.class public final LX/0N3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48585
    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 48579
    const/4 v0, 0x0

    .line 48580
    :goto_0
    if-lez p0, :cond_0

    .line 48581
    add-int/lit8 v0, v0, 0x1

    .line 48582
    ushr-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 48583
    :cond_0
    return v0
.end method

.method private static a(JJ)J
    .locals 6

    .prologue
    .line 48789
    long-to-double v0, p0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    long-to-double v4, p2

    div-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    return-wide v0
.end method

.method public static a(LX/0Oj;)LX/0N2;
    .locals 14

    .prologue
    .line 48773
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, LX/0N3;->a(ILX/0Oj;Z)Z

    .line 48774
    invoke-virtual {p0}, LX/0Oj;->l()J

    move-result-wide v1

    .line 48775
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v3

    .line 48776
    invoke-virtual {p0}, LX/0Oj;->l()J

    move-result-wide v4

    .line 48777
    invoke-virtual {p0}, LX/0Oj;->n()I

    move-result v6

    .line 48778
    invoke-virtual {p0}, LX/0Oj;->n()I

    move-result v7

    .line 48779
    invoke-virtual {p0}, LX/0Oj;->n()I

    move-result v8

    .line 48780
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    .line 48781
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    and-int/lit8 v9, v0, 0xf

    int-to-double v12, v9

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    double-to-int v9, v10

    .line 48782
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    and-int/lit16 v0, v0, 0xf0

    shr-int/lit8 v0, v0, 0x4

    int-to-double v12, v0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    double-to-int v10, v10

    .line 48783
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_0

    const/4 v11, 0x1

    .line 48784
    :goto_0
    iget-object v0, p0, LX/0Oj;->a:[B

    .line 48785
    iget v12, p0, LX/0Oj;->c:I

    move v12, v12

    .line 48786
    invoke-static {v0, v12}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v12

    .line 48787
    new-instance v0, LX/0N2;

    invoke-direct/range {v0 .. v12}, LX/0N2;-><init>(JIJIIIIIZ[B)V

    return-object v0

    .line 48788
    :cond_0
    const/4 v11, 0x0

    goto :goto_0
.end method

.method private static a(ILX/0My;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v1, 0x1

    const/16 v8, 0x8

    const/4 v3, 0x0

    .line 48746
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, LX/0My;->a(I)I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    move v4, v3

    .line 48747
    :goto_0
    if-ge v4, v5, :cond_5

    .line 48748
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, LX/0My;->a(I)I

    move-result v0

    .line 48749
    packed-switch v0, :pswitch_data_0

    .line 48750
    const-string v2, "VorbisUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "mapping type other than 0 not supported: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48751
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 48752
    :pswitch_0
    invoke-virtual {p1}, LX/0My;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48753
    invoke-virtual {p1, v9}, LX/0My;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 48754
    :goto_1
    invoke-virtual {p1}, LX/0My;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 48755
    invoke-virtual {p1, v8}, LX/0My;->a(I)I

    move-result v2

    add-int/lit8 v6, v2, 0x1

    move v2, v3

    .line 48756
    :goto_2
    if-ge v2, v6, :cond_2

    .line 48757
    add-int/lit8 v7, p0, -0x1

    invoke-static {v7}, LX/0N3;->a(I)I

    move-result v7

    invoke-virtual {p1, v7}, LX/0My;->b(I)V

    .line 48758
    add-int/lit8 v7, p0, -0x1

    invoke-static {v7}, LX/0N3;->a(I)I

    move-result v7

    invoke-virtual {p1, v7}, LX/0My;->b(I)V

    .line 48759
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v0, v1

    .line 48760
    goto :goto_1

    .line 48761
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/0My;->a(I)I

    move-result v2

    if-eqz v2, :cond_3

    .line 48762
    new-instance v0, LX/0L6;

    const-string v1, "to reserved bits must be zero after mapping coupling steps"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48763
    :cond_3
    if-le v0, v1, :cond_4

    move v2, v3

    .line 48764
    :goto_3
    if-ge v2, p0, :cond_4

    .line 48765
    invoke-virtual {p1, v9}, LX/0My;->b(I)V

    .line 48766
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    move v2, v3

    .line 48767
    :goto_4
    if-ge v2, v0, :cond_0

    .line 48768
    invoke-virtual {p1, v8}, LX/0My;->b(I)V

    .line 48769
    invoke-virtual {p1, v8}, LX/0My;->b(I)V

    .line 48770
    invoke-virtual {p1, v8}, LX/0My;->b(I)V

    .line 48771
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 48772
    :cond_5
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(ILX/0Oj;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 48738
    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v1

    if-eq v1, p0, :cond_2

    .line 48739
    if-eqz p2, :cond_1

    .line 48740
    :cond_0
    :goto_0
    return v0

    .line 48741
    :cond_1
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "expected header type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48742
    :cond_2
    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v1

    const/16 v2, 0x76

    if-ne v1, v2, :cond_3

    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v1

    const/16 v2, 0x6f

    if-ne v1, v2, :cond_3

    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v1

    const/16 v2, 0x72

    if-ne v1, v2, :cond_3

    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v1

    const/16 v2, 0x62

    if-ne v1, v2, :cond_3

    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v1

    const/16 v2, 0x69

    if-ne v1, v2, :cond_3

    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v1

    const/16 v2, 0x73

    if-eq v1, v2, :cond_4

    .line 48743
    :cond_3
    if-nez p2, :cond_0

    .line 48744
    new-instance v0, LX/0L6;

    const-string v1, "expected characters \'vorbis\'"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48745
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(LX/0Oj;I)[LX/0N1;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 48708
    const/4 v1, 0x5

    invoke-static {v1, p0, v0}, LX/0N3;->a(ILX/0Oj;Z)Z

    .line 48709
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v1

    add-int/lit8 v2, v1, 0x1

    .line 48710
    new-instance v3, LX/0My;

    iget-object v1, p0, LX/0Oj;->a:[B

    invoke-direct {v3, v1}, LX/0My;-><init>([B)V

    .line 48711
    iget v1, p0, LX/0Oj;->b:I

    move v1, v1

    .line 48712
    mul-int/lit8 v1, v1, 0x8

    invoke-virtual {v3, v1}, LX/0My;->b(I)V

    move v1, v0

    .line 48713
    :goto_0
    if-ge v1, v2, :cond_0

    .line 48714
    invoke-static {v3}, LX/0N3;->d(LX/0My;)LX/0Mz;

    .line 48715
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 48716
    :cond_0
    const/4 v1, 0x6

    invoke-virtual {v3, v1}, LX/0My;->a(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 48717
    :goto_1
    if-ge v0, v1, :cond_2

    .line 48718
    const/16 v2, 0x10

    invoke-virtual {v3, v2}, LX/0My;->a(I)I

    move-result v2

    if-eqz v2, :cond_1

    .line 48719
    new-instance v0, LX/0L6;

    const-string v1, "placeholder of time domain transforms not zeroed out"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48720
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 48721
    :cond_2
    invoke-static {v3}, LX/0N3;->c(LX/0My;)V

    .line 48722
    invoke-static {v3}, LX/0N3;->b(LX/0My;)V

    .line 48723
    invoke-static {p1, v3}, LX/0N3;->a(ILX/0My;)V

    .line 48724
    const/16 p1, 0x10

    .line 48725
    const/4 v0, 0x6

    invoke-virtual {v3, v0}, LX/0My;->a(I)I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    .line 48726
    new-array v2, v1, [LX/0N1;

    .line 48727
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_3

    .line 48728
    invoke-virtual {v3}, LX/0My;->a()Z

    move-result v4

    .line 48729
    invoke-virtual {v3, p1}, LX/0My;->a(I)I

    move-result v5

    .line 48730
    invoke-virtual {v3, p1}, LX/0My;->a(I)I

    move-result v6

    .line 48731
    const/16 v7, 0x8

    invoke-virtual {v3, v7}, LX/0My;->a(I)I

    move-result v7

    .line 48732
    new-instance p0, LX/0N1;

    invoke-direct {p0, v4, v5, v6, v7}, LX/0N1;-><init>(ZIII)V

    aput-object p0, v2, v0

    .line 48733
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 48734
    :cond_3
    move-object v0, v2

    .line 48735
    invoke-virtual {v3}, LX/0My;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 48736
    new-instance v0, LX/0L6;

    const-string v1, "framing bit after modes not set as expected"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48737
    :cond_4
    return-object v0
.end method

.method public static b(LX/0Oj;)LX/0N0;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 48691
    const/4 v1, 0x3

    invoke-static {v1, p0, v0}, LX/0N3;->a(ILX/0Oj;Z)Z

    .line 48692
    invoke-virtual {p0}, LX/0Oj;->l()J

    move-result-wide v2

    long-to-int v1, v2

    .line 48693
    invoke-virtual {p0, v1}, LX/0Oj;->d(I)Ljava/lang/String;

    move-result-object v2

    .line 48694
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xb

    .line 48695
    invoke-virtual {p0}, LX/0Oj;->l()J

    move-result-wide v4

    .line 48696
    long-to-int v3, v4

    new-array v3, v3, [Ljava/lang/String;

    .line 48697
    add-int/lit8 v1, v1, 0x4

    .line 48698
    :goto_0
    int-to-long v6, v0

    cmp-long v6, v6, v4

    if-gez v6, :cond_0

    .line 48699
    invoke-virtual {p0}, LX/0Oj;->l()J

    move-result-wide v6

    long-to-int v6, v6

    .line 48700
    add-int/lit8 v1, v1, 0x4

    .line 48701
    invoke-virtual {p0, v6}, LX/0Oj;->d(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v0

    .line 48702
    aget-object v6, v3, v0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v1, v6

    .line 48703
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48704
    :cond_0
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    .line 48705
    new-instance v0, LX/0L6;

    const-string v1, "framing bit expected to be set"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48706
    :cond_1
    add-int/lit8 v0, v1, 0x1

    .line 48707
    new-instance v1, LX/0N0;

    invoke-direct {v1, v2, v3, v0}, LX/0N0;-><init>(Ljava/lang/String;[Ljava/lang/String;I)V

    return-object v1
.end method

.method private static b(LX/0My;)V
    .locals 12

    .prologue
    const/4 v11, 0x6

    const/16 v10, 0x18

    const/16 v9, 0x8

    const/4 v1, 0x0

    .line 48666
    invoke-virtual {p0, v11}, LX/0My;->a(I)I

    move-result v0

    add-int/lit8 v4, v0, 0x1

    move v3, v1

    .line 48667
    :goto_0
    if-ge v3, v4, :cond_5

    .line 48668
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/0My;->a(I)I

    move-result v0

    .line 48669
    const/4 v2, 0x2

    if-le v0, v2, :cond_0

    .line 48670
    new-instance v0, LX/0L6;

    const-string v1, "residueType greater than 2 is not decodable"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48671
    :cond_0
    invoke-virtual {p0, v10}, LX/0My;->b(I)V

    .line 48672
    invoke-virtual {p0, v10}, LX/0My;->b(I)V

    .line 48673
    invoke-virtual {p0, v10}, LX/0My;->b(I)V

    .line 48674
    invoke-virtual {p0, v11}, LX/0My;->a(I)I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    .line 48675
    invoke-virtual {p0, v9}, LX/0My;->b(I)V

    .line 48676
    new-array v6, v5, [I

    move v2, v1

    .line 48677
    :goto_1
    if-ge v2, v5, :cond_1

    .line 48678
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LX/0My;->a(I)I

    move-result v7

    .line 48679
    invoke-virtual {p0}, LX/0My;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 48680
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, LX/0My;->a(I)I

    move-result v0

    .line 48681
    :goto_2
    mul-int/lit8 v0, v0, 0x8

    add-int/2addr v0, v7

    aput v0, v6, v2

    .line 48682
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v2, v1

    .line 48683
    :goto_3
    if-ge v2, v5, :cond_4

    move v0, v1

    .line 48684
    :goto_4
    if-ge v0, v9, :cond_3

    .line 48685
    aget v7, v6, v2

    const/4 v8, 0x1

    shl-int/2addr v8, v0

    and-int/2addr v7, v8

    if-eqz v7, :cond_2

    .line 48686
    invoke-virtual {p0, v9}, LX/0My;->b(I)V

    .line 48687
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 48688
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 48689
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 48690
    :cond_5
    return-void

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method private static c(LX/0My;)V
    .locals 15

    .prologue
    const/4 v14, 0x2

    const/16 v13, 0x10

    const/4 v12, 0x4

    const/16 v11, 0x8

    const/4 v1, 0x0

    .line 48623
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, LX/0My;->a(I)I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    move v4, v1

    .line 48624
    :goto_0
    if-ge v4, v5, :cond_7

    .line 48625
    invoke-virtual {p0, v13}, LX/0My;->a(I)I

    move-result v0

    .line 48626
    packed-switch v0, :pswitch_data_0

    .line 48627
    new-instance v1, LX/0L6;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "floor type greater than 1 not decodable: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v1

    .line 48628
    :pswitch_0
    invoke-virtual {p0, v11}, LX/0My;->b(I)V

    .line 48629
    invoke-virtual {p0, v13}, LX/0My;->b(I)V

    .line 48630
    invoke-virtual {p0, v13}, LX/0My;->b(I)V

    .line 48631
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, LX/0My;->b(I)V

    .line 48632
    invoke-virtual {p0, v11}, LX/0My;->b(I)V

    .line 48633
    invoke-virtual {p0, v12}, LX/0My;->a(I)I

    move-result v0

    add-int/lit8 v2, v0, 0x1

    move v0, v1

    .line 48634
    :goto_1
    if-ge v0, v2, :cond_6

    .line 48635
    invoke-virtual {p0, v11}, LX/0My;->b(I)V

    .line 48636
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 48637
    :pswitch_1
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, LX/0My;->a(I)I

    move-result v6

    .line 48638
    const/4 v0, -0x1

    .line 48639
    new-array v7, v6, [I

    move v2, v1

    .line 48640
    :goto_2
    if-ge v2, v6, :cond_1

    .line 48641
    invoke-virtual {p0, v12}, LX/0My;->a(I)I

    move-result v3

    aput v3, v7, v2

    .line 48642
    aget v3, v7, v2

    if-le v3, v0, :cond_0

    .line 48643
    aget v0, v7, v2

    .line 48644
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 48645
    :cond_1
    add-int/lit8 v0, v0, 0x1

    new-array v8, v0, [I

    move v0, v1

    .line 48646
    :goto_3
    array-length v2, v8

    if-ge v0, v2, :cond_4

    .line 48647
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/0My;->a(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    aput v2, v8, v0

    .line 48648
    invoke-virtual {p0, v14}, LX/0My;->a(I)I

    move-result v3

    .line 48649
    if-lez v3, :cond_2

    .line 48650
    invoke-virtual {p0, v11}, LX/0My;->b(I)V

    :cond_2
    move v2, v1

    .line 48651
    :goto_4
    const/4 v9, 0x1

    shl-int/2addr v9, v3

    if-ge v2, v9, :cond_3

    .line 48652
    invoke-virtual {p0, v11}, LX/0My;->b(I)V

    .line 48653
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 48654
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 48655
    :cond_4
    invoke-virtual {p0, v14}, LX/0My;->b(I)V

    .line 48656
    invoke-virtual {p0, v12}, LX/0My;->a(I)I

    move-result v9

    move v0, v1

    move v2, v1

    move v3, v1

    .line 48657
    :goto_5
    if-ge v2, v6, :cond_6

    .line 48658
    aget v10, v7, v2

    .line 48659
    aget v10, v8, v10

    add-int/2addr v3, v10

    .line 48660
    :goto_6
    if-ge v0, v3, :cond_5

    .line 48661
    invoke-virtual {p0, v9}, LX/0My;->b(I)V

    .line 48662
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 48663
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 48664
    :cond_6
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    .line 48665
    :cond_7
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static d(LX/0My;)LX/0Mz;
    .locals 14

    .prologue
    const/4 v8, 0x5

    const/4 v13, 0x4

    const/4 v12, 0x2

    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 48586
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, LX/0My;->a(I)I

    move-result v0

    const v1, 0x564342

    if-eq v0, v1, :cond_0

    .line 48587
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "expected code book to start with [0x56, 0x43, 0x42] at "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/0My;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48588
    :cond_0
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/0My;->a(I)I

    move-result v1

    .line 48589
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, LX/0My;->a(I)I

    move-result v2

    .line 48590
    new-array v3, v2, [J

    .line 48591
    invoke-virtual {p0}, LX/0My;->a()Z

    move-result v5

    .line 48592
    if-nez v5, :cond_2

    .line 48593
    invoke-virtual {p0}, LX/0My;->a()Z

    move-result v0

    .line 48594
    :goto_0
    if-ge v4, v2, :cond_4

    .line 48595
    if-eqz v0, :cond_1

    .line 48596
    invoke-virtual {p0}, LX/0My;->a()Z

    move-result v6

    if-nez v6, :cond_1

    .line 48597
    const-wide/16 v6, 0x0

    aput-wide v6, v3, v4

    .line 48598
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 48599
    :cond_1
    invoke-virtual {p0, v8}, LX/0My;->a(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    int-to-long v6, v6

    aput-wide v6, v3, v4

    goto :goto_1

    .line 48600
    :cond_2
    invoke-virtual {p0, v8}, LX/0My;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    move v7, v0

    move v0, v4

    .line 48601
    :goto_2
    if-ge v0, v2, :cond_4

    .line 48602
    sub-int v6, v2, v0

    invoke-static {v6}, LX/0N3;->a(I)I

    move-result v6

    invoke-virtual {p0, v6}, LX/0My;->a(I)I

    move-result v8

    move v6, v0

    move v0, v4

    .line 48603
    :goto_3
    if-ge v0, v8, :cond_3

    if-ge v6, v2, :cond_3

    .line 48604
    int-to-long v10, v7

    aput-wide v10, v3, v6

    .line 48605
    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 48606
    :cond_3
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move v0, v6

    .line 48607
    goto :goto_2

    .line 48608
    :cond_4
    invoke-virtual {p0, v13}, LX/0My;->a(I)I

    move-result v4

    .line 48609
    if-le v4, v12, :cond_5

    .line 48610
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "lookup type greater than 2 not decodable: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48611
    :cond_5
    if-eq v4, v9, :cond_6

    if-ne v4, v12, :cond_7

    .line 48612
    :cond_6
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, LX/0My;->b(I)V

    .line 48613
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, LX/0My;->b(I)V

    .line 48614
    invoke-virtual {p0, v13}, LX/0My;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 48615
    invoke-virtual {p0, v9}, LX/0My;->b(I)V

    .line 48616
    if-ne v4, v9, :cond_9

    .line 48617
    if-eqz v1, :cond_8

    .line 48618
    int-to-long v6, v2

    int-to-long v8, v1

    invoke-static {v6, v7, v8, v9}, LX/0N3;->a(JJ)J

    move-result-wide v6

    .line 48619
    :goto_4
    int-to-long v8, v0

    mul-long/2addr v6, v8

    long-to-int v0, v6

    invoke-virtual {p0, v0}, LX/0My;->b(I)V

    .line 48620
    :cond_7
    new-instance v0, LX/0Mz;

    invoke-direct/range {v0 .. v5}, LX/0Mz;-><init>(II[JIZ)V

    return-object v0

    .line 48621
    :cond_8
    const-wide/16 v6, 0x0

    goto :goto_4

    .line 48622
    :cond_9
    mul-int v6, v2, v1

    int-to-long v6, v6

    goto :goto_4
.end method
