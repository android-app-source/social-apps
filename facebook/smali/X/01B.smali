.class public final LX/01B;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/io/File;

.field private final b:Ljava/nio/MappedByteBuffer;

.field private final c:I


# direct methods
.method public constructor <init>(Ljava/io/File;I)V
    .locals 7

    .prologue
    .line 3862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3863
    iput-object p1, p0, LX/01B;->a:Ljava/io/File;

    .line 3864
    iput p2, p0, LX/01B;->c:I

    .line 3865
    const/4 v1, 0x0

    .line 3866
    :try_start_0
    new-instance v6, Ljava/io/RandomAccessFile;

    const-string v0, "rw"

    invoke-direct {v6, p1, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3867
    mul-int/lit8 v0, p2, 0x8

    int-to-long v4, v0

    .line 3868
    :try_start_1
    invoke-virtual {v6, v4, v5}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 3869
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/01B;->b:Ljava/nio/MappedByteBuffer;

    .line 3870
    iget-object v0, p0, LX/01B;->b:Ljava/nio/MappedByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3871
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V

    return-void

    .line 3872
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v1, :cond_0

    .line 3873
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    :cond_0
    throw v0

    .line 3874
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 3875
    iget-object v2, p0, LX/01B;->b:Ljava/nio/MappedByteBuffer;

    .line 3876
    iget v3, p0, LX/01B;->c:I

    .line 3877
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 3878
    mul-int/lit16 v1, p1, 0x3e8

    int-to-long v6, v1

    sub-long v6, v4, v6

    move v1, v0

    .line 3879
    :goto_0
    if-ge v1, v3, :cond_1

    .line 3880
    mul-int/lit8 v8, v1, 0x8

    invoke-virtual {v2, v8}, Ljava/nio/MappedByteBuffer;->getLong(I)J

    move-result-wide v8

    .line 3881
    cmp-long v10, v8, v6

    if-ltz v10, :cond_0

    cmp-long v8, v8, v4

    if-gtz v8, :cond_0

    .line 3882
    add-int/lit8 v0, v0, 0x1

    .line 3883
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3884
    :cond_1
    return v0
.end method

.method public final a()V
    .locals 12

    .prologue
    const-wide/16 v8, -0x1

    .line 3885
    iget-object v7, p0, LX/01B;->b:Ljava/nio/MappedByteBuffer;

    .line 3886
    iget v10, p0, LX/01B;->c:I

    .line 3887
    const/4 v1, -0x1

    .line 3888
    const/4 v0, 0x0

    move v6, v0

    move-wide v2, v8

    move v0, v1

    :goto_0
    if-ge v6, v10, :cond_2

    .line 3889
    mul-int/lit8 v1, v6, 0x8

    .line 3890
    invoke-virtual {v7, v1}, Ljava/nio/MappedByteBuffer;->getLong(I)J

    move-result-wide v4

    .line 3891
    cmp-long v11, v2, v8

    if-eqz v11, :cond_0

    cmp-long v11, v4, v2

    if-gez v11, :cond_1

    :cond_0
    move v0, v1

    move-wide v2, v4

    .line 3892
    :cond_1
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_0

    .line 3893
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v7, v0, v2, v3}, Ljava/nio/MappedByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    .line 3894
    return-void
.end method
