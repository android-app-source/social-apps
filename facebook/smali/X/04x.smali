.class public LX/04x;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation


# instance fields
.field private final a:[B

.field private final b:Ljavax/crypto/Cipher;

.field private final c:[B

.field public final d:Ljava/security/SecureRandom;

.field public e:[B


# direct methods
.method public constructor <init>([B[BLjava/security/SecureRandom;)V
    .locals 1

    .prologue
    .line 14893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14894
    iput-object p3, p0, LX/04x;->d:Ljava/security/SecureRandom;

    .line 14895
    iput-object p1, p0, LX/04x;->a:[B

    .line 14896
    const/16 v0, 0x10

    new-array v0, v0, [B

    .line 14897
    iget-object p1, p0, LX/04x;->d:Ljava/security/SecureRandom;

    invoke-virtual {p1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 14898
    move-object v0, v0

    .line 14899
    iput-object v0, p0, LX/04x;->e:[B

    .line 14900
    iput-object p2, p0, LX/04x;->c:[B

    .line 14901
    const-string v0, "AES/CBC/NOPadding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    iput-object v0, p0, LX/04x;->b:Ljavax/crypto/Cipher;

    .line 14902
    return-void
.end method

.method private static a(I)[B
    .locals 1

    .prologue
    .line 14903
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/04x;[B)[B
    .locals 5

    .prologue
    .line 14904
    :try_start_0
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v1, p0, LX/04x;->c:[B

    const-string v2, "AES"

    invoke-direct {v0, v1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 14905
    iget-object v1, p0, LX/04x;->b:Ljavax/crypto/Cipher;

    const/4 v2, 0x1

    new-instance v3, Ljavax/crypto/spec/IvParameterSpec;

    iget-object v4, p0, LX/04x;->e:[B

    invoke-direct {v3, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v1, v2, v0, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 14906
    iget-object v0, p0, LX/04x;->b:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    return-object v0

    .line 14907
    :catch_0
    move-exception v0

    .line 14908
    const-string v1, "FBUDPPrimingCryptography"

    const-string v2, "Bad padding exception in AES256Encrypt."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 14909
    :goto_0
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 14910
    :catch_1
    move-exception v0

    .line 14911
    const-string v1, "FBUDPPrimingCryptography"

    const-string v2, "Illegal block size exception in AES256Encrypt."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 14912
    :catch_2
    move-exception v0

    .line 14913
    const-string v1, "FBUDPPrimingCryptography"

    const-string v2, "Invalid algorithm parameter exception in AES256Encrypt."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 14914
    :catch_3
    move-exception v0

    .line 14915
    const-string v1, "FBUDPPrimingCryptography"

    const-string v2, "Invalid key exception in AES256Encrypt."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(LX/04x;[B[B[B)[B
    .locals 3

    .prologue
    .line 14916
    array-length v0, p1

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    array-length v0, p2

    const/16 v1, 0x5dc

    if-ge v0, v1, :cond_0

    array-length v0, p3

    const/16 v1, 0x100

    if-lt v0, v1, :cond_1

    .line 14917
    :cond_0
    const/4 v0, 0x0

    .line 14918
    :goto_0
    return-object v0

    .line 14919
    :cond_1
    array-length v0, p2

    add-int/lit8 v0, v0, 0x14

    add-int/lit8 v0, v0, 0x4

    array-length v1, p3

    add-int/2addr v0, v1

    .line 14920
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 14921
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 14922
    array-length v1, p2

    invoke-static {v1}, LX/04x;->a(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 14923
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 14924
    array-length v1, p3

    invoke-static {v1}, LX/04x;->a(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 14925
    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 14926
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    iget-object v1, p0, LX/04x;->a:[B

    sget-object v2, LX/04y;->HMAC_SHA256:LX/04y;

    const/4 p0, 0x0

    .line 14927
    :try_start_0
    new-instance p1, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v2}, LX/04y;->getAlgorithmName()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v1, p2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 14928
    invoke-virtual {v2}, LX/04y;->getAlgorithmName()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object p2

    .line 14929
    invoke-virtual {p2, p1}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 14930
    invoke-virtual {p2, v0}, Ljavax/crypto/Mac;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 14931
    :goto_1
    move-object v0, p0

    .line 14932
    goto :goto_0

    :catch_0
    goto :goto_1

    .line 14933
    :catch_1
    goto :goto_1
.end method
