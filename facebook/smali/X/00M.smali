.class public LX/00M;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final mConfig:LX/00K;

.field public mCrashReportEndpoint:Landroid/net/Uri;

.field public mProxy:Ljava/net/Proxy;

.field public mSkipSslCertChecks:Z


# direct methods
.method public constructor <init>(LX/00K;)V
    .locals 1

    .prologue
    .line 2169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170
    iput-object p1, p0, LX/00M;->mConfig:LX/00K;

    .line 2171
    iget-object v0, p0, LX/00M;->mConfig:LX/00K;

    .line 2172
    iget-object p1, v0, LX/00K;->mCrashReportUrl:Ljava/lang/String;

    move-object v0, p1

    .line 2173
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/00M;->mCrashReportEndpoint:Landroid/net/Uri;

    .line 2174
    return-void
.end method


# virtual methods
.method public send(LX/01l;)V
    .locals 4

    .prologue
    .line 2175
    :try_start_0
    new-instance v2, Ljava/net/URL;

    iget-object v0, p0, LX/00M;->mCrashReportEndpoint:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 2176
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Connect to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2177
    const/4 v0, 0x0

    .line 2178
    const/4 v3, 0x1

    move v1, v3

    .line 2179
    if-eqz v1, :cond_0

    .line 2180
    iget-object v0, p0, LX/00M;->mProxy:Ljava/net/Proxy;

    .line 2181
    :cond_0
    iget-boolean v1, p0, LX/00M;->mSkipSslCertChecks:Z

    if-eqz v1, :cond_1

    .line 2182
    const/4 v3, 0x1

    move v1, v3

    .line 2183
    if-eqz v1, :cond_1

    .line 2184
    new-instance v1, LX/0Bs;

    .line 2185
    const/16 p0, 0xbb8

    move v3, p0

    .line 2186
    invoke-direct {v1, v3, v0}, LX/0Bs;-><init>(ILjava/net/Proxy;)V

    move-object v0, v1

    .line 2187
    :goto_0
    const-string v3, "Android"

    move-object v1, v3

    .line 2188
    new-instance v3, LX/04c;

    invoke-direct {v3, v0}, LX/04c;-><init>(LX/04b;)V

    .line 2189
    new-instance v0, LX/04d;

    invoke-direct {v0}, LX/04d;-><init>()V

    invoke-virtual {v3, v2, p1, v0, v1}, LX/04c;->sendPost(Ljava/net/URL;Ljava/util/Map;LX/04d;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2190
    return-void

    .line 2191
    :catch_0
    move-exception v0

    .line 2192
    new-instance v1, LX/09b;

    const-string v2, "Error while sending report to Http Post Form."

    invoke-direct {v1, v2, v0}, LX/09b;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2193
    :cond_1
    new-instance v1, LX/04a;

    .line 2194
    const/16 p0, 0xbb8

    move v3, p0

    .line 2195
    invoke-direct {v1, v3, v0}, LX/04a;-><init>(ILjava/net/Proxy;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public setHost(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2196
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2197
    :cond_0
    const/4 v0, 0x0

    .line 2198
    :cond_1
    :goto_0
    return v0

    .line 2199
    :cond_2
    iget-object v1, p0, LX/00M;->mCrashReportEndpoint:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2200
    iget-object v1, p0, LX/00M;->mCrashReportEndpoint:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, LX/00M;->mCrashReportEndpoint:Landroid/net/Uri;

    goto :goto_0
.end method
