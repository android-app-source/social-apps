.class public final LX/0Nn;
.super LX/0Nk;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Nm;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Z

.field public final f:J


# direct methods
.method public constructor <init>(Ljava/lang/String;IIIZLjava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIIZ",
            "Ljava/util/List",
            "<",
            "LX/0Nm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51585
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/0Nk;-><init>(Ljava/lang/String;I)V

    .line 51586
    iput p2, p0, LX/0Nn;->a:I

    .line 51587
    iput p3, p0, LX/0Nn;->b:I

    .line 51588
    iput p4, p0, LX/0Nn;->c:I

    .line 51589
    iput-boolean p5, p0, LX/0Nn;->e:Z

    .line 51590
    iput-object p6, p0, LX/0Nn;->d:Ljava/util/List;

    .line 51591
    invoke-interface {p6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51592
    invoke-interface {p6}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nm;

    .line 51593
    iget-wide v2, v0, LX/0Nm;->d:J

    iget-wide v0, v0, LX/0Nm;->b:D

    const-wide v4, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, v4

    double-to-long v0, v0

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0Nn;->f:J

    .line 51594
    :goto_0
    return-void

    .line 51595
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0Nn;->f:J

    goto :goto_0
.end method
