.class public final LX/07x;
.super LX/0Ay;
.source ""


# instance fields
.field public final synthetic a:LX/07t;

.field private b:I


# direct methods
.method public constructor <init>(LX/07t;)V
    .locals 0

    .prologue
    .line 20362
    iput-object p1, p0, LX/07x;->a:LX/07t;

    invoke-direct {p0}, LX/0Ay;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 20363
    iget-object v0, p0, LX/07x;->a:LX/07t;

    invoke-virtual {v0}, LX/07t;->c()[LX/07v;

    .line 20364
    iget v0, p0, LX/07x;->b:I

    iget-object v1, p0, LX/07x;->a:LX/07t;

    iget-object v1, v1, LX/07t;->a:[LX/07v;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/0Is;
    .locals 3

    .prologue
    .line 20365
    iget-object v0, p0, LX/07x;->a:LX/07t;

    invoke-virtual {v0}, LX/07t;->c()[LX/07v;

    .line 20366
    iget-object v0, p0, LX/07x;->a:LX/07t;

    iget-object v0, v0, LX/07t;->a:[LX/07v;

    iget v1, p0, LX/07x;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/07x;->b:I

    aget-object v0, v0, v1

    .line 20367
    iget-object v1, p0, LX/07x;->a:LX/07t;

    iget-object v1, v1, LX/07t;->c:Ljava/util/zip/ZipFile;

    iget-object v2, v0, LX/07v;->a:Ljava/util/zip/ZipEntry;

    invoke-virtual {v1, v2}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v1

    .line 20368
    :try_start_0
    new-instance v2, LX/0Is;

    invoke-direct {v2, v0, v1}, LX/0Is;-><init>(LX/07w;Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20369
    return-object v2

    .line 20370
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    .line 20371
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_0
    throw v0
.end method
