.class public LX/0KF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/04o;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Z

.field private final c:Lcom/facebook/video/vps/VideoPlayerService;

.field private final d:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40432
    const-class v0, LX/0KF;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0KF;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 1

    .prologue
    .line 40433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40434
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0KF;->b:Z

    .line 40435
    iput-object p1, p0, LX/0KF;->c:Lcom/facebook/video/vps/VideoPlayerService;

    .line 40436
    iput-object p2, p0, LX/0KF;->d:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 40437
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;J[LX/0AR;LX/0Ln;ZLX/0Lq;)LX/0AR;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/0LQ;",
            ">;J[",
            "LX/0AR;",
            "LX/0Ln;",
            "Z",
            "LX/0Lq;",
            ")",
            "LX/0AR;"
        }
    .end annotation

    .prologue
    .line 40438
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 40439
    iget-object v0, p0, LX/0KF;->c:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0KF;->d:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v0

    .line 40440
    if-eqz v0, :cond_0

    .line 40441
    :try_start_0
    invoke-virtual {v0}, LX/0Kb;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40442
    :cond_0
    :goto_0
    return-void

    .line 40443
    :catch_0
    move-exception v0

    .line 40444
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Caught exception when enable evaluator: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public final a(Ljava/util/List;J[LX/0AR;LX/0Ld;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/0LQ;",
            ">;J[",
            "LX/0AR;",
            "LX/0Ld;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 40445
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DashEvaluatorProxy evaluate with "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " formats"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40446
    array-length v1, p4

    new-array v4, v1, [Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    .line 40447
    array-length v1, p4

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 40448
    aget-object v0, p4, v0

    iput-object v0, p5, LX/0Ld;->c:LX/0AR;

    .line 40449
    :cond_0
    :goto_0
    return-void

    .line 40450
    :cond_1
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 40451
    :goto_1
    array-length v1, p4

    if-ge v0, v1, :cond_2

    .line 40452
    aget-object v1, p4, v0

    .line 40453
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Format ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v1, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, LX/0AR;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bitrate "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, LX/0AR;->c:I

    div-int/lit16 v3, v3, 0x3e8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "kbps "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, LX/0AR;->f:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, LX/0AR;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40454
    new-instance v2, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    invoke-direct {v2, v1}, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;-><init>(LX/0AR;)V

    aput-object v2, v4, v0

    .line 40455
    iget-object v2, v1, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v6, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40456
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 40457
    :cond_2
    iget-object v0, p0, LX/0KF;->c:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0KF;->d:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v0

    .line 40458
    if-eqz v0, :cond_0

    .line 40459
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 40460
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0LQ;

    .line 40461
    new-instance v5, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;

    invoke-direct {v5, v2}, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;-><init>(LX/0LQ;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 40462
    :catch_0
    move-exception v0

    .line 40463
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Caught exception when do dash evalution: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 40464
    :cond_3
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "evaluation format before evaluate: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p5, LX/0Ld;->c:LX/0AR;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 40465
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "evaluation queueSize before evaluate: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p5, LX/0Ld;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40466
    new-instance v5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;

    invoke-direct {v5}, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;-><init>()V

    .line 40467
    iget v2, p5, LX/0Ld;->a:I

    iput v2, v5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->a:I

    .line 40468
    iget-object v2, p5, LX/0Ld;->c:LX/0AR;

    if-eqz v2, :cond_4

    .line 40469
    new-instance v2, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iget-object v3, p5, LX/0Ld;->c:LX/0AR;

    invoke-direct {v2, v3}, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;-><init>(LX/0AR;)V

    iput-object v2, v5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    :cond_4
    move-wide v2, p2

    .line 40470
    invoke-virtual/range {v0 .. v5}, LX/0Kb;->a(Ljava/util/List;J[Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;)V

    .line 40471
    iget-object v0, v5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    if-eqz v0, :cond_5

    .line 40472
    iget v0, v5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->a:I

    iput v0, p5, LX/0Ld;->a:I

    .line 40473
    iget v0, v5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->b:I

    iput v0, p5, LX/0Ld;->b:I

    .line 40474
    iget-object v0, v5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iget-object v0, v0, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0AR;

    iput-object v0, p5, LX/0Ld;->c:LX/0AR;

    .line 40475
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DashEvaluatorProxy choose "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p5, LX/0Ld;->c:LX/0AR;

    iget-object v1, v1, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p5, LX/0Ld;->c:LX/0AR;

    iget v1, v1, LX/0AR;->c:I

    div-int/lit16 v1, v1, 0x3e8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "kbps"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 40476
    :cond_5
    const/4 v0, 0x0

    aget-object v0, p4, v0

    iput-object v0, p5, LX/0Ld;->c:LX/0AR;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 40477
    iget-object v0, p0, LX/0KF;->c:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0KF;->d:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v0

    .line 40478
    if-eqz v0, :cond_0

    .line 40479
    :try_start_0
    invoke-virtual {v0}, LX/0Kb;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40480
    :cond_0
    :goto_0
    return-void

    .line 40481
    :catch_0
    move-exception v0

    .line 40482
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Caught exception when disable evaluator: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
