.class public LX/062;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Ljava/lang/reflect/Field;

.field public static b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17188
    const/4 v0, 0x0

    sput-boolean v0, LX/062;->b:Z

    .line 17189
    :try_start_0
    const-string v0, "org.apache.harmony.xnet.provider.jsse.SSLParametersImpl"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 17190
    const-string v0, "org.apache.harmony.xnet.provider.jsse.ClientSessionContext"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 17191
    const-class v0, Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

    const-string v1, "clientSessionContext"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 17192
    sput-object v0, LX/062;->a:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 17193
    const/4 v0, 0x1

    sput-boolean v0, LX/062;->b:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 17194
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17201
    return-void
.end method

.method public static a(Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;I)V
    .locals 2

    .prologue
    .line 17195
    :try_start_0
    sget-object v0, LX/062;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/ClientSessionContext;

    .line 17196
    invoke-virtual {v0, p1}, Lorg/apache/harmony/xnet/provider/jsse/ClientSessionContext;->setSessionTimeout(I)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 17197
    return-void

    .line 17198
    :catch_0
    move-exception v0

    .line 17199
    new-instance v1, LX/061;

    invoke-direct {v1, v0}, LX/061;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method
