.class public LX/0FF;
.super Ljava/lang/RuntimeException;
.source ""


# instance fields
.field private final mRemedy:LX/0FE;


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 32942
    sget-object v0, LX/0FE;->UNKNOWN:LX/0FE;

    invoke-direct {p0, p1, v0}, LX/0FF;-><init>(Ljava/lang/Throwable;LX/0FE;)V

    .line 32943
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;LX/0FE;)V
    .locals 0

    .prologue
    .line 32944
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 32945
    iput-object p2, p0, LX/0FF;->mRemedy:LX/0FE;

    .line 32946
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 3

    .prologue
    .line 32947
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 32948
    const-string v1, "Application is in corrupt state. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32949
    sget-object v1, LX/0FD;->$SwitchMap$com$facebook$common$dextricks$CorruptedApplicationStateException$Remedy:[I

    iget-object v2, p0, LX/0FF;->mRemedy:LX/0FE;

    invoke-virtual {v2}, LX/0FE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 32950
    :goto_0
    const-string v1, "[ mRemedy = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/0FF;->mRemedy:LX/0FE;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32951
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 32952
    :pswitch_0
    const-string v1, "Reboot device. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 32953
    :pswitch_1
    const-string v1, "Reinstall application."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getRemedy()LX/0FE;
    .locals 1

    .prologue
    .line 32954
    iget-object v0, p0, LX/0FF;->mRemedy:LX/0FE;

    return-object v0
.end method
