.class public final LX/05S;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final A:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final B:Ljava/lang/String;

.field public final C:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final D:I

.field public final E:Z

.field public final F:Z

.field public final G:Z

.field public final H:Z

.field public final I:Z

.field public final J:Z

.field public final K:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final a:Landroid/content/Context;

.field public final b:Ljava/lang/String;

.field public final c:LX/056;

.field public final d:LX/055;

.field public final e:LX/05G;

.field public final f:LX/05F;

.field public final g:LX/05G;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/05F;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:LX/05L;

.field public final j:LX/05K;

.field public final k:LX/04p;

.field public final l:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Landroid/os/Handler;

.field public final o:LX/05R;

.field public final p:LX/05O;

.field public final q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:LX/04v;

.field public final s:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final t:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final u:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final v:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final w:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final x:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final y:LX/05H;

.field public final z:LX/05M;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LX/056;LX/055;LX/05G;LX/05F;LX/05G;LX/05F;LX/05L;LX/05K;LX/04p;LX/05N;LX/05N;Landroid/os/Handler;LX/05R;LX/05O;Ljava/lang/String;LX/04v;LX/05N;LX/05N;LX/05N;LX/05N;LX/05N;LX/05N;LX/05H;LX/05M;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;LX/05N;IZZZZZZLjava/util/Map;)V
    .locals 2
    .param p7    # LX/05G;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/05F;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # LX/05N;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p17    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p27    # Ljava/util/concurrent/atomic/AtomicReference;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p37    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "LX/056;",
            "Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$ConnectionManagerCallbacks;",
            "LX/05G;",
            "LX/05F;",
            "LX/05G;",
            "LX/05F;",
            "LX/05L;",
            "LX/05K;",
            "LX/04p;",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/05N",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/os/Handler;",
            "LX/05R;",
            "LX/05O;",
            "Ljava/lang/String;",
            "LX/04v;",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/05N",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/05H;",
            "LX/05M;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;IZZZZZZ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16126
    invoke-static {p1}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, LX/05S;->a:Landroid/content/Context;

    .line 16127
    invoke-static {p2}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, LX/05S;->b:Ljava/lang/String;

    .line 16128
    invoke-static {p3}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/056;

    iput-object v1, p0, LX/05S;->c:LX/056;

    .line 16129
    invoke-static {p4}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/055;

    iput-object v1, p0, LX/05S;->d:LX/055;

    .line 16130
    invoke-static {p5}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05G;

    iput-object v1, p0, LX/05S;->e:LX/05G;

    .line 16131
    invoke-static {p6}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05F;

    iput-object v1, p0, LX/05S;->f:LX/05F;

    .line 16132
    iput-object p7, p0, LX/05S;->g:LX/05G;

    .line 16133
    iput-object p8, p0, LX/05S;->h:LX/05F;

    .line 16134
    invoke-static {p9}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05L;

    iput-object v1, p0, LX/05S;->i:LX/05L;

    .line 16135
    invoke-static {p10}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05K;

    iput-object v1, p0, LX/05S;->j:LX/05K;

    .line 16136
    invoke-static {p11}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/04p;

    iput-object v1, p0, LX/05S;->k:LX/04p;

    .line 16137
    iput-object p12, p0, LX/05S;->l:LX/05N;

    .line 16138
    invoke-static {p13}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05N;

    iput-object v1, p0, LX/05S;->m:LX/05N;

    .line 16139
    invoke-static/range {p14 .. p14}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    iput-object v1, p0, LX/05S;->n:Landroid/os/Handler;

    .line 16140
    invoke-static/range {p15 .. p15}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05R;

    iput-object v1, p0, LX/05S;->o:LX/05R;

    .line 16141
    invoke-static/range {p16 .. p16}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05O;

    iput-object v1, p0, LX/05S;->p:LX/05O;

    .line 16142
    move-object/from16 v0, p17

    iput-object v0, p0, LX/05S;->q:Ljava/lang/String;

    .line 16143
    invoke-static/range {p18 .. p18}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/04v;

    iput-object v1, p0, LX/05S;->r:LX/04v;

    .line 16144
    invoke-static/range {p19 .. p19}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05N;

    iput-object v1, p0, LX/05S;->s:LX/05N;

    .line 16145
    invoke-static/range {p20 .. p20}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05N;

    iput-object v1, p0, LX/05S;->t:LX/05N;

    .line 16146
    invoke-static/range {p21 .. p21}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05N;

    iput-object v1, p0, LX/05S;->u:LX/05N;

    .line 16147
    invoke-static/range {p22 .. p22}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05N;

    iput-object v1, p0, LX/05S;->v:LX/05N;

    .line 16148
    invoke-static/range {p23 .. p23}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05N;

    iput-object v1, p0, LX/05S;->w:LX/05N;

    .line 16149
    invoke-static/range {p24 .. p24}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05N;

    iput-object v1, p0, LX/05S;->x:LX/05N;

    .line 16150
    invoke-static/range {p25 .. p25}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05H;

    iput-object v1, p0, LX/05S;->y:LX/05H;

    .line 16151
    invoke-static/range {p26 .. p26}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05M;

    iput-object v1, p0, LX/05S;->z:LX/05M;

    .line 16152
    move-object/from16 v0, p27

    iput-object v0, p0, LX/05S;->A:Ljava/util/concurrent/atomic/AtomicReference;

    .line 16153
    invoke-static/range {p28 .. p28}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, LX/05S;->B:Ljava/lang/String;

    .line 16154
    invoke-static/range {p29 .. p29}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/05N;

    iput-object v1, p0, LX/05S;->C:LX/05N;

    .line 16155
    move/from16 v0, p30

    iput v0, p0, LX/05S;->D:I

    .line 16156
    move/from16 v0, p31

    iput-boolean v0, p0, LX/05S;->E:Z

    .line 16157
    move/from16 v0, p32

    iput-boolean v0, p0, LX/05S;->F:Z

    .line 16158
    move/from16 v0, p33

    iput-boolean v0, p0, LX/05S;->G:Z

    .line 16159
    move/from16 v0, p34

    iput-boolean v0, p0, LX/05S;->H:Z

    .line 16160
    move/from16 v0, p35

    iput-boolean v0, p0, LX/05S;->I:Z

    .line 16161
    move/from16 v0, p36

    iput-boolean v0, p0, LX/05S;->J:Z

    .line 16162
    move-object/from16 v0, p37

    iput-object v0, p0, LX/05S;->K:Ljava/util/Map;

    .line 16163
    return-void
.end method
