.class public LX/04c;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private mConnectionProvider:LX/04b;


# direct methods
.method public constructor <init>(LX/04b;)V
    .locals 0

    .prologue
    .line 13923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13924
    iput-object p1, p0, LX/04c;->mConnectionProvider:LX/04b;

    .line 13925
    return-void
.end method

.method public static encodeParameters(Ljava/util/Map;Ljava/io/OutputStream;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .prologue
    .line 13926
    const/4 v0, 0x1

    .line 13927
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/OutputStreamWriter;

    invoke-direct {v1, p1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v2, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 13928
    new-instance v3, LX/04z;

    invoke-direct {v3, v2}, LX/04z;-><init>(Ljava/io/Writer;)V

    .line 13929
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 13930
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    .line 13931
    if-nez v1, :cond_0

    .line 13932
    const/16 v1, 0x26

    invoke-virtual {v2, v1}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    .line 13933
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 13934
    if-nez v0, :cond_1

    .line 13935
    const-string v0, ""

    .line 13936
    :cond_1
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/04z;->write(Ljava/lang/String;)V

    .line 13937
    const/16 v1, 0x3d

    invoke-virtual {v2, v1}, Ljava/io/Writer;->write(I)V

    .line 13938
    instance-of v1, v0, Ljava/io/InputStream;

    if-eqz v1, :cond_2

    .line 13939
    check-cast v0, Ljava/io/InputStream;

    .line 13940
    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 13941
    const/16 v6, 0x100

    new-array v6, v6, [C

    .line 13942
    :goto_1
    :try_start_0
    invoke-virtual {v5, v6}, Ljava/io/InputStreamReader;->read([C)I

    move-result p0

    .line 13943
    const/4 p1, -0x1

    if-eq p0, p1, :cond_4

    .line 13944
    const/4 p1, 0x0

    invoke-virtual {v3, v6, p1, p0}, Ljava/io/Writer;->write([CII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 13945
    :catch_0
    :goto_2
    const/4 v0, 0x0

    move v1, v0

    .line 13946
    goto :goto_0

    .line 13947
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/04z;->write(Ljava/lang/String;)V

    goto :goto_2

    .line 13948
    :cond_3
    invoke-virtual {v2}, Ljava/io/Writer;->flush()V

    .line 13949
    return-void

    :cond_4
    goto :goto_2
.end method


# virtual methods
.method public sendPost(Ljava/net/URL;Ljava/util/Map;LX/04d;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URL;",
            "Ljava/util/Map",
            "<**>;",
            "LX/04d;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 13950
    iget-object v0, p0, LX/04c;->mConnectionProvider:LX/04b;

    invoke-interface {v0, p1}, LX/04b;->getConnection(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 13951
    const-string v0, "POST"

    invoke-virtual {v1, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 13952
    const-string v0, "User-Agent"

    invoke-virtual {v1, v0, p4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 13953
    const-string v0, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded"

    invoke-virtual {v1, v0, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 13954
    const-string v0, "Content-Encoding"

    const-string v2, "gzip"

    invoke-virtual {v1, v0, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 13955
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 13956
    :try_start_0
    new-instance v0, Ljava/util/zip/GZIPOutputStream;

    const v2, -0x7e7b3e7a

    invoke-static {v1, v2}, LX/04e;->c(Ljava/net/URLConnection;I)Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 13957
    invoke-static {p2, v0}, LX/04c;->encodeParameters(Ljava/util/Map;Ljava/io/OutputStream;)V

    .line 13958
    invoke-virtual {v0}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 13959
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 13960
    iput v0, p3, LX/04d;->mStatus:I

    .line 13961
    const v0, -0x4b8d4d3d

    invoke-static {v1, v0}, LX/04e;->b(Ljava/net/URLConnection;I)Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13962
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 13963
    return-void

    .line 13964
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v0
.end method
