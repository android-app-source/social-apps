.class public abstract LX/0Kg;
.super LX/0GU;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:LX/0Kp;

.field private final b:Landroid/os/Handler;

.field private final c:LX/0Ka;

.field public final d:LX/0L5;

.field public e:LX/0L4;

.field public f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

.field public g:LX/08M;

.field public h:LX/0M7;

.field public i:Z

.field public j:Z

.field private k:Z

.field public l:Z


# direct methods
.method public constructor <init>(LX/0L9;Landroid/os/Handler;LX/0Ka;)V
    .locals 2

    .prologue
    .line 41644
    const/4 v0, 0x1

    new-array v0, v0, [LX/0L9;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-direct {p0, v0}, LX/0GU;-><init>([LX/0L9;)V

    .line 41645
    new-instance v0, LX/0Kp;

    invoke-direct {v0}, LX/0Kp;-><init>()V

    iput-object v0, p0, LX/0Kg;->a:LX/0Kp;

    .line 41646
    iput-object p2, p0, LX/0Kg;->b:Landroid/os/Handler;

    .line 41647
    iput-object p3, p0, LX/0Kg;->c:LX/0Ka;

    .line 41648
    new-instance v0, LX/0L5;

    invoke-direct {v0}, LX/0L5;-><init>()V

    iput-object v0, p0, LX/0Kg;->d:LX/0L5;

    .line 41649
    return-void
.end method

.method private a(LX/0M4;)V
    .locals 3

    .prologue
    .line 41738
    iget-object v0, p0, LX/0Kg;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Kg;->c:LX/0Ka;

    if-eqz v0, :cond_0

    .line 41739
    iget-object v0, p0, LX/0Kg;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/video/vps/spatialaudio/OpusTrackRenderer$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/vps/spatialaudio/OpusTrackRenderer$1;-><init>(LX/0Kg;LX/0M4;)V

    const v2, -0x76a0247e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 41740
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(JJZ)V
    .locals 5

    .prologue
    .line 41677
    iget-boolean v0, p0, LX/0Kg;->j:Z

    if-eqz v0, :cond_1

    .line 41678
    :cond_0
    :goto_0
    return-void

    .line 41679
    :cond_1
    iput-boolean p5, p0, LX/0Kg;->k:Z

    .line 41680
    iget-object v0, p0, LX/0Kg;->e:LX/0L4;

    if-nez v0, :cond_2

    .line 41681
    iget-object v0, p0, LX/0Kg;->d:LX/0L5;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/0GU;->a(JLX/0L5;LX/0L7;)I

    move-result v0

    .line 41682
    const/4 v1, -0x4

    if-ne v0, v1, :cond_7

    .line 41683
    iget-object v0, p0, LX/0Kg;->d:LX/0L5;

    iget-object v0, v0, LX/0L5;->a:LX/0L4;

    iput-object v0, p0, LX/0Kg;->e:LX/0L4;

    .line 41684
    iget-object v0, p0, LX/0Kg;->e:LX/0L4;

    invoke-virtual {v0}, LX/0L4;->b()Landroid/media/MediaFormat;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Kg;->a(Landroid/media/MediaFormat;)V

    .line 41685
    const/4 v0, 0x1

    .line 41686
    :goto_1
    move v0, v0

    .line 41687
    if-eqz v0, :cond_0

    .line 41688
    :cond_2
    iget-object v0, p0, LX/0Kg;->f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

    if-nez v0, :cond_4

    .line 41689
    iget-object v0, p0, LX/0Kg;->e:LX/0L4;

    iget-object v0, v0, LX/0L4;->f:Ljava/util/List;

    .line 41690
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_3

    .line 41691
    new-instance v0, LX/0Kv;

    const-string v1, "Missing initialization data"

    invoke-direct {v0, v1}, LX/0Kv;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41692
    :cond_3
    :try_start_0
    new-instance v1, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

    const/16 v2, 0x10

    const/16 v3, 0x10

    const/16 v4, 0x2580

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;-><init>(IIILjava/util/List;)V

    iput-object v1, p0, LX/0Kg;->f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;
    :try_end_0
    .catch LX/0M4; {:try_start_0 .. :try_end_0} :catch_0

    .line 41693
    iget-object v0, p0, LX/0Kg;->f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->start()V

    .line 41694
    iget-object v0, p0, LX/0Kg;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->a:I

    .line 41695
    :cond_4
    :try_start_1
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 41696
    iget-boolean v0, p0, LX/0Kg;->j:Z

    if-eqz v0, :cond_8

    .line 41697
    :cond_5
    :goto_2
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 41698
    iget-boolean v2, p0, LX/0Kg;->i:Z

    if-eqz v2, :cond_b

    .line 41699
    :cond_6
    :goto_3
    move v0, v0
    :try_end_1
    .catch LX/0M4; {:try_start_1 .. :try_end_1} :catch_1

    .line 41700
    if-nez v0, :cond_5

    .line 41701
    iget-object v0, p0, LX/0Kg;->a:LX/0Kp;

    invoke-virtual {v0}, LX/0Kp;->a()V

    goto :goto_0

    .line 41702
    :catch_0
    move-exception v0

    .line 41703
    invoke-direct {p0, v0}, LX/0Kg;->a(LX/0M4;)V

    .line 41704
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 41705
    :catch_1
    move-exception v0

    .line 41706
    invoke-direct {p0, v0}, LX/0Kg;->a(LX/0M4;)V

    .line 41707
    new-instance v1, LX/0Kv;

    invoke-direct {v1, v0}, LX/0Kv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 41708
    :cond_8
    :try_start_2
    iget-object v0, p0, LX/0Kg;->h:LX/0M7;

    if-nez v0, :cond_9

    .line 41709
    iget-object v0, p0, LX/0Kg;->f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->b()LX/0M6;

    move-result-object v0

    check-cast v0, LX/0M7;

    iput-object v0, p0, LX/0Kg;->h:LX/0M7;

    .line 41710
    iget-object v0, p0, LX/0Kg;->h:LX/0M7;

    if-eqz v0, :cond_5

    .line 41711
    :cond_9
    iget-object v0, p0, LX/0Kg;->h:LX/0M7;

    invoke-virtual {v0, v1}, LX/0M5;->b(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 41712
    iput-boolean v1, p0, LX/0Kg;->j:Z

    .line 41713
    invoke-virtual {p0}, LX/0Kg;->k()V

    .line 41714
    iget-object v0, p0, LX/0Kg;->h:LX/0M7;

    invoke-virtual {v0}, LX/0M7;->b()V

    .line 41715
    iput-object v2, p0, LX/0Kg;->h:LX/0M7;

    goto :goto_2

    .line 41716
    :cond_a
    iget-object v0, p0, LX/0Kg;->h:LX/0M7;

    invoke-virtual {p0, v0}, LX/0Kg;->a(LX/0M7;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 41717
    iget-object v0, p0, LX/0Kg;->h:LX/0M7;

    invoke-virtual {v0}, LX/0M7;->b()V

    .line 41718
    iput-object v2, p0, LX/0Kg;->h:LX/0M7;

    goto :goto_2
    :try_end_2
    .catch LX/0M4; {:try_start_2 .. :try_end_2} :catch_1

    .line 41719
    :cond_b
    iget-object v2, p0, LX/0Kg;->g:LX/08M;

    if-nez v2, :cond_c

    .line 41720
    iget-object v2, p0, LX/0Kg;->f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

    invoke-virtual {v2}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a()LX/08M;

    move-result-object v2

    iput-object v2, p0, LX/0Kg;->g:LX/08M;

    .line 41721
    iget-object v2, p0, LX/0Kg;->g:LX/08M;

    if-eqz v2, :cond_6

    .line 41722
    :cond_c
    iget-object v2, p0, LX/0Kg;->d:LX/0L5;

    iget-object v3, p0, LX/0Kg;->g:LX/08M;

    iget-object v3, v3, LX/08M;->a:LX/0L7;

    invoke-virtual {p0, p1, p2, v2, v3}, LX/0GU;->a(JLX/0L5;LX/0L7;)I

    move-result v2

    .line 41723
    const/4 v3, -0x2

    if-eq v2, v3, :cond_6

    .line 41724
    const/4 v3, -0x4

    if-ne v2, v3, :cond_d

    .line 41725
    iget-object v0, p0, LX/0Kg;->d:LX/0L5;

    iget-object v0, v0, LX/0L5;->a:LX/0L4;

    iput-object v0, p0, LX/0Kg;->e:LX/0L4;

    move v0, v1

    .line 41726
    goto :goto_3

    .line 41727
    :cond_d
    const/4 v3, -0x1

    if-ne v2, v3, :cond_e

    .line 41728
    iget-object v2, p0, LX/0Kg;->g:LX/08M;

    invoke-virtual {v2, v1}, LX/0M5;->a(I)V

    .line 41729
    iget-object v2, p0, LX/0Kg;->f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

    iget-object v3, p0, LX/0Kg;->g:LX/08M;

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a(LX/08M;)V

    .line 41730
    iput-object v4, p0, LX/0Kg;->g:LX/08M;

    .line 41731
    iput-boolean v1, p0, LX/0Kg;->i:Z

    goto/16 :goto_3

    .line 41732
    :cond_e
    iget-boolean v2, p0, LX/0Kg;->l:Z

    if-eqz v2, :cond_f

    .line 41733
    iput-boolean v0, p0, LX/0Kg;->l:Z

    .line 41734
    iget-object v0, p0, LX/0Kg;->g:LX/08M;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, LX/0M5;->a(I)V

    .line 41735
    :cond_f
    iget-object v0, p0, LX/0Kg;->f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

    iget-object v2, p0, LX/0Kg;->g:LX/08M;

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a(LX/08M;)V

    .line 41736
    iput-object v4, p0, LX/0Kg;->g:LX/08M;

    move v0, v1

    .line 41737
    goto/16 :goto_3
.end method

.method public abstract a(Landroid/media/MediaFormat;)V
.end method

.method public final a(LX/0L4;)Z
    .locals 2

    .prologue
    .line 41676
    const-string v0, "audio/opus"

    iget-object v1, p1, LX/0L4;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public abstract a(LX/0M7;)Z
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 41675
    iget-boolean v0, p0, LX/0Kg;->j:Z

    return v0
.end method

.method public c(J)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41663
    iput-boolean v0, p0, LX/0Kg;->i:Z

    .line 41664
    iput-boolean v0, p0, LX/0Kg;->j:Z

    .line 41665
    iput-boolean v0, p0, LX/0Kg;->k:Z

    .line 41666
    iget-object v0, p0, LX/0Kg;->f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

    if-eqz v0, :cond_1

    .line 41667
    const/4 p1, 0x0

    .line 41668
    iput-object p1, p0, LX/0Kg;->g:LX/08M;

    .line 41669
    iget-object v0, p0, LX/0Kg;->h:LX/0M7;

    if-eqz v0, :cond_0

    .line 41670
    iget-object v0, p0, LX/0Kg;->h:LX/0M7;

    invoke-virtual {v0}, LX/0M7;->b()V

    .line 41671
    iput-object p1, p0, LX/0Kg;->h:LX/0M7;

    .line 41672
    :cond_0
    iget-object v0, p0, LX/0Kg;->f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->c()V

    .line 41673
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Kg;->l:Z

    .line 41674
    :cond_1
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 41662
    iget-object v0, p0, LX/0Kg;->e:LX/0L4;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/0Kg;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Kg;->h:LX/0M7;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 41661
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 41660
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 41650
    iput-object v0, p0, LX/0Kg;->g:LX/08M;

    .line 41651
    iput-object v0, p0, LX/0Kg;->h:LX/0M7;

    .line 41652
    iput-object v0, p0, LX/0Kg;->e:LX/0L4;

    .line 41653
    :try_start_0
    iget-object v0, p0, LX/0Kg;->f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

    if-eqz v0, :cond_0

    .line 41654
    iget-object v0, p0, LX/0Kg;->f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->d()V

    .line 41655
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Kg;->f:Lcom/google/android/exoplayer/ext/opus/OpusDecoder;

    .line 41656
    iget-object v0, p0, LX/0Kg;->a:LX/0Kp;

    iget v1, v0, LX/0Kp;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Kp;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41657
    :cond_0
    invoke-super {p0}, LX/0GU;->j()V

    .line 41658
    return-void

    .line 41659
    :catchall_0
    move-exception v0

    invoke-super {p0}, LX/0GU;->j()V

    throw v0
.end method

.method public abstract k()V
.end method
