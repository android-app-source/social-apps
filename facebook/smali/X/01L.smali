.class public LX/01L;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Z

.field public static b:LX/020;

.field public static c:[LX/022;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private static final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static e:Landroid/os/StrictMode$ThreadPolicy;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private static f:LX/09W;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private static g:Ljava/lang/String;

.field private static h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 4566
    sput-object v2, LX/01L;->c:[LX/022;

    .line 4567
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sput-object v1, LX/01L;->d:Ljava/util/Set;

    .line 4568
    sput-object v2, LX/01L;->e:Landroid/os/StrictMode$ThreadPolicy;

    .line 4569
    sput-object v2, LX/01L;->f:LX/09W;

    .line 4570
    const-string v1, "lib-main"

    sput-object v1, LX/01L;->g:Ljava/lang/String;

    .line 4571
    :try_start_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_1

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    .line 4572
    :cond_0
    :goto_0
    sput-boolean v0, LX/01L;->a:Z

    .line 4573
    return-void

    :catch_0
    goto :goto_0

    :catch_1
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4411
    return-void
.end method

.method public static declared-synchronized a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 4558
    const-class v1, LX/01L;

    monitor-enter v1

    :try_start_0
    invoke-static {}, LX/01L;->e()V

    .line 4559
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 4560
    sget-object v3, LX/01L;->c:[LX/022;

    .line 4561
    const/4 v0, 0x0

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    .line 4562
    aget-object v4, v3, v0

    invoke-virtual {v4, v2}, LX/022;->a(Ljava/util/Collection;)V

    .line 4563
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4564
    :cond_0
    const-string v0, ":"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 4565
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized a(LX/020;)V
    .locals 10
    .param p0    # LX/020;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 4529
    const-class v6, LX/01L;

    monitor-enter v6

    if-eqz p0, :cond_0

    .line 4530
    :try_start_0
    sput-object p0, LX/01L;->b:LX/020;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4531
    :goto_0
    monitor-exit v6

    return-void

    .line 4532
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    .line 4533
    const/4 v1, 0x0

    .line 4534
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-ge v0, v2, :cond_3

    move-object v0, v1

    .line 4535
    :goto_1
    move-object v5, v0

    .line 4536
    if-eqz v5, :cond_1

    const/4 v1, 0x1

    .line 4537
    :goto_2
    if-eqz v1, :cond_2

    invoke-static {}, LX/0Ip;->a()Ljava/lang/String;

    move-result-object v2

    .line 4538
    :goto_3
    if-nez v2, :cond_4

    .line 4539
    const/4 v0, 0x0

    .line 4540
    :goto_4
    move-object v3, v0

    .line 4541
    new-instance v0, LX/020;

    invoke-direct/range {v0 .. v5}, LX/020;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Runtime;Ljava/lang/reflect/Method;)V

    sput-object v0, LX/01L;->b:LX/020;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 4542
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 4543
    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    .line 4544
    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    .line 4545
    :cond_3
    :try_start_2
    const-class v0, Ljava/lang/Runtime;

    const-string v2, "nativeLoad"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class p0, Ljava/lang/String;

    aput-object p0, v3, v5

    const/4 v5, 0x1

    const-class p0, Ljava/lang/ClassLoader;

    aput-object p0, v3, v5

    const/4 v5, 0x2

    const-class p0, Ljava/lang/String;

    aput-object p0, v3, v5

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 4546
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    goto :goto_1

    .line 4547
    :catch_0
    move-exception v0

    .line 4548
    :goto_5
    const-string v2, "SoLoader"

    const-string v3, "Cannot get nativeLoad method"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 4549
    goto :goto_1

    .line 4550
    :catch_1
    move-exception v0

    goto :goto_5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4551
    :cond_4
    const-string v0, ":"

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 4552
    new-instance v7, Ljava/util/ArrayList;

    array-length v0, v3

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 4553
    array-length v8, v3

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v8, :cond_6

    aget-object v9, v3, v0

    .line 4554
    const-string p0, "!"

    invoke-virtual {v9, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_5

    .line 4555
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4556
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 4557
    :cond_6
    const-string v0, ":"

    invoke-static {v0, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method

.method public static declared-synchronized a(LX/022;)V
    .locals 6

    .prologue
    .line 4521
    const-class v1, LX/01L;

    monitor-enter v1

    :try_start_0
    invoke-static {}, LX/01L;->e()V

    .line 4522
    invoke-static {}, LX/01L;->c()I

    move-result v0

    invoke-virtual {p0, v0}, LX/022;->a(I)V

    .line 4523
    sget-object v0, LX/01L;->c:[LX/022;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [LX/022;

    .line 4524
    const/4 v2, 0x0

    aput-object p0, v0, v2

    .line 4525
    sget-object v2, LX/01L;->c:[LX/022;

    const/4 v3, 0x0

    const/4 v4, 0x1

    sget-object v5, LX/01L;->c:[LX/022;

    array-length v5, v5

    invoke-static {v2, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4526
    sput-object v0, LX/01L;->c:[LX/022;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4527
    monitor-exit v1

    return-void

    .line 4528
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 4519
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/01L;->a(Landroid/content/Context;ILX/020;)V

    .line 4520
    return-void
.end method

.method private static a(Landroid/content/Context;ILX/020;)V
    .locals 2
    .param p2    # LX/020;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 4514
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 4515
    :try_start_0
    invoke-static {p0, p1, p2}, LX/01L;->b(Landroid/content/Context;ILX/020;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4516
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 4517
    return-void

    .line 4518
    :catchall_0
    move-exception v1

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v1
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 4509
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    invoke-static {p0, v0}, LX/01L;->a(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4510
    return-void

    .line 4511
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4512
    :catch_0
    move-exception v0

    .line 4513
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 4507
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/01L;->a(Ljava/lang/String;I)V

    .line 4508
    return-void
.end method

.method public static declared-synchronized a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 4484
    const-class v2, LX/01L;

    monitor-enter v2

    :try_start_0
    sget-object v0, LX/01L;->c:[LX/022;

    if-nez v0, :cond_0

    .line 4485
    const-string v0, "http://www.android.com/"

    const-string v1, "java.vendor.url"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4486
    invoke-static {}, LX/01L;->e()V

    .line 4487
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 4488
    const/4 v0, 0x0

    :goto_1
    move-object v1, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4489
    if-eqz v1, :cond_4

    move-object v0, v1

    .line 4490
    :goto_2
    :try_start_1
    invoke-static {v0}, Ljava/lang/System;->mapLibraryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LX/01L;->b(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4491
    if-eqz v1, :cond_2

    .line 4492
    :try_start_2
    invoke-static {p0}, LX/02K;->b(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4493
    :cond_2
    :goto_3
    monitor-exit v2

    return-void

    .line 4494
    :cond_3
    :try_start_3
    sget-object v0, LX/01L;->f:LX/09W;

    if-nez v0, :cond_2

    .line 4495
    invoke-static {p0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 4496
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_4
    move-object v0, p0

    .line 4497
    goto :goto_2

    .line 4498
    :catch_0
    move-exception v0

    .line 4499
    :try_start_4
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 4500
    :catch_1
    move-exception v0

    .line 4501
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 4502
    if-eqz v1, :cond_5

    const-string v3, "unexpected e_machine:"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4503
    new-instance v1, LX/0Iq;

    invoke-direct {v1, v0}, LX/0Iq;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 4504
    :cond_5
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4505
    :sswitch_0
    const-string v1, "appstatelogger"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "asyncexecutor"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "compactdisk-jni"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "conceal"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "database"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v1, "double-conversion"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v1, "fb_jpegturbo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x6

    goto/16 :goto_0

    :sswitch_7
    const-string v1, "fb_sqlite_omnistore"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x7

    goto/16 :goto_0

    :sswitch_8
    const-string v1, "fbacore-jni"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x8

    goto/16 :goto_0

    :sswitch_9
    const-string v1, "fbacore"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x9

    goto/16 :goto_0

    :sswitch_a
    const-string v1, "folly-extended"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xa

    goto/16 :goto_0

    :sswitch_b
    const-string v1, "folly-futures"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v1, "folly"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v1, "imagepipeline"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v1, "java_com_facebook_tigon_iface_jni_iface-jni"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v1, "java_com_facebook_tigon_javaservice_jni_javaservice-jni"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v1, "jniexecutors"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v1, "liger-native"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v1, "liger"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v1, "loom"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v1, "lyramanager"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v1, "mobileconfig-jni"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v1, "native_loom_constants"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v1, "native_loom_logger"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v1, "native_loom_network_network"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x18

    goto/16 :goto_0

    :sswitch_19
    const-string v1, "native_loom_profiler_artcompat"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x19

    goto/16 :goto_0

    :sswitch_1a
    const-string v1, "native_loom_profiler_native_tracer"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string v1, "native_loom_profiler_profiler"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1b

    goto/16 :goto_0

    :sswitch_1c
    const-string v1, "native_loom_providers"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1c

    goto/16 :goto_0

    :sswitch_1d
    const-string v1, "native_loom_util_dalvik_util"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1d

    goto/16 :goto_0

    :sswitch_1e
    const-string v1, "native_loom_util_forkjail"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1e

    goto/16 :goto_0

    :sswitch_1f
    const-string v1, "native_loom_util_util"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1f

    goto/16 :goto_0

    :sswitch_20
    const-string v1, "native_loom_yarn_yarn"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x20

    goto/16 :goto_0

    :sswitch_21
    const-string v1, "native_omnistore_client_common_common"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x21

    goto/16 :goto_0

    :sswitch_22
    const-string v1, "native_omnistore_client_common_flatbuffers_flatbuffers-diff"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x22

    goto/16 :goto_0

    :sswitch_23
    const-string v1, "native_omnistore_jni_client_android_java-mqtt-protocol_java-mqtt-protocol"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x23

    goto/16 :goto_0

    :sswitch_24
    const-string v1, "native_omnistore_jni_client_android_jni_jni"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x24

    goto/16 :goto_0

    :sswitch_25
    const-string v1, "native_tigonliger_policy"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x25

    goto/16 :goto_0

    :sswitch_26
    const-string v1, "omnistore"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x26

    goto/16 :goto_0

    :sswitch_27
    const-string v1, "sslx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x27

    goto/16 :goto_0

    :sswitch_28
    const-string v1, "tigon4a"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x28

    goto/16 :goto_0

    :sswitch_29
    const-string v1, "tigonliger"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x29

    goto/16 :goto_0

    :sswitch_2a
    const-string v1, "xplat_CompactDisk_CompactDisk"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x2a

    goto/16 :goto_0

    :sswitch_2b
    const-string v1, "xplat_TigonLiger_TigonLiger"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x2b

    goto/16 :goto_0

    :sswitch_2c
    const-string v1, "xplat_TigonLiger_TigonLigerPolicy"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x2c

    goto/16 :goto_0

    :sswitch_2d
    const-string v1, "xplat_Tigon_Tigon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x2d

    goto/16 :goto_0

    :sswitch_2e
    const-string v1, "xplat_Tigon_TigonBodyProviders"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x2e

    goto/16 :goto_0

    :sswitch_2f
    const-string v1, "xplat_Tigon_TigonCanceller"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x2f

    goto/16 :goto_0

    :sswitch_30
    const-string v1, "xplat_Tigon_TigonIgnoreCancel"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x30

    goto/16 :goto_0

    :sswitch_31
    const-string v1, "xplat_Tigon_TigonPolicies"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x31

    goto/16 :goto_0

    :sswitch_32
    const-string v1, "xplat_Tigon_TigonPrimitives"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x32

    goto/16 :goto_0

    :sswitch_33
    const-string v1, "xplat_Tigon_TigonQueues"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x33

    goto/16 :goto_0

    :sswitch_34
    const-string v1, "xplat_Tigon_TigonRedirector"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x34

    goto/16 :goto_0

    :sswitch_35
    const-string v1, "xplat_Tigon_TigonRetrier"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x35

    goto/16 :goto_0

    :sswitch_36
    const-string v1, "xplat_Tigon_TigonSecretary"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x36

    goto/16 :goto_0

    :sswitch_37
    const-string v1, "xplat_Tigon_TigonStack"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x37

    goto/16 :goto_0

    :sswitch_38
    const-string v1, "xplat_Tigon_TigonSwapper"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x38

    goto/16 :goto_0

    :sswitch_39
    const-string v1, "xplat_Tigon_TigonSwitcher"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x39

    goto/16 :goto_0

    :sswitch_3a
    const-string v1, "xplat_Tigon_TigonTimers"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x3a

    goto/16 :goto_0

    :sswitch_3b
    const-string v1, "xplat_mobileconfig_mobileconfig"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x3b

    goto/16 :goto_0

    :sswitch_3c
    const-string v1, "yajl"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x3c

    goto/16 :goto_0

    .line 4506
    :pswitch_0
    const-string v0, "coldstart"

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7dc7b5a7 -> :sswitch_16
        -0x6d2f432a -> :sswitch_a
        -0x6bd1c3e9 -> :sswitch_32
        -0x693123ac -> :sswitch_2f
        -0x54ff08af -> :sswitch_1d
        -0x4ea0bfa2 -> :sswitch_28
        -0x4b972a86 -> :sswitch_35
        -0x42cd9068 -> :sswitch_2
        -0x42841f60 -> :sswitch_0
        -0x3f83dfbc -> :sswitch_9
        -0x39456bfd -> :sswitch_25
        -0x37e78616 -> :sswitch_21
        -0x361c173a -> :sswitch_17
        -0x3525a4a7 -> :sswitch_f
        -0x3007aeec -> :sswitch_23
        -0x2439a04f -> :sswitch_14
        -0x23ab6977 -> :sswitch_1b
        -0x20b6a029 -> :sswitch_30
        -0x1f21dcd7 -> :sswitch_1f
        -0x19e89eb4 -> :sswitch_39
        -0x16bf9164 -> :sswitch_8
        -0xf277bd7 -> :sswitch_2a
        -0x6ce7b98 -> :sswitch_26
        -0x3a940b1 -> :sswitch_1
        -0x1622e9a -> :sswitch_29
        0x32c6a1 -> :sswitch_13
        0x3603ec -> :sswitch_27
        0x387a4a -> :sswitch_3c
        0x1f18105 -> :sswitch_19
        0x5d17590 -> :sswitch_c
        0x62334b7 -> :sswitch_12
        0x6f9bedf -> :sswitch_38
        0xa55c900 -> :sswitch_24
        0xbc58bbb -> :sswitch_31
        0xc86b14b -> :sswitch_2d
        0x17a02518 -> :sswitch_7
        0x1aebcffb -> :sswitch_10
        0x2224cc03 -> :sswitch_1e
        0x23a0faf3 -> :sswitch_6
        0x27225892 -> :sswitch_5
        0x28ce2a6a -> :sswitch_34
        0x298a6359 -> :sswitch_2c
        0x2d63a347 -> :sswitch_2b
        0x3767c8fd -> :sswitch_d
        0x38af788f -> :sswitch_3
        0x4c448595 -> :sswitch_2e
        0x4e5220ad -> :sswitch_36
        0x4eb5891c -> :sswitch_22
        0x4f4a42ad -> :sswitch_33
        0x53c31af9 -> :sswitch_3a
        0x589201c7 -> :sswitch_3b
        0x5bbcdc69 -> :sswitch_20
        0x619796a7 -> :sswitch_18
        0x65c337fd -> :sswitch_37
        0x694c19ed -> :sswitch_11
        0x6aa9117b -> :sswitch_4
        0x6afd25ff -> :sswitch_e
        0x769bf3d3 -> :sswitch_b
        0x7a8dd0bc -> :sswitch_15
        0x7ac071ac -> :sswitch_1c
        0x7b2ca655 -> :sswitch_1a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 4480
    invoke-static {}, LX/01L;->e()V

    .line 4481
    :try_start_0
    invoke-static {p0}, Ljava/lang/System;->mapLibraryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/01L;->d(Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 4482
    :catch_0
    move-exception v0

    .line 4483
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static declared-synchronized b(Landroid/content/Context;ILX/020;)V
    .locals 9
    .param p2    # LX/020;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 4450
    const-class v3, LX/01L;

    monitor-enter v3

    :try_start_0
    sget-object v2, LX/01L;->c:[LX/022;

    if-nez v2, :cond_8

    .line 4451
    sput p1, LX/01L;->h:I

    .line 4452
    invoke-static {p2}, LX/01L;->a(LX/020;)V

    .line 4453
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 4454
    const-string v2, "LD_LIBRARY_PATH"

    invoke-static {v2}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4455
    if-nez v2, :cond_0

    .line 4456
    const-string v2, "/vendor/lib:/system/lib"

    .line 4457
    :cond_0
    const-string v5, ":"

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    move v2, v0

    .line 4458
    :goto_0
    array-length v6, v5

    if-ge v2, v6, :cond_1

    .line 4459
    new-instance v6, Ljava/io/File;

    aget-object v7, v5, v2

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4460
    new-instance v7, LX/021;

    const/4 v8, 0x2

    invoke-direct {v7, v6, v8}, LX/021;-><init>(Ljava/io/File;I)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4461
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4462
    :cond_1
    if-eqz p0, :cond_2

    .line 4463
    and-int/lit8 v2, p1, 0x1

    if-eqz v2, :cond_3

    .line 4464
    const/4 v0, 0x0

    new-instance v1, LX/0In;

    sget-object v2, LX/01L;->g:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, LX/0In;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 4465
    :cond_2
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LX/022;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/022;

    .line 4466
    invoke-static {}, LX/01L;->c()I

    move-result v4

    .line 4467
    array-length v1, v0

    :goto_2
    add-int/lit8 v2, v1, -0x1

    if-lez v1, :cond_7

    .line 4468
    aget-object v1, v0, v2

    invoke-virtual {v1, v4}, LX/022;->a(I)V

    move v1, v2

    goto :goto_2

    .line 4469
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .line 4470
    iget v2, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_4

    iget v2, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v2, v2, 0x80

    if-nez v2, :cond_4

    move v2, v1

    .line 4471
    :goto_3
    if-eqz v2, :cond_5

    .line 4472
    :goto_4
    const/4 v1, 0x0

    new-instance v2, LX/023;

    sget-object v5, LX/01L;->g:Ljava/lang/String;

    invoke-direct {v2, p0, v5, v0}, LX/023;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-virtual {v4, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 4473
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_4
    move v2, v0

    .line 4474
    goto :goto_3

    .line 4475
    :cond_5
    :try_start_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-gt v2, v6, :cond_6

    move v0, v1

    .line 4476
    :cond_6
    new-instance v2, LX/021;

    new-instance v6, Ljava/io/File;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v6, v0}, LX/021;-><init>(Ljava/io/File;I)V

    .line 4477
    const/4 v0, 0x0

    invoke-virtual {v4, v0, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v1

    goto :goto_4

    .line 4478
    :cond_7
    sput-object v0, LX/01L;->c:[LX/022;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4479
    :cond_8
    monitor-exit v3

    return-void
.end method

.method public static b(Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4425
    sget-object v0, LX/01L;->d:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v3, v1

    .line 4426
    :goto_0
    if-nez v3, :cond_a

    .line 4427
    sget-object v0, LX/01L;->e:Landroid/os/StrictMode$ThreadPolicy;

    if-nez v0, :cond_9

    .line 4428
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    sput-object v0, LX/01L;->e:Landroid/os/StrictMode$ThreadPolicy;

    move v0, v1

    .line 4429
    :goto_1
    sget-boolean v4, LX/01L;->a:Z

    if-eqz v4, :cond_0

    .line 4430
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SoLoader.loadLibrary["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/04t;->a(Ljava/lang/String;)V

    :cond_0
    move v7, v2

    move v2, v3

    move v3, v7

    .line 4431
    :goto_2
    if-nez v2, :cond_2

    :try_start_0
    sget-object v4, LX/01L;->c:[LX/022;

    array-length v4, v4

    if-ge v3, v4, :cond_2

    .line 4432
    sget-object v2, LX/01L;->c:[LX/022;

    aget-object v2, v2, v3

    invoke-virtual {v2, p0, p1}, LX/022;->a(Ljava/lang/String;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 4433
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_2

    :cond_1
    move v3, v2

    .line 4434
    goto :goto_0

    .line 4435
    :cond_2
    sget-boolean v3, LX/01L;->a:Z

    if-eqz v3, :cond_3

    .line 4436
    invoke-static {}, LX/04t;->a()V

    .line 4437
    :cond_3
    if-eqz v0, :cond_8

    .line 4438
    sget-object v0, LX/01L;->e:Landroid/os/StrictMode$ThreadPolicy;

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 4439
    sput-object v6, LX/01L;->e:Landroid/os/StrictMode$ThreadPolicy;

    move v0, v2

    .line 4440
    :goto_3
    if-nez v0, :cond_6

    .line 4441
    new-instance v0, Ljava/lang/UnsatisfiedLinkError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "couldn\'t find DSO to load: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsatisfiedLinkError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4442
    :catchall_0
    move-exception v1

    sget-boolean v2, LX/01L;->a:Z

    if-eqz v2, :cond_4

    .line 4443
    invoke-static {}, LX/04t;->a()V

    .line 4444
    :cond_4
    if-eqz v0, :cond_5

    .line 4445
    sget-object v0, LX/01L;->e:Landroid/os/StrictMode$ThreadPolicy;

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 4446
    sput-object v6, LX/01L;->e:Landroid/os/StrictMode$ThreadPolicy;

    :cond_5
    throw v1

    .line 4447
    :cond_6
    if-ne v0, v1, :cond_7

    .line 4448
    sget-object v0, LX/01L;->d:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4449
    :cond_7
    return-void

    :cond_8
    move v0, v2

    goto :goto_3

    :cond_9
    move v0, v2

    goto :goto_1

    :cond_a
    move v0, v3

    goto :goto_3
.end method

.method private static c()I
    .locals 2

    .prologue
    .line 4421
    const/4 v0, 0x0

    .line 4422
    sget v1, LX/01L;->h:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 4423
    const/4 v0, 0x1

    .line 4424
    :cond_0
    return v0
.end method

.method private static d(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 4415
    const/4 v0, 0x0

    :goto_0
    sget-object v1, LX/01L;->c:[LX/022;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 4416
    sget-object v1, LX/01L;->c:[LX/022;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, LX/022;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 4417
    if-eqz v1, :cond_0

    .line 4418
    return-object v1

    .line 4419
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4420
    :cond_1
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0, p0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static e()V
    .locals 2

    .prologue
    .line 4412
    sget-object v0, LX/01L;->c:[LX/022;

    if-nez v0, :cond_0

    .line 4413
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "SoLoader.init() not yet called"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4414
    :cond_0
    return-void
.end method
