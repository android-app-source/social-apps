.class public final LX/0Cg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Cf;


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/BrowserLiteChrome;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V
    .locals 0

    .prologue
    .line 27434
    iput-object p1, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/browser/lite/BrowserLiteChrome;B)V
    .locals 0

    .prologue
    .line 27433
    invoke-direct {p0, p1}, LX/0Cg;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V

    return-void
.end method

.method private a(I)LX/0Ce;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 27431
    new-instance v3, LX/0Ce;

    .line 27432
    invoke-static {p1}, LX/0Ch;->b(I)I

    move-result v0

    if-eq v0, v5, :cond_0

    move v0, v1

    :goto_0
    iget-object v4, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v4, v4, Lcom/facebook/browser/lite/BrowserLiteChrome;->y:LX/0Ch;

    invoke-virtual {v4, p1}, LX/0Ch;->a(I)I

    move-result v4

    if-eq v4, v5, :cond_1

    :goto_1
    invoke-direct {v3, v0, v1}, LX/0Ce;-><init>(ZZ)V

    return-object v3

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 27423
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-static {v0, p1}, LX/0DQ;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 27424
    invoke-static {v0}, LX/0DQ;->a(Landroid/content/pm/ResolveInfo;)Ljava/lang/String;

    move-result-object v0

    .line 27425
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 27426
    const-string v2, "action"

    const-string v3, "ACTION_OPEN_WITH"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27427
    const-string v2, "destination"

    if-eqz v0, :cond_0

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27428
    invoke-direct {p0, v1}, LX/0Cg;->a(Ljava/util/Map;)V

    .line 27429
    return-void

    .line 27430
    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method private a(Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 27421
    invoke-static {}, LX/0CQ;->a()LX/0CQ;

    move-result-object v0

    iget-object v1, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->C:Landroid/os/Bundle;

    invoke-virtual {v0, p1, v1}, LX/0CQ;->a(Ljava/util/Map;Landroid/os/Bundle;)V

    .line 27422
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 27342
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 27343
    const-string v1, "action"

    const-string v2, "ACTION_GO_BACK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27344
    invoke-direct {p0, v0}, LX/0Cg;->a(Ljava/util/Map;)V

    .line 27345
    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 27413
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-static {v0, p1}, LX/0DQ;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 27414
    invoke-static {v0}, LX/0DQ;->a(Landroid/content/pm/ResolveInfo;)Ljava/lang/String;

    move-result-object v0

    .line 27415
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 27416
    const-string v2, "action"

    const-string v3, "ACTION_LAUNCH_APP"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27417
    const-string v2, "destination"

    if-eqz v0, :cond_0

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27418
    invoke-direct {p0, v1}, LX/0Cg;->a(Ljava/util/Map;)V

    .line 27419
    return-void

    .line 27420
    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 27409
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 27410
    const-string v1, "action"

    const-string v2, "ACTION_GO_FORWARD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27411
    invoke-direct {p0, v0}, LX/0Cg;->a(Ljava/util/Map;)V

    .line 27412
    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 27401
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-static {v0, p1}, LX/0DQ;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 27402
    invoke-static {v0}, LX/0DQ;->a(Landroid/content/pm/ResolveInfo;)Ljava/lang/String;

    move-result-object v0

    .line 27403
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 27404
    const-string v2, "action"

    const-string v3, "ACTION_INSTALL_APP"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27405
    const-string v2, "destination"

    if-eqz v0, :cond_0

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27406
    invoke-direct {p0, v1}, LX/0Cg;->a(Ljava/util/Map;)V

    .line 27407
    return-void

    .line 27408
    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 27400
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    return v0
.end method

.method public final a(LX/0EK;)LX/0Ce;
    .locals 3

    .prologue
    .line 27384
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->B:LX/0Di;

    invoke-virtual {v0}, LX/0Di;->b()V

    .line 27385
    const-string v0, "ZOOM_IN"

    .line 27386
    iget-object v1, p1, LX/0EK;->b:Ljava/lang/String;

    move-object v1, v1

    .line 27387
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27388
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    .line 27389
    iget-object v1, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    invoke-static {v1}, LX/0Ch;->b(I)I

    move-result v1

    .line 27390
    iput v1, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    .line 27391
    :goto_0
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    invoke-static {v0, v1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->setTextZoom(Lcom/facebook/browser/lite/BrowserLiteChrome;I)V

    .line 27392
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    const/4 v1, 0x1

    .line 27393
    iput-boolean v1, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->z:Z

    .line 27394
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->t:LX/0CQ;

    iget-object v1, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    .line 27395
    new-instance v2, LX/0CJ;

    invoke-direct {v2, v0, v1}, LX/0CJ;-><init>(LX/0CQ;I)V

    invoke-static {v0, v2}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 27396
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    invoke-direct {p0, v0}, LX/0Cg;->a(I)LX/0Ce;

    move-result-object v0

    return-object v0

    .line 27397
    :cond_0
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->y:LX/0Ch;

    iget-object v2, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget v2, v2, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    invoke-virtual {v1, v2}, LX/0Ch;->a(I)I

    move-result v1

    .line 27398
    iput v1, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    .line 27399
    goto :goto_0
.end method

.method public onClick(LX/0EK;)V
    .locals 4

    .prologue
    .line 27346
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->B:LX/0Di;

    invoke-virtual {v0}, LX/0Di;->b()V

    .line 27347
    iget-object v0, p1, LX/0EK;->b:Ljava/lang/String;

    move-object v0, v0

    .line 27348
    const-string v1, "ACTION_GO_BACK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27349
    invoke-direct {p0}, LX/0Cg;->b()V

    .line 27350
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->goBack()V

    .line 27351
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->b()Z

    .line 27352
    return-void

    .line 27353
    :cond_1
    const-string v1, "ACTION_GO_FORWARD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 27354
    invoke-direct {p0}, LX/0Cg;->c()V

    .line 27355
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->goForward()V

    goto :goto_0

    .line 27356
    :cond_2
    const-string v1, "ACTION_OPEN_WITH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 27357
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-static {v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->z(Lcom/facebook/browser/lite/BrowserLiteChrome;)Landroid/content/Intent;

    move-result-object v0

    .line 27358
    invoke-direct {p0, v0}, LX/0Cg;->a(Landroid/content/Intent;)V

    .line 27359
    iget-object v1, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-static {v1, v0}, LX/0DQ;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    goto :goto_0

    .line 27360
    :cond_3
    const-string v1, "ACTION_LAUNCH_APP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 27361
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "extra_app_intent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 27362
    invoke-direct {p0, v0}, LX/0Cg;->b(Landroid/content/Intent;)V

    .line 27363
    iget-object v1, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-static {v1, v0}, LX/0DQ;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    goto :goto_0

    .line 27364
    :cond_4
    const-string v1, "CLEAR_DEBUG_OVERLAY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 27365
    sget-boolean v0, LX/0Df;->b:Z

    move v0, v0

    .line 27366
    if-eqz v0, :cond_0

    .line 27367
    invoke-static {}, LX/0Df;->a()LX/0Df;

    move-result-object v0

    .line 27368
    iget-object v1, v0, LX/0Df;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 27369
    iget-object v1, v0, LX/0Df;->c:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 27370
    goto :goto_0

    .line 27371
    :cond_5
    const-string v1, "ACTION_INSTALL_APP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 27372
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "extra_install_intent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 27373
    invoke-direct {p0, v0}, LX/0Cg;->c(Landroid/content/Intent;)V

    .line 27374
    iget-object v1, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-static {v1, v0}, LX/0DQ;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 27375
    :cond_6
    const-string v1, "OPEN_IN_MAIN_PROCESS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 27376
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v2}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 27377
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    const-class v3, Lcom/facebook/browser/lite/BrowserLiteFallbackActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 27378
    iget-object v1, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-static {v1, v0}, LX/0DQ;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 27379
    :cond_7
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 27380
    const-string v2, "action"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27381
    iget-object v0, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    if-eqz v0, :cond_8

    .line 27382
    const-string v0, "url"

    iget-object v2, p0, LX/0Cg;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v2}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27383
    :cond_8
    invoke-direct {p0, v1}, LX/0Cg;->a(Ljava/util/Map;)V

    goto/16 :goto_0
.end method
