.class public final enum LX/0JM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JM;

.field public static final enum COMPLETED:LX/0JM;

.field public static final enum PAUSED:LX/0JM;

.field public static final enum STREAM_SWITCH:LX/0JM;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39367
    new-instance v0, LX/0JM;

    const-string v1, "STREAM_SWITCH"

    const-string v2, "stream_switch"

    invoke-direct {v0, v1, v3, v2}, LX/0JM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JM;->STREAM_SWITCH:LX/0JM;

    .line 39368
    new-instance v0, LX/0JM;

    const-string v1, "PAUSED"

    const-string v2, "paused"

    invoke-direct {v0, v1, v4, v2}, LX/0JM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JM;->PAUSED:LX/0JM;

    .line 39369
    new-instance v0, LX/0JM;

    const-string v1, "COMPLETED"

    const-string v2, "completed"

    invoke-direct {v0, v1, v5, v2}, LX/0JM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JM;->COMPLETED:LX/0JM;

    .line 39370
    const/4 v0, 0x3

    new-array v0, v0, [LX/0JM;

    sget-object v1, LX/0JM;->STREAM_SWITCH:LX/0JM;

    aput-object v1, v0, v3

    sget-object v1, LX/0JM;->PAUSED:LX/0JM;

    aput-object v1, v0, v4

    sget-object v1, LX/0JM;->COMPLETED:LX/0JM;

    aput-object v1, v0, v5

    sput-object v0, LX/0JM;->$VALUES:[LX/0JM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39371
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39372
    iput-object p3, p0, LX/0JM;->value:Ljava/lang/String;

    .line 39373
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JM;
    .locals 1

    .prologue
    .line 39374
    const-class v0, LX/0JM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JM;

    return-object v0
.end method

.method public static values()[LX/0JM;
    .locals 1

    .prologue
    .line 39375
    sget-object v0, LX/0JM;->$VALUES:[LX/0JM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JM;

    return-object v0
.end method
