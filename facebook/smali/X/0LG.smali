.class public final LX/0LG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field public static final a:LX/0LG;


# instance fields
.field public final b:[I

.field private final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 43191
    new-instance v0, LX/0LG;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput v3, v1, v2

    invoke-direct {v0, v1, v3}, LX/0LG;-><init>([II)V

    sput-object v0, LX/0LG;->a:LX/0LG;

    return-void
.end method

.method private constructor <init>([II)V
    .locals 1

    .prologue
    .line 43192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43193
    if-eqz p1, :cond_0

    .line 43194
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, LX/0LG;->b:[I

    .line 43195
    iget-object v0, p0, LX/0LG;->b:[I

    invoke-static {v0}, Ljava/util/Arrays;->sort([I)V

    .line 43196
    :goto_0
    iput p2, p0, LX/0LG;->c:I

    .line 43197
    return-void

    .line 43198
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [I

    iput-object v0, p0, LX/0LG;->b:[I

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43199
    if-ne p0, p1, :cond_1

    .line 43200
    :cond_0
    :goto_0
    return v0

    .line 43201
    :cond_1
    instance-of v2, p1, LX/0LG;

    if-nez v2, :cond_2

    move v0, v1

    .line 43202
    goto :goto_0

    .line 43203
    :cond_2
    check-cast p1, LX/0LG;

    .line 43204
    iget-object v2, p0, LX/0LG;->b:[I

    iget-object v3, p1, LX/0LG;->b:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, LX/0LG;->c:I

    iget v3, p1, LX/0LG;->c:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 43205
    iget v0, p0, LX/0LG;->c:I

    iget-object v1, p0, LX/0LG;->b:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43206
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AudioCapabilities[maxChannelCount="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/0LG;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", supportedEncodings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0LG;->b:[I

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
