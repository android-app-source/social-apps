.class public final enum LX/05J;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/05J;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/05J;

.field public static final enum FBNS:LX/05J;

.field public static final enum FBNS_LITE:LX/05J;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15958
    new-instance v0, LX/05J;

    const-string v1, "FBNS_LITE"

    invoke-direct {v0, v1, v2}, LX/05J;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/05J;->FBNS_LITE:LX/05J;

    .line 15959
    new-instance v0, LX/05J;

    const-string v1, "FBNS"

    invoke-direct {v0, v1, v3}, LX/05J;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/05J;->FBNS:LX/05J;

    .line 15960
    const/4 v0, 0x2

    new-array v0, v0, [LX/05J;

    sget-object v1, LX/05J;->FBNS_LITE:LX/05J;

    aput-object v1, v0, v2

    sget-object v1, LX/05J;->FBNS:LX/05J;

    aput-object v1, v0, v3

    sput-object v0, LX/05J;->$VALUES:[LX/05J;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15961
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/05J;
    .locals 1

    .prologue
    .line 15962
    const-class v0, LX/05J;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/05J;

    return-object v0
.end method

.method public static values()[LX/05J;
    .locals 1

    .prologue
    .line 15963
    sget-object v0, LX/05J;->$VALUES:[LX/05J;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/05J;

    return-object v0
.end method
