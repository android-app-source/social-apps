.class public final LX/01W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/01X;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleReport(LX/009;LX/04Z;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 4846
    iget-object v2, p2, LX/04Z;->fileName:Ljava/io/File;

    .line 4847
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 4848
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Loading file "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4849
    :try_start_0
    invoke-static {p1, p2}, LX/009;->loadAcraCrashReport(LX/009;LX/04Z;)LX/01l;

    move-result-object v1

    .line 4850
    if-eqz v1, :cond_0

    .line 4851
    const-string v4, "ACRA_REPORT_TYPE"

    sget-object v5, LX/01V;->ACRA_CRASH_REPORT:LX/01V;

    invoke-virtual {v5}, LX/01V;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4852
    const-string v4, "ACRA_REPORT_FILENAME"

    invoke-virtual {v1, v4, v3}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4853
    const-string v4, "UPLOADED_BY_PROCESS"

    invoke-virtual {v1, v4, p3}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4854
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Sending file "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4855
    invoke-static {p1, v1}, LX/009;->sendCrashReport(LX/009;LX/01l;)V

    .line 4856
    invoke-static {v2}, LX/009;->deleteFile(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/09b; {:try_start_0 .. :try_end_0} :catch_2

    .line 4857
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 4858
    :catch_0
    move-exception v1

    .line 4859
    sget-object v3, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Failed to send crash reports"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4860
    invoke-static {v2}, LX/009;->deleteFile(Ljava/io/File;)Z

    goto :goto_0

    .line 4861
    :catch_1
    move-exception v1

    .line 4862
    sget-object v4, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Failed to load crash report for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4863
    invoke-static {v2}, LX/009;->deleteFile(Ljava/io/File;)Z

    goto :goto_0

    .line 4864
    :catch_2
    move-exception v1

    .line 4865
    sget-object v2, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to send crash report for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
