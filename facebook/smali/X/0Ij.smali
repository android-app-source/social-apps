.class public LX/0Ij;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/04v;

.field public final d:LX/01o;

.field public final e:Landroid/content/BroadcastReceiver;

.field private f:LX/06y;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38895
    const-class v0, LX/0Ij;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0Ij;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/04v;LX/01o;)V
    .locals 8

    .prologue
    .line 38896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38897
    iput-object p1, p0, LX/0Ij;->b:Landroid/content/Context;

    .line 38898
    iput-object p2, p0, LX/0Ij;->c:LX/04v;

    .line 38899
    iput-object p3, p0, LX/0Ij;->d:LX/01o;

    .line 38900
    invoke-static {p0}, LX/0Ij;->b(LX/0Ij;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 38901
    new-instance v1, LX/06y;

    const-string v2, "fbns_shared_id"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "fbns_shared_secret"

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "fbns_shared_timestamp"

    const-wide v6, 0x7fffffffffffffffL

    invoke-interface {v0, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, LX/06y;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    iput-object v1, p0, LX/0Ij;->f:LX/06y;

    .line 38902
    new-instance v0, LX/0Ii;

    invoke-direct {v0, p0}, LX/0Ii;-><init>(LX/0Ij;)V

    iput-object v0, p0, LX/0Ij;->e:Landroid/content/BroadcastReceiver;

    .line 38903
    return-void
.end method

.method public static b(LX/0Ij;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 38904
    iget-object v0, p0, LX/0Ij;->b:Landroid/content/Context;

    sget-object v1, LX/01p;->d:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0Ij;LX/06y;)V
    .locals 6

    .prologue
    .line 38905
    invoke-virtual {p1}, LX/06y;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38906
    iget-wide v4, p1, LX/06y;->b:J

    move-wide v0, v4

    .line 38907
    iget-object v2, p0, LX/0Ij;->f:LX/06y;

    .line 38908
    iget-wide v4, v2, LX/06y;->b:J

    move-wide v2, v4

    .line 38909
    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 38910
    iput-object p1, p0, LX/0Ij;->f:LX/06y;

    .line 38911
    invoke-static {p0}, LX/0Ij;->b(LX/0Ij;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "fbns_shared_id"

    iget-object v2, p0, LX/0Ij;->f:LX/06y;

    invoke-virtual {v2}, LX/06y;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "fbns_shared_secret"

    iget-object v2, p0, LX/0Ij;->f:LX/06y;

    invoke-virtual {v2}, LX/06y;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "fbns_shared_timestamp"

    iget-object v2, p0, LX/0Ij;->f:LX/06y;

    .line 38912
    iget-wide v4, v2, LX/06y;->b:J

    move-wide v2, v4

    .line 38913
    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 38914
    :cond_0
    return-void
.end method
