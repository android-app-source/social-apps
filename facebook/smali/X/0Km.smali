.class public final LX/0Km;
.super LX/02S;
.source ""


# instance fields
.field public final synthetic a:LX/00u;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/zip/ZipFile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/00u;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42051
    iput-object p1, p0, LX/0Km;->a:LX/00u;

    invoke-direct {p0}, LX/02S;-><init>()V

    .line 42052
    iput-object p2, p0, LX/0Km;->b:Ljava/lang/String;

    .line 42053
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Km;->c:Ljava/util/zip/ZipFile;

    .line 42054
    return-void
.end method

.method public constructor <init>(LX/00u;Ljava/lang/String;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 42055
    iput-object p1, p0, LX/0Km;->a:LX/00u;

    invoke-direct {p0}, LX/02S;-><init>()V

    .line 42056
    iput-object p2, p0, LX/0Km;->b:Ljava/lang/String;

    .line 42057
    new-instance v0, Ljava/util/zip/ZipFile;

    invoke-direct {v0, p3}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/0Km;->c:Ljava/util/zip/ZipFile;

    .line 42058
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 6

    .prologue
    .line 42059
    iget-object v0, p0, LX/0Km;->c:Ljava/util/zip/ZipFile;

    if-eqz v0, :cond_0

    .line 42060
    :try_start_0
    iget-object v0, p0, LX/0Km;->c:Ljava/util/zip/ZipFile;

    invoke-virtual {v0}, Ljava/util/zip/ZipFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42061
    :cond_0
    :goto_0
    return-void

    .line 42062
    :catch_0
    move-exception v0

    .line 42063
    const-string v1, "VoltronModuleManager"

    const-string v2, "failed to close module zip file for module %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/0Km;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final open(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 42064
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/0Km;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 42065
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "assets"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 42066
    iget-object v2, p0, LX/0Km;->c:Ljava/util/zip/ZipFile;

    if-eqz v2, :cond_1

    .line 42067
    iget-object v2, p0, LX/0Km;->c:Ljava/util/zip/ZipFile;

    invoke-virtual {v2, v1}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v2

    .line 42068
    if-eqz v2, :cond_0

    .line 42069
    iget-object v0, p0, LX/0Km;->c:Ljava/util/zip/ZipFile;

    iget-object v2, p0, LX/0Km;->c:Ljava/util/zip/ZipFile;

    invoke-virtual {v2, v1}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v0

    .line 42070
    :goto_0
    return-object v0

    .line 42071
    :cond_0
    const-string v1, "VoltronModuleManager"

    const-string v2, "Unable to find entry in zip file, falling back to apk assets"

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 42072
    :cond_1
    iget-object v1, p0, LX/0Km;->a:LX/00u;

    iget-object v1, v1, LX/00u;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0
.end method
