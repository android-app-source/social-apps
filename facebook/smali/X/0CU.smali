.class public final LX/0CU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0CT;


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/BrowserLiteChrome;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V
    .locals 0

    .prologue
    .line 27265
    iput-object p1, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 27266
    iget-object v0, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->A:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 27267
    :cond_0
    iget-object v0, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->stopLoading()V

    .line 27268
    :goto_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 27269
    const-string v1, "action"

    const-string v2, "STOP_LOADING"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27270
    const-string v1, "url"

    iget-object v2, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v2}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27271
    iget-object v1, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->t:LX/0CQ;

    iget-object v2, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteChrome;->C:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2}, LX/0CQ;->a(Ljava/util/Map;Landroid/os/Bundle;)V

    .line 27272
    return-void

    .line 27273
    :cond_1
    sget-object v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->a:Ljava/lang/String;

    const-string v1, "url is null onStopClicked. Don\'t stop loading."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 9

    .prologue
    .line 27274
    iget-object v0, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    const-wide/16 v7, -0x1

    .line 27275
    iget-wide v5, v0, LX/0D5;->j:J

    cmp-long v5, v5, v7

    if-nez v5, :cond_0

    iget-wide v5, v0, LX/0D5;->h:J

    cmp-long v5, v5, v7

    if-nez v5, :cond_0

    .line 27276
    const/4 v5, 0x1

    iput-boolean v5, v0, LX/0D5;->q:Z

    .line 27277
    :cond_0
    iget-object v0, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "about:blank"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->A:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 27278
    sget-object v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->a:Ljava/lang/String;

    const-string v1, "mWebview#getUrl() return %s, load mLastUrl instead."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v4, v4, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v4}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27279
    iget-object v0, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    iget-object v1, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0D5;->loadUrl(Ljava/lang/String;)V

    .line 27280
    :goto_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 27281
    const-string v1, "action"

    const-string v2, "REFRESH"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27282
    const-string v1, "url"

    iget-object v2, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v2}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27283
    iget-object v1, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->t:LX/0CQ;

    iget-object v2, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteChrome;->C:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2}, LX/0CQ;->a(Ljava/util/Map;Landroid/os/Bundle;)V

    .line 27284
    return-void

    .line 27285
    :cond_2
    iget-object v0, p0, LX/0CU;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->reload()V

    goto :goto_0
.end method
