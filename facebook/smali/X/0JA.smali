.class public final enum LX/0JA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JA;

.field public static final enum ATTEMPT_TO_CONNECT:LX/0JA;

.field public static final enum SHOW_CAST_MENU:LX/0JA;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39245
    new-instance v0, LX/0JA;

    const-string v1, "ATTEMPT_TO_CONNECT"

    const-string v2, "attempt_to_connect"

    invoke-direct {v0, v1, v3, v2}, LX/0JA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JA;->ATTEMPT_TO_CONNECT:LX/0JA;

    .line 39246
    new-instance v0, LX/0JA;

    const-string v1, "SHOW_CAST_MENU"

    const-string v2, "show_cast_menu"

    invoke-direct {v0, v1, v4, v2}, LX/0JA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JA;->SHOW_CAST_MENU:LX/0JA;

    .line 39247
    const/4 v0, 0x2

    new-array v0, v0, [LX/0JA;

    sget-object v1, LX/0JA;->ATTEMPT_TO_CONNECT:LX/0JA;

    aput-object v1, v0, v3

    sget-object v1, LX/0JA;->SHOW_CAST_MENU:LX/0JA;

    aput-object v1, v0, v4

    sput-object v0, LX/0JA;->$VALUES:[LX/0JA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39242
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39243
    iput-object p3, p0, LX/0JA;->value:Ljava/lang/String;

    .line 39244
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JA;
    .locals 1

    .prologue
    .line 39240
    const-class v0, LX/0JA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JA;

    return-object v0
.end method

.method public static values()[LX/0JA;
    .locals 1

    .prologue
    .line 39241
    sget-object v0, LX/0JA;->$VALUES:[LX/0JA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JA;

    return-object v0
.end method
