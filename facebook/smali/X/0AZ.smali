.class public LX/0AZ;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source ""

# interfaces
.implements LX/0Aa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/xml/sax/helpers/DefaultHandler;",
        "LX/0Aa",
        "<",
        "LX/0AY;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lorg/xmlpull/v1/XmlPullParserFactory;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24638
    const-string v0, "(\\d+)(?:/(\\d+))?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0AZ;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24609
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0AZ;-><init>(Ljava/lang/String;)V

    .line 24610
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 24639
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 24640
    iput-object p1, p0, LX/0AZ;->b:Ljava/lang/String;

    .line 24641
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    iput-object v0, p0, LX/0AZ;->c:Lorg/xmlpull/v1/XmlPullParserFactory;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24642
    return-void

    .line 24643
    :catch_0
    move-exception v0

    .line 24644
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t create XmlPullParserFactory instance"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;F)F
    .locals 3

    .prologue
    .line 24645
    const/4 v0, 0x0

    const-string v1, "frameRate"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24646
    if-eqz v0, :cond_0

    .line 24647
    sget-object v1, LX/0AZ;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 24648
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 24649
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 24650
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 24651
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 24652
    int-to-float v1, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    div-float p1, v1, v0

    .line 24653
    :cond_0
    :goto_0
    return p1

    .line 24654
    :cond_1
    int-to-float p1, v1

    goto :goto_0
.end method

.method private static a(II)I
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 24655
    if-ne p0, v0, :cond_1

    move p0, p1

    .line 24656
    :cond_0
    :goto_0
    return p0

    .line 24657
    :cond_1
    if-eq p1, v0, :cond_0

    .line 24658
    if-ne p0, p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0Av;->b(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(LX/0Ah;)I
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v0, -0x1

    .line 24659
    iget-object v2, p0, LX/0Ah;->c:LX/0AR;

    iget-object v2, v2, LX/0AR;->b:Ljava/lang/String;

    .line 24660
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 24661
    :cond_0
    :goto_0
    return v0

    .line 24662
    :cond_1
    invoke-static {v2}, LX/0Al;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 24663
    const/4 v0, 0x0

    goto :goto_0

    .line 24664
    :cond_2
    invoke-static {v2}, LX/0Al;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 24665
    const/4 v0, 0x1

    goto :goto_0

    .line 24666
    :cond_3
    invoke-static {v2}, LX/0Al;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "application/ttml+xml"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    move v0, v1

    .line 24667
    goto :goto_0

    .line 24668
    :cond_5
    const-string v3, "application/mp4"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 24669
    iget-object v2, p0, LX/0Ah;->c:LX/0AR;

    iget-object v2, v2, LX/0AR;->k:Ljava/lang/String;

    .line 24670
    const-string v3, "stpp"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "wvtt"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_6
    move v0, v1

    .line 24671
    goto :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 24672
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24673
    if-nez v0, :cond_0

    :goto_0
    return p2

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;IIFIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/0AR;
    .locals 13

    .prologue
    .line 24674
    new-instance v0, LX/0AR;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move/from16 v12, p11

    invoke-direct/range {v0 .. v12}, LX/0AR;-><init>(Ljava/lang/String;Ljava/lang/String;IIFIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method private static a(JJJJZJJLX/0Ly;Ljava/lang/String;JLjava/util/List;)LX/0AY;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJJZJJ",
            "LX/0Ly;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List",
            "<",
            "LX/0Am;",
            ">;)",
            "LX/0AY;"
        }
    .end annotation

    .prologue
    .line 24675
    new-instance v1, LX/0AY;

    move-wide/from16 v2, p0

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    move/from16 v10, p8

    move-wide/from16 v11, p9

    move-wide/from16 v13, p11

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    move-wide/from16 v17, p15

    move-object/from16 v19, p17

    invoke-direct/range {v1 .. v19}, LX/0AY;-><init>(JJJJZJJLX/0Ly;Ljava/lang/String;JLjava/util/List;)V

    return-object v1
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)LX/0AY;
    .locals 37

    .prologue
    .line 24766
    const-string v6, "availabilityStartTime"

    const-wide/16 v8, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v6, v8, v9}, LX/0AZ;->c(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v6

    .line 24767
    const-string v8, "availabilityEndTime"

    const-wide/16 v10, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v8, v10, v11}, LX/0AZ;->c(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v8

    .line 24768
    const-string v10, "mediaPresentationDuration"

    const-wide/16 v12, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v10, v12, v13}, LX/0AZ;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v30

    .line 24769
    const-string v10, "minBufferTime"

    const-wide/16 v12, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v10, v12, v13}, LX/0AZ;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v12

    .line 24770
    const/4 v10, 0x0

    const-string v11, "type"

    move-object/from16 v0, p1

    invoke-interface {v0, v10, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 24771
    if-eqz v10, :cond_0

    const-string v11, "dynamic"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    .line 24772
    :goto_0
    if-eqz v14, :cond_1

    const-string v10, "minimumUpdatePeriod"

    const-wide/16 v16, -0x1

    move-object/from16 v0, p1

    move-wide/from16 v1, v16

    invoke-static {v0, v10, v1, v2}, LX/0AZ;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v15

    .line 24773
    :goto_1
    if-eqz v14, :cond_2

    const-string v10, "timeShiftBufferDepth"

    const-wide/16 v18, -0x1

    move-object/from16 v0, p1

    move-wide/from16 v1, v18

    invoke-static {v0, v10, v1, v2}, LX/0AZ;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v17

    .line 24774
    :goto_2
    const-string v10, "publishFrameTime"

    const-wide/16 v20, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v20

    invoke-static {v0, v10, v1, v2}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v21

    .line 24775
    const/16 v27, 0x0

    .line 24776
    const/16 v26, 0x0

    .line 24777
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 24778
    if-eqz v14, :cond_3

    const-wide/16 v10, -0x1

    .line 24779
    :goto_3
    const/16 v20, 0x0

    .line 24780
    const/16 v19, 0x0

    move-wide/from16 v24, v10

    move-object/from16 v28, p2

    .line 24781
    :goto_4
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 24782
    const-string v10, "BaseURL"

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 24783
    if-nez v19, :cond_9

    .line 24784
    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 24785
    const/4 v10, 0x1

    move-object/from16 v19, v27

    move-object/from16 v36, v26

    move-object/from16 v26, v11

    move/from16 v11, v20

    move-object/from16 v20, v36

    .line 24786
    :goto_5
    const-string v27, "MPD"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/0Af;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_d

    .line 24787
    const-wide/16 v10, -0x1

    cmp-long v10, v30, v10

    if-nez v10, :cond_c

    .line 24788
    const-wide/16 v10, -0x1

    cmp-long v10, v24, v10

    if-eqz v10, :cond_c

    move-wide/from16 v10, v24

    .line 24789
    :goto_6
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->isEmpty()Z

    move-result v24

    if-eqz v24, :cond_b

    .line 24790
    new-instance v6, LX/0L6;

    const-string v7, "No periods found."

    invoke-direct {v6, v7}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v6

    .line 24791
    :cond_0
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 24792
    :cond_1
    const-wide/16 v15, -0x1

    goto :goto_1

    .line 24793
    :cond_2
    const-wide/16 v17, -0x1

    goto :goto_2

    .line 24794
    :cond_3
    const-wide/16 v10, 0x0

    goto :goto_3

    .line 24795
    :cond_4
    const-string v10, "UTCTiming"

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 24796
    invoke-static/range {p1 .. p1}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;)LX/0Ly;

    move-result-object v10

    move/from16 v11, v20

    move-object/from16 v20, v26

    move-object/from16 v26, v28

    move-object/from16 v36, v10

    move/from16 v10, v19

    move-object/from16 v19, v36

    goto :goto_5

    .line 24797
    :cond_5
    const-string v10, "Location"

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 24798
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v10

    move/from16 v11, v20

    move-object/from16 v26, v28

    move-object/from16 v20, v10

    move/from16 v10, v19

    move-object/from16 v19, v27

    goto :goto_5

    .line 24799
    :cond_6
    const-string v10, "Period"

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    if-nez v20, :cond_9

    .line 24800
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v28

    move-wide/from16 v3, v24

    invoke-direct {v0, v1, v2, v3, v4}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)Landroid/util/Pair;

    move-result-object v11

    .line 24801
    iget-object v10, v11, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, LX/0Am;

    .line 24802
    iget-wide v0, v10, LX/0Am;->b:J

    move-wide/from16 v32, v0

    const-wide/16 v34, -0x1

    cmp-long v29, v32, v34

    if-nez v29, :cond_8

    .line 24803
    if-eqz v14, :cond_7

    .line 24804
    const/4 v10, 0x1

    move v11, v10

    move-object/from16 v20, v26

    move/from16 v10, v19

    move-object/from16 v26, v28

    move-object/from16 v19, v27

    goto/16 :goto_5

    .line 24805
    :cond_7
    new-instance v6, LX/0L6;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unable to determine start of period "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v6

    .line 24806
    :cond_8
    iget-object v11, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    .line 24807
    const-wide/16 v32, -0x1

    cmp-long v11, v24, v32

    if-nez v11, :cond_a

    const-wide/16 v24, -0x1

    .line 24808
    :goto_7
    move-object/from16 v0, v23

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    move/from16 v10, v19

    move/from16 v11, v20

    move-object/from16 v20, v26

    move-object/from16 v19, v27

    move-object/from16 v26, v28

    goto/16 :goto_5

    .line 24809
    :cond_a
    iget-wide v0, v10, LX/0Am;->b:J

    move-wide/from16 v32, v0

    add-long v24, v24, v32

    goto :goto_7

    .line 24810
    :cond_b
    invoke-static/range {v6 .. v23}, LX/0AZ;->a(JJJJZJJLX/0Ly;Ljava/lang/String;JLjava/util/List;)LX/0AY;

    move-result-object v6

    return-object v6

    :cond_c
    move-wide/from16 v10, v30

    goto/16 :goto_6

    :cond_d
    move-object/from16 v27, v19

    move-object/from16 v28, v26

    move-object/from16 v26, v20

    move/from16 v19, v10

    move/from16 v20, v11

    goto/16 :goto_4
.end method

.method private static a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIFIILjava/lang/String;LX/0Ag;LX/0As;)LX/0Ah;
    .locals 20

    .prologue
    .line 24676
    const/4 v3, 0x0

    const-string v4, "id"

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 24677
    const-string v4, "bandwidth"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/0AZ;->e(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    move-result v10

    .line 24678
    const-string v4, "mimeType"

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v4, v1}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 24679
    const-string v5, "codecs"

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-static {v0, v5, v1}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 24680
    const-string v5, "width"

    move-object/from16 v0, p1

    move/from16 v1, p5

    invoke-static {v0, v5, v1}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v5

    .line 24681
    const-string v6, "height"

    move-object/from16 v0, p1

    move/from16 v1, p6

    invoke-static {v0, v6, v1}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v6

    .line 24682
    move-object/from16 v0, p1

    move/from16 v1, p7

    invoke-static {v0, v1}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;F)F

    move-result v7

    .line 24683
    const-string v8, "audioSamplingRate"

    move-object/from16 v0, p1

    move/from16 v1, p9

    invoke-static {v0, v8, v1}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v9

    .line 24684
    const-string v8, "FBQualityLabel"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v8, v11}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 24685
    const-string v8, "FBDefaultQuality"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v8, v11}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v8

    const/4 v11, 0x1

    if-ne v8, v11, :cond_0

    const/4 v14, 0x1

    .line 24686
    :goto_0
    const/4 v15, 0x0

    .line 24687
    const/4 v11, 0x0

    move/from16 v16, p8

    move-object/from16 v8, p11

    move-object/from16 v17, p2

    .line 24688
    :goto_1
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 24689
    const-string v18, "BaseURL"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 24690
    if-nez v11, :cond_9

    .line 24691
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 24692
    const/4 v11, 0x1

    move/from16 v19, v16

    move-object/from16 v16, v8

    move/from16 v8, v19

    .line 24693
    :goto_2
    const-string v18, "Representation"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/0Af;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_a

    move-object/from16 v11, p10

    .line 24694
    invoke-static/range {v3 .. v14}, LX/0AZ;->a(Ljava/lang/String;Ljava/lang/String;IIFIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/0AR;

    move-result-object v3

    .line 24695
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0AZ;->b:Ljava/lang/String;

    const/4 v5, -0x1

    if-eqz v16, :cond_8

    :goto_3
    move-object/from16 v0, v16

    invoke-static {v4, v5, v3, v0, v15}, LX/0AZ;->a(Ljava/lang/String;ILX/0AR;LX/0Ag;Ljava/lang/String;)LX/0Ah;

    move-result-object v3

    return-object v3

    .line 24696
    :cond_0
    const/4 v14, 0x0

    goto :goto_0

    .line 24697
    :cond_1
    const-string v18, "AudioChannelConfiguration"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 24698
    invoke-static/range {p1 .. p1}, LX/0AZ;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v16

    move/from16 v19, v16

    move-object/from16 v16, v8

    move/from16 v8, v19

    goto :goto_2

    .line 24699
    :cond_2
    const-string v18, "SegmentBase"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 24700
    check-cast v8, LX/0Aj;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2, v8}, LX/0AZ;->a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Aj;)LX/0Aj;

    move-result-object v8

    move/from16 v19, v16

    move-object/from16 v16, v8

    move/from16 v8, v19

    goto :goto_2

    .line 24701
    :cond_3
    const-string v18, "SegmentList"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 24702
    check-cast v8, LX/0BU;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2, v8}, LX/0AZ;->a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0BU;)LX/0BU;

    move-result-object v8

    move/from16 v19, v16

    move-object/from16 v16, v8

    move/from16 v8, v19

    goto :goto_2

    .line 24703
    :cond_4
    const-string v18, "SegmentTemplate"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 24704
    check-cast v8, LX/0Lw;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2, v8}, LX/0AZ;->a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Lw;)LX/0Lw;

    move-result-object v8

    move/from16 v19, v16

    move-object/from16 v16, v8

    move/from16 v8, v19

    goto/16 :goto_2

    .line 24705
    :cond_5
    const-string v18, "ContentProtection"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 24706
    invoke-static/range {p1 .. p1}, LX/0AZ;->c(Lorg/xmlpull/v1/XmlPullParser;)LX/0Lu;

    move-result-object v18

    .line 24707
    if-eqz v18, :cond_6

    .line 24708
    move-object/from16 v0, p12

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/0As;->a(LX/0Lu;)V

    :cond_6
    move/from16 v19, v16

    move-object/from16 v16, v8

    move/from16 v8, v19

    .line 24709
    goto/16 :goto_2

    :cond_7
    const-string v18, "FBInitializationBinary"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 24710
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v15

    move/from16 v19, v16

    move-object/from16 v16, v8

    move/from16 v8, v19

    goto/16 :goto_2

    .line 24711
    :cond_8
    new-instance v16, LX/0Aj;

    invoke-direct/range {v16 .. v17}, LX/0Aj;-><init>(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_9
    move/from16 v19, v16

    move-object/from16 v16, v8

    move/from16 v8, v19

    goto/16 :goto_2

    :cond_a
    move/from16 v19, v8

    move-object/from16 v8, v16

    move/from16 v16, v19

    goto/16 :goto_1
.end method

.method private static a(Ljava/lang/String;ILX/0AR;LX/0Ag;Ljava/lang/String;)LX/0Ah;
    .locals 7

    .prologue
    .line 24811
    int-to-long v2, p1

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-static/range {v1 .. v6}, LX/0Ah;->a(Ljava/lang/String;JLX/0AR;LX/0Ag;Ljava/lang/String;)LX/0Ah;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Aj;)LX/0Aj;
    .locals 16

    .prologue
    .line 24812
    const-string v4, "timescale"

    if-eqz p3, :cond_2

    move-object/from16 v0, p3

    iget-wide v2, v0, LX/0Ag;->b:J

    :goto_0
    move-object/from16 v0, p1

    invoke-static {v0, v4, v2, v3}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v3

    .line 24813
    const-string v2, "presentationTimeOffset"

    if-eqz p3, :cond_3

    move-object/from16 v0, p3

    iget-wide v6, v0, LX/0Ag;->c:J

    :goto_1
    move-object/from16 v0, p1

    invoke-static {v0, v2, v6, v7}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v5

    .line 24814
    if-eqz p3, :cond_4

    move-object/from16 v0, p3

    iget-wide v10, v0, LX/0Aj;->e:J

    .line 24815
    :goto_2
    if-eqz p3, :cond_5

    move-object/from16 v0, p3

    iget-wide v8, v0, LX/0Aj;->f:J

    .line 24816
    :goto_3
    const/4 v2, 0x0

    const-string v7, "indexRange"

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 24817
    if-eqz v2, :cond_7

    .line 24818
    const-string v7, "-"

    invoke-virtual {v2, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 24819
    const/4 v7, 0x0

    aget-object v7, v2, v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 24820
    const/4 v7, 0x1

    aget-object v2, v2, v7

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    sub-long/2addr v10, v8

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    .line 24821
    :goto_4
    if-eqz p3, :cond_6

    move-object/from16 v0, p3

    iget-object v2, v0, LX/0Ag;->a:LX/0Au;

    .line 24822
    :cond_0
    :goto_5
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 24823
    const-string v7, "Initialization"

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 24824
    invoke-direct/range {p0 .. p2}, LX/0AZ;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)LX/0Au;

    move-result-object v2

    .line 24825
    :cond_1
    const-string v7, "SegmentBase"

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/0Af;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    move-object/from16 v7, p2

    .line 24826
    invoke-static/range {v2 .. v11}, LX/0AZ;->a(LX/0Au;JJLjava/lang/String;JJ)LX/0Aj;

    move-result-object v2

    return-object v2

    .line 24827
    :cond_2
    const-wide/16 v2, 0x1

    goto :goto_0

    .line 24828
    :cond_3
    const-wide/16 v6, 0x0

    goto :goto_1

    .line 24829
    :cond_4
    const-wide/16 v10, 0x0

    goto :goto_2

    .line 24830
    :cond_5
    const-wide/16 v8, -0x1

    goto :goto_3

    .line 24831
    :cond_6
    const/4 v2, 0x0

    goto :goto_5

    :cond_7
    move-wide v14, v8

    move-wide v8, v10

    move-wide v10, v14

    goto :goto_4
.end method

.method private static a(LX/0Au;JJLjava/lang/String;JJ)LX/0Aj;
    .locals 11

    .prologue
    .line 24765
    new-instance v0, LX/0Aj;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object/from16 v6, p5

    move-wide/from16 v7, p6

    move-wide/from16 v9, p8

    invoke-direct/range {v0 .. v10}, LX/0Aj;-><init>(LX/0Au;JJLjava/lang/String;JJ)V

    return-object v0
.end method

.method private static a(IILjava/util/List;Ljava/util/List;)LX/0Ak;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LX/0Ah;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/0Lu;",
            ">;)",
            "LX/0Ak;"
        }
    .end annotation

    .prologue
    .line 24832
    new-instance v0, LX/0Ak;

    invoke-direct {v0, p0, p1, p2, p3}, LX/0Ak;-><init>(IILjava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method private static a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Ag;)LX/0Ak;
    .locals 19

    .prologue
    .line 24724
    const-string v2, "id"

    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v17

    .line 24725
    invoke-static/range {p1 .. p1}, LX/0AZ;->b(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v16

    .line 24726
    const/4 v2, 0x0

    const-string v3, "mimeType"

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 24727
    const/4 v2, 0x0

    const-string v3, "codecs"

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 24728
    const-string v2, "width"

    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v7

    .line 24729
    const-string v2, "height"

    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v8

    .line 24730
    const/high16 v2, -0x40800000    # -1.0f

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;F)F

    move-result v9

    .line 24731
    const/4 v10, -0x1

    .line 24732
    const-string v2, "audioSamplingRate"

    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v11

    .line 24733
    const/4 v2, 0x0

    const-string v3, "lang"

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 24734
    new-instance v14, LX/0As;

    invoke-direct {v14}, LX/0As;-><init>()V

    .line 24735
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 24736
    const/4 v15, 0x0

    move-object/from16 v13, p3

    move-object/from16 v4, p2

    .line 24737
    :goto_0
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 24738
    const-string v2, "BaseURL"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 24739
    if-nez v15, :cond_9

    .line 24740
    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 24741
    const/4 v2, 0x1

    move/from16 v3, v16

    .line 24742
    :goto_1
    const-string v15, "AdaptationSet"

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/0Af;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 24743
    invoke-virtual {v14}, LX/0As;->b()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v3, v1, v2}, LX/0AZ;->a(IILjava/util/List;Ljava/util/List;)LX/0Ak;

    move-result-object v2

    return-object v2

    .line 24744
    :cond_0
    const-string v2, "ContentProtection"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 24745
    invoke-static/range {p1 .. p1}, LX/0AZ;->c(Lorg/xmlpull/v1/XmlPullParser;)LX/0Lu;

    move-result-object v2

    .line 24746
    if-eqz v2, :cond_1

    .line 24747
    invoke-virtual {v14, v2}, LX/0As;->a(LX/0Lu;)V

    :cond_1
    move v2, v15

    move/from16 v3, v16

    .line 24748
    goto :goto_1

    :cond_2
    const-string v2, "ContentComponent"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 24749
    const/4 v2, 0x0

    const-string v3, "lang"

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v2}, LX/0AZ;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 24750
    invoke-static/range {p1 .. p1}, LX/0AZ;->b(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v2

    move/from16 v0, v16

    invoke-static {v0, v2}, LX/0AZ;->a(II)I

    move-result v2

    move v3, v2

    move v2, v15

    goto :goto_1

    .line 24751
    :cond_3
    const-string v2, "Representation"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 24752
    invoke-static/range {v2 .. v14}, LX/0AZ;->a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIFIILjava/lang/String;LX/0Ag;LX/0As;)LX/0Ah;

    move-result-object v3

    .line 24753
    invoke-virtual {v14}, LX/0As;->a()V

    .line 24754
    invoke-static {v3}, LX/0AZ;->a(LX/0Ah;)I

    move-result v2

    move/from16 v0, v16

    invoke-static {v0, v2}, LX/0AZ;->a(II)I

    move-result v2

    .line 24755
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v3, v2

    move v2, v15

    .line 24756
    goto :goto_1

    :cond_4
    const-string v2, "AudioChannelConfiguration"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 24757
    invoke-static/range {p1 .. p1}, LX/0AZ;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v10

    move v2, v15

    move/from16 v3, v16

    goto/16 :goto_1

    .line 24758
    :cond_5
    const-string v2, "SegmentBase"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 24759
    check-cast v13, LX/0Aj;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v13}, LX/0AZ;->a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Aj;)LX/0Aj;

    move-result-object v13

    move v2, v15

    move/from16 v3, v16

    goto/16 :goto_1

    .line 24760
    :cond_6
    const-string v2, "SegmentList"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 24761
    check-cast v13, LX/0BU;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v13}, LX/0AZ;->a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0BU;)LX/0BU;

    move-result-object v13

    move v2, v15

    move/from16 v3, v16

    goto/16 :goto_1

    .line 24762
    :cond_7
    const-string v2, "SegmentTemplate"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 24763
    check-cast v13, LX/0Lw;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v13}, LX/0AZ;->a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Lw;)LX/0Lw;

    move-result-object v13

    move v2, v15

    move/from16 v3, v16

    goto/16 :goto_1

    .line 24764
    :cond_8
    invoke-static/range {p1 .. p1}, LX/0Af;->a(Lorg/xmlpull/v1/XmlPullParser;)Z

    :cond_9
    move v2, v15

    move/from16 v3, v16

    goto/16 :goto_1

    :cond_a
    move v15, v2

    move/from16 v16, v3

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;JJ)LX/0Au;
    .locals 8

    .prologue
    .line 24723
    new-instance v1, LX/0Au;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, LX/0Au;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    return-object v1
.end method

.method public static a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0Au;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 24713
    invoke-interface {p0, v0, p2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 24714
    const-wide/16 v2, 0x0

    .line 24715
    const-wide/16 v4, -0x1

    .line 24716
    invoke-interface {p0, v0, p3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24717
    if-eqz v0, :cond_0

    .line 24718
    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 24719
    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 24720
    array-length v6, v0

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 24721
    const/4 v4, 0x1

    aget-object v0, v0, v4

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v4, v2

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    :cond_0
    move-object v0, p1

    .line 24722
    invoke-static/range {v0 .. v5}, LX/0AZ;->a(Ljava/lang/String;Ljava/lang/String;JJ)LX/0Au;

    move-result-object v0

    return-object v0
.end method

.method private static a(JJLjava/lang/String;)LX/0BT;
    .locals 8

    .prologue
    .line 24712
    new-instance v1, LX/0BT;

    move-wide v2, p0

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, LX/0BT;-><init>(JJLjava/lang/String;)V

    return-object v1
.end method

.method private static a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0BU;)LX/0BU;
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 24611
    const-string v2, "timescale"

    if-eqz p3, :cond_2

    iget-wide v0, p3, LX/0Ag;->b:J

    :goto_0
    invoke-static {p1, v2, v0, v1}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v2

    .line 24612
    const-string v4, "presentationTimeOffset"

    if-eqz p3, :cond_3

    iget-wide v0, p3, LX/0Ag;->c:J

    :goto_1
    invoke-static {p1, v4, v0, v1}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v4

    .line 24613
    const-string v6, "duration"

    if-eqz p3, :cond_4

    iget-wide v0, p3, LX/0BV;->e:J

    :goto_2
    invoke-static {p1, v6, v0, v1}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v7

    .line 24614
    const-string v1, "startNumber"

    if-eqz p3, :cond_5

    iget v0, p3, LX/0BV;->d:I

    :goto_3
    invoke-static {p1, v1, v0}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v6

    move-object v0, v9

    move-object v1, v9

    .line 24615
    :cond_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 24616
    const-string v10, "Initialization"

    invoke-static {p1, v10}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 24617
    invoke-direct {p0, p1, p2}, LX/0AZ;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)LX/0Au;

    move-result-object v9

    .line 24618
    :cond_1
    :goto_4
    const-string v10, "SegmentList"

    invoke-static {p1, v10}, LX/0Af;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 24619
    if-eqz p3, :cond_c

    .line 24620
    if-eqz v9, :cond_9

    move-object v10, v9

    .line 24621
    :goto_5
    if-eqz v1, :cond_a

    move-object v9, v1

    .line 24622
    :goto_6
    if-eqz v0, :cond_b

    :goto_7
    move-object v1, v10

    move-object v10, v0

    .line 24623
    :goto_8
    invoke-static/range {v1 .. v10}, LX/0AZ;->a(LX/0Au;JJIJLjava/util/List;Ljava/util/List;)LX/0BU;

    move-result-object v0

    return-object v0

    .line 24624
    :cond_2
    const-wide/16 v0, 0x1

    goto :goto_0

    .line 24625
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 24626
    :cond_4
    const-wide/16 v0, -0x1

    goto :goto_2

    .line 24627
    :cond_5
    const/4 v0, 0x1

    goto :goto_3

    .line 24628
    :cond_6
    const-string v10, "SegmentTimeline"

    invoke-static {p1, v10}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 24629
    invoke-static {p1}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/List;

    move-result-object v1

    goto :goto_4

    .line 24630
    :cond_7
    const-string v10, "SegmentURL"

    invoke-static {p1, v10}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 24631
    if-nez v0, :cond_8

    .line 24632
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 24633
    :cond_8
    const-string v10, "media"

    const-string v11, "mediaRange"

    invoke-static {p1, p2, v10, v11}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0Au;

    move-result-object v10

    move-object v10, v10

    .line 24634
    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 24635
    :cond_9
    iget-object v10, p3, LX/0Ag;->a:LX/0Au;

    goto :goto_5

    .line 24636
    :cond_a
    iget-object v9, p3, LX/0BV;->f:Ljava/util/List;

    goto :goto_6

    .line 24637
    :cond_b
    iget-object v0, p3, LX/0BU;->g:Ljava/util/List;

    goto :goto_7

    :cond_c
    move-object v10, v0

    move-object v11, v1

    move-object v1, v9

    move-object v9, v11

    goto :goto_8
.end method

.method private static a(LX/0Au;JJIJLjava/util/List;Ljava/util/List;)LX/0BU;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Au;",
            "JJIJ",
            "Ljava/util/List",
            "<",
            "LX/0BT;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/0Au;",
            ">;)",
            "LX/0BU;"
        }
    .end annotation

    .prologue
    .line 24481
    new-instance v0, LX/0BU;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move/from16 v6, p5

    move-wide/from16 v7, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, LX/0BU;-><init>(LX/0Au;JJIJLjava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method private static a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Lw;)LX/0Lw;
    .locals 15

    .prologue
    .line 24483
    const-string v4, "timescale"

    if-eqz p3, :cond_2

    move-object/from16 v0, p3

    iget-wide v2, v0, LX/0Ag;->b:J

    :goto_0
    move-object/from16 v0, p1

    invoke-static {v0, v4, v2, v3}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v4

    .line 24484
    const-string v6, "presentationTimeOffset"

    if-eqz p3, :cond_3

    move-object/from16 v0, p3

    iget-wide v2, v0, LX/0Ag;->c:J

    :goto_1
    move-object/from16 v0, p1

    invoke-static {v0, v6, v2, v3}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v6

    .line 24485
    const-string v8, "duration"

    if-eqz p3, :cond_4

    move-object/from16 v0, p3

    iget-wide v2, v0, LX/0BV;->e:J

    :goto_2
    move-object/from16 v0, p1

    invoke-static {v0, v8, v2, v3}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v9

    .line 24486
    const-string v3, "startNumber"

    if-eqz p3, :cond_5

    move-object/from16 v0, p3

    iget v2, v0, LX/0BV;->d:I

    :goto_3
    move-object/from16 v0, p1

    invoke-static {v0, v3, v2}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v8

    .line 24487
    const-string v3, "media"

    if-eqz p3, :cond_6

    move-object/from16 v0, p3

    iget-object v2, v0, LX/0Lw;->h:LX/0Lx;

    :goto_4
    move-object/from16 v0, p1

    invoke-static {v0, v3, v2}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Lx;)LX/0Lx;

    move-result-object v13

    .line 24488
    const-string v3, "initialization"

    if-eqz p3, :cond_7

    move-object/from16 v0, p3

    iget-object v2, v0, LX/0Lw;->g:LX/0Lx;

    :goto_5
    move-object/from16 v0, p1

    invoke-static {v0, v3, v2}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Lx;)LX/0Lx;

    move-result-object v12

    .line 24489
    const/4 v3, 0x0

    .line 24490
    const/4 v2, 0x0

    .line 24491
    :cond_0
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 24492
    const-string v11, "Initialization"

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 24493
    invoke-direct/range {p0 .. p2}, LX/0AZ;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)LX/0Au;

    move-result-object v3

    .line 24494
    :cond_1
    :goto_6
    const-string v11, "SegmentTemplate"

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/0Af;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 24495
    if-eqz p3, :cond_b

    .line 24496
    if-eqz v3, :cond_9

    .line 24497
    :goto_7
    if-eqz v2, :cond_a

    :goto_8
    move-object v11, v2

    :goto_9
    move-object/from16 v14, p2

    .line 24498
    invoke-static/range {v3 .. v14}, LX/0AZ;->a(LX/0Au;JJIJLjava/util/List;LX/0Lx;LX/0Lx;Ljava/lang/String;)LX/0Lw;

    move-result-object v2

    return-object v2

    .line 24499
    :cond_2
    const-wide/16 v2, 0x1

    goto :goto_0

    .line 24500
    :cond_3
    const-wide/16 v2, 0x0

    goto :goto_1

    .line 24501
    :cond_4
    const-wide/16 v2, -0x1

    goto :goto_2

    .line 24502
    :cond_5
    const/4 v2, 0x1

    goto :goto_3

    .line 24503
    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    .line 24504
    :cond_7
    const/4 v2, 0x0

    goto :goto_5

    .line 24505
    :cond_8
    const-string v11, "SegmentTimeline"

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 24506
    invoke-static/range {p1 .. p1}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/List;

    move-result-object v2

    goto :goto_6

    .line 24507
    :cond_9
    move-object/from16 v0, p3

    iget-object v3, v0, LX/0Ag;->a:LX/0Au;

    goto :goto_7

    .line 24508
    :cond_a
    move-object/from16 v0, p3

    iget-object v2, v0, LX/0BV;->f:Ljava/util/List;

    goto :goto_8

    :cond_b
    move-object v11, v2

    goto :goto_9
.end method

.method private static a(LX/0Au;JJIJLjava/util/List;LX/0Lx;LX/0Lx;Ljava/lang/String;)LX/0Lw;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Au;",
            "JJIJ",
            "Ljava/util/List",
            "<",
            "LX/0BT;",
            ">;",
            "LX/0Lx;",
            "LX/0Lx;",
            "Ljava/lang/String;",
            ")",
            "LX/0Lw;"
        }
    .end annotation

    .prologue
    .line 24482
    new-instance v0, LX/0Lw;

    move-object v1, p0

    move-wide v2, p1

    move-wide/from16 v4, p3

    move/from16 v6, p5

    move-wide/from16 v7, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, LX/0Lw;-><init>(LX/0Au;JJIJLjava/util/List;LX/0Lx;LX/0Lx;Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Lx;)LX/0Lx;
    .locals 3

    .prologue
    .line 24509
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24510
    if-eqz v0, :cond_0

    .line 24511
    const/4 p0, 0x4

    .line 24512
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    .line 24513
    new-array v2, p0, [I

    .line 24514
    new-array p0, p0, [Ljava/lang/String;

    .line 24515
    invoke-static {v0, v1, v2, p0}, LX/0Lx;->a(Ljava/lang/String;[Ljava/lang/String;[I[Ljava/lang/String;)I

    move-result p1

    .line 24516
    new-instance p2, LX/0Lx;

    invoke-direct {p2, v1, v2, p0, p1}, LX/0Lx;-><init>([Ljava/lang/String;[I[Ljava/lang/String;I)V

    move-object p2, p2

    .line 24517
    :cond_0
    return-object p2
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;)LX/0Ly;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24518
    const-string v0, "schemeIdUri"

    invoke-interface {p0, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24519
    const-string v1, "value"

    invoke-interface {p0, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 24520
    new-instance v2, LX/0Ly;

    invoke-direct {v2, v0, v1}, LX/0Ly;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 24521
    return-object v0
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)Landroid/util/Pair;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Ljava/lang/String;",
            "J)",
            "Landroid/util/Pair",
            "<",
            "LX/0Am;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 24522
    const-string v0, "id"

    invoke-interface {p1, v3, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 24523
    const-string v0, "start"

    invoke-static {p1, v0, p3, p4}, LX/0AZ;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v6

    .line 24524
    const-string v0, "duration"

    const-wide/16 v8, -0x1

    invoke-static {p1, v0, v8, v9}, LX/0AZ;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v8

    .line 24525
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 24526
    const/4 v0, 0x0

    move-object v1, v3

    move-object v2, p2

    .line 24527
    :cond_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 24528
    const-string v10, "BaseURL"

    invoke-static {p1, v10}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 24529
    if-nez v0, :cond_1

    .line 24530
    invoke-static {p1, v2}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 24531
    const/4 v0, 0x1

    .line 24532
    :cond_1
    :goto_0
    const-string v10, "Period"

    invoke-static {p1, v10}, LX/0Af;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 24533
    new-instance v0, LX/0Am;

    invoke-direct {v0, v4, v6, v7, v5}, LX/0Am;-><init>(Ljava/lang/String;JLjava/util/List;)V

    move-object v0, v0

    .line 24534
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 24535
    :cond_2
    const-string v10, "AdaptationSet"

    invoke-static {p1, v10}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 24536
    invoke-static {p0, p1, v2, v1}, LX/0AZ;->a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Ag;)LX/0Ak;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 24537
    :cond_3
    const-string v10, "SegmentBase"

    invoke-static {p1, v10}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 24538
    invoke-static {p0, p1, v2, v3}, LX/0AZ;->a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Aj;)LX/0Aj;

    move-result-object v1

    goto :goto_0

    .line 24539
    :cond_4
    const-string v10, "SegmentList"

    invoke-static {p1, v10}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 24540
    invoke-static {p0, p1, v2, v3}, LX/0AZ;->a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0BU;)LX/0BU;

    move-result-object v1

    goto :goto_0

    .line 24541
    :cond_5
    const-string v10, "SegmentTemplate"

    invoke-static {p1, v10}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 24542
    invoke-static {p0, p1, v2, v3}, LX/0AZ;->a(LX/0AZ;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;LX/0Lw;)LX/0Lw;

    move-result-object v1

    goto :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 24543
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24544
    if-nez v0, :cond_0

    :goto_0
    return-object p2

    :cond_0
    move-object p2, v0

    goto :goto_0
.end method

.method private static b(Lorg/xmlpull/v1/XmlPullParser;)I
    .locals 2

    .prologue
    .line 24545
    const/4 v0, 0x0

    const-string v1, "contentType"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24546
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "audio"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v1, "video"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v1, "text"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 24547
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24548
    if-nez v0, :cond_0

    .line 24549
    :goto_0
    return-wide p2

    :cond_0
    invoke-static {v0}, LX/08x;->c(Ljava/lang/String;)J

    move-result-wide p2

    goto :goto_0
.end method

.method private b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)LX/0Au;
    .locals 2

    .prologue
    .line 24550
    const-string v0, "sourceURL"

    const-string v1, "range"

    invoke-static {p1, p2, v0, v1}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0Au;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 24551
    if-nez p0, :cond_1

    move-object p0, p1

    .line 24552
    :cond_0
    :goto_0
    return-object p0

    .line 24553
    :cond_1
    if-eqz p1, :cond_0

    .line 24554
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0Av;->b(Z)V

    goto :goto_0
.end method

.method private static c(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 24555
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24556
    if-nez v0, :cond_0

    .line 24557
    :goto_0
    return-wide p2

    :cond_0
    invoke-static {v0}, LX/08x;->d(Ljava/lang/String;)J

    move-result-wide p2

    goto :goto_0
.end method

.method private static c(Lorg/xmlpull/v1/XmlPullParser;)LX/0Lu;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 24558
    const-string v0, "schemeIdUri"

    invoke-interface {p0, v4, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move v0, v3

    move-object v1, v4

    move-object v2, v4

    .line 24559
    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 24560
    const-string v6, "cenc:pssh"

    invoke-static {p0, v6}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_1

    .line 24561
    const/4 v0, 0x1

    .line 24562
    new-instance v1, LX/0M1;

    const-string v2, "video/mp4"

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v6

    invoke-direct {v1, v2, v6}, LX/0M1;-><init>(Ljava/lang/String;[B)V

    .line 24563
    iget-object v2, v1, LX/0M1;->b:[B

    invoke-static {v2}, LX/0Mm;->a([B)Ljava/util/UUID;

    move-result-object v2

    .line 24564
    :cond_1
    const-string v6, "ContentProtection"

    invoke-static {p0, v6}, LX/0Af;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 24565
    if-eqz v0, :cond_2

    if-nez v2, :cond_2

    .line 24566
    const-string v0, "MediaPresentationDescriptionParser"

    const-string v1, "Skipped unsupported ContentProtection element"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 24567
    :goto_0
    return-object v4

    .line 24568
    :cond_2
    new-instance v0, LX/0Lu;

    invoke-direct {v0, v5, v2, v1}, LX/0Lu;-><init>(Ljava/lang/String;Ljava/util/UUID;LX/0M1;)V

    move-object v4, v0

    .line 24569
    goto :goto_0
.end method

.method public static d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 24570
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24571
    if-nez v0, :cond_0

    :goto_0
    return-wide p2

    :cond_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide p2

    goto :goto_0
.end method

.method private static d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 24572
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 24573
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/0At;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static d(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/0BT;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 24574
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 24575
    const-wide/16 v0, 0x0

    .line 24576
    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 24577
    const-string v2, "S"

    invoke-static {p0, v2}, LX/0Af;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 24578
    const-string v2, "t"

    invoke-static {p0, v2, v0, v1}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v0

    .line 24579
    const-string v2, "d"

    .line 24580
    const-wide/16 v11, -0x1

    invoke-static {p0, v2, v11, v12}, LX/0AZ;->d(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v11

    move-wide v8, v11

    .line 24581
    const-string v2, "r"

    invoke-static {p0, v2, v3}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v2

    add-int/lit8 v7, v2, 0x1

    .line 24582
    const/4 v2, 0x1

    if-ne v7, v2, :cond_2

    .line 24583
    const-string v2, "FBMediaBinary"

    invoke-static {p0, v2, v10}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v8, v9, v2}, LX/0AZ;->a(JJLjava/lang/String;)LX/0BT;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24584
    :cond_1
    const-string v2, "SegmentTimeline"

    invoke-static {p0, v2}, LX/0Af;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 24585
    return-object v6

    :cond_2
    move v2, v3

    .line 24586
    :goto_0
    if-ge v2, v7, :cond_1

    .line 24587
    invoke-static {v0, v1, v8, v9, v10}, LX/0AZ;->a(JJLjava/lang/String;)LX/0BT;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24588
    add-long v4, v0, v8

    .line 24589
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-wide v0, v4

    goto :goto_0
.end method

.method private static e(Lorg/xmlpull/v1/XmlPullParser;)I
    .locals 2

    .prologue
    .line 24590
    const-string v0, "schemeIdUri"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24591
    const-string v1, "urn:mpeg:dash:23003:3:audio_channel_configuration:2011"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24592
    const-string v0, "value"

    invoke-static {p0, v0}, LX/0AZ;->e(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    move-result v0

    .line 24593
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 24594
    const-string v1, "AudioChannelConfiguration"

    invoke-static {p0, v1}, LX/0Af;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 24595
    return v0

    .line 24596
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static e(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 24597
    const/4 v0, -0x1

    invoke-static {p0, p1, v0}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/io/InputStream;)LX/0AY;
    .locals 3

    .prologue
    .line 24598
    :try_start_0
    iget-object v0, p0, LX/0AZ;->c:Lorg/xmlpull/v1/XmlPullParserFactory;

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 24599
    const/4 v1, 0x0

    invoke-interface {v0, p2, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 24600
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 24601
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const-string v1, "MPD"

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 24602
    :cond_0
    new-instance v0, LX/0L6;

    const-string v1, "inputStream does not contain a valid media presentation description"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 24603
    :catch_0
    move-exception v0

    .line 24604
    new-instance v1, LX/0L6;

    invoke-direct {v1, v0}, LX/0L6;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 24605
    :cond_1
    :try_start_1
    invoke-direct {p0, v0, p1}, LX/0AZ;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)LX/0AY;
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    return-object v0

    .line 24606
    :catch_1
    move-exception v0

    .line 24607
    new-instance v1, LX/0L6;

    invoke-direct {v1, v0}, LX/0L6;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final synthetic b(Ljava/lang/String;Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24608
    invoke-virtual {p0, p1, p2}, LX/0AZ;->a(Ljava/lang/String;Ljava/io/InputStream;)LX/0AY;

    move-result-object v0

    return-object v0
.end method
