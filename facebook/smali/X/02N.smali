.class public final LX/02N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/DoNotOptimize;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 7389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/02H;)Landroid/view/Choreographer$FrameCallback;
    .locals 2

    .prologue
    .line 7390
    new-instance v0, LX/02O;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p0}, LX/02O;-><init>(LX/02H;)V

    return-object v0
.end method

.method public static a(Landroid/view/Choreographer$FrameCallback;)V
    .locals 1

    .prologue
    .line 7391
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 7392
    return-void
.end method

.method public static b(Landroid/view/Choreographer$FrameCallback;)V
    .locals 1

    .prologue
    .line 7393
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 7394
    return-void
.end method
