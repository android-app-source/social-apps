.class public LX/0Dh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0D5;

.field public b:Z


# direct methods
.method public constructor <init>(LX/0D5;)V
    .locals 1

    .prologue
    .line 30947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30948
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Dh;->b:Z

    .line 30949
    iput-object p1, p0, LX/0Dh;->a:LX/0D5;

    .line 30950
    return-void
.end method

.method public static b(Ljava/lang/String;)J
    .locals 8

    .prologue
    const-wide/16 v0, -0x1

    .line 30951
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 30952
    :cond_0
    :goto_0
    return-wide v0

    .line 30953
    :cond_1
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 30954
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-ltz v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 30955
    :catch_0
    goto :goto_0
.end method
