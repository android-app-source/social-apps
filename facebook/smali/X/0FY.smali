.class public final LX/0FY;
.super Lcom/facebook/common/dextricks/MultiDexClassLoader;
.source ""


# static fields
.field public static sApiDetectionState:B


# instance fields
.field private final mAuxiliaryDexes:[Ldalvik/system/DexFile;

.field public final mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

.field public mDexFiles:[Ldalvik/system/DexFile;

.field private final mDexLoadFailureHistory:[Ljava/lang/String;

.field private mDexLoadFailurePosition:I

.field public final mLastLoadedDexIndex:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mPrimaryDexes:[Ldalvik/system/DexFile;

.field public final mPutativeLoader:Ljava/lang/ClassLoader;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33595
    const/4 v0, 0x0

    sput-byte v0, LX/0FY;->sApiDetectionState:B

    return-void
.end method

.method public constructor <init>(Ljava/lang/ClassLoader;Ljava/lang/ClassLoader;Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 33365
    invoke-direct {p0, p1}, Lcom/facebook/common/dextricks/MultiDexClassLoader;-><init>(Ljava/lang/ClassLoader;)V

    .line 33366
    const/4 v0, 0x0

    new-array v0, v0, [Ldalvik/system/DexFile;

    iput-object v0, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    .line 33367
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/0FY;->mLastLoadedDexIndex:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 33368
    iput-object p2, p0, LX/0FY;->mPutativeLoader:Ljava/lang/ClassLoader;

    .line 33369
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, LX/0FY;->mDexLoadFailureHistory:[Ljava/lang/String;

    .line 33370
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 33371
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 33372
    :try_start_0
    invoke-static {p3, p2, v0, v1}, Lcom/facebook/common/dextricks/MultiDexClassLoader;->learnApplicationDexFiles(Landroid/content/Context;Ljava/lang/ClassLoader;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 33373
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ldalvik/system/DexFile;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldalvik/system/DexFile;

    iput-object v0, p0, LX/0FY;->mPrimaryDexes:[Ldalvik/system/DexFile;

    .line 33374
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ldalvik/system/DexFile;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldalvik/system/DexFile;

    iput-object v0, p0, LX/0FY;->mAuxiliaryDexes:[Ldalvik/system/DexFile;

    .line 33375
    sget-object v0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    move-object v0, v0

    .line 33376
    iput-object v0, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    .line 33377
    return-void

    .line 33378
    :catch_0
    move-exception v2

    .line 33379
    const-string v3, "MultiDexClassLoader"

    const-string v4, "failure to locate primary/auxiliary dexes: perf loss"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private loadInnerFallbackApiClass(Ljava/lang/String;[Ldalvik/system/DexFile;I)Ljava/lang/Class;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ldalvik/system/DexFile;",
            "I)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 33582
    const/4 v0, 0x0

    .line 33583
    :try_start_0
    iget-object v3, p0, LX/0FY;->mPutativeLoader:Ljava/lang/ClassLoader;

    .line 33584
    const/4 v2, 0x0

    aget-object v2, p2, v2

    .line 33585
    if-eqz v2, :cond_1

    .line 33586
    invoke-virtual {v2, p1, v3}, Ldalvik/system/DexFile;->loadClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    move v2, v1

    .line 33587
    :goto_1
    if-nez v0, :cond_2

    if-ge v2, p3, :cond_2

    .line 33588
    :try_start_1
    aget-object v4, p2, v2

    .line 33589
    invoke-virtual {v4, p1, v3}, Ldalvik/system/DexFile;->loadClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    .line 33590
    if-eqz v0, :cond_0

    if-le v2, v1, :cond_0

    .line 33591
    invoke-direct {p0, v4, v2}, LX/0FY;->promoteDexFile(Ldalvik/system/DexFile;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 33592
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 33593
    :cond_1
    :try_start_2
    invoke-static {p0}, LX/0FY;->onNoDexInThePromotedFrontDexSpot(LX/0FY;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 33594
    :catchall_0
    move-exception v0

    :goto_2
    iget-object v2, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-virtual {v2, v1}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a(I)V

    throw v0

    :cond_2
    iget-object v1, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-virtual {v1, v2}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a(I)V

    return-object v0

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_2
.end method

.method private loadInnerNewApiClass(Ljava/lang/String;[Ldalvik/system/DexFile;I)Ljava/lang/Class;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ldalvik/system/DexFile;",
            "I)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 33569
    const/4 v0, 0x0

    .line 33570
    :try_start_0
    iget-object v3, p0, LX/0FY;->mPutativeLoader:Ljava/lang/ClassLoader;

    .line 33571
    const/4 v2, 0x0

    aget-object v2, p2, v2

    .line 33572
    if-eqz v2, :cond_1

    .line 33573
    invoke-static {v2, p1, v3}, Lcom/facebook/common/dextricks/DexFileLoadNew;->loadClassBinaryName(Ldalvik/system/DexFile;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    move v2, v1

    .line 33574
    :goto_1
    if-nez v0, :cond_2

    if-ge v2, p3, :cond_2

    .line 33575
    :try_start_1
    aget-object v4, p2, v2

    .line 33576
    invoke-static {v4, p1, v3}, Lcom/facebook/common/dextricks/DexFileLoadNew;->loadClassBinaryName(Ldalvik/system/DexFile;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    .line 33577
    if-eqz v0, :cond_0

    if-le v2, v1, :cond_0

    .line 33578
    invoke-direct {p0, v4, v2}, LX/0FY;->promoteDexFile(Ldalvik/system/DexFile;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 33579
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 33580
    :cond_1
    :try_start_2
    invoke-static {p0}, LX/0FY;->onNoDexInThePromotedFrontDexSpot(LX/0FY;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 33581
    :catchall_0
    move-exception v0

    :goto_2
    iget-object v2, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-virtual {v2, v1}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a(I)V

    throw v0

    :cond_2
    iget-object v1, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-virtual {v1, v2}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a(I)V

    return-object v0

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_2
.end method

.method private loadInnerOldApiClass(Ljava/lang/String;[Ldalvik/system/DexFile;I)Ljava/lang/Class;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ldalvik/system/DexFile;",
            "I)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 33556
    const/4 v0, 0x0

    .line 33557
    :try_start_0
    iget-object v3, p0, LX/0FY;->mPutativeLoader:Ljava/lang/ClassLoader;

    .line 33558
    const/4 v2, 0x0

    aget-object v2, p2, v2

    .line 33559
    if-eqz v2, :cond_1

    .line 33560
    invoke-static {v2, p1, v3}, Lcom/facebook/common/dextricks/DexFileLoadOld;->loadClassBinaryName(Ldalvik/system/DexFile;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    move v2, v1

    .line 33561
    :goto_1
    if-nez v0, :cond_2

    if-ge v2, p3, :cond_2

    .line 33562
    :try_start_1
    aget-object v4, p2, v2

    .line 33563
    invoke-static {v4, p1, v3}, Lcom/facebook/common/dextricks/DexFileLoadOld;->loadClassBinaryName(Ldalvik/system/DexFile;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    .line 33564
    if-eqz v0, :cond_0

    if-le v2, v1, :cond_0

    .line 33565
    invoke-direct {p0, v4, v2}, LX/0FY;->promoteDexFile(Ldalvik/system/DexFile;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 33566
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 33567
    :cond_1
    :try_start_2
    invoke-static {p0}, LX/0FY;->onNoDexInThePromotedFrontDexSpot(LX/0FY;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 33568
    :catchall_0
    move-exception v0

    :goto_2
    iget-object v2, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-virtual {v2, v1}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a(I)V

    throw v0

    :cond_2
    iget-object v1, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-virtual {v1, v2}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a(I)V

    return-object v0

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_2
.end method

.method private loadParentBootLoaderClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 33551
    invoke-virtual {p0}, LX/0FY;->getParent()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 33552
    if-eqz v0, :cond_0

    .line 33553
    :try_start_0
    iget-object v0, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-virtual {v0}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->b()V

    .line 33554
    invoke-virtual {p0}, LX/0FY;->getParent()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 33555
    :goto_0
    return-object v0

    :catch_0
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private noteClassLoadFailure(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 33547
    iget-object v1, p0, LX/0FY;->mDexLoadFailureHistory:[Ljava/lang/String;

    .line 33548
    monitor-enter v1

    .line 33549
    :try_start_0
    iget v0, p0, LX/0FY;->mDexLoadFailurePosition:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, LX/0FY;->mDexLoadFailurePosition:I

    array-length v2, v1

    rem-int/2addr v0, v2

    aput-object p1, v1, v0

    .line 33550
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static onNoDexInThePromotedFrontDexSpot(LX/0FY;)V
    .locals 1

    .prologue
    .line 33545
    iget-object v0, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-virtual {v0}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->c()V

    .line 33546
    return-void
.end method

.method private promoteDexFile(Ldalvik/system/DexFile;I)V
    .locals 4

    .prologue
    .line 33522
    iget-object v0, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    .line 33523
    array-length v1, v0

    .line 33524
    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x2

    move v1, v1

    .line 33525
    if-gt v1, p2, :cond_4

    .line 33526
    const/4 v2, 0x0

    .line 33527
    :goto_0
    move v1, v2

    .line 33528
    if-nez v1, :cond_0

    .line 33529
    :goto_1
    return-void

    .line 33530
    :cond_0
    iget-object v1, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    monitor-enter v1

    .line 33531
    const/4 v2, 0x1

    :try_start_0
    aget-object v2, v0, v2

    if-ne v2, p1, :cond_1

    .line 33532
    monitor-exit v1

    goto :goto_1

    .line 33533
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 33534
    :cond_1
    :try_start_1
    aget-object v2, v0, p2

    if-eq v2, p1, :cond_2

    .line 33535
    monitor-exit v1

    goto :goto_1

    .line 33536
    :cond_2
    const/4 v2, 0x0

    aput-object p1, v0, v2

    .line 33537
    :goto_2
    if-lez p2, :cond_3

    .line 33538
    add-int/lit8 v2, p2, -0x1

    aget-object v2, v0, v2

    aput-object v2, v0, p2

    .line 33539
    add-int/lit8 p2, p2, -0x1

    goto :goto_2

    .line 33540
    :cond_3
    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v0, v2

    .line 33541
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 33542
    :cond_4
    iget-object v2, p0, LX/0FY;->mLastLoadedDexIndex:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    .line 33543
    if-ge v2, p2, :cond_5

    iget-object v3, p0, LX/0FY;->mLastLoadedDexIndex:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3, v2, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 33544
    :cond_5
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final doConfigure(LX/02f;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 33497
    iget-object v0, p0, LX/0FY;->mPrimaryDexes:[Ldalvik/system/DexFile;

    array-length v3, v0

    .line 33498
    iget-object v0, p1, LX/02f;->mDexFiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 33499
    iget-object v0, p0, LX/0FY;->mAuxiliaryDexes:[Ldalvik/system/DexFile;

    array-length v5, v0

    .line 33500
    add-int v0, v3, v4

    add-int v6, v0, v5

    .line 33501
    mul-int/lit8 v0, v6, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 33502
    new-array v0, v0, [Ldalvik/system/DexFile;

    iput-object v0, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    .line 33503
    iget-object v0, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 33504
    const/4 v2, 0x1

    move v0, v1

    .line 33505
    :goto_0
    if-ge v0, v3, :cond_0

    .line 33506
    iget-object v7, p0, LX/0FY;->mPrimaryDexes:[Ldalvik/system/DexFile;

    aget-object v7, v7, v0

    .line 33507
    iget-object v8, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    aput-object v7, v8, v2

    .line 33508
    iget-object v8, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    add-int v9, v2, v6

    aput-object v7, v8, v9

    .line 33509
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    move v3, v1

    .line 33510
    :goto_1
    if-ge v3, v4, :cond_1

    .line 33511
    iget-object v0, p1, LX/02f;->mDexFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldalvik/system/DexFile;

    .line 33512
    iget-object v7, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    aput-object v0, v7, v2

    .line 33513
    iget-object v7, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    add-int v8, v2, v6

    aput-object v0, v7, v8

    .line 33514
    add-int/lit8 v0, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    move v0, v1

    move v1, v2

    .line 33515
    :goto_2
    if-ge v0, v5, :cond_2

    .line 33516
    iget-object v2, p0, LX/0FY;->mAuxiliaryDexes:[Ldalvik/system/DexFile;

    aget-object v2, v2, v0

    .line 33517
    iget-object v3, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    aput-object v2, v3, v1

    .line 33518
    iget-object v3, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    add-int v4, v1, v6

    aput-object v2, v3, v4

    .line 33519
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 33520
    :cond_2
    invoke-virtual {p0, p1}, Lcom/facebook/common/dextricks/MultiDexClassLoader;->setMadvAndMprotForOatFile(LX/02f;)V

    .line 33521
    return-void
.end method

.method public final doGetConfiguredDexFiles()[Ldalvik/system/DexFile;
    .locals 5

    .prologue
    .line 33488
    iget-object v1, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    .line 33489
    array-length v0, v1

    .line 33490
    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x2

    move v2, v0

    .line 33491
    new-array v3, v2, [Ldalvik/system/DexFile;

    .line 33492
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 33493
    add-int v4, v2, v0

    add-int/lit8 v4, v4, 0x1

    move v4, v4

    .line 33494
    aget-object v4, v1, v4

    aput-object v4, v3, v0

    .line 33495
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33496
    :cond_0
    return-object v3
.end method

.method public final findClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 33436
    const/4 v0, 0x0

    .line 33437
    iget-object v1, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    .line 33438
    array-length v2, v1

    .line 33439
    sget-byte v3, LX/0FY;->sApiDetectionState:B

    packed-switch v3, :pswitch_data_0

    .line 33440
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 33441
    invoke-static {v0}, Lcom/facebook/loom/logger/api/LoomLogger;->a(Ljava/lang/Class;)I

    .line 33442
    return-object v0

    .line 33443
    :pswitch_0
    iget-object v0, p0, LX/0FY;->mLastLoadedDexIndex:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-direct {p0, p1, v1, v0}, LX/0FY;->loadInnerNewApiClass(Ljava/lang/String;[Ldalvik/system/DexFile;I)Ljava/lang/Class;

    move-result-object v0

    .line 33444
    if-nez v0, :cond_0

    .line 33445
    invoke-direct {p0, p1, v1, v2}, LX/0FY;->loadInnerNewApiClass(Ljava/lang/String;[Ldalvik/system/DexFile;I)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 33446
    :pswitch_1
    iget-object v0, p0, LX/0FY;->mLastLoadedDexIndex:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-direct {p0, p1, v1, v0}, LX/0FY;->loadInnerOldApiClass(Ljava/lang/String;[Ldalvik/system/DexFile;I)Ljava/lang/Class;

    move-result-object v0

    .line 33447
    if-nez v0, :cond_0

    .line 33448
    invoke-direct {p0, p1, v1, v2}, LX/0FY;->loadInnerOldApiClass(Ljava/lang/String;[Ldalvik/system/DexFile;I)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 33449
    :pswitch_2
    iget-object v0, p0, LX/0FY;->mLastLoadedDexIndex:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-direct {p0, p1, v1, v0}, LX/0FY;->loadInnerFallbackApiClass(Ljava/lang/String;[Ldalvik/system/DexFile;I)Ljava/lang/Class;

    move-result-object v0

    .line 33450
    if-nez v0, :cond_0

    .line 33451
    invoke-direct {p0, p1, v1, v2}, LX/0FY;->loadInnerFallbackApiClass(Ljava/lang/String;[Ldalvik/system/DexFile;I)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 33452
    :pswitch_3
    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 33453
    iget-object v4, p0, LX/0FY;->mDexFiles:[Ldalvik/system/DexFile;

    .line 33454
    array-length v2, v4

    if-nez v2, :cond_2

    .line 33455
    :goto_1
    move-object v0, v0

    .line 33456
    goto :goto_0

    .line 33457
    :cond_1
    sget-object v0, Lcom/facebook/common/dextricks/MultiDexClassLoader;->sPrefabException:Ljava/lang/ClassNotFoundException;

    throw v0

    .line 33458
    :cond_2
    array-length v5, v4

    .line 33459
    iget-object v6, p0, LX/0FY;->mPutativeLoader:Ljava/lang/ClassLoader;

    .line 33460
    const/4 v2, 0x0

    :try_start_0
    aget-object v2, v4, v2

    .line 33461
    if-eqz v2, :cond_3

    .line 33462
    invoke-static {v2, p1, v6}, Lcom/facebook/common/dextricks/DexFileLoadNew;->loadClassBinaryName(Ldalvik/system/DexFile;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    move v2, v1

    .line 33463
    :goto_2
    if-nez v0, :cond_4

    if-ge v2, v5, :cond_4

    .line 33464
    aget-object v3, v4, v2

    invoke-static {v3, p1, v6}, Lcom/facebook/common/dextricks/DexFileLoadNew;->loadClassBinaryName(Ldalvik/system/DexFile;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v3

    .line 33465
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v0, v3

    goto :goto_2

    .line 33466
    :cond_3
    invoke-static {p0}, LX/0FY;->onNoDexInThePromotedFrontDexSpot(LX/0FY;)V

    move v2, v1

    goto :goto_2

    .line 33467
    :cond_4
    if-le v5, v1, :cond_5

    .line 33468
    const/4 v3, 0x1

    sput-byte v3, LX/0FY;->sApiDetectionState:B

    .line 33469
    :cond_5
    iget-object v3, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-virtual {v3, v2}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a(I)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 33470
    :catch_0
    const/4 v2, 0x0

    :try_start_1
    aget-object v2, v4, v2

    .line 33471
    if-eqz v2, :cond_6

    .line 33472
    invoke-static {v2, p1, v6}, Lcom/facebook/common/dextricks/DexFileLoadOld;->loadClassBinaryName(Ldalvik/system/DexFile;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    :goto_3
    move v2, v1

    .line 33473
    :goto_4
    if-nez v0, :cond_7

    if-ge v2, v5, :cond_7

    .line 33474
    aget-object v3, v4, v2

    invoke-static {v3, p1, v6}, Lcom/facebook/common/dextricks/DexFileLoadOld;->loadClassBinaryName(Ldalvik/system/DexFile;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v3

    .line 33475
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v0, v3

    goto :goto_4

    .line 33476
    :cond_6
    invoke-static {p0}, LX/0FY;->onNoDexInThePromotedFrontDexSpot(LX/0FY;)V
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 33477
    :catch_1
    const/4 v2, 0x3

    sput-byte v2, LX/0FY;->sApiDetectionState:B

    .line 33478
    aget-object v2, v4, v7

    .line 33479
    if-eqz v2, :cond_8

    .line 33480
    invoke-virtual {v2, p1, v6}, Ldalvik/system/DexFile;->loadClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    .line 33481
    :goto_5
    if-nez v0, :cond_9

    if-ge v1, v5, :cond_9

    .line 33482
    aget-object v0, v4, v1

    invoke-virtual {v0, p1, v6}, Ldalvik/system/DexFile;->loadClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    .line 33483
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_5

    .line 33484
    :cond_7
    const/4 v3, 0x2

    :try_start_2
    sput-byte v3, LX/0FY;->sApiDetectionState:B

    .line 33485
    iget-object v3, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-virtual {v3, v2}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a(I)V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodError; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 33486
    :cond_8
    invoke-static {p0}, LX/0FY;->onNoDexInThePromotedFrontDexSpot(LX/0FY;)V

    goto :goto_5

    .line 33487
    :cond_9
    iget-object v2, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-virtual {v2, v1}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final getRecentFailedClasses()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 33424
    iget-object v2, p0, LX/0FY;->mDexLoadFailureHistory:[Ljava/lang/String;

    .line 33425
    array-length v3, v2

    .line 33426
    new-array v4, v3, [Ljava/lang/String;

    .line 33427
    monitor-enter v2

    .line 33428
    :try_start_0
    iget v0, p0, LX/0FY;->mDexLoadFailurePosition:I

    .line 33429
    if-ge v0, v3, :cond_0

    .line 33430
    add-int/2addr v0, v3

    .line 33431
    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 33432
    add-int/lit8 v5, v1, 0x1

    sub-int v5, v0, v5

    rem-int/2addr v5, v3

    aget-object v5, v2, v5

    aput-object v5, v4, v1

    .line 33433
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 33434
    :cond_1
    monitor-exit v2

    return-object v4

    .line 33435
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final loadClass(Ljava/lang/String;Z)Ljava/lang/Class;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 33382
    const/4 v0, 0x0

    .line 33383
    const/16 p2, 0x2e

    const/4 v6, 0x6

    const/4 v5, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 33384
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 33385
    if-gt v3, v6, :cond_4
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33386
    :cond_0
    :goto_0
    :try_start_1
    move v1, v1

    .line 33387
    if-eqz v1, :cond_3

    .line 33388
    const/4 v1, 0x1

    .line 33389
    invoke-direct {p0, p1}, LX/0FY;->loadParentBootLoaderClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 33390
    if-eqz v0, :cond_1

    .line 33391
    :goto_1
    return-object v0

    :cond_1
    move v2, v1

    .line 33392
    :goto_2
    :try_start_2
    invoke-virtual {p0, p1}, LX/0FY;->findClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 33393
    iget-object v1, p0, LX/0FY;->mClassLoadingStats:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-virtual {v1}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->b()V
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 33394
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 33395
    if-nez v2, :cond_2

    .line 33396
    :try_start_3
    const-string v0, "com.facebook."

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 33397
    if-eqz v0, :cond_2

    .line 33398
    invoke-direct {p0, p1}, LX/0FY;->loadParentBootLoaderClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 33399
    if-eqz v0, :cond_2

    .line 33400
    const-string v1, "MultiDexClassLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was loaded on fallback. This should be fixed and added to the shouldAskParent method."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 33401
    :catchall_0
    move-exception v0

    throw v0

    .line 33402
    :cond_2
    :try_start_4
    invoke-direct {p0, p1}, LX/0FY;->noteClassLoadFailure(Ljava/lang/String;)V

    .line 33403
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_3
    move v2, v0

    goto :goto_2

    .line 33404
    :cond_4
    :try_start_5
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_0

    goto :goto_0

    .line 33405
    :sswitch_0
    const/16 v4, 0x10

    if-lt v3, v4, :cond_6

    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x73

    if-ne v3, v4, :cond_6

    const/16 v3, 0x9

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x75

    if-ne v3, v4, :cond_6

    const/16 v3, 0xf

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, p2, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, p2, :cond_6

    .line 33406
    const-string v3, "ndroid.support"

    const/4 v4, 0x1

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "test."

    const/16 v4, 0x10

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_5
    move v1, v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    .line 33407
    goto/16 :goto_0

    .line 33408
    :sswitch_1
    const/16 v4, 0x8

    if-lt v3, v4, :cond_0

    move v1, v2

    goto/16 :goto_0

    .line 33409
    :sswitch_2
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    sparse-switch v3, :sswitch_data_1

    goto/16 :goto_0

    :sswitch_3
    move v1, v2

    .line 33410
    goto/16 :goto_0

    :sswitch_4
    move v1, v2

    .line 33411
    goto/16 :goto_0

    :sswitch_5
    move v1, v2

    .line 33412
    goto/16 :goto_0

    .line 33413
    :sswitch_6
    if-lt v3, v6, :cond_0

    .line 33414
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_2

    goto/16 :goto_0

    .line 33415
    :sswitch_7
    const/16 v4, 0xc

    if-lt v3, v4, :cond_0

    const/4 v3, 0x5

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x70

    if-ne v3, v4, :cond_0

    const/4 v3, 0x6

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x61

    if-ne v3, v4, :cond_0

    const/16 v3, 0xa

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, p2, :cond_0

    move v1, v2

    goto/16 :goto_0

    :sswitch_8
    move v1, v2

    .line 33416
    goto/16 :goto_0

    .line 33417
    :sswitch_9
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x33

    if-ne v3, v4, :cond_0

    move v1, v2

    goto/16 :goto_0

    :sswitch_a
    move v1, v2

    .line 33418
    goto/16 :goto_0

    .line 33419
    :sswitch_b
    if-lt v3, v5, :cond_0

    const/4 v3, 0x3

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, p2, :cond_0

    move v1, v2

    goto/16 :goto_0

    .line 33420
    :sswitch_c
    if-lt v3, v6, :cond_0

    const/4 v3, 0x4

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x61

    if-ne v3, v4, :cond_0

    const/4 v3, 0x5

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C
    :try_end_5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    move-result v3

    const/16 v4, 0x6e

    if-ne v3, v4, :cond_0

    move v1, v2

    .line 33421
    goto/16 :goto_0

    .line 33422
    :catch_1
    move-exception v2

    .line 33423
    const-string v3, "MultiDexClassLoader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Class out of bounds: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x63 -> :sswitch_c
        0x64 -> :sswitch_1
        0x6a -> :sswitch_2
        0x6c -> :sswitch_5
        0x6f -> :sswitch_6
        0x73 -> :sswitch_b
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_3
        0x75 -> :sswitch_4
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x61 -> :sswitch_7
        0x6a -> :sswitch_8
        0x77 -> :sswitch_9
        0x78 -> :sswitch_a
    .end sparse-switch
.end method

.method public final onColdstartDone()V
    .locals 0

    .prologue
    .line 33381
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33380
    const-string v0, "MultiDexClassLoaderJava"

    return-object v0
.end method
