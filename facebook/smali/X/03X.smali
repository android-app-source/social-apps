.class public LX/03X;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Runnable;I)Ljava/lang/Runnable;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 10473
    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10474
    :goto_0
    return-object p0

    .line 10475
    :cond_0
    const/16 v0, 0xe

    invoke-static {v1, v0, p1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 10476
    new-instance v0, Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;

    invoke-direct {v0, p0, v1, p1}, Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;-><init>(Ljava/lang/Runnable;II)V

    move-object p0, v0

    .line 10477
    goto :goto_0
.end method

.method private static a(Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Callable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 10469
    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10470
    :goto_0
    return-object p0

    .line 10471
    :cond_0
    const/16 v0, 0xe

    invoke-static {v1, v0, p1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 10472
    new-instance v0, LX/09j;

    invoke-direct {v0, p0, v1, p1}, LX/09j;-><init>(Ljava/util/concurrent/Callable;II)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Ljava/util/concurrent/ExecutorCompletionService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorCompletionService",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/concurrent/Future;"
        }
    .end annotation

    .prologue
    .line 10468
    invoke-static {p1, p2}, LX/03X;->a(Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/concurrent/ExecutorCompletionService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 10467
    invoke-static {p1, p2}, LX/03X;->a(Ljava/lang/Runnable;I)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;Ljava/lang/Object;I)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 10466
    invoke-static {p1, p3}, LX/03X;->a(Ljava/lang/Runnable;I)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {p0, v0, p2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/concurrent/Future;"
        }
    .end annotation

    .prologue
    .line 10465
    invoke-static {p1, p2}, LX/03X;->a(Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    .locals 1

    .prologue
    .line 10463
    invoke-static {p1, p2}, LX/03X;->a(Ljava/lang/Runnable;I)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 10464
    return-void
.end method
