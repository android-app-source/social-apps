.class public LX/0Gr;
.super LX/0Gq;
.source ""


# instance fields
.field private e:LX/0Gg;


# direct methods
.method public constructor <init>(LX/0Gt;Ljava/lang/String;IILX/0Gg;)V
    .locals 1

    .prologue
    .line 36510
    invoke-direct {p0}, LX/0Gq;-><init>()V

    .line 36511
    invoke-virtual {p1, p2, p5, p3, p4}, LX/0Gt;->a(Ljava/lang/String;LX/04n;II)LX/0Ge;

    move-result-object v0

    iput-object v0, p0, LX/0Gr;->a:LX/0Ge;

    .line 36512
    iput-object p5, p0, LX/0Gr;->e:LX/0Gg;

    .line 36513
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Gr;->b:[B

    .line 36514
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 3

    .prologue
    .line 36515
    iget v0, p0, LX/0Gq;->c:I

    iget v1, p0, LX/0Gq;->d:I

    sub-int/2addr v0, v1

    .line 36516
    if-nez v0, :cond_1

    .line 36517
    const/4 v0, -0x1

    .line 36518
    :cond_0
    :goto_0
    return v0

    .line 36519
    :cond_1
    if-le p3, v0, :cond_4

    .line 36520
    :goto_1
    if-lez v0, :cond_0

    .line 36521
    :try_start_0
    iget-object v1, p0, LX/0Gq;->a:LX/0Ge;

    invoke-interface {v1, p1, p2, v0}, LX/0Ge;->a([BII)I

    move-result v0

    .line 36522
    if-lez v0, :cond_0

    .line 36523
    iget-object v1, p0, LX/0Gq;->b:[B

    if-eqz v1, :cond_2

    .line 36524
    iget-object v1, p0, LX/0Gq;->b:[B

    iget v2, p0, LX/0Gq;->d:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 36525
    :cond_2
    iget v1, p0, LX/0Gq;->d:I

    add-int/2addr v1, v0

    iput v1, p0, LX/0Gr;->d:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 36526
    :catch_0
    move-exception v0

    .line 36527
    iget-object v1, p0, LX/0Gr;->e:LX/0Gg;

    if-eqz v1, :cond_3

    .line 36528
    iget-object v1, p0, LX/0Gr;->e:LX/0Gg;

    invoke-virtual {v1, v0}, LX/0Gg;->a(Ljava/io/IOException;)V

    .line 36529
    :cond_3
    throw v0

    :cond_4
    move v0, p3

    goto :goto_1
.end method

.method public final a(LX/0OA;)J
    .locals 4

    .prologue
    .line 36530
    iget-object v0, p0, LX/0Gr;->e:LX/0Gg;

    if-eqz v0, :cond_0

    .line 36531
    iget-object v0, p0, LX/0Gr;->e:LX/0Gg;

    iget-object v1, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0AC;->NOT_CACHED:LX/0AC;

    invoke-virtual {v0, v1, v2}, LX/0Gg;->a(Ljava/lang/String;LX/0AC;)V

    .line 36532
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/0Gq;->a:LX/0Ge;

    invoke-interface {v0, p1}, LX/0Ge;->a(LX/0OA;)J

    move-result-wide v0

    .line 36533
    long-to-int v2, v0

    iput v2, p0, LX/0Gr;->c:I

    .line 36534
    const/4 v2, 0x0

    iput v2, p0, LX/0Gr;->d:I

    .line 36535
    iget v2, p0, LX/0Gq;->c:I

    const/high16 v3, 0x100000

    if-gt v2, v3, :cond_1

    .line 36536
    iget v2, p0, LX/0Gq;->c:I

    new-array v2, v2, [B

    iput-object v2, p0, LX/0Gr;->b:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36537
    :cond_1
    return-wide v0

    .line 36538
    :catch_0
    move-exception v0

    .line 36539
    iget-object v1, p0, LX/0Gr;->e:LX/0Gg;

    if-eqz v1, :cond_2

    .line 36540
    iget-object v1, p0, LX/0Gr;->e:LX/0Gg;

    invoke-virtual {v1, v0}, LX/0Gg;->a(Ljava/io/IOException;)V

    .line 36541
    :cond_2
    throw v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 36542
    :try_start_0
    iget-object v0, p0, LX/0Gq;->a:LX/0Ge;

    invoke-interface {v0}, LX/0Ge;->a()V

    .line 36543
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Gr;->a:LX/0Ge;

    .line 36544
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Gr;->e:LX/0Gg;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36545
    return-void

    .line 36546
    :catch_0
    move-exception v0

    .line 36547
    iget-object v1, p0, LX/0Gr;->e:LX/0Gg;

    if-eqz v1, :cond_0

    .line 36548
    iget-object v1, p0, LX/0Gr;->e:LX/0Gg;

    invoke-virtual {v1, v0}, LX/0Gg;->a(Ljava/io/IOException;)V

    .line 36549
    :cond_0
    throw v0
.end method
