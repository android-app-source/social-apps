.class public LX/056;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public A:LX/06T;

.field public B:Z

.field private C:I

.field public D:LX/055;

.field public E:Z

.field public F:Ljava/util/concurrent/atomic/AtomicInteger;

.field public G:Ljava/lang/String;

.field public H:Z

.field public I:J

.field private J:J

.field private K:Landroid/os/PowerManager;

.field public L:LX/059;

.field public M:Landroid/content/Context;

.field private volatile N:LX/04q;

.field public O:Landroid/content/BroadcastReceiver;

.field public P:Landroid/content/BroadcastReceiver;

.field public Q:LX/05b;

.field public R:LX/05e;

.field public S:Z

.field private T:Z

.field private U:Ljava/lang/reflect/Method;

.field public final V:LX/058;

.field public final W:Ljava/util/concurrent/atomic/AtomicLong;

.field private final X:Ljava/lang/Runnable;

.field private final Y:Ljava/lang/Runnable;

.field private final Z:Ljava/lang/Runnable;

.field public a:LX/05H;

.field public volatile b:LX/072;

.field public c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public d:LX/05h;

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0AF;",
            ">;"
        }
    .end annotation
.end field

.field public f:J

.field public g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field public h:J

.field public i:Z

.field public j:LX/05i;

.field public k:LX/06J;

.field public l:Ljava/util/concurrent/ExecutorService;

.field public volatile m:LX/072;

.field public n:I

.field public o:J

.field public p:LX/05L;

.field public volatile q:J

.field private r:LX/04p;

.field private s:LX/06F;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/rti/common/util/NonInjectProvider1",
            "<",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;",
            "LX/072;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/06I;

.field public u:LX/05F;

.field public v:LX/05G;

.field public w:LX/05F;

.field public x:LX/05G;

.field public y:Landroid/os/Handler;

.field public z:LX/06K;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15677
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/056;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 15678
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/056;->e:Ljava/util/Map;

    .line 15679
    iput-boolean v1, p0, LX/056;->i:Z

    .line 15680
    iput-boolean v1, p0, LX/056;->B:Z

    .line 15681
    new-instance v0, LX/057;

    invoke-direct {v0, p0}, LX/057;-><init>(LX/056;)V

    iput-object v0, p0, LX/056;->V:LX/058;

    .line 15682
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, LX/056;->W:Ljava/util/concurrent/atomic/AtomicLong;

    .line 15683
    new-instance v0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$2;

    invoke-direct {v0, p0}, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$2;-><init>(LX/056;)V

    iput-object v0, p0, LX/056;->X:Ljava/lang/Runnable;

    .line 15684
    new-instance v0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$3;

    invoke-direct {v0, p0}, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$3;-><init>(LX/056;)V

    iput-object v0, p0, LX/056;->Y:Ljava/lang/Runnable;

    .line 15685
    new-instance v0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$4;

    invoke-direct {v0, p0}, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$4;-><init>(LX/056;)V

    iput-object v0, p0, LX/056;->Z:Ljava/lang/Runnable;

    .line 15686
    return-void
.end method

.method private static a(LX/056;LX/072;LX/0BA;)V
    .locals 3

    .prologue
    .line 15512
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/reconnect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15513
    sget-object v0, LX/0AX;->EXPIRE_CONNECTION:LX/0AX;

    invoke-virtual {p0, p1, v0, p2}, LX/056;->a(LX/072;LX/0AX;LX/0BA;)Ljava/util/concurrent/Future;

    .line 15514
    invoke-static {p0}, LX/056;->s(LX/056;)V

    .line 15515
    return-void
.end method

.method public static a$redex0(LX/056;LX/0BA;LX/05B;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0BA;",
            "LX/05B",
            "<",
            "LX/0Hs;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 15516
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/lost; reason=%s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15517
    const-string v0, ""

    .line 15518
    iget-object v1, p0, LX/056;->b:LX/072;

    .line 15519
    if-eqz v1, :cond_0

    .line 15520
    iget-object v0, v1, LX/072;->m:LX/071;

    invoke-interface {v0}, LX/071;->f()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 15521
    :cond_0
    iget-object v1, p0, LX/056;->k:LX/06J;

    new-instance v2, LX/0BK;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Connection lost "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, LX/0BK;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1, v2}, LX/06J;->a(Ljava/lang/Throwable;)V

    .line 15522
    sget-object v0, LX/0B9;->a:[I

    invoke-virtual {p1}, LX/0BA;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 15523
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/lost/no_attempt; reason=%s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15524
    :goto_0
    invoke-virtual {p2}, LX/05B;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/0Hs;->FAILED_SOCKET_CONNECT_ERROR_SSL_CLOCK_SKEW:LX/0Hs;

    if-ne v0, v1, :cond_3

    .line 15525
    iput-boolean v6, p0, LX/056;->i:Z

    .line 15526
    :goto_1
    iget-object v0, p0, LX/056;->D:LX/055;

    .line 15527
    iget-object v1, v0, LX/055;->a:LX/051;

    .line 15528
    const-string v2, "MqttPushService"

    const-string v3, "connection/lost; lastState=%s."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, v1, LX/051;->o:LX/053;

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15529
    invoke-virtual {p2}, LX/05B;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 15530
    invoke-virtual {p2}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Hs;

    invoke-virtual {v1, v2}, LX/051;->a(LX/0Hs;)V

    .line 15531
    :cond_1
    sget-object v2, LX/053;->DISCONNECTED:LX/053;

    invoke-virtual {v1, v2}, LX/051;->a(LX/053;)Z

    .line 15532
    return-void

    .line 15533
    :pswitch_0
    iget-object v0, p0, LX/056;->t:LX/06I;

    invoke-virtual {v0}, LX/06I;->c()Z

    goto :goto_0

    .line 15534
    :pswitch_1
    iget-object v0, p0, LX/056;->j:LX/05i;

    sget-object v1, LX/06f;->CONNECTION_LOST:LX/06f;

    .line 15535
    iput-object v1, v0, LX/05i;->n:LX/06f;

    .line 15536
    iget-wide v0, p0, LX/056;->o:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-object v0, p0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/056;->o:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iget-object v2, p0, LX/056;->r:LX/04p;

    invoke-virtual {v2}, LX/04p;->b()LX/04q;

    move-result-object v2

    iget v2, v2, LX/04q;->A:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 15537
    iget-object v0, p0, LX/056;->t:LX/06I;

    invoke-virtual {v0}, LX/06I;->h()V

    .line 15538
    :goto_2
    iget-object v0, p0, LX/056;->t:LX/06I;

    invoke-virtual {v0}, LX/06I;->c()Z

    goto :goto_0

    .line 15539
    :cond_2
    iget-object v0, p0, LX/056;->t:LX/06I;

    invoke-virtual {v0}, LX/06I;->i()V

    .line 15540
    iget-object v0, p0, LX/056;->t:LX/06I;

    invoke-virtual {v0}, LX/06I;->d()V

    goto :goto_2

    .line 15541
    :cond_3
    iput-boolean v5, p0, LX/056;->i:Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a$redex0(LX/056;LX/0Ha;Z)V
    .locals 5

    .prologue
    .line 15542
    if-eqz p1, :cond_1

    .line 15543
    invoke-virtual {p1}, LX/0Ha;->a()Ljava/lang/String;

    move-result-object v1

    .line 15544
    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 15545
    const-string v0, "FbnsConnectionManager"

    const-string v2, "send/health_stats; stats=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15546
    :try_start_0
    sget-object v0, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    .line 15547
    if-eqz p2, :cond_0

    .line 15548
    sget-object v0, LX/0AL;->FIRE_AND_FORGET:LX/0AL;

    .line 15549
    :cond_0
    const-string v2, "/mqtt_health_stats"

    invoke-virtual {p0, v2, v1, v0}, LX/056;->a(Ljava/lang/String;Ljava/lang/String;LX/0AL;)I
    :try_end_0
    .catch LX/0BK; {:try_start_0 .. :try_end_0} :catch_0

    .line 15550
    :cond_1
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public static a$redex0(LX/056;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 15551
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 15552
    const-string v1, "com.facebook.rti.mqtt.ACTION_MQTT_CONFIG_CHANGED"

    invoke-static {v0, v1}, LX/06P;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 15553
    iget-object v0, p0, LX/056;->r:LX/04p;

    invoke-virtual {v0}, LX/04p;->a()V

    .line 15554
    iget-object v0, p0, LX/056;->r:LX/04p;

    invoke-virtual {v0}, LX/04p;->b()LX/04q;

    move-result-object v0

    .line 15555
    iget-object v1, p0, LX/056;->N:LX/04q;

    .line 15556
    iget-object v2, v0, LX/04q;->a:Ljava/lang/String;

    iget-object v3, v1, LX/04q;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v0, LX/04q;->c:I

    iget v3, v1, LX/04q;->c:I

    if-ne v2, v3, :cond_0

    iget v2, v0, LX/04q;->d:I

    iget v3, v1, LX/04q;->d:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, v0, LX/04q;->x:Z

    iget-boolean v3, v1, LX/04q;->x:Z

    if-eq v2, v3, :cond_8

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 15557
    iput-object v0, p0, LX/056;->N:LX/04q;

    .line 15558
    if-eqz v1, :cond_2

    .line 15559
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/reconnect; reason=config_changed"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15560
    sget-object v0, LX/0AX;->KICK_CONFIG_CHANGED:LX/0AX;

    invoke-virtual {p0, v0}, LX/056;->a(LX/0AX;)Ljava/util/concurrent/Future;

    .line 15561
    sget-object v0, LX/06f;->CONFIG_CHANGED:LX/06f;

    invoke-virtual {p0, v0}, LX/056;->a(LX/06f;)V

    .line 15562
    :cond_1
    :goto_1
    return-void

    .line 15563
    :cond_2
    invoke-virtual {p0}, LX/056;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 15564
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/kick; reason=config_changed"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15565
    sget-object v0, LX/06f;->CONFIG_CHANGED:LX/06f;

    invoke-virtual {p0, v0}, LX/056;->a(LX/06f;)V

    goto :goto_1

    .line 15566
    :cond_3
    const-string v1, "android.os.action.POWER_SAVE_MODE_CHANGED"

    invoke-static {v0, v1}, LX/06P;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 15567
    :try_start_0
    iget-object v0, p0, LX/056;->U:Ljava/lang/reflect/Method;

    if-nez v0, :cond_4

    .line 15568
    const-class v0, Landroid/os/PowerManager;

    const-string v1, "isPowerSaveMode"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/056;->U:Ljava/lang/reflect/Method;

    .line 15569
    :cond_4
    iget-object v0, p0, LX/056;->U:Ljava/lang/reflect/Method;

    iget-object v1, p0, LX/056;->K:Landroid/os/PowerManager;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 15570
    iget-object v1, p0, LX/056;->d:LX/05h;

    .line 15571
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "pow"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v2}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 15572
    const-string v3, "mqtt_device_state"

    invoke-virtual {v1, v3, v2}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 15573
    goto :goto_1

    .line 15574
    :catch_0
    move-exception v0

    .line 15575
    const-string v1, "FbnsConnectionManager"

    const-string v2, "exception/NoSuchMethodException"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 15576
    :catch_1
    move-exception v0

    .line 15577
    const-string v1, "FbnsConnectionManager"

    const-string v2, "exception/IllegalAccessException"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 15578
    :catch_2
    move-exception v0

    .line 15579
    const-string v1, "FbnsConnectionManager"

    const-string v2, "exception/InvocationTargetException"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 15580
    :cond_5
    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-static {v0, v1}, LX/06P;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15581
    iget-object v0, p0, LX/056;->d:LX/05h;

    iget-object v1, p0, LX/056;->L:LX/059;

    invoke-virtual {v1}, LX/059;->h()J

    move-result-wide v2

    iget-object v1, p0, LX/056;->L:LX/059;

    invoke-virtual {v1}, LX/059;->d()Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, LX/05h;->a(JLandroid/net/NetworkInfo;)V

    .line 15582
    iget-object v0, p0, LX/056;->L:LX/059;

    invoke-virtual {v0}, LX/059;->c()Z

    move-result v0

    .line 15583
    iget-object v1, p0, LX/056;->M:Landroid/content/Context;

    sget-object v2, LX/01p;->h:LX/01q;

    invoke-static {v1, v2}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v1

    move-object v1, v1

    .line 15584
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "mqtt/network_state"

    iget-object v3, p0, LX/056;->L:LX/059;

    invoke-virtual {v3}, LX/059;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 15585
    iget-object v1, p0, LX/056;->L:LX/059;

    invoke-virtual {v1}, LX/059;->g()J

    move-result-wide v2

    .line 15586
    const-string v1, "FbnsConnectionManager"

    const-string v4, "receiver/network; becameConnected=%b"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v1, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15587
    iget-wide v4, p0, LX/056;->J:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_6

    invoke-virtual {p0}, LX/056;->g()Z

    move-result v1

    if-nez v1, :cond_1

    .line 15588
    :cond_6
    iput-wide v2, p0, LX/056;->J:J

    .line 15589
    iget-object v1, p0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/056;->I:J

    .line 15590
    if-eqz v0, :cond_7

    .line 15591
    sget-object v0, LX/06f;->CONNECTIVITY_CHANGED:LX/06f;

    invoke-virtual {p0, v0}, LX/056;->a(LX/06f;)V

    goto/16 :goto_1

    .line 15592
    :cond_7
    invoke-static {p0}, LX/056;->v(LX/056;)V

    goto/16 :goto_1

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method private static c(LX/072;)Z
    .locals 1

    .prologue
    .line 15593
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/072;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/072;)Z
    .locals 1
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 15594
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/072;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(LX/072;)I
    .locals 12

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v6, 0x0

    const-wide/16 v2, 0x0

    .line 15595
    invoke-virtual {p1}, LX/072;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15596
    iget-wide v10, p1, LX/072;->C:J

    move-wide v0, v10

    .line 15597
    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 15598
    iget-object v0, p0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    .line 15599
    iget-wide v10, p1, LX/072;->C:J

    move-wide v4, v10

    .line 15600
    sub-long/2addr v0, v4

    .line 15601
    :goto_0
    iget-object v4, p0, LX/056;->r:LX/04p;

    invoke-virtual {v4}, LX/04p;->b()LX/04q;

    move-result-object v4

    iget v4, v4, LX/04q;->g:I

    int-to-long v4, v4

    mul-long/2addr v4, v8

    .line 15602
    sub-long v0, v4, v0

    .line 15603
    cmp-long v7, v0, v2

    if-gez v7, :cond_0

    .line 15604
    :goto_1
    div-long v0, v2, v8

    long-to-int v0, v0

    .line 15605
    const-string v1, "FbnsConnectionManager"

    const-string v2, "connection/connecting; timeoutRemain=%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15606
    :goto_2
    return v0

    .line 15607
    :cond_0
    cmp-long v2, v0, v4

    if-lez v2, :cond_2

    move-wide v2, v4

    .line 15608
    goto :goto_1

    :cond_1
    move v0, v6

    .line 15609
    goto :goto_2

    :cond_2
    move-wide v2, v0

    goto :goto_1

    :cond_3
    move-wide v0, v2

    goto :goto_0
.end method

.method public static s(LX/056;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 15610
    iget-object v0, p0, LX/056;->j:LX/05i;

    const-class v1, LX/06p;

    invoke-virtual {v0, v1}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06p;

    sget-object v1, LX/06w;->CountConnectAttempt:LX/06w;

    invoke-virtual {v0, v1}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 15611
    iget-object v0, p0, LX/056;->W:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v1, p0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 15612
    iget-object v0, p0, LX/056;->r:LX/04p;

    invoke-virtual {v0}, LX/04p;->b()LX/04q;

    move-result-object v0

    iget v0, v0, LX/04q;->s:I

    .line 15613
    const-string v1, "FbnsConnectionManager"

    const-string v2, "thread/set_priority; priority=%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15614
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setPriority(I)V

    .line 15615
    invoke-virtual {p0}, LX/056;->j()V

    .line 15616
    iget-object v0, p0, LX/056;->m:LX/072;

    if-eqz v0, :cond_1

    .line 15617
    const/4 v4, 0x0

    .line 15618
    const-string v0, "FbnsConnectionManager"

    const-string v1, "Using preemptive client op %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, LX/056;->n:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15619
    iget-object v0, p0, LX/056;->m:LX/072;

    .line 15620
    const/4 v1, 0x0

    iput-object v1, p0, LX/056;->m:LX/072;

    .line 15621
    iput v4, p0, LX/056;->n:I

    .line 15622
    move-object v0, v0

    .line 15623
    :goto_0
    monitor-enter p0

    .line 15624
    :try_start_0
    iget-object v1, p0, LX/056;->b:LX/072;

    .line 15625
    iput-object v0, p0, LX/056;->b:LX/072;

    .line 15626
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15627
    if-eqz v1, :cond_0

    .line 15628
    const-string v0, "FbnsConnectionManager"

    const-string v2, "connecting new client without disconnecting old one"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15629
    sget-object v0, LX/0AX;->EXPIRE_CONNECTION:LX/0AX;

    sget-object v2, LX/0BA;->DISCONNECTED:LX/0BA;

    invoke-virtual {p0, v1, v0, v2}, LX/056;->a(LX/072;LX/0AX;LX/0BA;)Ljava/util/concurrent/Future;

    .line 15630
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/056;->f:J

    .line 15631
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/connecting"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15632
    iget-object v0, p0, LX/056;->D:LX/055;

    .line 15633
    iget-object v1, v0, LX/055;->a:LX/051;

    .line 15634
    const-string v2, "MqttPushService"

    const-string v3, "connection/connecting"

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v0}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15635
    sget-object v2, LX/053;->CONNECTING:LX/053;

    invoke-virtual {v1, v2}, LX/051;->a(LX/053;)Z

    .line 15636
    return-void

    .line 15637
    :cond_1
    invoke-virtual {p0}, LX/056;->c()LX/072;

    move-result-object v0

    goto :goto_0

    .line 15638
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static v(LX/056;)V
    .locals 1

    .prologue
    .line 15639
    iget-object v0, p0, LX/056;->z:LX/06K;

    invoke-virtual {v0}, LX/06K;->d()V

    .line 15640
    iget-object v0, p0, LX/056;->A:LX/06T;

    invoke-virtual {v0}, LX/06T;->d()V

    .line 15641
    return-void
.end method

.method public static x(LX/056;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 15706
    iget-object v0, p0, LX/056;->m:LX/072;

    .line 15707
    if-eqz v0, :cond_0

    .line 15708
    iput-object v2, p0, LX/056;->m:LX/072;

    .line 15709
    const/4 v1, 0x0

    iput v1, p0, LX/056;->n:I

    .line 15710
    iput-object v2, v0, LX/072;->E:LX/077;

    .line 15711
    sget-object v1, LX/0AX;->ABORTED_PREEMPTIVE_RECONNECT:LX/0AX;

    invoke-virtual {v0, v1}, LX/072;->a(LX/0AX;)Ljava/util/concurrent/Future;

    .line 15712
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0AL;)I
    .locals 1

    .prologue
    .line 15642
    invoke-static {p2}, LX/05V;->b(Ljava/lang/String;)[B

    move-result-object v0

    .line 15643
    const/4 p2, 0x0

    invoke-virtual {p0, p1, v0, p3, p2}, LX/056;->a(Ljava/lang/String;[BLX/0AL;LX/0AM;)I

    move-result p2

    move v0, p2

    .line 15644
    return v0
.end method

.method public final a(Ljava/lang/String;[BLX/0AL;LX/0AM;)I
    .locals 9
    .param p4    # LX/0AM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 15645
    invoke-virtual {p0}, LX/056;->e()I

    move-result v5

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v8}, LX/056;->a(Ljava/lang/String;[BLX/0AL;LX/0AM;IJLjava/lang/String;)LX/05B;

    move-result-object v0

    .line 15646
    invoke-virtual {v0}, LX/05B;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 15647
    const/4 v0, -0x1

    .line 15648
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0B2;

    invoke-interface {v0}, LX/0B2;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final a()J
    .locals 6

    .prologue
    .line 15649
    iget-object v0, p0, LX/056;->b:LX/072;

    .line 15650
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/072;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15651
    iget-wide v4, v0, LX/072;->C:J

    move-wide v0, v4

    .line 15652
    iget-object v2, p0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 15653
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[BLX/0AL;LX/0AM;IJLjava/lang/String;)LX/05B;
    .locals 16
    .param p4    # LX/0AM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[B",
            "LX/0AL;",
            "LX/0AM;",
            "IJ",
            "Ljava/lang/String;",
            ")",
            "LX/05B",
            "<",
            "LX/0B2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15654
    move-object/from16 v0, p3

    iget v2, v0, LX/0AL;->mValue:I

    sget-object v3, LX/0AL;->ASSURED_DELIVERY:LX/0AL;

    iget v3, v3, LX/0AL;->mValue:I

    if-ge v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/01n;->a(Z)V

    .line 15655
    move-object/from16 v0, p0

    iget-object v3, v0, LX/056;->b:LX/072;

    .line 15656
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/072;->b()Z

    move-result v2

    if-nez v2, :cond_2

    .line 15657
    :cond_0
    invoke-static {}, LX/05B;->c()LX/05B;

    move-result-object v2

    .line 15658
    :goto_1
    return-object v2

    .line 15659
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 15660
    :cond_2
    :try_start_0
    invoke-virtual {v3}, LX/072;->m()I

    move-result v5

    .line 15661
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, LX/056;->e(LX/072;)I

    move-result v2

    add-int v6, p5, v2

    .line 15662
    sget-object v2, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    move-object/from16 v0, p3

    if-ne v0, v2, :cond_4

    .line 15663
    move-object/from16 v0, p0

    iget-object v2, v0, LX/056;->k:LX/06J;

    sget-object v4, LX/07S;->PUBACK:LX/07S;

    invoke-virtual {v2, v3, v4, v5, v6}, LX/06J;->a(LX/072;LX/07S;II)LX/0B1;

    move-result-object v2

    :goto_2
    move-object v6, v3

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move v10, v5

    move-object/from16 v11, p4

    move-wide/from16 v12, p6

    move-object/from16 v14, p8

    .line 15664
    invoke-virtual/range {v6 .. v14}, LX/072;->a(Ljava/lang/String;[BLX/0AL;ILX/0AM;JLjava/lang/String;)I

    .line 15665
    const-string v4, "/mqtt_health_stats"

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 15666
    sget-object v4, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    move-object/from16 v0, p3

    if-ne v0, v4, :cond_3

    .line 15667
    invoke-virtual/range {p0 .. p0}, LX/056;->f()V

    .line 15668
    :cond_3
    invoke-static {v2}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v2

    goto :goto_1

    .line 15669
    :cond_4
    new-instance v2, LX/0B1;

    sget-object v4, LX/07S;->PUBACK:LX/07S;

    int-to-long v6, v6

    invoke-direct/range {v2 .. v7}, LX/0B1;-><init>(LX/072;LX/07S;IJ)V

    .line 15670
    invoke-virtual {v2}, LX/0B1;->b()V
    :try_end_0
    .catch LX/0BK; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 15671
    :catch_0
    move-exception v2

    .line 15672
    const-string v4, "FbnsConnectionManager"

    const-string v5, "exception/publish"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v2, v5, v6}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15673
    sget-object v4, LX/0AX;->SEND_FAILURE:LX/0AX;

    sget-object v5, LX/0BA;->CONNECTION_LOST:LX/0BA;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, LX/056;->a(LX/072;LX/0AX;LX/0BA;)Ljava/util/concurrent/Future;

    .line 15674
    throw v2
.end method

.method public a(LX/072;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/072;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15675
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/072;LX/0AX;LX/0BA;)Ljava/util/concurrent/Future;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/072;",
            "LX/0AX;",
            "LX/0BA;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 15687
    monitor-enter p0

    .line 15688
    :try_start_0
    iget-object v0, p0, LX/056;->b:LX/072;

    if-ne v0, p1, :cond_2

    .line 15689
    const/4 v0, 0x0

    iput-object v0, p0, LX/056;->b:LX/072;

    .line 15690
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15691
    sget-object v0, LX/07C;->a:LX/07C;

    .line 15692
    if-eqz p1, :cond_0

    .line 15693
    invoke-virtual {p1}, LX/072;->e()Z

    move-result v1

    .line 15694
    iput-object v4, p1, LX/072;->E:LX/077;

    .line 15695
    invoke-virtual {p1, p2}, LX/072;->a(LX/0AX;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 15696
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, LX/056;->h:J

    .line 15697
    :cond_0
    if-nez v1, :cond_1

    .line 15698
    sget-object v1, LX/06b;->a:LX/06b;

    move-object v1, v1

    .line 15699
    invoke-static {p0, p3, v1}, LX/056;->a$redex0(LX/056;LX/0BA;LX/05B;)V

    .line 15700
    :cond_1
    return-object v0

    .line 15701
    :cond_2
    :try_start_1
    const-string v0, "FbnsConnectionManager"

    const-string v2, "Disconnecting old client after a new one already created"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 15702
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/0AX;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0AX;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 15703
    iget-object v0, p0, LX/056;->t:LX/06I;

    invoke-virtual {v0}, LX/06I;->d()V

    .line 15704
    invoke-static {p0}, LX/056;->x(LX/056;)V

    .line 15705
    iget-object v0, p0, LX/056;->b:LX/072;

    sget-object v1, LX/0BA;->BY_REQUEST:LX/0BA;

    invoke-virtual {p0, v0, p1, v1}, LX/056;->a(LX/072;LX/0AX;LX/0BA;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/06f;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 15449
    invoke-virtual {p0}, LX/056;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 15450
    iget-object v0, p0, LX/056;->A:LX/06T;

    invoke-virtual {v0}, LX/06T;->b()V

    .line 15451
    :goto_0
    iget-object v0, p0, LX/056;->D:LX/055;

    invoke-virtual {v0}, LX/055;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 15452
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/should_not_connect"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15453
    sget-object v0, LX/0AX;->KICK_SHOULD_NOT_CONNECT:LX/0AX;

    invoke-virtual {p0, v0}, LX/056;->a(LX/0AX;)Ljava/util/concurrent/Future;

    .line 15454
    :goto_1
    return-void

    .line 15455
    :cond_0
    invoke-virtual {p0}, LX/056;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 15456
    iget-wide v0, p0, LX/056;->I:J

    iget-wide v2, p0, LX/056;->o:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 15457
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/reconnect_due_to_network_change"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15458
    :cond_1
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/kick_connect"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15459
    invoke-virtual {p0}, LX/056;->j()V

    .line 15460
    iget-object v0, p0, LX/056;->j:LX/05i;

    .line 15461
    iput-object p1, v0, LX/05i;->n:LX/06f;

    .line 15462
    iget-boolean v0, p0, LX/056;->E:Z

    if-eqz v0, :cond_5

    sget-object v0, LX/06f;->PERSISTENT_KICK_SCREEN_CHANGE:LX/06f;

    invoke-virtual {p1, v0}, LX/06f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 15463
    iget-object v0, p0, LX/056;->t:LX/06I;

    invoke-virtual {v0}, LX/06I;->e()Z

    .line 15464
    :goto_2
    goto :goto_1

    .line 15465
    :cond_2
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/already_connected"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 15466
    :cond_3
    invoke-virtual {p0}, LX/056;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15467
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/already_connecting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 15468
    :cond_4
    iget-object v0, p0, LX/056;->z:LX/06K;

    invoke-virtual {v0}, LX/06K;->b()V

    goto :goto_0

    .line 15469
    :cond_5
    iget-boolean v0, p0, LX/056;->H:Z

    if-eqz v0, :cond_6

    sget-object v0, LX/06f;->CONNECTIVITY_CHANGED:LX/06f;

    invoke-virtual {p1, v0}, LX/06f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 15470
    iget-object v0, p0, LX/056;->t:LX/06I;

    invoke-virtual {v0}, LX/06I;->e()Z

    goto :goto_2

    .line 15471
    :cond_6
    iget-object v0, p0, LX/056;->t:LX/06I;

    invoke-virtual {v0}, LX/06I;->a()Ljava/util/concurrent/Future;

    goto :goto_2
.end method

.method public final a(Landroid/content/Context;LX/055;Ljava/lang/String;LX/06F;LX/06I;LX/05F;LX/05G;LX/05F;LX/05G;LX/06J;LX/06K;LX/06T;Ljava/util/concurrent/atomic/AtomicInteger;LX/05h;LX/05i;Landroid/os/Handler;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;LX/04p;LX/05H;LX/05L;Landroid/os/PowerManager;LX/059;LX/05b;LX/05e;Ljava/util/concurrent/ExecutorService;Ljava/util/List;IZZZZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$ConnectionManagerCallbacks;",
            "Ljava/lang/String;",
            "Lcom/facebook/rti/common/util/NonInjectProvider1",
            "<",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;",
            "LX/072;",
            ">;",
            "LX/06I;",
            "LX/05F;",
            "LX/05G;",
            "LX/05F;",
            "LX/05G;",
            "LX/06J;",
            "LX/06K;",
            "LX/06T;",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            "LX/05h;",
            "LX/05i;",
            "Landroid/os/Handler;",
            "Lcom/facebook/rti/common/time/MonotonicClock;",
            "LX/04p;",
            "LX/05H;",
            "LX/05L;",
            "Landroid/os/PowerManager;",
            "LX/059;",
            "LX/05b;",
            "LX/05e;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;IZZZZ)V"
        }
    .end annotation

    .prologue
    .line 15472
    iput-object p1, p0, LX/056;->M:Landroid/content/Context;

    .line 15473
    iput-object p2, p0, LX/056;->D:LX/055;

    .line 15474
    iput-object p3, p0, LX/056;->G:Ljava/lang/String;

    .line 15475
    iput-object p4, p0, LX/056;->s:LX/06F;

    .line 15476
    iput-object p5, p0, LX/056;->t:LX/06I;

    .line 15477
    iput-object p7, p0, LX/056;->v:LX/05G;

    .line 15478
    iput-object p6, p0, LX/056;->u:LX/05F;

    .line 15479
    iput-object p9, p0, LX/056;->x:LX/05G;

    .line 15480
    iput-object p8, p0, LX/056;->w:LX/05F;

    .line 15481
    iput-object p10, p0, LX/056;->k:LX/06J;

    .line 15482
    move-object/from16 v0, p11

    iput-object v0, p0, LX/056;->z:LX/06K;

    .line 15483
    move-object/from16 v0, p12

    iput-object v0, p0, LX/056;->A:LX/06T;

    .line 15484
    move-object/from16 v0, p13

    iput-object v0, p0, LX/056;->F:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 15485
    move-object/from16 v0, p14

    iput-object v0, p0, LX/056;->d:LX/05h;

    .line 15486
    move-object/from16 v0, p15

    iput-object v0, p0, LX/056;->j:LX/05i;

    .line 15487
    move-object/from16 v0, p16

    iput-object v0, p0, LX/056;->y:Landroid/os/Handler;

    .line 15488
    move-object/from16 v0, p17

    iput-object v0, p0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 15489
    move-object/from16 v0, p18

    iput-object v0, p0, LX/056;->r:LX/04p;

    .line 15490
    move-object/from16 v0, p19

    iput-object v0, p0, LX/056;->a:LX/05H;

    .line 15491
    move-object/from16 v0, p20

    iput-object v0, p0, LX/056;->p:LX/05L;

    .line 15492
    move-object/from16 v0, p21

    iput-object v0, p0, LX/056;->K:Landroid/os/PowerManager;

    .line 15493
    move-object/from16 v0, p22

    iput-object v0, p0, LX/056;->L:LX/059;

    .line 15494
    move-object/from16 v0, p23

    iput-object v0, p0, LX/056;->Q:LX/05b;

    .line 15495
    move-object/from16 v0, p24

    iput-object v0, p0, LX/056;->R:LX/05e;

    .line 15496
    move-object/from16 v0, p25

    iput-object v0, p0, LX/056;->l:Ljava/util/concurrent/ExecutorService;

    .line 15497
    iget-object v1, p0, LX/056;->t:LX/06I;

    iget-object v2, p0, LX/056;->X:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, LX/06I;->a(Ljava/lang/Runnable;)V

    .line 15498
    iget-object v1, p0, LX/056;->z:LX/06K;

    iget-object v2, p0, LX/056;->Y:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, LX/06K;->a(Ljava/lang/Runnable;)V

    .line 15499
    iget-object v1, p0, LX/056;->A:LX/06T;

    iget-object v2, p0, LX/056;->Z:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, LX/06T;->a(Ljava/lang/Runnable;)V

    .line 15500
    invoke-interface/range {p26 .. p26}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0AF;

    .line 15501
    iget-object v3, p0, LX/056;->e:Ljava/util/Map;

    iget-object v4, v1, LX/0AF;->a:Ljava/lang/String;

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 15502
    :cond_0
    move/from16 v0, p27

    iput v0, p0, LX/056;->C:I

    .line 15503
    iget-object v1, p0, LX/056;->r:LX/04p;

    invoke-virtual {v1}, LX/04p;->a()V

    .line 15504
    iget v1, p0, LX/056;->C:I

    if-nez v1, :cond_1

    .line 15505
    iget-object v1, p0, LX/056;->r:LX/04p;

    invoke-virtual {v1}, LX/04p;->b()LX/04q;

    move-result-object v1

    iget v1, v1, LX/04q;->h:I

    iput v1, p0, LX/056;->C:I

    .line 15506
    :cond_1
    move/from16 v0, p28

    iput-boolean v0, p0, LX/056;->E:Z

    .line 15507
    move/from16 v0, p29

    iput-boolean v0, p0, LX/056;->H:Z

    .line 15508
    iget-object v1, p0, LX/056;->r:LX/04p;

    invoke-virtual {v1}, LX/04p;->b()LX/04q;

    move-result-object v1

    iput-object v1, p0, LX/056;->N:LX/04q;

    .line 15509
    move/from16 v0, p30

    iput-boolean v0, p0, LX/056;->S:Z

    .line 15510
    move/from16 v0, p31

    iput-boolean v0, p0, LX/056;->T:Z

    .line 15511
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 15307
    iget-object v0, p0, LX/056;->r:LX/04p;

    invoke-virtual {v0}, LX/04p;->b()LX/04q;

    move-result-object v0

    iget v0, v0, LX/04q;->w:I

    .line 15308
    if-gez v0, :cond_1

    .line 15309
    :cond_0
    :goto_0
    return-void

    .line 15310
    :cond_1
    iget-object v1, p0, LX/056;->b:LX/072;

    .line 15311
    if-eqz v1, :cond_0

    .line 15312
    iget-object v2, p0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    invoke-virtual {v1}, LX/072;->g()J

    move-result-wide v4

    sub-long/2addr v2, v4

    int-to-long v0, v0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 15313
    invoke-virtual {p0, p1}, LX/056;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15314
    iget-object v1, p0, LX/056;->e:Ljava/util/Map;

    monitor-enter v1

    .line 15315
    :try_start_0
    invoke-virtual {p0, p1, p2}, LX/056;->b(Ljava/util/List;Ljava/util/List;)Landroid/util/Pair;

    move-result-object v0

    .line 15316
    if-eqz v0, :cond_0

    .line 15317
    iget-object v2, p0, LX/056;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$5;

    invoke-direct {v3, p0, v0}, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$5;-><init>(LX/056;Landroid/util/Pair;)V

    const v0, 0x253e2c8b

    invoke-static {v2, v3, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 15318
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;[BJLX/0AM;JLjava/lang/String;)Z
    .locals 9
    .param p5    # LX/0AM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 15319
    const-string v0, "FbnsConnectionManager"

    const-string v1, "send/publishAndWait; topic=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15320
    sget-object v3, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    invoke-virtual {p0}, LX/056;->e()I

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p5

    move-wide v6, p6

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, LX/056;->a(Ljava/lang/String;[BLX/0AL;LX/0AM;IJLjava/lang/String;)LX/05B;

    move-result-object v0

    .line 15321
    invoke-virtual {v0}, LX/05B;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 15322
    const/4 v0, 0x0

    .line 15323
    :goto_0
    return v0

    .line 15324
    :cond_0
    :try_start_0
    const-string v1, "FbnsConnectionManager"

    const-string v2, "send/publishAndWait; topic=%s, operation=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15325
    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0B2;

    invoke-interface {v0, p3, p4}, LX/0B2;->a(J)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1

    .line 15326
    const/4 v0, 0x1

    goto :goto_0

    .line 15327
    :catch_0
    move-exception v0

    .line 15328
    const-string v1, "FbnsConnectionManager"

    const-string v2, "send/publishAndWait/failed; topic=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15329
    throw v0

    .line 15330
    :catch_1
    move-exception v0

    .line 15331
    throw v0
.end method

.method public final b(Ljava/util/List;Ljava/util/List;)Landroid/util/Pair;
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mSubscribedTopics"
    .end annotation

    .prologue
    .line 15332
    if-eqz p1, :cond_1

    .line 15333
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0AF;

    .line 15334
    iget-object v2, p0, LX/056;->e:Ljava/util/Map;

    iget-object v3, v0, LX/0AF;->a:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 15335
    iget-object v2, p0, LX/056;->e:Ljava/util/Map;

    iget-object v3, v0, LX/0AF;->a:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 15336
    :cond_1
    if-eqz p2, :cond_3

    .line 15337
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 15338
    iget-object v2, p0, LX/056;->e:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 15339
    iget-object v2, p0, LX/056;->e:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 15340
    :cond_3
    iget-object v0, p0, LX/056;->b:LX/072;

    .line 15341
    if-eqz v0, :cond_4

    .line 15342
    iget-object v1, p0, LX/056;->e:Ljava/util/Map;

    invoke-virtual {v0, v1}, LX/072;->a(Ljava/util/Map;)Landroid/util/Pair;

    move-result-object v0

    .line 15343
    :goto_2
    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b()V
    .locals 5
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 15439
    iget-object v0, p0, LX/056;->D:LX/055;

    invoke-virtual {v0}, LX/055;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 15440
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/kick/disabled_by_service."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15441
    sget-object v0, LX/0AX;->KICK_SHOULD_NOT_CONNECT:LX/0AX;

    invoke-virtual {p0, v0}, LX/056;->a(LX/0AX;)Ljava/util/concurrent/Future;

    .line 15442
    :goto_0
    return-void

    .line 15443
    :cond_0
    iget-object v0, p0, LX/056;->b:LX/072;

    .line 15444
    if-nez v0, :cond_2

    .line 15445
    invoke-static {p0}, LX/056;->s(LX/056;)V

    .line 15446
    :cond_1
    :goto_1
    const-string v1, "FbnsConnectionManager"

    const-string v2, "connection/kick; isConnectedOrConnecting=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, LX/056;->c(LX/072;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 15447
    :cond_2
    invoke-virtual {v0}, LX/072;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 15448
    sget-object v1, LX/0BA;->DISCONNECTED:LX/0BA;

    invoke-static {p0, v0, v1}, LX/056;->a(LX/056;LX/072;LX/0BA;)V

    goto :goto_1
.end method

.method public final b(J)V
    .locals 7

    .prologue
    .line 15344
    iget-object v0, p0, LX/056;->b:LX/072;

    .line 15345
    iget-object v1, p0, LX/056;->j:LX/05i;

    sget-object v2, LX/06f;->EXPIRE_CONNECTION:LX/06f;

    .line 15346
    iput-object v2, v1, LX/05i;->n:LX/06f;

    .line 15347
    if-eqz v0, :cond_0

    .line 15348
    iget-wide v5, v0, LX/072;->C:J

    move-wide v2, v5

    .line 15349
    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    .line 15350
    sget-object v1, LX/0BA;->STALED_CONNECTION:LX/0BA;

    invoke-static {p0, v0, v1}, LX/056;->a(LX/056;LX/072;LX/0BA;)V

    .line 15351
    :goto_0
    return-void

    .line 15352
    :cond_0
    invoke-virtual {p0}, LX/056;->b()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 11
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x0

    .line 15353
    const-string v0, "FbnsConnectionManager"

    const-string v1, "send/keepalive"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15354
    iget-object v1, p0, LX/056;->b:LX/072;

    .line 15355
    :try_start_0
    iget-object v0, p0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    .line 15356
    iget-wide v4, p0, LX/056;->q:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    iget-object v0, p0, LX/056;->r:LX/04p;

    invoke-virtual {v0}, LX/04p;->b()LX/04q;

    move-result-object v0

    iget v0, v0, LX/04q;->q:I

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 15357
    :cond_0
    :goto_0
    return-void

    .line 15358
    :cond_1
    iput-wide v2, p0, LX/056;->q:J

    .line 15359
    sget-object v0, LX/07L;->a:LX/07L;

    move-object v0, v0

    .line 15360
    iput-object p1, v0, LX/07L;->c:Ljava/lang/String;

    .line 15361
    iget-boolean v0, p0, LX/056;->S:Z

    if-eqz v0, :cond_3

    .line 15362
    iget-object v0, p0, LX/056;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 15363
    iget-object v0, p0, LX/056;->j:LX/05i;

    const-class v2, LX/0BF;

    invoke-virtual {v0, v2}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/0BF;

    sget-object v2, LX/0BG;->ForegroundPing:LX/0BG;

    invoke-virtual {v0, v2}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 15364
    :goto_1
    invoke-static {v1}, LX/056;->d(LX/072;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15365
    iget-wide v9, v1, LX/072;->C:J

    move-wide v2, v9

    .line 15366
    iget-object v0, p0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    .line 15367
    sub-long v2, v4, v2

    .line 15368
    iget-object v0, p0, LX/056;->j:LX/05i;

    invoke-virtual {v0, v2, v3}, LX/05i;->b(J)LX/0Ha;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, LX/056;->a$redex0(LX/056;LX/0Ha;Z)V
    :try_end_0
    .catch LX/0BK; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 15369
    :catch_0
    move-exception v0

    .line 15370
    const-string v2, "FbnsConnectionManager"

    const-string v3, "exception/send_keepalive"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15371
    sget-object v0, LX/0AX;->SEND_FAILURE:LX/0AX;

    sget-object v2, LX/0BA;->CONNECTION_LOST:LX/0BA;

    invoke-virtual {p0, v1, v0, v2}, LX/056;->a(LX/072;LX/0AX;LX/0BA;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 15372
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/056;->j:LX/05i;

    const-class v2, LX/0BF;

    invoke-virtual {v0, v2}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/0BF;

    sget-object v2, LX/0BG;->BackgroundPing:LX/0BG;

    invoke-virtual {v0, v2}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    goto :goto_1

    .line 15373
    :cond_3
    invoke-static {v1}, LX/056;->d(LX/072;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 15374
    iget-object v0, p0, LX/056;->k:LX/06J;

    sget-object v2, LX/07S;->PINGRESP:LX/07S;

    const/4 v3, -0x1

    invoke-virtual {p0}, LX/056;->e()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/06J;->a(LX/072;LX/07S;II)LX/0B1;

    .line 15375
    invoke-virtual {v1}, LX/072;->l()V
    :try_end_1
    .catch LX/0BK; {:try_start_1 .. :try_end_1} :catch_0

    .line 15376
    :cond_4
    goto/16 :goto_0
.end method

.method public final c()LX/072;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 15377
    iget-object v3, p0, LX/056;->e:Ljava/util/Map;

    monitor-enter v3

    .line 15378
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v4, p0, LX/056;->e:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 15379
    iget-object v4, p0, LX/056;->p:LX/05L;

    invoke-interface {v4, v0}, LX/05L;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 15380
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15381
    iget-object v3, p0, LX/056;->s:LX/06F;

    .line 15382
    check-cast v0, Ljava/util/List;

    invoke-static {v3, v0}, LX/06F;->a(LX/06F;Ljava/util/List;)LX/072;

    move-result-object v4

    move-object v0, v4

    .line 15383
    check-cast v0, LX/072;

    .line 15384
    const-string v3, "FbnsConnectionManager"

    const-string v4, "connection/create_client; mqttClient=%s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15385
    new-instance v3, LX/077;

    iget-boolean v4, p0, LX/056;->T:Z

    invoke-direct {v3, p0, v0, v4}, LX/077;-><init>(LX/056;LX/072;Z)V

    .line 15386
    iput-object v3, v0, LX/072;->E:LX/077;

    .line 15387
    invoke-virtual {p0, v0}, LX/056;->a(LX/072;)Ljava/util/List;

    move-result-object v3

    .line 15388
    iget-boolean v4, p0, LX/056;->B:Z

    .line 15389
    iget-boolean v5, p0, LX/056;->B:Z

    if-nez v5, :cond_0

    :goto_0
    iput-boolean v1, p0, LX/056;->B:Z

    .line 15390
    invoke-virtual {v0, v3, v4}, LX/072;->a(Ljava/util/List;Z)V

    .line 15391
    return-object v0

    .line 15392
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move v1, v2

    .line 15393
    goto :goto_0
.end method

.method public c(Ljava/util/List;)V
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15394
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15395
    :cond_0
    :goto_0
    return-void

    .line 15396
    :cond_1
    iget-object v1, p0, LX/056;->b:LX/072;

    .line 15397
    invoke-static {v1}, LX/056;->d(LX/072;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15398
    :try_start_0
    invoke-virtual {v1}, LX/072;->m()I

    move-result v0

    .line 15399
    iget-object v2, p0, LX/056;->k:LX/06J;

    sget-object v3, LX/07S;->SUBACK:LX/07S;

    invoke-virtual {p0}, LX/056;->e()I

    move-result v4

    invoke-virtual {v2, v1, v3, v0, v4}, LX/06J;->a(LX/072;LX/07S;II)LX/0B1;

    .line 15400
    invoke-virtual {v1, v0, p1}, LX/072;->a(ILjava/util/List;)I
    :try_end_0
    .catch LX/0BK; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 15401
    :catch_0
    move-exception v0

    .line 15402
    const-string v2, "FbnsConnectionManager"

    const-string v3, "exception/subscribe"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15403
    sget-object v0, LX/0AX;->SEND_FAILURE:LX/0AX;

    sget-object v2, LX/0BA;->CONNECTION_LOST:LX/0BA;

    invoke-virtual {p0, v1, v0, v2}, LX/056;->a(LX/072;LX/0AX;LX/0BA;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 15404
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/056;->b(Ljava/lang/String;)V

    .line 15405
    return-void
.end method

.method public d(Ljava/util/List;)V
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15406
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15407
    :cond_0
    :goto_0
    return-void

    .line 15408
    :cond_1
    iget-object v1, p0, LX/056;->b:LX/072;

    .line 15409
    invoke-static {v1}, LX/056;->d(LX/072;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15410
    :try_start_0
    invoke-virtual {v1}, LX/072;->m()I

    move-result v0

    .line 15411
    iget-object v2, p0, LX/056;->k:LX/06J;

    sget-object v3, LX/07S;->UNSUBACK:LX/07S;

    invoke-virtual {p0}, LX/056;->e()I

    move-result v4

    invoke-virtual {v2, v1, v3, v0, v4}, LX/06J;->a(LX/072;LX/07S;II)LX/0B1;

    .line 15412
    invoke-virtual {v1, v0, p1}, LX/072;->b(ILjava/util/List;)I
    :try_end_0
    .catch LX/0BK; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 15413
    :catch_0
    move-exception v0

    .line 15414
    const-string v2, "FbnsConnectionManager"

    const-string v3, "exception/unsubscribe"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15415
    sget-object v0, LX/0AX;->SEND_FAILURE:LX/0AX;

    sget-object v2, LX/0BA;->CONNECTION_LOST:LX/0BA;

    invoke-virtual {p0, v1, v0, v2}, LX/056;->a(LX/072;LX/0AX;LX/0BA;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 15416
    invoke-virtual {p0}, LX/056;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15417
    iget-object v0, p0, LX/056;->r:LX/04p;

    invoke-virtual {v0}, LX/04p;->b()LX/04q;

    move-result-object v0

    iget v0, v0, LX/04q;->h:I

    .line 15418
    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/056;->C:I

    goto :goto_0
.end method

.method public e(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15419
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 15420
    iget-object v0, p0, LX/056;->A:LX/06T;

    invoke-virtual {v0}, LX/06T;->d()V

    .line 15421
    invoke-virtual {p0}, LX/056;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15422
    iget-object v0, p0, LX/056;->A:LX/06T;

    invoke-virtual {v0}, LX/06T;->c()V

    .line 15423
    :goto_0
    return-void

    .line 15424
    :cond_0
    iget-object v0, p0, LX/056;->z:LX/06K;

    invoke-virtual {v0}, LX/06K;->c()V

    goto :goto_0
.end method

.method public f(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15425
    return-void
.end method

.method public final g()Z
    .locals 1
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 15426
    iget-object v0, p0, LX/056;->b:LX/072;

    invoke-static {v0}, LX/056;->c(LX/072;)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 15427
    iget-object v0, p0, LX/056;->b:LX/072;

    invoke-static {v0}, LX/056;->d(LX/072;)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 15428
    iget-object v0, p0, LX/056;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 15429
    invoke-virtual {p0}, LX/056;->k()I

    move-result v0

    .line 15430
    iget-object v1, p0, LX/056;->F:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 15431
    invoke-virtual {p0}, LX/056;->f()V

    .line 15432
    :cond_0
    return-void
.end method

.method public final k()I
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 15433
    iget-object v0, p0, LX/056;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    .line 15434
    if-eqz v1, :cond_0

    .line 15435
    iget-object v0, p0, LX/056;->a:LX/05H;

    invoke-interface {v0}, LX/05H;->b()I

    move-result v0

    .line 15436
    :goto_0
    const-string v2, "FbnsConnectionManager"

    const-string v3, "state/keepalive; seconds=%d, isPersistent=%b, isAppFg=%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15437
    return v0

    .line 15438
    :cond_0
    iget-object v0, p0, LX/056;->a:LX/05H;

    invoke-interface {v0}, LX/05H;->a()I

    move-result v0

    goto :goto_0
.end method
