.class public final LX/02r;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static volatile b:LX/02r;

.field private static final c:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Random;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/03N;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/02w;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/037;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0Pg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9650
    const/4 v0, 0x0

    sput-object v0, LX/02r;->b:LX/02r;

    .line 9651
    new-instance v0, LX/03M;

    invoke-direct {v0}, LX/03M;-><init>()V

    sput-object v0, LX/02r;->c:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private constructor <init>(Landroid/util/SparseArray;LX/0Pg;LX/03N;)V
    .locals 2
    .param p2    # LX/0Pg;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "LX/02w;",
            ">;",
            "LX/0Pg;",
            "LX/03N;",
            ")V"
        }
    .end annotation

    .prologue
    .line 9644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9645
    iput-object p1, p0, LX/02r;->d:Landroid/util/SparseArray;

    .line 9646
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/02r;->e:Ljava/util/concurrent/atomic/AtomicReference;

    .line 9647
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0, p2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/02r;->f:Ljava/util/concurrent/atomic/AtomicReference;

    .line 9648
    iput-object p3, p0, LX/02r;->a:LX/03N;

    .line 9649
    return-void
.end method

.method private a(ILjava/lang/Object;II)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Lcom/facebook/loom/core/TraceControl$TraceStopReason;
        .end annotation
    .end param

    .prologue
    .line 9631
    iget-object v0, p0, LX/02r;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/037;

    .line 9632
    if-nez v0, :cond_1

    .line 9633
    :cond_0
    :goto_0
    return-void

    .line 9634
    :cond_1
    iget v1, v0, LX/037;->c:I

    and-int/2addr v1, p1

    if-eqz v1, :cond_0

    .line 9635
    iget-object v1, v0, LX/037;->d:LX/02w;

    iget v2, v0, LX/037;->g:I

    iget-object v3, v0, LX/037;->f:Ljava/lang/Object;

    invoke-interface {v1, v2, v3, p4, p2}, LX/02w;->a(ILjava/lang/Object;ILjava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9636
    iget-object v1, p0, LX/02r;->e:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 9637
    const-string v0, "LoomTraceControl"

    const-string v1, "Could not reset trace context to null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 9638
    :cond_2
    monitor-enter p0

    .line 9639
    packed-switch p3, :pswitch_data_0

    .line 9640
    :goto_1
    :try_start_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 9641
    :pswitch_0
    :try_start_1
    invoke-static {}, Lcom/facebook/loom/logger/Logger;->c()V

    .line 9642
    iget-object v1, p0, LX/02r;->a:LX/03N;

    new-instance v2, LX/037;

    const/16 v3, 0x72

    invoke-direct {v2, v0, v3}, LX/037;-><init>(LX/037;S)V

    invoke-virtual {v1, v2}, LX/03N;->d(LX/037;)V

    goto :goto_1

    .line 9643
    :pswitch_1
    iget-object v1, p0, LX/02r;->a:LX/03N;

    invoke-virtual {v1, v0}, LX/03N;->c(LX/037;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/util/SparseArray;LX/02q;LX/0Pg;)V
    .locals 3
    .param p1    # LX/02q;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0Pg;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "LX/02w;",
            ">;",
            "LX/02q;",
            "LX/0Pg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 9622
    sget-object v0, LX/02r;->b:LX/02r;

    if-nez v0, :cond_1

    .line 9623
    const-class v1, LX/02r;

    monitor-enter v1

    .line 9624
    :try_start_0
    sget-object v0, LX/02r;->b:LX/02r;

    if-nez v0, :cond_0

    .line 9625
    new-instance v0, LX/02r;

    new-instance v2, LX/03N;

    invoke-direct {v2, p1}, LX/03N;-><init>(LX/02q;)V

    invoke-direct {v0, p0, p2, v2}, LX/02r;-><init>(Landroid/util/SparseArray;LX/0Pg;LX/03N;)V

    sput-object v0, LX/02r;->b:LX/02r;

    .line 9626
    monitor-exit v1

    .line 9627
    return-void

    .line 9628
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "TraceControl already initialized"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9629
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 9630
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "TraceControl already initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static e()J
    .locals 4

    .prologue
    .line 9620
    :cond_0
    sget-object v0, LX/02r;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 9621
    return-wide v0
.end method


# virtual methods
.method public final a(ILjava/lang/Object;I)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 9618
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0, p3}, LX/02r;->a(ILjava/lang/Object;II)V

    .line 9619
    return-void
.end method

.method public final a(JS)V
    .locals 5

    .prologue
    .line 9652
    iget-object v0, p0, LX/02r;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/037;

    .line 9653
    if-eqz v0, :cond_0

    iget-wide v2, v0, LX/037;->a:J

    cmp-long v1, v2, p1

    if-eqz v1, :cond_1

    .line 9654
    :cond_0
    :goto_0
    return-void

    .line 9655
    :cond_1
    iget-object v1, p0, LX/02r;->e:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 9656
    const-string v1, "LoomTraceControl"

    const-string v2, "Could not reset trace context to null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 9657
    :cond_2
    monitor-enter p0

    .line 9658
    :try_start_0
    iget-object v1, p0, LX/02r;->a:LX/03N;

    new-instance v2, LX/037;

    invoke-direct {v2, v0, p3}, LX/037;-><init>(LX/037;S)V

    invoke-virtual {v1, v2}, LX/03N;->d(LX/037;)V

    .line 9659
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/0Pg;)V
    .locals 2
    .param p1    # LX/0Pg;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 9615
    iget-object v0, p0, LX/02r;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pg;

    .line 9616
    iget-object v1, p0, LX/02r;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 9617
    return-void
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 9613
    iget-object v0, p0, LX/02r;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/037;

    .line 9614
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/037;->d:LX/02w;

    instance-of v1, v1, LX/02z;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/037;->d:LX/02w;

    check-cast v1, LX/02z;

    iget v0, v0, LX/037;->g:I

    invoke-interface {v1, v0, p1}, LX/02z;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IILjava/lang/Object;I)Z
    .locals 11
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 9581
    iget-object v0, p0, LX/02r;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pg;

    .line 9582
    invoke-interface {v0, p1}, LX/0Pg;->a(I)LX/0Pk;

    move-result-object v5

    .line 9583
    if-nez v5, :cond_0

    .line 9584
    const/4 v0, 0x0

    .line 9585
    :goto_0
    return v0

    .line 9586
    :cond_0
    iget-object v1, p0, LX/02r;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/037;

    .line 9587
    if-eqz v1, :cond_1

    .line 9588
    const/4 v0, 0x0

    goto :goto_0

    .line 9589
    :cond_1
    iget-object v1, p0, LX/02r;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/02w;

    .line 9590
    if-nez v6, :cond_2

    .line 9591
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unregistered controller for id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9592
    :cond_2
    invoke-interface {v6, p3, v5}, LX/02w;->a(Ljava/lang/Object;LX/0Pk;)I

    move-result v9

    .line 9593
    if-nez v9, :cond_3

    .line 9594
    const/4 v0, 0x0

    goto :goto_0

    .line 9595
    :cond_3
    invoke-static {}, LX/02r;->e()J

    move-result-wide v2

    .line 9596
    new-instance v1, LX/037;

    invoke-static {v2, v3}, LX/0Ps;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, p3, v5}, LX/02w;->b(Ljava/lang/Object;LX/0Pk;)I

    move-result v10

    move v5, p1

    move-object v7, p3

    move v8, p4

    invoke-direct/range {v1 .. v10}, LX/037;-><init>(JLjava/lang/String;ILX/02w;Ljava/lang/Object;III)V

    .line 9597
    iget-object v2, p0, LX/02r;->e:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 9598
    invoke-interface {v0}, LX/0Pg;->b()I

    move-result v0

    .line 9599
    const/4 v2, -0x1

    if-ne v0, v2, :cond_4

    .line 9600
    const/16 v0, 0x7530

    .line 9601
    :cond_4
    and-int/lit8 v2, p2, 0x1

    if-eqz v2, :cond_5

    .line 9602
    const v0, 0x7fffffff

    .line 9603
    :cond_5
    and-int/lit8 v2, p2, 0x2

    if-eqz v2, :cond_6

    .line 9604
    iget-wide v2, v1, LX/037;->a:J

    invoke-static {v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(JI)V

    .line 9605
    :goto_1
    monitor-enter p0

    .line 9606
    :try_start_0
    iget-object v2, p0, LX/02r;->a:LX/03N;

    invoke-virtual {v2}, LX/03N;->a()V

    .line 9607
    iget-object v2, p0, LX/02r;->a:LX/03N;

    invoke-virtual {v2, v1, v0}, LX/03N;->a(LX/037;I)V

    .line 9608
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9609
    const/4 v0, 0x1

    goto :goto_0

    .line 9610
    :cond_6
    iget-wide v2, v1, LX/037;->a:J

    invoke-static {v2, v3, p2, v0}, Lcom/facebook/loom/logger/Logger;->a(JII)V

    goto :goto_1

    .line 9611
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 9612
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final b(ILjava/lang/Object;I)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 9579
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, LX/02r;->a(ILjava/lang/Object;II)V

    .line 9580
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 9578
    iget-object v0, p0, LX/02r;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9576
    iget-object v0, p0, LX/02r;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/037;

    .line 9577
    if-eqz v0, :cond_0

    iget-object v0, v0, LX/037;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "AAAAAAAAAAA"

    goto :goto_0
.end method
