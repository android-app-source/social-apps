.class public LX/09t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/09t;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 23084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23085
    return-void
.end method

.method public static a(LX/0QB;)LX/09t;
    .locals 3

    .prologue
    .line 23070
    sget-object v0, LX/09t;->a:LX/09t;

    if-nez v0, :cond_1

    .line 23071
    const-class v1, LX/09t;

    monitor-enter v1

    .line 23072
    :try_start_0
    sget-object v0, LX/09t;->a:LX/09t;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 23073
    if-eqz v2, :cond_0

    .line 23074
    :try_start_1
    new-instance v0, LX/09t;

    invoke-direct {v0}, LX/09t;-><init>()V

    .line 23075
    move-object v0, v0

    .line 23076
    sput-object v0, LX/09t;->a:LX/09t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23077
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 23078
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 23079
    :cond_1
    sget-object v0, LX/09t;->a:LX/09t;

    return-object v0

    .line 23080
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 23081
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 0

    .prologue
    .line 23082
    invoke-static {}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->b()V

    .line 23083
    return-void
.end method
