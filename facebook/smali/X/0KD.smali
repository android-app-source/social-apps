.class public final enum LX/0KD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0KD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0KD;

.field public static final enum BEHIND_LIVE_WINDOW_ERROR:LX/0KD;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 40428
    new-instance v0, LX/0KD;

    const-string v1, "BEHIND_LIVE_WINDOW_ERROR"

    const-string v2, "behind_live_window_error"

    invoke-direct {v0, v1, v3, v2}, LX/0KD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0KD;->BEHIND_LIVE_WINDOW_ERROR:LX/0KD;

    .line 40429
    const/4 v0, 0x1

    new-array v0, v0, [LX/0KD;

    sget-object v1, LX/0KD;->BEHIND_LIVE_WINDOW_ERROR:LX/0KD;

    aput-object v1, v0, v3

    sput-object v0, LX/0KD;->$VALUES:[LX/0KD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40424
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40425
    iput-object p3, p0, LX/0KD;->value:Ljava/lang/String;

    .line 40426
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0KD;
    .locals 1

    .prologue
    .line 40427
    const-class v0, LX/0KD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0KD;

    return-object v0
.end method

.method public static values()[LX/0KD;
    .locals 1

    .prologue
    .line 40423
    sget-object v0, LX/0KD;->$VALUES:[LX/0KD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0KD;

    return-object v0
.end method
