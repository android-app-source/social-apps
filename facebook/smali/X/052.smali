.class public abstract LX/052;
.super Landroid/app/Service;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private volatile a:LX/05E;

.field private final b:Ljava/lang/Object;

.field private volatile c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15288
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 15289
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/052;->b:Ljava/lang/Object;

    .line 15290
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 15283
    iget-object v1, p0, LX/052;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 15284
    :try_start_0
    iget-boolean v0, p0, LX/052;->c:Z

    if-nez v0, :cond_0

    .line 15285
    invoke-virtual {p0}, LX/052;->c()V

    .line 15286
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/052;->c:Z

    .line 15287
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract a(Landroid/content/Intent;II)V
.end method

.method public a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15281
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 15282
    return-void
.end method

.method public abstract b()Landroid/os/Looper;
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public final dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15278
    invoke-virtual {p0}, LX/052;->a()V

    .line 15279
    invoke-virtual {p0, p1, p2, p3}, LX/052;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 15280
    return-void
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x6c210b7    # 7.299925E-35f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 15270
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 15271
    const-string v1, "MqttBackgroundService"

    const-string v2, "Creating service"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15272
    invoke-virtual {p0}, LX/052;->b()Landroid/os/Looper;

    move-result-object v1

    .line 15273
    if-eqz v1, :cond_0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 15274
    new-instance v2, LX/05E;

    invoke-direct {v2, p0, v1}, LX/05E;-><init>(LX/052;Landroid/os/Looper;)V

    iput-object v2, p0, LX/052;->a:LX/05E;

    .line 15275
    :goto_0
    iget-object v1, p0, LX/052;->a:LX/05E;

    invoke-virtual {v1}, LX/05E;->a()V

    .line 15276
    const v1, -0x2958358e

    invoke-static {v1, v0}, LX/02F;->d(II)V

    return-void

    .line 15277
    :cond_0
    new-instance v1, LX/0Hl;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/0Hl;-><init>(LX/052;Landroid/os/Looper;)V

    iput-object v1, p0, LX/052;->a:LX/05E;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0xa872be7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 15267
    iget-object v1, p0, LX/052;->a:LX/05E;

    invoke-virtual {v1}, LX/05E;->b()V

    .line 15268
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 15269
    const/16 v1, 0x25

    const v2, 0x1d3b73e6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 15265
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0, p2}, LX/052;->onStartCommand(Landroid/content/Intent;II)I

    .line 15266
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, -0x418d1e3e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 15263
    iget-object v1, p0, LX/052;->a:LX/05E;

    invoke-virtual {v1, p1, p2, p3}, LX/05E;->a(Landroid/content/Intent;II)V

    .line 15264
    const/4 v1, 0x1

    const/16 v2, 0x25

    const v3, -0x1aa87744

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method
