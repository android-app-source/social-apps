.class public final LX/01u;
.super Landroid/app/Instrumentation;
.source ""


# instance fields
.field public final synthetic a:LX/000;

.field private b:Landroid/app/Instrumentation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/000;Landroid/app/Instrumentation;)V
    .locals 0
    .param p2    # Landroid/app/Instrumentation;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 5746
    iput-object p1, p0, LX/01u;->a:LX/000;

    invoke-direct {p0}, Landroid/app/Instrumentation;-><init>()V

    .line 5747
    iput-object p2, p0, LX/01u;->b:Landroid/app/Instrumentation;

    .line 5748
    return-void
.end method


# virtual methods
.method public final callActivityOnCreate(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5737
    instance-of v0, p1, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 5738
    check-cast v0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;

    iput-object p2, v0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->a:Landroid/os/Bundle;

    .line 5739
    invoke-super {p0, p1, v1}, Landroid/app/Instrumentation;->callActivityOnCreate(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 5740
    :goto_0
    return-void

    .line 5741
    :cond_0
    if-eqz p2, :cond_1

    const-string v0, "com.facebook.bundleHack"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p2, v1

    .line 5742
    :cond_1
    iget-object v0, p0, LX/01u;->a:LX/000;

    iget v0, v0, LX/000;->K:I

    .line 5743
    if-lez v0, :cond_2

    instance-of v0, p1, LX/03p;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 5744
    check-cast v0, LX/03p;

    invoke-interface {v0}, LX/03p;->d()V

    .line 5745
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Instrumentation;->callActivityOnCreate(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final callActivityOnPostCreate(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 5732
    instance-of v1, p1, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;

    if-eqz v1, :cond_1

    move-object p2, v0

    .line 5733
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Instrumentation;->callActivityOnPostCreate(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 5734
    return-void

    .line 5735
    :cond_1
    if-eqz p2, :cond_0

    const-string v1, "com.facebook.bundleHack"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object p2, v0

    .line 5736
    goto :goto_0
.end method

.method public final callActivityOnRestoreInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 5749
    instance-of v1, p1, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;

    if-eqz v1, :cond_2

    move-object p2, v0

    .line 5750
    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    .line 5751
    invoke-super {p0, p1, p2}, Landroid/app/Instrumentation;->callActivityOnRestoreInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 5752
    :cond_1
    return-void

    .line 5753
    :cond_2
    if-eqz p2, :cond_0

    const-string v1, "com.facebook.bundleHack"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object p2, v0

    .line 5754
    goto :goto_0
.end method

.method public final callActivityOnSaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 5719
    instance-of v0, p1, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;

    if-eqz v0, :cond_1

    .line 5720
    check-cast p1, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;

    iget-object v0, p1, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->a:Landroid/os/Bundle;

    .line 5721
    if-nez v0, :cond_0

    .line 5722
    const-string v0, "com.facebook.bundleHack"

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 5723
    :goto_0
    return-void

    .line 5724
    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5725
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 5726
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 5727
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 5728
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5729
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 5730
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Instrumentation;->callActivityOnSaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 5731
    const-string v0, "com.facebook.bundleHack"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final newActivity(Ljava/lang/Class;Landroid/content/Context;Landroid/os/IBinder;Landroid/app/Application;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Ljava/lang/CharSequence;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/Object;)Landroid/app/Activity;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/content/Context;",
            "Landroid/os/IBinder;",
            "Landroid/app/Application;",
            "Landroid/content/Intent;",
            "Landroid/content/pm/ActivityInfo;",
            "Ljava/lang/CharSequence;",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Landroid/app/Activity;"
        }
    .end annotation

    .prologue
    .line 5718
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should be unused by framework"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final newActivity(Ljava/lang/ClassLoader;Ljava/lang/String;Landroid/content/Intent;)Landroid/app/Activity;
    .locals 3

    .prologue
    .line 5714
    iget-object v0, p0, LX/01u;->a:LX/000;

    invoke-virtual {v0, p3}, LX/000;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;

    iget-object v1, p0, LX/01u;->a:LX/000;

    invoke-direct {v0, v1}, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;-><init>(LX/000;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Instrumentation;->newActivity(Ljava/lang/ClassLoader;Ljava/lang/String;Landroid/content/Intent;)Landroid/app/Activity;

    move-result-object v0

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 5715
    iget-object v0, p0, LX/01u;->b:Landroid/app/Instrumentation;

    if-eqz v0, :cond_0

    .line 5716
    iget-object v0, p0, LX/01u;->b:Landroid/app/Instrumentation;

    invoke-virtual {v0, p1}, Landroid/app/Instrumentation;->onCreate(Landroid/os/Bundle;)V

    .line 5717
    :cond_0
    return-void
.end method
