.class public LX/01M;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[I

.field public static final b:[J

.field public static final c:[Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4603
    new-array v0, v1, [I

    sput-object v0, LX/01M;->a:[I

    .line 4604
    new-array v0, v1, [J

    sput-object v0, LX/01M;->b:[J

    .line 4605
    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, LX/01M;->c:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 4581
    mul-int/lit8 v0, p0, 0x4

    invoke-static {v0}, LX/01M;->c(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public static a([III)I
    .locals 4

    .prologue
    .line 4582
    const/4 v1, 0x0

    .line 4583
    add-int/lit8 v0, p1, -0x1

    move v2, v1

    move v1, v0

    .line 4584
    :goto_0
    if-gt v2, v1, :cond_1

    .line 4585
    add-int v0, v2, v1

    ushr-int/lit8 v0, v0, 0x1

    .line 4586
    aget v3, p0, v0

    .line 4587
    if-ge v3, p2, :cond_0

    .line 4588
    add-int/lit8 v0, v0, 0x1

    move v2, v0

    goto :goto_0

    .line 4589
    :cond_0
    if-le v3, p2, :cond_2

    .line 4590
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    goto :goto_0

    .line 4591
    :cond_1
    xor-int/lit8 v0, v2, -0x1

    :cond_2
    return v0
.end method

.method public static a([JIJ)I
    .locals 6

    .prologue
    .line 4592
    const/4 v1, 0x0

    .line 4593
    add-int/lit8 v0, p1, -0x1

    move v2, v1

    move v1, v0

    .line 4594
    :goto_0
    if-gt v2, v1, :cond_1

    .line 4595
    add-int v0, v2, v1

    ushr-int/lit8 v0, v0, 0x1

    .line 4596
    aget-wide v4, p0, v0

    .line 4597
    cmp-long v3, v4, p2

    if-gez v3, :cond_0

    .line 4598
    add-int/lit8 v0, v0, 0x1

    move v2, v0

    goto :goto_0

    .line 4599
    :cond_0
    cmp-long v1, v4, p2

    if-lez v1, :cond_2

    .line 4600
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    goto :goto_0

    .line 4601
    :cond_1
    xor-int/lit8 v0, v2, -0x1

    :cond_2
    return v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 4574
    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 4575
    mul-int/lit8 v0, p0, 0x8

    invoke-static {v0}, LX/01M;->c(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    return v0
.end method

.method private static c(I)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 4576
    const/4 v0, 0x4

    :goto_0
    const/16 v1, 0x20

    if-ge v0, v1, :cond_0

    .line 4577
    shl-int v1, v2, v0

    add-int/lit8 v1, v1, -0xc

    if-gt p0, v1, :cond_1

    .line 4578
    shl-int v0, v2, v0

    add-int/lit8 p0, v0, -0xc

    .line 4579
    :cond_0
    return p0

    .line 4580
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
