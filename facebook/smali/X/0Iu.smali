.class public final LX/0Iu;
.super LX/0Ay;
.source ""


# instance fields
.field public final synthetic a:LX/0AP;

.field private b:I

.field public final c:Lcom/facebook/xzdecoder/XzInputStream;


# direct methods
.method public constructor <init>(LX/0AP;)V
    .locals 2

    .prologue
    .line 39019
    iput-object p1, p0, LX/0Iu;->a:LX/0AP;

    invoke-direct {p0}, LX/0Ay;-><init>()V

    .line 39020
    iget-object v0, p1, LX/0AP;->c:Ljava/util/zip/ZipFile;

    iget-object v1, p1, LX/0AP;->e:Ljava/util/zip/ZipEntry;

    invoke-virtual {v0, v1}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v1

    .line 39021
    :try_start_0
    new-instance v0, Lcom/facebook/xzdecoder/XzInputStream;

    invoke-direct {v0, v1}, Lcom/facebook/xzdecoder/XzInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, LX/0Iu;->c:Lcom/facebook/xzdecoder/XzInputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39022
    invoke-virtual {p0}, LX/0Iu;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 39023
    invoke-virtual {p0}, LX/0Iu;->close()V

    .line 39024
    :cond_0
    return-void

    .line 39025
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 39026
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 39027
    iget v0, p0, LX/0Iu;->b:I

    .line 39028
    :goto_0
    iget-object v1, p0, LX/0Iu;->a:LX/0AP;

    iget-object v1, v1, LX/0AP;->b:[LX/0Az;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, LX/0Iu;->a:LX/0AP;

    iget-object v1, v1, LX/0AP;->b:[LX/0Az;

    aget-object v1, v1, v0

    iget-boolean v1, v1, LX/0Az;->e:Z

    if-nez v1, :cond_0

    .line 39029
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 39030
    :cond_0
    iget-object v1, p0, LX/0Iu;->a:LX/0AP;

    iget-object v1, v1, LX/0AP;->b:[LX/0Az;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()LX/0Is;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 39031
    :goto_0
    iget v0, p0, LX/0Iu;->b:I

    iget-object v2, p0, LX/0Iu;->a:LX/0AP;

    iget-object v2, v2, LX/0AP;->b:[LX/0Az;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 39032
    iget-object v0, p0, LX/0Iu;->a:LX/0AP;

    iget-object v0, v0, LX/0AP;->b:[LX/0Az;

    iget v2, p0, LX/0Iu;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0Iu;->b:I

    aget-object v2, v0, v2

    .line 39033
    new-instance v3, LX/0It;

    iget v0, v2, LX/0Az;->a:I

    invoke-direct {v3, p0, v0}, LX/0It;-><init>(LX/0Iu;I)V

    .line 39034
    :try_start_0
    new-instance v0, LX/0Is;

    invoke-direct {v0, v2, v3}, LX/0Is;-><init>(LX/07w;Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39035
    iget-boolean v2, v2, LX/0Az;->e:Z

    if-nez v2, :cond_1

    .line 39036
    invoke-virtual {v0}, LX/0Is;->close()V

    goto :goto_0

    .line 39037
    :catchall_0
    move-exception v0

    .line 39038
    invoke-virtual {v3}, LX/0It;->close()V

    throw v0

    :cond_0
    move-object v0, v1

    .line 39039
    :cond_1
    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 39040
    iget-object v0, p0, LX/0Iu;->c:Lcom/facebook/xzdecoder/XzInputStream;

    invoke-virtual {v0}, Lcom/facebook/xzdecoder/XzInputStream;->close()V

    .line 39041
    return-void
.end method
