.class public final LX/05c;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field public final synthetic b:LX/05b;


# direct methods
.method public constructor <init>(LX/05b;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;)V
    .locals 0

    .prologue
    .line 16435
    iput-object p1, p0, LX/05c;->b:LX/05b;

    iput-object p2, p0, LX/05c;->a:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x6be210f4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 16436
    if-nez p2, :cond_0

    .line 16437
    const/16 v0, 0x27

    const v2, -0x6c4bdc22

    invoke-static {p2, v3, v0, v2, v1}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 16438
    :goto_0
    return-void

    .line 16439
    :cond_0
    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 16440
    iget-object v0, p0, LX/05c;->b:LX/05b;

    iget-object v0, v0, LX/05b;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 16441
    invoke-virtual {v2, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 16442
    iget-object v0, p0, LX/05c;->b:LX/05b;

    iget-object v0, v0, LX/05b;->f:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v3, p0, LX/05c;->a:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v3}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 16443
    iget-object v0, p0, LX/05c;->b:LX/05b;

    iget-object v0, v0, LX/05b;->h:LX/0AD;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v2}, LX/0AD;->a(Z)V

    .line 16444
    :cond_1
    const v0, 0x4c257822    # 4.3376776E7f

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0
.end method
