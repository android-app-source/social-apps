.class public LX/0J3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39177
    const/4 v0, 0x1

    .line 39178
    sput-boolean v0, LX/0Bx;->a:Z

    .line 39179
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39181
    return-void
.end method

.method public static a(Landroid/hardware/camera2/CameraDevice;)V
    .locals 1

    .prologue
    .line 39182
    invoke-virtual {p0}, Landroid/hardware/camera2/CameraDevice;->close()V

    .line 39183
    invoke-static {}, LX/0Bx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39184
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    .line 39185
    invoke-static {v0}, LX/0Bx;->b(I)V

    .line 39186
    :cond_0
    return-void
.end method
