.class public final LX/0Cv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/BrowserLiteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V
    .locals 0

    .prologue
    .line 28108
    iput-object p1, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/browser/lite/BrowserLiteFragment;B)V
    .locals 0

    .prologue
    .line 28109
    invoke-direct {p0, p1}, LX/0Cv;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 28110
    iget-object v2, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-boolean v2, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->K:Z

    if-nez v2, :cond_1

    .line 28111
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 28112
    :goto_0
    iget-object v2, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 28113
    iput v3, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->I:I

    .line 28114
    iget-object v2, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v3, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v4, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget v4, v4, Lcom/facebook/browser/lite/BrowserLiteFragment;->G:I

    iget-object v5, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget v5, v5, Lcom/facebook/browser/lite/BrowserLiteFragment;->H:I

    iget-object v6, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget v6, v6, Lcom/facebook/browser/lite/BrowserLiteFragment;->I:I

    iget-object v7, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-boolean v7, v7, Lcom/facebook/browser/lite/BrowserLiteFragment;->J:Z

    invoke-static {v3, v4, v5, v6, v7}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Lcom/facebook/browser/lite/BrowserLiteFragment;IIIZ)Z

    move-result v3

    .line 28115
    iput-boolean v3, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->K:Z

    .line 28116
    iget-object v2, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v3, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget v3, v3, Lcom/facebook/browser/lite/BrowserLiteFragment;->I:I

    .line 28117
    iput v3, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->H:I

    .line 28118
    iget-object v2, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 28119
    iput-boolean v1, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->J:Z

    .line 28120
    iget-object v2, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-boolean v2, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->K:Z

    if-nez v2, :cond_0

    .line 28121
    :goto_1
    return v0

    .line 28122
    :pswitch_0
    iget-object v2, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 28123
    iput v3, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->G:I

    .line 28124
    iget-object v2, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v3, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget v3, v3, Lcom/facebook/browser/lite/BrowserLiteFragment;->G:I

    .line 28125
    iput v3, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->H:I

    .line 28126
    goto :goto_0

    .line 28127
    :pswitch_1
    iget-object v2, p0, LX/0Cv;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 28128
    iput-boolean v0, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->J:Z

    .line 28129
    goto :goto_0

    :cond_0
    move v0, v1

    .line 28130
    goto :goto_1

    :cond_1
    move v0, v1

    .line 28131
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
