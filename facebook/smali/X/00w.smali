.class public final LX/00w;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field public static final b:LX/00w;


# instance fields
.field private volatile c:J

.field private volatile d:Z

.field public volatile e:Ljava/lang/Boolean;

.field private volatile f:Z

.field public volatile g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3321
    const-class v0, LX/00w;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/00w;->a:Ljava/lang/String;

    .line 3322
    new-instance v0, LX/00w;

    invoke-direct {v0}, LX/00w;-><init>()V

    sput-object v0, LX/00w;->b:LX/00w;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 3275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3276
    const/4 v0, -0x1

    iput v0, p0, LX/00w;->g:I

    .line 3277
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/00w;->c:J

    .line 3278
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/00w;->d:Z

    .line 3279
    return-void
.end method

.method public static a()LX/00w;
    .locals 1

    .prologue
    .line 3323
    sget-object v0, LX/00w;->b:LX/00w;

    return-object v0
.end method

.method public static b(I)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3335
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v0, v3, :cond_2

    .line 3336
    invoke-static {}, LX/00w;->n()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 3337
    :goto_0
    return v0

    .line 3338
    :cond_0
    invoke-static {}, LX/00w;->n()Landroid/app/Application;

    move-result-object v0

    const-string v3, "power"

    invoke-virtual {v0, v3}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 3339
    invoke-virtual {v0}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3340
    const/16 v0, 0x96

    if-gt p0, v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 3341
    :cond_2
    const/16 v0, 0x64

    if-gt p0, v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static g(LX/00w;)Z
    .locals 1

    .prologue
    .line 3324
    iget v0, p0, LX/00w;->g:I

    if-gtz v0, :cond_0

    invoke-static {}, LX/00w;->j()I

    move-result v0

    .line 3325
    const/16 p0, 0xc8

    if-gt v0, p0, :cond_2

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 3326
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static i(LX/00w;)Z
    .locals 3

    .prologue
    .line 3327
    iget v0, p0, LX/00w;->g:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    invoke-static {}, LX/00w;->j()I

    move-result v0

    const/4 v1, 0x0

    .line 3328
    invoke-static {v0}, LX/00w;->b(I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 3329
    :cond_0
    :goto_0
    move v0, v1

    .line 3330
    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 3331
    :cond_3
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p0, 0x17

    if-lt v2, p0, :cond_4

    .line 3332
    const/16 v2, 0x64

    if-gt v0, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 3333
    :cond_4
    invoke-static {}, LX/00w;->n()Landroid/app/Application;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 3334
    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    goto :goto_0
.end method

.method public static j()I
    .locals 5

    .prologue
    const v1, 0x7fffffff

    .line 3293
    invoke-static {}, LX/00w;->n()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 3294
    :goto_0
    return v0

    .line 3295
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_1

    .line 3296
    new-instance v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$RunningAppProcessInfo;-><init>()V

    .line 3297
    invoke-static {v0}, Landroid/app/ActivityManager;->getMyMemoryState(Landroid/app/ActivityManager$RunningAppProcessInfo;)V

    .line 3298
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    goto :goto_0

    .line 3299
    :cond_1
    invoke-static {}, LX/00w;->n()Landroid/app/Application;

    move-result-object v0

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 3300
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 3301
    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 3302
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    goto :goto_0

    :cond_3
    move v0, v1

    .line 3303
    goto :goto_0
.end method

.method public static k(LX/00w;)Z
    .locals 6

    .prologue
    .line 3304
    const/4 v0, 0x0

    .line 3305
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 3306
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/proc/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/cgroup"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3307
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 3308
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v4, v2, v5

    .line 3309
    sget-object v3, LX/016;->b:[I

    invoke-static {v1, v3, v2, v4, v4}, LX/016;->a(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    .line 3310
    aget-object v2, v2, v5

    move-object v1, v2

    .line 3311
    if-nez v1, :cond_1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3312
    :goto_0
    move v0, v0

    .line 3313
    invoke-static {}, LX/00w;->m()J

    move-result-wide v2

    .line 3314
    iput-boolean v0, p0, LX/00w;->d:Z

    .line 3315
    if-eqz v0, :cond_0

    .line 3316
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/00w;->f:Z

    .line 3317
    :cond_0
    iput-wide v2, p0, LX/00w;->c:J

    .line 3318
    return v0

    .line 3319
    :cond_1
    :try_start_1
    const-string v2, ":cpu:/apps/bg_non_interactive"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_0

    .line 3320
    :catch_0
    goto :goto_0
.end method

.method private static m()J
    .locals 2

    .prologue
    .line 3292
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static n()Landroid/app/Application;
    .locals 1

    .prologue
    .line 3291
    invoke-static {}, LX/00y;->a()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getApplication()Landroid/app/Application;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 3286
    iget-object v0, p0, LX/00w;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 3287
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "checkIfStartupWasInTheBackground has already been called!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3288
    :cond_0
    invoke-static {p0}, LX/00w;->k(LX/00w;)Z

    move-result v0

    .line 3289
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, LX/00w;->e:Ljava/lang/Boolean;

    .line 3290
    return v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 3285
    iget-object v0, p0, LX/00w;->e:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 3280
    iget-boolean v0, p0, LX/00w;->f:Z

    if-eqz v0, :cond_0

    .line 3281
    const/4 v0, 0x1

    .line 3282
    :goto_0
    return v0

    .line 3283
    :cond_0
    invoke-virtual {p0}, LX/00w;->e()Z

    .line 3284
    iget-boolean v0, p0, LX/00w;->f:Z

    goto :goto_0
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 3271
    iget-wide v0, p0, LX/00w;->c:J

    .line 3272
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    invoke-static {}, LX/00w;->m()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/16 v2, 0x1f4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 3273
    :cond_0
    invoke-static {p0}, LX/00w;->k(LX/00w;)Z

    move-result v0

    .line 3274
    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, LX/00w;->d:Z

    goto :goto_0
.end method
