.class public LX/0LI;
.super LX/0LH;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private final b:Landroid/media/AudioTimestamp;

.field private c:J

.field private d:J

.field private e:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43249
    invoke-direct {p0}, LX/0LH;-><init>()V

    .line 43250
    new-instance v0, Landroid/media/AudioTimestamp;

    invoke-direct {v0}, Landroid/media/AudioTimestamp;-><init>()V

    iput-object v0, p0, LX/0LI;->b:Landroid/media/AudioTimestamp;

    .line 43251
    return-void
.end method


# virtual methods
.method public a(Landroid/media/AudioTrack;Z)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 43252
    invoke-super {p0, p1, p2}, LX/0LH;->a(Landroid/media/AudioTrack;Z)V

    .line 43253
    iput-wide v0, p0, LX/0LI;->c:J

    .line 43254
    iput-wide v0, p0, LX/0LI;->d:J

    .line 43255
    iput-wide v0, p0, LX/0LI;->e:J

    .line 43256
    return-void
.end method

.method public final d()Z
    .locals 8

    .prologue
    .line 43257
    iget-object v0, p0, LX/0LH;->a:Landroid/media/AudioTrack;

    iget-object v1, p0, LX/0LI;->b:Landroid/media/AudioTimestamp;

    invoke-virtual {v0, v1}, Landroid/media/AudioTrack;->getTimestamp(Landroid/media/AudioTimestamp;)Z

    move-result v0

    .line 43258
    if-eqz v0, :cond_1

    .line 43259
    iget-object v1, p0, LX/0LI;->b:Landroid/media/AudioTimestamp;

    iget-wide v2, v1, Landroid/media/AudioTimestamp;->framePosition:J

    .line 43260
    iget-wide v4, p0, LX/0LI;->d:J

    cmp-long v1, v4, v2

    if-lez v1, :cond_0

    .line 43261
    iget-wide v4, p0, LX/0LI;->c:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/0LI;->c:J

    .line 43262
    :cond_0
    iput-wide v2, p0, LX/0LI;->d:J

    .line 43263
    iget-wide v4, p0, LX/0LI;->c:J

    const/16 v1, 0x20

    shl-long/2addr v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0LI;->e:J

    .line 43264
    :cond_1
    return v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 43265
    iget-object v0, p0, LX/0LI;->b:Landroid/media/AudioTimestamp;

    iget-wide v0, v0, Landroid/media/AudioTimestamp;->nanoTime:J

    return-wide v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 43266
    iget-wide v0, p0, LX/0LI;->e:J

    return-wide v0
.end method
