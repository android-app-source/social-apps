.class public final LX/0Ez;
.super Ljava/util/HashSet;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashSet",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32733
    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    .line 32734
    const-string v0, "honorific-prefix"

    invoke-virtual {p0, v0}, LX/0Ez;->add(Ljava/lang/Object;)Z

    .line 32735
    const-string v0, "given-name"

    invoke-virtual {p0, v0}, LX/0Ez;->add(Ljava/lang/Object;)Z

    .line 32736
    const-string v0, "additional-name"

    invoke-virtual {p0, v0}, LX/0Ez;->add(Ljava/lang/Object;)Z

    .line 32737
    const-string v0, "family-name"

    invoke-virtual {p0, v0}, LX/0Ez;->add(Ljava/lang/Object;)Z

    .line 32738
    const-string v0, "honorific-suffix"

    invoke-virtual {p0, v0}, LX/0Ez;->add(Ljava/lang/Object;)Z

    .line 32739
    return-void
.end method
