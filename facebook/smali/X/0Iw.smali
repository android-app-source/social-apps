.class public LX/0Iw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/conn/ssl/X509HostnameVerifier;


# instance fields
.field private final a:LX/0Iv;

.field private final b:LX/05x;


# direct methods
.method public constructor <init>(LX/0Iv;)V
    .locals 1

    .prologue
    .line 39063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39064
    iput-object p1, p0, LX/0Iw;->a:LX/0Iv;

    .line 39065
    new-instance v0, LX/05x;

    invoke-direct {v0}, LX/05x;-><init>()V

    iput-object v0, p0, LX/0Iw;->b:LX/05x;

    .line 39066
    return-void
.end method


# virtual methods
.method public final verify(Ljava/lang/String;Ljava/security/cert/X509Certificate;)V
    .locals 3

    .prologue
    .line 39042
    iget-object v0, p0, LX/0Iw;->b:LX/05x;

    invoke-virtual {v0, p1, p2}, LX/05x;->a(Ljava/lang/String;Ljava/security/cert/X509Certificate;)LX/07K;

    move-result-object v0

    .line 39043
    iget-boolean v1, v0, LX/07K;->a:Z

    move v0, v1

    .line 39044
    if-nez v0, :cond_0

    .line 39045
    new-instance v0, Ljavax/net/ssl/SSLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to verify certificate for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39046
    :cond_0
    return-void
.end method

.method public final verify(Ljava/lang/String;Ljavax/net/ssl/SSLSocket;)V
    .locals 3

    .prologue
    .line 39060
    invoke-virtual {p2}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/0Iw;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39061
    new-instance v0, Ljavax/net/ssl/SSLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to verify socket for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39062
    :cond_0
    return-void
.end method

.method public final verify(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 39048
    iget-object v0, p0, LX/0Iw;->a:LX/0Iv;

    if-eqz v0, :cond_0

    .line 39049
    iget-object v0, p0, LX/0Iw;->a:LX/0Iv;

    invoke-interface {v0, p1, p2, p3}, LX/0Iv;->a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 39050
    :cond_0
    array-length v0, p2

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 39051
    new-instance v0, Ljavax/net/ssl/SSLException;

    const-string v1, "Certificate has multiple common names"

    invoke-direct {v0, v1}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39052
    :cond_1
    array-length v0, p2

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 39053
    :goto_0
    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 39054
    invoke-static {p1}, LX/05x;->a(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_4

    invoke-static {p1, v2}, LX/05x;->a(Ljava/lang/String;Ljava/util/List;)LX/07K;

    move-result-object p0

    :goto_1
    move-object v0, p0

    .line 39055
    iget-boolean v1, v0, LX/07K;->a:Z

    move v0, v1

    .line 39056
    if-nez v0, :cond_3

    .line 39057
    new-instance v0, Ljavax/net/ssl/SSLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to verify cns and subjectAlts for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39058
    :cond_2
    const/4 v0, 0x0

    aget-object v0, p2, v0

    goto :goto_0

    .line 39059
    :cond_3
    return-void

    :cond_4
    invoke-static {p1, v0, v2}, LX/05x;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)LX/07K;

    move-result-object p0

    goto :goto_1
.end method

.method public final verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z
    .locals 1

    .prologue
    .line 39047
    iget-object v0, p0, LX/0Iw;->b:LX/05x;

    invoke-virtual {v0, p1, p2}, LX/05x;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v0

    return v0
.end method
