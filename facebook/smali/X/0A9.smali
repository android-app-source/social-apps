.class public LX/0A9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;

.field private static volatile b:LX/0A9;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23567
    const-class v0, LX/0A9;

    sput-object v0, LX/0A9;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 23565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23566
    return-void
.end method

.method public static a(LX/0QB;)LX/0A9;
    .locals 3

    .prologue
    .line 23478
    sget-object v0, LX/0A9;->b:LX/0A9;

    if-nez v0, :cond_1

    .line 23479
    const-class v1, LX/0A9;

    monitor-enter v1

    .line 23480
    :try_start_0
    sget-object v0, LX/0A9;->b:LX/0A9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 23481
    if-eqz v2, :cond_0

    .line 23482
    :try_start_1
    new-instance v0, LX/0A9;

    invoke-direct {v0}, LX/0A9;-><init>()V

    .line 23483
    move-object v0, v0

    .line 23484
    sput-object v0, LX/0A9;->b:LX/0A9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23485
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 23486
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 23487
    :cond_1
    sget-object v0, LX/0A9;->b:LX/0A9;

    return-object v0

    .line 23488
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 23489
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a()Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 23556
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "device_video_capabilities"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 23557
    const-string v1, "brand"

    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23558
    const-string v1, "manufacturer"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23559
    const-string v1, "video"

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23560
    invoke-static {v0}, LX/0A9;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 23561
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 23562
    invoke-static {v0}, LX/0A9;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 23563
    invoke-static {v0}, LX/0A9;->c(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 23564
    :cond_0
    return-object v0
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 3

    .prologue
    .line 23540
    const/4 v0, 0x0

    .line 23541
    const/4 v1, 0x1

    :try_start_0
    invoke-static {v1}, Landroid/media/CamcorderProfile;->get(I)Landroid/media/CamcorderProfile;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 23542
    :goto_0
    if-eqz v0, :cond_0

    .line 23543
    const-string v1, "cam_duration"

    iget v2, v0, Landroid/media/CamcorderProfile;->duration:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23544
    const-string v1, "cam_quality"

    iget v2, v0, Landroid/media/CamcorderProfile;->quality:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23545
    const-string v1, "cam_fileFormat"

    iget v2, v0, Landroid/media/CamcorderProfile;->fileFormat:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23546
    const-string v1, "cam_vCodec"

    iget v2, v0, Landroid/media/CamcorderProfile;->videoCodec:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23547
    const-string v1, "cam_vBitRate"

    iget v2, v0, Landroid/media/CamcorderProfile;->videoBitRate:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23548
    const-string v1, "cam_vFrameRate"

    iget v2, v0, Landroid/media/CamcorderProfile;->videoFrameRate:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23549
    const-string v1, "cam_vFrameWidth"

    iget v2, v0, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23550
    const-string v1, "cam_vFrameHeight"

    iget v2, v0, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23551
    const-string v1, "cam_aCodec"

    iget v2, v0, Landroid/media/CamcorderProfile;->audioCodec:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23552
    const-string v1, "cam_aBitRate"

    iget v2, v0, Landroid/media/CamcorderProfile;->audioBitRate:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23553
    const-string v1, "cam_aSampleRate"

    iget v2, v0, Landroid/media/CamcorderProfile;->audioSampleRate:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23554
    const-string v1, "cam_aChannels"

    iget v0, v0, Landroid/media/CamcorderProfile;->audioChannels:I

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23555
    :cond_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private static b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 15
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 23502
    new-instance v4, Landroid/util/SparseIntArray;

    invoke-direct {v4}, Landroid/util/SparseIntArray;-><init>()V

    .line 23503
    invoke-virtual {v4, v8, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 23504
    invoke-virtual {v4, v10, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 23505
    invoke-virtual {v4, v9, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 23506
    invoke-virtual {v4, v11, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 23507
    const/4 v1, 0x0

    .line 23508
    :try_start_0
    const/4 v2, 0x0

    .line 23509
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v6

    move v5, v2

    .line 23510
    :goto_0
    if-ge v5, v6, :cond_5

    .line 23511
    invoke-static {v5}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v7

    .line 23512
    invoke-virtual {v7}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v0

    if-nez v0, :cond_4

    .line 23513
    invoke-virtual {v7}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v12

    move v0, v2

    .line 23514
    :goto_1
    array-length v13, v12

    if-ge v0, v13, :cond_4

    .line 23515
    aget-object v13, v12, v0

    const-string v14, "video/avc"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 23516
    const-string v0, "video/avc"

    invoke-virtual {v7, v0}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 23517
    :goto_2
    move-object v1, v0

    .line 23518
    if-eqz v1, :cond_1

    .line 23519
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaCodecInfo$CodecCapabilities;

    move v2, v3

    .line 23520
    :goto_3
    iget-object v5, v0, Landroid/media/MediaCodecInfo$CodecCapabilities;->profileLevels:[Landroid/media/MediaCodecInfo$CodecProfileLevel;

    array-length v5, v5

    if-ge v2, v5, :cond_1

    .line 23521
    iget-object v5, v0, Landroid/media/MediaCodecInfo$CodecCapabilities;->profileLevels:[Landroid/media/MediaCodecInfo$CodecProfileLevel;

    aget-object v5, v5, v2

    .line 23522
    iget v6, v5, Landroid/media/MediaCodecInfo$CodecProfileLevel;->profile:I

    const/4 v7, -0x1

    invoke-virtual {v4, v6, v7}, Landroid/util/SparseIntArray;->get(II)I

    move-result v6

    .line 23523
    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    .line 23524
    iget v7, v5, Landroid/media/MediaCodecInfo$CodecProfileLevel;->level:I

    if-le v7, v6, :cond_0

    .line 23525
    iget v6, v5, Landroid/media/MediaCodecInfo$CodecProfileLevel;->profile:I

    iget v5, v5, Landroid/media/MediaCodecInfo$CodecProfileLevel;->level:I

    invoke-virtual {v4, v6, v5}, Landroid/util/SparseIntArray;->put(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23526
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 23527
    :catch_0
    move-exception v0

    .line 23528
    sget-object v2, LX/0A9;->a:Ljava/lang/Class;

    const-string v5, "Error trying to get decoder capabilities"

    invoke-static {v2, v5, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 23529
    :cond_1
    if-nez v1, :cond_2

    .line 23530
    :goto_4
    return-void

    .line 23531
    :cond_2
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/Object;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaCodecInfo;

    invoke-virtual {v0}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v4, v8}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v8

    invoke-virtual {v4, v10}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v9

    const/4 v0, 0x3

    invoke-virtual {v4, v9}, Landroid/util/SparseIntArray;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {v4, v11}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v10

    .line 23532
    const-string v2, "h264_decoder"

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaCodecInfo;

    invoke-virtual {v0}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23533
    const-string v0, "h264_base_level"

    invoke-virtual {v4, v8}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23534
    const-string v0, "h264_ext_level"

    invoke-virtual {v4, v10}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23535
    const-string v0, "h264_main_level"

    invoke-virtual {v4, v9}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23536
    const-string v0, "h264_high_level"

    invoke-virtual {v4, v11}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_4

    .line 23537
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 23538
    :cond_4
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto/16 :goto_0

    .line 23539
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method private static c(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 23491
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v1

    .line 23492
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 23493
    invoke-static {v0}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v2

    .line 23494
    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v3

    if-nez v3, :cond_0

    .line 23495
    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "vp9"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 23496
    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v0

    .line 23497
    :goto_1
    move-object v0, v0

    .line 23498
    const-string v1, "vp9_decoder"

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 23499
    return-void

    .line 23500
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 23501
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 1

    .prologue
    .line 23490
    invoke-static {}, LX/0A9;->a()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method
