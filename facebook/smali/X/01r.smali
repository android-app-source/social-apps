.class public abstract LX/01r;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/01r;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 5575
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 5576
    new-instance v0, LX/01s;

    invoke-direct {v0}, LX/01s;-><init>()V

    sput-object v0, LX/01r;->a:LX/01r;

    .line 5577
    :goto_0
    return-void

    .line 5578
    :cond_0
    new-instance v0, LX/0HS;

    invoke-direct {v0}, LX/0HS;-><init>()V

    sput-object v0, LX/01r;->a:LX/01r;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5574
    return-void
.end method

.method public static a(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 5569
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    .line 5570
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 5571
    :goto_0
    return-void

    .line 5572
    :cond_0
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/SharedPreferences;
.end method
