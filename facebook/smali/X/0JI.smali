.class public final enum LX/0JI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JI;

.field public static final enum SPHERICAL_VIDEO_HEADING_INDICATOR_CLICK:LX/0JI;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 39331
    new-instance v0, LX/0JI;

    const-string v1, "SPHERICAL_VIDEO_HEADING_INDICATOR_CLICK"

    const-string v2, "spherical_video_heading_indicator_click"

    invoke-direct {v0, v1, v3, v2}, LX/0JI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JI;->SPHERICAL_VIDEO_HEADING_INDICATOR_CLICK:LX/0JI;

    .line 39332
    const/4 v0, 0x1

    new-array v0, v0, [LX/0JI;

    sget-object v1, LX/0JI;->SPHERICAL_VIDEO_HEADING_INDICATOR_CLICK:LX/0JI;

    aput-object v1, v0, v3

    sput-object v0, LX/0JI;->$VALUES:[LX/0JI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39333
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39334
    iput-object p3, p0, LX/0JI;->value:Ljava/lang/String;

    .line 39335
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JI;
    .locals 1

    .prologue
    .line 39336
    const-class v0, LX/0JI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JI;

    return-object v0
.end method

.method public static values()[LX/0JI;
    .locals 1

    .prologue
    .line 39337
    sget-object v0, LX/0JI;->$VALUES:[LX/0JI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JI;

    return-object v0
.end method
