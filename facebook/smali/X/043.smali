.class public LX/043;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:I

.field public static b:I

.field public static c:I


# instance fields
.field public final d:Z

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:Z

.field public final h:I

.field public final i:I

.field public final j:Z

.field public final k:Z

.field public final l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12412
    const/high16 v0, 0x6400000

    sput v0, LX/043;->a:I

    .line 12413
    const/high16 v0, 0xa00000

    sput v0, LX/043;->b:I

    .line 12414
    const/high16 v0, 0x100000

    sput v0, LX/043;->c:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 12415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12416
    const-string v0, "UseExoServiceCache"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/043;->d:Z

    .line 12417
    const-string v0, "ExoCacheRootDirectory"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/043;->e:Ljava/lang/String;

    .line 12418
    const-string v0, "ExoUseSplitCache"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/043;->g:Z

    .line 12419
    const-string v0, "ExoCacheSize"

    sget v1, LX/043;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/043;->f:I

    .line 12420
    const-string v0, "ExoPrefetchCacheSize"

    sget v1, LX/043;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/043;->h:I

    .line 12421
    const-string v0, "ExoPlayCacheSize"

    sget v1, LX/043;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/043;->i:I

    .line 12422
    const-string v0, "ExoCacheSaveMetadata"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/043;->j:Z

    .line 12423
    const-string v0, "UseExoServiceCacheForProgressive"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/043;->k:Z

    .line 12424
    const-string v0, "UseExoServiceCacheForDash"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/043;->l:Z

    .line 12425
    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;IZIIZZZ)V
    .locals 0

    .prologue
    .line 12426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12427
    iput-boolean p1, p0, LX/043;->d:Z

    .line 12428
    iput-object p2, p0, LX/043;->e:Ljava/lang/String;

    .line 12429
    iput p3, p0, LX/043;->f:I

    .line 12430
    iput-boolean p4, p0, LX/043;->g:Z

    .line 12431
    iput p5, p0, LX/043;->h:I

    .line 12432
    iput p6, p0, LX/043;->i:I

    .line 12433
    iput-boolean p7, p0, LX/043;->j:Z

    .line 12434
    iput-boolean p8, p0, LX/043;->k:Z

    .line 12435
    iput-boolean p9, p0, LX/043;->l:Z

    .line 12436
    return-void
.end method
