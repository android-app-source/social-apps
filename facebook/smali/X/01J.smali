.class public LX/01J;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static b:[Ljava/lang/Object;

.field public static c:I

.field public static d:[Ljava/lang/Object;

.field public static e:I


# instance fields
.field public f:[I

.field public g:[Ljava/lang/Object;

.field public h:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4269
    sget-object v0, LX/01M;->a:[I

    iput-object v0, p0, LX/01J;->f:[I

    .line 4270
    sget-object v0, LX/01M;->c:[Ljava/lang/Object;

    iput-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    .line 4271
    const/4 v0, 0x0

    iput v0, p0, LX/01J;->h:I

    .line 4272
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 4273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4274
    if-nez p1, :cond_0

    .line 4275
    sget-object v0, LX/01M;->a:[I

    iput-object v0, p0, LX/01J;->f:[I

    .line 4276
    sget-object v0, LX/01M;->c:[Ljava/lang/Object;

    iput-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    .line 4277
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, LX/01J;->h:I

    .line 4278
    return-void

    .line 4279
    :cond_0
    invoke-direct {p0, p1}, LX/01J;->e(I)V

    goto :goto_0
.end method

.method public constructor <init>(LX/01J;)V
    .locals 0

    .prologue
    .line 4280
    invoke-direct {p0}, LX/01J;-><init>()V

    .line 4281
    if-eqz p1, :cond_0

    .line 4282
    invoke-virtual {p0, p1}, LX/01J;->a(LX/01J;)V

    .line 4283
    :cond_0
    return-void
.end method

.method private a()I
    .locals 5

    .prologue
    .line 4284
    iget v2, p0, LX/01J;->h:I

    .line 4285
    if-nez v2, :cond_1

    .line 4286
    const/4 v0, -0x1

    .line 4287
    :cond_0
    :goto_0
    return v0

    .line 4288
    :cond_1
    iget-object v0, p0, LX/01J;->f:[I

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, LX/01M;->a([III)I

    move-result v0

    .line 4289
    if-ltz v0, :cond_0

    .line 4290
    iget-object v1, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v3, v0, 0x1

    aget-object v1, v1, v3

    if-eqz v1, :cond_0

    .line 4291
    add-int/lit8 v1, v0, 0x1

    :goto_1
    if-ge v1, v2, :cond_3

    iget-object v3, p0, LX/01J;->f:[I

    aget v3, v3, v1

    if-nez v3, :cond_3

    .line 4292
    iget-object v3, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v4, v1, 0x1

    aget-object v3, v3, v4

    if-nez v3, :cond_2

    move v0, v1

    goto :goto_0

    .line 4293
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4294
    :cond_3
    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_4

    iget-object v2, p0, LX/01J;->f:[I

    aget v2, v2, v0

    if-nez v2, :cond_4

    .line 4295
    iget-object v2, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    .line 4296
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 4297
    :cond_4
    xor-int/lit8 v0, v1, -0x1

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;I)I
    .locals 5

    .prologue
    .line 4298
    iget v2, p0, LX/01J;->h:I

    .line 4299
    if-nez v2, :cond_1

    .line 4300
    const/4 v0, -0x1

    .line 4301
    :cond_0
    :goto_0
    return v0

    .line 4302
    :cond_1
    iget-object v0, p0, LX/01J;->f:[I

    invoke-static {v0, v2, p2}, LX/01M;->a([III)I

    move-result v0

    .line 4303
    if-ltz v0, :cond_0

    .line 4304
    iget-object v1, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v3, v0, 0x1

    aget-object v1, v1, v3

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4305
    add-int/lit8 v1, v0, 0x1

    :goto_1
    if-ge v1, v2, :cond_3

    iget-object v3, p0, LX/01J;->f:[I

    aget v3, v3, v1

    if-ne v3, p2, :cond_3

    .line 4306
    iget-object v3, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v4, v1, 0x1

    aget-object v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_0

    .line 4307
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4308
    :cond_3
    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_4

    iget-object v2, p0, LX/01J;->f:[I

    aget v2, v2, v0

    if-ne v2, p2, :cond_4

    .line 4309
    iget-object v2, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4310
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 4311
    :cond_4
    xor-int/lit8 v0, v1, -0x1

    goto :goto_0
.end method

.method private static a([I[Ljava/lang/Object;I)V
    .locals 4

    .prologue
    const/16 v2, 0xa

    const/4 v3, 0x2

    .line 4312
    array-length v0, p0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 4313
    const-class v1, LX/026;

    monitor-enter v1

    .line 4314
    :try_start_0
    sget v0, LX/01J;->e:I

    if-ge v0, v2, :cond_1

    .line 4315
    const/4 v0, 0x0

    sget-object v2, LX/01J;->d:[Ljava/lang/Object;

    aput-object v2, p1, v0

    .line 4316
    const/4 v0, 0x1

    aput-object p0, p1, v0

    .line 4317
    shl-int/lit8 v0, p2, 0x1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-lt v0, v3, :cond_0

    .line 4318
    const/4 v2, 0x0

    aput-object v2, p1, v0

    .line 4319
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 4320
    :cond_0
    sput-object p1, LX/01J;->d:[Ljava/lang/Object;

    .line 4321
    sget v0, LX/01J;->e:I

    add-int/lit8 v0, v0, 0x1

    sput v0, LX/01J;->e:I

    .line 4322
    :cond_1
    monitor-exit v1

    .line 4323
    :cond_2
    :goto_1
    return-void

    .line 4324
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 4325
    :cond_3
    array-length v0, p0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 4326
    const-class v1, LX/026;

    monitor-enter v1

    .line 4327
    :try_start_1
    sget v0, LX/01J;->c:I

    if-ge v0, v2, :cond_5

    .line 4328
    const/4 v0, 0x0

    sget-object v2, LX/01J;->b:[Ljava/lang/Object;

    aput-object v2, p1, v0

    .line 4329
    const/4 v0, 0x1

    aput-object p0, p1, v0

    .line 4330
    shl-int/lit8 v0, p2, 0x1

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-lt v0, v3, :cond_4

    .line 4331
    const/4 v2, 0x0

    aput-object v2, p1, v0

    .line 4332
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 4333
    :cond_4
    sput-object p1, LX/01J;->b:[Ljava/lang/Object;

    .line 4334
    sget v0, LX/01J;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, LX/01J;->c:I

    .line 4335
    :cond_5
    monitor-exit v1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method private e(I)V
    .locals 5

    .prologue
    .line 4336
    const/16 v0, 0x8

    if-ne p1, v0, :cond_2

    .line 4337
    const-class v1, LX/026;

    monitor-enter v1

    .line 4338
    :try_start_0
    sget-object v0, LX/01J;->d:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 4339
    sget-object v2, LX/01J;->d:[Ljava/lang/Object;

    .line 4340
    iput-object v2, p0, LX/01J;->g:[Ljava/lang/Object;

    .line 4341
    const/4 v0, 0x0

    aget-object v0, v2, v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    sput-object v0, LX/01J;->d:[Ljava/lang/Object;

    .line 4342
    const/4 v0, 0x1

    aget-object v0, v2, v0

    check-cast v0, [I

    check-cast v0, [I

    iput-object v0, p0, LX/01J;->f:[I

    .line 4343
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput-object v4, v2, v3

    aput-object v4, v2, v0

    .line 4344
    sget v0, LX/01J;->e:I

    add-int/lit8 v0, v0, -0x1

    sput v0, LX/01J;->e:I

    .line 4345
    monitor-exit v1

    .line 4346
    :goto_0
    return-void

    .line 4347
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4348
    :cond_1
    :goto_1
    new-array v0, p1, [I

    iput-object v0, p0, LX/01J;->f:[I

    .line 4349
    shl-int/lit8 v0, p1, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    goto :goto_0

    .line 4350
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 4351
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 4352
    const-class v1, LX/026;

    monitor-enter v1

    .line 4353
    :try_start_2
    sget-object v0, LX/01J;->b:[Ljava/lang/Object;

    if-eqz v0, :cond_3

    .line 4354
    sget-object v2, LX/01J;->b:[Ljava/lang/Object;

    .line 4355
    iput-object v2, p0, LX/01J;->g:[Ljava/lang/Object;

    .line 4356
    const/4 v0, 0x0

    aget-object v0, v2, v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    sput-object v0, LX/01J;->b:[Ljava/lang/Object;

    .line 4357
    const/4 v0, 0x1

    aget-object v0, v2, v0

    check-cast v0, [I

    check-cast v0, [I

    iput-object v0, p0, LX/01J;->f:[I

    .line 4358
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput-object v4, v2, v3

    aput-object v4, v2, v0

    .line 4359
    sget v0, LX/01J;->c:I

    add-int/lit8 v0, v0, -0x1

    sput v0, LX/01J;->c:I

    .line 4360
    monitor-exit v1

    goto :goto_0

    .line 4361
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_3
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 4362
    if-nez p1, :cond_0

    invoke-direct {p0}, LX/01J;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-direct {p0, p1, v0}, LX/01J;->a(Ljava/lang/Object;I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITV;)TV;"
        }
    .end annotation

    .prologue
    .line 4363
    shl-int/lit8 v0, p1, 0x1

    add-int/lit8 v0, v0, 0x1

    .line 4364
    iget-object v1, p0, LX/01J;->g:[Ljava/lang/Object;

    aget-object v1, v1, v0

    .line 4365
    iget-object v2, p0, LX/01J;->g:[Ljava/lang/Object;

    aput-object p2, v2, v0

    .line 4366
    return-object v1
.end method

.method public final a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 4367
    iget-object v0, p0, LX/01J;->f:[I

    array-length v0, v0

    if-ge v0, p1, :cond_1

    .line 4368
    iget-object v0, p0, LX/01J;->f:[I

    .line 4369
    iget-object v1, p0, LX/01J;->g:[Ljava/lang/Object;

    .line 4370
    invoke-direct {p0, p1}, LX/01J;->e(I)V

    .line 4371
    iget v2, p0, LX/01J;->h:I

    if-lez v2, :cond_0

    .line 4372
    iget-object v2, p0, LX/01J;->f:[I

    iget v3, p0, LX/01J;->h:I

    invoke-static {v0, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4373
    iget-object v2, p0, LX/01J;->g:[Ljava/lang/Object;

    iget v3, p0, LX/01J;->h:I

    shl-int/lit8 v3, v3, 0x1

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4374
    :cond_0
    iget v2, p0, LX/01J;->h:I

    invoke-static {v0, v1, v2}, LX/01J;->a([I[Ljava/lang/Object;I)V

    .line 4375
    :cond_1
    return-void
.end method

.method public final a(LX/01J;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01J",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 4376
    iget v1, p1, LX/01J;->h:I

    .line 4377
    iget v2, p0, LX/01J;->h:I

    add-int/2addr v2, v1

    invoke-virtual {p0, v2}, LX/01J;->a(I)V

    .line 4378
    iget v2, p0, LX/01J;->h:I

    if-nez v2, :cond_1

    .line 4379
    if-lez v1, :cond_0

    .line 4380
    iget-object v2, p1, LX/01J;->f:[I

    iget-object v3, p0, LX/01J;->f:[I

    invoke-static {v2, v0, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4381
    iget-object v2, p1, LX/01J;->g:[Ljava/lang/Object;

    iget-object v3, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v4, v1, 0x1

    invoke-static {v2, v0, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4382
    iput v1, p0, LX/01J;->h:I

    .line 4383
    :cond_0
    return-void

    .line 4384
    :cond_1
    :goto_0
    if-ge v0, v1, :cond_0

    .line 4385
    invoke-virtual {p1, v0}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v0}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4386
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 4387
    iget v1, p0, LX/01J;->h:I

    mul-int/lit8 v1, v1, 0x2

    .line 4388
    iget-object v2, p0, LX/01J;->g:[Ljava/lang/Object;

    .line 4389
    if-nez p1, :cond_2

    .line 4390
    :goto_0
    if-ge v0, v1, :cond_3

    .line 4391
    aget-object v3, v2, v0

    if-nez v3, :cond_0

    .line 4392
    shr-int/lit8 v0, v0, 0x1

    .line 4393
    :goto_1
    return v0

    .line 4394
    :cond_0
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 4395
    :cond_1
    add-int/lit8 v0, v0, 0x2

    :cond_2
    if-ge v0, v1, :cond_3

    .line 4396
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4397
    shr-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4398
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TK;"
        }
    .end annotation

    .prologue
    .line 4267
    iget-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final c(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .prologue
    .line 4150
    iget-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, p1, 0x1

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final clear()V
    .locals 3

    .prologue
    .line 4261
    iget v0, p0, LX/01J;->h:I

    if-eqz v0, :cond_0

    .line 4262
    iget-object v0, p0, LX/01J;->f:[I

    iget-object v1, p0, LX/01J;->g:[Ljava/lang/Object;

    iget v2, p0, LX/01J;->h:I

    invoke-static {v0, v1, v2}, LX/01J;->a([I[Ljava/lang/Object;I)V

    .line 4263
    sget-object v0, LX/01M;->a:[I

    iput-object v0, p0, LX/01J;->f:[I

    .line 4264
    sget-object v0, LX/01M;->c:[Ljava/lang/Object;

    iput-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    .line 4265
    const/4 v0, 0x0

    iput v0, p0, LX/01J;->h:I

    .line 4266
    :cond_0
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 4260
    invoke-virtual {p0, p1}, LX/01J;->a(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 4259
    invoke-virtual {p0, p1}, LX/01J;->b(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/16 v0, 0x8

    const/4 v5, 0x0

    .line 4234
    iget-object v1, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v2, p1, 0x1

    add-int/lit8 v2, v2, 0x1

    aget-object v1, v1, v2

    .line 4235
    iget v2, p0, LX/01J;->h:I

    const/4 v3, 0x1

    if-gt v2, v3, :cond_1

    .line 4236
    iget-object v0, p0, LX/01J;->f:[I

    iget-object v2, p0, LX/01J;->g:[Ljava/lang/Object;

    iget v3, p0, LX/01J;->h:I

    invoke-static {v0, v2, v3}, LX/01J;->a([I[Ljava/lang/Object;I)V

    .line 4237
    sget-object v0, LX/01M;->a:[I

    iput-object v0, p0, LX/01J;->f:[I

    .line 4238
    sget-object v0, LX/01M;->c:[Ljava/lang/Object;

    iput-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    .line 4239
    iput v5, p0, LX/01J;->h:I

    .line 4240
    :cond_0
    :goto_0
    return-object v1

    .line 4241
    :cond_1
    iget-object v2, p0, LX/01J;->f:[I

    array-length v2, v2

    if-le v2, v0, :cond_4

    iget v2, p0, LX/01J;->h:I

    iget-object v3, p0, LX/01J;->f:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x3

    if-ge v2, v3, :cond_4

    .line 4242
    iget v2, p0, LX/01J;->h:I

    if-le v2, v0, :cond_2

    iget v0, p0, LX/01J;->h:I

    iget v2, p0, LX/01J;->h:I

    shr-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4243
    :cond_2
    iget-object v2, p0, LX/01J;->f:[I

    .line 4244
    iget-object v3, p0, LX/01J;->g:[Ljava/lang/Object;

    .line 4245
    invoke-direct {p0, v0}, LX/01J;->e(I)V

    .line 4246
    iget v0, p0, LX/01J;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/01J;->h:I

    .line 4247
    if-lez p1, :cond_3

    .line 4248
    iget-object v0, p0, LX/01J;->f:[I

    invoke-static {v2, v5, v0, v5, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4249
    iget-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v4, p1, 0x1

    invoke-static {v3, v5, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4250
    :cond_3
    iget v0, p0, LX/01J;->h:I

    if-ge p1, v0, :cond_0

    .line 4251
    add-int/lit8 v0, p1, 0x1

    iget-object v4, p0, LX/01J;->f:[I

    iget v5, p0, LX/01J;->h:I

    sub-int/2addr v5, p1

    invoke-static {v2, v0, v4, p1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4252
    add-int/lit8 v0, p1, 0x1

    shl-int/lit8 v0, v0, 0x1

    iget-object v2, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v4, p1, 0x1

    iget v5, p0, LX/01J;->h:I

    sub-int/2addr v5, p1

    shl-int/lit8 v5, v5, 0x1

    invoke-static {v3, v0, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 4253
    :cond_4
    iget v0, p0, LX/01J;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/01J;->h:I

    .line 4254
    iget v0, p0, LX/01J;->h:I

    if-ge p1, v0, :cond_5

    .line 4255
    iget-object v0, p0, LX/01J;->f:[I

    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, LX/01J;->f:[I

    iget v4, p0, LX/01J;->h:I

    sub-int/2addr v4, p1

    invoke-static {v0, v2, v3, p1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4256
    iget-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    add-int/lit8 v2, p1, 0x1

    shl-int/lit8 v2, v2, 0x1

    iget-object v3, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v4, p1, 0x1

    iget v5, p0, LX/01J;->h:I

    sub-int/2addr v5, p1

    shl-int/lit8 v5, v5, 0x1

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4257
    :cond_5
    iget-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    iget v2, p0, LX/01J;->h:I

    shl-int/lit8 v2, v2, 0x1

    aput-object v6, v0, v2

    .line 4258
    iget-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    iget v2, p0, LX/01J;->h:I

    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0x1

    aput-object v6, v0, v2

    goto/16 :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4215
    if-ne p0, p1, :cond_1

    .line 4216
    :cond_0
    :goto_0
    return v0

    .line 4217
    :cond_1
    instance-of v2, p1, Ljava/util/Map;

    if-eqz v2, :cond_6

    .line 4218
    check-cast p1, Ljava/util/Map;

    .line 4219
    invoke-virtual {p0}, LX/01J;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v3

    if-eq v2, v3, :cond_2

    move v0, v1

    .line 4220
    goto :goto_0

    :cond_2
    move v2, v1

    .line 4221
    :goto_1
    :try_start_0
    iget v3, p0, LX/01J;->h:I

    if-ge v2, v3, :cond_0

    .line 4222
    invoke-virtual {p0, v2}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v3

    .line 4223
    invoke-virtual {p0, v2}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v4

    .line 4224
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 4225
    if-nez v4, :cond_4

    .line 4226
    if-nez v5, :cond_3

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    :cond_3
    move v0, v1

    .line 4227
    goto :goto_0

    .line 4228
    :cond_4
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-nez v3, :cond_5

    move v0, v1

    .line 4229
    goto :goto_0

    .line 4230
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4231
    :catch_0
    move v0, v1

    goto :goto_0

    .line 4232
    :catch_1
    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v1

    .line 4233
    goto :goto_0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 4213
    invoke-virtual {p0, p1}, LX/01J;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4214
    if-ltz v0, :cond_0

    iget-object v1, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x1

    aget-object v0, v1, v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 4205
    iget-object v5, p0, LX/01J;->f:[I

    .line 4206
    iget-object v6, p0, LX/01J;->g:[Ljava/lang/Object;

    .line 4207
    const/4 v0, 0x1

    iget v7, p0, LX/01J;->h:I

    move v2, v0

    move v3, v1

    move v4, v1

    :goto_0
    if-ge v3, v7, :cond_1

    .line 4208
    aget-object v0, v6, v2

    .line 4209
    aget v8, v5, v3

    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    xor-int/2addr v0, v8

    add-int/2addr v4, v0

    .line 4210
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v0, v2, 0x2

    move v2, v0

    goto :goto_0

    .line 4211
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    .line 4212
    :cond_1
    return v4
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 4204
    iget v0, p0, LX/01J;->h:I

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    const/16 v0, 0x8

    const/4 v1, 0x4

    const/4 v4, 0x0

    .line 4176
    if-nez p1, :cond_0

    .line 4177
    invoke-direct {p0}, LX/01J;->a()I

    move-result v2

    move v3, v4

    .line 4178
    :goto_0
    if-ltz v2, :cond_1

    .line 4179
    shl-int/lit8 v0, v2, 0x1

    add-int/lit8 v1, v0, 0x1

    .line 4180
    iget-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    aget-object v0, v0, v1

    .line 4181
    iget-object v2, p0, LX/01J;->g:[Ljava/lang/Object;

    aput-object p2, v2, v1

    .line 4182
    :goto_1
    return-object v0

    .line 4183
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    .line 4184
    invoke-direct {p0, p1, v3}, LX/01J;->a(Ljava/lang/Object;I)I

    move-result v2

    goto :goto_0

    .line 4185
    :cond_1
    xor-int/lit8 v2, v2, -0x1

    .line 4186
    iget v5, p0, LX/01J;->h:I

    iget-object v6, p0, LX/01J;->f:[I

    array-length v6, v6

    if-lt v5, v6, :cond_4

    .line 4187
    iget v5, p0, LX/01J;->h:I

    if-lt v5, v0, :cond_6

    iget v0, p0, LX/01J;->h:I

    iget v1, p0, LX/01J;->h:I

    shr-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4188
    :cond_2
    :goto_2
    iget-object v1, p0, LX/01J;->f:[I

    .line 4189
    iget-object v5, p0, LX/01J;->g:[Ljava/lang/Object;

    .line 4190
    invoke-direct {p0, v0}, LX/01J;->e(I)V

    .line 4191
    iget-object v0, p0, LX/01J;->f:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 4192
    iget-object v0, p0, LX/01J;->f:[I

    array-length v6, v1

    invoke-static {v1, v4, v0, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4193
    iget-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    array-length v6, v5

    invoke-static {v5, v4, v0, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4194
    :cond_3
    iget v0, p0, LX/01J;->h:I

    invoke-static {v1, v5, v0}, LX/01J;->a([I[Ljava/lang/Object;I)V

    .line 4195
    :cond_4
    iget v0, p0, LX/01J;->h:I

    if-ge v2, v0, :cond_5

    .line 4196
    iget-object v0, p0, LX/01J;->f:[I

    iget-object v1, p0, LX/01J;->f:[I

    add-int/lit8 v4, v2, 0x1

    iget v5, p0, LX/01J;->h:I

    sub-int/2addr v5, v2

    invoke-static {v0, v2, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4197
    iget-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, v2, 0x1

    iget-object v4, p0, LX/01J;->g:[Ljava/lang/Object;

    add-int/lit8 v5, v2, 0x1

    shl-int/lit8 v5, v5, 0x1

    iget v6, p0, LX/01J;->h:I

    sub-int/2addr v6, v2

    shl-int/lit8 v6, v6, 0x1

    invoke-static {v0, v1, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4198
    :cond_5
    iget-object v0, p0, LX/01J;->f:[I

    aput v3, v0, v2

    .line 4199
    iget-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, v2, 0x1

    aput-object p1, v0, v1

    .line 4200
    iget-object v0, p0, LX/01J;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    aput-object p2, v0, v1

    .line 4201
    iget v0, p0, LX/01J;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/01J;->h:I

    .line 4202
    const/4 v0, 0x0

    goto :goto_1

    .line 4203
    :cond_6
    iget v5, p0, LX/01J;->h:I

    if-ge v5, v1, :cond_2

    move v0, v1

    goto :goto_2
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 4172
    invoke-virtual {p0, p1}, LX/01J;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4173
    if-ltz v0, :cond_0

    .line 4174
    invoke-virtual {p0, v0}, LX/01J;->d(I)Ljava/lang/Object;

    move-result-object v0

    .line 4175
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 4171
    iget v0, p0, LX/01J;->h:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 4151
    invoke-virtual {p0}, LX/01J;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4152
    const-string v0, "{}"

    .line 4153
    :goto_0
    return-object v0

    .line 4154
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget v0, p0, LX/01J;->h:I

    mul-int/lit8 v0, v0, 0x1c

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 4155
    const/16 v0, 0x7b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 4156
    const/4 v0, 0x0

    :goto_1
    iget v2, p0, LX/01J;->h:I

    if-ge v0, v2, :cond_4

    .line 4157
    if-lez v0, :cond_1

    .line 4158
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4159
    :cond_1
    invoke-virtual {p0, v0}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v2

    .line 4160
    if-eq v2, p0, :cond_2

    .line 4161
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4162
    :goto_2
    const/16 v2, 0x3d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 4163
    invoke-virtual {p0, v0}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v2

    .line 4164
    if-eq v2, p0, :cond_3

    .line 4165
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4166
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4167
    :cond_2
    const-string v2, "(this Map)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 4168
    :cond_3
    const-string v2, "(this Map)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 4169
    :cond_4
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 4170
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
