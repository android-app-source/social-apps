.class public final LX/0Cu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/BrowserLiteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V
    .locals 0

    .prologue
    .line 28094
    iput-object p1, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0D5;)V
    .locals 4

    .prologue
    .line 28095
    iget-object v0, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {v0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->c(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;)V

    .line 28096
    iget-object v0, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    invoke-virtual {p1}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_TRACKING"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {v3}, Lcom/facebook/browser/lite/BrowserLiteFragment;->z(Lcom/facebook/browser/lite/BrowserLiteFragment;)I

    move-result v3

    .line 28097
    new-instance p1, LX/0CO;

    invoke-direct {p1, v0, v1, v2, v3}, LX/0CO;-><init>(LX/0CQ;Ljava/lang/String;Landroid/os/Bundle;I)V

    invoke-static {v0, p1}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 28098
    iget-object v0, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    const/4 v1, 0x1

    .line 28099
    iput-boolean v1, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->x:Z

    .line 28100
    iget-object v0, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    if-eqz v0, :cond_0

    .line 28101
    iget-object v0, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLitePreview;->a()V

    .line 28102
    iget-object v0, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    iget-object v1, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteFragment;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/BrowserLitePreview;->setPreviewLoadedStateListener(Landroid/view/View$OnClickListener;)V

    .line 28103
    :cond_0
    iget-object v0, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    if-eqz v0, :cond_1

    .line 28104
    iget-object v0, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->a()V

    .line 28105
    iget-object v0, p0, LX/0Cu;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    const/4 v1, 0x0

    .line 28106
    iput-object v1, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    .line 28107
    :cond_1
    return-void
.end method
