.class public final LX/0Nj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0LU;


# instance fields
.field public final a:I

.field public final b:LX/0AR;

.field public final c:J

.field public final d:LX/0ME;

.field public final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0MC;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Z

.field private final g:I

.field private final h:I

.field private i:[LX/0L4;

.field public j:LX/0O1;

.field private volatile k:Z

.field private l:Z

.field private m:Z


# direct methods
.method public constructor <init>(ILX/0AR;JLX/0ME;ZII)V
    .locals 1

    .prologue
    .line 51548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51549
    iput p1, p0, LX/0Nj;->a:I

    .line 51550
    iput-object p2, p0, LX/0Nj;->b:LX/0AR;

    .line 51551
    iput-wide p3, p0, LX/0Nj;->c:J

    .line 51552
    iput-object p5, p0, LX/0Nj;->d:LX/0ME;

    .line 51553
    iput-boolean p6, p0, LX/0Nj;->f:Z

    .line 51554
    iput p7, p0, LX/0Nj;->g:I

    .line 51555
    iput p8, p0, LX/0Nj;->h:I

    .line 51556
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    .line 51557
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 51546
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Nj;->k:Z

    .line 51547
    return-void
.end method

.method public final a(LX/0Lz;)V
    .locals 0

    .prologue
    .line 51545
    return-void
.end method

.method public final a(LX/0M8;)V
    .locals 0

    .prologue
    .line 51544
    return-void
.end method

.method public final a(LX/0Nj;)V
    .locals 11

    .prologue
    .line 51520
    invoke-virtual {p0}, LX/0Nj;->b()Z

    move-result v0

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51521
    iget-boolean v0, p0, LX/0Nj;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p1, LX/0Nj;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/0Nj;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 51522
    :cond_0
    :goto_0
    return-void

    .line 51523
    :cond_1
    const/4 v1, 0x1

    .line 51524
    invoke-virtual {p0}, LX/0Nj;->e()I

    move-result v4

    .line 51525
    const/4 v0, 0x0

    move v2, v0

    move v3, v1

    :goto_1
    if-ge v2, v4, :cond_2

    .line 51526
    iget-object v0, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MC;

    .line 51527
    iget-object v1, p1, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0MC;

    .line 51528
    const/4 v7, 0x1

    .line 51529
    iget-wide v5, v0, LX/0MC;->e:J

    const-wide/high16 v9, -0x8000000000000000L

    cmp-long v5, v5, v9

    if-eqz v5, :cond_3

    move v5, v7

    .line 51530
    :goto_2
    move v0, v5

    .line 51531
    and-int v1, v3, v0

    .line 51532
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v1

    goto :goto_1

    .line 51533
    :cond_2
    iput-boolean v3, p0, LX/0Nj;->m:Z

    goto :goto_0

    .line 51534
    :cond_3
    iget-object v5, v0, LX/0MC;->a:LX/0MP;

    iget-object v6, v0, LX/0MC;->b:LX/0L7;

    invoke-virtual {v5, v6}, LX/0MP;->a(LX/0L7;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 51535
    iget-object v5, v0, LX/0MC;->b:LX/0L7;

    iget-wide v5, v5, LX/0L7;->e:J

    .line 51536
    :goto_3
    iget-object v8, v1, LX/0MC;->a:LX/0MP;

    .line 51537
    :goto_4
    iget-object v9, v0, LX/0MC;->b:LX/0L7;

    invoke-virtual {v8, v9}, LX/0MP;->a(LX/0L7;)Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, v0, LX/0MC;->b:LX/0L7;

    iget-wide v9, v9, LX/0L7;->e:J

    cmp-long v9, v9, v5

    if-ltz v9, :cond_4

    iget-object v9, v0, LX/0MC;->b:LX/0L7;

    invoke-virtual {v9}, LX/0L7;->c()Z

    move-result v9

    if-nez v9, :cond_6

    .line 51538
    :cond_4
    invoke-virtual {v8}, LX/0MP;->d()V

    goto :goto_4

    .line 51539
    :cond_5
    iget-wide v5, v0, LX/0MC;->d:J

    const-wide/16 v9, 0x1

    add-long/2addr v5, v9

    goto :goto_3

    .line 51540
    :cond_6
    iget-object v5, v0, LX/0MC;->b:LX/0L7;

    invoke-virtual {v8, v5}, LX/0MP;->a(LX/0L7;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 51541
    iget-object v5, v0, LX/0MC;->b:LX/0L7;

    iget-wide v5, v5, LX/0L7;->e:J

    iput-wide v5, v0, LX/0MC;->e:J

    move v5, v7

    .line 51542
    goto :goto_2

    .line 51543
    :cond_7
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public final a_(I)LX/0LS;
    .locals 2

    .prologue
    .line 51558
    new-instance v0, LX/0MC;

    iget-object v1, p0, LX/0Nj;->j:LX/0O1;

    invoke-direct {v0, v1}, LX/0MC;-><init>(LX/0O1;)V

    .line 51559
    iget-object v1, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 51560
    return-object v0
.end method

.method public final b(I)LX/0L4;
    .locals 1

    .prologue
    .line 51518
    invoke-virtual {p0}, LX/0Nj;->b()Z

    move-result v0

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51519
    iget-object v0, p0, LX/0Nj;->i:[LX/0L4;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 51503
    iget-boolean v0, p0, LX/0Nj;->l:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, LX/0Nj;->k:Z

    if-eqz v0, :cond_4

    move v1, v2

    .line 51504
    :goto_0
    iget-object v0, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 51505
    iget-object v0, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MC;

    invoke-virtual {v0}, LX/0MC;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51506
    :goto_1
    return v2

    .line 51507
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 51508
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Nj;->l:Z

    .line 51509
    iget-object v0, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    new-array v0, v0, [LX/0L4;

    iput-object v0, p0, LX/0Nj;->i:[LX/0L4;

    .line 51510
    :goto_2
    iget-object v0, p0, LX/0Nj;->i:[LX/0L4;

    array-length v0, v0

    if-ge v2, v0, :cond_4

    .line 51511
    iget-object v0, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MC;

    .line 51512
    iget-object v1, v0, LX/0MC;->g:LX/0L4;

    move-object v0, v1

    .line 51513
    iget-object v1, v0, LX/0L4;->b:Ljava/lang/String;

    invoke-static {v1}, LX/0Al;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p0, LX/0Nj;->g:I

    if-ne v1, v4, :cond_2

    iget v1, p0, LX/0Nj;->h:I

    if-eq v1, v4, :cond_3

    .line 51514
    :cond_2
    iget v1, p0, LX/0Nj;->g:I

    iget v3, p0, LX/0Nj;->h:I

    invoke-virtual {v0, v1, v3}, LX/0L4;->a(II)LX/0L4;

    move-result-object v0

    .line 51515
    :cond_3
    iget-object v1, p0, LX/0Nj;->i:[LX/0L4;

    aput-object v0, v1, v2

    .line 51516
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 51517
    :cond_4
    iget-boolean v2, p0, LX/0Nj;->l:Z

    goto :goto_1
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 51499
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 51500
    iget-object v0, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MC;

    invoke-virtual {v0}, LX/0MC;->a()V

    .line 51501
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 51502
    :cond_0
    return-void
.end method

.method public final c(I)Z
    .locals 1

    .prologue
    .line 51488
    invoke-virtual {p0}, LX/0Nj;->b()Z

    move-result v0

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51489
    iget-object v0, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MC;

    invoke-virtual {v0}, LX/0MC;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()J
    .locals 8

    .prologue
    .line 51492
    const-wide/high16 v2, -0x8000000000000000L

    .line 51493
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 51494
    iget-object v0, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MC;

    .line 51495
    iget-wide v6, v0, LX/0MC;->f:J

    move-wide v4, v6

    .line 51496
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 51497
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 51498
    :cond_0
    return-wide v2
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 51490
    invoke-virtual {p0}, LX/0Nj;->b()Z

    move-result v0

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51491
    iget-object v0, p0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    return v0
.end method
