.class public LX/0GK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0GI;
.implements LX/0GJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0GI;",
        "LX/0GJ",
        "<",
        "LX/0AY;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final A:Z

.field private final b:Landroid/content/Context;

.field public volatile c:LX/0GH;

.field public d:LX/0AY;

.field private e:LX/0Od;

.field public f:LX/0GJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0GJ",
            "<",
            "LX/0AY;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/Object;

.field private final h:Landroid/net/Uri;

.field private final i:Landroid/net/Uri;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Z

.field private final m:Z

.field private final n:I

.field private final o:I

.field public final p:I

.field private final q:I

.field private final r:I

.field private final s:LX/0GN;

.field private final t:LX/0Gk;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final u:Landroid/os/Handler;

.field public final v:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final w:Z

.field public x:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LX/0GM;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/0Gg;

.field private final z:LX/0GB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34835
    const-class v0, LX/0GK;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0GK;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/content/Context;Landroid/os/Handler;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;LX/0GN;LX/0Gk;LX/0AY;ZLX/0GB;IZI)V
    .locals 12
    .param p10    # LX/0Gk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Landroid/content/Context;",
            "Landroid/os/Handler;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0GN;",
            "LX/0Gk;",
            "LX/0AY;",
            "Z",
            "LX/0GB;",
            "IZI)V"
        }
    .end annotation

    .prologue
    .line 34836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34837
    sget-object v2, LX/0GH;->UNKNOWN:LX/0GH;

    iput-object v2, p0, LX/0GK;->c:LX/0GH;

    .line 34838
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, LX/0GK;->g:Ljava/lang/Object;

    .line 34839
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v2, p0, LX/0GK;->v:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 34840
    new-instance v10, LX/0AZ;

    invoke-direct {v10}, LX/0AZ;-><init>()V

    .line 34841
    invoke-static/range {p8 .. p8}, LX/040;->p(Ljava/util/Map;)Z

    move-result v3

    .line 34842
    new-instance v2, LX/0Gg;

    const/4 v5, 0x0

    if-eqz v3, :cond_6

    invoke-static {}, LX/0Gi;->a()LX/04m;

    move-result-object v8

    :goto_0
    const-string v9, ""

    move-object/from16 v3, p5

    move-object/from16 v4, p10

    move/from16 v6, p15

    move-object/from16 v7, p6

    invoke-direct/range {v2 .. v9}, LX/0Gg;-><init>(Ljava/lang/String;LX/0Gk;ZZLjava/lang/String;LX/04m;Ljava/lang/String;)V

    iput-object v2, p0, LX/0GK;->y:LX/0Gg;

    .line 34843
    invoke-static/range {p8 .. p8}, LX/040;->u(Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 34844
    new-instance v2, LX/0G8;

    iget-object v3, p0, LX/0GK;->y:LX/0Gg;

    const-string v4, "ExoDashLive"

    invoke-static {p2, v3, v4}, LX/0Gt;->a(Landroid/content/Context;LX/04n;Ljava/lang/String;)LX/0OE;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v7, 0x1

    move-object/from16 v4, p5

    move-object/from16 v6, p4

    invoke-direct/range {v2 .. v7}, LX/0G8;-><init>(LX/0OE;Ljava/lang/String;ZLandroid/net/Uri;Z)V

    move-object v4, v2

    .line 34845
    :goto_1
    invoke-static/range {p8 .. p8}, LX/040;->H(Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34846
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "_nc_p_n"

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "_nc_p_o"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    .line 34847
    :cond_0
    new-instance v2, LX/0Od;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v10

    move-object v6, p3

    move-object v7, p0

    move-object/from16 v8, p11

    invoke-direct/range {v2 .. v8}, LX/0Od;-><init>(Ljava/lang/String;LX/0G7;LX/0Aa;Landroid/os/Handler;LX/0GI;Ljava/lang/Object;)V

    iput-object v2, p0, LX/0GK;->e:LX/0Od;

    .line 34848
    iput-object p2, p0, LX/0GK;->b:Landroid/content/Context;

    .line 34849
    move-object/from16 v0, p4

    iput-object v0, p0, LX/0GK;->i:Landroid/net/Uri;

    .line 34850
    move-object/from16 v0, p5

    iput-object v0, p0, LX/0GK;->j:Ljava/lang/String;

    .line 34851
    move-object/from16 v0, p6

    iput-object v0, p0, LX/0GK;->k:Ljava/lang/String;

    .line 34852
    iput-object p1, p0, LX/0GK;->h:Landroid/net/Uri;

    .line 34853
    move/from16 v0, p7

    iput-boolean v0, p0, LX/0GK;->l:Z

    .line 34854
    if-gtz p16, :cond_1

    invoke-static/range {p8 .. p8}, LX/040;->d(Ljava/util/Map;)I

    move-result p16

    :cond_1
    move/from16 v0, p16

    iput v0, p0, LX/0GK;->r:I

    .line 34855
    if-gtz p14, :cond_2

    invoke-static/range {p8 .. p8}, LX/040;->F(Ljava/util/Map;)I

    move-result p14

    :cond_2
    move/from16 v0, p14

    iput v0, p0, LX/0GK;->n:I

    .line 34856
    invoke-static/range {p8 .. p8}, LX/040;->w(Ljava/util/Map;)I

    move-result v2

    iput v2, p0, LX/0GK;->o:I

    .line 34857
    iget v2, p0, LX/0GK;->o:I

    if-eqz v2, :cond_3

    iget v2, p0, LX/0GK;->o:I

    if-lez v2, :cond_8

    invoke-static/range {p8 .. p8}, LX/040;->t(Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_3
    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, p0, LX/0GK;->m:Z

    .line 34858
    invoke-static/range {p8 .. p8}, LX/040;->C(Ljava/util/Map;)I

    move-result v2

    iput v2, p0, LX/0GK;->p:I

    .line 34859
    move-object/from16 v0, p9

    iput-object v0, p0, LX/0GK;->s:LX/0GN;

    .line 34860
    iget-object v2, p0, LX/0GK;->s:LX/0GN;

    invoke-static/range {p8 .. p8}, LX/040;->E(Ljava/util/Map;)I

    move-result v3

    invoke-virtual {v2, v3}, LX/0GN;->a(I)V

    .line 34861
    const/4 v2, 0x0

    iput-object v2, p0, LX/0GK;->x:Ljava/util/Collection;

    .line 34862
    move-object/from16 v0, p10

    iput-object v0, p0, LX/0GK;->t:LX/0Gk;

    .line 34863
    iput-object p3, p0, LX/0GK;->u:Landroid/os/Handler;

    .line 34864
    invoke-static/range {p8 .. p8}, LX/040;->I(Ljava/util/Map;)I

    move-result v2

    iput v2, p0, LX/0GK;->q:I

    .line 34865
    move-object/from16 v0, p13

    iput-object v0, p0, LX/0GK;->z:LX/0GB;

    .line 34866
    invoke-static/range {p8 .. p8}, LX/040;->e(Ljava/util/Map;)Z

    move-result v2

    iput-boolean v2, p0, LX/0GK;->A:Z

    .line 34867
    if-eqz p11, :cond_5

    invoke-static/range {p8 .. p8}, LX/040;->ai(Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 34868
    invoke-static/range {p8 .. p8}, LX/040;->al(Ljava/util/Map;)Z

    move-result v2

    invoke-static/range {p8 .. p8}, LX/040;->am(Ljava/util/Map;)Z

    move-result v3

    move-object/from16 v0, p11

    invoke-direct {p0, v0, v2, v3}, LX/0GK;->a(LX/0AY;ZZ)V

    .line 34869
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 34870
    invoke-static/range {p8 .. p8}, LX/040;->ak(Ljava/util/Map;)J

    move-result-wide v4

    .line 34871
    sget-object v6, LX/0GK;->a:Ljava/lang/String;

    const-string v7, "Availability end time is %d, current time is %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    move-object/from16 v0, p11

    iget-wide v10, v0, LX/0AY;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 34872
    iget-object v6, p0, LX/0GK;->z:LX/0GB;

    move-object/from16 v0, p5

    move-object/from16 v1, p11

    invoke-static {v0, v1, v6}, LX/0GF;->a(Ljava/lang/String;LX/0AY;LX/0GB;)V

    .line 34873
    const-wide v6, 0x7fffffffffffffffL

    cmp-long v6, v4, v6

    if-eqz v6, :cond_4

    move-object/from16 v0, p11

    iget-wide v6, v0, LX/0AY;->b:J

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-gtz v2, :cond_9

    invoke-static/range {p11 .. p11}, LX/0Gj;->a(LX/0AY;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 34874
    :cond_4
    const/4 v2, 0x0

    move-object/from16 v0, p11

    move/from16 v1, p12

    invoke-direct {p0, v0, v1, v2}, LX/0GK;->b(LX/0AY;ZZ)V

    .line 34875
    :cond_5
    :goto_3
    invoke-static/range {p8 .. p8}, LX/040;->au(Ljava/util/Map;)Z

    move-result v2

    iput-boolean v2, p0, LX/0GK;->w:Z

    .line 34876
    return-void

    .line 34877
    :cond_6
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 34878
    :cond_7
    const-string v2, "ExoDashLive"

    iget-object v3, p0, LX/0GK;->y:LX/0Gg;

    invoke-static {v2, v3}, LX/0Gt;->a(Ljava/lang/String;LX/04n;)LX/0Ge;

    move-result-object v4

    goto/16 :goto_1

    .line 34879
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 34880
    :cond_9
    sget-object v2, LX/0GK;->a:Ljava/lang/String;

    const-string v3, "Manifest is too old, skip it: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/0GK;->h:Landroid/net/Uri;

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
.end method

.method private declared-synchronized a(LX/0AY;)V
    .locals 1

    .prologue
    .line 34881
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/0GK;->d:LX/0AY;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34882
    monitor-exit p0

    return-void

    .line 34883
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(LX/0AY;ZZ)V
    .locals 9

    .prologue
    .line 34884
    if-nez p2, :cond_1

    if-nez p3, :cond_1

    .line 34885
    :cond_0
    return-void

    .line 34886
    :cond_1
    invoke-virtual {p1}, LX/0AY;->b()I

    move-result v2

    .line 34887
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 34888
    invoke-virtual {p1, v1}, LX/0AY;->a(I)LX/0Am;

    move-result-object v0

    .line 34889
    iget-object v0, v0, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    .line 34890
    iget-object v0, v0, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 34891
    if-eqz p2, :cond_4

    .line 34892
    iget-object v5, v0, LX/0Ah;->f:LX/0Au;

    move-object v5, v5

    .line 34893
    iget-object v6, v0, LX/0Ah;->g:Ljava/lang/String;

    move-object v6, v6

    .line 34894
    invoke-static {p0, v5, v6}, LX/0GK;->a(LX/0GK;LX/0Au;Ljava/lang/String;)V

    .line 34895
    :cond_4
    if-eqz p3, :cond_3

    .line 34896
    invoke-virtual {v0}, LX/0Ah;->f()LX/0BX;

    move-result-object v5

    .line 34897
    if-eqz v5, :cond_3

    .line 34898
    invoke-interface {v5}, LX/0BX;->a()I

    move-result v0

    .line 34899
    const-wide/16 v6, 0x0

    invoke-interface {v5, v6, v7}, LX/0BX;->a(J)I

    move-result v6

    .line 34900
    :goto_1
    if-gt v0, v6, :cond_3

    .line 34901
    invoke-interface {v5, v0}, LX/0BX;->b(I)LX/0Au;

    move-result-object v7

    invoke-interface {v5, v0}, LX/0BX;->c(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v7, v8}, LX/0GK;->a(LX/0GK;LX/0Au;Ljava/lang/String;)V

    .line 34902
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 34903
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private a(LX/0GH;)V
    .locals 2

    .prologue
    .line 34904
    iget-object v1, p0, LX/0GK;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 34905
    :try_start_0
    iput-object p1, p0, LX/0GK;->c:LX/0GH;

    .line 34906
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(LX/0GK;LX/0Au;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 34907
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 34908
    invoke-virtual {p1}, LX/0Au;->a()Landroid/net/Uri;

    move-result-object v1

    .line 34909
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p2, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 34910
    iget-object v2, p0, LX/0GK;->z:LX/0GB;

    iget-object v3, p0, LX/0GK;->j:Ljava/lang/String;

    array-length v4, v0

    invoke-virtual {v2, v3, v1, v0, v4}, LX/0GB;->a(Ljava/lang/String;Landroid/net/Uri;[BI)V

    .line 34911
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/0GK;->j:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34912
    :cond_0
    :goto_0
    return-void

    .line 34913
    :catch_0
    move-exception v0

    .line 34914
    sget-object v2, LX/0GK;->a:Ljava/lang/String;

    const-string v3, "Invalid inline binary is given for %s, uri=%s"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, LX/0GK;->j:Ljava/lang/String;

    aput-object v5, v4, v6

    aput-object v1, v4, v7

    invoke-static {v2, v0, v3, v4}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private b(LX/0AY;ZZ)V
    .locals 11

    .prologue
    .line 34915
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/0GK;->h:Landroid/net/Uri;

    aput-object v4, v2, v3

    .line 34916
    invoke-direct {p0, p1}, LX/0GK;->a(LX/0AY;)V

    .line 34917
    sget-object v0, LX/0GH;->PREPARED:LX/0GH;

    invoke-direct {p0, v0}, LX/0GK;->a(LX/0GH;)V

    .line 34918
    iget-object v0, p0, LX/0GK;->f:LX/0GJ;

    if-eqz v0, :cond_2

    .line 34919
    iget-object v0, p0, LX/0GK;->f:LX/0GJ;

    invoke-interface {v0, p1}, LX/0GJ;->a(Ljava/lang/Object;)V

    .line 34920
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0GK;->t:LX/0Gk;

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    .line 34921
    iget-object v0, p0, LX/0GK;->t:LX/0Gk;

    sget-object v1, LX/0H9;->MANIFEST_FETECH_END:LX/0H9;

    new-instance v2, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;

    iget-object v3, p0, LX/0GK;->j:Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1, v2}, LX/0Gk;->a(LX/0H9;Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V

    .line 34922
    :cond_1
    return-void

    .line 34923
    :cond_2
    iget-boolean v0, p0, LX/0GK;->l:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 34924
    iget-object v0, p0, LX/0GK;->s:LX/0GN;

    iget-object v2, p0, LX/0GK;->j:Ljava/lang/String;

    iget-object v3, p0, LX/0GK;->k:Ljava/lang/String;

    iget-boolean v1, p0, LX/0GK;->m:Z

    if-eqz v1, :cond_3

    iget-object v4, p0, LX/0GK;->i:Landroid/net/Uri;

    :goto_1
    iget v6, p0, LX/0GK;->r:I

    iget v7, p0, LX/0GK;->n:I

    iget v8, p0, LX/0GK;->o:I

    iget-object v9, p0, LX/0GK;->t:LX/0Gk;

    iget-boolean v10, p0, LX/0GK;->A:Z

    move-object v1, p0

    move-object v5, p1

    invoke-virtual/range {v0 .. v10}, LX/0GN;->a(LX/0GK;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/0AY;IIILX/0Gk;Z)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, LX/0GK;->x:Ljava/util/Collection;

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static b(LX/0GK;LX/0GJ;)Z
    .locals 3

    .prologue
    .line 34925
    iget-object v1, p0, LX/0GK;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 34926
    :try_start_0
    iget-object v0, p0, LX/0GK;->c:LX/0GH;

    sget-object v2, LX/0GH;->PREPARING:LX/0GH;

    if-ne v0, v2, :cond_0

    .line 34927
    iput-object p1, p0, LX/0GK;->f:LX/0GJ;

    .line 34928
    const/4 v0, 0x1

    monitor-exit v1

    .line 34929
    :goto_0
    return v0

    .line 34930
    :cond_0
    monitor-exit v1

    .line 34931
    const/4 v0, 0x0

    goto :goto_0

    .line 34932
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 34933
    iget-object v0, p0, LX/0GK;->e:LX/0Od;

    .line 34934
    iget-object v1, v0, LX/0Od;->m:Ljava/lang/Object;

    move-object v0, v1

    .line 34935
    if-nez v0, :cond_0

    const-string v0, "Not available"

    .line 34936
    :goto_0
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/0GK;->j:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, LX/0GK;->h:Landroid/net/Uri;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    .line 34937
    iget-object v0, p0, LX/0GK;->y:LX/0Gg;

    iget-object v1, p0, LX/0GK;->h:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0AC;->NOT_CACHED:LX/0AC;

    invoke-virtual {v0, v1, v2}, LX/0Gg;->a(Ljava/lang/String;LX/0AC;)V

    .line 34938
    return-void

    .line 34939
    :cond_0
    iget-object v0, p0, LX/0GK;->e:LX/0Od;

    .line 34940
    iget-object v1, v0, LX/0Od;->m:Ljava/lang/Object;

    move-object v0, v1

    .line 34941
    check-cast v0, LX/0AY;

    iget-boolean v0, v0, LX/0AY;->e:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 34830
    iget-object v0, p0, LX/0GK;->v:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 34831
    return-void
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 5

    .prologue
    .line 34832
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/0GK;->j:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/0GK;->h:Landroid/net/Uri;

    aput-object v4, v2, v3

    .line 34833
    iget-object v0, p0, LX/0GK;->y:LX/0Gg;

    invoke-virtual {v0, p1}, LX/0Gg;->a(Ljava/io/IOException;)V

    .line 34834
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 34827
    check-cast p1, LX/0AY;

    const/4 v0, 0x1

    .line 34828
    invoke-direct {p0, p1, v0, v0}, LX/0GK;->b(LX/0AY;ZZ)V

    .line 34829
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34824
    iget-object v0, p0, LX/0GK;->y:LX/0Gg;

    .line 34825
    iput-object p1, v0, LX/0Gg;->g:Ljava/lang/String;

    .line 34826
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 34820
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/0GK;->h:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    .line 34821
    iget-object v0, p0, LX/0GK;->t:LX/0Gk;

    if-eqz v0, :cond_0

    .line 34822
    iget-object v0, p0, LX/0GK;->t:LX/0Gk;

    sget-object v1, LX/0H9;->MANIFEST_MISALIGNED:LX/0H9;

    new-instance v2, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;

    iget-object v3, p0, LX/0GK;->j:Ljava/lang/String;

    iget-object v4, p0, LX/0GK;->h:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, p1, p2}, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, LX/0Gk;->a(LX/0H9;Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V

    .line 34823
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 34812
    iget-object v0, p0, LX/0GK;->e:LX/0Od;

    .line 34813
    iget-object v1, v0, LX/0Od;->m:Ljava/lang/Object;

    move-object v0, v1

    .line 34814
    if-nez v0, :cond_0

    const-string v0, "Not available"

    .line 34815
    :goto_0
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/0GK;->j:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, LX/0GK;->h:Landroid/net/Uri;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    .line 34816
    return-void

    .line 34817
    :cond_0
    iget-object v0, p0, LX/0GK;->e:LX/0Od;

    .line 34818
    iget-object v1, v0, LX/0Od;->m:Ljava/lang/Object;

    move-object v0, v1

    .line 34819
    check-cast v0, LX/0AY;

    iget-boolean v0, v0, LX/0AY;->e:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/io/IOException;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 34794
    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, LX/0GK;->h:Landroid/net/Uri;

    aput-object v3, v2, v6

    .line 34795
    invoke-static {p1}, LX/0Gj;->a(Ljava/io/IOException;)I

    move-result v0

    .line 34796
    sget-object v1, LX/0GH;->FAILED:LX/0GH;

    invoke-direct {p0, v1}, LX/0GK;->a(LX/0GH;)V

    .line 34797
    const/16 v1, 0x19a

    if-ne v0, v1, :cond_3

    iget-boolean v1, p0, LX/0GK;->w:Z

    if-eqz v1, :cond_3

    .line 34798
    sget-object v1, LX/0GH;->DISMISS:LX/0GH;

    invoke-direct {p0, v1}, LX/0GK;->a(LX/0GH;)V

    .line 34799
    invoke-virtual {p0, v5}, LX/0GK;->a(I)V

    .line 34800
    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, LX/0GK;->v:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, LX/0GK;->h:Landroid/net/Uri;

    aput-object v4, v3, v6

    .line 34801
    :cond_0
    iget-object v1, p0, LX/0GK;->f:LX/0GJ;

    if-eqz v1, :cond_1

    .line 34802
    iget-object v1, p0, LX/0GK;->f:LX/0GJ;

    invoke-interface {v1, p1}, LX/0GJ;->b(Ljava/io/IOException;)V

    .line 34803
    :cond_1
    iget-object v1, p0, LX/0GK;->t:LX/0Gk;

    if-eqz v1, :cond_2

    .line 34804
    iget-object v1, p0, LX/0GK;->t:LX/0Gk;

    sget-object v2, LX/0H9;->MANIFEST_FETECH_END:LX/0H9;

    new-instance v3, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;

    iget-object v4, p0, LX/0GK;->j:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v0}, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v1, v2, v3}, LX/0Gk;->a(LX/0H9;Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V

    .line 34805
    :cond_2
    iget-object v0, p0, LX/0GK;->y:LX/0Gg;

    invoke-virtual {v0, p1}, LX/0Gg;->a(Ljava/io/IOException;)V

    .line 34806
    :goto_0
    return-void

    .line 34807
    :cond_3
    iget v1, p0, LX/0GK;->q:I

    if-lez v1, :cond_0

    iget-object v1, p0, LX/0GK;->v:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, LX/0GK;->v:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    if-ltz v1, :cond_0

    .line 34808
    iget v0, p0, LX/0GK;->q:I

    iget-object v1, p0, LX/0GK;->v:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    .line 34809
    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 34810
    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, LX/0GK;->v:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, LX/0GK;->h:Landroid/net/Uri;

    aput-object v4, v3, v6

    .line 34811
    iget-object v1, p0, LX/0GK;->u:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/exoplayer/DashLiveManifestFetcher$2;

    invoke-direct {v2, p0}, Lcom/facebook/exoplayer/DashLiveManifestFetcher$2;-><init>(LX/0GK;)V

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    const v0, -0x257b4a6b

    invoke-static {v1, v2, v4, v5, v0}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34760
    iget-object v0, p0, LX/0GK;->y:LX/0Gg;

    .line 34761
    iget-object p0, v0, LX/0Gg;->g:Ljava/lang/String;

    move-object v0, p0

    .line 34762
    return-object v0
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 34784
    iget-object v1, p0, LX/0GK;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 34785
    :try_start_0
    iget-object v0, p0, LX/0GK;->c:LX/0GH;

    sget-object v2, LX/0GH;->PREPARING:LX/0GH;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, LX/0GK;->c:LX/0GH;

    sget-object v2, LX/0GH;->PREPARED:LX/0GH;

    if-ne v0, v2, :cond_1

    .line 34786
    :cond_0
    monitor-exit v1

    .line 34787
    :goto_0
    return-void

    .line 34788
    :cond_1
    sget-object v0, LX/0GH;->PREPARING:LX/0GH;

    iput-object v0, p0, LX/0GK;->c:LX/0GH;

    .line 34789
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34790
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/0GK;->h:Landroid/net/Uri;

    aput-object v4, v2, v3

    .line 34791
    iget-object v0, p0, LX/0GK;->e:LX/0Od;

    iget-object v1, p0, LX/0GK;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, LX/0Od;->a(Landroid/os/Looper;LX/0GJ;)V

    .line 34792
    iget-object v0, p0, LX/0GK;->y:LX/0Gg;

    iget-object v1, p0, LX/0GK;->h:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0AC;->NOT_CACHED:LX/0AC;

    invoke-virtual {v0, v1, v2}, LX/0Gg;->a(Ljava/lang/String;LX/0AC;)V

    goto :goto_0

    .line 34793
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 34776
    iget-object v1, p0, LX/0GK;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 34777
    :try_start_0
    iget-object v0, p0, LX/0GK;->c:LX/0GH;

    sget-object v2, LX/0GH;->PREPARED:LX/0GH;

    if-eq v0, v2, :cond_0

    .line 34778
    monitor-exit v1

    .line 34779
    :goto_0
    return-void

    .line 34780
    :cond_0
    iget v0, p0, LX/0GK;->q:I

    if-lez v0, :cond_1

    iget-object v0, p0, LX/0GK;->v:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, LX/0GK;->v:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-ltz v0, :cond_1

    .line 34781
    sget-object v0, LX/0GH;->UNKNOWN:LX/0GH;

    iput-object v0, p0, LX/0GK;->c:LX/0GH;

    .line 34782
    iget-object v0, p0, LX/0GK;->u:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/exoplayer/DashLiveManifestFetcher$1;

    invoke-direct {v2, p0}, Lcom/facebook/exoplayer/DashLiveManifestFetcher$1;-><init>(LX/0GK;)V

    const v3, 0x3daf27b9

    invoke-static {v0, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 34783
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f()LX/0Od;
    .locals 1

    .prologue
    .line 34775
    iget-object v0, p0, LX/0GK;->e:LX/0Od;

    return-object v0
.end method

.method public final g()LX/0GH;
    .locals 2

    .prologue
    .line 34772
    iget-object v1, p0, LX/0GK;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 34773
    :try_start_0
    iget-object v0, p0, LX/0GK;->c:LX/0GH;

    monitor-exit v1

    return-object v0

    .line 34774
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final h()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 34763
    iget-object v0, p0, LX/0GK;->c:LX/0GH;

    sget-object v2, LX/0GH;->PREPARED:LX/0GH;

    if-eq v0, v2, :cond_0

    move v0, v1

    .line 34764
    :goto_0
    return v0

    .line 34765
    :cond_0
    iget-object v0, p0, LX/0GK;->x:Ljava/util/Collection;

    if-nez v0, :cond_1

    move v0, v1

    .line 34766
    goto :goto_0

    .line 34767
    :cond_1
    iget-object v0, p0, LX/0GK;->x:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GM;

    .line 34768
    iget-object v3, v0, LX/0GM;->l:LX/0GL;

    sget-object p0, LX/0GL;->COMPLETED:LX/0GL;

    if-ne v3, p0, :cond_4

    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 34769
    if-nez v0, :cond_2

    move v0, v1

    .line 34770
    goto :goto_0

    .line 34771
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method
