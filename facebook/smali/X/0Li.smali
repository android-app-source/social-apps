.class public final LX/0Li;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 44111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(ZIIII)Landroid/graphics/Point;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 44112
    if-eqz p0, :cond_0

    if-le p3, p4, :cond_1

    move v2, v0

    :goto_0
    if-le p1, p2, :cond_2

    :goto_1
    if-eq v2, v0, :cond_0

    move v3, p1

    move p1, p2

    move p2, v3

    .line 44113
    :cond_0
    mul-int v0, p3, p2

    mul-int v1, p4, p1

    if-lt v0, v1, :cond_3

    .line 44114
    new-instance v0, Landroid/graphics/Point;

    mul-int v1, p1, p4

    invoke-static {v1, p3}, LX/08x;->a(II)I

    move-result v1

    invoke-direct {v0, p1, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 44115
    :goto_2
    return-object v0

    :cond_1
    move v2, v1

    .line 44116
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 44117
    :cond_3
    new-instance v0, Landroid/graphics/Point;

    mul-int v1, p2, p3

    invoke-static {v1, p4}, LX/08x;->a(II)I

    move-result v1

    invoke-direct {v0, v1, p2}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_2
.end method

.method private static a(LX/0AR;[Ljava/lang/String;ZI)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 44118
    if-eqz p1, :cond_2

    iget-object v0, p0, LX/0AR;->b:Ljava/lang/String;

    const/4 v3, 0x0

    .line 44119
    move v2, v3

    :goto_0
    array-length v4, p1

    if-ge v2, v4, :cond_0

    .line 44120
    aget-object v4, p1, v2

    invoke-static {v4, v0}, LX/08x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 44121
    const/4 v3, 0x1

    .line 44122
    :cond_0
    move v0, v3

    .line 44123
    if-nez v0, :cond_2

    .line 44124
    :cond_1
    :goto_1
    return v1

    .line 44125
    :cond_2
    if-eqz p2, :cond_3

    iget v0, p0, LX/0AR;->f:I

    const/16 v2, 0x500

    if-ge v0, v2, :cond_1

    iget v0, p0, LX/0AR;->g:I

    const/16 v2, 0x2d0

    if-ge v0, v2, :cond_1

    .line 44126
    :cond_3
    iget v0, p0, LX/0AR;->f:I

    if-lez v0, :cond_7

    iget v0, p0, LX/0AR;->g:I

    if-lez v0, :cond_7

    .line 44127
    sget v0, LX/08x;->a:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_6

    .line 44128
    iget-object v0, p0, LX/0AR;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0Al;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 44129
    const-string v2, "video/x-unknown"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 44130
    const-string v0, "video/avc"

    .line 44131
    :cond_4
    iget v2, p0, LX/0AR;->h:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_5

    .line 44132
    iget v2, p0, LX/0AR;->f:I

    iget v3, p0, LX/0AR;->g:I

    iget v4, p0, LX/0AR;->h:F

    float-to-double v4, v4

    const/4 p1, 0x1

    const/4 p2, 0x0

    .line 44133
    sget p0, LX/08x;->a:I

    const/16 p3, 0x15

    if-lt p0, p3, :cond_9

    move p0, p1

    :goto_2
    invoke-static {p0}, LX/0Av;->b(Z)V

    .line 44134
    invoke-static {v0, v1}, LX/08v;->c(Ljava/lang/String;Z)Landroid/media/MediaCodecInfo$VideoCapabilities;

    move-result-object p0

    .line 44135
    if-eqz p0, :cond_a

    invoke-virtual {p0, v2, v3, v4, v5}, Landroid/media/MediaCodecInfo$VideoCapabilities;->areSizeAndRateSupported(IID)Z

    move-result p0

    if-eqz p0, :cond_a

    :goto_3
    move v1, p1

    .line 44136
    goto :goto_1

    .line 44137
    :cond_5
    iget v2, p0, LX/0AR;->f:I

    iget v3, p0, LX/0AR;->g:I

    const/4 v5, 0x1

    const/4 p0, 0x0

    .line 44138
    sget v4, LX/08x;->a:I

    const/16 p1, 0x15

    if-lt v4, p1, :cond_b

    move v4, v5

    :goto_4
    invoke-static {v4}, LX/0Av;->b(Z)V

    .line 44139
    invoke-static {v0, v1}, LX/08v;->c(Ljava/lang/String;Z)Landroid/media/MediaCodecInfo$VideoCapabilities;

    move-result-object v4

    .line 44140
    if-eqz v4, :cond_c

    invoke-virtual {v4, v2, v3}, Landroid/media/MediaCodecInfo$VideoCapabilities;->isSizeSupported(II)Z

    move-result v4

    if-eqz v4, :cond_c

    :goto_5
    move v1, v5

    .line 44141
    goto :goto_1

    .line 44142
    :cond_6
    iget v0, p0, LX/0AR;->f:I

    iget v2, p0, LX/0AR;->g:I

    mul-int/2addr v0, v2

    if-gt v0, p3, :cond_1

    .line 44143
    :cond_7
    const/4 v1, 0x1

    goto :goto_1

    .line 44144
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_9
    move p0, p2

    .line 44145
    goto :goto_2

    :cond_a
    move p1, p2

    .line 44146
    goto :goto_3

    :cond_b
    move v4, p0

    .line 44147
    goto :goto_4

    :cond_c
    move v5, p0

    .line 44148
    goto :goto_5
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;[Ljava/lang/String;Z)[I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<+",
            "LX/0Ai;",
            ">;[",
            "Ljava/lang/String;",
            "Z)[I"
        }
    .end annotation

    .prologue
    .line 44149
    sget v0, LX/08x;->a:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    sget-object v0, LX/08x;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, LX/08x;->d:Ljava/lang/String;

    const-string v1, "BRAVIA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sony.dtv.hardware.panel.qfhd"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44150
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0xf00

    const/16 v2, 0x870

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 44151
    :goto_0
    move-object v0, v0

    .line 44152
    const/4 v3, 0x1

    iget v4, v0, Landroid/graphics/Point;->x:I

    iget v5, v0, Landroid/graphics/Point;->y:I

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, LX/0Li;->a(Ljava/util/List;[Ljava/lang/String;ZZII)[I

    move-result-object v0

    return-object v0

    .line 44153
    :cond_0
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 44154
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 44155
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 44156
    sget v2, LX/08x;->a:I

    const/16 p0, 0x17

    if-lt v2, p0, :cond_1

    .line 44157
    invoke-virtual {v0}, Landroid/view/Display;->getMode()Landroid/view/Display$Mode;

    move-result-object v2

    .line 44158
    invoke-virtual {v2}, Landroid/view/Display$Mode;->getPhysicalWidth()I

    move-result p0

    iput p0, v1, Landroid/graphics/Point;->x:I

    .line 44159
    invoke-virtual {v2}, Landroid/view/Display$Mode;->getPhysicalHeight()I

    move-result v2

    iput v2, v1, Landroid/graphics/Point;->y:I

    .line 44160
    :goto_1
    move-object v0, v1

    .line 44161
    goto :goto_0

    .line 44162
    :cond_1
    sget v2, LX/08x;->a:I

    const/16 p0, 0x11

    if-lt v2, p0, :cond_2

    .line 44163
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 44164
    goto :goto_1

    .line 44165
    :cond_2
    sget v2, LX/08x;->a:I

    const/16 p0, 0x10

    if-lt v2, p0, :cond_3

    .line 44166
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 44167
    goto :goto_1

    .line 44168
    :cond_3
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    iput v2, v1, Landroid/graphics/Point;->x:I

    .line 44169
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    iput v2, v1, Landroid/graphics/Point;->y:I

    .line 44170
    goto :goto_1
.end method

.method private static a(Ljava/util/List;[Ljava/lang/String;ZZII)[I
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/0Ai;",
            ">;[",
            "Ljava/lang/String;",
            "ZZII)[I"
        }
    .end annotation

    .prologue
    .line 44171
    const v4, 0x7fffffff

    .line 44172
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 44173
    invoke-static {}, LX/08v;->a()I

    move-result v7

    .line 44174
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v8

    .line 44175
    const/4 v3, 0x0

    move v5, v3

    :goto_0
    if-ge v5, v8, :cond_0

    .line 44176
    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Ai;

    invoke-interface {v3}, LX/0Ai;->Y_()LX/0AR;

    move-result-object v9

    .line 44177
    move/from16 v0, p2

    invoke-static {v9, p1, v0, v7}, LX/0Li;->a(LX/0AR;[Ljava/lang/String;ZI)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 44178
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44179
    iget v3, v9, LX/0AR;->f:I

    if-lez v3, :cond_3

    iget v3, v9, LX/0AR;->g:I

    if-lez v3, :cond_3

    if-lez p4, :cond_3

    if-lez p5, :cond_3

    .line 44180
    iget v3, v9, LX/0AR;->f:I

    iget v10, v9, LX/0AR;->g:I

    move/from16 v0, p3

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-static {v0, v1, v2, v3, v10}, LX/0Li;->a(ZIIII)Landroid/graphics/Point;

    move-result-object v10

    .line 44181
    iget v3, v9, LX/0AR;->f:I

    iget v11, v9, LX/0AR;->g:I

    mul-int/2addr v3, v11

    .line 44182
    iget v11, v9, LX/0AR;->f:I

    iget v12, v10, Landroid/graphics/Point;->x:I

    int-to-float v12, v12

    const v13, 0x3f7ae148    # 0.98f

    mul-float/2addr v12, v13

    float-to-int v12, v12

    if-lt v11, v12, :cond_3

    iget v9, v9, LX/0AR;->g:I

    iget v10, v10, Landroid/graphics/Point;->y:I

    int-to-float v10, v10

    const v11, 0x3f7ae148    # 0.98f

    mul-float/2addr v10, v11

    float-to-int v10, v10

    if-lt v9, v10, :cond_3

    if-ge v3, v4, :cond_3

    .line 44183
    :goto_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto :goto_0

    .line 44184
    :cond_0
    const v3, 0x7fffffff

    if-eq v4, v3, :cond_2

    .line 44185
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move v5, v3

    :goto_2
    if-ltz v5, :cond_2

    .line 44186
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Ai;

    invoke-interface {v3}, LX/0Ai;->Y_()LX/0AR;

    move-result-object v3

    .line 44187
    iget v7, v3, LX/0AR;->f:I

    if-lez v7, :cond_1

    iget v7, v3, LX/0AR;->g:I

    if-lez v7, :cond_1

    iget v7, v3, LX/0AR;->f:I

    iget v3, v3, LX/0AR;->g:I

    mul-int/2addr v3, v7

    if-le v3, v4, :cond_1

    .line 44188
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 44189
    :cond_1
    add-int/lit8 v3, v5, -0x1

    move v5, v3

    goto :goto_2

    .line 44190
    :cond_2
    invoke-static {v6}, LX/08x;->a(Ljava/util/List;)[I

    move-result-object v3

    return-object v3

    :cond_3
    move v3, v4

    goto :goto_1
.end method
