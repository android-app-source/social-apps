.class public LX/068;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/066;


# instance fields
.field private final a:LX/063;

.field private final b:Ljava/net/Socket;

.field private final c:[B

.field private final d:Ljava/lang/String;

.field private final e:I

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(LX/063;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17232
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    iput-object v0, p0, LX/068;->b:Ljava/net/Socket;

    .line 17233
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, LX/068;->c:[B

    .line 17234
    const-string v0, "dummy_host"

    iput-object v0, p0, LX/068;->d:Ljava/lang/String;

    .line 17235
    const/16 v0, 0x1bb

    iput v0, p0, LX/068;->e:I

    .line 17236
    iput-boolean v1, p0, LX/068;->f:Z

    .line 17237
    iput-boolean v1, p0, LX/068;->g:Z

    .line 17238
    iput-object p1, p0, LX/068;->a:LX/063;

    .line 17239
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 17240
    monitor-enter p0

    .line 17241
    :try_start_0
    sget-boolean v0, LX/063;->b:Z

    move v0, v0

    .line 17242
    if-eqz v0, :cond_0

    iget-boolean v3, p0, LX/068;->f:Z

    if-eqz v3, :cond_2

    .line 17243
    :cond_0
    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/068;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    move v0, v2

    .line 17244
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    move v0, v1

    .line 17245
    goto :goto_0

    .line 17246
    :cond_2
    :try_start_1
    iget-object v2, p0, LX/068;->b:Ljava/net/Socket;

    iget-object v3, p0, LX/068;->c:[B

    const-string v4, "dummy_host"

    const/16 v5, 0x1bb

    invoke-static {v2, v3, v4, v5}, LX/063;->a(Ljava/net/Socket;[BLjava/lang/String;I)V

    .line 17247
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/068;->g:Z
    :try_end_1
    .catch LX/061; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 17248
    const/4 v1, 0x1

    :try_start_2
    iput-boolean v1, p0, LX/068;->f:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 17249
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 17250
    :catch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/068;->f:Z

    move v0, v1

    goto :goto_0

    :catchall_1
    move-exception v0

    const/4 v1, 0x1

    iput-boolean v1, p0, LX/068;->f:Z

    throw v0
.end method
