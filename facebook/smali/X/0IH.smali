.class public final enum LX/0IH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0IH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0IH;

.field public static final enum DUPLICATED_NOTIFICATION:LX/0IH;

.field public static final enum NOTIFICATION_RECEIVED:LX/0IH;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38502
    new-instance v0, LX/0IH;

    const-string v1, "NOTIFICATION_RECEIVED"

    invoke-direct {v0, v1, v2}, LX/0IH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0IH;->NOTIFICATION_RECEIVED:LX/0IH;

    .line 38503
    new-instance v0, LX/0IH;

    const-string v1, "DUPLICATED_NOTIFICATION"

    invoke-direct {v0, v1, v3}, LX/0IH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0IH;->DUPLICATED_NOTIFICATION:LX/0IH;

    .line 38504
    const/4 v0, 0x2

    new-array v0, v0, [LX/0IH;

    sget-object v1, LX/0IH;->NOTIFICATION_RECEIVED:LX/0IH;

    aput-object v1, v0, v2

    sget-object v1, LX/0IH;->DUPLICATED_NOTIFICATION:LX/0IH;

    aput-object v1, v0, v3

    sput-object v0, LX/0IH;->$VALUES:[LX/0IH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38505
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0IH;
    .locals 1

    .prologue
    .line 38506
    const-class v0, LX/0IH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0IH;

    return-object v0
.end method

.method public static values()[LX/0IH;
    .locals 1

    .prologue
    .line 38507
    sget-object v0, LX/0IH;->$VALUES:[LX/0IH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0IH;

    return-object v0
.end method
