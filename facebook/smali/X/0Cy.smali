.class public final LX/0Cy;
.super LX/0Ci;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/browser/lite/BrowserLiteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteFragment;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 28313
    iput-object p1, p0, LX/0Cy;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 28314
    invoke-direct {p0, p2}, LX/0Ci;-><init>(Landroid/widget/ImageView;)V

    .line 28315
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 28316
    invoke-super {p0, p1}, LX/0Ci;->a(Landroid/graphics/Bitmap;)V

    .line 28317
    iget-object v0, p0, LX/0Cy;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    if-eqz v0, :cond_0

    .line 28318
    iget-object v0, p0, LX/0Cy;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    iget-object v1, p0, LX/0Ci;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->a(Landroid/widget/ImageView;)V

    .line 28319
    :cond_0
    iget-object v0, p0, LX/0Cy;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->p:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 28320
    iget-object v0, p0, LX/0Cy;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->p:Landroid/widget/RelativeLayout;

    sget v1, LX/0E2;->h:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 28321
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 28322
    :cond_1
    return-void
.end method

.method public final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 28312
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, LX/0Cy;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
