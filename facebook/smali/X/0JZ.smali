.class public LX/0JZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0JZ;


# instance fields
.field public a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public b:LX/0Jc;


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Jc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 39529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39530
    iput-object p1, p0, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 39531
    iput-object p2, p0, LX/0JZ;->b:LX/0Jc;

    .line 39532
    return-void
.end method

.method public static a(LX/0QB;)LX/0JZ;
    .locals 3

    .prologue
    .line 39541
    sget-object v0, LX/0JZ;->c:LX/0JZ;

    if-nez v0, :cond_1

    .line 39542
    const-class v1, LX/0JZ;

    monitor-enter v1

    .line 39543
    :try_start_0
    sget-object v0, LX/0JZ;->c:LX/0JZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 39544
    if-eqz v2, :cond_0

    .line 39545
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0JZ;->b(LX/0QB;)LX/0JZ;

    move-result-object v0

    sput-object v0, LX/0JZ;->c:LX/0JZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 39546
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 39547
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 39548
    :cond_1
    sget-object v0, LX/0JZ;->c:LX/0JZ;

    return-object v0

    .line 39549
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 39550
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/0JZ;
    .locals 3

    .prologue
    .line 39551
    new-instance v2, LX/0JZ;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/0Jc;->a(LX/0QB;)LX/0Jc;

    move-result-object v1

    check-cast v1, LX/0Jc;

    invoke-direct {v2, v0, v1}, LX/0JZ;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Jc;)V

    .line 39552
    return-object v2
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    const v2, 0x1d0009

    .line 39535
    iget-object v0, p0, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39536
    if-eqz p1, :cond_1

    .line 39537
    const/4 v0, 0x4

    .line 39538
    :goto_0
    iget-object v1, p0, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 39539
    :cond_0
    return-void

    .line 39540
    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 39533
    iget-object v0, p0, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1d0009

    const/16 v2, 0x66

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 39534
    return-void
.end method
