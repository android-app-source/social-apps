.class public LX/0Ig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/01o;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/01o;LX/05T;)V
    .locals 1

    .prologue
    .line 38813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38814
    iput-object p1, p0, LX/0Ig;->a:Landroid/content/Context;

    .line 38815
    iput-object p2, p0, LX/0Ig;->b:LX/01o;

    .line 38816
    invoke-static {p0}, LX/0Ig;->h(LX/0Ig;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 38817
    const-string p1, "mqtt_version"

    const-string p2, ""

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 38818
    iget-object p2, p3, LX/05T;->c:Ljava/lang/String;

    move-object p2, p2

    .line 38819
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 38820
    invoke-virtual {p0}, LX/0Ig;->a()V

    .line 38821
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string p1, "mqtt_version"

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 38822
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/SharedPreferences;)LX/0If;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 38823
    invoke-static {p0, p1}, LX/0Ig;->b(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v1

    .line 38824
    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38825
    :goto_0
    return-object v0

    .line 38826
    :cond_0
    :try_start_0
    invoke-static {v1}, LX/0If;->a(Ljava/lang/String;)LX/0If;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 38827
    :catch_0
    move-exception v1

    .line 38828
    const-string v2, "RegistrationState"

    const-string v3, "Parse failed"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;LX/0If;Landroid/content/SharedPreferences;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 38829
    :try_start_0
    invoke-virtual {p1}, LX/0If;->a()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 38830
    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, p0, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 38831
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 38832
    :catch_0
    move-exception v1

    .line 38833
    const-string v2, "RegistrationState"

    const-string v3, "RegistrationCacheEntry serialization failed"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 38834
    :try_start_0
    const-string v0, ""

    invoke-interface {p1, p0, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 38835
    :goto_0
    return-object v0

    .line 38836
    :catch_0
    move-exception v0

    .line 38837
    const-string v1, "RegistrationState"

    const-string v2, "get reg state string failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38838
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(LX/0Ig;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 38839
    iget-object v0, p0, LX/0Ig;->a:Landroid/content/Context;

    sget-object v1, LX/01p;->j:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static h(LX/0Ig;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 38840
    iget-object v0, p0, LX/0Ig;->a:Landroid/content/Context;

    sget-object v1, LX/01p;->e:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 38841
    const-string v0, "RegistrationState"

    const-string v1, "invalidateAllTokenCache"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38842
    invoke-static {p0}, LX/0Ig;->g(LX/0Ig;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 38843
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 38844
    invoke-interface {v1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 38845
    invoke-static {v0, v1}, LX/0Ig;->a(Ljava/lang/String;Landroid/content/SharedPreferences;)LX/0If;

    move-result-object v4

    .line 38846
    if-nez v4, :cond_0

    .line 38847
    const-string v4, "RegistrationState"

    const-string v5, "invalid value for %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v8

    invoke-static {v4, v5, v6}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 38848
    :cond_0
    const-string v5, ""

    iput-object v5, v4, LX/0If;->c:Ljava/lang/String;

    .line 38849
    iget-object v5, p0, LX/0Ig;->b:LX/01o;

    invoke-virtual {v5}, LX/01o;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v4, LX/0If;->d:Ljava/lang/Long;

    .line 38850
    :try_start_0
    invoke-virtual {v4}, LX/0If;->a()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 38851
    invoke-interface {v2, v0, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 38852
    :catch_0
    move-exception v0

    .line 38853
    const-string v4, "RegistrationState"

    const-string v5, "RegistrationCacheEntry serialization failed"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v4, v0, v5, v6}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 38854
    :cond_1
    invoke-static {v2}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 38855
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0If;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 38856
    invoke-static {p0}, LX/0Ig;->g(LX/0Ig;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 38857
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 38858
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 38859
    :try_start_0
    const-string v3, "RegistrationState"

    const-string v4, "getRegisteredApps retrieving %s:%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38860
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0If;->a(Ljava/lang/String;)LX/0If;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 38861
    :catch_0
    move-exception v0

    .line 38862
    const-string v3, "RegistrationState"

    const-string v4, "Parse failed"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v3, v0, v4, v5}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 38863
    :cond_0
    return-object v1
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 38864
    const-string v3, "RegistrationState"

    const-string v4, "getValidToken %s"

    new-array v5, v0, [Ljava/lang/Object;

    aput-object p1, v5, v1

    invoke-static {v3, v4, v5}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38865
    invoke-static {p1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-static {v0}, LX/01n;->a(Z)V

    .line 38866
    invoke-static {p0}, LX/0Ig;->g(LX/0Ig;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 38867
    invoke-static {p1, v0}, LX/0Ig;->b(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v1

    .line 38868
    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v0, v2

    .line 38869
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 38870
    goto :goto_0

    .line 38871
    :cond_1
    invoke-static {p1, v0}, LX/0Ig;->a(Ljava/lang/String;Landroid/content/SharedPreferences;)LX/0If;

    move-result-object v0

    .line 38872
    if-nez v0, :cond_2

    move-object v0, v2

    .line 38873
    goto :goto_1

    .line 38874
    :cond_2
    iget-object v1, p0, LX/0Ig;->b:LX/01o;

    invoke-virtual {v1}, LX/01o;->a()J

    move-result-wide v4

    .line 38875
    iget-object v1, v0, LX/0If;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/32 v8, 0x5265c00

    add-long/2addr v6, v8

    cmp-long v1, v6, v4

    if-ltz v1, :cond_3

    iget-object v1, v0, LX/0If;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v6, v4

    if-lez v1, :cond_4

    :cond_3
    move-object v0, v2

    .line 38876
    goto :goto_1

    .line 38877
    :cond_4
    iget-object v0, v0, LX/0If;->c:Ljava/lang/String;

    goto :goto_1
.end method
