.class public LX/0A0;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23183
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 23184
    return-void
.end method

.method public static a(Landroid/content/Context;LX/09y;Ljava/util/concurrent/ScheduledExecutorService;LX/0Zb;LX/0kb;LX/03V;LX/19s;LX/19l;)LX/0A1;
    .locals 7
    .param p2    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 23185
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "video-performance-tracking.data"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 23186
    new-instance v4, LX/0A2;

    const/16 v1, 0x7530

    invoke-direct {v4, v0, p2, v1, p5}, LX/0A2;-><init>(Ljava/io/File;Ljava/util/concurrent/ScheduledExecutorService;ILX/03V;)V

    .line 23187
    new-instance v0, LX/0A1;

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/0A1;-><init>(LX/09y;LX/0Zb;LX/0kb;LX/0A2;LX/19s;LX/19l;)V

    .line 23188
    invoke-virtual {v0}, LX/0A1;->a()V

    .line 23189
    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 0

    .prologue
    .line 23190
    invoke-virtual {p0}, LX/0Q3;->getBinder()LX/0RH;

    .line 23191
    return-void
.end method
