.class public LX/04q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final A:I

.field public final B:I

.field public final C:I

.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:I

.field public final q:I

.field public final r:I

.field public final s:I

.field public final t:I

.field public final u:J

.field public final v:J

.field public final w:I

.field public final x:Z

.field public final y:Z

.field public final z:I


# direct methods
.method private constructor <init>(Lorg/json/JSONObject;)V
    .locals 7

    .prologue
    const/16 v6, 0x1bb

    const/16 v3, 0xa

    const/4 v5, 0x5

    const/4 v4, 0x1

    const/16 v2, 0x3c

    .line 14596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14597
    const-string v0, "host_name_v6"

    const-string v1, "mqtt-mini.facebook.com"

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/04q;->a:Ljava/lang/String;

    .line 14598
    const-string v0, "analytics_endpoint"

    const-string v1, "https://b-api.facebook.com/method/logging.clientevent"

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/04q;->b:Ljava/lang/String;

    .line 14599
    const-string v0, "default_port"

    invoke-virtual {p1, v0, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->c:I

    .line 14600
    const-string v0, "backup_port"

    invoke-virtual {p1, v0, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->d:I

    .line 14601
    const-string v0, "dns_timeout_sec"

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->e:I

    .line 14602
    const-string v0, "socket_timeout_sec"

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->f:I

    .line 14603
    const-string v0, "mqtt_connect_timeout_sec"

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->g:I

    .line 14604
    const-string v0, "response_timeout_sec"

    const/16 v1, 0x3b

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->h:I

    .line 14605
    const-string v0, "back_to_back_retry_attempts"

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->i:I

    .line 14606
    const-string v0, "background_back_to_back_retry_attempts"

    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->j:I

    .line 14607
    const-string v0, "back_to_back_retry_interval_sec"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->k:I

    .line 14608
    const-string v0, "back_off_initial_retry_interval_sec"

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->l:I

    .line 14609
    const-string v0, "background_back_off_initial_retry_interval_sec"

    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->m:I

    .line 14610
    const-string v0, "back_off_max_retry_interval_sec"

    const/16 v1, 0x384

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->n:I

    .line 14611
    const-string v0, "foreground_keepalive_interval_sec"

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->o:I

    .line 14612
    const-string v0, "background_keepalive_interval_persistent_sec"

    const/16 v1, 0x384

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->p:I

    .line 14613
    const-string v0, "skip_ping_threshold_s"

    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->q:I

    .line 14614
    const-string v0, "happy_eyeballs_delay_ms"

    const/16 v1, 0x19

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->r:I

    .line 14615
    const-string v0, "mqtt_client_thread_priority_ui"

    invoke-virtual {p1, v0, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->s:I

    .line 14616
    const-string v0, "mqtt_client_thread_priority_worker"

    invoke-virtual {p1, v0, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->t:I

    .line 14617
    const-string v0, "analytics_log_min_interval_for_sent_ms"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, LX/04q;->u:J

    .line 14618
    const-string v0, "analytics_log_min_interval_for_received_ms"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, LX/04q;->v:J

    .line 14619
    const-string v0, "gcm_ping_mqtt_delay_sec"

    const/16 v1, 0x1e

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->w:I

    .line 14620
    const-string v0, "use_ssl"

    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/04q;->x:Z

    .line 14621
    const-string v0, "use_compression"

    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/04q;->y:Z

    .line 14622
    const-string v0, "ct"

    const v1, 0xea60

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->z:I

    .line 14623
    const-string v0, "short_mqtt_connection_sec"

    invoke-virtual {p1, v0, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->A:I

    .line 14624
    const-string v0, "connect_rate_limiter_limit"

    const/16 v1, 0x28

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->B:I

    .line 14625
    const-string v0, "connect_rate_limiter_interval_s"

    const/16 v1, 0x960

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/04q;->C:I

    .line 14626
    return-void
.end method

.method public static a()LX/04q;
    .locals 1

    .prologue
    .line 14627
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-static {v0}, LX/04q;->a(Lorg/json/JSONObject;)LX/04q;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lorg/json/JSONObject;)LX/04q;
    .locals 1

    .prologue
    .line 14628
    new-instance v0, LX/04q;

    invoke-direct {v0, p0}, LX/04q;-><init>(Lorg/json/JSONObject;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 14629
    if-ne p0, p1, :cond_1

    .line 14630
    :cond_0
    :goto_0
    return v0

    .line 14631
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 14632
    goto :goto_0

    .line 14633
    :cond_3
    check-cast p1, LX/04q;

    .line 14634
    iget v2, p0, LX/04q;->c:I

    iget v3, p1, LX/04q;->c:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 14635
    goto :goto_0

    .line 14636
    :cond_4
    iget v2, p0, LX/04q;->d:I

    iget v3, p1, LX/04q;->d:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 14637
    goto :goto_0

    .line 14638
    :cond_5
    iget v2, p0, LX/04q;->e:I

    iget v3, p1, LX/04q;->e:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 14639
    goto :goto_0

    .line 14640
    :cond_6
    iget v2, p0, LX/04q;->f:I

    iget v3, p1, LX/04q;->f:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 14641
    goto :goto_0

    .line 14642
    :cond_7
    iget v2, p0, LX/04q;->g:I

    iget v3, p1, LX/04q;->g:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 14643
    goto :goto_0

    .line 14644
    :cond_8
    iget v2, p0, LX/04q;->h:I

    iget v3, p1, LX/04q;->h:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 14645
    goto :goto_0

    .line 14646
    :cond_9
    iget v2, p0, LX/04q;->i:I

    iget v3, p1, LX/04q;->i:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 14647
    goto :goto_0

    .line 14648
    :cond_a
    iget v2, p0, LX/04q;->j:I

    iget v3, p1, LX/04q;->j:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 14649
    goto :goto_0

    .line 14650
    :cond_b
    iget v2, p0, LX/04q;->k:I

    iget v3, p1, LX/04q;->k:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 14651
    goto :goto_0

    .line 14652
    :cond_c
    iget v2, p0, LX/04q;->l:I

    iget v3, p1, LX/04q;->l:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 14653
    goto :goto_0

    .line 14654
    :cond_d
    iget v2, p0, LX/04q;->m:I

    iget v3, p1, LX/04q;->m:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 14655
    goto :goto_0

    .line 14656
    :cond_e
    iget v2, p0, LX/04q;->n:I

    iget v3, p1, LX/04q;->n:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 14657
    goto :goto_0

    .line 14658
    :cond_f
    iget v2, p0, LX/04q;->o:I

    iget v3, p1, LX/04q;->o:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 14659
    goto :goto_0

    .line 14660
    :cond_10
    iget v2, p0, LX/04q;->q:I

    iget v3, p1, LX/04q;->q:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 14661
    goto :goto_0

    .line 14662
    :cond_11
    iget v2, p0, LX/04q;->p:I

    iget v3, p1, LX/04q;->p:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 14663
    goto/16 :goto_0

    .line 14664
    :cond_12
    iget v2, p0, LX/04q;->r:I

    iget v3, p1, LX/04q;->r:I

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 14665
    goto/16 :goto_0

    .line 14666
    :cond_13
    iget v2, p0, LX/04q;->s:I

    iget v3, p1, LX/04q;->s:I

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 14667
    goto/16 :goto_0

    .line 14668
    :cond_14
    iget v2, p0, LX/04q;->t:I

    iget v3, p1, LX/04q;->t:I

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 14669
    goto/16 :goto_0

    .line 14670
    :cond_15
    iget-wide v2, p0, LX/04q;->u:J

    iget-wide v4, p1, LX/04q;->u:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_16

    move v0, v1

    .line 14671
    goto/16 :goto_0

    .line 14672
    :cond_16
    iget-wide v2, p0, LX/04q;->v:J

    iget-wide v4, p1, LX/04q;->v:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_17

    move v0, v1

    .line 14673
    goto/16 :goto_0

    .line 14674
    :cond_17
    iget v2, p0, LX/04q;->w:I

    iget v3, p1, LX/04q;->w:I

    if-eq v2, v3, :cond_18

    move v0, v1

    .line 14675
    goto/16 :goto_0

    .line 14676
    :cond_18
    iget-boolean v2, p0, LX/04q;->x:Z

    iget-boolean v3, p1, LX/04q;->x:Z

    if-eq v2, v3, :cond_19

    move v0, v1

    .line 14677
    goto/16 :goto_0

    .line 14678
    :cond_19
    iget-boolean v2, p0, LX/04q;->y:Z

    iget-boolean v3, p1, LX/04q;->y:Z

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 14679
    goto/16 :goto_0

    .line 14680
    :cond_1a
    iget v2, p0, LX/04q;->z:I

    iget v3, p1, LX/04q;->z:I

    if-eq v2, v3, :cond_1b

    move v0, v1

    .line 14681
    goto/16 :goto_0

    .line 14682
    :cond_1b
    iget-object v2, p0, LX/04q;->a:Ljava/lang/String;

    if-eqz v2, :cond_1d

    iget-object v2, p0, LX/04q;->a:Ljava/lang/String;

    iget-object v3, p1, LX/04q;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    :cond_1c
    move v0, v1

    .line 14683
    goto/16 :goto_0

    .line 14684
    :cond_1d
    iget-object v2, p1, LX/04q;->a:Ljava/lang/String;

    if-nez v2, :cond_1c

    .line 14685
    :cond_1e
    iget v2, p0, LX/04q;->A:I

    iget v3, p1, LX/04q;->A:I

    if-eq v2, v3, :cond_1f

    move v0, v1

    .line 14686
    goto/16 :goto_0

    .line 14687
    :cond_1f
    iget v2, p0, LX/04q;->B:I

    iget v3, p1, LX/04q;->B:I

    if-eq v2, v3, :cond_20

    move v0, v1

    .line 14688
    goto/16 :goto_0

    .line 14689
    :cond_20
    iget v2, p0, LX/04q;->C:I

    iget v3, p0, LX/04q;->C:I

    if-eq v2, v3, :cond_21

    move v0, v1

    .line 14690
    goto/16 :goto_0

    .line 14691
    :cond_21
    iget-object v2, p0, LX/04q;->b:Ljava/lang/String;

    if-eqz v2, :cond_22

    iget-object v0, p0, LX/04q;->b:Ljava/lang/String;

    iget-object v1, p1, LX/04q;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0

    :cond_22
    iget-object v2, p1, LX/04q;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 14692
    iget-object v0, p0, LX/04q;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/04q;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 14693
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, LX/04q;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/04q;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    .line 14694
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->c:I

    add-int/2addr v0, v3

    .line 14695
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->d:I

    add-int/2addr v0, v3

    .line 14696
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->e:I

    add-int/2addr v0, v3

    .line 14697
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->f:I

    add-int/2addr v0, v3

    .line 14698
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->g:I

    add-int/2addr v0, v3

    .line 14699
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->h:I

    add-int/2addr v0, v3

    .line 14700
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->i:I

    add-int/2addr v0, v3

    .line 14701
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->j:I

    add-int/2addr v0, v3

    .line 14702
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->k:I

    add-int/2addr v0, v3

    .line 14703
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->l:I

    add-int/2addr v0, v3

    .line 14704
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->m:I

    add-int/2addr v0, v3

    .line 14705
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->n:I

    add-int/2addr v0, v3

    .line 14706
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->o:I

    add-int/2addr v0, v3

    .line 14707
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->p:I

    add-int/2addr v0, v3

    .line 14708
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->q:I

    add-int/2addr v0, v3

    .line 14709
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->r:I

    add-int/2addr v0, v3

    .line 14710
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->s:I

    add-int/2addr v0, v3

    .line 14711
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->t:I

    add-int/2addr v0, v3

    .line 14712
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, LX/04q;->u:J

    iget-wide v6, p0, LX/04q;->u:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 14713
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, LX/04q;->v:J

    iget-wide v6, p0, LX/04q;->v:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 14714
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/04q;->w:I

    add-int/2addr v0, v3

    .line 14715
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/04q;->x:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    add-int/2addr v0, v3

    .line 14716
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, LX/04q;->y:Z

    if-eqz v3, :cond_3

    :goto_3
    add-int/2addr v0, v2

    .line 14717
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/04q;->z:I

    add-int/2addr v0, v1

    .line 14718
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/04q;->A:I

    add-int/2addr v0, v1

    .line 14719
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/04q;->B:I

    add-int/2addr v0, v1

    .line 14720
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/04q;->C:I

    add-int/2addr v0, v1

    .line 14721
    return v0

    :cond_0
    move v0, v1

    .line 14722
    goto/16 :goto_0

    :cond_1
    move v0, v1

    .line 14723
    goto/16 :goto_1

    :cond_2
    move v0, v1

    .line 14724
    goto :goto_2

    :cond_3
    move v2, v1

    .line 14725
    goto :goto_3
.end method
