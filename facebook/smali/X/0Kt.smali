.class public final LX/0Kt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Gb;


# instance fields
.field private final a:LX/0O1;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "LX/0Ks;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/os/Handler;

.field private final e:LX/0Kr;

.field private final f:J

.field private final g:J

.field public final h:F

.field public final i:F

.field public j:I

.field private k:J

.field private l:I

.field private m:Z

.field private n:Z


# direct methods
.method public constructor <init>(LX/0O1;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42374
    invoke-direct {p0, p1, v0, v0}, LX/0Kt;-><init>(LX/0O1;Landroid/os/Handler;LX/0Kr;)V

    .line 42375
    return-void
.end method

.method private constructor <init>(LX/0O1;Landroid/os/Handler;LX/0Kr;)V
    .locals 8

    .prologue
    .line 42376
    const/16 v4, 0x3a98

    const/16 v5, 0x7530

    const v6, 0x3e4ccccd    # 0.2f

    const v7, 0x3f4ccccd    # 0.8f

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, LX/0Kt;-><init>(LX/0O1;Landroid/os/Handler;LX/0Kr;IIFF)V

    .line 42377
    return-void
.end method

.method public constructor <init>(LX/0O1;Landroid/os/Handler;LX/0Kr;IIFF)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x3e8

    .line 42382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42383
    iput-object p1, p0, LX/0Kt;->a:LX/0O1;

    .line 42384
    iput-object p2, p0, LX/0Kt;->d:Landroid/os/Handler;

    .line 42385
    iput-object p3, p0, LX/0Kt;->e:LX/0Kr;

    .line 42386
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0Kt;->b:Ljava/util/List;

    .line 42387
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0Kt;->c:Ljava/util/HashMap;

    .line 42388
    int-to-long v0, p4

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/0Kt;->f:J

    .line 42389
    int-to-long v0, p5

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/0Kt;->g:J

    .line 42390
    iput p6, p0, LX/0Kt;->h:F

    .line 42391
    iput p7, p0, LX/0Kt;->i:F

    .line 42392
    return-void
.end method

.method private a(JJ)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 42378
    const-wide/16 v2, -0x1

    cmp-long v1, p3, v2

    if-nez v1, :cond_1

    .line 42379
    :cond_0
    :goto_0
    return v0

    .line 42380
    :cond_1
    sub-long v2, p3, p1

    .line 42381
    iget-wide v4, p0, LX/0Kt;->g:J

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v0, p0, LX/0Kt;->f:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 42312
    iget-object v0, p0, LX/0Kt;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Kt;->e:LX/0Kr;

    if-eqz v0, :cond_0

    .line 42313
    iget-object v0, p0, LX/0Kt;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/DefaultLoadControl$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/exoplayer/DefaultLoadControl$1;-><init>(LX/0Kt;Z)V

    const v2, -0x2da26d89

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 42314
    :cond_0
    return-void
.end method

.method private c()V
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 42346
    iget v0, p0, LX/0Kt;->l:I

    move v1, v2

    move v3, v0

    move v4, v2

    move v5, v2

    .line 42347
    :goto_0
    iget-object v0, p0, LX/0Kt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 42348
    iget-object v0, p0, LX/0Kt;->c:Ljava/util/HashMap;

    iget-object v6, p0, LX/0Kt;->b:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ks;

    .line 42349
    iget-boolean v6, v0, LX/0Ks;->c:Z

    or-int/2addr v5, v6

    .line 42350
    iget-wide v8, v0, LX/0Ks;->d:J

    cmp-long v6, v8, v10

    if-eqz v6, :cond_0

    move v6, v7

    :goto_1
    or-int/2addr v4, v6

    .line 42351
    iget v0, v0, LX/0Ks;->b:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 42352
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v6, v2

    .line 42353
    goto :goto_1

    .line 42354
    :cond_1
    iget-object v0, p0, LX/0Kt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    if-nez v5, :cond_2

    if-eqz v4, :cond_7

    :cond_2
    const/4 v0, 0x2

    if-eq v3, v0, :cond_3

    if-ne v3, v7, :cond_7

    iget-boolean v0, p0, LX/0Kt;->m:Z

    if-eqz v0, :cond_7

    :cond_3
    move v0, v7

    :goto_2
    iput-boolean v0, p0, LX/0Kt;->m:Z

    .line 42355
    iget-boolean v0, p0, LX/0Kt;->m:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, LX/0Kt;->n:Z

    if-nez v0, :cond_8

    .line 42356
    sget-object v0, LX/0OO;->a:LX/0OO;

    invoke-virtual {v0, v2}, LX/0OO;->a(I)V

    .line 42357
    iput-boolean v7, p0, LX/0Kt;->n:Z

    .line 42358
    invoke-direct {p0, v7}, LX/0Kt;->a(Z)V

    .line 42359
    :cond_4
    :goto_3
    iput-wide v10, p0, LX/0Kt;->k:J

    .line 42360
    iget-boolean v0, p0, LX/0Kt;->m:Z

    if-eqz v0, :cond_9

    .line 42361
    :goto_4
    iget-object v0, p0, LX/0Kt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 42362
    iget-object v0, p0, LX/0Kt;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 42363
    iget-object v1, p0, LX/0Kt;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ks;

    .line 42364
    iget-wide v0, v0, LX/0Ks;->d:J

    .line 42365
    cmp-long v3, v0, v10

    if-eqz v3, :cond_6

    iget-wide v4, p0, LX/0Kt;->k:J

    cmp-long v3, v4, v10

    if-eqz v3, :cond_5

    iget-wide v4, p0, LX/0Kt;->k:J

    cmp-long v3, v0, v4

    if-gez v3, :cond_6

    .line 42366
    :cond_5
    iput-wide v0, p0, LX/0Kt;->k:J

    .line 42367
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    move v0, v2

    .line 42368
    goto :goto_2

    .line 42369
    :cond_8
    iget-boolean v0, p0, LX/0Kt;->m:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, LX/0Kt;->n:Z

    if-eqz v0, :cond_4

    if-nez v5, :cond_4

    .line 42370
    sget-object v0, LX/0OO;->a:LX/0OO;

    invoke-virtual {v0, v2}, LX/0OO;->b(I)V

    .line 42371
    iput-boolean v2, p0, LX/0Kt;->n:Z

    .line 42372
    invoke-direct {p0, v2}, LX/0Kt;->a(Z)V

    goto :goto_3

    .line 42373
    :cond_9
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 42344
    iget-object v0, p0, LX/0Kt;->a:LX/0O1;

    iget v1, p0, LX/0Kt;->j:I

    invoke-interface {v0, v1}, LX/0O1;->a(I)V

    .line 42345
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 42339
    iget-object v0, p0, LX/0Kt;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 42340
    iget-object v0, p0, LX/0Kt;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ks;

    .line 42341
    iget v1, p0, LX/0Kt;->j:I

    iget v0, v0, LX/0Ks;->a:I

    sub-int v0, v1, v0

    iput v0, p0, LX/0Kt;->j:I

    .line 42342
    invoke-direct {p0}, LX/0Kt;->c()V

    .line 42343
    return-void
.end method

.method public final a(Ljava/lang/Object;I)V
    .locals 2

    .prologue
    .line 42335
    iget-object v0, p0, LX/0Kt;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42336
    iget-object v0, p0, LX/0Kt;->c:Ljava/util/HashMap;

    new-instance v1, LX/0Ks;

    invoke-direct {v1, p2}, LX/0Ks;-><init>(I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42337
    iget v0, p0, LX/0Kt;->j:I

    add-int/2addr v0, p2

    iput v0, p0, LX/0Kt;->j:I

    .line 42338
    return-void
.end method

.method public final a(Ljava/lang/Object;JJZ)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42316
    invoke-direct {p0, p2, p3, p4, p5}, LX/0Kt;->a(JJ)I

    move-result v4

    .line 42317
    iget-object v0, p0, LX/0Kt;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ks;

    .line 42318
    iget v3, v0, LX/0Ks;->b:I

    if-ne v3, v4, :cond_0

    iget-wide v6, v0, LX/0Ks;->d:J

    cmp-long v3, v6, p4

    if-nez v3, :cond_0

    iget-boolean v3, v0, LX/0Ks;->c:Z

    if-eq v3, p6, :cond_5

    :cond_0
    move v3, v1

    .line 42319
    :goto_0
    if-eqz v3, :cond_1

    .line 42320
    iput v4, v0, LX/0Ks;->b:I

    .line 42321
    iput-wide p4, v0, LX/0Ks;->d:J

    .line 42322
    iput-boolean p6, v0, LX/0Ks;->c:Z

    .line 42323
    :cond_1
    iget-object v0, p0, LX/0Kt;->a:LX/0O1;

    invoke-interface {v0}, LX/0O1;->b()I

    move-result v4

    .line 42324
    int-to-float v0, v4

    iget v5, p0, LX/0Kt;->j:I

    int-to-float v5, v5

    div-float/2addr v0, v5

    .line 42325
    iget v5, p0, LX/0Kt;->i:F

    cmpl-float v5, v0, v5

    if-lez v5, :cond_8

    const/4 v0, 0x0

    :goto_1
    move v5, v0

    .line 42326
    iget v0, p0, LX/0Kt;->l:I

    if-eq v0, v5, :cond_6

    move v0, v1

    .line 42327
    :goto_2
    if-eqz v0, :cond_2

    .line 42328
    iput v5, p0, LX/0Kt;->l:I

    .line 42329
    :cond_2
    if-nez v3, :cond_3

    if-eqz v0, :cond_4

    .line 42330
    :cond_3
    invoke-direct {p0}, LX/0Kt;->c()V

    .line 42331
    :cond_4
    iget v0, p0, LX/0Kt;->j:I

    if-ge v4, v0, :cond_7

    const-wide/16 v4, -0x1

    cmp-long v0, p4, v4

    if-eqz v0, :cond_7

    iget-wide v4, p0, LX/0Kt;->k:J

    cmp-long v0, p4, v4

    if-gtz v0, :cond_7

    move v0, v1

    :goto_3
    return v0

    :cond_5
    move v3, v2

    .line 42332
    goto :goto_0

    :cond_6
    move v0, v2

    .line 42333
    goto :goto_2

    :cond_7
    move v0, v2

    .line 42334
    goto :goto_3

    :cond_8
    iget v5, p0, LX/0Kt;->h:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_9

    const/4 v0, 0x2

    goto :goto_1

    :cond_9
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final b()LX/0O1;
    .locals 1

    .prologue
    .line 42315
    iget-object v0, p0, LX/0Kt;->a:LX/0O1;

    return-object v0
.end method
