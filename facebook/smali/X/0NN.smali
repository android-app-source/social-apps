.class public final LX/0NN;
.super LX/0NL;
.source ""


# instance fields
.field private final a:LX/0N4;

.field private final b:LX/0NJ;

.field public final c:LX/0Oi;

.field private d:I

.field private e:I

.field public f:Z

.field public g:Z

.field private h:Z

.field public i:I

.field public j:I

.field public k:Z

.field private l:J


# direct methods
.method public constructor <init>(LX/0N4;LX/0NJ;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 50097
    invoke-direct {p0}, LX/0NL;-><init>()V

    .line 50098
    iput-object p1, p0, LX/0NN;->a:LX/0N4;

    .line 50099
    iput-object p2, p0, LX/0NN;->b:LX/0NJ;

    .line 50100
    new-instance v0, LX/0Oi;

    const/16 v1, 0xa

    new-array v1, v1, [B

    invoke-direct {v0, v1}, LX/0Oi;-><init>([B)V

    iput-object v0, p0, LX/0NN;->c:LX/0Oi;

    .line 50101
    iput v2, p0, LX/0NN;->d:I

    .line 50102
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 50094
    iput p1, p0, LX/0NN;->d:I

    .line 50095
    const/4 v0, 0x0

    iput v0, p0, LX/0NN;->e:I

    .line 50096
    return-void
.end method

.method private a(LX/0Oj;[BI)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 50086
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v1

    iget v2, p0, LX/0NN;->e:I

    sub-int v2, p3, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 50087
    if-gtz v1, :cond_1

    .line 50088
    :cond_0
    :goto_0
    return v0

    .line 50089
    :cond_1
    if-nez p2, :cond_2

    .line 50090
    invoke-virtual {p1, v1}, LX/0Oj;->c(I)V

    .line 50091
    :goto_1
    iget v2, p0, LX/0NN;->e:I

    add-int/2addr v1, v2

    iput v1, p0, LX/0NN;->e:I

    .line 50092
    iget v1, p0, LX/0NN;->e:I

    if-eq v1, p3, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 50093
    :cond_2
    iget v2, p0, LX/0NN;->e:I

    invoke-virtual {p1, p2, v2, v1}, LX/0Oj;->a([BII)V

    goto :goto_1
.end method

.method private c()V
    .locals 9

    .prologue
    const/16 v8, 0x1e

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/16 v7, 0xf

    const/4 v6, 0x1

    .line 50008
    iget-object v0, p0, LX/0NN;->c:LX/0Oi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Oi;->a(I)V

    .line 50009
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0NN;->l:J

    .line 50010
    iget-boolean v0, p0, LX/0NN;->f:Z

    if-eqz v0, :cond_1

    .line 50011
    iget-object v0, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v0, v5}, LX/0Oi;->b(I)V

    .line 50012
    iget-object v0, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v0, v4}, LX/0Oi;->c(I)I

    move-result v0

    int-to-long v0, v0

    shl-long/2addr v0, v8

    .line 50013
    iget-object v2, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v2, v6}, LX/0Oi;->b(I)V

    .line 50014
    iget-object v2, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v2, v7}, LX/0Oi;->c(I)I

    move-result v2

    shl-int/lit8 v2, v2, 0xf

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 50015
    iget-object v2, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v2, v6}, LX/0Oi;->b(I)V

    .line 50016
    iget-object v2, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v2, v7}, LX/0Oi;->c(I)I

    move-result v2

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 50017
    iget-object v2, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v2, v6}, LX/0Oi;->b(I)V

    .line 50018
    iget-boolean v2, p0, LX/0NN;->h:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, LX/0NN;->g:Z

    if-eqz v2, :cond_0

    .line 50019
    iget-object v2, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v2, v5}, LX/0Oi;->b(I)V

    .line 50020
    iget-object v2, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v2, v4}, LX/0Oi;->c(I)I

    move-result v2

    int-to-long v2, v2

    shl-long/2addr v2, v8

    .line 50021
    iget-object v4, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v4, v6}, LX/0Oi;->b(I)V

    .line 50022
    iget-object v4, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v4, v7}, LX/0Oi;->c(I)I

    move-result v4

    shl-int/lit8 v4, v4, 0xf

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 50023
    iget-object v4, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v4, v6}, LX/0Oi;->b(I)V

    .line 50024
    iget-object v4, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v4, v7}, LX/0Oi;->c(I)I

    move-result v4

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 50025
    iget-object v4, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v4, v6}, LX/0Oi;->b(I)V

    .line 50026
    iget-object v4, p0, LX/0NN;->b:LX/0NJ;

    invoke-virtual {v4, v2, v3}, LX/0NJ;->a(J)J

    .line 50027
    iput-boolean v6, p0, LX/0NN;->h:Z

    .line 50028
    :cond_0
    iget-object v2, p0, LX/0NN;->b:LX/0NJ;

    invoke-virtual {v2, v0, v1}, LX/0NJ;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/0NN;->l:J

    .line 50029
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50081
    iput v0, p0, LX/0NN;->d:I

    .line 50082
    iput v0, p0, LX/0NN;->e:I

    .line 50083
    iput-boolean v0, p0, LX/0NN;->h:Z

    .line 50084
    iget-object v0, p0, LX/0NN;->a:LX/0N4;

    invoke-virtual {v0}, LX/0N4;->a()V

    .line 50085
    return-void
.end method

.method public final a(LX/0Oj;ZLX/0LU;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 50030
    if-eqz p2, :cond_0

    .line 50031
    iget v0, p0, LX/0NN;->d:I

    packed-switch v0, :pswitch_data_0

    .line 50032
    :goto_0
    :pswitch_0
    invoke-direct {p0, v6}, LX/0NN;->a(I)V

    .line 50033
    :cond_0
    :goto_1
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    if-lez v0, :cond_5

    .line 50034
    iget v0, p0, LX/0NN;->d:I

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 50035
    :pswitch_1
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    invoke-virtual {p1, v0}, LX/0Oj;->c(I)V

    goto :goto_1

    .line 50036
    :pswitch_2
    const-string v0, "TsExtractor"

    const-string v2, "Unexpected start indicator reading extended header"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 50037
    :pswitch_3
    iget v0, p0, LX/0NN;->j:I

    if-eq v0, v5, :cond_1

    .line 50038
    const-string v0, "TsExtractor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected start indicator: expected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, LX/0NN;->j:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " more bytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 50039
    :cond_1
    iget-object v0, p0, LX/0NN;->a:LX/0N4;

    invoke-virtual {v0}, LX/0N4;->b()V

    goto :goto_0

    .line 50040
    :pswitch_4
    iget-object v0, p0, LX/0NN;->c:LX/0Oi;

    iget-object v0, v0, LX/0Oi;->a:[B

    const/16 v2, 0x9

    invoke-direct {p0, p1, v0, v2}, LX/0NN;->a(LX/0Oj;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50041
    const/16 p2, 0x8

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 p3, -0x1

    .line 50042
    iget-object v3, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v3, v0}, LX/0Oi;->a(I)V

    .line 50043
    iget-object v3, p0, LX/0NN;->c:LX/0Oi;

    const/16 v4, 0x18

    invoke-virtual {v3, v4}, LX/0Oi;->c(I)I

    move-result v3

    .line 50044
    if-eq v3, v2, :cond_6

    .line 50045
    const-string v2, "TsExtractor"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p2, "Unexpected start code prefix: "

    invoke-direct {v4, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 50046
    iput p3, p0, LX/0NN;->j:I

    .line 50047
    :goto_2
    move v0, v0

    .line 50048
    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_3
    invoke-direct {p0, v0}, LX/0NN;->a(I)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_3

    .line 50049
    :pswitch_5
    const/16 v0, 0xa

    iget v2, p0, LX/0NN;->i:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 50050
    iget-object v2, p0, LX/0NN;->c:LX/0Oi;

    iget-object v2, v2, LX/0Oi;->a:[B

    invoke-direct {p0, p1, v2, v0}, LX/0NN;->a(LX/0Oj;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget v2, p0, LX/0NN;->i:I

    invoke-direct {p0, p1, v0, v2}, LX/0NN;->a(LX/0Oj;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50051
    invoke-direct {p0}, LX/0NN;->c()V

    .line 50052
    iget-object v0, p0, LX/0NN;->a:LX/0N4;

    iget-wide v2, p0, LX/0NN;->l:J

    iget-boolean v4, p0, LX/0NN;->k:Z

    invoke-virtual {v0, v2, v3, v4}, LX/0N4;->a(JZ)V

    .line 50053
    const/4 v0, 0x3

    invoke-direct {p0, v0}, LX/0NN;->a(I)V

    goto/16 :goto_1

    .line 50054
    :pswitch_6
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    .line 50055
    iget v2, p0, LX/0NN;->j:I

    if-ne v2, v5, :cond_4

    move v2, v1

    .line 50056
    :goto_4
    if-lez v2, :cond_3

    .line 50057
    sub-int/2addr v0, v2

    .line 50058
    iget v2, p1, LX/0Oj;->b:I

    move v2, v2

    .line 50059
    add-int/2addr v2, v0

    invoke-virtual {p1, v2}, LX/0Oj;->a(I)V

    .line 50060
    :cond_3
    iget-object v2, p0, LX/0NN;->a:LX/0N4;

    invoke-virtual {v2, p1}, LX/0N4;->a(LX/0Oj;)V

    .line 50061
    iget v2, p0, LX/0NN;->j:I

    if-eq v2, v5, :cond_0

    .line 50062
    iget v2, p0, LX/0NN;->j:I

    sub-int v0, v2, v0

    iput v0, p0, LX/0NN;->j:I

    .line 50063
    iget v0, p0, LX/0NN;->j:I

    if-nez v0, :cond_0

    .line 50064
    iget-object v0, p0, LX/0NN;->a:LX/0N4;

    invoke-virtual {v0}, LX/0N4;->b()V

    .line 50065
    invoke-direct {p0, v6}, LX/0NN;->a(I)V

    goto/16 :goto_1

    .line 50066
    :cond_4
    iget v2, p0, LX/0NN;->j:I

    sub-int v2, v0, v2

    goto :goto_4

    .line 50067
    :cond_5
    return-void

    .line 50068
    :cond_6
    iget-object v0, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v0, p2}, LX/0Oi;->b(I)V

    .line 50069
    iget-object v0, p0, LX/0NN;->c:LX/0Oi;

    const/16 v3, 0x10

    invoke-virtual {v0, v3}, LX/0Oi;->c(I)I

    move-result v0

    .line 50070
    iget-object v3, p0, LX/0NN;->c:LX/0Oi;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, LX/0Oi;->b(I)V

    .line 50071
    iget-object v3, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v3

    iput-boolean v3, p0, LX/0NN;->k:Z

    .line 50072
    iget-object v3, p0, LX/0NN;->c:LX/0Oi;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/0Oi;->b(I)V

    .line 50073
    iget-object v3, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v3

    iput-boolean v3, p0, LX/0NN;->f:Z

    .line 50074
    iget-object v3, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v3

    iput-boolean v3, p0, LX/0NN;->g:Z

    .line 50075
    iget-object v3, p0, LX/0NN;->c:LX/0Oi;

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, LX/0Oi;->b(I)V

    .line 50076
    iget-object v3, p0, LX/0NN;->c:LX/0Oi;

    invoke-virtual {v3, p2}, LX/0Oi;->c(I)I

    move-result v3

    iput v3, p0, LX/0NN;->i:I

    .line 50077
    if-nez v0, :cond_7

    .line 50078
    iput p3, p0, LX/0NN;->j:I

    :goto_5
    move v0, v2

    .line 50079
    goto/16 :goto_2

    .line 50080
    :cond_7
    add-int/lit8 v0, v0, 0x6

    add-int/lit8 v0, v0, -0x9

    iget v3, p0, LX/0NN;->i:I

    sub-int/2addr v0, v3

    iput v0, p0, LX/0NN;->j:I

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
