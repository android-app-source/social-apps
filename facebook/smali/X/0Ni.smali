.class public LX/0Ni;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Nh;


# instance fields
.field private A:[B

.field public final a:Z

.field private final b:LX/0G6;

.field private final c:LX/0Nq;

.field public final d:LX/0Nl;

.field public final e:LX/0Nb;

.field private final f:LX/04m;

.field public final g:LX/0Nt;

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:J

.field private final k:J

.field public final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0Nf;",
            ">;"
        }
    .end annotation
.end field

.field public m:I

.field private n:[LX/0Nv;

.field public o:[LX/0Nn;

.field public p:[J

.field private q:[J

.field private r:I

.field public s:Z

.field private t:[B

.field public u:Z

.field public v:J

.field public w:Ljava/io/IOException;

.field private x:Landroid/net/Uri;

.field private y:[B

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLX/0G6;Ljava/lang/String;LX/0Nk;LX/0Nb;LX/04m;LX/0Nt;I)V
    .locals 14

    .prologue
    .line 51432
    const-wide/16 v10, 0x1388

    const-wide/16 v12, 0x4e20

    move-object v1, p0

    move v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v13}, LX/0Ni;-><init>(ZLX/0G6;Ljava/lang/String;LX/0Nk;LX/0Nb;LX/04m;LX/0Nt;IJJ)V

    .line 51433
    return-void
.end method

.method private constructor <init>(ZLX/0G6;Ljava/lang/String;LX/0Nk;LX/0Nb;LX/04m;LX/0Nt;IJJ)V
    .locals 15

    .prologue
    .line 51250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51251
    move/from16 v0, p1

    iput-boolean v0, p0, LX/0Ni;->a:Z

    .line 51252
    move-object/from16 v0, p2

    iput-object v0, p0, LX/0Ni;->b:LX/0G6;

    .line 51253
    move-object/from16 v0, p5

    iput-object v0, p0, LX/0Ni;->e:LX/0Nb;

    .line 51254
    move-object/from16 v0, p6

    iput-object v0, p0, LX/0Ni;->f:LX/04m;

    .line 51255
    move-object/from16 v0, p7

    iput-object v0, p0, LX/0Ni;->g:LX/0Nt;

    .line 51256
    move/from16 v0, p8

    iput v0, p0, LX/0Ni;->h:I

    .line 51257
    const-wide/16 v2, 0x3e8

    mul-long v2, v2, p9

    iput-wide v2, p0, LX/0Ni;->j:J

    .line 51258
    const-wide/16 v2, 0x3e8

    mul-long v2, v2, p11

    iput-wide v2, p0, LX/0Ni;->k:J

    .line 51259
    move-object/from16 v0, p4

    iget-object v2, v0, LX/0Nk;->g:Ljava/lang/String;

    iput-object v2, p0, LX/0Ni;->i:Ljava/lang/String;

    .line 51260
    new-instance v2, LX/0Nq;

    invoke-direct {v2}, LX/0Nq;-><init>()V

    iput-object v2, p0, LX/0Ni;->c:LX/0Nq;

    .line 51261
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, LX/0Ni;->l:Ljava/util/ArrayList;

    .line 51262
    move-object/from16 v0, p4

    iget v2, v0, LX/0Nk;->h:I

    if-nez v2, :cond_0

    .line 51263
    check-cast p4, LX/0Nl;

    move-object/from16 v0, p4

    iput-object v0, p0, LX/0Ni;->d:LX/0Nl;

    .line 51264
    :goto_0
    return-void

    .line 51265
    :cond_0
    new-instance v2, LX/0AR;

    const-string v3, "0"

    const-string v4, "application/x-mpegURL"

    const/4 v5, -0x1

    const/4 v6, -0x1

    const/high16 v7, -0x40800000    # -1.0f

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/4 v10, -0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, LX/0AR;-><init>(Ljava/lang/String;Ljava/lang/String;IIFIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51266
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 51267
    new-instance v3, LX/0Nv;

    move-object/from16 v0, p3

    invoke-direct {v3, v0, v2}, LX/0Nv;-><init>(Ljava/lang/String;LX/0AR;)V

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51268
    new-instance v2, LX/0Nl;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, p3

    invoke-direct/range {v2 .. v8}, LX/0Nl;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, LX/0Ni;->d:LX/0Nl;

    goto :goto_0
.end method

.method private static a(LX/0Ni;J)I
    .locals 11

    .prologue
    const-wide/16 v0, 0x0

    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 51434
    const-wide/16 v6, -0x1

    cmp-long v2, p1, v6

    if-nez v2, :cond_0

    move-wide p1, v0

    .line 51435
    :cond_0
    long-to-float v2, p1

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v2, v4

    float-to-int v6, v2

    move v2, v3

    move v4, v5

    .line 51436
    :goto_0
    iget-object v7, p0, LX/0Ni;->n:[LX/0Nv;

    array-length v7, v7

    if-ge v2, v7, :cond_3

    .line 51437
    iget-object v7, p0, LX/0Ni;->q:[J

    aget-wide v8, v7, v2

    cmp-long v7, v8, v0

    if-nez v7, :cond_2

    .line 51438
    iget-object v4, p0, LX/0Ni;->n:[LX/0Nv;

    aget-object v4, v4, v2

    iget-object v4, v4, LX/0Nv;->b:LX/0AR;

    iget v4, v4, LX/0AR;->c:I

    if-gt v4, v6, :cond_1

    .line 51439
    :goto_1
    return v2

    :cond_1
    move v4, v2

    .line 51440
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 51441
    :cond_3
    if-eq v4, v5, :cond_4

    const/4 v3, 0x1

    :cond_4
    invoke-static {v3}, LX/0Av;->b(Z)V

    move v2, v4

    .line 51442
    goto :goto_1
.end method

.method private static a(LX/0Ni;LX/0AR;)I
    .locals 3

    .prologue
    .line 51443
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/0Ni;->n:[LX/0Nv;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 51444
    iget-object v1, p0, LX/0Ni;->n:[LX/0Nv;

    aget-object v1, v1, v0

    iget-object v1, v1, LX/0Nv;->b:LX/0AR;

    invoke-virtual {v1, p1}, LX/0AR;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51445
    return v0

    .line 51446
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51447
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid format: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(LX/0Ni;LX/0Nu;J)I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 51448
    invoke-static {p0}, LX/0Ni;->m(LX/0Ni;)V

    .line 51449
    iget-object v0, p0, LX/0Ni;->f:LX/04m;

    invoke-interface {v0}, LX/04m;->a()J

    move-result-wide v0

    .line 51450
    iget-object v2, p0, LX/0Ni;->q:[J

    iget v3, p0, LX/0Ni;->r:I

    aget-wide v2, v2, v3

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 51451
    invoke-static {p0, v0, v1}, LX/0Ni;->a(LX/0Ni;J)I

    move-result v0

    .line 51452
    :goto_0
    return v0

    .line 51453
    :cond_0
    if-nez p1, :cond_1

    .line 51454
    iget v0, p0, LX/0Ni;->r:I

    goto :goto_0

    .line 51455
    :cond_1
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 51456
    iget v0, p0, LX/0Ni;->r:I

    goto :goto_0

    .line 51457
    :cond_2
    invoke-static {p0, v0, v1}, LX/0Ni;->a(LX/0Ni;J)I

    move-result v2

    .line 51458
    iget v0, p0, LX/0Ni;->r:I

    if-ne v2, v0, :cond_3

    .line 51459
    iget v0, p0, LX/0Ni;->r:I

    goto :goto_0

    .line 51460
    :cond_3
    iget v0, p0, LX/0Ni;->h:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    iget-wide v0, p1, LX/0LQ;->h:J

    .line 51461
    :goto_1
    sub-long/2addr v0, p2

    .line 51462
    iget-object v3, p0, LX/0Ni;->q:[J

    iget v4, p0, LX/0Ni;->r:I

    aget-wide v4, v3, v4

    cmp-long v3, v4, v6

    if-nez v3, :cond_5

    iget v3, p0, LX/0Ni;->r:I

    if-le v2, v3, :cond_4

    iget-wide v4, p0, LX/0Ni;->k:J

    cmp-long v3, v0, v4

    if-ltz v3, :cond_5

    :cond_4
    iget v3, p0, LX/0Ni;->r:I

    if-ge v2, v3, :cond_7

    iget-wide v4, p0, LX/0Ni;->j:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_7

    :cond_5
    move v0, v2

    .line 51463
    goto :goto_0

    .line 51464
    :cond_6
    iget-wide v0, p1, LX/0LQ;->i:J

    goto :goto_1

    .line 51465
    :cond_7
    iget v0, p0, LX/0Ni;->r:I

    goto :goto_0
.end method

.method private static a(LX/0Ni;Landroid/net/Uri;Ljava/lang/String;I)LX/0Ne;
    .locals 8

    .prologue
    .line 51486
    new-instance v0, LX/0OA;

    const-wide/16 v2, 0x0

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, LX/0OA;-><init>(Landroid/net/Uri;JJLjava/lang/String;I)V

    .line 51487
    new-instance v1, LX/0Ne;

    iget-object v2, p0, LX/0Ni;->b:LX/0G6;

    iget-object v4, p0, LX/0Ni;->t:[B

    move-object v3, v0

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, LX/0Ne;-><init>(LX/0G6;LX/0OA;[BLjava/lang/String;I)V

    return-object v1
.end method

.method private static a(LX/0Ni;Landroid/net/Uri;Ljava/lang/String;[B)V
    .locals 5

    .prologue
    const/16 v3, 0x10

    .line 51466
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51467
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 51468
    :goto_0
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v0, v3}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v1

    .line 51469
    new-array v2, v3, [B

    .line 51470
    array-length v0, v1

    if-le v0, v3, :cond_1

    array-length v0, v1

    add-int/lit8 v0, v0, -0x10

    .line 51471
    :goto_1
    array-length v3, v1

    rsub-int/lit8 v3, v3, 0x10

    add-int/2addr v3, v0

    array-length v4, v1

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 51472
    iput-object p1, p0, LX/0Ni;->x:Landroid/net/Uri;

    .line 51473
    iput-object p3, p0, LX/0Ni;->y:[B

    .line 51474
    iput-object p2, p0, LX/0Ni;->z:Ljava/lang/String;

    .line 51475
    iput-object v2, p0, LX/0Ni;->A:[B

    .line 51476
    return-void

    :cond_0
    move-object v0, p2

    .line 51477
    goto :goto_0

    .line 51478
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static c(LX/0Ni;I)Z
    .locals 6

    .prologue
    .line 51479
    iget-object v0, p0, LX/0Ni;->o:[LX/0Nn;

    aget-object v0, v0, p1

    .line 51480
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v1, p0, LX/0Ni;->p:[J

    aget-wide v4, v1, p1

    sub-long/2addr v2, v4

    .line 51481
    iget v0, v0, LX/0Nn;->b:I

    mul-int/lit16 v0, v0, 0x3e8

    div-int/lit8 v0, v0, 0x2

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/0Ni;I)I
    .locals 3

    .prologue
    .line 51482
    iget-object v0, p0, LX/0Ni;->o:[LX/0Nn;

    aget-object v1, v0, p1

    .line 51483
    iget-object v0, v1, LX/0Nn;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x3

    if-le v0, v2, :cond_0

    iget-object v0, v1, LX/0Nn;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    .line 51484
    :goto_0
    iget v1, v1, LX/0Nn;->a:I

    add-int/2addr v0, v1

    return v0

    .line 51485
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(LX/0Ni;I)LX/0Ng;
    .locals 9

    .prologue
    .line 51424
    iget-object v0, p0, LX/0Ni;->i:Ljava/lang/String;

    iget-object v1, p0, LX/0Ni;->n:[LX/0Nv;

    aget-object v1, v1, p1

    iget-object v1, v1, LX/0Nv;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0At;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 51425
    new-instance v0, LX/0OA;

    const-wide/16 v2, 0x0

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, LX/0OA;-><init>(Landroid/net/Uri;JJLjava/lang/String;I)V

    .line 51426
    new-instance v8, LX/0Ng;

    iget-object v2, p0, LX/0Ni;->b:LX/0G6;

    iget-object v4, p0, LX/0Ni;->t:[B

    iget-object v5, p0, LX/0Ni;->c:LX/0Nq;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v1, v8

    move-object v3, v0

    move v6, p1

    invoke-direct/range {v1 .. v7}, LX/0Ng;-><init>(LX/0G6;LX/0OA;[BLX/0Nq;ILjava/lang/String;)V

    return-object v8
.end method

.method private static k(LX/0Ni;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51427
    iput-object v0, p0, LX/0Ni;->x:Landroid/net/Uri;

    .line 51428
    iput-object v0, p0, LX/0Ni;->y:[B

    .line 51429
    iput-object v0, p0, LX/0Ni;->z:Ljava/lang/String;

    .line 51430
    iput-object v0, p0, LX/0Ni;->A:[B

    .line 51431
    return-void
.end method

.method private static l(LX/0Ni;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 51419
    move v0, v1

    :goto_0
    iget-object v2, p0, LX/0Ni;->q:[J

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 51420
    iget-object v2, p0, LX/0Ni;->q:[J

    aget-wide v2, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 51421
    :goto_1
    return v1

    .line 51422
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51423
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private static m(LX/0Ni;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 51413
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 51414
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/0Ni;->q:[J

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 51415
    iget-object v1, p0, LX/0Ni;->q:[J

    aget-wide v4, v1, v0

    cmp-long v1, v4, v8

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0Ni;->q:[J

    aget-wide v4, v1, v0

    sub-long v4, v2, v4

    const-wide/32 v6, 0xea60

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 51416
    iget-object v1, p0, LX/0Ni;->q:[J

    aput-wide v8, v1, v0

    .line 51417
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51418
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 51410
    iget-object v0, p0, LX/0Ni;->w:Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 51411
    iget-object v0, p0, LX/0Ni;->w:Ljava/io/IOException;

    throw v0

    .line 51412
    :cond_0
    return-void
.end method

.method public final a(LX/0LP;)V
    .locals 7

    .prologue
    .line 51391
    instance-of v0, p1, LX/0Ng;

    if-eqz v0, :cond_1

    .line 51392
    check-cast p1, LX/0Ng;

    .line 51393
    iget-object v0, p1, LX/0Lb;->a:[B

    move-object v0, v0

    .line 51394
    iput-object v0, p0, LX/0Ni;->t:[B

    .line 51395
    iget v0, p1, LX/0Ng;->a:I

    .line 51396
    iget-object v1, p1, LX/0Ng;->j:LX/0Nn;

    move-object v1, v1

    .line 51397
    iget-object v3, p0, LX/0Ni;->p:[J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    aput-wide v5, v3, v0

    .line 51398
    iget-object v3, p0, LX/0Ni;->o:[LX/0Nn;

    aput-object v1, v3, v0

    .line 51399
    iget-boolean v3, p0, LX/0Ni;->u:Z

    iget-boolean v4, v1, LX/0Nn;->e:Z

    or-int/2addr v3, v4

    iput-boolean v3, p0, LX/0Ni;->u:Z

    .line 51400
    iget-boolean v3, p0, LX/0Ni;->u:Z

    if-eqz v3, :cond_2

    const-wide/16 v3, -0x1

    :goto_0
    iput-wide v3, p0, LX/0Ni;->v:J

    .line 51401
    :cond_0
    :goto_1
    return-void

    .line 51402
    :cond_1
    instance-of v0, p1, LX/0Ne;

    if-eqz v0, :cond_0

    .line 51403
    check-cast p1, LX/0Ne;

    .line 51404
    iget-object v0, p1, LX/0Lb;->a:[B

    move-object v0, v0

    .line 51405
    iput-object v0, p0, LX/0Ni;->t:[B

    .line 51406
    iget-object v0, p1, LX/0LP;->e:LX/0OA;

    iget-object v0, v0, LX/0OA;->a:Landroid/net/Uri;

    iget-object v1, p1, LX/0Ne;->a:Ljava/lang/String;

    .line 51407
    iget-object v2, p1, LX/0Ne;->i:[B

    move-object v2, v2

    .line 51408
    invoke-static {p0, v0, v1, v2}, LX/0Ni;->a(LX/0Ni;Landroid/net/Uri;Ljava/lang/String;[B)V

    goto :goto_1

    .line 51409
    :cond_2
    iget-wide v3, v1, LX/0Nn;->f:J

    goto :goto_0
.end method

.method public final a(LX/0Nl;[LX/0Nv;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 51376
    new-instance v1, LX/0Nd;

    invoke-direct {v1, p0}, LX/0Nd;-><init>(LX/0Ni;)V

    invoke-static {p2, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 51377
    const v1, 0x7fffffff

    move v3, v2

    move v4, v0

    .line 51378
    :goto_0
    array-length v5, p2

    if-ge v0, v5, :cond_1

    .line 51379
    iget-object v5, p1, LX/0Nl;->a:Ljava/util/List;

    aget-object v6, p2, v0

    invoke-interface {v5, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    .line 51380
    if-ge v5, v1, :cond_0

    move v1, v5

    move v4, v0

    .line 51381
    :cond_0
    aget-object v5, p2, v0

    iget-object v5, v5, LX/0Nv;->b:LX/0AR;

    .line 51382
    iget v6, v5, LX/0AR;->f:I

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 51383
    iget v5, v5, LX/0AR;->g:I

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 51384
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51385
    :cond_1
    if-lez v3, :cond_2

    .line 51386
    :goto_1
    if-lez v2, :cond_3

    .line 51387
    :goto_2
    iget-object v0, p0, LX/0Ni;->l:Ljava/util/ArrayList;

    new-instance v1, LX/0Nf;

    invoke-direct {v1, p2, v4, v3, v2}, LX/0Nf;-><init>([LX/0Nv;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51388
    return-void

    .line 51389
    :cond_2
    const/16 v3, 0x780

    goto :goto_1

    .line 51390
    :cond_3
    const/16 v2, 0x438

    goto :goto_2
.end method

.method public final a(LX/0Nu;JLX/0LW;)V
    .locals 24

    .prologue
    .line 51303
    move-object/from16 v0, p0

    iget v2, v0, LX/0Ni;->h:I

    if-nez v2, :cond_2

    .line 51304
    move-object/from16 v0, p0

    iget v2, v0, LX/0Ni;->r:I

    .line 51305
    :cond_0
    const/4 v9, 0x0

    .line 51306
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0Ni;->o:[LX/0Nn;

    aget-object v4, v3, v2

    .line 51307
    if-nez v4, :cond_3

    .line 51308
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0Ni;->e(LX/0Ni;I)LX/0Ng;

    move-result-object v2

    move-object/from16 v0, p4

    iput-object v2, v0, LX/0LW;->b:LX/0LP;

    .line 51309
    :cond_1
    :goto_1
    return-void

    .line 51310
    :cond_2
    invoke-static/range {p0 .. p3}, LX/0Ni;->a(LX/0Ni;LX/0Nu;J)I

    move-result v2

    .line 51311
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0Ni;->n:[LX/0Nv;

    aget-object v3, v3, v2

    iget-object v3, v3, LX/0Nv;->b:LX/0AR;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/0LP;->d:LX/0AR;

    invoke-virtual {v3, v4}, LX/0AR;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget v3, v0, LX/0Ni;->h:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v9, 0x1

    goto :goto_0

    .line 51312
    :cond_3
    move-object/from16 v0, p0

    iput v2, v0, LX/0Ni;->r:I

    .line 51313
    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/0Ni;->u:Z

    if-eqz v3, :cond_6

    .line 51314
    if-nez p1, :cond_4

    .line 51315
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0Ni;->d(LX/0Ni;I)I

    move-result v3

    :goto_2
    move/from16 v18, v3

    .line 51316
    :goto_3
    iget v3, v4, LX/0Nn;->a:I

    sub-int v3, v18, v3

    .line 51317
    iget-object v5, v4, LX/0Nn;->d:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v3, v5, :cond_a

    .line 51318
    iget-boolean v3, v4, LX/0Nn;->e:Z

    if-nez v3, :cond_9

    .line 51319
    const/4 v2, 0x1

    move-object/from16 v0, p4

    iput-boolean v2, v0, LX/0LW;->c:Z

    goto :goto_1

    .line 51320
    :cond_4
    if-eqz v9, :cond_5

    move-object/from16 v0, p1

    iget v3, v0, LX/0LQ;->j:I

    .line 51321
    :goto_4
    iget v5, v4, LX/0Nn;->a:I

    if-ge v3, v5, :cond_19

    .line 51322
    new-instance v2, LX/0Kn;

    invoke-direct {v2}, LX/0Kn;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/0Ni;->w:Ljava/io/IOException;

    goto :goto_1

    .line 51323
    :cond_5
    move-object/from16 v0, p1

    iget v3, v0, LX/0LQ;->j:I

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 51324
    :cond_6
    if-nez p1, :cond_7

    .line 51325
    iget-object v3, v4, LX/0Nn;->d:Ljava/util/List;

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-static {v3, v5, v6, v7}, LX/08x;->a(Ljava/util/List;Ljava/lang/Object;ZZ)I

    move-result v3

    iget v5, v4, LX/0Nn;->a:I

    add-int/2addr v3, v5

    goto :goto_2

    .line 51326
    :cond_7
    if-eqz v9, :cond_8

    move-object/from16 v0, p1

    iget v3, v0, LX/0LQ;->j:I

    goto :goto_2

    :cond_8
    move-object/from16 v0, p1

    iget v3, v0, LX/0LQ;->j:I

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 51327
    :cond_9
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0Ni;->c(LX/0Ni;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 51328
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0Ni;->e(LX/0Ni;I)LX/0Ng;

    move-result-object v2

    move-object/from16 v0, p4

    iput-object v2, v0, LX/0LW;->b:LX/0LP;

    goto/16 :goto_1

    .line 51329
    :cond_a
    iget-object v2, v4, LX/0Nn;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v13, v2

    check-cast v13, LX/0Nm;

    .line 51330
    iget-object v2, v4, LX/0Nk;->g:Ljava/lang/String;

    iget-object v3, v13, LX/0Nm;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0At;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 51331
    iget-boolean v2, v13, LX/0Nm;->e:Z

    if-eqz v2, :cond_d

    .line 51332
    iget-object v2, v4, LX/0Nk;->g:Ljava/lang/String;

    iget-object v4, v13, LX/0Nm;->f:Ljava/lang/String;

    invoke-static {v2, v4}, LX/0At;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 51333
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Ni;->x:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 51334
    iget-object v3, v13, LX/0Nm;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, LX/0Ni;->r:I

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3, v4}, LX/0Ni;->a(LX/0Ni;Landroid/net/Uri;Ljava/lang/String;I)LX/0Ne;

    move-result-object v2

    move-object/from16 v0, p4

    iput-object v2, v0, LX/0LW;->b:LX/0LP;

    goto/16 :goto_1

    .line 51335
    :cond_b
    iget-object v4, v13, LX/0Nm;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Ni;->z:Ljava/lang/String;

    invoke-static {v4, v5}, LX/08x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 51336
    iget-object v4, v13, LX/0Nm;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Ni;->y:[B

    move-object/from16 v0, p0

    invoke-static {v0, v2, v4, v5}, LX/0Ni;->a(LX/0Ni;Landroid/net/Uri;Ljava/lang/String;[B)V

    .line 51337
    :cond_c
    :goto_5
    new-instance v2, LX/0OA;

    iget-wide v4, v13, LX/0Nm;->h:J

    iget-wide v6, v13, LX/0Nm;->i:J

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, LX/0OA;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 51338
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0Ni;->u:Z

    if-eqz v4, :cond_10

    .line 51339
    if-nez p1, :cond_e

    .line 51340
    const-wide/16 v6, 0x0

    .line 51341
    :goto_6
    iget-wide v4, v13, LX/0Nm;->b:D

    const-wide v10, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v4, v10

    double-to-long v4, v4

    add-long v16, v6, v4

    .line 51342
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Ni;->n:[LX/0Nv;

    move-object/from16 v0, p0

    iget v5, v0, LX/0Ni;->r:I

    aget-object v4, v4, v5

    iget-object v5, v4, LX/0Nv;->b:LX/0AR;

    .line 51343
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    .line 51344
    const-string v4, ".aac"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 51345
    new-instance v8, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;

    invoke-direct {v8, v6, v7}, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;-><init>(J)V

    .line 51346
    new-instance v3, LX/0Nj;

    const/4 v4, 0x0

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-direct/range {v3 .. v11}, LX/0Nj;-><init>(ILX/0AR;JLX/0ME;ZII)V

    move-object/from16 v20, v3

    .line 51347
    :goto_7
    new-instance v9, LX/0Nu;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/0Ni;->b:LX/0G6;

    const/4 v12, 0x0

    iget v0, v13, LX/0Nm;->c:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0Ni;->y:[B

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0Ni;->A:[B

    move-object/from16 v22, v0

    move-object v11, v2

    move-object v13, v5

    move-wide v14, v6

    invoke-direct/range {v9 .. v22}, LX/0Nu;-><init>(LX/0G6;LX/0OA;ILX/0AR;JJIILX/0Nj;[B[B)V

    move-object/from16 v0, p4

    iput-object v9, v0, LX/0LW;->b:LX/0LP;

    goto/16 :goto_1

    .line 51348
    :cond_d
    invoke-static/range {p0 .. p0}, LX/0Ni;->k(LX/0Ni;)V

    goto :goto_5

    .line 51349
    :cond_e
    if-eqz v9, :cond_f

    .line 51350
    move-object/from16 v0, p1

    iget-wide v6, v0, LX/0LQ;->h:J

    goto :goto_6

    .line 51351
    :cond_f
    move-object/from16 v0, p1

    iget-wide v6, v0, LX/0LQ;->i:J

    goto :goto_6

    .line 51352
    :cond_10
    iget-wide v6, v13, LX/0Nm;->d:J

    goto :goto_6

    .line 51353
    :cond_11
    const-string v4, ".mp3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 51354
    new-instance v8, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;

    invoke-direct {v8, v6, v7}, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;-><init>(J)V

    .line 51355
    new-instance v3, LX/0Nj;

    const/4 v4, 0x0

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-direct/range {v3 .. v11}, LX/0Nj;-><init>(ILX/0AR;JLX/0ME;ZII)V

    move-object/from16 v20, v3

    .line 51356
    goto :goto_7

    :cond_12
    const-string v4, ".webvtt"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_13

    const-string v4, ".vtt"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 51357
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0Ni;->g:LX/0Nt;

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0Ni;->a:Z

    iget v8, v13, LX/0Nm;->c:I

    invoke-virtual {v3, v4, v8, v6, v7}, LX/0Nt;->a(ZIJ)LX/0NJ;

    move-result-object v3

    .line 51358
    if-eqz v3, :cond_1

    .line 51359
    new-instance v8, LX/0Nw;

    invoke-direct {v8, v3}, LX/0Nw;-><init>(LX/0NJ;)V

    .line 51360
    new-instance v3, LX/0Nj;

    const/4 v4, 0x0

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-direct/range {v3 .. v11}, LX/0Nj;-><init>(ILX/0AR;JLX/0ME;ZII)V

    move-object/from16 v20, v3

    .line 51361
    goto :goto_7

    :cond_14
    if-eqz p1, :cond_15

    move-object/from16 v0, p1

    iget v3, v0, LX/0Nu;->a:I

    iget v4, v13, LX/0Nm;->c:I

    if-ne v3, v4, :cond_15

    move-object/from16 v0, p1

    iget-object v3, v0, LX/0LP;->d:LX/0AR;

    invoke-virtual {v5, v3}, LX/0AR;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    .line 51362
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0Ni;->g:LX/0Nt;

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0Ni;->a:Z

    iget v8, v13, LX/0Nm;->c:I

    invoke-virtual {v3, v4, v8, v6, v7}, LX/0Nt;->a(ZIJ)LX/0NJ;

    move-result-object v4

    .line 51363
    if-eqz v4, :cond_1

    .line 51364
    const/4 v3, 0x0

    .line 51365
    iget-object v8, v5, LX/0AR;->k:Ljava/lang/String;

    .line 51366
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_17

    .line 51367
    invoke-static {v8}, LX/0Al;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "audio/mp4a-latm"

    if-eq v10, v11, :cond_16

    .line 51368
    const/4 v3, 0x2

    .line 51369
    :cond_16
    invoke-static {v8}, LX/0Al;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v10, "video/avc"

    if-eq v8, v10, :cond_17

    .line 51370
    or-int/lit8 v3, v3, 0x4

    .line 51371
    :cond_17
    new-instance v8, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;

    invoke-direct {v8, v4, v3}, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;-><init>(LX/0NJ;I)V

    .line 51372
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0Ni;->l:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v4, v0, LX/0Ni;->m:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v11, v3

    check-cast v11, LX/0Nf;

    .line 51373
    new-instance v3, LX/0Nj;

    const/4 v4, 0x0

    iget v10, v11, LX/0Nf;->c:I

    iget v11, v11, LX/0Nf;->d:I

    invoke-direct/range {v3 .. v11}, LX/0Nj;-><init>(ILX/0AR;JLX/0ME;ZII)V

    move-object/from16 v20, v3

    .line 51374
    goto/16 :goto_7

    .line 51375
    :cond_18
    move-object/from16 v0, p1

    iget-object v3, v0, LX/0Nu;->k:LX/0Nj;

    move-object/from16 v20, v3

    goto/16 :goto_7

    :cond_19
    move/from16 v18, v3

    goto/16 :goto_3
.end method

.method public final a(LX/0Nv;)V
    .locals 2

    .prologue
    .line 51301
    iget-object v0, p0, LX/0Ni;->l:Ljava/util/ArrayList;

    new-instance v1, LX/0Nf;

    invoke-direct {v1, p1}, LX/0Nf;-><init>(LX/0Nv;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51302
    return-void
.end method

.method public final a(LX/0LP;Ljava/io/IOException;)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    const-wide/16 v8, 0x0

    const/4 v3, 0x0

    .line 51278
    invoke-virtual {p1}, LX/0LP;->e()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-nez v0, :cond_2

    instance-of v0, p1, LX/0Nu;

    if-nez v0, :cond_0

    instance-of v0, p1, LX/0Ng;

    if-nez v0, :cond_0

    instance-of v0, p1, LX/0Ne;

    if-eqz v0, :cond_2

    :cond_0
    instance-of v0, p2, LX/0OL;

    if-eqz v0, :cond_2

    .line 51279
    check-cast p2, LX/0OL;

    .line 51280
    iget v4, p2, LX/0OL;->responseCode:I

    .line 51281
    const/16 v0, 0x194

    if-eq v4, v0, :cond_1

    const/16 v0, 0x19a

    if-ne v4, v0, :cond_2

    .line 51282
    :cond_1
    instance-of v0, p1, LX/0Nu;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 51283
    check-cast v0, LX/0Nu;

    .line 51284
    iget-object v0, v0, LX/0LP;->d:LX/0AR;

    invoke-static {p0, v0}, LX/0Ni;->a(LX/0Ni;LX/0AR;)I

    move-result v0

    .line 51285
    :goto_0
    iget-object v1, p0, LX/0Ni;->q:[J

    aget-wide v6, v1, v0

    cmp-long v1, v6, v8

    if-eqz v1, :cond_5

    move v1, v2

    .line 51286
    :goto_1
    iget-object v5, p0, LX/0Ni;->q:[J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    aput-wide v6, v5, v0

    .line 51287
    if-eqz v1, :cond_6

    .line 51288
    const-string v0, "HlsChunkSource"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Already blacklisted variant ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, LX/0LP;->e:LX/0OA;

    iget-object v2, v2, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 51289
    :cond_2
    :goto_2
    return v3

    .line 51290
    :cond_3
    instance-of v0, p1, LX/0Ng;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 51291
    check-cast v0, LX/0Ng;

    .line 51292
    iget v0, v0, LX/0Ng;->a:I

    goto :goto_0

    :cond_4
    move-object v0, p1

    .line 51293
    check-cast v0, LX/0Ne;

    .line 51294
    iget v0, v0, LX/0Ne;->h:I

    goto :goto_0

    :cond_5
    move v1, v3

    .line 51295
    goto :goto_1

    .line 51296
    :cond_6
    invoke-static {p0}, LX/0Ni;->l(LX/0Ni;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 51297
    const-string v0, "HlsChunkSource"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Blacklisted variant ("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p1, LX/0LP;->e:LX/0OA;

    iget-object v3, v3, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 51298
    goto :goto_2

    .line 51299
    :cond_7
    const-string v1, "HlsChunkSource"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Final variant not blacklisted ("

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "): "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, LX/0LP;->e:LX/0OA;

    iget-object v4, v4, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 51300
    iget-object v1, p0, LX/0Ni;->q:[J

    aput-wide v8, v1, v0

    goto :goto_2
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 51270
    iput p1, p0, LX/0Ni;->m:I

    .line 51271
    iget-object v0, p0, LX/0Ni;->l:Ljava/util/ArrayList;

    iget v1, p0, LX/0Ni;->m:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nf;

    .line 51272
    iget v1, v0, LX/0Nf;->b:I

    iput v1, p0, LX/0Ni;->r:I

    .line 51273
    iget-object v0, v0, LX/0Nf;->a:[LX/0Nv;

    iput-object v0, p0, LX/0Ni;->n:[LX/0Nv;

    .line 51274
    iget-object v0, p0, LX/0Ni;->n:[LX/0Nv;

    array-length v0, v0

    new-array v0, v0, [LX/0Nn;

    iput-object v0, p0, LX/0Ni;->o:[LX/0Nn;

    .line 51275
    iget-object v0, p0, LX/0Ni;->n:[LX/0Nv;

    array-length v0, v0

    new-array v0, v0, [J

    iput-object v0, p0, LX/0Ni;->p:[J

    .line 51276
    iget-object v0, p0, LX/0Ni;->n:[LX/0Nv;

    array-length v0, v0

    new-array v0, v0, [J

    iput-object v0, p0, LX/0Ni;->q:[J

    .line 51277
    return-void
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 51269
    iget-wide v0, p0, LX/0Ni;->v:J

    return-wide v0
.end method
