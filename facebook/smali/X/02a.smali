.class public final LX/02a;
.super LX/02b;
.source ""


# instance fields
.field private final mDexes:[LX/02Z;


# direct methods
.method public constructor <init>([LX/02Z;)V
    .locals 1

    .prologue
    .line 8884
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, LX/02b;-><init>(I[LX/02Z;)V

    .line 8885
    iput-object p1, p0, LX/02a;->mDexes:[LX/02Z;

    .line 8886
    return-void
.end method

.method private findDexToOptimize(LX/02U;J)LX/08U;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 8868
    :goto_0
    iget-object v2, p0, LX/02a;->mDexes:[LX/02Z;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 8869
    const-wide/16 v2, 0x10

    shl-long/2addr v2, v0

    and-long/2addr v2, p2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 8870
    iget-object v2, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x0

    aget-object v2, v2, v3

    .line 8871
    new-instance v3, Ljava/io/File;

    iget-object v4, p1, LX/02U;->root:Ljava/io/File;

    invoke-direct {v3, v4, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8872
    invoke-static {v3}, LX/02V;->open(Ljava/io/File;)LX/02V;

    move-result-object v2

    .line 8873
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v2, v3}, LX/02V;->tryAcquire(I)LX/02W;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 8874
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/02V;->close()V

    .line 8875
    :cond_0
    if-eqz v3, :cond_4

    .line 8876
    :try_start_1
    new-instance v1, LX/08U;

    invoke-direct {v1, v0, v3}, LX/08U;-><init>(ILX/02W;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v1

    .line 8877
    :goto_1
    return-object v0

    .line 8878
    :catch_0
    move-exception v1

    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 8879
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_3
    invoke-virtual {v2}, LX/02V;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_1
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, LX/02V;->close()V

    goto :goto_2

    .line 8880
    :catchall_1
    move-exception v0

    if-eqz v3, :cond_3

    .line 8881
    invoke-virtual {v3}, LX/02W;->close()V

    :cond_3
    throw v0

    .line 8882
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 8883
    goto :goto_1
.end method

.method private isFileCorruptionException(LX/0FK;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 8862
    iget v2, p1, LX/0FK;->status:I

    if-eq v2, v1, :cond_1

    .line 8863
    :cond_0
    :goto_0
    return v0

    .line 8864
    :cond_1
    iget-object v2, p1, LX/0FK;->errout:Ljava/lang/String;

    .line 8865
    if-eqz v2, :cond_0

    .line 8866
    const-string v3, "E/dalvikvm: ERROR: bad checksum"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 8867
    if-eqz v3, :cond_2

    if-lez v3, :cond_0

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private static makeFakeCacheSymlink(LX/02a;Ljava/io/File;Ljava/io/File;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 8766
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 8767
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "expected file to exist: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8768
    :cond_0
    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 8769
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "expected file to exist: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8770
    :cond_1
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".jar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "classes.dex"

    .line 8771
    :goto_0
    invoke-static {p1, p2, v0}, LX/02Q;->dexOptGenerateCacheFileName(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 8772
    const-string v1, "[opt] symlink %s -> %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8773
    invoke-static {p3, v0}, LX/02Q;->symlink(Ljava/io/File;Ljava/io/File;)V

    .line 8774
    return-void

    .line 8775
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static openDexInsideOdex(LX/02a;Ljava/io/FileInputStream;)LX/08V;
    .locals 8

    .prologue
    const/16 v6, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8847
    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 8848
    invoke-virtual {p1}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    .line 8849
    const-wide/16 v4, 0x8

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 8850
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 8851
    invoke-virtual {v3, v2}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v4

    if-eq v4, v6, :cond_0

    .line 8852
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid odex file"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8853
    :cond_0
    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 8854
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    .line 8855
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    .line 8856
    const-string v2, "dexOffset:%s dexLength:%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-static {v2, v6}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8857
    if-gtz v4, :cond_1

    move v2, v0

    :goto_0
    if-gtz v5, :cond_2

    :goto_1
    or-int/2addr v0, v2

    if-eqz v0, :cond_3

    .line 8858
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid odex file"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v2, v1

    .line 8859
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 8860
    :cond_3
    int-to-long v0, v4

    invoke-virtual {v3, v0, v1}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 8861
    new-instance v0, LX/08V;

    invoke-direct {v0, p1, v5}, LX/08V;-><init>(Ljava/io/InputStream;I)V

    return-object v0
.end method

.method private optimize1(LX/02U;Ljava/io/File;LX/08Z;LX/08T;LX/082;LX/08U;[Ljava/io/File;[B)V
    .locals 16

    .prologue
    .line 8887
    invoke-virtual/range {p4 .. p4}, LX/08T;->startOptimizing()V

    .line 8888
    move-object/from16 v0, p0

    iget-object v2, v0, LX/02c;->expectedFiles:[Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/08U;->dexNr:I

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x0

    aget-object v5, v2, v3

    .line 8889
    move-object/from16 v0, p0

    iget-object v2, v0, LX/02c;->expectedFiles:[Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/08U;->dexNr:I

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    .line 8890
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v12, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8891
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/02U;->root:Ljava/io/File;

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8892
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/02U;->root:Ljava/io/File;

    invoke-direct {v13, v4, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8893
    const-string v2, "[opt] started optimizing %s -> %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v4, v6

    const/4 v3, 0x1

    aput-object v13, v4, v3

    invoke-static {v2, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8894
    move-object/from16 v0, p7

    array-length v2, v0

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/02c;->expectedFiles:[Ljava/lang/String;

    array-length v3, v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    add-int/lit8 v6, v2, -0x1

    .line 8895
    new-array v8, v6, [Ljava/lang/String;

    .line 8896
    const/4 v3, 0x0

    .line 8897
    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p7

    array-length v4, v0

    if-ge v2, v4, :cond_0

    .line 8898
    add-int/lit8 v4, v3, 0x1

    aget-object v7, p7, v2

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v3

    .line 8899
    add-int/lit8 v2, v2, 0x2

    move v3, v4

    goto :goto_0

    .line 8900
    :cond_0
    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/02c;->expectedFiles:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_2

    .line 8901
    div-int/lit8 v4, v2, 0x2

    move-object/from16 v0, p6

    iget v7, v0, LX/08U;->dexNr:I

    if-eq v4, v7, :cond_1

    .line 8902
    add-int/lit8 v4, v3, 0x1

    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p1

    iget-object v9, v0, LX/02U;->root:Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/02c;->expectedFiles:[Ljava/lang/String;

    aget-object v10, v10, v2

    invoke-direct {v7, v9, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v3

    move v3, v4

    .line 8903
    :cond_1
    add-int/lit8 v2, v2, 0x2

    goto :goto_1

    .line 8904
    :cond_2
    if-ne v3, v6, :cond_5

    const/4 v2, 0x1

    :goto_2
    const-string v3, "accounted for all dex files"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, LX/02P;->assertThat(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 8905
    new-instance v14, Ljava/io/FileInputStream;

    invoke-direct {v14, v13}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 v11, 0x0

    .line 8906
    :try_start_0
    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/02a;->openDexInsideOdex(LX/02a;Ljava/io/FileInputStream;)LX/08V;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    move-result-object v3

    const/4 v10, 0x0

    .line 8907
    :try_start_1
    invoke-virtual {v3}, LX/08V;->available()I

    move-result v4

    .line 8908
    const/4 v2, 0x1

    if-gt v4, v2, :cond_3

    .line 8909
    const/4 v4, -0x1

    .line 8910
    :cond_3
    const-string v2, "[opt] size hint for %s: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v13, v6, v7

    const/4 v7, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-static {v2, v6}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8911
    new-instance v6, Ljava/io/RandomAccessFile;

    const-string v2, "rw"

    invoke-direct {v6, v12, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    const/4 v9, 0x0

    .line 8912
    :try_start_2
    const-string v7, "xdex"

    move-object/from16 v2, p5

    invoke-virtual/range {v2 .. v8}, LX/082;->run(Ljava/io/InputStream;ILjava/lang/String;Ljava/io/RandomAccessFile;Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 8913
    :try_start_3
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-static {v2}, LX/08B;->fileno(Ljava/io/FileDescriptor;)I

    move-result v2

    .line 8914
    move-object/from16 v0, p8

    invoke-static {v2, v0}, Lcom/facebook/common/dextricks/DalvikInternals;->replaceOdexDepBlock(I[B)V

    .line 8915
    move-object/from16 v0, p3

    iget-object v4, v0, LX/08Z;->config:LX/08F;

    iget-object v4, v4, LX/08F;->prio:LX/08D;

    iget v4, v4, LX/08D;->ioPriority:I

    invoke-static {v2, v4}, Lcom/facebook/common/dextricks/DalvikInternals;->fsync(II)V

    .line 8916
    invoke-static {v2}, LX/02Q;->tryDiscardPageCache(I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    .line 8917
    :try_start_4
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 8918
    if-eqz v3, :cond_4

    :try_start_5
    invoke-virtual {v3}, LX/08V;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 8919
    :cond_4
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V

    .line 8920
    invoke-virtual/range {p4 .. p4}, LX/08T;->startCommitting()J

    move-result-wide v2

    const-wide/16 v4, 0x10

    move-object/from16 v0, p6

    iget v6, v0, LX/08U;->dexNr:I

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    .line 8921
    const-string v4, "[opt] started commit"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8922
    invoke-static {v12, v13}, LX/02Q;->renameOrThrow(Ljava/io/File;Ljava/io/File;)V

    .line 8923
    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, LX/08T;->finishCommit(J)V

    .line 8924
    const-string v2, "[opt] finished commit"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8925
    return-void

    .line 8926
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 8927
    :catchall_0
    move-exception v2

    .line 8928
    :try_start_6
    invoke-static {v12}, LX/02Q;->deleteRecursive(Ljava/io/File;)V

    throw v2
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 8929
    :catch_0
    move-exception v2

    :try_start_7
    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 8930
    :catchall_1
    move-exception v4

    move-object v15, v4

    move-object v4, v2

    move-object v2, v15

    :goto_3
    if-eqz v4, :cond_7

    :try_start_8
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    :goto_4
    :try_start_9
    throw v2
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 8931
    :catch_1
    move-exception v2

    :try_start_a
    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 8932
    :catchall_2
    move-exception v4

    move-object v15, v4

    move-object v4, v2

    move-object v2, v15

    :goto_5
    if-eqz v3, :cond_6

    if-eqz v4, :cond_8

    :try_start_b
    invoke-virtual {v3}, LX/08V;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    :cond_6
    :goto_6
    :try_start_c
    throw v2
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    .line 8933
    :catch_2
    move-exception v2

    :try_start_d
    throw v2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .line 8934
    :catchall_3
    move-exception v3

    move-object v15, v3

    move-object v3, v2

    move-object v2, v15

    :goto_7
    if-eqz v3, :cond_9

    :try_start_e
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_5

    :goto_8
    throw v2

    .line 8935
    :catch_3
    move-exception v5

    :try_start_f
    invoke-static {v4, v5}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 8936
    :catchall_4
    move-exception v2

    move-object v4, v10

    goto :goto_5

    .line 8937
    :cond_7
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    goto :goto_4

    .line 8938
    :catch_4
    move-exception v3

    :try_start_10
    invoke-static {v4, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 8939
    :catchall_5
    move-exception v2

    move-object v3, v11

    goto :goto_7

    .line 8940
    :cond_8
    invoke-virtual {v3}, LX/08V;->close()V
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    goto :goto_6

    .line 8941
    :catch_5
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_8

    :cond_9
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V

    goto :goto_8

    .line 8942
    :catchall_6
    move-exception v2

    move-object v4, v9

    goto :goto_3
.end method

.method private prepareTmpDirForXdex([Ljava/io/File;LX/02U;Ljava/io/File;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 8824
    invoke-static {}, LX/02Q;->findSystemDalvikCache()Ljava/io/File;

    move-result-object v3

    .line 8825
    new-instance v4, Ljava/io/File;

    const-string v0, "dalvik-cache"

    invoke-direct {v4, p3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8826
    invoke-static {v4}, LX/02Q;->mkdirOrThrow(Ljava/io/File;)V

    .line 8827
    new-instance v0, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v1, 0x3a

    invoke-direct {v0, v1}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 8828
    const-string v1, "BOOTCLASSPATH"

    invoke-static {v1}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 8829
    invoke-virtual {v0}, Landroid/text/TextUtils$SimpleStringSplitter;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 8830
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 8831
    const-string v1, ".jar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "classes.dex"

    .line 8832
    :goto_1
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 8833
    invoke-static {v4, v6, v1}, LX/02Q;->dexOptGenerateCacheFileName(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 8834
    invoke-static {v3, v6, v1}, LX/02Q;->dexOptGenerateCacheFileName(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 8835
    const-string v6, "[opt] symlink %s -> %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v0, v7, v2

    const/4 v8, 0x1

    aput-object v1, v7, v8

    invoke-static {v6, v7}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8836
    invoke-static {v1, v0}, LX/02Q;->symlink(Ljava/io/File;Ljava/io/File;)V

    goto :goto_0

    .line 8837
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 8838
    :goto_2
    array-length v1, p1

    if-ge v0, v1, :cond_3

    .line 8839
    add-int/lit8 v1, v0, 0x0

    aget-object v1, p1, v1

    add-int/lit8 v3, v0, 0x1

    aget-object v3, p1, v3

    invoke-static {p0, v4, v1, v3}, LX/02a;->makeFakeCacheSymlink(LX/02a;Ljava/io/File;Ljava/io/File;Ljava/io/File;)V

    .line 8840
    add-int/lit8 v0, v0, 0x2

    goto :goto_2

    .line 8841
    :cond_3
    :goto_3
    iget-object v0, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    array-length v0, v0

    if-ge v2, v0, :cond_4

    .line 8842
    new-instance v0, Ljava/io/File;

    iget-object v1, p2, LX/02U;->root:Ljava/io/File;

    iget-object v3, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    add-int/lit8 v5, v2, 0x0

    aget-object v3, v3, v5

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8843
    new-instance v1, Ljava/io/File;

    iget-object v3, p2, LX/02U;->root:Ljava/io/File;

    iget-object v5, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    add-int/lit8 v6, v2, 0x1

    aget-object v5, v5, v6

    invoke-direct {v1, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 8844
    invoke-static {p0, v4, v0, v1}, LX/02a;->makeFakeCacheSymlink(LX/02a;Ljava/io/File;Ljava/io/File;Ljava/io/File;)V

    .line 8845
    add-int/lit8 v2, v2, 0x2

    goto :goto_3

    .line 8846
    :cond_4
    return-void
.end method


# virtual methods
.method public final getSchemeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8823
    const-string v0, "OdexSchemeXdex"

    return-object v0
.end method

.method public final needOptimization(J)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 8819
    iget-object v2, p0, LX/02a;->mDexes:[LX/02Z;

    array-length v2, v2

    shl-int v2, v0, v2

    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    .line 8820
    const/4 v4, 0x4

    shr-long v4, p1, v4

    .line 8821
    const-string v6, "expectedDexBits:0x%08x actualDexBits:0x%08x"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v0

    invoke-static {v6, v7}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8822
    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final optimize(LX/02U;LX/08Z;LX/0FQ;)V
    .locals 16
    .param p3    # LX/0FQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 8776
    const/4 v2, 0x0

    .line 8777
    invoke-virtual/range {p1 .. p1}, LX/02U;->getDependencyOdexFiles()[Ljava/io/File;

    move-result-object v9

    .line 8778
    invoke-virtual/range {p1 .. p1}, LX/02U;->getOdexCachePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/dextricks/DalvikInternals;->readOdexDepBlock(Ljava/lang/String;)[B

    move-result-object v10

    .line 8779
    const-string v3, "dexopt"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/02U;->makeTemporaryDirectory(Ljava/lang/String;)LX/087;

    move-result-object v14

    const/4 v12, 0x0

    .line 8780
    :try_start_0
    const-string v3, "[opt] opened tmpDir %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v14, LX/087;->directory:Ljava/io/File;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8781
    move-object/from16 v0, p2

    iget-object v3, v0, LX/08Z;->config:LX/08F;

    iget v3, v3, LX/08F;->flags:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    .line 8782
    :goto_0
    if-eqz v3, :cond_4

    .line 8783
    new-instance v7, LX/0Fh;

    iget-object v3, v14, LX/087;->directory:Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v7, v0, v3}, LX/0Fh;-><init>(LX/08Z;Ljava/io/File;)V

    .line 8784
    :goto_1
    const-string v3, "[opt] starting optimization pass; creating job"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8785
    new-instance v6, LX/08T;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object/from16 v0, p2

    invoke-direct {v6, v0}, LX/08T;-><init>(LX/08Z;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v11, 0x0

    .line 8786
    :try_start_1
    const-string v3, "[opt] opened job"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8787
    if-eqz p3, :cond_0

    .line 8788
    move-object/from16 v0, p0

    iget-object v3, v0, LX/02a;->mDexes:[LX/02Z;

    array-length v3, v3

    iget-wide v4, v6, LX/08T;->initialStatus:J

    const/4 v8, 0x4

    shr-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->bitCount(J)I

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4, v5}, LX/0FQ;->onProgress(IIZ)V

    .line 8789
    :cond_0
    if-nez v2, :cond_a

    .line 8790
    iget-object v2, v14, LX/087;->directory:Ljava/io/File;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v9, v1, v2}, LX/02a;->prepareTmpDirForXdex([Ljava/io/File;LX/02U;Ljava/io/File;)V

    .line 8791
    const/4 v2, 0x1

    move v13, v2

    .line 8792
    :goto_2
    iget-wide v2, v6, LX/08T;->initialStatus:J

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3}, LX/02a;->findDexToOptimize(LX/02U;J)LX/08U;

    move-result-object v8

    .line 8793
    const-string v2, "[opt] dto %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v8, v3, v4

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 8794
    if-eqz v8, :cond_1

    .line 8795
    :try_start_2
    iget-object v4, v14, LX/087;->directory:Ljava/io/File;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    invoke-direct/range {v2 .. v10}, LX/02a;->optimize1(LX/02U;Ljava/io/File;LX/08Z;LX/08T;LX/082;LX/08U;[Ljava/io/File;[B)V
    :try_end_2
    .catch LX/0FK; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 8796
    :try_start_3
    invoke-virtual {v8}, LX/08U;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 8797
    :cond_1
    :try_start_4
    invoke-virtual {v6}, LX/08T;->close()V

    .line 8798
    :goto_3
    if-nez v8, :cond_9

    .line 8799
    const-string v2, "[opt] optimization complete"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 8800
    if-eqz v14, :cond_2

    invoke-virtual {v14}, LX/087;->close()V

    .line 8801
    :cond_2
    return-void

    .line 8802
    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 8803
    :cond_4
    :try_start_5
    new-instance v7, LX/085;

    move-object/from16 v0, p2

    iget-object v3, v0, LX/08Z;->dexStoreConfig:LX/080;

    iget-object v4, v14, LX/087;->directory:Ljava/io/File;

    invoke-direct {v7, v3, v4}, LX/085;-><init>(LX/080;Ljava/io/File;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_1

    .line 8804
    :catch_0
    move-exception v2

    :try_start_6
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 8805
    :catchall_0
    move-exception v3

    move-object v15, v3

    move-object v3, v2

    move-object v2, v15

    :goto_4
    if-eqz v14, :cond_5

    if-eqz v3, :cond_8

    :try_start_7
    invoke-virtual {v14}, LX/087;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4

    :cond_5
    :goto_5
    throw v2

    .line 8806
    :catch_1
    move-exception v2

    .line 8807
    :try_start_8
    sget-boolean v3, LX/02Q;->isKernelPageCacheFlushIsBroken:Z

    if-nez v3, :cond_6

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/02a;->isFileCorruptionException(LX/0FK;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 8808
    const-string v2, "detected odex file corruption: trying again with kernel workaround"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8809
    const/4 v2, 0x1

    sput-boolean v2, LX/02Q;->isKernelPageCacheFlushIsBroken:Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 8810
    :try_start_9
    invoke-virtual {v8}, LX/08U;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 8811
    :try_start_a
    invoke-virtual {v6}, LX/08T;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_3

    .line 8812
    :catchall_1
    move-exception v2

    move-object v3, v12

    goto :goto_4

    .line 8813
    :cond_6
    :try_start_b
    throw v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 8814
    :catchall_2
    move-exception v2

    :try_start_c
    invoke-virtual {v8}, LX/08U;->close()V

    throw v2
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 8815
    :catch_2
    move-exception v2

    :try_start_d
    throw v2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .line 8816
    :catchall_3
    move-exception v3

    move-object v15, v3

    move-object v3, v2

    move-object v2, v15

    :goto_6
    if-eqz v3, :cond_7

    :try_start_e
    invoke-virtual {v6}, LX/08T;->close()V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :goto_7
    :try_start_f
    throw v2

    :catch_3
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_7

    :cond_7
    invoke-virtual {v6}, LX/08T;->close()V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_0
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto :goto_7

    .line 8817
    :catch_4
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_8
    invoke-virtual {v14}, LX/087;->close()V

    goto :goto_5

    .line 8818
    :catchall_4
    move-exception v2

    move-object v3, v11

    goto :goto_6

    :cond_9
    move v2, v13

    goto/16 :goto_1

    :cond_a
    move v13, v2

    goto/16 :goto_2
.end method
