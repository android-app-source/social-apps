.class public LX/03q;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)V
    .locals 2

    .prologue
    .line 10804
    const/16 v0, 0x8

    const/16 v1, 0x1f

    invoke-static {v0, v1, p0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    .line 10805
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 10806
    invoke-static {}, Landroid/os/Trace;->endSection()V

    .line 10807
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 10808
    const/16 v0, 0x8

    const/16 v1, 0x1e

    invoke-static {v0, v1, p1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    .line 10809
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 10810
    invoke-static {p0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    .line 10811
    :cond_0
    return-void
.end method
