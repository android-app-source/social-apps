.class public final LX/0Oe;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final h:[Ljava/lang/String;

.field private static final i:[I

.field private static final j:[I

.field private static final k:[I

.field private static final l:[I

.field private static final m:[I

.field private static final n:[I


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/16 v3, 0xe

    .line 53503
    new-array v0, v4, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "audio/mpeg-L1"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "audio/mpeg-L2"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "audio/mpeg"

    aput-object v2, v0, v1

    sput-object v0, LX/0Oe;->h:[Ljava/lang/String;

    .line 53504
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/0Oe;->i:[I

    .line 53505
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, LX/0Oe;->j:[I

    .line 53506
    new-array v0, v3, [I

    fill-array-data v0, :array_2

    sput-object v0, LX/0Oe;->k:[I

    .line 53507
    new-array v0, v3, [I

    fill-array-data v0, :array_3

    sput-object v0, LX/0Oe;->l:[I

    .line 53508
    new-array v0, v3, [I

    fill-array-data v0, :array_4

    sput-object v0, LX/0Oe;->m:[I

    .line 53509
    new-array v0, v3, [I

    fill-array-data v0, :array_5

    sput-object v0, LX/0Oe;->n:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xac44
        0xbb80
        0x7d00
    .end array-data

    .line 53510
    :array_1
    .array-data 4
        0x20
        0x40
        0x60
        0x80
        0xa0
        0xc0
        0xe0
        0x100
        0x120
        0x140
        0x160
        0x180
        0x1a0
        0x1c0
    .end array-data

    .line 53511
    :array_2
    .array-data 4
        0x20
        0x30
        0x38
        0x40
        0x50
        0x60
        0x70
        0x80
        0x90
        0xa0
        0xb0
        0xc0
        0xe0
        0x100
    .end array-data

    .line 53512
    :array_3
    .array-data 4
        0x20
        0x30
        0x38
        0x40
        0x50
        0x60
        0x70
        0x80
        0xa0
        0xc0
        0xe0
        0x100
        0x140
        0x180
    .end array-data

    .line 53513
    :array_4
    .array-data 4
        0x20
        0x28
        0x30
        0x38
        0x40
        0x50
        0x60
        0x70
        0x80
        0xa0
        0xc0
        0xe0
        0x100
        0x140
    .end array-data

    .line 53514
    :array_5
    .array-data 4
        0x8
        0x10
        0x18
        0x20
        0x28
        0x30
        0x38
        0x40
        0x50
        0x60
        0x70
        0x80
        0x90
        0xa0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/high16 v2, -0x200000

    const/4 v6, 0x3

    const/4 v0, -0x1

    .line 53516
    and-int v1, p0, v2

    if-eq v1, v2, :cond_1

    .line 53517
    :cond_0
    :goto_0
    return v0

    .line 53518
    :cond_1
    ushr-int/lit8 v1, p0, 0x13

    and-int/lit8 v3, v1, 0x3

    .line 53519
    if-eq v3, v7, :cond_0

    .line 53520
    ushr-int/lit8 v1, p0, 0x11

    and-int/lit8 v4, v1, 0x3

    .line 53521
    if-eqz v4, :cond_0

    .line 53522
    ushr-int/lit8 v1, p0, 0xc

    and-int/lit8 v1, v1, 0xf

    .line 53523
    if-eqz v1, :cond_0

    const/16 v2, 0xf

    if-eq v1, v2, :cond_0

    .line 53524
    ushr-int/lit8 v2, p0, 0xa

    and-int/lit8 v2, v2, 0x3

    .line 53525
    if-eq v2, v6, :cond_0

    .line 53526
    sget-object v0, LX/0Oe;->i:[I

    aget v0, v0, v2

    .line 53527
    if-ne v3, v8, :cond_2

    .line 53528
    div-int/lit8 v0, v0, 0x2

    move v2, v0

    .line 53529
    :goto_1
    ushr-int/lit8 v0, p0, 0x9

    and-int/lit8 v5, v0, 0x1

    .line 53530
    if-ne v4, v6, :cond_4

    .line 53531
    if-ne v3, v6, :cond_3

    sget-object v0, LX/0Oe;->j:[I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    .line 53532
    :goto_2
    mul-int/lit16 v0, v0, 0x2ee0

    div-int/2addr v0, v2

    add-int/2addr v0, v5

    mul-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 53533
    :cond_2
    if-nez v3, :cond_9

    .line 53534
    div-int/lit8 v0, v0, 0x4

    move v2, v0

    goto :goto_1

    .line 53535
    :cond_3
    sget-object v0, LX/0Oe;->k:[I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_2

    .line 53536
    :cond_4
    if-ne v3, v6, :cond_6

    .line 53537
    if-ne v4, v8, :cond_5

    sget-object v0, LX/0Oe;->l:[I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    :goto_3
    move v1, v0

    .line 53538
    :goto_4
    if-ne v3, v6, :cond_7

    .line 53539
    const v0, 0x23280

    mul-int/2addr v0, v1

    div-int/2addr v0, v2

    add-int/2addr v0, v5

    goto :goto_0

    .line 53540
    :cond_5
    sget-object v0, LX/0Oe;->m:[I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_3

    .line 53541
    :cond_6
    sget-object v0, LX/0Oe;->n:[I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    move v1, v0

    goto :goto_4

    .line 53542
    :cond_7
    if-ne v4, v7, :cond_8

    const v0, 0x11940

    :goto_5
    mul-int/2addr v0, v1

    div-int/2addr v0, v2

    add-int/2addr v0, v5

    goto :goto_0

    :cond_8
    const v0, 0x23280

    goto :goto_5

    :cond_9
    move v2, v0

    goto :goto_1
.end method

.method public static a(ILX/0Oe;)Z
    .locals 11

    .prologue
    const/high16 v2, -0x200000

    const/4 v5, 0x2

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 53543
    and-int v1, p0, v2

    if-eq v1, v2, :cond_0

    move v8, v0

    .line 53544
    :goto_0
    return v8

    .line 53545
    :cond_0
    ushr-int/lit8 v1, p0, 0x13

    and-int/lit8 v1, v1, 0x3

    .line 53546
    if-ne v1, v8, :cond_1

    move v8, v0

    .line 53547
    goto :goto_0

    .line 53548
    :cond_1
    ushr-int/lit8 v2, p0, 0x11

    and-int/lit8 v6, v2, 0x3

    .line 53549
    if-nez v6, :cond_2

    move v8, v0

    .line 53550
    goto :goto_0

    .line 53551
    :cond_2
    ushr-int/lit8 v2, p0, 0xc

    and-int/lit8 v2, v2, 0xf

    .line 53552
    if-eqz v2, :cond_3

    const/16 v3, 0xf

    if-ne v2, v3, :cond_4

    :cond_3
    move v8, v0

    .line 53553
    goto :goto_0

    .line 53554
    :cond_4
    ushr-int/lit8 v3, p0, 0xa

    and-int/lit8 v3, v3, 0x3

    .line 53555
    if-ne v3, v9, :cond_5

    move v8, v0

    .line 53556
    goto :goto_0

    .line 53557
    :cond_5
    sget-object v0, LX/0Oe;->i:[I

    aget v4, v0, v3

    .line 53558
    if-ne v1, v5, :cond_8

    .line 53559
    div-int/lit8 v4, v4, 0x2

    .line 53560
    :cond_6
    :goto_1
    ushr-int/lit8 v0, p0, 0x9

    and-int/lit8 v7, v0, 0x1

    .line 53561
    if-ne v6, v9, :cond_a

    .line 53562
    if-ne v1, v9, :cond_9

    sget-object v0, LX/0Oe;->j:[I

    add-int/lit8 v2, v2, -0x1

    aget v0, v0, v2

    .line 53563
    :goto_2
    mul-int/lit16 v2, v0, 0x2ee0

    div-int/2addr v2, v4

    add-int/2addr v2, v7

    mul-int/lit8 v3, v2, 0x4

    .line 53564
    const/16 v7, 0x180

    .line 53565
    :goto_3
    sget-object v2, LX/0Oe;->h:[Ljava/lang/String;

    rsub-int/lit8 v6, v6, 0x3

    aget-object v2, v2, v6

    .line 53566
    shr-int/lit8 v6, p0, 0x6

    and-int/lit8 v6, v6, 0x3

    if-ne v6, v9, :cond_7

    move v5, v8

    .line 53567
    :cond_7
    mul-int/lit16 v6, v0, 0x3e8

    move-object v0, p1

    .line 53568
    iput v1, v0, LX/0Oe;->a:I

    .line 53569
    iput-object v2, v0, LX/0Oe;->b:Ljava/lang/String;

    .line 53570
    iput v3, v0, LX/0Oe;->c:I

    .line 53571
    iput v4, v0, LX/0Oe;->d:I

    .line 53572
    iput v5, v0, LX/0Oe;->e:I

    .line 53573
    iput v6, v0, LX/0Oe;->f:I

    .line 53574
    iput v7, v0, LX/0Oe;->g:I

    .line 53575
    goto :goto_0

    .line 53576
    :cond_8
    if-nez v1, :cond_6

    .line 53577
    div-int/lit8 v4, v4, 0x4

    goto :goto_1

    .line 53578
    :cond_9
    sget-object v0, LX/0Oe;->k:[I

    add-int/lit8 v2, v2, -0x1

    aget v0, v0, v2

    goto :goto_2

    .line 53579
    :cond_a
    if-ne v1, v9, :cond_d

    .line 53580
    if-ne v6, v5, :cond_c

    sget-object v0, LX/0Oe;->l:[I

    add-int/lit8 v2, v2, -0x1

    aget v0, v0, v2

    .line 53581
    :goto_4
    const/16 v2, 0x480

    move v10, v2

    move v2, v0

    move v0, v10

    .line 53582
    :cond_b
    const v3, 0x23280

    move v10, v3

    move v3, v2

    move v2, v0

    move v0, v10

    :goto_5
    mul-int/2addr v0, v3

    div-int/2addr v0, v4

    add-int/2addr v0, v7

    move v7, v2

    move v10, v0

    move v0, v3

    move v3, v10

    goto :goto_3

    .line 53583
    :cond_c
    sget-object v0, LX/0Oe;->m:[I

    add-int/lit8 v2, v2, -0x1

    aget v0, v0, v2

    goto :goto_4

    .line 53584
    :cond_d
    sget-object v0, LX/0Oe;->n:[I

    add-int/lit8 v2, v2, -0x1

    aget v2, v0, v2

    .line 53585
    if-ne v6, v8, :cond_e

    const/16 v0, 0x240

    .line 53586
    :goto_6
    if-ne v6, v8, :cond_b

    const v3, 0x11940

    move v10, v3

    move v3, v2

    move v2, v0

    move v0, v10

    goto :goto_5

    .line 53587
    :cond_e
    const/16 v0, 0x480

    goto :goto_6
.end method
