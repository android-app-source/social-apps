.class public final LX/0FX;
.super Lcom/facebook/common/dextricks/MultiDexClassLoader;
.source ""


# instance fields
.field private mDexFiles:[Ldalvik/system/DexFile;

.field private final mPutativeLoader:Ljava/lang/ClassLoader;


# direct methods
.method public constructor <init>(Ljava/lang/ClassLoader;Ljava/lang/ClassLoader;)V
    .locals 0

    .prologue
    .line 33362
    invoke-direct {p0, p1}, Lcom/facebook/common/dextricks/MultiDexClassLoader;-><init>(Ljava/lang/ClassLoader;)V

    .line 33363
    iput-object p2, p0, LX/0FX;->mPutativeLoader:Ljava/lang/ClassLoader;

    .line 33364
    return-void
.end method


# virtual methods
.method public final doConfigure(LX/02f;)V
    .locals 2

    .prologue
    .line 33359
    iget-object v0, p1, LX/02f;->mDexFiles:Ljava/util/ArrayList;

    iget-object v1, p1, LX/02f;->mDexFiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ldalvik/system/DexFile;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldalvik/system/DexFile;

    iput-object v0, p0, LX/0FX;->mDexFiles:[Ldalvik/system/DexFile;

    .line 33360
    invoke-virtual {p0, p1}, Lcom/facebook/common/dextricks/MultiDexClassLoader;->setMadvAndMprotForOatFile(LX/02f;)V

    .line 33361
    return-void
.end method

.method public final doGetConfiguredDexFiles()[Ldalvik/system/DexFile;
    .locals 1

    .prologue
    .line 33349
    iget-object v0, p0, LX/0FX;->mDexFiles:[Ldalvik/system/DexFile;

    return-object v0
.end method

.method public final findClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 33352
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/0FX;->mDexFiles:[Ldalvik/system/DexFile;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 33353
    iget-object v1, p0, LX/0FX;->mDexFiles:[Ldalvik/system/DexFile;

    aget-object v1, v1, v0

    iget-object v2, p0, LX/0FX;->mPutativeLoader:Ljava/lang/ClassLoader;

    invoke-virtual {v1, p1, v2}, Ldalvik/system/DexFile;->loadClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1

    .line 33354
    if-eqz v1, :cond_0

    .line 33355
    invoke-static {v1}, Lcom/facebook/loom/logger/api/LoomLogger;->a(Ljava/lang/Class;)I

    .line 33356
    return-object v1

    .line 33357
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33358
    :cond_1
    sget-object v0, Lcom/facebook/common/dextricks/MultiDexClassLoader;->sPrefabException:Ljava/lang/ClassNotFoundException;

    throw v0
.end method

.method public final onColdstartDone()V
    .locals 0

    .prologue
    .line 33351
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33350
    const-string v0, "MultiDexClassLoaderBoring"

    return-object v0
.end method
