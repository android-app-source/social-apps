.class public final LX/0AP;
.super LX/07u;
.source ""


# instance fields
.field public final synthetic a:LX/029;

.field public final b:[LX/0Az;

.field public final c:Ljava/util/zip/ZipFile;

.field private final d:I

.field public final e:Ljava/util/zip/ZipEntry;


# direct methods
.method public constructor <init>(LX/029;LX/025;)V
    .locals 19

    .prologue
    .line 23813
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LX/0AP;->a:LX/029;

    invoke-direct/range {p0 .. p0}, LX/07u;-><init>()V

    .line 23814
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 23815
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 23816
    invoke-static {}, LX/01z;->a()[Ljava/lang/String;

    move-result-object v8

    .line 23817
    new-instance v9, Ljava/util/zip/ZipFile;

    invoke-static/range {p1 .. p1}, LX/029;->a(LX/029;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 23818
    :try_start_0
    invoke-static/range {p1 .. p1}, LX/029;->b(LX/029;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v2

    .line 23819
    invoke-static/range {p1 .. p1}, LX/029;->c(LX/029;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v10

    .line 23820
    if-eqz v2, :cond_0

    if-nez v10, :cond_1

    .line 23821
    :cond_0
    const/4 v2, 0x0

    new-array v2, v2, [LX/0Az;

    move-object/from16 v0, p0

    iput-object v2, v0, LX/0AP;->b:[LX/0Az;

    .line 23822
    move-object/from16 v0, p0

    iput-object v9, v0, LX/0AP;->c:Ljava/util/zip/ZipFile;

    .line 23823
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, LX/0AP;->e:Ljava/util/zip/ZipEntry;

    .line 23824
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/0AP;->d:I

    .line 23825
    :goto_0
    return-void

    .line 23826
    :cond_1
    invoke-virtual {v9, v2}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v11

    const/4 v3, 0x0

    .line 23827
    :try_start_1
    new-instance v12, Ljava/util/LinkedHashSet;

    invoke-direct {v12}, Ljava/util/LinkedHashSet;-><init>()V

    .line 23828
    new-instance v13, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v13, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 23829
    new-instance v14, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v2, 0x20

    invoke-direct {v14, v2}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 23830
    :cond_2
    :goto_1
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_9

    .line 23831
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    .line 23832
    const/4 v5, 0x0

    .line 23833
    const/4 v4, 0x0

    .line 23834
    const/4 v2, 0x0

    .line 23835
    invoke-virtual {v14, v15}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 23836
    invoke-virtual {v14}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_3

    .line 23837
    invoke-virtual {v14}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v5

    .line 23838
    invoke-virtual {v14}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_3

    .line 23839
    invoke-virtual {v14}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v4

    .line 23840
    invoke-virtual {v14}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_3

    .line 23841
    invoke-virtual {v14}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v2

    .line 23842
    :cond_3
    if-eqz v5, :cond_4

    if-eqz v4, :cond_4

    if-nez v2, :cond_6

    .line 23843
    :cond_4
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "illegal line in xzs metadata: ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 23844
    :catch_0
    move-exception v2

    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 23845
    :catchall_0
    move-exception v3

    move-object/from16 v18, v3

    move-object v3, v2

    move-object/from16 v2, v18

    :goto_2
    if-eqz v11, :cond_5

    if-eqz v3, :cond_b

    :try_start_3
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_5
    :goto_3
    :try_start_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 23846
    :catchall_1
    move-exception v2

    .line 23847
    invoke-virtual {v9}, Ljava/util/zip/ZipFile;->close()V

    throw v2

    .line 23848
    :cond_6
    :try_start_5
    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "assets/lib/"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v16

    if-nez v16, :cond_2

    .line 23849
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 23850
    const/16 v16, 0x2f

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v16

    .line 23851
    const/16 v17, -0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_7

    .line 23852
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "illegal line in xzs metadata: ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 23853
    :catchall_2
    move-exception v2

    goto :goto_2

    .line 23854
    :cond_7
    const/4 v15, 0x0

    move/from16 v0, v16

    invoke-virtual {v5, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 23855
    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 23856
    invoke-static {v8, v15}, LX/01z;->a([Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    .line 23857
    new-instance v17, LX/0Az;

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-direct {v0, v5, v2, v4, v1}, LX/0Az;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 23858
    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 23859
    if-ltz v16, :cond_2

    .line 23860
    invoke-interface {v12, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 23861
    invoke-virtual {v7, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Az;

    .line 23862
    if-eqz v2, :cond_8

    iget v2, v2, LX/0Az;->b:I

    move/from16 v0, v16

    if-ge v0, v2, :cond_2

    .line 23863
    :cond_8
    move-object/from16 v0, v17

    invoke-virtual {v7, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 23864
    :cond_9
    invoke-interface {v12}, Ljava/util/Set;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v12, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, LX/025;->a([Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 23865
    if-eqz v11, :cond_a

    :try_start_6
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    .line 23866
    :cond_a
    invoke-virtual {v7}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Az;

    .line 23867
    const/4 v4, 0x1

    iput-boolean v4, v2, LX/0Az;->e:Z

    goto :goto_4

    .line 23868
    :catch_1
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    :cond_b
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    goto/16 :goto_3

    .line 23869
    :cond_c
    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/0AP;->d:I

    .line 23870
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [LX/0Az;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [LX/0Az;

    move-object/from16 v0, p0

    iput-object v2, v0, LX/0AP;->b:[LX/0Az;

    .line 23871
    move-object/from16 v0, p0

    iput-object v10, v0, LX/0AP;->e:Ljava/util/zip/ZipEntry;

    .line 23872
    move-object/from16 v0, p0

    iput-object v9, v0, LX/0AP;->c:Ljava/util/zip/ZipFile;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()LX/0AO;
    .locals 2

    .prologue
    .line 23873
    new-instance v0, LX/0AO;

    iget-object v1, p0, LX/0AP;->b:[LX/0Az;

    invoke-direct {v0, v1}, LX/0AO;-><init>([LX/07w;)V

    return-object v0
.end method

.method public final b()LX/0Ay;
    .locals 2

    .prologue
    .line 23874
    iget-object v0, p0, LX/0AP;->e:Ljava/util/zip/ZipEntry;

    if-nez v0, :cond_0

    .line 23875
    new-instance v0, LX/07r;

    invoke-direct {v0}, LX/07r;-><init>()V

    .line 23876
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/0Iu;

    invoke-direct {v0, p0}, LX/0Iu;-><init>(LX/0AP;)V

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 23877
    iget-object v0, p0, LX/0AP;->c:Ljava/util/zip/ZipFile;

    invoke-virtual {v0}, Ljava/util/zip/ZipFile;->close()V

    .line 23878
    return-void
.end method
