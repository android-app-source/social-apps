.class public final enum LX/04G;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/04G;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/04G;

.field public static final enum BACKGROUND_PLAY:LX/04G;

.field public static final enum CANVAS:LX/04G;

.field public static final enum CHANNEL_PLAYER:LX/04G;

.field public static final enum FULL_SCREEN_PLAYER:LX/04G;

.field public static final enum INLINE_PLAYER:LX/04G;

.field public static final enum OTHERS:LX/04G;

.field public static final enum RICH_DOCUMENT:LX/04G;

.field public static final enum TAROT:LX/04G;

.field public static final enum TV_PLAYER:LX/04G;

.field public static final enum VIDEO_HOME_PLAYER:LX/04G;

.field public static final enum WATCH_AND_BROWSE:LX/04G;

.field public static final enum WATCH_AND_GO:LX/04G;

.field public static final enum WATCH_AND_GO_FULLSCREEN:LX/04G;

.field public static final enum YOUTUBE_FULL_SCREEN_PLAYER:LX/04G;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 12944
    new-instance v0, LX/04G;

    const-string v1, "CHANNEL_PLAYER"

    const-string v2, "channel"

    invoke-direct {v0, v1, v4, v2}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->CHANNEL_PLAYER:LX/04G;

    .line 12945
    new-instance v0, LX/04G;

    const-string v1, "INLINE_PLAYER"

    const-string v2, "inline"

    invoke-direct {v0, v1, v5, v2}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    .line 12946
    new-instance v0, LX/04G;

    const-string v1, "FULL_SCREEN_PLAYER"

    const-string v2, "full_screen"

    invoke-direct {v0, v1, v6, v2}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    .line 12947
    new-instance v0, LX/04G;

    const-string v1, "YOUTUBE_FULL_SCREEN_PLAYER"

    const-string v2, "youtube_full_screen"

    invoke-direct {v0, v1, v7, v2}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->YOUTUBE_FULL_SCREEN_PLAYER:LX/04G;

    .line 12948
    new-instance v0, LX/04G;

    const-string v1, "TV_PLAYER"

    const-string v2, "tv"

    invoke-direct {v0, v1, v8, v2}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->TV_PLAYER:LX/04G;

    .line 12949
    new-instance v0, LX/04G;

    const-string v1, "RICH_DOCUMENT"

    const/4 v2, 0x5

    const-string v3, "rich_document"

    invoke-direct {v0, v1, v2, v3}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->RICH_DOCUMENT:LX/04G;

    .line 12950
    new-instance v0, LX/04G;

    const-string v1, "VIDEO_HOME_PLAYER"

    const/4 v2, 0x6

    const-string v3, "video_home"

    invoke-direct {v0, v1, v2, v3}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->VIDEO_HOME_PLAYER:LX/04G;

    .line 12951
    new-instance v0, LX/04G;

    const-string v1, "WATCH_AND_BROWSE"

    const/4 v2, 0x7

    const-string v3, "watch_and_browse"

    invoke-direct {v0, v1, v2, v3}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->WATCH_AND_BROWSE:LX/04G;

    .line 12952
    new-instance v0, LX/04G;

    const-string v1, "WATCH_AND_GO"

    const/16 v2, 0x8

    const-string v3, "watch_and_go"

    invoke-direct {v0, v1, v2, v3}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->WATCH_AND_GO:LX/04G;

    .line 12953
    new-instance v0, LX/04G;

    const-string v1, "WATCH_AND_GO_FULLSCREEN"

    const/16 v2, 0x9

    const-string v3, "watch_and_go_fullscreen"

    invoke-direct {v0, v1, v2, v3}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->WATCH_AND_GO_FULLSCREEN:LX/04G;

    .line 12954
    new-instance v0, LX/04G;

    const-string v1, "CANVAS"

    const/16 v2, 0xa

    const-string v3, "canvas"

    invoke-direct {v0, v1, v2, v3}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->CANVAS:LX/04G;

    .line 12955
    new-instance v0, LX/04G;

    const-string v1, "BACKGROUND_PLAY"

    const/16 v2, 0xb

    const-string v3, "background_play"

    invoke-direct {v0, v1, v2, v3}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->BACKGROUND_PLAY:LX/04G;

    .line 12956
    new-instance v0, LX/04G;

    const-string v1, "TAROT"

    const/16 v2, 0xc

    const-string v3, "tarot"

    invoke-direct {v0, v1, v2, v3}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->TAROT:LX/04G;

    .line 12957
    new-instance v0, LX/04G;

    const-string v1, "OTHERS"

    const/16 v2, 0xd

    const-string v3, "others"

    invoke-direct {v0, v1, v2, v3}, LX/04G;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04G;->OTHERS:LX/04G;

    .line 12958
    const/16 v0, 0xe

    new-array v0, v0, [LX/04G;

    sget-object v1, LX/04G;->CHANNEL_PLAYER:LX/04G;

    aput-object v1, v0, v4

    sget-object v1, LX/04G;->INLINE_PLAYER:LX/04G;

    aput-object v1, v0, v5

    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    aput-object v1, v0, v6

    sget-object v1, LX/04G;->YOUTUBE_FULL_SCREEN_PLAYER:LX/04G;

    aput-object v1, v0, v7

    sget-object v1, LX/04G;->TV_PLAYER:LX/04G;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/04G;->RICH_DOCUMENT:LX/04G;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/04G;->VIDEO_HOME_PLAYER:LX/04G;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/04G;->WATCH_AND_BROWSE:LX/04G;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/04G;->WATCH_AND_GO:LX/04G;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/04G;->WATCH_AND_GO_FULLSCREEN:LX/04G;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/04G;->CANVAS:LX/04G;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/04G;->BACKGROUND_PLAY:LX/04G;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/04G;->TAROT:LX/04G;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/04G;->OTHERS:LX/04G;

    aput-object v2, v0, v1

    sput-object v0, LX/04G;->$VALUES:[LX/04G;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12959
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12960
    iput-object p3, p0, LX/04G;->value:Ljava/lang/String;

    .line 12961
    return-void
.end method

.method public static asPlayerType(Ljava/lang/String;)LX/04G;
    .locals 1

    .prologue
    .line 12962
    :try_start_0
    invoke-static {p0}, LX/04G;->valueOf(Ljava/lang/String;)LX/04G;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 12963
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    goto :goto_0
.end method

.method public static fromString(Ljava/lang/String;)LX/04G;
    .locals 5

    .prologue
    .line 12964
    invoke-static {}, LX/04G;->values()[LX/04G;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 12965
    iget-object v4, v0, LX/04G;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 12966
    :goto_1
    return-object v0

    .line 12967
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 12968
    :cond_1
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/04G;
    .locals 1

    .prologue
    .line 12969
    const-class v0, LX/04G;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/04G;

    return-object v0
.end method

.method public static values()[LX/04G;
    .locals 1

    .prologue
    .line 12970
    sget-object v0, LX/04G;->$VALUES:[LX/04G;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/04G;

    return-object v0
.end method
