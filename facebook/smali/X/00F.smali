.class public final LX/00F;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/DoNotOptimize;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:I

.field private static c:I

.field private static d:I

.field private static final e:I

.field private static f:LX/01B;

.field private static g:I

.field private static h:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private static i:I

.field private static j:LX/01E;

.field private static k:I

.field private static l:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1979
    const-class v0, LX/00F;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/00F;->a:Ljava/lang/String;

    .line 1980
    const/4 v0, 0x5

    sput v0, LX/00F;->b:I

    .line 1981
    const/16 v0, 0x1e

    sput v0, LX/00F;->c:I

    .line 1982
    const/16 v0, 0x28

    .line 1983
    sput v0, LX/00F;->d:I

    sput v0, LX/00F;->e:I

    .line 1984
    const/4 v0, -0x1

    sput v0, LX/00F;->k:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1978
    return-void
.end method

.method public static a()J
    .locals 2

    .prologue
    .line 1976
    sget-wide v0, LX/00F;->l:J

    return-wide v0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1971
    packed-switch p0, :pswitch_data_0

    .line 1972
    const-string v0, "??? %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1973
    :pswitch_0
    const-string v0, "disable app"

    goto :goto_0

    .line 1974
    :pswitch_1
    const-string v0, "clear data and log out"

    goto :goto_0

    .line 1975
    :pswitch_2
    const-string v0, "clear caches"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(ILandroid/content/Context;Ljava/lang/String;)V
    .locals 11
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x3

    .line 1936
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 1937
    new-instance v3, Ljava/io/File;

    const-string v0, "remedy_log"

    invoke-direct {v3, v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1938
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1939
    sput-wide v4, LX/00F;->l:J

    .line 1940
    :try_start_0
    invoke-static {v3}, LX/01E;->a(Ljava/io/File;)LX/01E;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1941
    :goto_0
    new-instance v6, Ljava/io/File;

    const-string v7, "app_was_disabled"

    invoke-direct {v6, v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1942
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1943
    new-instance v2, LX/09U;

    invoke-direct {v2, p1}, LX/09U;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, LX/09U;->b()V

    .line 1944
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 1945
    :cond_0
    if-eqz v0, :cond_1

    .line 1946
    iget-wide v6, v0, LX/01E;->a:J

    sub-long v6, v4, v6

    .line 1947
    sput-object v0, LX/00F;->j:LX/01E;

    .line 1948
    new-array v2, v10, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, v0, LX/01E;->b:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v2, v8

    const/4 v8, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v2, v8

    const/4 v8, 0x2

    const v9, 0x5265c00

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v2, v8

    .line 1949
    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-gez v2, :cond_5

    .line 1950
    sget-object v1, LX/00F;->a:Ljava/lang/String;

    const-string v2, "remedy is from the future!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1951
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    iget v0, v0, LX/01E;->b:I

    if-ge v0, p0, :cond_3

    :cond_2
    if-lez p0, :cond_3

    .line 1952
    sput p0, LX/00F;->i:I

    .line 1953
    invoke-static {p1}, LX/00F;->a(Landroid/content/Context;)V

    .line 1954
    invoke-static {p1, p0, p2}, LX/00F;->c(Landroid/content/Context;ILjava/lang/String;)V

    .line 1955
    :try_start_1
    new-instance v0, LX/01E;

    invoke-direct {v0, v4, v5, p0}, LX/01E;-><init>(JI)V

    invoke-virtual {v0, v3}, LX/01E;->b(Ljava/io/File;)V

    .line 1956
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "recorded application of remedy level "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1957
    :goto_2
    if-lt p0, v10, :cond_3

    .line 1958
    :try_start_2
    invoke-static {p1}, LX/00F;->a(Landroid/content/Context;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 1959
    :goto_3
    invoke-static {}, LX/00F;->e()V

    .line 1960
    :cond_3
    return-void

    .line 1961
    :catch_0
    move-exception v0

    .line 1962
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1963
    sget-object v6, LX/00F;->a:Ljava/lang/String;

    const-string v7, "unable to read remedy log file"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1964
    :cond_4
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-object v0, v1

    goto/16 :goto_0

    .line 1965
    :cond_5
    const-wide/32 v8, 0x5265c00

    cmp-long v2, v6, v8

    if-ltz v2, :cond_1

    .line 1966
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-object v0, v1

    goto :goto_1

    .line 1967
    :catch_1
    move-exception v0

    .line 1968
    sget-object v1, LX/00F;->a:Ljava/lang/String;

    const-string v2, "error recording remedy log"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 1969
    :catch_2
    move-exception v0

    .line 1970
    sget-object v1, LX/00F;->a:Ljava/lang/String;

    const-string v2, "error killing sibling processes"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3
.end method

.method private static a(LX/01B;Landroid/content/Context;Ljava/lang/String;J)V
    .locals 11
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 1917
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 1918
    new-instance v5, Ljava/io/File;

    const-string v6, "crash_lock"

    invoke-direct {v5, v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1919
    invoke-static {v5}, LX/01D;->a(Ljava/io/File;)LX/01D;

    move-result-object v5

    const/4 v1, 0x0

    .line 1920
    const/16 v6, 0x3840

    :try_start_0
    invoke-virtual {p0, v6}, LX/01B;->a(I)I

    move-result v6

    .line 1921
    sput v6, LX/00F;->k:I

    .line 1922
    sget v7, LX/00F;->d:I

    if-lt v6, v7, :cond_1

    .line 1923
    :goto_0
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x1

    const/16 v6, 0x3840

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    .line 1924
    const-wide/32 v6, 0x5265c00

    cmp-long v3, p3, v6

    if-gez v3, :cond_6

    if-le v2, v0, :cond_6

    .line 1925
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v2, 0x1

    long-to-double v6, p3

    const-wide v8, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x2

    const-wide v6, 0x3fb61e4f765fd8aeL    # 0.0864

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v2

    .line 1926
    :goto_1
    invoke-static {v0, p1, p2}, LX/00F;->a(ILandroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1927
    if-eqz v5, :cond_0

    invoke-virtual {v5}, LX/01D;->close()V

    .line 1928
    :cond_0
    return-void

    .line 1929
    :cond_1
    :try_start_1
    sget v2, LX/00F;->c:I

    if-lt v6, v2, :cond_2

    move v2, v3

    .line 1930
    goto :goto_0

    .line 1931
    :cond_2
    sget v2, LX/00F;->b:I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-lt v6, v2, :cond_3

    move v2, v0

    .line 1932
    goto :goto_0

    :cond_3
    move v2, v4

    .line 1933
    goto :goto_0

    .line 1934
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1935
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_2
    if-eqz v5, :cond_4

    if-eqz v1, :cond_5

    :try_start_3
    invoke-virtual {v5}, LX/01D;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_4
    :goto_3
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v5}, LX/01D;->close()V

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 1909
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1910
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 1911
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    .line 1912
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1913
    iget v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    if-ne v4, v2, :cond_0

    iget v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-eq v4, v1, :cond_0

    .line 1914
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 1915
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0

    .line 1916
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1889
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1890
    sget-boolean v0, LX/01A;->a:Z

    if-eqz v0, :cond_0

    .line 1891
    const/4 v0, 0x3

    sput v0, LX/00F;->b:I

    .line 1892
    const/4 v0, 0x5

    sput v0, LX/00F;->c:I

    .line 1893
    const/4 v0, 0x7

    sput v0, LX/00F;->d:I

    .line 1894
    :cond_0
    sput p1, LX/00F;->g:I

    .line 1895
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 1896
    new-instance v1, Ljava/io/File;

    const-string v2, "crash_log"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1897
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 1898
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 1899
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    .line 1900
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    cmp-long v0, v6, v2

    if-gez v0, :cond_1

    .line 1901
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1902
    new-instance v0, Ljava/io/IOException;

    const-string v1, "could not delete crash log file"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1903
    :cond_1
    new-instance v0, LX/01B;

    sget v2, LX/00F;->e:I

    invoke-direct {v0, v1, v2}, LX/01B;-><init>(Ljava/io/File;I)V

    sput-object v0, LX/00F;->f:LX/01B;

    .line 1904
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    sput-object v0, LX/00F;->h:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 1905
    new-instance v0, LX/01C;

    invoke-direct {v0}, LX/01C;-><init>()V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 1906
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_2

    .line 1907
    sget-object v0, LX/00F;->f:LX/01B;

    invoke-static {v0, p0, p2, v4, v5}, LX/00F;->a(LX/01B;Landroid/content/Context;Ljava/lang/String;J)V

    .line 1908
    :cond_2
    return-void
.end method

.method public static a(Ljava/io/File;[Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1877
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    move v0, v1

    .line 1878
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_2

    .line 1879
    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1880
    :cond_0
    :goto_1
    return-void

    .line 1881
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1882
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1883
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 1884
    if-eqz v0, :cond_0

    .line 1885
    array-length v2, v0

    :goto_2
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 1886
    invoke-static {v3, p1}, LX/00F;->a(Ljava/io/File;[Ljava/lang/String;)V

    .line 1887
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1888
    :cond_3
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    goto :goto_1
.end method

.method public static a(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1868
    sget v0, LX/00F;->g:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 1869
    :try_start_0
    sget-object v0, LX/00F;->f:LX/01B;

    invoke-virtual {v0}, LX/01B;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1870
    :cond_0
    :goto_0
    :try_start_1
    invoke-static {p1}, LX/00F;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 1871
    :goto_1
    sget v0, LX/00F;->g:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    .line 1872
    sget-object v0, LX/00F;->h:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p0, p1}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 1873
    :cond_1
    invoke-static {}, LX/00F;->e()V

    .line 1874
    return-void

    .line 1875
    :catch_0
    move-exception v0

    .line 1876
    :try_start_2
    sget-object v1, LX/00F;->a:Ljava/lang/String;

    const-string v2, "unable to record crash in crash log!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    goto :goto_0

    :catch_2
    goto :goto_1
.end method

.method private static a(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1860
    sget-object v0, LX/00F;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Uncaught exception in \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v2

    invoke-virtual {v2}, LX/00G;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\':"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1861
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 1862
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 1863
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1864
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1865
    sget-object v4, LX/00F;->a:Ljava/lang/String;

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1866
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1867
    :cond_0
    return-void
.end method

.method public static b()I
    .locals 1

    .prologue
    .line 1813
    sget v0, LX/00F;->i:I

    return v0
.end method

.method private static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1856
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 1857
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "clearing cache dir "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1858
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, LX/00F;->a(Ljava/io/File;[Ljava/lang/String;)V

    .line 1859
    return-void
.end method

.method private static b(Landroid/content/Context;ILjava/lang/String;)V
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1848
    const-string v0, "[employee only] %s using class %s in process %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, LX/00F;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    if-nez p2, :cond_0

    const-string p2, "default"

    :cond_0
    aput-object p2, v1, v5

    const/4 v2, 0x2

    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v3

    .line 1849
    iget-object v4, v3, LX/00G;->b:Ljava/lang/String;

    move-object v3, v4

    .line 1850
    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1851
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v1

    const-string v2, "[fb] crash mitigation"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v4

    invoke-virtual {v4}, LX/00G;->f()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x108001d

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 1852
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_1

    .line 1853
    new-instance v2, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v2}, Landroid/app/Notification$BigTextStyle;-><init>()V

    invoke-virtual {v2, v0}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    .line 1854
    :cond_1
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1855
    return-void
.end method

.method public static c()LX/01E;
    .locals 1

    .prologue
    .line 1847
    sget-object v0, LX/00F;->j:LX/01E;

    return-object v0
.end method

.method private static c(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1843
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 1844
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "clearing data dir "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1845
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "crash_log"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "crash_lock"

    aput-object v3, v0, v2

    const/4 v2, 0x2

    const-string v3, "remedy_log"

    aput-object v3, v0, v2

    const/4 v2, 0x3

    const-string v3, "app_was_disabled"

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-string v3, "ACRA-INSTALLATION"

    aput-object v3, v0, v2

    const/4 v2, 0x5

    const-string v3, "is_employee"

    aput-object v3, v0, v2

    invoke-static {v1, v0}, LX/00F;->a(Ljava/io/File;[Ljava/lang/String;)V

    .line 1846
    return-void
.end method

.method private static c(Landroid/content/Context;ILjava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1827
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    const-string v2, "flags/is_employee"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    move v0, v0

    .line 1828
    if-eqz v0, :cond_0

    .line 1829
    :try_start_0
    invoke-static {p0, p1, p2}, LX/00F;->b(Landroid/content/Context;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1830
    :cond_0
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 1831
    sget-object v0, LX/00F;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown remedy level "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1832
    :goto_1
    return-void

    .line 1833
    :catch_0
    move-exception v0

    .line 1834
    sget-object v1, LX/00F;->a:Ljava/lang/String;

    const-string v2, "non-fatal error showing notification"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1835
    :pswitch_0
    invoke-static {p0}, LX/00F;->d(Landroid/content/Context;)V

    goto :goto_1

    .line 1836
    :pswitch_1
    invoke-static {p0}, LX/00F;->c(Landroid/content/Context;)V

    goto :goto_1

    .line 1837
    :pswitch_2
    if-eqz p2, :cond_1

    .line 1838
    :try_start_1
    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00E;

    .line 1839
    invoke-virtual {v0, p0}, LX/00E;->a(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 1840
    :cond_1
    :goto_2
    invoke-static {p0}, LX/00F;->b(Landroid/content/Context;)V

    goto :goto_1

    .line 1841
    :catch_1
    move-exception v0

    .line 1842
    sget-object v1, LX/00F;->a:Ljava/lang/String;

    const-string v2, "using custom remedy class failed; continuing"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static d()J
    .locals 2

    .prologue
    .line 1826
    sget v0, LX/00F;->k:I

    int-to-long v0, v0

    return-wide v0
.end method

.method private static d(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1817
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 1818
    new-instance v1, Ljava/io/File;

    const-string v2, "app_was_disabled"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1819
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1820
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1821
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "could not disable crash loop: could not create signal file"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1822
    :catch_0
    move-exception v0

    .line 1823
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1824
    :cond_0
    new-instance v0, LX/09U;

    invoke-direct {v0, p0}, LX/09U;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LX/09U;->a()V

    .line 1825
    return-void
.end method

.method private static e()V
    .locals 1

    .prologue
    .line 1814
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 1815
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 1816
    :goto_0
    goto/32 :goto_0
.end method
