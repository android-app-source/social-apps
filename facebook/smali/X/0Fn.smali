.class public final LX/0Fn;
.super LX/089;
.source ""


# instance fields
.field public mConsumingStream:Z

.field private mLastPartIs:LX/0Fm;

.field private final mResProvider:LX/02S;

.field public final mXzs:Lcom/facebook/xzdecoder/XzInputStream;


# direct methods
.method public constructor <init>(LX/02Y;LX/02S;Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 34051
    invoke-direct {p0, p1}, LX/089;-><init>(LX/02Y;)V

    .line 34052
    iput-object p2, p0, LX/0Fn;->mResProvider:LX/02S;

    .line 34053
    new-instance v0, Lcom/facebook/xzdecoder/XzInputStream;

    invoke-direct {v0, p3}, Lcom/facebook/xzdecoder/XzInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, LX/0Fn;->mXzs:Lcom/facebook/xzdecoder/XzInputStream;

    .line 34054
    return-void
.end method

.method private getJarFileSizeFromMeta(LX/02S;Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 34055
    invoke-virtual {p1, p2}, LX/02S;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 34056
    :try_start_0
    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 34057
    :try_start_1
    new-instance v5, Ljava/io/BufferedReader;

    invoke-direct {v5, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 34058
    :try_start_2
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 34059
    const/4 v1, 0x0

    const/16 v6, 0x20

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 34060
    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 34061
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    move-result v0

    .line 34062
    :try_start_3
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 34063
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 34064
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_0
    return v0

    .line 34065
    :catch_0
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 34066
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_0
    if-eqz v1, :cond_2

    :try_start_6
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :goto_1
    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 34067
    :catch_1
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 34068
    :catchall_1
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_2
    if-eqz v1, :cond_3

    :try_start_9
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    :goto_3
    :try_start_a
    throw v0
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 34069
    :catch_2
    move-exception v0

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 34070
    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_4
    if-eqz v3, :cond_1

    if-eqz v2, :cond_4

    :try_start_c
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_5

    :cond_1
    :goto_5
    throw v0

    .line 34071
    :catch_3
    move-exception v5

    :try_start_d
    invoke-static {v1, v5}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 34072
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 34073
    :cond_2
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto :goto_1

    .line 34074
    :catch_4
    move-exception v4

    :try_start_e
    invoke-static {v1, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 34075
    :catchall_4
    move-exception v0

    goto :goto_4

    .line 34076
    :cond_3
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    goto :goto_3

    .line 34077
    :catch_5
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_5

    .line 34078
    :catchall_5
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 34049
    iget-object v0, p0, LX/0Fn;->mXzs:Lcom/facebook/xzdecoder/XzInputStream;

    invoke-static {v0}, LX/02Q;->safeClose(Ljava/io/Closeable;)V

    .line 34050
    return-void
.end method

.method public final nextImpl(LX/02Z;)LX/08A;
    .locals 13

    .prologue
    .line 34035
    iget-boolean v0, p0, LX/0Fn;->mConsumingStream:Z

    if-eqz v0, :cond_0

    .line 34036
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "previous InputDex not closed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34037
    :cond_0
    iget-object v0, p0, LX/0Fn;->mLastPartIs:LX/0Fm;

    if-eqz v0, :cond_2

    .line 34038
    iget-object v0, p0, LX/0Fn;->mLastPartIs:LX/0Fm;

    invoke-virtual {v0}, LX/0Fm;->available()I

    move-result v0

    .line 34039
    if-lez v0, :cond_1

    .line 34040
    iget-object v1, p0, LX/0Fn;->mLastPartIs:LX/0Fm;

    int-to-long v2, v0

    .line 34041
    const-wide/16 v4, 0x0

    .line 34042
    const v6, 0x8000

    new-array v6, v6, [B

    .line 34043
    :goto_0
    cmp-long v7, v4, v2

    if-gez v7, :cond_1

    const/4 v7, 0x0

    const-wide/32 v8, 0x8000

    sub-long v10, v2, v4

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v1, v6, v7, v8}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_1

    .line 34044
    int-to-long v8, v7

    add-long/2addr v4, v8

    goto :goto_0

    .line 34045
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Fn;->mLastPartIs:LX/0Fm;

    .line 34046
    :cond_2
    iget-object v0, p0, LX/0Fn;->mResProvider:LX/02S;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, LX/02Z;->assetName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".meta"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/0Fn;->getJarFileSizeFromMeta(LX/02S;Ljava/lang/String;)I

    move-result v0

    .line 34047
    new-instance v1, LX/0Fm;

    invoke-direct {v1, p0, v0}, LX/0Fm;-><init>(LX/0Fn;I)V

    iput-object v1, p0, LX/0Fn;->mLastPartIs:LX/0Fm;

    .line 34048
    new-instance v0, LX/08A;

    iget-object v1, p0, LX/0Fn;->mLastPartIs:LX/0Fm;

    invoke-direct {v0, p1, v1}, LX/08A;-><init>(LX/02Z;Ljava/io/InputStream;)V

    return-object v0
.end method
