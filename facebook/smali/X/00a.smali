.class public LX/00a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SharedPreferencesUse",
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation


# static fields
.field public static final a:LX/00a;

.field public static final b:Ljava/util/concurrent/ExecutorService;


# instance fields
.field public c:Landroid/content/SharedPreferences;

.field public volatile d:Ljava/lang/String;

.field public volatile e:Ljava/lang/String;

.field public volatile f:J

.field public volatile g:J

.field public volatile h:I

.field public volatile i:Ljava/lang/String;

.field public volatile j:Ljava/lang/String;

.field public volatile k:Ljava/lang/String;

.field public volatile l:Z

.field public volatile m:Z

.field public volatile n:Z

.field public volatile o:LX/00b;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2490
    new-instance v0, LX/00a;

    invoke-direct {v0}, LX/00a;-><init>()V

    sput-object v0, LX/00a;->a:LX/00a;

    .line 2491
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, LX/00a;->b:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2421
    iput-object v1, p0, LX/00a;->d:Ljava/lang/String;

    .line 2422
    iput-object v1, p0, LX/00a;->e:Ljava/lang/String;

    .line 2423
    iput-wide v4, p0, LX/00a;->f:J

    .line 2424
    iput-wide v4, p0, LX/00a;->g:J

    .line 2425
    const/4 v0, -0x1

    iput v0, p0, LX/00a;->h:I

    .line 2426
    iput-object v1, p0, LX/00a;->i:Ljava/lang/String;

    .line 2427
    iput-object v1, p0, LX/00a;->j:Ljava/lang/String;

    .line 2428
    iput-object v1, p0, LX/00a;->k:Ljava/lang/String;

    .line 2429
    iput-boolean v2, p0, LX/00a;->l:Z

    .line 2430
    iput-boolean v2, p0, LX/00a;->m:Z

    .line 2431
    iput-boolean v2, p0, LX/00a;->n:Z

    .line 2432
    sget-object v0, LX/00b;->NONE:LX/00b;

    iput-object v0, p0, LX/00a;->o:LX/00b;

    .line 2433
    return-void
.end method

.method public static a()LX/00a;
    .locals 1

    .prologue
    .line 2489
    sget-object v0, LX/00a;->a:LX/00a;

    return-object v0
.end method

.method public static j(LX/00a;)V
    .locals 14

    .prologue
    .line 2482
    const/4 v6, 0x0

    .line 2483
    iget-wide v8, p0, LX/00a;->f:J

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    iget v7, p0, LX/00a;->h:I

    const/4 v8, -0x1

    if-ne v7, v8, :cond_3

    .line 2484
    :cond_0
    :goto_0
    move v0, v6

    .line 2485
    if-nez v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LX/00a;->g:J

    const-wide/32 v4, 0x1d4c0

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 2486
    :cond_1
    sget-object v0, LX/00b;->FROZEN:LX/00b;

    iput-object v0, p0, LX/00a;->o:LX/00b;

    .line 2487
    :goto_1
    return-void

    .line 2488
    :cond_2
    invoke-direct {p0}, LX/00a;->l()V

    goto :goto_1

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-wide v10, p0, LX/00a;->f:J

    iget v7, p0, LX/00a;->h:I

    int-to-long v12, v7

    add-long/2addr v10, v12

    cmp-long v7, v8, v10

    if-gez v7, :cond_0

    const/4 v6, 0x1

    goto :goto_0
.end method

.method private l()V
    .locals 9

    .prologue
    .line 2465
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicy()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 2466
    new-instance v1, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicy()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>(Landroid/os/StrictMode$ThreadPolicy;)V

    invoke-virtual {v1}, Landroid/os/StrictMode$ThreadPolicy$Builder;->permitNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 2467
    :try_start_0
    iget-object v3, p0, LX/00a;->d:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/00a;->i:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/00a;->j:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/00a;->e:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 2468
    :cond_0
    sget-object v3, LX/00b;->ERROR_TEMPLATE:LX/00b;

    iput-object v3, p0, LX/00a;->o:LX/00b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2469
    :goto_0
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 2470
    return-void

    .line 2471
    :catchall_0
    move-exception v1

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v1

    .line 2472
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    .line 2473
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, LX/00a;->j:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, LX/00a;->n()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, LX/00a;->k:Ljava/lang/String;

    .line 2474
    iget-object v5, p0, LX/00a;->d:Ljava/lang/String;

    const-string v6, "CURSOR_BOOKMARK"

    iget-object v7, p0, LX/00a;->e:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "CLIENT_QUERY_ID_BOOKMARK"

    iget-object v7, p0, LX/00a;->k:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 2475
    sget-object v6, LX/00d;->a:LX/00d;

    move-object v6, v6

    .line 2476
    iget-object v7, v6, LX/00d;->c:Ljava/lang/String;

    move-object v6, v7

    .line 2477
    new-instance v7, Landroid/net/Uri$Builder;

    invoke-direct {v7}, Landroid/net/Uri$Builder;-><init>()V

    const-string v8, "https"

    invoke-virtual {v7, v8}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "graphqlbatch"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 2478
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2479
    iget-object v6, p0, LX/00a;->i:Ljava/lang/String;

    invoke-static {v5, v6, v3, v4}, LX/04w;->a(Ljava/lang/String;Ljava/lang/String;J)LX/00b;

    move-result-object v3

    iput-object v3, p0, LX/00a;->o:LX/00b;

    .line 2480
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, LX/00a;->g:J

    .line 2481
    sget-object v3, LX/00a;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/common/udppriming/client/ColdStartPrimingInformation$1;

    invoke-direct {v4, p0}, Lcom/facebook/common/udppriming/client/ColdStartPrimingInformation$1;-><init>(LX/00a;)V

    const v5, -0x7daeef1a

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto/16 :goto_0
.end method

.method private declared-synchronized m()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2452
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/00a;->c:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 2453
    :goto_0
    monitor-exit p0

    return v0

    .line 2454
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    const-string v1, "COLD_START_PRIME_INFO/FIRST_FETCH_STRING"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/00a;->d:Ljava/lang/String;

    .line 2455
    iget-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    const-string v1, "COLD_START_PRIME_INFO/STORY_CURSOR"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/00a;->e:Ljava/lang/String;

    .line 2456
    iget-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    const-string v1, "COLD_START_PRIME_INFO/LAST_HEAD_FETCH_TIME"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, LX/00a;->f:J

    .line 2457
    iget-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    const-string v1, "COLD_START_PRIME_INFO/FROZEN_FEED_TIME"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/00a;->h:I

    .line 2458
    iget-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    const-string v1, "COLD_START_PRIME_INFO/LAST_UDP_PRIMING_TIME"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, LX/00a;->g:J

    .line 2459
    iget-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    const-string v1, "COLD_START_PRIME_INFO/USER_AGENT"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/00a;->i:Ljava/lang/String;

    .line 2460
    iget-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    const-string v1, "COLD_START_PRIME_INFO/USER_ID"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/00a;->j:Ljava/lang/String;

    .line 2461
    iget-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    const-string v1, "COLD_START_PRIME_INFO/COLD_START_ADVANCE_PRIME_ENABLED"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/00a;->l:Z

    .line 2462
    iget-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    const-string v1, "COLD_START_PRIME_INFO/COLD_START_ADVANCE_PRIME_FROM_NODEX_ENABLED"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/00a;->m:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2463
    const/4 v0, 0x1

    goto :goto_0

    .line 2464
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static n()Ljava/util/UUID;
    .locals 2

    .prologue
    .line 2449
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 2450
    :try_start_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2451
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2441
    if-nez p1, :cond_1

    iget-object v0, p0, LX/00a;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2442
    :cond_0
    :goto_0
    return-void

    .line 2443
    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, LX/00a;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2444
    :cond_2
    iput-object p1, p0, LX/00a;->e:Ljava/lang/String;

    .line 2445
    iget-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 2446
    iget-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2447
    const-string v1, "COLD_START_PRIME_INFO/STORY_CURSOR"

    iget-object v2, p0, LX/00a;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2448
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 2437
    iget-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 2438
    const-string v0, "COLD_START_PRIMING_INFO_STORAGE"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    .line 2439
    invoke-direct {p0}, LX/00a;->m()Z

    move-result v0

    .line 2440
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2436
    iget-object v0, p0, LX/00a;->o:LX/00b;

    sget-object v1, LX/00b;->SENT:LX/00b;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2434
    iget-object v1, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 2435
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/00a;->c:Landroid/content/SharedPreferences;

    const-string v2, "COLD_START_PRIME_INFO/STORY_CURSOR"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
