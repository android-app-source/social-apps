.class public LX/06m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/06n;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private d:I


# direct methods
.method public constructor <init>(III)V
    .locals 1

    .prologue
    .line 18141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18142
    iput p1, p0, LX/06m;->a:I

    .line 18143
    iput p2, p0, LX/06m;->b:I

    .line 18144
    iput p3, p0, LX/06m;->c:I

    .line 18145
    const/4 v0, 0x0

    iput v0, p0, LX/06m;->d:I

    .line 18146
    return-void
.end method


# virtual methods
.method public final a(Z)I
    .locals 1

    .prologue
    .line 18154
    invoke-virtual {p0, p1}, LX/06m;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18155
    iget v0, p0, LX/06m;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/06m;->d:I

    .line 18156
    iget v0, p0, LX/06m;->c:I

    .line 18157
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a()LX/06l;
    .locals 1

    .prologue
    .line 18153
    sget-object v0, LX/06l;->BACK_TO_BACK:LX/06l;

    return-object v0
.end method

.method public final b(Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 18148
    if-eqz p1, :cond_2

    .line 18149
    iget v2, p0, LX/06m;->d:I

    iget v3, p0, LX/06m;->a:I

    if-ge v2, v3, :cond_1

    .line 18150
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 18151
    goto :goto_0

    .line 18152
    :cond_2
    iget v2, p0, LX/06m;->d:I

    iget v3, p0, LX/06m;->b:I

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 18147
    const-string v0, "BackToBackRetryStrategy: attempt:%d/%d/%d, delay:%d seconds"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, LX/06m;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, LX/06m;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, LX/06m;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, LX/06m;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/05V;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
