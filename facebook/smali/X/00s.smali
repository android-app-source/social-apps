.class public LX/00s;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static failureReason:Ljava/lang/String;

.field public static resultValue:LX/02j;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3148
    const-string v0, ""

    sput-object v0, LX/00s;->failureReason:Ljava/lang/String;

    .line 3149
    sget-object v0, LX/02j;->NOT_ATTEMPTED:LX/02j;

    sput-object v0, LX/00s;->resultValue:LX/02j;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3147
    return-void
.end method

.method public static getFailureString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3133
    sget-object v0, LX/00s;->resultValue:LX/02j;

    move-object v0, v0

    .line 3134
    sget-object v1, LX/02j;->FAILURE:LX/02j;

    if-ne v0, v1, :cond_0

    .line 3135
    sget-object v0, LX/00s;->failureReason:Ljava/lang/String;

    return-object v0

    .line 3136
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No failure string is provided when the operation did not fail."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static replaceBuffer(LX/00o;)V
    .locals 4

    .prologue
    .line 3137
    sget-object v0, LX/00s;->resultValue:LX/02j;

    sget-object v1, LX/02j;->NOT_ATTEMPTED:LX/02j;

    if-eq v0, v1, :cond_0

    .line 3138
    const-string v0, "DalvikReplaceBuffer"

    const-string v1, "Multiple attempts to replace the buffer detected!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3139
    :goto_0
    return-void

    .line 3140
    :cond_0
    :try_start_0
    iget v0, p0, LX/00o;->bufferSizeBytes:I

    invoke-static {v0}, Lcom/facebook/common/dextricks/DalvikInternals;->fixLinearAllocBuffer(I)V

    .line 3141
    sget-object v0, LX/02j;->SUCCESS:LX/02j;

    sput-object v0, LX/00s;->resultValue:LX/02j;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3142
    :catch_0
    move-exception v0

    .line 3143
    sget-object v1, LX/02j;->FAILURE:LX/02j;

    sput-object v1, LX/00s;->resultValue:LX/02j;

    .line 3144
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    sput-object v1, LX/00s;->failureReason:Ljava/lang/String;

    .line 3145
    const-string v1, "DalvikReplaceBuffer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to replace LinearAlloc buffer (at size "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, LX/00o;->bufferSizeBytes:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "). Continuing with standard buffer."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
