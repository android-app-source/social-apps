.class public final LX/0Lw;
.super LX/0BV;
.source ""


# instance fields
.field public final g:LX/0Lx;

.field public final h:LX/0Lx;

.field private final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Au;JJIJLjava/util/List;LX/0Lx;LX/0Lx;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Au;",
            "JJIJ",
            "Ljava/util/List",
            "<",
            "LX/0BT;",
            ">;",
            "LX/0Lx;",
            "LX/0Lx;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44855
    invoke-direct/range {p0 .. p9}, LX/0BV;-><init>(LX/0Au;JJIJLjava/util/List;)V

    .line 44856
    iput-object p10, p0, LX/0Lw;->g:LX/0Lx;

    .line 44857
    iput-object p11, p0, LX/0Lw;->h:LX/0Lx;

    .line 44858
    iput-object p12, p0, LX/0Lw;->i:Ljava/lang/String;

    .line 44859
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 9

    .prologue
    .line 44860
    iget-object v0, p0, LX/0BV;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 44861
    iget-object v0, p0, LX/0BV;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LX/0BV;->d:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    .line 44862
    :goto_0
    return v0

    .line 44863
    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 44864
    const/4 v0, -0x1

    goto :goto_0

    .line 44865
    :cond_1
    iget-wide v0, p0, LX/0BV;->e:J

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    iget-wide v2, p0, LX/0Ag;->b:J

    div-long/2addr v0, v2

    .line 44866
    iget v2, p0, LX/0BV;->d:I

    .line 44867
    add-long v5, p1, v0

    const-wide/16 v7, 0x1

    sub-long/2addr v5, v7

    div-long/2addr v5, v0

    move-wide v0, v5

    .line 44868
    long-to-int v0, v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public final a(LX/0Ah;)LX/0Au;
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 44869
    iget-object v0, p0, LX/0Lw;->g:LX/0Lx;

    if-eqz v0, :cond_0

    .line 44870
    iget-object v0, p0, LX/0Lw;->g:LX/0Lx;

    iget-object v1, p1, LX/0Ah;->c:LX/0AR;

    iget-object v1, v1, LX/0AR;->a:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p1, LX/0Ah;->c:LX/0AR;

    iget v3, v3, LX/0AR;->c:I

    invoke-virtual/range {v0 .. v5}, LX/0Lx;->a(Ljava/lang/String;IIJ)Ljava/lang/String;

    move-result-object v3

    .line 44871
    new-instance v1, LX/0Au;

    iget-object v2, p0, LX/0Lw;->i:Ljava/lang/String;

    const-wide/16 v6, -0x1

    invoke-direct/range {v1 .. v7}, LX/0Au;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 44872
    :goto_0
    return-object v1

    :cond_0
    invoke-super {p0, p1}, LX/0BV;->a(LX/0Ah;)LX/0Au;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(LX/0Ah;I)LX/0Au;
    .locals 8

    .prologue
    .line 44873
    iget-object v0, p0, LX/0BV;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 44874
    iget-object v0, p0, LX/0BV;->f:Ljava/util/List;

    iget v1, p0, LX/0BV;->d:I

    sub-int v1, p2, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0BT;

    iget-wide v4, v0, LX/0BT;->a:J

    .line 44875
    :goto_0
    iget-object v0, p0, LX/0Lw;->h:LX/0Lx;

    iget-object v1, p1, LX/0Ah;->c:LX/0AR;

    iget-object v1, v1, LX/0AR;->a:Ljava/lang/String;

    iget-object v2, p1, LX/0Ah;->c:LX/0AR;

    iget v3, v2, LX/0AR;->c:I

    move v2, p2

    invoke-virtual/range {v0 .. v5}, LX/0Lx;->a(Ljava/lang/String;IIJ)Ljava/lang/String;

    move-result-object v3

    .line 44876
    new-instance v1, LX/0Au;

    iget-object v2, p0, LX/0Lw;->i:Ljava/lang/String;

    const-wide/16 v4, 0x0

    const-wide/16 v6, -0x1

    invoke-direct/range {v1 .. v7}, LX/0Au;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    return-object v1

    .line 44877
    :cond_0
    iget v0, p0, LX/0BV;->d:I

    sub-int v0, p2, v0

    int-to-long v0, v0

    iget-wide v2, p0, LX/0BV;->e:J

    mul-long v4, v0, v2

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 44878
    iget-object v0, p0, LX/0BV;->f:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0BV;->f:Ljava/util/List;

    iget v1, p0, LX/0BV;->d:I

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0BT;

    iget-object v0, v0, LX/0BT;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 44879
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 44880
    const-string v0, "<SegmentTimeline>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44881
    iget-object v0, p0, LX/0BV;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 44882
    iget-object v0, p0, LX/0BV;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0BT;

    .line 44883
    invoke-virtual {v0}, LX/0BT;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 44884
    :cond_0
    const-string v0, "</SegmentTimeline>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44885
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
