.class public LX/0Hr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0BE;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private final b:LX/01o;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LX/01o;)V
    .locals 3

    .prologue
    .line 38116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38117
    new-instance v0, LX/01q;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "rti.mqtt.fbns_notification_store_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/01q;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v0}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, LX/0Hr;->a:Landroid/content/SharedPreferences;

    .line 38118
    iput-object p3, p0, LX/0Hr;->b:LX/01o;

    .line 38119
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38120
    const-string v0, "S"

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 38121
    monitor-enter p0

    :try_start_0
    const-string v0, "NotificationDeliveryStoreSharedPreferences"

    const-string v1, "remove %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38122
    iget-object v0, p0, LX/0Hr;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38123
    monitor-exit p0

    return-void

    .line 38124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 8

    .prologue
    .line 38125
    monitor-enter p0

    :try_start_0
    const-string v0, "NotificationDeliveryStoreSharedPreferences"

    const-string v1, "add %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38126
    iget-object v0, p0, LX/0Hr;->b:LX/01o;

    invoke-virtual {v0}, LX/01o;->a()J

    move-result-wide v4

    .line 38127
    new-instance v1, LX/0Hq;

    move-object v2, p2

    move-object v3, p1

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, LX/0Hq;-><init>(Landroid/content/Intent;Ljava/lang/String;JJ)V

    invoke-virtual {v1}, LX/0Hq;->a()Ljava/lang/String;

    move-result-object v0

    .line 38128
    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 38129
    iget-object v1, p0, LX/0Hr;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38130
    :cond_0
    monitor-exit p0

    return-void

    .line 38131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0Hn;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 38132
    monitor-enter p0

    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 38133
    iget-object v0, p0, LX/0Hr;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 38134
    iget-object v4, p0, LX/0Hr;->a:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 38135
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 38136
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, LX/0Hq;->a(Ljava/lang/Object;)LX/0Hq;

    move-result-object v6

    .line 38137
    if-eqz v6, :cond_0

    iget-wide v8, v6, LX/0Hq;->a:J

    const-wide/32 v10, 0x5265c00

    add-long/2addr v8, v10

    iget-object v7, p0, LX/0Hr;->b:LX/01o;

    invoke-virtual {v7}, LX/01o;->a()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-ltz v7, :cond_0

    iget-wide v8, v6, LX/0Hq;->a:J

    iget-object v7, p0, LX/0Hr;->b:LX/01o;

    invoke-virtual {v7}, LX/01o;->a()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-lez v7, :cond_1

    .line 38138
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v4, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move v2, v1

    .line 38139
    goto :goto_0

    .line 38140
    :cond_1
    iget-wide v8, v6, LX/0Hq;->b:J

    const-wide/32 v10, 0x493e0

    add-long/2addr v8, v10

    iget-object v0, p0, LX/0Hr;->b:LX/01o;

    invoke-virtual {v0}, LX/01o;->a()J

    move-result-wide v10

    cmp-long v0, v8, v10

    if-gez v0, :cond_4

    .line 38141
    const-string v0, "NotificationDeliveryStoreSharedPreferences"

    const-string v2, "checkAndUpdateRetryList found %s %d %d"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v6, LX/0Hn;->d:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-wide v10, v6, LX/0Hq;->a:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-wide v10, v6, LX/0Hq;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v0, v2, v7}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38142
    iget-object v0, p0, LX/0Hr;->b:LX/01o;

    invoke-virtual {v0}, LX/01o;->a()J

    move-result-wide v8

    iput-wide v8, v6, LX/0Hq;->b:J

    .line 38143
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38144
    iget-object v0, v6, LX/0Hn;->d:Ljava/lang/String;

    invoke-virtual {v6}, LX/0Hq;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move v0, v1

    :goto_1
    move v2, v0

    .line 38145
    goto/16 :goto_0

    .line 38146
    :cond_2
    if-eqz v2, :cond_3

    .line 38147
    invoke-static {v4}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38148
    :cond_3
    monitor-exit p0

    return-object v3

    .line 38149
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v0, v2

    goto :goto_1
.end method
