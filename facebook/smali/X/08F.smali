.class public final LX/08F;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final flags:I

.field public final maximumOptimizationAttempts:I

.field public final optTimeSliceMs:I

.field public final prio:LX/08D;

.field public final processPollMs:I

.field public final timeBetweenOptimizationAttemptsMs:I

.field public final yieldTimeSliceMs:I


# direct methods
.method private constructor <init>(LX/08D;IIIIII)V
    .locals 0

    .prologue
    .line 20980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20981
    iput-object p1, p0, LX/08F;->prio:LX/08D;

    .line 20982
    iput p2, p0, LX/08F;->flags:I

    .line 20983
    iput p3, p0, LX/08F;->optTimeSliceMs:I

    .line 20984
    iput p4, p0, LX/08F;->yieldTimeSliceMs:I

    .line 20985
    iput p5, p0, LX/08F;->processPollMs:I

    .line 20986
    iput p6, p0, LX/08F;->timeBetweenOptimizationAttemptsMs:I

    .line 20987
    iput p7, p0, LX/08F;->maximumOptimizationAttempts:I

    .line 20988
    return-void
.end method

.method public synthetic constructor <init>(LX/08D;IIIIIILX/0Fi;)V
    .locals 0

    .prologue
    .line 20989
    invoke-direct/range {p0 .. p7}, LX/08F;-><init>(LX/08D;IIIIII)V

    return-void
.end method
