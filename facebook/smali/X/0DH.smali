.class public final LX/0DH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/0DI;


# direct methods
.method public constructor <init>(LX/0DI;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 29749
    iput-object p1, p0, LX/0DH;->c:LX/0DI;

    iput-object p2, p0, LX/0DH;->a:Ljava/lang/String;

    iput-object p3, p0, LX/0DH;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x833244

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 29750
    iget-object v1, p0, LX/0DH;->c:LX/0DI;

    iget-object v2, p0, LX/0DH;->a:Ljava/lang/String;

    iget-object v3, p0, LX/0DH;->b:Ljava/lang/String;

    .line 29751
    iget-object v5, v1, LX/0DI;->e:LX/0D5;

    if-nez v5, :cond_0

    .line 29752
    :goto_0
    const v1, 0x235b7eae

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 29753
    :cond_0
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 29754
    const-string v6, "action"

    const-string p0, "instant_experience_chrome_basic_action_pressed"

    invoke-virtual {v5, v6, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29755
    const-string v6, "url"

    iget-object p0, v1, LX/0DI;->e:LX/0D5;

    invoke-virtual {p0}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29756
    iget-object v6, v1, LX/0DI;->f:LX/0CQ;

    iget-object p0, v1, LX/0DI;->g:Landroid/os/Bundle;

    invoke-virtual {v6, v5, p0}, LX/0CQ;->a(Ljava/util/Map;Landroid/os/Bundle;)V

    .line 29757
    iget-object v5, v1, LX/0DI;->e:LX/0D5;

    const-string v6, "(function(objID, redirectUrl){  if (\'FBExtensions\' in window && \'Event\' in FBExtensions) {    FBExtensions.Event._fire(      \'chrome.basicButtonPressed\',      atob(objID),      atob(redirectUrl)    );  }})(\'%s\', \'%s\');"

    const/4 p0, 0x2

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v2, p0, p1

    const/4 p1, 0x1

    aput-object v3, p0, p1

    invoke-static {v6, p0}, LX/0DI;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0D5;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
