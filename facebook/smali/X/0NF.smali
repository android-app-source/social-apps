.class public final LX/0NF;
.super LX/0N4;
.source ""


# instance fields
.field private final b:LX/0Oj;

.field private c:Z

.field private d:J

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(LX/0LS;)V
    .locals 2

    .prologue
    .line 49716
    invoke-direct {p0, p1}, LX/0N4;-><init>(LX/0LS;)V

    .line 49717
    invoke-static {}, LX/0L4;->a()LX/0L4;

    move-result-object v0

    invoke-interface {p1, v0}, LX/0LS;->a(LX/0L4;)V

    .line 49718
    new-instance v0, LX/0Oj;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, LX/0NF;->b:LX/0Oj;

    .line 49719
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 49691
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0NF;->c:Z

    .line 49692
    return-void
.end method

.method public final a(JZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 49710
    if-nez p3, :cond_0

    .line 49711
    :goto_0
    return-void

    .line 49712
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0NF;->c:Z

    .line 49713
    iput-wide p1, p0, LX/0NF;->d:J

    .line 49714
    iput v1, p0, LX/0NF;->e:I

    .line 49715
    iput v1, p0, LX/0NF;->f:I

    goto :goto_0
.end method

.method public final a(LX/0Oj;)V
    .locals 7

    .prologue
    const/16 v6, 0xa

    .line 49697
    iget-boolean v0, p0, LX/0NF;->c:Z

    if-nez v0, :cond_0

    .line 49698
    :goto_0
    return-void

    .line 49699
    :cond_0
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    .line 49700
    iget v1, p0, LX/0NF;->f:I

    if-ge v1, v6, :cond_1

    .line 49701
    iget v1, p0, LX/0NF;->f:I

    rsub-int/lit8 v1, v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 49702
    iget-object v2, p1, LX/0Oj;->a:[B

    .line 49703
    iget v3, p1, LX/0Oj;->b:I

    move v3, v3

    .line 49704
    iget-object v4, p0, LX/0NF;->b:LX/0Oj;

    iget-object v4, v4, LX/0Oj;->a:[B

    iget v5, p0, LX/0NF;->f:I

    invoke-static {v2, v3, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49705
    iget v2, p0, LX/0NF;->f:I

    add-int/2addr v1, v2

    if-ne v1, v6, :cond_1

    .line 49706
    iget-object v1, p0, LX/0NF;->b:LX/0Oj;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, LX/0Oj;->b(I)V

    .line 49707
    iget-object v1, p0, LX/0NF;->b:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->r()I

    move-result v1

    add-int/lit8 v1, v1, 0xa

    iput v1, p0, LX/0NF;->e:I

    .line 49708
    :cond_1
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    invoke-interface {v1, p1, v0}, LX/0LS;->a(LX/0Oj;I)V

    .line 49709
    iget v1, p0, LX/0NF;->f:I

    add-int/2addr v0, v1

    iput v0, p0, LX/0NF;->f:I

    goto :goto_0
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 49693
    iget-boolean v0, p0, LX/0NF;->c:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/0NF;->e:I

    if-eqz v0, :cond_0

    iget v0, p0, LX/0NF;->f:I

    iget v1, p0, LX/0NF;->e:I

    if-eq v0, v1, :cond_1

    .line 49694
    :cond_0
    :goto_0
    return-void

    .line 49695
    :cond_1
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    iget-wide v2, p0, LX/0NF;->d:J

    const/4 v4, 0x1

    iget v5, p0, LX/0NF;->e:I

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    .line 49696
    iput-boolean v6, p0, LX/0NF;->c:Z

    goto :goto_0
.end method
