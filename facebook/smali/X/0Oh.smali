.class public final LX/0Oh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[B

.field public static final b:[F

.field private static final c:Ljava/lang/Object;

.field private static d:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53605
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/0Oh;->a:[B

    .line 53606
    const/16 v0, 0x11

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    sput-object v0, LX/0Oh;->b:[F

    .line 53607
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0Oh;->c:Ljava/lang/Object;

    .line 53608
    const/16 v0, 0xa

    new-array v0, v0, [I

    sput-object v0, LX/0Oh;->d:[I

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 53609
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f8ba2e9
        0x3f68ba2f
        0x3fba2e8c
        0x3f9b26ca
        0x400ba2e9
        0x3fe8ba2f
        0x403a2e8c
        0x401b26ca
        0x3fd1745d
        0x3fae8ba3
        0x3ff83e10
        0x3fcede62
        0x3faaaaab
        0x3fc00000    # 1.5f
        0x40000000    # 2.0f
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 53774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53775
    return-void
.end method

.method public static a([BI)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 53752
    sget-object v4, LX/0Oh;->c:Ljava/lang/Object;

    monitor-enter v4

    move v3, v0

    move v1, v0

    .line 53753
    :goto_0
    if-ge v1, p1, :cond_1

    .line 53754
    :try_start_0
    invoke-static {p0, v1, p1}, LX/0Oh;->a([BII)I

    move-result v2

    .line 53755
    if-ge v2, p1, :cond_3

    .line 53756
    sget-object v1, LX/0Oh;->d:[I

    array-length v1, v1

    if-gt v1, v3, :cond_0

    .line 53757
    sget-object v1, LX/0Oh;->d:[I

    sget-object v5, LX/0Oh;->d:[I

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x2

    invoke-static {v1, v5}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    sput-object v1, LX/0Oh;->d:[I

    .line 53758
    :cond_0
    sget-object v5, LX/0Oh;->d:[I

    add-int/lit8 v1, v3, 0x1

    aput v2, v5, v3

    .line 53759
    add-int/lit8 v2, v2, 0x3

    move v3, v1

    move v1, v2

    goto :goto_0

    .line 53760
    :cond_1
    sub-int v5, p1, v3

    move v1, v0

    move v2, v0

    .line 53761
    :goto_1
    if-ge v0, v3, :cond_2

    .line 53762
    sget-object v6, LX/0Oh;->d:[I

    aget v6, v6, v0

    .line 53763
    sub-int/2addr v6, v2

    .line 53764
    invoke-static {p0, v2, p0, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 53765
    add-int/2addr v1, v6

    .line 53766
    add-int/lit8 v7, v1, 0x1

    const/4 v8, 0x0

    aput-byte v8, p0, v1

    .line 53767
    add-int/lit8 v1, v7, 0x1

    const/4 v8, 0x0

    aput-byte v8, p0, v7

    .line 53768
    add-int/lit8 v6, v6, 0x3

    add-int/2addr v2, v6

    .line 53769
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 53770
    :cond_2
    sub-int v0, v5, v1

    .line 53771
    invoke-static {p0, v2, p0, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 53772
    monitor-exit v4

    return v5

    .line 53773
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method private static a([BII)I
    .locals 3

    .prologue
    .line 53748
    move v0, p1

    :goto_0
    add-int/lit8 v1, p2, -0x2

    if-ge v0, v1, :cond_0

    .line 53749
    aget-byte v1, p0, v0

    if-nez v1, :cond_1

    add-int/lit8 v1, v0, 0x1

    aget-byte v1, p0, v1

    if-nez v1, :cond_1

    add-int/lit8 v1, v0, 0x2

    aget-byte v1, p0, v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    move p2, v0

    .line 53750
    :cond_0
    return p2

    .line 53751
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a([BII[Z)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 53717
    sub-int v3, p2, p1

    .line 53718
    if-ltz v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 53719
    if-nez v3, :cond_2

    .line 53720
    :cond_0
    :goto_1
    return p2

    :cond_1
    move v0, v2

    .line 53721
    goto :goto_0

    .line 53722
    :cond_2
    if-eqz p3, :cond_5

    .line 53723
    aget-boolean v0, p3, v2

    if-eqz v0, :cond_3

    .line 53724
    invoke-static {p3}, LX/0Oh;->a([Z)V

    .line 53725
    add-int/lit8 p2, p1, -0x3

    goto :goto_1

    .line 53726
    :cond_3
    if-le v3, v1, :cond_4

    aget-boolean v0, p3, v1

    if-eqz v0, :cond_4

    aget-byte v0, p0, p1

    if-ne v0, v1, :cond_4

    .line 53727
    invoke-static {p3}, LX/0Oh;->a([Z)V

    .line 53728
    add-int/lit8 p2, p1, -0x2

    goto :goto_1

    .line 53729
    :cond_4
    if-le v3, v6, :cond_5

    aget-boolean v0, p3, v6

    if-eqz v0, :cond_5

    aget-byte v0, p0, p1

    if-nez v0, :cond_5

    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    if-ne v0, v1, :cond_5

    .line 53730
    invoke-static {p3}, LX/0Oh;->a([Z)V

    .line 53731
    add-int/lit8 p2, p1, -0x1

    goto :goto_1

    .line 53732
    :cond_5
    add-int/lit8 v4, p2, -0x1

    .line 53733
    add-int/lit8 v0, p1, 0x2

    :goto_2
    if-ge v0, v4, :cond_9

    .line 53734
    aget-byte v5, p0, v0

    and-int/lit16 v5, v5, 0xfe

    if-nez v5, :cond_8

    .line 53735
    add-int/lit8 v5, v0, -0x2

    aget-byte v5, p0, v5

    if-nez v5, :cond_7

    add-int/lit8 v5, v0, -0x1

    aget-byte v5, p0, v5

    if-nez v5, :cond_7

    aget-byte v5, p0, v0

    if-ne v5, v1, :cond_7

    .line 53736
    if-eqz p3, :cond_6

    .line 53737
    invoke-static {p3}, LX/0Oh;->a([Z)V

    .line 53738
    :cond_6
    add-int/lit8 p2, v0, -0x2

    goto :goto_1

    .line 53739
    :cond_7
    add-int/lit8 v0, v0, -0x2

    .line 53740
    :cond_8
    add-int/lit8 v0, v0, 0x3

    goto :goto_2

    .line 53741
    :cond_9
    if-eqz p3, :cond_0

    .line 53742
    if-le v3, v6, :cond_b

    add-int/lit8 v0, p2, -0x3

    aget-byte v0, p0, v0

    if-nez v0, :cond_a

    add-int/lit8 v0, p2, -0x2

    aget-byte v0, p0, v0

    if-nez v0, :cond_a

    aget-byte v0, p0, v4

    if-ne v0, v1, :cond_a

    move v0, v1

    :goto_3
    aput-boolean v0, p3, v2

    .line 53743
    if-le v3, v1, :cond_10

    add-int/lit8 v0, p2, -0x2

    aget-byte v0, p0, v0

    if-nez v0, :cond_f

    aget-byte v0, p0, v4

    if-nez v0, :cond_f

    move v0, v1

    :goto_4
    aput-boolean v0, p3, v1

    .line 53744
    aget-byte v0, p0, v4

    if-nez v0, :cond_12

    :goto_5
    aput-boolean v1, p3, v6

    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 53745
    goto :goto_3

    :cond_b
    if-ne v3, v6, :cond_d

    aget-boolean v0, p3, v6

    if-eqz v0, :cond_c

    add-int/lit8 v0, p2, -0x2

    aget-byte v0, p0, v0

    if-nez v0, :cond_c

    aget-byte v0, p0, v4

    if-ne v0, v1, :cond_c

    move v0, v1

    goto :goto_3

    :cond_c
    move v0, v2

    goto :goto_3

    :cond_d
    aget-boolean v0, p3, v1

    if-eqz v0, :cond_e

    aget-byte v0, p0, v4

    if-ne v0, v1, :cond_e

    move v0, v1

    goto :goto_3

    :cond_e
    move v0, v2

    goto :goto_3

    :cond_f
    move v0, v2

    .line 53746
    goto :goto_4

    :cond_10
    aget-boolean v0, p3, v6

    if-eqz v0, :cond_11

    aget-byte v0, p0, v4

    if-nez v0, :cond_11

    move v0, v1

    goto :goto_4

    :cond_11
    move v0, v2

    goto :goto_4

    :cond_12
    move v1, v2

    .line 53747
    goto :goto_5
.end method

.method public static a(LX/0Oi;)LX/0Og;
    .locals 18

    .prologue
    .line 53636
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0Oi;->c(I)I

    move-result v5

    .line 53637
    const/16 v2, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0Oi;->b(I)V

    .line 53638
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    move-result v3

    .line 53639
    const/4 v4, 0x1

    .line 53640
    const/4 v2, 0x0

    .line 53641
    const/16 v6, 0x64

    if-eq v5, v6, :cond_0

    const/16 v6, 0x6e

    if-eq v5, v6, :cond_0

    const/16 v6, 0x7a

    if-eq v5, v6, :cond_0

    const/16 v6, 0xf4

    if-eq v5, v6, :cond_0

    const/16 v6, 0x2c

    if-eq v5, v6, :cond_0

    const/16 v6, 0x53

    if-eq v5, v6, :cond_0

    const/16 v6, 0x56

    if-eq v5, v6, :cond_0

    const/16 v6, 0x76

    if-eq v5, v6, :cond_0

    const/16 v6, 0x80

    if-eq v5, v6, :cond_0

    const/16 v6, 0x8a

    if-ne v5, v6, :cond_14

    .line 53642
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    move-result v6

    .line 53643
    const/4 v4, 0x3

    if-ne v6, v4, :cond_1

    .line 53644
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->b()Z

    move-result v2

    .line 53645
    :cond_1
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    .line 53646
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    .line 53647
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/0Oi;->b(I)V

    .line 53648
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->b()Z

    move-result v4

    .line 53649
    if-eqz v4, :cond_5

    .line 53650
    const/4 v4, 0x3

    if-eq v6, v4, :cond_3

    const/16 v4, 0x8

    .line 53651
    :goto_0
    const/4 v5, 0x0

    move v7, v5

    :goto_1
    if-ge v7, v4, :cond_5

    .line 53652
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->b()Z

    move-result v5

    .line 53653
    if-eqz v5, :cond_2

    .line 53654
    const/4 v5, 0x6

    if-ge v7, v5, :cond_4

    const/16 v5, 0x10

    :goto_2
    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0Oh;->a(LX/0Oi;I)V

    .line 53655
    :cond_2
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_1

    .line 53656
    :cond_3
    const/16 v4, 0xc

    goto :goto_0

    .line 53657
    :cond_4
    const/16 v5, 0x40

    goto :goto_2

    :cond_5
    move v7, v2

    move v2, v6

    .line 53658
    :goto_3
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    move-result v4

    add-int/lit8 v9, v4, 0x4

    .line 53659
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    move-result v10

    .line 53660
    const/4 v11, 0x0

    .line 53661
    const/4 v12, 0x0

    .line 53662
    if-nez v10, :cond_9

    .line 53663
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    move-result v4

    add-int/lit8 v11, v4, 0x4

    .line 53664
    :cond_6
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    .line 53665
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/0Oi;->b(I)V

    .line 53666
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    move-result v4

    add-int/lit8 v5, v4, 0x1

    .line 53667
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    move-result v4

    add-int/lit8 v6, v4, 0x1

    .line 53668
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->b()Z

    move-result v8

    .line 53669
    if-eqz v8, :cond_a

    const/4 v4, 0x1

    :goto_4
    rsub-int/lit8 v4, v4, 0x2

    mul-int/2addr v4, v6

    .line 53670
    if-nez v8, :cond_7

    .line 53671
    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/0Oi;->b(I)V

    .line 53672
    :cond_7
    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/0Oi;->b(I)V

    .line 53673
    mul-int/lit8 v6, v5, 0x10

    .line 53674
    mul-int/lit8 v5, v4, 0x10

    .line 53675
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->b()Z

    move-result v4

    .line 53676
    if-eqz v4, :cond_13

    .line 53677
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    move-result v14

    .line 53678
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    move-result v15

    .line 53679
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    move-result v16

    .line 53680
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    move-result v17

    .line 53681
    if-nez v2, :cond_c

    .line 53682
    const/4 v4, 0x1

    .line 53683
    if-eqz v8, :cond_b

    const/4 v2, 0x1

    :goto_5
    rsub-int/lit8 v2, v2, 0x2

    .line 53684
    :goto_6
    add-int v13, v14, v15

    mul-int/2addr v4, v13

    sub-int v4, v6, v4

    .line 53685
    add-int v6, v16, v17

    mul-int/2addr v2, v6

    sub-int/2addr v5, v2

    .line 53686
    :goto_7
    const/high16 v2, 0x3f800000    # 1.0f

    .line 53687
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->b()Z

    move-result v6

    .line 53688
    if-eqz v6, :cond_12

    .line 53689
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->b()Z

    move-result v6

    .line 53690
    if-eqz v6, :cond_12

    .line 53691
    const/16 v6, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/0Oi;->c(I)I

    move-result v6

    .line 53692
    const/16 v13, 0xff

    if-ne v6, v13, :cond_10

    .line 53693
    const/16 v6, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/0Oi;->c(I)I

    move-result v6

    .line 53694
    const/16 v13, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, LX/0Oi;->c(I)I

    move-result v13

    .line 53695
    if-eqz v6, :cond_8

    if-eqz v13, :cond_8

    .line 53696
    int-to-float v2, v6

    int-to-float v6, v13

    div-float/2addr v2, v6

    :cond_8
    move v6, v2

    .line 53697
    :goto_8
    new-instance v2, LX/0Og;

    invoke-direct/range {v2 .. v12}, LX/0Og;-><init>(IIIFZZIIIZ)V

    return-object v2

    .line 53698
    :cond_9
    const/4 v4, 0x1

    if-ne v10, v4, :cond_6

    .line 53699
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->b()Z

    move-result v12

    .line 53700
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->e()I

    .line 53701
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->e()I

    .line 53702
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    move-result v4

    int-to-long v14, v4

    .line 53703
    const/4 v4, 0x0

    :goto_9
    int-to-long v0, v4

    move-wide/from16 v16, v0

    cmp-long v5, v16, v14

    if-gez v5, :cond_6

    .line 53704
    invoke-virtual/range {p0 .. p0}, LX/0Oi;->d()I

    .line 53705
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 53706
    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 53707
    :cond_b
    const/4 v2, 0x0

    goto :goto_5

    .line 53708
    :cond_c
    const/4 v4, 0x3

    if-ne v2, v4, :cond_d

    const/4 v4, 0x1

    .line 53709
    :goto_a
    const/4 v13, 0x1

    if-ne v2, v13, :cond_e

    const/4 v2, 0x2

    move v13, v2

    .line 53710
    :goto_b
    if-eqz v8, :cond_f

    const/4 v2, 0x1

    :goto_c
    rsub-int/lit8 v2, v2, 0x2

    mul-int/2addr v2, v13

    goto :goto_6

    .line 53711
    :cond_d
    const/4 v4, 0x2

    goto :goto_a

    .line 53712
    :cond_e
    const/4 v2, 0x1

    move v13, v2

    goto :goto_b

    .line 53713
    :cond_f
    const/4 v2, 0x0

    goto :goto_c

    .line 53714
    :cond_10
    sget-object v13, LX/0Oh;->b:[F

    array-length v13, v13

    if-ge v6, v13, :cond_11

    .line 53715
    sget-object v2, LX/0Oh;->b:[F

    aget v2, v2, v6

    move v6, v2

    goto :goto_8

    .line 53716
    :cond_11
    const-string v13, "NalUnitUtil"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Unexpected aspect_ratio_idc value: "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v13, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_12
    move v6, v2

    goto :goto_8

    :cond_13
    move v4, v6

    goto/16 :goto_7

    :cond_14
    move v7, v2

    move v2, v4

    goto/16 :goto_3
.end method

.method private static a(LX/0Oi;I)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 53628
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    :goto_0
    if-ge v2, p1, :cond_2

    .line 53629
    if-eqz v1, :cond_0

    .line 53630
    invoke-virtual {p0}, LX/0Oi;->e()I

    move-result v1

    .line 53631
    add-int/2addr v1, v0

    add-int/lit16 v1, v1, 0x100

    rem-int/lit16 v1, v1, 0x100

    .line 53632
    :cond_0
    if-nez v1, :cond_1

    .line 53633
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 53634
    goto :goto_1

    .line 53635
    :cond_2
    return-void
.end method

.method public static a([Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53624
    aput-boolean v1, p0, v1

    .line 53625
    const/4 v0, 0x1

    aput-boolean v1, p0, v0

    .line 53626
    const/4 v0, 0x2

    aput-boolean v1, p0, v0

    .line 53627
    return-void
.end method

.method public static a(LX/0Oj;)[B
    .locals 6

    .prologue
    .line 53615
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v0

    .line 53616
    iget v1, p0, LX/0Oj;->b:I

    move v1, v1

    .line 53617
    invoke-virtual {p0, v0}, LX/0Oj;->c(I)V

    .line 53618
    iget-object v2, p0, LX/0Oj;->a:[B

    const/4 p0, 0x0

    .line 53619
    sget-object v3, LX/0OY;->a:[B

    array-length v3, v3

    add-int/2addr v3, v0

    new-array v3, v3, [B

    .line 53620
    sget-object v4, LX/0OY;->a:[B

    sget-object v5, LX/0OY;->a:[B

    array-length v5, v5

    invoke-static {v4, p0, v3, p0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 53621
    sget-object v4, LX/0OY;->a:[B

    array-length v4, v4

    invoke-static {v2, v1, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 53622
    move-object v0, v3

    .line 53623
    return-object v0
.end method

.method public static b(LX/0Oi;)LX/0Of;
    .locals 4

    .prologue
    .line 53610
    invoke-virtual {p0}, LX/0Oi;->d()I

    move-result v0

    .line 53611
    invoke-virtual {p0}, LX/0Oi;->d()I

    move-result v1

    .line 53612
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/0Oi;->b(I)V

    .line 53613
    invoke-virtual {p0}, LX/0Oi;->b()Z

    move-result v2

    .line 53614
    new-instance v3, LX/0Of;

    invoke-direct {v3, v0, v1, v2}, LX/0Of;-><init>(IIZ)V

    return-object v3
.end method
