.class public LX/0GE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0GB;

.field public final d:LX/0GN;

.field private final e:LX/0Gh;

.field public f:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "LX/0GK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34553
    const-class v0, LX/0GE;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0GE;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILandroid/content/Context;LX/0Gh;)V
    .locals 2

    .prologue
    .line 34546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34547
    new-instance v0, Landroid/util/LruCache;

    invoke-direct {v0, p1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/0GE;->f:Landroid/util/LruCache;

    .line 34548
    iput-object p2, p0, LX/0GE;->b:Landroid/content/Context;

    .line 34549
    new-instance v0, LX/0GB;

    invoke-direct {v0}, LX/0GB;-><init>()V

    iput-object v0, p0, LX/0GE;->c:LX/0GB;

    .line 34550
    new-instance v0, LX/0GN;

    iget-object v1, p0, LX/0GE;->c:LX/0GB;

    invoke-direct {v0, p2, v1}, LX/0GN;-><init>(Landroid/content/Context;LX/0GB;)V

    iput-object v0, p0, LX/0GE;->d:LX/0GN;

    .line 34551
    iput-object p3, p0, LX/0GE;->e:LX/0Gh;

    .line 34552
    return-void
.end method

.method private declared-synchronized a(I)V
    .locals 2

    .prologue
    .line 34543
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0GE;->f:Landroid/util/LruCache;

    const-string v1, "DashLiveChunkSourceCache"

    invoke-static {v0, p1, v1}, LX/0Gj;->a(Landroid/util/LruCache;ILjava/lang/String;)Landroid/util/LruCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34544
    monitor-exit p0

    return-void

    .line 34545
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(LX/0GE;Ljava/util/Map;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34452
    invoke-static {p1}, LX/040;->w(Ljava/util/Map;)I

    move-result v12

    .line 34453
    if-nez v12, :cond_0

    .line 34454
    :goto_0
    return-void

    .line 34455
    :cond_0
    iget-object v0, p0, LX/0GE;->c:LX/0GB;

    invoke-static {p1}, LX/040;->x(Ljava/util/Map;)I

    move-result v1

    invoke-virtual {v0, v1, v12}, LX/0GB;->a(II)V

    .line 34456
    iget-object v0, p0, LX/0GE;->d:LX/0GN;

    invoke-static {p1}, LX/040;->an(Ljava/util/Map;)Z

    move-result v1

    const/4 v3, 0x0

    .line 34457
    sget-object v2, LX/040;->at:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 34458
    sget-object v2, LX/040;->at:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    .line 34459
    :goto_1
    move v2, v2

    .line 34460
    invoke-static {p1}, LX/040;->ap(Ljava/util/Map;)I

    move-result v3

    invoke-static {p1}, LX/040;->aq(Ljava/util/Map;)I

    move-result v4

    .line 34461
    sget-object v5, LX/040;->aw:Ljava/lang/String;

    invoke-interface {p1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 34462
    sget-object v5, LX/040;->aw:Ljava/lang/String;

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 34463
    :goto_2
    move v5, v5

    .line 34464
    const/4 v7, 0x0

    .line 34465
    sget-object v6, LX/040;->ax:Ljava/lang/String;

    invoke-interface {p1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 34466
    sget-object v6, LX/040;->ax:Ljava/lang/String;

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_4

    const/4 v6, 0x1

    .line 34467
    :goto_3
    move v6, v6

    .line 34468
    invoke-static {p1}, LX/040;->h(Ljava/util/Map;)I

    move-result v7

    invoke-static {p1}, LX/040;->i(Ljava/util/Map;)I

    move-result v8

    const/4 v10, 0x0

    .line 34469
    sget-object v9, LX/040;->az:Ljava/lang/String;

    invoke-interface {p1, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 34470
    sget-object v9, LX/040;->az:Ljava/lang/String;

    invoke-interface {p1, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    if-eqz v9, :cond_6

    const/4 v9, 0x1

    .line 34471
    :goto_4
    move v9, v9

    .line 34472
    invoke-static {p1}, LX/040;->p(Ljava/util/Map;)Z

    move-result v10

    const/4 v13, 0x0

    .line 34473
    sget-object v11, LX/040;->s:Ljava/lang/String;

    invoke-interface {p1, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 34474
    sget-object v11, LX/040;->s:Ljava/lang/String;

    invoke-interface {p1, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    if-eqz v11, :cond_8

    const/4 v11, 0x1

    .line 34475
    :goto_5
    move v11, v11

    .line 34476
    const/16 v13, 0x2710

    .line 34477
    iput-boolean v1, v0, LX/0GN;->i:Z

    .line 34478
    iput-boolean v2, v0, LX/0GN;->j:Z

    .line 34479
    if-lez v3, :cond_a

    :goto_6
    iput v3, v0, LX/0GN;->k:I

    .line 34480
    if-lez v4, :cond_b

    :goto_7
    iput v4, v0, LX/0GN;->l:I

    .line 34481
    if-lez v5, :cond_c

    :goto_8
    iput v5, v0, LX/0GN;->m:I

    .line 34482
    iput-boolean v6, v0, LX/0GN;->n:Z

    .line 34483
    iput v7, v0, LX/0GN;->o:I

    .line 34484
    iput v8, v0, LX/0GN;->p:I

    .line 34485
    iput-boolean v9, v0, LX/0GN;->q:Z

    .line 34486
    iput-boolean v10, v0, LX/0GN;->r:Z

    .line 34487
    iput-boolean v11, v0, LX/0GN;->s:Z

    .line 34488
    invoke-direct {p0, v12}, LX/0GE;->a(I)V

    .line 34489
    invoke-static {p1}, LX/040;->x(Ljava/util/Map;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    .line 34490
    mul-int v1, v12, v0

    invoke-static {p1}, LX/040;->ap(Ljava/util/Map;)I

    move-result v2

    invoke-static {p1}, LX/040;->aq(Ljava/util/Map;)I

    move-result v3

    invoke-static {v1, v0, v2, v3}, LX/0Gv;->a(IIII)V

    goto/16 :goto_0

    :cond_1
    move v2, v3

    .line 34491
    goto/16 :goto_1

    :cond_2
    move v2, v3

    .line 34492
    goto/16 :goto_1

    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_2

    :cond_4
    move v6, v7

    .line 34493
    goto :goto_3

    :cond_5
    move v6, v7

    .line 34494
    goto/16 :goto_3

    :cond_6
    move v9, v10

    .line 34495
    goto :goto_4

    :cond_7
    move v9, v10

    .line 34496
    goto :goto_4

    :cond_8
    move v11, v13

    .line 34497
    goto :goto_5

    :cond_9
    move v11, v13

    .line 34498
    goto :goto_5

    :cond_a
    move v3, v13

    .line 34499
    goto :goto_6

    :cond_b
    move v4, v13

    .line 34500
    goto :goto_7

    .line 34501
    :cond_c
    const/4 v5, 0x3

    goto :goto_8
.end method


# virtual methods
.method public final a()LX/0GB;
    .locals 1

    .prologue
    .line 34542
    iget-object v0, p0, LX/0GE;->c:LX/0GB;

    return-object v0
.end method

.method public final declared-synchronized a(Landroid/net/Uri;Landroid/os/Handler;Ljava/lang/String;Landroid/net/Uri;Ljava/util/Map;LX/0Gk;LX/0AY;IZLjava/lang/String;I)LX/0GK;
    .locals 19
    .param p6    # LX/0Gk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Landroid/os/Handler;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Gk;",
            "LX/0AY;",
            "IZ",
            "Ljava/lang/String;",
            "I)",
            "LX/0GK;"
        }
    .end annotation

    .prologue
    .line 34527
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-static {v0, v1}, LX/0GE;->a(LX/0GE;Ljava/util/Map;)V

    .line 34528
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0GE;->f:Landroid/util/LruCache;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0GK;

    .line 34529
    if-eqz v2, :cond_2

    .line 34530
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Returning existing dash live manifest fetcher: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 34531
    invoke-static/range {p5 .. p5}, LX/040;->y(Ljava/util/Map;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 34532
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0GE;->f:Landroid/util/LruCache;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34533
    :cond_0
    invoke-virtual/range {p10 .. p10}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 34534
    move-object/from16 v0, p10

    invoke-virtual {v2, v0}, LX/0GK;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34535
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v2

    .line 34536
    :cond_2
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Allocate new dash live manifest fetcher: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 34537
    new-instance v2, LX/0GK;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0GE;->b:Landroid/content/Context;

    invoke-virtual/range {p10 .. p10}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v8, "default"

    :goto_1
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, LX/0GE;->d:LX/0GN;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, LX/0GE;->c:LX/0GB;

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p3

    move-object/from16 v10, p5

    move-object/from16 v12, p6

    move-object/from16 v13, p7

    move/from16 v16, p8

    move/from16 v17, p9

    move/from16 v18, p11

    invoke-direct/range {v2 .. v18}, LX/0GK;-><init>(Landroid/net/Uri;Landroid/content/Context;Landroid/os/Handler;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;LX/0GN;LX/0Gk;LX/0AY;ZLX/0GB;IZI)V

    .line 34538
    invoke-static/range {p5 .. p5}, LX/040;->y(Ljava/util/Map;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 34539
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0GE;->f:Landroid/util/LruCache;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 34540
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_3
    move-object/from16 v8, p10

    .line 34541
    goto :goto_1
.end method

.method public final declared-synchronized a(Landroid/net/Uri;Landroid/os/Handler;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;LX/0Gk;Ljava/lang/String;II)V
    .locals 19
    .param p7    # LX/0Gk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Landroid/os/Handler;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Gk;",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 34506
    monitor-enter p0

    :try_start_0
    invoke-static/range {p6 .. p6}, LX/040;->z(Ljava/util/Map;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0GE;->d:LX/0GN;

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, LX/0GN;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 34507
    sget-object v2, LX/0GE;->a:Ljava/lang/String;

    const-string v3, "Prefetch is disbaled for origin: %s, videoId: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p5, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    invoke-static {v2, v3, v4}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34508
    :goto_0
    monitor-exit p0

    return-void

    .line 34509
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-static {v0, v1}, LX/0GE;->a(LX/0GE;Ljava/util/Map;)V

    .line 34510
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0GE;->f:Landroid/util/LruCache;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0GK;

    .line 34511
    if-eqz v2, :cond_2

    .line 34512
    invoke-virtual {v2}, LX/0GK;->g()LX/0GH;

    move-result-object v3

    .line 34513
    sget-object v4, LX/0GH;->PREPARING:LX/0GH;

    if-eq v3, v4, :cond_1

    sget-object v4, LX/0GH;->PREPARED:LX/0GH;

    if-ne v3, v4, :cond_4

    .line 34514
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Prefetch is already under way "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 34515
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 34516
    :cond_2
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Allocate new prefetch entry: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", videoServerUri"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 34517
    const/4 v13, 0x0

    .line 34518
    invoke-static/range {p6 .. p6}, LX/040;->ai(Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 34519
    new-instance v2, LX/0AZ;

    invoke-direct {v2}, LX/0AZ;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-static {v2, v3, v0}, LX/0Gj;->a(LX/0AZ;Ljava/lang/String;Ljava/lang/String;)LX/0AY;

    move-result-object v13

    .line 34520
    :cond_3
    new-instance v2, LX/0GK;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0GE;->b:Landroid/content/Context;

    invoke-static/range {p6 .. p6}, LX/040;->D(Ljava/util/Map;)Z

    move-result v9

    move-object/from16 v0, p0

    iget-object v11, v0, LX/0GE;->d:LX/0GN;

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-object v15, v0, LX/0GE;->c:LX/0GB;

    const/16 v17, 0x0

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p3

    move-object/from16 v8, p5

    move-object/from16 v10, p6

    move-object/from16 v12, p7

    move/from16 v16, p9

    move/from16 v18, p10

    invoke-direct/range {v2 .. v18}, LX/0GK;-><init>(Landroid/net/Uri;Landroid/content/Context;Landroid/os/Handler;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;LX/0GN;LX/0Gk;LX/0AY;ZLX/0GB;IZI)V

    .line 34521
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0GE;->f:Landroid/util/LruCache;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34522
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Start loading dash live manifest: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 34523
    invoke-static/range {p6 .. p6}, LX/040;->I(Ljava/util/Map;)I

    move-result v3

    .line 34524
    if-lez v3, :cond_5

    .line 34525
    invoke-virtual {v2, v3}, LX/0GK;->a(I)V

    .line 34526
    :cond_5
    invoke-virtual {v2}, LX/0GK;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public final declared-synchronized a(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;Landroid/os/Handler;Landroid/net/Uri;Ljava/util/Map;LX/0Gk;II)V
    .locals 9
    .param p5    # LX/0Gk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;",
            "Landroid/os/Handler;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Gk;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 34502
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/0GD;

    move-object v1, p1

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, LX/0GD;-><init>(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;LX/0GE;Landroid/os/Handler;Landroid/net/Uri;Ljava/util/Map;LX/0Gk;II)V

    .line 34503
    iget-object v1, p0, LX/0GE;->e:LX/0Gh;

    invoke-virtual {v1, v0}, LX/0Gh;->a(LX/0GC;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34504
    monitor-exit p0

    return-void

    .line 34505
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
