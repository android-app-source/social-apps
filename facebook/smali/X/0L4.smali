.class public final LX/0L4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field public final e:J

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field public final g:Z

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:F

.field public final n:I

.field public final o:I

.field public final p:I

.field public final q:I

.field public final r:Ljava/lang/String;

.field public final s:J

.field private t:I

.field private u:Landroid/media/MediaFormat;

.field private v:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIJIIIFII",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List",
            "<[B>;ZIIII)V"
        }
    .end annotation

    .prologue
    .line 42907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42908
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/0L4;->v:Z

    .line 42909
    iput-object p1, p0, LX/0L4;->a:Ljava/lang/String;

    .line 42910
    invoke-static {p2}, LX/0Av;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/0L4;->b:Ljava/lang/String;

    .line 42911
    iput p3, p0, LX/0L4;->c:I

    .line 42912
    iput p4, p0, LX/0L4;->d:I

    .line 42913
    iput-wide p5, p0, LX/0L4;->e:J

    .line 42914
    iput p7, p0, LX/0L4;->h:I

    .line 42915
    iput p8, p0, LX/0L4;->i:I

    .line 42916
    iput p9, p0, LX/0L4;->l:I

    .line 42917
    iput p10, p0, LX/0L4;->m:F

    .line 42918
    iput p11, p0, LX/0L4;->n:I

    .line 42919
    iput p12, p0, LX/0L4;->o:I

    .line 42920
    move-object/from16 v0, p13

    iput-object v0, p0, LX/0L4;->r:Ljava/lang/String;

    .line 42921
    move-wide/from16 v0, p14

    iput-wide v0, p0, LX/0L4;->s:J

    .line 42922
    if-nez p16, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p16

    :cond_0
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0L4;->f:Ljava/util/List;

    .line 42923
    move/from16 v0, p17

    iput-boolean v0, p0, LX/0L4;->g:Z

    .line 42924
    move/from16 v0, p18

    iput v0, p0, LX/0L4;->j:I

    .line 42925
    move/from16 v0, p19

    iput v0, p0, LX/0L4;->k:I

    .line 42926
    move/from16 v0, p20

    iput v0, p0, LX/0L4;->p:I

    .line 42927
    move/from16 v0, p21

    iput v0, p0, LX/0L4;->q:I

    .line 42928
    return-void
.end method

.method public static a()LX/0L4;
    .locals 6

    .prologue
    .line 43008
    const/4 v0, 0x0

    const-string v1, "application/id3"

    const/4 v2, -0x1

    const-wide/16 v4, -0x1

    invoke-static {v0, v1, v2, v4, v5}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IJ)LX/0L4;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;)LX/0L4;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIJII",
            "Ljava/util/List",
            "<[B>;)",
            "LX/0L4;"
        }
    .end annotation

    .prologue
    .line 43007
    const/4 v9, -0x1

    const/high16 v10, -0x40800000    # -1.0f

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-static/range {v0 .. v10}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)LX/0L4;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)LX/0L4;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIJII",
            "Ljava/util/List",
            "<[B>;IF)",
            "LX/0L4;"
        }
    .end annotation

    .prologue
    .line 43006
    new-instance v0, LX/0L4;

    const/4 v11, -0x1

    const/4 v12, -0x1

    const/4 v13, 0x0

    const-wide v14, 0x7fffffffffffffffL

    const/16 v17, 0x0

    const/16 v18, -0x1

    const/16 v19, -0x1

    const/16 v20, -0x1

    const/16 v21, -0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-wide/from16 v5, p4

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p9

    move/from16 v10, p10

    move-object/from16 v16, p8

    invoke-direct/range {v0 .. v21}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIJII",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/String;",
            ")",
            "LX/0L4;"
        }
    .end annotation

    .prologue
    .line 43005
    new-instance v0, LX/0L4;

    const/4 v7, -0x1

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/high16 v10, -0x40800000    # -1.0f

    const-wide v14, 0x7fffffffffffffffL

    const/16 v17, 0x0

    const/16 v18, -0x1

    const/16 v19, -0x1

    const/16 v20, -0x1

    const/16 v21, -0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-wide/from16 v5, p4

    move/from16 v11, p6

    move/from16 v12, p7

    move-object/from16 v13, p9

    move-object/from16 v16, p8

    invoke-direct/range {v0 .. v21}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;IJ)LX/0L4;
    .locals 23

    .prologue
    .line 43004
    new-instance v0, LX/0L4;

    const/4 v4, -0x1

    const/4 v7, -0x1

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/high16 v10, -0x40800000    # -1.0f

    const/4 v11, -0x1

    const/4 v12, -0x1

    const/4 v13, 0x0

    const-wide v14, 0x7fffffffffffffffL

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, -0x1

    const/16 v19, -0x1

    const/16 v20, -0x1

    const/16 v21, -0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move-wide/from16 v5, p3

    invoke-direct/range {v0 .. v21}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)LX/0L4;
    .locals 9

    .prologue
    .line 43003
    const-wide v6, 0x7fffffffffffffffL

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-wide v3, p3

    move-object v5, p5

    invoke-static/range {v0 .. v7}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;J)LX/0L4;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;J)LX/0L4;
    .locals 22

    .prologue
    .line 43002
    new-instance v0, LX/0L4;

    const/4 v4, -0x1

    const/4 v7, -0x1

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/high16 v10, -0x40800000    # -1.0f

    const/4 v11, -0x1

    const/4 v12, -0x1

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, -0x1

    const/16 v19, -0x1

    const/16 v20, -0x1

    const/16 v21, -0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move-wide/from16 v5, p3

    move-object/from16 v13, p5

    move-wide/from16 v14, p6

    invoke-direct/range {v0 .. v21}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;IJLjava/util/List;Ljava/lang/String;)LX/0L4;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IJ",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/String;",
            ")",
            "LX/0L4;"
        }
    .end annotation

    .prologue
    .line 43001
    new-instance v0, LX/0L4;

    const/4 v4, -0x1

    const/4 v7, -0x1

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/high16 v10, -0x40800000    # -1.0f

    const/4 v11, -0x1

    const/4 v12, -0x1

    const-wide v14, 0x7fffffffffffffffL

    const/16 v17, 0x0

    const/16 v18, -0x1

    const/16 v19, -0x1

    const/16 v20, -0x1

    const/16 v21, -0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move-wide/from16 v5, p3

    move-object/from16 v13, p6

    move-object/from16 v16, p5

    invoke-direct/range {v0 .. v21}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v0
.end method

.method private static final a(Landroid/media/MediaFormat;Ljava/lang/String;I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 42998
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 42999
    invoke-virtual {p0, p1, p2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 43000
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)LX/0L4;
    .locals 23

    .prologue
    .line 42997
    new-instance v1, LX/0L4;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0L4;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0L4;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, LX/0L4;->c:I

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/0L4;->e:J

    move-object/from16 v0, p0

    iget v8, v0, LX/0L4;->h:I

    move-object/from16 v0, p0

    iget v9, v0, LX/0L4;->i:I

    move-object/from16 v0, p0

    iget v10, v0, LX/0L4;->l:I

    move-object/from16 v0, p0

    iget v11, v0, LX/0L4;->m:F

    move-object/from16 v0, p0

    iget v12, v0, LX/0L4;->n:I

    move-object/from16 v0, p0

    iget v13, v0, LX/0L4;->o:I

    move-object/from16 v0, p0

    iget-object v14, v0, LX/0L4;->r:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v15, v0, LX/0L4;->s:J

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0L4;->f:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/0L4;->g:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->j:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->k:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->p:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->q:I

    move/from16 v22, v0

    move/from16 v5, p1

    invoke-direct/range {v1 .. v22}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v1
.end method

.method public final a(II)LX/0L4;
    .locals 23

    .prologue
    .line 43009
    new-instance v1, LX/0L4;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0L4;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0L4;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, LX/0L4;->c:I

    move-object/from16 v0, p0

    iget v5, v0, LX/0L4;->d:I

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/0L4;->e:J

    move-object/from16 v0, p0

    iget v8, v0, LX/0L4;->h:I

    move-object/from16 v0, p0

    iget v9, v0, LX/0L4;->i:I

    move-object/from16 v0, p0

    iget v10, v0, LX/0L4;->l:I

    move-object/from16 v0, p0

    iget v11, v0, LX/0L4;->m:F

    move-object/from16 v0, p0

    iget v12, v0, LX/0L4;->n:I

    move-object/from16 v0, p0

    iget v13, v0, LX/0L4;->o:I

    move-object/from16 v0, p0

    iget-object v14, v0, LX/0L4;->r:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v15, v0, LX/0L4;->s:J

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0L4;->f:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/0L4;->g:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->p:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->q:I

    move/from16 v22, v0

    move/from16 v19, p1

    move/from16 v20, p2

    invoke-direct/range {v1 .. v22}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v1
.end method

.method public final a(J)LX/0L4;
    .locals 23

    .prologue
    .line 42996
    new-instance v1, LX/0L4;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0L4;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0L4;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, LX/0L4;->c:I

    move-object/from16 v0, p0

    iget v5, v0, LX/0L4;->d:I

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/0L4;->e:J

    move-object/from16 v0, p0

    iget v8, v0, LX/0L4;->h:I

    move-object/from16 v0, p0

    iget v9, v0, LX/0L4;->i:I

    move-object/from16 v0, p0

    iget v10, v0, LX/0L4;->l:I

    move-object/from16 v0, p0

    iget v11, v0, LX/0L4;->m:F

    move-object/from16 v0, p0

    iget v12, v0, LX/0L4;->n:I

    move-object/from16 v0, p0

    iget v13, v0, LX/0L4;->o:I

    move-object/from16 v0, p0

    iget-object v14, v0, LX/0L4;->r:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0L4;->f:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/0L4;->g:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->j:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->k:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->p:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->q:I

    move/from16 v22, v0

    move-wide/from16 v15, p1

    invoke-direct/range {v1 .. v22}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;)LX/0L4;
    .locals 23

    .prologue
    .line 42995
    new-instance v1, LX/0L4;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0L4;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0L4;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, LX/0L4;->c:I

    move-object/from16 v0, p0

    iget v5, v0, LX/0L4;->d:I

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/0L4;->e:J

    move-object/from16 v0, p0

    iget v8, v0, LX/0L4;->h:I

    move-object/from16 v0, p0

    iget v9, v0, LX/0L4;->i:I

    move-object/from16 v0, p0

    iget v10, v0, LX/0L4;->l:I

    move-object/from16 v0, p0

    iget v11, v0, LX/0L4;->m:F

    move-object/from16 v0, p0

    iget v12, v0, LX/0L4;->n:I

    move-object/from16 v0, p0

    iget v13, v0, LX/0L4;->o:I

    move-object/from16 v0, p0

    iget-wide v15, v0, LX/0L4;->s:J

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0L4;->f:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/0L4;->g:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->j:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->k:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->p:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->q:I

    move/from16 v22, v0

    move-object/from16 v14, p1

    invoke-direct/range {v1 .. v22}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;IIILjava/lang/String;)LX/0L4;
    .locals 23

    .prologue
    .line 42994
    new-instance v1, LX/0L4;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0L4;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, LX/0L4;->d:I

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/0L4;->e:J

    move-object/from16 v0, p0

    iget v10, v0, LX/0L4;->l:I

    move-object/from16 v0, p0

    iget v11, v0, LX/0L4;->m:F

    move-object/from16 v0, p0

    iget v12, v0, LX/0L4;->n:I

    move-object/from16 v0, p0

    iget v13, v0, LX/0L4;->o:I

    move-object/from16 v0, p0

    iget-wide v15, v0, LX/0L4;->s:J

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0L4;->f:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/0L4;->g:Z

    move/from16 v18, v0

    const/16 v19, -0x1

    const/16 v20, -0x1

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->p:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->q:I

    move/from16 v22, v0

    move-object/from16 v2, p1

    move/from16 v4, p2

    move/from16 v8, p3

    move/from16 v9, p4

    move-object/from16 v14, p5

    invoke-direct/range {v1 .. v22}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v1
.end method

.method public final b(II)LX/0L4;
    .locals 23

    .prologue
    .line 42993
    new-instance v1, LX/0L4;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0L4;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0L4;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, LX/0L4;->c:I

    move-object/from16 v0, p0

    iget v5, v0, LX/0L4;->d:I

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/0L4;->e:J

    move-object/from16 v0, p0

    iget v8, v0, LX/0L4;->h:I

    move-object/from16 v0, p0

    iget v9, v0, LX/0L4;->i:I

    move-object/from16 v0, p0

    iget v10, v0, LX/0L4;->l:I

    move-object/from16 v0, p0

    iget v11, v0, LX/0L4;->m:F

    move-object/from16 v0, p0

    iget v12, v0, LX/0L4;->n:I

    move-object/from16 v0, p0

    iget v13, v0, LX/0L4;->o:I

    move-object/from16 v0, p0

    iget-object v14, v0, LX/0L4;->r:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v15, v0, LX/0L4;->s:J

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0L4;->f:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/0L4;->g:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->j:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->k:I

    move/from16 v20, v0

    move/from16 v21, p1

    move/from16 v22, p2

    invoke-direct/range {v1 .. v22}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v1
.end method

.method public final b(J)LX/0L4;
    .locals 23

    .prologue
    .line 42992
    new-instance v1, LX/0L4;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0L4;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0L4;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, LX/0L4;->c:I

    move-object/from16 v0, p0

    iget v5, v0, LX/0L4;->d:I

    move-object/from16 v0, p0

    iget v8, v0, LX/0L4;->h:I

    move-object/from16 v0, p0

    iget v9, v0, LX/0L4;->i:I

    move-object/from16 v0, p0

    iget v10, v0, LX/0L4;->l:I

    move-object/from16 v0, p0

    iget v11, v0, LX/0L4;->m:F

    move-object/from16 v0, p0

    iget v12, v0, LX/0L4;->n:I

    move-object/from16 v0, p0

    iget v13, v0, LX/0L4;->o:I

    move-object/from16 v0, p0

    iget-object v14, v0, LX/0L4;->r:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v15, v0, LX/0L4;->s:J

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0L4;->f:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/0L4;->g:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->j:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->k:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->p:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->q:I

    move/from16 v22, v0

    move-wide/from16 v6, p1

    invoke-direct/range {v1 .. v22}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v1
.end method

.method public final b(Ljava/lang/String;)LX/0L4;
    .locals 23

    .prologue
    .line 42991
    new-instance v1, LX/0L4;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0L4;->b:Ljava/lang/String;

    const/4 v4, -0x1

    const/4 v5, -0x1

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/0L4;->e:J

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/4 v10, -0x1

    const/high16 v11, -0x40800000    # -1.0f

    const/4 v12, -0x1

    const/4 v13, -0x1

    const/4 v14, 0x0

    const-wide v15, 0x7fffffffffffffffL

    const/16 v17, 0x0

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->j:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0L4;->k:I

    move/from16 v20, v0

    const/16 v21, -0x1

    const/16 v22, -0x1

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v22}, LX/0L4;-><init>(Ljava/lang/String;Ljava/lang/String;IIJIIIFIILjava/lang/String;JLjava/util/List;ZIIII)V

    return-object v1
.end method

.method public final b()Landroid/media/MediaFormat;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 42966
    iget-object v0, p0, LX/0L4;->u:Landroid/media/MediaFormat;

    if-nez v0, :cond_4

    .line 42967
    new-instance v2, Landroid/media/MediaFormat;

    invoke-direct {v2}, Landroid/media/MediaFormat;-><init>()V

    .line 42968
    const-string v0, "mime"

    iget-object v1, p0, LX/0L4;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 42969
    const-string v0, "language"

    iget-object v1, p0, LX/0L4;->r:Ljava/lang/String;

    .line 42970
    if-eqz v1, :cond_0

    .line 42971
    invoke-virtual {v2, v0, v1}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 42972
    :cond_0
    const-string v0, "max-input-size"

    iget v1, p0, LX/0L4;->d:I

    invoke-static {v2, v0, v1}, LX/0L4;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 42973
    const-string v0, "width"

    iget v1, p0, LX/0L4;->h:I

    invoke-static {v2, v0, v1}, LX/0L4;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 42974
    const-string v0, "height"

    iget v1, p0, LX/0L4;->i:I

    invoke-static {v2, v0, v1}, LX/0L4;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 42975
    const-string v0, "rotation-degrees"

    iget v1, p0, LX/0L4;->l:I

    invoke-static {v2, v0, v1}, LX/0L4;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 42976
    const-string v0, "max-width"

    iget v1, p0, LX/0L4;->j:I

    invoke-static {v2, v0, v1}, LX/0L4;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 42977
    const-string v0, "max-height"

    iget v1, p0, LX/0L4;->k:I

    invoke-static {v2, v0, v1}, LX/0L4;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 42978
    const-string v0, "channel-count"

    iget v1, p0, LX/0L4;->n:I

    invoke-static {v2, v0, v1}, LX/0L4;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 42979
    const-string v0, "sample-rate"

    iget v1, p0, LX/0L4;->o:I

    invoke-static {v2, v0, v1}, LX/0L4;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 42980
    const-string v0, "encoder-delay"

    iget v1, p0, LX/0L4;->p:I

    invoke-static {v2, v0, v1}, LX/0L4;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 42981
    const-string v0, "encoder-padding"

    iget v1, p0, LX/0L4;->q:I

    invoke-static {v2, v0, v1}, LX/0L4;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 42982
    iget-boolean v0, p0, LX/0L4;->v:Z

    if-eqz v0, :cond_1

    .line 42983
    const-string v0, "is-adts"

    const/4 v1, 0x1

    invoke-static {v2, v0, v1}, LX/0L4;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 42984
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0L4;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 42985
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "csd-"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/0L4;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/media/MediaFormat;->setByteBuffer(Ljava/lang/String;Ljava/nio/ByteBuffer;)V

    .line 42986
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 42987
    :cond_2
    iget-wide v0, p0, LX/0L4;->e:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 42988
    const-string v0, "durationUs"

    iget-wide v4, p0, LX/0L4;->e:J

    invoke-virtual {v2, v0, v4, v5}, Landroid/media/MediaFormat;->setLong(Ljava/lang/String;J)V

    .line 42989
    :cond_3
    iput-object v2, p0, LX/0L4;->u:Landroid/media/MediaFormat;

    .line 42990
    :cond_4
    iget-object v0, p0, LX/0L4;->u:Landroid/media/MediaFormat;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42957
    if-ne p0, p1, :cond_1

    move v3, v4

    .line 42958
    :cond_0
    :goto_0
    return v3

    .line 42959
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 42960
    check-cast p1, LX/0L4;

    .line 42961
    iget-boolean v0, p0, LX/0L4;->g:Z

    iget-boolean v1, p1, LX/0L4;->g:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/0L4;->c:I

    iget v1, p1, LX/0L4;->c:I

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/0L4;->d:I

    iget v1, p1, LX/0L4;->d:I

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/0L4;->h:I

    iget v1, p1, LX/0L4;->h:I

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/0L4;->i:I

    iget v1, p1, LX/0L4;->i:I

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/0L4;->l:I

    iget v1, p1, LX/0L4;->l:I

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/0L4;->m:F

    iget v1, p1, LX/0L4;->m:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, LX/0L4;->j:I

    iget v1, p1, LX/0L4;->j:I

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/0L4;->k:I

    iget v1, p1, LX/0L4;->k:I

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/0L4;->p:I

    iget v1, p1, LX/0L4;->p:I

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/0L4;->q:I

    iget v1, p1, LX/0L4;->q:I

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/0L4;->n:I

    iget v1, p1, LX/0L4;->n:I

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/0L4;->o:I

    iget v1, p1, LX/0L4;->o:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/0L4;->a:Ljava/lang/String;

    iget-object v1, p1, LX/0L4;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/08x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0L4;->r:Ljava/lang/String;

    iget-object v1, p1, LX/0L4;->r:Ljava/lang/String;

    invoke-static {v0, v1}, LX/08x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0L4;->b:Ljava/lang/String;

    iget-object v1, p1, LX/0L4;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/08x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0L4;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p1, LX/0L4;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    move v2, v3

    .line 42962
    :goto_1
    iget-object v0, p0, LX/0L4;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 42963
    iget-object v0, p0, LX/0L4;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iget-object v1, p1, LX/0L4;->f:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42964
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v3, v4

    .line 42965
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 42930
    iget v0, p0, LX/0L4;->t:I

    if-nez v0, :cond_5

    .line 42931
    iget-object v0, p0, LX/0L4;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 42932
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/0L4;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 42933
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/0L4;->c:I

    add-int/2addr v0, v2

    .line 42934
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/0L4;->d:I

    add-int/2addr v0, v2

    .line 42935
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/0L4;->h:I

    add-int/2addr v0, v2

    .line 42936
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/0L4;->i:I

    add-int/2addr v0, v2

    .line 42937
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/0L4;->l:I

    add-int/2addr v0, v2

    .line 42938
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/0L4;->m:F

    invoke-static {v2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 42939
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, LX/0L4;->e:J

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 42940
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, LX/0L4;->g:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_2
    add-int/2addr v0, v2

    .line 42941
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/0L4;->j:I

    add-int/2addr v0, v2

    .line 42942
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/0L4;->k:I

    add-int/2addr v0, v2

    .line 42943
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/0L4;->p:I

    add-int/2addr v0, v2

    .line 42944
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/0L4;->q:I

    add-int/2addr v0, v2

    .line 42945
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/0L4;->n:I

    add-int/2addr v0, v2

    .line 42946
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/0L4;->o:I

    add-int/2addr v0, v2

    .line 42947
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/0L4;->r:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 42948
    :goto_4
    iget-object v2, p0, LX/0L4;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 42949
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/0L4;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/2addr v0, v2

    .line 42950
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 42951
    :cond_0
    iget-object v0, p0, LX/0L4;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 42952
    :cond_1
    iget-object v0, p0, LX/0L4;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 42953
    :cond_2
    const/16 v0, 0x4d5

    goto :goto_2

    .line 42954
    :cond_3
    iget-object v0, p0, LX/0L4;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 42955
    :cond_4
    iput v0, p0, LX/0L4;->t:I

    .line 42956
    :cond_5
    iget v0, p0, LX/0L4;->t:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 42929
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaFormat("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0L4;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0L4;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0L4;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0L4;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0L4;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0L4;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0L4;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0L4;->m:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0L4;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0L4;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0L4;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/0L4;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/0L4;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0L4;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0L4;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0L4;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/0L4;->q:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
