.class public LX/0Gv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0G6;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:LX/0Go;

.field private static c:I

.field private static d:I


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Landroid/net/Uri;

.field private final g:Z

.field private final h:Ljava/lang/String;

.field private final i:LX/0GB;

.field private final j:LX/0Gg;

.field private final k:LX/0Gt;

.field private l:LX/0Gn;

.field private m:LX/0G6;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x1f40

    .line 36796
    const-class v0, LX/0Gv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0Gv;->a:Ljava/lang/String;

    .line 36797
    new-instance v0, LX/0Go;

    const/16 v1, 0x78

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/0Go;-><init>(II)V

    sput-object v0, LX/0Gv;->b:LX/0Go;

    .line 36798
    sput v3, LX/0Gv;->c:I

    .line 36799
    sput v3, LX/0Gv;->d:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;ZLjava/lang/String;LX/0GB;LX/0Gg;)V
    .locals 8

    .prologue
    .line 36794
    new-instance v7, LX/0Gu;

    invoke-direct {v7}, LX/0Gu;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, LX/0Gv;-><init>(Ljava/lang/String;Landroid/net/Uri;ZLjava/lang/String;LX/0GB;LX/0Gg;LX/0Gt;)V

    .line 36795
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/net/Uri;ZLjava/lang/String;LX/0GB;LX/0Gg;LX/0Gt;)V
    .locals 1

    .prologue
    .line 36783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36784
    invoke-static {p6}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36785
    iput-object p1, p0, LX/0Gv;->e:Ljava/lang/String;

    .line 36786
    iput-object p2, p0, LX/0Gv;->f:Landroid/net/Uri;

    .line 36787
    iput-boolean p3, p0, LX/0Gv;->g:Z

    .line 36788
    iput-object p4, p0, LX/0Gv;->h:Ljava/lang/String;

    .line 36789
    iput-object p5, p0, LX/0Gv;->i:LX/0GB;

    .line 36790
    iput-object p6, p0, LX/0Gv;->j:LX/0Gg;

    .line 36791
    iput-object p7, p0, LX/0Gv;->k:LX/0Gt;

    .line 36792
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Gv;->m:LX/0G6;

    .line 36793
    return-void
.end method

.method public static a(IIII)V
    .locals 6

    .prologue
    const/16 v0, 0x1f40

    .line 36773
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 36774
    sget-object v1, LX/0Gv;->b:LX/0Go;

    monitor-enter v1

    .line 36775
    :try_start_0
    sget-object v2, LX/0Gv;->b:LX/0Go;

    if-lez p0, :cond_0

    :goto_0
    if-lez p1, :cond_1

    :goto_1
    invoke-virtual {v2, p0, p1}, LX/0Go;->a(II)V

    .line 36776
    if-lez p2, :cond_2

    :goto_2
    sput p2, LX/0Gv;->c:I

    .line 36777
    if-lez p3, :cond_3

    :goto_3
    sput p3, LX/0Gv;->d:I

    .line 36778
    monitor-exit v1

    return-void

    .line 36779
    :cond_0
    const/16 p0, 0x78

    goto :goto_0

    :cond_1
    const/16 p1, 0xc

    goto :goto_1

    :cond_2
    move p2, v0

    .line 36780
    goto :goto_2

    :cond_3
    move p3, v0

    .line 36781
    goto :goto_3

    .line 36782
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Ljava/lang/String;Landroid/net/Uri;)Z
    .locals 3

    .prologue
    .line 36800
    new-instance v0, LX/0Gn;

    invoke-direct {v0, p0, p1}, LX/0Gn;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 36801
    sget-object v1, LX/0Gv;->b:LX/0Go;

    monitor-enter v1

    .line 36802
    :try_start_0
    sget-object v2, LX/0Gv;->b:LX/0Go;

    invoke-virtual {v2, v0}, LX/0Go;->a(LX/0Gn;)LX/0G6;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 36803
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private c(LX/0OA;)LX/0OA;
    .locals 12

    .prologue
    .line 36768
    iget-object v0, p0, LX/0Gv;->f:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 36769
    :goto_0
    return-object p1

    .line 36770
    :cond_0
    iget-object v0, p0, LX/0Gv;->f:Landroid/net/Uri;

    iget-object v1, p1, LX/0OA;->a:Landroid/net/Uri;

    iget-object v2, p0, LX/0Gv;->e:Ljava/lang/String;

    iget-boolean v3, p0, LX/0Gv;->g:Z

    invoke-static {v0, v1, v2, v3}, LX/0Gj;->a(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v2

    .line 36771
    new-instance v1, LX/0OA;

    iget-object v3, p1, LX/0OA;->b:[B

    iget-wide v4, p1, LX/0OA;->c:J

    iget-wide v6, p1, LX/0OA;->d:J

    iget-wide v8, p1, LX/0OA;->e:J

    iget-object v10, p1, LX/0OA;->f:Ljava/lang/String;

    iget v11, p1, LX/0OA;->g:I

    invoke-direct/range {v1 .. v11}, LX/0OA;-><init>(Landroid/net/Uri;[BJJJLjava/lang/String;I)V

    move-object p1, v1

    .line 36772
    goto :goto_0
.end method


# virtual methods
.method public final a([BII)I
    .locals 1

    .prologue
    .line 36767
    iget-object v0, p0, LX/0Gv;->m:LX/0G6;

    invoke-interface {v0, p1, p2, p3}, LX/0G6;->a([BII)I

    move-result v0

    return v0
.end method

.method public final a(LX/0OA;)J
    .locals 7

    .prologue
    .line 36712
    new-instance v0, LX/0Gn;

    iget-object v1, p0, LX/0Gv;->e:Ljava/lang/String;

    iget-object v2, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, LX/0Gn;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iput-object v0, p0, LX/0Gv;->l:LX/0Gn;

    .line 36713
    sget-object v6, LX/0Gv;->b:LX/0Go;

    monitor-enter v6

    .line 36714
    :try_start_0
    sget-object v0, LX/0Gv;->b:LX/0Go;

    iget-object v1, p0, LX/0Gv;->l:LX/0Gn;

    invoke-virtual {v0, v1}, LX/0Go;->a(LX/0Gn;)LX/0G6;

    move-result-object v0

    .line 36715
    if-nez v0, :cond_1

    .line 36716
    iget-object v0, p0, LX/0Gv;->i:LX/0GB;

    iget-object v1, p0, LX/0Gv;->e:Ljava/lang/String;

    iget-object v2, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/0GB;->a(Ljava/lang/String;Landroid/net/Uri;)[B

    move-result-object v0

    .line 36717
    if-nez v0, :cond_0

    .line 36718
    new-instance v0, LX/0Gr;

    iget-object v1, p0, LX/0Gv;->k:LX/0Gt;

    iget-object v2, p0, LX/0Gv;->h:Ljava/lang/String;

    sget v3, LX/0Gv;->c:I

    sget v4, LX/0Gv;->d:I

    iget-object v5, p0, LX/0Gv;->j:LX/0Gg;

    invoke-direct/range {v0 .. v5}, LX/0Gr;-><init>(LX/0Gt;Ljava/lang/String;IILX/0Gg;)V

    iput-object v0, p0, LX/0Gv;->m:LX/0G6;

    .line 36719
    :goto_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36720
    invoke-direct {p0, p1}, LX/0Gv;->c(LX/0OA;)LX/0OA;

    move-result-object v0

    .line 36721
    iget-object v1, p0, LX/0Gv;->m:LX/0G6;

    invoke-interface {v1, v0}, LX/0G6;->a(LX/0OA;)J

    move-result-wide v0

    return-wide v0

    .line 36722
    :cond_0
    :try_start_1
    new-instance v1, LX/0Gp;

    array-length v2, v0

    iget-object v3, p0, LX/0Gv;->j:LX/0Gg;

    invoke-direct {v1, v0, v2, v3}, LX/0Gp;-><init>([BILX/0Gg;)V

    iput-object v1, p0, LX/0Gv;->m:LX/0G6;

    .line 36723
    sget-object v0, LX/0Gv;->b:LX/0Go;

    iget-object v1, p0, LX/0Gv;->l:LX/0Gn;

    iget-object v2, p0, LX/0Gv;->m:LX/0G6;

    invoke-virtual {v0, v1, v2}, LX/0Go;->a(LX/0Gn;LX/0G6;)V

    goto :goto_0

    .line 36724
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 36725
    :cond_1
    :try_start_2
    instance-of v1, v0, LX/0Gp;

    if-eqz v1, :cond_2

    .line 36726
    check-cast v0, LX/0Gp;

    .line 36727
    new-instance v1, LX/0Gp;

    iget-object v2, v0, LX/0Gp;->a:[B

    iget v0, v0, LX/0Gp;->b:I

    iget-object v3, p0, LX/0Gv;->j:LX/0Gg;

    invoke-direct {v1, v2, v0, v3}, LX/0Gp;-><init>([BILX/0Gg;)V

    iput-object v1, p0, LX/0Gv;->m:LX/0G6;

    goto :goto_0

    .line 36728
    :cond_2
    check-cast v0, LX/0Gs;

    .line 36729
    invoke-virtual {v0}, LX/0Gq;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 36730
    new-instance v1, LX/0Gp;

    iget-object v2, v0, LX/0Gq;->b:[B

    iget v0, v0, LX/0Gq;->c:I

    iget-object v3, p0, LX/0Gv;->j:LX/0Gg;

    invoke-direct {v1, v2, v0, v3}, LX/0Gp;-><init>([BILX/0Gg;)V

    iput-object v1, p0, LX/0Gv;->m:LX/0G6;

    .line 36731
    sget-object v0, LX/0Gv;->b:LX/0Go;

    iget-object v1, p0, LX/0Gv;->l:LX/0Gn;

    iget-object v2, p0, LX/0Gv;->m:LX/0G6;

    invoke-virtual {v0, v1, v2}, LX/0Go;->a(LX/0Gn;LX/0G6;)V

    goto :goto_0

    .line 36732
    :cond_3
    sget-object v1, LX/0Gv;->b:LX/0Go;

    iget-object v2, p0, LX/0Gv;->l:LX/0Gn;

    invoke-virtual {v1, v2}, LX/0Go;->b(LX/0Gn;)LX/0G6;

    .line 36733
    iget-object v1, p0, LX/0Gv;->j:LX/0Gg;

    .line 36734
    iput-object v1, v0, LX/0Gs;->k:LX/0Gg;

    .line 36735
    iput-object v0, p0, LX/0Gv;->m:LX/0G6;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final a()V
    .locals 7

    .prologue
    .line 36756
    iget-object v0, p0, LX/0Gv;->m:LX/0G6;

    invoke-interface {v0}, LX/0G6;->a()V

    .line 36757
    iget-object v0, p0, LX/0Gv;->m:LX/0G6;

    instance-of v0, v0, LX/0Gq;

    if-eqz v0, :cond_0

    .line 36758
    iget-object v0, p0, LX/0Gv;->m:LX/0G6;

    check-cast v0, LX/0Gq;

    .line 36759
    invoke-virtual {v0}, LX/0Gq;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/0Gq;->b:[B

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/0Gq;->b:[B

    array-length v1, v1

    iget v2, v0, LX/0Gq;->c:I

    if-lt v1, v2, :cond_0

    .line 36760
    sget-object v1, LX/0Gv;->b:LX/0Go;

    monitor-enter v1

    .line 36761
    :try_start_0
    sget-object v2, LX/0Gv;->b:LX/0Go;

    iget-object v3, p0, LX/0Gv;->l:LX/0Gn;

    new-instance v4, LX/0Gp;

    iget-object v5, v0, LX/0Gq;->b:[B

    iget v6, v0, LX/0Gq;->c:I

    invoke-direct {v4, v5, v6}, LX/0Gp;-><init>([BI)V

    invoke-virtual {v2, v3, v4}, LX/0Go;->a(LX/0Gn;LX/0G6;)V

    .line 36762
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36763
    iget-object v1, p0, LX/0Gv;->i:LX/0GB;

    iget-object v2, p0, LX/0Gv;->l:LX/0Gn;

    iget-object v2, v2, LX/0Gn;->a:Ljava/lang/String;

    iget-object v3, p0, LX/0Gv;->l:LX/0Gn;

    iget-object v3, v3, LX/0Gn;->b:Landroid/net/Uri;

    iget-object v4, v0, LX/0Gq;->b:[B

    iget v0, v0, LX/0Gq;->c:I

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0GB;->a(Ljava/lang/String;Landroid/net/Uri;[BI)V

    .line 36764
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Gv;->m:LX/0G6;

    .line 36765
    return-void

    .line 36766
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(LX/0OA;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 36736
    new-instance v7, LX/0Gn;

    iget-object v1, p0, LX/0Gv;->e:Ljava/lang/String;

    iget-object v2, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-direct {v7, v1, v2}, LX/0Gn;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 36737
    sget-object v8, LX/0Gv;->b:LX/0Go;

    monitor-enter v8

    .line 36738
    :try_start_0
    sget-object v1, LX/0Gv;->b:LX/0Go;

    invoke-virtual {v1, v7}, LX/0Go;->a(LX/0Gn;)LX/0G6;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 36739
    monitor-exit v8

    .line 36740
    :goto_0
    return v0

    .line 36741
    :cond_0
    iget-object v1, p0, LX/0Gv;->i:LX/0GB;

    iget-object v2, p0, LX/0Gv;->e:Ljava/lang/String;

    iget-object v3, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, LX/0GB;->a(Ljava/lang/String;Landroid/net/Uri;)[B

    move-result-object v1

    .line 36742
    if-eqz v1, :cond_1

    .line 36743
    monitor-exit v8

    goto :goto_0

    .line 36744
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 36745
    :cond_1
    :try_start_1
    new-instance v0, LX/0Gs;

    iget-object v1, p0, LX/0Gv;->k:LX/0Gt;

    iget-object v2, p0, LX/0Gv;->h:Ljava/lang/String;

    sget v3, LX/0Gv;->c:I

    sget v4, LX/0Gv;->d:I

    iget-object v5, p0, LX/0Gv;->i:LX/0GB;

    iget-object v6, p0, LX/0Gv;->j:LX/0Gg;

    invoke-direct/range {v0 .. v6}, LX/0Gs;-><init>(LX/0Gt;Ljava/lang/String;IILX/0GB;LX/0Gg;)V

    .line 36746
    sget-object v1, LX/0Gv;->b:LX/0Go;

    invoke-virtual {v1, v7, v0}, LX/0Go;->a(LX/0Gn;LX/0G6;)V

    .line 36747
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 36748
    :try_start_2
    invoke-direct {p0, p1}, LX/0Gv;->c(LX/0OA;)LX/0OA;

    move-result-object v1

    .line 36749
    iget-object v2, p0, LX/0Gv;->e:Ljava/lang/String;

    iget-object v3, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v3}, LX/0Gs;->a(LX/0OA;Ljava/lang/String;Landroid/net/Uri;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    goto :goto_0

    .line 36750
    :catch_0
    move-exception v0

    .line 36751
    sget-object v1, LX/0Gv;->b:LX/0Go;

    monitor-enter v1

    .line 36752
    :try_start_3
    sget-object v2, LX/0Gv;->b:LX/0Go;

    invoke-virtual {v2, v7}, LX/0Go;->b(LX/0Gn;)LX/0G6;

    .line 36753
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 36754
    throw v0

    .line 36755
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method
