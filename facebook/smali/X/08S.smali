.class public final enum LX/08S;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/08S;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/08S;

.field public static final enum ALWAYS:LX/08S;

.field public static final enum MAYBE:LX/08S;

.field public static final enum NEVER:LX/08S;

.field public static final enum UNKNOWN:LX/08S;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21120
    new-instance v0, LX/08S;

    const-string v1, "ALWAYS"

    invoke-direct {v0, v1, v2}, LX/08S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/08S;->ALWAYS:LX/08S;

    .line 21121
    new-instance v0, LX/08S;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/08S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/08S;->UNKNOWN:LX/08S;

    .line 21122
    new-instance v0, LX/08S;

    const-string v1, "MAYBE"

    invoke-direct {v0, v1, v4}, LX/08S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/08S;->MAYBE:LX/08S;

    .line 21123
    new-instance v0, LX/08S;

    const-string v1, "NEVER"

    invoke-direct {v0, v1, v5}, LX/08S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/08S;->NEVER:LX/08S;

    .line 21124
    const/4 v0, 0x4

    new-array v0, v0, [LX/08S;

    sget-object v1, LX/08S;->ALWAYS:LX/08S;

    aput-object v1, v0, v2

    sget-object v1, LX/08S;->UNKNOWN:LX/08S;

    aput-object v1, v0, v3

    sget-object v1, LX/08S;->MAYBE:LX/08S;

    aput-object v1, v0, v4

    sget-object v1, LX/08S;->NEVER:LX/08S;

    aput-object v1, v0, v5

    sput-object v0, LX/08S;->$VALUES:[LX/08S;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/08S;
    .locals 1

    .prologue
    .line 21126
    const-class v0, LX/08S;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/08S;

    return-object v0
.end method

.method public static values()[LX/08S;
    .locals 1

    .prologue
    .line 21127
    sget-object v0, LX/08S;->$VALUES:[LX/08S;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/08S;

    return-object v0
.end method
