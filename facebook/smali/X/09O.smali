.class public final LX/09O;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:I

.field public final c:F

.field public final d:LX/09N;

.field public final e:LX/09N;

.field public final f:LX/09N;

.field public final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/09N;",
            ">;"
        }
    .end annotation
.end field

.field public final h:F

.field public final i:F

.field public final j:Z

.field public final k:F


# direct methods
.method public constructor <init>(LX/09M;JJ)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/high16 v6, 0x447a0000    # 1000.0f

    const/4 v1, 0x0

    .line 22425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22426
    iput-wide p2, p0, LX/09O;->a:J

    .line 22427
    iget v0, p1, LX/09M;->e:I

    iput v0, p0, LX/09O;->b:I

    .line 22428
    iget-wide v4, p1, LX/09M;->f:J

    long-to-float v0, v4

    div-float/2addr v0, v6

    iput v0, p0, LX/09O;->c:F

    .line 22429
    iget-object v0, p1, LX/09M;->g:LX/09N;

    iput-object v0, p0, LX/09O;->d:LX/09N;

    .line 22430
    iget-object v0, p1, LX/09M;->h:LX/09N;

    iput-object v0, p0, LX/09O;->e:LX/09N;

    .line 22431
    iget-object v0, p1, LX/09M;->i:LX/09N;

    iput-object v0, p0, LX/09O;->f:LX/09N;

    .line 22432
    iget-object v0, p1, LX/09M;->j:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/09O;->g:LX/0Px;

    .line 22433
    iget-object v0, p0, LX/09O;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22434
    iput v1, p0, LX/09O;->h:F

    .line 22435
    iput v1, p0, LX/09O;->i:F

    .line 22436
    :goto_0
    iget-boolean v0, p1, LX/09M;->k:Z

    iput-boolean v0, p0, LX/09O;->j:Z

    .line 22437
    iget-wide v0, p1, LX/09M;->o:J

    long-to-float v0, v0

    div-float/2addr v0, v6

    iput v0, p0, LX/09O;->k:F

    .line 22438
    return-void

    .line 22439
    :cond_0
    iget-object v0, p0, LX/09O;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_1

    iget-object v0, p0, LX/09O;->g:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/09N;

    .line 22440
    invoke-virtual {v0}, LX/09N;->b()F

    move-result v0

    add-float/2addr v1, v0

    .line 22441
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 22442
    :cond_1
    iput v1, p0, LX/09O;->h:F

    .line 22443
    iget-object v0, p0, LX/09O;->g:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/09N;

    .line 22444
    iget-wide v8, v0, LX/09N;->b:J

    move-wide v2, v8

    .line 22445
    cmp-long v2, v2, p4

    if-gez v2, :cond_2

    .line 22446
    iget-wide v8, v0, LX/09N;->b:J

    move-wide v2, v8

    .line 22447
    sub-long v2, p4, v2

    long-to-float v0, v2

    div-float/2addr v0, v6

    sub-float v0, v1, v0

    .line 22448
    :goto_2
    iput v0, p0, LX/09O;->i:F

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final j()F
    .locals 2

    .prologue
    .line 22449
    iget v0, p0, LX/09O;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/09O;->c:F

    iget v1, p0, LX/09O;->b:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method
