.class public final LX/0Eb;
.super LX/0Dc;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;


# direct methods
.method public constructor <init>(Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;)V
    .locals 0

    .prologue
    .line 31985
    iput-object p1, p0, LX/0Eb;->a:Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;

    invoke-direct {p0}, LX/0Dc;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;ILandroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 31986
    if-eqz p1, :cond_1

    .line 31987
    if-eqz p2, :cond_0

    .line 31988
    :try_start_0
    iget-object v0, p0, LX/0Eb;->a:Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;

    const-string v1, "errorCode"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "errorMessage"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p1, v1, v2}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a$redex0(Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;ILjava/lang/String;)V

    .line 31989
    :goto_0
    return-void

    .line 31990
    :cond_0
    iget-object v0, p0, LX/0Eb;->a:Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;

    invoke-static {v0, p1, p3}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->b(Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 31991
    :catch_0
    sget-object v0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a:Ljava/lang/String;

    const-string v1, "Exception when handling callback for %s!"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 31992
    iget-object v4, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v4, v4

    .line 31993
    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 31994
    :cond_1
    goto :goto_0
.end method
