.class public final LX/06h;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:LX/056;


# direct methods
.method public constructor <init>(LX/056;)V
    .locals 0

    .prologue
    .line 18098
    iput-object p1, p0, LX/06h;->a:LX/056;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, 0x360ed973

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 18099
    const-string v1, "FbnsConnectionManager"

    const-string v2, "receiver/power_save_mode"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18100
    if-nez p2, :cond_0

    .line 18101
    const/16 v1, 0x27

    const v2, -0x6eb6a190

    invoke-static {p2, v4, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 18102
    :goto_0
    return-void

    .line 18103
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.os.action.POWER_SAVE_MODE_CHANGED"

    invoke-static {v1, v2}, LX/06P;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 18104
    const v1, -0x599a6588

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 18105
    :cond_1
    iget-object v1, p0, LX/06h;->a:LX/056;

    invoke-static {v1, p2}, LX/056;->a$redex0(LX/056;Landroid/content/Intent;)V

    .line 18106
    const v1, -0xf2a871f

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0
.end method
