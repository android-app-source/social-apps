.class public LX/0Dk;
.super Landroid/app/Dialog;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31003
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 31004
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/0Dk;->requestWindowFeature(I)Z

    .line 31005
    sget v0, LX/0E9;->b:I

    invoke-virtual {p0, v0}, LX/0Dk;->setContentView(I)V

    .line 31006
    invoke-virtual {p0}, LX/0Dk;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 31007
    invoke-virtual {p0}, LX/0Dk;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 31008
    const/4 p1, -0x1

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 31009
    const/4 p1, -0x2

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 31010
    const/16 p1, 0x50

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 31011
    iget p1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit8 p1, p1, -0x3

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 31012
    invoke-virtual {p0}, LX/0Dk;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 31013
    return-void
.end method
