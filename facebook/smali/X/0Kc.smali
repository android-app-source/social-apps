.class public LX/0Kc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Gk;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Kb;

.field private final c:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41575
    const-class v0, LX/0Gk;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0Kc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Kb;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 41571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41572
    iput-object p1, p0, LX/0Kc;->b:LX/0Kb;

    .line 41573
    iput-object p2, p0, LX/0Kc;->c:Landroid/os/Handler;

    .line 41574
    return-void
.end method


# virtual methods
.method public final a(LX/0H9;Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V
    .locals 3

    .prologue
    .line 41568
    iget-object v0, p0, LX/0Kc;->b:LX/0Kb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Kc;->c:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 41569
    iget-object v0, p0, LX/0Kc;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/video/vps/VpsEventCallbackImpl$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/video/vps/VpsEventCallbackImpl$1;-><init>(LX/0Kc;LX/0H9;Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V

    const v2, 0x6984e48e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 41570
    :cond_0
    return-void
.end method
