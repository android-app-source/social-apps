.class public LX/09m;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/08e;

.field public final c:LX/08n;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 22889
    new-instance v0, LX/09l;

    invoke-direct {v0}, LX/09l;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/09m;-><init>(Landroid/content/Context;LX/08e;I)V

    .line 22890
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/08e;)V
    .locals 1

    .prologue
    .line 22964
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/09m;-><init>(Landroid/content/Context;LX/08e;I)V

    .line 22965
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/08e;I)V
    .locals 1

    .prologue
    .line 22959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22960
    iput-object p1, p0, LX/09m;->a:Landroid/content/Context;

    .line 22961
    iput-object p2, p0, LX/09m;->b:LX/08e;

    .line 22962
    new-instance v0, LX/08n;

    invoke-direct {v0, p1, p2, p0, p3}, LX/08n;-><init>(Landroid/content/Context;LX/08e;LX/09m;I)V

    iput-object v0, p0, LX/09m;->c:LX/08n;

    .line 22963
    return-void
.end method

.method public static a(LX/09m;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22956
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 22957
    iget-object v0, p0, LX/09m;->a:Landroid/content/Context;

    invoke-static {v0}, LX/04u;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 22958
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/09m;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(LX/09m;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 22955
    iget-object v0, p0, LX/09m;->a:Landroid/content/Context;

    sget-object v1, LX/01p;->f:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 15

    .prologue
    .line 22923
    iget-object v0, p0, LX/09m;->c:LX/08n;

    .line 22924
    invoke-static {v0}, LX/08n;->d(LX/08n;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 22925
    iget-object v2, v0, LX/08n;->a:Landroid/content/Context;

    invoke-static {v2}, LX/04u;->d(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 22926
    iget-object v1, v0, LX/08n;->b:LX/08e;

    invoke-virtual {v1}, LX/08e;->c()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 22927
    const/4 v1, 0x2

    const-string v2, "PREINSTALLER"

    invoke-static {v0, v1, v2}, LX/08n;->a$redex0(LX/08n;ILjava/lang/String;)V

    .line 22928
    :goto_0
    return-void

    .line 22929
    :cond_0
    iget v1, v0, LX/08n;->e:I

    const-string v2, "PREINSTALLER_DISABLED"

    invoke-static {v0, v1, v2}, LX/08n;->a$redex0(LX/08n;ILjava/lang/String;)V

    goto :goto_0

    .line 22930
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "shared_qe_flag"

    iget-object v3, v0, LX/08n;->b:LX/08e;

    invoke-virtual {v3}, LX/08e;->b()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "sharing_state_enabled"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "register_and_stop"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 22931
    iget v1, v0, LX/08n;->e:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    iget-object v1, v0, LX/08n;->a:Landroid/content/Context;

    invoke-static {v1}, LX/04u;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 22932
    iget v1, v0, LX/08n;->e:I

    const-string v2, "LEADER"

    invoke-static {v0, v1, v2}, LX/08n;->a$redex0(LX/08n;ILjava/lang/String;)V

    goto :goto_0

    .line 22933
    :cond_2
    iget-object v1, v0, LX/08n;->a:Landroid/content/Context;

    invoke-static {v1}, LX/04u;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 22934
    iget v1, v0, LX/08n;->e:I

    const-string v2, "NO_LEADER"

    invoke-static {v0, v1, v2}, LX/08n;->a$redex0(LX/08n;ILjava/lang/String;)V

    goto :goto_0

    .line 22935
    :cond_3
    iget-object v1, v0, LX/08n;->a:Landroid/content/Context;

    invoke-static {v1}, LX/04u;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 22936
    iget-object v2, v0, LX/08n;->a:Landroid/content/Context;

    invoke-static {v2, v1}, LX/05U;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, LX/08n;->a:Landroid/content/Context;

    invoke-static {v2, v1}, LX/0Hk;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 22937
    :cond_4
    iget v1, v0, LX/08n;->e:I

    const-string v2, "QE_CONTROLLER_UNAVAILABLE"

    invoke-static {v0, v1, v2}, LX/08n;->a$redex0(LX/08n;ILjava/lang/String;)V

    goto :goto_0

    .line 22938
    :cond_5
    iget-object v1, v0, LX/08n;->c:LX/08o;

    new-instance v2, LX/0ID;

    invoke-direct {v2, v0}, LX/0ID;-><init>(LX/08n;)V

    const/4 v14, 0x0

    .line 22939
    iget-object v4, v1, LX/08o;->a:Landroid/content/Context;

    sget-object v5, LX/01p;->f:LX/01q;

    invoke-static {v4, v5}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v4

    move-object v4, v4

    .line 22940
    const-string v5, "cached_qe_flag"

    iget v6, v1, LX/08o;->c:I

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 22941
    iget-object v5, v1, LX/08o;->a:Landroid/content/Context;

    invoke-static {v5}, LX/04u;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 22942
    iget-object v5, v1, LX/08o;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 22943
    const-string v5, "shared_qe_flag"

    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 22944
    invoke-virtual {v2, v4}, LX/0ID;->a(I)V

    .line 22945
    :goto_1
    goto/16 :goto_0

    .line 22946
    :cond_6
    new-instance v6, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v4, 0x0

    invoke-direct {v6, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 22947
    iget-object v4, v1, LX/08o;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v5, Lcom/facebook/rti/push/client/SharedConfigProvider$1;

    invoke-direct {v5, v1, v6, v2, v8}, Lcom/facebook/rti/push/client/SharedConfigProvider$1;-><init>(LX/08o;Ljava/util/concurrent/atomic/AtomicBoolean;LX/0ID;I)V

    const-wide/16 v12, 0x7530

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v12, v13, v7}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v7

    .line 22948
    new-instance v4, LX/0IF;

    move-object v5, v1

    move-object v9, v2

    invoke-direct/range {v4 .. v9}, LX/0IF;-><init>(LX/08o;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/concurrent/ScheduledFuture;ILX/0ID;)V

    .line 22949
    iget-object v5, v1, LX/08o;->a:Landroid/content/Context;

    new-instance v6, Landroid/content/IntentFilter;

    const-string v7, "com.facebook.rti.intent.SHARED_QE_FLAG_RESPONSE"

    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 22950
    new-instance v7, Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v7, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 22951
    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22952
    new-instance v6, Landroid/content/Intent;

    const-string v5, "com.facebook.rti.intent.SHARED_QE_FLAG_REQUEST"

    invoke-direct {v6, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 22953
    const-string v5, "pkg_name"

    invoke-virtual {v6, v5, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 22954
    iget-object v5, v1, LX/08o;->d:LX/04v;

    const/4 v11, -0x1

    move-object v8, v14

    move-object v9, v4

    move-object v10, v14

    move-object v12, v14

    move-object v13, v14

    invoke-virtual/range {v5 .. v13}, LX/04v;->a(Landroid/content/Intent;Ljava/util/List;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)I

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 22902
    invoke-static {p0}, LX/09m;->e(LX/09m;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 22903
    const-string v1, "register_and_stop"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 22904
    if-eqz v1, :cond_1

    .line 22905
    const-string v0, "FbnsClientWrapper"

    const-string v1, "not stopping FbnsService because waiting for register to complete"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22906
    :cond_0
    :goto_0
    return-void

    .line 22907
    :cond_1
    iget-object v1, p0, LX/09m;->a:Landroid/content/Context;

    const/4 p0, 0x1

    const/4 v6, 0x0

    .line 22908
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/rti/push/service/FbnsService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 22909
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/05U;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 22910
    sget-object v2, LX/09o;->a:Ljava/lang/String;

    const-string v3, "FBNS Service not found"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22911
    :goto_1
    if-eqz p1, :cond_0

    .line 22912
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 22913
    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0

    .line 22914
    :cond_2
    sget-object v3, LX/09o;->a:Ljava/lang/String;

    const-string v4, "Stopping running FBNS service %s"

    new-array v5, p0, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v3, v4, v5}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22915
    new-instance v3, Landroid/content/Intent;

    const-string v4, "Orca.STOP"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 22916
    new-instance v4, Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 22917
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 22918
    new-instance v5, LX/04v;

    invoke-direct {v5, v1}, LX/04v;-><init>(Landroid/content/Context;)V

    .line 22919
    invoke-virtual {v5, v3, v4}, LX/04v;->a(Landroid/content/Intent;Landroid/content/ComponentName;)Landroid/content/ComponentName;

    move-result-object v3

    .line 22920
    if-nez v3, :cond_3

    .line 22921
    sget-object v3, LX/09o;->a:Ljava/lang/String;

    const-string v4, "Missing %s"

    new-array v5, p0, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v3, v4, v5}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22922
    :cond_3
    invoke-static {v1, v6, v2}, LX/09o;->a(Landroid/content/Context;ZLjava/lang/String;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 22897
    iget-object v0, p0, LX/09m;->c:LX/08n;

    const/4 p0, 0x0

    .line 22898
    invoke-static {v0}, LX/08n;->d(LX/08n;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 22899
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "shared_qe_flag"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "sharing_state_enabled"

    invoke-interface {v1, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "register_and_stop"

    invoke-interface {v1, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "cached_qe_flag"

    iget v3, v0, LX/08n;->e:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "shared_flag"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "leader_package"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 22900
    iget v1, v0, LX/08n;->e:I

    const-string v2, "SHARING_IS_DISABLED"

    invoke-static {v0, v1, v2}, LX/08n;->a$redex0(LX/08n;ILjava/lang/String;)V

    .line 22901
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 22891
    invoke-static {p0}, LX/09m;->e(LX/09m;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "shared_flag"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 22892
    iget-object v1, p0, LX/09m;->a:Landroid/content/Context;

    invoke-static {v1}, LX/04u;->d(Landroid/content/Context;)Z

    move-result v1

    .line 22893
    if-eqz v1, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 22894
    iget-object v1, p0, LX/09m;->a:Landroid/content/Context;

    invoke-static {p0, v0}, LX/09m;->a(LX/09m;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/09o;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 22895
    :goto_0
    return-void

    .line 22896
    :cond_0
    iget-object v0, p0, LX/09m;->a:Landroid/content/Context;

    iget-object v1, p0, LX/09m;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/09o;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
