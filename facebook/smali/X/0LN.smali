.class public final LX/0LN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field public static a:Z

.field public static b:Z


# instance fields
.field public A:I

.field private B:J

.field private C:J

.field private D:J

.field private E:F

.field private F:[B

.field private G:I

.field private H:I

.field public final c:LX/0LG;

.field private final d:I

.field public final e:Landroid/os/ConditionVariable;

.field private final f:[J

.field public final g:LX/0LH;

.field private h:Landroid/media/AudioTrack;

.field public i:Landroid/media/AudioTrack;

.field private j:I

.field private k:I

.field private l:I

.field private m:Z

.field private n:I

.field public o:I

.field private p:J

.field private q:I

.field private r:I

.field private s:J

.field private t:J

.field private u:Z

.field private v:J

.field private w:Ljava/lang/reflect/Method;

.field private x:J

.field private y:J

.field private z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43585
    sput-boolean v0, LX/0LN;->a:Z

    .line 43586
    sput-boolean v0, LX/0LN;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 43583
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, LX/0LN;-><init>(LX/0LG;I)V

    .line 43584
    return-void
.end method

.method public constructor <init>(LX/0LG;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 43568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43569
    iput-object p1, p0, LX/0LN;->c:LX/0LG;

    .line 43570
    iput p2, p0, LX/0LN;->d:I

    .line 43571
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, LX/0LN;->e:Landroid/os/ConditionVariable;

    .line 43572
    sget v0, LX/08x;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 43573
    :try_start_0
    const-class v0, Landroid/media/AudioTrack;

    const-string v1, "getLatency"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/0LN;->w:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43574
    :cond_0
    :goto_0
    sget v0, LX/08x;->a:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 43575
    new-instance v0, LX/0LJ;

    invoke-direct {v0}, LX/0LJ;-><init>()V

    iput-object v0, p0, LX/0LN;->g:LX/0LH;

    .line 43576
    :goto_1
    const/16 v0, 0xa

    new-array v0, v0, [J

    iput-object v0, p0, LX/0LN;->f:[J

    .line 43577
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/0LN;->E:F

    .line 43578
    iput v3, p0, LX/0LN;->A:I

    .line 43579
    return-void

    .line 43580
    :cond_1
    sget v0, LX/08x;->a:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    .line 43581
    new-instance v0, LX/0LI;

    invoke-direct {v0}, LX/0LI;-><init>()V

    iput-object v0, p0, LX/0LN;->g:LX/0LH;

    goto :goto_1

    .line 43582
    :cond_2
    new-instance v0, LX/0LH;

    invoke-direct {v0}, LX/0LH;-><init>()V

    iput-object v0, p0, LX/0LN;->g:LX/0LH;

    goto :goto_1

    :catch_0
    goto :goto_0
.end method

.method private static a(ILjava/nio/ByteBuffer;)I
    .locals 3

    .prologue
    .line 43555
    const/4 v0, 0x7

    if-eq p0, v0, :cond_0

    const/16 v0, 0x8

    if-ne p0, v0, :cond_1

    .line 43556
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .line 43557
    add-int/lit8 v1, v0, 0x4

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit8 v1, v1, 0x1

    shl-int/lit8 v1, v1, 0x6

    add-int/lit8 v0, v0, 0x5

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xfc

    shr-int/lit8 v0, v0, 0x2

    or-int/2addr v0, v1

    .line 43558
    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x20

    move v0, v0

    .line 43559
    :goto_0
    return v0

    .line 43560
    :cond_1
    const/4 v0, 0x5

    if-ne p0, v0, :cond_2

    .line 43561
    const/16 v0, 0x600

    move v0, v0

    .line 43562
    goto :goto_0

    .line 43563
    :cond_2
    const/4 v0, 0x6

    if-ne p0, v0, :cond_3

    .line 43564
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xc0

    shr-int/lit8 v0, v0, 0x6

    .line 43565
    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    const/4 v0, 0x6

    :goto_1
    mul-int/lit16 v0, v0, 0x100

    move v0, v0

    .line 43566
    goto :goto_0

    .line 43567
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected audio encoding: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    sget-object v0, LX/0OW;->a:[I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit8 v1, v1, 0x30

    shr-int/lit8 v1, v1, 0x4

    aget v0, v0, v1

    goto :goto_1
.end method

.method private a(J)J
    .locals 3

    .prologue
    .line 43554
    iget v0, p0, LX/0LN;->n:I

    int-to-long v0, v0

    div-long v0, p1, v0

    return-wide v0
.end method

.method private a(Landroid/media/MediaFormat;ZI)V
    .locals 8

    .prologue
    .line 43509
    const-string v0, "channel-count"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    .line 43510
    packed-switch v2, :pswitch_data_0

    .line 43511
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported channel count: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43512
    :pswitch_0
    const/4 v0, 0x4

    .line 43513
    :goto_0
    const-string v1, "sample-rate"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v3

    .line 43514
    const-string v1, "mime"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 43515
    if-eqz p2, :cond_0

    invoke-static {v1}, LX/0LN;->b(Ljava/lang/String;)I

    move-result v1

    .line 43516
    :goto_1
    invoke-virtual {p0}, LX/0LN;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, LX/0LN;->j:I

    if-ne v4, v3, :cond_1

    iget v4, p0, LX/0LN;->k:I

    if-ne v4, v0, :cond_1

    iget v4, p0, LX/0LN;->l:I

    if-ne v4, v1, :cond_1

    .line 43517
    :goto_2
    return-void

    .line 43518
    :pswitch_1
    const/16 v0, 0xc

    .line 43519
    goto :goto_0

    .line 43520
    :pswitch_2
    const/16 v0, 0x1c

    .line 43521
    goto :goto_0

    .line 43522
    :pswitch_3
    const/16 v0, 0xcc

    .line 43523
    goto :goto_0

    .line 43524
    :pswitch_4
    const/16 v0, 0xdc

    .line 43525
    goto :goto_0

    .line 43526
    :pswitch_5
    const/16 v0, 0xfc

    .line 43527
    goto :goto_0

    .line 43528
    :pswitch_6
    const/16 v0, 0x4fc

    .line 43529
    goto :goto_0

    .line 43530
    :pswitch_7
    sget v0, LX/0Ko;->a:I

    goto :goto_0

    .line 43531
    :cond_0
    const/4 v1, 0x2

    goto :goto_1

    .line 43532
    :cond_1
    invoke-virtual {p0}, LX/0LN;->j()V

    .line 43533
    iput v1, p0, LX/0LN;->l:I

    .line 43534
    iput-boolean p2, p0, LX/0LN;->m:Z

    .line 43535
    iput v3, p0, LX/0LN;->j:I

    .line 43536
    iput v0, p0, LX/0LN;->k:I

    .line 43537
    mul-int/lit8 v2, v2, 0x2

    iput v2, p0, LX/0LN;->n:I

    .line 43538
    if-eqz p3, :cond_2

    move-object v0, p0

    .line 43539
    :goto_3
    iput p3, v0, LX/0LN;->o:I

    .line 43540
    if-eqz p2, :cond_9

    const-wide/16 v0, -0x1

    :goto_4
    iput-wide v0, p0, LX/0LN;->p:J

    goto :goto_2

    .line 43541
    :cond_2
    if-eqz p2, :cond_5

    .line 43542
    const/4 v0, 0x5

    if-eq v1, v0, :cond_3

    const/4 v0, 0x6

    if-ne v1, v0, :cond_4

    .line 43543
    :cond_3
    const/16 p3, 0x5000

    move-object v0, p0

    goto :goto_3

    .line 43544
    :cond_4
    const p3, 0xc000

    move-object v0, p0

    goto :goto_3

    .line 43545
    :cond_5
    invoke-static {v3, v0, v1}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v2

    .line 43546
    const/4 v0, -0x2

    if-eq v2, v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 43547
    mul-int/lit8 v1, v2, 0x4

    .line 43548
    const-wide/32 v4, 0x3d090

    invoke-direct {p0, v4, v5}, LX/0LN;->c(J)J

    move-result-wide v4

    long-to-int v0, v4

    iget v3, p0, LX/0LN;->n:I

    mul-int p3, v0, v3

    .line 43549
    int-to-long v2, v2

    const-wide/32 v4, 0xb71b0

    invoke-direct {p0, v4, v5}, LX/0LN;->c(J)J

    move-result-wide v4

    iget v0, p0, LX/0LN;->n:I

    int-to-long v6, v0

    mul-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    long-to-int v0, v2

    .line 43550
    if-ge v1, p3, :cond_7

    move-object v0, p0

    goto :goto_3

    .line 43551
    :cond_6
    const/4 v0, 0x0

    goto :goto_5

    .line 43552
    :cond_7
    if-le v1, v0, :cond_8

    move p3, v0

    move-object v0, p0

    goto :goto_3

    :cond_8
    move p3, v1

    move-object v0, p0

    goto :goto_3

    .line 43553
    :cond_9
    iget v0, p0, LX/0LN;->o:I

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, LX/0LN;->a(J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LX/0LN;->b(J)J

    move-result-wide v0

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 43502
    const/4 v1, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 43503
    :goto_1
    return v0

    .line 43504
    :sswitch_0
    const-string v2, "audio/ac3"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "audio/eac3"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "audio/vnd.dts"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "audio/vnd.dts.hd"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    .line 43505
    :pswitch_0
    const/4 v0, 0x5

    goto :goto_1

    .line 43506
    :pswitch_1
    const/4 v0, 0x6

    goto :goto_1

    .line 43507
    :pswitch_2
    const/4 v0, 0x7

    goto :goto_1

    .line 43508
    :pswitch_3
    const/16 v0, 0x8

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x41455b98 -> :sswitch_2
        0xb269698 -> :sswitch_0
        0x59ae0c65 -> :sswitch_1
        0x59c2dc42 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(J)J
    .locals 5

    .prologue
    .line 43501
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p1

    iget v2, p0, LX/0LN;->j:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private c(J)J
    .locals 5

    .prologue
    .line 43500
    iget v0, p0, LX/0LN;->j:I

    int-to-long v0, v0

    mul-long/2addr v0, p1

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 43492
    invoke-virtual {p0}, LX/0LN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43493
    sget v0, LX/08x;->a:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 43494
    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    iget v1, p0, LX/0LN;->E:F

    .line 43495
    invoke-virtual {v0, v1}, Landroid/media/AudioTrack;->setVolume(F)I

    .line 43496
    :cond_0
    :goto_0
    return-void

    .line 43497
    :cond_1
    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    iget v1, p0, LX/0LN;->E:F

    .line 43498
    invoke-virtual {v0, v1, v1}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    .line 43499
    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 43487
    iget-object v0, p0, LX/0LN;->h:Landroid/media/AudioTrack;

    if-nez v0, :cond_0

    .line 43488
    :goto_0
    return-void

    .line 43489
    :cond_0
    iget-object v0, p0, LX/0LN;->h:Landroid/media/AudioTrack;

    .line 43490
    const/4 v1, 0x0

    iput-object v1, p0, LX/0LN;->h:Landroid/media/AudioTrack;

    .line 43491
    new-instance v1, Lcom/google/android/exoplayer/audio/AudioTrack$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/exoplayer/audio/AudioTrack$2;-><init>(LX/0LN;Landroid/media/AudioTrack;)V

    invoke-virtual {v1}, Lcom/google/android/exoplayer/audio/AudioTrack$2;->start()V

    goto :goto_0
.end method

.method private o()V
    .locals 12

    .prologue
    .line 43445
    iget-object v0, p0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v0}, LX/0LH;->c()J

    move-result-wide v2

    .line 43446
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    .line 43447
    :cond_0
    :goto_0
    return-void

    .line 43448
    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    .line 43449
    iget-wide v0, p0, LX/0LN;->t:J

    sub-long v0, v4, v0

    const-wide/16 v6, 0x7530

    cmp-long v0, v0, v6

    if-ltz v0, :cond_3

    .line 43450
    iget-object v0, p0, LX/0LN;->f:[J

    iget v1, p0, LX/0LN;->q:I

    sub-long v6, v2, v4

    aput-wide v6, v0, v1

    .line 43451
    iget v0, p0, LX/0LN;->q:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0xa

    iput v0, p0, LX/0LN;->q:I

    .line 43452
    iget v0, p0, LX/0LN;->r:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    .line 43453
    iget v0, p0, LX/0LN;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0LN;->r:I

    .line 43454
    :cond_2
    iput-wide v4, p0, LX/0LN;->t:J

    .line 43455
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0LN;->s:J

    .line 43456
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, LX/0LN;->r:I

    if-ge v0, v1, :cond_3

    .line 43457
    iget-wide v6, p0, LX/0LN;->s:J

    iget-object v1, p0, LX/0LN;->f:[J

    aget-wide v8, v1, v0

    iget v1, p0, LX/0LN;->r:I

    int-to-long v10, v1

    div-long/2addr v8, v10

    add-long/2addr v6, v8

    iput-wide v6, p0, LX/0LN;->s:J

    .line 43458
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 43459
    :cond_3
    invoke-static {p0}, LX/0LN;->s(LX/0LN;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 43460
    iget-wide v0, p0, LX/0LN;->v:J

    sub-long v0, v4, v0

    const-wide/32 v6, 0x7a120

    cmp-long v0, v0, v6

    if-ltz v0, :cond_0

    .line 43461
    iget-object v0, p0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v0}, LX/0LH;->d()Z

    move-result v0

    iput-boolean v0, p0, LX/0LN;->u:Z

    .line 43462
    iget-boolean v0, p0, LX/0LN;->u:Z

    if-eqz v0, :cond_4

    .line 43463
    iget-object v0, p0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v0}, LX/0LH;->e()J

    move-result-wide v0

    const-wide/16 v6, 0x3e8

    div-long/2addr v0, v6

    .line 43464
    iget-object v6, p0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v6}, LX/0LH;->f()J

    move-result-wide v6

    .line 43465
    iget-wide v8, p0, LX/0LN;->C:J

    cmp-long v8, v0, v8

    if-gez v8, :cond_6

    .line 43466
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0LN;->u:Z

    .line 43467
    :cond_4
    :goto_2
    iget-object v0, p0, LX/0LN;->w:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, LX/0LN;->m:Z

    if-nez v0, :cond_5

    .line 43468
    :try_start_0
    iget-object v0, p0, LX/0LN;->w:Ljava/lang/reflect/Method;

    iget-object v1, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-wide v2, p0, LX/0LN;->p:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/0LN;->D:J

    .line 43469
    iget-wide v0, p0, LX/0LN;->D:J

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/0LN;->D:J

    .line 43470
    iget-wide v0, p0, LX/0LN;->D:J

    const-wide/32 v2, 0x4c4b40

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    .line 43471
    const-string v0, "AudioTrack"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring impossibly large audio latency: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/0LN;->D:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 43472
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0LN;->D:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43473
    :cond_5
    :goto_3
    iput-wide v4, p0, LX/0LN;->v:J

    goto/16 :goto_0

    .line 43474
    :cond_6
    sub-long v8, v0, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/32 v10, 0x4c4b40

    cmp-long v8, v8, v10

    if-lez v8, :cond_8

    .line 43475
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Spurious audio timestamp (system clock mismatch): "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 43476
    sget-boolean v1, LX/0LN;->b:Z

    if-eqz v1, :cond_7

    .line 43477
    new-instance v1, LX/0LL;

    invoke-direct {v1, v0}, LX/0LL;-><init>(Ljava/lang/String;)V

    throw v1

    .line 43478
    :cond_7
    const-string v1, "AudioTrack"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 43479
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0LN;->u:Z

    goto/16 :goto_2

    .line 43480
    :cond_8
    invoke-direct {p0, v6, v7}, LX/0LN;->b(J)J

    move-result-wide v8

    sub-long/2addr v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/32 v10, 0x4c4b40

    cmp-long v8, v8, v10

    if-lez v8, :cond_4

    .line 43481
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Spurious audio timestamp (frame position mismatch): "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 43482
    sget-boolean v1, LX/0LN;->b:Z

    if-eqz v1, :cond_9

    .line 43483
    new-instance v1, LX/0LL;

    invoke-direct {v1, v0}, LX/0LL;-><init>(Ljava/lang/String;)V

    throw v1

    .line 43484
    :cond_9
    const-string v1, "AudioTrack"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 43485
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0LN;->u:Z

    goto/16 :goto_2

    .line 43486
    :catch_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0LN;->w:Ljava/lang/reflect/Method;

    goto/16 :goto_3
.end method

.method private p()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 43438
    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    .line 43439
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 43440
    return-void

    .line 43441
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43442
    iput-object v2, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    .line 43443
    :goto_0
    new-instance v1, LX/0LK;

    iget v2, p0, LX/0LN;->j:I

    iget v3, p0, LX/0LN;->k:I

    iget v4, p0, LX/0LN;->o:I

    invoke-direct {v1, v0, v2, v3, v4}, LX/0LK;-><init>(IIII)V

    throw v1

    .line 43444
    :catch_0
    iput-object v2, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    throw v0
.end method

.method private q()J
    .locals 2

    .prologue
    .line 43437
    iget-boolean v0, p0, LX/0LN;->m:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/0LN;->y:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LX/0LN;->x:J

    invoke-direct {p0, v0, v1}, LX/0LN;->a(J)J

    move-result-wide v0

    goto :goto_0
.end method

.method private r()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 43429
    iput-wide v2, p0, LX/0LN;->s:J

    .line 43430
    iput v0, p0, LX/0LN;->r:I

    .line 43431
    iput v0, p0, LX/0LN;->q:I

    .line 43432
    iput-wide v2, p0, LX/0LN;->t:J

    .line 43433
    iput-boolean v0, p0, LX/0LN;->u:Z

    .line 43434
    iput-wide v2, p0, LX/0LN;->v:J

    .line 43435
    return-void
.end method

.method public static s(LX/0LN;)Z
    .locals 2

    .prologue
    .line 43436
    sget v0, LX/08x;->a:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_1

    iget v0, p0, LX/0LN;->l:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/0LN;->l:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x1

    .line 43291
    iget-object v0, p0, LX/0LN;->e:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 43292
    if-nez p1, :cond_2

    .line 43293
    new-instance v0, Landroid/media/AudioTrack;

    iget v1, p0, LX/0LN;->d:I

    iget v2, p0, LX/0LN;->j:I

    iget v3, p0, LX/0LN;->k:I

    iget v4, p0, LX/0LN;->l:I

    iget v5, p0, LX/0LN;->o:I

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    .line 43294
    :goto_0
    invoke-direct {p0}, LX/0LN;->p()V

    .line 43295
    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v7

    .line 43296
    sget-boolean v0, LX/0LN;->a:Z

    if-eqz v0, :cond_1

    .line 43297
    sget v0, LX/08x;->a:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    .line 43298
    iget-object v0, p0, LX/0LN;->h:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0LN;->h:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v0

    if-eq v7, v0, :cond_0

    .line 43299
    invoke-direct {p0}, LX/0LN;->m()V

    .line 43300
    :cond_0
    iget-object v0, p0, LX/0LN;->h:Landroid/media/AudioTrack;

    if-nez v0, :cond_1

    .line 43301
    new-instance v0, Landroid/media/AudioTrack;

    iget v1, p0, LX/0LN;->d:I

    const/16 v2, 0xfa0

    const/4 v3, 0x4

    const/4 v6, 0x0

    move v4, v8

    move v5, v8

    invoke-direct/range {v0 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    iput-object v0, p0, LX/0LN;->h:Landroid/media/AudioTrack;

    .line 43302
    :cond_1
    iget-object v0, p0, LX/0LN;->g:LX/0LH;

    iget-object v1, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    invoke-static {p0}, LX/0LN;->s(LX/0LN;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0LH;->a(Landroid/media/AudioTrack;Z)V

    .line 43303
    invoke-direct {p0}, LX/0LN;->l()V

    .line 43304
    return v7

    .line 43305
    :cond_2
    new-instance v0, Landroid/media/AudioTrack;

    iget v1, p0, LX/0LN;->d:I

    iget v2, p0, LX/0LN;->j:I

    iget v3, p0, LX/0LN;->k:I

    iget v4, p0, LX/0LN;->l:I

    iget v5, p0, LX/0LN;->o:I

    move v7, p1

    invoke-direct/range {v0 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    iput-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    goto :goto_0
.end method

.method public final a(Ljava/nio/ByteBuffer;IIJ)I
    .locals 10

    .prologue
    .line 43306
    if-nez p3, :cond_1

    .line 43307
    const/4 v0, 0x2

    .line 43308
    :cond_0
    :goto_0
    return v0

    .line 43309
    :cond_1
    invoke-static {p0}, LX/0LN;->s(LX/0LN;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 43310
    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 43311
    const/4 v0, 0x0

    goto :goto_0

    .line 43312
    :cond_2
    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v0}, LX/0LH;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 43313
    const/4 v0, 0x0

    goto :goto_0

    .line 43314
    :cond_3
    const/4 v2, 0x0

    .line 43315
    iget v0, p0, LX/0LN;->H:I

    if-nez v0, :cond_11

    .line 43316
    iput p3, p0, LX/0LN;->H:I

    .line 43317
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 43318
    iget-boolean v0, p0, LX/0LN;->m:Z

    if-eqz v0, :cond_4

    iget v0, p0, LX/0LN;->z:I

    if-nez v0, :cond_4

    .line 43319
    iget v0, p0, LX/0LN;->l:I

    invoke-static {v0, p1}, LX/0LN;->a(ILjava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, LX/0LN;->z:I

    .line 43320
    :cond_4
    iget-boolean v0, p0, LX/0LN;->m:Z

    if-eqz v0, :cond_9

    iget v0, p0, LX/0LN;->z:I

    int-to-long v0, v0

    .line 43321
    :goto_1
    invoke-direct {p0, v0, v1}, LX/0LN;->b(J)J

    move-result-wide v0

    .line 43322
    sub-long v0, p4, v0

    .line 43323
    iget v3, p0, LX/0LN;->A:I

    if-nez v3, :cond_a

    .line 43324
    const-wide/16 v4, 0x0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/0LN;->B:J

    .line 43325
    const/4 v0, 0x1

    iput v0, p0, LX/0LN;->A:I

    move v0, v2

    .line 43326
    :goto_2
    sget v1, LX/08x;->a:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_7

    .line 43327
    iget-object v1, p0, LX/0LN;->F:[B

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/0LN;->F:[B

    array-length v1, v1

    if-ge v1, p3, :cond_6

    .line 43328
    :cond_5
    new-array v1, p3, [B

    iput-object v1, p0, LX/0LN;->F:[B

    .line 43329
    :cond_6
    iget-object v1, p0, LX/0LN;->F:[B

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, p3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 43330
    const/4 v1, 0x0

    iput v1, p0, LX/0LN;->G:I

    .line 43331
    :cond_7
    :goto_3
    const/4 v1, 0x0

    .line 43332
    sget v2, LX/08x;->a:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_c

    .line 43333
    iget-wide v2, p0, LX/0LN;->x:J

    iget-object v4, p0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v4}, LX/0LH;->b()J

    move-result-wide v4

    iget v6, p0, LX/0LN;->n:I

    int-to-long v6, v6

    mul-long/2addr v4, v6

    sub-long/2addr v2, v4

    long-to-int v2, v2

    .line 43334
    iget v3, p0, LX/0LN;->o:I

    sub-int v2, v3, v2

    .line 43335
    if-lez v2, :cond_8

    .line 43336
    iget v1, p0, LX/0LN;->H:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 43337
    iget-object v2, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    iget-object v3, p0, LX/0LN;->F:[B

    iget v4, p0, LX/0LN;->G:I

    invoke-virtual {v2, v3, v4, v1}, Landroid/media/AudioTrack;->write([BII)I

    move-result v1

    .line 43338
    if-ltz v1, :cond_8

    .line 43339
    iget v2, p0, LX/0LN;->G:I

    add-int/2addr v2, v1

    iput v2, p0, LX/0LN;->G:I

    .line 43340
    :cond_8
    :goto_4
    if-gez v1, :cond_d

    .line 43341
    new-instance v0, LX/0LM;

    invoke-direct {v0, v1}, LX/0LM;-><init>(I)V

    throw v0

    .line 43342
    :cond_9
    int-to-long v0, p3

    invoke-direct {p0, v0, v1}, LX/0LN;->a(J)J

    move-result-wide v0

    goto :goto_1

    .line 43343
    :cond_a
    iget-wide v4, p0, LX/0LN;->B:J

    invoke-direct {p0}, LX/0LN;->q()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, LX/0LN;->b(J)J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 43344
    iget v3, p0, LX/0LN;->A:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_b

    sub-long v6, v4, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    const-wide/32 v8, 0x30d40

    cmp-long v3, v6, v8

    if-lez v3, :cond_b

    .line 43345
    const-string v3, "AudioTrack"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Discontinuity detected [expected "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", got "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 43346
    const/4 v3, 0x2

    iput v3, p0, LX/0LN;->A:I

    .line 43347
    :cond_b
    iget v3, p0, LX/0LN;->A:I

    const/4 v6, 0x2

    if-ne v3, v6, :cond_10

    .line 43348
    iget-wide v2, p0, LX/0LN;->B:J

    sub-long/2addr v0, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0LN;->B:J

    .line 43349
    const/4 v0, 0x1

    iput v0, p0, LX/0LN;->A:I

    .line 43350
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 43351
    :cond_c
    iget-object v1, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    iget v2, p0, LX/0LN;->H:I

    .line 43352
    const/4 v3, 0x1

    invoke-virtual {v1, p1, v2, v3}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;II)I

    move-result v3

    move v1, v3

    .line 43353
    goto :goto_4

    .line 43354
    :cond_d
    iget v2, p0, LX/0LN;->H:I

    sub-int/2addr v2, v1

    iput v2, p0, LX/0LN;->H:I

    .line 43355
    iget-boolean v2, p0, LX/0LN;->m:Z

    if-nez v2, :cond_e

    .line 43356
    iget-wide v2, p0, LX/0LN;->x:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0LN;->x:J

    .line 43357
    :cond_e
    iget v1, p0, LX/0LN;->H:I

    if-nez v1, :cond_0

    .line 43358
    iget-boolean v1, p0, LX/0LN;->m:Z

    if-eqz v1, :cond_f

    .line 43359
    iget-wide v2, p0, LX/0LN;->y:J

    iget v1, p0, LX/0LN;->z:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0LN;->y:J

    .line 43360
    :cond_f
    or-int/lit8 v0, v0, 0x2

    goto/16 :goto_0

    :cond_10
    move v0, v2

    goto/16 :goto_2

    :cond_11
    move v0, v2

    goto/16 :goto_3
.end method

.method public final a(Z)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 43361
    invoke-virtual {p0}, LX/0LN;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, LX/0LN;->A:I

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 43362
    if-nez v0, :cond_1

    .line 43363
    const-wide/high16 v0, -0x8000000000000000L

    .line 43364
    :cond_0
    :goto_1
    return-wide v0

    .line 43365
    :cond_1
    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 43366
    invoke-direct {p0}, LX/0LN;->o()V

    .line 43367
    :cond_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    div-long/2addr v0, v4

    .line 43368
    iget-boolean v2, p0, LX/0LN;->u:Z

    if-eqz v2, :cond_3

    .line 43369
    iget-object v2, p0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v2}, LX/0LH;->e()J

    move-result-wide v2

    div-long/2addr v2, v4

    sub-long/2addr v0, v2

    .line 43370
    long-to-float v0, v0

    iget-object v1, p0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v1}, LX/0LH;->g()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-long v0, v0

    .line 43371
    invoke-direct {p0, v0, v1}, LX/0LN;->c(J)J

    move-result-wide v0

    .line 43372
    iget-object v2, p0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v2}, LX/0LH;->f()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 43373
    invoke-direct {p0, v0, v1}, LX/0LN;->b(J)J

    move-result-wide v0

    iget-wide v2, p0, LX/0LN;->B:J

    add-long/2addr v0, v2

    .line 43374
    goto :goto_1

    .line 43375
    :cond_3
    iget v2, p0, LX/0LN;->r:I

    if-nez v2, :cond_4

    .line 43376
    iget-object v0, p0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v0}, LX/0LH;->c()J

    move-result-wide v0

    iget-wide v2, p0, LX/0LN;->B:J

    add-long/2addr v0, v2

    .line 43377
    :goto_2
    if-nez p1, :cond_0

    .line 43378
    iget-wide v2, p0, LX/0LN;->D:J

    sub-long/2addr v0, v2

    goto :goto_1

    .line 43379
    :cond_4
    iget-wide v2, p0, LX/0LN;->s:J

    add-long/2addr v0, v2

    iget-wide v2, p0, LX/0LN;->B:J

    add-long/2addr v0, v2

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 43380
    iget v0, p0, LX/0LN;->E:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 43381
    iput p1, p0, LX/0LN;->E:F

    .line 43382
    invoke-direct {p0}, LX/0LN;->l()V

    .line 43383
    :cond_0
    return-void
.end method

.method public final a(Landroid/media/MediaFormat;Z)V
    .locals 1

    .prologue
    .line 43384
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/0LN;->a(Landroid/media/MediaFormat;ZI)V

    .line 43385
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 43386
    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 43387
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0LN;->a(I)I

    move-result v0

    return v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 43388
    iget-wide v0, p0, LX/0LN;->p:J

    return-wide v0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 43389
    invoke-virtual {p0}, LX/0LN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43390
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, LX/0LN;->C:J

    .line 43391
    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 43392
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 9

    .prologue
    .line 43393
    invoke-virtual {p0}, LX/0LN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43394
    iget-object v0, p0, LX/0LN;->g:LX/0LH;

    invoke-direct {p0}, LX/0LN;->q()J

    move-result-wide v2

    .line 43395
    invoke-virtual {v0}, LX/0LH;->b()J

    move-result-wide v4

    iput-wide v4, v0, LX/0LH;->h:J

    .line 43396
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    iput-wide v4, v0, LX/0LH;->g:J

    .line 43397
    iput-wide v2, v0, LX/0LH;->i:J

    .line 43398
    iget-object v4, v0, LX/0LH;->a:Landroid/media/AudioTrack;

    invoke-virtual {v4}, Landroid/media/AudioTrack;->stop()V

    .line 43399
    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 4

    .prologue
    .line 43400
    invoke-virtual {p0}, LX/0LN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, LX/0LN;->q()J

    move-result-wide v0

    iget-object v2, p0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v2}, LX/0LH;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 43401
    invoke-static {p0}, LX/0LN;->s(LX/0LN;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 43402
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()V
    .locals 5

    .prologue
    .line 43403
    invoke-virtual {p0}, LX/0LN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43404
    invoke-direct {p0}, LX/0LN;->r()V

    .line 43405
    iget-object v0, p0, LX/0LN;->g:LX/0LH;

    .line 43406
    iget-wide v1, v0, LX/0LH;->g:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    .line 43407
    :cond_0
    :goto_0
    return-void

    .line 43408
    :cond_1
    iget-object v1, v0, LX/0LH;->a:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->pause()V

    goto :goto_0
.end method

.method public final j()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 43409
    invoke-virtual {p0}, LX/0LN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43410
    iput-wide v4, p0, LX/0LN;->x:J

    .line 43411
    iput-wide v4, p0, LX/0LN;->y:J

    .line 43412
    iput v2, p0, LX/0LN;->z:I

    .line 43413
    iput v2, p0, LX/0LN;->H:I

    .line 43414
    iput v2, p0, LX/0LN;->A:I

    .line 43415
    iput-wide v4, p0, LX/0LN;->D:J

    .line 43416
    invoke-direct {p0}, LX/0LN;->r()V

    .line 43417
    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    .line 43418
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 43419
    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    .line 43420
    :cond_0
    iget-object v0, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    .line 43421
    iput-object v3, p0, LX/0LN;->i:Landroid/media/AudioTrack;

    .line 43422
    iget-object v1, p0, LX/0LN;->g:LX/0LH;

    invoke-virtual {v1, v3, v2}, LX/0LH;->a(Landroid/media/AudioTrack;Z)V

    .line 43423
    iget-object v1, p0, LX/0LN;->e:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 43424
    new-instance v1, Lcom/google/android/exoplayer/audio/AudioTrack$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/exoplayer/audio/AudioTrack$1;-><init>(LX/0LN;Landroid/media/AudioTrack;)V

    invoke-virtual {v1}, Lcom/google/android/exoplayer/audio/AudioTrack$1;->start()V

    .line 43425
    :cond_1
    return-void
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 43426
    invoke-virtual {p0}, LX/0LN;->j()V

    .line 43427
    invoke-direct {p0}, LX/0LN;->m()V

    .line 43428
    return-void
.end method
