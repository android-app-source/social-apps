.class public final enum LX/04g;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/04g;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/04g;

.field public static final enum BY_ANDROID:LX/04g;

.field public static final enum BY_AUTOPLAY:LX/04g;

.field public static final enum BY_BACKGROUND_PLAY:LX/04g;

.field public static final enum BY_BOOKMARK:LX/04g;

.field public static final enum BY_CHAT_HEADS_COLLAPSE:LX/04g;

.field public static final enum BY_CHROME_CAST:LX/04g;

.field public static final enum BY_COMMERCIAL_BREAK:LX/04g;

.field public static final enum BY_DEBUG_SILENT:LX/04g;

.field public static final enum BY_DIALOG:LX/04g;

.field public static final enum BY_DIVEBAR:LX/04g;

.field public static final enum BY_FLYOUT:LX/04g;

.field public static final enum BY_INLINE_CHANNEL_FEED_TRANSITION:LX/04g;

.field public static final enum BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

.field public static final enum BY_JEWEL:LX/04g;

.field public static final enum BY_LIVE_POLLER_TRANSITION:LX/04g;

.field public static final enum BY_LIVE_STREAM_EOF:LX/04g;

.field public static final enum BY_MANAGER:LX/04g;

.field public static final enum BY_MEDIA_TRAY_DISMISS:LX/04g;

.field public static final enum BY_NEWSFEED_OCCLUSION:LX/04g;

.field public static final enum BY_NEWSFEED_ONPAUSE:LX/04g;

.field public static final enum BY_ORIENTATION_CHANGE:LX/04g;

.field public static final enum BY_PAGE_VIDEOLIST_STORY:LX/04g;

.field public static final enum BY_PLAYER:LX/04g;

.field public static final enum BY_PLAYER_INTERNAL_ERROR:LX/04g;

.field public static final enum BY_PREPARER:LX/04g;

.field public static final enum BY_REPORTING_FLOW:LX/04g;

.field public static final enum BY_SEEKBAR_CONTROLLER:LX/04g;

.field public static final enum BY_THREAD_VIEW_DISMISS:LX/04g;

.field public static final enum BY_USER:LX/04g;

.field public static final enum BY_USER_SWIPE:LX/04g;

.field public static final enum BY_VIDEO_CHAINING_TAP:LX/04g;

.field public static final enum BY_VIDEO_HOME_END:LX/04g;

.field public static final enum BY_VIDEO_HOME_PAUSE:LX/04g;

.field public static final enum BY_WAIT_FOR_LIVE_VOD_TRANSITION:LX/04g;

.field public static final UNSET:LX/04g;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 13998
    new-instance v0, LX/04g;

    const-string v1, "BY_USER"

    const-string v2, "user_initiated"

    invoke-direct {v0, v1, v4, v2}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_USER:LX/04g;

    .line 13999
    new-instance v0, LX/04g;

    const-string v1, "BY_USER_SWIPE"

    const-string v2, "user_swipe"

    invoke-direct {v0, v1, v5, v2}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_USER_SWIPE:LX/04g;

    .line 14000
    new-instance v0, LX/04g;

    const-string v1, "BY_PLAYER"

    const-string v2, "player_initiated"

    invoke-direct {v0, v1, v6, v2}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_PLAYER:LX/04g;

    .line 14001
    new-instance v0, LX/04g;

    const-string v1, "BY_MANAGER"

    const-string v2, "manager_initiated"

    invoke-direct {v0, v1, v7, v2}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_MANAGER:LX/04g;

    .line 14002
    new-instance v0, LX/04g;

    const-string v1, "BY_AUTOPLAY"

    const-string v2, "autoplay_initiated"

    invoke-direct {v0, v1, v8, v2}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    .line 14003
    new-instance v0, LX/04g;

    const-string v1, "BY_ANDROID"

    const/4 v2, 0x5

    const-string v3, "android_initiated"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_ANDROID:LX/04g;

    .line 14004
    new-instance v0, LX/04g;

    const-string v1, "BY_FLYOUT"

    const/4 v2, 0x6

    const-string v3, "flyout"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_FLYOUT:LX/04g;

    .line 14005
    new-instance v0, LX/04g;

    const-string v1, "BY_DIVEBAR"

    const/4 v2, 0x7

    const-string v3, "divebar"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_DIVEBAR:LX/04g;

    .line 14006
    new-instance v0, LX/04g;

    const-string v1, "BY_BOOKMARK"

    const/16 v2, 0x8

    const-string v3, "bookmark"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_BOOKMARK:LX/04g;

    .line 14007
    new-instance v0, LX/04g;

    const-string v1, "BY_JEWEL"

    const/16 v2, 0x9

    const-string v3, "jewel"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_JEWEL:LX/04g;

    .line 14008
    new-instance v0, LX/04g;

    const-string v1, "BY_DIALOG"

    const/16 v2, 0xa

    const-string v3, "dialog"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_DIALOG:LX/04g;

    .line 14009
    new-instance v0, LX/04g;

    const-string v1, "BY_NEWSFEED_OCCLUSION"

    const/16 v2, 0xb

    const-string v3, "nf_occlusion"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_NEWSFEED_OCCLUSION:LX/04g;

    .line 14010
    new-instance v0, LX/04g;

    const-string v1, "BY_NEWSFEED_ONPAUSE"

    const/16 v2, 0xc

    const-string v3, "nf_onpause"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_NEWSFEED_ONPAUSE:LX/04g;

    .line 14011
    new-instance v0, LX/04g;

    const-string v1, "BY_ORIENTATION_CHANGE"

    const/16 v2, 0xd

    const-string v3, "orientation_change"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_ORIENTATION_CHANGE:LX/04g;

    .line 14012
    new-instance v0, LX/04g;

    const-string v1, "BY_VIDEO_CHAINING_TAP"

    const/16 v2, 0xe

    const-string v3, "video_chaining_tap"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_VIDEO_CHAINING_TAP:LX/04g;

    .line 14013
    new-instance v0, LX/04g;

    const-string v1, "BY_INLINE_CHANNEL_FEED_TRANSITION"

    const/16 v2, 0xf

    const-string v3, "inline_channel_feed_transition"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_INLINE_CHANNEL_FEED_TRANSITION:LX/04g;

    .line 14014
    new-instance v0, LX/04g;

    const-string v1, "BY_INLINE_FULLSCREEN_TRANSITION"

    const/16 v2, 0x10

    const-string v3, "inline_fullscreen_transition"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    .line 14015
    new-instance v0, LX/04g;

    const-string v1, "BY_THREAD_VIEW_DISMISS"

    const/16 v2, 0x11

    const-string v3, "thread_view_dismiss"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_THREAD_VIEW_DISMISS:LX/04g;

    .line 14016
    new-instance v0, LX/04g;

    const-string v1, "BY_MEDIA_TRAY_DISMISS"

    const/16 v2, 0x12

    const-string v3, "media_tray_dismiss"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_MEDIA_TRAY_DISMISS:LX/04g;

    .line 14017
    new-instance v0, LX/04g;

    const-string v1, "BY_SEEKBAR_CONTROLLER"

    const/16 v2, 0x13

    const-string v3, "seek_controller"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_SEEKBAR_CONTROLLER:LX/04g;

    .line 14018
    new-instance v0, LX/04g;

    const-string v1, "BY_DEBUG_SILENT"

    const/16 v2, 0x14

    const-string v3, "debug_silent"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_DEBUG_SILENT:LX/04g;

    .line 14019
    new-instance v0, LX/04g;

    const-string v1, "BY_PAGE_VIDEOLIST_STORY"

    const/16 v2, 0x15

    const-string v3, "page_videolist_story"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_PAGE_VIDEOLIST_STORY:LX/04g;

    .line 14020
    new-instance v0, LX/04g;

    const-string v1, "BY_PLAYER_INTERNAL_ERROR"

    const/16 v2, 0x16

    const-string v3, "player_internal_error"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_PLAYER_INTERNAL_ERROR:LX/04g;

    .line 14021
    new-instance v0, LX/04g;

    const-string v1, "BY_PREPARER"

    const/16 v2, 0x17

    const-string v3, "video_prepare_controller"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_PREPARER:LX/04g;

    .line 14022
    new-instance v0, LX/04g;

    const-string v1, "BY_CHROME_CAST"

    const/16 v2, 0x18

    const-string v3, "chrome_cast"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_CHROME_CAST:LX/04g;

    .line 14023
    new-instance v0, LX/04g;

    const-string v1, "BY_REPORTING_FLOW"

    const/16 v2, 0x19

    const-string v3, "reporting_flow"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_REPORTING_FLOW:LX/04g;

    .line 14024
    new-instance v0, LX/04g;

    const-string v1, "BY_COMMERCIAL_BREAK"

    const/16 v2, 0x1a

    const-string v3, "commercial_break"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_COMMERCIAL_BREAK:LX/04g;

    .line 14025
    new-instance v0, LX/04g;

    const-string v1, "BY_CHAT_HEADS_COLLAPSE"

    const/16 v2, 0x1b

    const-string v3, "chat_heads_collapse"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_CHAT_HEADS_COLLAPSE:LX/04g;

    .line 14026
    new-instance v0, LX/04g;

    const-string v1, "BY_LIVE_POLLER_TRANSITION"

    const/16 v2, 0x1c

    const-string v3, "live_poller_transition"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_LIVE_POLLER_TRANSITION:LX/04g;

    .line 14027
    new-instance v0, LX/04g;

    const-string v1, "BY_WAIT_FOR_LIVE_VOD_TRANSITION"

    const/16 v2, 0x1d

    const-string v3, "wait_for_live_vod_transition"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_WAIT_FOR_LIVE_VOD_TRANSITION:LX/04g;

    .line 14028
    new-instance v0, LX/04g;

    const-string v1, "BY_LIVE_STREAM_EOF"

    const/16 v2, 0x1e

    const-string v3, "live_stream_eof"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_LIVE_STREAM_EOF:LX/04g;

    .line 14029
    new-instance v0, LX/04g;

    const-string v1, "BY_VIDEO_HOME_END"

    const/16 v2, 0x1f

    const-string v3, "video_home_end"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_VIDEO_HOME_END:LX/04g;

    .line 14030
    new-instance v0, LX/04g;

    const-string v1, "BY_VIDEO_HOME_PAUSE"

    const/16 v2, 0x20

    const-string v3, "video_home_pause"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_VIDEO_HOME_PAUSE:LX/04g;

    .line 14031
    new-instance v0, LX/04g;

    const-string v1, "BY_BACKGROUND_PLAY"

    const/16 v2, 0x21

    const-string v3, "background_play"

    invoke-direct {v0, v1, v2, v3}, LX/04g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/04g;->BY_BACKGROUND_PLAY:LX/04g;

    .line 14032
    const/16 v0, 0x22

    new-array v0, v0, [LX/04g;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    aput-object v1, v0, v4

    sget-object v1, LX/04g;->BY_USER_SWIPE:LX/04g;

    aput-object v1, v0, v5

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    aput-object v1, v0, v6

    sget-object v1, LX/04g;->BY_MANAGER:LX/04g;

    aput-object v1, v0, v7

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/04g;->BY_ANDROID:LX/04g;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/04g;->BY_FLYOUT:LX/04g;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/04g;->BY_DIVEBAR:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/04g;->BY_BOOKMARK:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/04g;->BY_JEWEL:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/04g;->BY_DIALOG:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/04g;->BY_NEWSFEED_OCCLUSION:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/04g;->BY_NEWSFEED_ONPAUSE:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/04g;->BY_ORIENTATION_CHANGE:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/04g;->BY_VIDEO_CHAINING_TAP:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/04g;->BY_INLINE_CHANNEL_FEED_TRANSITION:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/04g;->BY_THREAD_VIEW_DISMISS:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/04g;->BY_MEDIA_TRAY_DISMISS:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/04g;->BY_SEEKBAR_CONTROLLER:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/04g;->BY_DEBUG_SILENT:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/04g;->BY_PAGE_VIDEOLIST_STORY:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/04g;->BY_PLAYER_INTERNAL_ERROR:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/04g;->BY_PREPARER:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/04g;->BY_CHROME_CAST:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/04g;->BY_REPORTING_FLOW:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/04g;->BY_COMMERCIAL_BREAK:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/04g;->BY_CHAT_HEADS_COLLAPSE:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/04g;->BY_LIVE_POLLER_TRANSITION:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/04g;->BY_WAIT_FOR_LIVE_VOD_TRANSITION:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/04g;->BY_LIVE_STREAM_EOF:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/04g;->BY_VIDEO_HOME_END:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/04g;->BY_VIDEO_HOME_PAUSE:LX/04g;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/04g;->BY_BACKGROUND_PLAY:LX/04g;

    aput-object v2, v0, v1

    sput-object v0, LX/04g;->$VALUES:[LX/04g;

    .line 14033
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    sput-object v0, LX/04g;->UNSET:LX/04g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 14034
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14035
    iput-object p3, p0, LX/04g;->value:Ljava/lang/String;

    .line 14036
    return-void
.end method

.method public static asEventTriggerType(Ljava/lang/String;)LX/04g;
    .locals 5

    .prologue
    .line 14037
    invoke-static {}, LX/04g;->values()[LX/04g;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 14038
    iget-object v4, v3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 14039
    return-object v3

    .line 14040
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14041
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/04g;
    .locals 1

    .prologue
    .line 14042
    const-class v0, LX/04g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/04g;

    return-object v0
.end method

.method public static values()[LX/04g;
    .locals 1

    .prologue
    .line 14043
    sget-object v0, LX/04g;->$VALUES:[LX/04g;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/04g;

    return-object v0
.end method
