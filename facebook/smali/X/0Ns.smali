.class public final LX/0Ns;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0L9;
.implements LX/0L8;
.implements LX/0LX;


# instance fields
.field private A:LX/0LP;

.field public B:LX/0Nu;

.field public C:LX/0Nu;

.field private D:LX/0ON;

.field private E:Ljava/io/IOException;

.field private F:I

.field private G:J

.field private H:J

.field private final a:LX/0Ni;

.field private final b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/0Nj;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:I

.field private final e:LX/0LW;

.field public final f:I

.field private final g:LX/0Gb;

.field public final h:Landroid/os/Handler;

.field public final i:LX/0Nr;

.field private j:I

.field public k:Z

.field private l:Z

.field private m:I

.field public n:I

.field private o:LX/0AR;

.field private p:[LX/0L4;

.field private q:[Z

.field private r:[Z

.field private s:[LX/0L4;

.field private t:[I

.field private u:[I

.field public v:[Z

.field private w:J

.field private x:J

.field public y:J

.field public z:Z


# direct methods
.method public constructor <init>(LX/0Ni;LX/0Gb;I)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 52068
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, LX/0Ns;-><init>(LX/0Ni;LX/0Gb;ILandroid/os/Handler;LX/0Nr;I)V

    .line 52069
    return-void
.end method

.method private constructor <init>(LX/0Ni;LX/0Gb;ILandroid/os/Handler;LX/0Nr;I)V
    .locals 8

    .prologue
    .line 52070
    const/4 v7, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, LX/0Ns;-><init>(LX/0Ni;LX/0Gb;ILandroid/os/Handler;LX/0Nr;II)V

    .line 52071
    return-void
.end method

.method private constructor <init>(LX/0Ni;LX/0Gb;ILandroid/os/Handler;LX/0Nr;II)V
    .locals 2

    .prologue
    .line 52072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52073
    iput-object p1, p0, LX/0Ns;->a:LX/0Ni;

    .line 52074
    iput-object p2, p0, LX/0Ns;->g:LX/0Gb;

    .line 52075
    iput p3, p0, LX/0Ns;->d:I

    .line 52076
    iput p7, p0, LX/0Ns;->c:I

    .line 52077
    iput-object p4, p0, LX/0Ns;->h:Landroid/os/Handler;

    .line 52078
    iput-object p5, p0, LX/0Ns;->i:LX/0Nr;

    .line 52079
    iput p6, p0, LX/0Ns;->f:I

    .line 52080
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/0Ns;->y:J

    .line 52081
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    .line 52082
    new-instance v0, LX/0LW;

    invoke-direct {v0}, LX/0LW;-><init>()V

    iput-object v0, p0, LX/0Ns;->e:LX/0LW;

    .line 52083
    return-void
.end method

.method private static a(LX/0L4;LX/0AR;Ljava/lang/String;)LX/0L4;
    .locals 6

    .prologue
    const/4 v0, -0x1

    .line 52084
    iget v1, p1, LX/0AR;->f:I

    if-ne v1, v0, :cond_0

    move v3, v0

    .line 52085
    :goto_0
    iget v1, p1, LX/0AR;->g:I

    if-ne v1, v0, :cond_1

    move v4, v0

    .line 52086
    :goto_1
    iget-object v1, p1, LX/0AR;->a:Ljava/lang/String;

    iget v2, p1, LX/0AR;->c:I

    move-object v0, p0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, LX/0L4;->a(Ljava/lang/String;IIILjava/lang/String;)LX/0L4;

    move-result-object v0

    return-object v0

    .line 52087
    :cond_0
    iget v3, p1, LX/0AR;->f:I

    goto :goto_0

    .line 52088
    :cond_1
    iget v4, p1, LX/0AR;->g:I

    goto :goto_1
.end method

.method private a(IZ)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 52089
    iget-object v0, p0, LX/0Ns;->q:[Z

    aget-boolean v0, v0, p1

    if-eq v0, p2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 52090
    iget-object v0, p0, LX/0Ns;->u:[I

    aget v0, v0, p1

    .line 52091
    iget-object v3, p0, LX/0Ns;->v:[Z

    aget-boolean v3, v3, v0

    if-eq v3, p2, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, LX/0Av;->b(Z)V

    .line 52092
    iget-object v2, p0, LX/0Ns;->q:[Z

    aput-boolean p2, v2, p1

    .line 52093
    iget-object v2, p0, LX/0Ns;->v:[Z

    aput-boolean p2, v2, v0

    .line 52094
    iget v0, p0, LX/0Ns;->n:I

    if-eqz p2, :cond_2

    :goto_1
    add-int/2addr v0, v1

    iput v0, p0, LX/0Ns;->n:I

    .line 52095
    return-void

    :cond_1
    move v0, v2

    .line 52096
    goto :goto_0

    .line 52097
    :cond_2
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private a(JIILX/0AR;JJ)V
    .locals 12

    .prologue
    .line 52098
    iget-object v0, p0, LX/0Ns;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Ns;->i:LX/0Nr;

    if-eqz v0, :cond_0

    .line 52099
    iget-object v0, p0, LX/0Ns;->h:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/hls/HlsSampleSource$1;

    move-object v2, p0

    move-wide v3, p1

    move v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    invoke-direct/range {v1 .. v11}, Lcom/google/android/exoplayer/hls/HlsSampleSource$1;-><init>(LX/0Ns;JIILX/0AR;JJ)V

    const v2, -0x3e393944

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 52100
    :cond_0
    return-void
.end method

.method private a(JIILX/0AR;JJJJ)V
    .locals 18

    .prologue
    .line 52101
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Ns;->h:Landroid/os/Handler;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Ns;->i:LX/0Nr;

    if-eqz v2, :cond_0

    .line 52102
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Ns;->h:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/exoplayer/hls/HlsSampleSource$2;

    move-object/from16 v4, p0

    move-wide/from16 v5, p1

    move/from16 v7, p3

    move/from16 v8, p4

    move-object/from16 v9, p5

    move-wide/from16 v10, p6

    move-wide/from16 v12, p8

    move-wide/from16 v14, p10

    move-wide/from16 v16, p12

    invoke-direct/range {v3 .. v17}, Lcom/google/android/exoplayer/hls/HlsSampleSource$2;-><init>(LX/0Ns;JIILX/0AR;JJJJ)V

    const v4, 0xb14dc6f

    invoke-static {v2, v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 52103
    :cond_0
    return-void
.end method

.method private a(LX/0AR;IJ)V
    .locals 7

    .prologue
    .line 52200
    iget-object v0, p0, LX/0Ns;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Ns;->i:LX/0Nr;

    if-eqz v0, :cond_0

    .line 52201
    iget-object v6, p0, LX/0Ns;->h:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/exoplayer/hls/HlsSampleSource$5;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer/hls/HlsSampleSource$5;-><init>(LX/0Ns;LX/0AR;IJ)V

    const v1, -0x3fcbae39

    invoke-static {v6, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 52202
    :cond_0
    return-void
.end method

.method private a(LX/0Nj;)V
    .locals 14

    .prologue
    .line 52104
    const/4 v3, 0x0

    .line 52105
    const/4 v0, -0x1

    .line 52106
    invoke-virtual {p1}, LX/0Nj;->e()I

    move-result v7

    .line 52107
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v7, :cond_4

    .line 52108
    invoke-virtual {p1, v1}, LX/0Nj;->b(I)LX/0L4;

    move-result-object v2

    iget-object v2, v2, LX/0L4;->b:Ljava/lang/String;

    .line 52109
    invoke-static {v2}, LX/0Al;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 52110
    const/4 v2, 0x3

    .line 52111
    :goto_1
    if-le v2, v3, :cond_3

    move v0, v1

    .line 52112
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    goto :goto_0

    .line 52113
    :cond_0
    invoke-static {v2}, LX/0Al;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 52114
    const/4 v2, 0x2

    goto :goto_1

    .line 52115
    :cond_1
    invoke-static {v2}, LX/0Al;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 52116
    const/4 v2, 0x1

    goto :goto_1

    .line 52117
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 52118
    :cond_3
    if-ne v2, v3, :cond_d

    const/4 v2, -0x1

    if-eq v0, v2, :cond_d

    .line 52119
    const/4 v0, -0x1

    move v2, v3

    goto :goto_2

    .line 52120
    :cond_4
    iget-object v1, p0, LX/0Ns;->a:LX/0Ni;

    .line 52121
    iget-object v2, v1, LX/0Ni;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v8, v2

    .line 52122
    const/4 v1, -0x1

    if-eq v0, v1, :cond_6

    const/4 v1, 0x1

    .line 52123
    :goto_3
    iput v7, p0, LX/0Ns;->m:I

    .line 52124
    if-eqz v1, :cond_5

    .line 52125
    iget v1, p0, LX/0Ns;->m:I

    add-int/lit8 v2, v8, -0x1

    add-int/2addr v1, v2

    iput v1, p0, LX/0Ns;->m:I

    .line 52126
    :cond_5
    iget v1, p0, LX/0Ns;->m:I

    new-array v1, v1, [LX/0L4;

    iput-object v1, p0, LX/0Ns;->p:[LX/0L4;

    .line 52127
    iget v1, p0, LX/0Ns;->m:I

    new-array v1, v1, [Z

    iput-object v1, p0, LX/0Ns;->q:[Z

    .line 52128
    iget v1, p0, LX/0Ns;->m:I

    new-array v1, v1, [Z

    iput-object v1, p0, LX/0Ns;->r:[Z

    .line 52129
    iget v1, p0, LX/0Ns;->m:I

    new-array v1, v1, [LX/0L4;

    iput-object v1, p0, LX/0Ns;->s:[LX/0L4;

    .line 52130
    iget v1, p0, LX/0Ns;->m:I

    new-array v1, v1, [I

    iput-object v1, p0, LX/0Ns;->t:[I

    .line 52131
    iget v1, p0, LX/0Ns;->m:I

    new-array v1, v1, [I

    iput-object v1, p0, LX/0Ns;->u:[I

    .line 52132
    new-array v1, v7, [Z

    iput-object v1, p0, LX/0Ns;->v:[Z

    .line 52133
    iget-object v1, p0, LX/0Ns;->a:LX/0Ni;

    invoke-virtual {v1}, LX/0Ni;->d()J

    move-result-wide v10

    .line 52134
    const/4 v2, 0x0

    .line 52135
    const/4 v1, 0x0

    move v6, v1

    :goto_4
    if-ge v6, v7, :cond_b

    .line 52136
    invoke-virtual {p1, v6}, LX/0Nj;->b(I)LX/0L4;

    move-result-object v1

    invoke-virtual {v1, v10, v11}, LX/0L4;->b(J)LX/0L4;

    move-result-object v9

    .line 52137
    const/4 v1, 0x0

    .line 52138
    iget-object v3, v9, LX/0L4;->b:Ljava/lang/String;

    invoke-static {v3}, LX/0Al;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 52139
    iget-object v1, p0, LX/0Ns;->a:LX/0Ni;

    .line 52140
    iget-object v3, v1, LX/0Ni;->d:LX/0Nl;

    iget-object v3, v3, LX/0Nl;->d:Ljava/lang/String;

    move-object v1, v3

    .line 52141
    move-object v5, v1

    .line 52142
    :goto_5
    if-ne v6, v0, :cond_a

    .line 52143
    const/4 v1, 0x0

    move v3, v1

    :goto_6
    if-ge v3, v8, :cond_9

    .line 52144
    iget-object v1, p0, LX/0Ns;->u:[I

    aput v6, v1, v2

    .line 52145
    iget-object v1, p0, LX/0Ns;->t:[I

    aput v3, v1, v2

    .line 52146
    iget-object v1, p0, LX/0Ns;->a:LX/0Ni;

    .line 52147
    iget-object v4, v1, LX/0Ni;->l:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Nf;

    iget-object v4, v4, LX/0Nf;->a:[LX/0Nv;

    .line 52148
    array-length v12, v4

    const/4 v13, 0x1

    if-ne v12, v13, :cond_e

    const/4 v12, 0x0

    aget-object v4, v4, v12

    :goto_7
    move-object v1, v4

    .line 52149
    iget-object v12, p0, LX/0Ns;->p:[LX/0L4;

    add-int/lit8 v4, v2, 0x1

    if-nez v1, :cond_8

    const/4 v1, 0x0

    invoke-virtual {v9, v1}, LX/0L4;->b(Ljava/lang/String;)LX/0L4;

    move-result-object v1

    :goto_8
    aput-object v1, v12, v2

    .line 52150
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v4

    goto :goto_6

    .line 52151
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 52152
    :cond_7
    const-string v3, "application/eia-608"

    iget-object v4, v9, LX/0L4;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 52153
    iget-object v1, p0, LX/0Ns;->a:LX/0Ni;

    .line 52154
    iget-object v3, v1, LX/0Ni;->d:LX/0Nl;

    iget-object v3, v3, LX/0Nl;->e:Ljava/lang/String;

    move-object v1, v3

    .line 52155
    move-object v5, v1

    goto :goto_5

    .line 52156
    :cond_8
    iget-object v1, v1, LX/0Nv;->b:LX/0AR;

    invoke-static {v9, v1, v5}, LX/0Ns;->a(LX/0L4;LX/0AR;Ljava/lang/String;)LX/0L4;

    move-result-object v1

    goto :goto_8

    :cond_9
    move v1, v2

    .line 52157
    :goto_9
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v2, v1

    goto :goto_4

    .line 52158
    :cond_a
    iget-object v1, p0, LX/0Ns;->u:[I

    aput v6, v1, v2

    .line 52159
    iget-object v1, p0, LX/0Ns;->t:[I

    const/4 v3, -0x1

    aput v3, v1, v2

    .line 52160
    iget-object v3, p0, LX/0Ns;->p:[LX/0L4;

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v9, v5}, LX/0L4;->a(Ljava/lang/String;)LX/0L4;

    move-result-object v4

    aput-object v4, v3, v2

    goto :goto_9

    .line 52161
    :cond_b
    return-void

    :cond_c
    move-object v5, v1

    goto :goto_5

    :cond_d
    move v2, v3

    goto/16 :goto_2

    :cond_e
    const/4 v4, 0x0

    goto :goto_7
.end method

.method public static c(J)J
    .locals 2

    .prologue
    .line 52162
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    return-wide v0
.end method

.method private d(J)V
    .locals 3

    .prologue
    .line 52163
    iput-wide p1, p0, LX/0Ns;->x:J

    .line 52164
    iput-wide p1, p0, LX/0Ns;->w:J

    .line 52165
    iget-object v0, p0, LX/0Ns;->r:[Z

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 52166
    iget-object v0, p0, LX/0Ns;->a:LX/0Ni;

    .line 52167
    iget-boolean v1, v0, LX/0Ni;->a:Z

    if-eqz v1, :cond_0

    .line 52168
    iget-object v1, v0, LX/0Ni;->g:LX/0Nt;

    .line 52169
    iget-object v0, v1, LX/0Nt;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 52170
    :cond_0
    invoke-direct {p0, p1, p2}, LX/0Ns;->e(J)V

    .line 52171
    return-void
.end method

.method private e(J)V
    .locals 1

    .prologue
    .line 52172
    iput-wide p1, p0, LX/0Ns;->y:J

    .line 52173
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Ns;->z:Z

    .line 52174
    iget-object v0, p0, LX/0Ns;->D:LX/0ON;

    .line 52175
    iget-boolean p1, v0, LX/0ON;->c:Z

    move v0, p1

    .line 52176
    if-eqz v0, :cond_0

    .line 52177
    iget-object v0, p0, LX/0Ns;->D:LX/0ON;

    invoke-virtual {v0}, LX/0ON;->b()V

    .line 52178
    :goto_0
    return-void

    .line 52179
    :cond_0
    invoke-direct {p0}, LX/0Ns;->h()V

    .line 52180
    invoke-direct {p0}, LX/0Ns;->j()V

    goto :goto_0
.end method

.method private g()LX/0Nj;
    .locals 4

    .prologue
    .line 52181
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nj;

    .line 52182
    :goto_0
    iget-object v1, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    const/4 v2, 0x0

    .line 52183
    invoke-virtual {v0}, LX/0Nj;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 52184
    :cond_0
    :goto_1
    move v1, v2

    .line 52185
    if-nez v1, :cond_1

    .line 52186
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nj;

    invoke-virtual {v0}, LX/0Nj;->c()V

    .line 52187
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nj;

    goto :goto_0

    .line 52188
    :cond_1
    return-object v0

    :cond_2
    move v1, v2

    .line 52189
    :goto_2
    iget-object v3, p0, LX/0Ns;->v:[Z

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 52190
    iget-object v3, p0, LX/0Ns;->v:[Z

    aget-boolean v3, v3, v1

    if-eqz v3, :cond_3

    invoke-virtual {v0, v1}, LX/0Nj;->c(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 52191
    const/4 v2, 0x1

    goto :goto_1

    .line 52192
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private h()V
    .locals 2

    .prologue
    .line 52193
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 52194
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nj;

    invoke-virtual {v0}, LX/0Nj;->c()V

    .line 52195
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 52196
    :cond_0
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 52197
    invoke-direct {p0}, LX/0Ns;->i()V

    .line 52198
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Ns;->C:LX/0Nu;

    .line 52199
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52062
    iput-object v0, p0, LX/0Ns;->B:LX/0Nu;

    .line 52063
    iput-object v0, p0, LX/0Ns;->A:LX/0LP;

    .line 52064
    iput-object v0, p0, LX/0Ns;->E:Ljava/io/IOException;

    .line 52065
    const/4 v0, 0x0

    iput v0, p0, LX/0Ns;->F:I

    .line 52066
    return-void
.end method

.method private j()V
    .locals 15

    .prologue
    .line 52010
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 52011
    invoke-static {p0}, LX/0Ns;->l(LX/0Ns;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 52012
    iget-wide v11, p0, LX/0Ns;->y:J

    .line 52013
    :goto_0
    move-wide v4, v11

    .line 52014
    iget-object v0, p0, LX/0Ns;->E:Ljava/io/IOException;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v7, v0

    .line 52015
    :goto_1
    iget-object v0, p0, LX/0Ns;->D:LX/0ON;

    .line 52016
    iget-boolean v1, v0, LX/0ON;->c:Z

    move v0, v1

    .line 52017
    if-nez v0, :cond_0

    if-eqz v7, :cond_3

    :cond_0
    const/4 v6, 0x1

    .line 52018
    :goto_2
    iget-object v0, p0, LX/0Ns;->g:LX/0Gb;

    iget-wide v2, p0, LX/0Ns;->w:J

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, LX/0Gb;->a(Ljava/lang/Object;JJZ)Z

    move-result v0

    .line 52019
    if-eqz v7, :cond_4

    .line 52020
    iget-wide v0, p0, LX/0Ns;->G:J

    sub-long v0, v8, v0

    .line 52021
    iget v2, p0, LX/0Ns;->F:I

    int-to-long v2, v2

    .line 52022
    const-wide/16 v11, 0x1

    sub-long v11, v2, v11

    const-wide/16 v13, 0x3e8

    mul-long/2addr v11, v13

    const-wide/16 v13, 0x1388

    invoke-static {v11, v12, v13, v14}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v11

    move-wide v2, v11

    .line 52023
    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 52024
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Ns;->E:Ljava/io/IOException;

    .line 52025
    iget-object v0, p0, LX/0Ns;->D:LX/0ON;

    iget-object v1, p0, LX/0Ns;->A:LX/0LP;

    invoke-virtual {v0, v1, p0}, LX/0ON;->a(LX/0LO;LX/0LX;)V

    .line 52026
    :cond_1
    :goto_3
    return-void

    .line 52027
    :cond_2
    const/4 v0, 0x0

    move v7, v0

    goto :goto_1

    .line 52028
    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    .line 52029
    :cond_4
    iget-object v1, p0, LX/0Ns;->D:LX/0ON;

    .line 52030
    iget-boolean v2, v1, LX/0ON;->c:Z

    move v1, v2

    .line 52031
    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/0Ns;->k:Z

    if-eqz v0, :cond_5

    iget v0, p0, LX/0Ns;->n:I

    if-eqz v0, :cond_1

    .line 52032
    :cond_5
    iget-object v2, p0, LX/0Ns;->a:LX/0Ni;

    iget-object v3, p0, LX/0Ns;->C:LX/0Nu;

    iget-wide v0, p0, LX/0Ns;->y:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v0, v0, v4

    if-eqz v0, :cond_6

    iget-wide v0, p0, LX/0Ns;->y:J

    :goto_4
    iget-object v4, p0, LX/0Ns;->e:LX/0LW;

    invoke-virtual {v2, v3, v0, v1, v4}, LX/0Ni;->a(LX/0Nu;JLX/0LW;)V

    .line 52033
    iget-object v0, p0, LX/0Ns;->e:LX/0LW;

    iget-boolean v0, v0, LX/0LW;->c:Z

    .line 52034
    iget-object v1, p0, LX/0Ns;->e:LX/0LW;

    iget-object v1, v1, LX/0LW;->b:LX/0LP;

    .line 52035
    iget-object v2, p0, LX/0Ns;->e:LX/0LW;

    const/4 v4, 0x0

    .line 52036
    iput v4, v2, LX/0LW;->a:I

    .line 52037
    const/4 v3, 0x0

    iput-object v3, v2, LX/0LW;->b:LX/0LP;

    .line 52038
    iput-boolean v4, v2, LX/0LW;->c:Z

    .line 52039
    if-eqz v0, :cond_7

    .line 52040
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Ns;->z:Z

    .line 52041
    iget-object v0, p0, LX/0Ns;->g:LX/0Gb;

    iget-wide v2, p0, LX/0Ns;->w:J

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, LX/0Gb;->a(Ljava/lang/Object;JJZ)Z

    goto :goto_3

    .line 52042
    :cond_6
    iget-wide v0, p0, LX/0Ns;->w:J

    goto :goto_4

    .line 52043
    :cond_7
    if-eqz v1, :cond_1

    .line 52044
    iput-wide v8, p0, LX/0Ns;->H:J

    .line 52045
    iput-object v1, p0, LX/0Ns;->A:LX/0LP;

    .line 52046
    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    .line 52047
    instance-of v1, v0, LX/0Nu;

    move v0, v1

    .line 52048
    if-eqz v0, :cond_b

    .line 52049
    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    move-object v10, v0

    check-cast v10, LX/0Nu;

    .line 52050
    invoke-static {p0}, LX/0Ns;->l(LX/0Ns;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 52051
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/0Ns;->y:J

    .line 52052
    :cond_8
    iget-object v0, v10, LX/0Nu;->k:LX/0Nj;

    .line 52053
    iget-object v1, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v1

    if-eq v1, v0, :cond_a

    .line 52054
    :cond_9
    iget-object v1, p0, LX/0Ns;->g:LX/0Gb;

    invoke-interface {v1}, LX/0Gb;->b()LX/0O1;

    move-result-object v1

    .line 52055
    iput-object v1, v0, LX/0Nj;->j:LX/0O1;

    .line 52056
    iget-object v2, v0, LX/0Nj;->d:LX/0ME;

    invoke-interface {v2, v0}, LX/0ME;->a(LX/0LU;)V

    .line 52057
    iget-object v1, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 52058
    :cond_a
    iget-object v0, v10, LX/0LP;->e:LX/0OA;

    iget-wide v1, v0, LX/0OA;->e:J

    iget v3, v10, LX/0LP;->b:I

    iget v4, v10, LX/0LP;->c:I

    iget-object v5, v10, LX/0LP;->d:LX/0AR;

    iget-wide v6, v10, LX/0LQ;->h:J

    iget-wide v8, v10, LX/0LQ;->i:J

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, LX/0Ns;->a(JIILX/0AR;JJ)V

    .line 52059
    iput-object v10, p0, LX/0Ns;->B:LX/0Nu;

    .line 52060
    :goto_5
    iget-object v0, p0, LX/0Ns;->D:LX/0ON;

    iget-object v1, p0, LX/0Ns;->A:LX/0LP;

    invoke-virtual {v0, v1, p0}, LX/0ON;->a(LX/0LO;LX/0LX;)V

    goto/16 :goto_3

    .line 52061
    :cond_b
    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    iget-object v0, v0, LX/0LP;->e:LX/0OA;

    iget-wide v1, v0, LX/0OA;->e:J

    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    iget v3, v0, LX/0LP;->b:I

    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    iget v4, v0, LX/0LP;->c:I

    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    iget-object v5, v0, LX/0LP;->d:LX/0AR;

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, LX/0Ns;->a(JIILX/0AR;JJ)V

    goto :goto_5

    :cond_c
    iget-boolean v11, p0, LX/0Ns;->z:Z

    if-nez v11, :cond_d

    iget-boolean v11, p0, LX/0Ns;->k:Z

    if-eqz v11, :cond_e

    iget v11, p0, LX/0Ns;->n:I

    if-nez v11, :cond_e

    :cond_d
    const-wide/16 v11, -0x1

    goto/16 :goto_0

    :cond_e
    iget-object v11, p0, LX/0Ns;->B:LX/0Nu;

    if-eqz v11, :cond_f

    iget-object v11, p0, LX/0Ns;->B:LX/0Nu;

    iget-wide v11, v11, LX/0LQ;->i:J

    goto/16 :goto_0

    :cond_f
    iget-object v11, p0, LX/0Ns;->C:LX/0Nu;

    iget-wide v11, v11, LX/0LQ;->i:J

    goto/16 :goto_0
.end method

.method public static l(LX/0Ns;)Z
    .locals 4

    .prologue
    .line 52067
    iget-wide v0, p0, LX/0Ns;->y:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final Z_()LX/0L8;
    .locals 1

    .prologue
    .line 51798
    iget v0, p0, LX/0Ns;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Ns;->j:I

    .line 51799
    return-object p0
.end method

.method public final a(IJLX/0L5;LX/0L7;)I
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, -0x2

    .line 51800
    iget-boolean v0, p0, LX/0Ns;->k:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51801
    iput-wide p2, p0, LX/0Ns;->w:J

    .line 51802
    iget-object v0, p0, LX/0Ns;->r:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_0

    invoke-static {p0}, LX/0Ns;->l(LX/0Ns;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v4

    .line 51803
    :goto_0
    return v0

    .line 51804
    :cond_1
    invoke-direct {p0}, LX/0Ns;->g()LX/0Nj;

    move-result-object v2

    .line 51805
    invoke-virtual {v2}, LX/0Nj;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v4

    .line 51806
    goto :goto_0

    .line 51807
    :cond_2
    iget-object v0, p0, LX/0Ns;->o:LX/0AR;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0Ns;->o:LX/0AR;

    iget-object v5, v2, LX/0Nj;->b:LX/0AR;

    invoke-virtual {v0, v5}, LX/0AR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 51808
    :cond_3
    iget-object v0, v2, LX/0Nj;->b:LX/0AR;

    iget v5, v2, LX/0Nj;->a:I

    iget-wide v6, v2, LX/0Nj;->c:J

    invoke-direct {p0, v0, v5, v6, v7}, LX/0Ns;->a(LX/0AR;IJ)V

    .line 51809
    iget-object v0, v2, LX/0Nj;->b:LX/0AR;

    iput-object v0, p0, LX/0Ns;->o:LX/0AR;

    .line 51810
    :cond_4
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v3, :cond_5

    .line 51811
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nj;

    invoke-virtual {v2, v0}, LX/0Nj;->a(LX/0Nj;)V

    .line 51812
    :cond_5
    iget-object v0, p0, LX/0Ns;->u:[I

    aget v5, v0, p1

    move v0, v1

    .line 51813
    :goto_1
    iget-object v6, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    add-int/lit8 v7, v0, 0x1

    if-le v6, v7, :cond_6

    invoke-virtual {v2, v5}, LX/0Nj;->c(I)Z

    move-result v6

    if-nez v6, :cond_6

    .line 51814
    iget-object v6, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v6, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nj;

    .line 51815
    invoke-virtual {v0}, LX/0Nj;->b()Z

    move-result v6

    if-nez v6, :cond_c

    move v0, v4

    .line 51816
    goto :goto_0

    .line 51817
    :cond_6
    invoke-virtual {v2, v5}, LX/0Nj;->b(I)LX/0L4;

    move-result-object v0

    .line 51818
    if-eqz v0, :cond_7

    iget-object v6, p0, LX/0Ns;->s:[LX/0L4;

    aget-object v6, v6, p1

    invoke-virtual {v0, v6}, LX/0L4;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 51819
    iput-object v0, p4, LX/0L5;->a:LX/0L4;

    .line 51820
    iget-object v1, p0, LX/0Ns;->s:[LX/0L4;

    aput-object v0, v1, p1

    .line 51821
    const/4 v0, -0x4

    goto :goto_0

    .line 51822
    :cond_7
    invoke-virtual {v2}, LX/0Nj;->b()Z

    move-result v0

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51823
    iget-object v0, v2, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MC;

    invoke-virtual {v0, p5}, LX/0MC;->a(LX/0L7;)Z

    move-result v0

    move v0, v0

    .line 51824
    if-eqz v0, :cond_a

    .line 51825
    iget-wide v4, p5, LX/0L7;->e:J

    iget-wide v6, p0, LX/0Ns;->x:J

    cmp-long v0, v4, v6

    if-gez v0, :cond_9

    move v0, v3

    .line 51826
    :goto_2
    iget v2, p5, LX/0L7;->d:I

    if-eqz v0, :cond_8

    const/high16 v1, 0x8000000

    :cond_8
    or-int v0, v2, v1

    iput v0, p5, LX/0L7;->d:I

    .line 51827
    const/4 v0, -0x3

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 51828
    goto :goto_2

    .line 51829
    :cond_a
    iget-boolean v0, p0, LX/0Ns;->z:Z

    if-eqz v0, :cond_b

    .line 51830
    const/4 v0, -0x1

    goto/16 :goto_0

    :cond_b
    move v0, v4

    .line 51831
    goto/16 :goto_0

    :cond_c
    move v8, v2

    move-object v2, v0

    move v0, v8

    goto :goto_1
.end method

.method public final a(I)LX/0L4;
    .locals 1

    .prologue
    .line 51832
    iget-boolean v0, p0, LX/0Ns;->k:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51833
    iget-object v0, p0, LX/0Ns;->p:[LX/0L4;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a(IJ)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 51834
    iget-boolean v0, p0, LX/0Ns;->k:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51835
    invoke-direct {p0, p1, v3}, LX/0Ns;->a(IZ)V

    .line 51836
    iget-object v0, p0, LX/0Ns;->s:[LX/0L4;

    aput-object v2, v0, p1

    .line 51837
    iget-object v0, p0, LX/0Ns;->r:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    .line 51838
    iput-object v2, p0, LX/0Ns;->o:LX/0AR;

    .line 51839
    iget-boolean v0, p0, LX/0Ns;->l:Z

    .line 51840
    iget-boolean v1, p0, LX/0Ns;->l:Z

    if-nez v1, :cond_0

    .line 51841
    iget-object v1, p0, LX/0Ns;->g:LX/0Gb;

    iget v2, p0, LX/0Ns;->d:I

    invoke-interface {v1, p0, v2}, LX/0Gb;->a(Ljava/lang/Object;I)V

    .line 51842
    iput-boolean v3, p0, LX/0Ns;->l:Z

    .line 51843
    :cond_0
    iget-object v1, p0, LX/0Ns;->a:LX/0Ni;

    .line 51844
    iget-boolean v2, v1, LX/0Ni;->u:Z

    move v1, v2

    .line 51845
    if-eqz v1, :cond_1

    const-wide/16 p2, 0x0

    .line 51846
    :cond_1
    iget-object v1, p0, LX/0Ns;->t:[I

    aget v1, v1, p1

    .line 51847
    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    iget-object v2, p0, LX/0Ns;->a:LX/0Ni;

    .line 51848
    iget p1, v2, LX/0Ni;->m:I

    move v2, p1

    .line 51849
    if-eq v1, v2, :cond_3

    .line 51850
    iget-object v0, p0, LX/0Ns;->a:LX/0Ni;

    invoke-virtual {v0, v1}, LX/0Ni;->b(I)V

    .line 51851
    invoke-direct {p0, p2, p3}, LX/0Ns;->d(J)V

    .line 51852
    :cond_2
    :goto_0
    return-void

    .line 51853
    :cond_3
    iget v1, p0, LX/0Ns;->n:I

    if-ne v1, v3, :cond_2

    .line 51854
    iput-wide p2, p0, LX/0Ns;->x:J

    .line 51855
    if-eqz v0, :cond_4

    iget-wide v0, p0, LX/0Ns;->w:J

    cmp-long v0, v0, p2

    if-nez v0, :cond_4

    .line 51856
    invoke-direct {p0}, LX/0Ns;->j()V

    goto :goto_0

    .line 51857
    :cond_4
    iput-wide p2, p0, LX/0Ns;->w:J

    .line 51858
    invoke-direct {p0, p2, p3}, LX/0Ns;->e(J)V

    goto :goto_0
.end method

.method public final a(LX/0LO;)V
    .locals 14

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51859
    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51860
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 51861
    iget-wide v4, p0, LX/0Ns;->H:J

    sub-long v12, v10, v4

    .line 51862
    iget-object v0, p0, LX/0Ns;->a:LX/0Ni;

    iget-object v3, p0, LX/0Ns;->A:LX/0LP;

    invoke-virtual {v0, v3}, LX/0Ni;->a(LX/0LP;)V

    .line 51863
    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    .line 51864
    instance-of v3, v0, LX/0Nu;

    move v0, v3

    .line 51865
    if-eqz v0, :cond_2

    .line 51866
    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    iget-object v3, p0, LX/0Ns;->B:LX/0Nu;

    if-ne v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, LX/0Av;->b(Z)V

    .line 51867
    iget-object v0, p0, LX/0Ns;->B:LX/0Nu;

    iput-object v0, p0, LX/0Ns;->C:LX/0Nu;

    .line 51868
    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    invoke-virtual {v0}, LX/0LP;->e()J

    move-result-wide v1

    iget-object v0, p0, LX/0Ns;->B:LX/0Nu;

    iget v3, v0, LX/0LP;->b:I

    iget-object v0, p0, LX/0Ns;->B:LX/0Nu;

    iget v4, v0, LX/0LP;->c:I

    iget-object v0, p0, LX/0Ns;->B:LX/0Nu;

    iget-object v5, v0, LX/0LP;->d:LX/0AR;

    iget-object v0, p0, LX/0Ns;->B:LX/0Nu;

    iget-wide v6, v0, LX/0LQ;->h:J

    iget-object v0, p0, LX/0Ns;->B:LX/0Nu;

    iget-wide v8, v0, LX/0LQ;->i:J

    move-object v0, p0

    invoke-direct/range {v0 .. v13}, LX/0Ns;->a(JIILX/0AR;JJJJ)V

    .line 51869
    :goto_2
    invoke-direct {p0}, LX/0Ns;->i()V

    .line 51870
    invoke-direct {p0}, LX/0Ns;->j()V

    .line 51871
    return-void

    :cond_0
    move v0, v2

    .line 51872
    goto :goto_0

    :cond_1
    move v1, v2

    .line 51873
    goto :goto_1

    .line 51874
    :cond_2
    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    invoke-virtual {v0}, LX/0LP;->e()J

    move-result-wide v1

    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    iget v3, v0, LX/0LP;->b:I

    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    iget v4, v0, LX/0LP;->c:I

    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    iget-object v5, v0, LX/0LP;->d:LX/0AR;

    move-object v0, p0

    move-wide v8, v6

    invoke-direct/range {v0 .. v13}, LX/0Ns;->a(JIILX/0AR;JJJJ)V

    goto :goto_2
.end method

.method public final a(LX/0LO;Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 51875
    iget-object v0, p0, LX/0Ns;->a:LX/0Ni;

    iget-object v1, p0, LX/0Ns;->A:LX/0LP;

    invoke-virtual {v0, v1, p2}, LX/0Ni;->a(LX/0LP;Ljava/io/IOException;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 51876
    iget-object v0, p0, LX/0Ns;->C:LX/0Nu;

    if-nez v0, :cond_0

    invoke-static {p0}, LX/0Ns;->l(LX/0Ns;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51877
    iget-wide v0, p0, LX/0Ns;->x:J

    iput-wide v0, p0, LX/0Ns;->y:J

    .line 51878
    :cond_0
    invoke-direct {p0}, LX/0Ns;->i()V

    .line 51879
    :goto_0
    iget-object v0, p0, LX/0Ns;->h:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0Ns;->i:LX/0Nr;

    if-eqz v0, :cond_1

    .line 51880
    iget-object v0, p0, LX/0Ns;->h:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/hls/HlsSampleSource$4;

    invoke-direct {v1, p0, p2}, Lcom/google/android/exoplayer/hls/HlsSampleSource$4;-><init>(LX/0Ns;Ljava/io/IOException;)V

    const p1, 0x7f2169ce

    invoke-static {v0, v1, p1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 51881
    :cond_1
    invoke-direct {p0}, LX/0Ns;->j()V

    .line 51882
    return-void

    .line 51883
    :cond_2
    iput-object p2, p0, LX/0Ns;->E:Ljava/io/IOException;

    .line 51884
    iget v0, p0, LX/0Ns;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Ns;->F:I

    .line 51885
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0Ns;->G:J

    goto :goto_0
.end method

.method public final a(J)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 51886
    iget-boolean v0, p0, LX/0Ns;->k:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 51887
    :goto_0
    return v0

    .line 51888
    :cond_0
    iget-object v0, p0, LX/0Ns;->a:LX/0Ni;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 51889
    iget-boolean v5, v0, LX/0Ni;->s:Z

    if-nez v5, :cond_1

    .line 51890
    iput-boolean v3, v0, LX/0Ni;->s:Z

    .line 51891
    :try_start_0
    iget-object v5, v0, LX/0Ni;->e:LX/0Nb;

    iget-object v6, v0, LX/0Ni;->d:LX/0Nl;

    invoke-interface {v5, v6, v0}, LX/0Nb;->a(LX/0Nl;LX/0Nh;)V

    .line 51892
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, LX/0Ni;->b(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51893
    :cond_1
    :goto_1
    iget-object v5, v0, LX/0Ni;->w:Ljava/io/IOException;

    if-nez v5, :cond_7

    :goto_2
    move v0, v3

    .line 51894
    if-nez v0, :cond_2

    move v0, v2

    .line 51895
    goto :goto_0

    .line 51896
    :cond_2
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 51897
    :goto_3
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nj;

    .line 51898
    invoke-virtual {v0}, LX/0Nj;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 51899
    invoke-direct {p0, v0}, LX/0Ns;->a(LX/0Nj;)V

    .line 51900
    iput-boolean v1, p0, LX/0Ns;->k:Z

    .line 51901
    invoke-direct {p0}, LX/0Ns;->j()V

    move v0, v1

    .line 51902
    goto :goto_0

    .line 51903
    :cond_3
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v1, :cond_4

    .line 51904
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nj;

    invoke-virtual {v0}, LX/0Nj;->c()V

    goto :goto_3

    .line 51905
    :cond_4
    iget-object v0, p0, LX/0Ns;->D:LX/0ON;

    if-nez v0, :cond_5

    .line 51906
    new-instance v0, LX/0ON;

    const-string v3, "Loader:HLS"

    invoke-direct {v0, v3}, LX/0ON;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0Ns;->D:LX/0ON;

    .line 51907
    iget-object v0, p0, LX/0Ns;->g:LX/0Gb;

    iget v3, p0, LX/0Ns;->d:I

    invoke-interface {v0, p0, v3}, LX/0Gb;->a(Ljava/lang/Object;I)V

    .line 51908
    iput-boolean v1, p0, LX/0Ns;->l:Z

    .line 51909
    :cond_5
    iget-object v0, p0, LX/0Ns;->D:LX/0ON;

    .line 51910
    iget-boolean v1, v0, LX/0ON;->c:Z

    move v0, v1

    .line 51911
    if-nez v0, :cond_6

    .line 51912
    iput-wide p1, p0, LX/0Ns;->y:J

    .line 51913
    iput-wide p1, p0, LX/0Ns;->w:J

    .line 51914
    :cond_6
    invoke-direct {p0}, LX/0Ns;->j()V

    move v0, v2

    .line 51915
    goto :goto_0

    .line 51916
    :catch_0
    move-exception v5

    .line 51917
    iput-object v5, v0, LX/0Ni;->w:Ljava/io/IOException;

    goto :goto_1

    :cond_7
    move v3, v4

    .line 51918
    goto :goto_2
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 51919
    iget-object v0, p0, LX/0Ns;->r:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    .line 51920
    iget-object v0, p0, LX/0Ns;->r:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    .line 51921
    iget-wide v0, p0, LX/0Ns;->x:J

    .line 51922
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 51923
    iget-object v0, p0, LX/0Ns;->E:Ljava/io/IOException;

    if-eqz v0, :cond_0

    iget v0, p0, LX/0Ns;->F:I

    iget v1, p0, LX/0Ns;->c:I

    if-le v0, v1, :cond_0

    .line 51924
    iget-object v0, p0, LX/0Ns;->E:Ljava/io/IOException;

    throw v0

    .line 51925
    :cond_0
    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    if-nez v0, :cond_1

    .line 51926
    iget-object v0, p0, LX/0Ns;->a:LX/0Ni;

    invoke-virtual {v0}, LX/0Ni;->a()V

    .line 51927
    :cond_1
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 51928
    iget-boolean v0, p0, LX/0Ns;->k:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51929
    iget v0, p0, LX/0Ns;->n:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51930
    iget-object v0, p0, LX/0Ns;->a:LX/0Ni;

    .line 51931
    iget-boolean v1, v0, LX/0Ni;->u:Z

    move v0, v1

    .line 51932
    if-eqz v0, :cond_0

    const-wide/16 p1, 0x0

    .line 51933
    :cond_0
    invoke-static {p0}, LX/0Ns;->l(LX/0Ns;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, LX/0Ns;->y:J

    .line 51934
    :goto_1
    iput-wide p1, p0, LX/0Ns;->w:J

    .line 51935
    iput-wide p1, p0, LX/0Ns;->x:J

    .line 51936
    cmp-long v0, v0, p1

    if-nez v0, :cond_3

    .line 51937
    :goto_2
    return-void

    .line 51938
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 51939
    :cond_2
    iget-wide v0, p0, LX/0Ns;->w:J

    goto :goto_1

    .line 51940
    :cond_3
    invoke-direct {p0, p1, p2}, LX/0Ns;->d(J)V

    goto :goto_2
.end method

.method public final b(IJ)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51941
    iget-boolean v0, p0, LX/0Ns;->k:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51942
    iget-object v0, p0, LX/0Ns;->q:[Z

    aget-boolean v0, v0, p1

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51943
    iput-wide p2, p0, LX/0Ns;->w:J

    .line 51944
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51945
    invoke-direct {p0}, LX/0Ns;->g()LX/0Nj;

    move-result-object v0

    iget-wide v4, p0, LX/0Ns;->w:J

    .line 51946
    invoke-virtual {v0}, LX/0Nj;->b()Z

    move-result v1

    if-nez v1, :cond_4

    .line 51947
    :cond_0
    invoke-direct {p0}, LX/0Ns;->j()V

    .line 51948
    iget-boolean v0, p0, LX/0Ns;->z:Z

    if-eqz v0, :cond_2

    move v2, v3

    .line 51949
    :cond_1
    :goto_0
    return v2

    .line 51950
    :cond_2
    invoke-static {p0}, LX/0Ns;->l(LX/0Ns;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v1, v2

    .line 51951
    :goto_1
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 51952
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nj;

    .line 51953
    invoke-virtual {v0}, LX/0Nj;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 51954
    iget-object v4, p0, LX/0Ns;->u:[I

    aget v4, v4, p1

    .line 51955
    invoke-virtual {v0, v4}, LX/0Nj;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v3

    .line 51956
    goto :goto_0

    .line 51957
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 51958
    :cond_4
    const/4 v1, 0x0

    :goto_2
    iget-object p2, p0, LX/0Ns;->v:[Z

    array-length p2, p2

    if-ge v1, p2, :cond_0

    .line 51959
    iget-object p2, p0, LX/0Ns;->v:[Z

    aget-boolean p2, p2, v1

    if-nez p2, :cond_5

    .line 51960
    invoke-virtual {v0}, LX/0Nj;->b()Z

    move-result p2

    invoke-static {p2}, LX/0Av;->b(Z)V

    .line 51961
    iget-object p2, v0, LX/0Nj;->e:Landroid/util/SparseArray;

    invoke-virtual {p2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/0MC;

    invoke-virtual {p2, v4, v5}, LX/0MC;->a(J)V

    .line 51962
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 51963
    iget-boolean v0, p0, LX/0Ns;->k:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51964
    iget v0, p0, LX/0Ns;->m:I

    return v0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51965
    iget-boolean v0, p0, LX/0Ns;->k:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51966
    invoke-direct {p0, p1, v2}, LX/0Ns;->a(IZ)V

    .line 51967
    iget v0, p0, LX/0Ns;->n:I

    if-nez v0, :cond_1

    .line 51968
    iget-object v0, p0, LX/0Ns;->a:LX/0Ni;

    .line 51969
    const/4 v1, 0x0

    iput-object v1, v0, LX/0Ni;->w:Ljava/io/IOException;

    .line 51970
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/0Ns;->w:J

    .line 51971
    iget-boolean v0, p0, LX/0Ns;->l:Z

    if-eqz v0, :cond_0

    .line 51972
    iget-object v0, p0, LX/0Ns;->g:LX/0Gb;

    invoke-interface {v0, p0}, LX/0Gb;->a(Ljava/lang/Object;)V

    .line 51973
    iput-boolean v2, p0, LX/0Ns;->l:Z

    .line 51974
    :cond_0
    iget-object v0, p0, LX/0Ns;->D:LX/0ON;

    .line 51975
    iget-boolean v1, v0, LX/0ON;->c:Z

    move v0, v1

    .line 51976
    if-eqz v0, :cond_2

    .line 51977
    iget-object v0, p0, LX/0Ns;->D:LX/0ON;

    invoke-virtual {v0}, LX/0ON;->b()V

    .line 51978
    :cond_1
    :goto_0
    return-void

    .line 51979
    :cond_2
    invoke-direct {p0}, LX/0Ns;->h()V

    .line 51980
    iget-object v0, p0, LX/0Ns;->g:LX/0Gb;

    invoke-interface {v0}, LX/0Gb;->a()V

    goto :goto_0
.end method

.method public final d()J
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 51981
    iget-boolean v0, p0, LX/0Ns;->k:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51982
    iget v0, p0, LX/0Ns;->n:I

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51983
    invoke-static {p0}, LX/0Ns;->l(LX/0Ns;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 51984
    iget-wide v0, p0, LX/0Ns;->y:J

    .line 51985
    :cond_0
    :goto_1
    return-wide v0

    .line 51986
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 51987
    :cond_2
    iget-boolean v0, p0, LX/0Ns;->z:Z

    if-eqz v0, :cond_3

    .line 51988
    const-wide/16 v0, -0x3

    goto :goto_1

    .line 51989
    :cond_3
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nj;

    invoke-virtual {v0}, LX/0Nj;->d()J

    move-result-wide v2

    .line 51990
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v1, :cond_4

    .line 51991
    iget-object v0, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    iget-object v1, p0, LX/0Ns;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Nj;

    invoke-virtual {v0}, LX/0Nj;->d()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 51992
    :goto_2
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-wide v0, p0, LX/0Ns;->w:J

    goto :goto_1

    :cond_4
    move-wide v0, v2

    goto :goto_2
.end method

.method public final e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51993
    iget v0, p0, LX/0Ns;->j:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51994
    iget v0, p0, LX/0Ns;->j:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0Ns;->j:I

    if-nez v0, :cond_1

    iget-object v0, p0, LX/0Ns;->D:LX/0ON;

    if-eqz v0, :cond_1

    .line 51995
    iget-boolean v0, p0, LX/0Ns;->l:Z

    if-eqz v0, :cond_0

    .line 51996
    iget-object v0, p0, LX/0Ns;->g:LX/0Gb;

    invoke-interface {v0, p0}, LX/0Gb;->a(Ljava/lang/Object;)V

    .line 51997
    iput-boolean v1, p0, LX/0Ns;->l:Z

    .line 51998
    :cond_0
    iget-object v0, p0, LX/0Ns;->D:LX/0ON;

    invoke-virtual {v0}, LX/0ON;->c()V

    .line 51999
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Ns;->D:LX/0ON;

    .line 52000
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 52001
    goto :goto_0
.end method

.method public final f()V
    .locals 5

    .prologue
    .line 52002
    iget-object v0, p0, LX/0Ns;->A:LX/0LP;

    invoke-virtual {v0}, LX/0LP;->e()J

    move-result-wide v0

    .line 52003
    iget-object v2, p0, LX/0Ns;->h:Landroid/os/Handler;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/0Ns;->i:LX/0Nr;

    if-eqz v2, :cond_0

    .line 52004
    iget-object v2, p0, LX/0Ns;->h:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/exoplayer/hls/HlsSampleSource$3;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/exoplayer/hls/HlsSampleSource$3;-><init>(LX/0Ns;J)V

    const v4, 0x7aad4b11

    invoke-static {v2, v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 52005
    :cond_0
    iget v0, p0, LX/0Ns;->n:I

    if-lez v0, :cond_1

    .line 52006
    iget-wide v0, p0, LX/0Ns;->y:J

    invoke-direct {p0, v0, v1}, LX/0Ns;->e(J)V

    .line 52007
    :goto_0
    return-void

    .line 52008
    :cond_1
    invoke-direct {p0}, LX/0Ns;->h()V

    .line 52009
    iget-object v0, p0, LX/0Ns;->g:LX/0Gb;

    invoke-interface {v0}, LX/0Gb;->a()V

    goto :goto_0
.end method
