.class public final enum LX/0JT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JT;

.field public static final enum VIDEO_HOME_BACKGROUNDED:LX/0JT;

.field public static final enum VIDEO_HOME_BADGE_FETCHED:LX/0JT;

.field public static final enum VIDEO_HOME_CLICK:LX/0JT;

.field public static final enum VIDEO_HOME_DATA_PREFETCH:LX/0JT;

.field public static final enum VIDEO_HOME_DUPLICATE:LX/0JT;

.field public static final enum VIDEO_HOME_END_SCREEN_DISPLAYED:LX/0JT;

.field public static final enum VIDEO_HOME_FOREGROUNDED:LX/0JT;

.field public static final enum VIDEO_HOME_METADATA_FETCHED:LX/0JT;

.field public static final enum VIDEO_HOME_PULL_TO_REFRESH:LX/0JT;

.field public static final enum VIDEO_HOME_SESSION_END:LX/0JT;

.field public static final enum VIDEO_HOME_SESSION_START:LX/0JT;

.field public static final enum VIDEO_HOME_SWIPE:LX/0JT;

.field public static final enum VIDEO_HOME_TTI:LX/0JT;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39478
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_DATA_PREFETCH"

    const-string v2, "video_home_data_prefetch"

    invoke-direct {v0, v1, v4, v2}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_DATA_PREFETCH:LX/0JT;

    .line 39479
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_SESSION_START"

    const-string v2, "video_home_session_start"

    invoke-direct {v0, v1, v5, v2}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_SESSION_START:LX/0JT;

    .line 39480
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_BACKGROUNDED"

    const-string v2, "video_home_backgrounded"

    invoke-direct {v0, v1, v6, v2}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_BACKGROUNDED:LX/0JT;

    .line 39481
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_FOREGROUNDED"

    const-string v2, "video_home_foregrounded"

    invoke-direct {v0, v1, v7, v2}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_FOREGROUNDED:LX/0JT;

    .line 39482
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_SESSION_END"

    const-string v2, "video_home_session_end"

    invoke-direct {v0, v1, v8, v2}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_SESSION_END:LX/0JT;

    .line 39483
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_TTI"

    const/4 v2, 0x5

    const-string v3, "video_home_tti"

    invoke-direct {v0, v1, v2, v3}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_TTI:LX/0JT;

    .line 39484
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_BADGE_FETCHED"

    const/4 v2, 0x6

    const-string v3, "video_home_badge_fetched"

    invoke-direct {v0, v1, v2, v3}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_BADGE_FETCHED:LX/0JT;

    .line 39485
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_METADATA_FETCHED"

    const/4 v2, 0x7

    const-string v3, "video_home_metadata_fetched"

    invoke-direct {v0, v1, v2, v3}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_METADATA_FETCHED:LX/0JT;

    .line 39486
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_CLICK"

    const/16 v2, 0x8

    const-string v3, "video_home_click"

    invoke-direct {v0, v1, v2, v3}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_CLICK:LX/0JT;

    .line 39487
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_SWIPE"

    const/16 v2, 0x9

    const-string v3, "video_home_swipe"

    invoke-direct {v0, v1, v2, v3}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_SWIPE:LX/0JT;

    .line 39488
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_PULL_TO_REFRESH"

    const/16 v2, 0xa

    const-string v3, "video_home_pull_to_refresh"

    invoke-direct {v0, v1, v2, v3}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_PULL_TO_REFRESH:LX/0JT;

    .line 39489
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_DUPLICATE"

    const/16 v2, 0xb

    const-string v3, "video_home_duplicate"

    invoke-direct {v0, v1, v2, v3}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_DUPLICATE:LX/0JT;

    .line 39490
    new-instance v0, LX/0JT;

    const-string v1, "VIDEO_HOME_END_SCREEN_DISPLAYED"

    const/16 v2, 0xc

    const-string v3, "video_home_end_screen_displayed"

    invoke-direct {v0, v1, v2, v3}, LX/0JT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JT;->VIDEO_HOME_END_SCREEN_DISPLAYED:LX/0JT;

    .line 39491
    const/16 v0, 0xd

    new-array v0, v0, [LX/0JT;

    sget-object v1, LX/0JT;->VIDEO_HOME_DATA_PREFETCH:LX/0JT;

    aput-object v1, v0, v4

    sget-object v1, LX/0JT;->VIDEO_HOME_SESSION_START:LX/0JT;

    aput-object v1, v0, v5

    sget-object v1, LX/0JT;->VIDEO_HOME_BACKGROUNDED:LX/0JT;

    aput-object v1, v0, v6

    sget-object v1, LX/0JT;->VIDEO_HOME_FOREGROUNDED:LX/0JT;

    aput-object v1, v0, v7

    sget-object v1, LX/0JT;->VIDEO_HOME_SESSION_END:LX/0JT;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0JT;->VIDEO_HOME_TTI:LX/0JT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0JT;->VIDEO_HOME_BADGE_FETCHED:LX/0JT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0JT;->VIDEO_HOME_METADATA_FETCHED:LX/0JT;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0JT;->VIDEO_HOME_CLICK:LX/0JT;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0JT;->VIDEO_HOME_SWIPE:LX/0JT;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0JT;->VIDEO_HOME_PULL_TO_REFRESH:LX/0JT;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0JT;->VIDEO_HOME_DUPLICATE:LX/0JT;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0JT;->VIDEO_HOME_END_SCREEN_DISPLAYED:LX/0JT;

    aput-object v2, v0, v1

    sput-object v0, LX/0JT;->$VALUES:[LX/0JT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39474
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39475
    iput-object p3, p0, LX/0JT;->value:Ljava/lang/String;

    .line 39476
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JT;
    .locals 1

    .prologue
    .line 39477
    const-class v0, LX/0JT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JT;

    return-object v0
.end method

.method public static values()[LX/0JT;
    .locals 1

    .prologue
    .line 39473
    sget-object v0, LX/0JT;->$VALUES:[LX/0JT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JT;

    return-object v0
.end method
