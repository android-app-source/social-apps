.class public final LX/0Cx;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/BrowserLiteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V
    .locals 0

    .prologue
    .line 28311
    iput-object p1, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/browser/lite/BrowserLiteFragment;B)V
    .locals 0

    .prologue
    .line 28310
    invoke-direct {p0, p1}, LX/0Cx;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    return-void
.end method

.method private a(LX/0D5;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28279
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->i:LX/0Dm;

    invoke-virtual {v0, p2}, LX/0Dm;->a(Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v3

    .line 28280
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-boolean v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->T:Z

    if-eqz v0, :cond_1

    .line 28281
    const/4 v0, 0x0

    .line 28282
    if-nez p2, :cond_5

    .line 28283
    :cond_0
    :goto_0
    move-object v4, v0

    .line 28284
    if-eqz v4, :cond_1

    .line 28285
    if-eqz v3, :cond_2

    move v0, v1

    :goto_1
    const-wide/16 v9, -0x1

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 28286
    iget-object v6, p1, LX/0D5;->c:LX/0Dh;

    .line 28287
    iget-boolean v8, v6, LX/0Dh;->b:Z

    move v6, v8

    .line 28288
    if-nez v6, :cond_7

    .line 28289
    :cond_1
    :goto_2
    if-eqz v3, :cond_3

    .line 28290
    sget-object v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v4, "Use prefetched response for %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {v0, v4, v1}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 28291
    :goto_3
    return-object v0

    :cond_2
    move v0, v2

    .line 28292
    goto :goto_1

    .line 28293
    :cond_3
    sget-boolean v0, LX/0Df;->b:Z

    move v0, v0

    .line 28294
    if-eqz v0, :cond_4

    invoke-static {p2}, LX/047;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 28295
    sget-object v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v3, "Download from Internet for %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {v0, v3, v1}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28296
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 28297
    :cond_5
    const-string v4, "https://connect.facebook.net/en_US/fbevents.js"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 28298
    const-string v0, "fbevents"

    goto :goto_0

    .line 28299
    :cond_6
    const-string v4, "https://www.facebook.com/tr?"

    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 28300
    const-string v0, "tr"

    goto :goto_0

    .line 28301
    :cond_7
    new-array v6, v7, [Ljava/lang/Object;

    aput-object v4, v6, v5

    .line 28302
    const/4 v6, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_8
    move v5, v6

    :goto_4
    packed-switch v5, :pswitch_data_0

    goto :goto_2

    .line 28303
    :pswitch_0
    iget-wide v5, p1, LX/0D5;->m:J

    cmp-long v5, v5, v9

    if-nez v5, :cond_1

    .line 28304
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, p1, LX/0D5;->m:J

    .line 28305
    iput-boolean v0, p1, LX/0D5;->o:Z

    goto :goto_2

    .line 28306
    :sswitch_0
    const-string v7, "fbevents"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    goto :goto_4

    :sswitch_1
    const-string v5, "tr"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    move v5, v7

    goto :goto_4

    .line 28307
    :pswitch_1
    iget-wide v5, p1, LX/0D5;->n:J

    cmp-long v5, v5, v9

    if-nez v5, :cond_1

    .line 28308
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, p1, LX/0D5;->n:J

    .line 28309
    iput-boolean v0, p1, LX/0D5;->p:Z

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0xe7e -> :sswitch_1
        0x56e270d5 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 28270
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    const/4 v1, 0x2

    aput-object p4, v0, v1

    .line 28271
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/16 v0, -0xa

    if-ne v0, p2, :cond_0

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->u:Ljava/lang/String;

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/047;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28272
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {v0, p4}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d(Lcom/facebook/browser/lite/BrowserLiteFragment;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28273
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 28274
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/facebook/browser/lite/BrowserLiteFragment$BrowserLiteWebViewClient$1;

    invoke-direct {v1, p0, p1, p4}, Lcom/facebook/browser/lite/BrowserLiteFragment$BrowserLiteWebViewClient$1;-><init>(LX/0Cx;Landroid/webkit/WebView;Ljava/lang/String;)V

    const-wide/16 v2, 0x3e8

    const v4, 0x45a0eda1

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 28275
    :cond_0
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->u:Ljava/lang/String;

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, LX/0Cx;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->F:I

    if-nez v0, :cond_1

    .line 28276
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 28277
    iput p2, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->F:I

    .line 28278
    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 28263
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 28264
    iput-object p1, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->u:Ljava/lang/String;

    .line 28265
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    if-eqz v0, :cond_0

    .line 28266
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-virtual {v0, p1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(Ljava/lang/String;)V

    .line 28267
    :cond_0
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    if-eqz v0, :cond_1

    .line 28268
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    invoke-virtual {v0, p1}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->a(Ljava/lang/String;)V

    .line 28269
    :cond_1
    return-void
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 28262
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-gt v0, v2, :cond_0

    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0D5;

    invoke-virtual {v0}, LX/0D5;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final doUpdateVisitedHistory(Landroid/webkit/WebView;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 28143
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->doUpdateVisitedHistory(Landroid/webkit/WebView;Ljava/lang/String;Z)V

    .line 28144
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 28145
    invoke-direct {p0, p2}, LX/0Cx;->a(Ljava/lang/String;)V

    .line 28146
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 28248
    invoke-virtual {p1}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 28249
    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 28250
    invoke-direct {p0, v0}, LX/0Cx;->a(Ljava/lang/String;)V

    .line 28251
    iget-object v1, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    iget-object v2, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {v2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->z(Lcom/facebook/browser/lite/BrowserLiteFragment;)I

    move-result v2

    .line 28252
    new-instance p2, LX/0CP;

    invoke-direct {p2, v1, v0, v2}, LX/0CP;-><init>(LX/0CQ;Ljava/lang/String;I)V

    invoke-static {v1, p2}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 28253
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->l:LX/0DA;

    if-eqz v0, :cond_0

    .line 28254
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->l:LX/0DA;

    .line 28255
    iget-object v1, v0, LX/0DA;->e:LX/0D5;

    if-eqz v1, :cond_0

    .line 28256
    iget-object v1, v0, LX/0DA;->e:LX/0D5;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "(function(d, s, id){    var sdkURL = \"%s\";    var js, fjs = d.getElementsByTagName(s)[0];    if (d.getElementById(id) || \'FBExtensions\' in window) {return;}    js = d.createElement(s); js.id = id;    js.src = sdkURL;    fjs.parentNode.insertBefore(js, fjs);}(document, \'script\', \'facebook-platform-extensions-sdk\'));"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object p2, v0, LX/0DA;->b:Ljava/lang/String;

    aput-object p2, v5, v6

    invoke-static {v2, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0D5;->a(Ljava/lang/String;)V

    .line 28257
    :cond_0
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 28258
    iput-boolean v3, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->x:Z

    .line 28259
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {v0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(Lcom/facebook/browser/lite/BrowserLiteFragment;Landroid/webkit/WebView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28260
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Ljava/lang/String;)V

    .line 28261
    :cond_1
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p3    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 28238
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 28239
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-wide v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->N:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 28240
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 28241
    iput-wide v2, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->N:J

    .line 28242
    :cond_0
    invoke-direct {p0, p2}, LX/0Cx;->a(Ljava/lang/String;)V

    .line 28243
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_TRACKING"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 28244
    new-instance v3, LX/0CN;

    invoke-direct {v3, v0, v1, p2, v2}, LX/0CN;-><init>(LX/0CQ;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v0, v3}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 28245
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->B:LX/0Dt;

    if-eqz v0, :cond_1

    .line 28246
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->B:LX/0Dt;

    invoke-virtual {v0}, LX/0Dp;->b()V

    .line 28247
    :cond_1
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 28234
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    .line 28235
    invoke-direct {p0, p1, p2, p3, p4}, LX/0Cx;->a(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 28236
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 28237
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 28231
    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getErrorCode()I

    move-result v0

    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v0, v1, v2}, LX/0Cx;->a(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 28232
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V

    .line 28233
    return-void
.end method

.method public final onReceivedHttpAuthRequest(Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28228
    if-eqz p2, :cond_0

    .line 28229
    invoke-virtual {p2}, Landroid/webkit/HttpAuthHandler;->cancel()V

    .line 28230
    :cond_0
    return-void
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 3

    .prologue
    .line 28221
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {p3}, Landroid/net/http/SslError;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Lcom/facebook/browser/lite/BrowserLiteFragment;Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28222
    new-instance v0, LX/0EX;

    invoke-direct {v0}, LX/0EX;-><init>()V

    .line 28223
    iget-object v1, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 28224
    iput-object v1, v0, LX/0EX;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 28225
    iget-object v1, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "SSLDialog"

    invoke-virtual {v0, v1, v2}, LX/0EX;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 28226
    :cond_0
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->cancel()V

    .line 28227
    return-void
.end method

.method public final shouldInterceptRequest(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 28220
    check-cast p1, LX/0D5;

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0Cx;->a(LX/0D5;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    return-object v0
.end method

.method public final shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 1

    .prologue
    .line 28219
    check-cast p1, LX/0D5;

    invoke-direct {p0, p1, p2}, LX/0Cx;->a(LX/0D5;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    return-object v0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 28147
    :try_start_0
    sget-object v3, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v4, "shouldOverrideUrlLoading %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-static {v3, v4, v5}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28148
    check-cast p1, LX/0D5;

    .line 28149
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "about:blank"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    .line 28150
    :goto_0
    return v0

    .line 28151
    :cond_1
    iget-object v3, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-boolean v3, v3, Lcom/facebook/browser/lite/BrowserLiteFragment;->w:Z

    if-eqz v3, :cond_3

    if-eqz p2, :cond_2

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :cond_2
    invoke-static {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 28152
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    const/4 v3, 0x0

    .line 28153
    iput-boolean v3, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->w:Z

    .line 28154
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->w(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    .line 28155
    :cond_3
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-boolean v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->O:Z

    if-nez v0, :cond_5

    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 28156
    if-eqz p2, :cond_4

    const-string v3, "https://play.google.com/store/apps/details?id="

    invoke-virtual {p2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    .line 28157
    :cond_4
    const/4 v3, 0x0

    .line 28158
    :goto_1
    move v0, v3

    .line 28159
    if-eqz v0, :cond_5

    .line 28160
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 28161
    invoke-static {v0, p1, p2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a$redex0(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;Ljava/lang/String;)V

    .line 28162
    move v0, v1

    .line 28163
    goto :goto_0

    .line 28164
    :cond_5
    invoke-static {p2}, LX/047;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 28165
    const/4 v4, 0x0

    .line 28166
    if-eqz v3, :cond_6

    sget-object v0, LX/047;->b:Ljava/util/Set;

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    :cond_6
    move v0, v4

    .line 28167
    :goto_2
    move v0, v0

    .line 28168
    if-eqz v0, :cond_9

    .line 28169
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {v0, p1, p2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-le v0, v1, :cond_8

    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    iget-object v4, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v4, v4, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v0, v4}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0D5;

    invoke-virtual {v0}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 28170
    :goto_3
    iget-object v4, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v4, v4, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 28171
    const/4 v6, 0x0

    .line 28172
    iget-object v7, v4, LX/0CQ;->d:LX/0DT;

    if-eqz v7, :cond_7
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 28173
    :try_start_1
    iget-object v7, v4, LX/0CQ;->d:LX/0DT;

    invoke-interface {v7, v5, v0}, LX/0DT;->a(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    move-result v6

    .line 28174
    :cond_7
    :goto_4
    move v0, v6

    .line 28175
    if-eqz v0, :cond_9

    .line 28176
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 28177
    invoke-static {v0, p1, p2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a$redex0(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;Ljava/lang/String;)V

    .line 28178
    move v0, v1

    .line 28179
    goto/16 :goto_0

    .line 28180
    :cond_8
    invoke-virtual {p1}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 28181
    :cond_9
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    .line 28182
    const/4 v4, 0x0

    .line 28183
    iget-object v5, v0, LX/0CQ;->d:LX/0DT;

    if-eqz v5, :cond_a
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 28184
    :try_start_3
    iget-object v5, v0, LX/0CQ;->d:LX/0DT;

    invoke-interface {v5, p2}, LX/0DT;->d(Ljava/lang/String;)Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    :try_start_4
    move-result v4

    .line 28185
    :cond_a
    :goto_5
    move v0, v4

    .line 28186
    if-eqz v0, :cond_b

    move v0, v1

    .line 28187
    goto/16 :goto_0

    .line 28188
    :cond_b
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 28189
    if-eqz v0, :cond_15

    if-eqz v3, :cond_15

    invoke-virtual {v0, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    const/4 v4, 0x1

    :goto_6
    move v4, v4

    .line 28190
    if-eqz v4, :cond_e

    .line 28191
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 28192
    iget-object v1, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-wide v6, v1, Lcom/facebook/browser/lite/BrowserLiteFragment;->N:J

    sub-long v6, v4, v6

    const-wide/16 v8, 0x3e8

    cmp-long v1, v6, v8

    if-lez v1, :cond_d

    .line 28193
    iget-object v1, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {v1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->q(Lcom/facebook/browser/lite/BrowserLiteFragment;)I

    .line 28194
    iget-object v1, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteFragment;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 28195
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->s(Lcom/facebook/browser/lite/BrowserLiteFragment;)I

    .line 28196
    :cond_c
    :goto_7
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 28197
    iput-wide v4, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->N:J

    .line 28198
    move v0, v2

    .line 28199
    goto/16 :goto_0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    .line 28200
    :cond_d
    goto :goto_7

    .line 28201
    :catch_0
    sget-object v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v1, "shouldOverrideUrlLoading error"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 28202
    goto/16 :goto_0

    .line 28203
    :cond_e
    if-nez v3, :cond_f

    .line 28204
    :try_start_5
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {v0, p2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d(Lcom/facebook/browser/lite/BrowserLiteFragment;Ljava/lang/String;)Z

    .line 28205
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 28206
    invoke-static {v0, p1, p2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a$redex0(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;Ljava/lang/String;)V

    .line 28207
    :goto_8
    move v0, v1

    .line 28208
    goto/16 :goto_0

    .line 28209
    :cond_f
    iget-object v0, p0, LX/0Cx;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 28210
    invoke-static {v0, p1, v3, v4, v5}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a$redex0(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;Landroid/net/Uri;Ljava/util/Map;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    .line 28211
    goto :goto_8

    :cond_10
    :try_start_6
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v0, v3}, LX/0DQ;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v3

    goto/16 :goto_1
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0

    .line 28212
    :cond_11
    :try_start_7
    invoke-virtual {v3}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v5

    .line 28213
    if-nez v5, :cond_12

    move v0, v4

    .line 28214
    goto/16 :goto_2

    .line 28215
    :cond_12
    sget-object v0, LX/047;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_13
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 28216
    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 28217
    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_14
    move v0, v4

    .line 28218
    goto/16 :goto_2
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0

    :catch_1
    :try_start_8
    goto/16 :goto_4
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_0

    :catch_2
    :try_start_9
    goto/16 :goto_5
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0

    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_6
.end method
