.class public LX/07Q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field private final b:LX/05K;

.field private final c:LX/05L;

.field private final d:LX/075;

.field public e:Ljava/io/DataOutputStream;


# direct methods
.method public constructor <init>(ILX/05K;LX/05L;LX/075;)V
    .locals 0

    .prologue
    .line 19708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19709
    iput p1, p0, LX/07Q;->a:I

    .line 19710
    iput-object p2, p0, LX/07Q;->b:LX/05K;

    .line 19711
    iput-object p3, p0, LX/07Q;->c:LX/05L;

    .line 19712
    iput-object p4, p0, LX/07Q;->d:LX/075;

    .line 19713
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/07W;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 19714
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LX/07W;->e()LX/07S;

    move-result-object v2

    .line 19715
    sget-object v3, LX/07Y;->a:[I

    invoke-virtual {v2}, LX/07S;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 19716
    const-string v2, "MessageEncoder"

    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown message type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/07W;->e()LX/07S;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    const-string v4, "send/unexpected; type=%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p1}, LX/07W;->e()LX/07S;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v3, v4, v5}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v1

    .line 19717
    :goto_0
    invoke-virtual {p1}, LX/07W;->e()LX/07S;

    move-result-object v1

    invoke-virtual {v1}, LX/07S;->name()Ljava/lang/String;

    move-result-object v1

    .line 19718
    instance-of v3, p1, LX/07X;

    if-eqz v3, :cond_0

    .line 19719
    check-cast p1, LX/07X;

    .line 19720
    const-string v1, "PUBLISH_"

    invoke-virtual {p1}, LX/07X;->a()LX/0B5;

    move-result-object v3

    iget-object v3, v3, LX/0B5;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 19721
    :cond_0
    iget-object v3, p0, LX/07Q;->d:LX/075;

    invoke-virtual {v3, v2}, LX/075;->a(I)V

    .line 19722
    iget-object v3, p0, LX/07Q;->d:LX/075;

    invoke-virtual {v3, v1, v2}, LX/075;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 19723
    monitor-exit p0

    return-void

    .line 19724
    :pswitch_0
    :try_start_1
    instance-of v1, p1, LX/07j;

    if-nez v1, :cond_1

    .line 19725
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 19726
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 19727
    :cond_1
    :try_start_2
    move-object v0, p1

    check-cast v0, LX/07j;

    move-object v1, v0

    .line 19728
    const-string v3, "MessageEncoder"

    const-string v4, "send/%s; retcode=%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-virtual {v1}, LX/07j;->a()LX/07e;

    move-result-object v6

    iget-byte v6, v6, LX/07e;->a:B

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19729
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 19730
    invoke-virtual {v1}, LX/07j;->a()LX/07e;

    move-result-object v3

    iget-byte v3, v3, LX/07e;->a:B

    if-nez v3, :cond_11

    .line 19731
    invoke-virtual {v1}, LX/07j;->b()LX/07h;

    move-result-object v3

    if-eqz v3, :cond_10

    :goto_1
    invoke-static {v0}, LX/01n;->b(Z)V

    .line 19732
    invoke-virtual {v1}, LX/07j;->b()LX/07h;

    move-result-object v0

    invoke-virtual {v0}, LX/07h;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/07Z;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 19733
    iget-object v3, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    .line 19734
    iget-object v4, v1, LX/07W;->a:LX/07R;

    move-object v4, v4

    .line 19735
    invoke-static {v4}, LX/07Z;->a(LX/07R;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19736
    iget-object v3, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    array-length v4, v0

    add-int/lit8 v4, v4, 0x4

    invoke-static {v3, v4}, LX/07Z;->a(Ljava/io/DataOutputStream;I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    .line 19737
    iget-object v4, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19738
    iget-object v2, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, LX/07j;->a()LX/07e;

    move-result-object v4

    iget-byte v4, v4, LX/07e;->a:B

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19739
    iget-object v2, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    array-length v4, v0

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 19740
    add-int/lit8 v2, v3, 0x4

    .line 19741
    iget-object v3, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 19742
    array-length v0, v0

    add-int/2addr v0, v2

    .line 19743
    iget-object v2, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 19744
    :goto_2
    move v1, v0

    .line 19745
    move v2, v1

    .line 19746
    goto/16 :goto_0

    .line 19747
    :pswitch_1
    instance-of v1, p1, LX/0I0;

    if-nez v1, :cond_2

    .line 19748
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 19749
    :cond_2
    move-object v0, p1

    check-cast v0, LX/0I0;

    move-object v1, v0

    .line 19750
    const-string v3, "MessageEncoder"

    const-string v4, "send/%s; topics=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-virtual {v1}, LX/0I0;->b()LX/0I1;

    move-result-object v6

    iget-object v6, v6, LX/0I1;->a:Ljava/util/List;

    aput-object v6, v5, v2

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19751
    const/4 v3, 0x0

    .line 19752
    iget-object v0, v1, LX/07W;->a:LX/07R;

    move-object v4, v0

    .line 19753
    invoke-virtual {v1}, LX/0I0;->a()LX/0B6;

    move-result-object v5

    .line 19754
    invoke-virtual {v1}, LX/0I0;->b()LX/0I1;

    move-result-object v6

    .line 19755
    iget-object v0, v6, LX/0I1;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0AF;

    .line 19756
    iget-object v0, v0, LX/0AF;->a:Ljava/lang/String;

    .line 19757
    invoke-static {v0}, LX/07Z;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 19758
    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    .line 19759
    add-int/lit8 v0, v0, 0x1

    move v2, v0

    .line 19760
    goto :goto_3

    .line 19761
    :cond_3
    add-int/lit8 v2, v2, 0x2

    .line 19762
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-static {v4}, LX/07Z;->a(LX/07R;)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19763
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-static {v0, v2}, LX/07Z;->a(Ljava/io/DataOutputStream;I)I

    move-result v0

    add-int/lit8 v4, v0, 0x1

    .line 19764
    iget v0, v5, LX/0B6;->a:I

    .line 19765
    iget-object v5, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 19766
    iget-object v0, v6, LX/0I1;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0AF;

    .line 19767
    iget-object v6, v0, LX/0AF;->a:Ljava/lang/String;

    .line 19768
    invoke-static {v6}, LX/07Z;->a(Ljava/lang/String;)[B

    move-result-object v6

    .line 19769
    iget-object v7, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    array-length v8, v6

    invoke-virtual {v7, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 19770
    iget-object v7, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    array-length v8, v6

    invoke-virtual {v7, v6, v3, v8}, Ljava/io/DataOutputStream;->write([BII)V

    .line 19771
    iget-object v6, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    iget v0, v0, LX/0AF;->b:I

    invoke-virtual {v6, v0}, Ljava/io/DataOutputStream;->write(I)V

    goto :goto_4

    .line 19772
    :cond_4
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    .line 19773
    add-int v0, v4, v2

    .line 19774
    move v1, v0

    .line 19775
    move v2, v1

    .line 19776
    goto/16 :goto_0

    .line 19777
    :pswitch_2
    instance-of v1, p1, LX/0Hy;

    if-nez v1, :cond_5

    .line 19778
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 19779
    :cond_5
    move-object v0, p1

    check-cast v0, LX/0Hy;

    move-object v1, v0

    .line 19780
    const-string v3, "MessageEncoder"

    const-string v4, "send/%s; id=%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-virtual {v1}, LX/0Hy;->a()LX/0B6;

    move-result-object v6

    iget v6, v6, LX/0B6;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19781
    invoke-virtual {v1}, LX/0Hy;->b()LX/0Hz;

    move-result-object v0

    iget-object v0, v0, LX/0Hz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 19782
    add-int/lit8 v2, v0, 0x2

    .line 19783
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    .line 19784
    iget-object v3, v1, LX/07W;->a:LX/07R;

    move-object v3, v3

    .line 19785
    invoke-static {v3}, LX/07Z;->a(LX/07R;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19786
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-static {v0, v2}, LX/07Z;->a(Ljava/io/DataOutputStream;I)I

    move-result v0

    add-int/lit8 v3, v0, 0x1

    .line 19787
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, LX/0Hy;->a()LX/0B6;

    move-result-object v4

    iget v4, v4, LX/0B6;->a:I

    invoke-virtual {v0, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 19788
    invoke-virtual {v1}, LX/0Hy;->b()LX/0Hz;

    move-result-object v0

    iget-object v0, v0, LX/0Hz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 19789
    iget-object v5, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto :goto_5

    .line 19790
    :cond_6
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    .line 19791
    add-int v0, v3, v2

    .line 19792
    move v1, v0

    .line 19793
    move v2, v1

    .line 19794
    goto/16 :goto_0

    .line 19795
    :pswitch_3
    instance-of v1, p1, LX/0I3;

    if-nez v1, :cond_7

    .line 19796
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 19797
    :cond_7
    move-object v0, p1

    check-cast v0, LX/0I3;

    move-object v1, v0

    .line 19798
    const-string v3, "MessageEncoder"

    const-string v4, "send/%s; topics=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-virtual {v1}, LX/0I3;->b()LX/0I4;

    move-result-object v6

    iget-object v6, v6, LX/0I4;->a:Ljava/util/List;

    aput-object v6, v5, v2

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19799
    const/4 v3, 0x0

    .line 19800
    iget-object v0, v1, LX/07W;->a:LX/07R;

    move-object v4, v0

    .line 19801
    invoke-virtual {v1}, LX/0I3;->a()LX/0B6;

    move-result-object v5

    .line 19802
    invoke-virtual {v1}, LX/0I3;->b()LX/0I4;

    move-result-object v6

    .line 19803
    iget-object v0, v6, LX/0I4;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 19804
    invoke-static {v0}, LX/07Z;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 19805
    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    move v2, v0

    .line 19806
    goto :goto_6

    .line 19807
    :cond_8
    add-int/lit8 v2, v2, 0x2

    .line 19808
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-static {v4}, LX/07Z;->a(LX/07R;)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19809
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-static {v0, v2}, LX/07Z;->a(Ljava/io/DataOutputStream;I)I

    move-result v0

    add-int/lit8 v4, v0, 0x1

    .line 19810
    iget v0, v5, LX/0B6;->a:I

    .line 19811
    iget-object v5, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 19812
    iget-object v0, v6, LX/0I4;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 19813
    invoke-static {v0}, LX/07Z;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 19814
    iget-object v6, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    array-length v7, v0

    invoke-virtual {v6, v7}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 19815
    iget-object v6, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    array-length v7, v0

    invoke-virtual {v6, v0, v3, v7}, Ljava/io/DataOutputStream;->write([BII)V

    goto :goto_7

    .line 19816
    :cond_9
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    .line 19817
    add-int v0, v4, v2

    .line 19818
    move v1, v0

    .line 19819
    move v2, v1

    .line 19820
    goto/16 :goto_0

    .line 19821
    :pswitch_4
    instance-of v1, p1, LX/0I2;

    if-nez v1, :cond_a

    .line 19822
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 19823
    :cond_a
    move-object v0, p1

    check-cast v0, LX/0I2;

    move-object v1, v0

    .line 19824
    const-string v3, "MessageEncoder"

    const-string v4, "send/%s; id=%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-virtual {v1}, LX/0I2;->a()LX/0B6;

    move-result-object v6

    iget v6, v6, LX/0B6;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19825
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    .line 19826
    iget-object v2, v1, LX/07W;->a:LX/07R;

    move-object v2, v2

    .line 19827
    invoke-static {v2}, LX/07Z;->a(LX/07R;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19828
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    const/4 v2, 0x2

    invoke-static {v0, v2}, LX/07Z;->a(Ljava/io/DataOutputStream;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 19829
    iget-object v2, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, LX/0I2;->a()LX/0B6;

    move-result-object v3

    iget v3, v3, LX/0B6;->a:I

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 19830
    iget-object v2, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 19831
    add-int/lit8 v0, v0, 0x2

    .line 19832
    move v1, v0

    .line 19833
    move v2, v1

    .line 19834
    goto/16 :goto_0

    .line 19835
    :pswitch_5
    instance-of v1, p1, LX/07X;

    if-nez v1, :cond_b

    .line 19836
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 19837
    :cond_b
    move-object v0, p1

    check-cast v0, LX/07X;

    move-object v1, v0

    .line 19838
    const-string v3, "MessageEncoder"

    const-string v4, "send/%s; id=%d, qos=%d, topic=%s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-virtual {v1}, LX/07X;->a()LX/0B5;

    move-result-object v6

    iget v6, v6, LX/0B5;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x2

    .line 19839
    iget-object v0, v1, LX/07W;->a:LX/07R;

    move-object v6, v0

    .line 19840
    iget v6, v6, LX/07R;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x3

    invoke-virtual {v1}, LX/07X;->a()LX/0B5;

    move-result-object v6

    iget-object v6, v6, LX/0B5;->a:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19841
    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 19842
    iget-object v0, v1, LX/07W;->a:LX/07R;

    move-object v5, v0

    .line 19843
    invoke-virtual {v1}, LX/07X;->a()LX/0B5;

    move-result-object v6

    .line 19844
    invoke-virtual {v1}, LX/07X;->b()[B

    move-result-object v0

    .line 19845
    iget v2, p0, LX/07Q;->a:I

    if-eqz v2, :cond_c

    .line 19846
    invoke-static {v0}, LX/05K;->a([B)[B

    move-result-object v2

    .line 19847
    iget v7, p0, LX/07Q;->a:I

    if-ne v3, v7, :cond_13

    array-length v7, v2

    array-length v8, v0

    if-le v7, v8, :cond_13

    .line 19848
    const/4 v2, 0x1

    .line 19849
    iput-boolean v2, v5, LX/07R;->e:Z

    .line 19850
    :cond_c
    :goto_8
    iget-object v2, v6, LX/0B5;->a:Ljava/lang/String;

    .line 19851
    invoke-static {v2}, LX/07Z;->a(Ljava/lang/String;)[B

    move-result-object v7

    .line 19852
    array-length v2, v7

    add-int/lit8 v8, v2, 0x2

    iget v2, v5, LX/07R;->c:I

    if-lez v2, :cond_14

    move v2, v3

    :goto_9
    add-int/2addr v2, v8

    .line 19853
    array-length v3, v0

    add-int/2addr v2, v3

    .line 19854
    iget-object v3, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-static {v5}, LX/07Z;->a(LX/07R;)I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19855
    iget-object v3, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-static {v3, v2}, LX/07Z;->a(Ljava/io/DataOutputStream;I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    .line 19856
    iget-object v8, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    array-length v9, v7

    invoke-virtual {v8, v9}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 19857
    iget-object v8, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    array-length v9, v7

    invoke-virtual {v8, v7, v4, v9}, Ljava/io/DataOutputStream;->write([BII)V

    .line 19858
    iget v5, v5, LX/07R;->c:I

    if-lez v5, :cond_d

    .line 19859
    iget-object v5, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    iget v6, v6, LX/0B5;->b:I

    invoke-virtual {v5, v6}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 19860
    :cond_d
    iget-object v5, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    array-length v6, v0

    invoke-virtual {v5, v0, v4, v6}, Ljava/io/DataOutputStream;->write([BII)V

    .line 19861
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    .line 19862
    add-int v0, v3, v2

    .line 19863
    move v1, v0

    .line 19864
    move v2, v1

    .line 19865
    goto/16 :goto_0

    .line 19866
    :pswitch_6
    instance-of v1, p1, LX/0B7;

    if-nez v1, :cond_e

    .line 19867
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 19868
    :cond_e
    move-object v0, p1

    check-cast v0, LX/0B7;

    move-object v1, v0

    .line 19869
    const-string v3, "MessageEncoder"

    const-string v4, "send/%s; id=%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-virtual {v1}, LX/0B7;->a()LX/0B6;

    move-result-object v6

    iget v6, v6, LX/0B6;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19870
    iget-object v0, v1, LX/07W;->a:LX/07R;

    move-object v0, v0

    .line 19871
    invoke-virtual {v1}, LX/0B7;->a()LX/0B6;

    move-result-object v2

    .line 19872
    iget-object v3, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-static {v0}, LX/07Z;->a(LX/07R;)I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19873
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    const/4 v3, 0x2

    invoke-static {v0, v3}, LX/07Z;->a(Ljava/io/DataOutputStream;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 19874
    iget-object v3, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    iget v2, v2, LX/0B6;->a:I

    invoke-virtual {v3, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 19875
    add-int/lit8 v0, v0, 0x2

    .line 19876
    iget-object v2, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 19877
    move v1, v0

    .line 19878
    move v2, v1

    .line 19879
    goto/16 :goto_0

    .line 19880
    :pswitch_7
    const-string v1, "MessageEncoder"

    const-string v3, "send/%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v1, v3, v4}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19881
    const/4 v2, 0x0

    .line 19882
    iget-object v0, p1, LX/07W;->a:LX/07R;

    move-object v0, v0

    .line 19883
    iget-object v1, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-static {v0}, LX/07Z;->a(LX/07R;)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19884
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19885
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    .line 19886
    move v1, v2

    .line 19887
    move v2, v1

    .line 19888
    goto/16 :goto_0

    .line 19889
    :pswitch_8
    const-string v1, "MessageEncoder"

    const-string v3, "send/%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v1, v3, v4}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19890
    const/4 v2, 0x0

    .line 19891
    iget-object v0, p1, LX/07W;->a:LX/07R;

    move-object v0, v0

    .line 19892
    iget-object v1, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-static {v0}, LX/07Z;->a(LX/07R;)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19893
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19894
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    .line 19895
    move v1, v2

    .line 19896
    move v2, v1

    .line 19897
    goto/16 :goto_0

    .line 19898
    :pswitch_9
    instance-of v1, p1, LX/07V;

    if-nez v1, :cond_f

    .line 19899
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 19900
    :cond_f
    move-object v0, p1

    check-cast v0, LX/07V;

    move-object v1, v0

    .line 19901
    const-string v3, "MessageEncoder"

    const-string v4, "send/%s; keepaliveSec=%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-virtual {v1}, LX/07V;->a()LX/07U;

    move-result-object v6

    iget v6, v6, LX/07U;->h:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19902
    iget-object v2, p0, LX/07Q;->c:LX/05L;

    iget-object v3, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-interface {v2, v3, v1}, LX/05L;->a(Ljava/io/DataOutputStream;LX/07V;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    move v2, v1

    .line 19903
    goto/16 :goto_0

    :cond_10
    :try_start_3
    move v0, v2

    .line 19904
    goto/16 :goto_1

    .line 19905
    :cond_11
    invoke-virtual {v1}, LX/07j;->b()LX/07h;

    move-result-object v3

    if-nez v3, :cond_12

    :goto_a
    invoke-static {v0}, LX/01n;->b(Z)V

    .line 19906
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    .line 19907
    iget-object v3, v1, LX/07W;->a:LX/07R;

    move-object v3, v3

    .line 19908
    invoke-static {v3}, LX/07Z;->a(LX/07R;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19909
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19910
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19911
    iget-object v0, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, LX/07j;->a()LX/07e;

    move-result-object v2

    iget-byte v2, v2, LX/07e;->a:B

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 19912
    const/4 v0, 0x4

    .line 19913
    iget-object v2, p0, LX/07Q;->e:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    goto/16 :goto_2

    :cond_12
    move v0, v2

    .line 19914
    goto :goto_a
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_13
    move-object v0, v2

    .line 19915
    goto/16 :goto_8

    :cond_14
    move v2, v4

    .line 19916
    goto/16 :goto_9

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
