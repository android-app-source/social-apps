.class public final LX/0Fa;
.super LX/084;
.source ""


# instance fields
.field private final mDexStore:LX/02U;

.field private final mFlags:I

.field private final mTmpDir:LX/087;


# direct methods
.method public constructor <init>(LX/02U;I)V
    .locals 1

    .prologue
    .line 33604
    invoke-direct {p0}, LX/084;-><init>()V

    .line 33605
    iput-object p1, p0, LX/0Fa;->mDexStore:LX/02U;

    .line 33606
    iput p2, p0, LX/0Fa;->mFlags:I

    .line 33607
    const-string v0, "turbo-art-compiler"

    invoke-virtual {p1, v0}, LX/02U;->makeTemporaryDirectory(Ljava/lang/String;)LX/087;

    move-result-object v0

    iput-object v0, p0, LX/0Fa;->mTmpDir:LX/087;

    .line 33608
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 33609
    iget-object v0, p0, LX/0Fa;->mTmpDir:LX/087;

    invoke-virtual {v0}, LX/087;->close()V

    .line 33610
    return-void
.end method

.method public final compile(LX/08A;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 33611
    iget-object v0, p1, LX/08A;->dex:LX/02Z;

    invoke-static {v0}, LX/0Fb;->makeDexName(LX/02Z;)Ljava/lang/String;

    move-result-object v0

    .line 33612
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, LX/0Fa;->mDexStore:LX/02U;

    iget-object v3, v3, LX/02U;->root:Ljava/io/File;

    invoke-direct {v1, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33613
    iget v3, p0, LX/0Fa;->mFlags:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33614
    :goto_0
    return-void

    .line 33615
    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, LX/0Fa;->mTmpDir:LX/087;

    iget-object v3, v3, LX/087;->directory:Ljava/io/File;

    invoke-direct {v1, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33616
    invoke-virtual {p1}, LX/08A;->getDexContents()Ljava/io/InputStream;

    move-result-object v3

    .line 33617
    :try_start_0
    invoke-virtual {p1, v3}, LX/08A;->getSizeHint(Ljava/io/InputStream;)I

    move-result v4

    .line 33618
    const-string v5, "size hint for %s: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v7

    invoke-static {v5, v6}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33619
    new-instance v4, Ljava/io/RandomAccessFile;

    const-string v5, "rw"

    invoke-direct {v4, v1, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 33620
    const v5, 0x7fffffff

    const v6, 0x8000

    :try_start_1
    new-array v6, v6, [B

    invoke-static {v4, v3, v5, v6}, LX/02Q;->copyBytes(Ljava/io/RandomAccessFile;Ljava/io/InputStream;I[B)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 33621
    :try_start_2
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 33622
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 33623
    :cond_1
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, LX/0Fa;->mDexStore:LX/02U;

    iget-object v3, v3, LX/02U;->root:Ljava/io/File;

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1, v2}, LX/02Q;->renameOrThrow(Ljava/io/File;Ljava/io/File;)V

    goto :goto_0

    .line 33624
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 33625
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_1
    if-eqz v1, :cond_3

    :try_start_4
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :goto_2
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 33626
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 33627
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v3, :cond_2

    if-eqz v2, :cond_4

    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_2
    :goto_4
    throw v0

    .line 33628
    :catch_2
    move-exception v4

    :try_start_8
    invoke-static {v1, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 33629
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 33630
    :cond_3
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 33631
    :catch_3
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_4

    .line 33632
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method
