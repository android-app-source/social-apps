.class public LX/0Dj;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:I

.field private static b:LX/0Dj;


# instance fields
.field public c:LX/0Di;

.field public d:Landroid/os/Handler;

.field public e:Ljava/lang/Runnable;

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30983
    const/4 v0, -0x1

    sput v0, LX/0Dj;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30985
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Dj;->f:Z

    return-void
.end method

.method public static declared-synchronized a()LX/0Dj;
    .locals 2

    .prologue
    .line 30986
    const-class v1, LX/0Dj;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0Dj;->b:LX/0Dj;

    if-nez v0, :cond_0

    .line 30987
    new-instance v0, LX/0Dj;

    invoke-direct {v0}, LX/0Dj;-><init>()V

    sput-object v0, LX/0Dj;->b:LX/0Dj;

    .line 30988
    :cond_0
    sget-object v0, LX/0Dj;->b:LX/0Dj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 30989
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized c()V
    .locals 5

    .prologue
    .line 30990
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0Dj;->f:Z

    if-nez v0, :cond_0

    sget v0, LX/0Dj;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gtz v0, :cond_1

    .line 30991
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 30992
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/0Dj;->f:Z

    .line 30993
    iget-object v0, p0, LX/0Dj;->d:Landroid/os/Handler;

    if-nez v0, :cond_2

    .line 30994
    invoke-static {}, LX/0Di;->a()LX/0Di;

    move-result-object v0

    iput-object v0, p0, LX/0Dj;->c:LX/0Di;

    .line 30995
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/0Dj;->d:Landroid/os/Handler;

    .line 30996
    new-instance v0, Lcom/facebook/browser/lite/logging/VideoTimeSpentLogger$1;

    invoke-direct {v0, p0}, Lcom/facebook/browser/lite/logging/VideoTimeSpentLogger$1;-><init>(LX/0Dj;)V

    iput-object v0, p0, LX/0Dj;->e:Ljava/lang/Runnable;

    .line 30997
    :cond_2
    iget-object v0, p0, LX/0Dj;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/0Dj;->e:Ljava/lang/Runnable;

    sget v2, LX/0Dj;->a:I

    int-to-long v2, v2

    const v4, 0x34978c54

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 30998
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    .prologue
    .line 30999
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0Dj;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 31000
    :goto_0
    monitor-exit p0

    return-void

    .line 31001
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, LX/0Dj;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 31002
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
