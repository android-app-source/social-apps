.class public final LX/0DF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:LX/0DK;

.field public final synthetic b:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

.field public final synthetic c:LX/0DI;


# direct methods
.method public constructor <init>(LX/0DI;LX/0DK;Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)V
    .locals 0

    .prologue
    .line 29733
    iput-object p1, p0, LX/0DF;->c:LX/0DI;

    iput-object p2, p0, LX/0DF;->a:LX/0DK;

    iput-object p3, p0, LX/0DF;->b:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 29734
    iget-object v0, p0, LX/0DF;->c:LX/0DI;

    iget-object v1, p0, LX/0DF;->a:LX/0DK;

    invoke-virtual {v1, p3}, LX/0DK;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 29735
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 29736
    const-string p2, "action"

    const-string p3, "instant_experience_chrome_navigation_link_pressed"

    invoke-virtual {p1, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29737
    const-string p2, "url"

    iget-object p3, v0, LX/0DI;->e:LX/0D5;

    invoke-virtual {p3}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29738
    iget-object p2, v0, LX/0DI;->f:LX/0CQ;

    iget-object p3, v0, LX/0DI;->g:Landroid/os/Bundle;

    invoke-virtual {p2, p1, p3}, LX/0CQ;->a(Ljava/util/Map;Landroid/os/Bundle;)V

    .line 29739
    iget-object p1, v0, LX/0DI;->e:LX/0D5;

    const-string p2, "window.location.href = atob(\'%s\');"

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    const/4 p4, 0x0

    aput-object v1, p3, p4

    invoke-static {p2, p3}, LX/0DI;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/0D5;->a(Ljava/lang/String;)V

    .line 29740
    iget-object v0, p0, LX/0DF;->b:Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->b()V

    .line 29741
    return-void
.end method
