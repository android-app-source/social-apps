.class public final enum LX/01S;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/01S;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/01S;

.field public static final enum DEVELOPMENT:LX/01S;

.field public static final enum FACEBOOK:LX/01S;

.field public static final enum PUBLIC:LX/01S;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4765
    new-instance v0, LX/01S;

    const-string v1, "PUBLIC"

    invoke-direct {v0, v1, v2}, LX/01S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01S;->PUBLIC:LX/01S;

    .line 4766
    new-instance v0, LX/01S;

    const-string v1, "FACEBOOK"

    invoke-direct {v0, v1, v3}, LX/01S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01S;->FACEBOOK:LX/01S;

    .line 4767
    new-instance v0, LX/01S;

    const-string v1, "DEVELOPMENT"

    invoke-direct {v0, v1, v4}, LX/01S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01S;->DEVELOPMENT:LX/01S;

    .line 4768
    const/4 v0, 0x3

    new-array v0, v0, [LX/01S;

    sget-object v1, LX/01S;->PUBLIC:LX/01S;

    aput-object v1, v0, v2

    sget-object v1, LX/01S;->FACEBOOK:LX/01S;

    aput-object v1, v0, v3

    sget-object v1, LX/01S;->DEVELOPMENT:LX/01S;

    aput-object v1, v0, v4

    sput-object v0, LX/01S;->$VALUES:[LX/01S;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4764
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/01S;
    .locals 1

    .prologue
    .line 4762
    const-class v0, LX/01S;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/01S;

    return-object v0
.end method

.method public static values()[LX/01S;
    .locals 1

    .prologue
    .line 4763
    sget-object v0, LX/01S;->$VALUES:[LX/01S;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/01S;

    return-object v0
.end method
