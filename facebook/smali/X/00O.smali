.class public LX/00O;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final BLACKLISTED_METHODS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final LOG_TAG:Ljava/lang/String;

.field public mANRDataProvider:LX/09r;

.field public mANRDetected:Z

.field public final mANRReport:LX/00N;

.field public mANRReportTime:J

.field private final mAnrDetectorRunnable:Lcom/facebook/acra/ANRDetectorRunnable;

.field public final mCauseThrowable:Ljava/lang/Throwable;

.field public final mHandler:Landroid/os/Handler;

.field public mIsInternalBuild:Z

.field public mLastStackTrace:[Ljava/lang/StackTraceElement;

.field public mLastTick:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2309
    new-instance v0, LX/01j;

    invoke-direct {v0}, LX/01j;-><init>()V

    sput-object v0, LX/00O;->BLACKLISTED_METHODS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/acra/ANRDetectorRunnable;LX/00N;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 2301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2302
    const-class v0, LX/00O;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/00O;->LOG_TAG:Ljava/lang/String;

    .line 2303
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/00O;->mANRReportTime:J

    .line 2304
    iput-object p3, p0, LX/00O;->mHandler:Landroid/os/Handler;

    .line 2305
    iput-object p1, p0, LX/00O;->mAnrDetectorRunnable:Lcom/facebook/acra/ANRDetectorRunnable;

    .line 2306
    iput-object p2, p0, LX/00O;->mANRReport:LX/00N;

    .line 2307
    new-instance v0, LX/01k;

    const-string v1, "ANR detected by ANRWatchdog"

    invoke-direct {v0, v1}, LX/01k;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/00O;->mCauseThrowable:Ljava/lang/Throwable;

    .line 2308
    return-void
.end method

.method private static endAndProcessANRDataCapture(LX/00O;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 2288
    iget-boolean v0, p0, LX/00O;->mANRDetected:Z

    if-eqz v0, :cond_1

    .line 2289
    iput-boolean v4, p0, LX/00O;->mANRDetected:Z

    .line 2290
    invoke-static {p0}, LX/00O;->shouldReportANRStats(LX/00O;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2291
    invoke-virtual {p0}, LX/00O;->getRealTimeSinceBoot()J

    move-result-wide v0

    iget-wide v2, p0, LX/00O;->mANRReportTime:J

    sub-long/2addr v0, v2

    .line 2292
    iget-object v2, p0, LX/00O;->mANRReport:LX/00N;

    const/4 v9, 0x0

    .line 2293
    iget-object v3, v2, LX/00N;->mErrorReporter:LX/009;

    const-string v5, "anr_recovery_delay"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 2294
    iget-object v3, v2, LX/00N;->mANRDataProvider:LX/09r;

    if-eqz v3, :cond_2

    iget-object v3, v2, LX/00N;->mANRDataProvider:LX/09r;

    invoke-interface {v3}, LX/09r;->shouldANRDetectorRun()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2295
    iget-object v3, v2, LX/00N;->mContext:Landroid/content/Context;

    const-string v5, "traces"

    invoke-virtual {v3, v5, v9}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-static {v3}, LX/00N;->purgeDirectory(Ljava/io/File;)V

    .line 2296
    :cond_0
    :goto_0
    iget-object v0, p0, LX/00O;->mANRDataProvider:LX/09r;

    if-eqz v0, :cond_1

    .line 2297
    iget-object v0, p0, LX/00O;->mANRDataProvider:LX/09r;

    invoke-interface {v0, v4}, LX/09r;->reportAnrState(Z)V

    .line 2298
    :cond_1
    invoke-static {p0}, LX/00O;->sendMessageToHandler(LX/00O;)V

    .line 2299
    return-void

    .line 2300
    :cond_2
    iget-object v3, v2, LX/00N;->mErrorReporter:LX/009;

    const v5, 0x7fffffff

    const/4 v6, 0x0

    const/4 v7, 0x1

    new-array v7, v7, [LX/01V;

    sget-object v8, LX/01V;->CACHED_ANR_REPORT:LX/01V;

    aput-object v8, v7, v9

    invoke-virtual {v3, v5, v6, v7}, LX/009;->prepareReports(ILX/01i;[LX/01V;)I

    goto :goto_0
.end method

.method public static isANRReportGKAvailable(LX/00O;)Z
    .locals 1

    .prologue
    .line 2287
    iget-object v0, p0, LX/00O;->mANRDataProvider:LX/09r;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static sendMessageToHandler(LX/00O;)V
    .locals 3

    .prologue
    .line 2245
    iget-object v0, p0, LX/00O;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, LX/00O;->mAnrDetectorRunnable:Lcom/facebook/acra/ANRDetectorRunnable;

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    move-result-object v0

    .line 2246
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 2247
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setAsynchronous(Z)V

    .line 2248
    :cond_0
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2249
    return-void
.end method

.method public static shouldReportANRStats(LX/00O;)Z
    .locals 3

    .prologue
    .line 2282
    invoke-static {p0}, LX/00O;->isANRReportGKAvailable(LX/00O;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2283
    iget-object v0, p0, LX/00O;->mANRDataProvider:LX/09r;

    invoke-interface {v0}, LX/09r;->shouldANRDetectorRun()Z

    move-result v0

    .line 2284
    :goto_0
    return v0

    .line 2285
    :cond_0
    invoke-static {}, LX/00L;->getCachedANRGKValue()Z

    move-result v0

    .line 2286
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Using cached ANR GK: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public detectANR(J)V
    .locals 11

    .prologue
    .line 2253
    iget-object v0, p0, LX/00O;->mAnrDetectorRunnable:Lcom/facebook/acra/ANRDetectorRunnable;

    .line 2254
    iget v1, v0, Lcom/facebook/acra/ANRDetectorRunnable;->mTick:I

    move v0, v1

    .line 2255
    iget v1, p0, LX/00O;->mLastTick:I

    if-ne v0, v1, :cond_3

    .line 2256
    iget-boolean v1, p0, LX/00O;->mIsInternalBuild:Z

    if-eqz v1, :cond_4

    invoke-static {}, Landroid/os/Debug;->isDebuggerConnected()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2257
    if-nez v1, :cond_3

    .line 2258
    const/4 v9, 0x1

    .line 2259
    iget-boolean v3, p0, LX/00O;->mANRDetected:Z

    if-nez v3, :cond_0

    .line 2260
    iput-boolean v9, p0, LX/00O;->mANRDetected:Z

    .line 2261
    iget-object v3, p0, LX/00O;->mANRDataProvider:LX/09r;

    if-eqz v3, :cond_0

    .line 2262
    iget-object v3, p0, LX/00O;->mANRDataProvider:LX/09r;

    invoke-interface {v3, v9}, LX/09r;->reportAnrState(Z)V

    .line 2263
    :cond_0
    iget-object v3, p0, LX/00O;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    .line 2264
    const/4 v4, 0x0

    .line 2265
    array-length v6, v3

    move v5, v4

    :goto_1
    if-ge v5, v6, :cond_1

    aget-object v7, v3, v5

    .line 2266
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "."

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v7}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2267
    sget-object v8, LX/00O;->BLACKLISTED_METHODS:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2268
    const/4 v4, 0x1

    .line 2269
    :cond_1
    move v4, v4

    .line 2270
    if-nez v4, :cond_2

    iget-object v4, p0, LX/00O;->mLastStackTrace:[Ljava/lang/StackTraceElement;

    invoke-static {v4, v3}, Ljava/util/Arrays;->deepEquals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2271
    iput-object v3, p0, LX/00O;->mLastStackTrace:[Ljava/lang/StackTraceElement;

    .line 2272
    invoke-static {p0}, LX/00O;->shouldReportANRStats(LX/00O;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2273
    invoke-virtual {p0}, LX/00O;->getRealTimeSinceBoot()J

    move-result-wide v5

    iput-wide v5, p0, LX/00O;->mANRReportTime:J

    .line 2274
    iget-object v4, p0, LX/00O;->mCauseThrowable:Ljava/lang/Throwable;

    invoke-virtual {v4, v3}, Ljava/lang/Throwable;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 2275
    iget-object v3, p0, LX/00O;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Generating ANR Report"

    iget-object v5, p0, LX/00O;->mCauseThrowable:Ljava/lang/Throwable;

    invoke-static {v3, v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2276
    iget-object v4, p0, LX/00O;->mANRReport:LX/00N;

    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    move-result-wide v7

    invoke-static {p0}, LX/00O;->isANRReportGKAvailable(LX/00O;)Z

    move-result v3

    if-nez v3, :cond_5

    :goto_2
    move-wide v5, p1

    invoke-virtual/range {v4 .. v9}, LX/00N;->collectThreadDump(JJZ)V

    .line 2277
    :cond_2
    :goto_3
    iput v0, p0, LX/00O;->mLastTick:I

    .line 2278
    return-void

    .line 2279
    :cond_3
    invoke-static {p0}, LX/00O;->endAndProcessANRDataCapture(LX/00O;)V

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2280
    :cond_5
    const/4 v9, 0x0

    goto :goto_2

    .line 2281
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method public getRealTimeSinceBoot()J
    .locals 2

    .prologue
    .line 2252
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public start()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .prologue
    .line 2250
    invoke-static {p0}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->getInstance(LX/00O;)Lcom/facebook/acra/ANRDetector$ANRDetectorThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->start()V

    .line 2251
    return-void
.end method
