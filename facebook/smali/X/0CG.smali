.class public final LX/0CG;
.super LX/0C7;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/0CQ;


# direct methods
.method public constructor <init>(LX/0CQ;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27127
    iput-object p1, p0, LX/0CG;->b:LX/0CQ;

    iput-object p2, p0, LX/0CG;->a:Landroid/content/Context;

    invoke-direct {p0, p1}, LX/0C7;-><init>(LX/0CQ;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0DT;)V
    .locals 2

    .prologue
    .line 27128
    iget-object v0, p0, LX/0CG;->a:Landroid/content/Context;

    .line 27129
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p0, 0x13

    if-ge v1, p0, :cond_0

    .line 27130
    :goto_0
    invoke-interface {p1}, LX/0DT;->b()V

    .line 27131
    return-void

    .line 27132
    :cond_0
    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 27133
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v1

    .line 27134
    invoke-static {v1}, LX/048;->a(Landroid/webkit/CookieManager;)V

    goto :goto_0
.end method
