.class public abstract LX/051;
.super LX/052;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public b:LX/05Q;

.field public c:LX/056;

.field public d:LX/059;

.field public e:LX/06R;

.field public f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field public volatile g:LX/05h;

.field public h:LX/05i;

.field public i:LX/05b;

.field public j:LX/04p;

.field public k:LX/04v;

.field public l:J

.field public volatile m:LX/04q;

.field public final n:LX/055;

.field public o:LX/053;

.field public p:LX/05Z;

.field private final q:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 15131
    invoke-direct {p0}, LX/052;-><init>()V

    .line 15132
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 15133
    sget-object v0, LX/053;->DISCONNECTED:LX/053;

    iput-object v0, p0, LX/051;->o:LX/053;

    .line 15134
    new-instance v0, LX/054;

    invoke-direct {v0, p0}, LX/054;-><init>(LX/051;)V

    iput-object v0, p0, LX/051;->q:Landroid/os/IBinder;

    .line 15135
    new-instance v0, LX/055;

    invoke-direct {v0, p0}, LX/055;-><init>(LX/051;)V

    iput-object v0, p0, LX/051;->n:LX/055;

    return-void
.end method

.method private static a(Landroid/os/Messenger;)V
    .locals 4
    .param p0    # Landroid/os/Messenger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 15111
    if-nez p0, :cond_0

    .line 15112
    :goto_0
    return-void

    .line 15113
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 15114
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 15115
    invoke-virtual {p0, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 15116
    :catch_0
    move-exception v0

    .line 15117
    const-string v1, "MqttPushService"

    const-string v2, "exception/send_ack"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private b(LX/06f;)V
    .locals 1

    .prologue
    .line 15136
    iget-object v0, p0, LX/051;->c:LX/056;

    invoke-virtual {v0, p1}, LX/056;->a(LX/06f;)V

    .line 15137
    return-void
.end method

.method private q()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 15138
    sget-object v0, LX/01p;->h:LX/01q;

    invoke-static {p0, v0}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(LX/0AX;)Ljava/util/concurrent/Future;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0AX;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 15139
    const-string v0, "MqttPushService"

    const-string v1, "service/stop; reason=%s\'"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15140
    sget-object v0, LX/07C;->a:LX/07C;

    .line 15141
    iget-object v1, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 15142
    const-string v1, "MqttPushService"

    const-string v2, "service/stop/inactive_connection"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15143
    :goto_0
    return-object v0

    .line 15144
    :cond_0
    invoke-virtual {p0}, LX/051;->i()V

    .line 15145
    iget-object v0, p0, LX/051;->c:LX/056;

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 15146
    invoke-static {v0}, LX/056;->v(LX/056;)V

    .line 15147
    iget-object v1, v0, LX/056;->Q:LX/05b;

    .line 15148
    iget-object v2, v1, LX/05b;->d:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ScreenStateListener unregistration should be called on MqttThread. Current Looper:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/05Y;->a(ZLjava/lang/String;)V

    .line 15149
    :try_start_0
    iget-object v2, v1, LX/05b;->b:Landroid/content/Context;

    iget-object v3, v1, LX/05b;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 15150
    :goto_1
    iget-object v2, v1, LX/05b;->g:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 15151
    iget-object v1, v0, LX/056;->L:LX/059;

    iget-object v2, v0, LX/056;->V:LX/058;

    invoke-virtual {v1, v2}, LX/059;->b(LX/058;)V

    .line 15152
    iget-object v1, v0, LX/056;->O:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 15153
    :try_start_1
    iget-object v1, v0, LX/056;->M:Landroid/content/Context;

    iget-object v2, v0, LX/056;->O:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 15154
    :goto_2
    iput-object v6, v0, LX/056;->O:Landroid/content/BroadcastReceiver;

    .line 15155
    :cond_1
    iget-object v1, v0, LX/056;->P:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_2

    .line 15156
    :try_start_2
    iget-object v1, v0, LX/056;->M:Landroid/content/Context;

    iget-object v2, v0, LX/056;->P:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 15157
    :goto_3
    iput-object v6, v0, LX/056;->P:Landroid/content/BroadcastReceiver;

    .line 15158
    :cond_2
    iget-object v1, v0, LX/056;->R:LX/05e;

    .line 15159
    iget-object v2, v1, LX/05e;->c:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_3

    .line 15160
    :try_start_3
    iget-object v2, v1, LX/05e;->a:Landroid/content/Context;

    iget-object v3, v1, LX/05e;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    .line 15161
    :goto_4
    const/4 v2, 0x0

    iput-object v2, v1, LX/05e;->c:Landroid/content/BroadcastReceiver;

    .line 15162
    :cond_3
    iget-object v0, p0, LX/051;->c:LX/056;

    invoke-virtual {v0, p1}, LX/056;->a(LX/0AX;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 15163
    sget-object v1, LX/053;->DISCONNECTED:LX/053;

    invoke-virtual {p0, v1}, LX/051;->a(LX/053;)Z

    .line 15164
    move-object v0, v0

    .line 15165
    goto :goto_0

    .line 15166
    :catch_0
    move-exception v1

    .line 15167
    const-string v2, "FbnsConnectionManager"

    const-string v3, "Failed to unregister broadcast receiver"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 15168
    :catch_1
    move-exception v1

    .line 15169
    const-string v2, "FbnsConnectionManager"

    const-string v3, "Failed to unregister broadcast receiver"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :catch_2
    goto :goto_1

    .line 15170
    :catch_3
    move-exception v2

    .line 15171
    const-string v3, "ZeroRatingConnectionConfigOverrides"

    const-string v4, "Failed to unregister broadcast receiver"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4
.end method

.method public a(LX/06f;)V
    .locals 12

    .prologue
    const/4 v4, 0x1

    .line 15172
    const-string v0, "MqttPushService"

    const-string v1, "service/start; reason=%s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15173
    iget-object v0, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 15174
    iget-object v0, p0, LX/051;->h:LX/05i;

    invoke-virtual {p1}, LX/06f;->name()Ljava/lang/String;

    move-result-object v1

    .line 15175
    iget-object v5, v0, LX/05i;->f:LX/05j;

    .line 15176
    iget-object v6, v5, LX/05j;->d:Ljava/lang/String;

    if-nez v6, :cond_0

    .line 15177
    iput-object v1, v5, LX/05j;->d:Ljava/lang/String;

    .line 15178
    iget-object v6, v5, LX/05j;->f:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v7, v5, LX/05j;->b:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v7}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 15179
    invoke-virtual {v5}, LX/05j;->e()V

    .line 15180
    invoke-static {v5}, LX/05j;->f(LX/05j;)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "last_seen"

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 15181
    iget-object v8, v5, LX/05j;->e:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v9, v5, LX/05j;->c:LX/01o;

    invoke-virtual {v9}, LX/01o;->a()J

    move-result-wide v10

    sub-long v6, v10, v6

    invoke-virtual {v8, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 15182
    invoke-virtual {v5}, LX/05j;->b()V

    .line 15183
    :cond_0
    invoke-virtual {p0}, LX/051;->h()V

    .line 15184
    iget-object v0, p0, LX/051;->c:LX/056;

    const/4 v5, 0x0

    .line 15185
    new-instance v1, LX/06h;

    invoke-direct {v1, v0}, LX/06h;-><init>(LX/056;)V

    iput-object v1, v0, LX/056;->O:Landroid/content/BroadcastReceiver;

    .line 15186
    iget-object v1, v0, LX/056;->M:Landroid/content/Context;

    iget-object v2, v0, LX/056;->O:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.os.action.POWER_SAVE_MODE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, LX/056;->y:Landroid/os/Handler;

    invoke-virtual {v1, v2, v3, v5, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 15187
    new-instance v1, LX/06i;

    invoke-direct {v1, v0}, LX/06i;-><init>(LX/056;)V

    iput-object v1, v0, LX/056;->P:Landroid/content/BroadcastReceiver;

    .line 15188
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 15189
    const-string v2, "com.facebook.rti.mqtt.ACTION_MQTT_CONFIG_OVERRIDE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 15190
    const-string v2, "com.facebook.rti.mqtt.ACTION_MQTT_CONFIG_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 15191
    iget-object v2, v0, LX/056;->M:Landroid/content/Context;

    iget-object v3, v0, LX/056;->P:Landroid/content/BroadcastReceiver;

    iget-object v4, v0, LX/056;->y:Landroid/os/Handler;

    invoke-virtual {v2, v3, v1, v5, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 15192
    iget-object v1, v0, LX/056;->L:LX/059;

    iget-object v2, v0, LX/056;->V:LX/058;

    invoke-virtual {v1, v2}, LX/059;->a(LX/058;)V

    .line 15193
    iget-object v1, v0, LX/056;->R:LX/05e;

    .line 15194
    iget-object v2, v1, LX/05e;->c:Landroid/content/BroadcastReceiver;

    if-nez v2, :cond_1

    .line 15195
    new-instance v2, LX/06j;

    invoke-direct {v2, v1}, LX/06j;-><init>(LX/05e;)V

    iput-object v2, v1, LX/05e;->c:Landroid/content/BroadcastReceiver;

    .line 15196
    iget-object v2, v1, LX/05e;->a:Landroid/content/Context;

    iget-object v3, v1, LX/05e;->c:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "com.facebook.rti.mqtt.ACTION_ZR_SWITCH"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 15197
    :cond_1
    invoke-direct {p0}, LX/051;->q()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "mqtt/network_state"

    iget-object v2, p0, LX/051;->d:LX/059;

    invoke-virtual {v2}, LX/059;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 15198
    :cond_2
    invoke-direct {p0, p1}, LX/051;->b(LX/06f;)V

    .line 15199
    return-void
.end method

.method public a(LX/07W;)V
    .locals 0

    .prologue
    .line 15200
    return-void
.end method

.method public a(LX/0Hs;)V
    .locals 0
    .param p1    # LX/0Hs;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 15262
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 15201
    return-void
.end method

.method public a(Landroid/content/Intent;II)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 15202
    const-string v2, "NULL"

    .line 15203
    const/4 v3, 0x0

    .line 15204
    if-eqz p1, :cond_0

    .line 15205
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 15206
    const-string v0, "caller"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 15207
    :cond_0
    const-string v0, "MqttPushService"

    const-string v1, "service/onStart; flag=%d, id=%d, intent=%s, caller=%s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v11

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object v2, v4, v5

    const/4 v5, 0x3

    aput-object v3, v4, v5

    invoke-static {v0, v1, v4}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15208
    iget-object v1, p0, LX/051;->g:LX/05h;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v5

    iget-object v0, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    const/4 v7, -0x1

    iget-object v0, p0, LX/051;->d:LX/059;

    invoke-virtual {v0}, LX/059;->h()J

    move-result-wide v8

    iget-object v0, p0, LX/051;->d:LX/059;

    invoke-virtual {v0}, LX/059;->d()Landroid/net/NetworkInfo;

    move-result-object v10

    invoke-virtual/range {v1 .. v10}, LX/05h;->a(Ljava/lang/String;Ljava/lang/String;LX/05B;LX/05B;ZIJLandroid/net/NetworkInfo;)V

    .line 15209
    if-nez p1, :cond_2

    .line 15210
    const-string v0, "MqttPushService"

    const-string v1, "service/onStart/process_restart"

    new-array v2, v11, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15211
    sget-object v0, LX/06f;->SERVICE_RESTART:LX/06f;

    invoke-virtual {p0, v0}, LX/051;->a(LX/06f;)V

    .line 15212
    :cond_1
    :goto_0
    return-void

    .line 15213
    :cond_2
    const-string v0, "Orca.STOP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 15214
    const-string v0, "MESSENGER"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    .line 15215
    sget-object v1, LX/0AX;->SERVICE_STOP:LX/0AX;

    invoke-virtual {p0, v1}, LX/051;->a(LX/0AX;)Ljava/util/concurrent/Future;

    .line 15216
    invoke-virtual {p0}, LX/051;->stopSelf()V

    .line 15217
    invoke-static {v0}, LX/051;->a(Landroid/os/Messenger;)V

    goto :goto_0

    .line 15218
    :cond_3
    const-string v0, "Orca.START"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 15219
    const-string v0, "MESSENGER"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    .line 15220
    sget-object v1, LX/06f;->SERVICE_START:LX/06f;

    invoke-virtual {p0, v1}, LX/051;->a(LX/06f;)V

    .line 15221
    invoke-static {v0}, LX/051;->a(Landroid/os/Messenger;)V

    goto :goto_0

    .line 15222
    :cond_4
    const-string v0, "Orca.PERSISTENT_KICK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "Orca.PERSISTENT_KICK_SKIP_PING"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 15223
    :cond_5
    invoke-virtual {p0}, LX/051;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/051;->c:LX/056;

    invoke-virtual {v0}, LX/056;->h()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 15224
    const-string v0, "Orca.PERSISTENT_KICK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15225
    iget-object v0, p0, LX/051;->c:LX/056;

    const-string v1, "caller"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/056;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 15226
    :cond_6
    iget-object v0, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_7

    .line 15227
    sget-object v0, LX/06f;->PERSISTENT_KICK:LX/06f;

    invoke-virtual {p0, v0}, LX/051;->a(LX/06f;)V

    goto :goto_0

    .line 15228
    :cond_7
    const-string v0, "onDeviceActive"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "onAppStopped"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 15229
    :cond_8
    sget-object v0, LX/06f;->PERSISTENT_KICK_SCREEN_CHANGE:LX/06f;

    invoke-direct {p0, v0}, LX/051;->b(LX/06f;)V

    goto/16 :goto_0

    .line 15230
    :cond_9
    sget-object v0, LX/06f;->PERSISTENT_KICK:LX/06f;

    invoke-direct {p0, v0}, LX/051;->b(LX/06f;)V

    goto/16 :goto_0

    .line 15231
    :cond_a
    const-string v0, "Orca.EXPIRE_CONNECTION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 15232
    iget-object v0, p0, LX/051;->c:LX/056;

    const-string v1, "EXPIRED_SESSION"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/056;->b(J)V

    goto/16 :goto_0

    .line 15233
    :cond_b
    const-string v0, "Orca.PING"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 15234
    iget-object v0, p0, LX/051;->c:LX/056;

    invoke-virtual {v0}, LX/056;->d()V

    goto/16 :goto_0

    .line 15235
    :cond_c
    invoke-virtual {p0, p1}, LX/051;->a(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 15236
    :try_start_0
    iget-object v0, p0, LX/051;->c:LX/056;

    .line 15237
    iget-wide v4, v0, LX/056;->I:J

    move-wide v0, v4

    .line 15238
    const-string v2, "[ MqttPushService ]"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 15239
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "persistence="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/051;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 15240
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    .line 15241
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "networkChangedTime="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 15242
    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v0, v0, LX/05Q;->a:LX/05T;

    invoke-virtual {v0}, LX/05T;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15243
    :goto_1
    return-void

    .line 15244
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 15245
    :cond_1
    iget-object v0, p0, LX/051;->c:LX/056;

    .line 15246
    const-string v1, "[ FbnsConnectionManager ]"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 15247
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "keepAliveIntervalSeconds="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, LX/056;->F:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 15248
    iget-object v1, v0, LX/056;->b:LX/072;

    .line 15249
    if-eqz v1, :cond_2

    .line 15250
    invoke-virtual {v1, p2}, LX/072;->a(Ljava/io/PrintWriter;)V

    .line 15251
    :goto_2
    const-string v0, "[ MqttHealthStats ]"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 15252
    iget-object v0, p0, LX/051;->c:LX/056;

    invoke-virtual {v0}, LX/056;->a()J

    move-result-wide v0

    .line 15253
    iget-object v2, p0, LX/051;->h:LX/05i;

    invoke-virtual {v2, v0, v1}, LX/05i;->a(J)LX/0Ha;

    move-result-object v0

    .line 15254
    const/4 v1, 0x2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 15255
    :try_start_1
    iget-boolean v2, v0, LX/0Ha;->i:Z

    invoke-static {v0, v2}, LX/0Ha;->a(LX/0Ha;Z)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    move-result-object v2

    .line 15256
    :goto_3
    move-object v0, v2

    .line 15257
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 15258
    :catch_0
    goto :goto_1

    .line 15259
    :cond_2
    :try_start_3
    const-string v1, "mMqttClient=null"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :catch_1
    const-string v2, ""

    goto :goto_3
.end method

.method public a(Ljava/lang/String;JZ)V
    .locals 0

    .prologue
    .line 15260
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 15261
    return-void
.end method

.method public a(Ljava/lang/String;[BJ)V
    .locals 0

    .prologue
    .line 15118
    return-void
.end method

.method public a(LX/053;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 15119
    iget-object v1, p0, LX/051;->o:LX/053;

    if-ne p1, v1, :cond_0

    .line 15120
    :goto_0
    return v0

    .line 15121
    :cond_0
    const-string v1, "MqttPushService"

    const-string v3, "connection/%s; lastState=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v0

    iget-object v0, p0, LX/051;->o:LX/053;

    aput-object v0, v4, v2

    invoke-static {v1, v3, v4}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15122
    iput-object p1, p0, LX/051;->o:LX/053;

    .line 15123
    iget-object v0, p0, LX/051;->p:LX/05Z;

    invoke-virtual {p1}, LX/053;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/05Z;->a(Ljava/lang/String;)V

    .line 15124
    sget-object v0, LX/07B;->a:[I

    invoke-virtual {p1}, LX/053;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 15125
    iget-object v0, p0, LX/051;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    neg-long v0, v0

    .line 15126
    :goto_1
    invoke-direct {p0}, LX/051;->q()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "mqtt/connect_state"

    invoke-interface {v3, v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    move v0, v2

    .line 15127
    goto :goto_0

    .line 15128
    :pswitch_0
    iget-wide v0, p0, LX/051;->l:J

    goto :goto_1

    .line 15129
    :pswitch_1
    const-wide/16 v0, 0x0

    .line 15130
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 15038
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()V
    .locals 11

    .prologue
    .line 15039
    iget-object v0, p0, LX/051;->b:LX/05Q;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/01n;->b(Z)V

    .line 15040
    invoke-virtual {p0}, LX/051;->e()LX/05Q;

    move-result-object v0

    iput-object v0, p0, LX/051;->b:LX/05Q;

    .line 15041
    invoke-virtual {p0}, LX/051;->f()V

    .line 15042
    invoke-virtual {p0}, LX/051;->g()V

    .line 15043
    iget-object v1, p0, LX/051;->g:LX/05h;

    const-string v2, "SERVICE_CREATE"

    const/4 v3, 0x0

    .line 15044
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v4, v0

    .line 15045
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v5, v0

    .line 15046
    iget-object v0, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    const/4 v7, -0x1

    iget-object v0, p0, LX/051;->d:LX/059;

    invoke-virtual {v0}, LX/059;->h()J

    move-result-wide v8

    iget-object v0, p0, LX/051;->d:LX/059;

    invoke-virtual {v0}, LX/059;->d()Landroid/net/NetworkInfo;

    move-result-object v10

    invoke-virtual/range {v1 .. v10}, LX/05h;->a(Ljava/lang/String;Ljava/lang/String;LX/05B;LX/05B;ZIJLandroid/net/NetworkInfo;)V

    .line 15047
    return-void

    .line 15048
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 11

    .prologue
    .line 15049
    const-string v0, "MqttPushService"

    const-string v1, "service/destroyed; started=%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15050
    iget-object v1, p0, LX/051;->g:LX/05h;

    const-string v2, "SERVICE_DESTROY"

    const/4 v3, 0x0

    .line 15051
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v4, v0

    .line 15052
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v5, v0

    .line 15053
    iget-object v0, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    const/4 v7, -0x1

    iget-object v0, p0, LX/051;->d:LX/059;

    invoke-virtual {v0}, LX/059;->h()J

    move-result-wide v8

    iget-object v0, p0, LX/051;->d:LX/059;

    invoke-virtual {v0}, LX/059;->d()Landroid/net/NetworkInfo;

    move-result-object v10

    invoke-virtual/range {v1 .. v10}, LX/05h;->a(Ljava/lang/String;Ljava/lang/String;LX/05B;LX/05B;ZIJLandroid/net/NetworkInfo;)V

    .line 15054
    invoke-virtual {p0}, LX/051;->j()V

    .line 15055
    return-void
.end method

.method public abstract e()LX/05Q;
.end method

.method public f()V
    .locals 11

    .prologue
    .line 15056
    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v1, v0, LX/05Q;->o:LX/056;

    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v2, v0, LX/05Q;->c:LX/059;

    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v3, v0, LX/05Q;->d:LX/06Q;

    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v4, v0, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v5, v0, LX/05Q;->f:LX/05h;

    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v6, v0, LX/05Q;->g:LX/05i;

    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v7, v0, LX/05Q;->h:LX/05b;

    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v8, v0, LX/05Q;->u:LX/04p;

    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v9, v0, LX/05Q;->w:LX/05Z;

    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v10, v0, LX/05Q;->B:LX/04v;

    move-object v0, p0

    .line 15057
    iput-object v1, v0, LX/051;->c:LX/056;

    .line 15058
    iput-object v2, v0, LX/051;->d:LX/059;

    .line 15059
    iput-object v3, v0, LX/051;->e:LX/06R;

    .line 15060
    iput-object v4, v0, LX/051;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 15061
    iput-object v5, v0, LX/051;->g:LX/05h;

    .line 15062
    iput-object v6, v0, LX/051;->h:LX/05i;

    .line 15063
    iput-object v7, v0, LX/051;->i:LX/05b;

    .line 15064
    iput-object v8, v0, LX/051;->j:LX/04p;

    .line 15065
    iput-object v9, v0, LX/051;->p:LX/05Z;

    .line 15066
    iput-object v10, v0, LX/051;->k:LX/04v;

    .line 15067
    iget-object p0, v0, LX/051;->j:LX/04p;

    invoke-virtual {p0}, LX/04p;->a()V

    .line 15068
    iget-object p0, v0, LX/051;->j:LX/04p;

    invoke-virtual {p0}, LX/04p;->b()LX/04q;

    move-result-object p0

    iput-object p0, v0, LX/051;->m:LX/04q;

    .line 15069
    return-void
.end method

.method public g()V
    .locals 6

    .prologue
    .line 15070
    iget-object v0, p0, LX/051;->h:LX/05i;

    sget-object v1, LX/06a;->ServiceCreatedTimestamp:LX/06a;

    iget-object v2, p0, LX/051;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    .line 15071
    invoke-static {v0, v1}, LX/05i;->a(LX/05i;LX/06a;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 15072
    invoke-direct {p0}, LX/051;->q()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "mqtt/connect_state"

    iget-object v2, p0, LX/051;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    neg-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "mqtt/service_created"

    iget-object v2, p0, LX/051;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 15073
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 15074
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 15075
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 15076
    iget-object v0, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15077
    sget-object v0, LX/0AX;->SERVICE_DESTROY:LX/0AX;

    invoke-virtual {p0, v0}, LX/051;->a(LX/0AX;)Ljava/util/concurrent/Future;

    .line 15078
    :cond_0
    iget-object v0, p0, LX/051;->c:LX/056;

    sget-object v1, LX/0AX;->SERVICE_DESTROY:LX/0AX;

    invoke-virtual {v0, v1}, LX/056;->a(LX/0AX;)Ljava/util/concurrent/Future;

    .line 15079
    iget-object v0, p0, LX/051;->b:LX/05Q;

    .line 15080
    iget-boolean v1, v0, LX/05Q;->E:Z

    if-eqz v1, :cond_2

    .line 15081
    :cond_1
    :goto_0
    return-void

    .line 15082
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/05Q;->E:Z

    .line 15083
    iget-object v1, v0, LX/05Q;->n:LX/06T;

    if-eqz v1, :cond_3

    .line 15084
    iget-object v1, v0, LX/05Q;->n:LX/06T;

    invoke-virtual {v1}, LX/06T;->a()V

    .line 15085
    :cond_3
    iget-object v1, v0, LX/05Q;->c:LX/059;

    if-eqz v1, :cond_4

    .line 15086
    iget-object v1, v0, LX/05Q;->c:LX/059;

    invoke-virtual {v1}, LX/059;->a()V

    .line 15087
    :cond_4
    iget-object v1, v0, LX/05Q;->v:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v1, :cond_5

    .line 15088
    iget-object v1, v0, LX/05Q;->v:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 15089
    :cond_5
    iget-object v1, v0, LX/05Q;->m:LX/06K;

    if-eqz v1, :cond_1

    .line 15090
    iget-object v1, v0, LX/05Q;->m:LX/06K;

    invoke-virtual {v1}, LX/06K;->a()V

    goto :goto_0
.end method

.method public k()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 15091
    iget-object v2, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_0

    .line 15092
    const-string v1, "MqttPushService"

    const-string v2, "connection/service_not_started"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15093
    :goto_0
    return v0

    .line 15094
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 15095
    iget-object v3, p0, LX/051;->e:LX/06R;

    invoke-interface {v3, v2}, LX/06R;->a(Ljava/util/Map;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 15096
    const-string v3, "MqttPushService"

    const-string v4, "connection/should_not_connect; reason=%s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v0

    invoke-static {v3, v4, v1}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 15097
    goto :goto_0
.end method

.method public o()V
    .locals 0

    .prologue
    .line 15098
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15099
    iget-object v0, p0, LX/051;->k:LX/04v;

    invoke-virtual {v0, p1}, LX/04v;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15100
    const-string v0, "MqttPushService"

    const-string v1, "service/onBind/bindingUnauthroized; intent=%s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15101
    :cond_0
    const-string v0, "MqttPushService"

    const-string v1, "service/onBind; intent=%s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15102
    iget-object v0, p0, LX/051;->q:Landroid/os/IBinder;

    return-object v0
.end method

.method public final onDestroy()V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v11, 0x2

    const/16 v0, 0x24

    const v1, -0x2b7fe99a

    invoke-static {v11, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 15103
    iget-object v1, p0, LX/051;->g:LX/05h;

    if-eqz v1, :cond_0

    .line 15104
    iget-object v1, p0, LX/051;->g:LX/05h;

    const-string v2, "SERVICE_ON_DESTROY"

    .line 15105
    sget-object v4, LX/06b;->a:LX/06b;

    move-object v4, v4

    .line 15106
    sget-object v5, LX/06b;->a:LX/06b;

    move-object v5, v5

    .line 15107
    iget-object v6, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    const/4 v7, -0x1

    const-wide/16 v8, 0x0

    move-object v10, v3

    invoke-virtual/range {v1 .. v10}, LX/05h;->a(Ljava/lang/String;Ljava/lang/String;LX/05B;LX/05B;ZIJLandroid/net/NetworkInfo;)V

    .line 15108
    :cond_0
    invoke-super {p0}, LX/052;->onDestroy()V

    .line 15109
    const/16 v1, 0x25

    const v2, -0x1761936f

    invoke-static {v11, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15110
    const-string v0, "N/A"

    return-object v0
.end method
