.class public final LX/0MK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0L9;
.implements LX/0L8;
.implements LX/0LU;
.implements LX/0LX;


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0ME;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private A:J

.field private B:J

.field private C:LX/0ON;

.field private D:LX/0MG;

.field private E:Ljava/io/IOException;

.field private F:I

.field private G:J

.field private H:Z

.field public I:I

.field private J:I

.field private final b:LX/0MH;

.field private final c:LX/0O1;

.field private final d:I

.field public final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0MI;",
            ">;"
        }
    .end annotation
.end field

.field private final f:I

.field private final g:Landroid/net/Uri;

.field private final h:LX/0G6;

.field public final i:Landroid/os/Handler;

.field public final j:LX/0MF;

.field public final k:I

.field private volatile l:Z

.field private volatile m:LX/0M8;

.field private volatile n:LX/0Lz;

.field private o:Z

.field private p:I

.field private q:[LX/0L4;

.field private r:J

.field private s:[Z

.field private t:[Z

.field public u:[Z

.field private v:I

.field private w:J

.field private x:J

.field private y:J

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45612
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/0MK;->a:Ljava/util/List;

    .line 45613
    :try_start_0
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    const-string v1, "com.google.android.exoplayer.extractor.webm.WebmExtractor"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/0ME;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_a

    .line 45614
    :goto_0
    :try_start_1
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    const-string v1, "com.google.android.exoplayer.extractor.mp4.FragmentedMp4Extractor"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/0ME;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_9

    .line 45615
    :goto_1
    :try_start_2
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    const-string v1, "com.google.android.exoplayer.extractor.mp4.Mp4Extractor"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/0ME;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_8

    .line 45616
    :goto_2
    :try_start_3
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    const-string v1, "com.google.android.exoplayer.extractor.mp3.Mp3Extractor"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/0ME;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_7

    .line 45617
    :goto_3
    :try_start_4
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    const-string v1, "com.google.android.exoplayer.extractor.ts.AdtsExtractor"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/0ME;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_6

    .line 45618
    :goto_4
    :try_start_5
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    const-string v1, "com.google.android.exoplayer.extractor.ts.TsExtractor"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/0ME;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_5 .. :try_end_5} :catch_5

    .line 45619
    :goto_5
    :try_start_6
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    const-string v1, "com.google.android.exoplayer.extractor.flv.FlvExtractor"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/0ME;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_6} :catch_4

    .line 45620
    :goto_6
    :try_start_7
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    const-string v1, "com.google.android.exoplayer.extractor.ogg.OggVorbisExtractor"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/0ME;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_7 .. :try_end_7} :catch_3

    .line 45621
    :goto_7
    :try_start_8
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    const-string v1, "com.google.android.exoplayer.extractor.ts.PsExtractor"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/0ME;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catch Ljava/lang/ClassNotFoundException; {:try_start_8 .. :try_end_8} :catch_2

    .line 45622
    :goto_8
    :try_start_9
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    const-string v1, "com.google.android.exoplayer.extractor.wav.WavExtractor"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/0ME;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Ljava/lang/ClassNotFoundException; {:try_start_9 .. :try_end_9} :catch_1

    .line 45623
    :goto_9
    :try_start_a
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    const-string v1, "com.google.android.exoplayer.ext.flac.FlacExtractor"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/0ME;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Ljava/lang/ClassNotFoundException; {:try_start_a .. :try_end_a} :catch_0

    .line 45624
    :goto_a
    return-void

    :catch_0
    goto :goto_a

    :catch_1
    goto :goto_9

    :catch_2
    goto :goto_8

    :catch_3
    goto :goto_7

    :catch_4
    goto :goto_6

    :catch_5
    goto :goto_5

    :catch_6
    goto :goto_4

    :catch_7
    goto :goto_3

    :catch_8
    goto/16 :goto_2

    :catch_9
    goto/16 :goto_1

    :catch_a
    goto/16 :goto_0
.end method

.method private varargs constructor <init>(Landroid/net/Uri;LX/0G6;LX/0O1;IILandroid/os/Handler;LX/0MF;I[LX/0ME;)V
    .locals 3

    .prologue
    .line 45625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45626
    iput-object p1, p0, LX/0MK;->g:Landroid/net/Uri;

    .line 45627
    iput-object p2, p0, LX/0MK;->h:LX/0G6;

    .line 45628
    iput-object p7, p0, LX/0MK;->j:LX/0MF;

    .line 45629
    iput-object p6, p0, LX/0MK;->i:Landroid/os/Handler;

    .line 45630
    iput p8, p0, LX/0MK;->k:I

    .line 45631
    iput-object p3, p0, LX/0MK;->c:LX/0O1;

    .line 45632
    iput p4, p0, LX/0MK;->d:I

    .line 45633
    iput p5, p0, LX/0MK;->f:I

    .line 45634
    if-eqz p9, :cond_0

    array-length v0, p9

    if-nez v0, :cond_1

    .line 45635
    :cond_0
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array p9, v0, [LX/0ME;

    .line 45636
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p9

    if-ge v1, v0, :cond_1

    .line 45637
    :try_start_0
    sget-object v0, LX/0MK;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ME;

    aput-object v0, p9, v1
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 45638
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 45639
    :catch_0
    move-exception v0

    .line 45640
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unexpected error creating default extractor"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 45641
    :catch_1
    move-exception v0

    .line 45642
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unexpected error creating default extractor"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 45643
    :cond_1
    new-instance v0, LX/0MH;

    invoke-direct {v0, p9, p0}, LX/0MH;-><init>([LX/0ME;LX/0LU;)V

    iput-object v0, p0, LX/0MK;->b:LX/0MH;

    .line 45644
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    .line 45645
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/0MK;->y:J

    .line 45646
    return-void
.end method

.method private varargs constructor <init>(Landroid/net/Uri;LX/0G6;LX/0O1;II[LX/0ME;)V
    .locals 10

    .prologue
    .line 45647
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v9}, LX/0MK;-><init>(Landroid/net/Uri;LX/0G6;LX/0O1;IILandroid/os/Handler;LX/0MF;I[LX/0ME;)V

    .line 45648
    return-void
.end method

.method public varargs constructor <init>(Landroid/net/Uri;LX/0G6;LX/0O1;I[LX/0ME;)V
    .locals 7

    .prologue
    .line 45649
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/0MK;-><init>(Landroid/net/Uri;LX/0G6;LX/0O1;II[LX/0ME;)V

    .line 45650
    return-void
.end method

.method private c(J)V
    .locals 1

    .prologue
    .line 45651
    iput-wide p1, p0, LX/0MK;->y:J

    .line 45652
    iget-object v0, p0, LX/0MK;->C:LX/0ON;

    .line 45653
    iget-boolean p1, v0, LX/0ON;->c:Z

    move v0, p1

    .line 45654
    if-nez v0, :cond_0

    invoke-direct {p0}, LX/0MK;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 45655
    :goto_0
    return-void

    .line 45656
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0MK;->H:Z

    .line 45657
    iget-object v0, p0, LX/0MK;->C:LX/0ON;

    .line 45658
    iget-boolean p1, v0, LX/0ON;->c:Z

    move v0, p1

    .line 45659
    if-eqz v0, :cond_1

    .line 45660
    iget-object v0, p0, LX/0MK;->C:LX/0ON;

    invoke-virtual {v0}, LX/0ON;->b()V

    goto :goto_0

    .line 45661
    :cond_1
    invoke-direct {p0}, LX/0MK;->j()V

    .line 45662
    invoke-direct {p0}, LX/0MK;->g()V

    goto :goto_0
.end method

.method private d(J)LX/0MG;
    .locals 9

    .prologue
    .line 45663
    new-instance v0, LX/0MG;

    iget-object v1, p0, LX/0MK;->g:Landroid/net/Uri;

    iget-object v2, p0, LX/0MK;->h:LX/0G6;

    iget-object v3, p0, LX/0MK;->b:LX/0MH;

    iget-object v4, p0, LX/0MK;->c:LX/0O1;

    iget v5, p0, LX/0MK;->d:I

    iget-object v6, p0, LX/0MK;->m:LX/0M8;

    invoke-interface {v6, p1, p2}, LX/0M8;->b(J)J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, LX/0MG;-><init>(Landroid/net/Uri;LX/0G6;LX/0MH;LX/0O1;IJ)V

    return-object v0
.end method

.method private g()V
    .locals 14

    .prologue
    const-wide/16 v8, -0x1

    const-wide/high16 v6, -0x8000000000000000L

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45664
    iget-boolean v0, p0, LX/0MK;->H:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0MK;->C:LX/0ON;

    .line 45665
    iget-boolean v3, v0, LX/0ON;->c:Z

    move v0, v3

    .line 45666
    if-eqz v0, :cond_1

    .line 45667
    :cond_0
    :goto_0
    return-void

    .line 45668
    :cond_1
    iget-object v0, p0, LX/0MK;->E:Ljava/io/IOException;

    if-eqz v0, :cond_7

    .line 45669
    invoke-direct {p0}, LX/0MK;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 45670
    iget-object v0, p0, LX/0MK;->D:LX/0MG;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45671
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, LX/0MK;->G:J

    sub-long/2addr v4, v6

    .line 45672
    iget v0, p0, LX/0MK;->F:I

    int-to-long v6, v0

    .line 45673
    const-wide/16 v10, 0x1

    sub-long v10, v6, v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    const-wide/16 v12, 0x1388

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    move-wide v6, v10

    .line 45674
    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    .line 45675
    const/4 v0, 0x0

    iput-object v0, p0, LX/0MK;->E:Ljava/io/IOException;

    .line 45676
    iget-boolean v0, p0, LX/0MK;->o:Z

    if-nez v0, :cond_5

    .line 45677
    :goto_2
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 45678
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MI;

    invoke-virtual {v0}, LX/0MC;->a()V

    .line 45679
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    .line 45680
    goto :goto_1

    .line 45681
    :cond_3
    invoke-direct {p0}, LX/0MK;->h()LX/0MG;

    move-result-object v0

    iput-object v0, p0, LX/0MK;->D:LX/0MG;

    .line 45682
    :cond_4
    :goto_3
    iget v0, p0, LX/0MK;->I:I

    iput v0, p0, LX/0MK;->J:I

    .line 45683
    iget-object v0, p0, LX/0MK;->C:LX/0ON;

    iget-object v1, p0, LX/0MK;->D:LX/0MG;

    invoke-virtual {v0, v1, p0}, LX/0ON;->a(LX/0LO;LX/0LX;)V

    goto :goto_0

    .line 45684
    :cond_5
    iget-object v0, p0, LX/0MK;->m:LX/0M8;

    invoke-interface {v0}, LX/0M8;->a()Z

    move-result v0

    if-nez v0, :cond_4

    iget-wide v4, p0, LX/0MK;->r:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_4

    .line 45685
    :goto_4
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 45686
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MI;

    invoke-virtual {v0}, LX/0MC;->a()V

    .line 45687
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 45688
    :cond_6
    invoke-direct {p0}, LX/0MK;->h()LX/0MG;

    move-result-object v0

    iput-object v0, p0, LX/0MK;->D:LX/0MG;

    .line 45689
    iget-wide v2, p0, LX/0MK;->w:J

    iput-wide v2, p0, LX/0MK;->A:J

    .line 45690
    iput-boolean v1, p0, LX/0MK;->z:Z

    goto :goto_3

    .line 45691
    :cond_7
    const-wide/16 v4, 0x0

    iput-wide v4, p0, LX/0MK;->B:J

    .line 45692
    iput-boolean v2, p0, LX/0MK;->z:Z

    .line 45693
    iget-boolean v0, p0, LX/0MK;->o:Z

    if-nez v0, :cond_8

    .line 45694
    invoke-direct {p0}, LX/0MK;->h()LX/0MG;

    move-result-object v0

    iput-object v0, p0, LX/0MK;->D:LX/0MG;

    .line 45695
    :goto_5
    iget v0, p0, LX/0MK;->I:I

    iput v0, p0, LX/0MK;->J:I

    .line 45696
    iget-object v0, p0, LX/0MK;->C:LX/0ON;

    iget-object v1, p0, LX/0MK;->D:LX/0MG;

    invoke-virtual {v0, v1, p0}, LX/0ON;->a(LX/0LO;LX/0LX;)V

    goto/16 :goto_0

    .line 45697
    :cond_8
    invoke-direct {p0}, LX/0MK;->k()Z

    move-result v0

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45698
    iget-wide v2, p0, LX/0MK;->r:J

    cmp-long v0, v2, v8

    if-eqz v0, :cond_9

    iget-wide v2, p0, LX/0MK;->y:J

    iget-wide v4, p0, LX/0MK;->r:J

    cmp-long v0, v2, v4

    if-ltz v0, :cond_9

    .line 45699
    iput-boolean v1, p0, LX/0MK;->H:Z

    .line 45700
    iput-wide v6, p0, LX/0MK;->y:J

    goto/16 :goto_0

    .line 45701
    :cond_9
    iget-wide v0, p0, LX/0MK;->y:J

    invoke-direct {p0, v0, v1}, LX/0MK;->d(J)LX/0MG;

    move-result-object v0

    iput-object v0, p0, LX/0MK;->D:LX/0MG;

    .line 45702
    iput-wide v6, p0, LX/0MK;->y:J

    goto :goto_5
.end method

.method private h()LX/0MG;
    .locals 8

    .prologue
    .line 45703
    new-instance v0, LX/0MG;

    iget-object v1, p0, LX/0MK;->g:Landroid/net/Uri;

    iget-object v2, p0, LX/0MK;->h:LX/0G6;

    iget-object v3, p0, LX/0MK;->b:LX/0MH;

    iget-object v4, p0, LX/0MK;->c:LX/0O1;

    iget v5, p0, LX/0MK;->d:I

    const-wide/16 v6, 0x0

    invoke-direct/range {v0 .. v7}, LX/0MG;-><init>(Landroid/net/Uri;LX/0G6;LX/0MH;LX/0O1;IJ)V

    return-object v0
.end method

.method private j()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 45704
    move v1, v2

    :goto_0
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 45705
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MI;

    invoke-virtual {v0}, LX/0MC;->a()V

    .line 45706
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 45707
    :cond_0
    iput-object v3, p0, LX/0MK;->D:LX/0MG;

    .line 45708
    iput-object v3, p0, LX/0MK;->E:Ljava/io/IOException;

    .line 45709
    iput v2, p0, LX/0MK;->F:I

    .line 45710
    return-void
.end method

.method private k()Z
    .locals 4

    .prologue
    .line 45711
    iget-wide v0, p0, LX/0MK;->y:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()Z
    .locals 1

    .prologue
    .line 45712
    iget-object v0, p0, LX/0MK;->E:Ljava/io/IOException;

    instance-of v0, v0, LX/0MJ;

    return v0
.end method


# virtual methods
.method public final Z_()LX/0L8;
    .locals 1

    .prologue
    .line 45713
    iget v0, p0, LX/0MK;->v:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0MK;->v:I

    .line 45714
    return-object p0
.end method

.method public final a(IJLX/0L5;LX/0L7;)I
    .locals 6

    .prologue
    const/4 v2, -0x2

    const/4 v1, 0x0

    .line 45715
    iput-wide p2, p0, LX/0MK;->w:J

    .line 45716
    iget-object v0, p0, LX/0MK;->t:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/0MK;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 45717
    :goto_0
    return v0

    .line 45718
    :cond_1
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MI;

    .line 45719
    iget-object v3, p0, LX/0MK;->s:[Z

    aget-boolean v3, v3, p1

    if-eqz v3, :cond_2

    .line 45720
    iget-object v2, v0, LX/0MC;->g:LX/0L4;

    move-object v0, v2

    .line 45721
    iput-object v0, p4, LX/0L5;->a:LX/0L4;

    .line 45722
    iget-object v0, p0, LX/0MK;->n:LX/0Lz;

    iput-object v0, p4, LX/0L5;->b:LX/0Lz;

    .line 45723
    iget-object v0, p0, LX/0MK;->s:[Z

    aput-boolean v1, v0, p1

    .line 45724
    const/4 v0, -0x4

    goto :goto_0

    .line 45725
    :cond_2
    invoke-virtual {v0, p5}, LX/0MC;->a(LX/0L7;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 45726
    iget-wide v2, p5, LX/0L7;->e:J

    iget-wide v4, p0, LX/0MK;->x:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_4

    const/4 v0, 0x1

    .line 45727
    :goto_1
    iget v2, p5, LX/0L7;->d:I

    if-eqz v0, :cond_5

    const/high16 v0, 0x8000000

    :goto_2
    or-int/2addr v0, v2

    iput v0, p5, LX/0L7;->d:I

    .line 45728
    iget-boolean v0, p0, LX/0MK;->z:Z

    if-eqz v0, :cond_3

    .line 45729
    iget-wide v2, p0, LX/0MK;->A:J

    iget-wide v4, p5, LX/0L7;->e:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/0MK;->B:J

    .line 45730
    iput-boolean v1, p0, LX/0MK;->z:Z

    .line 45731
    :cond_3
    iget-wide v0, p5, LX/0L7;->e:J

    iget-wide v2, p0, LX/0MK;->B:J

    add-long/2addr v0, v2

    iput-wide v0, p5, LX/0L7;->e:J

    .line 45732
    const/4 v0, -0x3

    goto :goto_0

    :cond_4
    move v0, v1

    .line 45733
    goto :goto_1

    :cond_5
    move v0, v1

    .line 45734
    goto :goto_2

    .line 45735
    :cond_6
    iget-boolean v0, p0, LX/0MK;->H:Z

    if-eqz v0, :cond_7

    .line 45736
    const/4 v0, -0x1

    goto :goto_0

    :cond_7
    move v0, v2

    .line 45737
    goto :goto_0
.end method

.method public final a(I)LX/0L4;
    .locals 1

    .prologue
    .line 45738
    iget-boolean v0, p0, LX/0MK;->o:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45739
    iget-object v0, p0, LX/0MK;->q:[LX/0L4;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 45597
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0MK;->l:Z

    .line 45598
    return-void
.end method

.method public final a(IJ)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 45599
    iget-boolean v0, p0, LX/0MK;->o:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45600
    iget-object v0, p0, LX/0MK;->u:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45601
    iget v0, p0, LX/0MK;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0MK;->p:I

    .line 45602
    iget-object v0, p0, LX/0MK;->u:[Z

    aput-boolean v1, v0, p1

    .line 45603
    iget-object v0, p0, LX/0MK;->s:[Z

    aput-boolean v1, v0, p1

    .line 45604
    iget-object v0, p0, LX/0MK;->t:[Z

    aput-boolean v2, v0, p1

    .line 45605
    iget v0, p0, LX/0MK;->p:I

    if-ne v0, v1, :cond_1

    .line 45606
    iget-object v0, p0, LX/0MK;->m:LX/0M8;

    invoke-interface {v0}, LX/0M8;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 p2, 0x0

    .line 45607
    :cond_0
    iput-wide p2, p0, LX/0MK;->w:J

    .line 45608
    iput-wide p2, p0, LX/0MK;->x:J

    .line 45609
    invoke-direct {p0, p2, p3}, LX/0MK;->c(J)V

    .line 45610
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 45611
    goto :goto_0
.end method

.method public final a(LX/0LO;)V
    .locals 1

    .prologue
    .line 45464
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0MK;->H:Z

    .line 45465
    return-void
.end method

.method public final a(LX/0LO;Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 45466
    iput-object p2, p0, LX/0MK;->E:Ljava/io/IOException;

    .line 45467
    iget v0, p0, LX/0MK;->I:I

    iget v1, p0, LX/0MK;->J:I

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, LX/0MK;->F:I

    .line 45468
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0MK;->G:J

    .line 45469
    iget-object v0, p0, LX/0MK;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0MK;->j:LX/0MF;

    if-eqz v0, :cond_0

    .line 45470
    iget-object v0, p0, LX/0MK;->i:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/extractor/ExtractorSampleSource$1;

    invoke-direct {v1, p0, p2}, Lcom/google/android/exoplayer/extractor/ExtractorSampleSource$1;-><init>(LX/0MK;Ljava/io/IOException;)V

    const p1, -0x25074dc1    # -3.50008889E16f

    invoke-static {v0, v1, p1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 45471
    :cond_0
    invoke-direct {p0}, LX/0MK;->g()V

    .line 45472
    return-void

    .line 45473
    :cond_1
    iget v0, p0, LX/0MK;->F:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(LX/0Lz;)V
    .locals 0

    .prologue
    .line 45474
    iput-object p1, p0, LX/0MK;->n:LX/0Lz;

    .line 45475
    return-void
.end method

.method public final a(LX/0M8;)V
    .locals 0

    .prologue
    .line 45476
    iput-object p1, p0, LX/0MK;->m:LX/0M8;

    .line 45477
    return-void
.end method

.method public final a(J)Z
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 45478
    iget-boolean v2, p0, LX/0MK;->o:Z

    if-eqz v2, :cond_1

    move v0, v1

    .line 45479
    :cond_0
    :goto_0
    return v0

    .line 45480
    :cond_1
    iget-object v2, p0, LX/0MK;->C:LX/0ON;

    if-nez v2, :cond_2

    .line 45481
    new-instance v2, LX/0ON;

    const-string v3, "Loader:ExtractorSampleSource"

    invoke-direct {v2, v3}, LX/0ON;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, LX/0MK;->C:LX/0ON;

    .line 45482
    :cond_2
    invoke-direct {p0}, LX/0MK;->g()V

    .line 45483
    iget-object v2, p0, LX/0MK;->m:LX/0M8;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, LX/0MK;->l:Z

    if-eqz v2, :cond_0

    const/4 v4, 0x0

    .line 45484
    move v3, v4

    :goto_1
    iget-object v2, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v3, v2, :cond_6

    .line 45485
    iget-object v2, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0MI;

    invoke-virtual {v2}, LX/0MC;->d()Z

    move-result v2

    if-nez v2, :cond_5

    .line 45486
    :goto_2
    move v2, v4

    .line 45487
    if-eqz v2, :cond_0

    .line 45488
    iget-object v2, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 45489
    new-array v2, v3, [Z

    iput-object v2, p0, LX/0MK;->u:[Z

    .line 45490
    new-array v2, v3, [Z

    iput-object v2, p0, LX/0MK;->t:[Z

    .line 45491
    new-array v2, v3, [Z

    iput-object v2, p0, LX/0MK;->s:[Z

    .line 45492
    new-array v2, v3, [LX/0L4;

    iput-object v2, p0, LX/0MK;->q:[LX/0L4;

    .line 45493
    iput-wide v8, p0, LX/0MK;->r:J

    move v2, v0

    .line 45494
    :goto_3
    if-ge v2, v3, :cond_4

    .line 45495
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MI;

    .line 45496
    iget-object v4, v0, LX/0MC;->g:LX/0L4;

    move-object v0, v4

    .line 45497
    iget-object v4, p0, LX/0MK;->q:[LX/0L4;

    aput-object v0, v4, v2

    .line 45498
    iget-wide v4, v0, LX/0L4;->e:J

    cmp-long v4, v4, v8

    if-eqz v4, :cond_3

    iget-wide v4, v0, LX/0L4;->e:J

    iget-wide v6, p0, LX/0MK;->r:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 45499
    iget-wide v4, v0, LX/0L4;->e:J

    iput-wide v4, p0, LX/0MK;->r:J

    .line 45500
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 45501
    :cond_4
    iput-boolean v1, p0, LX/0MK;->o:Z

    move v0, v1

    .line 45502
    goto :goto_0

    .line 45503
    :cond_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 45504
    :cond_6
    const/4 v4, 0x1

    goto :goto_2
.end method

.method public final a_(I)LX/0LS;
    .locals 2

    .prologue
    .line 45505
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MI;

    .line 45506
    if-nez v0, :cond_0

    .line 45507
    new-instance v0, LX/0MI;

    iget-object v1, p0, LX/0MK;->c:LX/0O1;

    invoke-direct {v0, p0, v1}, LX/0MI;-><init>(LX/0MK;LX/0O1;)V

    .line 45508
    iget-object v1, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 45509
    :cond_0
    return-object v0
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 45510
    iget-object v0, p0, LX/0MK;->t:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    .line 45511
    iget-object v0, p0, LX/0MK;->t:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    .line 45512
    iget-wide v0, p0, LX/0MK;->x:J

    .line 45513
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 45514
    iget-object v0, p0, LX/0MK;->E:Ljava/io/IOException;

    if-nez v0, :cond_1

    .line 45515
    :cond_0
    return-void

    .line 45516
    :cond_1
    invoke-direct {p0}, LX/0MK;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 45517
    iget-object v0, p0, LX/0MK;->E:Ljava/io/IOException;

    throw v0

    .line 45518
    :cond_2
    iget v0, p0, LX/0MK;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 45519
    iget v0, p0, LX/0MK;->f:I

    .line 45520
    :goto_0
    iget v1, p0, LX/0MK;->F:I

    if-le v1, v0, :cond_0

    .line 45521
    iget-object v0, p0, LX/0MK;->E:Ljava/io/IOException;

    throw v0

    .line 45522
    :cond_3
    iget-object v0, p0, LX/0MK;->m:LX/0M8;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0MK;->m:LX/0M8;

    invoke-interface {v0}, LX/0M8;->a()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x6

    goto :goto_0

    :cond_4
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final b(J)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45523
    iget-boolean v0, p0, LX/0MK;->o:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45524
    iget v0, p0, LX/0MK;->p:I

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45525
    iget-object v0, p0, LX/0MK;->m:LX/0M8;

    invoke-interface {v0}, LX/0M8;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 p1, 0x0

    .line 45526
    :cond_0
    invoke-direct {p0}, LX/0MK;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-wide v4, p0, LX/0MK;->y:J

    .line 45527
    :goto_1
    iput-wide p1, p0, LX/0MK;->w:J

    .line 45528
    iput-wide p1, p0, LX/0MK;->x:J

    .line 45529
    cmp-long v0, v4, p1

    if-nez v0, :cond_4

    .line 45530
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 45531
    goto :goto_0

    .line 45532
    :cond_3
    iget-wide v4, p0, LX/0MK;->w:J

    goto :goto_1

    .line 45533
    :cond_4
    invoke-direct {p0}, LX/0MK;->k()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    move v3, v2

    move v4, v0

    .line 45534
    :goto_3
    if-eqz v4, :cond_6

    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 45535
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MI;

    invoke-virtual {v0, p1, p2}, LX/0MC;->b(J)Z

    move-result v0

    and-int/2addr v4, v0

    .line 45536
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_5
    move v0, v2

    .line 45537
    goto :goto_2

    .line 45538
    :cond_6
    if-nez v4, :cond_7

    .line 45539
    invoke-direct {p0, p1, p2}, LX/0MK;->c(J)V

    .line 45540
    :cond_7
    :goto_4
    iget-object v0, p0, LX/0MK;->t:[Z

    array-length v0, v0

    if-ge v2, v0, :cond_1

    .line 45541
    iget-object v0, p0, LX/0MK;->t:[Z

    aput-boolean v1, v0, v2

    .line 45542
    add-int/lit8 v2, v2, 0x1

    goto :goto_4
.end method

.method public final b(IJ)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45543
    iget-boolean v0, p0, LX/0MK;->o:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45544
    iget-object v0, p0, LX/0MK;->u:[Z

    aget-boolean v0, v0, p1

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45545
    iput-wide p2, p0, LX/0MK;->w:J

    .line 45546
    iget-wide v4, p0, LX/0MK;->w:J

    .line 45547
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    iget-object v0, p0, LX/0MK;->u:[Z

    array-length v0, v0

    if-ge v3, v0, :cond_1

    .line 45548
    iget-object v0, p0, LX/0MK;->u:[Z

    aget-boolean v0, v0, v3

    if-nez v0, :cond_0

    .line 45549
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MI;

    invoke-virtual {v0, v4, v5}, LX/0MC;->a(J)V

    .line 45550
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 45551
    :cond_1
    iget-boolean v0, p0, LX/0MK;->H:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 45552
    :goto_1
    return v0

    .line 45553
    :cond_2
    invoke-direct {p0}, LX/0MK;->g()V

    .line 45554
    invoke-direct {p0}, LX/0MK;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    .line 45555
    goto :goto_1

    .line 45556
    :cond_3
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MI;

    invoke-virtual {v0}, LX/0MC;->g()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 45557
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 45558
    iget-boolean v0, p0, LX/0MK;->o:Z

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45559
    iget-object v0, p0, LX/0MK;->u:[Z

    aget-boolean v0, v0, p1

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45560
    iget v0, p0, LX/0MK;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0MK;->p:I

    .line 45561
    iget-object v0, p0, LX/0MK;->u:[Z

    aput-boolean v2, v0, p1

    .line 45562
    iget v0, p0, LX/0MK;->p:I

    if-nez v0, :cond_0

    .line 45563
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/0MK;->w:J

    .line 45564
    iget-object v0, p0, LX/0MK;->C:LX/0ON;

    .line 45565
    iget-boolean v1, v0, LX/0ON;->c:Z

    move v0, v1

    .line 45566
    if-eqz v0, :cond_1

    .line 45567
    iget-object v0, p0, LX/0MK;->C:LX/0ON;

    invoke-virtual {v0}, LX/0ON;->b()V

    .line 45568
    :cond_0
    :goto_0
    return-void

    .line 45569
    :cond_1
    invoke-direct {p0}, LX/0MK;->j()V

    .line 45570
    iget-object v0, p0, LX/0MK;->c:LX/0O1;

    invoke-interface {v0, v2}, LX/0O1;->a(I)V

    goto :goto_0
.end method

.method public final d()J
    .locals 10

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    .line 45571
    iget-boolean v0, p0, LX/0MK;->H:Z

    if-eqz v0, :cond_1

    .line 45572
    const-wide/16 v2, -0x3

    .line 45573
    :cond_0
    :goto_0
    return-wide v2

    .line 45574
    :cond_1
    invoke-direct {p0}, LX/0MK;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 45575
    iget-wide v2, p0, LX/0MK;->y:J

    goto :goto_0

    .line 45576
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    move-wide v2, v4

    :goto_1
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 45577
    iget-object v0, p0, LX/0MK;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0MI;

    .line 45578
    iget-wide v8, v0, LX/0MC;->f:J

    move-wide v6, v8

    .line 45579
    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 45580
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 45581
    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    iget-wide v2, p0, LX/0MK;->w:J

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45582
    iget v0, p0, LX/0MK;->v:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45583
    iget v0, p0, LX/0MK;->v:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0MK;->v:I

    if-nez v0, :cond_1

    .line 45584
    iget-object v0, p0, LX/0MK;->C:LX/0ON;

    if-eqz v0, :cond_0

    .line 45585
    iget-object v0, p0, LX/0MK;->C:LX/0ON;

    invoke-virtual {v0}, LX/0ON;->c()V

    .line 45586
    iput-object v1, p0, LX/0MK;->C:LX/0ON;

    .line 45587
    :cond_0
    iget-object v0, p0, LX/0MK;->b:LX/0MH;

    iget-object v0, v0, LX/0MH;->c:LX/0ME;

    if-eqz v0, :cond_1

    .line 45588
    iget-object v0, p0, LX/0MK;->b:LX/0MH;

    .line 45589
    iput-object v1, v0, LX/0MH;->c:LX/0ME;

    .line 45590
    :cond_1
    return-void

    .line 45591
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 45592
    iget v0, p0, LX/0MK;->p:I

    if-lez v0, :cond_0

    .line 45593
    iget-wide v0, p0, LX/0MK;->y:J

    invoke-direct {p0, v0, v1}, LX/0MK;->c(J)V

    .line 45594
    :goto_0
    return-void

    .line 45595
    :cond_0
    invoke-direct {p0}, LX/0MK;->j()V

    .line 45596
    iget-object v0, p0, LX/0MK;->c:LX/0O1;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0O1;->a(I)V

    goto :goto_0
.end method
