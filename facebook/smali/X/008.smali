.class public final LX/008;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final LOAD_ALL_ASYNC_OPTIMIZATION:I = 0x4

.field public static final LOAD_ALL_BETA_BUILD:I = 0x1

.field public static final LOAD_ALL_OPEN_ONLY:I = 0x2

.field public static final LOAD_SECONDARY:I = 0x8

.field public static deoptTaint:Z

.field private static mDeri:LX/02X;

.field private static sMainDexStore:LX/02U;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 995
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 996
    return-void
.end method

.method public static declared-synchronized getLastCompilationTime(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 990
    const-class v2, LX/008;

    monitor-enter v2

    const-wide/16 v0, 0x0

    .line 991
    :try_start_0
    sget-object v3, LX/008;->sMainDexStore:LX/02U;

    if-eqz v3, :cond_0

    .line 992
    sget-object v0, LX/008;->sMainDexStore:LX/02U;

    invoke-virtual {v0}, LX/02U;->getLastRegenTime()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 993
    :cond_0
    monitor-exit v2

    return-wide v0

    .line 994
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static declared-synchronized getMainDexStore()LX/02U;
    .locals 3

    .prologue
    .line 977
    const-class v1, LX/008;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/008;->sMainDexStore:LX/02U;

    if-nez v0, :cond_0

    .line 978
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "main dex store not loaded"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 979
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 980
    :cond_0
    :try_start_1
    sget-object v0, LX/008;->sMainDexStore:LX/02U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static getMainDexStoreLoadInformation()LX/02X;
    .locals 2

    .prologue
    .line 974
    sget-object v0, LX/008;->mDeri:LX/02X;

    if-nez v0, :cond_0

    .line 975
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "main dex store not yet loaded"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 976
    :cond_0
    sget-object v0, LX/008;->mDeri:LX/02X;

    return-object v0
.end method

.method private static inCtScanOptMode()Z
    .locals 1

    .prologue
    .line 971
    sget-boolean v0, LX/01A;->a:Z

    if-eqz v0, :cond_0

    .line 972
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 973
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static loadAll(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 969
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/008;->loadAll(Landroid/content/Context;Z)V

    .line 970
    return-void
.end method

.method public static declared-synchronized loadAll(Landroid/content/Context;ILX/006;)V
    .locals 3
    .param p2    # LX/006;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 981
    const-class v1, LX/008;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/008;->mDeri:LX/02X;

    if-eqz v0, :cond_0

    .line 982
    new-instance v0, Ljava/lang/AssertionError;

    const-string v2, "loadAll already loaded dex files"

    invoke-direct {v0, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 983
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 984
    :cond_0
    if-nez p2, :cond_1

    .line 985
    :try_start_1
    new-instance p2, LX/0C3;

    invoke-direct {p2}, LX/0C3;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 986
    :cond_1
    :try_start_2
    invoke-static {p0, p1, p2}, LX/008;->loadAllImpl(Landroid/content/Context;ILX/006;)LX/02X;

    move-result-object v0

    sput-object v0, LX/008;->mDeri:LX/02X;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 987
    monitor-exit v1

    return-void

    .line 988
    :catch_0
    move-exception v0

    .line 989
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static loadAll(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 967
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/008;->loadAll(Landroid/content/Context;ZLX/006;)V

    .line 968
    return-void
.end method

.method public static loadAll(Landroid/content/Context;ZLX/006;)V
    .locals 1
    .param p2    # LX/006;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 965
    const/4 v0, 0x1

    invoke-static {p0, v0, p2}, LX/008;->loadAll(Landroid/content/Context;ILX/006;)V

    .line 966
    return-void
.end method

.method private static loadAllImpl(Landroid/content/Context;ILX/006;)LX/02X;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 915
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_3

    move v0, v1

    .line 916
    :goto_0
    const-string v4, "DLL.loadAll betaBuild:%s flags:0x%08x"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v4, v5}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 917
    const-string v4, "DLL2_load_main"

    const v5, 0x880001

    invoke-virtual {p2, v4, v5}, LX/006;->a(Ljava/lang/String;I)LX/00X;

    move-result-object v4

    .line 918
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 919
    invoke-static {v5}, Lcom/facebook/common/dextricks/DalvikInternals;->realpath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 920
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/app_secondary_program_dex"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, LX/02Q;->deleteRecursiveNoThrow(Ljava/io/File;)V

    .line 921
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/app_secondary_program_dex_opt"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, LX/02Q;->deleteRecursiveNoThrow(Ljava/io/File;)V

    .line 922
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 923
    const-string v7, "resolved non-canonical data directory %s to %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v5, 0x1

    aput-object v6, v8, v5

    invoke-static {v7, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 924
    :cond_0
    new-instance v5, Ljava/io/File;

    const-string v7, "dex"

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    new-instance v6, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 926
    invoke-static {p2, p0, v0}, LX/008;->obtainResProvider(LX/006;Landroid/content/Context;Z)LX/02S;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v7

    .line 927
    if-nez v7, :cond_4

    .line 928
    :try_start_1
    const-string v0, "Nothing to do in DexLibLoader.loadAll: no resProvider"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 929
    new-instance v0, LX/02X;

    invoke-direct {v0}, LX/02X;-><init>()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 930
    if-eqz v7, :cond_1

    :try_start_2
    invoke-virtual {v7}, LX/02S;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 931
    :cond_1
    if-eqz v4, :cond_2

    invoke-virtual {v4}, LX/00X;->close()V

    :cond_2
    :goto_1
    return-object v0

    :cond_3
    move v0, v2

    .line 932
    goto/16 :goto_0

    .line 933
    :cond_4
    :try_start_3
    const-string v0, "opening dex store %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    invoke-static {v0, v8}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 934
    invoke-static {v5, v6, v7}, LX/02U;->open(Ljava/io/File;Ljava/io/File;LX/02S;)LX/02U;

    move-result-object v5

    .line 935
    sput-object v5, LX/008;->sMainDexStore:LX/02U;

    .line 936
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_d

    .line 937
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_10

    move v0, v1

    .line 938
    :goto_2
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_5

    .line 939
    or-int/lit8 v0, v0, 0x10

    .line 940
    :cond_5
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_6

    .line 941
    or-int/lit8 v0, v0, 0x4

    .line 942
    :cond_6
    invoke-static {}, LX/008;->inCtScanOptMode()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 943
    const-string v1, "disabling background optimization in CT-Scan mode"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 944
    and-int/lit8 v0, v0, -0x6

    .line 945
    or-int/lit8 v0, v0, 0x8

    .line 946
    :cond_7
    invoke-virtual {v5, v0, p2, p0}, LX/02U;->loadAll(ILX/006;Landroid/content/Context;)LX/02X;

    move-result-object v0

    .line 947
    iget v1, v0, LX/02X;->loadResult:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_b

    .line 948
    invoke-static {}, LX/008;->inCtScanOptMode()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 949
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "In ct-scan mode, but not running optimized code. Out of disk space? Bad config?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 950
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 951
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_3
    if-eqz v7, :cond_8

    if-eqz v1, :cond_e

    :try_start_5
    invoke-virtual {v7}, LX/02S;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_8
    :goto_4
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 952
    :catch_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 953
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_5
    if-eqz v4, :cond_9

    if-eqz v3, :cond_f

    :try_start_8
    invoke-virtual {v4}, LX/00X;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    :cond_9
    :goto_6
    throw v0

    .line 954
    :cond_a
    :try_start_9
    const-string v1, "running deoptimized code"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 955
    const/4 v1, 0x1

    sput-boolean v1, LX/008;->deoptTaint:Z
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 956
    :cond_b
    :goto_7
    if-eqz v7, :cond_c

    :try_start_a
    invoke-virtual {v7}, LX/02S;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 957
    :cond_c
    if-eqz v4, :cond_2

    invoke-virtual {v4}, LX/00X;->close()V

    goto/16 :goto_1

    .line 958
    :cond_d
    :try_start_b
    const-string v0, "skipping actual loadAll as requested"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    move-object v0, v3

    .line 959
    goto :goto_7

    .line 960
    :catch_2
    move-exception v2

    :try_start_c
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 961
    :catchall_2
    move-exception v0

    goto :goto_5

    .line 962
    :cond_e
    invoke-virtual {v7}, LX/02S;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_4

    .line 963
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_f
    invoke-virtual {v4}, LX/00X;->close()V

    goto :goto_6

    .line 964
    :catchall_3
    move-exception v0

    move-object v1, v3

    goto :goto_3

    :cond_10
    move v0, v2

    goto/16 :goto_2
.end method

.method public static obtainResProvider(LX/006;Landroid/content/Context;Z)LX/02S;
    .locals 5

    .prologue
    .line 908
    const-string v0, "DLL2_obtain_res_provider"

    const v1, 0x880002

    invoke-virtual {p0, v0, v1}, LX/006;->a(Ljava/lang/String;I)LX/00X;

    move-result-object v2

    const/4 v1, 0x0

    .line 909
    :try_start_0
    invoke-static {p1, p2}, LX/008;->obtainResProviderInternal(Landroid/content/Context;Z)LX/02S;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 910
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/00X;->close()V

    :cond_0
    return-object v0

    .line 911
    :catch_0
    move-exception v0

    .line 912
    :try_start_1
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 913
    :catch_1
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 914
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_3
    invoke-virtual {v2}, LX/00X;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_1
    :goto_1
    throw v0

    :catch_2
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, LX/00X;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private static obtainResProviderInternal(Landroid/content/Context;Z)LX/02S;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 896
    new-instance v0, LX/02R;

    invoke-direct {v0, p0}, LX/02R;-><init>(Landroid/content/Context;)V

    .line 897
    :try_start_0
    const-string v2, "metadata.txt"

    invoke-virtual {v0, v2}, LX/02S;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 898
    :goto_0
    return-object v0

    .line 899
    :catch_0
    :goto_1
    if-eqz p1, :cond_0

    .line 900
    new-instance v0, LX/0FJ;

    invoke-direct {v0, p0}, LX/0FJ;-><init>(Landroid/content/Context;)V

    .line 901
    :try_start_1
    const-string v2, "metadata.txt"

    invoke-virtual {v0, v2}, LX/02S;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 902
    const-string v2, "using exopackage"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 903
    :catch_1
    const-string v2, "using exo res provider failed"

    new-array v3, v4, [Ljava/lang/Object;

    .line 904
    invoke-static {v2, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 905
    :cond_0
    move-object v0, v1

    .line 906
    goto :goto_0

    .line 907
    :catch_2
    goto :goto_1
.end method

.method public static declared-synchronized setRegenerateOnNextLoadHint(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 879
    const-class v1, LX/008;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/008;->sMainDexStore:LX/02U;

    if-eqz v0, :cond_0

    .line 880
    sget-object v0, LX/008;->sMainDexStore:LX/02U;

    invoke-virtual {v0}, LX/02U;->forceRegenerateOnNextLoad()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 881
    :goto_0
    monitor-exit v1

    return-void

    .line 882
    :cond_0
    :try_start_1
    const-string v0, "setRegenerateOnNextLoadHint called without a main dex store present"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 883
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static verifyCanaryClasses()Ljava/lang/RuntimeException;
    .locals 4

    .prologue
    .line 884
    :try_start_0
    invoke-static {}, LX/02U;->dexStoreListHead()LX/02U;

    move-result-object v0

    move-object v1, v0

    .line 885
    :goto_0
    if-eqz v1, :cond_1

    .line 886
    iget-object v0, v1, LX/02U;->mLoadedManifest:LX/02Y;

    move-object v2, v0

    .line 887
    if-eqz v2, :cond_0

    .line 888
    const/4 v0, 0x0

    :goto_1
    iget-object v3, v2, LX/02Y;->dexes:[LX/02Z;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 889
    iget-object v3, v2, LX/02Y;->dexes:[LX/02Z;

    aget-object v3, v3, v0

    iget-object v3, v3, LX/02Z;->canaryClass:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 890
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 891
    :cond_0
    iget-object v0, v1, LX/02U;->next:LX/02U;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    goto :goto_0

    .line 892
    :cond_1
    const/4 v0, 0x0

    .line 893
    :goto_2
    return-object v0

    .line 894
    :catch_0
    move-exception v0

    .line 895
    invoke-static {v0}, LX/02Q;->runtimeExFrom(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    goto :goto_2
.end method
