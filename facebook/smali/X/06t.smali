.class public abstract LX/06t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/06r;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field public final c:LX/01o;

.field private final d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

.field public final e:Ljava/lang/String;

.field private final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/content/SharedPreferences;

.field private h:I

.field private i:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LX/01o;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 18263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18264
    iput-object p1, p0, LX/06t;->a:Landroid/content/Context;

    .line 18265
    iput-object p2, p0, LX/06t;->b:Ljava/lang/String;

    .line 18266
    iput-object p3, p0, LX/06t;->c:LX/01o;

    .line 18267
    iput-object p4, p0, LX/06t;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    .line 18268
    iput-object p5, p0, LX/06t;->e:Ljava/lang/String;

    .line 18269
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/06t;->f:Ljava/util/HashMap;

    .line 18270
    return-void
.end method

.method private b()I
    .locals 4

    .prologue
    .line 18188
    iget-object v0, p0, LX/06t;->c:LX/01o;

    invoke-virtual {v0}, LX/01o;->a()J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private d()V
    .locals 8

    .prologue
    .line 18189
    iget-object v1, p0, LX/06t;->f:Ljava/util/HashMap;

    monitor-enter v1

    .line 18190
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    iget-object v2, p0, LX/06t;->f:Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 18191
    iget-object v2, p0, LX/06t;->f:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 18192
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18193
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18194
    :goto_0
    return-void

    .line 18195
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 18196
    :cond_0
    invoke-direct {p0}, LX/06t;->e()V

    .line 18197
    iget-object v1, p0, LX/06t;->g:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 18198
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 18199
    iget-object v4, p0, LX/06t;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-wide/16 v6, 0x0

    invoke-interface {v4, v1, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 18200
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 18201
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    add-long/2addr v4, v6

    invoke-interface {v2, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 18202
    :cond_1
    invoke-static {v2}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 18203
    iget-object v0, p0, LX/06t;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/06t;->i:J

    goto :goto_0
.end method

.method private declared-synchronized e()V
    .locals 4

    .prologue
    .line 18204
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/06t;->g:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 18205
    iget-object v0, p0, LX/06t;->a:Landroid/content/Context;

    new-instance v1, LX/01q;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "rti.mqtt.counter."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/06t;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 18206
    iget-object v3, p0, LX/06t;->e:Ljava/lang/String;

    move-object v3, v3

    .line 18207
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/01q;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, LX/06t;->g:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18208
    :cond_0
    monitor-exit p0

    return-void

    .line 18209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final varargs a(J[Ljava/lang/String;)LX/06t;
    .locals 7

    .prologue
    .line 18210
    invoke-direct {p0}, LX/06t;->b()I

    move-result v0

    .line 18211
    iget v1, p0, LX/06t;->h:I

    if-eq v1, v0, :cond_0

    .line 18212
    iput v0, p0, LX/06t;->h:I

    .line 18213
    invoke-direct {p0}, LX/06t;->d()V

    .line 18214
    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 18215
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 18216
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18217
    const/4 v1, 0x0

    :goto_0
    array-length v3, p3

    if-ge v1, v3, :cond_1

    .line 18218
    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18219
    aget-object v3, p3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18220
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 18221
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 18222
    iget-object v2, p0, LX/06t;->f:Ljava/util/HashMap;

    monitor-enter v2

    .line 18223
    :try_start_0
    iget-object v0, p0, LX/06t;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 18224
    if-nez v0, :cond_2

    .line 18225
    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 18226
    :cond_2
    iget-object v3, p0, LX/06t;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v4, p1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18227
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18228
    iget-object v0, p0, LX/06t;->d:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/06t;->i:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 18229
    invoke-direct {p0}, LX/06t;->d()V

    .line 18230
    :cond_3
    return-object p0

    .line 18231
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Z)Lorg/json/JSONObject;
    .locals 11

    .prologue
    .line 18232
    invoke-direct {p0}, LX/06t;->e()V

    .line 18233
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 18234
    invoke-direct {p0}, LX/06t;->b()I

    move-result v4

    .line 18235
    iget-object v0, p0, LX/06t;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 18236
    iget-object v1, p0, LX/06t;->g:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 18237
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 18238
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    .line 18239
    if-nez v1, :cond_8

    .line 18240
    :cond_1
    :goto_1
    move v1, v2

    .line 18241
    if-gt v1, v4, :cond_2

    add-int/lit8 v2, v1, 0x3

    if-ge v2, v4, :cond_3

    .line 18242
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v5, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 18243
    :cond_3
    if-eqz p1, :cond_4

    .line 18244
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v3, v1, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 18245
    :cond_4
    if-eq v1, v4, :cond_0

    .line 18246
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 18247
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v5, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 18248
    :cond_5
    invoke-static {v5}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 18249
    invoke-virtual {v3}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-nez v0, :cond_6

    .line 18250
    const/4 v0, 0x0

    .line 18251
    :goto_2
    return-object v0

    .line 18252
    :cond_6
    if-nez p1, :cond_7

    move-object v0, v3

    .line 18253
    goto :goto_2

    .line 18254
    :cond_7
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 18255
    const-string v1, "period_ms"

    .line 18256
    iget-object v7, p0, LX/06t;->c:LX/01o;

    invoke-virtual {v7}, LX/01o;->a()J

    move-result-wide v7

    const-wide/32 v9, 0x5265c00

    rem-long/2addr v7, v9

    move-wide v4, v7

    .line 18257
    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 18258
    const-string v1, "data"

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 18259
    :cond_8
    const-string v7, "."

    invoke-virtual {v1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 18260
    if-lez v7, :cond_1

    .line 18261
    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_1

    .line 18262
    :catch_0
    goto :goto_1
.end method
