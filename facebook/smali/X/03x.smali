.class public final enum LX/03x;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/03x;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/03x;

.field public static final enum NORMAL:LX/03x;

.field public static final enum TEST:LX/03x;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11990
    new-instance v0, LX/03x;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, LX/03x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/03x;->NORMAL:LX/03x;

    .line 11991
    new-instance v0, LX/03x;

    const-string v1, "TEST"

    invoke-direct {v0, v1, v3}, LX/03x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/03x;->TEST:LX/03x;

    .line 11992
    const/4 v0, 0x2

    new-array v0, v0, [LX/03x;

    sget-object v1, LX/03x;->NORMAL:LX/03x;

    aput-object v1, v0, v2

    sget-object v1, LX/03x;->TEST:LX/03x;

    aput-object v1, v0, v3

    sput-object v0, LX/03x;->$VALUES:[LX/03x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11994
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/03x;
    .locals 1

    .prologue
    .line 11995
    const-class v0, LX/03x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/03x;

    return-object v0
.end method

.method public static values()[LX/03x;
    .locals 1

    .prologue
    .line 11993
    sget-object v0, LX/03x;->$VALUES:[LX/03x;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/03x;

    return-object v0
.end method
