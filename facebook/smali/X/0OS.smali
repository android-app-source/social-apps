.class public final LX/0OS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0O8;


# instance fields
.field private final a:LX/0OQ;

.field private final b:J

.field private c:LX/0OA;

.field private d:Ljava/io/File;

.field private e:Ljava/io/FileOutputStream;

.field private f:J

.field private g:J


# direct methods
.method public constructor <init>(LX/0OQ;J)V
    .locals 2

    .prologue
    .line 53022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53023
    invoke-static {p1}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0OQ;

    iput-object v0, p0, LX/0OS;->a:LX/0OQ;

    .line 53024
    iput-wide p2, p0, LX/0OS;->b:J

    .line 53025
    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    .line 53055
    iget-object v0, p0, LX/0OS;->a:LX/0OQ;

    iget-object v1, p0, LX/0OS;->c:LX/0OA;

    iget-object v1, v1, LX/0OA;->f:Ljava/lang/String;

    iget-object v2, p0, LX/0OS;->c:LX/0OA;

    iget-wide v2, v2, LX/0OA;->c:J

    iget-wide v4, p0, LX/0OS;->g:J

    add-long/2addr v2, v4

    iget-object v4, p0, LX/0OS;->c:LX/0OA;

    iget-wide v4, v4, LX/0OA;->e:J

    iget-wide v6, p0, LX/0OS;->g:J

    sub-long/2addr v4, v6

    iget-wide v6, p0, LX/0OS;->b:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, LX/0OQ;->a(Ljava/lang/String;JJ)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/0OS;->d:Ljava/io/File;

    .line 53056
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, LX/0OS;->d:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/0OS;->e:Ljava/io/FileOutputStream;

    .line 53057
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0OS;->f:J

    .line 53058
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 53043
    iget-object v0, p0, LX/0OS;->e:Ljava/io/FileOutputStream;

    if-nez v0, :cond_0

    .line 53044
    :goto_0
    return-void

    .line 53045
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/0OS;->e:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    .line 53046
    iget-object v0, p0, LX/0OS;->e:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53047
    iget-object v0, p0, LX/0OS;->e:Ljava/io/FileOutputStream;

    invoke-static {v0}, LX/08x;->a(Ljava/io/OutputStream;)V

    .line 53048
    iget-object v0, p0, LX/0OS;->a:LX/0OQ;

    iget-object v1, p0, LX/0OS;->d:Ljava/io/File;

    invoke-interface {v0, v1}, LX/0OQ;->a(Ljava/io/File;)V

    .line 53049
    iput-object v2, p0, LX/0OS;->e:Ljava/io/FileOutputStream;

    .line 53050
    iput-object v2, p0, LX/0OS;->d:Ljava/io/File;

    goto :goto_0

    .line 53051
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0OS;->e:Ljava/io/FileOutputStream;

    invoke-static {v1}, LX/08x;->a(Ljava/io/OutputStream;)V

    .line 53052
    iget-object v1, p0, LX/0OS;->d:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 53053
    iput-object v2, p0, LX/0OS;->e:Ljava/io/FileOutputStream;

    .line 53054
    iput-object v2, p0, LX/0OS;->d:Ljava/io/File;

    throw v0
.end method


# virtual methods
.method public final a(LX/0OA;)LX/0O8;
    .locals 4

    .prologue
    .line 53059
    iget-wide v0, p1, LX/0OA;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 53060
    :try_start_0
    iput-object p1, p0, LX/0OS;->c:LX/0OA;

    .line 53061
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0OS;->g:J

    .line 53062
    invoke-direct {p0}, LX/0OS;->b()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53063
    return-object p0

    .line 53064
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 53065
    :catch_0
    move-exception v0

    .line 53066
    new-instance v1, LX/0OR;

    invoke-direct {v1, v0}, LX/0OR;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 53039
    :try_start_0
    invoke-direct {p0}, LX/0OS;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53040
    return-void

    .line 53041
    :catch_0
    move-exception v0

    .line 53042
    new-instance v1, LX/0OR;

    invoke-direct {v1, v0}, LX/0OR;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public final a([BII)V
    .locals 8

    .prologue
    .line 53026
    const/4 v0, 0x0

    .line 53027
    :goto_0
    if-ge v0, p3, :cond_1

    .line 53028
    :try_start_0
    iget-wide v2, p0, LX/0OS;->f:J

    iget-wide v4, p0, LX/0OS;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 53029
    invoke-direct {p0}, LX/0OS;->c()V

    .line 53030
    invoke-direct {p0}, LX/0OS;->b()V

    .line 53031
    :cond_0
    sub-int v1, p3, v0

    int-to-long v2, v1

    iget-wide v4, p0, LX/0OS;->b:J

    iget-wide v6, p0, LX/0OS;->f:J

    sub-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    .line 53032
    iget-object v2, p0, LX/0OS;->e:Ljava/io/FileOutputStream;

    add-int v3, p2, v0

    invoke-virtual {v2, p1, v3, v1}, Ljava/io/FileOutputStream;->write([BII)V

    .line 53033
    add-int/2addr v0, v1

    .line 53034
    iget-wide v2, p0, LX/0OS;->f:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0OS;->f:J

    .line 53035
    iget-wide v2, p0, LX/0OS;->g:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0OS;->g:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 53036
    :catch_0
    move-exception v0

    .line 53037
    new-instance v1, LX/0OR;

    invoke-direct {v1, v0}, LX/0OR;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 53038
    :cond_1
    return-void
.end method
