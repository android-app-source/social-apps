.class public LX/07P;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19693
    return-void
.end method

.method public static a(LX/07R;Ljava/lang/Object;Ljava/lang/Object;)LX/07W;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 19694
    sget-object v0, LX/07i;->a:[I

    iget-object v1, p0, LX/07R;->a:LX/07S;

    invoke-virtual {v1}, LX/07S;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 19695
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown message type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/07R;->a:LX/07S;

    invoke-virtual {v2}, LX/07S;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19696
    :pswitch_0
    new-instance v0, LX/07V;

    check-cast p1, LX/07U;

    check-cast p2, LX/079;

    invoke-direct {v0, p0, p1, p2}, LX/07V;-><init>(LX/07R;LX/07U;LX/079;)V

    .line 19697
    :goto_0
    return-object v0

    .line 19698
    :pswitch_1
    new-instance v0, LX/07j;

    check-cast p1, LX/07e;

    check-cast p2, LX/07h;

    invoke-direct {v0, p0, p1, p2}, LX/07j;-><init>(LX/07R;LX/07e;LX/07h;)V

    goto :goto_0

    .line 19699
    :pswitch_2
    new-instance v0, LX/0I0;

    check-cast p1, LX/0B6;

    check-cast p2, LX/0I1;

    invoke-direct {v0, p0, p1, p2}, LX/0I0;-><init>(LX/07R;LX/0B6;LX/0I1;)V

    goto :goto_0

    .line 19700
    :pswitch_3
    new-instance v0, LX/0Hy;

    check-cast p1, LX/0B6;

    check-cast p2, LX/0Hz;

    invoke-direct {v0, p0, p1, p2}, LX/0Hy;-><init>(LX/07R;LX/0B6;LX/0Hz;)V

    goto :goto_0

    .line 19701
    :pswitch_4
    new-instance v0, LX/0I3;

    check-cast p1, LX/0B6;

    check-cast p2, LX/0I4;

    invoke-direct {v0, p0, p1, p2}, LX/0I3;-><init>(LX/07R;LX/0B6;LX/0I4;)V

    goto :goto_0

    .line 19702
    :pswitch_5
    new-instance v0, LX/07X;

    check-cast p1, LX/0B5;

    check-cast p2, [B

    check-cast p2, [B

    invoke-direct {v0, p0, p1, p2}, LX/07X;-><init>(LX/07R;LX/0B5;[B)V

    goto :goto_0

    .line 19703
    :pswitch_6
    new-instance v0, LX/07W;

    invoke-direct {v0, p0, v2, v2}, LX/07W;-><init>(LX/07R;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 19704
    :pswitch_7
    new-instance v0, LX/07W;

    invoke-direct {v0, p0, v2, v2}, LX/07W;-><init>(LX/07R;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 19705
    :pswitch_8
    new-instance v0, LX/07W;

    invoke-direct {v0, p0, v2, v2}, LX/07W;-><init>(LX/07R;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 19706
    :pswitch_9
    new-instance v0, LX/0B7;

    check-cast p1, LX/0B6;

    invoke-direct {v0, p0, p1}, LX/0B7;-><init>(LX/07R;LX/0B6;)V

    goto :goto_0

    .line 19707
    :pswitch_a
    new-instance v0, LX/0I2;

    check-cast p1, LX/0B6;

    invoke-direct {v0, p0, p1}, LX/0I2;-><init>(LX/07R;LX/0B6;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
