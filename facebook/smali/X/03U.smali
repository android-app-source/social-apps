.class public LX/03U;
.super LX/03V;
.source ""


# static fields
.field public static final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/009;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;

.field private static k:Z


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:Ljava/util/Random;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/009;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Z

.field private i:Landroid/content/Context;

.field private j:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10430
    const-class v0, LX/03U;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/03U;->b:Ljava/lang/String;

    .line 10431
    new-instance v0, LX/0VF;

    invoke-direct {v0}, LX/0VF;-><init>()V

    sput-object v0, LX/03U;->a:LX/0Or;

    .line 10432
    sput-boolean v1, LX/03U;->k:Z

    return-void
.end method

.method private constructor <init>(LX/0Or;LX/0Or;Ljava/util/concurrent/ExecutorService;Ljava/util/Random;LX/0Or;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/Random;",
            "LX/0Or",
            "<",
            "LX/009;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 10421
    invoke-direct {p0}, LX/03V;-><init>()V

    .line 10422
    const/4 v0, 0x0

    iput-object v0, p0, LX/03U;->j:Ljava/lang/Boolean;

    .line 10423
    iput-object p1, p0, LX/03U;->c:LX/0Or;

    .line 10424
    iput-object p2, p0, LX/03U;->d:LX/0Or;

    .line 10425
    iput-object p3, p0, LX/03U;->e:Ljava/util/concurrent/ExecutorService;

    .line 10426
    iput-object p4, p0, LX/03U;->f:Ljava/util/Random;

    .line 10427
    iput-object p5, p0, LX/03U;->g:LX/0Or;

    .line 10428
    iput-boolean p6, p0, LX/03U;->h:Z

    .line 10429
    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;Ljava/util/concurrent/ExecutorService;Ljava/util/Random;Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/Random;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 10418
    sget-object v5, LX/03U;->a:LX/0Or;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, LX/03U;-><init>(LX/0Or;LX/0Or;Ljava/util/concurrent/ExecutorService;Ljava/util/Random;LX/0Or;Z)V

    .line 10419
    iput-object p5, p0, LX/03U;->i:Landroid/content/Context;

    .line 10420
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 10400
    iget-object v0, p0, LX/03U;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 10401
    :cond_0
    :goto_0
    return-void

    .line 10402
    :cond_1
    iget-object v0, p0, LX/03U;->i:Landroid/content/Context;

    if-nez v0, :cond_2

    .line 10403
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/03U;->j:Ljava/lang/Boolean;

    goto :goto_0

    .line 10404
    :cond_2
    const/4 v0, 0x0

    .line 10405
    :try_start_0
    iget-object v3, p0, LX/03U;->i:Landroid/content/Context;

    const-string v4, "soft_errors_pref"

    invoke-virtual {v3, v4}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 10406
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileInputStream;->read()I

    move-result v3

    if-ne v3, v1, :cond_3

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, LX/03U;->j:Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 10407
    if-eqz v0, :cond_0

    .line 10408
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 10409
    :catch_0
    goto :goto_0

    :cond_3
    move v1, v2

    .line 10410
    goto :goto_1

    .line 10411
    :catch_1
    if-eqz v0, :cond_0

    .line 10412
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 10413
    :catch_2
    goto :goto_0

    .line 10414
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_2
    if-eqz v1, :cond_4

    .line 10415
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 10416
    :cond_4
    :goto_3
    throw v0

    :catch_3
    goto :goto_3

    .line 10417
    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x100

    .line 10395
    sget-boolean v0, LX/03U;->k:Z

    if-eqz v0, :cond_0

    .line 10396
    :goto_0
    return-void

    .line 10397
    :cond_0
    invoke-static {v2, v3}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10398
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "runtimeLinterReport category: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " message: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/03W;->THREAD:LX/03W;

    invoke-static {v2, v3, v0, v1}, LX/018;->a(JLjava/lang/String;LX/03W;)V

    .line 10399
    :cond_1
    iget-object v6, p0, LX/03U;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;

    move-object v1, p0

    move-object v2, p1

    move v3, p4

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;-><init>(LX/03U;Ljava/lang/String;ILjava/util/Map;Ljava/lang/String;)V

    const v1, -0x3c1dec53

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public static a$redex0(LX/03U;Ljava/lang/String;IZ)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 10386
    iget-object v0, p0, LX/03U;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/03U;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne v0, v3, :cond_3

    :cond_0
    move v0, v2

    .line 10387
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/03U;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/03U;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 10388
    :cond_1
    if-eqz p3, :cond_4

    move-object p1, v1

    .line 10389
    :cond_2
    :goto_1
    return-object p1

    .line 10390
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 10391
    :cond_4
    iget-object v0, p0, LX/03U;->f:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    rem-int/2addr v0, p2

    if-eqz v0, :cond_5

    move-object p1, v1

    .line 10392
    goto :goto_1

    .line 10393
    :cond_5
    if-eq p2, v2, :cond_2

    .line 10394
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [freq="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1
.end method

.method private b(LX/0VG;)Ljava/lang/String;
    .locals 6

    .prologue
    const-wide/16 v4, 0x100

    const/4 v1, 0x0

    .line 10376
    sget-boolean v0, LX/03U;->k:Z

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 10377
    :goto_0
    return-object v0

    .line 10378
    :cond_0
    iget-object v0, p0, LX/03U;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v2, LX/03R;->YES:LX/03R;

    if-eq v0, v2, :cond_1

    iget-object v0, p0, LX/03U;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 10379
    :goto_1
    invoke-virtual {p1}, LX/0VG;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 10380
    invoke-direct {p0, p1}, LX/03U;->c(LX/0VG;)V

    move-object v0, v1

    .line 10381
    goto :goto_0

    .line 10382
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 10383
    :cond_3
    invoke-static {v4, v5}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 10384
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "softReport category: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/0VG;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " message: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, LX/0VG;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/03W;->THREAD:LX/03W;

    invoke-static {v4, v5, v0, v1}, LX/018;->a(JLjava/lang/String;LX/03W;)V

    .line 10385
    :cond_4
    invoke-direct {p0, p1}, LX/03U;->d(LX/0VG;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c(LX/0VG;)V
    .locals 6

    .prologue
    .line 10370
    iget-object v0, p0, LX/03U;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/009;

    .line 10371
    const-string v1, "soft_error_message"

    invoke-virtual {p1}, LX/0VG;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 10372
    sget-object v1, LX/03U;->b:Ljava/lang/String;

    const-string v2, "category: %s message: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, LX/0VG;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p1}, LX/0VG;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10373
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Soft error FAILING HARDER: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/0VG;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, LX/0VG;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, LX/0VG;->c()Ljava/lang/Throwable;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 10374
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/009;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 10375
    return-void
.end method

.method private d(LX/0VG;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 10328
    invoke-virtual {p1}, LX/0VG;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LX/0VG;->e()I

    move-result v1

    invoke-virtual {p1}, LX/0VG;->f()Z

    move-result v2

    invoke-static {p0, v0, v1, v2}, LX/03U;->a$redex0(LX/03U;Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0VG;)V
    .locals 5

    .prologue
    .line 10363
    invoke-direct {p0}, LX/03U;->a()V

    .line 10364
    invoke-direct {p0, p1}, LX/03U;->b(LX/0VG;)Ljava/lang/String;

    move-result-object v0

    .line 10365
    if-nez v0, :cond_0

    .line 10366
    :goto_0
    return-void

    .line 10367
    :cond_0
    invoke-virtual {p1}, LX/0VG;->b()Ljava/lang/String;

    move-result-object v1

    .line 10368
    new-instance v2, LX/0VH;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, LX/0VG;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/0VG;->c()Ljava/lang/Throwable;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/0VH;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 10369
    iget-object v3, p0, LX/03U;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$1;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$1;-><init>(LX/03U;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const v0, -0x33f38983    # -3.6821492E7f

    invoke-static {v3, v4, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 10361
    iget-object v0, p0, LX/03U;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/009;

    invoke-virtual {v0, p1}, LX/009;->removeCustomData(Ljava/lang/String;)Ljava/lang/String;

    .line 10362
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0VI;)V
    .locals 2

    .prologue
    .line 10326
    iget-object v0, p0, LX/03U;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/009;

    new-instance v1, LX/0VJ;

    invoke-direct {v1, p0, p2}, LX/0VJ;-><init>(LX/03U;LX/0VI;)V

    invoke-virtual {v0, p1, v1}, LX/009;->putLazyCustomData(Ljava/lang/String;LX/00T;)V

    .line 10327
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x100

    .line 10329
    sget-boolean v0, LX/03U;->k:Z

    if-eqz v0, :cond_0

    .line 10330
    :goto_0
    return-void

    .line 10331
    :cond_0
    invoke-static {v2, v3}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10332
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "StrictModeReport category: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " message: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/03W;->THREAD:LX/03W;

    invoke-static {v2, v3, v0, v1}, LX/018;->a(JLjava/lang/String;LX/03W;)V

    .line 10333
    :cond_1
    iget-object v0, p0, LX/03U;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;-><init>(LX/03U;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const v2, -0x5194c3fe

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 10334
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, LX/03U;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;I)V

    .line 10335
    return-void
.end method

.method public final a(Ljava/net/Proxy;)V
    .locals 1

    .prologue
    .line 10336
    iget-object v0, p0, LX/03U;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/009;

    invoke-virtual {v0, p1}, LX/009;->setReportProxy(Ljava/net/Proxy;)V

    .line 10337
    return-void
.end method

.method public final a(Z)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 10338
    iget-object v2, p0, LX/03U;->i:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 10339
    :cond_0
    :goto_0
    return v0

    .line 10340
    :cond_1
    const/4 v2, 0x0

    .line 10341
    :try_start_0
    iget-object v3, p0, LX/03U;->i:Landroid/content/Context;

    const-string v4, "soft_errors_pref"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 10342
    if-eqz p1, :cond_3

    move v3, v1

    :goto_1
    :try_start_1
    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 10343
    if-eqz v2, :cond_2

    .line 10344
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_2
    move v0, v1

    .line 10345
    goto :goto_0

    :cond_3
    move v3, v0

    .line 10346
    goto :goto_1

    .line 10347
    :catch_0
    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_0

    .line 10348
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    goto :goto_0

    .line 10349
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_4

    .line 10350
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 10351
    :cond_4
    :goto_4
    throw v0

    :catch_2
    goto :goto_2

    :catch_3
    goto :goto_4

    .line 10352
    :catch_4
    move-object v1, v2

    goto :goto_3
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 10353
    iget-object v0, p0, LX/03U;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/009;

    invoke-virtual {v0, p1}, LX/009;->removeLazyCustomData(Ljava/lang/String;)LX/00T;

    .line 10354
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 10355
    iget-object v0, p0, LX/03U;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/009;

    invoke-virtual {v0, p1}, LX/009;->setUserId(Ljava/lang/String;)V

    .line 10356
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 10357
    iget-object v0, p0, LX/03U;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/009;

    invoke-virtual {v0, p1, p2}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 10358
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 10359
    iget-object v0, p0, LX/03U;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/009;

    invoke-virtual {v0, p1}, LX/009;->registerActivity(Ljava/lang/String;)V

    .line 10360
    return-void
.end method
