.class public final LX/01G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# instance fields
.field private final a:Lcom/facebook/analytics/appstatelogger/AppStateLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/analytics/appstatelogger/AppStateLogger;)V
    .locals 0

    .prologue
    .line 4101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4102
    iput-object p1, p0, LX/01G;->a:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    .line 4103
    return-void
.end method


# virtual methods
.method public final onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 4098
    iget-object v0, p0, LX/01G;->a:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    const/4 v1, 0x1

    .line 4099
    invoke-static {v0, p1, v1}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a$redex0(Lcom/facebook/analytics/appstatelogger/AppStateLogger;Landroid/app/Activity;I)V

    .line 4100
    return-void
.end method

.method public final onActivityDestroyed(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 4095
    iget-object v0, p0, LX/01G;->a:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    const/4 v1, 0x2

    .line 4096
    invoke-static {v0, p1, v1}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a$redex0(Lcom/facebook/analytics/appstatelogger/AppStateLogger;Landroid/app/Activity;I)V

    .line 4097
    return-void
.end method

.method public final onActivityPaused(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 4082
    iget-object v0, p0, LX/01G;->a:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    const/4 v1, 0x6

    .line 4083
    invoke-static {v0, p1, v1}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a$redex0(Lcom/facebook/analytics/appstatelogger/AppStateLogger;Landroid/app/Activity;I)V

    .line 4084
    return-void
.end method

.method public final onActivityResumed(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 4092
    iget-object v0, p0, LX/01G;->a:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    const/4 v1, 0x5

    .line 4093
    invoke-static {v0, p1, v1}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a$redex0(Lcom/facebook/analytics/appstatelogger/AppStateLogger;Landroid/app/Activity;I)V

    .line 4094
    return-void
.end method

.method public final onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 4091
    return-void
.end method

.method public final onActivityStarted(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 4088
    iget-object v0, p0, LX/01G;->a:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    const/4 v1, 0x3

    .line 4089
    invoke-static {v0, p1, v1}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a$redex0(Lcom/facebook/analytics/appstatelogger/AppStateLogger;Landroid/app/Activity;I)V

    .line 4090
    return-void
.end method

.method public final onActivityStopped(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 4085
    iget-object v0, p0, LX/01G;->a:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    const/4 v1, 0x4

    .line 4086
    invoke-static {v0, p1, v1}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a$redex0(Lcom/facebook/analytics/appstatelogger/AppStateLogger;Landroid/app/Activity;I)V

    .line 4087
    return-void
.end method
