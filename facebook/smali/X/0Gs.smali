.class public LX/0Gs;
.super LX/0Gq;
.source ""


# instance fields
.field private final e:LX/0Gt;

.field private final f:Ljava/lang/String;

.field private final g:LX/0GB;

.field private final h:I

.field private final i:I

.field private j:LX/0Gg;

.field public k:LX/0Gg;

.field private l:Ljava/io/IOException;

.field private m:I

.field private n:Z


# direct methods
.method public constructor <init>(LX/0Gt;Ljava/lang/String;IILX/0GB;LX/0Gg;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 36687
    invoke-direct {p0}, LX/0Gq;-><init>()V

    .line 36688
    iput-object p1, p0, LX/0Gs;->e:LX/0Gt;

    .line 36689
    iput-object p2, p0, LX/0Gs;->f:Ljava/lang/String;

    .line 36690
    iput p3, p0, LX/0Gs;->h:I

    .line 36691
    iput p4, p0, LX/0Gs;->i:I

    .line 36692
    iput-object p5, p0, LX/0Gs;->g:LX/0GB;

    .line 36693
    iput-object p6, p0, LX/0Gs;->j:LX/0Gg;

    .line 36694
    iput-object v1, p0, LX/0Gs;->a:LX/0Ge;

    .line 36695
    iput-object v1, p0, LX/0Gs;->b:[B

    .line 36696
    iput-object v1, p0, LX/0Gs;->l:Ljava/io/IOException;

    .line 36697
    iput v0, p0, LX/0Gs;->c:I

    .line 36698
    iput v0, p0, LX/0Gs;->d:I

    .line 36699
    iput v0, p0, LX/0Gs;->m:I

    .line 36700
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Gs;->n:Z

    .line 36701
    return-void
.end method

.method private a(J)V
    .locals 1

    .prologue
    .line 36702
    const v0, 0x17130521

    :try_start_0
    invoke-static {p0, p1, p2, v0}, LX/02L;->a(Ljava/lang/Object;JI)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36703
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private static a(LX/0Ge;)V
    .locals 1

    .prologue
    .line 36677
    :try_start_0
    invoke-interface {p0}, LX/0Ge;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36678
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private b([BII)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 36679
    iget v0, p0, LX/0Gs;->m:I

    if-gez v0, :cond_0

    .line 36680
    iput v1, p0, LX/0Gs;->m:I

    .line 36681
    :cond_0
    iget v0, p0, LX/0Gq;->d:I

    iget v2, p0, LX/0Gs;->m:I

    sub-int/2addr v0, v2

    .line 36682
    if-nez v0, :cond_1

    move p3, v1

    .line 36683
    :goto_0
    return p3

    .line 36684
    :cond_1
    if-le v0, p3, :cond_2

    .line 36685
    :goto_1
    iget-object v0, p0, LX/0Gq;->b:[B

    iget v1, p0, LX/0Gs;->m:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 36686
    iget v0, p0, LX/0Gs;->m:I

    add-int/2addr v0, p3

    iput v0, p0, LX/0Gs;->m:I

    goto :goto_0

    :cond_2
    move p3, v0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0OA;Ljava/lang/String;Landroid/net/Uri;)I
    .locals 7

    .prologue
    const/high16 v0, 0x100000

    const/4 v2, 0x0

    .line 36617
    iget-object v1, p0, LX/0Gs;->j:LX/0Gg;

    if-eqz v1, :cond_0

    .line 36618
    iget-object v1, p0, LX/0Gs;->j:LX/0Gg;

    iget-object v3, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/0AC;->NOT_CACHED:LX/0AC;

    invoke-virtual {v1, v3, v4}, LX/0Gg;->a(Ljava/lang/String;LX/0AC;)V

    .line 36619
    :cond_0
    monitor-enter p0

    .line 36620
    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, LX/0Gs;->l:Ljava/io/IOException;

    .line 36621
    const/4 v1, -0x1

    iput v1, p0, LX/0Gs;->c:I

    .line 36622
    const/4 v1, -0x1

    iput v1, p0, LX/0Gs;->d:I

    .line 36623
    const/4 v1, -0x1

    iput v1, p0, LX/0Gs;->m:I

    .line 36624
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/0Gs;->n:Z

    .line 36625
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36626
    iget-object v1, p0, LX/0Gs;->e:LX/0Gt;

    iget-object v3, p0, LX/0Gs;->f:Ljava/lang/String;

    iget-object v4, p0, LX/0Gs;->j:LX/0Gg;

    iget v5, p0, LX/0Gs;->h:I

    iget v6, p0, LX/0Gs;->i:I

    invoke-virtual {v1, v3, v4, v5, v6}, LX/0Gt;->a(Ljava/lang/String;LX/04n;II)LX/0Ge;

    move-result-object v1

    .line 36627
    :try_start_1
    invoke-interface {v1, p1}, LX/0Ge;->a(LX/0OA;)J

    move-result-wide v4

    .line 36628
    monitor-enter p0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 36629
    long-to-int v3, v4

    :try_start_2
    iput v3, p0, LX/0Gs;->c:I

    .line 36630
    const/4 v3, 0x0

    iput v3, p0, LX/0Gs;->d:I

    .line 36631
    iget v3, p0, LX/0Gq;->c:I

    if-le v3, v0, :cond_4

    .line 36632
    :goto_0
    new-array v3, v0, [B

    .line 36633
    iput-object v1, p0, LX/0Gs;->a:LX/0Ge;

    .line 36634
    iput-object v3, p0, LX/0Gs;->b:[B

    .line 36635
    const v4, -0x685d438e

    invoke-static {p0, v4}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 36636
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 36637
    :goto_1
    :try_start_3
    iget-boolean v4, p0, LX/0Gs;->n:Z

    if-eqz v4, :cond_1

    iget v4, p0, LX/0Gq;->d:I

    if-ge v4, v0, :cond_1

    .line 36638
    iget v4, p0, LX/0Gq;->d:I

    iget v5, p0, LX/0Gq;->d:I

    sub-int v5, v0, v5

    invoke-interface {v1, v3, v4, v5}, LX/0Ge;->a([BII)I

    move-result v4

    .line 36639
    monitor-enter p0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 36640
    if-gez v4, :cond_6

    .line 36641
    :try_start_4
    iget v0, p0, LX/0Gq;->d:I

    iput v0, p0, LX/0Gs;->c:I

    .line 36642
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 36643
    :cond_1
    :try_start_5
    monitor-enter p0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 36644
    const/4 v0, 0x0

    :try_start_6
    iput-boolean v0, p0, LX/0Gs;->n:Z

    .line 36645
    iget v0, p0, LX/0Gq;->c:I

    iget v3, p0, LX/0Gq;->d:I

    if-ne v0, v3, :cond_3

    .line 36646
    invoke-static {v1}, LX/0Gs;->a(LX/0Ge;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 36647
    const/4 v0, 0x0

    :try_start_7
    iput-object v0, p0, LX/0Gs;->a:LX/0Ge;

    .line 36648
    iget-object v0, p0, LX/0Gs;->g:LX/0GB;

    if-eqz v0, :cond_2

    .line 36649
    iget-object v0, p0, LX/0Gs;->g:LX/0GB;

    iget-object v1, p0, LX/0Gq;->b:[B

    iget v3, p0, LX/0Gq;->c:I

    invoke-virtual {v0, p2, p3, v1, v3}, LX/0GB;->a(Ljava/lang/String;Landroid/net/Uri;[BI)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    :cond_2
    move-object v1, v2

    .line 36650
    :cond_3
    const v0, 0x41134139    # 9.2034235f

    :try_start_8
    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 36651
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 36652
    :goto_2
    iput-object v2, p0, LX/0Gs;->j:LX/0Gg;

    .line 36653
    iget v0, p0, LX/0Gq;->d:I

    return v0

    .line 36654
    :catchall_0
    move-exception v0

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    throw v0

    .line 36655
    :cond_4
    :try_start_a
    iget v0, p0, LX/0Gq;->c:I

    goto :goto_0

    .line 36656
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v0
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0

    .line 36657
    :catch_0
    move-exception v0

    .line 36658
    iget-object v3, p0, LX/0Gs;->j:LX/0Gg;

    if-eqz v3, :cond_5

    .line 36659
    iget-object v3, p0, LX/0Gs;->j:LX/0Gg;

    invoke-virtual {v3, v0}, LX/0Gg;->a(Ljava/io/IOException;)V

    .line 36660
    :cond_5
    monitor-enter p0

    .line 36661
    const/4 v3, 0x0

    :try_start_c
    iput-boolean v3, p0, LX/0Gs;->n:Z

    .line 36662
    invoke-static {v1}, LX/0Gs;->a(LX/0Ge;)V

    .line 36663
    const/4 v1, 0x0

    iput-object v1, p0, LX/0Gs;->a:LX/0Ge;

    .line 36664
    iget v1, p0, LX/0Gs;->m:I

    if-gez v1, :cond_8

    .line 36665
    const v1, -0xcfab80a

    invoke-static {p0, v1}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 36666
    throw v0

    .line 36667
    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    throw v0

    .line 36668
    :cond_6
    :try_start_d
    iget v5, p0, LX/0Gq;->d:I

    add-int/2addr v5, v4

    iput v5, p0, LX/0Gs;->d:I

    .line 36669
    if-lez v4, :cond_7

    .line 36670
    const v4, 0x5530e658

    invoke-static {p0, v4}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 36671
    :cond_7
    monitor-exit p0

    goto :goto_1

    :catchall_3
    move-exception v0

    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :try_start_e
    throw v0
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_0

    .line 36672
    :catchall_4
    move-exception v0

    :goto_3
    :try_start_f
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    :try_start_10
    throw v0
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_0

    .line 36673
    :cond_8
    :try_start_11
    iput-object v0, p0, LX/0Gs;->l:Ljava/io/IOException;

    .line 36674
    const v0, -0x10dd863d

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 36675
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    goto :goto_2

    .line 36676
    :catchall_5
    move-exception v0

    move-object v1, v2

    goto :goto_3
.end method

.method public final a([BII)I
    .locals 3

    .prologue
    .line 36580
    :goto_0
    iget-boolean v0, p0, LX/0Gs;->n:Z

    if-eqz v0, :cond_4

    .line 36581
    monitor-enter p0

    .line 36582
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, LX/0Gs;->b([BII)I

    move-result v0

    .line 36583
    if-lez v0, :cond_2

    .line 36584
    iget-object v1, p0, LX/0Gs;->k:LX/0Gg;

    if-eqz v1, :cond_0

    .line 36585
    iget-object v1, p0, LX/0Gs;->k:LX/0Gg;

    invoke-virtual {v1, v0}, LX/0Gg;->a(I)V

    .line 36586
    :cond_0
    monitor-exit p0

    .line 36587
    :cond_1
    :goto_1
    return v0

    .line 36588
    :cond_2
    iget v0, p0, LX/0Gs;->i:I

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, LX/0Gs;->a(J)V

    .line 36589
    iget v0, p0, LX/0Gq;->d:I

    iget v1, p0, LX/0Gs;->m:I

    if-gt v0, v1, :cond_3

    .line 36590
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Gs;->n:Z

    .line 36591
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Read timed out"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0Gs;->l:Ljava/io/IOException;

    .line 36592
    :cond_3
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 36593
    :cond_4
    iget-object v0, p0, LX/0Gs;->l:Ljava/io/IOException;

    if-eqz v0, :cond_6

    .line 36594
    iget-object v0, p0, LX/0Gs;->k:LX/0Gg;

    if-eqz v0, :cond_5

    .line 36595
    iget-object v0, p0, LX/0Gs;->k:LX/0Gg;

    iget-object v1, p0, LX/0Gs;->l:Ljava/io/IOException;

    invoke-virtual {v0, v1}, LX/0Gg;->a(Ljava/io/IOException;)V

    .line 36596
    :cond_5
    iget-object v0, p0, LX/0Gs;->l:Ljava/io/IOException;

    throw v0

    .line 36597
    :cond_6
    invoke-direct {p0, p1, p2, p3}, LX/0Gs;->b([BII)I

    move-result v0

    .line 36598
    if-lez v0, :cond_7

    .line 36599
    iget-object v1, p0, LX/0Gs;->k:LX/0Gg;

    if-eqz v1, :cond_1

    .line 36600
    iget-object v1, p0, LX/0Gs;->k:LX/0Gg;

    invoke-virtual {v1, v0}, LX/0Gg;->a(I)V

    goto :goto_1

    .line 36601
    :cond_7
    monitor-enter p0

    .line 36602
    :try_start_1
    iget-object v1, p0, LX/0Gq;->a:LX/0Ge;

    .line 36603
    monitor-exit p0

    .line 36604
    if-nez v1, :cond_8

    .line 36605
    const/4 v0, -0x1

    goto :goto_1

    .line 36606
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 36607
    :cond_8
    :try_start_2
    invoke-interface {v1, p1, p2, p3}, LX/0Ge;->a([BII)I

    move-result v0

    .line 36608
    if-lez v0, :cond_1

    .line 36609
    iget-object v2, p0, LX/0Gs;->k:LX/0Gg;

    if-eqz v2, :cond_1

    .line 36610
    iget-object v2, p0, LX/0Gs;->k:LX/0Gg;

    invoke-virtual {v2, v0}, LX/0Gg;->a(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 36611
    :catch_0
    move-exception v0

    .line 36612
    iget-object v2, p0, LX/0Gs;->k:LX/0Gg;

    if-eqz v2, :cond_9

    .line 36613
    iget-object v2, p0, LX/0Gs;->k:LX/0Gg;

    invoke-virtual {v2, v0}, LX/0Gg;->a(Ljava/io/IOException;)V

    .line 36614
    :cond_9
    invoke-static {v1}, LX/0Gs;->a(LX/0Ge;)V

    .line 36615
    const/4 v1, 0x0

    iput-object v1, p0, LX/0Gs;->a:LX/0Ge;

    .line 36616
    throw v0
.end method

.method public final a(LX/0OA;)J
    .locals 3

    .prologue
    .line 36559
    monitor-enter p0

    .line 36560
    :try_start_0
    invoke-virtual {p0}, LX/0Gq;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/0AC;->CACHED:LX/0AC;

    .line 36561
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 36562
    iget-object v1, p0, LX/0Gs;->k:LX/0Gg;

    if-eqz v1, :cond_0

    .line 36563
    iget-object v1, p0, LX/0Gs;->k:LX/0Gg;

    iget-object v2, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/0Gg;->a(Ljava/lang/String;LX/0AC;)V

    .line 36564
    :cond_0
    monitor-enter p0

    .line 36565
    :try_start_1
    iget v0, p0, LX/0Gq;->c:I

    if-gez v0, :cond_3

    .line 36566
    iget v0, p0, LX/0Gs;->h:I

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, LX/0Gs;->a(J)V

    .line 36567
    iget v0, p0, LX/0Gq;->c:I

    if-gez v0, :cond_3

    .line 36568
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Gs;->n:Z

    .line 36569
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Connection timed out"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0Gs;->l:Ljava/io/IOException;

    .line 36570
    iget-object v0, p0, LX/0Gs;->k:LX/0Gg;

    if-eqz v0, :cond_1

    .line 36571
    iget-object v0, p0, LX/0Gs;->k:LX/0Gg;

    iget-object v1, p0, LX/0Gs;->l:Ljava/io/IOException;

    invoke-virtual {v0, v1}, LX/0Gg;->a(Ljava/io/IOException;)V

    .line 36572
    :cond_1
    iget-object v0, p0, LX/0Gs;->l:Ljava/io/IOException;

    throw v0

    .line 36573
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 36574
    :cond_2
    :try_start_2
    sget-object v0, LX/0AC;->SEMI_CACHED:LX/0AC;

    goto :goto_0

    .line 36575
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 36576
    :cond_3
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 36577
    iget-object v0, p0, LX/0Gs;->k:LX/0Gg;

    if-eqz v0, :cond_4

    .line 36578
    iget-object v0, p0, LX/0Gs;->k:LX/0Gg;

    invoke-virtual {v0}, LX/0Gg;->b()V

    .line 36579
    :cond_4
    iget v0, p0, LX/0Gq;->c:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36550
    iget-object v0, p0, LX/0Gs;->k:LX/0Gg;

    if-eqz v0, :cond_0

    .line 36551
    iget-object v0, p0, LX/0Gs;->k:LX/0Gg;

    invoke-virtual {v0}, LX/0Gg;->c()V

    .line 36552
    iput-object v1, p0, LX/0Gs;->k:LX/0Gg;

    .line 36553
    :cond_0
    monitor-enter p0

    .line 36554
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/0Gs;->n:Z

    .line 36555
    iget-object v0, p0, LX/0Gq;->a:LX/0Ge;

    if-eqz v0, :cond_1

    .line 36556
    iget-object v0, p0, LX/0Gq;->a:LX/0Ge;

    invoke-static {v0}, LX/0Gs;->a(LX/0Ge;)V

    .line 36557
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Gs;->a:LX/0Ge;

    .line 36558
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
