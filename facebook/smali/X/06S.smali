.class public LX/06S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/06R;


# instance fields
.field public final a:LX/059;


# direct methods
.method public constructor <init>(LX/059;)V
    .locals 0

    .prologue
    .line 17879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17880
    iput-object p1, p0, LX/06S;->a:LX/059;

    .line 17881
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 17882
    iget-object v0, p0, LX/06S;->a:LX/059;

    invoke-virtual {v0}, LX/059;->c()Z

    move-result v0

    .line 17883
    if-nez v0, :cond_0

    .line 17884
    iget-object v1, p0, LX/06S;->a:LX/059;

    invoke-virtual {v1}, LX/059;->d()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 17885
    if-nez p1, :cond_1

    .line 17886
    :cond_0
    :goto_0
    move v0, v0

    .line 17887
    return v0

    .line 17888
    :cond_1
    if-nez v1, :cond_2

    .line 17889
    const-string v2, "MqttNetworkManagerMonitor"

    const-string v3, "no_info"

    invoke-interface {p1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 17890
    :cond_2
    const-string v2, "MqttNetworkManagerMonitor"

    const-string v3, "%s_%s_%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object p0

    aput-object p0, v4, v5

    invoke-static {v3, v4}, LX/05V;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
