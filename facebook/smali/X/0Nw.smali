.class public final LX/0Nw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ME;


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/regex/Pattern;


# instance fields
.field private final c:LX/0NJ;

.field private final d:LX/0Oj;

.field private e:LX/0LU;

.field private f:[B

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52301
    const-string v0, "LOCAL:([^,]+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nw;->a:Ljava/util/regex/Pattern;

    .line 52302
    const-string v0, "MPEGTS:(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Nw;->b:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(LX/0NJ;)V
    .locals 1

    .prologue
    .line 52303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52304
    iput-object p1, p0, LX/0Nw;->c:LX/0NJ;

    .line 52305
    new-instance v0, LX/0Oj;

    invoke-direct {v0}, LX/0Oj;-><init>()V

    iput-object v0, p0, LX/0Nw;->d:LX/0Oj;

    .line 52306
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, LX/0Nw;->f:[B

    .line 52307
    return-void
.end method

.method private a(J)LX/0LS;
    .locals 9

    .prologue
    .line 52267
    iget-object v0, p0, LX/0Nw;->e:LX/0LU;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v8

    .line 52268
    const-string v0, "id"

    const-string v1, "text/vtt"

    const/4 v2, -0x1

    const-wide/16 v3, -0x1

    const-string v5, "en"

    move-wide v6, p1

    invoke-static/range {v0 .. v7}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;J)LX/0L4;

    move-result-object v0

    invoke-interface {v8, v0}, LX/0LS;->a(LX/0L4;)V

    .line 52269
    iget-object v0, p0, LX/0Nw;->e:LX/0LU;

    invoke-interface {v0}, LX/0LU;->a()V

    .line 52270
    return-object v8
.end method

.method private a()V
    .locals 14

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    .line 52271
    new-instance v5, LX/0Oj;

    iget-object v0, p0, LX/0Nw;->f:[B

    invoke-direct {v5, v0}, LX/0Oj;-><init>([B)V

    .line 52272
    invoke-static {v5}, LX/0Nz;->a(LX/0Oj;)V

    move-wide v0, v2

    move-wide v6, v2

    .line 52273
    :cond_0
    :goto_0
    invoke-virtual {v5}, LX/0Oj;->v()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 52274
    const-string v9, "X-TIMESTAMP-MAP"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 52275
    sget-object v0, LX/0Nw;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 52276
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-nez v1, :cond_1

    .line 52277
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "X-TIMESTAMP-MAP doesn\'t contain local timestamp: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52278
    :cond_1
    sget-object v1, LX/0Nw;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 52279
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-nez v6, :cond_2

    .line 52280
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "X-TIMESTAMP-MAP doesn\'t contain media timestamp: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52281
    :cond_2
    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Nz;->a(Ljava/lang/String;)J

    move-result-wide v6

    .line 52282
    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, LX/0NJ;->b(J)J

    move-result-wide v0

    goto :goto_0

    .line 52283
    :cond_3
    invoke-virtual {v5}, LX/0Oj;->v()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 52284
    sget-object v9, LX/0Ny;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v9, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 52285
    :goto_1
    invoke-virtual {v5}, LX/0Oj;->v()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    goto :goto_1

    .line 52286
    :cond_4
    sget-object v9, LX/0Ny;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v9, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    .line 52287
    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 52288
    :goto_2
    move-object v5, v8

    .line 52289
    if-nez v5, :cond_5

    .line 52290
    invoke-direct {p0, v2, v3}, LX/0Nw;->a(J)LX/0LS;

    .line 52291
    :goto_3
    return-void

    .line 52292
    :cond_5
    invoke-virtual {v5, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0Nz;->a(Ljava/lang/String;)J

    move-result-wide v8

    .line 52293
    iget-object v2, p0, LX/0Nw;->c:LX/0NJ;

    add-long/2addr v0, v8

    sub-long/2addr v0, v6

    .line 52294
    const-wide/32 v10, 0x15f90

    mul-long/2addr v10, v0

    const-wide/32 v12, 0xf4240

    div-long/2addr v10, v12

    move-wide v0, v10

    .line 52295
    invoke-virtual {v2, v0, v1}, LX/0NJ;->a(J)J

    move-result-wide v2

    .line 52296
    sub-long v0, v2, v8

    .line 52297
    invoke-direct {p0, v0, v1}, LX/0Nw;->a(J)LX/0LS;

    move-result-object v1

    .line 52298
    iget-object v0, p0, LX/0Nw;->d:LX/0Oj;

    iget-object v5, p0, LX/0Nw;->f:[B

    iget v6, p0, LX/0Nw;->g:I

    invoke-virtual {v0, v5, v6}, LX/0Oj;->a([BI)V

    .line 52299
    iget-object v0, p0, LX/0Nw;->d:LX/0Oj;

    iget v5, p0, LX/0Nw;->g:I

    invoke-interface {v1, v0, v5}, LX/0LS;->a(LX/0Oj;I)V

    .line 52300
    iget v5, p0, LX/0Nw;->g:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    goto :goto_3

    :cond_6
    const/4 v8, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/0MA;LX/0MM;)I
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 52255
    invoke-interface {p1}, LX/0MA;->d()J

    move-result-wide v0

    long-to-int v1, v0

    .line 52256
    iget v0, p0, LX/0Nw;->g:I

    iget-object v3, p0, LX/0Nw;->f:[B

    array-length v3, v3

    if-ne v0, v3, :cond_0

    .line 52257
    iget-object v3, p0, LX/0Nw;->f:[B

    if-eq v1, v2, :cond_2

    move v0, v1

    :goto_0
    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x2

    invoke-static {v3, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, LX/0Nw;->f:[B

    .line 52258
    :cond_0
    iget-object v0, p0, LX/0Nw;->f:[B

    iget v3, p0, LX/0Nw;->g:I

    iget-object v4, p0, LX/0Nw;->f:[B

    array-length v4, v4

    iget v5, p0, LX/0Nw;->g:I

    sub-int/2addr v4, v5

    invoke-interface {p1, v0, v3, v4}, LX/0MA;->a([BII)I

    move-result v0

    .line 52259
    if-eq v0, v2, :cond_3

    .line 52260
    iget v3, p0, LX/0Nw;->g:I

    add-int/2addr v0, v3

    iput v0, p0, LX/0Nw;->g:I

    .line 52261
    if-eq v1, v2, :cond_1

    iget v0, p0, LX/0Nw;->g:I

    if-eq v0, v1, :cond_3

    .line 52262
    :cond_1
    const/4 v0, 0x0

    .line 52263
    :goto_1
    return v0

    .line 52264
    :cond_2
    iget-object v0, p0, LX/0Nw;->f:[B

    array-length v0, v0

    goto :goto_0

    .line 52265
    :cond_3
    invoke-direct {p0}, LX/0Nw;->a()V

    move v0, v2

    .line 52266
    goto :goto_1
.end method

.method public final a(LX/0LU;)V
    .locals 1

    .prologue
    .line 52252
    iput-object p1, p0, LX/0Nw;->e:LX/0LU;

    .line 52253
    sget-object v0, LX/0M8;->f:LX/0M8;

    invoke-interface {p1, v0}, LX/0LU;->a(LX/0M8;)V

    .line 52254
    return-void
.end method

.method public final a(LX/0MA;)Z
    .locals 1

    .prologue
    .line 52251
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 52250
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method
