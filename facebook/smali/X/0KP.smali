.class public final LX/0KP;
.super LX/0BS;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/vps/VideoPlayerService;


# direct methods
.method public constructor <init>(Lcom/facebook/video/vps/VideoPlayerService;)V
    .locals 0

    .prologue
    .line 41091
    iput-object p1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-direct {p0}, LX/0BS;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/VideoPlayRequest;)LX/0AY;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 41092
    if-nez p2, :cond_0

    .line 41093
    :goto_0
    return-object v0

    .line 41094
    :cond_0
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v3

    .line 41095
    :try_start_0
    iget-object v1, p2, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p2, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 41096
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p2, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->g:Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-virtual {v2, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 41097
    :goto_1
    if-nez v1, :cond_1

    :try_start_1
    iget-object v2, p2, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->h:Landroid/os/ParcelFileDescriptor;

    if-eqz v2, :cond_1

    .line 41098
    iget-object v2, p2, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->h:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    .line 41099
    invoke-virtual {v2}, Ljava/io/FileDescriptor;->valid()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 41100
    new-instance v2, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    iget-object v4, p2, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->h:Landroid/os/ParcelFileDescriptor;

    invoke-direct {v2, v4}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    move-object v1, v2

    .line 41101
    :cond_1
    if-eqz v1, :cond_2

    .line 41102
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Using dash for "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41103
    new-instance v0, LX/0AZ;

    invoke-direct {v0}, LX/0AZ;-><init>()V

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Gj;->a(LX/0AZ;Ljava/io/InputStream;Ljava/lang/String;)LX/0AY;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 41104
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 41105
    :goto_2
    invoke-static {v3}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_0

    .line 41106
    :catch_0
    const-string v1, "inputStream close fail, nothing we can do"

    invoke-static {v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_2

    .line 41107
    :cond_2
    :try_start_3
    iget-object v2, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Using dash with direct url: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 41108
    invoke-static {v3}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto/16 :goto_0

    .line 41109
    :catch_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 41110
    :goto_3
    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 41111
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v1, :cond_3

    .line 41112
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 41113
    :cond_3
    :goto_5
    invoke-static {v3}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v0

    .line 41114
    :catch_2
    const-string v1, "inputStream close fail, nothing we can do"

    invoke-static {v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_5

    .line 41115
    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_4

    .line 41116
    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_3

    :catch_5
    move-exception v0

    goto :goto_3

    :cond_4
    move-object v1, v0

    goto/16 :goto_1
.end method

.method private a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41117
    if-eqz p2, :cond_0

    const-string v2, "file"

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    move v3, v2

    .line 41118
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v2, v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/util/Map;)LX/0Kx;

    move-result-object v2

    .line 41119
    if-nez v2, :cond_1

    .line 41120
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "ExoPlayer cannot be null!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 41121
    :cond_0
    const/4 v2, 0x0

    move v3, v2

    goto :goto_0

    .line 41122
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v6, v2, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    monitor-enter v6

    .line 41123
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->e:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0GT;

    .line 41124
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->f:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0GT;

    .line 41125
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41126
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v6, v2, Lcom/facebook/video/vps/VideoPlayerService;->k:Ljava/util/HashMap;

    monitor-enter v6

    .line 41127
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->k:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/exoplayer/ipc/RendererContext;

    .line 41128
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 41129
    if-eqz v4, :cond_2

    if-eqz v5, :cond_2

    if-eqz v7, :cond_2

    .line 41130
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v3, "Renders already built, using them"

    move-object/from16 v0, p1

    invoke-static {v2, v3, v0}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41131
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const/4 v6, 0x1

    move-object/from16 v3, p1

    invoke-static/range {v2 .. v7}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0GT;LX/0GT;ZLcom/facebook/exoplayer/ipc/RendererContext;)V

    .line 41132
    :goto_1
    return-void

    .line 41133
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 41134
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    .line 41135
    :cond_2
    new-instance v19, LX/0Ka;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1}, LX/0Ka;-><init>(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41136
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->n:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;

    .line 41137
    if-nez v3, :cond_a

    .line 41138
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->h:LX/0H5;

    sget-object v4, LX/0H5;->DASH_LIVE:LX/0H5;

    if-ne v2, v4, :cond_6

    .line 41139
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Building dash live renderers "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v2, v3, v0}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41140
    const/4 v9, 0x0

    .line 41141
    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7}, LX/0KP;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/VideoPlayRequest;)LX/0AY;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v9

    .line 41142
    :goto_2
    new-instance v8, LX/0Kc;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->q:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Kb;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v3}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v8, v2, v3}, LX/0Kc;-><init>(LX/0Kb;Landroid/os/Handler;)V

    .line 41143
    if-eqz v7, :cond_3

    iget-boolean v2, v7, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->j:Z

    if-eqz v2, :cond_3

    const/4 v11, 0x1

    .line 41144
    :goto_3
    new-instance v28, LX/0GR;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->d:Ljava/lang/String;

    if-nez v7, :cond_4

    const/4 v15, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v2}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v17

    new-instance v21, LX/0KV;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1}, LX/0KV;-><init>(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v2}, Lcom/facebook/video/vps/VideoPlayerService;->k(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/net/Uri;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->h:LX/0GE;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v4}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v6, v6, Lcom/facebook/video/vps/VideoPlayerService;->c:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget v10, v10, Lcom/facebook/video/vps/VideoPlayerService;->s:I

    if-nez v7, :cond_5

    const-string v12, "default"

    :goto_5
    move-object/from16 v0, p0

    iget-object v7, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget v13, v7, Lcom/facebook/video/vps/VideoPlayerService;->t:I

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v13}, LX/0GE;->a(Landroid/net/Uri;Landroid/os/Handler;Ljava/lang/String;Landroid/net/Uri;Ljava/util/Map;LX/0Gk;LX/0AY;IZLjava/lang/String;I)LX/0GK;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->h:LX/0GE;

    invoke-virtual {v2}, LX/0GE;->a()LX/0GB;

    move-result-object v25

    move-object/from16 v12, v28

    move-object/from16 v13, v18

    move-object/from16 v18, p3

    move-object/from16 v20, v19

    move-object/from16 v22, v19

    move-object/from16 v26, v8

    move/from16 v27, v11

    invoke-direct/range {v12 .. v27}, LX/0GR;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/os/Handler;Ljava/util/Map;LX/0Ju;LX/0Jr;LX/0KU;LX/0KZ;Landroid/net/Uri;LX/0GK;LX/0GB;LX/0Gk;Z)V

    .line 41145
    new-instance v2, LX/0KT;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    move-object/from16 v0, p1

    invoke-direct {v2, v3, v0}, LX/0KT;-><init>(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, LX/0GR;->a(LX/0GP;)V

    goto/16 :goto_1

    .line 41146
    :catch_0
    move-exception v2

    .line 41147
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Parsing manifest for live failed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 41148
    :cond_3
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 41149
    :cond_4
    iget-object v15, v7, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->d:Ljava/lang/String;

    goto/16 :goto_4

    :cond_5
    iget-object v12, v7, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->e:Ljava/lang/String;

    goto :goto_5

    .line 41150
    :cond_6
    if-eqz p2, :cond_7

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, ".m3u8"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 41151
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v3, "Build HLS renderers"

    move-object/from16 v0, p1

    invoke-static {v2, v3, v0}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41152
    new-instance v2, LX/0Nq;

    invoke-direct {v2}, LX/0Nq;-><init>()V

    .line 41153
    new-instance v3, LX/0Od;

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/0OD;

    const-string v6, "ExoService"

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, LX/0OD;-><init>(Ljava/lang/String;LX/0OH;)V

    invoke-direct {v3, v4, v5, v2}, LX/0Od;-><init>(Ljava/lang/String;LX/0G7;LX/0Aa;)V

    .line 41154
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v2}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v3, v2, v0}, LX/0Od;->a(Landroid/os/Looper;LX/0GJ;)V

    goto/16 :goto_1

    .line 41155
    :cond_7
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    if-eqz v2, :cond_9

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, ".mpd"

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    if-eqz v7, :cond_9

    .line 41156
    :try_start_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7}, LX/0KP;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/VideoPlayRequest;)LX/0AY;

    move-result-object v4

    .line 41157
    const/4 v2, 0x0

    .line 41158
    iget-boolean v5, v7, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->k:Z

    if-eqz v5, :cond_8

    iget-object v5, v7, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->g:Ljava/lang/String;

    if-eqz v5, :cond_8

    .line 41159
    new-instance v2, LX/0K4;

    iget-object v5, v7, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->g:Ljava/lang/String;

    invoke-direct {v2, v5}, LX/0K4;-><init>(Ljava/lang/String;)V

    .line 41160
    :cond_8
    if-eqz v4, :cond_a

    .line 41161
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v3, v0, v4, v2, v1}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0AY;LX/0K4;LX/0Ka;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_1

    .line 41162
    :catch_1
    move-exception v2

    .line 41163
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v4, "MALFORMED"

    move-object/from16 v0, p1

    invoke-static {v3, v4, v2, v0}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Ljava/lang/Throwable;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto/16 :goto_1

    .line 41164
    :cond_9
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    if-eqz v2, :cond_a

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, ".mpd"

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    if-nez v7, :cond_a

    .line 41165
    const-string v2, "Request is gone, fallback to progressive"

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41166
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v4, "Start build progressive renderers"

    move-object/from16 v0, p1

    invoke-static {v2, v4, v0}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41167
    if-eqz v3, :cond_b

    .line 41168
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Building progressive renderer from file data source "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;)V

    .line 41169
    new-instance v4, LX/0OG;

    invoke-direct {v4}, LX/0OG;-><init>()V

    .line 41170
    invoke-static/range {p3 .. p3}, LX/040;->L(Ljava/util/Map;)I

    move-result v2

    move v3, v2

    .line 41171
    :goto_6
    new-instance v2, LX/0MK;

    new-instance v5, LX/0OB;

    invoke-static/range {p3 .. p3}, LX/040;->J(Ljava/util/Map;)I

    move-result v6

    invoke-direct {v5, v6}, LX/0OB;-><init>(I)V

    invoke-static/range {p3 .. p3}, LX/040;->J(Ljava/util/Map;)I

    move-result v6

    mul-int/2addr v6, v3

    const/4 v3, 0x1

    new-array v7, v3, [LX/0ME;

    const/4 v3, 0x0

    new-instance v8, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;

    invoke-direct {v8}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;-><init>()V

    aput-object v8, v7, v3

    move-object/from16 v3, p2

    invoke-direct/range {v2 .. v7}, LX/0MK;-><init>(Landroid/net/Uri;LX/0G6;LX/0O1;I[LX/0ME;)V

    .line 41172
    new-instance v3, LX/0Jw;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    sget-object v6, LX/0L1;->a:LX/0L1;

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v5}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v10

    new-instance v11, LX/0Ka;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    move-object/from16 v0, p1

    invoke-direct {v11, v5, v0}, LX/0Ka;-><init>(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    const/4 v12, -0x1

    move-object v5, v2

    invoke-direct/range {v3 .. v12}, LX/0Jw;-><init>(Landroid/content/Context;LX/0L9;LX/0L1;IJLandroid/os/Handler;LX/0Ju;I)V

    .line 41173
    new-instance v13, LX/0GX;

    sget-object v15, LX/0L1;->a:LX/0L1;

    const/16 v16, 0x0

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v4}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v18

    move-object v14, v2

    invoke-direct/range {v13 .. v19}, LX/0GX;-><init>(LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jr;)V

    .line 41174
    move-object/from16 v0, p0

    iget-object v10, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const/4 v14, 0x0

    new-instance v15, Lcom/facebook/exoplayer/ipc/RendererContext;

    sget-object v2, LX/0HE;->PROGRESSIVE_DOWNLOAD:LX/0HE;

    invoke-virtual {v2}, LX/0HE;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v15, v2, v4, v5, v6}, Lcom/facebook/exoplayer/ipc/RendererContext;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    move-object/from16 v11, p1

    move-object v12, v3

    invoke-static/range {v10 .. v15}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0GT;LX/0GT;ZLcom/facebook/exoplayer/ipc/RendererContext;)V

    goto/16 :goto_1

    .line 41175
    :cond_b
    invoke-static/range {p3 .. p3}, LX/040;->K(Ljava/util/Map;)I

    move-result v10

    .line 41176
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->e:LX/0K9;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v4, v4, Lcom/facebook/video/vps/VideoPlayerService;->c:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->i:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v9}, Lcom/facebook/video/vps/VideoPlayerService;->f(Lcom/facebook/video/vps/VideoPlayerService;)LX/04m;

    move-result-object v9

    invoke-virtual/range {v2 .. v9}, LX/0K9;->a(Ljava/lang/String;Landroid/net/Uri;IZZLjava/util/Map;LX/04m;)LX/0G6;

    move-result-object v4

    move v3, v10

    goto/16 :goto_6
.end method

.method private a(Ljava/lang/String;Ljava/lang/RuntimeException;)V
    .locals 4

    .prologue
    const/16 v3, 0x3e8

    .line 41177
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 41178
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->q:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Kb;

    .line 41179
    if-eqz v0, :cond_1

    .line 41180
    const/4 p0, 0x0

    .line 41181
    sget-object v2, LX/040;->ak:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 41182
    sget-object v2, LX/040;->ak:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    .line 41183
    :goto_0
    move v1, v2

    .line 41184
    if-eqz v1, :cond_1

    .line 41185
    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    .line 41186
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v3, :cond_0

    .line 41187
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 41188
    :cond_0
    invoke-virtual {p2}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v2, v1}, LX/0Kb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 41189
    :cond_1
    return-void

    :cond_2
    move v2, p0

    .line 41190
    goto :goto_0

    :cond_3
    move v2, p0

    .line 41191
    goto :goto_0
.end method

.method private b(Lcom/facebook/exoplayer/ipc/VideoPlayRequest;)Lcom/facebook/exoplayer/ipc/VideoPlayerSession;
    .locals 10

    .prologue
    .line 41192
    new-instance v5, Lcom/facebook/exoplayer/ipc/MediaRenderer;

    iget-object v0, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->d:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v0, v1, v2}, Lcom/facebook/exoplayer/ipc/MediaRenderer;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 41193
    new-instance v6, Lcom/facebook/exoplayer/ipc/MediaRenderer;

    iget-object v0, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->d:Ljava/lang/String;

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v0, v1, v2}, Lcom/facebook/exoplayer/ipc/MediaRenderer;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 41194
    new-instance v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v1, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->a:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->b:Landroid/net/Uri;

    iget-object v4, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->c:Ljava/lang/String;

    iget-object v7, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->f:Landroid/net/Uri;

    iget-object v8, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->i:LX/0H5;

    iget-object v9, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->l:Ljava/util/Map;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/MediaRenderer;Landroid/net/Uri;LX/0H5;Ljava/util/Map;)V

    .line 41195
    iget-object v1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v1, Lcom/facebook/video/vps/VideoPlayerService;->n:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41196
    return-object v0
.end method

.method private b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;J)V
    .locals 6

    .prologue
    .line 41065
    if-nez p1, :cond_0

    .line 41066
    :goto_0
    return-void

    .line 41067
    :cond_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v1

    .line 41068
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 41069
    invoke-static {v0}, LX/040;->Z(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->h:LX/0H5;

    invoke-static {v0}, LX/0H5;->isLive(LX/0H5;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 41070
    :goto_1
    iget-object v2, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "seekTo "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", useRelativePos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41071
    if-eqz v1, :cond_3

    .line 41072
    if-eqz v0, :cond_2

    .line 41073
    invoke-interface {v1, p2, p3}, LX/0Kx;->b(J)V

    goto :goto_0

    .line 41074
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 41075
    :cond_2
    invoke-interface {v1, p2, p3}, LX/0Kx;->a(J)V

    goto :goto_0

    .line 41076
    :cond_3
    const-string v0, "error seekTo"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0
.end method

.method private b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;F)V
    .locals 4

    .prologue
    .line 41197
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p2}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/MediaRenderer;)LX/0GT;

    move-result-object v0

    .line 41198
    iget-object v1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v1

    .line 41199
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    instance-of v2, v0, LX/0GX;

    if-nez v2, :cond_0

    instance-of v2, v0, LX/0Kj;

    if-nez v2, :cond_0

    instance-of v2, v0, LX/0Kh;

    if-eqz v2, :cond_1

    .line 41200
    :cond_0
    const/4 v2, 0x1

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, LX/0Kx;->a(LX/0GS;ILjava/lang/Object;)V

    .line 41201
    :goto_0
    return-void

    .line 41202
    :cond_1
    const-string v0, "error set volume"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0
.end method

.method private b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Landroid/view/Surface;)V
    .locals 5

    .prologue
    .line 41203
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p2}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/MediaRenderer;)LX/0GT;

    move-result-object v0

    .line 41204
    iget-object v1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v1

    .line 41205
    if-eqz v0, :cond_0

    instance-of v2, v0, LX/0Jw;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 41206
    iget-object v2, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "set surface "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for renderer "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41207
    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p3}, LX/0Kx;->b(LX/0GS;ILjava/lang/Object;)V

    .line 41208
    :goto_0
    return-void

    .line 41209
    :cond_0
    const-string v0, "error set surface"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0
.end method

.method private d(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Z)V
    .locals 3

    .prologue
    .line 41210
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v0

    .line 41211
    if-eqz v0, :cond_2

    .line 41212
    invoke-interface {v0, p2}, LX/0Kx;->a(Z)V

    .line 41213
    if-eqz p2, :cond_1

    .line 41214
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    if-eqz v0, :cond_0

    .line 41215
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->x:Landroid/util/LruCache;

    iget-object v1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v1, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v2, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v0, v1, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41216
    :cond_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    .line 41217
    iput-object p1, v0, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 41218
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->x:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41219
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->n(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41220
    :cond_1
    :goto_0
    return-void

    .line 41221
    :cond_2
    const-string v0, "error setPlayWhenReady"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0
.end method

.method private k(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)I
    .locals 1

    .prologue
    .line 41222
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v0

    .line 41223
    if-eqz v0, :cond_0

    .line 41224
    invoke-interface {v0}, LX/0Kx;->a()I

    move-result v0

    .line 41225
    :goto_0
    return v0

    .line 41226
    :cond_0
    const-string v0, "error getPlaybackState"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41227
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private l(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Z
    .locals 1

    .prologue
    .line 41228
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v0

    .line 41229
    if-eqz v0, :cond_0

    .line 41230
    invoke-interface {v0}, LX/0Kx;->b()Z

    move-result v0

    .line 41231
    :goto_0
    return v0

    .line 41232
    :cond_0
    const-string v0, "error getPlayWhenReady"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41233
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Z
    .locals 2

    .prologue
    .line 41234
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 41235
    :try_start_0
    invoke-direct {p0, p1}, LX/0KP;->n(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 41236
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private n(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Z
    .locals 1

    .prologue
    .line 41077
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v0

    .line 41078
    if-eqz v0, :cond_0

    .line 41079
    invoke-interface {v0}, LX/0Kx;->c()V

    .line 41080
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->p(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41081
    const/4 v0, 0x1

    .line 41082
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 3

    .prologue
    .line 41237
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    .line 41238
    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41239
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->n(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41240
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ToBeReleased VideoPlayerSessions after remove, size is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v2}, Lcom/facebook/video/vps/VideoPlayerService;->j(Lcom/facebook/video/vps/VideoPlayerService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41241
    return-void
.end method

.method private p(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)I
    .locals 1

    .prologue
    .line 41304
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v0

    .line 41305
    if-eqz v0, :cond_0

    .line 41306
    invoke-interface {v0}, LX/0Kx;->i()I

    move-result v0

    .line 41307
    :goto_0
    return v0

    .line 41308
    :cond_0
    const-string v0, "no available player to getBufferedPercentage"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41309
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private q(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J
    .locals 2

    .prologue
    .line 41353
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v0

    .line 41354
    if-eqz v0, :cond_0

    .line 41355
    invoke-interface {v0}, LX/0Kx;->h()J

    move-result-wide v0

    .line 41356
    :goto_0
    return-wide v0

    .line 41357
    :cond_0
    const-string v0, "no available player to getBufferedPositionMs"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41358
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private r(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J
    .locals 2

    .prologue
    .line 41347
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v0

    .line 41348
    if-eqz v0, :cond_0

    .line 41349
    invoke-interface {v0}, LX/0Kx;->e()J

    move-result-wide v0

    .line 41350
    :goto_0
    return-wide v0

    .line 41351
    :cond_0
    const-string v0, "no available player to getDurationUs"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41352
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private s(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 1

    .prologue
    .line 41345
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41346
    return-void
.end method

.method private t(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 6

    .prologue
    .line 41333
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v2

    .line 41334
    if-nez v2, :cond_0

    .line 41335
    :goto_0
    return-void

    .line 41336
    :cond_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v3, v0, Lcom/facebook/video/vps/VideoPlayerService;->v:Ljava/util/HashMap;

    monitor-enter v3

    .line 41337
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->v:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 41338
    if-eqz v0, :cond_2

    .line 41339
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 41340
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 41341
    invoke-interface {v2, v5, v1}, LX/0Kx;->b(II)V

    goto :goto_1

    .line 41342
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 41343
    :cond_1
    :try_start_1
    invoke-interface {v2}, LX/0Kx;->f()J

    move-result-wide v0

    invoke-interface {v2, v0, v1}, LX/0Kx;->a(J)V

    .line 41344
    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private u(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 6

    .prologue
    .line 41316
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v1

    .line 41317
    if-nez v1, :cond_0

    .line 41318
    :goto_0
    return-void

    .line 41319
    :cond_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v0, Lcom/facebook/video/vps/VideoPlayerService;->v:Ljava/util/HashMap;

    monitor-enter v2

    .line 41320
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->v:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 41321
    if-eqz v0, :cond_1

    .line 41322
    monitor-exit v2

    goto :goto_0

    .line 41323
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 41324
    :cond_1
    :try_start_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 41325
    const/4 v0, 0x0

    :goto_1
    const/4 v4, 0x2

    if-ge v0, v4, :cond_3

    .line 41326
    invoke-interface {v1, v0}, LX/0Kx;->a(I)I

    move-result v4

    .line 41327
    if-ltz v4, :cond_2

    invoke-interface {v1, v0, v4}, LX/0Kx;->a(II)LX/0L4;

    move-result-object v5

    iget-object v5, v5, LX/0L4;->b:Ljava/lang/String;

    invoke-static {v5}, LX/0Al;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 41328
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41329
    const/4 v4, -0x1

    invoke-interface {v1, v0, v4}, LX/0Kx;->b(II)V

    .line 41330
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 41331
    :cond_3
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->v:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41332
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)I
    .locals 2

    .prologue
    .line 41310
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41311
    :try_start_1
    invoke-direct {p0, p1}, LX/0KP;->k(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 41312
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 41313
    :catch_0
    move-exception v0

    .line 41314
    const-string v1, "getPlaybackState"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41315
    throw v0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)J
    .locals 18

    .prologue
    .line 41242
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v6, v2, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 41243
    :try_start_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".mpd"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 41244
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Preparing dash live chunk source for: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;)V

    .line 41245
    invoke-static {v6}, LX/040;->X(Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 41246
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->h:LX/0GE;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v3}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v5, v3, Lcom/facebook/video/vps/VideoPlayerService;->c:Landroid/net/Uri;

    new-instance v7, LX/0Kc;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v3, v3, Lcom/facebook/video/vps/VideoPlayerService;->q:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Kb;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v8}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v8

    invoke-direct {v7, v3, v8}, LX/0Kc;-><init>(LX/0Kb;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget v8, v3, Lcom/facebook/video/vps/VideoPlayerService;->s:I

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget v9, v3, Lcom/facebook/video/vps/VideoPlayerService;->t:I

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v9}, LX/0GE;->a(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;Landroid/os/Handler;Landroid/net/Uri;Ljava/util/Map;LX/0Gk;II)V

    .line 41247
    :goto_0
    const-wide/16 v2, 0x0

    .line 41248
    :goto_1
    return-wide v2

    .line 41249
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v7, v2, Lcom/facebook/video/vps/VideoPlayerService;->h:LX/0GE;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v2}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v9

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v11, v2, Lcom/facebook/video/vps/VideoPlayerService;->c:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->e:Ljava/lang/String;

    new-instance v14, LX/0Kc;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->q:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Kb;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v3}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v14, v2, v3}, LX/0Kc;-><init>(LX/0Kb;Landroid/os/Handler;)V

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v2}, Lcom/facebook/video/vps/VideoPlayerService;->l(Lcom/facebook/video/vps/VideoPlayerService;)I

    move-result v16

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v2}, Lcom/facebook/video/vps/VideoPlayerService;->m(Lcom/facebook/video/vps/VideoPlayerService;)I

    move-result v17

    move-object v13, v6

    invoke-virtual/range {v7 .. v17}, LX/0GE;->a(Landroid/net/Uri;Landroid/os/Handler;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;LX/0Gk;Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 41250
    :catch_0
    move-exception v2

    .line 41251
    const-string v3, "prefetch"

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41252
    throw v2

    .line 41253
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->d:LX/043;

    iget-boolean v2, v2, LX/043;->d:Z

    if-eqz v2, :cond_2

    .line 41254
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->e:LX/0K9;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v3, v3, Lcom/facebook/video/vps/VideoPlayerService;->c:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v4}, Lcom/facebook/video/vps/VideoPlayerService;->f(Lcom/facebook/video/vps/VideoPlayerService;)LX/04m;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v3, v4}, LX/0K9;->a(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;Landroid/net/Uri;LX/04m;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v2

    goto :goto_1

    .line 41255
    :cond_2
    const-wide/16 v2, -0x1

    goto/16 :goto_1

    .line 41256
    :catch_1
    move-exception v2

    .line 41257
    :try_start_2
    sget-object v3, Lcom/facebook/video/vps/VideoPlayerService;->a:Ljava/lang/String;

    const-string v4, "Exception in prefetch "

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 41258
    const-wide/16 v2, -0x1

    goto/16 :goto_1
.end method

.method public final a()Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;
    .locals 4

    .prologue
    .line 41294
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->e:LX/0K9;

    invoke-virtual {v0}, LX/0K9;->d()LX/0KJ;

    move-result-object v0

    .line 41295
    new-instance v1, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;

    invoke-direct {v1}, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;-><init>()V

    .line 41296
    iget-wide v2, v0, LX/0KJ;->a:J

    iput-wide v2, v1, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->a:J

    .line 41297
    iget-wide v2, v0, LX/0KJ;->b:J

    iput-wide v2, v1, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->b:J

    .line 41298
    iget-wide v2, v0, LX/0KJ;->d:J

    iput-wide v2, v1, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->d:J

    .line 41299
    iget-wide v2, v0, LX/0KJ;->c:J

    iput-wide v2, v1, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->c:J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41300
    return-object v1

    .line 41301
    :catch_0
    move-exception v0

    .line 41302
    const-string v1, "getPerformanceMetrics"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41303
    throw v0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/exoplayer/ipc/VideoCacheStatus;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 41286
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->d:LX/043;

    iget-boolean v0, v0, LX/043;->d:Z

    if-nez v0, :cond_0

    .line 41287
    new-instance v0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;

    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;-><init>(ZJJ)V

    .line 41288
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->e:LX/0K9;

    invoke-virtual {v0, p1}, LX/0K9;->b(Ljava/lang/String;)Lcom/facebook/exoplayer/ipc/VideoCacheStatus;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 41289
    :catch_0
    move-exception v0

    .line 41290
    new-instance v1, Landroid/os/RemoteException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 41291
    :catch_1
    move-exception v0

    .line 41292
    const-string v1, "getVideoCacheStatus"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41293
    throw v0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayRequest;)Lcom/facebook/exoplayer/ipc/VideoPlayerSession;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 41278
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->d:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v2, 0x1

    iget-object v3, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->a:Landroid/net/Uri;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->f:Landroid/net/Uri;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->g:Ljava/lang/String;

    if-nez v3, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x4

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->h:Landroid/os/ParcelFileDescriptor;

    aput-object v2, v1, v0

    .line 41279
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41280
    :try_start_1
    invoke-direct {p0, p1}, LX/0KP;->b(Lcom/facebook/exoplayer/ipc/VideoPlayRequest;)Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0

    .line 41281
    :cond_0
    iget-object v0, p1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 41282
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0

    .line 41283
    :catch_0
    move-exception v0

    .line 41284
    const-string v1, "registerSession"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41285
    throw v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 41271
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setPrefetchConcurrency "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41272
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->f:LX/0Gh;

    .line 41273
    iput p1, v0, LX/0Gh;->e:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41274
    return-void

    .line 41275
    :catch_0
    move-exception v0

    .line 41276
    const-string v1, "setPrefetchConcurrency"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41277
    throw v0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 41259
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setDashLiveParameters "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41260
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->b:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41261
    :try_start_1
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    .line 41262
    iput p1, v0, Lcom/facebook/video/vps/VideoPlayerService;->s:I

    .line 41263
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    .line 41264
    iput p2, v0, Lcom/facebook/video/vps/VideoPlayerService;->t:I

    .line 41265
    monitor-exit v1

    .line 41266
    return-void

    .line 41267
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 41268
    :catch_0
    move-exception v0

    .line 41269
    const-string v1, "setDashLiveParameters"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41270
    throw v0
.end method

.method public final a(LX/042;)V
    .locals 3

    .prologue
    .line 41083
    if-nez p1, :cond_1

    .line 41084
    :cond_0
    :goto_0
    return-void

    .line 41085
    :cond_1
    :try_start_0
    invoke-static {p1}, Lcom/facebook/video/vps/VideoPlayerService;->b(LX/042;)LX/0Kb;

    move-result-object v0

    .line 41086
    iget-object v1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v1, Lcom/facebook/video/vps/VideoPlayerService;->q:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41087
    goto :goto_0

    .line 41088
    :catch_0
    move-exception v0

    .line 41089
    const-string v1, "setNonPlayerSessionListener"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41090
    throw v0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 40970
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "setVideoServerBaseUri is "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40971
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    .line 40972
    iput-object p1, v0, Lcom/facebook/video/vps/VideoPlayerService;->c:Landroid/net/Uri;

    .line 40973
    return-void

    .line 40974
    :cond_0
    const-string v0, "null"

    goto :goto_0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;J)V
    .locals 2

    .prologue
    .line 40962
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40963
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, LX/0KP;->b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;J)V

    .line 40964
    monitor-exit v1

    .line 40965
    return-void

    .line 40966
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 40967
    :catch_0
    move-exception v0

    .line 40968
    const-string v1, "seekTo"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40969
    throw v0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/042;)V
    .locals 3

    .prologue
    .line 40952
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "addListener"

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40953
    :try_start_0
    new-instance v0, LX/0Kb;

    invoke-direct {v0, p2}, LX/0Kb;-><init>(LX/042;)V

    .line 40954
    iget-object v1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v1, Lcom/facebook/video/vps/VideoPlayerService;->i:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40955
    :try_start_1
    iget-object v2, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->i:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40956
    monitor-exit v1

    .line 40957
    return-void

    .line 40958
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 40959
    :catch_0
    move-exception v0

    .line 40960
    const-string v1, "addListener"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40961
    throw v0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 40942
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "buildRenderers"

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40943
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "buildRenderers Uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40944
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40945
    :try_start_1
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    invoke-direct {p0, p1, p2, v0}, LX/0KP;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Landroid/net/Uri;Ljava/util/Map;)V

    .line 40946
    monitor-exit v1

    .line 40947
    return-void

    .line 40948
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 40949
    :catch_0
    move-exception v0

    .line 40950
    const-string v1, "buildRenderers"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40951
    throw v0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;F)V
    .locals 3

    .prologue
    .line 40932
    if-nez p2, :cond_0

    .line 40933
    const-string v0, "audio renderer is null"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40934
    :goto_0
    return-void

    .line 40935
    :cond_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setVolume "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for renderer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/exoplayer/ipc/MediaRenderer;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40936
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40937
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, LX/0KP;->b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;F)V

    .line 40938
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 40939
    :catch_0
    move-exception v0

    .line 40940
    const-string v1, "setVolume"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40941
    throw v0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Landroid/view/Surface;)V
    .locals 3

    .prologue
    .line 40922
    if-nez p2, :cond_0

    .line 40923
    const-string v0, "video renderer is null"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40924
    :goto_0
    return-void

    .line 40925
    :cond_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setSurface "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for renderer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40926
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40927
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, LX/0KP;->b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Landroid/view/Surface;)V

    .line 40928
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 40929
    :catch_0
    move-exception v0

    .line 40930
    const-string v1, "setSurface"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40931
    throw v0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;)V
    .locals 3

    .prologue
    .line 40913
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 40914
    :cond_0
    :goto_0
    return-void

    .line 40915
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p2}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/MediaRenderer;)LX/0GT;

    move-result-object v0

    .line 40916
    iget-object v1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v1

    .line 40917
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    instance-of v2, v0, LX/0Kj;

    if-nez v2, :cond_2

    instance-of v2, v0, LX/0Kh;

    if-eqz v2, :cond_0

    .line 40918
    :cond_2
    const/4 v2, 0x2

    invoke-interface {v1, v0, v2, p3}, LX/0Kx;->a(LX/0GS;ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 40919
    :catch_0
    move-exception v0

    .line 40920
    const-string v1, "setDeviceOrientationFrame"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40921
    throw v0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/MediaRenderer;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 40903
    :try_start_0
    iget-object v2, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v2}, Lcom/facebook/video/vps/VideoPlayerService;->q(Lcom/facebook/video/vps/VideoPlayerService;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 40904
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v2, "is videoRenderer null? "

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p2, :cond_1

    move v2, v0

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " hashcode is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/facebook/video/vps/VideoPlayerService;->d(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40905
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "is audioRenderer null? "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p3, :cond_2

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " hashcode is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->d(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40906
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Size of the renderers map? "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v1}, Lcom/facebook/video/vps/VideoPlayerService;->r(Lcom/facebook/video/vps/VideoPlayerService;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->d(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40907
    :cond_0
    return-void

    :cond_1
    move v2, v1

    .line 40908
    goto :goto_0

    :cond_2
    move v0, v1

    .line 40909
    goto :goto_1

    .line 40910
    :catch_0
    move-exception v0

    .line 40911
    const-string v1, "prepareRenderers"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40912
    throw v0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;)V
    .locals 3

    .prologue
    .line 40894
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 40895
    :cond_0
    :goto_0
    return-void

    .line 40896
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p2}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/MediaRenderer;)LX/0GT;

    move-result-object v0

    .line 40897
    iget-object v1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v1

    .line 40898
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    instance-of v2, v0, LX/0Kj;

    if-nez v2, :cond_2

    instance-of v2, v0, LX/0Kh;

    if-eqz v2, :cond_0

    .line 40899
    :cond_2
    const/4 v2, 0x3

    invoke-interface {v1, v0, v2, p3}, LX/0Kx;->a(LX/0GS;ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 40900
    :catch_0
    move-exception v0

    .line 40901
    const-string v1, "setSpatialAudioFocus"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40902
    throw v0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Z)V
    .locals 3

    .prologue
    .line 40881
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setPlayWhenReady to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40882
    if-eqz p2, :cond_0

    .line 40883
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->f:LX/0Gh;

    .line 40884
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0Gh;->i:Z

    .line 40885
    :goto_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40886
    :try_start_1
    invoke-direct {p0, p1, p2}, LX/0KP;->d(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Z)V

    .line 40887
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 40888
    return-void

    .line 40889
    :cond_0
    :try_start_2
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->f:LX/0Gh;

    invoke-virtual {v0}, LX/0Gh;->b()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 40890
    :catch_0
    move-exception v0

    .line 40891
    const-string v1, "setPlayWhenReady"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40892
    throw v0

    .line 40893
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 40874
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40875
    :try_start_1
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 40876
    monitor-exit v1

    return-void

    .line 40877
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 40878
    :catch_0
    move-exception v0

    .line 40879
    const-string v1, "setExperimentationConfigs"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40880
    throw v0
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    .line 40856
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onPlayerActivityStateChange "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 40857
    if-eqz p1, :cond_1

    .line 40858
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const/4 v1, 0x1

    .line 40859
    iput-boolean v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->u:Z

    .line 40860
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 40861
    sget-object v1, LX/040;->ae:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 40862
    sget-object v1, LX/040;->ae:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 40863
    :goto_0
    move v0, v1

    .line 40864
    if-lez v0, :cond_0

    .line 40865
    iget-object v1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v1}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->w:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 40866
    iget-object v1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v1}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v2, v2, Lcom/facebook/video/vps/VideoPlayerService;->w:Ljava/lang/Runnable;

    int-to-long v4, v0

    const v0, -0x29643ac3

    invoke-static {v1, v2, v4, v5, v0}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 40867
    :cond_0
    :goto_1
    return-void

    .line 40868
    :cond_1
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const/4 v1, 0x0

    .line 40869
    iput-boolean v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->u:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40870
    goto :goto_1

    .line 40871
    :catch_0
    move-exception v0

    .line 40872
    const-string v1, "onPlayerActivityStateChange"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40873
    throw v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 40848
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->g:LX/0KC;

    .line 40849
    invoke-static {v0}, LX/0KC;->c(LX/0KC;)I

    move-result v1

    if-nez v1, :cond_1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40850
    :cond_0
    return-void

    .line 40851
    :catch_0
    move-exception v0

    .line 40852
    const-string v1, "notifyConnectivityChange"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40853
    throw v0

    .line 40854
    :cond_1
    invoke-static {v0}, LX/0KC;->b(LX/0KC;)Ljava/util/HashSet;

    move-result-object v1

    .line 40855
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/042;)V
    .locals 2

    .prologue
    .line 40839
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "removeListener"

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40840
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40841
    :try_start_1
    invoke-direct {p0, p1}, LX/0KP;->s(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40842
    monitor-exit v1

    .line 40843
    return-void

    .line 40844
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 40845
    :catch_0
    move-exception v0

    .line 40846
    const-string v1, "removeListener"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40847
    throw v0
.end method

.method public final b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Z)V
    .locals 2

    .prologue
    .line 40975
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0}, Lcom/facebook/video/vps/VideoPlayerService;->q(Lcom/facebook/video/vps/VideoPlayerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40976
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "release: releaseRightAway: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->d(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40977
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ToBeReleased VideoPlayerSessions before remove, size is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v1}, Lcom/facebook/video/vps/VideoPlayerService;->j(Lcom/facebook/video/vps/VideoPlayerService;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->d(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40978
    :cond_0
    if-eqz p2, :cond_1

    .line 40979
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40980
    :try_start_1
    invoke-direct {p0, p1}, LX/0KP;->o(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 40981
    monitor-exit v1

    .line 40982
    :cond_1
    return-void

    .line 40983
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 40984
    :catch_0
    move-exception v0

    .line 40985
    const-string v1, "release"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40986
    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 40987
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->f:LX/0Gh;

    invoke-virtual {v0, p1}, LX/0Gh;->b(Ljava/lang/String;)V

    .line 40988
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->h:LX/0GE;

    .line 40989
    iget-object v1, v0, LX/0GE;->d:LX/0GN;

    move-object v0, v1

    .line 40990
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    .line 40991
    iget-object v1, v0, LX/0GN;->g:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40992
    return-void

    .line 40993
    :catch_0
    move-exception v0

    .line 40994
    const-string v1, "enablePrefetchForOrigin"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 40995
    throw v0
.end method

.method public final b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Z
    .locals 2

    .prologue
    .line 40996
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40997
    :try_start_1
    invoke-direct {p0, p1}, LX/0KP;->l(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 40998
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 40999
    :catch_0
    move-exception v0

    .line 41000
    const-string v1, "getPlayWhenReady"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41001
    throw v0
.end method

.method public final c(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Z)J
    .locals 2

    .prologue
    .line 41002
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1, p2}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZZ)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    .line 41003
    :catch_0
    move-exception v0

    .line 41004
    const-string v1, "getInternalCurrentPositionMs"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41005
    throw v0
.end method

.method public final c(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 2

    .prologue
    .line 41006
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "stop"

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41007
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 41008
    invoke-static {v0}, LX/040;->W(Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 41009
    invoke-direct {p0, p1}, LX/0KP;->m(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41010
    :cond_0
    :goto_0
    return-void

    .line 41011
    :cond_1
    const-string v0, "error stop"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 41012
    :catch_0
    move-exception v0

    .line 41013
    const-string v1, "stop"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41014
    throw v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 41015
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->f:LX/0Gh;

    invoke-virtual {v0, p1}, LX/0Gh;->a(Ljava/lang/String;)V

    .line 41016
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->h:LX/0GE;

    .line 41017
    iget-object v1, v0, LX/0GE;->d:LX/0GN;

    move-object v0, v1

    .line 41018
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    .line 41019
    iget-object v1, v0, LX/0GN;->g:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41020
    return-void

    .line 41021
    :catch_0
    move-exception v0

    .line 41022
    const-string v1, "disablePrefetchForOrigin"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41023
    throw v0
.end method

.method public final d(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;
    .locals 2

    .prologue
    .line 41024
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->h(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 41025
    :catch_0
    move-exception v0

    .line 41026
    const-string v1, "getVideoPlayerStreamMetadata"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41027
    throw v0
.end method

.method public final e(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J
    .locals 4

    .prologue
    .line 41028
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "getDurationUs"

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41029
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41030
    :try_start_1
    invoke-direct {p0, p1}, LX/0KP;->r(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 41031
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 41032
    :catch_0
    move-exception v0

    .line 41033
    const-string v1, "getDurationUs"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41034
    throw v0
.end method

.method public final f(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J
    .locals 2

    .prologue
    .line 41035
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->b(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    .line 41036
    :catch_0
    move-exception v0

    .line 41037
    const-string v1, "getCurrentPositionMs"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41038
    throw v0
.end method

.method public final g(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J
    .locals 4

    .prologue
    .line 41039
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "getBufferedPositionMs"

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41040
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41041
    :try_start_1
    invoke-direct {p0, p1}, LX/0KP;->q(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 41042
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 41043
    :catch_0
    move-exception v0

    .line 41044
    const-string v1, "getBufferedPositionMs"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41045
    throw v0
.end method

.method public final h(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)I
    .locals 2

    .prologue
    .line 41046
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "getBufferedPercentage"

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41047
    :try_start_0
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41048
    :try_start_1
    invoke-direct {p0, p1}, LX/0KP;->p(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 41049
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 41050
    :catch_0
    move-exception v0

    .line 41051
    const-string v1, "getBufferedPercentage"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41052
    throw v0
.end method

.method public final i(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 2

    .prologue
    .line 41053
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "turnOnVideoStream"

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41054
    :try_start_0
    invoke-direct {p0, p1}, LX/0KP;->t(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41055
    return-void

    .line 41056
    :catch_0
    move-exception v0

    .line 41057
    const-string v1, "turnOnVideoStream"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41058
    throw v0
.end method

.method public final j(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 2

    .prologue
    .line 41059
    iget-object v0, p0, LX/0KP;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "turnOffVideoStream"

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41060
    :try_start_0
    invoke-direct {p0, p1}, LX/0KP;->u(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41061
    return-void

    .line 41062
    :catch_0
    move-exception v0

    .line 41063
    const-string v1, "turnOffVideoStream"

    invoke-direct {p0, v1, v0}, LX/0KP;->a(Ljava/lang/String;Ljava/lang/RuntimeException;)V

    .line 41064
    throw v0
.end method
