.class public final LX/0Ii;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:LX/0Ij;


# direct methods
.method public constructor <init>(LX/0Ij;)V
    .locals 0

    .prologue
    .line 38885
    iput-object p1, p0, LX/0Ii;->a:LX/0Ij;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/16 v0, 0x26

    const v1, -0x41b53019

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 38886
    invoke-virtual {p0}, LX/0Ii;->getResultCode()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 38887
    invoke-virtual {p0, v8}, LX/0Ii;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v1

    .line 38888
    const-string v2, "/settings/mqtt/id/mqtt_device_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 38889
    const-string v3, "/settings/mqtt/id/mqtt_device_secret"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 38890
    const-string v4, "/settings/mqtt/id/timestamp"

    const-wide v6, 0x7fffffffffffffffL

    invoke-virtual {v1, v4, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 38891
    new-instance v1, LX/06y;

    invoke-direct {v1, v2, v3, v4, v5}, LX/06y;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 38892
    sget-object v2, LX/0Ij;->a:Ljava/lang/String;

    const-string v3, "New ids from sharing: %s"

    new-array v4, v8, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38893
    iget-object v2, p0, LX/0Ii;->a:LX/0Ij;

    invoke-static {v2, v1}, LX/0Ij;->b(LX/0Ij;LX/06y;)V

    .line 38894
    :cond_0
    const/16 v1, 0x27

    const v2, 0x26ff82da

    invoke-static {p2, v9, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void
.end method
