.class public LX/0I5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/05L;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/DataOutputStream;LX/07V;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 38309
    iget-object v0, p2, LX/07W;->a:LX/07R;

    move-object v5, v0

    .line 38310
    invoke-virtual {p2}, LX/07V;->a()LX/07U;

    move-result-object v6

    .line 38311
    invoke-virtual {p2}, LX/07V;->b()LX/079;

    move-result-object v4

    .line 38312
    iget-object v0, v4, LX/079;->a:Ljava/lang/String;

    .line 38313
    invoke-static {v0}, LX/07Z;->a(Ljava/lang/String;)[B

    move-result-object v7

    .line 38314
    array-length v0, v7

    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v2, v0, 0x0

    .line 38315
    iget-object v0, v4, LX/079;->b:Ljava/lang/String;

    .line 38316
    if-eqz v0, :cond_6

    invoke-static {v0}, LX/07Z;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 38317
    :goto_0
    iget-object v1, v4, LX/079;->c:Ljava/lang/String;

    .line 38318
    if-eqz v1, :cond_7

    invoke-static {v1}, LX/07Z;->a(Ljava/lang/String;)[B

    move-result-object v1

    .line 38319
    :goto_1
    iget-boolean v3, v6, LX/07U;->d:Z

    if-eqz v3, :cond_0

    .line 38320
    array-length v3, v0

    add-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 38321
    array-length v3, v1

    add-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 38322
    :cond_0
    iget-object v3, v4, LX/079;->d:LX/078;

    if-eqz v3, :cond_8

    iget-object v3, v4, LX/079;->d:LX/078;

    invoke-virtual {v3}, LX/078;->a()Ljava/lang/String;

    move-result-object v3

    .line 38323
    :goto_2
    if-eqz v3, :cond_9

    invoke-static {v3}, LX/07Z;->a(Ljava/lang/String;)[B

    move-result-object v3

    .line 38324
    :goto_3
    iget-boolean v8, v6, LX/07U;->b:Z

    if-eqz v8, :cond_1

    .line 38325
    array-length v8, v3

    add-int/lit8 v8, v8, 0x2

    add-int/2addr v2, v8

    .line 38326
    :cond_1
    iget-object v4, v4, LX/079;->e:Ljava/lang/String;

    .line 38327
    if-eqz v4, :cond_a

    invoke-static {v4}, LX/07Z;->a(Ljava/lang/String;)[B

    move-result-object v4

    .line 38328
    :goto_4
    iget-boolean v8, v6, LX/07U;->c:Z

    if-eqz v8, :cond_2

    .line 38329
    array-length v8, v4

    add-int/lit8 v8, v8, 0x2

    add-int/2addr v2, v8

    .line 38330
    :cond_2
    add-int/lit8 v2, v2, 0xc

    .line 38331
    invoke-static {v5}, LX/07Z;->a(LX/07R;)I

    move-result v5

    invoke-virtual {p1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 38332
    invoke-static {p1, v2}, LX/07Z;->a(Ljava/io/DataOutputStream;I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    .line 38333
    invoke-virtual {p1, v9}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 38334
    const/4 v8, 0x6

    invoke-virtual {p1, v8}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 38335
    const/16 v8, 0x4d

    invoke-virtual {p1, v8}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 38336
    const/16 v8, 0x51

    invoke-virtual {p1, v8}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 38337
    const/16 v8, 0x49

    invoke-virtual {p1, v8}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 38338
    const/16 v8, 0x73

    invoke-virtual {p1, v8}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 38339
    const/16 v8, 0x64

    invoke-virtual {p1, v8}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 38340
    const/16 v8, 0x70

    invoke-virtual {p1, v8}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 38341
    iget v8, v6, LX/07U;->a:I

    invoke-virtual {p1, v8}, Ljava/io/DataOutputStream;->write(I)V

    .line 38342
    invoke-static {v6}, LX/07Z;->a(LX/07U;)I

    move-result v8

    invoke-virtual {p1, v8}, Ljava/io/DataOutputStream;->write(I)V

    .line 38343
    iget v8, v6, LX/07U;->h:I

    invoke-virtual {p1, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 38344
    array-length v8, v7

    invoke-virtual {p1, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 38345
    array-length v8, v7

    invoke-virtual {p1, v7, v9, v8}, Ljava/io/DataOutputStream;->write([BII)V

    .line 38346
    iget-boolean v7, v6, LX/07U;->d:Z

    if-eqz v7, :cond_3

    .line 38347
    array-length v7, v0

    invoke-virtual {p1, v7}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 38348
    array-length v7, v0

    invoke-virtual {p1, v0, v9, v7}, Ljava/io/DataOutputStream;->write([BII)V

    .line 38349
    array-length v0, v1

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 38350
    array-length v0, v1

    invoke-virtual {p1, v1, v9, v0}, Ljava/io/DataOutputStream;->write([BII)V

    .line 38351
    :cond_3
    iget-boolean v0, v6, LX/07U;->b:Z

    if-eqz v0, :cond_4

    .line 38352
    array-length v0, v3

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 38353
    array-length v0, v3

    invoke-virtual {p1, v3, v9, v0}, Ljava/io/DataOutputStream;->write([BII)V

    .line 38354
    :cond_4
    iget-boolean v0, v6, LX/07U;->c:Z

    if-eqz v0, :cond_5

    .line 38355
    array-length v0, v4

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 38356
    array-length v0, v4

    invoke-virtual {p1, v4, v9, v0}, Ljava/io/DataOutputStream;->write([BII)V

    .line 38357
    :cond_5
    invoke-virtual {p1}, Ljava/io/DataOutputStream;->flush()V

    .line 38358
    add-int v0, v5, v2

    .line 38359
    return v0

    .line 38360
    :cond_6
    new-array v0, v9, [B

    goto/16 :goto_0

    .line 38361
    :cond_7
    new-array v1, v9, [B

    goto/16 :goto_1

    .line 38362
    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 38363
    :cond_9
    new-array v3, v9, [B

    goto/16 :goto_3

    .line 38364
    :cond_a
    new-array v4, v9, [B

    goto/16 :goto_4
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38365
    return-object p1
.end method
