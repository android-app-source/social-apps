.class public LX/0IM;
.super LX/04p;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private volatile b:LX/04q;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38593
    invoke-direct {p0}, LX/04p;-><init>()V

    .line 38594
    iput-object p1, p0, LX/0IM;->a:Landroid/content/Context;

    .line 38595
    invoke-static {}, LX/04q;->a()LX/04q;

    move-result-object v0

    iput-object v0, p0, LX/0IM;->b:LX/04q;

    .line 38596
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 38597
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 38598
    invoke-virtual {p0, v0}, LX/04p;->a(Lorg/json/JSONObject;)V

    .line 38599
    invoke-static {v0}, LX/04q;->a(Lorg/json/JSONObject;)LX/04q;

    move-result-object v0

    iput-object v0, p0, LX/0IM;->b:LX/04q;

    .line 38600
    return-void
.end method

.method public final b()LX/04q;
    .locals 1

    .prologue
    .line 38601
    iget-object v0, p0, LX/0IM;->b:LX/04q;

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 38602
    iget-object v0, p0, LX/0IM;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.rti.mqtt.ACTION_MQTT_CONFIG_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 38603
    return-void
.end method
