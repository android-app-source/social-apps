.class public final enum LX/0JH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JH;

.field public static final enum FIELD_OF_VIEW_VERTICAL:LX/0JH;

.field public static final enum PITCH_ANGLE:LX/0JH;

.field public static final enum PLAYING_SURFACE:LX/0JH;

.field public static final enum TARGET_FIELD_OF_VIEW_VERTICAL:LX/0JH;

.field public static final enum TARGET_PITCH_ANGLE:LX/0JH;

.field public static final enum TARGET_YAW_ANGLE:LX/0JH;

.field public static final enum YAW_ANGLE:LX/0JH;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39321
    new-instance v0, LX/0JH;

    const-string v1, "PITCH_ANGLE"

    const-string v2, "pitch"

    invoke-direct {v0, v1, v4, v2}, LX/0JH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JH;->PITCH_ANGLE:LX/0JH;

    .line 39322
    new-instance v0, LX/0JH;

    const-string v1, "YAW_ANGLE"

    const-string v2, "yaw"

    invoke-direct {v0, v1, v5, v2}, LX/0JH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JH;->YAW_ANGLE:LX/0JH;

    .line 39323
    new-instance v0, LX/0JH;

    const-string v1, "TARGET_PITCH_ANGLE"

    const-string v2, "target_pitch"

    invoke-direct {v0, v1, v6, v2}, LX/0JH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JH;->TARGET_PITCH_ANGLE:LX/0JH;

    .line 39324
    new-instance v0, LX/0JH;

    const-string v1, "TARGET_YAW_ANGLE"

    const-string v2, "target_yaw"

    invoke-direct {v0, v1, v7, v2}, LX/0JH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JH;->TARGET_YAW_ANGLE:LX/0JH;

    .line 39325
    new-instance v0, LX/0JH;

    const-string v1, "FIELD_OF_VIEW_VERTICAL"

    const-string v2, "spherical_fov_y"

    invoke-direct {v0, v1, v8, v2}, LX/0JH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JH;->FIELD_OF_VIEW_VERTICAL:LX/0JH;

    .line 39326
    new-instance v0, LX/0JH;

    const-string v1, "TARGET_FIELD_OF_VIEW_VERTICAL"

    const/4 v2, 0x5

    const-string v3, "target_spherical_fov_y"

    invoke-direct {v0, v1, v2, v3}, LX/0JH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JH;->TARGET_FIELD_OF_VIEW_VERTICAL:LX/0JH;

    .line 39327
    new-instance v0, LX/0JH;

    const-string v1, "PLAYING_SURFACE"

    const/4 v2, 0x6

    const-string v3, "playing_surface"

    invoke-direct {v0, v1, v2, v3}, LX/0JH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JH;->PLAYING_SURFACE:LX/0JH;

    .line 39328
    const/4 v0, 0x7

    new-array v0, v0, [LX/0JH;

    sget-object v1, LX/0JH;->PITCH_ANGLE:LX/0JH;

    aput-object v1, v0, v4

    sget-object v1, LX/0JH;->YAW_ANGLE:LX/0JH;

    aput-object v1, v0, v5

    sget-object v1, LX/0JH;->TARGET_PITCH_ANGLE:LX/0JH;

    aput-object v1, v0, v6

    sget-object v1, LX/0JH;->TARGET_YAW_ANGLE:LX/0JH;

    aput-object v1, v0, v7

    sget-object v1, LX/0JH;->FIELD_OF_VIEW_VERTICAL:LX/0JH;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0JH;->TARGET_FIELD_OF_VIEW_VERTICAL:LX/0JH;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0JH;->PLAYING_SURFACE:LX/0JH;

    aput-object v2, v0, v1

    sput-object v0, LX/0JH;->$VALUES:[LX/0JH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39318
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39319
    iput-object p3, p0, LX/0JH;->value:Ljava/lang/String;

    .line 39320
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JH;
    .locals 1

    .prologue
    .line 39330
    const-class v0, LX/0JH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JH;

    return-object v0
.end method

.method public static values()[LX/0JH;
    .locals 1

    .prologue
    .line 39329
    sget-object v0, LX/0JH;->$VALUES:[LX/0JH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JH;

    return-object v0
.end method
