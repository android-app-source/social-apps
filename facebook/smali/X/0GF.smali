.class public final LX/0GF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0GB;

.field private final c:Landroid/net/ConnectivityManager;

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LX/0GB;II)V
    .locals 1

    .prologue
    .line 34554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34555
    iput-object p2, p0, LX/0GF;->a:Ljava/lang/String;

    .line 34556
    iput-object p3, p0, LX/0GF;->b:LX/0GB;

    .line 34557
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, LX/0GF;->c:Landroid/net/ConnectivityManager;

    .line 34558
    iput p4, p0, LX/0GF;->d:I

    .line 34559
    iput p5, p0, LX/0GF;->e:I

    .line 34560
    return-void
.end method

.method public static a(LX/0GF;)I
    .locals 1

    .prologue
    .line 34561
    iget-object v0, p0, LX/0GF;->c:Landroid/net/ConnectivityManager;

    invoke-static {v0}, LX/0GG;->b(Landroid/net/ConnectivityManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/0GF;->e:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/0GF;->d:I

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;LX/0AY;LX/0GB;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 34562
    invoke-virtual {p1, v1}, LX/0AY;->a(I)LX/0Am;

    move-result-object v2

    .line 34563
    invoke-virtual {v2, v1}, LX/0Am;->a(I)I

    move-result v3

    .line 34564
    const/4 v0, 0x0

    .line 34565
    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    .line 34566
    iget-object v0, v2, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    move-object v2, v0

    .line 34567
    :goto_0
    if-nez v2, :cond_2

    .line 34568
    :cond_0
    :goto_1
    return-void

    .line 34569
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    :cond_2
    iget-object v0, v2, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 34570
    iget-object v0, v2, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 34571
    iget-object v3, v0, LX/0Ah;->c:LX/0AR;

    iget-boolean v3, v3, LX/0AR;->e:Z

    if-eqz v3, :cond_1

    .line 34572
    iget-object v0, v0, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {p2, p0, v0}, LX/0GB;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v2, v0

    goto :goto_0
.end method


# virtual methods
.method public final a([LX/0AR;)LX/0AR;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 34573
    invoke-static {p0}, LX/0GF;->a(LX/0GF;)I

    move-result v3

    .line 34574
    iget-object v0, p0, LX/0GF;->b:LX/0GB;

    iget-object v2, p0, LX/0GF;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0GB;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move v0, v1

    .line 34575
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_4

    .line 34576
    aget-object v2, p1, v0

    .line 34577
    if-eqz v4, :cond_1

    .line 34578
    iget-object v5, v2, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, v2, LX/0AR;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "d"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_0
    move-object v0, v2

    .line 34579
    :goto_1
    return-object v0

    .line 34580
    :cond_1
    if-lez v3, :cond_2

    .line 34581
    iget v5, v2, LX/0AR;->f:I

    move v5, v5

    .line 34582
    if-gt v5, v3, :cond_3

    move-object v0, v2

    .line 34583
    goto :goto_1

    .line 34584
    :cond_2
    iget-boolean v5, v2, LX/0AR;->e:Z

    if-eqz v5, :cond_3

    .line 34585
    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, v2, LX/0AR;->a:Ljava/lang/String;

    aput-object v4, v3, v1

    .line 34586
    move-object v0, v2

    .line 34587
    goto :goto_1

    .line 34588
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34589
    :cond_4
    new-array v2, v8, [Ljava/lang/Object;

    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    aget-object v3, p1, v3

    iget-object v3, v3, LX/0AR;->a:Ljava/lang/String;

    aput-object v3, v2, v1

    .line 34590
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p1, v0

    goto :goto_1
.end method
