.class public final LX/0CW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/browser/lite/BrowserLiteChrome;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteChrome;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27290
    iput-object p1, p0, LX/0CW;->b:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iput-object p2, p0, LX/0CW;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x21ef806e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 27291
    iget-object v1, p0, LX/0CW;->b:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.EXTRA_ACTION_MENU_ITEMS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 27292
    if-nez v1, :cond_1

    .line 27293
    iget-object v1, p0, LX/0CW;->b:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v1, v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    if-nez v1, :cond_0

    .line 27294
    const v1, -0x128df467

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 27295
    :goto_0
    return-void

    .line 27296
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 27297
    const-string v2, "action"

    iget-object v3, p0, LX/0CW;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27298
    const-string v2, "url"

    iget-object v3, p0, LX/0CW;->b:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v3, v3, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v3}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27299
    iget-object v2, p0, LX/0CW;->b:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v2, v2, Lcom/facebook/browser/lite/BrowserLiteChrome;->t:LX/0CQ;

    iget-object v3, p0, LX/0CW;->b:Lcom/facebook/browser/lite/BrowserLiteChrome;

    iget-object v3, v3, Lcom/facebook/browser/lite/BrowserLiteChrome;->C:Landroid/os/Bundle;

    invoke-virtual {v2, v1, v3}, LX/0CQ;->a(Ljava/util/Map;Landroid/os/Bundle;)V

    .line 27300
    :goto_1
    const v1, -0x77e05520

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 27301
    :cond_1
    iget-object v2, p0, LX/0CW;->b:Lcom/facebook/browser/lite/BrowserLiteChrome;

    .line 27302
    invoke-static {v2, v1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a$redex0(Lcom/facebook/browser/lite/BrowserLiteChrome;Ljava/util/ArrayList;)V

    .line 27303
    goto :goto_1
.end method
