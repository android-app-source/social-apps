.class public final LX/0BW;
.super LX/0Ah;
.source ""

# interfaces
.implements LX/0BX;


# instance fields
.field private final e:LX/0BV;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLX/0AR;LX/0BV;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 26668
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, LX/0Ah;-><init>(Ljava/lang/String;JLX/0AR;LX/0Ag;Ljava/lang/String;Ljava/lang/String;B)V

    .line 26669
    iput-object p5, p0, LX/0BW;->e:LX/0BV;

    .line 26670
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 26671
    iget-object v0, p0, LX/0BW;->e:LX/0BV;

    .line 26672
    iget p0, v0, LX/0BV;->d:I

    move v0, p0

    .line 26673
    return v0
.end method

.method public final a(J)I
    .locals 1

    .prologue
    .line 26693
    iget-object v0, p0, LX/0BW;->e:LX/0BV;

    invoke-virtual {v0, p1, p2}, LX/0BV;->a(J)I

    move-result v0

    return v0
.end method

.method public final a(JJ)I
    .locals 8

    .prologue
    .line 26674
    iget-object v0, p0, LX/0BW;->e:LX/0BV;

    .line 26675
    iget v1, v0, LX/0BV;->d:I

    move v1, v1

    .line 26676
    invoke-virtual {v0, p3, p4}, LX/0BV;->a(J)I

    move-result v2

    .line 26677
    iget-object v3, v0, LX/0BV;->f:Ljava/util/List;

    if-nez v3, :cond_6

    .line 26678
    iget-wide v3, v0, LX/0BV;->e:J

    const-wide/32 v5, 0xf4240

    mul-long/2addr v3, v5

    iget-wide v5, v0, LX/0Ag;->b:J

    div-long/2addr v3, v5

    .line 26679
    iget v5, v0, LX/0BV;->d:I

    div-long v3, p1, v3

    long-to-int v3, v3

    add-int/2addr v3, v5

    .line 26680
    if-ge v3, v1, :cond_0

    .line 26681
    :goto_0
    move v0, v1

    .line 26682
    return v0

    .line 26683
    :cond_0
    const/4 v1, -0x1

    if-eq v2, v1, :cond_1

    if-le v3, v2, :cond_1

    move v1, v2

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_0

    .line 26684
    :goto_1
    if-gt v3, v4, :cond_4

    .line 26685
    add-int v2, v3, v4

    div-int/lit8 v2, v2, 0x2

    .line 26686
    invoke-virtual {v0, v2}, LX/0BV;->a(I)J

    move-result-wide v5

    .line 26687
    cmp-long v7, v5, p1

    if-gez v7, :cond_2

    .line 26688
    add-int/lit8 v3, v2, 0x1

    goto :goto_1

    .line 26689
    :cond_2
    cmp-long v4, v5, p1

    if-lez v4, :cond_3

    .line 26690
    add-int/lit8 v4, v2, -0x1

    goto :goto_1

    :cond_3
    move v1, v2

    .line 26691
    goto :goto_0

    .line 26692
    :cond_4
    if-ne v3, v1, :cond_5

    move v1, v3

    goto :goto_0

    :cond_5
    move v1, v4

    goto :goto_0

    :cond_6
    move v4, v2

    move v3, v1

    goto :goto_1
.end method

.method public final a(I)J
    .locals 2

    .prologue
    .line 26661
    iget-object v0, p0, LX/0BW;->e:LX/0BV;

    invoke-virtual {v0, p1}, LX/0BV;->a(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(IJ)J
    .locals 6

    .prologue
    .line 26662
    iget-object v0, p0, LX/0BW;->e:LX/0BV;

    const-wide/32 v4, 0xf4240

    .line 26663
    iget-object v2, v0, LX/0BV;->f:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 26664
    iget-object v2, v0, LX/0BV;->f:Ljava/util/List;

    iget v3, v0, LX/0BV;->d:I

    sub-int v3, p1, v3

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0BT;

    iget-wide v2, v2, LX/0BT;->b:J

    .line 26665
    mul-long/2addr v2, v4

    iget-wide v4, v0, LX/0Ag;->b:J

    div-long/2addr v2, v4

    .line 26666
    :goto_0
    move-wide v0, v2

    .line 26667
    return-wide v0

    :cond_0
    invoke-virtual {v0, p2, p3}, LX/0BV;->a(J)I

    move-result v2

    if-ne p1, v2, :cond_1

    invoke-virtual {v0, p1}, LX/0BV;->a(I)J

    move-result-wide v2

    sub-long v2, p2, v2

    goto :goto_0

    :cond_1
    iget-wide v2, v0, LX/0BV;->e:J

    mul-long/2addr v2, v4

    iget-wide v4, v0, LX/0Ag;->b:J

    div-long/2addr v2, v4

    goto :goto_0
.end method

.method public final b(I)LX/0Au;
    .locals 1

    .prologue
    .line 26660
    iget-object v0, p0, LX/0BW;->e:LX/0BV;

    invoke-virtual {v0, p0, p1}, LX/0BV;->a(LX/0Ah;I)LX/0Au;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 26659
    iget-object v0, p0, LX/0BW;->e:LX/0BV;

    invoke-virtual {v0}, LX/0BV;->c()Z

    move-result v0

    return v0
.end method

.method public final c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26658
    iget-object v0, p0, LX/0BW;->e:LX/0BV;

    invoke-virtual {v0, p1}, LX/0BV;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/0Au;
    .locals 1

    .prologue
    .line 26657
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()LX/0BX;
    .locals 0

    .prologue
    .line 26656
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 26652
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 26653
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "format:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0Ah;->c:LX/0AR;

    iget-object v2, v2, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26654
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "segments:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0BW;->e:LX/0BV;

    invoke-virtual {v2}, LX/0BV;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26655
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
