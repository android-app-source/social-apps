.class public LX/0Di;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/0Di;


# instance fields
.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0CQ;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 30974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30975
    invoke-static {}, LX/0CQ;->a()LX/0CQ;

    move-result-object v0

    iput-object v0, p0, LX/0Di;->c:LX/0CQ;

    .line 30976
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0Di;->b:Ljava/util/ArrayList;

    .line 30977
    return-void
.end method

.method public static a()LX/0Di;
    .locals 1

    .prologue
    .line 30971
    sget-object v0, LX/0Di;->a:LX/0Di;

    if-nez v0, :cond_0

    .line 30972
    new-instance v0, LX/0Di;

    invoke-direct {v0}, LX/0Di;-><init>()V

    sput-object v0, LX/0Di;->a:LX/0Di;

    .line 30973
    :cond_0
    sget-object v0, LX/0Di;->a:LX/0Di;

    return-object v0
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    .line 30962
    iget-object v0, p0, LX/0Di;->c:LX/0CQ;

    .line 30963
    iget-object v1, v0, LX/0CQ;->c:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 30964
    if-nez v0, :cond_1

    .line 30965
    :cond_0
    :goto_1
    return-void

    .line 30966
    :cond_1
    iget-object v0, p0, LX/0Di;->b:Ljava/util/ArrayList;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30967
    iget-object v0, p0, LX/0Di;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 30968
    iget-object v0, p0, LX/0Di;->c:LX/0CQ;

    invoke-virtual {p0}, LX/0Di;->c()[J

    move-result-object v1

    .line 30969
    new-instance v2, LX/0C9;

    invoke-direct {v2, v0, v1}, LX/0C9;-><init>(LX/0CQ;[J)V

    invoke-static {v0, v2}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 30970
    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final c()[J
    .locals 6

    .prologue
    .line 30956
    iget-object v0, p0, LX/0Di;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [J

    .line 30957
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0Di;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 30958
    iget-object v0, p0, LX/0Di;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 30959
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 30960
    :cond_0
    iget-object v0, p0, LX/0Di;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 30961
    return-object v2
.end method
