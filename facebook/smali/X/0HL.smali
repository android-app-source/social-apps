.class public LX/0HL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 37538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37539
    invoke-static {p1, p2}, LX/06c;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/0HL;->a:Ljava/io/File;

    .line 37540
    return-void
.end method


# virtual methods
.method public final a(LX/0HK;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 37541
    iget-object v0, p0, LX/0HL;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37542
    iget-object v0, p0, LX/0HL;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37543
    const-string v0, "AnalyticsStorage"

    const-string v1, "Unable to open analytics storage."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37544
    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, LX/0HL;->a:Ljava/io/File;

    .line 37545
    const-string v2, "%s_%d.batch"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 37546
    iget-object p0, p1, LX/0HK;->b:Ljava/util/UUID;

    if-nez p0, :cond_1

    .line 37547
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p0

    iput-object p0, p1, LX/0HK;->b:Ljava/util/UUID;

    .line 37548
    :cond_1
    iget-object p0, p1, LX/0HK;->b:Ljava/util/UUID;

    move-object p0, p0

    .line 37549
    invoke-virtual {p0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v3, v6

    const/4 v6, 0x1

    .line 37550
    iget p0, p1, LX/0HK;->c:I

    move p0, p0

    .line 37551
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v3, v6

    invoke-static {v2, v3}, LX/05V;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 37552
    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 37553
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 37554
    const-string v0, "AnalyticsStorage"

    const-string v2, "Duplicate file %s"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-static {v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37555
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    .line 37556
    if-nez v0, :cond_2

    .line 37557
    const-string v0, "AnalyticsStorage"

    const-string v2, "File %s was not deleted"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-static {v0, v2, v3}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37558
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 37559
    iput-wide v2, p1, LX/0HK;->i:J

    .line 37560
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37561
    :try_start_1
    new-instance v1, Ljava/io/OutputStreamWriter;

    const-string v0, "UTF8"

    invoke-direct {v1, v2, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 37562
    :try_start_2
    invoke-virtual {p1}, LX/0HK;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 37563
    :try_start_3
    invoke-virtual {v1}, Ljava/io/Writer;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 37564
    :goto_0
    return-void

    .line 37565
    :catch_0
    move-exception v0

    .line 37566
    const-string v2, "AnalyticsStorage"

    const-string v3, "Batch file creation failed %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 37567
    :catch_1
    move-exception v0

    .line 37568
    const-string v1, "AnalyticsStorage"

    const-string v3, "UTF8 encoding is not supported"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37569
    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 37570
    :catch_2
    move-exception v0

    .line 37571
    const-string v1, "AnalyticsStorage"

    const-string v2, "failed to close output stream"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 37572
    :catch_3
    move-exception v0

    .line 37573
    const-string v1, "AnalyticsStorage"

    const-string v2, "failed to close writer"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 37574
    :catch_4
    move-exception v0

    .line 37575
    :try_start_5
    const-string v2, "AnalyticsStorage"

    const-string v3, "failed to write session to file"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 37576
    :try_start_6
    invoke-virtual {v1}, Ljava/io/Writer;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_0

    .line 37577
    :catch_5
    move-exception v0

    .line 37578
    const-string v1, "AnalyticsStorage"

    const-string v2, "failed to close writer"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 37579
    :catchall_0
    move-exception v0

    .line 37580
    :try_start_7
    invoke-virtual {v1}, Ljava/io/Writer;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 37581
    :goto_1
    throw v0

    .line 37582
    :catch_6
    move-exception v1

    .line 37583
    const-string v2, "AnalyticsStorage"

    const-string v3, "failed to close writer"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
