.class public final LX/0Mt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Mv;

.field private final b:LX/0Oj;

.field private c:J

.field private d:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48313
    new-instance v0, LX/0Mv;

    invoke-direct {v0}, LX/0Mv;-><init>()V

    iput-object v0, p0, LX/0Mt;->a:LX/0Mv;

    .line 48314
    new-instance v0, LX/0Oj;

    const/16 v1, 0x11a

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, LX/0Mt;->b:LX/0Oj;

    .line 48315
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0Mt;->c:J

    return-void
.end method


# virtual methods
.method public final a(JLX/0MA;)J
    .locals 11

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v8, 0x0

    .line 48294
    iget-wide v6, p0, LX/0Mt;->c:J

    cmp-long v0, v6, v4

    if-eqz v0, :cond_2

    iget-wide v6, p0, LX/0Mt;->d:J

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 48295
    iget-object v0, p0, LX/0Mt;->a:LX/0Mv;

    iget-object v3, p0, LX/0Mt;->b:LX/0Oj;

    invoke-static {p3, v0, v3, v2}, LX/0Mw;->a(LX/0MA;LX/0Mv;LX/0Oj;Z)Z

    .line 48296
    iget-object v0, p0, LX/0Mt;->a:LX/0Mv;

    iget-wide v2, v0, LX/0Mv;->c:J

    sub-long v2, p1, v2

    .line 48297
    cmp-long v0, v2, v8

    if-lez v0, :cond_0

    const-wide/32 v6, 0x11940

    cmp-long v0, v2, v6

    if-lez v0, :cond_3

    .line 48298
    :cond_0
    iget-object v0, p0, LX/0Mt;->a:LX/0Mv;

    iget v0, v0, LX/0Mv;->i:I

    iget-object v4, p0, LX/0Mt;->a:LX/0Mv;

    iget v4, v4, LX/0Mv;->h:I

    add-int/2addr v0, v4

    cmp-long v4, v2, v8

    if-gtz v4, :cond_1

    const/4 v1, 0x2

    :cond_1
    mul-int/2addr v0, v1

    int-to-long v0, v0

    .line 48299
    invoke-interface {p3}, LX/0MA;->c()J

    move-result-wide v4

    sub-long v0, v4, v0

    iget-wide v4, p0, LX/0Mt;->c:J

    mul-long/2addr v2, v4

    iget-wide v4, p0, LX/0Mt;->d:J

    div-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 48300
    :goto_1
    return-wide v0

    :cond_2
    move v0, v2

    .line 48301
    goto :goto_0

    .line 48302
    :cond_3
    invoke-interface {p3}, LX/0MA;->a()V

    move-wide v0, v4

    .line 48303
    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 48304
    iget-object v0, p0, LX/0Mt;->a:LX/0Mv;

    invoke-virtual {v0}, LX/0Mv;->a()V

    .line 48305
    iget-object v0, p0, LX/0Mt;->b:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->a()V

    .line 48306
    return-void
.end method

.method public final a(JJ)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 48307
    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    cmp-long v0, p3, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->a(Z)V

    .line 48308
    iput-wide p1, p0, LX/0Mt;->c:J

    .line 48309
    iput-wide p3, p0, LX/0Mt;->d:J

    .line 48310
    return-void

    .line 48311
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
