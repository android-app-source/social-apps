.class public final LX/0LA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0GW;


# instance fields
.field private a:Z

.field private b:J

.field private c:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static b(J)J
    .locals 4

    .prologue
    .line 43048
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sub-long/2addr v0, p0

    return-wide v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 43049
    iget-boolean v0, p0, LX/0LA;->a:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/0LA;->c:J

    invoke-static {v0, v1}, LX/0LA;->b(J)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LX/0LA;->b:J

    goto :goto_0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 43050
    iput-wide p1, p0, LX/0LA;->b:J

    .line 43051
    invoke-static {p1, p2}, LX/0LA;->b(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/0LA;->c:J

    .line 43052
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 43053
    iget-boolean v0, p0, LX/0LA;->a:Z

    if-nez v0, :cond_0

    .line 43054
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0LA;->a:Z

    .line 43055
    iget-wide v0, p0, LX/0LA;->b:J

    invoke-static {v0, v1}, LX/0LA;->b(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/0LA;->c:J

    .line 43056
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 43057
    iget-boolean v0, p0, LX/0LA;->a:Z

    if-eqz v0, :cond_0

    .line 43058
    iget-wide v0, p0, LX/0LA;->c:J

    invoke-static {v0, v1}, LX/0LA;->b(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/0LA;->b:J

    .line 43059
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0LA;->a:Z

    .line 43060
    :cond_0
    return-void
.end method
