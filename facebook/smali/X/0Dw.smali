.class public LX/0Dw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/app/Activity;

.field public b:LX/0Cr;

.field public c:Landroid/view/View;

.field public d:Landroid/view/View;

.field public e:Landroid/view/View;

.field public f:Landroid/view/View;

.field public g:Landroid/view/View;

.field public h:Landroid/widget/TextView;

.field public i:Landroid/widget/TextView;

.field public j:Landroid/widget/TextView;

.field public k:Landroid/widget/TextView;

.field public l:Landroid/widget/TextView;

.field public m:Landroid/view/View;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Landroid/view/View;

.field public s:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 31243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31244
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Dw;->s:Z

    .line 31245
    iput-object p1, p0, LX/0Dw;->a:Landroid/app/Activity;

    .line 31246
    iput-object p2, p0, LX/0Dw;->c:Landroid/view/View;

    .line 31247
    return-void
.end method

.method public static a(Landroid/app/Activity;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 31263
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 31264
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 31265
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0Dw;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 31260
    iget-object v0, p0, LX/0Dw;->b:LX/0Cr;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Dw;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Dw;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Dw;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Dw;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 31261
    iget-object v0, p0, LX/0Dw;->b:LX/0Cr;

    const/4 v1, 0x0

    iget-object v2, p0, LX/0Dw;->o:Ljava/lang/String;

    iget-object v3, p0, LX/0Dw;->p:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v2, v3}, LX/0Cr;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 31262
    :cond_0
    return-void
.end method

.method public static a(LX/0Dw;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 31266
    iget-object v0, p0, LX/0Dw;->f:Landroid/view/View;

    sget v1, LX/0E7;->h:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/0Dw;->h:Landroid/widget/TextView;

    .line 31267
    iget-object v0, p0, LX/0Dw;->f:Landroid/view/View;

    sget v1, LX/0E7;->i:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/0Dw;->i:Landroid/widget/TextView;

    .line 31268
    iget-object v0, p0, LX/0Dw;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31269
    iget-object v0, p0, LX/0Dw;->f:Landroid/view/View;

    sget v1, LX/0E7;->j:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 31270
    invoke-static {p0, v0}, LX/0Dw;->a(LX/0Dw;Landroid/view/View;)V

    .line 31271
    iget-object v0, p0, LX/0Dw;->i:Landroid/widget/TextView;

    sget v1, LX/0E7;->n:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 31272
    iget-object v0, p0, LX/0Dw;->i:Landroid/widget/TextView;

    new-instance v1, LX/0Dv;

    invoke-direct {v1, p0, p1}, LX/0Dv;-><init>(LX/0Dw;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31273
    return-void
.end method

.method public static g(LX/0Dw;)V
    .locals 3

    .prologue
    .line 31256
    iget-object v0, p0, LX/0Dw;->c:Landroid/view/View;

    const v1, 0x7f0d07c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 31257
    iget-object v1, p0, LX/0Dw;->c:Landroid/view/View;

    const v2, 0x7f0d07b9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 31258
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTop(I)V

    .line 31259
    return-void
.end method

.method public static i(LX/0Dw;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 31252
    iget-object v0, p0, LX/0Dw;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 31253
    iget-object v0, p0, LX/0Dw;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 31254
    iget-object v0, p0, LX/0Dw;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 31255
    return-void
.end method

.method public static j(LX/0Dw;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 31248
    iget-object v0, p0, LX/0Dw;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 31249
    iget-object v0, p0, LX/0Dw;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 31250
    iget-object v0, p0, LX/0Dw;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 31251
    return-void
.end method
