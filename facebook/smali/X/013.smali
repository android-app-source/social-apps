.class public LX/013;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/io/File;


# instance fields
.field public final b:Ljava/lang/Object;

.field private final c:Ljava/util/List;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.util.ArrayList._Constructor"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/00j;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3520
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/kernel/debug/tracing/trace"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/013;->a:Ljava/io/File;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3518
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/013;->b:Ljava/lang/Object;

    .line 3519
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/013;->c:Ljava/util/List;

    return-void
.end method

.method private static a(LX/013;Z)V
    .locals 2
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation

    .prologue
    .line 3509
    iput-boolean p1, p0, LX/013;->d:Z

    .line 3510
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/013;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 3511
    iget-object v0, p0, LX/013;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00j;

    .line 3512
    if-eqz p1, :cond_0

    .line 3513
    invoke-interface {v0}, LX/00j;->a()V

    .line 3514
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3515
    :cond_0
    invoke-interface {v0}, LX/00j;->b()V

    goto :goto_1

    .line 3516
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 3521
    iget-object v1, p0, LX/013;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3522
    :try_start_0
    sget-object v0, LX/013;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 3523
    new-instance v0, Lcom/facebook/systrace/TraceListenerNotifier$1;

    invoke-direct {v0, p0, v2, v3}, Lcom/facebook/systrace/TraceListenerNotifier$1;-><init>(LX/013;J)V

    const-string v2, "fbsystrace notification thread"

    const v3, 0x57d6a46c

    invoke-static {v0, v2, v3}, LX/00l;->a(Ljava/lang/Runnable;Ljava/lang/String;I)Ljava/lang/Thread;

    move-result-object v0

    .line 3524
    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/lang/Thread;->setPriority(I)V

    .line 3525
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 3526
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/00j;)V
    .locals 2

    .prologue
    .line 3504
    iget-object v1, p0, LX/013;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3505
    :try_start_0
    iget-object v0, p0, LX/013;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3506
    iget-boolean v0, p0, LX/013;->d:Z

    if-eqz v0, :cond_0

    .line 3507
    invoke-interface {p1}, LX/00j;->a()V

    .line 3508
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 3497
    iget-object v1, p0, LX/013;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3498
    const-wide/16 v2, 0x1

    :try_start_0
    const-string v0, "Run Trace Listeners"

    invoke-static {v2, v3, v0}, LX/018;->a(JLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 3499
    const/4 v0, 0x1

    :try_start_1
    invoke-static {p0, v0}, LX/013;->a(LX/013;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3500
    const-wide/16 v2, 0x1

    :try_start_2
    invoke-static {v2, v3}, LX/018;->a(J)V

    .line 3501
    monitor-exit v1

    return-void

    .line 3502
    :catchall_0
    move-exception v0

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, LX/018;->a(J)V

    throw v0

    .line 3503
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public final b(LX/00j;)V
    .locals 2

    .prologue
    .line 3492
    iget-object v1, p0, LX/013;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3493
    :try_start_0
    iget-object v0, p0, LX/013;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 3494
    iget-boolean v0, p0, LX/013;->d:Z

    if-eqz v0, :cond_0

    .line 3495
    invoke-interface {p1}, LX/00j;->b()V

    .line 3496
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 3489
    iget-object v1, p0, LX/013;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3490
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, v0}, LX/013;->a(LX/013;Z)V

    .line 3491
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
