.class public final LX/0NP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public f:J

.field public g:J


# direct methods
.method public constructor <init>(IIIII)V
    .locals 0

    .prologue
    .line 50279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50280
    iput p1, p0, LX/0NP;->a:I

    .line 50281
    iput p2, p0, LX/0NP;->b:I

    .line 50282
    iput p3, p0, LX/0NP;->c:I

    .line 50283
    iput p4, p0, LX/0NP;->d:I

    .line 50284
    iput p5, p0, LX/0NP;->e:I

    .line 50285
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 12

    .prologue
    .line 50286
    iget-wide v8, p0, LX/0NP;->g:J

    .line 50287
    iget v10, p0, LX/0NP;->d:I

    iget v11, p0, LX/0NP;->a:I

    div-int/2addr v10, v11

    move v10, v10

    .line 50288
    int-to-long v10, v10

    div-long/2addr v8, v10

    move-wide v4, v8

    .line 50289
    iget v6, p0, LX/0NP;->a:I

    move v6, v6

    .line 50290
    int-to-long v6, v6

    div-long/2addr v4, v6

    move-wide v0, v4

    .line 50291
    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    iget v2, p0, LX/0NP;->b:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public final b(J)J
    .locals 5

    .prologue
    .line 50292
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p1

    iget v2, p0, LX/0NP;->c:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public final f()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 50293
    iget-wide v0, p0, LX/0NP;->f:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/0NP;->g:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
