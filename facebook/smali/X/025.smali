.class public abstract LX/025;
.super LX/021;
.source ""


# instance fields
.field public c:[Ljava/lang/String;

.field public final e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 6199
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v0, v0

    .line 6200
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/021;-><init>(Ljava/io/File;I)V

    .line 6201
    iput-object p1, p0, LX/025;->e:Landroid/content/Context;

    .line 6202
    return-void
.end method

.method private static a(LX/025;BLX/0AO;LX/0Ay;)V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 6174
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "regenerating DSO store "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6175
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, LX/021;->a:Ljava/io/File;

    const-string v5, "dso_manifest"

    invoke-direct {v0, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 6176
    new-instance v6, Ljava/io/RandomAccessFile;

    const-string v3, "rw"

    invoke-direct {v6, v0, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 6177
    if-ne p1, v4, :cond_2

    .line 6178
    :try_start_0
    invoke-static {v6}, LX/0AO;->a(Ljava/io/DataInput;)LX/0AO;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v0

    .line 6179
    :goto_0
    if-nez v0, :cond_9

    .line 6180
    :try_start_1
    new-instance v0, LX/0AO;

    const/4 v3, 0x0

    new-array v3, v3, [LX/07w;

    invoke-direct {v0, v3}, LX/0AO;-><init>([LX/07w;)V

    move-object v5, v0

    .line 6181
    :goto_1
    iget-object v0, p2, LX/0AO;->a:[LX/07w;

    invoke-static {p0, v0}, LX/025;->a(LX/025;[LX/07w;)V

    .line 6182
    const v0, 0x8000

    new-array v7, v0, [B

    .line 6183
    :cond_0
    :goto_2
    invoke-virtual {p3}, LX/0Ay;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 6184
    invoke-virtual {p3}, LX/0Ay;->b()LX/0Is;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v8

    move v3, v1

    move v0, v4

    .line 6185
    :goto_3
    if-eqz v0, :cond_3

    :try_start_2
    iget-object v9, v5, LX/0AO;->a:[LX/07w;

    array-length v9, v9

    if-ge v3, v9, :cond_3

    .line 6186
    iget-object v9, v5, LX/0AO;->a:[LX/07w;

    aget-object v9, v9, v3

    iget-object v9, v9, LX/07w;->c:Ljava/lang/String;

    iget-object v10, v8, LX/0Is;->a:LX/07w;

    iget-object v10, v10, LX/07w;->c:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, v5, LX/0AO;->a:[LX/07w;

    aget-object v9, v9, v3

    iget-object v9, v9, LX/07w;->d:Ljava/lang/String;

    iget-object v10, v8, LX/0Is;->a:LX/07w;

    iget-object v10, v10, LX/07w;->d:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    move v0, v1

    .line 6187
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :catch_0
    :cond_2
    move-object v0, v2

    goto :goto_0

    .line 6188
    :cond_3
    if-eqz v0, :cond_4

    .line 6189
    invoke-static {p0, v8, v7}, LX/025;->a(LX/025;LX/0Is;[B)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 6190
    :cond_4
    if-eqz v8, :cond_0

    :try_start_3
    invoke-virtual {v8}, LX/0Is;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    .line 6191
    :catch_1
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 6192
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_4
    if-eqz v2, :cond_8

    :try_start_5
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_4

    :goto_5
    throw v0

    .line 6193
    :catch_2
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 6194
    :catchall_1
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_6
    if-eqz v8, :cond_5

    if-eqz v1, :cond_6

    :try_start_7
    invoke-virtual {v8}, LX/0Is;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :cond_5
    :goto_7
    :try_start_8
    throw v0

    .line 6195
    :catchall_2
    move-exception v0

    goto :goto_4

    .line 6196
    :catch_3
    move-exception v3

    invoke-static {v1, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_7

    :cond_6
    invoke-virtual {v8}, LX/0Is;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_7

    .line 6197
    :cond_7
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V

    return-void

    :catch_4
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_8
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V

    goto :goto_5

    .line 6198
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_6

    :cond_9
    move-object v5, v0

    goto/16 :goto_1
.end method

.method private static a(LX/025;LX/0Is;[B)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 6148
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "extracting DSO "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, LX/0Is;->a:LX/07w;

    iget-object v1, v1, LX/07w;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6149
    iget-object v0, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v0, v5, v5}, Ljava/io/File;->setWritable(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6150
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot make directory writable for us: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6151
    :cond_0
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, LX/021;->a:Ljava/io/File;

    iget-object v1, p1, LX/0Is;->a:LX/07w;

    iget-object v1, v1, LX/07w;->c:Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 6152
    :try_start_0
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "rw"

    invoke-direct {v0, v2, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 6153
    :goto_0
    :try_start_1
    iget-object v0, p1, LX/0Is;->b:Ljava/io/InputStream;

    .line 6154
    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    .line 6155
    if-le v0, v5, :cond_1

    .line 6156
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    int-to-long v4, v0

    .line 6157
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x15

    if-lt v0, v6, :cond_1

    .line 6158
    invoke-static {v3, v4, v5}, LX/0Ir;->fallocateIfSupported(Ljava/io/FileDescriptor;J)V

    .line 6159
    :cond_1
    iget-object v0, p1, LX/0Is;->b:Ljava/io/InputStream;

    const v3, 0x7fffffff

    const/4 v5, 0x0

    .line 6160
    move v4, v5

    .line 6161
    :goto_1
    if-ge v4, v3, :cond_2

    array-length v6, p2

    sub-int p0, v3, v4

    invoke-static {v6, p0}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v0, p2, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    const/4 p0, -0x1

    if-eq v6, p0, :cond_2

    .line 6162
    invoke-virtual {v1, p2, v5, v6}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 6163
    add-int/2addr v4, v6

    goto :goto_1

    .line 6164
    :cond_2
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 6165
    const/4 v0, 0x1

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v0

    if-nez v0, :cond_3

    .line 6166
    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot make file executable: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6167
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    throw v0

    .line 6168
    :catch_0
    move-exception v0

    .line 6169
    const-string v1, "fb-UnpackingSoSource"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "error overwriting "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " trying to delete and start over"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6170
    invoke-static {v2}, LX/01z;->a(Ljava/io/File;)V

    .line 6171
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "rw"

    invoke-direct {v0, v2, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v1, v0

    goto/16 :goto_0

    .line 6172
    :cond_3
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    .line 6173
    return-void
.end method

.method private static a(LX/025;[LX/07w;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 6132
    iget-object v0, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    .line 6133
    if-nez v4, :cond_0

    .line 6134
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to list directory "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    .line 6135
    :goto_0
    array-length v2, v4

    if-ge v0, v2, :cond_4

    .line 6136
    aget-object v5, v4, v0

    .line 6137
    const-string v2, "dso_state"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "dso_lock"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "dso_deps"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "dso_manifest"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v3, v1

    move v2, v1

    .line 6138
    :goto_1
    if-nez v2, :cond_2

    array-length v6, p1

    if-ge v3, v6, :cond_2

    .line 6139
    aget-object v6, p1, v3

    iget-object v6, v6, LX/07w;->c:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 6140
    const/4 v2, 0x1

    .line 6141
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 6142
    :cond_2
    if-nez v2, :cond_3

    .line 6143
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, LX/021;->a:Ljava/io/File;

    invoke-direct {v2, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 6144
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "deleting unaccounted-for file "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6145
    invoke-static {v2}, LX/01z;->a(Ljava/io/File;)V

    .line 6146
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6147
    :cond_4
    return-void
.end method

.method private a(LX/01D;I[B)Z
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 6087
    new-instance v5, Ljava/io/File;

    iget-object v0, p0, LX/021;->a:Ljava/io/File;

    const-string v2, "dso_state"

    invoke-direct {v5, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 6088
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v0, "rw"

    invoke-direct {v2, v5, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 6089
    :try_start_0
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v0

    .line 6090
    if-eq v0, v7, :cond_0

    .line 6091
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "dso store "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " regeneration interrupted: wiping clean"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_7

    move v0, v1

    .line 6092
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V

    .line 6093
    new-instance v2, Ljava/io/File;

    iget-object v4, p0, LX/021;->a:Ljava/io/File;

    const-string v6, "dso_deps"

    invoke-direct {v2, v4, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 6094
    new-instance v6, Ljava/io/RandomAccessFile;

    const-string v4, "rw"

    invoke-direct {v6, v2, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 6095
    :try_start_1
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v8

    long-to-int v4, v8

    new-array v4, v4, [B

    .line 6096
    invoke-virtual {v6, v4}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v8

    array-length v9, v4

    if-eq v8, v9, :cond_1

    move v0, v1

    .line 6097
    :cond_1
    invoke-static {v4, p3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_2

    move v0, v1

    .line 6098
    :cond_2
    if-nez v0, :cond_d

    .line 6099
    const/4 v4, 0x0

    invoke-static {v5, v4}, LX/025;->b(Ljava/io/File;B)V

    .line 6100
    invoke-virtual {p0}, LX/025;->a()LX/07u;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    move-result-object v8

    .line 6101
    :try_start_2
    invoke-virtual {v8}, LX/07u;->a()LX/0AO;

    move-result-object v4

    .line 6102
    invoke-virtual {v8}, LX/07u;->b()LX/0Ay;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    move-result-object v9

    .line 6103
    :try_start_3
    invoke-static {p0, v0, v4, v9}, LX/025;->a(LX/025;BLX/0AO;LX/0Ay;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    .line 6104
    if-eqz v9, :cond_3

    :try_start_4
    invoke-virtual {v9}, LX/0Ay;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 6105
    :cond_3
    if-eqz v8, :cond_4

    :try_start_5
    invoke-virtual {v8}, LX/07u;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 6106
    :cond_4
    :goto_1
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V

    .line 6107
    if-nez v4, :cond_b

    .line 6108
    :goto_2
    return v1

    .line 6109
    :catch_0
    move v0, v1

    goto :goto_0

    .line 6110
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 6111
    :catchall_0
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_3
    if-eqz v3, :cond_5

    :try_start_7
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2

    :goto_4
    throw v0

    :catch_2
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_5
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V

    goto :goto_4

    .line 6112
    :catch_3
    move-exception v1

    :try_start_8
    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 6113
    :catchall_1
    move-exception v0

    :goto_5
    if-eqz v9, :cond_6

    if-eqz v1, :cond_8

    :try_start_9
    invoke-virtual {v9}, LX/0Ay;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    :cond_6
    :goto_6
    :try_start_a
    throw v0
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 6114
    :catch_4
    move-exception v0

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 6115
    :catchall_2
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_7
    if-eqz v8, :cond_7

    if-eqz v1, :cond_9

    :try_start_c
    invoke-virtual {v8}, LX/07u;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_7
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    :cond_7
    :goto_8
    :try_start_d
    throw v0
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    .line 6116
    :catch_5
    move-exception v0

    :try_start_e
    throw v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 6117
    :catchall_3
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_9
    if-eqz v3, :cond_a

    :try_start_f
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_8

    :goto_a
    throw v0

    .line 6118
    :catch_6
    move-exception v2

    :try_start_10
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 6119
    :catchall_4
    move-exception v0

    move-object v1, v3

    goto :goto_7

    .line 6120
    :cond_8
    invoke-virtual {v9}, LX/0Ay;->close()V
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    goto :goto_6

    .line 6121
    :catch_7
    move-exception v2

    :try_start_11
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_8

    .line 6122
    :catchall_5
    move-exception v0

    goto :goto_9

    .line 6123
    :cond_9
    invoke-virtual {v8}, LX/07u;->close()V
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_5
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    goto :goto_8

    .line 6124
    :catch_8
    move-exception v1

    invoke-static {v3, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_a

    :cond_a
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V

    goto :goto_a

    .line 6125
    :cond_b
    new-instance v0, Lcom/facebook/soloader/UnpackingSoSource$1;

    move-object v1, p0

    move-object v3, p3

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/soloader/UnpackingSoSource$1;-><init>(LX/025;Ljava/io/File;[BLX/0AO;Ljava/io/File;LX/01D;)V

    .line 6126
    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_c

    .line 6127
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SoSync:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :goto_b
    move v1, v7

    .line 6128
    goto/16 :goto_2

    .line 6129
    :cond_c
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_b

    .line 6130
    :catchall_6
    move-exception v0

    move-object v1, v3

    goto :goto_5

    .line 6131
    :catchall_7
    move-exception v0

    goto/16 :goto_3

    :cond_d
    move-object v4, v3

    goto/16 :goto_1
.end method

.method public static b(Ljava/io/File;B)V
    .locals 7

    .prologue
    .line 6203
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v0, "rw"

    invoke-direct {v2, p0, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 6204
    const-wide/16 v4, 0x0

    :try_start_0
    invoke-virtual {v2, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 6205
    invoke-virtual {v2, p1}, Ljava/io/RandomAccessFile;->write(I)V

    .line 6206
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 6207
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 6208
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V

    return-void

    .line 6209
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6210
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_0
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()LX/07u;
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 6071
    iget-object v0, p0, LX/021;->a:Ljava/io/File;

    invoke-static {v0}, LX/01z;->b(Ljava/io/File;)V

    .line 6072
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/021;->a:Ljava/io/File;

    const-string v2, "dso_lock"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 6073
    invoke-static {v0}, LX/01D;->a(Ljava/io/File;)LX/01D;

    move-result-object v1

    .line 6074
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "locked dso store "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6075
    invoke-virtual {p0}, LX/025;->b()[B

    move-result-object v0

    invoke-direct {p0, v1, p1, v0}, LX/025;->a(LX/01D;I[B)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 6076
    const/4 v0, 0x0

    .line 6077
    :goto_0
    if-eqz v0, :cond_1

    .line 6078
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "releasing dso store lock for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6079
    invoke-virtual {v0}, LX/01D;->close()V

    .line 6080
    :goto_1
    return-void

    .line 6081
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "dso store is up-to-date: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0

    .line 6082
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "not releasing dso store lock for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (syncer thread started)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 6083
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 6084
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "releasing dso store lock for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6085
    invoke-virtual {v1}, LX/01D;->close()V

    .line 6086
    :goto_2
    throw v0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "not releasing dso store lock for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/021;->a:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (syncer thread started)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public final a([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 6069
    iput-object p1, p0, LX/025;->c:[Ljava/lang/String;

    .line 6070
    return-void
.end method

.method public b()[B
    .locals 7

    .prologue
    .line 6051
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6052
    invoke-virtual {p0}, LX/025;->a()LX/07u;

    move-result-object v3

    const/4 v1, 0x0

    .line 6053
    :try_start_0
    invoke-virtual {v3}, LX/07u;->a()LX/0AO;

    move-result-object v0

    iget-object v4, v0, LX/0AO;->a:[LX/07w;

    .line 6054
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 6055
    array-length v0, v4

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6056
    const/4 v0, 0x0

    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    .line 6057
    aget-object v5, v4, v0

    iget-object v5, v5, LX/07w;->c:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6058
    aget-object v5, v4, v0

    iget-object v5, v5, LX/07w;->d:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 6059
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6060
    :cond_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/07u;->close()V

    .line 6061
    :cond_1
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    .line 6062
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6063
    return-object v0

    .line 6064
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6065
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v3, :cond_2

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v3}, LX/07u;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v3}, LX/07u;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 6066
    iget-object v0, p0, LX/025;->c:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 6067
    invoke-super {p0}, LX/021;->c()[Ljava/lang/String;

    move-result-object v0

    .line 6068
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/025;->c:[Ljava/lang/String;

    goto :goto_0
.end method
