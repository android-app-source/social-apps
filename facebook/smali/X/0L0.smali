.class public final LX/0L0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field public final a:Landroid/os/Handler;

.field private final b:Landroid/os/HandlerThread;

.field private final c:Landroid/os/Handler;

.field public final d:LX/0LA;

.field public final e:LX/0LA;

.field public final f:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0GT;",
            ">;"
        }
    .end annotation
.end field

.field public final h:[[LX/0L4;

.field public final i:[I

.field private final j:J

.field private final k:J

.field public l:[LX/0GT;

.field public m:LX/0GT;

.field public n:LX/0GW;

.field private o:Z

.field public p:Z

.field private q:Z

.field public r:I

.field public s:I

.field private t:I

.field public u:J

.field private v:J

.field public volatile w:J

.field public volatile x:J

.field public volatile y:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;Z[III)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    .line 42760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42761
    iput v0, p0, LX/0L0;->s:I

    .line 42762
    iput v0, p0, LX/0L0;->t:I

    .line 42763
    iput-object p1, p0, LX/0L0;->c:Landroid/os/Handler;

    .line 42764
    iput-boolean p2, p0, LX/0L0;->p:Z

    .line 42765
    int-to-long v0, p4

    mul-long/2addr v0, v4

    iput-wide v0, p0, LX/0L0;->j:J

    .line 42766
    int-to-long v0, p5

    mul-long/2addr v0, v4

    iput-wide v0, p0, LX/0L0;->k:J

    .line 42767
    array-length v0, p3

    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, LX/0L0;->i:[I

    .line 42768
    const/4 v0, 0x1

    iput v0, p0, LX/0L0;->r:I

    .line 42769
    iput-wide v2, p0, LX/0L0;->w:J

    .line 42770
    iput-wide v2, p0, LX/0L0;->y:J

    .line 42771
    new-instance v0, LX/0LA;

    invoke-direct {v0}, LX/0LA;-><init>()V

    iput-object v0, p0, LX/0L0;->d:LX/0LA;

    .line 42772
    new-instance v0, LX/0LA;

    invoke-direct {v0}, LX/0LA;-><init>()V

    iput-object v0, p0, LX/0L0;->e:LX/0LA;

    .line 42773
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/0L0;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 42774
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0L0;->g:Ljava/util/List;

    .line 42775
    array-length v0, p3

    new-array v0, v0, [[LX/0L4;

    iput-object v0, p0, LX/0L0;->h:[[LX/0L4;

    .line 42776
    new-instance v0, Lcom/google/android/exoplayer/util/PriorityHandlerThread;

    const-string v1, "ExoPlayerImplInternal:Handler"

    const/16 v2, -0x10

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer/util/PriorityHandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, LX/0L0;->b:Landroid/os/HandlerThread;

    .line 42777
    iget-object v0, p0, LX/0L0;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 42778
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, LX/0L0;->b:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, LX/0L0;->a:Landroid/os/Handler;

    .line 42779
    return-void
.end method

.method private a(ILjava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42780
    :try_start_0
    check-cast p2, Landroid/util/Pair;

    .line 42781
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/0GS;

    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v0, p1, v1}, LX/0GS;->a(ILjava/lang/Object;)V

    .line 42782
    iget v0, p0, LX/0L0;->r:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/0L0;->r:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 42783
    iget-object v0, p0, LX/0L0;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 42784
    :cond_0
    monitor-enter p0

    .line 42785
    :try_start_1
    iget v0, p0, LX/0L0;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0L0;->t:I

    .line 42786
    const v0, -0x389d08c1

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 42787
    monitor-exit p0

    .line 42788
    return-void

    .line 42789
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 42790
    :catchall_1
    move-exception v0

    monitor-enter p0

    .line 42791
    :try_start_2
    iget v1, p0, LX/0L0;->t:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0L0;->t:I

    .line 42792
    const v1, 0x5c71232f

    invoke-static {p0, v1}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 42793
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    :catchall_2
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0
.end method

.method public static a(LX/0L0;I)V
    .locals 3

    .prologue
    .line 42794
    iget v0, p0, LX/0L0;->r:I

    if-eq v0, p1, :cond_0

    .line 42795
    iput p1, p0, LX/0L0;->r:I

    .line 42796
    iget-object v0, p0, LX/0L0;->c:Landroid/os/Handler;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 42797
    :cond_0
    return-void
.end method

.method private static a(LX/0L0;IJJ)V
    .locals 4

    .prologue
    .line 42798
    add-long v0, p2, p4

    .line 42799
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 42800
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 42801
    iget-object v0, p0, LX/0L0;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 42802
    :goto_0
    return-void

    .line 42803
    :cond_0
    iget-object v2, p0, LX/0L0;->a:Landroid/os/Handler;

    invoke-virtual {v2, p1, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public static a(LX/0L0;LX/0GT;IZ)V
    .locals 4

    .prologue
    .line 42844
    iget-wide v0, p0, LX/0L0;->x:J

    const/4 v2, 0x1

    .line 42845
    iget v3, p1, LX/0GT;->a:I

    if-ne v3, v2, :cond_2

    :goto_0
    invoke-static {v2}, LX/0Av;->b(Z)V

    .line 42846
    const/4 v2, 0x2

    iput v2, p1, LX/0GT;->a:I

    .line 42847
    invoke-virtual {p1, p2, v0, v1, p3}, LX/0GT;->a(IJZ)V

    .line 42848
    iget-object v0, p0, LX/0L0;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42849
    invoke-virtual {p1}, LX/0GT;->g()LX/0GW;

    move-result-object v1

    .line 42850
    if-eqz v1, :cond_0

    .line 42851
    iget-object v0, p0, LX/0L0;->n:LX/0GW;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 42852
    iput-object v1, p0, LX/0L0;->n:LX/0GW;

    .line 42853
    iput-object p1, p0, LX/0L0;->m:LX/0GT;

    .line 42854
    :cond_0
    return-void

    .line 42855
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 42856
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static a(LX/0L0;LX/0GT;)Z
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 42804
    invoke-virtual {p1}, LX/0GT;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 42805
    :cond_0
    :goto_0
    return v0

    .line 42806
    :cond_1
    invoke-virtual {p1}, LX/0GT;->c()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 42807
    goto :goto_0

    .line 42808
    :cond_2
    iget v2, p0, LX/0L0;->r:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    .line 42809
    invoke-virtual {p1}, LX/0GT;->e()J

    move-result-wide v4

    .line 42810
    invoke-virtual {p1}, LX/0GT;->f()J

    move-result-wide v6

    .line 42811
    iget-boolean v2, p0, LX/0L0;->q:Z

    if-eqz v2, :cond_4

    iget-wide v2, p0, LX/0L0;->k:J

    .line 42812
    :goto_1
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-lez v8, :cond_0

    cmp-long v8, v6, v10

    if-eqz v8, :cond_0

    const-wide/16 v8, -0x3

    cmp-long v8, v6, v8

    if-eqz v8, :cond_0

    iget-wide v8, p0, LX/0L0;->x:J

    add-long/2addr v2, v8

    cmp-long v2, v6, v2

    if-gez v2, :cond_0

    cmp-long v2, v4, v10

    if-eqz v2, :cond_3

    const-wide/16 v2, -0x2

    cmp-long v2, v4, v2

    if-eqz v2, :cond_3

    cmp-long v2, v6, v4

    if-gez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 42813
    :cond_4
    iget-wide v2, p0, LX/0L0;->j:J

    goto :goto_1
.end method

.method private b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 42814
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/0L0;->q:Z

    .line 42815
    iput-boolean p1, p0, LX/0L0;->p:Z

    .line 42816
    if-nez p1, :cond_1

    .line 42817
    invoke-static {p0}, LX/0L0;->i(LX/0L0;)V

    .line 42818
    invoke-static {p0}, LX/0L0;->j(LX/0L0;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42819
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0L0;->c:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 42820
    return-void

    .line 42821
    :cond_1
    :try_start_1
    iget v0, p0, LX/0L0;->r:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 42822
    invoke-static {p0}, LX/0L0;->h(LX/0L0;)V

    .line 42823
    iget-object v0, p0, LX/0L0;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 42824
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0L0;->c:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    throw v0

    .line 42825
    :cond_2
    :try_start_2
    iget v0, p0, LX/0L0;->r:I

    if-ne v0, v2, :cond_0

    .line 42826
    iget-object v0, p0, LX/0L0;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private c(J)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v0, 0x0

    .line 42827
    :try_start_0
    iget-wide v2, p0, LX/0L0;->x:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    .line 42828
    iget-object v0, p0, LX/0L0;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 42829
    :goto_0
    return-void

    .line 42830
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, LX/0L0;->q:Z

    .line 42831
    mul-long v2, p1, v6

    iput-wide v2, p0, LX/0L0;->x:J

    .line 42832
    iget-object v1, p0, LX/0L0;->d:LX/0LA;

    invoke-virtual {v1}, LX/0LA;->c()V

    .line 42833
    iget-object v1, p0, LX/0L0;->d:LX/0LA;

    iget-wide v2, p0, LX/0L0;->x:J

    invoke-virtual {v1, v2, v3}, LX/0LA;->a(J)V

    .line 42834
    iget v1, p0, LX/0L0;->r:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, LX/0L0;->r:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 42835
    :cond_1
    iget-object v0, p0, LX/0L0;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    goto :goto_0

    :cond_2
    move v1, v0

    .line 42836
    :goto_1
    :try_start_2
    iget-object v0, p0, LX/0L0;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 42837
    iget-object v0, p0, LX/0L0;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GT;

    .line 42838
    invoke-static {v0}, LX/0L0;->d(LX/0GT;)V

    .line 42839
    iget-wide v2, p0, LX/0L0;->x:J

    invoke-virtual {v0, v2, v3}, LX/0GT;->b(J)V

    .line 42840
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 42841
    :cond_3
    const/4 v0, 0x3

    invoke-static {p0, v0}, LX/0L0;->a(LX/0L0;I)V

    .line 42842
    iget-object v0, p0, LX/0L0;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 42843
    iget-object v0, p0, LX/0L0;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0L0;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    throw v0
.end method

.method private static d(LX/0GT;)V
    .locals 2

    .prologue
    .line 42741
    iget v0, p0, LX/0GT;->a:I

    move v0, v0

    .line 42742
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 42743
    iget v0, p0, LX/0GT;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 42744
    const/4 v0, 0x2

    iput v0, p0, LX/0GT;->a:I

    .line 42745
    invoke-virtual {p0}, LX/0GT;->i()V

    .line 42746
    :cond_0
    return-void

    .line 42747
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/0L0;LX/0GT;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 42748
    invoke-static {p1}, LX/0L0;->d(LX/0GT;)V

    .line 42749
    iget v0, p1, LX/0GT;->a:I

    move v0, v0

    .line 42750
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 42751
    const/4 v1, 0x1

    .line 42752
    iget v0, p1, LX/0GT;->a:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 42753
    iput v1, p1, LX/0GT;->a:I

    .line 42754
    invoke-virtual {p1}, LX/0GT;->j()V

    .line 42755
    iget-object v0, p0, LX/0L0;->m:LX/0GT;

    if-ne p1, v0, :cond_0

    .line 42756
    iput-object v2, p0, LX/0L0;->n:LX/0GW;

    .line 42757
    iput-object v2, p0, LX/0L0;->m:LX/0GT;

    .line 42758
    :cond_0
    return-void

    .line 42759
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(LX/0L0;)V
    .locals 14

    .prologue
    .line 42507
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 42508
    const/4 v1, 0x1

    .line 42509
    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, LX/0L0;->l:[LX/0GT;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 42510
    iget-object v4, p0, LX/0L0;->l:[LX/0GT;

    aget-object v4, v4, v0

    .line 42511
    iget v5, v4, LX/0GT;->a:I

    move v5, v5

    .line 42512
    if-nez v5, :cond_0

    .line 42513
    iget-wide v6, p0, LX/0L0;->x:J

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 42514
    iget v5, v4, LX/0GT;->a:I

    if-nez v5, :cond_e

    move v5, v8

    :goto_1
    invoke-static {v5}, LX/0Av;->b(Z)V

    .line 42515
    invoke-virtual {v4, v6, v7}, LX/0GT;->a(J)Z

    move-result v5

    if-eqz v5, :cond_f

    :goto_2
    iput v8, v4, LX/0GT;->a:I

    .line 42516
    iget v5, v4, LX/0GT;->a:I

    move v5, v5

    .line 42517
    if-nez v5, :cond_0

    .line 42518
    invoke-virtual {v4}, LX/0GT;->d()V

    .line 42519
    const/4 v1, 0x0

    .line 42520
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42521
    :cond_1
    if-nez v1, :cond_2

    .line 42522
    const/4 v1, 0x2

    const-wide/16 v4, 0xa

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/0L0;->a(LX/0L0;IJJ)V

    .line 42523
    :goto_3
    return-void

    .line 42524
    :cond_2
    const-wide/16 v2, 0x0

    .line 42525
    const/4 v4, 0x1

    .line 42526
    const/4 v1, 0x1

    .line 42527
    const/4 v0, 0x0

    :goto_4
    iget-object v5, p0, LX/0L0;->l:[LX/0GT;

    array-length v5, v5

    if-ge v0, v5, :cond_9

    .line 42528
    iget-object v5, p0, LX/0L0;->l:[LX/0GT;

    aget-object v6, v5, v0

    .line 42529
    invoke-virtual {v6}, LX/0GT;->X_()I

    move-result v7

    .line 42530
    new-array v8, v7, [LX/0L4;

    .line 42531
    const/4 v5, 0x0

    :goto_5
    if-ge v5, v7, :cond_3

    .line 42532
    invoke-virtual {v6, v5}, LX/0GT;->a(I)LX/0L4;

    move-result-object v9

    aput-object v9, v8, v5

    .line 42533
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 42534
    :cond_3
    iget-object v5, p0, LX/0L0;->h:[[LX/0L4;

    aput-object v8, v5, v0

    .line 42535
    if-lez v7, :cond_5

    .line 42536
    const-wide/16 v10, -0x1

    cmp-long v5, v2, v10

    if-eqz v5, :cond_4

    .line 42537
    invoke-virtual {v6}, LX/0GT;->e()J

    move-result-wide v10

    .line 42538
    const-wide/16 v12, -0x1

    cmp-long v5, v10, v12

    if-nez v5, :cond_6

    .line 42539
    const-wide/16 v2, -0x1

    .line 42540
    :cond_4
    :goto_6
    iget-object v5, p0, LX/0L0;->i:[I

    aget v5, v5, v0

    .line 42541
    if-ltz v5, :cond_5

    array-length v7, v8

    if-ge v5, v7, :cond_5

    .line 42542
    const/4 v7, 0x0

    invoke-static {p0, v6, v5, v7}, LX/0L0;->a(LX/0L0;LX/0GT;IZ)V

    .line 42543
    if-eqz v4, :cond_7

    invoke-virtual {v6}, LX/0GT;->b()Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    .line 42544
    :goto_7
    if-eqz v1, :cond_8

    invoke-static {p0, v6}, LX/0L0;->a(LX/0L0;LX/0GT;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    .line 42545
    :cond_5
    :goto_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 42546
    :cond_6
    const-wide/16 v12, -0x2

    cmp-long v5, v10, v12

    if-eqz v5, :cond_4

    .line 42547
    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    goto :goto_6

    .line 42548
    :cond_7
    const/4 v4, 0x0

    goto :goto_7

    .line 42549
    :cond_8
    const/4 v1, 0x0

    goto :goto_8

    .line 42550
    :cond_9
    iput-wide v2, p0, LX/0L0;->w:J

    .line 42551
    if-eqz v4, :cond_c

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_a

    iget-wide v4, p0, LX/0L0;->x:J

    cmp-long v0, v2, v4

    if-gtz v0, :cond_c

    .line 42552
    :cond_a
    const/4 v0, 0x5

    move-object v1, p0

    .line 42553
    :goto_9
    iput v0, v1, LX/0L0;->r:I

    .line 42554
    iget-object v0, p0, LX/0L0;->c:Landroid/os/Handler;

    const/4 v1, 0x1

    iget v2, p0, LX/0L0;->r:I

    const/4 v3, 0x0

    iget-object v4, p0, LX/0L0;->h:[[LX/0L4;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 42555
    iget-boolean v0, p0, LX/0L0;->p:Z

    if-eqz v0, :cond_b

    iget v0, p0, LX/0L0;->r:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_b

    .line 42556
    invoke-static {p0}, LX/0L0;->h(LX/0L0;)V

    .line 42557
    :cond_b
    iget-object v0, p0, LX/0L0;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_3

    .line 42558
    :cond_c
    if-eqz v1, :cond_d

    const/4 v0, 0x4

    move-object v1, p0

    goto :goto_9

    :cond_d
    const/4 v0, 0x3

    move-object v1, p0

    goto :goto_9

    :cond_e
    move v5, v9

    .line 42559
    goto/16 :goto_1

    :cond_f
    move v8, v9

    .line 42560
    goto/16 :goto_2
.end method

.method private static h(LX/0L0;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 42561
    iput-boolean v0, p0, LX/0L0;->q:Z

    .line 42562
    iget-object v1, p0, LX/0L0;->d:LX/0LA;

    invoke-virtual {v1}, LX/0LA;->b()V

    .line 42563
    iget-object v1, p0, LX/0L0;->e:LX/0LA;

    invoke-virtual {v1}, LX/0LA;->b()V

    move v1, v0

    .line 42564
    :goto_0
    iget-object v0, p0, LX/0L0;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 42565
    iget-object v0, p0, LX/0L0;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GT;

    invoke-virtual {v0}, LX/0GT;->s()V

    .line 42566
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 42567
    :cond_0
    return-void
.end method

.method private static i(LX/0L0;)V
    .locals 2

    .prologue
    .line 42568
    iget-object v0, p0, LX/0L0;->d:LX/0LA;

    invoke-virtual {v0}, LX/0LA;->c()V

    .line 42569
    iget-object v0, p0, LX/0L0;->e:LX/0LA;

    invoke-virtual {v0}, LX/0LA;->c()V

    .line 42570
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0L0;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 42571
    iget-object v0, p0, LX/0L0;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GT;

    invoke-static {v0}, LX/0L0;->d(LX/0GT;)V

    .line 42572
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 42573
    :cond_0
    return-void
.end method

.method private static j(LX/0L0;)V
    .locals 4

    .prologue
    .line 42574
    iget-object v0, p0, LX/0L0;->n:LX/0GW;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0L0;->g:Ljava/util/List;

    iget-object v1, p0, LX/0L0;->m:LX/0GT;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0L0;->m:LX/0GT;

    invoke-virtual {v0}, LX/0GT;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42575
    iget-object v0, p0, LX/0L0;->n:LX/0GW;

    invoke-interface {v0}, LX/0GW;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0L0;->x:J

    .line 42576
    iget-object v0, p0, LX/0L0;->d:LX/0LA;

    iget-wide v2, p0, LX/0L0;->x:J

    invoke-virtual {v0, v2, v3}, LX/0LA;->a(J)V

    .line 42577
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/0L0;->v:J

    .line 42578
    return-void

    .line 42579
    :cond_0
    iget-object v0, p0, LX/0L0;->d:LX/0LA;

    invoke-virtual {v0}, LX/0LA;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0L0;->x:J

    goto :goto_0
.end method

.method private k()V
    .locals 15

    .prologue
    .line 42580
    const-string v0, "doSomeWork"

    invoke-static {v0}, LX/08K;->a(Ljava/lang/String;)V

    .line 42581
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 42582
    iget-wide v0, p0, LX/0L0;->w:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    iget-wide v0, p0, LX/0L0;->w:J

    .line 42583
    :goto_0
    const/4 v6, 0x1

    .line 42584
    const/4 v5, 0x1

    .line 42585
    invoke-static {p0}, LX/0L0;->j(LX/0L0;)V

    .line 42586
    const/4 v4, 0x0

    move v14, v4

    move v4, v5

    move v5, v6

    move-wide v6, v0

    move v1, v14

    :goto_1
    iget-object v0, p0, LX/0L0;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 42587
    iget-object v0, p0, LX/0L0;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GT;

    .line 42588
    iget-wide v8, p0, LX/0L0;->x:J

    iget-wide v10, p0, LX/0L0;->v:J

    invoke-virtual {v0, v8, v9, v10, v11}, LX/0GT;->a(JJ)V

    .line 42589
    if-eqz v5, :cond_3

    invoke-virtual {v0}, LX/0GT;->b()Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v5, 0x1

    .line 42590
    :goto_2
    invoke-static {p0, v0}, LX/0L0;->a(LX/0L0;LX/0GT;)Z

    move-result v8

    .line 42591
    if-nez v8, :cond_0

    .line 42592
    invoke-virtual {v0}, LX/0GT;->d()V

    .line 42593
    :cond_0
    if-eqz v4, :cond_4

    if-eqz v8, :cond_4

    const/4 v4, 0x1

    .line 42594
    :goto_3
    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-eqz v8, :cond_1

    .line 42595
    invoke-virtual {v0}, LX/0GT;->e()J

    move-result-wide v8

    .line 42596
    invoke-virtual {v0}, LX/0GT;->f()J

    move-result-wide v10

    .line 42597
    const-wide/16 v12, -0x1

    cmp-long v0, v10, v12

    if-nez v0, :cond_5

    .line 42598
    const-wide/16 v6, -0x1

    .line 42599
    :cond_1
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 42600
    :cond_2
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    .line 42601
    :cond_3
    const/4 v5, 0x0

    goto :goto_2

    .line 42602
    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    .line 42603
    :cond_5
    const-wide/16 v12, -0x3

    cmp-long v0, v10, v12

    if-eqz v0, :cond_1

    const-wide/16 v12, -0x1

    cmp-long v0, v8, v12

    if-eqz v0, :cond_6

    const-wide/16 v12, -0x2

    cmp-long v0, v8, v12

    if-eqz v0, :cond_6

    cmp-long v0, v10, v8

    if-gez v0, :cond_1

    .line 42604
    :cond_6
    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    goto :goto_4

    .line 42605
    :cond_7
    iput-wide v6, p0, LX/0L0;->y:J

    .line 42606
    if-eqz v5, :cond_d

    iget-wide v0, p0, LX/0L0;->w:J

    const-wide/16 v6, -0x1

    cmp-long v0, v0, v6

    if-eqz v0, :cond_8

    iget-wide v0, p0, LX/0L0;->w:J

    iget-wide v6, p0, LX/0L0;->x:J

    cmp-long v0, v0, v6

    if-gtz v0, :cond_d

    .line 42607
    :cond_8
    const/4 v0, 0x5

    invoke-static {p0, v0}, LX/0L0;->a(LX/0L0;I)V

    .line 42608
    invoke-static {p0}, LX/0L0;->i(LX/0L0;)V

    .line 42609
    :cond_9
    :goto_5
    iget-object v0, p0, LX/0L0;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 42610
    iget-boolean v0, p0, LX/0L0;->p:Z

    if-eqz v0, :cond_a

    iget v0, p0, LX/0L0;->r:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_b

    :cond_a
    iget v0, p0, LX/0L0;->r:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_f

    .line 42611
    :cond_b
    const/4 v1, 0x7

    const-wide/16 v4, 0xa

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/0L0;->a(LX/0L0;IJJ)V

    .line 42612
    :cond_c
    :goto_6
    invoke-static {}, LX/08K;->a()V

    .line 42613
    return-void

    .line 42614
    :cond_d
    iget v0, p0, LX/0L0;->r:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_e

    if-eqz v4, :cond_e

    .line 42615
    const/4 v0, 0x4

    invoke-static {p0, v0}, LX/0L0;->a(LX/0L0;I)V

    .line 42616
    iget-boolean v0, p0, LX/0L0;->p:Z

    if-eqz v0, :cond_9

    .line 42617
    invoke-static {p0}, LX/0L0;->h(LX/0L0;)V

    goto :goto_5

    .line 42618
    :cond_e
    iget v0, p0, LX/0L0;->r:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    if-nez v4, :cond_9

    .line 42619
    iget-boolean v0, p0, LX/0L0;->p:Z

    iput-boolean v0, p0, LX/0L0;->q:Z

    .line 42620
    const/4 v0, 0x3

    invoke-static {p0, v0}, LX/0L0;->a(LX/0L0;I)V

    .line 42621
    invoke-static {p0}, LX/0L0;->i(LX/0L0;)V

    goto :goto_5

    .line 42622
    :cond_f
    iget-object v0, p0, LX/0L0;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 42623
    const/4 v1, 0x7

    const-wide/16 v4, 0x3e8

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/0L0;->a(LX/0L0;IJJ)V

    goto :goto_6
.end method

.method private l()V
    .locals 1

    .prologue
    .line 42624
    invoke-static {p0}, LX/0L0;->n(LX/0L0;)V

    .line 42625
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/0L0;->a(LX/0L0;I)V

    .line 42626
    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 42627
    invoke-static {p0}, LX/0L0;->n(LX/0L0;)V

    .line 42628
    invoke-static {p0, v0}, LX/0L0;->a(LX/0L0;I)V

    .line 42629
    monitor-enter p0

    .line 42630
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/0L0;->o:Z

    .line 42631
    const v0, 0x7ab866cf

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 42632
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static n(LX/0L0;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 42633
    iget-object v1, p0, LX/0L0;->a:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 42634
    iget-object v1, p0, LX/0L0;->a:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 42635
    iput-boolean v0, p0, LX/0L0;->q:Z

    .line 42636
    iget-object v1, p0, LX/0L0;->d:LX/0LA;

    invoke-virtual {v1}, LX/0LA;->c()V

    .line 42637
    iget-object v1, p0, LX/0L0;->e:LX/0LA;

    invoke-virtual {v1}, LX/0LA;->c()V

    .line 42638
    iget-object v1, p0, LX/0L0;->l:[LX/0GT;

    if-nez v1, :cond_0

    .line 42639
    :goto_0
    return-void

    .line 42640
    :cond_0
    :goto_1
    iget-object v1, p0, LX/0L0;->l:[LX/0GT;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 42641
    iget-object v1, p0, LX/0L0;->l:[LX/0GT;

    aget-object v1, v1, v0

    .line 42642
    :try_start_0
    invoke-static {p0, v1}, LX/0L0;->e(LX/0L0;LX/0GT;)V
    :try_end_0
    .catch LX/0Kv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 42643
    :goto_2
    :try_start_1
    const/4 v5, -0x1

    .line 42644
    iget v2, v1, LX/0GT;->a:I

    const/4 v4, 0x2

    if-eq v2, v4, :cond_2

    iget v2, v1, LX/0GT;->a:I

    const/4 v4, 0x3

    if-eq v2, v4, :cond_2

    iget v2, v1, LX/0GT;->a:I

    if-eq v2, v5, :cond_2

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, LX/0Av;->b(Z)V

    .line 42645
    iput v5, v1, LX/0GT;->a:I

    .line 42646
    invoke-virtual {v1}, LX/0GT;->q()V
    :try_end_1
    .catch LX/0Kv; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_3

    .line 42647
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 42648
    :cond_1
    iput-object v3, p0, LX/0L0;->l:[LX/0GT;

    .line 42649
    iput-object v3, p0, LX/0L0;->n:LX/0GW;

    .line 42650
    iput-object v3, p0, LX/0L0;->m:LX/0GT;

    .line 42651
    iget-object v0, p0, LX/0L0;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 42652
    :catch_0
    move-exception v2

    .line 42653
    const-string v4, "ExoPlayerImplInternal"

    const-string v5, "Stop failed."

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 42654
    :catch_1
    move-exception v2

    .line 42655
    const-string v4, "ExoPlayerImplInternal"

    const-string v5, "Stop failed."

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 42656
    :catch_2
    move-exception v2

    .line 42657
    const-string v4, "ExoPlayerImplInternal"

    const-string v5, "Release failed."

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 42658
    :catch_3
    move-exception v2

    .line 42659
    const-string v4, "ExoPlayerImplInternal"

    const-string v5, "Release failed."

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 42660
    :cond_2
    const/4 v2, 0x0

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized b(LX/0GS;ILjava/lang/Object;)V
    .locals 5

    .prologue
    .line 42661
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0L0;->o:Z

    if-eqz v0, :cond_1

    .line 42662
    const-string v0, "ExoPlayerImplInternal"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sent message("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") after release. Message ignored."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42663
    :cond_0
    monitor-exit p0

    return-void

    .line 42664
    :cond_1
    :try_start_1
    iget v0, p0, LX/0L0;->s:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/0L0;->s:I

    .line 42665
    iget-object v1, p0, LX/0L0;->a:Landroid/os/Handler;

    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-static {p1, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-virtual {v1, v2, p2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 42666
    :goto_0
    iget v1, p0, LX/0L0;->t:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-gt v1, v0, :cond_0

    .line 42667
    const v1, -0x6ed6944

    :try_start_2
    invoke-static {p0, v1}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 42668
    :catch_0
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 42669
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 2

    .prologue
    .line 42670
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0L0;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 42671
    :goto_0
    monitor-exit p0

    return-void

    .line 42672
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0L0;->a:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 42673
    :goto_1
    iget-boolean v0, p0, LX/0L0;->o:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 42674
    const v0, 0x41d1702c

    :try_start_2
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 42675
    :catch_0
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 42676
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 42677
    :cond_1
    :try_start_4
    iget-object v0, p0, LX/0L0;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 13

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 42678
    :try_start_0
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 42679
    :goto_0
    return v0

    .line 42680
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [LX/0GT;

    check-cast v0, [LX/0GT;

    .line 42681
    invoke-static {p0}, LX/0L0;->n(LX/0L0;)V

    .line 42682
    iput-object v0, p0, LX/0L0;->l:[LX/0GT;

    .line 42683
    iget-object v2, p0, LX/0L0;->h:[[LX/0L4;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 42684
    const/4 v2, 0x2

    invoke-static {p0, v2}, LX/0L0;->a(LX/0L0;I)V

    .line 42685
    invoke-static {p0}, LX/0L0;->g(LX/0L0;)V

    .line 42686
    move v0, v1

    .line 42687
    goto :goto_0

    .line 42688
    :pswitch_1
    invoke-static {p0}, LX/0L0;->g(LX/0L0;)V

    move v0, v1

    .line 42689
    goto :goto_0

    .line 42690
    :pswitch_2
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    invoke-direct {p0, v0}, LX/0L0;->b(Z)V

    move v0, v1

    .line 42691
    goto :goto_0

    .line 42692
    :pswitch_3
    invoke-direct {p0}, LX/0L0;->k()V

    move v0, v1

    .line 42693
    goto :goto_0

    .line 42694
    :pswitch_4
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    .line 42695
    int-to-long v5, v0

    const/16 v7, 0x20

    shl-long/2addr v5, v7

    int-to-long v7, v2

    const-wide v9, 0xffffffffL

    and-long/2addr v7, v9

    or-long/2addr v5, v7

    move-wide v2, v5

    .line 42696
    invoke-direct {p0, v2, v3}, LX/0L0;->c(J)V

    move v0, v1

    .line 42697
    goto :goto_0

    .line 42698
    :pswitch_5
    invoke-direct {p0}, LX/0L0;->l()V

    move v0, v1

    .line 42699
    goto :goto_0

    .line 42700
    :pswitch_6
    invoke-direct {p0}, LX/0L0;->m()V

    move v0, v1

    .line 42701
    goto :goto_0

    .line 42702
    :pswitch_7
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v0, v2}, LX/0L0;->a(ILjava/lang/Object;)V

    move v0, v1

    .line 42703
    goto :goto_0

    .line 42704
    :pswitch_8
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    const/4 v10, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 42705
    iget-object v7, p0, LX/0L0;->i:[I

    aget v7, v7, v0

    if-ne v7, v2, :cond_2
    :try_end_0
    .catch LX/0Kv; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 42706
    :cond_1
    :goto_1
    move v0, v1

    .line 42707
    goto :goto_0

    .line 42708
    :catch_0
    move-exception v0

    .line 42709
    const-string v2, "ExoPlayerImplInternal"

    const-string v3, "Internal track renderer error."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 42710
    iget-object v2, p0, LX/0L0;->c:Landroid/os/Handler;

    invoke-virtual {v2, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 42711
    invoke-direct {p0}, LX/0L0;->l()V

    move v0, v1

    .line 42712
    goto :goto_0

    .line 42713
    :catch_1
    move-exception v0

    .line 42714
    const-string v2, "ExoPlayerImplInternal"

    const-string v3, "Internal runtime error."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 42715
    iget-object v2, p0, LX/0L0;->c:Landroid/os/Handler;

    new-instance v3, LX/0Kv;

    invoke-direct {v3, v0, v1}, LX/0Kv;-><init>(Ljava/lang/Throwable;Z)V

    invoke-virtual {v2, v4, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 42716
    invoke-direct {p0}, LX/0L0;->l()V

    move v0, v1

    .line 42717
    goto/16 :goto_0

    .line 42718
    :cond_2
    iget-object v7, p0, LX/0L0;->i:[I

    aput v2, v7, v0

    .line 42719
    iget v7, p0, LX/0L0;->r:I

    if-eq v7, v5, :cond_1

    iget v7, p0, LX/0L0;->r:I

    if-eq v7, v10, :cond_1

    .line 42720
    iget-object v7, p0, LX/0L0;->l:[LX/0GT;

    aget-object v9, v7, v0

    .line 42721
    iget v7, v9, LX/0GT;->a:I

    move v7, v7

    .line 42722
    if-eqz v7, :cond_1

    const/4 v8, -0x1

    if-eq v7, v8, :cond_1

    invoke-virtual {v9}, LX/0GT;->X_()I

    move-result v8

    if-eqz v8, :cond_1

    .line 42723
    if-eq v7, v10, :cond_3

    const/4 v8, 0x3

    if-ne v7, v8, :cond_7

    :cond_3
    move v8, v5

    .line 42724
    :goto_2
    if-ltz v2, :cond_8

    iget-object v7, p0, LX/0L0;->h:[[LX/0L4;

    aget-object v7, v7, v0

    array-length v7, v7

    if-ge v2, v7, :cond_8

    move v7, v5

    .line 42725
    :goto_3
    if-eqz v8, :cond_5

    .line 42726
    if-nez v7, :cond_4

    iget-object v10, p0, LX/0L0;->m:LX/0GT;

    if-ne v9, v10, :cond_4

    .line 42727
    iget-object v10, p0, LX/0L0;->d:LX/0LA;

    iget-object v11, p0, LX/0L0;->n:LX/0GW;

    invoke-interface {v11}, LX/0GW;->a()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, LX/0LA;->a(J)V

    .line 42728
    :cond_4
    invoke-static {p0, v9}, LX/0L0;->e(LX/0L0;LX/0GT;)V

    .line 42729
    iget-object v10, p0, LX/0L0;->g:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 42730
    :cond_5
    if-eqz v7, :cond_1

    .line 42731
    iget-boolean v7, p0, LX/0L0;->p:Z

    if-eqz v7, :cond_9

    iget v7, p0, LX/0L0;->r:I

    const/4 v10, 0x4

    if-ne v7, v10, :cond_9

    move v7, v5

    .line 42732
    :goto_4
    if-nez v8, :cond_a

    if-eqz v7, :cond_a

    .line 42733
    :goto_5
    invoke-static {p0, v9, v2, v5}, LX/0L0;->a(LX/0L0;LX/0GT;IZ)V

    .line 42734
    if-eqz v7, :cond_6

    .line 42735
    invoke-virtual {v9}, LX/0GT;->s()V

    .line 42736
    :cond_6
    iget-object v5, p0, LX/0L0;->a:Landroid/os/Handler;

    const/4 v6, 0x7

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    :cond_7
    move v8, v6

    .line 42737
    goto :goto_2

    :cond_8
    move v7, v6

    .line 42738
    goto :goto_3

    :cond_9
    move v7, v6

    .line 42739
    goto :goto_4

    :cond_a
    move v5, v6

    .line 42740
    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method
