.class public LX/08i;
.super LX/16B;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile n:LX/08i;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/09k;

.field public final d:LX/0Xl;

.field public final e:Landroid/content/Context;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0dC;

.field public final h:Ljava/util/concurrent/ExecutorService;

.field public final i:LX/09m;

.field public final j:LX/09n;

.field private final k:Ljava/lang/Runnable;

.field public final l:Landroid/content/Intent;

.field public m:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21534
    const-class v0, LX/08i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/08i;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/09k;LX/0Xl;LX/0Or;LX/0dC;Ljava/util/concurrent/ExecutorService;Landroid/content/Context;LX/1qm;)V
    .locals 4
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/09k;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "Ljava/util/concurrent/ExecutorService;",
            "Landroid/content/Context;",
            "LX/1qm;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 21453
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 21454
    new-instance v0, Lcom/facebook/rti/orca/FbnsLiteInitializer$1;

    invoke-direct {v0, p0}, Lcom/facebook/rti/orca/FbnsLiteInitializer$1;-><init>(LX/08i;)V

    iput-object v0, p0, LX/08i;->k:Ljava/lang/Runnable;

    .line 21455
    iput-object p1, p0, LX/08i;->b:LX/0Or;

    .line 21456
    iput-object p2, p0, LX/08i;->c:LX/09k;

    .line 21457
    iput-object p3, p0, LX/08i;->d:LX/0Xl;

    .line 21458
    iput-object p4, p0, LX/08i;->f:LX/0Or;

    .line 21459
    iput-object p5, p0, LX/08i;->g:LX/0dC;

    .line 21460
    iput-object p6, p0, LX/08i;->h:Ljava/util/concurrent/ExecutorService;

    .line 21461
    iput-object p7, p0, LX/08i;->e:Landroid/content/Context;

    .line 21462
    new-instance v0, LX/09m;

    invoke-direct {v0, p7}, LX/09m;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/08i;->i:LX/09m;

    .line 21463
    new-instance v0, LX/09n;

    iget-object v1, p0, LX/08i;->e:Landroid/content/Context;

    iget-object v2, p0, LX/08i;->i:LX/09m;

    invoke-virtual {p8}, LX/1qm;->b()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/09n;-><init>(Landroid/content/Context;LX/09m;I)V

    iput-object v0, p0, LX/08i;->j:LX/09n;

    .line 21464
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, LX/08i;->l:Landroid/content/Intent;

    .line 21465
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, LX/08i;->e:Landroid/content/Context;

    const-class v2, Lcom/facebook/rti/orca/MainService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 21466
    iget-object v1, p0, LX/08i;->l:Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 21467
    return-void
.end method

.method public static a(LX/0QB;)LX/08i;
    .locals 3

    .prologue
    .line 21535
    sget-object v0, LX/08i;->n:LX/08i;

    if-nez v0, :cond_1

    .line 21536
    const-class v1, LX/08i;

    monitor-enter v1

    .line 21537
    :try_start_0
    sget-object v0, LX/08i;->n:LX/08i;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 21538
    if-eqz v2, :cond_0

    .line 21539
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/08i;->b(LX/0QB;)LX/08i;

    move-result-object v0

    sput-object v0, LX/08i;->n:LX/08i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 21540
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 21541
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 21542
    :cond_1
    sget-object v0, LX/08i;->n:LX/08i;

    return-object v0

    .line 21543
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 21544
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/08i;
    .locals 9

    .prologue
    .line 21545
    new-instance v0, LX/08i;

    const/16 v1, 0x2fd

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/09k;->a(LX/0QB;)LX/09k;

    move-result-object v2

    check-cast v2, LX/09k;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    const/16 v4, 0x15e7

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v5

    check-cast v5, LX/0dC;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    const-class v7, Landroid/content/Context;

    invoke-interface {p0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {p0}, LX/1qm;->a(LX/0QB;)LX/1qm;

    move-result-object v8

    check-cast v8, LX/1qm;

    invoke-direct/range {v0 .. v8}, LX/08i;-><init>(LX/0Or;LX/09k;LX/0Xl;LX/0Or;LX/0dC;Ljava/util/concurrent/ExecutorService;Landroid/content/Context;LX/1qm;)V

    .line 21546
    return-object v0
.end method

.method public static k(LX/08i;)V
    .locals 5

    .prologue
    .line 21507
    iget-object v0, p0, LX/08i;->c:LX/09k;

    invoke-virtual {v0}, LX/09k;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 21508
    :goto_0
    return-void

    .line 21509
    :cond_0
    iget-object v0, p0, LX/08i;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 21510
    invoke-direct {p0}, LX/08i;->q()V

    goto :goto_0

    .line 21511
    :cond_1
    iget-object v0, p0, LX/08i;->c:LX/09k;

    .line 21512
    iget-boolean v1, v0, LX/09k;->f:Z

    move v0, v1

    .line 21513
    if-eqz v0, :cond_2

    .line 21514
    const/4 v4, 0x1

    .line 21515
    iget-object v0, p0, LX/08i;->e:Landroid/content/Context;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, LX/08i;->e:Landroid/content/Context;

    const-class v3, Lcom/facebook/rti/push/service/FbnsService;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {v0, v4, v1}, LX/09p;->a(Landroid/content/Context;ZLandroid/content/ComponentName;)V

    .line 21516
    iget-object v0, p0, LX/08i;->e:Landroid/content/Context;

    const-class v1, Lcom/facebook/rti/orca/FbnsLiteBroadcastReceiver;

    invoke-static {v0, v1, v4}, LX/09p;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    .line 21517
    iget-object v0, p0, LX/08i;->i:LX/09m;

    const-string v1, "FbnsLiteInitializer"

    .line 21518
    iget-object v2, v0, LX/09m;->a:Landroid/content/Context;

    iget-object v3, v0, LX/09m;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, LX/09o;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 21519
    iget-object v0, p0, LX/08i;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/orca/FbnsLiteInitializer$3;

    invoke-direct {v1, p0}, Lcom/facebook/rti/orca/FbnsLiteInitializer$3;-><init>(LX/08i;)V

    const v2, 0x2be1fa7b

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 21520
    :goto_1
    iget-object v0, p0, LX/08i;->c:LX/09k;

    .line 21521
    iget-boolean v1, v0, LX/09k;->g:Z

    move v0, v1

    .line 21522
    if-eqz v0, :cond_3

    .line 21523
    :try_start_0
    iget-object v0, p0, LX/08i;->e:Landroid/content/Context;

    iget-object v1, p0, LX/08i;->l:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 21524
    :goto_2
    goto :goto_0

    .line 21525
    :cond_2
    invoke-direct {p0}, LX/08i;->o()V

    goto :goto_1

    .line 21526
    :cond_3
    invoke-direct {p0}, LX/08i;->q()V

    goto :goto_0

    .line 21527
    :catch_0
    move-exception v0

    .line 21528
    sget-object v1, LX/08i;->a:Ljava/lang/String;

    const-string v2, "failed to startDummyStickyService"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public static l(LX/08i;)V
    .locals 7

    .prologue
    .line 21529
    iget-object v0, p0, LX/08i;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/08i;->g:LX/0dC;

    invoke-virtual {v0}, LX/0dC;->a()Ljava/lang/String;

    iget-object v0, p0, LX/08i;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    .line 21530
    iget-object v0, p0, LX/08i;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const/4 v2, 0x1

    .line 21531
    :goto_0
    iget-object v0, p0, LX/08i;->e:Landroid/content/Context;

    iget-object v1, p0, LX/08i;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, LX/08i;->g:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, LX/08i;->m:I

    iget-object v5, p0, LX/08i;->c:LX/09k;

    invoke-virtual {v5}, LX/09k;->j()I

    move-result v5

    iget-object v6, p0, LX/08i;->c:LX/09k;

    invoke-virtual {v6}, LX/09k;->k()I

    move-result v6

    invoke-static/range {v0 .. v6}, LX/09o;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;III)V

    .line 21532
    return-void

    .line 21533
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 21485
    iget-object v0, p0, LX/08i;->j:LX/09n;

    invoke-virtual {v0}, LX/09n;->b()V

    .line 21486
    iget-object v0, p0, LX/08i;->i:LX/09m;

    const-class v1, Lcom/facebook/rti/push/service/FbnsService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    .line 21487
    invoke-static {v0}, LX/09m;->e(LX/09m;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 21488
    const-string v3, "register_and_stop"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 21489
    if-eqz v3, :cond_1

    .line 21490
    const-string v2, "FbnsClientWrapper"

    const-string v3, "not stopping FbnsService because waiting for register to complete"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v5}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21491
    :cond_0
    :goto_0
    iget-object v0, p0, LX/08i;->e:Landroid/content/Context;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, LX/08i;->e:Landroid/content/Context;

    const-class v3, Lcom/facebook/rti/push/service/FbnsService;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {v0, v4, v1}, LX/09p;->a(Landroid/content/Context;ZLandroid/content/ComponentName;)V

    .line 21492
    iget-object v0, p0, LX/08i;->e:Landroid/content/Context;

    const-class v1, Lcom/facebook/rti/orca/FbnsLiteBroadcastReceiver;

    invoke-static {v0, v1, v4}, LX/09p;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    .line 21493
    return-void

    .line 21494
    :cond_1
    iget-object v3, v0, LX/09m;->a:Landroid/content/Context;

    const/4 v0, 0x1

    const/4 v8, 0x0

    .line 21495
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v1, v5}, LX/05U;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 21496
    sget-object v5, LX/09o;->a:Ljava/lang/String;

    const-string v6, "FBNS Service not found"

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21497
    :cond_2
    :goto_1
    if-eqz v4, :cond_0

    .line 21498
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 21499
    :cond_3
    sget-object v5, LX/09o;->a:Ljava/lang/String;

    const-string v6, "Stopping running FBNS service %s"

    new-array v7, v0, [Ljava/lang/Object;

    aput-object v1, v7, v8

    invoke-static {v5, v6, v7}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21500
    new-instance v5, Landroid/content/Intent;

    const-string v6, "Orca.STOP"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 21501
    new-instance v6, Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 21502
    invoke-virtual {v5, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 21503
    new-instance v7, LX/04v;

    invoke-direct {v7, v3}, LX/04v;-><init>(Landroid/content/Context;)V

    .line 21504
    invoke-virtual {v7, v5, v6}, LX/04v;->a(Landroid/content/Intent;Landroid/content/ComponentName;)Landroid/content/ComponentName;

    move-result-object v5

    .line 21505
    if-nez v5, :cond_2

    .line 21506
    sget-object v5, LX/09o;->a:Ljava/lang/String;

    const-string v6, "Missing %s"

    new-array v7, v0, [Ljava/lang/Object;

    aput-object v1, v7, v8

    invoke-static {v5, v6, v7}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private q()V
    .locals 4

    .prologue
    .line 21481
    :try_start_0
    iget-object v0, p0, LX/08i;->e:Landroid/content/Context;

    iget-object v1, p0, LX/08i;->l:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 21482
    :goto_0
    return-void

    .line 21483
    :catch_0
    move-exception v0

    .line 21484
    sget-object v1, LX/08i;->a:Ljava/lang/String;

    const-string v2, "failed to stopDummyStickyService"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 3
    .param p1    # Lcom/facebook/auth/component/AuthenticationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 21477
    iget-object v0, p0, LX/08i;->c:LX/09k;

    invoke-virtual {v0}, LX/09k;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 21478
    :goto_0
    return-void

    .line 21479
    :cond_0
    invoke-static {p0}, LX/08i;->l(LX/08i;)V

    .line 21480
    iget-object v0, p0, LX/08i;->h:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/08i;->k:Ljava/lang/Runnable;

    const v2, -0x4b5f0a11

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 21471
    iget-object v0, p0, LX/08i;->c:LX/09k;

    invoke-virtual {v0}, LX/09k;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 21472
    :goto_0
    return-void

    .line 21473
    :cond_0
    invoke-direct {p0}, LX/08i;->o()V

    .line 21474
    invoke-direct {p0}, LX/08i;->q()V

    .line 21475
    iget-object v0, p0, LX/08i;->e:Landroid/content/Context;

    invoke-static {v0}, LX/09o;->b(Landroid/content/Context;)V

    .line 21476
    goto :goto_0
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 21468
    iget-object v0, p0, LX/08i;->c:LX/09k;

    invoke-virtual {v0}, LX/09k;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 21469
    :goto_0
    return-void

    .line 21470
    :cond_0
    iget-object v0, p0, LX/08i;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rti/orca/FbnsLiteInitializer$2;

    invoke-direct {v1, p0}, Lcom/facebook/rti/orca/FbnsLiteInitializer$2;-><init>(LX/08i;)V

    const v2, 0x124ce163

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
