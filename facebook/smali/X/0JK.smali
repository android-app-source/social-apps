.class public final enum LX/0JK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JK;

.field public static final enum HEADING_RESET:LX/0JK;

.field public static final enum VIEWPORT_ROTATED:LX/0JK;

.field public static final enum VIEWPORT_ZOOMED:LX/0JK;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39351
    new-instance v0, LX/0JK;

    const-string v1, "VIEWPORT_ROTATED"

    const-string v2, "viewport_rotated"

    invoke-direct {v0, v1, v3, v2}, LX/0JK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JK;->VIEWPORT_ROTATED:LX/0JK;

    .line 39352
    new-instance v0, LX/0JK;

    const-string v1, "VIEWPORT_ZOOMED"

    const-string v2, "viewport_zoomed"

    invoke-direct {v0, v1, v4, v2}, LX/0JK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JK;->VIEWPORT_ZOOMED:LX/0JK;

    .line 39353
    new-instance v0, LX/0JK;

    const-string v1, "HEADING_RESET"

    const-string v2, "heading_reset"

    invoke-direct {v0, v1, v5, v2}, LX/0JK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JK;->HEADING_RESET:LX/0JK;

    .line 39354
    const/4 v0, 0x3

    new-array v0, v0, [LX/0JK;

    sget-object v1, LX/0JK;->VIEWPORT_ROTATED:LX/0JK;

    aput-object v1, v0, v3

    sget-object v1, LX/0JK;->VIEWPORT_ZOOMED:LX/0JK;

    aput-object v1, v0, v4

    sget-object v1, LX/0JK;->HEADING_RESET:LX/0JK;

    aput-object v1, v0, v5

    sput-object v0, LX/0JK;->$VALUES:[LX/0JK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39355
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39356
    iput-object p3, p0, LX/0JK;->value:Ljava/lang/String;

    .line 39357
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JK;
    .locals 1

    .prologue
    .line 39358
    const-class v0, LX/0JK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JK;

    return-object v0
.end method

.method public static values()[LX/0JK;
    .locals 1

    .prologue
    .line 39359
    sget-object v0, LX/0JK;->$VALUES:[LX/0JK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JK;

    return-object v0
.end method
