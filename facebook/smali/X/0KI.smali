.class public LX/0KI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0G6;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.util.Log.e",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.v"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field private final c:LX/0OG;

.field private d:LX/0OS;

.field private final e:LX/0K9;

.field private final f:Z

.field private g:LX/0Ge;

.field private h:LX/0G6;

.field private i:LX/0OT;

.field private j:LX/0KG;

.field private k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0KH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40498
    const-class v0, LX/0KI;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0KI;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0K9;LX/0Ge;Z)V
    .locals 1

    .prologue
    .line 40643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40644
    iput-object p2, p0, LX/0KI;->e:LX/0K9;

    .line 40645
    iput-object p1, p0, LX/0KI;->a:Ljava/lang/String;

    .line 40646
    iput-object p3, p0, LX/0KI;->g:LX/0Ge;

    .line 40647
    new-instance v0, LX/0OG;

    invoke-direct {v0}, LX/0OG;-><init>()V

    iput-object v0, p0, LX/0KI;->c:LX/0OG;

    .line 40648
    const/4 v0, 0x0

    iput-object v0, p0, LX/0KI;->d:LX/0OS;

    .line 40649
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/0KI;->k:Ljava/util/Set;

    .line 40650
    iput-boolean p4, p0, LX/0KI;->f:Z

    .line 40651
    return-void
.end method

.method private static a(LX/0KI;LX/0OQ;)LX/0OT;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 40638
    iget-object v0, p0, LX/0KI;->j:LX/0KG;

    iget-object v0, v0, LX/0KG;->d:Ljava/lang/String;

    invoke-interface {p1, v0}, LX/0OQ;->a(Ljava/lang/String;)Ljava/util/NavigableSet;

    move-result-object v0

    .line 40639
    if-eqz v0, :cond_1

    .line 40640
    invoke-interface {v0}, Ljava/util/NavigableSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0OT;

    .line 40641
    iget-object v2, p0, LX/0KI;->j:LX/0KG;

    iget-wide v2, v2, LX/0KG;->b:J

    iget-wide v4, v0, LX/0OT;->b:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 40642
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/0KI;Ljava/lang/String;JJZ)V
    .locals 2

    .prologue
    .line 40628
    monitor-enter p0

    .line 40629
    :try_start_0
    iget-object v0, p0, LX/0KI;->k:Ljava/util/Set;

    const/4 v1, 0x0

    new-array v1, v1, [LX/0KH;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0KH;

    .line 40630
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40631
    if-eqz v0, :cond_1

    .line 40632
    iget-object v0, p0, LX/0KI;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0KH;

    .line 40633
    if-eqz p6, :cond_0

    .line 40634
    invoke-interface {v0, p4, p5}, LX/0KH;->a(J)V

    goto :goto_0

    .line 40635
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 40636
    :cond_0
    invoke-interface {v0, p4, p5}, LX/0KH;->b(J)V

    goto :goto_0

    .line 40637
    :cond_1
    return-void
.end method

.method private b()V
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v12, 0x0

    .line 40617
    iget-object v0, p0, LX/0KI;->i:LX/0OT;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0KI;->i:LX/0OT;

    iget-boolean v0, v0, LX/0OT;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 40618
    iget-object v0, p0, LX/0KI;->i:LX/0OT;

    iget-object v0, v0, LX/0OT;->e:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 40619
    iget-object v0, p0, LX/0KI;->j:LX/0KG;

    iget-wide v4, v0, LX/0KG;->b:J

    iget-object v0, p0, LX/0KI;->i:LX/0OT;

    iget-wide v6, v0, LX/0OT;->b:J

    sub-long v6, v4, v6

    .line 40620
    iget-object v0, p0, LX/0KI;->i:LX/0OT;

    iget-wide v4, v0, LX/0OT;->c:J

    sub-long/2addr v4, v6

    iget-object v0, p0, LX/0KI;->j:LX/0KG;

    invoke-virtual {v0}, LX/0KG;->a()J

    move-result-wide v8

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    .line 40621
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LX/0KI;->j:LX/0KG;

    iget-wide v10, v5, LX/0KG;->a:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v12

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x2

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x3

    iget-object v5, p0, LX/0KI;->a:Ljava/lang/String;

    aput-object v5, v4, v1

    const/4 v1, 0x4

    iget-object v5, p0, LX/0KI;->j:LX/0KG;

    iget-object v5, v5, LX/0KG;->d:Ljava/lang/String;

    aput-object v5, v4, v1

    .line 40622
    new-instance v1, LX/0OA;

    const/4 v3, 0x0

    iget-object v0, p0, LX/0KI;->j:LX/0KG;

    iget-wide v4, v0, LX/0KG;->a:J

    iget-object v0, p0, LX/0KI;->j:LX/0KG;

    iget-object v10, v0, LX/0KG;->d:Ljava/lang/String;

    iget-object v0, p0, LX/0KI;->j:LX/0KG;

    iget v11, v0, LX/0KG;->e:I

    invoke-direct/range {v1 .. v11}, LX/0OA;-><init>(Landroid/net/Uri;[BJJJLjava/lang/String;I)V

    .line 40623
    iget-object v0, p0, LX/0KI;->c:LX/0OG;

    invoke-virtual {v0, v1}, LX/0OG;->a(LX/0OA;)J

    .line 40624
    iget-object v0, p0, LX/0KI;->j:LX/0KG;

    iget-object v5, v0, LX/0KG;->d:Ljava/lang/String;

    iget-object v0, p0, LX/0KI;->j:LX/0KG;

    iget-wide v6, v0, LX/0KG;->a:J

    move-object v4, p0

    move v10, v12

    invoke-static/range {v4 .. v10}, LX/0KI;->a(LX/0KI;Ljava/lang/String;JJZ)V

    .line 40625
    iget-object v0, p0, LX/0KI;->c:LX/0OG;

    iput-object v0, p0, LX/0KI;->h:LX/0G6;

    .line 40626
    return-void

    :cond_0
    move v0, v12

    .line 40627
    goto :goto_0
.end method

.method private static c(LX/0KI;)LX/0OT;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 40611
    iget-object v0, p0, LX/0KI;->e:LX/0K9;

    invoke-virtual {v0}, LX/0K9;->a()LX/0OV;

    move-result-object v0

    invoke-static {p0, v0}, LX/0KI;->a(LX/0KI;LX/0OQ;)LX/0OT;

    move-result-object v0

    .line 40612
    if-nez v0, :cond_0

    .line 40613
    iget-object v0, p0, LX/0KI;->e:LX/0K9;

    .line 40614
    iget-object v1, v0, LX/0K9;->d:LX/0OV;

    move-object v0, v1

    .line 40615
    invoke-static {p0, v0}, LX/0KI;->a(LX/0KI;LX/0OQ;)LX/0OT;

    move-result-object v0

    .line 40616
    :cond_0
    return-object v0
.end method

.method private d()V
    .locals 24

    .prologue
    .line 40580
    invoke-static/range {p0 .. p0}, LX/0KI;->c(LX/0KI;)LX/0OT;

    move-result-object v2

    .line 40581
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KI;->j:LX/0KG;

    invoke-virtual {v3}, LX/0KG;->b()Z

    move-result v3

    if-eqz v3, :cond_4

    const-wide/16 v10, -0x1

    .line 40582
    :goto_0
    if-eqz v2, :cond_0

    .line 40583
    iget-wide v2, v2, LX/0OT;->b:J

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0KI;->j:LX/0KG;

    iget-wide v4, v4, LX/0KG;->b:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    .line 40584
    :cond_0
    new-instance v3, LX/0OA;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    iget-object v4, v2, LX/0KG;->c:Landroid/net/Uri;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    iget-wide v6, v2, LX/0KG;->a:J

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    iget-wide v8, v2, LX/0KG;->b:J

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    iget-object v12, v2, LX/0KG;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    iget v13, v2, LX/0KG;->e:I

    invoke-direct/range {v3 .. v13}, LX/0OA;-><init>(Landroid/net/Uri;[BJJJLjava/lang/String;I)V

    .line 40585
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->g:LX/0Ge;

    invoke-interface {v2, v3}, LX/0Ge;->a(LX/0OA;)J

    move-result-wide v20

    .line 40586
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->g:LX/0Ge;

    invoke-interface {v2}, LX/0Ge;->c()Ljava/util/Map;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0KI;->a:Ljava/lang/String;

    invoke-static {v2, v4}, LX/0Gj;->a(Ljava/util/Map;Ljava/lang/String;)J

    move-result-wide v4

    .line 40587
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->e:LX/0K9;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0KI;->j:LX/0KG;

    iget-object v6, v6, LX/0KG;->d:Ljava/lang/String;

    new-instance v7, LX/0KA;

    invoke-direct {v7, v4, v5}, LX/0KA;-><init>(J)V

    invoke-virtual {v2, v6, v7}, LX/0K9;->a(Ljava/lang/String;LX/0KA;)V

    .line 40588
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    invoke-virtual {v2}, LX/0KG;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 40589
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    invoke-virtual {v2, v4, v5}, LX/0KG;->a(J)V

    .line 40590
    :cond_1
    sget-object v2, LX/0KI;->b:Ljava/lang/String;

    const-string v4, "Cache data source open spec(HTTP). Offset: %d, Length %d Open Length %d Videoid %s Key %s"

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-wide v8, v3, LX/0OA;->c:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v3, 0x2

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0KI;->a:Ljava/lang/String;

    aput-object v6, v5, v3

    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0KI;->j:LX/0KG;

    iget-object v6, v6, LX/0KG;->d:Ljava/lang/String;

    aput-object v6, v5, v3

    invoke-static {v2, v4, v5}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40591
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->g:LX/0Ge;

    move-object/from16 v0, p0

    iput-object v2, v0, LX/0KI;->h:LX/0G6;

    .line 40592
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    iget-object v3, v2, LX/0KG;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    iget-wide v4, v2, LX/0KG;->b:J

    move-wide/from16 v0, v20

    invoke-static {v0, v1, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    const/4 v8, 0x1

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v8}, LX/0KI;->a(LX/0KI;Ljava/lang/String;JJZ)V

    .line 40593
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->i:LX/0OT;

    if-eqz v2, :cond_3

    .line 40594
    const-wide/16 v2, 0x0

    cmp-long v2, v20, v2

    if-lez v2, :cond_6

    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/0KI;->f:Z

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->e:LX/0K9;

    invoke-virtual {v2}, LX/0K9;->c()J

    move-result-wide v2

    cmp-long v2, v20, v2

    if-gtz v2, :cond_6

    .line 40595
    :cond_2
    :try_start_0
    new-instance v3, LX/0OS;

    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/0KI;->f:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->e:LX/0K9;

    invoke-virtual {v2}, LX/0K9;->a()LX/0OV;

    move-result-object v2

    :goto_1
    const-wide v4, 0x7fffffffffffffffL

    invoke-direct {v3, v2, v4, v5}, LX/0OS;-><init>(LX/0OQ;J)V

    move-object/from16 v0, p0

    iput-object v3, v0, LX/0KI;->d:LX/0OS;

    .line 40596
    new-instance v13, LX/0OA;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    iget-object v14, v2, LX/0KG;->c:Landroid/net/Uri;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    iget-wide v0, v2, LX/0KG;->a:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    iget-wide v0, v2, LX/0KG;->b:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    iget-object v0, v2, LX/0KG;->d:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->j:LX/0KG;

    iget v0, v2, LX/0KG;->e:I

    move/from16 v23, v0

    invoke-direct/range {v13 .. v23}, LX/0OA;-><init>(Landroid/net/Uri;[BJJJLjava/lang/String;I)V

    .line 40597
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->d:LX/0OS;

    invoke-virtual {v2, v13}, LX/0OS;->a(LX/0OA;)LX/0O8;

    .line 40598
    sget-object v2, LX/0KI;->b:Ljava/lang/String;

    const-string v3, "Cache data sink open spec. Offset: %d, Length %d Open Length %d Videoid %s Key %s"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, v13, LX/0OA;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0KI;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x4

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0KI;->j:LX/0KG;

    iget-object v6, v6, LX/0KG;->d:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch LX/0OR; {:try_start_0 .. :try_end_0} :catch_0

    .line 40599
    :cond_3
    :goto_2
    return-void

    .line 40600
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KI;->j:LX/0KG;

    invoke-virtual {v3}, LX/0KG;->a()J

    move-result-wide v10

    goto/16 :goto_0

    .line 40601
    :cond_5
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->e:LX/0K9;

    invoke-virtual {v2}, LX/0K9;->b()LX/0OV;
    :try_end_1
    .catch LX/0OR; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto/16 :goto_1

    .line 40602
    :catch_0
    move-exception v2

    .line 40603
    new-instance v3, Ljava/io/IOException;

    invoke-virtual {v2}, LX/0OR;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 40604
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->i:LX/0OT;

    iget-boolean v2, v2, LX/0OT;->d:Z

    if-nez v2, :cond_3

    .line 40605
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/0KI;->f:Z

    if-eqz v2, :cond_8

    .line 40606
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->e:LX/0K9;

    invoke-virtual {v2}, LX/0K9;->a()LX/0OV;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 40607
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->e:LX/0K9;

    invoke-virtual {v2}, LX/0K9;->a()LX/0OV;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KI;->i:LX/0OT;

    invoke-virtual {v2, v3}, LX/0OV;->a(LX/0OT;)V

    .line 40608
    :cond_7
    :goto_3
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, LX/0KI;->i:LX/0OT;

    goto :goto_2

    .line 40609
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->e:LX/0K9;

    invoke-virtual {v2}, LX/0K9;->b()LX/0OV;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 40610
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0KI;->e:LX/0K9;

    invoke-virtual {v2}, LX/0K9;->b()LX/0OV;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0KI;->i:LX/0OT;

    invoke-virtual {v2, v3}, LX/0OV;->a(LX/0OT;)V

    goto :goto_3
.end method

.method private e()Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 40555
    invoke-direct {p0}, LX/0KI;->f()V

    .line 40556
    iget-object v2, p0, LX/0KI;->j:LX/0KG;

    invoke-virtual {v2}, LX/0KG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 40557
    :goto_0
    return v0

    .line 40558
    :cond_0
    iget-object v2, p0, LX/0KI;->e:LX/0K9;

    invoke-virtual {v2}, LX/0K9;->a()LX/0OV;

    move-result-object v2

    .line 40559
    if-eqz v2, :cond_1

    .line 40560
    iget-object v3, p0, LX/0KI;->j:LX/0KG;

    iget-object v3, v3, LX/0KG;->d:Ljava/lang/String;

    iget-object v4, p0, LX/0KI;->j:LX/0KG;

    iget-wide v4, v4, LX/0KG;->b:J

    invoke-interface {v2, v3, v4, v5}, LX/0OQ;->a(Ljava/lang/String;J)LX/0OT;

    move-result-object v3

    iput-object v3, p0, LX/0KI;->i:LX/0OT;

    .line 40561
    :cond_1
    iget-boolean v3, p0, LX/0KI;->f:Z

    if-nez v3, :cond_4

    iget-object v3, p0, LX/0KI;->e:LX/0K9;

    .line 40562
    iget-object v4, v3, LX/0K9;->d:LX/0OV;

    move-object v3, v4

    .line 40563
    if-eqz v3, :cond_4

    iget-object v3, p0, LX/0KI;->e:LX/0K9;

    .line 40564
    iget-object v4, v3, LX/0K9;->d:LX/0OV;

    move-object v3, v4

    .line 40565
    if-eq v2, v3, :cond_4

    iget-object v3, p0, LX/0KI;->i:LX/0OT;

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/0KI;->i:LX/0OT;

    iget-boolean v3, v3, LX/0OT;->d:Z

    if-nez v3, :cond_4

    .line 40566
    :cond_2
    iget-object v3, p0, LX/0KI;->i:LX/0OT;

    if-eqz v3, :cond_3

    .line 40567
    iget-object v3, p0, LX/0KI;->i:LX/0OT;

    invoke-interface {v2, v3}, LX/0OQ;->a(LX/0OT;)V

    .line 40568
    :cond_3
    iget-object v2, p0, LX/0KI;->e:LX/0K9;

    .line 40569
    iget-object v3, v2, LX/0K9;->d:LX/0OV;

    move-object v2, v3

    .line 40570
    iget-object v3, p0, LX/0KI;->j:LX/0KG;

    iget-object v3, v3, LX/0KG;->d:Ljava/lang/String;

    iget-object v4, p0, LX/0KI;->j:LX/0KG;

    iget-wide v4, v4, LX/0KG;->b:J

    invoke-virtual {v2, v3, v4, v5}, LX/0OV;->a(Ljava/lang/String;J)LX/0OT;

    move-result-object v2

    iput-object v2, p0, LX/0KI;->i:LX/0OT;

    .line 40571
    :cond_4
    iget-object v2, p0, LX/0KI;->i:LX/0OT;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/0KI;->i:LX/0OT;

    iget-boolean v2, v2, LX/0OT;->d:Z

    if-eqz v2, :cond_6

    .line 40572
    iget-object v2, p0, LX/0KI;->i:LX/0OT;

    .line 40573
    iget-wide v6, v2, LX/0OT;->c:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_8

    const/4 v6, 0x1

    :goto_1
    move v2, v6

    .line 40574
    if-nez v2, :cond_5

    move v0, v1

    :cond_5
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 40575
    invoke-direct {p0}, LX/0KI;->b()V

    :goto_2
    move v0, v1

    .line 40576
    goto :goto_0

    .line 40577
    :cond_6
    iget-object v2, p0, LX/0KI;->i:LX/0OT;

    if-nez v2, :cond_7

    .line 40578
    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, LX/0KI;->a:Ljava/lang/String;

    aput-object v5, v4, v0

    .line 40579
    :cond_7
    invoke-direct {p0}, LX/0KI;->d()V

    goto :goto_2

    :cond_8
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private f()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 40536
    iget-object v0, p0, LX/0KI;->h:LX/0G6;

    if-eqz v0, :cond_0

    .line 40537
    iget-object v0, p0, LX/0KI;->h:LX/0G6;

    invoke-interface {v0}, LX/0G6;->a()V

    .line 40538
    iput-object v6, p0, LX/0KI;->h:LX/0G6;

    .line 40539
    :cond_0
    iget-object v0, p0, LX/0KI;->i:LX/0OT;

    if-eqz v0, :cond_3

    .line 40540
    iget-object v0, p0, LX/0KI;->i:LX/0OT;

    iget-boolean v0, v0, LX/0OT;->d:Z

    if-nez v0, :cond_2

    .line 40541
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/0KI;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/0KI;->j:LX/0KG;

    iget-object v4, v4, LX/0KG;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/0KI;->i:LX/0OT;

    iget-wide v4, v4, LX/0OT;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 40542
    iget-object v0, p0, LX/0KI;->d:LX/0OS;

    if-eqz v0, :cond_1

    .line 40543
    iget-object v0, p0, LX/0KI;->d:LX/0OS;

    invoke-virtual {v0}, LX/0OS;->a()V

    .line 40544
    :cond_1
    iget-boolean v0, p0, LX/0KI;->f:Z

    if-eqz v0, :cond_4

    .line 40545
    iget-object v0, p0, LX/0KI;->e:LX/0K9;

    invoke-virtual {v0}, LX/0K9;->a()LX/0OV;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 40546
    iget-object v0, p0, LX/0KI;->e:LX/0K9;

    invoke-virtual {v0}, LX/0K9;->a()LX/0OV;

    move-result-object v0

    iget-object v1, p0, LX/0KI;->i:LX/0OT;

    invoke-virtual {v0, v1}, LX/0OV;->a(LX/0OT;)V

    .line 40547
    :cond_2
    :goto_0
    iput-object v6, p0, LX/0KI;->i:LX/0OT;

    .line 40548
    :cond_3
    return-void

    .line 40549
    :cond_4
    iget-object v0, p0, LX/0KI;->e:LX/0K9;

    .line 40550
    iget-object v1, v0, LX/0K9;->d:LX/0OV;

    move-object v0, v1

    .line 40551
    if-eqz v0, :cond_2

    .line 40552
    iget-object v0, p0, LX/0KI;->e:LX/0K9;

    .line 40553
    iget-object v1, v0, LX/0K9;->d:LX/0OV;

    move-object v0, v1

    .line 40554
    iget-object v1, p0, LX/0KI;->i:LX/0OT;

    invoke-virtual {v0, v1}, LX/0OV;->a(LX/0OT;)V

    goto :goto_0
.end method


# virtual methods
.method public final a([BII)I
    .locals 7

    .prologue
    const/4 v2, -0x1

    .line 40517
    iget-object v0, p0, LX/0KI;->h:LX/0G6;

    invoke-interface {v0, p1, p2, p3}, LX/0G6;->a([BII)I

    move-result v0

    .line 40518
    if-ne v0, v2, :cond_2

    .line 40519
    invoke-direct {p0}, LX/0KI;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 40520
    :cond_0
    :goto_0
    return v0

    .line 40521
    :cond_1
    iget-object v0, p0, LX/0KI;->h:LX/0G6;

    invoke-interface {v0, p1, p2, p3}, LX/0G6;->a([BII)I

    move-result v0

    .line 40522
    if-eq v0, v2, :cond_0

    :cond_2
    move v1, v0

    .line 40523
    if-eq v1, v2, :cond_5

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 40524
    iget-object v0, p0, LX/0KI;->j:LX/0KG;

    .line 40525
    iget-wide v3, v0, LX/0KG;->f:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    .line 40526
    iget-wide v3, v0, LX/0KG;->f:J

    int-to-long v5, v1

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-ltz v3, :cond_6

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, LX/0Av;->b(Z)V

    .line 40527
    iget-wide v3, v0, LX/0KG;->f:J

    int-to-long v5, v1

    sub-long/2addr v3, v5

    iput-wide v3, v0, LX/0KG;->f:J

    .line 40528
    :cond_3
    iget-wide v3, v0, LX/0KG;->a:J

    int-to-long v5, v1

    add-long/2addr v3, v5

    iput-wide v3, v0, LX/0KG;->a:J

    .line 40529
    iget-wide v3, v0, LX/0KG;->b:J

    int-to-long v5, v1

    add-long/2addr v3, v5

    iput-wide v3, v0, LX/0KG;->b:J

    .line 40530
    iget-object v0, p0, LX/0KI;->h:LX/0G6;

    iget-object v2, p0, LX/0KI;->g:LX/0Ge;

    if-ne v0, v2, :cond_4

    .line 40531
    iget-object v0, p0, LX/0KI;->i:LX/0OT;

    if-eqz v0, :cond_4

    .line 40532
    iget-object v0, p0, LX/0KI;->d:LX/0OS;

    invoke-virtual {v0, p1, p2, v1}, LX/0OS;->a([BII)V

    :cond_4
    move v0, v1

    .line 40533
    goto :goto_0

    .line 40534
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 40535
    :cond_6
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final a(LX/0OA;)J
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 40505
    iget-object v0, p0, LX/0KI;->j:LX/0KG;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 40506
    new-instance v0, LX/0KG;

    iget-object v3, p1, LX/0OA;->f:Ljava/lang/String;

    iget-object v4, p0, LX/0KI;->a:Ljava/lang/String;

    iget-object v5, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-static {v3, v4, v5}, LX/0K9;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p1, v3}, LX/0KG;-><init>(LX/0OA;Ljava/lang/String;)V

    iput-object v0, p0, LX/0KI;->j:LX/0KG;

    .line 40507
    invoke-direct {p0}, LX/0KI;->e()Z

    .line 40508
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    iget-wide v6, p1, LX/0OA;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    iget-wide v6, p1, LX/0OA;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    const/4 v1, 0x2

    iget-wide v6, p1, LX/0OA;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/0KI;->a:Ljava/lang/String;

    aput-object v2, v4, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/0KI;->j:LX/0KG;

    iget-object v2, v2, LX/0KG;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 40509
    iget-object v0, p0, LX/0KI;->e:LX/0K9;

    iget-object v1, p0, LX/0KI;->j:LX/0KG;

    iget-object v1, v1, LX/0KG;->d:Ljava/lang/String;

    .line 40510
    iget-object v2, v0, LX/0K9;->g:LX/0KB;

    invoke-virtual {v2, v1}, LX/0KB;->a(Ljava/lang/String;)LX/0KA;

    move-result-object v2

    move-object v0, v2

    .line 40511
    if-eqz v0, :cond_0

    .line 40512
    iget-wide v0, v0, LX/0KA;->a:J

    .line 40513
    iget-object v2, p0, LX/0KI;->j:LX/0KG;

    invoke-virtual {v2}, LX/0KG;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p1, LX/0OA;->d:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 40514
    iget-object v2, p0, LX/0KI;->j:LX/0KG;

    iget-wide v4, p1, LX/0OA;->d:J

    sub-long/2addr v0, v4

    invoke-virtual {v2, v0, v1}, LX/0KG;->a(J)V

    .line 40515
    :cond_0
    iget-object v0, p0, LX/0KI;->j:LX/0KG;

    invoke-virtual {v0}, LX/0KG;->a()J

    move-result-wide v0

    return-wide v0

    :cond_1
    move v0, v2

    .line 40516
    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 40502
    invoke-direct {p0}, LX/0KI;->f()V

    .line 40503
    const/4 v0, 0x0

    iput-object v0, p0, LX/0KI;->j:LX/0KG;

    .line 40504
    return-void
.end method

.method public final declared-synchronized a(LX/0KH;)V
    .locals 1

    .prologue
    .line 40499
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0KI;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40500
    monitor-exit p0

    return-void

    .line 40501
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
