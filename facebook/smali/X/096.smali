.class public LX/096;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/11o;)J
    .locals 4

    .prologue
    .line 22069
    invoke-interface {p0}, LX/11o;->a()Ljava/lang/String;

    move-result-object v0

    .line 22070
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    .line 22071
    :goto_0
    const/16 v2, 0x20

    shl-long/2addr v0, v2

    invoke-interface {p0}, LX/11o;->b()I

    move-result v2

    int-to-long v2, v2

    or-long/2addr v0, v2

    return-wide v0

    .line 22072
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/11o;Ljava/lang/String;I)LX/11o;
    .locals 7

    .prologue
    const/16 v0, 0x8

    .line 22064
    invoke-interface {p0, p1}, LX/11o;->a(Ljava/lang/String;)LX/11o;

    move-result-object v6

    .line 22065
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22066
    :goto_0
    return-object v0

    .line 22067
    :cond_0
    const/16 v1, 0x1a

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22068
    goto :goto_0
.end method

.method public static a(LX/11o;Ljava/lang/String;LX/0P1;JI)LX/11o;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11o;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;JI)",
            "LX/11o;"
        }
    .end annotation

    .prologue
    const/16 v0, 0x8

    .line 22059
    invoke-interface {p0, p1, p2, p3, p4}, LX/11o;->a(Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v6

    .line 22060
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22061
    :goto_0
    return-object v0

    .line 22062
    :cond_0
    const/16 v1, 0x19

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p5

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22063
    goto :goto_0
.end method

.method public static a(LX/11o;Ljava/lang/String;Ljava/lang/String;I)LX/11o;
    .locals 7

    .prologue
    const/16 v0, 0x8

    .line 22054
    invoke-interface {p0, p1, p2}, LX/11o;->a(Ljava/lang/String;Ljava/lang/String;)LX/11o;

    move-result-object v6

    .line 22055
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22056
    :goto_0
    return-object v0

    .line 22057
    :cond_0
    const/16 v1, 0x1d

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p3

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22058
    goto :goto_0
.end method

.method public static a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11o;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)",
            "LX/11o;"
        }
    .end annotation

    .prologue
    const/16 v0, 0x8

    .line 22004
    invoke-interface {p0, p1, p2, p3}, LX/11o;->a(Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/11o;

    move-result-object v6

    .line 22005
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22006
    :goto_0
    return-object v0

    .line 22007
    :cond_0
    const/16 v1, 0x1a

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22008
    goto :goto_0
.end method

.method public static a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11o;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;JI)",
            "LX/11o;"
        }
    .end annotation

    .prologue
    const/16 v0, 0x8

    .line 22049
    invoke-interface/range {p0 .. p5}, LX/11o;->a(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v6

    .line 22050
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22051
    :goto_0
    return-object v0

    .line 22052
    :cond_0
    const/16 v1, 0x1a

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p6

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22053
    goto :goto_0
.end method

.method public static b(LX/11o;Ljava/lang/String;I)LX/11o;
    .locals 7

    .prologue
    const/16 v0, 0x8

    .line 22044
    invoke-interface {p0, p1}, LX/11o;->b(Ljava/lang/String;)LX/11o;

    move-result-object v6

    .line 22045
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22046
    :goto_0
    return-object v0

    .line 22047
    :cond_0
    const/16 v1, 0x1b

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22048
    goto :goto_0
.end method

.method public static b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11o;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)",
            "LX/11o;"
        }
    .end annotation

    .prologue
    const/16 v0, 0x8

    .line 22039
    invoke-interface {p0, p1, p2, p3}, LX/11o;->b(Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/11o;

    move-result-object v6

    .line 22040
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22041
    :goto_0
    return-object v0

    .line 22042
    :cond_0
    const/16 v1, 0x1b

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22043
    goto :goto_0
.end method

.method public static b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11o;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;JI)",
            "LX/11o;"
        }
    .end annotation

    .prologue
    const/16 v0, 0x8

    .line 22034
    invoke-interface/range {p0 .. p5}, LX/11o;->b(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v6

    .line 22035
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22036
    :goto_0
    return-object v0

    .line 22037
    :cond_0
    const/16 v1, 0x1b

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p6

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22038
    goto :goto_0
.end method

.method public static c(LX/11o;Ljava/lang/String;I)LX/11o;
    .locals 7

    .prologue
    const/16 v0, 0x8

    .line 22029
    invoke-interface {p0, p1}, LX/11o;->c(Ljava/lang/String;)LX/11o;

    move-result-object v6

    .line 22030
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22031
    :goto_0
    return-object v0

    .line 22032
    :cond_0
    const/16 v1, 0x1c

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22033
    goto :goto_0
.end method

.method public static c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11o;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)",
            "LX/11o;"
        }
    .end annotation

    .prologue
    const/16 v0, 0x8

    .line 22024
    invoke-interface {p0, p1, p2, p3}, LX/11o;->c(Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/11o;

    move-result-object v6

    .line 22025
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22026
    :goto_0
    return-object v0

    .line 22027
    :cond_0
    const/16 v1, 0x1c

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22028
    goto :goto_0
.end method

.method public static c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11o;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;JI)",
            "LX/11o;"
        }
    .end annotation

    .prologue
    const/16 v0, 0x8

    .line 22019
    invoke-interface/range {p0 .. p5}, LX/11o;->c(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v6

    .line 22020
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22021
    :goto_0
    return-object v0

    .line 22022
    :cond_0
    const/16 v1, 0x1c

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p6

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22023
    goto :goto_0
.end method

.method public static d(LX/11o;Ljava/lang/String;I)LX/11o;
    .locals 7

    .prologue
    const/16 v0, 0x8

    .line 22014
    invoke-interface {p0, p1}, LX/11o;->d(Ljava/lang/String;)LX/11o;

    move-result-object v6

    .line 22015
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22016
    :goto_0
    return-object v0

    .line 22017
    :cond_0
    const/16 v1, 0x1d

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22018
    goto :goto_0
.end method

.method public static e(LX/11o;Ljava/lang/String;I)LX/11o;
    .locals 7

    .prologue
    const/16 v0, 0x8

    .line 22009
    invoke-interface {p0, p1}, LX/11o;->e(Ljava/lang/String;)LX/11o;

    move-result-object v6

    .line 22010
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 22011
    :goto_0
    return-object v0

    .line 22012
    :cond_0
    const/16 v1, 0x19

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {p0}, LX/096;->a(LX/11o;)J

    move-result-wide v4

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-object v0, v6

    .line 22013
    goto :goto_0
.end method
