.class public final LX/0OO;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0OO;


# instance fields
.field private final b:Ljava/lang/Object;

.field private final c:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53005
    new-instance v0, LX/0OO;

    invoke-direct {v0}, LX/0OO;-><init>()V

    sput-object v0, LX/0OO;->a:LX/0OO;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 53000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53001
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0OO;->b:Ljava/lang/Object;

    .line 53002
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, LX/0OO;->c:Ljava/util/PriorityQueue;

    .line 53003
    const v0, 0x7fffffff

    iput v0, p0, LX/0OO;->d:I

    .line 53004
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 52989
    iget-object v1, p0, LX/0OO;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 52990
    :try_start_0
    iget-object v0, p0, LX/0OO;->c:Ljava/util/PriorityQueue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 52991
    iget v0, p0, LX/0OO;->d:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/0OO;->d:I

    .line 52992
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 52993
    iget-object v1, p0, LX/0OO;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 52994
    :try_start_0
    iget-object v0, p0, LX/0OO;->c:Ljava/util/PriorityQueue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 52995
    iget-object v0, p0, LX/0OO;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7fffffff

    :goto_0
    iput v0, p0, LX/0OO;->d:I

    .line 52996
    iget-object v0, p0, LX/0OO;->b:Ljava/lang/Object;

    const v2, 0x6c779ab9

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 52997
    monitor-exit v1

    return-void

    .line 52998
    :cond_0
    iget-object v0, p0, LX/0OO;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 52999
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
