.class public LX/0GR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation

.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final A:LX/0Lm;

.field private final B:J

.field private final C:Z

.field private final D:Z

.field private final E:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final F:Z

.field public final G:Z

.field private final H:Z

.field private final I:I

.field private final a:LX/0Gb;

.field private final b:Landroid/net/Uri;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:LX/0KU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:LX/0KZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Landroid/content/Context;

.field private final h:Landroid/os/Handler;

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:Z

.field public final n:Z

.field private final o:Z

.field private final p:Z

.field private final q:Z

.field private final r:J

.field private final s:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final t:LX/0Ju;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final u:LX/0Jr;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final v:LX/0Od;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Od",
            "<",
            "LX/0AY;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/0GK;

.field private final x:Z

.field private final y:LX/0GB;

.field private final z:LX/0Gk;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/os/Handler;Ljava/util/Map;LX/0Ju;LX/0Jr;LX/0KU;LX/0KZ;Landroid/net/Uri;LX/0GK;LX/0GB;LX/0Gk;Z)V
    .locals 10
    .param p7    # LX/0Ju;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/0Jr;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/0KU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/0KZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p14    # LX/0Gk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Landroid/os/Handler;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ju;",
            "LX/0Jr;",
            "LX/0KU;",
            "LX/0KZ;",
            "Landroid/net/Uri;",
            "LX/0GK;",
            "LX/0GB;",
            "LX/0Gk;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 35370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35371
    invoke-static/range {p12 .. p12}, LX/0Av;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35372
    iput-object p1, p0, LX/0GR;->b:Landroid/net/Uri;

    .line 35373
    iput-object p2, p0, LX/0GR;->c:Ljava/lang/String;

    .line 35374
    iput-object p3, p0, LX/0GR;->d:Ljava/lang/String;

    .line 35375
    iput-object p4, p0, LX/0GR;->g:Landroid/content/Context;

    .line 35376
    iput-object p5, p0, LX/0GR;->h:Landroid/os/Handler;

    .line 35377
    invoke-static/range {p6 .. p6}, LX/040;->c(Ljava/util/Map;)I

    move-result v2

    iput v2, p0, LX/0GR;->i:I

    .line 35378
    invoke-static/range {p6 .. p6}, LX/040;->a(Ljava/util/Map;)I

    move-result v2

    iput v2, p0, LX/0GR;->j:I

    .line 35379
    invoke-static/range {p6 .. p6}, LX/040;->b(Ljava/util/Map;)I

    move-result v2

    iput v2, p0, LX/0GR;->k:I

    .line 35380
    invoke-static/range {p6 .. p6}, LX/040;->d(Ljava/util/Map;)I

    move-result v2

    iput v2, p0, LX/0GR;->l:I

    .line 35381
    invoke-static/range {p6 .. p6}, LX/040;->t(Ljava/util/Map;)Z

    move-result v2

    iput-boolean v2, p0, LX/0GR;->m:Z

    .line 35382
    invoke-static/range {p6 .. p6}, LX/040;->e(Ljava/util/Map;)Z

    move-result v2

    iput-boolean v2, p0, LX/0GR;->n:Z

    .line 35383
    invoke-static/range {p6 .. p6}, LX/040;->v(Ljava/util/Map;)Z

    move-result v2

    iput-boolean v2, p0, LX/0GR;->o:Z

    .line 35384
    invoke-static/range {p6 .. p6}, LX/040;->w(Ljava/util/Map;)I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, LX/0GR;->x:Z

    .line 35385
    invoke-static/range {p6 .. p6}, LX/040;->Z(Ljava/util/Map;)Z

    move-result v2

    iput-boolean v2, p0, LX/0GR;->p:Z

    .line 35386
    invoke-static/range {p6 .. p6}, LX/040;->A(Ljava/util/Map;)Z

    move-result v2

    iput-boolean v2, p0, LX/0GR;->q:Z

    .line 35387
    invoke-static/range {p6 .. p6}, LX/040;->ad(Ljava/util/Map;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, p0, LX/0GR;->r:J

    .line 35388
    invoke-static/range {p6 .. p6}, LX/040;->V(Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 35389
    new-instance v2, LX/0Gc;

    new-instance v3, LX/0OB;

    iget v4, p0, LX/0GR;->i:I

    invoke-direct {v3, v4}, LX/0OB;-><init>(I)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {p6 .. p6}, LX/040;->O(Ljava/util/Map;)I

    move-result v6

    invoke-static/range {p6 .. p6}, LX/040;->P(Ljava/util/Map;)I

    move-result v7

    invoke-static/range {p6 .. p6}, LX/040;->Q(Ljava/util/Map;)F

    move-result v8

    invoke-static/range {p6 .. p6}, LX/040;->R(Ljava/util/Map;)F

    move-result v9

    invoke-direct/range {v2 .. v9}, LX/0Gc;-><init>(LX/0O1;Landroid/os/Handler;LX/0GZ;IIFF)V

    iput-object v2, p0, LX/0GR;->a:LX/0Gb;

    .line 35390
    :goto_1
    move-object/from16 v0, p7

    iput-object v0, p0, LX/0GR;->t:LX/0Ju;

    .line 35391
    move-object/from16 v0, p8

    iput-object v0, p0, LX/0GR;->u:LX/0Jr;

    .line 35392
    move-object/from16 v0, p9

    iput-object v0, p0, LX/0GR;->e:LX/0KU;

    .line 35393
    move-object/from16 v0, p10

    iput-object v0, p0, LX/0GR;->f:LX/0KZ;

    .line 35394
    move-object/from16 v0, p12

    iput-object v0, p0, LX/0GR;->w:LX/0GK;

    .line 35395
    invoke-virtual/range {p12 .. p12}, LX/0GK;->f()LX/0Od;

    move-result-object v2

    iput-object v2, p0, LX/0GR;->v:LX/0Od;

    .line 35396
    move-object/from16 v0, p11

    iput-object v0, p0, LX/0GR;->s:Landroid/net/Uri;

    .line 35397
    move-object/from16 v0, p13

    iput-object v0, p0, LX/0GR;->y:LX/0GB;

    .line 35398
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0GR;->z:LX/0Gk;

    .line 35399
    invoke-static/range {p6 .. p6}, LX/040;->af(Ljava/util/Map;)I

    move-result v2

    invoke-static {v2}, LX/0Lm;->fromValue(I)LX/0Lm;

    move-result-object v2

    iput-object v2, p0, LX/0GR;->A:LX/0Lm;

    .line 35400
    invoke-static/range {p6 .. p6}, LX/040;->ag(Ljava/util/Map;)J

    move-result-wide v2

    iput-wide v2, p0, LX/0GR;->B:J

    .line 35401
    invoke-static/range {p6 .. p6}, LX/040;->aj(Ljava/util/Map;)Z

    move-result v2

    iput-boolean v2, p0, LX/0GR;->C:Z

    .line 35402
    move/from16 v0, p15

    iput-boolean v0, p0, LX/0GR;->D:Z

    .line 35403
    invoke-static/range {p6 .. p6}, LX/040;->au(Ljava/util/Map;)Z

    move-result v2

    iput-boolean v2, p0, LX/0GR;->G:Z

    .line 35404
    move-object/from16 v0, p6

    iput-object v0, p0, LX/0GR;->E:Ljava/util/Map;

    .line 35405
    invoke-static/range {p6 .. p6}, LX/040;->an(Ljava/util/Map;)Z

    move-result v2

    iput-boolean v2, p0, LX/0GR;->F:Z

    .line 35406
    invoke-static/range {p6 .. p6}, LX/040;->p(Ljava/util/Map;)Z

    move-result v2

    iput-boolean v2, p0, LX/0GR;->H:Z

    .line 35407
    invoke-static/range {p6 .. p6}, LX/040;->q(Ljava/util/Map;)I

    move-result v2

    iput v2, p0, LX/0GR;->I:I

    .line 35408
    return-void

    .line 35409
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 35410
    :cond_1
    new-instance v2, LX/0Kt;

    new-instance v3, LX/0OB;

    iget v4, p0, LX/0GR;->i:I

    invoke-direct {v3, v4}, LX/0OB;-><init>(I)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {p6 .. p6}, LX/040;->O(Ljava/util/Map;)I

    move-result v6

    invoke-static/range {p6 .. p6}, LX/040;->P(Ljava/util/Map;)I

    move-result v7

    invoke-static/range {p6 .. p6}, LX/040;->S(Ljava/util/Map;)F

    move-result v8

    invoke-static/range {p6 .. p6}, LX/040;->T(Ljava/util/Map;)F

    move-result v9

    invoke-direct/range {v2 .. v9}, LX/0Kt;-><init>(LX/0O1;Landroid/os/Handler;LX/0Kr;IIFF)V

    iput-object v2, p0, LX/0GR;->a:LX/0Gb;

    goto :goto_1
.end method

.method private static a(LX/0GR;LX/04m;)LX/0G6;
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    const/4 v9, 0x1

    .line 35354
    if-nez p1, :cond_6

    iget-boolean v0, p0, LX/0GR;->H:Z

    if-eqz v0, :cond_6

    .line 35355
    sget-object v0, LX/0Gi;->a:LX/0OC;

    move-object p1, v0

    .line 35356
    move-object v6, p1

    .line 35357
    :goto_0
    iget-boolean v0, p0, LX/0GR;->F:Z

    if-eqz v0, :cond_1

    .line 35358
    new-instance v0, LX/0Gg;

    iget-object v1, p0, LX/0GR;->c:Ljava/lang/String;

    iget-object v2, p0, LX/0GR;->z:LX/0Gk;

    iget-boolean v4, p0, LX/0GR;->D:Z

    iget-object v5, p0, LX/0GR;->w:LX/0GK;

    invoke-virtual {v5}, LX/0GK;->c()Ljava/lang/String;

    move-result-object v5

    const-string v7, ""

    invoke-direct/range {v0 .. v7}, LX/0Gg;-><init>(Ljava/lang/String;LX/0Gk;ZZLjava/lang/String;LX/04m;Ljava/lang/String;)V

    .line 35359
    new-instance v1, LX/0Gv;

    iget-object v2, p0, LX/0GR;->c:Ljava/lang/String;

    iget-boolean v3, p0, LX/0GR;->m:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/0GR;->s:Landroid/net/Uri;

    :goto_1
    const-string v5, "ExoDashLive"

    iget-object v6, p0, LX/0GR;->y:LX/0GB;

    move v4, v9

    move-object v7, v0

    invoke-direct/range {v1 .. v7}, LX/0Gv;-><init>(Ljava/lang/String;Landroid/net/Uri;ZLjava/lang/String;LX/0GB;LX/0Gg;)V

    .line 35360
    :goto_2
    return-object v1

    :cond_0
    move-object v3, v8

    .line 35361
    goto :goto_1

    .line 35362
    :cond_1
    iget-boolean v0, p0, LX/0GR;->x:Z

    if-eqz v0, :cond_3

    .line 35363
    new-instance v0, LX/0Gg;

    iget-object v1, p0, LX/0GR;->c:Ljava/lang/String;

    iget-object v2, p0, LX/0GR;->z:LX/0Gk;

    iget-boolean v4, p0, LX/0GR;->D:Z

    iget-object v5, p0, LX/0GR;->w:LX/0GK;

    invoke-virtual {v5}, LX/0GK;->c()Ljava/lang/String;

    move-result-object v5

    const-string v7, ""

    invoke-direct/range {v0 .. v7}, LX/0Gg;-><init>(Ljava/lang/String;LX/0Gk;ZZLjava/lang/String;LX/04m;Ljava/lang/String;)V

    .line 35364
    new-instance v1, LX/0Gd;

    iget-object v2, p0, LX/0GR;->c:Ljava/lang/String;

    iget-boolean v3, p0, LX/0GR;->m:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/0GR;->s:Landroid/net/Uri;

    :goto_3
    const-string v4, "ExoDashLive"

    invoke-static {v4, v0}, LX/0Gt;->a(Ljava/lang/String;LX/04n;)LX/0Ge;

    move-result-object v4

    iget-object v5, p0, LX/0GR;->y:LX/0GB;

    iget-boolean v7, p0, LX/0GR;->q:Z

    move v6, v9

    move-object v8, v0

    invoke-direct/range {v1 .. v8}, LX/0Gd;-><init>(Ljava/lang/String;Landroid/net/Uri;LX/0Ge;LX/0GB;ZZLX/0Gg;)V

    goto :goto_2

    :cond_2
    move-object v3, v8

    goto :goto_3

    .line 35365
    :cond_3
    iget-boolean v0, p0, LX/0GR;->m:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, LX/0GR;->o:Z

    if-nez v0, :cond_4

    .line 35366
    new-instance v1, LX/0Gf;

    iget-object v0, p0, LX/0GR;->s:Landroid/net/Uri;

    iget-object v2, p0, LX/0GR;->c:Ljava/lang/String;

    const-string v4, "ExoDashLive"

    invoke-static {v4, v6}, LX/0Gt;->a(Ljava/lang/String;LX/04n;)LX/0Ge;

    move-result-object v4

    invoke-direct {v1, v0, v2, v4, v3}, LX/0Gf;-><init>(Landroid/net/Uri;Ljava/lang/String;LX/0Ge;I)V

    goto :goto_2

    .line 35367
    :cond_4
    iget-boolean v0, p0, LX/0GR;->m:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, LX/0GR;->o:Z

    if-eqz v0, :cond_5

    .line 35368
    new-instance v0, LX/0G8;

    iget-object v1, p0, LX/0GR;->g:Landroid/content/Context;

    const-string v2, "ExoDashLive"

    invoke-static {v1, v6, v2}, LX/0Gt;->a(Landroid/content/Context;LX/04n;Ljava/lang/String;)LX/0OE;

    move-result-object v1

    iget-object v2, p0, LX/0GR;->c:Ljava/lang/String;

    iget-object v4, p0, LX/0GR;->s:Landroid/net/Uri;

    move v3, v9

    move v5, v9

    invoke-direct/range {v0 .. v5}, LX/0G8;-><init>(LX/0OE;Ljava/lang/String;ZLandroid/net/Uri;Z)V

    move-object v1, v0

    goto :goto_2

    .line 35369
    :cond_5
    iget-object v0, p0, LX/0GR;->g:Landroid/content/Context;

    const-string v1, "ExoDashLive"

    invoke-static {v0, v6, v1}, LX/0Gt;->a(Landroid/content/Context;LX/04n;Ljava/lang/String;)LX/0OE;

    move-result-object v1

    goto :goto_2

    :cond_6
    move-object v6, p1

    goto/16 :goto_0
.end method

.method public static a(LX/0GR;LX/0AY;)LX/0GX;
    .locals 24

    .prologue
    .line 35338
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0AY;->a(I)LX/0Am;

    move-result-object v3

    .line 35339
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, LX/0Am;->a(I)I

    move-result v4

    .line 35340
    const/4 v2, 0x0

    .line 35341
    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 35342
    iget-object v2, v3, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Ak;

    .line 35343
    :cond_0
    if-nez v2, :cond_1

    .line 35344
    const/4 v3, 0x0

    .line 35345
    :goto_0
    return-object v3

    .line 35346
    :cond_1
    iget-object v2, v2, LX/0Ak;->c:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Ah;

    iget-object v2, v2, LX/0Ah;->c:LX/0AR;

    iget-object v2, v2, LX/0AR;->b:Ljava/lang/String;

    .line 35347
    const-string v3, "audio/mp4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "audio/mp4a-latm"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 35348
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Creating Audio Sample Source: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", useStandaloneMediaClock: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/0GR;->p:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 35349
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0GR;->a(LX/0GR;LX/04m;)LX/0G6;

    move-result-object v6

    .line 35350
    new-instance v3, LX/0Lq;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0GR;->v:LX/0Od;

    invoke-static {}, LX/0Lt;->a()LX/0Lt;

    move-result-object v5

    new-instance v7, LX/0Le;

    invoke-direct {v7}, LX/0Le;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, LX/0GR;->l:I

    int-to-long v8, v2

    const-wide/16 v10, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, LX/0GR;->h:Landroid/os/Handler;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget v0, v0, LX/0GR;->I:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/0GR;->r:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0GR;->A:LX/0Lm;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/0GR;->B:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/0GR;->C:Z

    move/from16 v22, v0

    const/16 v23, 0x0

    invoke-direct/range {v3 .. v23}, LX/0Lq;-><init>(LX/0Od;LX/0Lr;LX/0G6;LX/04o;JJLandroid/os/Handler;LX/0KU;IZIJLX/0Lm;JZZ)V

    .line 35351
    new-instance v2, LX/0LY;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0GR;->a:LX/0Gb;

    move-object/from16 v0, p0

    iget v5, v0, LX/0GR;->j:I

    move-object/from16 v0, p0

    iget v6, v0, LX/0GR;->i:I

    mul-int/2addr v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0GR;->h:Landroid/os/Handler;

    const/4 v7, 0x0

    const/16 v8, 0x3ea

    invoke-direct/range {v2 .. v8}, LX/0LY;-><init>(LX/0LZ;LX/0Gb;ILandroid/os/Handler;LX/0KZ;I)V

    .line 35352
    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/0GR;->p:Z

    if-eqz v3, :cond_3

    new-instance v3, LX/0GY;

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, LX/0GR;->h:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/0GR;->u:LX/0Jr;

    move-object v4, v2

    invoke-direct/range {v3 .. v8}, LX/0GY;-><init>(LX/0L9;LX/0M3;ZLandroid/os/Handler;LX/0Jr;)V

    goto/16 :goto_0

    :cond_3
    new-instance v3, LX/0GX;

    sget-object v5, LX/0L1;->a:LX/0L1;

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, LX/0GR;->h:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0GR;->u:LX/0Jr;

    move-object v4, v2

    invoke-direct/range {v3 .. v9}, LX/0GX;-><init>(LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jr;)V

    goto/16 :goto_0

    .line 35353
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method public static b(LX/0GR;LX/0AY;)LX/0Jw;
    .locals 24

    .prologue
    .line 35311
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0AY;->a(I)LX/0Am;

    move-result-object v3

    .line 35312
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, LX/0Am;->a(I)I

    move-result v4

    .line 35313
    const/4 v2, 0x0

    .line 35314
    const/4 v5, -0x1

    if-eq v4, v5, :cond_8

    .line 35315
    iget-object v2, v3, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Ak;

    move-object v3, v2

    .line 35316
    :goto_0
    if-nez v3, :cond_0

    .line 35317
    const/4 v3, 0x0

    .line 35318
    :goto_1
    return-object v3

    .line 35319
    :cond_0
    iget-object v2, v3, LX/0Ak;->c:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Ah;

    iget-object v2, v2, LX/0Ah;->c:LX/0AR;

    iget-object v2, v2, LX/0AR;->b:Ljava/lang/String;

    .line 35320
    const-string v4, "video/avc"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "video/mp4"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "video/x-vnd.on2.vp9"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "video/webm"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 35321
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Creating Video Sample Source: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", cachingEnabled = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0GR;->o:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 35322
    const/4 v6, 0x0

    .line 35323
    const/16 v23, 0x0

    .line 35324
    iget-object v2, v3, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x1

    if-le v2, v4, :cond_5

    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/0GR;->n:Z

    if-eqz v2, :cond_5

    .line 35325
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/0GR;->H:Z

    if-eqz v2, :cond_3

    invoke-static {}, LX/0Gi;->a()LX/04m;

    move-result-object v6

    .line 35326
    :goto_2
    new-instance v2, LX/0GG;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0GR;->g:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0GR;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0GR;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/0GR;->E:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/0GR;->y:LX/0GB;

    move-object/from16 v0, p0

    iget-boolean v9, v0, LX/0GR;->H:Z

    if-eqz v9, :cond_2

    move-object/from16 v0, p0

    iget-boolean v9, v0, LX/0GR;->D:Z

    if-eqz v9, :cond_4

    :cond_2
    const/4 v9, 0x1

    :goto_3
    invoke-direct/range {v2 .. v9}, LX/0GG;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/04m;Ljava/util/Map;LX/0GB;Z)V

    .line 35327
    const/16 v23, 0x1

    move-object v7, v2

    .line 35328
    :goto_4
    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0GR;->a(LX/0GR;LX/04m;)LX/0G6;

    move-result-object v6

    .line 35329
    new-instance v3, LX/0Lq;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0GR;->v:LX/0Od;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0GR;->g:Landroid/content/Context;

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-static {v2, v5, v8}, LX/0Lt;->a(Landroid/content/Context;ZZ)LX/0Lt;

    move-result-object v5

    move-object/from16 v0, p0

    iget v2, v0, LX/0GR;->l:I

    int-to-long v8, v2

    const-wide/16 v10, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, LX/0GR;->h:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/0GR;->e:LX/0KU;

    const/4 v14, 0x0

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget v0, v0, LX/0GR;->I:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/0GR;->r:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0GR;->A:LX/0Lm;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/0GR;->B:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/0GR;->C:Z

    move/from16 v22, v0

    invoke-direct/range {v3 .. v23}, LX/0Lq;-><init>(LX/0Od;LX/0Lr;LX/0G6;LX/04o;JJLandroid/os/Handler;LX/0KU;IZIJLX/0Lm;JZZ)V

    .line 35330
    new-instance v2, LX/0LY;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0GR;->a:LX/0Gb;

    move-object/from16 v0, p0

    iget v5, v0, LX/0GR;->k:I

    move-object/from16 v0, p0

    iget v6, v0, LX/0GR;->i:I

    mul-int/2addr v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0GR;->h:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/0GR;->f:LX/0KZ;

    const/16 v8, 0x3e9

    invoke-direct/range {v2 .. v8}, LX/0LY;-><init>(LX/0LZ;LX/0Gb;ILandroid/os/Handler;LX/0KZ;I)V

    .line 35331
    new-instance v3, LX/0Jw;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0GR;->g:Landroid/content/Context;

    sget-object v6, LX/0L1;->a:LX/0L1;

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, LX/0GR;->h:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/0GR;->t:LX/0Ju;

    const/4 v12, -0x1

    move-object v5, v2

    invoke-direct/range {v3 .. v12}, LX/0Jw;-><init>(Landroid/content/Context;LX/0L9;LX/0L1;IJLandroid/os/Handler;LX/0Ju;I)V

    goto/16 :goto_1

    .line 35332
    :cond_3
    new-instance v6, LX/0OC;

    invoke-direct {v6}, LX/0OC;-><init>()V

    goto/16 :goto_2

    .line 35333
    :cond_4
    const/4 v9, 0x0

    goto/16 :goto_3

    .line 35334
    :cond_5
    iget-object v2, v3, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_6

    .line 35335
    new-instance v7, LX/0GO;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0GR;->y:LX/0GB;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0GR;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/0GB;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v7, v2}, LX/0GO;-><init>(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 35336
    :cond_6
    new-instance v7, LX/0Le;

    invoke-direct {v7}, LX/0Le;-><init>()V

    goto/16 :goto_4

    .line 35337
    :cond_7
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unexpected mime type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_8
    move-object v3, v2

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/0GP;)V
    .locals 2

    .prologue
    .line 35302
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Building renderers for dash live with url "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0GR;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 35303
    iget-object v0, p0, LX/0GR;->w:LX/0GK;

    new-instance v1, LX/0GQ;

    invoke-direct {v1, p0, p1}, LX/0GQ;-><init>(LX/0GR;LX/0GP;)V

    .line 35304
    iget-object p0, v0, LX/0GK;->v:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 35305
    invoke-static {v0, v1}, LX/0GK;->b(LX/0GK;LX/0GJ;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 35306
    iget-object p0, v0, LX/0GK;->c:LX/0GH;

    sget-object p1, LX/0GH;->PREPARED:LX/0GH;

    if-ne p0, p1, :cond_1

    .line 35307
    iget-object p0, v0, LX/0GK;->d:LX/0AY;

    invoke-interface {v1, p0}, LX/0GJ;->a(Ljava/lang/Object;)V

    .line 35308
    :cond_0
    :goto_0
    return-void

    .line 35309
    :cond_1
    iput-object v1, v0, LX/0GK;->f:LX/0GJ;

    .line 35310
    invoke-virtual {v0}, LX/0GK;->d()V

    goto :goto_0
.end method
