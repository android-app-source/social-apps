.class public LX/0K9;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/043;

.field private final c:Landroid/content/Context;

.field public final d:LX/0OV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/0OV;

.field private final f:LX/0KQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/0KB;

.field private final h:LX/0KK;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:LX/0KK;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40381
    const-class v0, LX/0K9;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0K9;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/043;LX/0KQ;)V
    .locals 9
    .param p3    # LX/0KQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 40341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40342
    iput-object p1, p0, LX/0K9;->c:Landroid/content/Context;

    .line 40343
    iput-object p2, p0, LX/0K9;->b:LX/043;

    .line 40344
    iput-object p3, p0, LX/0K9;->f:LX/0KQ;

    .line 40345
    new-instance v0, LX/0KB;

    invoke-static {p0}, LX/0K9;->h(LX/0K9;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0KB;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/0K9;->g:LX/0KB;

    .line 40346
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget-boolean v0, v0, LX/043;->d:Z

    if-nez v0, :cond_0

    .line 40347
    iput-object v6, p0, LX/0K9;->d:LX/0OV;

    .line 40348
    iput-object v6, p0, LX/0K9;->e:LX/0OV;

    .line 40349
    iput-object v6, p0, LX/0K9;->h:LX/0KK;

    .line 40350
    iput-object v6, p0, LX/0K9;->i:LX/0KK;

    .line 40351
    :goto_0
    return-void

    .line 40352
    :cond_0
    invoke-static {p0}, LX/0K9;->f(LX/0K9;)Ljava/io/File;

    move-result-object v0

    .line 40353
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 40354
    invoke-static {p0}, LX/0K9;->g(LX/0K9;)Ljava/io/File;

    move-result-object v0

    .line 40355
    iget-object v1, p0, LX/0K9;->b:LX/043;

    iget-boolean v1, v1, LX/043;->g:Z

    if-eqz v1, :cond_4

    .line 40356
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 40357
    new-instance v0, LX/0OV;

    invoke-static {p0}, LX/0K9;->g(LX/0K9;)Ljava/io/File;

    move-result-object v1

    new-instance v2, LX/0OU;

    iget-object v3, p0, LX/0K9;->b:LX/043;

    iget v3, v3, LX/043;->h:I

    int-to-long v4, v3

    invoke-direct {v2, v4, v5}, LX/0OU;-><init>(J)V

    invoke-direct {v0, v1, v2}, LX/0OV;-><init>(Ljava/io/File;LX/0K7;)V

    iput-object v0, p0, LX/0K9;->e:LX/0OV;

    .line 40358
    new-instance v0, LX/0OV;

    invoke-static {p0}, LX/0K9;->f(LX/0K9;)Ljava/io/File;

    move-result-object v1

    new-instance v2, LX/0K8;

    iget-object v3, p0, LX/0K9;->b:LX/043;

    iget v3, v3, LX/043;->i:I

    int-to-long v4, v3

    iget-object v3, p0, LX/0K9;->e:LX/0OV;

    invoke-direct {v2, p0, v4, v5, v3}, LX/0K8;-><init>(LX/0K9;JLX/0OQ;)V

    invoke-direct {v0, v1, v2}, LX/0OV;-><init>(Ljava/io/File;LX/0K7;)V

    iput-object v0, p0, LX/0K9;->d:LX/0OV;

    .line 40359
    new-instance v0, LX/0KK;

    iget-object v1, p0, LX/0K9;->e:LX/0OV;

    invoke-direct {v0, v1}, LX/0KK;-><init>(LX/0OQ;)V

    iput-object v0, p0, LX/0K9;->h:LX/0KK;

    .line 40360
    :goto_1
    new-instance v0, LX/0KK;

    iget-object v1, p0, LX/0K9;->d:LX/0OV;

    invoke-direct {v0, v1}, LX/0KK;-><init>(LX/0OQ;)V

    iput-object v0, p0, LX/0K9;->i:LX/0KK;

    .line 40361
    iget-object v0, p0, LX/0K9;->g:LX/0KB;

    iget-object v1, p0, LX/0K9;->d:LX/0OV;

    iget-object v2, p0, LX/0K9;->e:LX/0OV;

    const/4 v4, 0x0

    .line 40362
    iget-object v3, v0, LX/0KB;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 40363
    if-eqz v5, :cond_2

    .line 40364
    array-length v6, v5

    move v3, v4

    :goto_2
    if-ge v3, v6, :cond_2

    aget-object v7, v5, v3

    .line 40365
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v8}, LX/0OQ;->a(Ljava/lang/String;)Ljava/util/NavigableSet;

    move-result-object v8

    if-nez v8, :cond_1

    if-eqz v2, :cond_1

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v8}, LX/0OQ;->a(Ljava/lang/String;)Ljava/util/NavigableSet;

    move-result-object v8

    if-nez v8, :cond_1

    .line 40366
    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p3

    aput-object p3, p2, v4

    .line 40367
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 40368
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 40369
    :cond_2
    invoke-direct {p0}, LX/0K9;->j()Ljava/util/Set;

    move-result-object v0

    invoke-static {p0}, LX/0K9;->h(LX/0K9;)Ljava/io/File;

    move-result-object v1

    const/4 v3, 0x0

    .line 40370
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 40371
    if-nez v4, :cond_5

    .line 40372
    :cond_3
    goto/16 :goto_0

    .line 40373
    :cond_4
    iput-object v6, p0, LX/0K9;->e:LX/0OV;

    .line 40374
    new-instance v0, LX/0OV;

    invoke-static {p0}, LX/0K9;->f(LX/0K9;)Ljava/io/File;

    move-result-object v1

    new-instance v2, LX/0OU;

    iget-object v3, p0, LX/0K9;->b:LX/043;

    iget v3, v3, LX/043;->f:I

    int-to-long v4, v3

    invoke-direct {v2, v4, v5}, LX/0OU;-><init>(J)V

    invoke-direct {v0, v1, v2}, LX/0OV;-><init>(Ljava/io/File;LX/0K7;)V

    iput-object v0, p0, LX/0K9;->d:LX/0OV;

    .line 40375
    iput-object v6, p0, LX/0K9;->h:LX/0KK;

    goto :goto_1

    .line 40376
    :cond_5
    array-length v5, v4

    move v2, v3

    :goto_3
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 40377
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".v1.meta"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 40378
    array-length v8, v7

    if-eqz v8, :cond_6

    aget-object v7, v7, v3

    invoke-interface {v0, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 40379
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 40380
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 40326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 40327
    invoke-virtual {p2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "127.0.0.1"

    invoke-virtual {p2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40328
    const-string v1, "remote-uri"

    invoke-virtual {p2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    .line 40329
    :cond_0
    if-nez p0, :cond_1

    .line 40330
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40331
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40332
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40333
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 40334
    :goto_0
    return-object v0

    .line 40335
    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40336
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40337
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40338
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40339
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40340
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/0K9;Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;JI)V
    .locals 8

    .prologue
    .line 40321
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget-boolean v0, v0, LX/043;->j:Z

    if-nez v0, :cond_0

    .line 40322
    :goto_0
    return-void

    .line 40323
    :cond_0
    new-instance v0, LX/0KN;

    invoke-static {p0}, LX/0K9;->h(LX/0K9;)Ljava/io/File;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/0KN;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 40324
    iget-object v1, p1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, LX/0K9;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 40325
    const/4 v5, -0x1

    iget v6, p1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->g:I

    move-wide v2, p2

    move v4, p4

    invoke-virtual/range {v0 .. v6}, LX/0KN;->a(Ljava/lang/String;JIII)V

    goto :goto_0
.end method

.method private static b(LX/0K9;Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;Landroid/net/Uri;LX/04m;)J
    .locals 18

    .prologue
    .line 40275
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->b:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-static {v2, v3, v4}, LX/0K9;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v15

    .line 40276
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0K9;->g:LX/0KB;

    invoke-virtual {v2, v15}, LX/0KB;->a(Ljava/lang/String;)LX/0KA;

    move-result-object v14

    .line 40277
    if-eqz v14, :cond_0

    .line 40278
    iget-wide v2, v14, LX/0KA;->a:J

    move-object/from16 v0, p1

    iget v4, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->d:I

    int-to-long v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    move-object/from16 v0, p1

    iput v2, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->d:I

    .line 40279
    :cond_0
    new-instance v16, LX/0K6;

    move-object/from16 v0, p1

    iget v2, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->d:I

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, LX/0K6;-><init>(I)V

    .line 40280
    invoke-virtual/range {p0 .. p0}, LX/0K9;->a()LX/0OV;

    move-result-object v2

    .line 40281
    if-eqz v2, :cond_1

    .line 40282
    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, LX/0OV;->a(Ljava/lang/String;LX/0K5;)Ljava/util/NavigableSet;

    .line 40283
    :cond_1
    sget-object v2, LX/0K9;->a:Ljava/lang/String;

    const-string v3, "Prefetching VideoId:%s, PrefetchLength:%d  CacheKey:%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p1

    iget v6, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object v15, v4, v5

    invoke-static {v2, v3, v4}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40284
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->b:Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x1

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-object/from16 v9, p3

    invoke-virtual/range {v2 .. v9}, LX/0K9;->a(Ljava/lang/String;Landroid/net/Uri;IZZLjava/util/Map;LX/04m;)LX/0G6;

    move-result-object v17

    .line 40285
    const-wide/16 v12, 0x0

    .line 40286
    :try_start_0
    new-instance v2, LX/0OA;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v0, p1

    iget v8, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->d:I

    int-to-long v8, v8

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->c:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-direct/range {v2 .. v11}, LX/0OA;-><init>(Landroid/net/Uri;JJJLjava/lang/String;I)V

    .line 40287
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, LX/0G6;->a(LX/0OA;)J

    move-result-wide v2

    move-object/from16 v0, p1

    iget v4, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->d:I

    int-to-long v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    move-object/from16 v0, p1

    iput v2, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->d:I

    .line 40288
    const v2, 0xffff

    new-array v6, v2, [B
    :try_end_0
    .catch LX/0OL; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40289
    const-wide/16 v2, 0x0

    move-wide v4, v2

    move-wide v2, v12

    .line 40290
    :goto_0
    :try_start_1
    move-object/from16 v0, p1

    iget v7, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->d:I

    int-to-long v8, v7

    cmp-long v7, v2, v8

    if-eqz v7, :cond_2

    .line 40291
    const/4 v7, 0x0

    const v8, 0xffff

    move-object/from16 v0, p1

    iget v9, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->d:I

    long-to-int v4, v4

    sub-int v4, v9, v4

    invoke-static {v8, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    move-object/from16 v0, v17

    invoke-interface {v0, v6, v7, v4}, LX/0G6;->a([BII)I
    :try_end_1
    .catch LX/0OL; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    int-to-long v4, v4

    .line 40292
    const-wide/16 v8, -0x1

    cmp-long v7, v4, v8

    if-eqz v7, :cond_2

    .line 40293
    add-long/2addr v2, v4

    goto :goto_0

    .line 40294
    :cond_2
    invoke-interface/range {v17 .. v17}, LX/0G6;->a()V

    .line 40295
    invoke-virtual/range {p0 .. p0}, LX/0K9;->a()LX/0OV;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v15, v0}, LX/0OV;->b(Ljava/lang/String;LX/0K5;)V

    .line 40296
    :goto_1
    sget-object v4, LX/0K9;->a:Ljava/lang/String;

    const-string v5, "Prefetched VideoId:%s, PrefetchLength: %d Actual prefetch length :%d  CacheKey:%s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->b:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    move-object/from16 v0, p1

    iget v8, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->d:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static/range {v16 .. v16}, LX/0K6;->a(LX/0K6;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object v15, v6, v7

    invoke-static {v4, v5, v6}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40297
    if-nez v14, :cond_8

    .line 40298
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0K9;->g:LX/0KB;

    invoke-virtual {v4, v15}, LX/0KB;->a(Ljava/lang/String;)LX/0KA;

    move-result-object v4

    .line 40299
    :goto_2
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-lez v5, :cond_3

    .line 40300
    if-eqz v4, :cond_5

    iget-wide v4, v4, LX/0KA;->a:J

    :goto_3
    long-to-int v6, v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v5, v6}, LX/0K9;->a(LX/0K9;Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;JI)V

    .line 40301
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0K9;->f:LX/0KQ;

    if-nez v4, :cond_6

    .line 40302
    :goto_4
    return-wide v2

    .line 40303
    :catch_0
    move-exception v2

    move-object v4, v2

    move-wide v2, v12

    .line 40304
    :goto_5
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-nez v5, :cond_4

    .line 40305
    :try_start_2
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 40306
    :catchall_0
    move-exception v2

    invoke-interface/range {v17 .. v17}, LX/0G6;->a()V

    .line 40307
    invoke-virtual/range {p0 .. p0}, LX/0K9;->a()LX/0OV;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, LX/0OV;->b(Ljava/lang/String;LX/0K5;)V

    throw v2

    .line 40308
    :cond_4
    invoke-interface/range {v17 .. v17}, LX/0G6;->a()V

    .line 40309
    invoke-virtual/range {p0 .. p0}, LX/0K9;->a()LX/0OV;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v15, v0}, LX/0OV;->b(Ljava/lang/String;LX/0K5;)V

    goto :goto_1

    .line 40310
    :catch_1
    move-exception v2

    move-object v4, v2

    move-wide v2, v12

    .line 40311
    :goto_6
    :try_start_3
    sget-object v5, LX/0K9;->a:Ljava/lang/String;

    const-string v6, "IO Exception prefetching CacheKey:%s, Read:%d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->c:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v4, v6, v7}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 40312
    invoke-interface/range {v17 .. v17}, LX/0G6;->a()V

    .line 40313
    invoke-virtual/range {p0 .. p0}, LX/0K9;->a()LX/0OV;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v15, v0}, LX/0OV;->b(Ljava/lang/String;LX/0K5;)V

    goto/16 :goto_1

    .line 40314
    :cond_5
    const-wide v4, 0x7fffffffffffffffL

    goto :goto_3

    .line 40315
    :cond_6
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0K9;->b(Ljava/lang/String;)Lcom/facebook/exoplayer/ipc/VideoCacheStatus;

    move-result-object v2

    .line 40316
    if-eqz v2, :cond_7

    iget-boolean v3, v2, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->a:Z

    if-eqz v3, :cond_7

    .line 40317
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0K9;->f:LX/0KQ;

    sget-object v4, LX/0H9;->PREFETCH_COMPLETE:LX/0H9;

    new-instance v5, Lcom/facebook/exoplayer/ipc/VpsPrefetchCompleteEvent;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->b:Ljava/lang/String;

    invoke-direct {v5, v6, v2}, Lcom/facebook/exoplayer/ipc/VpsPrefetchCompleteEvent;-><init>(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoCacheStatus;)V

    invoke-virtual {v3, v4, v5}, LX/0KQ;->a(LX/0H9;Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V

    .line 40318
    :cond_7
    invoke-static/range {v16 .. v16}, LX/0K6;->a(LX/0K6;)J

    move-result-wide v2

    goto :goto_4

    .line 40319
    :catch_2
    move-exception v4

    goto :goto_6

    .line 40320
    :catch_3
    move-exception v4

    goto :goto_5

    :cond_8
    move-object v4, v14

    goto/16 :goto_2
.end method

.method public static f(LX/0K9;)Ljava/io/File;
    .locals 3

    .prologue
    .line 40274
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/0K9;->b:LX/043;

    iget-object v2, v2, LX/043;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ExoPlayerCacheDir/videocache"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static g(LX/0K9;)Ljava/io/File;
    .locals 3

    .prologue
    .line 40273
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/0K9;->b:LX/043;

    iget-object v2, v2, LX/043;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ExoPlayerCacheDir/videoprefetchcache"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static h(LX/0K9;)Ljava/io/File;
    .locals 3

    .prologue
    .line 40272
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/0K9;->b:LX/043;

    iget-object v2, v2, LX/043;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ExoPlayerCacheDir/videocachemetadata"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static i(LX/0K9;)J
    .locals 2

    .prologue
    .line 40382
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget-boolean v0, v0, LX/043;->g:Z

    if-eqz v0, :cond_0

    .line 40383
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget v0, v0, LX/043;->h:I

    int-to-long v0, v0

    .line 40384
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget v0, v0, LX/043;->f:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method private j()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40263
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 40264
    invoke-virtual {p0}, LX/0K9;->a()LX/0OV;

    move-result-object v0

    .line 40265
    if-eqz v0, :cond_1

    .line 40266
    invoke-virtual {v0}, LX/0OV;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 40267
    const/16 v3, 0x2e

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 40268
    if-gtz v3, :cond_2

    const/4 v3, 0x0

    :goto_1
    move-object v0, v3

    .line 40269
    if-eqz v0, :cond_0

    .line 40270
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 40271
    :cond_1
    return-object v1

    :cond_2
    const/4 p0, 0x0

    invoke-virtual {v0, p0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;Landroid/net/Uri;LX/04m;)J
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x5
    .end annotation

    .prologue
    .line 40257
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->d:I

    int-to-long v0, v0

    invoke-static {p0}, LX/0K9;->i(LX/0K9;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 40258
    invoke-static {p0, p1, p2, p3}, LX/0K9;->b(LX/0K9;Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;Landroid/net/Uri;LX/04m;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 40259
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 40260
    :cond_0
    :try_start_1
    sget-object v0, LX/0K9;->a:Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Attempting to prefetch more bytes than the prefetch size %s %d %d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p1, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p0}, LX/0K9;->i(LX/0K9;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 40261
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 40262
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;IZZLjava/util/Map;LX/04m;)LX/0G6;
    .locals 6
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "IZZ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/04m;",
            ")",
            "LX/0G6;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 40235
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget-boolean v0, v0, LX/043;->d:Z

    if-eqz v0, :cond_6

    .line 40236
    if-eqz p5, :cond_4

    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget-boolean v0, v0, LX/043;->k:Z

    if-eqz v0, :cond_4

    move v0, v3

    .line 40237
    :goto_0
    if-eqz v0, :cond_5

    .line 40238
    invoke-static {p0}, LX/0K9;->f(LX/0K9;)Ljava/io/File;

    move-result-object v0

    .line 40239
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 40240
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 40241
    :cond_0
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget-boolean v0, v0, LX/043;->g:Z

    if-eqz v0, :cond_1

    .line 40242
    invoke-static {p0}, LX/0K9;->g(LX/0K9;)Ljava/io/File;

    move-result-object v0

    .line 40243
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 40244
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 40245
    :cond_1
    invoke-static {p0}, LX/0K9;->h(LX/0K9;)Ljava/io/File;

    move-result-object v0

    .line 40246
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 40247
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 40248
    :cond_2
    new-instance v1, LX/0Gf;

    new-instance v0, LX/0OD;

    const-string v2, "ExoService"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, p7}, LX/0OD;-><init>(Ljava/lang/String;LX/0OH;LX/04n;)V

    invoke-direct {v1, p2, p1, v0, p3}, LX/0Gf;-><init>(Landroid/net/Uri;Ljava/lang/String;LX/0Ge;I)V

    .line 40249
    new-instance v0, LX/0KI;

    invoke-direct {v0, p1, p0, v1, p4}, LX/0KI;-><init>(Ljava/lang/String;LX/0K9;LX/0Ge;Z)V

    .line 40250
    iget-object v1, p0, LX/0K9;->i:LX/0KK;

    invoke-virtual {v0, v1}, LX/0KI;->a(LX/0KH;)V

    .line 40251
    :goto_1
    invoke-interface {p6}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 40252
    new-instance v1, LX/0KL;

    invoke-direct {v1, p1, v0, p6}, LX/0KL;-><init>(Ljava/lang/String;LX/0G6;Ljava/util/Map;)V

    move-object v0, v1

    .line 40253
    :cond_3
    return-object v0

    .line 40254
    :cond_4
    if-nez p5, :cond_6

    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget-boolean v0, v0, LX/043;->l:Z

    if-eqz v0, :cond_6

    move v0, v3

    .line 40255
    goto :goto_0

    .line 40256
    :cond_5
    new-instance v0, LX/0G8;

    new-instance v1, LX/0OE;

    iget-object v2, p0, LX/0K9;->c:Landroid/content/Context;

    const-string v4, "ExoService"

    invoke-direct {v1, v2, p7, v4}, LX/0OE;-><init>(Landroid/content/Context;LX/04n;Ljava/lang/String;)V

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LX/0G8;-><init>(LX/0OE;Ljava/lang/String;ZLandroid/net/Uri;Z)V

    goto :goto_1

    :cond_6
    move v0, v5

    goto :goto_0
.end method

.method public final a()LX/0OV;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 40232
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget-boolean v0, v0, LX/043;->g:Z

    if-eqz v0, :cond_0

    .line 40233
    iget-object v0, p0, LX/0K9;->e:LX/0OV;

    .line 40234
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0K9;->d:LX/0OV;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/0KA;)V
    .locals 1

    .prologue
    .line 40230
    iget-object v0, p0, LX/0K9;->g:LX/0KB;

    invoke-virtual {v0, p1, p2}, LX/0KB;->a(Ljava/lang/String;LX/0KA;)V

    .line 40231
    return-void
.end method

.method public final b()LX/0OV;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 40229
    iget-object v0, p0, LX/0K9;->d:LX/0OV;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/exoplayer/ipc/VideoCacheStatus;
    .locals 2

    .prologue
    .line 40222
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget-boolean v0, v0, LX/043;->j:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/0K9;->h(LX/0K9;)Ljava/io/File;

    move-result-object v0

    .line 40223
    invoke-static {v0, p1}, LX/0KN;->b(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    move v0, v1

    .line 40224
    if-nez v0, :cond_1

    .line 40225
    :cond_0
    const/4 v0, 0x0

    .line 40226
    :goto_0
    return-object v0

    .line 40227
    :cond_1
    new-instance v0, LX/0KN;

    invoke-static {p0}, LX/0K9;->h(LX/0K9;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/0KN;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 40228
    invoke-virtual {v0}, LX/0KN;->a()Lcom/facebook/exoplayer/ipc/VideoCacheStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 40219
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget-boolean v0, v0, LX/043;->g:Z

    if-eqz v0, :cond_0

    .line 40220
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget v0, v0, LX/043;->i:I

    int-to-long v0, v0

    .line 40221
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget v0, v0, LX/043;->f:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final d()LX/0KJ;
    .locals 12

    .prologue
    const-wide/16 v2, 0x0

    .line 40211
    iget-object v0, p0, LX/0K9;->b:LX/043;

    iget-boolean v0, v0, LX/043;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0K9;->i:LX/0KK;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0K9;->h:LX/0KK;

    if-nez v0, :cond_1

    .line 40212
    :cond_0
    new-instance v1, LX/0KJ;

    move-wide v4, v2

    move-wide v6, v2

    move-wide v8, v2

    invoke-direct/range {v1 .. v9}, LX/0KJ;-><init>(JJJJ)V

    .line 40213
    :goto_0
    return-object v1

    .line 40214
    :cond_1
    iget-object v0, p0, LX/0K9;->i:LX/0KK;

    invoke-virtual {v0}, LX/0KK;->a()LX/0KJ;

    move-result-object v0

    .line 40215
    iget-object v1, p0, LX/0K9;->b:LX/043;

    iget-boolean v1, v1, LX/043;->g:Z

    if-nez v1, :cond_2

    move-object v1, v0

    .line 40216
    goto :goto_0

    .line 40217
    :cond_2
    iget-object v1, p0, LX/0K9;->h:LX/0KK;

    invoke-virtual {v1}, LX/0KK;->a()LX/0KJ;

    move-result-object v8

    .line 40218
    new-instance v1, LX/0KJ;

    iget-wide v2, v0, LX/0KJ;->a:J

    iget-wide v4, v8, LX/0KJ;->a:J

    add-long/2addr v2, v4

    iget-wide v4, v0, LX/0KJ;->b:J

    iget-wide v6, v8, LX/0KJ;->b:J

    add-long/2addr v4, v6

    iget-wide v6, v0, LX/0KJ;->c:J

    iget-wide v10, v8, LX/0KJ;->c:J

    add-long/2addr v6, v10

    iget-wide v10, v0, LX/0KJ;->d:J

    iget-wide v8, v8, LX/0KJ;->d:J

    add-long/2addr v8, v10

    invoke-direct/range {v1 .. v9}, LX/0KJ;-><init>(JJJJ)V

    goto :goto_0
.end method
