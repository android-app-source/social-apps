.class public LX/02s;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final d:Ljava/io/FilenameFilter;

.field public static final e:Ljava/io/FilenameFilter;


# instance fields
.field public a:LX/039;

.field public b:I

.field public c:J

.field public final f:Ljava/io/File;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9718
    new-instance v0, LX/03B;

    invoke-direct {v0}, LX/03B;-><init>()V

    sput-object v0, LX/02s;->d:Ljava/io/FilenameFilter;

    .line 9719
    new-instance v0, LX/03C;

    invoke-direct {v0}, LX/03C;-><init>()V

    sput-object v0, LX/02s;->e:Ljava/io/FilenameFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 9706
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    .line 9707
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    .line 9708
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 9709
    if-eqz v0, :cond_1

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v3, "mounted"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9710
    invoke-virtual {p1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    .line 9711
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 9712
    :cond_0
    :goto_0
    move-object v0, v0

    .line 9713
    invoke-direct {p0, v0}, LX/02s;-><init>(Ljava/io/File;)V

    .line 9714
    return-void

    .line 9715
    :cond_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move-object v0, v1

    .line 9716
    goto :goto_0

    :cond_3
    move-object v0, v2

    .line 9717
    goto :goto_0
.end method

.method private constructor <init>(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 9700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9701
    const/4 v0, 0x0

    iput v0, p0, LX/02s;->b:I

    .line 9702
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/02s;->c:J

    .line 9703
    new-instance v0, LX/039;

    invoke-direct {v0}, LX/039;-><init>()V

    iput-object v0, p0, LX/02s;->a:LX/039;

    .line 9704
    iput-object p1, p0, LX/02s;->f:Ljava/io/File;

    .line 9705
    return-void
.end method

.method public static a(Ljava/io/File;Ljava/io/FilenameFilter;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/io/FilenameFilter;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9660
    invoke-virtual {p0, p1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    .line 9661
    if-nez v0, :cond_0

    .line 9662
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 9663
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/02s;Ljava/io/File;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 9690
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    .line 9691
    :cond_0
    return-void

    .line 9692
    :cond_1
    sget-object v0, LX/02s;->d:Ljava/io/FilenameFilter;

    invoke-static {p1, v0}, LX/02s;->a(Ljava/io/File;Ljava/io/FilenameFilter;)Ljava/util/List;

    move-result-object v0

    .line 9693
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p2, :cond_0

    .line 9694
    new-instance v1, LX/03D;

    invoke-direct {v1, p0}, LX/03D;-><init>(LX/02s;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 9695
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, p2

    invoke-interface {v0, v5, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 9696
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 9697
    iget-object v0, p0, LX/02s;->a:LX/039;

    iget v2, v0, LX/039;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, LX/039;->e:I

    goto :goto_0

    .line 9698
    :cond_2
    iget-object v2, p0, LX/02s;->a:LX/039;

    iget v3, v2, LX/039;->d:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, LX/039;->d:I

    .line 9699
    const-string v2, "FileManager"

    const-string v3, "Can\'t delete cache file: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(LX/02s;Ljava/io/File;Ljava/io/File;J)V
    .locals 7

    .prologue
    .line 9681
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    .line 9682
    :cond_0
    return-void

    .line 9683
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v2, v0, p3

    .line 9684
    sget-object v0, LX/02s;->d:Ljava/io/FilenameFilter;

    invoke-static {p1, v0}, LX/02s;->a(Ljava/io/File;Ljava/io/FilenameFilter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 9685
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-gez v4, :cond_2

    .line 9686
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 9687
    new-instance v4, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {p0, v0, v4}, LX/02s;->a(LX/02s;Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 9688
    iget-object v0, p0, LX/02s;->a:LX/039;

    iget v4, v0, LX/039;->f:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, LX/039;->f:I

    goto :goto_0

    .line 9689
    :cond_3
    iget-object v0, p0, LX/02s;->a:LX/039;

    iget v4, v0, LX/039;->d:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, LX/039;->d:I

    goto :goto_0
.end method

.method public static a(LX/02s;Ljava/io/File;Ljava/io/File;)Z
    .locals 6
    .param p1    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9672
    if-eqz p2, :cond_1

    .line 9673
    invoke-virtual {p1, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 9674
    :goto_0
    return v0

    .line 9675
    :cond_0
    iget-object v2, p0, LX/02s;->a:LX/039;

    iget v3, v2, LX/039;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, LX/039;->b:I

    .line 9676
    const-string v2, "FileManager"

    const-string v3, "Can\'t move file: %s"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9677
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_2

    .line 9678
    iget-object v2, p0, LX/02s;->a:LX/039;

    iget v3, v2, LX/039;->a:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, LX/039;->a:I

    .line 9679
    const-string v2, "FileManager"

    const-string v3, "Can\'t delete file: %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    move v0, v1

    .line 9680
    goto :goto_0
.end method

.method public static g(LX/02s;)Ljava/io/File;
    .locals 3

    .prologue
    .line 9671
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/02s;->f:Ljava/io/File;

    const-string v2, "upload"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final d()Ljava/lang/Iterable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9664
    invoke-static {p0}, LX/02s;->g(LX/02s;)Ljava/io/File;

    move-result-object v0

    sget-object v1, LX/02s;->e:Ljava/io/FilenameFilter;

    invoke-static {v0, v1}, LX/02s;->a(Ljava/io/File;Ljava/io/FilenameFilter;)Ljava/util/List;

    move-result-object v0

    .line 9665
    invoke-static {p0}, LX/02s;->g(LX/02s;)Ljava/io/File;

    move-result-object v1

    sget-object v2, LX/02s;->d:Ljava/io/FilenameFilter;

    invoke-static {v1, v2}, LX/02s;->a(Ljava/io/File;Ljava/io/FilenameFilter;)Ljava/util/List;

    move-result-object v1

    .line 9666
    iget-object v2, p0, LX/02s;->f:Ljava/io/File;

    move-object v2, v2

    .line 9667
    sget-object v3, LX/02s;->e:Ljava/io/FilenameFilter;

    invoke-static {v2, v3}, LX/02s;->a(Ljava/io/File;Ljava/io/FilenameFilter;)Ljava/util/List;

    move-result-object v2

    .line 9668
    iget-object v3, p0, LX/02s;->f:Ljava/io/File;

    move-object v3, v3

    .line 9669
    sget-object v4, LX/02s;->d:Ljava/io/FilenameFilter;

    invoke-static {v3, v4}, LX/02s;->a(Ljava/io/File;Ljava/io/FilenameFilter;)Ljava/util/List;

    move-result-object v3

    .line 9670
    invoke-static {v2, v3, v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
