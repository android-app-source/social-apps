.class public final LX/0AK;
.super Landroid/util/SparseArray;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/SparseArray",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23646
    invoke-direct {p0}, Landroid/util/SparseArray;-><init>()V

    .line 23647
    const/4 v0, 0x0

    const-string v1, "/buddy_list"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23648
    const/4 v0, 0x1

    const-string v1, "/create_thread"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23649
    const/4 v0, 0x2

    const-string v1, "/create_thread_response"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23650
    const/4 v0, 0x3

    const-string v1, "/delete_thread_notification"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23651
    const/4 v0, 0x4

    const-string v1, "/delete_messages_notification"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23652
    const/4 v0, 0x5

    const-string v1, "/orca_message_notifications"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23653
    const/4 v0, 0x6

    const-string v1, "/friending_state_change"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23654
    const/4 v0, 0x7

    const-string v1, "/friend_request"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23655
    const/16 v0, 0x8

    const-string v1, "/friend_requests_seen"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23656
    const/16 v0, 0x9

    const-string v1, "/graphql"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23657
    const/16 v0, 0xa

    const-string v1, "/group_msg"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23658
    const/16 v0, 0xb

    const-string v1, "/group_notifs_unseen"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23659
    const/16 v0, 0xc

    const-string v1, "/group_msgs_unseen"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23660
    const/16 v0, 0xd

    const-string v1, "/inbox"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23661
    const/16 v0, 0xe

    const-string v1, "/action_id_notification"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23662
    const/16 v0, 0xf

    const-string v1, "/aura_notification"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23663
    const/16 v0, 0x10

    const-string v1, "/aura_signal"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23664
    const/16 v0, 0x11

    const-string v1, "/friends_locations"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23665
    const/16 v0, 0x12

    const-string v1, "/mark_thread"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23666
    const/16 v0, 0x13

    const-string v1, "/mark_thread_response"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23667
    const/16 v0, 0x14

    const-string v1, "/mercury"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23668
    const/16 v0, 0x15

    const-string v1, "/messenger_sync"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23669
    const/16 v0, 0x16

    const-string v1, "/messenger_sync_ack"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23670
    const/16 v0, 0x17

    const-string v1, "/messenger_sync_create_queue"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23671
    const/16 v0, 0x18

    const-string v1, "/messenger_sync_get_diffs"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23672
    const/16 v0, 0x19

    const-string v1, "/messaging"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23673
    const/16 v0, 0x1a

    const-string v1, "/messaging_events"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23674
    const/16 v0, 0x1b

    const-string v1, "/mobile_requests_count"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23675
    const/16 v0, 0x1c

    const-string v1, "/mobile_video_encode"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23676
    const/16 v0, 0x1d

    const-string v1, "/orca_notification_updates"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23677
    const/16 v0, 0x1e

    const-string v1, "/notifications_sync"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23678
    const/16 v0, 0x1f

    const-string v1, "/notifications_read"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23679
    const/16 v0, 0x20

    const-string v1, "/notifications_seen"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23680
    const/16 v0, 0x21

    const-string v1, "/push_notification"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23681
    const/16 v0, 0x22

    const-string v1, "/pp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23682
    const/16 v0, 0x23

    const-string v1, "/orca_presence"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23683
    const/16 v0, 0x24

    const-string v1, "/privacy_changed"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23684
    const/16 v0, 0x25

    const-string v1, "/privacy_updates"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23685
    const/16 v0, 0x26

    const-string v1, "/send_additional_contacts"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23686
    const/16 v0, 0x27

    const-string v1, "/send_chat_event"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23687
    const/16 v0, 0x28

    const-string v1, "/send_delivery_receipt"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23688
    const/16 v0, 0x29

    const-string v1, "/send_endpoint_capabilities"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23689
    const/16 v0, 0x2a

    const-string v1, "/foreground_state"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23690
    const/16 v0, 0x2b

    const-string v1, "/aura_location"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23691
    const/16 v0, 0x2c

    const-string v1, "/send_location"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23692
    const/16 v0, 0x2d

    const-string v1, "/send_message2"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23693
    const/16 v0, 0x2e

    const-string v1, "/send_message"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23694
    const/16 v0, 0x2f

    const-string v1, "/send_message_response"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23695
    const/16 v0, 0x30

    const-string v1, "/ping"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23696
    const/16 v0, 0x31

    const-string v1, "/presence"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23697
    const/16 v0, 0x32

    const-string v1, "/send_push_notification_ack"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23698
    const/16 v0, 0x33

    const-string v1, "/rich_presence"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23699
    const/16 v0, 0x34

    const-string v1, "/send_skype"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23700
    const/16 v0, 0x35

    const-string v1, "/typing"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23701
    const/16 v0, 0x36

    const-string v1, "/set_client_settings"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23702
    const/16 v0, 0x37

    const-string v1, "/shoerack_notifications"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23703
    const/16 v0, 0x38

    const-string v1, "/orca_ticker_updates"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23704
    const/16 v0, 0x39

    const-string v1, "/orca_typing_notifications"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23705
    const/16 v0, 0x3a

    const-string v1, "/typ"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23706
    const/16 v0, 0x3b

    const-string v1, "/t_ms"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23707
    const/16 v0, 0x3c

    const-string v1, "/orca_video_notifications"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23708
    const/16 v0, 0x3d

    const-string v1, "/orca_visibility_updates"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23709
    const/16 v0, 0x3e

    const-string v1, "/webrtc"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23710
    const/16 v0, 0x3f

    const-string v1, "/webrtc_response"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23711
    const/16 v0, 0x40

    const-string v1, "/subscribe"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23712
    const/16 v0, 0x41

    const-string v1, "/t_p"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23713
    const/16 v0, 0x42

    const-string v1, "/push_ack"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23714
    const/16 v0, 0x44

    const-string v1, "/webrtc_binary"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23715
    const/16 v0, 0x45

    const-string v1, "/t_sm"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23716
    const/16 v0, 0x46

    const-string v1, "/t_sm_rp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23717
    const/16 v0, 0x47

    const-string v1, "/t_vs"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23718
    const/16 v0, 0x48

    const-string v1, "/t_rtc"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23719
    const/16 v0, 0x49

    const-string v1, "/echo"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23720
    const/16 v0, 0x4a

    const-string v1, "/pages_messaging"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23721
    const/16 v0, 0x4b

    const-string v1, "/t_omnistore_sync"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23722
    const/16 v0, 0x4c

    const-string v1, "/fbns_msg"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23723
    const/16 v0, 0x4d

    const-string v1, "/t_ps"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23724
    const/16 v0, 0x4e

    const-string v1, "/t_dr_batch"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23725
    const/16 v0, 0x4f

    const-string v1, "/fbns_reg_req"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23726
    const/16 v0, 0x50

    const-string v1, "/fbns_reg_resp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23727
    const/16 v0, 0x51

    const-string v1, "/omnistore_subscribe_collection"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23728
    const/16 v0, 0x52

    const-string v1, "/fbns_unreg_req"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23729
    const/16 v0, 0x53

    const-string v1, "/fbns_unreg_resp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23730
    const/16 v0, 0x54

    const-string v1, "/omnistore_change_record"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23731
    const/16 v0, 0x55

    const-string v1, "/t_dr_response"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23732
    const/16 v0, 0x56

    const-string v1, "/quick_promotion_refresh"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23733
    const/16 v0, 0x57

    const-string v1, "/v_ios"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23734
    const/16 v0, 0x58

    const-string v1, "/pubsub"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23735
    const/16 v0, 0x59

    const-string v1, "/get_media"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23736
    const/16 v0, 0x5a

    const-string v1, "/get_media_resp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23737
    const/16 v0, 0x5b

    const-string v1, "/mqtt_health_stats"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23738
    const/16 v0, 0x5c

    const-string v1, "/t_sp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23739
    const/16 v0, 0x5d

    const-string v1, "/groups_landing_updates"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23740
    const/16 v0, 0x5e

    const-string v1, "/rs"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23741
    const/16 v0, 0x5f

    const-string v1, "/t_sm_b"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23742
    const/16 v0, 0x60

    const-string v1, "/t_sm_b_rsp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23743
    const/16 v0, 0x61

    const-string v1, "/t_ms_gd"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23744
    const/16 v0, 0x62

    const-string v1, "/t_rtc_multi"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23745
    const/16 v0, 0x63

    const-string v1, "/friend_accepted"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23746
    const/16 v0, 0x64

    const-string v1, "/t_tn"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23747
    const/16 v0, 0x65

    const-string v1, "/t_mf_as"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23748
    const/16 v0, 0x66

    const-string v1, "/t_fs"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23749
    const/16 v0, 0x67

    const-string v1, "/t_tp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23750
    const/16 v0, 0x68

    const-string v1, "/t_stp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23751
    const/16 v0, 0x69

    const-string v1, "/t_st"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23752
    const/16 v0, 0x6a

    const-string v1, "/omni"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23753
    const/16 v0, 0x6b

    const-string v1, "/t_push"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23754
    const/16 v0, 0x6c

    const-string v1, "/omni_c"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23755
    const/16 v0, 0x6d

    const-string v1, "/t_sac"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23756
    const/16 v0, 0x6e

    const-string v1, "/omnistore_resnapshot"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23757
    const/16 v0, 0x6f

    const-string v1, "/t_spc"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23758
    const/16 v0, 0x70

    const-string v1, "/t_callability_req"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23759
    const/16 v0, 0x71

    const-string v1, "/t_callability_resp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23760
    const/16 v0, 0x74

    const-string v1, "/t_ec"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23761
    const/16 v0, 0x75

    const-string v1, "/t_tcp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23762
    const/16 v0, 0x76

    const-string v1, "/t_tcpr"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23763
    const/16 v0, 0x77

    const-string v1, "/t_ts"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23764
    const/16 v0, 0x78

    const-string v1, "/t_ts_rp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23765
    const/16 v0, 0x79

    const-string v1, "/t_mt_req"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23766
    const/16 v0, 0x7a

    const-string v1, "/t_mt_resp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23767
    const/16 v0, 0x7b

    const-string v1, "/t_inbox"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23768
    const/16 v0, 0x7c

    const-string v1, "/p_a_req"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23769
    const/16 v0, 0x7d

    const-string v1, "/p_a_resp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23770
    const/16 v0, 0x7e

    const-string v1, "/unsubscribe"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23771
    const/16 v0, 0x7f

    const-string v1, "/t_graphql_req"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23772
    const/16 v0, 0x80

    const-string v1, "/t_graphql_resp"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23773
    const/16 v0, 0x81

    const-string v1, "/t_app_update"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23774
    const/16 v0, 0x82

    const-string v1, "/p_updated"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23775
    const/16 v0, 0x83

    const-string v1, "/t_omnistore_sync_low_pri"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23776
    const/16 v0, 0x84

    const-string v1, "/ig_send_message"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23777
    const/16 v0, 0x85

    const-string v1, "/ig_send_message_response"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23778
    const/16 v0, 0x86

    const-string v1, "/ig_sub_iris"

    invoke-virtual {p0, v0, v1}, LX/0AK;->put(ILjava/lang/Object;)V

    .line 23779
    return-void
.end method
