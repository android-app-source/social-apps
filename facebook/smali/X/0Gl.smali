.class public final LX/0Gl;
.super Landroid/util/LruCache;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "LX/0Gn;",
        "LX/0G6;",
        ">;"
    }
.end annotation


# static fields
.field public static final synthetic a:Z


# instance fields
.field public final synthetic b:LX/0Go;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36402
    const-class v0, LX/0Go;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/0Gl;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LX/0Go;I)V
    .locals 0

    .prologue
    .line 36403
    iput-object p1, p0, LX/0Gl;->b:LX/0Go;

    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 36404
    check-cast p2, LX/0Gn;

    check-cast p3, LX/0G6;

    .line 36405
    if-eqz p1, :cond_1

    .line 36406
    invoke-static {p3}, LX/0Go;->b(LX/0G6;)V

    .line 36407
    iget-object v0, p0, LX/0Gl;->b:LX/0Go;

    iget-object v0, v0, LX/0Go;->d:Ljava/util/HashMap;

    iget-object v1, p2, LX/0Gn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LruCache;

    .line 36408
    sget-boolean v1, LX/0Gl;->a:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36409
    :cond_0
    invoke-virtual {v0, p2}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36410
    :cond_1
    return-void
.end method
