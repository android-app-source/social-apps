.class public abstract LX/00B;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/DoNotOptimize;
.end annotation


# instance fields
.field public volatile next:LX/00B;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1786
    return-void
.end method

.method public static staplePreviousException(Ljava/lang/Throwable;Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 2

    .prologue
    .line 1787
    const/4 v0, 0x0

    .line 1788
    if-eq p0, p1, :cond_1

    .line 1789
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1790
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/Throwable;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1791
    const/4 v0, 0x1

    .line 1792
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 1793
    invoke-static {p0, p1}, LX/0Bi;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    .line 1794
    :cond_1
    return-object p0

    :catch_0
    goto :goto_0

    :catch_1
    goto :goto_0
.end method


# virtual methods
.method public abstract translate(Ljava/lang/Throwable;Ljava/util/Map;)Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Throwable;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
