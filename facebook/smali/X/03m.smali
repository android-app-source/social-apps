.class public abstract LX/03m;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/Object;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Boolean;

.field private e:Z

.field private volatile f:Ljava/lang/UnsatisfiedLinkError;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10749
    const-class v0, LX/03m;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/03m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 10750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10751
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/03m;->b:Ljava/lang/Object;

    .line 10752
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/03m;->d:Ljava/lang/Boolean;

    .line 10753
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/03m;->e:Z

    .line 10754
    const/4 v0, 0x0

    iput-object v0, p0, LX/03m;->f:Ljava/lang/UnsatisfiedLinkError;

    .line 10755
    iput-object p1, p0, LX/03m;->c:Ljava/util/List;

    .line 10756
    return-void
.end method


# virtual methods
.method public T_()Z
    .locals 4

    .prologue
    .line 10757
    iget-object v1, p0, LX/03m;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 10758
    :try_start_0
    iget-object v0, p0, LX/03m;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 10759
    iget-boolean v0, p0, LX/03m;->e:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10760
    :goto_0
    return v0

    .line 10761
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/03m;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 10762
    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 10763
    :catch_0
    move-exception v0

    .line 10764
    :try_start_2
    sget-object v2, LX/03m;->a:Ljava/lang/String;

    const-string v3, "Failed to load native lib: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 10765
    iput-object v0, p0, LX/03m;->f:Ljava/lang/UnsatisfiedLinkError;

    .line 10766
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/03m;->e:Z

    .line 10767
    :goto_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/03m;->d:Ljava/lang/Boolean;

    .line 10768
    iget-boolean v0, p0, LX/03m;->e:Z

    monitor-exit v1

    goto :goto_0

    .line 10769
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 10770
    :cond_1
    :try_start_3
    invoke-virtual {p0}, LX/03m;->c()V

    .line 10771
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/03m;->e:Z

    .line 10772
    const/4 v0, 0x0

    iput-object v0, p0, LX/03m;->c:Ljava/util/List;
    :try_end_3
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method public b()V
    .locals 1

    .prologue
    .line 10773
    invoke-virtual {p0}, LX/03m;->T_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 10774
    iget-object v0, p0, LX/03m;->f:Ljava/lang/UnsatisfiedLinkError;

    throw v0

    .line 10775
    :cond_0
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 10776
    return-void
.end method
