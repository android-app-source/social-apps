.class public final LX/0Lh;
.super LX/0LR;
.source ""


# instance fields
.field private final k:LX/0L4;

.field private final l:LX/0Lz;

.field private volatile m:I

.field private volatile n:Z


# direct methods
.method public constructor <init>(LX/0G6;LX/0OA;ILX/0AR;JJILX/0L4;LX/0Lz;I)V
    .locals 15

    .prologue
    .line 44087
    const/4 v13, 0x1

    move-object v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move/from16 v12, p9

    move/from16 v14, p12

    invoke-direct/range {v3 .. v14}, LX/0LR;-><init>(LX/0G6;LX/0OA;ILX/0AR;JJIZI)V

    .line 44088
    move-object/from16 v0, p10

    iput-object v0, p0, LX/0Lh;->k:LX/0L4;

    .line 44089
    move-object/from16 v0, p11

    iput-object v0, p0, LX/0Lh;->l:LX/0Lz;

    .line 44090
    return-void
.end method


# virtual methods
.method public final b()LX/0L4;
    .locals 1

    .prologue
    .line 44091
    iget-object v0, p0, LX/0Lh;->k:LX/0L4;

    return-object v0
.end method

.method public final c()LX/0Lz;
    .locals 1

    .prologue
    .line 44092
    iget-object v0, p0, LX/0Lh;->l:LX/0Lz;

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 44093
    iget v0, p0, LX/0Lh;->m:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 44094
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Lh;->n:Z

    .line 44095
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 44096
    iget-boolean v0, p0, LX/0Lh;->n:Z

    return v0
.end method

.method public final h()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 44097
    iget-object v1, p0, LX/0LP;->e:LX/0OA;

    iget v2, p0, LX/0Lh;->m:I

    invoke-static {v1, v2}, LX/08x;->a(LX/0OA;I)LX/0OA;

    move-result-object v1

    .line 44098
    :try_start_0
    iget-object v2, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v2, v1}, LX/0G6;->a(LX/0OA;)J

    .line 44099
    :goto_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 44100
    iget v1, p0, LX/0Lh;->m:I

    add-int/2addr v0, v1

    iput v0, p0, LX/0Lh;->m:I

    .line 44101
    iget-object v0, p0, LX/0LR;->k:LX/0MC;

    move-object v0, v0

    .line 44102
    iget-object v1, p0, LX/0LP;->g:LX/0G6;

    const v2, 0x7fffffff

    const/4 v3, 0x1

    .line 44103
    iget-object v4, v0, LX/0MC;->a:LX/0MP;

    invoke-virtual {v4, v1, v2, v3}, LX/0MP;->a(LX/0G6;IZ)I

    move-result v4

    move v0, v4

    .line 44104
    goto :goto_0

    .line 44105
    :cond_0
    iget v5, p0, LX/0Lh;->m:I

    .line 44106
    iget-object v0, p0, LX/0LR;->k:LX/0MC;

    move-object v1, v0

    .line 44107
    iget-wide v2, p0, LX/0LQ;->h:J

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, LX/0MC;->a(JIII[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44108
    iget-object v0, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v0}, LX/0G6;->a()V

    .line 44109
    return-void

    .line 44110
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v1}, LX/0G6;->a()V

    throw v0
.end method
