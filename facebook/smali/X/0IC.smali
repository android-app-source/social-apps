.class public abstract LX/0IC;
.super LX/0HQ;
.source ""


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "LX/0IB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/0IB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38441
    invoke-direct {p0}, LX/0HQ;-><init>()V

    .line 38442
    if-nez p1, :cond_0

    .line 38443
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "intentService cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38444
    :cond_0
    iput-object p1, p0, LX/0IC;->a:Ljava/lang/Class;

    .line 38445
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v0, 0x26

    const v1, 0xf0b9e7b

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 38446
    const-string v1, "FbnsCallbackReceiver"

    const-string v2, "onReceive %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38447
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 38448
    const/16 v1, 0x27

    const v2, -0x4613232

    invoke-static {p2, v7, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 38449
    :goto_0
    return-void

    .line 38450
    :cond_0
    iget-object v1, p0, LX/0IC;->a:Ljava/lang/Class;

    invoke-virtual {p2, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 38451
    invoke-static {p1, p2}, LX/0HQ;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    .line 38452
    if-nez v1, :cond_1

    .line 38453
    const-string v1, "FbnsCallbackReceiver"

    const-string v2, "service %s does not exist"

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, LX/0IC;->a:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38454
    :cond_1
    const v1, -0x2bf6e684

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0
.end method
