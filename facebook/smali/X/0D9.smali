.class public LX/0D9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:Landroid/view/View;

.field public final c:Z

.field public d:Landroid/content/res/Resources;

.field public e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 29643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29644
    iput-object p1, p0, LX/0D9;->a:Landroid/app/Activity;

    .line 29645
    iput-object p2, p0, LX/0D9;->b:Landroid/view/View;

    .line 29646
    iput-boolean p3, p0, LX/0D9;->c:Z

    .line 29647
    iget-object p1, p0, LX/0D9;->a:Landroid/app/Activity;

    new-instance p2, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$1;

    invoke-direct {p2, p0}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$1;-><init>(LX/0D9;)V

    invoke-virtual {p1, p2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 29648
    return-void
.end method

.method public static a(LX/0D9;Landroid/view/View;Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 1

    .prologue
    .line 29640
    if-eqz p1, :cond_0

    .line 29641
    new-instance v0, LX/0D8;

    invoke-direct {v0, p0, p4, p2, p3}, LX/0D8;-><init>(LX/0D9;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29642
    :cond_0
    return-void
.end method
