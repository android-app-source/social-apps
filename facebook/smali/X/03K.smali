.class public LX/03K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/03G;


# static fields
.field public static final a:LX/03K;


# instance fields
.field private b:Ljava/lang/String;

.field private c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10030
    new-instance v0, LX/03K;

    invoke-direct {v0}, LX/03K;-><init>()V

    sput-object v0, LX/03K;->a:LX/03K;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 9991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9992
    const-string v0, "unknown"

    iput-object v0, p0, LX/03K;->b:Ljava/lang/String;

    .line 9993
    const/4 v0, 0x5

    iput v0, p0, LX/03K;->c:I

    .line 9994
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 10027
    iget-object v0, p0, LX/03K;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 10028
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/03K;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 10029
    :cond_0
    return-object p1
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 10015
    invoke-direct {p0, p2}, LX/03K;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 10016
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 p0, 0xa

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 10017
    if-nez p4, :cond_0

    .line 10018
    const-string p0, ""

    .line 10019
    :goto_0
    move-object p0, p0

    .line 10020
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 10021
    invoke-static {p1, v0, v1}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 10022
    return-void

    .line 10023
    :cond_0
    new-instance p0, Ljava/io/StringWriter;

    invoke-direct {p0}, Ljava/io/StringWriter;-><init>()V

    .line 10024
    new-instance p3, Ljava/io/PrintWriter;

    invoke-direct {p3, p0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 10025
    invoke-virtual {p4, p3}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 10026
    invoke-virtual {p0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private b(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 10013
    invoke-direct {p0, p2}, LX/03K;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, p3}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 10014
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 10011
    iput p1, p0, LX/03K;->c:I

    .line 10012
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 10009
    invoke-direct {p0, p1, p2, p3}, LX/03K;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 10010
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 10007
    const/4 v0, 0x5

    invoke-direct {p0, v0, p1, p2}, LX/03K;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 10008
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 10005
    const/4 v0, 0x5

    invoke-direct {p0, v0, p1, p2, p3}, LX/03K;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 10006
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 10004
    iget v0, p0, LX/03K;->c:I

    return v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 10002
    const/4 v0, 0x6

    invoke-direct {p0, v0, p1, p2}, LX/03K;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 10003
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 10000
    const/4 v0, 0x6

    invoke-direct {p0, v0, p1, p2, p3}, LX/03K;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 10001
    return-void
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 9999
    iget v0, p0, LX/03K;->c:I

    if-gt v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 9997
    const/4 v0, 0x6

    invoke-direct {p0, v0, p1, p2}, LX/03K;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 9998
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 9995
    const/4 v0, 0x6

    invoke-direct {p0, v0, p1, p2, p3}, LX/03K;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9996
    return-void
.end method
