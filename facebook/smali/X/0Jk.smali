.class public final LX/0Jk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0GJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0GJ",
        "<",
        "LX/0Nk;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Jl;

.field private b:LX/0Jp;


# direct methods
.method public constructor <init>(LX/0Jl;LX/0Jp;)V
    .locals 0

    .prologue
    .line 39715
    iput-object p1, p0, LX/0Jk;->a:LX/0Jl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39716
    iput-object p2, p0, LX/0Jk;->b:LX/0Jp;

    .line 39717
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 39718
    check-cast p1, LX/0Nk;

    const/4 v11, 0x0

    const/4 v1, 0x1

    .line 39719
    iget-object v0, p0, LX/0Jk;->a:LX/0Jl;

    iget-boolean v0, v0, LX/0Jl;->k:Z

    if-eqz v0, :cond_0

    .line 39720
    new-instance v0, LX/0OE;

    iget-object v2, p0, LX/0Jk;->a:LX/0Jl;

    iget-object v2, v2, LX/0Ji;->d:Landroid/content/Context;

    const-string v3, "ExoPlayer_HLS"

    invoke-direct {v0, v2, v3}, LX/0OE;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 39721
    new-instance v2, LX/0K3;

    iget-object v3, p0, LX/0Jk;->a:LX/0Jl;

    iget-object v3, v3, LX/0Jl;->j:LX/1m0;

    invoke-direct {v2, v0, v11, v3}, LX/0K3;-><init>(LX/0OE;Ljava/lang/String;LX/1m0;)V

    .line 39722
    :goto_0
    new-instance v7, LX/0Nt;

    invoke-direct {v7}, LX/0Nt;-><init>()V

    .line 39723
    new-instance v0, LX/0Ni;

    iget-object v3, p0, LX/0Jk;->a:LX/0Jl;

    iget-object v3, v3, LX/0Ji;->c:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/0Jk;->a:LX/0Jl;

    iget-object v4, v4, LX/0Ji;->d:Landroid/content/Context;

    invoke-static {v4}, LX/0Nc;->a(Landroid/content/Context;)LX/0Nc;

    move-result-object v5

    iget-object v4, p0, LX/0Jk;->a:LX/0Jl;

    iget-object v6, v4, LX/0Jl;->i:LX/0OC;

    move-object v4, p1

    move v8, v1

    invoke-direct/range {v0 .. v8}, LX/0Ni;-><init>(ZLX/0G6;Ljava/lang/String;LX/0Nk;LX/0Nb;LX/04m;LX/0Nt;I)V

    .line 39724
    new-instance v4, LX/0Ns;

    iget-object v2, p0, LX/0Jk;->a:LX/0Jl;

    iget-object v2, v2, LX/0Jl;->h:LX/0Gb;

    iget-object v3, p0, LX/0Jk;->a:LX/0Jl;

    iget v3, v3, LX/0Jl;->a:I

    iget-object v5, p0, LX/0Jk;->a:LX/0Jl;

    iget v5, v5, LX/0Jl;->b:I

    mul-int/2addr v3, v5

    invoke-direct {v4, v0, v2, v3}, LX/0Ns;-><init>(LX/0Ni;LX/0Gb;I)V

    .line 39725
    new-instance v2, LX/0Jx;

    iget-object v0, p0, LX/0Jk;->a:LX/0Jl;

    iget-object v3, v0, LX/0Ji;->d:Landroid/content/Context;

    const-wide/16 v6, 0x0

    iget-object v0, p0, LX/0Jk;->a:LX/0Jl;

    iget-object v8, v0, LX/0Ji;->e:Landroid/os/Handler;

    iget-object v0, p0, LX/0Jk;->a:LX/0Jl;

    iget-object v9, v0, LX/0Ji;->f:LX/0Jv;

    const/4 v10, -0x1

    move v5, v1

    invoke-direct/range {v2 .. v10}, LX/0Jx;-><init>(Landroid/content/Context;LX/0L9;IJLandroid/os/Handler;LX/0Jv;I)V

    .line 39726
    new-instance v0, LX/0Jt;

    iget-object v1, p0, LX/0Jk;->a:LX/0Jl;

    iget-object v1, v1, LX/0Ji;->e:Landroid/os/Handler;

    iget-object v3, p0, LX/0Jk;->a:LX/0Jl;

    iget-object v3, v3, LX/0Ji;->g:LX/0Js;

    invoke-direct {v0, v4, v1, v3}, LX/0Jt;-><init>(LX/0L9;Landroid/os/Handler;LX/0Js;)V

    .line 39727
    iget-object v1, p0, LX/0Jk;->b:LX/0Jp;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3, v11}, LX/0Jp;->a(LX/0Jw;LX/0GT;ILjava/lang/String;)V

    .line 39728
    return-void

    .line 39729
    :cond_0
    new-instance v2, LX/0OD;

    const-string v0, "ExoPlayer_HLS"

    iget-object v3, p0, LX/0Jk;->a:LX/0Jl;

    iget-object v3, v3, LX/0Jl;->i:LX/0OC;

    invoke-direct {v2, v0, v11, v3}, LX/0OD;-><init>(Ljava/lang/String;LX/0OH;LX/04n;)V

    goto :goto_0
.end method

.method public final b(Ljava/io/IOException;)V
    .locals 1

    .prologue
    .line 39713
    iget-object v0, p0, LX/0Jk;->b:LX/0Jp;

    invoke-interface {v0, p1}, LX/0Jp;->a(Ljava/lang/Exception;)V

    .line 39714
    return-void
.end method
