.class public LX/02b;
.super LX/02c;
.source ""


# direct methods
.method public constructor <init>(I[LX/02Z;)V
    .locals 4

    .prologue
    .line 9004
    array-length v0, p2

    mul-int/lit8 v0, v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    .line 9005
    const/4 v0, 0x0

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 9006
    aget-object v2, p2, v0

    invoke-static {v2}, LX/02b;->makeDexName(LX/02Z;)Ljava/lang/String;

    move-result-object v2

    .line 9007
    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x0

    aput-object v2, v1, v3

    .line 9008
    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2}, LX/02b;->makeOdexName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    .line 9009
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9010
    :cond_0
    move-object v0, v1

    .line 9011
    invoke-direct {p0, p1, v0}, LX/02c;-><init>(I[Ljava/lang/String;)V

    .line 9012
    return-void
.end method

.method public constructor <init>([LX/02Z;)V
    .locals 1

    .prologue
    .line 9002
    const/16 v0, 0x8

    invoke-direct {p0, v0, p1}, LX/02b;-><init>(I[LX/02Z;)V

    .line 9003
    return-void
.end method

.method public static addConfiguredDexOptOptions(LX/080;LX/086;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8976
    iget-byte v0, p0, LX/080;->dalvikVerify:B

    if-eqz v0, :cond_0

    .line 8977
    iget-byte v0, p0, LX/080;->dalvikVerify:B

    packed-switch v0, :pswitch_data_0

    .line 8978
    const-string v0, "ignoring unknown Dalvik verify value %s in config file"

    new-array v1, v4, [Ljava/lang/Object;

    iget-byte v2, p0, LX/080;->dalvikVerify:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8979
    :cond_0
    :goto_0
    iget-byte v0, p0, LX/080;->dalvikOptimize:B

    if-eqz v0, :cond_1

    .line 8980
    iget-byte v0, p0, LX/080;->dalvikOptimize:B

    packed-switch v0, :pswitch_data_1

    .line 8981
    const-string v0, "ignoring unknown Dalvik optimize value %s in config file"

    new-array v1, v4, [Ljava/lang/Object;

    iget-byte v2, p0, LX/080;->dalvikOptimize:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8982
    :cond_1
    :goto_1
    iget-byte v0, p0, LX/080;->dalvikRegisterMaps:B

    if-eqz v0, :cond_2

    .line 8983
    iget-byte v0, p0, LX/080;->dalvikRegisterMaps:B

    packed-switch v0, :pswitch_data_2

    .line 8984
    const-string v0, "ignoring unknown Dalvik register map value %s in config file"

    new-array v1, v4, [Ljava/lang/Object;

    iget-byte v2, p0, LX/080;->dalvikRegisterMaps:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/02P;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8985
    :cond_2
    :goto_2
    return-void

    .line 8986
    :pswitch_0
    const-string v0, "using DALVIK_VERIFY_NONE as requested in config file"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8987
    const-string v0, "-Vn"

    invoke-virtual {p1, v0}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    goto :goto_0

    .line 8988
    :pswitch_1
    const-string v0, "using DALVIK_VERIFY_REMOTE as requested in config file"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8989
    const-string v0, "-Vr"

    invoke-virtual {p1, v0}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    goto :goto_0

    .line 8990
    :pswitch_2
    const-string v0, "using DALVIK_VERIFY_ALL as requested in config file"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8991
    const-string v0, "-Va"

    invoke-virtual {p1, v0}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    goto :goto_0

    .line 8992
    :pswitch_3
    const-string v0, "using DALVIK_OPT_NONE as requested in config file"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8993
    const-string v0, "-On"

    invoke-virtual {p1, v0}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    goto :goto_1

    .line 8994
    :pswitch_4
    const-string v0, "using DALVIK_OPT_VERIFIED as requested in config file"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8995
    const-string v0, "-Ov"

    invoke-virtual {p1, v0}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    goto :goto_1

    .line 8996
    :pswitch_5
    const-string v0, "using DALVIK_OPT_ALL as requested in config file"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8997
    const-string v0, "-Oa"

    invoke-virtual {p1, v0}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    goto :goto_1

    .line 8998
    :pswitch_6
    const-string v0, "using DALVIK_OPT_FULL as requested in config file"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 8999
    const-string v0, "-Of"

    invoke-virtual {p1, v0}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    goto :goto_1

    .line 9000
    :pswitch_7
    const-string v0, "-Rn"

    invoke-virtual {p1, v0}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    goto :goto_2

    .line 9001
    :pswitch_8
    const-string v0, "-Ry"

    invoke-virtual {p1, v0}, LX/086;->addArgument(Ljava/lang/String;)LX/086;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static makeDexName(LX/02Z;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 8943
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "prog-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/02Z;->hash:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".dex.jar"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static makeDummyZip(Ljava/io/File;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 8951
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 8952
    :try_start_0
    new-instance v4, Ljava/util/zip/ZipOutputStream;

    invoke-direct {v4, v3}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 8953
    :try_start_1
    new-instance v0, Ljava/util/zip/ZipEntry;

    const-string v1, "META-INF/MANIFEST.MF"

    invoke-direct {v0, v1}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 8954
    new-instance v5, Ljava/io/PrintStream;

    invoke-direct {v5, v4}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 8955
    :try_start_2
    const-string v0, "Manifest-Version: 1.0"

    invoke-virtual {v5, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 8956
    const-string v0, "Created-By: OdexSchemeTurbo"

    invoke-virtual {v5, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 8957
    invoke-virtual {v5}, Ljava/io/PrintStream;->flush()V

    .line 8958
    invoke-virtual {v5}, Ljava/io/PrintStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    .line 8959
    :try_start_3
    invoke-virtual {v5}, Ljava/io/PrintStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 8960
    :try_start_4
    invoke-virtual {v4}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 8961
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    return-void

    .line 8962
    :catch_0
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 8963
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_0
    if-eqz v1, :cond_0

    :try_start_6
    invoke-virtual {v5}, Ljava/io/PrintStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :goto_1
    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 8964
    :catch_1
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 8965
    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_2
    if-eqz v1, :cond_1

    :try_start_9
    invoke-virtual {v4}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    :goto_3
    :try_start_a
    throw v0
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 8966
    :catch_2
    move-exception v0

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 8967
    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_4
    if-eqz v2, :cond_2

    :try_start_c
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_5

    :goto_5
    throw v0

    .line 8968
    :catch_3
    move-exception v5

    :try_start_d
    invoke-static {v1, v5}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 8969
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 8970
    :cond_0
    invoke-virtual {v5}, Ljava/io/PrintStream;->close()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto :goto_1

    .line 8971
    :catch_4
    move-exception v4

    :try_start_e
    invoke-static {v1, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 8972
    :catchall_4
    move-exception v0

    goto :goto_4

    .line 8973
    :cond_1
    invoke-virtual {v4}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    goto :goto_3

    .line 8974
    :catch_5
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    goto :goto_5

    .line 8975
    :catchall_5
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static makeOdexName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 8950
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, LX/02Q;->stripLastExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".odex"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configureClassLoader(Ljava/io/File;LX/02f;)V
    .locals 5

    .prologue
    .line 8946
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 8947
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    add-int/lit8 v3, v0, 0x0

    aget-object v2, v2, v3

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    add-int/lit8 v4, v0, 0x1

    aget-object v3, v3, v4

    invoke-direct {v2, p1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p2, v1, v2}, LX/02f;->addDex(Ljava/io/File;Ljava/io/File;)V

    .line 8948
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 8949
    :cond_0
    return-void
.end method

.method public getSchemeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8945
    const-string v0, "OdexSchemeTurbo"

    return-object v0
.end method

.method public final makeCompiler(LX/02U;I)LX/084;
    .locals 1

    .prologue
    .line 8944
    new-instance v0, LX/083;

    invoke-direct {v0, p1, p2}, LX/083;-><init>(LX/02U;I)V

    return-object v0
.end method
