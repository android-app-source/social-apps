.class public LX/0Fb;
.super LX/02c;
.source ""


# static fields
.field public static sAttemptedArtHackInstallation:Z


# direct methods
.method public constructor <init>(I[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 33633
    invoke-direct {p0, p1, p2}, LX/02c;-><init>(I[Ljava/lang/String;)V

    .line 33634
    return-void
.end method

.method public constructor <init>([LX/02Z;)V
    .locals 2

    .prologue
    .line 33635
    const/16 v0, 0x8

    invoke-static {p1}, LX/0Fb;->makeExpectedFileList([LX/02Z;)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/0Fb;-><init>(I[Ljava/lang/String;)V

    .line 33636
    return-void
.end method

.method public static makeDexName(LX/02Z;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 33637
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "prog-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/02Z;->hash:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".dex"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static makeExpectedFileList([LX/02Z;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 33638
    array-length v0, p0

    new-array v1, v0, [Ljava/lang/String;

    .line 33639
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 33640
    aget-object v2, p0, v0

    invoke-static {v2}, LX/0Fb;->makeDexName(LX/02Z;)Ljava/lang/String;

    move-result-object v2

    .line 33641
    aput-object v2, v1, v0

    .line 33642
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33643
    :cond_0
    return-object v1
.end method


# virtual methods
.method public configureClassLoader(Ljava/io/File;LX/02f;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 33644
    sget-boolean v0, LX/0Fb;->sAttemptedArtHackInstallation:Z

    if-nez v0, :cond_0

    .line 33645
    const/4 v0, 0x1

    sput-boolean v0, LX/0Fb;->sAttemptedArtHackInstallation:Z

    .line 33646
    const/4 v0, 0x6

    :try_start_0
    invoke-static {v0}, Lcom/facebook/common/dextricks/DalvikInternals;->installArtHacks(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 33647
    :cond_0
    :goto_0
    invoke-static {}, Lcom/facebook/common/dextricks/DalvikInternals;->getEnabledThreadArtHacks()I

    move-result v2

    .line 33648
    or-int/lit8 v0, v2, 0x6

    invoke-static {v0}, Lcom/facebook/common/dextricks/DalvikInternals;->setEnabledThreadArtHacks(I)I

    .line 33649
    :try_start_1
    const-string v0, "enabled ART verifier hack (warning-level logs following are expected)"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v3}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v0, v1

    .line 33650
    :goto_1
    iget-object v3, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 33651
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-direct {v3, p1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p2, v3}, LX/02f;->addDex(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 33652
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 33653
    :catch_0
    move-exception v0

    .line 33654
    const-string v2, "failed to install verifier-disabling ART hacks; continuing slowly"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 33655
    :cond_1
    invoke-static {v2}, Lcom/facebook/common/dextricks/DalvikInternals;->setEnabledThreadArtHacks(I)I

    .line 33656
    const-string v0, "restored old ART hack mask"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33657
    return-void

    .line 33658
    :catchall_0
    move-exception v0

    invoke-static {v2}, Lcom/facebook/common/dextricks/DalvikInternals;->setEnabledThreadArtHacks(I)I

    .line 33659
    const-string v2, "restored old ART hack mask"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    throw v0
.end method

.method public getSchemeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33660
    const-string v0, "OdexSchemeArtTurbo"

    return-object v0
.end method

.method public final makeCompiler(LX/02U;I)LX/084;
    .locals 1

    .prologue
    .line 33661
    new-instance v0, LX/0Fa;

    invoke-direct {v0, p1, p2}, LX/0Fa;-><init>(LX/02U;I)V

    return-object v0
.end method
