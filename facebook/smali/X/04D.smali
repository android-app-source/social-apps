.class public final enum LX/04D;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/04D;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/04D;

.field public static final enum ACTOR_PROFILE_VIDEO:LX/04D;

.field public static final enum BACKGROUND_PLAY:LX/04D;

.field public static final enum BACKSTAGE_VIDEOS:LX/04D;

.field public static final enum CAROUSEL_VIDEO:LX/04D;

.field public static final enum CHANNEL_VIEW:LX/04D;

.field public static final enum COLLAGE_MIXED_MEDIA:LX/04D;

.field public static final enum COMMENT:LX/04D;

.field public static final enum COMPOSER:LX/04D;

.field public static final enum CULTURAL_MOMENTS_SHARE:LX/04D;

.field public static final enum EVENT:LX/04D;

.field public static final enum EVENT_CHEVRON:LX/04D;

.field public static final enum FACECAST_NUX:LX/04D;

.field public static final enum FEED:LX/04D;

.field public static final enum FEED_CHEVRON:LX/04D;

.field public static final enum FULLSCREEN_VIDEO_FROM_NOTIFICATIONS:LX/04D;

.field public static final enum GROUP:LX/04D;

.field public static final enum GROUP_CHEVRON:LX/04D;

.field public static final enum INSPIRATION_CAMERA:LX/04D;

.field public static final enum INSTANT_ARTICLES:LX/04D;

.field public static final enum INSTANT_SHOPPING:LX/04D;

.field public static final enum LIVE_MAP:LX/04D;

.field public static final enum LIVE_VIDEO_END_SCREEN:LX/04D;

.field public static final enum MEDIA_GALLERY:LX/04D;

.field public static final enum MEDIA_PICKER:LX/04D;

.field public static final enum MESSENGER_CONTENT_SEARCH:LX/04D;

.field public static final enum MESSENGER_MEDIA_TRAY_POPUP:LX/04D;

.field public static final enum MESSENGER_MONTAGE_VIEWER_INLINE:LX/04D;

.field public static final enum MESSENGER_MONTAGE_VIEWER_RVP:LX/04D;

.field public static final enum MESSENGER_MULTIMEDIA_EDITOR:LX/04D;

.field public static final enum MESSENGER_QUICK_CAM_VIEW:LX/04D;

.field public static final enum MESSENGER_THREAD:LX/04D;

.field public static final enum MESSENGER_VIDEO_MEDIA_SHARE_VIEW:LX/04D;

.field public static final enum MESSENGER_VIDEO_MESSAGE_VIEW:LX/04D;

.field public static final enum MESSENGER_VIDEO_THREAD_INLINE:LX/04D;

.field public static final enum MESSENGER_VIDEO_THREAD_RVP:LX/04D;

.field public static final enum MOMENTS_FULLSCREEN_VIDEO:LX/04D;

.field public static final enum MOMENTS_THUMBNAIL_VIDEO:LX/04D;

.field public static final enum OPTIMISTIC_POSTING_INLINE:LX/04D;

.field public static final enum PAGE_POLITICAL_ISSUES_OPINION_VIDEO:LX/04D;

.field public static final enum PAGE_TIMELINE:LX/04D;

.field public static final enum PAGE_TIMELINE_CHEVRON:LX/04D;

.field public static final enum PAGE_VIDEO_CARD:LX/04D;

.field public static final enum PAGE_VIDEO_HUB:LX/04D;

.field public static final enum PAGE_VIDEO_LIST_PERMALINK:LX/04D;

.field public static final enum PAGE_VIDEO_LIST_STORY:LX/04D;

.field public static final enum PERMALINK:LX/04D;

.field public static final enum PERMALINK_FROM_NOTIFICATIONS:LX/04D;

.field public static final enum PERMALINK_FROM_PUSH:LX/04D;

.field public static final enum PROFILE_FAVORITE_MEDIA_PICKER:LX/04D;

.field public static final enum PROFILE_VIDEO:LX/04D;

.field public static final enum PROFILE_VIDEO_CARD:LX/04D;

.field public static final enum PROFILE_VIDEO_HUB:LX/04D;

.field public static final enum PROFILE_VIDEO_PREVIEW:LX/04D;

.field public static final enum QUICK_PROMOTION_FEED:LX/04D;

.field public static final enum REACTION_OVERLAY:LX/04D;

.field public static final enum RESULTS_PAGE_MIXED_MEDIA:LX/04D;

.field public static final enum SAVED_DASHBOARD:LX/04D;

.field public static final enum SAVED_REMINDER:LX/04D;

.field public static final enum SEARCH_RESULTS:LX/04D;

.field public static final enum SIMPLE_PICKER:LX/04D;

.field public static final enum SINGLE_CREATOR_SET_FOOTER:LX/04D;

.field public static final enum SINGLE_CREATOR_SET_INLINE:LX/04D;

.field public static final enum SINGLE_CREATOR_SET_VIDEO:LX/04D;

.field public static final enum SOUVENIR:LX/04D;

.field public static final enum TAROT_COVER_CARD:LX/04D;

.field public static final enum TAROT_END_CARD:LX/04D;

.field public static final enum TAROT_FEED:LX/04D;

.field public static final enum TAROT_MANAGE_SCREEN:LX/04D;

.field public static final enum TAROT_VIDEO_CARD:LX/04D;

.field public static final enum UNKNOWN:LX/04D;

.field public static final enum USER_TIMELINE:LX/04D;

.field public static final enum USER_TIMELINE_CHEVRON:LX/04D;

.field public static final enum VERVE:LX/04D;

.field public static final enum VIDEO_ALBUM_PERMALINK:LX/04D;

.field public static final enum VIDEO_CHAINING_INLINE:LX/04D;

.field public static final enum VIDEO_EDITING_GALLERY:LX/04D;

.field public static final enum VIDEO_HOME:LX/04D;

.field public static final enum VIDEO_HOME_GUIDE:LX/04D;

.field public static final enum VIDEO_SETS:LX/04D;


# instance fields
.field public final origin:Ljava/lang/String;

.field public final subOrigin:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 12734
    new-instance v0, LX/04D;

    const-string v1, "FEED"

    const-string v2, "newsfeed"

    const-string v3, "unknown"

    invoke-direct {v0, v1, v5, v2, v3}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->FEED:LX/04D;

    .line 12735
    new-instance v0, LX/04D;

    const-string v1, "VIDEO_SETS"

    const-string v2, "newsfeed"

    const-string v3, "video_sets"

    invoke-direct {v0, v1, v6, v2, v3}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->VIDEO_SETS:LX/04D;

    .line 12736
    new-instance v0, LX/04D;

    const-string v1, "ACTOR_PROFILE_VIDEO"

    const-string v2, "newsfeed"

    const-string v3, "actor_profile_video"

    invoke-direct {v0, v1, v7, v2, v3}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->ACTOR_PROFILE_VIDEO:LX/04D;

    .line 12737
    new-instance v0, LX/04D;

    const-string v1, "USER_TIMELINE"

    const-string v2, "user_timeline"

    const-string v3, "unknown"

    invoke-direct {v0, v1, v8, v2, v3}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->USER_TIMELINE:LX/04D;

    .line 12738
    new-instance v0, LX/04D;

    const-string v1, "PROFILE_VIDEO_HUB"

    const-string v2, "user_timeline"

    const-string v3, "timeline_video_hub"

    invoke-direct {v0, v1, v9, v2, v3}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PROFILE_VIDEO_HUB:LX/04D;

    .line 12739
    new-instance v0, LX/04D;

    const-string v1, "PROFILE_VIDEO_CARD"

    const/4 v2, 0x5

    const-string v3, "user_timeline"

    const-string v4, "timeline_video_card"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PROFILE_VIDEO_CARD:LX/04D;

    .line 12740
    new-instance v0, LX/04D;

    const-string v1, "PROFILE_VIDEO"

    const/4 v2, 0x6

    const-string v3, "user_timeline"

    const-string v4, "profile_video"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PROFILE_VIDEO:LX/04D;

    .line 12741
    new-instance v0, LX/04D;

    const-string v1, "PROFILE_VIDEO_PREVIEW"

    const/4 v2, 0x7

    const-string v3, "user_timeline"

    const-string v4, "profile_video_preview"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PROFILE_VIDEO_PREVIEW:LX/04D;

    .line 12742
    new-instance v0, LX/04D;

    const-string v1, "PROFILE_FAVORITE_MEDIA_PICKER"

    const/16 v2, 0x8

    const-string v3, "user_timeline"

    const-string v4, "profile_favorite_media_picker"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PROFILE_FAVORITE_MEDIA_PICKER:LX/04D;

    .line 12743
    new-instance v0, LX/04D;

    const-string v1, "PAGE_TIMELINE"

    const/16 v2, 0x9

    const-string v3, "page_timeline"

    const-string v4, "unknown"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PAGE_TIMELINE:LX/04D;

    .line 12744
    new-instance v0, LX/04D;

    const-string v1, "PAGE_POLITICAL_ISSUES_OPINION_VIDEO"

    const/16 v2, 0xa

    const-string v3, "page_timeline"

    const-string v4, "page_political_issues_tab"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PAGE_POLITICAL_ISSUES_OPINION_VIDEO:LX/04D;

    .line 12745
    new-instance v0, LX/04D;

    const-string v1, "PAGE_VIDEO_CARD"

    const/16 v2, 0xb

    const-string v3, "page_timeline"

    const-string v4, "page_video_card"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PAGE_VIDEO_CARD:LX/04D;

    .line 12746
    new-instance v0, LX/04D;

    const-string v1, "PAGE_VIDEO_HUB"

    const/16 v2, 0xc

    const-string v3, "page_timeline"

    const-string v4, "page_video_hub"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PAGE_VIDEO_HUB:LX/04D;

    .line 12747
    new-instance v0, LX/04D;

    const-string v1, "PAGE_VIDEO_LIST_PERMALINK"

    const/16 v2, 0xd

    const-string v3, "page_timeline"

    const-string v4, "page_video_list_permalink"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PAGE_VIDEO_LIST_PERMALINK:LX/04D;

    .line 12748
    new-instance v0, LX/04D;

    const-string v1, "PAGE_VIDEO_LIST_STORY"

    const/16 v2, 0xe

    const-string v3, "newsfeed"

    const-string v4, "page_video_list_story"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PAGE_VIDEO_LIST_STORY:LX/04D;

    .line 12749
    new-instance v0, LX/04D;

    const-string v1, "EVENT"

    const/16 v2, 0xf

    const-string v3, "other"

    const-string v4, "event"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->EVENT:LX/04D;

    .line 12750
    new-instance v0, LX/04D;

    const-string v1, "GROUP"

    const/16 v2, 0x10

    const-string v3, "group"

    const-string v4, "unknown"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->GROUP:LX/04D;

    .line 12751
    new-instance v0, LX/04D;

    const-string v1, "PERMALINK"

    const/16 v2, 0x11

    const-string v3, "permalink"

    const-string v4, "unknown"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PERMALINK:LX/04D;

    .line 12752
    new-instance v0, LX/04D;

    const-string v1, "PERMALINK_FROM_PUSH"

    const/16 v2, 0x12

    const-string v3, "permalink"

    const-string v4, "push"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PERMALINK_FROM_PUSH:LX/04D;

    .line 12753
    new-instance v0, LX/04D;

    const-string v1, "PERMALINK_FROM_NOTIFICATIONS"

    const/16 v2, 0x13

    const-string v3, "permalink"

    const-string v4, "notifications"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PERMALINK_FROM_NOTIFICATIONS:LX/04D;

    .line 12754
    new-instance v0, LX/04D;

    const-string v1, "MESSENGER_THREAD"

    const/16 v2, 0x14

    const-string v3, "fbmessenger"

    const-string v4, "messenger_thread"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MESSENGER_THREAD:LX/04D;

    .line 12755
    new-instance v0, LX/04D;

    const-string v1, "CHANNEL_VIEW"

    const/16 v2, 0x15

    const-string v3, "newsfeed"

    const-string v4, "channel_view"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->CHANNEL_VIEW:LX/04D;

    .line 12756
    new-instance v0, LX/04D;

    const-string v1, "VIDEO_CHAINING_INLINE"

    const/16 v2, 0x16

    const-string v3, "newsfeed"

    const-string v4, "video_chaining_inline"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->VIDEO_CHAINING_INLINE:LX/04D;

    .line 12757
    new-instance v0, LX/04D;

    const-string v1, "MEDIA_PICKER"

    const/16 v2, 0x17

    const-string v3, "other"

    const-string v4, "media_picker"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MEDIA_PICKER:LX/04D;

    .line 12758
    new-instance v0, LX/04D;

    const-string v1, "REACTION_OVERLAY"

    const/16 v2, 0x18

    const-string v3, "other"

    const-string v4, "reaction_overlay"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->REACTION_OVERLAY:LX/04D;

    .line 12759
    new-instance v0, LX/04D;

    const-string v1, "VIDEO_ALBUM_PERMALINK"

    const/16 v2, 0x19

    const-string v3, "other"

    const-string v4, "video_album_permalink"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->VIDEO_ALBUM_PERMALINK:LX/04D;

    .line 12760
    new-instance v0, LX/04D;

    const-string v1, "COLLAGE_MIXED_MEDIA"

    const/16 v2, 0x1a

    const-string v3, "newsfeed"

    const-string v4, "collage_mixed_media"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->COLLAGE_MIXED_MEDIA:LX/04D;

    .line 12761
    new-instance v0, LX/04D;

    const-string v1, "RESULTS_PAGE_MIXED_MEDIA"

    const/16 v2, 0x1b

    const-string v3, "search"

    const-string v4, "results_page_mixed_media"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->RESULTS_PAGE_MIXED_MEDIA:LX/04D;

    .line 12762
    new-instance v0, LX/04D;

    const-string v1, "VERVE"

    const/16 v2, 0x1c

    const-string v3, "other"

    const-string v4, "verve"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->VERVE:LX/04D;

    .line 12763
    new-instance v0, LX/04D;

    const-string v1, "SAVED_DASHBOARD"

    const/16 v2, 0x1d

    const-string v3, "saved"

    const-string v4, "saved_dashboard"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->SAVED_DASHBOARD:LX/04D;

    .line 12764
    new-instance v0, LX/04D;

    const-string v1, "SAVED_REMINDER"

    const/16 v2, 0x1e

    const-string v3, "saved"

    const-string v4, "saved_reminder"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->SAVED_REMINDER:LX/04D;

    .line 12765
    new-instance v0, LX/04D;

    const-string v1, "SEARCH_RESULTS"

    const/16 v2, 0x1f

    const-string v3, "search"

    const-string v4, "results"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->SEARCH_RESULTS:LX/04D;

    .line 12766
    new-instance v0, LX/04D;

    const-string v1, "SOUVENIR"

    const/16 v2, 0x20

    const-string v3, "other"

    const-string v4, "souvenir"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->SOUVENIR:LX/04D;

    .line 12767
    new-instance v0, LX/04D;

    const-string v1, "INSTANT_ARTICLES"

    const/16 v2, 0x21

    const-string v3, "instant_articles"

    const-string v4, "instant_articles"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->INSTANT_ARTICLES:LX/04D;

    .line 12768
    new-instance v0, LX/04D;

    const-string v1, "INSTANT_SHOPPING"

    const/16 v2, 0x22

    const-string v3, "instant_shopping"

    const-string v4, "instant_shopping"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->INSTANT_SHOPPING:LX/04D;

    .line 12769
    new-instance v0, LX/04D;

    const-string v1, "BACKSTAGE_VIDEOS"

    const/16 v2, 0x23

    const-string v3, "backstage"

    const-string v4, "backsrage_production_video"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->BACKSTAGE_VIDEOS:LX/04D;

    .line 12770
    new-instance v0, LX/04D;

    const-string v1, "MOMENTS_THUMBNAIL_VIDEO"

    const/16 v2, 0x24

    const-string v3, "moments"

    const-string v4, "thumbnail"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MOMENTS_THUMBNAIL_VIDEO:LX/04D;

    .line 12771
    new-instance v0, LX/04D;

    const-string v1, "MOMENTS_FULLSCREEN_VIDEO"

    const/16 v2, 0x25

    const-string v3, "moments"

    const-string v4, "fullscreen"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MOMENTS_FULLSCREEN_VIDEO:LX/04D;

    .line 12772
    new-instance v0, LX/04D;

    const-string v1, "FULLSCREEN_VIDEO_FROM_NOTIFICATIONS"

    const/16 v2, 0x26

    const-string v3, "notifications"

    const-string v4, "notifications"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->FULLSCREEN_VIDEO_FROM_NOTIFICATIONS:LX/04D;

    .line 12773
    new-instance v0, LX/04D;

    const-string v1, "MESSENGER_MONTAGE_VIEWER_INLINE"

    const/16 v2, 0x27

    const-string v3, "montage"

    const-string v4, "viewer"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MESSENGER_MONTAGE_VIEWER_INLINE:LX/04D;

    .line 12774
    new-instance v0, LX/04D;

    const-string v1, "MESSENGER_MONTAGE_VIEWER_RVP"

    const/16 v2, 0x28

    const-string v3, "montage"

    const-string v4, "viewer_rvp"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MESSENGER_MONTAGE_VIEWER_RVP:LX/04D;

    .line 12775
    new-instance v0, LX/04D;

    const-string v1, "VIDEO_EDITING_GALLERY"

    const/16 v2, 0x29

    const-string v3, "video_editing_gallery"

    const-string v4, "video_editing_gallery_preview"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->VIDEO_EDITING_GALLERY:LX/04D;

    .line 12776
    new-instance v0, LX/04D;

    const-string v1, "LIVE_VIDEO_END_SCREEN"

    const/16 v2, 0x2a

    const-string v3, "live_video_end_screen"

    const-string v4, "live_video_end_screen"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->LIVE_VIDEO_END_SCREEN:LX/04D;

    .line 12777
    new-instance v0, LX/04D;

    const-string v1, "OPTIMISTIC_POSTING_INLINE"

    const/16 v2, 0x2b

    const-string v3, "optimistic_posting_inline"

    const-string v4, "optimistic_posting_inline"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->OPTIMISTIC_POSTING_INLINE:LX/04D;

    .line 12778
    new-instance v0, LX/04D;

    const-string v1, "VIDEO_HOME"

    const/16 v2, 0x2c

    const-string v3, "video_home"

    const-string v4, "video_home_main"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->VIDEO_HOME:LX/04D;

    .line 12779
    new-instance v0, LX/04D;

    const-string v1, "VIDEO_HOME_GUIDE"

    const/16 v2, 0x2d

    const-string v3, "video_home"

    const-string v4, "video_home_guide"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->VIDEO_HOME_GUIDE:LX/04D;

    .line 12780
    new-instance v0, LX/04D;

    const-string v1, "SIMPLE_PICKER"

    const/16 v2, 0x2e

    const-string v3, "simple_picker"

    const-string v4, "simple_picker"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->SIMPLE_PICKER:LX/04D;

    .line 12781
    new-instance v0, LX/04D;

    const-string v1, "QUICK_PROMOTION_FEED"

    const/16 v2, 0x2f

    const-string v3, "newsfeed"

    const-string v4, "quick_promotion"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->QUICK_PROMOTION_FEED:LX/04D;

    .line 12782
    new-instance v0, LX/04D;

    const-string v1, "CULTURAL_MOMENTS_SHARE"

    const/16 v2, 0x30

    const-string v3, "newsfeed"

    const-string v4, "cultural_moments_share"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->CULTURAL_MOMENTS_SHARE:LX/04D;

    .line 12783
    new-instance v0, LX/04D;

    const-string v1, "COMPOSER"

    const/16 v2, 0x31

    const-string v3, "composer"

    const-string v4, "composer"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->COMPOSER:LX/04D;

    .line 12784
    new-instance v0, LX/04D;

    const-string v1, "MEDIA_GALLERY"

    const/16 v2, 0x32

    const-string v3, "media_gallery"

    const-string v4, "media_gallery"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MEDIA_GALLERY:LX/04D;

    .line 12785
    new-instance v0, LX/04D;

    const-string v1, "COMMENT"

    const/16 v2, 0x33

    const-string v3, "comment"

    const-string v4, "comment"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->COMMENT:LX/04D;

    .line 12786
    new-instance v0, LX/04D;

    const-string v1, "FEED_CHEVRON"

    const/16 v2, 0x34

    const-string v3, "newsfeed"

    const-string v4, "chevron"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->FEED_CHEVRON:LX/04D;

    .line 12787
    new-instance v0, LX/04D;

    const-string v1, "USER_TIMELINE_CHEVRON"

    const/16 v2, 0x35

    const-string v3, "user_timeline"

    const-string v4, "chevron"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->USER_TIMELINE_CHEVRON:LX/04D;

    .line 12788
    new-instance v0, LX/04D;

    const-string v1, "PAGE_TIMELINE_CHEVRON"

    const/16 v2, 0x36

    const-string v3, "page_timeline"

    const-string v4, "chevron"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->PAGE_TIMELINE_CHEVRON:LX/04D;

    .line 12789
    new-instance v0, LX/04D;

    const-string v1, "EVENT_CHEVRON"

    const/16 v2, 0x37

    const-string v3, "other"

    const-string v4, "event_chevron"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->EVENT_CHEVRON:LX/04D;

    .line 12790
    new-instance v0, LX/04D;

    const-string v1, "GROUP_CHEVRON"

    const/16 v2, 0x38

    const-string v3, "group"

    const-string v4, "chevron"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->GROUP_CHEVRON:LX/04D;

    .line 12791
    new-instance v0, LX/04D;

    const-string v1, "LIVE_MAP"

    const/16 v2, 0x39

    const-string v3, "other"

    const-string v4, "live_map"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->LIVE_MAP:LX/04D;

    .line 12792
    new-instance v0, LX/04D;

    const-string v1, "FACECAST_NUX"

    const/16 v2, 0x3a

    const-string v3, "facecast_nux"

    const-string v4, "facecast_nux"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->FACECAST_NUX:LX/04D;

    .line 12793
    new-instance v0, LX/04D;

    const-string v1, "BACKGROUND_PLAY"

    const/16 v2, 0x3b

    const-string v3, "background_play"

    const-string v4, "background_play"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->BACKGROUND_PLAY:LX/04D;

    .line 12794
    new-instance v0, LX/04D;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x3c

    const-string v3, "unknown"

    const-string v4, "unknown"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->UNKNOWN:LX/04D;

    .line 12795
    new-instance v0, LX/04D;

    const-string v1, "MESSENGER_VIDEO_THREAD_RVP"

    const/16 v2, 0x3d

    const-string v3, "messenger_thread"

    const-string v4, "messenger_thread_rvp"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MESSENGER_VIDEO_THREAD_RVP:LX/04D;

    .line 12796
    new-instance v0, LX/04D;

    const-string v1, "MESSENGER_VIDEO_THREAD_INLINE"

    const/16 v2, 0x3e

    const-string v3, "messenger_thread"

    const-string v4, "messenger_thread_inline"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MESSENGER_VIDEO_THREAD_INLINE:LX/04D;

    .line 12797
    new-instance v0, LX/04D;

    const-string v1, "CAROUSEL_VIDEO"

    const/16 v2, 0x3f

    const-string v3, "newsfeed"

    const-string v4, "carousel_video"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->CAROUSEL_VIDEO:LX/04D;

    .line 12798
    new-instance v0, LX/04D;

    const-string v1, "SINGLE_CREATOR_SET_VIDEO"

    const/16 v2, 0x40

    const-string v3, "newsfeed"

    const-string v4, "single_creator_set_video"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->SINGLE_CREATOR_SET_VIDEO:LX/04D;

    .line 12799
    new-instance v0, LX/04D;

    const-string v1, "SINGLE_CREATOR_SET_FOOTER"

    const/16 v2, 0x41

    const-string v3, "newsfeed"

    const-string v4, "single_creator_set_footer"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->SINGLE_CREATOR_SET_FOOTER:LX/04D;

    .line 12800
    new-instance v0, LX/04D;

    const-string v1, "SINGLE_CREATOR_SET_INLINE"

    const/16 v2, 0x42

    const-string v3, "newsfeed"

    const-string v4, "single_creator_set_inline"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->SINGLE_CREATOR_SET_INLINE:LX/04D;

    .line 12801
    new-instance v0, LX/04D;

    const-string v1, "TAROT_COVER_CARD"

    const/16 v2, 0x43

    const-string v3, "tarot"

    const-string v4, "tarot_cover_media_card"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->TAROT_COVER_CARD:LX/04D;

    .line 12802
    new-instance v0, LX/04D;

    const-string v1, "TAROT_END_CARD"

    const/16 v2, 0x44

    const-string v3, "tarot"

    const-string v4, "tarot_end_card"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->TAROT_END_CARD:LX/04D;

    .line 12803
    new-instance v0, LX/04D;

    const-string v1, "TAROT_FEED"

    const/16 v2, 0x45

    const-string v3, "newsfeed"

    const-string v4, "tarot"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->TAROT_FEED:LX/04D;

    .line 12804
    new-instance v0, LX/04D;

    const-string v1, "TAROT_MANAGE_SCREEN"

    const/16 v2, 0x46

    const-string v3, "tarot"

    const-string v4, "tarot_manage_screen"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->TAROT_MANAGE_SCREEN:LX/04D;

    .line 12805
    new-instance v0, LX/04D;

    const-string v1, "TAROT_VIDEO_CARD"

    const/16 v2, 0x47

    const-string v3, "tarot"

    const-string v4, "tarot_video_card"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->TAROT_VIDEO_CARD:LX/04D;

    .line 12806
    new-instance v0, LX/04D;

    const-string v1, "MESSENGER_CONTENT_SEARCH"

    const/16 v2, 0x48

    const-string v3, "content_search"

    const-string v4, "messenger_content_search"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MESSENGER_CONTENT_SEARCH:LX/04D;

    .line 12807
    new-instance v0, LX/04D;

    const-string v1, "MESSENGER_MULTIMEDIA_EDITOR"

    const/16 v2, 0x49

    const-string v3, "meltimedia_editor_preview"

    const-string v4, "meltimedia_editor_preview"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MESSENGER_MULTIMEDIA_EDITOR:LX/04D;

    .line 12808
    new-instance v0, LX/04D;

    const-string v1, "MESSENGER_MEDIA_TRAY_POPUP"

    const/16 v2, 0x4a

    const-string v3, "media_tray_popup_view"

    const-string v4, "media_tray_popup_view"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MESSENGER_MEDIA_TRAY_POPUP:LX/04D;

    .line 12809
    new-instance v0, LX/04D;

    const-string v1, "MESSENGER_QUICK_CAM_VIEW"

    const/16 v2, 0x4b

    const-string v3, "quick_cam_view"

    const-string v4, "quick_cam_view"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MESSENGER_QUICK_CAM_VIEW:LX/04D;

    .line 12810
    new-instance v0, LX/04D;

    const-string v1, "MESSENGER_VIDEO_MESSAGE_VIEW"

    const/16 v2, 0x4c

    const-string v3, "video_message_view"

    const-string v4, "video_message_view"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MESSENGER_VIDEO_MESSAGE_VIEW:LX/04D;

    .line 12811
    new-instance v0, LX/04D;

    const-string v1, "MESSENGER_VIDEO_MEDIA_SHARE_VIEW"

    const/16 v2, 0x4d

    const-string v3, "media_share_view"

    const-string v4, "media_share_view"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->MESSENGER_VIDEO_MEDIA_SHARE_VIEW:LX/04D;

    .line 12812
    new-instance v0, LX/04D;

    const-string v1, "INSPIRATION_CAMERA"

    const/16 v2, 0x4e

    const-string v3, "other"

    const-string v4, "inspiration_camera"

    invoke-direct {v0, v1, v2, v3, v4}, LX/04D;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/04D;->INSPIRATION_CAMERA:LX/04D;

    .line 12813
    const/16 v0, 0x4f

    new-array v0, v0, [LX/04D;

    sget-object v1, LX/04D;->FEED:LX/04D;

    aput-object v1, v0, v5

    sget-object v1, LX/04D;->VIDEO_SETS:LX/04D;

    aput-object v1, v0, v6

    sget-object v1, LX/04D;->ACTOR_PROFILE_VIDEO:LX/04D;

    aput-object v1, v0, v7

    sget-object v1, LX/04D;->USER_TIMELINE:LX/04D;

    aput-object v1, v0, v8

    sget-object v1, LX/04D;->PROFILE_VIDEO_HUB:LX/04D;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/04D;->PROFILE_VIDEO_CARD:LX/04D;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/04D;->PROFILE_VIDEO:LX/04D;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/04D;->PROFILE_VIDEO_PREVIEW:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/04D;->PROFILE_FAVORITE_MEDIA_PICKER:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/04D;->PAGE_TIMELINE:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/04D;->PAGE_POLITICAL_ISSUES_OPINION_VIDEO:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/04D;->PAGE_VIDEO_CARD:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/04D;->PAGE_VIDEO_HUB:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/04D;->PAGE_VIDEO_LIST_PERMALINK:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/04D;->PAGE_VIDEO_LIST_STORY:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/04D;->EVENT:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/04D;->GROUP:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/04D;->PERMALINK:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/04D;->PERMALINK_FROM_PUSH:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/04D;->PERMALINK_FROM_NOTIFICATIONS:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/04D;->MESSENGER_THREAD:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/04D;->CHANNEL_VIEW:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/04D;->VIDEO_CHAINING_INLINE:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/04D;->MEDIA_PICKER:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/04D;->REACTION_OVERLAY:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/04D;->VIDEO_ALBUM_PERMALINK:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/04D;->COLLAGE_MIXED_MEDIA:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/04D;->RESULTS_PAGE_MIXED_MEDIA:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/04D;->VERVE:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/04D;->SAVED_DASHBOARD:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/04D;->SAVED_REMINDER:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/04D;->SEARCH_RESULTS:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/04D;->SOUVENIR:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/04D;->INSTANT_ARTICLES:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/04D;->INSTANT_SHOPPING:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/04D;->BACKSTAGE_VIDEOS:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/04D;->MOMENTS_THUMBNAIL_VIDEO:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/04D;->MOMENTS_FULLSCREEN_VIDEO:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/04D;->FULLSCREEN_VIDEO_FROM_NOTIFICATIONS:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/04D;->MESSENGER_MONTAGE_VIEWER_INLINE:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/04D;->MESSENGER_MONTAGE_VIEWER_RVP:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/04D;->VIDEO_EDITING_GALLERY:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/04D;->LIVE_VIDEO_END_SCREEN:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/04D;->OPTIMISTIC_POSTING_INLINE:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/04D;->VIDEO_HOME:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/04D;->VIDEO_HOME_GUIDE:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/04D;->SIMPLE_PICKER:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/04D;->QUICK_PROMOTION_FEED:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, LX/04D;->CULTURAL_MOMENTS_SHARE:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, LX/04D;->COMPOSER:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, LX/04D;->MEDIA_GALLERY:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, LX/04D;->COMMENT:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, LX/04D;->FEED_CHEVRON:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, LX/04D;->USER_TIMELINE_CHEVRON:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, LX/04D;->PAGE_TIMELINE_CHEVRON:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, LX/04D;->EVENT_CHEVRON:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, LX/04D;->GROUP_CHEVRON:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, LX/04D;->LIVE_MAP:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, LX/04D;->FACECAST_NUX:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, LX/04D;->BACKGROUND_PLAY:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, LX/04D;->UNKNOWN:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, LX/04D;->MESSENGER_VIDEO_THREAD_RVP:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, LX/04D;->MESSENGER_VIDEO_THREAD_INLINE:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, LX/04D;->CAROUSEL_VIDEO:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, LX/04D;->SINGLE_CREATOR_SET_VIDEO:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, LX/04D;->SINGLE_CREATOR_SET_FOOTER:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, LX/04D;->SINGLE_CREATOR_SET_INLINE:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, LX/04D;->TAROT_COVER_CARD:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, LX/04D;->TAROT_END_CARD:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, LX/04D;->TAROT_FEED:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, LX/04D;->TAROT_MANAGE_SCREEN:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, LX/04D;->TAROT_VIDEO_CARD:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, LX/04D;->MESSENGER_CONTENT_SEARCH:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, LX/04D;->MESSENGER_MULTIMEDIA_EDITOR:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, LX/04D;->MESSENGER_MEDIA_TRAY_POPUP:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, LX/04D;->MESSENGER_QUICK_CAM_VIEW:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, LX/04D;->MESSENGER_VIDEO_MESSAGE_VIEW:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, LX/04D;->MESSENGER_VIDEO_MEDIA_SHARE_VIEW:LX/04D;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, LX/04D;->INSPIRATION_CAMERA:LX/04D;

    aput-object v2, v0, v1

    sput-object v0, LX/04D;->$VALUES:[LX/04D;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12824
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12825
    iput-object p3, p0, LX/04D;->origin:Ljava/lang/String;

    .line 12826
    iput-object p4, p0, LX/04D;->subOrigin:Ljava/lang/String;

    .line 12827
    return-void
.end method

.method public static asPlayerOrigin(Ljava/lang/String;)LX/04D;
    .locals 5
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 12817
    if-nez p0, :cond_1

    .line 12818
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    .line 12819
    :cond_0
    :goto_0
    return-object v0

    .line 12820
    :cond_1
    invoke-static {}, LX/04D;->values()[LX/04D;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 12821
    invoke-virtual {v0}, LX/04D;->asString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 12822
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 12823
    :cond_2
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/04D;
    .locals 1

    .prologue
    .line 12816
    const-class v0, LX/04D;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/04D;

    return-object v0
.end method

.method public static values()[LX/04D;
    .locals 1

    .prologue
    .line 12815
    sget-object v0, LX/04D;->$VALUES:[LX/04D;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/04D;

    return-object v0
.end method


# virtual methods
.method public final asString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 12814
    const-string v0, "%s::%s"

    iget-object v1, p0, LX/04D;->origin:Ljava/lang/String;

    iget-object v2, p0, LX/04D;->subOrigin:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
