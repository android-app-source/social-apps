.class public final LX/005;
.super LX/006;
.source ""


# instance fields
.field private final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Stack",
            "<",
            "LX/017;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/017;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/017;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private d:LX/03v;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 773
    invoke-direct {p0}, LX/006;-><init>()V

    .line 774
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, LX/005;->a:Ljava/lang/ThreadLocal;

    .line 775
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/005;->b:Ljava/util/Map;

    .line 776
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/005;->c:Ljava/util/ArrayList;

    return-void
.end method

.method private a()Ljava/util/Stack;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Stack",
            "<",
            "LX/017;",
            ">;"
        }
    .end annotation

    .prologue
    .line 777
    iget-object v0, p0, LX/005;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 778
    if-nez v0, :cond_0

    .line 779
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    .line 780
    iget-object v1, p0, LX/005;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 781
    :cond_0
    return-object v0
.end method

.method public static c(LX/005;Ljava/lang/String;)LX/017;
    .locals 4

    .prologue
    .line 782
    invoke-static {p0, p1}, LX/005;->e(LX/005;Ljava/lang/String;)LX/017;

    move-result-object v0

    .line 783
    const-wide/16 v2, 0x6

    invoke-static {v2, v3, p1}, LX/018;->a(JLjava/lang/String;)V

    .line 784
    return-object v0
.end method

.method public static declared-synchronized c(LX/005;LX/017;)V
    .locals 10

    .prologue
    .line 785
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p1, LX/017;->d:J

    .line 786
    iget-boolean v0, p1, LX/017;->e:Z

    .line 787
    sget-object v1, LX/00w;->b:LX/00w;

    move-object v1, v1

    .line 788
    invoke-virtual {v1}, LX/00w;->e()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p1, LX/017;->e:Z

    .line 789
    iget-object v0, p1, LX/017;->f:LX/00q;

    if-eqz v0, :cond_0

    .line 790
    iget-object v0, p1, LX/017;->f:LX/00q;

    invoke-virtual {v0}, LX/00q;->o()V

    .line 791
    :cond_0
    iget-object v0, p0, LX/005;->d:LX/03v;

    if-eqz v0, :cond_1

    .line 792
    iget-object v1, p0, LX/005;->d:LX/03v;

    iget v2, p1, LX/017;->a:I

    iget-object v3, p1, LX/017;->b:Ljava/lang/String;

    iget-wide v4, p1, LX/017;->c:J

    iget-wide v6, p1, LX/017;->d:J

    iget-boolean v8, p1, LX/017;->e:Z

    iget-object v9, p1, LX/017;->f:LX/00q;

    invoke-virtual/range {v1 .. v9}, LX/03v;->a(ILjava/lang/String;JJZLX/00q;)V

    .line 793
    iget-object v0, p0, LX/005;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794
    :cond_1
    monitor-exit p0

    return-void

    .line 795
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized e(LX/005;Ljava/lang/String;)LX/017;
    .locals 4

    .prologue
    .line 796
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/017;

    invoke-direct {v0, p0}, LX/017;-><init>(LX/005;)V

    .line 797
    iput-object p1, v0, LX/017;->b:Ljava/lang/String;

    .line 798
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, LX/017;->c:J

    .line 799
    sget-object v1, LX/00w;->b:LX/00w;

    move-object v1, v1

    .line 800
    invoke-virtual {v1}, LX/00w;->e()Z

    move-result v1

    iput-boolean v1, v0, LX/017;->e:Z

    .line 801
    const/4 v1, 0x0

    iput-object v1, v0, LX/017;->f:LX/00q;

    .line 802
    iget-object v1, p0, LX/005;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803
    monitor-exit p0

    return-object v0

    .line 804
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)LX/00X;
    .locals 2

    .prologue
    .line 805
    invoke-static {p0, p1}, LX/005;->c(LX/005;Ljava/lang/String;)LX/017;

    move-result-object v0

    .line 806
    iput p2, v0, LX/017;->a:I

    .line 807
    invoke-direct {p0}, LX/005;->a()Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 808
    return-object v0
.end method

.method public final declared-synchronized a(LX/03v;)V
    .locals 1

    .prologue
    .line 809
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/005;->d:LX/03v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 810
    monitor-exit p0

    return-void

    .line 811
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StringFormatUse"
        }
    .end annotation

    .prologue
    .line 812
    invoke-direct {p0}, LX/005;->a()Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/017;

    .line 813
    iget-object v1, v0, LX/017;->b:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 814
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unbalanced LightweightPerfEvents.stop(). Expected: %s Actual: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, v0, LX/017;->b:Ljava/lang/String;

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object p1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 815
    :cond_0
    const-wide/16 v5, 0x6

    invoke-static {v5, v6}, LX/018;->a(J)V

    .line 816
    invoke-static {p0, v0}, LX/005;->c(LX/005;LX/017;)V

    .line 817
    return-void
.end method

.method public final b(Ljava/lang/String;I)LX/00X;
    .locals 2

    .prologue
    .line 818
    invoke-static {p0, p1}, LX/005;->c(LX/005;Ljava/lang/String;)LX/017;

    move-result-object v0

    .line 819
    invoke-static {}, LX/00q;->a()LX/00q;

    move-result-object v1

    iput-object v1, v0, LX/017;->f:LX/00q;

    .line 820
    move-object v0, v0

    .line 821
    iput p2, v0, LX/017;->a:I

    .line 822
    invoke-direct {p0}, LX/005;->a()Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 823
    return-object v0
.end method

.method public final declared-synchronized b(LX/03v;)V
    .locals 12

    .prologue
    .line 824
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/005;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v0, 0x0

    move v10, v0

    :goto_0
    if-ge v10, v11, :cond_0

    iget-object v0, p0, LX/005;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/017;

    .line 825
    iget v2, v0, LX/017;->a:I

    iget-object v3, v0, LX/017;->b:Ljava/lang/String;

    iget-wide v4, v0, LX/017;->c:J

    iget-wide v6, v0, LX/017;->d:J

    iget-boolean v8, v0, LX/017;->e:Z

    iget-object v9, v0, LX/017;->f:LX/00q;

    move-object v1, p1

    invoke-virtual/range {v1 .. v9}, LX/03v;->a(ILjava/lang/String;JJZLX/00q;)V

    .line 826
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_0

    .line 827
    :cond_0
    iget-object v0, p0, LX/005;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 828
    monitor-exit p0

    return-void

    .line 829
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StringFormatUse"
        }
    .end annotation

    .prologue
    .line 830
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/005;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/017;

    .line 831
    if-nez v0, :cond_0

    .line 832
    const-string v0, "LWP"

    const-string v1, "LightweightPerfEvents.stopAsync() called for %s without first calling startAsync()."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 833
    :goto_0
    monitor-exit p0

    return-void

    .line 834
    :cond_0
    :try_start_1
    const-wide/16 v4, 0x6

    iget-object v6, v0, LX/017;->b:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, LX/018;->c(JLjava/lang/String;I)V

    .line 835
    invoke-static {p0, v0}, LX/005;->c(LX/005;LX/017;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 836
    goto :goto_0

    .line 837
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 838
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/005;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 839
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 840
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 841
    :cond_0
    :try_start_1
    invoke-static {p0, p1}, LX/005;->e(LX/005;Ljava/lang/String;)LX/017;

    move-result-object v2

    .line 842
    const-wide/16 v4, 0x6

    const/4 v3, 0x0

    invoke-static {v4, v5, p1, v3}, LX/018;->b(JLjava/lang/String;I)V

    .line 843
    move-object v0, v2

    .line 844
    iput p2, v0, LX/017;->a:I

    .line 845
    iget-object v1, p0, LX/005;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 846
    monitor-exit p0

    return-void
.end method
