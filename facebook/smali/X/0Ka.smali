.class public final LX/0Ka;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Jr;
.implements LX/0Ju;
.implements LX/0KZ;
.implements LX/0GJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/video/vps/spatialaudio/AudioSpatializer$EventListener;",
        "Lcom/facebook/video/vps/spatialaudio/SpatialAudioTrackRenderer$EventListener;",
        "LX/0Jr;",
        "LX/0Ju;",
        "LX/0KZ;",
        "LX/0GJ",
        "<",
        "LX/0Nk;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/vps/VideoPlayerService;

.field private final b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;


# direct methods
.method public constructor <init>(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 0

    .prologue
    .line 41479
    iput-object p1, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41480
    iput-object p2, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 41481
    return-void
.end method

.method private a(ILX/0AR;IJJ)V
    .locals 8

    .prologue
    .line 41482
    new-instance v2, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    invoke-direct {v2, p2}, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;-><init>(LX/0AR;)V

    .line 41483
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->h(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;

    move-result-object v1

    .line 41484
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v3, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v0, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v0

    .line 41485
    iput-object v2, v1, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    .line 41486
    if-nez v0, :cond_0

    .line 41487
    :goto_0
    return-void

    .line 41488
    :cond_0
    if-nez p2, :cond_1

    const/4 v2, 0x0

    :cond_1
    move v1, p1

    move v3, p3

    move-wide v4, p4

    move-wide v6, p6

    :try_start_0
    invoke-virtual/range {v0 .. v7}, LX/0Kb;->a(ILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;IJJ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 41489
    :catch_0
    move-exception v1

    .line 41490
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error onDownstreamFormatChanged for listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; caused by: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0
.end method

.method private a(LX/0Nk;)V
    .locals 12

    .prologue
    .line 41491
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "On hls manifest: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, LX/0Nk;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1, v2}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41492
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v11, v0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 41493
    new-instance v1, LX/0OE;

    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v2, "ExoService"

    invoke-direct {v1, v0, v2}, LX/0OE;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 41494
    new-instance v0, LX/0G8;

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v4, v4, Lcom/facebook/video/vps/VideoPlayerService;->c:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/0G8;-><init>(LX/0OE;Ljava/lang/String;ZLandroid/net/Uri;Z)V

    .line 41495
    new-instance v1, LX/0Ni;

    const/4 v2, 0x1

    iget-object v3, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v3, v3, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v3}, LX/0Nc;->a(Landroid/content/Context;)LX/0Nc;

    move-result-object v6

    new-instance v7, LX/0OC;

    invoke-direct {v7}, LX/0OC;-><init>()V

    new-instance v8, LX/0Nt;

    invoke-direct {v8}, LX/0Nt;-><init>()V

    const/4 v9, 0x1

    move-object v3, v0

    move-object v5, p1

    invoke-direct/range {v1 .. v9}, LX/0Ni;-><init>(ZLX/0G6;Ljava/lang/String;LX/0Nk;LX/0Nb;LX/04m;LX/0Nt;I)V

    .line 41496
    invoke-static {v11}, LX/040;->c(Ljava/util/Map;)I

    move-result v0

    .line 41497
    invoke-static {v11}, LX/040;->a(Ljava/util/Map;)I

    move-result v2

    .line 41498
    new-instance v4, LX/0Kt;

    new-instance v3, LX/0OB;

    invoke-direct {v3, v0}, LX/0OB;-><init>(I)V

    invoke-direct {v4, v3}, LX/0Kt;-><init>(LX/0O1;)V

    .line 41499
    new-instance v3, LX/0Ns;

    mul-int/2addr v0, v2

    invoke-direct {v3, v1, v4, v0}, LX/0Ns;-><init>(LX/0Ni;LX/0Gb;I)V

    .line 41500
    new-instance v9, LX/0Ka;

    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-direct {v9, v0, v1}, LX/0Ka;-><init>(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41501
    new-instance v1, LX/0Jw;

    iget-object v2, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    sget-object v4, LX/0L1;->a:LX/0L1;

    const/4 v5, 0x1

    const-wide/16 v6, 0x0

    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v8

    const/4 v10, -0x1

    invoke-direct/range {v1 .. v10}, LX/0Jw;-><init>(Landroid/content/Context;LX/0L9;LX/0L1;IJLandroid/os/Handler;LX/0Ju;I)V

    .line 41502
    invoke-static {v11}, LX/040;->Z(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v5, LX/0GY;

    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v0

    invoke-direct {v5, v3, v0, v9}, LX/0GY;-><init>(LX/0L9;Landroid/os/Handler;LX/0Jr;)V

    .line 41503
    :goto_0
    :try_start_0
    iget-object v2, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v3, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    const/4 v6, 0x0

    new-instance v7, Lcom/facebook/exoplayer/ipc/RendererContext;

    sget-object v0, LX/0HE;->HLS:LX/0HE;

    invoke-virtual {v0}, LX/0HE;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, v0, v4, v8, v9}, Lcom/facebook/exoplayer/ipc/RendererContext;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    move-object v4, v1

    .line 41504
    invoke-static/range {v2 .. v7}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0GT;LX/0GT;ZLcom/facebook/exoplayer/ipc/RendererContext;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41505
    :goto_1
    return-void

    .line 41506
    :cond_0
    new-instance v5, LX/0GX;

    sget-object v0, LX/0L1;->a:LX/0L1;

    iget-object v2, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v2}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v5, v3, v0, v2, v9}, LX/0GX;-><init>(LX/0L9;LX/0L1;Landroid/os/Handler;LX/0Jr;)V

    goto :goto_0

    .line 41507
    :catch_0
    move-exception v0

    .line 41508
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Caught exception when building hls renderers: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 41509
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v0

    .line 41510
    if-eqz v0, :cond_0

    .line 41511
    :try_start_0
    invoke-virtual {v0, p1}, LX/0Kb;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41512
    :cond_0
    :goto_0
    return-void

    .line 41513
    :catch_0
    move-exception v1

    .line 41514
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error onSpatialAudioBufferUnderrun for listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; caused by: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0
.end method

.method public final a(IIIF)V
    .locals 4

    .prologue
    .line 41524
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onVideoSizeChanged w="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pixelWHRatio="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1, v2}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41525
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->h(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;

    move-result-object v0

    .line 41526
    iput p1, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->a:I

    .line 41527
    iput p2, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->b:I

    .line 41528
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v0

    .line 41529
    if-nez v0, :cond_0

    .line 41530
    :goto_0
    return-void

    .line 41531
    :cond_0
    :try_start_0
    invoke-virtual {v0, p1, p2, p4}, LX/0Kb;->a(IIF)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 41532
    :catch_0
    move-exception v1

    .line 41533
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error onVideoSizeChanged for listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; caused by: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 4

    .prologue
    .line 41456
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v0

    .line 41457
    if-eqz v0, :cond_0

    .line 41458
    :try_start_0
    invoke-virtual {v0, p1, p2, p3}, LX/0Kb;->a(IJ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41459
    :cond_0
    :goto_0
    return-void

    .line 41460
    :catch_0
    move-exception v1

    .line 41461
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error onDroppedFrames for listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; caused by: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0
.end method

.method public final a(IJJ)V
    .locals 6

    .prologue
    .line 41515
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    const-string v2, "ChunkLoad upstream discarded: sourceId=%d, mediaStartTimeMs=%d, mediaEndTimeMs=%d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    .line 41516
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41517
    return-void
.end method

.method public final a(IJJJ)V
    .locals 6

    .prologue
    .line 41518
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    const-string v2, "ChunkLoad started: sourceId=%d, length=%d, mediaStartTimeMs=%d, mediaEndTimeMs=%d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    .line 41519
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41520
    return-void
.end method

.method public final a(IJJJJ)V
    .locals 6

    .prologue
    .line 41521
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    const-string v2, "ChunkLoad completed: sourceId=%d, mediaStartTimeMs=%d, mediaEndTimeMs=%d, elapsedMs=%d, durationMs=%d"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    .line 41522
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41523
    return-void
.end method

.method public final a(ILX/0AR;IJ)V
    .locals 10

    .prologue
    .line 41470
    if-eqz p2, :cond_0

    iget-object v0, p2, LX/0AR;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 41471
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Format: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p2, LX/0AR;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", bitrate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, LX/0AR;->c:I

    div-int/lit16 v2, v2, 0x3e8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kbps, w: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, LX/0AR;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", h:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, LX/0AR;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1, v2}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41472
    :cond_0
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->b(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J

    move-result-wide v6

    .line 41473
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v8, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v8

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    .line 41474
    :try_start_0
    invoke-direct/range {v0 .. v7}, LX/0Ka;->a(ILX/0AR;IJJ)V

    .line 41475
    monitor-exit v8

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(ILjava/io/IOException;)V
    .locals 6

    .prologue
    .line 41476
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    const-string v2, "ChunkLoad error: sourceId=%d, message=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 41477
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41478
    return-void
.end method

.method public final a(LX/0L3;)V
    .locals 3

    .prologue
    .line 41467
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "MALFORMED"

    iget-object v2, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 41468
    invoke-static {v0, v1, p1, v2}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Ljava/lang/Throwable;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41469
    return-void
.end method

.method public final a(LX/0LK;)V
    .locals 3

    .prologue
    .line 41464
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "MALFORMED"

    iget-object v2, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 41465
    invoke-static {v0, v1, p1, v2}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Ljava/lang/Throwable;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41466
    return-void
.end method

.method public final a(LX/0LM;)V
    .locals 0

    .prologue
    .line 41463
    return-void
.end method

.method public final a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 0

    .prologue
    .line 41462
    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 3

    .prologue
    .line 41454
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "Surface is drawn"

    iget-object v2, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1, v2}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41455
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 41453
    check-cast p1, LX/0Nk;

    invoke-direct {p0, p1}, LX/0Ka;->a(LX/0Nk;)V

    return-void
.end method

.method public final a(Ljava/lang/String;JJ)V
    .locals 0

    .prologue
    .line 41452
    return-void
.end method

.method public final b(IJ)V
    .locals 6

    .prologue
    .line 41449
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    const-string v2, "ChunkLoad cancelled: sourceId=%d, bytesLoaded=%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    .line 41450
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41451
    return-void
.end method

.method public final b(Ljava/io/IOException;)V
    .locals 3

    .prologue
    .line 41446
    iget-object v0, p0, LX/0Ka;->a:Lcom/facebook/video/vps/VideoPlayerService;

    const-string v1, "ERROR_IO"

    iget-object v2, p0, LX/0Ka;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 41447
    invoke-static {v0, v1, p1, v2}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Ljava/lang/Throwable;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41448
    return-void
.end method
