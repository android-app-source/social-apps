.class public final LX/0Ee;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0EY;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0EY",
        "<",
        "Lcom/facebook/browserextensions/ipc/GetUserIDJSBridgeCall;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;
    .locals 6

    .prologue
    .line 32416
    new-instance v0, Lcom/facebook/browserextensions/ipc/GetUserIDJSBridgeCall;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/GetUserIDJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32417
    new-instance v0, Lcom/facebook/browserextensions/ipc/GetUserIDJSBridgeCall;

    invoke-direct {v0, p1}, Lcom/facebook/browserextensions/ipc/GetUserIDJSBridgeCall;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32415
    new-array v0, p1, [Lcom/facebook/browserextensions/ipc/GetUserIDJSBridgeCall;

    return-object v0
.end method
