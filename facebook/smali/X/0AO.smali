.class public final LX/0AO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:[LX/07w;


# direct methods
.method public constructor <init>([LX/07w;)V
    .locals 0

    .prologue
    .line 23799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23800
    iput-object p1, p0, LX/0AO;->a:[LX/07w;

    .line 23801
    return-void
.end method

.method public static final a(Ljava/io/DataInput;)LX/0AO;
    .locals 6

    .prologue
    .line 23802
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v0

    .line 23803
    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 23804
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "wrong dso manifest version"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23805
    :cond_0
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 23806
    if-gez v1, :cond_1

    .line 23807
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "illegal number of shared libraries"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23808
    :cond_1
    new-array v2, v1, [LX/07w;

    .line 23809
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 23810
    new-instance v3, LX/07w;

    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/07w;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v0

    .line 23811
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 23812
    :cond_2
    new-instance v0, LX/0AO;

    invoke-direct {v0, v2}, LX/0AO;-><init>([LX/07w;)V

    return-object v0
.end method
