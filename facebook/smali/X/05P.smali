.class public final LX/05P;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;

.field public C:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public D:I

.field public E:Z

.field public F:Z

.field public G:Z

.field public H:Z

.field public I:Z

.field public J:Z

.field private K:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Landroid/content/Context;

.field public b:Ljava/lang/String;

.field public c:LX/056;

.field public d:LX/055;

.field public e:LX/05G;

.field public f:LX/05F;

.field public g:LX/05G;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/05F;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/05L;

.field public j:LX/05K;

.field public k:LX/04p;

.field public l:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public n:Landroid/os/Handler;

.field public o:LX/05R;

.field public p:LX/05O;

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/04v;

.field public s:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public x:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/05H;

.field public z:LX/05M;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16024
    iput v0, p0, LX/05P;->D:I

    .line 16025
    iput-boolean v0, p0, LX/05P;->E:Z

    .line 16026
    iput-boolean v0, p0, LX/05P;->G:Z

    .line 16027
    iput-boolean v0, p0, LX/05P;->H:Z

    .line 16028
    iput-boolean v0, p0, LX/05P;->I:Z

    .line 16029
    iput-boolean v0, p0, LX/05P;->J:Z

    return-void
.end method


# virtual methods
.method public final a(LX/04p;)LX/05P;
    .locals 0

    .prologue
    .line 16030
    iput-object p1, p0, LX/05P;->k:LX/04p;

    .line 16031
    return-object p0
.end method

.method public final a(LX/04v;)LX/05P;
    .locals 0

    .prologue
    .line 16032
    iput-object p1, p0, LX/05P;->r:LX/04v;

    .line 16033
    return-object p0
.end method

.method public final a(LX/055;)LX/05P;
    .locals 0

    .prologue
    .line 16034
    iput-object p1, p0, LX/05P;->d:LX/055;

    .line 16035
    return-object p0
.end method

.method public final a(LX/056;)LX/05P;
    .locals 0

    .prologue
    .line 16036
    iput-object p1, p0, LX/05P;->c:LX/056;

    .line 16037
    return-object p0
.end method

.method public final a(LX/05F;)LX/05P;
    .locals 0

    .prologue
    .line 16038
    iput-object p1, p0, LX/05P;->f:LX/05F;

    .line 16039
    return-object p0
.end method

.method public final a(LX/05G;)LX/05P;
    .locals 0

    .prologue
    .line 16040
    iput-object p1, p0, LX/05P;->e:LX/05G;

    .line 16041
    return-object p0
.end method

.method public final a(LX/05H;)LX/05P;
    .locals 0

    .prologue
    .line 16044
    iput-object p1, p0, LX/05P;->y:LX/05H;

    .line 16045
    return-object p0
.end method

.method public final a(LX/05K;)LX/05P;
    .locals 0

    .prologue
    .line 16042
    iput-object p1, p0, LX/05P;->j:LX/05K;

    .line 16043
    return-object p0
.end method

.method public final a(LX/05L;)LX/05P;
    .locals 0

    .prologue
    .line 16054
    iput-object p1, p0, LX/05P;->i:LX/05L;

    .line 16055
    return-object p0
.end method

.method public final a(LX/05M;)LX/05P;
    .locals 0

    .prologue
    .line 16052
    iput-object p1, p0, LX/05P;->z:LX/05M;

    .line 16053
    return-object p0
.end method

.method public final a(LX/05N;)LX/05P;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/05P;"
        }
    .end annotation

    .prologue
    .line 16050
    iput-object p1, p0, LX/05P;->l:LX/05N;

    .line 16051
    return-object p0
.end method

.method public final a(LX/05O;)LX/05P;
    .locals 0

    .prologue
    .line 16048
    iput-object p1, p0, LX/05P;->p:LX/05O;

    .line 16049
    return-object p0
.end method

.method public final a(LX/05R;)LX/05P;
    .locals 0

    .prologue
    .line 16046
    iput-object p1, p0, LX/05P;->o:LX/05R;

    .line 16047
    return-object p0
.end method

.method public final a(Landroid/content/Context;)LX/05P;
    .locals 0

    .prologue
    .line 16019
    iput-object p1, p0, LX/05P;->a:Landroid/content/Context;

    .line 16020
    return-object p0
.end method

.method public final a(Landroid/os/Handler;)LX/05P;
    .locals 0

    .prologue
    .line 16021
    iput-object p1, p0, LX/05P;->n:Landroid/os/Handler;

    .line 16022
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/05P;
    .locals 0

    .prologue
    .line 16017
    iput-object p1, p0, LX/05P;->b:Ljava/lang/String;

    .line 16018
    return-object p0
.end method

.method public final a(Ljava/util/concurrent/atomic/AtomicReference;)LX/05P;
    .locals 0
    .param p1    # Ljava/util/concurrent/atomic/AtomicReference;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "LX/05P;"
        }
    .end annotation

    .prologue
    .line 16015
    iput-object p1, p0, LX/05P;->A:Ljava/util/concurrent/atomic/AtomicReference;

    .line 16016
    return-object p0
.end method

.method public final a()LX/05S;
    .locals 39

    .prologue
    .line 16014
    new-instance v1, LX/05S;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/05P;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/05P;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/05P;->c:LX/056;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/05P;->d:LX/055;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/05P;->e:LX/05G;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/05P;->f:LX/05F;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/05P;->g:LX/05G;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/05P;->h:LX/05F;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/05P;->i:LX/05L;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/05P;->j:LX/05K;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/05P;->k:LX/04p;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/05P;->l:LX/05N;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/05P;->m:LX/05N;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/05P;->n:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->o:LX/05R;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->p:LX/05O;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->q:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->r:LX/04v;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->s:LX/05N;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->t:LX/05N;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->u:LX/05N;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->v:LX/05N;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->w:LX/05N;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->x:LX/05N;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->y:LX/05H;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->z:LX/05M;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->A:Ljava/util/concurrent/atomic/AtomicReference;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->B:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->C:LX/05N;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/05P;->D:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/05P;->E:Z

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/05P;->F:Z

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/05P;->G:Z

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/05P;->H:Z

    move/from16 v35, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/05P;->I:Z

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/05P;->J:Z

    move/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/05P;->K:Ljava/util/Map;

    move-object/from16 v38, v0

    invoke-direct/range {v1 .. v38}, LX/05S;-><init>(Landroid/content/Context;Ljava/lang/String;LX/056;LX/055;LX/05G;LX/05F;LX/05G;LX/05F;LX/05L;LX/05K;LX/04p;LX/05N;LX/05N;Landroid/os/Handler;LX/05R;LX/05O;Ljava/lang/String;LX/04v;LX/05N;LX/05N;LX/05N;LX/05N;LX/05N;LX/05N;LX/05H;LX/05M;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;LX/05N;IZZZZZZLjava/util/Map;)V

    return-object v1
.end method

.method public final b(LX/05F;)LX/05P;
    .locals 0
    .param p1    # LX/05F;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 16012
    iput-object p1, p0, LX/05P;->h:LX/05F;

    .line 16013
    return-object p0
.end method

.method public final b(LX/05G;)LX/05P;
    .locals 0
    .param p1    # LX/05G;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 16010
    iput-object p1, p0, LX/05P;->g:LX/05G;

    .line 16011
    return-object p0
.end method

.method public final b(LX/05N;)LX/05P;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05N",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "LX/05P;"
        }
    .end annotation

    .prologue
    .line 16008
    iput-object p1, p0, LX/05P;->m:LX/05N;

    .line 16009
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/05P;
    .locals 0

    .prologue
    .line 16006
    iput-object p1, p0, LX/05P;->B:Ljava/lang/String;

    .line 16007
    return-object p0
.end method

.method public final c(LX/05N;)LX/05P;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/05P;"
        }
    .end annotation

    .prologue
    .line 16004
    iput-object p1, p0, LX/05P;->s:LX/05N;

    .line 16005
    return-object p0
.end method

.method public final d(LX/05N;)LX/05P;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05N",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/05P;"
        }
    .end annotation

    .prologue
    .line 16002
    iput-object p1, p0, LX/05P;->t:LX/05N;

    .line 16003
    return-object p0
.end method

.method public final e(LX/05N;)LX/05P;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/05P;"
        }
    .end annotation

    .prologue
    .line 16000
    iput-object p1, p0, LX/05P;->u:LX/05N;

    .line 16001
    return-object p0
.end method

.method public final e(Z)LX/05P;
    .locals 0

    .prologue
    .line 15998
    iput-boolean p1, p0, LX/05P;->I:Z

    .line 15999
    return-object p0
.end method

.method public final f(LX/05N;)LX/05P;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/05P;"
        }
    .end annotation

    .prologue
    .line 15996
    iput-object p1, p0, LX/05P;->v:LX/05N;

    .line 15997
    return-object p0
.end method

.method public final g(LX/05N;)LX/05P;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/05P;"
        }
    .end annotation

    .prologue
    .line 15994
    iput-object p1, p0, LX/05P;->w:LX/05N;

    .line 15995
    return-object p0
.end method

.method public final h(LX/05N;)LX/05P;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/05P;"
        }
    .end annotation

    .prologue
    .line 15992
    iput-object p1, p0, LX/05P;->x:LX/05N;

    .line 15993
    return-object p0
.end method

.method public final i(LX/05N;)LX/05P;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/05P;"
        }
    .end annotation

    .prologue
    .line 15990
    iput-object p1, p0, LX/05P;->C:LX/05N;

    .line 15991
    return-object p0
.end method
