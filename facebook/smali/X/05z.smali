.class public LX/05z;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:I

.field public final c:I

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/066;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/060;

.field public final f:LX/062;

.field public final g:LX/063;

.field public final h:LX/064;

.field public final i:LX/05w;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17151
    const-class v0, LX/05z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/05z;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/05w;)V
    .locals 3

    .prologue
    .line 17152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17153
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/05z;->d:Ljava/util/Set;

    .line 17154
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, LX/05z;->c:I

    .line 17155
    const v0, 0x14ff0

    iput v0, p0, LX/05z;->b:I

    .line 17156
    new-instance v0, LX/060;

    invoke-direct {v0}, LX/060;-><init>()V

    iput-object v0, p0, LX/05z;->e:LX/060;

    .line 17157
    new-instance v0, LX/062;

    invoke-direct {v0}, LX/062;-><init>()V

    iput-object v0, p0, LX/05z;->f:LX/062;

    .line 17158
    new-instance v0, LX/063;

    invoke-direct {v0}, LX/063;-><init>()V

    iput-object v0, p0, LX/05z;->g:LX/063;

    .line 17159
    new-instance v0, LX/064;

    invoke-direct {v0}, LX/064;-><init>()V

    iput-object v0, p0, LX/05z;->h:LX/064;

    .line 17160
    iput-object p1, p0, LX/05z;->i:LX/05w;

    .line 17161
    iget-object v0, p0, LX/05z;->d:Ljava/util/Set;

    new-instance v1, LX/065;

    invoke-direct {v1}, LX/065;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 17162
    iget-object v0, p0, LX/05z;->d:Ljava/util/Set;

    new-instance v1, LX/067;

    invoke-direct {v1}, LX/067;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 17163
    iget-object v0, p0, LX/05z;->d:Ljava/util/Set;

    new-instance v1, LX/068;

    iget-object v2, p0, LX/05z;->g:LX/063;

    invoke-direct {v1, v2}, LX/068;-><init>(LX/063;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 17164
    iget-object v0, p0, LX/05z;->d:Ljava/util/Set;

    new-instance v1, LX/069;

    iget-object v2, p0, LX/05z;->e:LX/060;

    invoke-direct {v1, v2}, LX/069;-><init>(LX/060;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 17165
    iget-object v0, p0, LX/05z;->d:Ljava/util/Set;

    new-instance v1, LX/06A;

    iget-object v2, p0, LX/05z;->f:LX/062;

    invoke-direct {v1, v2}, LX/06A;-><init>(LX/062;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 17166
    return-void
.end method
