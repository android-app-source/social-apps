.class public final LX/0Ft;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/DoNotOptimize;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34093
    return-void
.end method

.method public static a(Ljava/lang/Throwable;)I
    .locals 2

    .prologue
    .line 34094
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    invoke-static {p0}, LX/0Fr;->a(Ljava/lang/Throwable;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, LX/0Fs;->a(Ljava/lang/Throwable;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 34095
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    invoke-static {p0}, LX/0Fr;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/0Fs;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
