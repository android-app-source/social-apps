.class public final LX/0At;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 25119
    invoke-static {p0, p1}, LX/0At;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/StringBuilder;II)Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0x2f

    const/16 v4, 0x2e

    .line 25120
    if-lt p1, p2, :cond_0

    .line 25121
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 25122
    :goto_0
    return-object v0

    .line 25123
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    if-ne v0, v5, :cond_1

    .line 25124
    add-int/lit8 p1, p1, 0x1

    :cond_1
    move v0, p1

    move v3, p1

    .line 25125
    :goto_1
    if-gt v0, p2, :cond_7

    .line 25126
    if-ne v0, p2, :cond_2

    move v2, v0

    .line 25127
    :goto_2
    add-int/lit8 v1, v3, 0x1

    if-ne v0, v1, :cond_4

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_4

    .line 25128
    invoke-virtual {p0, v3, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 25129
    sub-int v0, v2, v3

    sub-int/2addr p2, v0

    move v0, v3

    .line 25130
    goto :goto_1

    .line 25131
    :cond_2
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v5, :cond_3

    .line 25132
    add-int/lit8 v1, v0, 0x1

    move v2, v1

    goto :goto_2

    .line 25133
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 25134
    goto :goto_1

    .line 25135
    :cond_4
    add-int/lit8 v1, v3, 0x2

    if-ne v0, v1, :cond_6

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_6

    add-int/lit8 v1, v3, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_6

    .line 25136
    const-string v0, "/"

    add-int/lit8 v1, v3, -0x2

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    .line 25137
    if-le v1, p1, :cond_5

    move v0, v1

    .line 25138
    :goto_3
    invoke-virtual {p0, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 25139
    sub-int v0, v2, v0

    sub-int/2addr p2, v0

    move v0, v1

    move v3, v1

    .line 25140
    goto :goto_1

    :cond_5
    move v0, p1

    .line 25141
    goto :goto_3

    .line 25142
    :cond_6
    add-int/lit8 v0, v0, 0x1

    move v3, v0

    .line 25143
    goto :goto_1

    .line 25144
    :cond_7
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)[I
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v8, 0x2f

    const/4 v7, -0x1

    .line 25145
    const/4 v0, 0x4

    new-array v3, v0, [I

    .line 25146
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25147
    aput v7, v3, v5

    move-object v0, v3

    .line 25148
    :goto_0
    return-object v0

    .line 25149
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 25150
    const/16 v1, 0x23

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 25151
    if-ne v1, v7, :cond_a

    .line 25152
    :goto_1
    const/16 v1, 0x3f

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 25153
    if-eq v1, v7, :cond_1

    if-le v1, v0, :cond_2

    :cond_1
    move v1, v0

    .line 25154
    :cond_2
    invoke-virtual {p0, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 25155
    if-eq v2, v7, :cond_3

    if-le v2, v1, :cond_4

    :cond_3
    move v2, v1

    .line 25156
    :cond_4
    const/16 v6, 0x3a

    invoke-virtual {p0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 25157
    if-le v6, v2, :cond_5

    move v6, v7

    .line 25158
    :cond_5
    add-int/lit8 v2, v6, 0x2

    if-ge v2, v1, :cond_8

    add-int/lit8 v2, v6, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v2, v8, :cond_8

    add-int/lit8 v2, v6, 0x2

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v2, v8, :cond_8

    move v2, v4

    .line 25159
    :goto_2
    if-eqz v2, :cond_9

    .line 25160
    add-int/lit8 v2, v6, 0x3

    invoke-virtual {p0, v8, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 25161
    if-eq v2, v7, :cond_6

    if-le v2, v1, :cond_7

    :cond_6
    move v2, v1

    .line 25162
    :cond_7
    :goto_3
    aput v6, v3, v5

    .line 25163
    aput v2, v3, v4

    .line 25164
    const/4 v2, 0x2

    aput v1, v3, v2

    .line 25165
    const/4 v1, 0x3

    aput v0, v3, v1

    move-object v0, v3

    .line 25166
    goto :goto_0

    :cond_8
    move v2, v5

    .line 25167
    goto :goto_2

    .line 25168
    :cond_9
    add-int/lit8 v2, v6, 0x1

    goto :goto_3

    :cond_a
    move v0, v1

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/16 v8, 0x2f

    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 25169
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 25170
    if-nez p0, :cond_0

    const-string p0, ""

    .line 25171
    :cond_0
    if-nez p1, :cond_1

    const-string p1, ""

    .line 25172
    :cond_1
    invoke-static {p1}, LX/0At;->a(Ljava/lang/String;)[I

    move-result-object v2

    .line 25173
    aget v0, v2, v7

    if-eq v0, v9, :cond_2

    .line 25174
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25175
    aget v0, v2, v5

    aget v2, v2, v6

    invoke-static {v1, v0, v2}, LX/0At;->a(Ljava/lang/StringBuilder;II)Ljava/lang/String;

    .line 25176
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 25177
    :goto_0
    return-object v0

    .line 25178
    :cond_2
    invoke-static {p0}, LX/0At;->a(Ljava/lang/String;)[I

    move-result-object v3

    .line 25179
    const/4 v0, 0x3

    aget v0, v2, v0

    if-nez v0, :cond_3

    .line 25180
    const/4 v0, 0x3

    aget v0, v3, v0

    invoke-virtual {v1, p0, v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 25181
    :cond_3
    aget v0, v2, v6

    if-nez v0, :cond_4

    .line 25182
    aget v0, v3, v6

    invoke-virtual {v1, p0, v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 25183
    :cond_4
    aget v0, v2, v5

    if-eqz v0, :cond_5

    .line 25184
    aget v0, v3, v7

    add-int/lit8 v0, v0, 0x1

    .line 25185
    invoke-virtual {v1, p0, v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25186
    aget v3, v2, v5

    add-int/2addr v3, v0

    aget v2, v2, v6

    add-int/2addr v0, v2

    invoke-static {v1, v3, v0}, LX/0At;->a(Ljava/lang/StringBuilder;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 25187
    :cond_5
    aget v0, v2, v5

    aget v4, v2, v6

    if-eq v0, v4, :cond_6

    aget v0, v2, v5

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v8, :cond_6

    .line 25188
    aget v0, v3, v5

    invoke-virtual {v1, p0, v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25189
    aget v0, v3, v5

    aget v3, v3, v5

    aget v2, v2, v6

    add-int/2addr v2, v3

    invoke-static {v1, v0, v2}, LX/0At;->a(Ljava/lang/StringBuilder;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 25190
    :cond_6
    aget v0, v3, v7

    add-int/lit8 v0, v0, 0x2

    aget v4, v3, v5

    if-ge v0, v4, :cond_7

    aget v0, v3, v5

    aget v4, v3, v6

    if-ne v0, v4, :cond_7

    .line 25191
    aget v0, v3, v5

    invoke-virtual {v1, p0, v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25192
    aget v0, v3, v5

    aget v3, v3, v5

    aget v2, v2, v6

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v0, v2}, LX/0At;->a(Ljava/lang/StringBuilder;II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 25193
    :cond_7
    aget v0, v3, v6

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v8, v0}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v0

    .line 25194
    if-ne v0, v9, :cond_8

    aget v0, v3, v5

    .line 25195
    :goto_1
    invoke-virtual {v1, p0, v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25196
    aget v3, v3, v5

    aget v2, v2, v6

    add-int/2addr v0, v2

    invoke-static {v1, v3, v0}, LX/0At;->a(Ljava/lang/StringBuilder;II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 25197
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
