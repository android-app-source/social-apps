.class public LX/0Jo;
.super LX/0Ji;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field private final h:LX/1m0;

.field private final i:LX/0wr;

.field private final j:LX/16V;

.field private final k:LX/0Sh;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/content/Context;Landroid/os/Handler;LX/0Jv;LX/0Js;LX/16V;LX/0Sh;LX/1m0;LX/0wr;II)V
    .locals 0

    .prologue
    .line 39751
    invoke-direct/range {p0 .. p5}, LX/0Ji;-><init>(Landroid/net/Uri;Landroid/content/Context;Landroid/os/Handler;LX/0Jv;LX/0Js;)V

    .line 39752
    iput-object p6, p0, LX/0Jo;->j:LX/16V;

    .line 39753
    iput-object p7, p0, LX/0Jo;->k:LX/0Sh;

    .line 39754
    iput-object p8, p0, LX/0Jo;->h:LX/1m0;

    .line 39755
    iput-object p9, p0, LX/0Jo;->i:LX/0wr;

    .line 39756
    iput p10, p0, LX/0Jo;->a:I

    .line 39757
    iput p11, p0, LX/0Jo;->b:I

    .line 39758
    return-void
.end method

.method private static a(Landroid/net/Uri;)LX/0Jn;
    .locals 2

    .prologue
    .line 39759
    invoke-static {p0}, LX/1m0;->f(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39760
    invoke-static {p0}, LX/1m0;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/0Jo;->a(Landroid/net/Uri;)LX/0Jn;

    move-result-object v0

    .line 39761
    :goto_0
    return-object v0

    .line 39762
    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 39763
    invoke-static {v0}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39764
    const-string v1, ".mp4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39765
    sget-object v0, LX/0Jn;->MP4:LX/0Jn;

    goto :goto_0

    .line 39766
    :cond_1
    const-string v1, ".webm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 39767
    sget-object v0, LX/0Jn;->WEBM:LX/0Jn;

    goto :goto_0

    .line 39768
    :cond_2
    sget-object v0, LX/0Jn;->UNKNOWN:LX/0Jn;

    goto :goto_0
.end method

.method private a(LX/0L9;Landroid/os/Handler;)LX/0Jx;
    .locals 9

    .prologue
    .line 39769
    new-instance v0, LX/0Jx;

    iget-object v1, p0, LX/0Ji;->d:Landroid/content/Context;

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    iget-object v7, p0, LX/0Ji;->f:LX/0Jv;

    const/4 v8, -0x1

    move-object v2, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v8}, LX/0Jx;-><init>(Landroid/content/Context;LX/0L9;IJLandroid/os/Handler;LX/0Jv;I)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/09L;
    .locals 1

    .prologue
    .line 39770
    sget-object v0, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    return-object v0
.end method

.method public final a(LX/0Jp;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 39771
    new-instance v2, LX/0OE;

    iget-object v0, p0, LX/0Ji;->d:Landroid/content/Context;

    const-string v1, "ExoHttpSource"

    invoke-direct {v2, v0, v1}, LX/0OE;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 39772
    iget-object v0, p0, LX/0Ji;->c:Landroid/net/Uri;

    invoke-static {v0}, LX/0Jo;->a(Landroid/net/Uri;)LX/0Jn;

    move-result-object v0

    .line 39773
    sget-object v1, LX/0Jm;->a:[I

    invoke-virtual {v0}, LX/0Jn;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 39774
    new-instance v1, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;

    invoke-direct {v1}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;-><init>()V

    :goto_0
    move-object v6, v1

    .line 39775
    new-instance v0, LX/0MK;

    iget-object v1, p0, LX/0Ji;->c:Landroid/net/Uri;

    new-instance v3, LX/0OB;

    iget v4, p0, LX/0Jo;->a:I

    invoke-direct {v3, v4}, LX/0OB;-><init>(I)V

    iget v4, p0, LX/0Jo;->a:I

    iget v5, p0, LX/0Jo;->b:I

    mul-int/2addr v4, v5

    const/4 v5, 0x1

    new-array v5, v5, [LX/0ME;

    aput-object v6, v5, v7

    invoke-direct/range {v0 .. v5}, LX/0MK;-><init>(Landroid/net/Uri;LX/0G6;LX/0O1;I[LX/0ME;)V

    .line 39776
    iget-object v1, p0, LX/0Ji;->e:Landroid/os/Handler;

    invoke-direct {p0, v0, v1}, LX/0Jo;->a(LX/0L9;Landroid/os/Handler;)LX/0Jx;

    move-result-object v1

    .line 39777
    iget-object v2, p0, LX/0Ji;->e:Landroid/os/Handler;

    .line 39778
    new-instance v3, LX/0Jt;

    iget-object v4, p0, LX/0Ji;->g:LX/0Js;

    invoke-direct {v3, v0, v2, v4}, LX/0Jt;-><init>(LX/0L9;Landroid/os/Handler;LX/0Js;)V

    move-object v0, v3

    .line 39779
    const/4 v2, 0x0

    invoke-interface {p1, v1, v0, v7, v2}, LX/0Jp;->a(LX/0Jw;LX/0GT;ILjava/lang/String;)V

    .line 39780
    return-void

    .line 39781
    :pswitch_0
    new-instance v1, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;

    invoke-direct {v1}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;-><init>()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
