.class public final LX/0F5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/3Lz;

.field public c:Ljava/lang/String;

.field private d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0W9;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/3Lz;LX/0Or;LX/0W9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/3Lz;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0W9;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32824
    iput-object p1, p0, LX/0F5;->a:Ljava/lang/String;

    .line 32825
    iput-object p2, p0, LX/0F5;->b:LX/3Lz;

    .line 32826
    iput-object p3, p0, LX/0F5;->d:LX/0Or;

    .line 32827
    iput-object p4, p0, LX/0F5;->e:LX/0W9;

    .line 32828
    return-void
.end method

.method public static a(LX/0F5;LX/4hT;)Z
    .locals 3

    .prologue
    .line 32814
    iget-object v0, p0, LX/0F5;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 32815
    iget-object v1, p0, LX/0F5;->b:LX/3Lz;

    invoke-virtual {p1}, LX/4hT;->getCountryCode()I

    move-result v2

    invoke-virtual {v1, v2}, LX/3Lz;->getRegionCodeForCountryCode(I)Ljava/lang/String;

    move-result-object v1

    .line 32816
    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0F5;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 32817
    iget-object v0, p0, LX/0F5;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 32818
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 32819
    :cond_0
    iget-object v0, p0, LX/0F5;->e:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->b()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v0

    .line 32820
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 32821
    :cond_2
    const-string v0, "US"

    .line 32822
    :cond_3
    return-object v0
.end method
