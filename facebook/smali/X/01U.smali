.class public final enum LX/01U;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/01U;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/01U;

.field public static final enum DEBUG:LX/01U;

.field public static final enum IN_HOUSE:LX/01U;

.field public static final enum PROD:LX/01U;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4799
    new-instance v0, LX/01U;

    const-string v1, "DEBUG"

    invoke-direct {v0, v1, v2}, LX/01U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01U;->DEBUG:LX/01U;

    .line 4800
    new-instance v0, LX/01U;

    const-string v1, "IN_HOUSE"

    invoke-direct {v0, v1, v3}, LX/01U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01U;->IN_HOUSE:LX/01U;

    .line 4801
    new-instance v0, LX/01U;

    const-string v1, "PROD"

    invoke-direct {v0, v1, v4}, LX/01U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/01U;->PROD:LX/01U;

    .line 4802
    const/4 v0, 0x3

    new-array v0, v0, [LX/01U;

    sget-object v1, LX/01U;->DEBUG:LX/01U;

    aput-object v1, v0, v2

    sget-object v1, LX/01U;->IN_HOUSE:LX/01U;

    aput-object v1, v0, v3

    sget-object v1, LX/01U;->PROD:LX/01U;

    aput-object v1, v0, v4

    sput-object v0, LX/01U;->$VALUES:[LX/01U;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4793
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/01U;
    .locals 1

    .prologue
    .line 4798
    const-class v0, LX/01U;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/01U;

    return-object v0
.end method

.method public static values()[LX/01U;
    .locals 1

    .prologue
    .line 4803
    sget-object v0, LX/01U;->$VALUES:[LX/01U;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/01U;

    return-object v0
.end method


# virtual methods
.method public final getPermission()Ljava/lang/String;
    .locals 4

    .prologue
    .line 4797
    const-string v0, "com.facebook.permission.%s.FB_APP_COMMUNICATION"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, LX/01U;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getReceiverPermission()Ljava/lang/String;
    .locals 4

    .prologue
    .line 4794
    sget-object v0, LX/01U;->PROD:LX/01U;

    invoke-virtual {p0, v0}, LX/01U;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4795
    const-string v0, "com.facebook.receiver.permission.ACCESS"

    .line 4796
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.facebook.receiver.permission.%s.ACCESS"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, LX/01U;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
