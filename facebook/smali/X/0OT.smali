.class public final LX/0OT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/0OT;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/util/regex/Pattern;

.field private static final h:Ljava/util/regex/Pattern;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:J

.field public final d:Z

.field public final e:Ljava/io/File;

.field public final f:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53138
    const-string v0, "^(.+)\\.(\\d+)\\.(\\d+)\\.v1\\.exo$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0OT;->g:Ljava/util/regex/Pattern;

    .line 53139
    const-string v0, "^(.+)\\.(\\d+)\\.(\\d+)\\.v2\\.exo$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0OT;->h:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;JJZJLjava/io/File;)V
    .locals 0

    .prologue
    .line 53130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53131
    iput-object p1, p0, LX/0OT;->a:Ljava/lang/String;

    .line 53132
    iput-wide p2, p0, LX/0OT;->b:J

    .line 53133
    iput-wide p4, p0, LX/0OT;->c:J

    .line 53134
    iput-boolean p6, p0, LX/0OT;->d:Z

    .line 53135
    iput-object p9, p0, LX/0OT;->e:Ljava/io/File;

    .line 53136
    iput-wide p7, p0, LX/0OT;->f:J

    .line 53137
    return-void
.end method

.method public static a(Ljava/io/File;)LX/0OT;
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 53104
    sget-object v1, LX/0OT;->h:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 53105
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_1

    .line 53106
    :cond_0
    :goto_0
    return-object v0

    .line 53107
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v4, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    .line 53108
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    move v5, v3

    move v2, v3

    .line 53109
    :goto_1
    if-ge v5, v6, :cond_3

    .line 53110
    invoke-virtual {v1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x25

    if-ne v7, v8, :cond_2

    .line 53111
    add-int/lit8 v2, v2, 0x1

    .line 53112
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 53113
    :cond_3
    if-nez v2, :cond_4

    .line 53114
    :goto_2
    move-object v1, v1

    .line 53115
    if-eqz v1, :cond_0

    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v0, 0x3

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    move-object v6, p0

    invoke-static/range {v1 .. v6}, LX/0OT;->a(Ljava/lang/String;JJLjava/io/File;)LX/0OT;

    move-result-object v0

    goto :goto_0

    .line 53116
    :cond_4
    mul-int/lit8 v5, v2, 0x2

    sub-int v5, v6, v5

    .line 53117
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 53118
    sget-object v8, LX/08x;->g:Ljava/util/regex/Pattern;

    invoke-virtual {v8, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    move v11, v3

    move v3, v2

    move v2, v11

    .line 53119
    :goto_3
    if-lez v3, :cond_5

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->find()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 53120
    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x10

    invoke-static {v9, v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    int-to-char v9, v9

    .line 53121
    invoke-virtual {v8}, Ljava/util/regex/Matcher;->start()I

    move-result v10

    invoke-virtual {v7, v1, v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 53122
    invoke-virtual {v8}, Ljava/util/regex/Matcher;->end()I

    move-result v2

    .line 53123
    add-int/lit8 v3, v3, -0x1

    .line 53124
    goto :goto_3

    .line 53125
    :cond_5
    if-ge v2, v6, :cond_6

    .line 53126
    invoke-virtual {v7, v1, v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 53127
    :cond_6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-eq v2, v5, :cond_7

    .line 53128
    const/4 v1, 0x0

    goto :goto_2

    .line 53129
    :cond_7
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;J)LX/0OT;
    .locals 11

    .prologue
    const-wide/16 v4, -0x1

    .line 53103
    new-instance v0, LX/0OT;

    const/4 v6, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v7, v4

    invoke-direct/range {v0 .. v9}, LX/0OT;-><init>(Ljava/lang/String;JJZJLjava/io/File;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;JJ)LX/0OT;
    .locals 11

    .prologue
    .line 53102
    new-instance v0, LX/0OT;

    const/4 v6, 0x0

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v9}, LX/0OT;-><init>(Ljava/lang/String;JJZJLjava/io/File;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;JJLjava/io/File;)LX/0OT;
    .locals 11

    .prologue
    .line 53101
    new-instance v0, LX/0OT;

    invoke-virtual/range {p5 .. p5}, Ljava/io/File;->length()J

    move-result-wide v4

    const/4 v6, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-wide v7, p3

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v9}, LX/0OT;-><init>(Ljava/lang/String;JJZJLjava/io/File;)V

    return-object v0
.end method

.method public static a(Ljava/io/File;Ljava/lang/String;JJ)Ljava/io/File;
    .locals 8

    .prologue
    .line 53067
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    .line 53068
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    move v4, v3

    move v2, v3

    .line 53069
    :goto_0
    if-ge v4, v5, :cond_1

    .line 53070
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, LX/08x;->a(C)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 53071
    add-int/lit8 v2, v2, 0x1

    .line 53072
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 53073
    :cond_1
    if-nez v2, :cond_2

    .line 53074
    :goto_1
    move-object v2, p1

    .line 53075
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".v2.exo"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0

    .line 53076
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    mul-int/lit8 v4, v2, 0x2

    add-int/2addr v4, v5

    invoke-direct {v6, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    move v4, v2

    move v2, v3

    .line 53077
    :goto_2
    if-lez v4, :cond_4

    .line 53078
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 53079
    invoke-static {v2}, LX/08x;->a(C)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 53080
    const/16 v7, 0x25

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53081
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    move v2, v3

    goto :goto_2

    .line 53082
    :cond_3
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v2, v3

    .line 53083
    goto :goto_2

    .line 53084
    :cond_4
    if-ge v2, v5, :cond_5

    .line 53085
    invoke-virtual {v6, p1, v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 53086
    :cond_5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;J)LX/0OT;
    .locals 11

    .prologue
    const-wide/16 v4, -0x1

    .line 53100
    new-instance v0, LX/0OT;

    const/4 v6, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v7, v4

    invoke-direct/range {v0 .. v9}, LX/0OT;-><init>(Ljava/lang/String;JJZJLjava/io/File;)V

    return-object v0
.end method

.method public static b(Ljava/io/File;)Ljava/io/File;
    .locals 6

    .prologue
    .line 53093
    sget-object v0, LX/0OT;->g:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 53094
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53095
    :goto_0
    return-object p0

    .line 53096
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 53097
    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v4, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, LX/0OT;->a(Ljava/io/File;Ljava/lang/String;JJ)Ljava/io/File;

    move-result-object v0

    .line 53098
    invoke-virtual {p0, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-object p0, v0

    .line 53099
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0OT;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 53088
    iget-object v0, p0, LX/0OT;->a:Ljava/lang/String;

    iget-object v1, p1, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53089
    iget-object v0, p0, LX/0OT;->a:Ljava/lang/String;

    iget-object v1, p1, LX/0OT;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 53090
    :goto_0
    return v0

    .line 53091
    :cond_0
    iget-wide v0, p0, LX/0OT;->b:J

    iget-wide v2, p1, LX/0OT;->b:J

    sub-long/2addr v0, v2

    .line 53092
    cmp-long v2, v0, v4

    if-nez v2, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    cmp-long v0, v0, v4

    if-gez v0, :cond_2

    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 53087
    check-cast p1, LX/0OT;

    invoke-virtual {p0, p1}, LX/0OT;->a(LX/0OT;)I

    move-result v0

    return v0
.end method
