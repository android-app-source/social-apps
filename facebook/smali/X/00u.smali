.class public LX/00u;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static d:LX/00u;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/lang/String;

.field private c:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 3258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3259
    iput-object p1, p0, LX/00u;->a:Landroid/content/Context;

    .line 3260
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 3261
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 3262
    :try_start_0
    invoke-static {v0}, Lcom/facebook/common/dextricks/DalvikInternals;->realpath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3263
    if-eqz v2, :cond_0

    .line 3264
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3265
    :goto_0
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "modules"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/00u;->b:Ljava/lang/String;

    .line 3266
    return-void

    .line 3267
    :cond_0
    :try_start_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 3268
    :catch_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)LX/00u;
    .locals 3

    .prologue
    .line 3251
    const-class v2, LX/00u;

    monitor-enter v2

    :try_start_0
    sget-object v0, LX/00u;->d:LX/00u;

    if-eqz v0, :cond_0

    .line 3252
    sget-object v0, LX/00u;->d:LX/00u;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3253
    :goto_0
    monitor-exit v2

    return-object v0

    .line 3254
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 3255
    new-instance v0, LX/00u;

    if-eqz v1, :cond_1

    move-object p0, v1

    :cond_1
    invoke-direct {v0, p0}, LX/00u;-><init>(Landroid/content/Context;)V

    .line 3256
    sput-object v0, LX/00u;->d:LX/00u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3257
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static a(LX/00u;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 3170
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3171
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 3172
    if-eqz v1, :cond_0

    .line 3173
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 3174
    invoke-static {p0, v3}, LX/00u;->a(LX/00u;Ljava/io/File;)V

    .line 3175
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3176
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 3177
    :goto_1
    return-void

    .line 3178
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)LX/02U;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const v8, 0xad0001

    .line 3226
    iget-object v1, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v1, :cond_0

    .line 3227
    iget-object v1, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v1, v8, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 3228
    iget-object v1, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v1, v8, v2, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 3229
    :cond_0
    const/16 v2, 0x57

    .line 3230
    :try_start_0
    new-instance v3, Ljava/io/File;

    iget-object v1, p0, LX/00u;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3231
    invoke-direct {p0, p1}, LX/00u;->g(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 3232
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    .line 3233
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 3234
    invoke-static {p0, p1}, LX/00u;->f(LX/00u;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3235
    if-nez v5, :cond_1

    .line 3236
    const-string v5, "0"

    .line 3237
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v7, v9

    add-int/lit8 v7, v7, 0x5

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-dex"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 3238
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 3239
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, LX/00u;->b:Ljava/lang/String;

    invoke-direct {v6, v7, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_4

    new-instance v1, LX/0Km;

    invoke-direct {v1, p0, p1, v4}, LX/0Km;-><init>(LX/00u;Ljava/lang/String;Ljava/io/File;)V

    :goto_1
    invoke-static {v6, v3, v1}, LX/02U;->open(Ljava/io/File;Ljava/io/File;LX/02S;)LX/02U;

    move-result-object v1

    .line 3240
    invoke-virtual {v1}, LX/02U;->loadManifest()LX/02Y;

    .line 3241
    invoke-virtual {v1}, LX/02U;->getParentNames()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    :goto_2
    if-ge v0, v4, :cond_6

    aget-object v5, v3, v0

    .line 3242
    const-string v6, "dex"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 3243
    invoke-direct {p0, v5}, LX/00u;->b(Ljava/lang/String;)LX/02U;

    .line 3244
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v1, v0

    .line 3245
    goto :goto_0

    .line 3246
    :cond_4
    new-instance v1, LX/0Km;

    invoke-direct {v1, p0, p1}, LX/0Km;-><init>(LX/00u;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 3247
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v1, :cond_5

    .line 3248
    iget-object v1, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-interface {v1, v8, v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    :cond_5
    throw v0

    .line 3249
    :cond_6
    iget-object v0, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v0, :cond_7

    .line 3250
    iget-object v0, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x2

    invoke-interface {v0, v8, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    :cond_7
    return-object v1
.end method

.method public static b(LX/00u;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0Kk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3222
    iget-object v0, p0, LX/00u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 3223
    const-string v1, "app_modules.json"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 3224
    new-instance v1, LX/0Kl;

    invoke-direct {v1}, LX/0Kl;-><init>()V

    .line 3225
    invoke-virtual {v1, v0}, LX/0Kl;->a(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3207
    invoke-direct {p0, p1}, LX/00u;->b(Ljava/lang/String;)LX/02U;

    move-result-object v0

    .line 3208
    invoke-virtual {v0}, LX/02U;->getParentNames()[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 3209
    const-string v4, "dex"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 3210
    invoke-direct {p0, v3}, LX/00u;->d(Ljava/lang/String;)V

    .line 3211
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3212
    :cond_1
    invoke-direct {p0, p1}, LX/00u;->g(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 3213
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3214
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-lib-xzs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "assets"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "libs.xzs"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "assets"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "libs.txt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, LX/01y;->a(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 3215
    :cond_2
    :goto_1
    return-void

    .line 3216
    :cond_3
    iget-object v0, p0, LX/00u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 3217
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/libs.txt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 3218
    if-eqz v0, :cond_2

    .line 3219
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 3220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-lib-xzs"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "assets/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/libs.xzs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "assets/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/libs.txt"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/01y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 3221
    :catch_0
    goto :goto_1
.end method

.method public static f(LX/00u;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 3202
    :try_start_0
    invoke-static {p0}, LX/00u;->b(LX/00u;)Ljava/util/List;

    move-result-object v0

    .line 3203
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Kk;

    .line 3204
    iget-object v2, v0, LX/0Kk;->b:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    .line 3205
    :goto_0
    move-object v0, v0

    .line 3206
    if-eqz v0, :cond_1

    iget-object v0, v0, LX/0Kk;->c:Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 3194
    invoke-static {p0, p1}, LX/00u;->f(LX/00u;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3195
    if-nez v0, :cond_0

    .line 3196
    const-string v0, "VoltronModuleManager"

    const-string v1, "Hash not found for module %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3197
    const/4 v0, 0x0

    .line 3198
    :goto_0
    return-object v0

    .line 3199
    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/00u;->b:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v1

    .line 3200
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "files"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "voltron"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".zip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 3201
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 1

    .prologue
    .line 3191
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3192
    monitor-exit p0

    return-void

    .line 3193
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3179
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v0, :cond_0

    .line 3180
    iget-object v0, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xad0002

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 3181
    iget-object v0, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xad0002

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 3182
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, LX/00u;->b(Ljava/lang/String;)LX/02U;

    move-result-object v0

    .line 3183
    const/4 v1, 0x1

    new-instance v2, LX/0C3;

    invoke-direct {v2}, LX/0C3;-><init>()V

    iget-object v3, p0, LX/00u;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, v2, v3}, LX/02U;->loadAll(ILX/006;Landroid/content/Context;)LX/02X;

    .line 3184
    invoke-direct {p0, p1}, LX/00u;->d(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3185
    :try_start_2
    iget-object v0, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v0, :cond_1

    .line 3186
    iget-object v0, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xad0002

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x2

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3187
    :cond_1
    monitor-exit p0

    return-void

    .line 3188
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v1, :cond_2

    .line 3189
    iget-object v1, p0, LX/00u;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0xad0002

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    const/16 v4, 0x57

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    :cond_2
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3190
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
