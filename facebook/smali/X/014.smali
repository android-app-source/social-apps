.class public LX/014;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/014;


# instance fields
.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3550
    new-instance v0, LX/014;

    const-string v1, ""

    invoke-direct {v0, v1}, LX/014;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/014;->a:LX/014;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3546
    if-eqz p1, :cond_0

    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3547
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3548
    :cond_1
    iput-object p1, p0, LX/014;->b:Ljava/lang/String;

    .line 3549
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/014;
    .locals 2

    .prologue
    .line 3540
    if-nez p0, :cond_0

    .line 3541
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3542
    :cond_0
    const-string v0, ""

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3543
    sget-object v0, LX/014;->a:LX/014;

    .line 3544
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/014;

    invoke-direct {v0, p0}, LX/014;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3537
    const-string v0, ""

    iget-object v1, p0, LX/014;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3538
    const-string v0, "___DEFAULT___"

    .line 3539
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/014;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3532
    if-ne p0, p1, :cond_1

    .line 3533
    :cond_0
    :goto_0
    return v0

    .line 3534
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 3535
    :cond_3
    check-cast p1, LX/014;

    .line 3536
    iget-object v2, p0, LX/014;->b:Ljava/lang/String;

    iget-object v3, p1, LX/014;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 3531
    iget-object v0, p0, LX/014;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3530
    iget-object v0, p0, LX/014;->b:Ljava/lang/String;

    return-object v0
.end method
