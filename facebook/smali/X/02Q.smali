.class public final LX/02Q;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static isKernelPageCacheFlushIsBroken:Z

.field private static sDalvikCacheDirectory:Ljava/io/File;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7624
    const/4 v0, 0x0

    sput-boolean v0, LX/02Q;->isKernelPageCacheFlushIsBroken:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyBytes(Ljava/io/OutputStream;Ljava/io/InputStream;I)I
    .locals 1

    .prologue
    .line 7622
    const v0, 0x8000

    new-array v0, v0, [B

    invoke-static {p0, p1, p2, v0}, LX/02Q;->copyBytes(Ljava/io/OutputStream;Ljava/io/InputStream;I[B)I

    move-result v0

    return v0
.end method

.method public static copyBytes(Ljava/io/OutputStream;Ljava/io/InputStream;I[B)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7617
    move v0, v1

    .line 7618
    :goto_0
    if-ge v0, p2, :cond_0

    sub-int v2, p2, v0

    invoke-static {p1, p3, v2}, LX/02Q;->slurp(Ljava/io/InputStream;[BI)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 7619
    invoke-virtual {p0, p3, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 7620
    add-int/2addr v0, v2

    goto :goto_0

    .line 7621
    :cond_0
    return v0
.end method

.method public static copyBytes(Ljava/io/RandomAccessFile;Ljava/io/InputStream;I[B)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7612
    move v0, v1

    .line 7613
    :goto_0
    if-ge v0, p2, :cond_0

    sub-int v2, p2, v0

    invoke-static {p1, p3, v2}, LX/02Q;->slurp(Ljava/io/InputStream;[BI)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 7614
    invoke-virtual {p0, p3, v1, v2}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 7615
    add-int/2addr v0, v2

    goto :goto_0

    .line 7616
    :cond_0
    return v0
.end method

.method public static deleteRecursive(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 7610
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/dextricks/DalvikInternals;->deleteRecursive(Ljava/lang/String;)V

    .line 7611
    return-void
.end method

.method public static deleteRecursiveNoThrow(Ljava/io/File;)V
    .locals 4
    .param p0    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 7605
    if-eqz p0, :cond_0

    .line 7606
    :try_start_0
    invoke-static {p0}, LX/02Q;->deleteRecursive(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7607
    :cond_0
    :goto_0
    return-void

    .line 7608
    :catch_0
    move-exception v0

    .line 7609
    const-string v1, "error deleting %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static dexOptGenerateCacheFileName(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 7601
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 7602
    if-eqz p2, :cond_0

    .line 7603
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7604
    :cond_0
    new-instance v1, Ljava/io/File;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "/"

    const-string v3, "@"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method public static extensions(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 7597
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 7598
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 7599
    const/4 v0, 0x0

    .line 7600
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static findSystemDalvikCache()Ljava/io/File;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 7582
    sget-object v0, LX/02Q;->sDalvikCacheDirectory:Ljava/io/File;

    if-nez v0, :cond_2

    .line 7583
    const-string v0, "os.arch"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 7584
    const-string v1, "arm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 7585
    const-string v0, "arm"

    .line 7586
    :goto_0
    const-string v1, "ANDROID_DATA"

    invoke-static {v1}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 7587
    if-nez v1, :cond_0

    .line 7588
    const-string v1, "/data"

    .line 7589
    :cond_0
    const-string v2, "%s/dalvik-cache/%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 7590
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_1

    .line 7591
    const-string v0, "%s/dalvik-cache"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v1, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 7592
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v1, LX/02Q;->sDalvikCacheDirectory:Ljava/io/File;

    .line 7593
    :cond_2
    sget-object v0, LX/02Q;->sDalvikCacheDirectory:Ljava/io/File;

    return-object v0

    .line 7594
    :cond_3
    const-string v1, "x86"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "i"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "86"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 7595
    :cond_4
    const-string v0, "x86"

    goto :goto_0

    .line 7596
    :cond_5
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown arch: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static fsyncRecursive(Ljava/io/File;LX/08D;)V
    .locals 4

    .prologue
    .line 7573
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 7574
    const-string v1, "_lock"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7575
    :cond_0
    :goto_0
    return-void

    .line 7576
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7577
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 7578
    invoke-static {v3, p1}, LX/02Q;->fsyncRecursive(Ljava/io/File;LX/08D;)V

    .line 7579
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 7580
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7581
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iget v1, p1, LX/08D;->ioPriority:I

    invoke-static {v0, v1}, Lcom/facebook/common/dextricks/DalvikInternals;->fsyncNamed(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static lastExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 7569
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 7570
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 7571
    const/4 v0, 0x0

    .line 7572
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static link(Ljava/io/File;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 7504
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/common/dextricks/DalvikInternals;->link(Ljava/lang/String;Ljava/lang/String;I)V

    .line 7505
    return-void
.end method

.method public static makeDataFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 7568
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static mkdirOrThrow(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 7501
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    .line 7502
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot mkdir: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7503
    :cond_0
    return-void
.end method

.method public static openDataSyncedFile(Ljava/io/File;)Ljava/io/RandomAccessFile;
    .locals 4

    .prologue
    .line 7506
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/dextricks/DalvikInternals;->openUnixSyncReadWriteFd(Ljava/lang/String;)I

    move-result v1

    .line 7507
    :try_start_0
    new-instance v0, Ljava/io/RandomAccessFile;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/proc/self/task/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/fd/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "rw"

    invoke-direct {v0, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7508
    invoke-static {v1}, Lcom/facebook/common/dextricks/DalvikInternals;->closeUnixFd(I)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/facebook/common/dextricks/DalvikInternals;->closeUnixFd(I)V

    throw v0
.end method

.method public static openUnlinkedTemporaryFile(Ljava/io/File;)Ljava/io/RandomAccessFile;
    .locals 4

    .prologue
    .line 7509
    const-string v0, "dextricks"

    const-string v1, ".tmp"

    invoke-static {v0, v1, p0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 7510
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v2, "rw"

    invoke-direct {v1, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 7511
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    .line 7512
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    .line 7513
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "could not unlink "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7514
    :cond_0
    return-object v1
.end method

.method public static readProgramOutputFile(Ljava/io/RandomAccessFile;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 7515
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 7516
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    const-wide/32 v2, 0x20000

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 7517
    new-array v0, v0, [B

    .line 7518
    invoke-virtual {p0, v0}, Ljava/io/RandomAccessFile;->read([B)I

    .line 7519
    const-string v1, "UTF-8"

    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v1

    sget-object v2, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v1, v2}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v1

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/nio/charset/CharsetDecoder;->replaceWith(Ljava/lang/String;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7520
    const-string v0, "WARNING: linker: "

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7521
    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 7522
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 7523
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 7524
    :cond_0
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 7525
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static renameOrThrow(Ljava/io/File;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 7526
    invoke-virtual {p0, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7527
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "rename of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7528
    :cond_0
    return-void
.end method

.method public static runtimeExFrom(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    .locals 1
    .param p0    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 7529
    if-nez p0, :cond_0

    .line 7530
    new-instance p0, Ljava/lang/RuntimeException;

    const-string v0, "missing exception object"

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 7531
    :goto_0
    return-object p0

    .line 7532
    :cond_0
    instance-of v0, p0, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_1

    .line 7533
    check-cast p0, Ljava/lang/RuntimeException;

    goto :goto_0

    .line 7534
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static safeClose(Landroid/net/LocalSocket;)V
    .locals 4

    .prologue
    .line 7535
    if-eqz p0, :cond_0

    .line 7536
    :try_start_0
    invoke-virtual {p0}, Landroid/net/LocalSocket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 7537
    :cond_0
    :goto_0
    return-void

    .line 7538
    :catch_0
    move-exception v0

    .line 7539
    const-string v1, "error closing %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static safeClose(Ljava/io/Closeable;)V
    .locals 4

    .prologue
    .line 7540
    if-eqz p0, :cond_0

    .line 7541
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 7542
    :cond_0
    :goto_0
    return-void

    .line 7543
    :catch_0
    move-exception v0

    .line 7544
    const-string v1, "error closing %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static slurp(Ljava/io/InputStream;[BI)I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 7545
    array-length v1, p1

    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v1, v2

    .line 7546
    :goto_0
    if-ge v1, v3, :cond_0

    .line 7547
    sub-int v2, v3, v1

    invoke-virtual {p0, p1, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 7548
    if-ltz v2, :cond_0

    .line 7549
    add-int/2addr v1, v2

    goto :goto_0

    .line 7550
    :cond_0
    if-ne v2, v0, :cond_1

    if-nez v1, :cond_1

    .line 7551
    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static stripExtensions(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 7552
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 7553
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 7554
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 7555
    :cond_0
    return-object p0
.end method

.method public static stripLastExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 7556
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 7557
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 7558
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 7559
    :cond_0
    return-object p0
.end method

.method public static symlink(Ljava/io/File;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 7560
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/facebook/common/dextricks/DalvikInternals;->link(Ljava/lang/String;Ljava/lang/String;I)V

    .line 7561
    return-void
.end method

.method public static tryDiscardPageCache(I)V
    .locals 7

    .prologue
    .line 7562
    sget-boolean v0, LX/02Q;->isKernelPageCacheFlushIsBroken:Z

    if-nez v0, :cond_0

    .line 7563
    const-wide/16 v2, 0x0

    const-wide/32 v4, 0x7fffffff

    const/4 v6, 0x4

    move v1, p0

    :try_start_0
    invoke-static/range {v1 .. v6}, Lcom/facebook/common/dextricks/DalvikInternals;->fadvise(IJJI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 7564
    :cond_0
    :goto_0
    return-void

    .line 7565
    :catch_0
    move-exception v0

    .line 7566
    const-string v1, "failed to fadvise"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/02P;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7567
    const/4 v0, 0x1

    sput-boolean v0, LX/02Q;->isKernelPageCacheFlushIsBroken:Z

    goto :goto_0
.end method
