.class public final LX/0Kz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Kx;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:LX/0L0;

.field public final c:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "LX/0KW;",
            ">;"
        }
    .end annotation
.end field

.field public final d:[[LX/0L4;

.field private final e:[I

.field public f:Z

.field public g:I

.field public h:I


# direct methods
.method public constructor <init>(III)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    .prologue
    .line 42493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42494
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Kz;->f:Z

    .line 42495
    const/4 v0, 0x1

    iput v0, p0, LX/0Kz;->g:I

    .line 42496
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LX/0Kz;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 42497
    new-array v0, p1, [[LX/0L4;

    iput-object v0, p0, LX/0Kz;->d:[[LX/0L4;

    .line 42498
    new-array v0, p1, [I

    iput-object v0, p0, LX/0Kz;->e:[I

    .line 42499
    new-instance v0, LX/0Ky;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/0Ky;-><init>(LX/0Kz;Landroid/os/Looper;)V

    iput-object v0, p0, LX/0Kz;->a:Landroid/os/Handler;

    .line 42500
    new-instance v0, LX/0L0;

    iget-object v1, p0, LX/0Kz;->a:Landroid/os/Handler;

    iget-boolean v2, p0, LX/0Kz;->f:Z

    iget-object v3, p0, LX/0Kz;->e:[I

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, LX/0L0;-><init>(Landroid/os/Handler;Z[III)V

    iput-object v0, p0, LX/0Kz;->b:LX/0L0;

    .line 42501
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 42492
    iget v0, p0, LX/0Kz;->g:I

    return v0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 42491
    iget-object v0, p0, LX/0Kz;->e:[I

    aget v0, v0, p1

    return v0
.end method

.method public final a(II)LX/0L4;
    .locals 1

    .prologue
    .line 42490
    iget-object v0, p0, LX/0Kz;->d:[[LX/0L4;

    aget-object v0, v0, p1

    aget-object v0, v0, p2

    return-object v0
.end method

.method public final a(J)V
    .locals 8

    .prologue
    .line 42482
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    .line 42483
    iput-wide p1, v0, LX/0L0;->u:J

    .line 42484
    iget-object v1, v0, LX/0L0;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 42485
    iget-object v1, v0, LX/0L0;->a:Landroid/os/Handler;

    const/4 v2, 0x6

    .line 42486
    const/16 v6, 0x20

    ushr-long v6, p1, v6

    long-to-int v6, v6

    move v3, v6

    .line 42487
    long-to-int v6, p1

    move v4, v6

    .line 42488
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 42489
    return-void
.end method

.method public final a(LX/0GS;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 42478
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    .line 42479
    iget v1, v0, LX/0L0;->s:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0L0;->s:I

    .line 42480
    iget-object v1, v0, LX/0L0;->a:Landroid/os/Handler;

    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-static {p1, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p0

    invoke-virtual {v1, v2, p2, v3, p0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 42481
    return-void
.end method

.method public final a(LX/0KW;)V
    .locals 1

    .prologue
    .line 42476
    iget-object v0, p0, LX/0Kz;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 42477
    return-void
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 42467
    iget-boolean v0, p0, LX/0Kz;->f:Z

    if-eq v0, p1, :cond_0

    .line 42468
    iput-boolean p1, p0, LX/0Kz;->f:Z

    .line 42469
    iget v0, p0, LX/0Kz;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Kz;->h:I

    .line 42470
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    const/4 v2, 0x0

    .line 42471
    iget-object v3, v0, LX/0L0;->a:Landroid/os/Handler;

    const/4 v4, 0x3

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v4, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 42472
    iget-object v0, p0, LX/0Kz;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0KW;

    .line 42473
    iget v2, p0, LX/0Kz;->g:I

    invoke-interface {v0, p1, v2}, LX/0KW;->a(ZI)V

    goto :goto_1

    .line 42474
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 42475
    goto :goto_0
.end method

.method public final varargs a([LX/0GT;)V
    .locals 2

    .prologue
    .line 42463
    iget-object v0, p0, LX/0Kz;->d:[[LX/0L4;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 42464
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    .line 42465
    iget-object v1, v0, LX/0L0;->a:Landroid/os/Handler;

    const/4 p0, 0x1

    invoke-virtual {v1, p0, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 42466
    return-void
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 42502
    iget-object v0, p0, LX/0Kz;->e:[I

    aget v0, v0, p1

    if-eq v0, p2, :cond_0

    .line 42503
    iget-object v0, p0, LX/0Kz;->e:[I

    aput p2, v0, p1

    .line 42504
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    .line 42505
    iget-object v1, v0, LX/0L0;->a:Landroid/os/Handler;

    const/16 p0, 0x8

    invoke-virtual {v1, p0, p1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 42506
    :cond_0
    return-void
.end method

.method public final b(J)V
    .locals 6

    .prologue
    .line 42434
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    .line 42435
    iget-object v1, v0, LX/0L0;->e:LX/0LA;

    const-wide/16 v3, 0x3e8

    mul-long/2addr v3, p1

    invoke-virtual {v1, v3, v4}, LX/0LA;->a(J)V

    .line 42436
    return-void
.end method

.method public final b(LX/0GS;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 42437
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    invoke-virtual {v0, p1, p2, p3}, LX/0L0;->b(LX/0GS;ILjava/lang/Object;)V

    .line 42438
    return-void
.end method

.method public final b(LX/0KW;)V
    .locals 1

    .prologue
    .line 42439
    iget-object v0, p0, LX/0Kz;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 42440
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 42441
    iget-boolean v0, p0, LX/0Kz;->f:Z

    return v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 42442
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    .line 42443
    iget-object v1, v0, LX/0L0;->a:Landroid/os/Handler;

    const/4 p0, 0x4

    invoke-virtual {v1, p0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 42444
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 42445
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    invoke-virtual {v0}, LX/0L0;->f()V

    .line 42446
    iget-object v0, p0, LX/0Kz;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 42447
    return-void
.end method

.method public final e()J
    .locals 6

    .prologue
    .line 42448
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    const-wide/16 v2, -0x1

    .line 42449
    iget-wide v4, v0, LX/0L0;->w:J

    cmp-long v4, v4, v2

    if-nez v4, :cond_0

    :goto_0
    move-wide v0, v2

    .line 42450
    return-wide v0

    :cond_0
    iget-wide v2, v0, LX/0L0;->w:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    goto :goto_0
.end method

.method public final f()J
    .locals 6

    .prologue
    .line 42451
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    .line 42452
    iget-object v2, v0, LX/0L0;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-lez v2, :cond_0

    iget-wide v2, v0, LX/0L0;->u:J

    :goto_0
    move-wide v0, v2

    .line 42453
    return-wide v0

    :cond_0
    iget-wide v2, v0, LX/0L0;->x:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    goto :goto_0
.end method

.method public final g()J
    .locals 6

    .prologue
    .line 42454
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    .line 42455
    iget-object v2, v0, LX/0L0;->e:LX/0LA;

    invoke-virtual {v2}, LX/0LA;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    move-wide v0, v2

    .line 42456
    return-wide v0
.end method

.method public final h()J
    .locals 6

    .prologue
    .line 42457
    iget-object v0, p0, LX/0Kz;->b:LX/0L0;

    const-wide/16 v2, -0x1

    .line 42458
    iget-wide v4, v0, LX/0L0;->y:J

    cmp-long v4, v4, v2

    if-nez v4, :cond_0

    :goto_0
    move-wide v0, v2

    .line 42459
    return-wide v0

    :cond_0
    iget-wide v2, v0, LX/0L0;->y:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    goto :goto_0
.end method

.method public final i()I
    .locals 10

    .prologue
    const-wide/16 v0, 0x64

    const-wide/16 v8, -0x1

    .line 42460
    invoke-virtual {p0}, LX/0Kz;->h()J

    move-result-wide v2

    .line 42461
    invoke-virtual {p0}, LX/0Kz;->e()J

    move-result-wide v4

    .line 42462
    cmp-long v6, v2, v8

    if-eqz v6, :cond_0

    cmp-long v6, v4, v8

    if-nez v6, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-nez v6, :cond_2

    :goto_1
    long-to-int v0, v0

    goto :goto_0

    :cond_2
    mul-long/2addr v0, v2

    div-long/2addr v0, v4

    goto :goto_1
.end method
