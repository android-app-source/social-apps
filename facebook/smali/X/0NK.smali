.class public final LX/0NK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0LS;


# direct methods
.method public constructor <init>(LX/0LS;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 49959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49960
    iput-object p1, p0, LX/0NK;->a:LX/0LS;

    .line 49961
    const-string v2, "application/eia-608"

    const/4 v3, -0x1

    const-wide/16 v4, -0x1

    move-object v6, v1

    invoke-static/range {v1 .. v6}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)LX/0L4;

    move-result-object v0

    invoke-interface {p1, v0}, LX/0LS;->a(LX/0L4;)V

    .line 49962
    return-void
.end method


# virtual methods
.method public final a(JLX/0Oj;)V
    .locals 11

    .prologue
    const/16 v8, 0xff

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 49963
    :goto_0
    invoke-virtual {p3}, LX/0Oj;->b()I

    move-result v0

    if-le v0, v4, :cond_4

    move v0, v6

    .line 49964
    :cond_0
    invoke-virtual {p3}, LX/0Oj;->f()I

    move-result v1

    .line 49965
    add-int/2addr v0, v1

    .line 49966
    if-eq v1, v8, :cond_0

    move v5, v6

    .line 49967
    :cond_1
    invoke-virtual {p3}, LX/0Oj;->f()I

    move-result v1

    .line 49968
    add-int/2addr v5, v1

    .line 49969
    if-eq v1, v8, :cond_1

    .line 49970
    const/4 v1, 0x0

    .line 49971
    const/4 v2, 0x4

    if-ne v0, v2, :cond_2

    const/16 v2, 0x8

    if-ge v5, v2, :cond_5

    .line 49972
    :cond_2
    :goto_1
    move v0, v1

    .line 49973
    if-eqz v0, :cond_3

    .line 49974
    iget-object v0, p0, LX/0NK;->a:LX/0LS;

    invoke-interface {v0, p3, v5}, LX/0LS;->a(LX/0Oj;I)V

    .line 49975
    iget-object v1, p0, LX/0NK;->a:LX/0LS;

    const/4 v7, 0x0

    move-wide v2, p1

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    goto :goto_0

    .line 49976
    :cond_3
    invoke-virtual {p3, v5}, LX/0Oj;->c(I)V

    goto :goto_0

    .line 49977
    :cond_4
    return-void

    .line 49978
    :cond_5
    iget v2, p3, LX/0Oj;->b:I

    move v2, v2

    .line 49979
    invoke-virtual {p3}, LX/0Oj;->f()I

    move-result v3

    .line 49980
    invoke-virtual {p3}, LX/0Oj;->g()I

    move-result v7

    .line 49981
    invoke-virtual {p3}, LX/0Oj;->m()I

    move-result v9

    .line 49982
    invoke-virtual {p3}, LX/0Oj;->f()I

    move-result v10

    .line 49983
    invoke-virtual {p3, v2}, LX/0Oj;->b(I)V

    .line 49984
    const/16 v2, 0xb5

    if-ne v3, v2, :cond_2

    const/16 v2, 0x31

    if-ne v7, v2, :cond_2

    const v2, 0x47413934

    if-ne v9, v2, :cond_2

    const/4 v2, 0x3

    if-ne v10, v2, :cond_2

    const/4 v1, 0x1

    goto :goto_1
.end method
