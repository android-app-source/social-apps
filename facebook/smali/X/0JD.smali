.class public final enum LX/0JD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JD;

.field public static final enum CHANNELFEED_VIDEO:LX/0JD;

.field public static final enum CHANNEL_INFO_OVERLAY:LX/0JD;

.field public static final enum CREATOR_SPACE_LIST:LX/0JD;

.field public static final enum FULLSCREEN_VIDEO:LX/0JD;

.field public static final enum GO_LIVE_BUTTON:LX/0JD;

.field public static final enum LIVE_TOIPICS_BUTON:LX/0JD;

.field public static final enum LIVE_VIDEO:LX/0JD;

.field public static final enum NEW_ITEMS_PILL:LX/0JD;

.field public static final enum SEARCH_BAR:LX/0JD;

.field public static final enum SEE_MORE_BUTTON:LX/0JD;

.field public static final enum SUBSCRIBE_BUTTON:LX/0JD;

.field public static final enum UNSUBSCRIBE_BUTTON:LX/0JD;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39276
    new-instance v0, LX/0JD;

    const-string v1, "GO_LIVE_BUTTON"

    const-string v2, "go_live_now_composer_open_button"

    invoke-direct {v0, v1, v4, v2}, LX/0JD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JD;->GO_LIVE_BUTTON:LX/0JD;

    .line 39277
    new-instance v0, LX/0JD;

    const-string v1, "LIVE_TOIPICS_BUTON"

    const-string v2, "live_topics_button"

    invoke-direct {v0, v1, v5, v2}, LX/0JD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JD;->LIVE_TOIPICS_BUTON:LX/0JD;

    .line 39278
    new-instance v0, LX/0JD;

    const-string v1, "NEW_ITEMS_PILL"

    const-string v2, "new_items_pill"

    invoke-direct {v0, v1, v6, v2}, LX/0JD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JD;->NEW_ITEMS_PILL:LX/0JD;

    .line 39279
    new-instance v0, LX/0JD;

    const-string v1, "FULLSCREEN_VIDEO"

    const-string v2, "fullscreen_video"

    invoke-direct {v0, v1, v7, v2}, LX/0JD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JD;->FULLSCREEN_VIDEO:LX/0JD;

    .line 39280
    new-instance v0, LX/0JD;

    const-string v1, "LIVE_VIDEO"

    const-string v2, "live_video"

    invoke-direct {v0, v1, v8, v2}, LX/0JD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JD;->LIVE_VIDEO:LX/0JD;

    .line 39281
    new-instance v0, LX/0JD;

    const-string v1, "SEE_MORE_BUTTON"

    const/4 v2, 0x5

    const-string v3, "see_more_button"

    invoke-direct {v0, v1, v2, v3}, LX/0JD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JD;->SEE_MORE_BUTTON:LX/0JD;

    .line 39282
    new-instance v0, LX/0JD;

    const-string v1, "SUBSCRIBE_BUTTON"

    const/4 v2, 0x6

    const-string v3, "subscribe_button"

    invoke-direct {v0, v1, v2, v3}, LX/0JD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JD;->SUBSCRIBE_BUTTON:LX/0JD;

    .line 39283
    new-instance v0, LX/0JD;

    const-string v1, "UNSUBSCRIBE_BUTTON"

    const/4 v2, 0x7

    const-string v3, "unsubscribe_button"

    invoke-direct {v0, v1, v2, v3}, LX/0JD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JD;->UNSUBSCRIBE_BUTTON:LX/0JD;

    .line 39284
    new-instance v0, LX/0JD;

    const-string v1, "CHANNELFEED_VIDEO"

    const/16 v2, 0x8

    const-string v3, "channelfeed_video"

    invoke-direct {v0, v1, v2, v3}, LX/0JD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JD;->CHANNELFEED_VIDEO:LX/0JD;

    .line 39285
    new-instance v0, LX/0JD;

    const-string v1, "CHANNEL_INFO_OVERLAY"

    const/16 v2, 0x9

    const-string v3, "channel_info_overlay"

    invoke-direct {v0, v1, v2, v3}, LX/0JD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JD;->CHANNEL_INFO_OVERLAY:LX/0JD;

    .line 39286
    new-instance v0, LX/0JD;

    const-string v1, "CREATOR_SPACE_LIST"

    const/16 v2, 0xa

    const-string v3, "creator_space_list"

    invoke-direct {v0, v1, v2, v3}, LX/0JD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JD;->CREATOR_SPACE_LIST:LX/0JD;

    .line 39287
    new-instance v0, LX/0JD;

    const-string v1, "SEARCH_BAR"

    const/16 v2, 0xb

    const-string v3, "search_bar"

    invoke-direct {v0, v1, v2, v3}, LX/0JD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JD;->SEARCH_BAR:LX/0JD;

    .line 39288
    const/16 v0, 0xc

    new-array v0, v0, [LX/0JD;

    sget-object v1, LX/0JD;->GO_LIVE_BUTTON:LX/0JD;

    aput-object v1, v0, v4

    sget-object v1, LX/0JD;->LIVE_TOIPICS_BUTON:LX/0JD;

    aput-object v1, v0, v5

    sget-object v1, LX/0JD;->NEW_ITEMS_PILL:LX/0JD;

    aput-object v1, v0, v6

    sget-object v1, LX/0JD;->FULLSCREEN_VIDEO:LX/0JD;

    aput-object v1, v0, v7

    sget-object v1, LX/0JD;->LIVE_VIDEO:LX/0JD;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0JD;->SEE_MORE_BUTTON:LX/0JD;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0JD;->SUBSCRIBE_BUTTON:LX/0JD;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0JD;->UNSUBSCRIBE_BUTTON:LX/0JD;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0JD;->CHANNELFEED_VIDEO:LX/0JD;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0JD;->CHANNEL_INFO_OVERLAY:LX/0JD;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0JD;->CREATOR_SPACE_LIST:LX/0JD;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0JD;->SEARCH_BAR:LX/0JD;

    aput-object v2, v0, v1

    sput-object v0, LX/0JD;->$VALUES:[LX/0JD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39289
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39290
    iput-object p3, p0, LX/0JD;->value:Ljava/lang/String;

    .line 39291
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JD;
    .locals 1

    .prologue
    .line 39292
    const-class v0, LX/0JD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JD;

    return-object v0
.end method

.method public static values()[LX/0JD;
    .locals 1

    .prologue
    .line 39293
    sget-object v0, LX/0JD;->$VALUES:[LX/0JD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JD;

    return-object v0
.end method
