.class public final LX/0Cs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/0D5;

.field public final synthetic c:Lcom/facebook/browser/lite/BrowserLiteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteFragment;ILX/0D5;)V
    .locals 0

    .prologue
    .line 28087
    iput-object p1, p0, LX/0Cs;->c:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iput p2, p0, LX/0Cs;->a:I

    iput-object p3, p0, LX/0Cs;->b:LX/0D5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x6416559f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 28082
    iget-object v1, p0, LX/0Cs;->c:Lcom/facebook/browser/lite/BrowserLiteFragment;

    const-string v2, "continue_reading"

    invoke-static {v1, v2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->e(Lcom/facebook/browser/lite/BrowserLiteFragment;Ljava/lang/String;)V

    .line 28083
    iget v1, p0, LX/0Cs;->a:I

    if-lez v1, :cond_0

    .line 28084
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(function(){var destination = document.getElementsByTagName(\'p\')["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/0Cs;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "].offsetTop;window.scrollTo(0, destination);})();"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 28085
    iget-object v2, p0, LX/0Cs;->b:LX/0D5;

    invoke-virtual {v2, v1}, LX/0D5;->a(Ljava/lang/String;)V

    .line 28086
    :cond_0
    const v1, 0x532c96c0

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
