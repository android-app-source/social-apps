.class public final LX/0Ma;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0MX;


# instance fields
.field private final a:[J

.field private final b:[J

.field private final c:J


# direct methods
.method private constructor <init>([J[JJ)V
    .locals 1

    .prologue
    .line 46549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46550
    iput-object p1, p0, LX/0Ma;->a:[J

    .line 46551
    iput-object p2, p0, LX/0Ma;->b:[J

    .line 46552
    iput-wide p3, p0, LX/0Ma;->c:J

    .line 46553
    return-void
.end method

.method public static a(LX/0Oe;LX/0Oj;JJ)LX/0Ma;
    .locals 18

    .prologue
    .line 46519
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0Oj;->c(I)V

    .line 46520
    invoke-virtual/range {p1 .. p1}, LX/0Oj;->m()I

    move-result v2

    .line 46521
    if-gtz v2, :cond_0

    .line 46522
    const/4 v2, 0x0

    .line 46523
    :goto_0
    return-object v2

    .line 46524
    :cond_0
    move-object/from16 v0, p0

    iget v6, v0, LX/0Oe;->d:I

    .line 46525
    int-to-long v2, v2

    const-wide/32 v8, 0xf4240

    const/16 v4, 0x7d00

    if-lt v6, v4, :cond_1

    const/16 v4, 0x480

    :goto_1
    int-to-long v4, v4

    mul-long/2addr v4, v8

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, LX/08x;->a(JJJ)J

    move-result-wide v8

    .line 46526
    invoke-virtual/range {p1 .. p1}, LX/0Oj;->g()I

    move-result v10

    .line 46527
    invoke-virtual/range {p1 .. p1}, LX/0Oj;->g()I

    move-result v11

    .line 46528
    invoke-virtual/range {p1 .. p1}, LX/0Oj;->g()I

    move-result v12

    .line 46529
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0Oj;->c(I)V

    .line 46530
    move-object/from16 v0, p0

    iget v2, v0, LX/0Oe;->c:I

    int-to-long v2, v2

    add-long v4, p2, v2

    .line 46531
    add-int/lit8 v2, v10, 0x1

    new-array v13, v2, [J

    .line 46532
    add-int/lit8 v2, v10, 0x1

    new-array v14, v2, [J

    .line 46533
    const/4 v2, 0x0

    const-wide/16 v6, 0x0

    aput-wide v6, v13, v2

    .line 46534
    const/4 v2, 0x0

    aput-wide v4, v14, v2

    .line 46535
    const/4 v2, 0x1

    :goto_2
    array-length v3, v13

    if-ge v2, v3, :cond_3

    .line 46536
    packed-switch v12, :pswitch_data_0

    .line 46537
    const/4 v2, 0x0

    goto :goto_0

    .line 46538
    :cond_1
    const/16 v4, 0x240

    goto :goto_1

    .line 46539
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, LX/0Oj;->f()I

    move-result v3

    .line 46540
    :goto_3
    mul-int/2addr v3, v11

    int-to-long v6, v3

    add-long/2addr v4, v6

    .line 46541
    int-to-long v6, v2

    mul-long/2addr v6, v8

    int-to-long v0, v10

    move-wide/from16 v16, v0

    div-long v6, v6, v16

    aput-wide v6, v13, v2

    .line 46542
    const-wide/16 v6, -0x1

    cmp-long v3, p4, v6

    if-nez v3, :cond_2

    move-wide v6, v4

    :goto_4
    aput-wide v6, v14, v2

    .line 46543
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 46544
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, LX/0Oj;->g()I

    move-result v3

    goto :goto_3

    .line 46545
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, LX/0Oj;->j()I

    move-result v3

    goto :goto_3

    .line 46546
    :pswitch_3
    invoke-virtual/range {p1 .. p1}, LX/0Oj;->s()I

    move-result v3

    goto :goto_3

    .line 46547
    :cond_2
    move-wide/from16 v0, p4

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    goto :goto_4

    .line 46548
    :cond_3
    new-instance v2, LX/0Ma;

    invoke-direct {v2, v13, v14, v8, v9}, LX/0Ma;-><init>([J[JJ)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(J)J
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 46518
    iget-object v0, p0, LX/0Ma;->a:[J

    iget-object v1, p0, LX/0Ma;->b:[J

    invoke-static {v1, p1, p2, v2, v2}, LX/08x;->a([JJZZ)I

    move-result v1

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 46517
    const/4 v0, 0x1

    return v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 46516
    iget-wide v0, p0, LX/0Ma;->c:J

    return-wide v0
.end method

.method public final b(J)J
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 46515
    iget-object v0, p0, LX/0Ma;->b:[J

    iget-object v1, p0, LX/0Ma;->a:[J

    invoke-static {v1, p1, p2, v2, v2}, LX/08x;->a([JJZZ)I

    move-result v1

    aget-wide v0, v0, v1

    return-wide v0
.end method
