.class public final LX/0MZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:I

.field public static final b:[Ljava/nio/charset/Charset;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 46384
    const-string v0, "ID3"

    invoke-static {v0}, LX/08x;->e(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/0MZ;->a:I

    .line 46385
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/nio/charset/Charset;

    const/4 v1, 0x0

    const-string v2, "ISO-8859-1"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "UTF-16LE"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "UTF-16BE"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, LX/0MZ;->b:[Ljava/nio/charset/Charset;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 46386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0MA;)LX/0ML;
    .locals 12

    .prologue
    const/16 v8, 0xa

    const/4 v2, 0x0

    .line 46363
    new-instance v3, LX/0Oj;

    invoke-direct {v3, v8}, LX/0Oj;-><init>(I)V

    .line 46364
    const/4 v0, 0x0

    move v1, v2

    .line 46365
    :goto_0
    iget-object v4, v3, LX/0Oj;->a:[B

    invoke-interface {p0, v4, v2, v8}, LX/0MA;->c([BII)V

    .line 46366
    invoke-virtual {v3, v2}, LX/0Oj;->b(I)V

    .line 46367
    invoke-virtual {v3}, LX/0Oj;->j()I

    move-result v4

    sget v5, LX/0MZ;->a:I

    if-ne v4, v5, :cond_4

    .line 46368
    invoke-virtual {v3}, LX/0Oj;->f()I

    move-result v4

    .line 46369
    invoke-virtual {v3}, LX/0Oj;->f()I

    move-result v5

    .line 46370
    invoke-virtual {v3}, LX/0Oj;->f()I

    move-result v6

    .line 46371
    invoke-virtual {v3}, LX/0Oj;->r()I

    move-result v7

    .line 46372
    if-nez v0, :cond_3

    const/4 v11, 0x4

    const/4 v10, 0x2

    .line 46373
    const/16 v9, 0xff

    if-eq v5, v9, :cond_5

    if-lt v4, v10, :cond_5

    if-gt v4, v11, :cond_5

    const/high16 v9, 0x300000

    if-gt v7, v9, :cond_5

    if-ne v4, v10, :cond_0

    and-int/lit8 v9, v6, 0x3f

    if-nez v9, :cond_5

    and-int/lit8 v9, v6, 0x40

    if-nez v9, :cond_5

    :cond_0
    const/4 v9, 0x3

    if-ne v4, v9, :cond_1

    and-int/lit8 v9, v6, 0x1f

    if-nez v9, :cond_5

    :cond_1
    if-ne v4, v11, :cond_2

    and-int/lit8 v9, v6, 0xf

    if-nez v9, :cond_5

    :cond_2
    const/4 v9, 0x1

    :goto_1
    move v5, v9

    .line 46374
    if-eqz v5, :cond_3

    .line 46375
    new-array v0, v7, [B

    .line 46376
    invoke-interface {p0, v0, v2, v7}, LX/0MA;->c([BII)V

    .line 46377
    new-instance v5, LX/0Oj;

    invoke-direct {v5, v0}, LX/0Oj;-><init>([B)V

    invoke-static {v5, v4, v6}, LX/0MZ;->a(LX/0Oj;II)LX/0ML;

    move-result-object v0

    .line 46378
    :goto_2
    add-int/lit8 v4, v7, 0xa

    add-int/2addr v1, v4

    .line 46379
    goto :goto_0

    .line 46380
    :cond_3
    invoke-interface {p0, v7}, LX/0MA;->c(I)V

    goto :goto_2

    .line 46381
    :cond_4
    invoke-interface {p0}, LX/0MA;->a()V

    .line 46382
    invoke-interface {p0, v1}, LX/0MA;->c(I)V

    .line 46383
    return-object v0

    :cond_5
    const/4 v9, 0x0

    goto :goto_1
.end method

.method private static a(LX/0Oj;II)LX/0ML;
    .locals 12

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x3

    const/4 v3, 0x4

    const/4 v1, 0x0

    .line 46289
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 46290
    const/4 v6, 0x4

    if-eq p1, v6, :cond_c

    .line 46291
    and-int/lit16 v6, p2, 0x80

    if-eqz v6, :cond_2

    .line 46292
    iget-object v7, p0, LX/0Oj;->a:[B

    .line 46293
    array-length v6, v7

    move v11, v0

    move v0, v6

    move v6, v11

    .line 46294
    :goto_0
    add-int/lit8 v8, v6, 0x1

    if-ge v8, v0, :cond_1

    .line 46295
    aget-byte v8, v7, v6

    and-int/lit16 v8, v8, 0xff

    const/16 v9, 0xff

    if-ne v8, v9, :cond_0

    add-int/lit8 v8, v6, 0x1

    aget-byte v8, v7, v8

    if-nez v8, :cond_0

    .line 46296
    add-int/lit8 v8, v6, 0x2

    add-int/lit8 v9, v6, 0x1

    sub-int v10, v0, v6

    add-int/lit8 v10, v10, -0x2

    invoke-static {v7, v8, v7, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 46297
    add-int/lit8 v0, v0, -0x1

    .line 46298
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 46299
    :cond_1
    invoke-virtual {p0, v0}, LX/0Oj;->a(I)V

    .line 46300
    :cond_2
    :goto_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 46301
    if-ne p1, v4, :cond_7

    and-int/lit8 v0, p2, 0x40

    if-eqz v0, :cond_7

    .line 46302
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v0

    if-ge v0, v3, :cond_3

    move-object v0, v1

    .line 46303
    :goto_2
    return-object v0

    .line 46304
    :cond_3
    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v0

    .line 46305
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v2

    if-le v0, v2, :cond_4

    move-object v0, v1

    .line 46306
    goto :goto_2

    .line 46307
    :cond_4
    if-lt v0, v5, :cond_5

    .line 46308
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, LX/0Oj;->c(I)V

    .line 46309
    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v2

    .line 46310
    invoke-virtual {p0, v3}, LX/0Oj;->b(I)V

    .line 46311
    iget v3, p0, LX/0Oj;->c:I

    move v3, v3

    .line 46312
    sub-int v2, v3, v2

    invoke-virtual {p0, v2}, LX/0Oj;->a(I)V

    .line 46313
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v2

    if-ge v2, v0, :cond_5

    move-object v0, v1

    .line 46314
    goto :goto_2

    .line 46315
    :cond_5
    invoke-virtual {p0, v0}, LX/0Oj;->c(I)V

    .line 46316
    :cond_6
    :goto_3
    const/4 v10, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v9, 0x4

    const/4 v2, 0x0

    .line 46317
    :goto_4
    if-ne p1, v10, :cond_13

    .line 46318
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v0

    const/4 v3, 0x6

    if-ge v0, v3, :cond_e

    move-object v0, v2

    .line 46319
    :goto_5
    move-object v2, v0

    .line 46320
    if-eqz v2, :cond_b

    .line 46321
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v4, :cond_6

    .line 46322
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v3, v0}, LX/0ML;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ML;

    move-result-object v0

    .line 46323
    if-eqz v0, :cond_6

    goto :goto_2

    .line 46324
    :cond_7
    if-ne p1, v3, :cond_6

    and-int/lit8 v0, p2, 0x40

    if-eqz v0, :cond_6

    .line 46325
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v0

    if-ge v0, v3, :cond_8

    move-object v0, v1

    .line 46326
    goto :goto_2

    .line 46327
    :cond_8
    invoke-virtual {p0}, LX/0Oj;->r()I

    move-result v0

    .line 46328
    if-lt v0, v5, :cond_9

    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    if-le v0, v2, :cond_a

    :cond_9
    move-object v0, v1

    .line 46329
    goto :goto_2

    .line 46330
    :cond_a
    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    goto :goto_3

    :cond_b
    move-object v0, v1

    .line 46331
    goto :goto_2

    .line 46332
    :cond_c
    invoke-static {p0, v0}, LX/0MZ;->a(LX/0Oj;Z)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 46333
    invoke-static {p0, v0}, LX/0MZ;->b(LX/0Oj;Z)V

    goto/16 :goto_1

    .line 46334
    :cond_d
    invoke-static {p0, v2}, LX/0MZ;->a(LX/0Oj;Z)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 46335
    invoke-static {p0, v2}, LX/0MZ;->b(LX/0Oj;Z)V

    goto/16 :goto_1

    .line 46336
    :cond_e
    const/4 v0, 0x3

    const-string v3, "US-ASCII"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, LX/0Oj;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v3

    .line 46337
    const-string v0, "\u0000\u0000\u0000"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    move-object v0, v2

    .line 46338
    goto :goto_5

    .line 46339
    :cond_f
    invoke-virtual {p0}, LX/0Oj;->j()I

    move-result v0

    .line 46340
    if-eqz v0, :cond_10

    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v7

    if-le v0, v7, :cond_11

    :cond_10
    move-object v0, v2

    .line 46341
    goto :goto_5

    .line 46342
    :cond_11
    const-string v7, "COM"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    .line 46343
    :cond_12
    invoke-virtual {p0, v0}, LX/0Oj;->c(I)V

    goto/16 :goto_4

    .line 46344
    :cond_13
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v0

    const/16 v3, 0xa

    if-ge v0, v3, :cond_14

    move-object v0, v2

    .line 46345
    goto/16 :goto_5

    .line 46346
    :cond_14
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {p0, v9, v0}, LX/0Oj;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v7

    .line 46347
    const-string v0, "\u0000\u0000\u0000\u0000"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    move-object v0, v2

    .line 46348
    goto/16 :goto_5

    .line 46349
    :cond_15
    if-ne p1, v9, :cond_17

    invoke-virtual {p0}, LX/0Oj;->r()I

    move-result v0

    .line 46350
    :goto_6
    if-eqz v0, :cond_16

    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    if-le v0, v3, :cond_18

    :cond_16
    move-object v0, v2

    .line 46351
    goto/16 :goto_5

    .line 46352
    :cond_17
    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v0

    goto :goto_6

    .line 46353
    :cond_18
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v3

    .line 46354
    if-ne p1, v9, :cond_19

    and-int/lit8 v8, v3, 0xc

    if-nez v8, :cond_1a

    :cond_19
    const/4 v8, 0x3

    if-ne p1, v8, :cond_1d

    and-int/lit16 v3, v3, 0xc0

    if-eqz v3, :cond_1d

    :cond_1a
    move v3, v5

    .line 46355
    :goto_7
    if-nez v3, :cond_12

    const-string v3, "COMM"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 46356
    :cond_1b
    invoke-virtual {p0}, LX/0Oj;->f()I

    move-result v3

    .line 46357
    if-ltz v3, :cond_1c

    sget-object v7, LX/0MZ;->b:[Ljava/nio/charset/Charset;

    array-length v7, v7

    if-lt v3, v7, :cond_1e

    :cond_1c
    move-object v0, v2

    .line 46358
    goto/16 :goto_5

    :cond_1d
    move v3, v6

    .line 46359
    goto :goto_7

    .line 46360
    :cond_1e
    sget-object v7, LX/0MZ;->b:[Ljava/nio/charset/Charset;

    aget-object v3, v7, v3

    .line 46361
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0, v3}, LX/0Oj;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "\u0000"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 46362
    array-length v3, v0

    if-ne v3, v10, :cond_1f

    aget-object v2, v0, v6

    aget-object v0, v0, v5

    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_5

    :cond_1f
    move-object v0, v2

    goto/16 :goto_5
.end method

.method public static a(LX/0Oj;Z)Z
    .locals 12

    .prologue
    const/4 v2, 0x1

    const-wide/16 v10, 0x7f

    const/4 v3, 0x0

    .line 46234
    invoke-virtual {p0, v3}, LX/0Oj;->b(I)V

    .line 46235
    :goto_0
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_5

    .line 46236
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 46237
    :goto_1
    return v0

    .line 46238
    :cond_0
    invoke-virtual {p0}, LX/0Oj;->k()J

    move-result-wide v0

    .line 46239
    if-nez p1, :cond_2

    .line 46240
    const-wide/32 v4, 0x808080

    and-long/2addr v4, v0

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    move v0, v3

    .line 46241
    goto :goto_1

    .line 46242
    :cond_1
    and-long v4, v0, v10

    const/16 v6, 0x8

    shr-long v6, v0, v6

    and-long/2addr v6, v10

    const/4 v8, 0x7

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/16 v6, 0x10

    shr-long v6, v0, v6

    and-long/2addr v6, v10

    const/16 v8, 0xe

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/16 v6, 0x18

    shr-long/2addr v0, v6

    and-long/2addr v0, v10

    const/16 v6, 0x15

    shl-long/2addr v0, v6

    or-long/2addr v0, v4

    .line 46243
    :cond_2
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    int-to-long v4, v4

    cmp-long v4, v0, v4

    if-lez v4, :cond_3

    move v0, v3

    .line 46244
    goto :goto_1

    .line 46245
    :cond_3
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v4

    .line 46246
    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_4

    .line 46247
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v4

    const/4 v5, 0x4

    if-ge v4, v5, :cond_4

    move v0, v3

    .line 46248
    goto :goto_1

    .line 46249
    :cond_4
    long-to-int v0, v0

    invoke-virtual {p0, v0}, LX/0Oj;->c(I)V

    goto :goto_0

    :cond_5
    move v0, v2

    .line 46250
    goto :goto_1
.end method

.method public static b(LX/0Oj;Z)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 46251
    invoke-virtual {p0, v4}, LX/0Oj;->b(I)V

    .line 46252
    iget-object v9, p0, LX/0Oj;->a:[B

    .line 46253
    :goto_0
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 46254
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    if-nez v0, :cond_1

    .line 46255
    :cond_0
    return-void

    .line 46256
    :cond_1
    if-eqz p1, :cond_3

    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v0

    .line 46257
    :goto_1
    invoke-virtual {p0}, LX/0Oj;->g()I

    move-result v8

    .line 46258
    and-int/lit8 v1, v8, 0x1

    if-eqz v1, :cond_8

    .line 46259
    iget v1, p0, LX/0Oj;->b:I

    move v1, v1

    .line 46260
    add-int/lit8 v2, v1, 0x4

    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-static {v9, v2, v9, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 46261
    add-int/lit8 v1, v0, -0x4

    .line 46262
    and-int/lit8 v2, v8, -0x2

    .line 46263
    iget v0, p0, LX/0Oj;->c:I

    move v0, v0

    .line 46264
    add-int/lit8 v0, v0, -0x4

    invoke-virtual {p0, v0}, LX/0Oj;->a(I)V

    .line 46265
    :goto_2
    and-int/lit8 v0, v2, 0x2

    if-eqz v0, :cond_7

    .line 46266
    iget v0, p0, LX/0Oj;->b:I

    move v0, v0

    .line 46267
    add-int/lit8 v0, v0, 0x1

    move v3, v4

    move v5, v0

    .line 46268
    :goto_3
    add-int/lit8 v6, v3, 0x1

    if-ge v6, v1, :cond_4

    .line 46269
    add-int/lit8 v6, v0, -0x1

    aget-byte v6, v9, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0xff

    if-ne v6, v7, :cond_2

    aget-byte v6, v9, v0

    if-nez v6, :cond_2

    .line 46270
    add-int/lit8 v0, v0, 0x1

    .line 46271
    add-int/lit8 v1, v1, -0x1

    .line 46272
    :cond_2
    add-int/lit8 v6, v5, 0x1

    add-int/lit8 v7, v0, 0x1

    aget-byte v0, v9, v0

    aput-byte v0, v9, v5

    .line 46273
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v5, v6

    move v0, v7

    goto :goto_3

    .line 46274
    :cond_3
    invoke-virtual {p0}, LX/0Oj;->r()I

    move-result v0

    goto :goto_1

    .line 46275
    :cond_4
    iget v3, p0, LX/0Oj;->c:I

    move v3, v3

    .line 46276
    sub-int v6, v0, v5

    sub-int/2addr v3, v6

    invoke-virtual {p0, v3}, LX/0Oj;->a(I)V

    .line 46277
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v3

    sub-int/2addr v3, v0

    invoke-static {v9, v0, v9, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 46278
    and-int/lit8 v0, v2, -0x3

    .line 46279
    :goto_4
    if-ne v0, v8, :cond_5

    if-eqz p1, :cond_6

    .line 46280
    :cond_5
    iget v2, p0, LX/0Oj;->b:I

    move v2, v2

    .line 46281
    add-int/lit8 v2, v2, -0x6

    .line 46282
    shr-int/lit8 v3, v1, 0x15

    and-int/lit8 v3, v3, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v9, v2

    .line 46283
    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v5, v1, 0xe

    and-int/lit8 v5, v5, 0x7f

    int-to-byte v5, v5

    aput-byte v5, v9, v3

    .line 46284
    add-int/lit8 v3, v2, 0x2

    shr-int/lit8 v5, v1, 0x7

    and-int/lit8 v5, v5, 0x7f

    int-to-byte v5, v5

    aput-byte v5, v9, v3

    .line 46285
    add-int/lit8 v3, v2, 0x3

    and-int/lit8 v5, v1, 0x7f

    int-to-byte v5, v5

    aput-byte v5, v9, v3

    .line 46286
    add-int/lit8 v3, v2, 0x4

    shr-int/lit8 v5, v0, 0x8

    int-to-byte v5, v5

    aput-byte v5, v9, v3

    .line 46287
    add-int/lit8 v2, v2, 0x5

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, v9, v2

    .line 46288
    :cond_6
    invoke-virtual {p0, v1}, LX/0Oj;->c(I)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_4

    :cond_8
    move v2, v8

    move v1, v0

    goto/16 :goto_2
.end method
