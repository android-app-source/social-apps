.class public LX/04f;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field private a:Ljava/io/OutputStream;

.field private b:I


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 0

    .prologue
    .line 13994
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 13995
    iput-object p1, p0, LX/04f;->a:Ljava/io/OutputStream;

    .line 13996
    iput p2, p0, LX/04f;->b:I

    .line 13997
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 13975
    iget-object v0, p0, LX/04f;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 13976
    return-void
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 13977
    iget-object v0, p0, LX/04f;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 13978
    return-void
.end method

.method public final write(I)V
    .locals 5

    .prologue
    const/16 v4, 0x18

    const/16 v3, 0x8

    .line 13979
    const/16 v0, 0x17

    iget v1, p0, LX/04f;->b:I

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 13980
    :try_start_0
    iget-object v0, p0, LX/04f;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13981
    iget v0, p0, LX/04f;->b:I

    invoke-static {v3, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 13982
    return-void

    .line 13983
    :catchall_0
    move-exception v0

    iget v2, p0, LX/04f;->b:I

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0
.end method

.method public final write([B)V
    .locals 5

    .prologue
    const/16 v4, 0x18

    const/4 v3, 0x1

    .line 13984
    const/16 v0, 0x8

    const/16 v1, 0x17

    iget v2, p0, LX/04f;->b:I

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 13985
    :try_start_0
    iget-object v0, p0, LX/04f;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13986
    iget v0, p0, LX/04f;->b:I

    invoke-static {v3, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 13987
    return-void

    .line 13988
    :catchall_0
    move-exception v0

    iget v2, p0, LX/04f;->b:I

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0
.end method

.method public final write([BII)V
    .locals 5

    .prologue
    const/16 v4, 0x18

    const/16 v3, 0x8

    .line 13989
    const/16 v0, 0x17

    iget v1, p0, LX/04f;->b:I

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 13990
    :try_start_0
    iget-object v0, p0, LX/04f;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13991
    iget v0, p0, LX/04f;->b:I

    invoke-static {v3, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 13992
    return-void

    .line 13993
    :catchall_0
    move-exception v0

    iget v2, p0, LX/04f;->b:I

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0
.end method
