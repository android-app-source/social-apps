.class public final LX/0CH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public final synthetic a:LX/0CQ;


# direct methods
.method public constructor <init>(LX/0CQ;)V
    .locals 0

    .prologue
    .line 27135
    iput-object p1, p0, LX/0CH;->a:LX/0CQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 27136
    iget-object v0, p0, LX/0CH;->a:LX/0CQ;

    .line 27137
    if-nez p2, :cond_0

    .line 27138
    const/4 v1, 0x0

    .line 27139
    :goto_0
    move-object v1, v1

    .line 27140
    iput-object v1, v0, LX/0CQ;->d:LX/0DT;

    .line 27141
    invoke-static {}, LX/0Dm;->a()LX/0Dm;

    move-result-object v0

    iget-object v1, p0, LX/0CH;->a:LX/0CQ;

    invoke-virtual {v1}, LX/0CQ;->d()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Dm;->a(Ljava/util/HashSet;)V

    .line 27142
    return-void

    .line 27143
    :cond_0
    const-string v1, "com.facebook.browser.lite.ipc.BrowserLiteCallback"

    invoke-interface {p2, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    .line 27144
    if-eqz v1, :cond_1

    instance-of p1, v1, LX/0DT;

    if-eqz p1, :cond_1

    .line 27145
    check-cast v1, LX/0DT;

    goto :goto_0

    .line 27146
    :cond_1
    new-instance v1, LX/0DU;

    invoke-direct {v1, p2}, LX/0DU;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 27147
    iget-object v0, p0, LX/0CH;->a:LX/0CQ;

    const/4 v1, 0x0

    .line 27148
    iput-object v1, v0, LX/0CQ;->d:LX/0DT;

    .line 27149
    return-void
.end method
