.class public LX/0K1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/0AZ;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:LX/0TD;

.field private final e:LX/0Sh;


# direct methods
.method public constructor <init>(LX/0AZ;Ljava/lang/String;Ljava/lang/String;LX/0TD;LX/0Sh;)V
    .locals 0

    .prologue
    .line 40080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40081
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40082
    iput-object p1, p0, LX/0K1;->a:LX/0AZ;

    .line 40083
    iput-object p2, p0, LX/0K1;->c:Ljava/lang/String;

    .line 40084
    iput-object p3, p0, LX/0K1;->b:Ljava/lang/String;

    .line 40085
    iput-object p4, p0, LX/0K1;->d:LX/0TD;

    .line 40086
    iput-object p5, p0, LX/0K1;->e:LX/0Sh;

    .line 40087
    return-void
.end method

.method public static a$redex0(LX/0K1;LX/0GJ;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0GJ",
            "<",
            "LX/0AY;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 40088
    const-string v0, "ManifestContentFetcher.startParsing"

    const v1, -0x34e7019e    # -1.0026594E7f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 40089
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 40090
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40091
    :try_start_1
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, LX/0K1;->b:Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 40092
    iget-object v1, p0, LX/0K1;->a:LX/0AZ;

    iget-object v2, p0, LX/0K1;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0AZ;->a(Ljava/lang/String;Ljava/io/InputStream;)LX/0AY;

    move-result-object v0

    .line 40093
    if-eqz p2, :cond_1

    .line 40094
    iget-object v1, p0, LX/0K1;->e:LX/0Sh;

    new-instance v2, Lcom/facebook/video/view/exo/ManifestContentFetcher$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/facebook/video/view/exo/ManifestContentFetcher$1;-><init>(LX/0K1;LX/0GJ;LX/0AY;)V

    invoke-virtual {v1, v2}, LX/0Sh;->b(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 40095
    :goto_1
    const v0, -0x28d2fd6c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 40096
    return-void

    .line 40097
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 40098
    :cond_1
    :try_start_2
    invoke-interface {p1, v0}, LX/0GJ;->a(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 40099
    :catch_0
    move-exception v0

    .line 40100
    if-eqz p2, :cond_2

    .line 40101
    :try_start_3
    iget-object v1, p0, LX/0K1;->e:LX/0Sh;

    new-instance v2, Lcom/facebook/video/view/exo/ManifestContentFetcher$2;

    invoke-direct {v2, p0, p1, v0}, Lcom/facebook/video/view/exo/ManifestContentFetcher$2;-><init>(LX/0K1;LX/0GJ;Ljava/io/IOException;)V

    invoke-virtual {v1, v2}, LX/0Sh;->b(Ljava/lang/Runnable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 40102
    :catchall_0
    move-exception v0

    const v1, 0x73148e5d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 40103
    :cond_2
    :try_start_4
    invoke-interface {p1, v0}, LX/0GJ;->b(Ljava/io/IOException;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0GJ;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0GJ",
            "<",
            "LX/0AY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40104
    iget-object v0, p0, LX/0K1;->d:LX/0TD;

    new-instance v1, Lcom/facebook/video/view/exo/ManifestContentFetcher$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/view/exo/ManifestContentFetcher$3;-><init>(LX/0K1;LX/0GJ;)V

    const v2, 0x6abc34c2

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 40105
    return-void
.end method
