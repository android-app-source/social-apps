.class public final LX/0KT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0GP;


# instance fields
.field public final synthetic a:Lcom/facebook/video/vps/VideoPlayerService;

.field private b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;


# direct methods
.method public constructor <init>(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 0

    .prologue
    .line 41388
    iput-object p1, p0, LX/0KT;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41389
    iput-object p2, p0, LX/0KT;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 41390
    return-void
.end method


# virtual methods
.method public final a(LX/0Jw;LX/0GX;JILjava/lang/String;)V
    .locals 7

    .prologue
    .line 41391
    :try_start_0
    iget-object v0, p0, LX/0KT;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 41392
    const/4 v2, 0x1

    .line 41393
    sget-object v1, LX/040;->S:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 41394
    sget-object v1, LX/040;->S:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    .line 41395
    :goto_0
    move v0, v1

    .line 41396
    if-eqz v0, :cond_0

    .line 41397
    iget-object v0, p0, LX/0KT;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0KT;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 41398
    invoke-static {v0, v1, v2}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/lang/Long;)V

    .line 41399
    :cond_0
    iget-object v0, p0, LX/0KT;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0KT;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    const/4 v4, 0x0

    new-instance v5, Lcom/facebook/exoplayer/ipc/RendererContext;

    sget-object v2, LX/0HE;->DASH_LIVE:LX/0HE;

    invoke-virtual {v2}, LX/0HE;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v5, v2, v3, p5, p6}, Lcom/facebook/exoplayer/ipc/RendererContext;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    move-object v2, p1

    move-object v3, p2

    .line 41400
    invoke-static/range {v0 .. v5}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0GT;LX/0GT;ZLcom/facebook/exoplayer/ipc/RendererContext;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41401
    :goto_1
    return-void

    .line 41402
    :catch_0
    move-exception v0

    .line 41403
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Caught exception when building dash liverenderers: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/0KT;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_1

    .line 41404
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 41405
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 41406
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onError: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41407
    iget-object v0, p0, LX/0KT;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, LX/0KT;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 41408
    invoke-static {v0, p1, p2, v1}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Ljava/lang/Throwable;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41409
    return-void
.end method
