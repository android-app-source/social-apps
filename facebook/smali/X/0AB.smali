.class public final enum LX/0AB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0AB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0AB;

.field public static final enum LAUNCH_SERVER:LX/0AB;

.field public static final enum SHUTDOWN_SERVER:LX/0AB;

.field public static final enum VIDEO_CACHE_REQUEST_ABORTED:LX/0AB;

.field public static final enum VIDEO_CACHE_REQUEST_FAILED:LX/0AB;

.field public static final enum VIDEO_CACHE_REQUEST_FINISHED:LX/0AB;

.field public static final enum VIDEO_ENCODINGS:LX/0AB;

.field public static final enum VIDEO_LIVE_DATA_USAGE:LX/0AB;

.field public static final enum VIDEO_PREFETCH:LX/0AB;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 23579
    new-instance v0, LX/0AB;

    const-string v1, "LAUNCH_SERVER"

    const-string v2, "launch_video_server"

    invoke-direct {v0, v1, v4, v2}, LX/0AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AB;->LAUNCH_SERVER:LX/0AB;

    .line 23580
    new-instance v0, LX/0AB;

    const-string v1, "SHUTDOWN_SERVER"

    const-string v2, "shutdown_video_server"

    invoke-direct {v0, v1, v5, v2}, LX/0AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AB;->SHUTDOWN_SERVER:LX/0AB;

    .line 23581
    new-instance v0, LX/0AB;

    const-string v1, "VIDEO_CACHE_REQUEST_FINISHED"

    const-string v2, "video_cache_request_finished"

    invoke-direct {v0, v1, v6, v2}, LX/0AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AB;->VIDEO_CACHE_REQUEST_FINISHED:LX/0AB;

    .line 23582
    new-instance v0, LX/0AB;

    const-string v1, "VIDEO_CACHE_REQUEST_ABORTED"

    const-string v2, "video_cache_request_aborted"

    invoke-direct {v0, v1, v7, v2}, LX/0AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AB;->VIDEO_CACHE_REQUEST_ABORTED:LX/0AB;

    .line 23583
    new-instance v0, LX/0AB;

    const-string v1, "VIDEO_CACHE_REQUEST_FAILED"

    const-string v2, "video_cache_request_failed"

    invoke-direct {v0, v1, v8, v2}, LX/0AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AB;->VIDEO_CACHE_REQUEST_FAILED:LX/0AB;

    .line 23584
    new-instance v0, LX/0AB;

    const-string v1, "VIDEO_PREFETCH"

    const/4 v2, 0x5

    const-string v3, "video_prefetch"

    invoke-direct {v0, v1, v2, v3}, LX/0AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AB;->VIDEO_PREFETCH:LX/0AB;

    .line 23585
    new-instance v0, LX/0AB;

    const-string v1, "VIDEO_ENCODINGS"

    const/4 v2, 0x6

    const-string v3, "video_encodings"

    invoke-direct {v0, v1, v2, v3}, LX/0AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AB;->VIDEO_ENCODINGS:LX/0AB;

    .line 23586
    new-instance v0, LX/0AB;

    const-string v1, "VIDEO_LIVE_DATA_USAGE"

    const/4 v2, 0x7

    const-string v3, "video_live_data_usage"

    invoke-direct {v0, v1, v2, v3}, LX/0AB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0AB;->VIDEO_LIVE_DATA_USAGE:LX/0AB;

    .line 23587
    const/16 v0, 0x8

    new-array v0, v0, [LX/0AB;

    sget-object v1, LX/0AB;->LAUNCH_SERVER:LX/0AB;

    aput-object v1, v0, v4

    sget-object v1, LX/0AB;->SHUTDOWN_SERVER:LX/0AB;

    aput-object v1, v0, v5

    sget-object v1, LX/0AB;->VIDEO_CACHE_REQUEST_FINISHED:LX/0AB;

    aput-object v1, v0, v6

    sget-object v1, LX/0AB;->VIDEO_CACHE_REQUEST_ABORTED:LX/0AB;

    aput-object v1, v0, v7

    sget-object v1, LX/0AB;->VIDEO_CACHE_REQUEST_FAILED:LX/0AB;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0AB;->VIDEO_PREFETCH:LX/0AB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0AB;->VIDEO_ENCODINGS:LX/0AB;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0AB;->VIDEO_LIVE_DATA_USAGE:LX/0AB;

    aput-object v2, v0, v1

    sput-object v0, LX/0AB;->$VALUES:[LX/0AB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23588
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23589
    iput-object p3, p0, LX/0AB;->value:Ljava/lang/String;

    .line 23590
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0AB;
    .locals 1

    .prologue
    .line 23591
    const-class v0, LX/0AB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0AB;

    return-object v0
.end method

.method public static values()[LX/0AB;
    .locals 1

    .prologue
    .line 23592
    sget-object v0, LX/0AB;->$VALUES:[LX/0AB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0AB;

    return-object v0
.end method
