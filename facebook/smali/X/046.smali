.class public final enum LX/046;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/046;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/046;

.field public static final enum DISABLED_BY_360_AUTOPLAY_SENSOR_REQUIREMENT:LX/046;

.field public static final enum DISABLED_BY_ACCESSIBILITY:LX/046;

.field public static final enum DISABLED_BY_ALREADY_SEEN:LX/046;

.field public static final enum DISABLED_BY_AUTOPLAY_SETTING:LX/046;

.field public static final enum DISABLED_BY_CACHE_NOT_READY:LX/046;

.field public static final enum DISABLED_BY_CONNECTION:LX/046;

.field public static final enum DISABLED_BY_DATA_SAVINGS_MODE:LX/046;

.field public static final enum DISABLED_BY_LOW_BATTERY:LX/046;

.field public static final enum DISABLED_BY_METERED_NETWORK:LX/046;

.field public static final enum DISABLED_BY_PLAYER_ERROR_STATE:LX/046;

.field public static final enum DISABLED_BY_POWER_SAVING:LX/046;

.field public static final enum DISABLED_BY_SERVER:LX/046;

.field public static final enum DISABLED_BY_STREAM_NOT_FOUND:LX/046;

.field public static final enum DISABLED_BY_UNKNOWN_AUTOPLAY_SETTINGS:LX/046;

.field public static final enum DISABLED_BY_VOD_NOT_READY:LX/046;

.field public static final enum DISABLED_BY_ZERORATING:LX/046;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 12443
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_SERVER"

    const-string v2, "server_blocked"

    invoke-direct {v0, v1, v4, v2}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_SERVER:LX/046;

    .line 12444
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_AUTOPLAY_SETTING"

    const-string v2, "user_setting_off"

    invoke-direct {v0, v1, v5, v2}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_AUTOPLAY_SETTING:LX/046;

    .line 12445
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_CONNECTION"

    const-string v2, "network_connectivity_low"

    invoke-direct {v0, v1, v6, v2}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_CONNECTION:LX/046;

    .line 12446
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_ALREADY_SEEN"

    const-string v2, "video_already_seen"

    invoke-direct {v0, v1, v7, v2}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_ALREADY_SEEN:LX/046;

    .line 12447
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_UNKNOWN_AUTOPLAY_SETTINGS"

    const-string v2, "disabled_by_unknown_settings"

    invoke-direct {v0, v1, v8, v2}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_UNKNOWN_AUTOPLAY_SETTINGS:LX/046;

    .line 12448
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_ZERORATING"

    const/4 v2, 0x5

    const-string v3, "zerorating_enabled"

    invoke-direct {v0, v1, v2, v3}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_ZERORATING:LX/046;

    .line 12449
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_METERED_NETWORK"

    const/4 v2, 0x6

    const-string v3, "user_network_metered"

    invoke-direct {v0, v1, v2, v3}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_METERED_NETWORK:LX/046;

    .line 12450
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_LOW_BATTERY"

    const/4 v2, 0x7

    const-string v3, "low_battery_level"

    invoke-direct {v0, v1, v2, v3}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_LOW_BATTERY:LX/046;

    .line 12451
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_POWER_SAVING"

    const/16 v2, 0x8

    const-string v3, "power_saving_enabled"

    invoke-direct {v0, v1, v2, v3}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_POWER_SAVING:LX/046;

    .line 12452
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_CACHE_NOT_READY"

    const/16 v2, 0x9

    const-string v3, "cache_not_ready"

    invoke-direct {v0, v1, v2, v3}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_CACHE_NOT_READY:LX/046;

    .line 12453
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_VOD_NOT_READY"

    const/16 v2, 0xa

    const-string v3, "vod_not_ready"

    invoke-direct {v0, v1, v2, v3}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_VOD_NOT_READY:LX/046;

    .line 12454
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_360_AUTOPLAY_SENSOR_REQUIREMENT"

    const/16 v2, 0xb

    const-string v3, "user_setting_failed_360_sensor_requirement"

    invoke-direct {v0, v1, v2, v3}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_360_AUTOPLAY_SENSOR_REQUIREMENT:LX/046;

    .line 12455
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_DATA_SAVINGS_MODE"

    const/16 v2, 0xc

    const-string v3, "data_savings_mode_active"

    invoke-direct {v0, v1, v2, v3}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_DATA_SAVINGS_MODE:LX/046;

    .line 12456
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_PLAYER_ERROR_STATE"

    const/16 v2, 0xd

    const-string v3, "player_error_state"

    invoke-direct {v0, v1, v2, v3}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_PLAYER_ERROR_STATE:LX/046;

    .line 12457
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_STREAM_NOT_FOUND"

    const/16 v2, 0xe

    const-string v3, "stream_not_found"

    invoke-direct {v0, v1, v2, v3}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_STREAM_NOT_FOUND:LX/046;

    .line 12458
    new-instance v0, LX/046;

    const-string v1, "DISABLED_BY_ACCESSIBILITY"

    const/16 v2, 0xf

    const-string v3, "accessibility_enabled"

    invoke-direct {v0, v1, v2, v3}, LX/046;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/046;->DISABLED_BY_ACCESSIBILITY:LX/046;

    .line 12459
    const/16 v0, 0x10

    new-array v0, v0, [LX/046;

    sget-object v1, LX/046;->DISABLED_BY_SERVER:LX/046;

    aput-object v1, v0, v4

    sget-object v1, LX/046;->DISABLED_BY_AUTOPLAY_SETTING:LX/046;

    aput-object v1, v0, v5

    sget-object v1, LX/046;->DISABLED_BY_CONNECTION:LX/046;

    aput-object v1, v0, v6

    sget-object v1, LX/046;->DISABLED_BY_ALREADY_SEEN:LX/046;

    aput-object v1, v0, v7

    sget-object v1, LX/046;->DISABLED_BY_UNKNOWN_AUTOPLAY_SETTINGS:LX/046;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/046;->DISABLED_BY_ZERORATING:LX/046;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/046;->DISABLED_BY_METERED_NETWORK:LX/046;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/046;->DISABLED_BY_LOW_BATTERY:LX/046;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/046;->DISABLED_BY_POWER_SAVING:LX/046;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/046;->DISABLED_BY_CACHE_NOT_READY:LX/046;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/046;->DISABLED_BY_VOD_NOT_READY:LX/046;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/046;->DISABLED_BY_360_AUTOPLAY_SENSOR_REQUIREMENT:LX/046;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/046;->DISABLED_BY_DATA_SAVINGS_MODE:LX/046;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/046;->DISABLED_BY_PLAYER_ERROR_STATE:LX/046;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/046;->DISABLED_BY_STREAM_NOT_FOUND:LX/046;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/046;->DISABLED_BY_ACCESSIBILITY:LX/046;

    aput-object v2, v0, v1

    sput-object v0, LX/046;->$VALUES:[LX/046;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12460
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12461
    iput-object p3, p0, LX/046;->value:Ljava/lang/String;

    .line 12462
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/046;
    .locals 1

    .prologue
    .line 12463
    const-class v0, LX/046;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/046;

    return-object v0
.end method

.method public static values()[LX/046;
    .locals 1

    .prologue
    .line 12464
    sget-object v0, LX/046;->$VALUES:[LX/046;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/046;

    return-object v0
.end method
