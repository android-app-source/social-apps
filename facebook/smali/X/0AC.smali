.class public final enum LX/0AC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0AC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0AC;

.field public static final enum CACHED:LX/0AC;

.field public static final enum NOT_APPLY:LX/0AC;

.field public static final enum NOT_CACHED:LX/0AC;

.field public static final enum SEMI_CACHED:LX/0AC;


# instance fields
.field public value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23601
    new-instance v0, LX/0AC;

    const-string v1, "NOT_CACHED"

    invoke-direct {v0, v1, v3, v3}, LX/0AC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0AC;->NOT_CACHED:LX/0AC;

    .line 23602
    new-instance v0, LX/0AC;

    const-string v1, "CACHED"

    invoke-direct {v0, v1, v4, v4}, LX/0AC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0AC;->CACHED:LX/0AC;

    .line 23603
    new-instance v0, LX/0AC;

    const-string v1, "SEMI_CACHED"

    invoke-direct {v0, v1, v5, v5}, LX/0AC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0AC;->SEMI_CACHED:LX/0AC;

    .line 23604
    new-instance v0, LX/0AC;

    const-string v1, "NOT_APPLY"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v6, v2}, LX/0AC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0AC;->NOT_APPLY:LX/0AC;

    .line 23605
    const/4 v0, 0x4

    new-array v0, v0, [LX/0AC;

    sget-object v1, LX/0AC;->NOT_CACHED:LX/0AC;

    aput-object v1, v0, v3

    sget-object v1, LX/0AC;->CACHED:LX/0AC;

    aput-object v1, v0, v4

    sget-object v1, LX/0AC;->SEMI_CACHED:LX/0AC;

    aput-object v1, v0, v5

    sget-object v1, LX/0AC;->NOT_APPLY:LX/0AC;

    aput-object v1, v0, v6

    sput-object v0, LX/0AC;->$VALUES:[LX/0AC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 23606
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23607
    iput p3, p0, LX/0AC;->value:I

    .line 23608
    return-void
.end method

.method public static fromValue(I)LX/0AC;
    .locals 5

    .prologue
    .line 23596
    invoke-static {}, LX/0AC;->values()[LX/0AC;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 23597
    iget v4, v0, LX/0AC;->value:I

    if-ne v4, p0, :cond_0

    .line 23598
    :goto_1
    return-object v0

    .line 23599
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 23600
    :cond_1
    sget-object v0, LX/0AC;->NOT_APPLY:LX/0AC;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/0AC;
    .locals 1

    .prologue
    .line 23595
    const-class v0, LX/0AC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0AC;

    return-object v0
.end method

.method public static values()[LX/0AC;
    .locals 1

    .prologue
    .line 23593
    sget-object v0, LX/0AC;->$VALUES:[LX/0AC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0AC;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 23594
    iget v0, p0, LX/0AC;->value:I

    return v0
.end method
