.class public final LX/0OG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0G7;


# instance fields
.field private final a:LX/04n;

.field private b:Ljava/io/RandomAccessFile;

.field private c:Ljava/lang/String;

.field private d:J

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52862
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0OG;-><init>(LX/04n;)V

    .line 52863
    return-void
.end method

.method public constructor <init>(LX/04n;)V
    .locals 0

    .prologue
    .line 52864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52865
    iput-object p1, p0, LX/0OG;->a:LX/04n;

    .line 52866
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 6

    .prologue
    .line 52867
    iget-wide v0, p0, LX/0OG;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 52868
    const/4 v0, -0x1

    .line 52869
    :cond_0
    :goto_0
    return v0

    .line 52870
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/0OG;->b:Ljava/io/RandomAccessFile;

    iget-wide v2, p0, LX/0OG;->d:J

    int-to-long v4, p3

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, p1, p2, v1}, Ljava/io/RandomAccessFile;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 52871
    if-lez v0, :cond_0

    .line 52872
    iget-wide v2, p0, LX/0OG;->d:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/0OG;->d:J

    .line 52873
    iget-object v1, p0, LX/0OG;->a:LX/04n;

    if-eqz v1, :cond_0

    .line 52874
    iget-object v1, p0, LX/0OG;->a:LX/04n;

    invoke-interface {v1, v0}, LX/04n;->a(I)V

    goto :goto_0

    .line 52875
    :catch_0
    move-exception v0

    .line 52876
    new-instance v1, LX/0OF;

    invoke-direct {v1, v0}, LX/0OF;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public final a(LX/0OA;)J
    .locals 4

    .prologue
    .line 52877
    :try_start_0
    iget-object v0, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0OG;->c:Ljava/lang/String;

    .line 52878
    new-instance v0, Ljava/io/RandomAccessFile;

    iget-object v1, p1, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "r"

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/0OG;->b:Ljava/io/RandomAccessFile;

    .line 52879
    iget-object v0, p0, LX/0OG;->b:Ljava/io/RandomAccessFile;

    iget-wide v2, p1, LX/0OA;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 52880
    iget-wide v0, p1, LX/0OA;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0OG;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    iget-wide v2, p1, LX/0OA;->d:J

    sub-long/2addr v0, v2

    :goto_0
    iput-wide v0, p0, LX/0OG;->d:J

    .line 52881
    iget-wide v0, p0, LX/0OG;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 52882
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52883
    :catch_0
    move-exception v0

    .line 52884
    new-instance v1, LX/0OF;

    invoke-direct {v1, v0}, LX/0OF;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 52885
    :cond_0
    :try_start_1
    iget-wide v0, p1, LX/0OA;->e:J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 52886
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0OG;->e:Z

    .line 52887
    iget-object v0, p0, LX/0OG;->a:LX/04n;

    if-eqz v0, :cond_2

    .line 52888
    iget-object v0, p0, LX/0OG;->a:LX/04n;

    invoke-interface {v0}, LX/04n;->b()V

    .line 52889
    :cond_2
    iget-wide v0, p0, LX/0OG;->d:J

    return-wide v0
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 52890
    iput-object v2, p0, LX/0OG;->c:Ljava/lang/String;

    .line 52891
    iget-object v0, p0, LX/0OG;->b:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_0

    .line 52892
    :try_start_0
    iget-object v0, p0, LX/0OG;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52893
    iput-object v2, p0, LX/0OG;->b:Ljava/io/RandomAccessFile;

    .line 52894
    iget-boolean v0, p0, LX/0OG;->e:Z

    if-eqz v0, :cond_0

    .line 52895
    iput-boolean v3, p0, LX/0OG;->e:Z

    .line 52896
    iget-object v0, p0, LX/0OG;->a:LX/04n;

    if-eqz v0, :cond_0

    .line 52897
    iget-object v0, p0, LX/0OG;->a:LX/04n;

    invoke-interface {v0}, LX/04n;->c()V

    .line 52898
    :cond_0
    return-void

    .line 52899
    :catch_0
    move-exception v0

    .line 52900
    :try_start_1
    new-instance v1, LX/0OF;

    invoke-direct {v1, v0}, LX/0OF;-><init>(Ljava/io/IOException;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52901
    :catchall_0
    move-exception v0

    iput-object v2, p0, LX/0OG;->b:Ljava/io/RandomAccessFile;

    .line 52902
    iget-boolean v1, p0, LX/0OG;->e:Z

    if-eqz v1, :cond_1

    .line 52903
    iput-boolean v3, p0, LX/0OG;->e:Z

    .line 52904
    iget-object v1, p0, LX/0OG;->a:LX/04n;

    if-eqz v1, :cond_1

    .line 52905
    iget-object v1, p0, LX/0OG;->a:LX/04n;

    invoke-interface {v1}, LX/04n;->c()V

    :cond_1
    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52906
    iget-object v0, p0, LX/0OG;->c:Ljava/lang/String;

    return-object v0
.end method
