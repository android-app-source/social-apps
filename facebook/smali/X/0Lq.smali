.class public LX/0Lq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0LZ;
.implements LX/0Lp;


# instance fields
.field private A:I

.field private B:LX/0LB;

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Ljava/io/IOException;

.field private I:J

.field private J:J

.field public final a:Landroid/os/Handler;

.field public final b:LX/0KU;

.field private final c:LX/0G6;

.field private final d:LX/04o;

.field private final e:LX/0Ld;

.field private final f:LX/0Od;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Od",
            "<",
            "LX/0AY;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Lr;

.field private final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0Lk;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0Ln;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0OX;

.field private final k:J

.field private final l:J

.field private final m:[J

.field private final n:Z

.field private final o:I

.field private final p:Z

.field private final q:I

.field private final r:J

.field private final s:LX/0Lm;

.field private final t:J

.field private final u:Z

.field private final v:Z

.field private w:Z

.field private x:LX/0AY;

.field private y:LX/0AY;

.field private z:LX/0Lk;


# direct methods
.method public constructor <init>(LX/0AY;LX/0Lr;LX/0G6;LX/04o;)V
    .locals 21

    .prologue
    .line 44630
    const/4 v2, 0x0

    new-instance v7, LX/08J;

    invoke-direct {v7}, LX/08J;-><init>()V

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const-wide/16 v18, 0x0

    const/16 v20, 0x0

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    invoke-direct/range {v1 .. v20}, LX/0Lq;-><init>(LX/0Od;LX/0AY;LX/0Lr;LX/0G6;LX/04o;LX/0OX;JJZLandroid/os/Handler;LX/0KU;IZIJZ)V

    .line 44631
    return-void
.end method

.method private constructor <init>(LX/0Lr;LX/0G6;LX/04o;JILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Lr;",
            "LX/0G6;",
            "LX/04o;",
            "JI",
            "Ljava/util/List",
            "<",
            "LX/0Ah;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44632
    invoke-static {p4, p5, p6, p7}, LX/0Lq;->a(JILjava/util/List;)LX/0AY;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2, p3}, LX/0Lq;-><init>(LX/0AY;LX/0Lr;LX/0G6;LX/04o;)V

    .line 44633
    return-void
.end method

.method public varargs constructor <init>(LX/0Lr;LX/0G6;LX/04o;JI[LX/0Ah;)V
    .locals 8

    .prologue
    .line 44605
    invoke-static {p7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-direct/range {v0 .. v7}, LX/0Lq;-><init>(LX/0Lr;LX/0G6;LX/04o;JILjava/util/List;)V

    .line 44606
    return-void
.end method

.method private constructor <init>(LX/0Od;LX/0AY;LX/0Lr;LX/0G6;LX/04o;LX/0OX;JJZLandroid/os/Handler;LX/0KU;IZIJLX/0Lm;JZZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Od",
            "<",
            "LX/0AY;",
            ">;",
            "LX/0AY;",
            "LX/0Lr;",
            "LX/0G6;",
            "LX/04o;",
            "LX/0OX;",
            "JJZ",
            "Landroid/os/Handler;",
            "LX/0KU;",
            "IZIJ",
            "LX/0Lm;",
            "JZZ)V"
        }
    .end annotation

    .prologue
    .line 44634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44635
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/0Lq;->w:Z

    .line 44636
    iput-object p1, p0, LX/0Lq;->f:LX/0Od;

    .line 44637
    iput-object p2, p0, LX/0Lq;->x:LX/0AY;

    .line 44638
    iput-object p3, p0, LX/0Lq;->g:LX/0Lr;

    .line 44639
    iput-object p4, p0, LX/0Lq;->c:LX/0G6;

    .line 44640
    iput-object p5, p0, LX/0Lq;->d:LX/04o;

    .line 44641
    iput-object p6, p0, LX/0Lq;->j:LX/0OX;

    .line 44642
    iput-wide p7, p0, LX/0Lq;->k:J

    .line 44643
    iput-wide p9, p0, LX/0Lq;->l:J

    .line 44644
    iput-boolean p11, p0, LX/0Lq;->D:Z

    .line 44645
    iput-object p12, p0, LX/0Lq;->a:Landroid/os/Handler;

    .line 44646
    move-object/from16 v0, p13

    iput-object v0, p0, LX/0Lq;->b:LX/0KU;

    .line 44647
    move/from16 v0, p14

    iput v0, p0, LX/0Lq;->o:I

    .line 44648
    new-instance v2, LX/0Ld;

    invoke-direct {v2}, LX/0Ld;-><init>()V

    iput-object v2, p0, LX/0Lq;->e:LX/0Ld;

    .line 44649
    const/4 v2, 0x2

    new-array v2, v2, [J

    iput-object v2, p0, LX/0Lq;->m:[J

    .line 44650
    move/from16 v0, p15

    iput-boolean v0, p0, LX/0Lq;->p:Z

    .line 44651
    move/from16 v0, p16

    mul-int/lit16 v2, v0, 0x3e8

    iput v2, p0, LX/0Lq;->q:I

    .line 44652
    move-wide/from16 v0, p17

    iput-wide v0, p0, LX/0Lq;->r:J

    .line 44653
    move-object/from16 v0, p19

    iput-object v0, p0, LX/0Lq;->s:LX/0Lm;

    .line 44654
    move-wide/from16 v0, p20

    iput-wide v0, p0, LX/0Lq;->t:J

    .line 44655
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iput-object v2, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    .line 44656
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, LX/0Lq;->h:Ljava/util/ArrayList;

    .line 44657
    iget-boolean v2, p2, LX/0AY;->e:Z

    iput-boolean v2, p0, LX/0Lq;->n:Z

    .line 44658
    move/from16 v0, p22

    iput-boolean v0, p0, LX/0Lq;->u:Z

    .line 44659
    move/from16 v0, p23

    iput-boolean v0, p0, LX/0Lq;->v:Z

    .line 44660
    return-void
.end method

.method private constructor <init>(LX/0Od;LX/0AY;LX/0Lr;LX/0G6;LX/04o;LX/0OX;JJZLandroid/os/Handler;LX/0KU;IZIJZ)V
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Od",
            "<",
            "LX/0AY;",
            ">;",
            "LX/0AY;",
            "LX/0Lr;",
            "LX/0G6;",
            "LX/04o;",
            "LX/0OX;",
            "JJZ",
            "Landroid/os/Handler;",
            "LX/0KU;",
            "IZIJZ)V"
        }
    .end annotation

    .prologue
    .line 44661
    sget-object v20, LX/0Lm;->DEFAULT:LX/0Lm;

    const-wide/16 v21, 0x0

    const/16 v24, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    move/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move/from16 v15, p14

    move/from16 v16, p15

    move/from16 v17, p16

    move-wide/from16 v18, p17

    move/from16 v23, p19

    invoke-direct/range {v1 .. v24}, LX/0Lq;-><init>(LX/0Od;LX/0AY;LX/0Lr;LX/0G6;LX/04o;LX/0OX;JJZLandroid/os/Handler;LX/0KU;IZIJLX/0Lm;JZZ)V

    .line 44662
    return-void
.end method

.method public constructor <init>(LX/0Od;LX/0Lr;LX/0G6;LX/04o;JJLandroid/os/Handler;LX/0KU;IZIJLX/0Lm;JZZ)V
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Od",
            "<",
            "LX/0AY;",
            ">;",
            "LX/0Lr;",
            "LX/0G6;",
            "LX/04o;",
            "JJ",
            "Landroid/os/Handler;",
            "LX/0KU;",
            "IZIJ",
            "LX/0Lm;",
            "JZZ)V"
        }
    .end annotation

    .prologue
    .line 44663
    invoke-virtual/range {p1 .. p1}, LX/0Od;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0AY;

    new-instance v7, LX/08J;

    invoke-direct {v7}, LX/08J;-><init>()V

    const-wide/16 v0, 0x3e8

    mul-long v8, p5, v0

    const-wide/16 v0, 0x3e8

    mul-long v10, p7, v0

    const/4 v12, 0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v13, p9

    move-object/from16 v14, p10

    move/from16 v15, p11

    move/from16 v16, p12

    move/from16 v17, p13

    move-wide/from16 v18, p14

    move-object/from16 v20, p16

    move-wide/from16 v21, p17

    move/from16 v23, p19

    move/from16 v24, p20

    invoke-direct/range {v1 .. v24}, LX/0Lq;-><init>(LX/0Od;LX/0AY;LX/0Lr;LX/0G6;LX/04o;LX/0OX;JJZLandroid/os/Handler;LX/0KU;IZIJLX/0Lm;JZZ)V

    .line 44664
    return-void
.end method

.method private static a(JILjava/util/List;)LX/0AY;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/List",
            "<",
            "LX/0Ah;",
            ">;)",
            "LX/0AY;"
        }
    .end annotation

    .prologue
    .line 44665
    new-instance v2, LX/0Ak;

    const/4 v3, 0x0

    move/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v2, v3, v0, v1}, LX/0Ak;-><init>(IILjava/util/List;)V

    .line 44666
    new-instance v8, LX/0Am;

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v8, v3, v4, v5, v2}, LX/0Am;-><init>(Ljava/lang/String;JLjava/util/List;)V

    .line 44667
    new-instance v3, LX/0AY;

    const-wide/16 v4, -0x1

    const-wide/16 v6, -0x1

    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    const-wide/16 v13, -0x1

    const-wide/16 v15, -0x1

    const/16 v17, 0x0

    const/16 v18, 0x0

    const-wide/16 v19, 0x0

    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v21

    move-wide/from16 v8, p0

    invoke-direct/range {v3 .. v21}, LX/0AY;-><init>(JJJJZJJLX/0Ly;Ljava/lang/String;JLjava/util/List;)V

    return-object v3
.end method

.method private static a(ILX/0AR;Ljava/lang/String;J)LX/0L4;
    .locals 11

    .prologue
    const/4 v3, -0x1

    const/4 v8, 0x0

    .line 44668
    packed-switch p0, :pswitch_data_0

    .line 44669
    :goto_0
    return-object v8

    .line 44670
    :pswitch_0
    iget-object v0, p1, LX/0AR;->a:Ljava/lang/String;

    iget v2, p1, LX/0AR;->c:I

    iget v6, p1, LX/0AR;->f:I

    iget v7, p1, LX/0AR;->g:I

    move-object v1, p2

    move-wide v4, p3

    invoke-static/range {v0 .. v8}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;)LX/0L4;

    move-result-object v8

    goto :goto_0

    .line 44671
    :pswitch_1
    iget-object v0, p1, LX/0AR;->a:Ljava/lang/String;

    iget v2, p1, LX/0AR;->c:I

    iget v6, p1, LX/0AR;->i:I

    iget v7, p1, LX/0AR;->j:I

    iget-object v9, p1, LX/0AR;->l:Ljava/lang/String;

    move-object v1, p2

    move-wide v4, p3

    invoke-static/range {v0 .. v9}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v8

    goto :goto_0

    .line 44672
    :pswitch_2
    iget-object v1, p1, LX/0AR;->a:Ljava/lang/String;

    iget v3, p1, LX/0AR;->c:I

    iget-object v6, p1, LX/0AR;->l:Ljava/lang/String;

    move-object v2, p2

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)LX/0L4;

    move-result-object v8

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(LX/0Au;LX/0Au;LX/0Ah;LX/0LV;LX/0G6;II)LX/0LP;
    .locals 8

    .prologue
    .line 44673
    if-eqz p0, :cond_0

    .line 44674
    invoke-virtual {p0, p1}, LX/0Au;->a(LX/0Au;)LX/0Au;

    move-result-object p1

    .line 44675
    if-nez p1, :cond_1

    .line 44676
    :goto_0
    new-instance v0, LX/0OA;

    invoke-virtual {p0}, LX/0Au;->a()Landroid/net/Uri;

    move-result-object v1

    iget-wide v2, p0, LX/0Au;->a:J

    iget-wide v4, p0, LX/0Au;->b:J

    .line 44677
    iget-object v6, p2, LX/0Ah;->e:Ljava/lang/String;

    move-object v6, v6

    .line 44678
    invoke-direct/range {v0 .. v6}, LX/0OA;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 44679
    new-instance v1, LX/0Lg;

    iget-object v5, p2, LX/0Ah;->c:LX/0AR;

    move-object v2, p4

    move-object v3, v0

    move v4, p6

    move-object v6, p3

    move v7, p5

    invoke-direct/range {v1 .. v7}, LX/0Lg;-><init>(LX/0G6;LX/0OA;ILX/0AR;LX/0LV;I)V

    return-object v1

    :cond_0
    move-object p0, p1

    .line 44680
    goto :goto_0

    :cond_1
    move-object p0, p1

    goto :goto_0
.end method

.method private static a(LX/0Ln;LX/0Lo;LX/0G6;LX/0L4;LX/0Lk;II)LX/0LP;
    .locals 24

    .prologue
    .line 44681
    move-object/from16 v0, p1

    iget-object v9, v0, LX/0Lo;->c:LX/0Ah;

    .line 44682
    iget-object v12, v9, LX/0Ah;->c:LX/0AR;

    .line 44683
    move-object/from16 v0, p1

    move/from16 v1, p5

    invoke-virtual {v0, v1}, LX/0Lo;->a(I)J

    move-result-wide v22

    .line 44684
    move-object/from16 v0, p1

    move/from16 v1, p5

    invoke-virtual {v0, v1}, LX/0Lo;->b(I)J

    move-result-wide v10

    .line 44685
    move-object/from16 v0, p1

    move/from16 v1, p5

    invoke-virtual {v0, v1}, LX/0Lo;->e(I)LX/0Au;

    move-result-object v6

    .line 44686
    new-instance v2, LX/0OA;

    invoke-virtual {v6}, LX/0Au;->a()Landroid/net/Uri;

    move-result-object v3

    iget-wide v4, v6, LX/0Au;->a:J

    iget-wide v6, v6, LX/0Au;->b:J

    invoke-virtual {v9}, LX/0Ah;->g()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, LX/0OA;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 44687
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/0Ln;->b:J

    iget-wide v6, v9, LX/0Ah;->d:J

    sub-long v13, v4, v6

    .line 44688
    iget-object v3, v12, LX/0AR;->b:Ljava/lang/String;

    invoke-static {v3}, LX/0Lq;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 44689
    new-instance v3, LX/0Lh;

    const/4 v6, 0x1

    move-object/from16 v0, p4

    iget-object v13, v0, LX/0Lk;->a:LX/0L4;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, LX/0Ln;->a:I

    move-object/from16 v4, p2

    move-object v5, v2

    move-object v7, v12

    move-wide/from16 v8, v22

    move/from16 v12, p5

    invoke-direct/range {v3 .. v15}, LX/0Lh;-><init>(LX/0G6;LX/0OA;ILX/0AR;JJILX/0L4;LX/0Lz;I)V

    .line 44690
    :goto_0
    return-object v3

    .line 44691
    :cond_0
    if-eqz p3, :cond_1

    const/16 v20, 0x1

    .line 44692
    :goto_1
    new-instance v3, LX/0La;

    move-object/from16 v0, p1

    iget-object v15, v0, LX/0Lo;->b:LX/0LV;

    move-object/from16 v0, p4

    iget v0, v0, LX/0Lk;->b:I

    move/from16 v17, v0

    move-object/from16 v0, p4

    iget v0, v0, LX/0Lk;->c:I

    move/from16 v18, v0

    invoke-static/range {p0 .. p0}, LX/0Ln;->a(LX/0Ln;)LX/0Lz;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, LX/0Ln;->a:I

    move/from16 v21, v0

    move-object/from16 v4, p2

    move-object v5, v2

    move/from16 v6, p6

    move-object v7, v12

    move-wide/from16 v8, v22

    move/from16 v12, p5

    move-object/from16 v16, p3

    invoke-direct/range {v3 .. v21}, LX/0La;-><init>(LX/0G6;LX/0OA;ILX/0AR;JJIJLX/0LV;LX/0L4;IILX/0Lz;ZI)V

    goto :goto_0

    .line 44693
    :cond_1
    const/16 v20, 0x0

    goto :goto_1
.end method

.method private static a(LX/0AR;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 44694
    iget-object v0, p0, LX/0AR;->b:Ljava/lang/String;

    .line 44695
    invoke-static {v0}, LX/0Al;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 44696
    iget-object v0, p0, LX/0AR;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0Al;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 44697
    :cond_0
    :goto_0
    return-object v0

    .line 44698
    :cond_1
    invoke-static {v0}, LX/0Al;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 44699
    iget-object v0, p0, LX/0AR;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0Al;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 44700
    :cond_2
    invoke-static {v0}, LX/0Lq;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 44701
    const-string v1, "application/mp4"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 44702
    const-string v0, "stpp"

    iget-object v1, p0, LX/0AR;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 44703
    const-string v0, "application/ttml+xml"

    goto :goto_0

    .line 44704
    :cond_3
    const-string v0, "wvtt"

    iget-object v1, p0, LX/0AR;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 44705
    const-string v0, "application/x-mp4vtt"

    goto :goto_0

    .line 44706
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/0AY;)Z
    .locals 18

    .prologue
    .line 44707
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0AY;->a(I)LX/0Am;

    move-result-object v3

    .line 44708
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, LX/0Am;->a(I)I

    move-result v4

    .line 44709
    const/4 v2, 0x0

    .line 44710
    const/4 v5, -0x1

    if-eq v4, v5, :cond_6

    .line 44711
    iget-object v2, v3, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Ak;

    move-object v8, v2

    .line 44712
    :goto_0
    if-nez v8, :cond_0

    .line 44713
    const/4 v2, 0x0

    .line 44714
    :goto_1
    return v2

    .line 44715
    :cond_0
    const-wide/16 v6, -0x1

    .line 44716
    const-wide/16 v4, -0x1

    .line 44717
    const/4 v3, 0x0

    .line 44718
    const/4 v2, 0x0

    .line 44719
    iget-object v8, v8, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v8, v6

    move-wide v6, v4

    move v4, v3

    move-object v3, v2

    :cond_1
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Ah;

    .line 44720
    instance-of v5, v2, LX/0BW;

    if-nez v5, :cond_2

    .line 44721
    const/4 v2, 0x0

    goto :goto_1

    .line 44722
    :cond_2
    check-cast v2, LX/0BW;

    .line 44723
    const-wide/16 v12, -0x1

    cmp-long v5, v8, v12

    if-nez v5, :cond_3

    .line 44724
    invoke-virtual {v2}, LX/0BW;->a()I

    move-result v4

    .line 44725
    const-wide/16 v6, -0x1

    invoke-virtual {v2, v6, v7}, LX/0BW;->a(J)I

    move-result v5

    .line 44726
    sub-int v3, v5, v4

    add-int/lit8 v3, v3, 0x1

    .line 44727
    invoke-virtual {v2, v4}, LX/0BW;->a(I)J

    move-result-wide v6

    .line 44728
    invoke-virtual {v2, v5}, LX/0BW;->a(I)J

    move-result-wide v4

    move-wide v8, v6

    move-wide v6, v4

    move v4, v3

    move-object v3, v2

    .line 44729
    goto :goto_2

    .line 44730
    :cond_3
    invoke-virtual {v2}, LX/0BW;->a()I

    move-result v5

    .line 44731
    const-wide/16 v12, -0x1

    invoke-virtual {v2, v12, v13}, LX/0BW;->a(J)I

    move-result v11

    .line 44732
    sub-int v12, v11, v5

    add-int/lit8 v12, v12, 0x1

    .line 44733
    invoke-virtual {v2, v5}, LX/0BW;->a(I)J

    move-result-wide v14

    .line 44734
    invoke-virtual {v2, v11}, LX/0BW;->a(I)J

    move-result-wide v16

    .line 44735
    if-ne v12, v4, :cond_4

    cmp-long v5, v14, v8

    if-nez v5, :cond_4

    cmp-long v5, v16, v6

    if-eqz v5, :cond_1

    .line 44736
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->f:LX/0Od;

    invoke-virtual {v3}, LX/0BW;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, LX/0BW;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v3, v2}, LX/0Od;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 44737
    const/4 v2, 0x1

    goto :goto_1

    .line 44738
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    move-object v8, v2

    goto :goto_0
.end method

.method private static a(Ljava/util/List;J)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/0LQ;",
            ">;J)Z"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 44739
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v2, v4

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0LQ;

    .line 44740
    iget-wide v8, v0, LX/0LQ;->i:J

    iget-wide v0, v0, LX/0LQ;->h:J

    sub-long v0, v8, v0

    .line 44741
    cmp-long v7, v0, v4

    if-ltz v7, :cond_2

    .line 44742
    add-long/2addr v0, v2

    .line 44743
    :goto_1
    cmp-long v2, v0, p1

    if-ltz v2, :cond_0

    .line 44744
    const/4 v0, 0x1

    .line 44745
    :goto_2
    return v0

    :cond_0
    move-wide v2, v0

    .line 44746
    goto :goto_0

    .line 44747
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move-wide v0, v2

    goto :goto_1
.end method

.method private b(LX/0AY;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 44748
    iget-boolean v0, p0, LX/0Lq;->v:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/0Lq;->w:Z

    if-nez v0, :cond_0

    .line 44749
    invoke-direct {p0, p1}, LX/0Lq;->a(LX/0AY;)Z

    move-result v0

    iput-boolean v0, p0, LX/0Lq;->w:Z

    .line 44750
    :cond_0
    invoke-virtual {p1, v8}, LX/0AY;->a(I)LX/0Am;

    move-result-object v1

    .line 44751
    :goto_0
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ln;

    iget-wide v2, v0, LX/0Ln;->b:J

    iget-wide v4, v1, LX/0Am;->b:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    .line 44752
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ln;

    .line 44753
    iget-object v2, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    iget v0, v0, LX/0Ln;->a:I

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_0

    .line 44754
    :cond_1
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-virtual {p1}, LX/0AY;->b()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 44755
    :goto_1
    return-void

    .line 44756
    :cond_2
    :try_start_0
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 44757
    if-lez v1, :cond_3

    .line 44758
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ln;

    const/4 v2, 0x0

    iget-object v3, p0, LX/0Lq;->z:LX/0Lk;

    invoke-virtual {v0, p1, v2, v3}, LX/0Ln;->a(LX/0AY;ILX/0Lk;)V

    .line 44759
    const/4 v0, 0x1

    if-le v1, v0, :cond_3

    .line 44760
    add-int/lit8 v1, v1, -0x1

    .line 44761
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ln;

    iget-object v2, p0, LX/0Lq;->z:LX/0Lk;

    invoke-virtual {v0, p1, v1, v2}, LX/0Ln;->a(LX/0AY;ILX/0Lk;)V
    :try_end_0
    .catch LX/0Kn; {:try_start_0 .. :try_end_0} :catch_0

    .line 44762
    :cond_3
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    :goto_2
    invoke-virtual {p1}, LX/0AY;->b()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 44763
    new-instance v0, LX/0Ln;

    iget v1, p0, LX/0Lq;->A:I

    iget-object v4, p0, LX/0Lq;->z:LX/0Lk;

    iget-boolean v5, p0, LX/0Lq;->p:Z

    iget-boolean v6, p0, LX/0Lq;->u:Z

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, LX/0Ln;-><init>(ILX/0AY;ILX/0Lk;ZZ)V

    .line 44764
    iget-object v1, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    iget v2, p0, LX/0Lq;->A:I

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 44765
    iget v0, p0, LX/0Lq;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Lq;->A:I

    .line 44766
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 44767
    :catch_0
    move-exception v0

    .line 44768
    iput-object v0, p0, LX/0Lq;->H:Ljava/io/IOException;

    goto :goto_1

    .line 44769
    :cond_4
    invoke-direct {p0}, LX/0Lq;->e()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LX/0Lq;->d(J)LX/0LB;

    move-result-object v0

    .line 44770
    iget-object v1, p0, LX/0Lq;->B:LX/0LB;

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/0Lq;->B:LX/0LB;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 44771
    :cond_5
    iput-object v0, p0, LX/0Lq;->B:LX/0LB;

    .line 44772
    iget-object v0, p0, LX/0Lq;->B:LX/0LB;

    .line 44773
    iget-object v1, p0, LX/0Lq;->a:Landroid/os/Handler;

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/0Lq;->b:LX/0KU;

    if-eqz v1, :cond_6

    .line 44774
    iget-object v1, p0, LX/0Lq;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/exoplayer/dash/DashChunkSource$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/exoplayer/dash/DashChunkSource$1;-><init>(LX/0Lq;LX/0LB;)V

    const v3, 0x70310ba

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 44775
    :cond_6
    :goto_3
    iput-boolean v8, p0, LX/0Lq;->F:Z

    .line 44776
    iput-object p1, p0, LX/0Lq;->x:LX/0AY;

    goto/16 :goto_1

    .line 44777
    :cond_7
    iget-object v1, p0, LX/0Lq;->a:Landroid/os/Handler;

    if-eqz v1, :cond_8

    iget-object v1, p0, LX/0Lq;->b:LX/0KU;

    if-eqz v1, :cond_8

    .line 44778
    iget-object v1, p0, LX/0Lq;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/exoplayer/dash/DashChunkSource$2;

    invoke-direct {v2, p0, v0}, Lcom/google/android/exoplayer/dash/DashChunkSource$2;-><init>(LX/0Lq;LX/0LB;)V

    const v3, 0x687604b4

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 44779
    :cond_8
    goto :goto_3
.end method

.method private b(J)Z
    .locals 7

    .prologue
    .line 44622
    const/4 v0, 0x1

    .line 44623
    sget-object v1, LX/0Lj;->a:[I

    iget-object v2, p0, LX/0Lq;->s:LX/0Lm;

    invoke-virtual {v2}, LX/0Lm;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 44624
    :goto_0
    if-eqz v0, :cond_0

    iget-wide v2, p0, LX/0Lq;->t:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    iget-wide v2, p0, LX/0Lq;->t:J

    iget-wide v4, p0, LX/0Lq;->J:J

    add-long/2addr v2, v4

    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    .line 44625
    const/4 v0, 0x0

    .line 44626
    :cond_0
    return v0

    .line 44627
    :pswitch_0
    iget-boolean v0, p0, LX/0Lq;->G:Z

    goto :goto_0

    .line 44628
    :pswitch_1
    iget-boolean v0, p0, LX/0Lq;->F:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 44629
    const-string v0, "text/vtt"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "application/ttml+xml"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)LX/0Ln;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 44374
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ln;

    .line 44375
    iget-wide v5, v0, LX/0Ln;->h:J

    move-wide v2, v5

    .line 44376
    cmp-long v0, p1, v2

    if-gez v0, :cond_1

    .line 44377
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ln;

    .line 44378
    :goto_0
    return-object v0

    .line 44379
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    :cond_1
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_2

    .line 44380
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ln;

    .line 44381
    invoke-virtual {v0}, LX/0Ln;->b()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    goto :goto_0

    .line 44382
    :cond_2
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    iget-object v1, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ln;

    goto :goto_0
.end method

.method private d(J)LX/0LB;
    .locals 13

    .prologue
    const-wide/16 v8, -0x1

    const-wide/16 v10, 0x3e8

    .line 44383
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ln;

    .line 44384
    iget-object v1, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    iget-object v2, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Ln;

    .line 44385
    iget-object v2, p0, LX/0Lq;->x:LX/0AY;

    iget-boolean v2, v2, LX/0AY;->e:Z

    if-eqz v2, :cond_0

    .line 44386
    iget-boolean v2, v1, LX/0Ln;->g:Z

    move v2, v2

    .line 44387
    if-eqz v2, :cond_1

    .line 44388
    :cond_0
    new-instance v2, LX/0LD;

    invoke-virtual {v0}, LX/0Ln;->a()J

    move-result-wide v4

    invoke-virtual {v1}, LX/0Ln;->b()J

    move-result-wide v0

    invoke-direct {v2, v4, v5, v0, v1}, LX/0LD;-><init>(JJ)V

    move-object v1, v2

    .line 44389
    :goto_0
    return-object v1

    .line 44390
    :cond_1
    invoke-virtual {v0}, LX/0Ln;->a()J

    move-result-wide v2

    .line 44391
    iget-boolean v0, v1, LX/0Ln;->f:Z

    move v0, v0

    .line 44392
    if-eqz v0, :cond_2

    const-wide v4, 0x7fffffffffffffffL

    .line 44393
    :goto_1
    iget-object v0, p0, LX/0Lq;->j:LX/0OX;

    invoke-interface {v0}, LX/0OX;->a()J

    move-result-wide v0

    mul-long/2addr v0, v10

    iget-object v6, p0, LX/0Lq;->x:LX/0AY;

    iget-wide v6, v6, LX/0AY;->a:J

    mul-long/2addr v6, v10

    sub-long v6, p1, v6

    sub-long v6, v0, v6

    .line 44394
    iget-object v0, p0, LX/0Lq;->x:LX/0AY;

    iget-wide v0, v0, LX/0AY;->g:J

    cmp-long v0, v0, v8

    if-nez v0, :cond_3

    .line 44395
    :goto_2
    new-instance v1, LX/0LC;

    iget-object v10, p0, LX/0Lq;->j:LX/0OX;

    invoke-direct/range {v1 .. v10}, LX/0LC;-><init>(JJJJLX/0OX;)V

    goto :goto_0

    .line 44396
    :cond_2
    invoke-virtual {v1}, LX/0Ln;->b()J

    move-result-wide v4

    goto :goto_1

    .line 44397
    :cond_3
    iget-object v0, p0, LX/0Lq;->x:LX/0AY;

    iget-wide v0, v0, LX/0AY;->g:J

    mul-long v8, v0, v10

    goto :goto_2
.end method

.method private e()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 44398
    iget-wide v0, p0, LX/0Lq;->l:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 44399
    iget-object v0, p0, LX/0Lq;->j:LX/0OX;

    invoke-interface {v0}, LX/0OX;->a()J

    move-result-wide v0

    mul-long/2addr v0, v4

    iget-wide v2, p0, LX/0Lq;->l:J

    add-long/2addr v0, v2

    .line 44400
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    mul-long/2addr v0, v4

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Lo;JZLjava/util/List;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Lo;",
            "JZ",
            "Ljava/util/List",
            "<+",
            "LX/0LQ;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 44401
    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p2, p3}, LX/0Lo;->a(J)I

    move-result v0

    .line 44402
    :goto_0
    iget-boolean v1, p0, LX/0Lq;->n:Z

    if-eqz v1, :cond_0

    iget-wide v2, p0, LX/0Lq;->r:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 44403
    invoke-virtual {p1}, LX/0Lo;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-wide v2, p0, LX/0Lq;->r:J

    invoke-static {p5, v2, v3}, LX/0Lq;->a(Ljava/util/List;J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44404
    iget v1, p0, LX/0Lq;->q:I

    if-lez v1, :cond_4

    .line 44405
    iget-object v1, p0, LX/0Lq;->m:[J

    const/4 v2, 0x0

    aget-wide v2, v1, v2

    iget-object v1, p0, LX/0Lq;->m:[J

    const/4 v4, 0x1

    aget-wide v4, v1, v4

    iget v1, p0, LX/0Lq;->q:I

    int-to-long v6, v1

    sub-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 44406
    invoke-virtual {p1, v2, v3}, LX/0Lo;->a(J)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 44407
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Buffered duration is greater than "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/0Lq;->r:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", jumping to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", queue size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44408
    :cond_0
    return v0

    .line 44409
    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p1}, LX/0Lo;->a()I

    move-result v0

    goto :goto_0

    :cond_2
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0LQ;

    invoke-virtual {v0}, LX/0LQ;->i()I

    move-result v0

    .line 44410
    iget-boolean v1, p1, LX/0Lo;->i:Z

    if-nez v1, :cond_5

    .line 44411
    :cond_3
    :goto_2
    move v0, v0

    .line 44412
    goto :goto_0

    .line 44413
    :cond_4
    invoke-virtual {p1}, LX/0Lo;->b()I

    move-result v0

    goto :goto_1

    .line 44414
    :cond_5
    invoke-virtual {p1}, LX/0Lo;->a()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 44415
    invoke-virtual {p1}, LX/0Lo;->a()I

    move-result v0

    goto :goto_2

    .line 44416
    :cond_6
    invoke-virtual {p1}, LX/0Lo;->b()I

    move-result v1

    if-le v0, v1, :cond_3

    .line 44417
    invoke-virtual {p1}, LX/0Lo;->b()I

    move-result v0

    goto :goto_2
.end method

.method public final a(I)LX/0L4;
    .locals 1

    .prologue
    .line 44418
    iget-object v0, p0, LX/0Lq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Lk;

    iget-object v0, v0, LX/0Lk;->a:LX/0L4;

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 44419
    iget-object v0, p0, LX/0Lq;->H:Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 44420
    iget-object v0, p0, LX/0Lq;->H:Ljava/io/IOException;

    throw v0

    .line 44421
    :cond_0
    iget-object v0, p0, LX/0Lq;->f:LX/0Od;

    if-eqz v0, :cond_1

    .line 44422
    iget-object v0, p0, LX/0Lq;->f:LX/0Od;

    invoke-virtual {v0}, LX/0Od;->c()V

    .line 44423
    :cond_1
    return-void
.end method

.method public final a(J)V
    .locals 9

    .prologue
    .line 44424
    iget-object v0, p0, LX/0Lq;->f:LX/0Od;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Lq;->x:LX/0AY;

    iget-boolean v0, v0, LX/0AY;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Lq;->H:Ljava/io/IOException;

    if-eqz v0, :cond_1

    .line 44425
    :cond_0
    :goto_0
    return-void

    .line 44426
    :cond_1
    iget-object v0, p0, LX/0Lq;->f:LX/0Od;

    .line 44427
    iget-object v1, v0, LX/0Od;->m:Ljava/lang/Object;

    move-object v0, v1

    .line 44428
    check-cast v0, LX/0AY;

    .line 44429
    if-eqz v0, :cond_2

    iget-object v1, p0, LX/0Lq;->y:LX/0AY;

    if-eq v0, v1, :cond_2

    .line 44430
    invoke-direct {p0, v0}, LX/0Lq;->b(LX/0AY;)V

    .line 44431
    iput-object v0, p0, LX/0Lq;->y:LX/0AY;

    .line 44432
    :cond_2
    iget-object v0, p0, LX/0Lq;->x:LX/0AY;

    iget-wide v0, v0, LX/0AY;->f:J

    .line 44433
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    .line 44434
    const-wide/16 v0, 0x1388

    .line 44435
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 44436
    iget-wide v4, p0, LX/0Lq;->I:J

    cmp-long v4, p1, v4

    if-eqz v4, :cond_4

    .line 44437
    iput-wide p1, p0, LX/0Lq;->I:J

    .line 44438
    iput-wide v2, p0, LX/0Lq;->J:J

    .line 44439
    :cond_4
    invoke-direct {p0, v2, v3}, LX/0Lq;->b(J)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, LX/0Lq;->f:LX/0Od;

    .line 44440
    iget-wide v7, v4, LX/0Od;->n:J

    move-wide v4, v7

    .line 44441
    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 44442
    iget-object v0, p0, LX/0Lq;->f:LX/0Od;

    invoke-virtual {v0}, LX/0Od;->g()V

    goto :goto_0
.end method

.method public final a(LX/0AY;III)V
    .locals 8

    .prologue
    .line 44443
    invoke-virtual {p1, p2}, LX/0AY;->a(I)LX/0Am;

    move-result-object v0

    iget-object v0, v0, LX/0Am;->c:Ljava/util/List;

    .line 44444
    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    .line 44445
    iget-object v1, v0, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Ah;

    iget-object v2, v1, LX/0Ah;->c:LX/0AR;

    .line 44446
    invoke-static {v2}, LX/0Lq;->a(LX/0AR;)Ljava/lang/String;

    move-result-object v3

    .line 44447
    if-nez v3, :cond_0

    .line 44448
    const-string v0, "DashChunkSource"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Skipped track "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (unknown media mime type)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 44449
    :goto_0
    return-void

    .line 44450
    :cond_0
    iget v4, v0, LX/0Ak;->b:I

    iget-boolean v0, p1, LX/0AY;->e:Z

    if-eqz v0, :cond_1

    const-wide/16 v0, -0x1

    :goto_1
    invoke-static {v4, v2, v3, v0, v1}, LX/0Lq;->a(ILX/0AR;Ljava/lang/String;J)LX/0L4;

    move-result-object v0

    .line 44451
    if-nez v0, :cond_2

    .line 44452
    const-string v0, "DashChunkSource"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Skipped track "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (unknown media format)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 44453
    :cond_1
    iget-wide v0, p1, LX/0AY;->c:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v0, v6

    goto :goto_1

    .line 44454
    :cond_2
    iget-object v1, p0, LX/0Lq;->h:Ljava/util/ArrayList;

    new-instance v3, LX/0Lk;

    invoke-direct {v3, v0, p3, v2}, LX/0Lk;-><init>(LX/0L4;ILX/0AR;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(LX/0AY;II[I)V
    .locals 10

    .prologue
    .line 44349
    iget-object v0, p0, LX/0Lq;->d:LX/04o;

    if-nez v0, :cond_0

    .line 44350
    const-string v0, "DashChunkSource"

    const-string v1, "Skipping adaptive track (missing format evaluator)"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 44351
    :goto_0
    return-void

    .line 44352
    :cond_0
    invoke-virtual {p1, p2}, LX/0AY;->a(I)LX/0Am;

    move-result-object v0

    iget-object v0, v0, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    .line 44353
    const/4 v4, 0x0

    .line 44354
    const/4 v5, 0x0

    .line 44355
    const/4 v2, 0x0

    .line 44356
    array-length v1, p4

    new-array v3, v1, [LX/0AR;

    .line 44357
    const/4 v1, 0x0

    move v7, v1

    :goto_1
    array-length v1, v3

    if-ge v7, v1, :cond_2

    .line 44358
    iget-object v1, v0, LX/0Ak;->c:Ljava/util/List;

    aget v6, p4, v7

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Ah;

    iget-object v6, v1, LX/0Ah;->c:LX/0AR;

    .line 44359
    if-eqz v2, :cond_1

    iget v1, v6, LX/0AR;->g:I

    if-le v1, v5, :cond_6

    :cond_1
    move-object v1, v6

    .line 44360
    :goto_2
    iget v2, v6, LX/0AR;->f:I

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 44361
    iget v2, v6, LX/0AR;->g:I

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 44362
    aput-object v6, v3, v7

    .line 44363
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move-object v2, v1

    goto :goto_1

    .line 44364
    :cond_2
    new-instance v1, LX/0Lc;

    invoke-direct {v1}, LX/0Lc;-><init>()V

    invoke-static {v3, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 44365
    iget-boolean v1, p0, LX/0Lq;->n:Z

    if-eqz v1, :cond_3

    const-wide/16 v6, -0x1

    .line 44366
    :goto_3
    invoke-static {v2}, LX/0Lq;->a(LX/0AR;)Ljava/lang/String;

    move-result-object v1

    .line 44367
    if-nez v1, :cond_4

    .line 44368
    const-string v0, "DashChunkSource"

    const-string v1, "Skipped adaptive track (unknown media mime type)"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 44369
    :cond_3
    iget-wide v6, p1, LX/0AY;->c:J

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    goto :goto_3

    .line 44370
    :cond_4
    iget v0, v0, LX/0Ak;->b:I

    invoke-static {v0, v2, v1, v6, v7}, LX/0Lq;->a(ILX/0AR;Ljava/lang/String;J)LX/0L4;

    move-result-object v1

    .line 44371
    if-nez v1, :cond_5

    .line 44372
    const-string v0, "DashChunkSource"

    const-string v1, "Skipped adaptive track (unknown media format)"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 44373
    :cond_5
    iget-object v6, p0, LX/0Lq;->h:Ljava/util/ArrayList;

    new-instance v0, LX/0Lk;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0L4;->b(Ljava/lang/String;)LX/0L4;

    move-result-object v1

    move v2, p3

    invoke-direct/range {v0 .. v5}, LX/0Lk;-><init>(LX/0L4;I[LX/0AR;II)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    move-object v1, v2

    goto :goto_2
.end method

.method public final a(LX/0LP;)V
    .locals 5

    .prologue
    .line 44455
    instance-of v0, p1, LX/0Lg;

    if-eqz v0, :cond_0

    .line 44456
    check-cast p1, LX/0Lg;

    .line 44457
    iget-object v0, p1, LX/0LP;->d:LX/0AR;

    iget-object v1, v0, LX/0AR;->a:Ljava/lang/String;

    .line 44458
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    iget v2, p1, LX/0LP;->f:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ln;

    .line 44459
    if-nez v0, :cond_1

    .line 44460
    :cond_0
    :goto_0
    return-void

    .line 44461
    :cond_1
    iget-object v2, v0, LX/0Ln;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Lo;

    .line 44462
    iget-object v2, p1, LX/0Lg;->h:LX/0L4;

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 44463
    if-eqz v2, :cond_2

    .line 44464
    iget-object v2, p1, LX/0Lg;->h:LX/0L4;

    move-object v2, v2

    .line 44465
    iput-object v2, v1, LX/0Lo;->e:LX/0L4;

    .line 44466
    :cond_2
    iget-object v2, v1, LX/0Lo;->d:LX/0BX;

    if-nez v2, :cond_3

    .line 44467
    iget-object v2, p1, LX/0Lg;->j:LX/0M8;

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 44468
    if-eqz v2, :cond_3

    .line 44469
    new-instance v3, LX/0Ls;

    .line 44470
    iget-object v2, p1, LX/0Lg;->j:LX/0M8;

    move-object v2, v2

    .line 44471
    check-cast v2, LX/0M9;

    iget-object v4, p1, LX/0LP;->e:LX/0OA;

    iget-object v4, v4, LX/0OA;->a:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v2, v4}, LX/0Ls;-><init>(LX/0M9;Ljava/lang/String;)V

    iput-object v3, v1, LX/0Lo;->d:LX/0BX;

    .line 44472
    :cond_3
    iget-object v1, v0, LX/0Ln;->e:LX/0Lz;

    if-nez v1, :cond_0

    .line 44473
    iget-object v1, p1, LX/0Lg;->i:LX/0Lz;

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 44474
    if-eqz v1, :cond_0

    .line 44475
    iget-object v1, p1, LX/0Lg;->i:LX/0Lz;

    move-object v1, v1

    .line 44476
    iput-object v1, v0, LX/0Ln;->e:LX/0Lz;

    .line 44477
    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public final a(Ljava/util/List;JLX/0LW;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/0LQ;",
            ">;J",
            "LX/0LW;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44478
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->H:Ljava/io/IOException;

    if-eqz v4, :cond_1

    .line 44479
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, LX/0LW;->b:LX/0LP;

    .line 44480
    :cond_0
    :goto_0
    return-void

    .line 44481
    :cond_1
    const/4 v4, 0x0

    .line 44482
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->e:LX/0Ld;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v6

    iput v6, v5, LX/0Ld;->a:I

    .line 44483
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->e:LX/0Ld;

    iget-object v5, v5, LX/0Ld;->c:LX/0AR;

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/0Lq;->E:Z

    if-nez v5, :cond_5

    .line 44484
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->z:LX/0Lk;

    invoke-virtual {v5}, LX/0Lk;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 44485
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->e:LX/0Ld;

    iget-object v5, v5, LX/0Ld;->c:LX/0AR;

    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/0Lq;->w:Z

    if-nez v5, :cond_5

    .line 44486
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->d:LX/04o;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->z:LX/0Lk;

    iget-object v8, v5, LX/0Lk;->f:[LX/0AR;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0Lq;->e:LX/0Ld;

    move-object/from16 v5, p1

    move-wide/from16 v6, p2

    invoke-interface/range {v4 .. v9}, LX/04o;->a(Ljava/util/List;J[LX/0AR;LX/0Ld;)V

    .line 44487
    const/4 v4, 0x1

    move v8, v4

    .line 44488
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->e:LX/0Ld;

    iget-object v12, v4, LX/0Ld;->c:LX/0AR;

    .line 44489
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->e:LX/0Ld;

    iget v4, v4, LX/0Ld;->a:I

    move-object/from16 v0, p4

    iput v4, v0, LX/0LW;->a:I

    .line 44490
    if-nez v12, :cond_6

    .line 44491
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, LX/0LW;->b:LX/0LP;

    goto :goto_0

    .line 44492
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->e:LX/0Ld;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0Lq;->z:LX/0Lk;

    iget-object v6, v6, LX/0Lk;->e:LX/0AR;

    iput-object v6, v5, LX/0Ld;->c:LX/0AR;

    .line 44493
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->e:LX/0Ld;

    const/4 v6, 0x2

    iput v6, v5, LX/0Ld;->b:I

    :cond_5
    move v8, v4

    goto :goto_1

    .line 44494
    :cond_6
    move-object/from16 v0, p4

    iget v4, v0, LX/0LW;->a:I

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    if-ne v4, v5, :cond_7

    move-object/from16 v0, p4

    iget-object v4, v0, LX/0LW;->b:LX/0LP;

    if-eqz v4, :cond_7

    move-object/from16 v0, p4

    iget-object v4, v0, LX/0LW;->b:LX/0LP;

    iget-object v4, v4, LX/0LP;->d:LX/0AR;

    invoke-virtual {v4, v12}, LX/0AR;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 44495
    :cond_7
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, LX/0LW;->b:LX/0LP;

    .line 44496
    const/4 v10, 0x0

    .line 44497
    const/4 v7, 0x0

    .line 44498
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->B:LX/0LB;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->m:[J

    invoke-interface {v4, v5}, LX/0LB;->b([J)[J

    .line 44499
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 44500
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0Lq;->n:Z

    if-eqz v4, :cond_8

    .line 44501
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0Lq;->D:Z

    if-eqz v4, :cond_d

    .line 44502
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->m:[J

    const/4 v5, 0x0

    aget-wide v4, v4, v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0Lq;->m:[J

    const/4 v7, 0x1

    aget-wide v6, v6, v7

    move-object/from16 v0, p0

    iget-wide v10, v0, LX/0Lq;->k:J

    sub-long/2addr v6, v10

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p2

    .line 44503
    :cond_8
    :goto_2
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2}, LX/0Lq;->c(J)LX/0Ln;

    move-result-object v4

    .line 44504
    const/4 v10, 0x1

    move-object v9, v4

    move-wide/from16 v6, p2

    .line 44505
    :goto_3
    if-nez v8, :cond_9

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0Lq;->E:Z

    if-eqz v4, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->z:LX/0Lk;

    invoke-virtual {v4}, LX/0Lk;->a()Z

    move-result v4

    if-eqz v4, :cond_19

    .line 44506
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->d:LX/04o;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->z:LX/0Lk;

    iget-object v8, v5, LX/0Lk;->f:[LX/0AR;

    move-object/from16 v5, p1

    move-object/from16 v11, p0

    invoke-interface/range {v4 .. v11}, LX/04o;->a(Ljava/util/List;J[LX/0AR;LX/0Ln;ZLX/0Lq;)LX/0AR;

    move-result-object v4

    .line 44507
    if-eqz v4, :cond_19

    .line 44508
    :goto_4
    iget-object v5, v9, LX/0Ln;->c:Ljava/util/HashMap;

    iget-object v4, v4, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, LX/0Lo;

    .line 44509
    iget-object v11, v13, LX/0Lo;->c:LX/0Ah;

    .line 44510
    const/4 v4, 0x0

    .line 44511
    const/4 v5, 0x0

    .line 44512
    iget-object v8, v13, LX/0Lo;->e:LX/0L4;

    .line 44513
    if-nez v8, :cond_a

    .line 44514
    invoke-virtual {v11}, LX/0Ah;->c()LX/0Au;

    move-result-object v4

    .line 44515
    :cond_a
    iget-object v12, v13, LX/0Lo;->d:LX/0BX;

    if-nez v12, :cond_b

    .line 44516
    invoke-virtual {v11}, LX/0Ah;->e()LX/0Au;

    move-result-object v5

    .line 44517
    :cond_b
    if-nez v4, :cond_c

    if-eqz v5, :cond_14

    .line 44518
    :cond_c
    iget-object v7, v13, LX/0Lo;->b:LX/0LV;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/0Lq;->c:LX/0G6;

    iget v9, v9, LX/0Ln;->a:I

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0Lq;->e:LX/0Ld;

    iget v10, v6, LX/0Ld;->b:I

    move-object v6, v11

    invoke-static/range {v4 .. v10}, LX/0Lq;->a(LX/0Au;LX/0Au;LX/0Ah;LX/0LV;LX/0G6;II)LX/0LP;

    move-result-object v4

    .line 44519
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, LX/0Lq;->E:Z

    .line 44520
    move-object/from16 v0, p4

    iput-object v4, v0, LX/0LW;->b:LX/0LP;

    goto/16 :goto_0

    .line 44521
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->m:[J

    const/4 v5, 0x1

    aget-wide v4, v4, v5

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    move-wide/from16 v0, p2

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 44522
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0Lq;->m:[J

    const/4 v7, 0x0

    aget-wide v6, v6, v7

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p2

    goto/16 :goto_2

    .line 44523
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0Lq;->D:Z

    if-eqz v4, :cond_f

    .line 44524
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/0Lq;->D:Z

    .line 44525
    :cond_f
    move-object/from16 v0, p4

    iget v4, v0, LX/0LW;->a:I

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0LQ;

    .line 44526
    iget-wide v14, v4, LX/0LQ;->i:J

    .line 44527
    const/4 v6, 0x0

    .line 44528
    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/0Lq;->n:Z

    if-eqz v5, :cond_11

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->m:[J

    const/4 v9, 0x0

    aget-wide v16, v5, v9

    cmp-long v5, v14, v16

    if-gez v5, :cond_11

    .line 44529
    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/0Lq;->p:Z

    if-eqz v5, :cond_10

    .line 44530
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Discontinuity detected for live: nextSegmentStartTimeUs is "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", availableRangeStartTimeUs is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0Lq;->m:[J

    const/4 v7, 0x0

    aget-wide v6, v6, v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", representation id is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v12, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44531
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->m:[J

    const/4 v6, 0x1

    aget-wide v6, v5, v6

    const-wide/16 v10, 0x1

    sub-long/2addr v6, v10

    move-wide/from16 v0, p2

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 44532
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->m:[J

    const/4 v9, 0x0

    aget-wide v10, v5, v9

    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p2

    .line 44533
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2}, LX/0Lq;->c(J)LX/0Ln;

    move-result-object v6

    .line 44534
    const/4 v10, 0x1

    .line 44535
    const/4 v5, 0x1

    move/from16 v19, v5

    move-object v5, v6

    move/from16 v6, v19

    .line 44536
    :goto_5
    if-nez v6, :cond_1a

    .line 44537
    const/4 v10, 0x0

    .line 44538
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->i:Landroid/util/SparseArray;

    iget v6, v4, LX/0LP;->f:I

    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Ln;

    .line 44539
    if-nez v5, :cond_13

    .line 44540
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->i:Landroid/util/SparseArray;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Ln;

    .line 44541
    const/4 v10, 0x1

    move-object v9, v4

    move-wide/from16 v6, p2

    goto/16 :goto_3

    .line 44542
    :cond_10
    new-instance v4, LX/0Kn;

    invoke-direct {v4}, LX/0Kn;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, LX/0Lq;->H:Ljava/io/IOException;

    goto/16 :goto_0

    .line 44543
    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->x:LX/0AY;

    iget-boolean v5, v5, LX/0AY;->e:Z

    if-eqz v5, :cond_12

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->m:[J

    const/4 v9, 0x1

    aget-wide v16, v5, v9

    cmp-long v5, v14, v16

    if-ltz v5, :cond_12

    .line 44544
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/0Lq;->F:Z

    .line 44545
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0Lq;->G:Z

    if-nez v4, :cond_0

    .line 44546
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/0Lq;->G:Z

    goto/16 :goto_0

    .line 44547
    :cond_12
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->i:Landroid/util/SparseArray;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v9}, Landroid/util/SparseArray;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v5, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Ln;

    .line 44548
    iget v9, v4, LX/0LP;->f:I

    iget v11, v5, LX/0Ln;->a:I

    if-ne v9, v11, :cond_1b

    .line 44549
    iget-object v5, v5, LX/0Ln;->c:Ljava/util/HashMap;

    iget-object v9, v4, LX/0LP;->d:LX/0AR;

    iget-object v9, v9, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Lo;

    .line 44550
    invoke-virtual {v4}, LX/0LQ;->i()I

    move-result v9

    invoke-virtual {v5, v9}, LX/0Lo;->c(I)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 44551
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->x:LX/0AY;

    iget-boolean v4, v4, LX/0AY;->e:Z

    if-nez v4, :cond_0

    .line 44552
    const/4 v4, 0x1

    move-object/from16 v0, p4

    iput-boolean v4, v0, LX/0LW;->c:Z

    goto/16 :goto_0

    .line 44553
    :cond_13
    invoke-virtual {v5}, LX/0Ln;->c()Z

    move-result v6

    if-nez v6, :cond_1a

    .line 44554
    iget-object v6, v5, LX/0Ln;->c:Ljava/util/HashMap;

    iget-object v7, v4, LX/0LP;->d:LX/0AR;

    iget-object v7, v7, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0Lo;

    .line 44555
    invoke-virtual {v4}, LX/0LQ;->i()I

    move-result v7

    invoke-virtual {v6, v7}, LX/0Lo;->c(I)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 44556
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0Lq;->i:Landroid/util/SparseArray;

    iget v4, v4, LX/0LP;->f:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Ln;

    .line 44557
    const/4 v10, 0x1

    move-object v9, v4

    move-wide/from16 v6, p2

    goto/16 :goto_3

    :cond_14
    move-object/from16 v12, p0

    move-wide v14, v6

    move/from16 v16, v10

    move-object/from16 v17, p1

    .line 44558
    invoke-virtual/range {v12 .. v17}, LX/0Lq;->a(LX/0Lo;JZLjava/util/List;)I

    move-result v17

    .line 44559
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/0Lq;->t:J

    const-wide/16 v14, -0xa

    cmp-long v4, v4, v14

    if-nez v4, :cond_15

    invoke-virtual {v13}, LX/0Lo;->b()I

    move-result v4

    move/from16 v0, v17

    if-le v0, v4, :cond_15

    .line 44560
    invoke-virtual {v13}, LX/0Lo;->b()I

    move-result v17

    .line 44561
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0Lq;->n:Z

    if-eqz v4, :cond_16

    invoke-virtual {v13}, LX/0Lo;->b()I

    move-result v4

    move/from16 v0, v17

    if-ne v0, v4, :cond_16

    .line 44562
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/0Lq;->F:Z

    .line 44563
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/0Lq;->G:Z

    if-nez v4, :cond_16

    .line 44564
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/0Lq;->G:Z

    .line 44565
    :cond_16
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, LX/0Lq;->c:LX/0G6;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0Lq;->z:LX/0Lk;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Lq;->e:LX/0Ld;

    iget v0, v4, LX/0Ld;->b:I

    move/from16 v18, v0

    move-object v12, v9

    move-object v15, v8

    invoke-static/range {v12 .. v18}, LX/0Lq;->a(LX/0Ln;LX/0Lo;LX/0G6;LX/0L4;LX/0Lk;II)LX/0LP;
    :try_end_0
    .catch LX/0Ll; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 44566
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, LX/0Lq;->E:Z

    .line 44567
    move-object/from16 v0, p4

    iput-object v4, v0, LX/0LW;->b:LX/0LP;

    goto/16 :goto_0

    .line 44568
    :catch_0
    move-exception v4

    .line 44569
    move-object/from16 v0, p0

    iput-object v4, v0, LX/0Lq;->H:Ljava/io/IOException;

    goto/16 :goto_0

    .line 44570
    :catch_1
    move-exception v4

    move-object v5, v4

    .line 44571
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 44572
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v9, "useSegmentShift="

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v9, v0, LX/0Lq;->u:Z

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44573
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v9, ", segmentShift="

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, v13, LX/0Lo;->f:I

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44574
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v9, ", segmentNum="

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44575
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v9, ", firstAvaiable="

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, LX/0Lo;->a()I

    move-result v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44576
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v9, ", lastAvaiable="

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, LX/0Lo;->b()I

    move-result v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44577
    iget-object v4, v13, LX/0Lo;->c:LX/0Ah;

    instance-of v4, v4, LX/0BW;

    if-eqz v4, :cond_17

    .line 44578
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v4, ", "

    invoke-direct {v9, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v13, LX/0Lo;->c:LX/0Ah;

    check-cast v4, LX/0BW;

    invoke-virtual {v4}, LX/0BW;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44579
    :cond_17
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_18

    .line 44580
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v4, ", nextChunkIndex="

    invoke-direct {v9, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0LQ;

    invoke-virtual {v4}, LX/0LQ;->i()I

    move-result v4

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44581
    :cond_18
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v9, ", playbackPositionUs="

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44582
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, ", startingNewPeriod="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44583
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, ", innerMessage="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44584
    new-instance v4, Ljava/lang/IndexOutOfBoundsException;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_19
    move-object v4, v12

    goto/16 :goto_4

    :cond_1a
    move-object v9, v5

    move-wide/from16 v6, p2

    goto/16 :goto_3

    :cond_1b
    move-object v5, v7

    goto/16 :goto_5
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 44585
    iget-object v0, p0, LX/0Lq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Lk;

    iput-object v0, p0, LX/0Lq;->z:LX/0Lk;

    .line 44586
    iget-object v0, p0, LX/0Lq;->z:LX/0Lk;

    invoke-virtual {v0}, LX/0Lk;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44587
    iget-object v0, p0, LX/0Lq;->d:LX/04o;

    invoke-interface {v0}, LX/04o;->a()V

    .line 44588
    :cond_0
    iget-object v0, p0, LX/0Lq;->f:LX/0Od;

    if-eqz v0, :cond_2

    .line 44589
    iget-object v0, p0, LX/0Lq;->f:LX/0Od;

    .line 44590
    iget v1, v0, LX/0Od;->f:I

    add-int/lit8 p1, v1, 0x1

    iput p1, v0, LX/0Od;->f:I

    if-nez v1, :cond_1

    .line 44591
    const/4 v1, 0x0

    iput v1, v0, LX/0Od;->j:I

    .line 44592
    const/4 v1, 0x0

    iput-object v1, v0, LX/0Od;->l:LX/0Ob;

    .line 44593
    :cond_1
    iget-object v0, p0, LX/0Lq;->f:LX/0Od;

    .line 44594
    iget-object v1, v0, LX/0Od;->m:Ljava/lang/Object;

    move-object v0, v1

    .line 44595
    check-cast v0, LX/0AY;

    invoke-direct {p0, v0}, LX/0Lq;->b(LX/0AY;)V

    .line 44596
    :goto_0
    return-void

    .line 44597
    :cond_2
    iget-object v0, p0, LX/0Lq;->x:LX/0AY;

    invoke-direct {p0, v0}, LX/0Lq;->b(LX/0AY;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 44598
    iget-boolean v2, p0, LX/0Lq;->C:Z

    if-nez v2, :cond_0

    .line 44599
    iput-boolean v0, p0, LX/0Lq;->C:Z

    .line 44600
    :try_start_0
    iget-object v2, p0, LX/0Lq;->g:LX/0Lr;

    iget-object v3, p0, LX/0Lq;->x:LX/0AY;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4, p0}, LX/0Lr;->a(LX/0AY;ILX/0Lp;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44601
    :cond_0
    :goto_0
    iget-object v2, p0, LX/0Lq;->H:Ljava/io/IOException;

    if-nez v2, :cond_1

    :goto_1
    return v0

    .line 44602
    :catch_0
    move-exception v2

    .line 44603
    iput-object v2, p0, LX/0Lq;->H:Ljava/io/IOException;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 44604
    goto :goto_1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 44607
    iget-object v0, p0, LX/0Lq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 44608
    iget-object v0, p0, LX/0Lq;->z:LX/0Lk;

    invoke-virtual {v0}, LX/0Lk;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44609
    iget-object v0, p0, LX/0Lq;->d:LX/04o;

    invoke-interface {v0}, LX/04o;->b()V

    .line 44610
    :cond_0
    iget-object v0, p0, LX/0Lq;->f:LX/0Od;

    if-eqz v0, :cond_1

    .line 44611
    iget-object v0, p0, LX/0Lq;->f:LX/0Od;

    .line 44612
    iget v2, v0, LX/0Od;->f:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, LX/0Od;->f:I

    if-nez v2, :cond_1

    .line 44613
    iget-object v2, v0, LX/0Od;->g:LX/0ON;

    if-eqz v2, :cond_1

    .line 44614
    iget-object v2, v0, LX/0Od;->g:LX/0ON;

    invoke-virtual {v2}, LX/0ON;->c()V

    .line 44615
    const/4 v2, 0x0

    iput-object v2, v0, LX/0Od;->g:LX/0ON;

    .line 44616
    :cond_1
    iget-object v0, p0, LX/0Lq;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 44617
    iget-object v0, p0, LX/0Lq;->e:LX/0Ld;

    iput-object v1, v0, LX/0Ld;->c:LX/0AR;

    .line 44618
    iput-object v1, p0, LX/0Lq;->B:LX/0LB;

    .line 44619
    iput-object v1, p0, LX/0Lq;->H:Ljava/io/IOException;

    .line 44620
    iput-object v1, p0, LX/0Lq;->z:LX/0Lk;

    .line 44621
    return-void
.end method
