.class public final LX/075;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/072;


# direct methods
.method public constructor <init>(LX/072;)V
    .locals 0

    .prologue
    .line 19022
    iput-object p1, p0, LX/075;->a:LX/072;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    .line 19023
    if-ltz p1, :cond_0

    .line 19024
    iget-object v0, p0, LX/075;->a:LX/072;

    iget-object v0, v0, LX/072;->d:LX/05i;

    const-class v1, LX/06s;

    invoke-virtual {v0, v1}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06s;

    int-to-long v2, p1

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, LX/075;->a:LX/072;

    iget-object v5, v5, LX/072;->A:Ljava/lang/String;

    aput-object v5, v1, v4

    const/4 v4, 0x1

    const-string v5, "m"

    aput-object v5, v1, v4

    const/4 v4, 0x2

    const-string v5, "s"

    aput-object v5, v1, v4

    const/4 v4, 0x3

    const-string v5, "b"

    aput-object v5, v1, v4

    invoke-virtual {v0, v2, v3, v1}, LX/06t;->a(J[Ljava/lang/String;)LX/06t;

    .line 19025
    sget-object v0, LX/07M;->a:LX/07M;

    move-object v0, v0

    .line 19026
    int-to-long v2, p1

    .line 19027
    iget-object v1, v0, LX/07M;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 19028
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 19029
    iget-object v0, p0, LX/075;->a:LX/072;

    iget-object v0, v0, LX/072;->d:LX/05i;

    const-class v1, LX/06s;

    invoke-virtual {v0, v1}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06s;

    const-wide/16 v2, 0x1

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, LX/075;->a:LX/072;

    iget-object v5, v5, LX/072;->A:Ljava/lang/String;

    aput-object v5, v1, v4

    const-string v4, "m"

    aput-object v4, v1, v6

    const/4 v4, 0x2

    const-string v5, "s"

    aput-object v5, v1, v4

    const/4 v4, 0x3

    const-string v5, "c"

    aput-object v5, v1, v4

    invoke-virtual {v0, v2, v3, v1}, LX/06t;->a(J[Ljava/lang/String;)LX/06t;

    .line 19030
    iget-object v0, p0, LX/075;->a:LX/072;

    iget-object v0, v0, LX/072;->E:LX/077;

    .line 19031
    if-eqz v0, :cond_0

    .line 19032
    int-to-long v2, p2

    invoke-virtual {v0, p1, v2, v3, v6}, LX/077;->a(Ljava/lang/String;JZ)V

    .line 19033
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 19034
    iget-object v0, p0, LX/075;->a:LX/072;

    iget-object v0, v0, LX/072;->d:LX/05i;

    const-class v1, LX/06s;

    invoke-virtual {v0, v1}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06s;

    const-wide/16 v2, 0x1

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    iget-object v4, p0, LX/075;->a:LX/072;

    iget-object v4, v4, LX/072;->A:Ljava/lang/String;

    aput-object v4, v1, v6

    const-string v4, "m"

    aput-object v4, v1, v7

    const/4 v4, 0x2

    const-string v5, "r"

    aput-object v5, v1, v4

    const/4 v4, 0x3

    const-string v5, "c"

    aput-object v5, v1, v4

    invoke-virtual {v0, v2, v3, v1}, LX/06t;->a(J[Ljava/lang/String;)LX/06t;

    .line 19035
    iget-object v0, p0, LX/075;->a:LX/072;

    iget-object v0, v0, LX/072;->d:LX/05i;

    iget-object v1, p0, LX/075;->a:LX/072;

    iget-object v1, v1, LX/072;->A:Ljava/lang/String;

    invoke-virtual {v0, p1, p2, v1, v7}, LX/05i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 19036
    iget-object v0, p0, LX/075;->a:LX/072;

    iget-object v0, v0, LX/072;->k:LX/05k;

    invoke-virtual {v0}, LX/05k;->a()V

    .line 19037
    iget-object v0, p0, LX/075;->a:LX/072;

    iget-object v0, v0, LX/072;->E:LX/077;

    .line 19038
    if-eqz v0, :cond_0

    .line 19039
    invoke-static {p2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 19040
    :goto_0
    int-to-long v2, p3

    invoke-virtual {v0, p1, v2, v3, v6}, LX/077;->a(Ljava/lang/String;JZ)V

    .line 19041
    :cond_0
    return-void

    :cond_1
    move-object p1, p2

    .line 19042
    goto :goto_0
.end method

.method public final b(I)V
    .locals 6

    .prologue
    .line 19043
    if-ltz p1, :cond_0

    .line 19044
    iget-object v0, p0, LX/075;->a:LX/072;

    iget-object v0, v0, LX/072;->d:LX/05i;

    const-class v1, LX/06s;

    invoke-virtual {v0, v1}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06s;

    int-to-long v2, p1

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, LX/075;->a:LX/072;

    iget-object v5, v5, LX/072;->A:Ljava/lang/String;

    aput-object v5, v1, v4

    const/4 v4, 0x1

    const-string v5, "m"

    aput-object v5, v1, v4

    const/4 v4, 0x2

    const-string v5, "r"

    aput-object v5, v1, v4

    const/4 v4, 0x3

    const-string v5, "b"

    aput-object v5, v1, v4

    invoke-virtual {v0, v2, v3, v1}, LX/06t;->a(J[Ljava/lang/String;)LX/06t;

    .line 19045
    sget-object v0, LX/07M;->a:LX/07M;

    move-object v0, v0

    .line 19046
    int-to-long v2, p1

    .line 19047
    iget-object v1, v0, LX/07M;->e:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 19048
    :cond_0
    return-void
.end method
