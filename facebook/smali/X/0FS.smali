.class public final LX/0FS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final dexStoreRoot:Ljava/lang/String;

.field public final replyTo:Landroid/os/Messenger;

.field public final startId:I


# direct methods
.method public constructor <init>(Landroid/os/Bundle;I)V
    .locals 1

    .prologue
    .line 33222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33223
    iput p2, p0, LX/0FS;->startId:I

    .line 33224
    const-string v0, "client"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    iput-object v0, p0, LX/0FS;->replyTo:Landroid/os/Messenger;

    .line 33225
    iget-object v0, p0, LX/0FS;->replyTo:Landroid/os/Messenger;

    if-nez v0, :cond_0

    .line 33226
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33227
    :cond_0
    const-string v0, "dexStoreRoot"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0FS;->dexStoreRoot:Ljava/lang/String;

    .line 33228
    iget-object v0, p0, LX/0FS;->dexStoreRoot:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 33229
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33230
    :cond_1
    return-void
.end method


# virtual methods
.method public final getShortName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 33231
    iget-object v0, p0, LX/0FS;->dexStoreRoot:Ljava/lang/String;

    iget-object v1, p0, LX/0FS;->dexStoreRoot:Ljava/lang/String;

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
