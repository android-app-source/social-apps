.class public LX/0Go;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:Z


# instance fields
.field private b:I

.field private c:I

.field public d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/LruCache",
            "<",
            "LX/0Gn;",
            "LX/0G6;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "LX/0Gn;",
            "LX/0G6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36479
    const-class v0, LX/0Go;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/0Go;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 36473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36474
    iput p1, p0, LX/0Go;->b:I

    .line 36475
    iput p2, p0, LX/0Go;->c:I

    .line 36476
    invoke-direct {p0, p1}, LX/0Go;->a(I)Landroid/util/LruCache;

    move-result-object v0

    iput-object v0, p0, LX/0Go;->e:Landroid/util/LruCache;

    .line 36477
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0Go;->d:Ljava/util/HashMap;

    .line 36478
    return-void
.end method

.method private a(I)Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/LruCache",
            "<",
            "LX/0Gn;",
            "LX/0G6;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36472
    new-instance v0, LX/0Gl;

    invoke-direct {v0, p0, p1}, LX/0Gl;-><init>(LX/0Go;I)V

    return-object v0
.end method

.method private static b(LX/0Go;I)Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/LruCache",
            "<",
            "LX/0Gn;",
            "LX/0G6;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36480
    new-instance v0, LX/0Gm;

    invoke-direct {v0, p0, p1}, LX/0Gm;-><init>(LX/0Go;I)V

    return-object v0
.end method

.method public static b(LX/0G6;)V
    .locals 1

    .prologue
    .line 36470
    :try_start_0
    invoke-interface {p0}, LX/0G6;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36471
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(LX/0Gn;)LX/0G6;
    .locals 3

    .prologue
    .line 36463
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Go;->e:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0G6;

    .line 36464
    if-eqz v0, :cond_1

    .line 36465
    iget-object v1, p0, LX/0Go;->d:Ljava/util/HashMap;

    iget-object v2, p1, LX/0Gn;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/LruCache;

    .line 36466
    sget-boolean v2, LX/0Go;->a:Z

    if-nez v2, :cond_0

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36467
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 36468
    :cond_0
    :try_start_1
    invoke-virtual {v1, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 36469
    :cond_1
    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a(II)V
    .locals 7

    .prologue
    .line 36445
    monitor-enter p0

    if-lez p1, :cond_0

    if-gtz p2, :cond_1

    .line 36446
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 36447
    :cond_1
    :try_start_0
    iget v0, p0, LX/0Go;->c:I

    if-eq p2, v0, :cond_4

    .line 36448
    iget-object v0, p0, LX/0Go;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 36449
    invoke-static {p0, p2}, LX/0Go;->b(LX/0Go;I)Landroid/util/LruCache;

    move-result-object v3

    .line 36450
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/LruCache;

    invoke-virtual {v1}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v4

    .line 36451
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Gn;

    .line 36452
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v3, v1, v6}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 36453
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 36454
    :cond_2
    :try_start_1
    iget-object v1, p0, LX/0Go;->d:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 36455
    :cond_3
    iput p2, p0, LX/0Go;->c:I

    .line 36456
    :cond_4
    iget v0, p0, LX/0Go;->b:I

    if-eq p1, v0, :cond_0

    .line 36457
    invoke-direct {p0, p1}, LX/0Go;->a(I)Landroid/util/LruCache;

    move-result-object v1

    .line 36458
    iget-object v0, p0, LX/0Go;->e:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v2

    .line 36459
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Gn;

    .line 36460
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 36461
    :cond_5
    iput-object v1, p0, LX/0Go;->e:Landroid/util/LruCache;

    .line 36462
    iput p1, p0, LX/0Go;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public final declared-synchronized a(LX/0Gn;LX/0G6;)V
    .locals 3

    .prologue
    .line 36437
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Go;->d:Ljava/util/HashMap;

    iget-object v1, p1, LX/0Gn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LruCache;

    .line 36438
    if-nez v0, :cond_0

    .line 36439
    iget v0, p0, LX/0Go;->c:I

    invoke-static {p0, v0}, LX/0Go;->b(LX/0Go;I)Landroid/util/LruCache;

    move-result-object v0

    .line 36440
    iget-object v1, p0, LX/0Go;->d:Ljava/util/HashMap;

    iget-object v2, p1, LX/0Gn;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36441
    :cond_0
    invoke-virtual {v0, p1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36442
    iget-object v0, p0, LX/0Go;->e:Landroid/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36443
    monitor-exit p0

    return-void

    .line 36444
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/0Gn;)LX/0G6;
    .locals 3

    .prologue
    .line 36430
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Go;->e:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0G6;

    .line 36431
    if-eqz v0, :cond_1

    .line 36432
    iget-object v1, p0, LX/0Go;->d:Ljava/util/HashMap;

    iget-object v2, p1, LX/0Gn;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/LruCache;

    .line 36433
    sget-boolean v2, LX/0Go;->a:Z

    if-nez v2, :cond_0

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36434
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 36435
    :cond_0
    :try_start_1
    invoke-virtual {v1, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 36436
    :cond_1
    monitor-exit p0

    return-object v0
.end method
