.class public LX/00p;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static b:Lcom/facebook/analytics/logger/HoneyClientEvent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public static c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public static d:Ljava/lang/Throwable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3039
    const-class v0, LX/00p;

    sput-object v0, LX/00p;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3038
    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 2944
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2945
    const-string v1, "app"

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2946
    const-string v1, "sdk_version"

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2947
    const-string v1, "fingerprint"

    sget-object v2, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2948
    const-string v1, "model"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2949
    const-string v1, "device"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2950
    const-string v1, "manufacturer"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2951
    const-string v1, "cpu_abi"

    sget-object v2, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2952
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 4

    .prologue
    .line 3031
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "dalvik_hack_telemetry"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3032
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3033
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 3034
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3035
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create dir: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3036
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2971
    sget-object v0, LX/03x;->NORMAL:LX/03x;

    .line 2972
    const/4 v1, 0x0

    .line 2973
    sget-object v5, LX/00s;->resultValue:LX/02j;

    move-object v5, v5

    .line 2974
    sget-object v6, LX/02j;->NOT_ATTEMPTED:LX/02j;

    if-eq v5, v6, :cond_d

    .line 2975
    :cond_0
    :goto_0
    move v1, v1

    .line 2976
    if-eqz v1, :cond_c

    .line 2977
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 2978
    :try_start_0
    const-string v5, "attempted"

    .line 2979
    invoke-static {p0, p1, v5}, LX/00p;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    .line 2980
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    move v5, v6

    .line 2981
    if-eqz v5, :cond_2

    .line 2982
    const-string v1, "attempted"

    const/4 v5, 0x0

    invoke-static {p0, p1, v1, v5}, LX/00p;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2983
    :try_start_1
    sget-object v0, LX/03x;->TEST:LX/03x;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move v3, v2

    move-object v2, v0

    move-object v0, v4

    .line 2984
    :goto_1
    if-eqz v0, :cond_4

    .line 2985
    const-string v1, "dalvik_hack_storage_failure"

    sput-object v1, LX/00p;->c:Ljava/lang/String;

    .line 2986
    sput-object v0, LX/00p;->d:Ljava/lang/Throwable;

    .line 2987
    const-string v1, "dalvik_hack_storage_failure"

    invoke-static {v1}, LX/00p;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2988
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 2989
    new-instance v3, Ljava/io/PrintWriter;

    invoke-direct {v3, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 2990
    invoke-virtual {v0, v3}, Ljava/io/IOException;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 2991
    const-string v3, "io_stack_trace"

    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2992
    const-string v2, "io_exception"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2993
    sput-object v1, LX/00p;->b:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2994
    sget-object v1, LX/00p;->a:Ljava/lang/Class;

    const-string v2, "IOException"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2995
    :cond_1
    :goto_2
    return-void

    .line 2996
    :cond_2
    :try_start_2
    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v1

    const v2, 0x3c23d70a    # 0.01f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    .line 2997
    const-string v1, "attempted"

    const/4 v2, 0x1

    invoke-static {p0, p1, v1, v2}, LX/00p;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2998
    sget-object v1, LX/00o;->FBANDROID_RELEASE:LX/00o;

    invoke-static {v1}, LX/00s;->replaceBuffer(LX/00o;)V

    .line 2999
    sget-object v0, LX/03x;->TEST:LX/03x;

    .line 3000
    const-string v1, "attempted"

    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2}, LX/00p;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_3
    move-object v2, v0

    move-object v0, v4

    .line 3001
    goto :goto_1

    .line 3002
    :catch_0
    move-exception v1

    move v2, v3

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_3
    move v3, v2

    move-object v2, v1

    .line 3003
    goto :goto_1

    .line 3004
    :cond_4
    invoke-static {v2}, LX/00p;->a(LX/03x;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3005
    if-nez v3, :cond_7

    .line 3006
    sget-object v0, LX/00s;->resultValue:LX/02j;

    move-object v0, v0

    .line 3007
    :goto_4
    sget-object v1, LX/03x;->NORMAL:LX/03x;

    if-ne v2, v1, :cond_8

    .line 3008
    sget-object v1, LX/02j;->FAILURE:LX/02j;

    if-ne v0, v1, :cond_b

    .line 3009
    const-string v1, "dalvik_hack_failure"

    invoke-static {v1}, LX/00p;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 3010
    const-string v4, "dalvik_hack_failure"

    sput-object v4, LX/00p;->c:Ljava/lang/String;

    .line 3011
    new-instance v4, Ljava/lang/Throwable;

    const-string v5, "Failed to apply dalvik hack."

    invoke-direct {v4, v5}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    sput-object v4, LX/00p;->d:Ljava/lang/Throwable;

    .line 3012
    :goto_5
    if-nez v1, :cond_5

    .line 3013
    const-string v1, "dalvik_hack_error"

    invoke-static {v1}, LX/00p;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 3014
    const-string v4, "report_type"

    invoke-virtual {v2}, LX/03x;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 3015
    const-string v2, "result"

    invoke-virtual {v0}, LX/02j;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 3016
    const-string v2, "dalvik_hack_error"

    sput-object v2, LX/00p;->c:Ljava/lang/String;

    .line 3017
    new-instance v2, Ljava/lang/Throwable;

    const-string v4, "Unexpected telemetry state."

    invoke-direct {v2, v4}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    sput-object v2, LX/00p;->d:Ljava/lang/Throwable;

    .line 3018
    :cond_5
    const-string v2, "app"

    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 3019
    sget-object v2, LX/02j;->FAILURE:LX/02j;

    if-ne v0, v2, :cond_6

    .line 3020
    const-string v2, "failure_string"

    if-nez v3, :cond_a

    invoke-static {}, LX/00s;->getFailureString()Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 3021
    :cond_6
    sput-object v1, LX/00p;->b:Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_2

    .line 3022
    :cond_7
    sget-object v0, LX/02j;->FAILURE:LX/02j;

    goto :goto_4

    .line 3023
    :cond_8
    sget-object v1, LX/02j;->SUCCESS:LX/02j;

    if-ne v0, v1, :cond_9

    .line 3024
    const-string v1, "dalvik_hack_telemetry_success"

    invoke-static {v1}, LX/00p;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    goto :goto_5

    .line 3025
    :cond_9
    sget-object v1, LX/02j;->FAILURE:LX/02j;

    if-ne v0, v1, :cond_b

    .line 3026
    const-string v1, "dalvik_hack_telemetry_failure"

    invoke-static {v1}, LX/00p;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 3027
    const-string v4, "dalvik_hack_telemetry_failure"

    sput-object v4, LX/00p;->c:Ljava/lang/String;

    .line 3028
    new-instance v4, Ljava/lang/Throwable;

    const-string v5, "Failed to apply dalvik hack."

    invoke-direct {v4, v5}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    sput-object v4, LX/00p;->d:Ljava/lang/Throwable;

    goto :goto_5

    .line 3029
    :cond_a
    const-string v0, "Previous attempt crashed the process"

    goto :goto_6

    .line 3030
    :catch_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto/16 :goto_3

    :cond_b
    move-object v1, v4

    goto :goto_5

    :cond_c
    move-object v2, v0

    move-object v0, v4

    goto/16 :goto_1

    :cond_d
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x10

    if-ge v5, v6, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 2959
    invoke-static {p0, p1, p2}, LX/00p;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 2960
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-ne p3, v1, :cond_1

    .line 2961
    :cond_0
    return-void

    .line 2962
    :cond_1
    if-eqz p3, :cond_2

    .line 2963
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2964
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write(I)V

    .line 2965
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 2966
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2967
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create a file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2968
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 2969
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2970
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to delete a file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static a(LX/03x;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2953
    sget-object v1, LX/03x;->TEST:LX/03x;

    if-ne p0, v1, :cond_0

    .line 2954
    :goto_0
    :pswitch_0
    return v0

    .line 2955
    :cond_0
    sget-object v1, LX/00s;->resultValue:LX/02j;

    move-object v1, v1

    .line 2956
    sget-object v2, LX/03y;->a:[I

    invoke-virtual {v1}, LX/02j;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2957
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown replace buffer result="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2958
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
