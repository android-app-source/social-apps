.class public final LX/028;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field public final synthetic a:LX/01N;

.field private b:Z


# direct methods
.method public constructor <init>(LX/01N;)V
    .locals 1

    .prologue
    .line 6313
    iput-object p1, p0, LX/028;->a:LX/01N;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 6314
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/028;->b:Z

    .line 6315
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 6343
    iget-boolean v0, p0, LX/028;->b:Z

    if-eqz v0, :cond_0

    .line 6344
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream is closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6345
    :cond_0
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 10

    .prologue
    .line 6327
    invoke-direct {p0}, LX/028;->a()V

    .line 6328
    invoke-virtual {p0}, LX/028;->flush()V

    .line 6329
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/028;->b:Z

    .line 6330
    iget-object v0, p0, LX/028;->a:LX/01N;

    iget-object v0, v0, LX/01N;->c:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    .line 6331
    iget-object v1, p0, LX/028;->a:LX/01N;

    .line 6332
    iget-object v6, v1, LX/01N;->b:Ljava/io/RandomAccessFile;

    const-wide/16 v8, 0x1

    invoke-virtual {v6, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 6333
    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_0

    .line 6334
    aget-byte v3, v0, v2

    and-int/lit16 v3, v3, 0xff

    .line 6335
    sget-object v4, LX/01N;->e:[B

    ushr-int/lit8 v5, v3, 0x4

    aget-byte v4, v4, v5

    .line 6336
    sget-object v5, LX/01N;->e:[B

    and-int/lit8 v3, v3, 0xf

    aget-byte v3, v5, v3

    .line 6337
    iget-object v5, v1, LX/01N;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v5, v4}, Ljava/io/RandomAccessFile;->writeByte(I)V

    .line 6338
    iget-object v4, v1, LX/01N;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v4, v3}, Ljava/io/RandomAccessFile;->writeByte(I)V

    .line 6339
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 6340
    :cond_0
    iget-object v0, p0, LX/028;->a:LX/01N;

    const/4 v1, 0x0

    .line 6341
    iput-boolean v1, v0, LX/01N;->d:Z

    .line 6342
    return-void
.end method

.method public final flush()V
    .locals 0

    .prologue
    .line 6325
    invoke-direct {p0}, LX/028;->a()V

    .line 6326
    return-void
.end method

.method public final write(I)V
    .locals 1

    .prologue
    .line 6322
    invoke-direct {p0}, LX/028;->a()V

    .line 6323
    iget-object v0, p0, LX/028;->a:LX/01N;

    iget-object v0, v0, LX/01N;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1}, Ljava/io/RandomAccessFile;->write(I)V

    .line 6324
    return-void
.end method

.method public final write([B)V
    .locals 1

    .prologue
    .line 6319
    invoke-direct {p0}, LX/028;->a()V

    .line 6320
    iget-object v0, p0, LX/028;->a:LX/01N;

    iget-object v0, v0, LX/01N;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1}, Ljava/io/RandomAccessFile;->write([B)V

    .line 6321
    return-void
.end method

.method public final write([BII)V
    .locals 1

    .prologue
    .line 6316
    invoke-direct {p0}, LX/028;->a()V

    .line 6317
    iget-object v0, p0, LX/028;->a:LX/01N;

    iget-object v0, v0, LX/01N;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 6318
    return-void
.end method
