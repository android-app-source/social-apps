.class public final LX/0Ib;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public a:Ljava/util/LinkedList;
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38707
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/0Ib;->a:Ljava/util/LinkedList;

    return-void
.end method

.method public static a(LX/0Ib;LX/0IQ;)V
    .locals 2

    .prologue
    .line 38701
    iget-object v0, p1, LX/0IQ;->f:Ljava/lang/String;

    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38702
    :goto_0
    return-void

    .line 38703
    :cond_0
    iget-object v0, p0, LX/0Ib;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_1

    .line 38704
    iget-object v0, p0, LX/0Ib;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 38705
    :cond_1
    iget-object v0, p0, LX/0Ib;->a:Ljava/util/LinkedList;

    iget-object v1, p1, LX/0IQ;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static b(LX/0Ib;LX/0IQ;)Z
    .locals 2

    .prologue
    .line 38700
    iget-object v0, p0, LX/0Ib;->a:Ljava/util/LinkedList;

    iget-object v1, p1, LX/0IQ;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
