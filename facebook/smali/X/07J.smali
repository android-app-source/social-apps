.class public final LX/07J;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:[C


# direct methods
.method public constructor <init>(Ljavax/security/auth/x500/X500Principal;)V
    .locals 1

    .prologue
    .line 19584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19585
    const-string v0, "RFC2253"

    invoke-virtual {p1, v0}, Ljavax/security/auth/x500/X500Principal;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/07J;->a:Ljava/lang/String;

    .line 19586
    iget-object v0, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, LX/07J;->b:I

    .line 19587
    iget-object v0, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, LX/07J;->g:[C

    .line 19588
    return-void
.end method

.method public static a(LX/07J;I)I
    .locals 8

    .prologue
    const/16 v7, 0x61

    const/16 v6, 0x46

    const/16 v5, 0x41

    const/16 v4, 0x39

    const/16 v3, 0x30

    .line 19434
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, LX/07J;->b:I

    if-lt v0, v1, :cond_0

    .line 19435
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19436
    :cond_0
    iget-object v0, p0, LX/07J;->g:[C

    aget-char v0, v0, p1

    .line 19437
    if-lt v0, v3, :cond_1

    if-gt v0, v4, :cond_1

    .line 19438
    add-int/lit8 v0, v0, -0x30

    .line 19439
    :goto_0
    iget-object v1, p0, LX/07J;->g:[C

    add-int/lit8 v2, p1, 0x1

    aget-char v1, v1, v2

    .line 19440
    if-lt v1, v3, :cond_4

    if-gt v1, v4, :cond_4

    .line 19441
    add-int/lit8 v1, v1, -0x30

    .line 19442
    :goto_1
    shl-int/lit8 v0, v0, 0x4

    add-int/2addr v0, v1

    return v0

    .line 19443
    :cond_1
    if-lt v0, v7, :cond_2

    const/16 v1, 0x66

    if-gt v0, v1, :cond_2

    .line 19444
    add-int/lit8 v0, v0, -0x57

    goto :goto_0

    .line 19445
    :cond_2
    if-lt v0, v5, :cond_3

    if-gt v0, v6, :cond_3

    .line 19446
    add-int/lit8 v0, v0, -0x37

    goto :goto_0

    .line 19447
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19448
    :cond_4
    if-lt v1, v7, :cond_5

    const/16 v2, 0x66

    if-gt v1, v2, :cond_5

    .line 19449
    add-int/lit8 v1, v1, -0x57

    goto :goto_1

    .line 19450
    :cond_5
    if-lt v1, v5, :cond_6

    if-gt v1, v6, :cond_6

    .line 19451
    add-int/lit8 v1, v1, -0x37

    goto :goto_1

    .line 19452
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(LX/07J;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v3, 0x3d

    const/16 v2, 0x20

    .line 19565
    :goto_0
    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    if-ne v0, v2, :cond_0

    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    goto :goto_0

    .line 19566
    :cond_0
    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-ne v0, v1, :cond_1

    .line 19567
    const/4 v0, 0x0

    .line 19568
    :goto_1
    return-object v0

    .line 19569
    :cond_1
    iget v0, p0, LX/07J;->c:I

    iput v0, p0, LX/07J;->d:I

    .line 19570
    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    .line 19571
    :goto_2
    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-ge v0, v1, :cond_2

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    if-eq v0, v3, :cond_2

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    if-eq v0, v2, :cond_2

    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    goto :goto_2

    .line 19572
    :cond_2
    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-lt v0, v1, :cond_3

    .line 19573
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected end of DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19574
    :cond_3
    iget v0, p0, LX/07J;->c:I

    iput v0, p0, LX/07J;->e:I

    .line 19575
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    if-ne v0, v2, :cond_6

    .line 19576
    :goto_3
    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-ge v0, v1, :cond_4

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    if-eq v0, v3, :cond_4

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    if-ne v0, v2, :cond_4

    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    goto :goto_3

    .line 19577
    :cond_4
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    if-ne v0, v3, :cond_5

    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-ne v0, v1, :cond_6

    .line 19578
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected end of DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19579
    :cond_6
    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    .line 19580
    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-ge v0, v1, :cond_7

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    if-eq v0, v2, :cond_6

    .line 19581
    :cond_7
    iget v0, p0, LX/07J;->e:I

    iget v1, p0, LX/07J;->d:I

    sub-int/2addr v0, v1

    const/4 v1, 0x4

    if-le v0, v1, :cond_b

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->d:I

    add-int/lit8 v1, v1, 0x3

    aget-char v0, v0, v1

    const/16 v1, 0x2e

    if-ne v0, v1, :cond_b

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->d:I

    aget-char v0, v0, v1

    const/16 v1, 0x4f

    if-eq v0, v1, :cond_8

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->d:I

    aget-char v0, v0, v1

    const/16 v1, 0x6f

    if-ne v0, v1, :cond_b

    :cond_8
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->d:I

    add-int/lit8 v1, v1, 0x1

    aget-char v0, v0, v1

    const/16 v1, 0x49

    if-eq v0, v1, :cond_9

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->d:I

    add-int/lit8 v1, v1, 0x1

    aget-char v0, v0, v1

    const/16 v1, 0x69

    if-ne v0, v1, :cond_b

    :cond_9
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->d:I

    add-int/lit8 v1, v1, 0x2

    aget-char v0, v0, v1

    const/16 v1, 0x44

    if-eq v0, v1, :cond_a

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->d:I

    add-int/lit8 v1, v1, 0x2

    aget-char v0, v0, v1

    const/16 v1, 0x64

    if-ne v0, v1, :cond_b

    .line 19582
    :cond_a
    iget v0, p0, LX/07J;->d:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/07J;->d:I

    .line 19583
    :cond_b
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/07J;->g:[C

    iget v2, p0, LX/07J;->d:I

    iget v3, p0, LX/07J;->e:I

    iget v4, p0, LX/07J;->d:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    goto/16 :goto_1
.end method

.method private static b(LX/07J;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 19551
    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    .line 19552
    iget v0, p0, LX/07J;->c:I

    iput v0, p0, LX/07J;->d:I

    .line 19553
    iget v0, p0, LX/07J;->d:I

    iput v0, p0, LX/07J;->e:I

    .line 19554
    :goto_0
    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-ne v0, v1, :cond_0

    .line 19555
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected end of DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19556
    :cond_0
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x22

    if-ne v0, v1, :cond_1

    .line 19557
    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    .line 19558
    :goto_1
    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-ge v0, v1, :cond_3

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    goto :goto_1

    .line 19559
    :cond_1
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x5c

    if-ne v0, v1, :cond_2

    .line 19560
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->e:I

    invoke-static {p0}, LX/07J;->e(LX/07J;)C

    move-result v2

    aput-char v2, v0, v1

    .line 19561
    :goto_2
    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    .line 19562
    iget v0, p0, LX/07J;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->e:I

    goto :goto_0

    .line 19563
    :cond_2
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->e:I

    iget-object v2, p0, LX/07J;->g:[C

    iget v3, p0, LX/07J;->c:I

    aget-char v2, v2, v3

    aput-char v2, v0, v1

    goto :goto_2

    .line 19564
    :cond_3
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/07J;->g:[C

    iget v2, p0, LX/07J;->d:I

    iget v3, p0, LX/07J;->e:I

    iget v4, p0, LX/07J;->d:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method private static c(LX/07J;)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v3, 0x20

    .line 19530
    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, LX/07J;->b:I

    if-lt v0, v1, :cond_0

    .line 19531
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected end of DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19532
    :cond_0
    iget v0, p0, LX/07J;->c:I

    iput v0, p0, LX/07J;->d:I

    .line 19533
    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    .line 19534
    :goto_0
    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x3b

    if-ne v0, v1, :cond_4

    .line 19535
    :cond_1
    iget v0, p0, LX/07J;->c:I

    iput v0, p0, LX/07J;->e:I

    .line 19536
    :cond_2
    iget v0, p0, LX/07J;->e:I

    iget v1, p0, LX/07J;->d:I

    sub-int v2, v0, v1

    .line 19537
    const/4 v0, 0x5

    if-lt v2, v0, :cond_3

    and-int/lit8 v0, v2, 0x1

    if-nez v0, :cond_7

    .line 19538
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected end of DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19539
    :cond_4
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    if-ne v0, v3, :cond_5

    .line 19540
    iget v0, p0, LX/07J;->c:I

    iput v0, p0, LX/07J;->e:I

    .line 19541
    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    .line 19542
    :goto_1
    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-ge v0, v1, :cond_2

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    if-ne v0, v3, :cond_2

    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    goto :goto_1

    .line 19543
    :cond_5
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x41

    if-lt v0, v1, :cond_6

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x46

    if-gt v0, v1, :cond_6

    .line 19544
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v2, v0, v1

    add-int/lit8 v2, v2, 0x20

    int-to-char v2, v2

    aput-char v2, v0, v1

    .line 19545
    :cond_6
    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    goto/16 :goto_0

    .line 19546
    :cond_7
    div-int/lit8 v0, v2, 0x2

    new-array v3, v0, [B

    .line 19547
    const/4 v1, 0x0

    iget v0, p0, LX/07J;->d:I

    add-int/lit8 v0, v0, 0x1

    :goto_2
    array-length v4, v3

    if-ge v1, v4, :cond_8

    .line 19548
    invoke-static {p0, v0}, LX/07J;->a(LX/07J;I)I

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    .line 19549
    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 19550
    :cond_8
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/07J;->g:[C

    iget v3, p0, LX/07J;->d:I

    invoke-direct {v0, v1, v3, v2}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method public static e(LX/07J;)C
    .locals 8

    .prologue
    .line 19496
    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    .line 19497
    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-ne v0, v1, :cond_0

    .line 19498
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected end of DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19499
    :cond_0
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 19500
    const/16 v6, 0x80

    const/16 v2, 0x3f

    .line 19501
    iget v0, p0, LX/07J;->c:I

    invoke-static {p0, v0}, LX/07J;->a(LX/07J;I)I

    move-result v1

    .line 19502
    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    .line 19503
    if-ge v1, v6, :cond_1

    .line 19504
    int-to-char v0, v1

    .line 19505
    :goto_0
    move v0, v0

    .line 19506
    :goto_1
    return v0

    .line 19507
    :sswitch_0
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    goto :goto_1

    .line 19508
    :cond_1
    const/16 v0, 0xc0

    if-lt v1, v0, :cond_8

    const/16 v0, 0xf7

    if-gt v1, v0, :cond_8

    .line 19509
    const/16 v0, 0xdf

    if-gt v1, v0, :cond_3

    .line 19510
    const/4 v0, 0x1

    .line 19511
    and-int/lit8 v1, v1, 0x1f

    .line 19512
    :goto_2
    const/4 v3, 0x0

    move v7, v3

    move v3, v1

    move v1, v7

    :goto_3
    if-ge v1, v0, :cond_7

    .line 19513
    iget v4, p0, LX/07J;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/07J;->c:I

    .line 19514
    iget v4, p0, LX/07J;->c:I

    iget v5, p0, LX/07J;->b:I

    if-eq v4, v5, :cond_2

    iget-object v4, p0, LX/07J;->g:[C

    iget v5, p0, LX/07J;->c:I

    aget-char v4, v4, v5

    const/16 v5, 0x5c

    if-eq v4, v5, :cond_5

    :cond_2
    move v0, v2

    .line 19515
    goto :goto_0

    .line 19516
    :cond_3
    const/16 v0, 0xef

    if-gt v1, v0, :cond_4

    .line 19517
    const/4 v0, 0x2

    .line 19518
    and-int/lit8 v1, v1, 0xf

    goto :goto_2

    .line 19519
    :cond_4
    const/4 v0, 0x3

    .line 19520
    and-int/lit8 v1, v1, 0x7

    goto :goto_2

    .line 19521
    :cond_5
    iget v4, p0, LX/07J;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/07J;->c:I

    .line 19522
    iget v4, p0, LX/07J;->c:I

    invoke-static {p0, v4}, LX/07J;->a(LX/07J;I)I

    move-result v4

    .line 19523
    iget v5, p0, LX/07J;->c:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, LX/07J;->c:I

    .line 19524
    and-int/lit16 v5, v4, 0xc0

    if-eq v5, v6, :cond_6

    move v0, v2

    .line 19525
    goto :goto_0

    .line 19526
    :cond_6
    shl-int/lit8 v3, v3, 0x6

    and-int/lit8 v4, v4, 0x3f

    add-int/2addr v3, v4

    .line 19527
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 19528
    :cond_7
    int-to-char v0, v3

    goto :goto_0

    :cond_8
    move v0, v2

    .line 19529
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x22 -> :sswitch_0
        0x23 -> :sswitch_0
        0x25 -> :sswitch_0
        0x2a -> :sswitch_0
        0x2b -> :sswitch_0
        0x2c -> :sswitch_0
        0x3b -> :sswitch_0
        0x3c -> :sswitch_0
        0x3d -> :sswitch_0
        0x3e -> :sswitch_0
        0x5c -> :sswitch_0
        0x5f -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 19453
    iput v0, p0, LX/07J;->c:I

    .line 19454
    iput v0, p0, LX/07J;->d:I

    .line 19455
    iput v0, p0, LX/07J;->e:I

    .line 19456
    iput v0, p0, LX/07J;->f:I

    .line 19457
    invoke-static {p0}, LX/07J;->a(LX/07J;)Ljava/lang/String;

    move-result-object v0

    .line 19458
    if-nez v0, :cond_1

    move-object v1, v2

    .line 19459
    :cond_0
    :goto_0
    return-object v1

    .line 19460
    :cond_1
    const-string v1, ""

    .line 19461
    iget v3, p0, LX/07J;->c:I

    iget v4, p0, LX/07J;->b:I

    if-ne v3, v4, :cond_2

    move-object v1, v2

    .line 19462
    goto :goto_0

    .line 19463
    :cond_2
    iget-object v3, p0, LX/07J;->g:[C

    iget v4, p0, LX/07J;->c:I

    aget-char v3, v3, v4

    sparse-switch v3, :sswitch_data_0

    .line 19464
    const/16 v6, 0x20

    .line 19465
    iget v1, p0, LX/07J;->c:I

    iput v1, p0, LX/07J;->d:I

    .line 19466
    iget v1, p0, LX/07J;->c:I

    iput v1, p0, LX/07J;->e:I

    .line 19467
    :cond_3
    :goto_1
    iget v1, p0, LX/07J;->c:I

    iget v3, p0, LX/07J;->b:I

    if-lt v1, v3, :cond_6

    .line 19468
    new-instance v1, Ljava/lang/String;

    iget-object v3, p0, LX/07J;->g:[C

    iget v4, p0, LX/07J;->d:I

    iget v5, p0, LX/07J;->e:I

    iget v6, p0, LX/07J;->d:I

    sub-int/2addr v5, v6

    invoke-direct {v1, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    .line 19469
    :goto_2
    move-object v1, v1

    .line 19470
    :goto_3
    :sswitch_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19471
    iget v0, p0, LX/07J;->c:I

    iget v1, p0, LX/07J;->b:I

    if-lt v0, v1, :cond_4

    move-object v1, v2

    .line 19472
    goto :goto_0

    .line 19473
    :sswitch_1
    invoke-static {p0}, LX/07J;->b(LX/07J;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 19474
    :sswitch_2
    invoke-static {p0}, LX/07J;->c(LX/07J;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 19475
    :cond_4
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_5

    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_5

    .line 19476
    iget-object v0, p0, LX/07J;->g:[C

    iget v1, p0, LX/07J;->c:I

    aget-char v0, v0, v1

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_5

    .line 19477
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19478
    :cond_5
    iget v0, p0, LX/07J;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/07J;->c:I

    .line 19479
    invoke-static {p0}, LX/07J;->a(LX/07J;)Ljava/lang/String;

    move-result-object v0

    .line 19480
    if-nez v0, :cond_1

    .line 19481
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed DN: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/07J;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19482
    :cond_6
    iget-object v1, p0, LX/07J;->g:[C

    iget v3, p0, LX/07J;->c:I

    aget-char v1, v1, v3

    sparse-switch v1, :sswitch_data_1

    .line 19483
    iget-object v1, p0, LX/07J;->g:[C

    iget v3, p0, LX/07J;->e:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/07J;->e:I

    iget-object v4, p0, LX/07J;->g:[C

    iget v5, p0, LX/07J;->c:I

    aget-char v4, v4, v5

    aput-char v4, v1, v3

    .line 19484
    iget v1, p0, LX/07J;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/07J;->c:I

    goto/16 :goto_1

    .line 19485
    :sswitch_3
    new-instance v1, Ljava/lang/String;

    iget-object v3, p0, LX/07J;->g:[C

    iget v4, p0, LX/07J;->d:I

    iget v5, p0, LX/07J;->e:I

    iget v6, p0, LX/07J;->d:I

    sub-int/2addr v5, v6

    invoke-direct {v1, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    goto/16 :goto_2

    .line 19486
    :sswitch_4
    iget-object v1, p0, LX/07J;->g:[C

    iget v3, p0, LX/07J;->e:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/07J;->e:I

    invoke-static {p0}, LX/07J;->e(LX/07J;)C

    move-result v4

    aput-char v4, v1, v3

    .line 19487
    iget v1, p0, LX/07J;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/07J;->c:I

    goto/16 :goto_1

    .line 19488
    :sswitch_5
    iget v1, p0, LX/07J;->e:I

    iput v1, p0, LX/07J;->f:I

    .line 19489
    iget v1, p0, LX/07J;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/07J;->c:I

    .line 19490
    iget-object v1, p0, LX/07J;->g:[C

    iget v3, p0, LX/07J;->e:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/07J;->e:I

    aput-char v6, v1, v3

    .line 19491
    :goto_4
    iget v1, p0, LX/07J;->c:I

    iget v3, p0, LX/07J;->b:I

    if-ge v1, v3, :cond_7

    iget-object v1, p0, LX/07J;->g:[C

    iget v3, p0, LX/07J;->c:I

    aget-char v1, v1, v3

    if-ne v1, v6, :cond_7

    .line 19492
    iget-object v1, p0, LX/07J;->g:[C

    iget v3, p0, LX/07J;->e:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/07J;->e:I

    aput-char v6, v1, v3

    .line 19493
    iget v1, p0, LX/07J;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/07J;->c:I

    goto :goto_4

    .line 19494
    :cond_7
    iget v1, p0, LX/07J;->c:I

    iget v3, p0, LX/07J;->b:I

    if-eq v1, v3, :cond_8

    iget-object v1, p0, LX/07J;->g:[C

    iget v3, p0, LX/07J;->c:I

    aget-char v1, v1, v3

    const/16 v3, 0x2c

    if-eq v1, v3, :cond_8

    iget-object v1, p0, LX/07J;->g:[C

    iget v3, p0, LX/07J;->c:I

    aget-char v1, v1, v3

    const/16 v3, 0x2b

    if-eq v1, v3, :cond_8

    iget-object v1, p0, LX/07J;->g:[C

    iget v3, p0, LX/07J;->c:I

    aget-char v1, v1, v3

    const/16 v3, 0x3b

    if-ne v1, v3, :cond_3

    .line 19495
    :cond_8
    new-instance v1, Ljava/lang/String;

    iget-object v3, p0, LX/07J;->g:[C

    iget v4, p0, LX/07J;->d:I

    iget v5, p0, LX/07J;->f:I

    iget v6, p0, LX/07J;->d:I

    sub-int/2addr v5, v6

    invoke-direct {v1, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_1
        0x23 -> :sswitch_2
        0x2b -> :sswitch_0
        0x2c -> :sswitch_0
        0x3b -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x20 -> :sswitch_5
        0x2b -> :sswitch_3
        0x2c -> :sswitch_3
        0x3b -> :sswitch_3
        0x5c -> :sswitch_4
    .end sparse-switch
.end method
