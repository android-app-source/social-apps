.class public final enum LX/0JS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0JS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0JS;

.field public static final enum BADGE_CHECK_INTERVAL:LX/0JS;

.field public static final enum BADGE_COUNT:LX/0JS;

.field public static final enum BROADCAST_STATUS_FIELD:LX/0JS;

.field public static final enum CHANNEL_ID:LX/0JS;

.field public static final enum CLICK_TARGET:LX/0JS;

.field public static final enum CREATOR_STATUS:LX/0JS;

.field public static final enum DATA_PREFETCH_STATUS:LX/0JS;

.field public static final enum DATA_STALE_INTERVAL:LX/0JS;

.field public static final enum DUPLICATE_STORY_ID:LX/0JS;

.field public static final enum ENTRY_POINT_TYPE:LX/0JS;

.field public static final enum EVENT_TARGET:LX/0JS;

.field public static final enum EVENT_TARGET_INFO:LX/0JS;

.field public static final enum FETCH_REASON:LX/0JS;

.field public static final enum NEW_BADGE_COUNT:LX/0JS;

.field public static final enum NUMBER_OF_SECTION:LX/0JS;

.field public static final enum OLD_BADGE_COUNT:LX/0JS;

.field public static final enum POSITION_IN_UNIT:LX/0JS;

.field public static final enum PREFETCH_ENABLED:LX/0JS;

.field public static final enum REACTION_COMPONENT_TRACKING_FIELD:LX/0JS;

.field public static final enum REACTION_UNITS_TYPES:LX/0JS;

.field public static final enum REACTION_UNIT_TYPE:LX/0JS;

.field public static final enum SESSION_DURATION:LX/0JS;

.field public static final enum SESSION_ID:LX/0JS;

.field public static final enum TARGET_ID:LX/0JS;

.field public static final enum TIME_SINCE_PREFETCH:LX/0JS;

.field public static final enum TIME_SINCE_START:LX/0JS;

.field public static final enum TRACKING:LX/0JS;

.field public static final enum TTI_PERF_S:LX/0JS;

.field public static final enum TTI_TYPE:LX/0JS;

.field public static final enum UNIT_POSITION:LX/0JS;

.field public static final enum UNSEEN_NOTIFICATIONS_COUNT:LX/0JS;

.field public static final enum VIDEO_HOME_SESSION_ID:LX/0JS;

.field public static final enum VIDEO_SEARCH_EXPERIENCE:LX/0JS;

.field public static final enum VPV_DURATION:LX/0JS;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39433
    new-instance v0, LX/0JS;

    const-string v1, "SESSION_ID"

    const-string v2, "session_id"

    invoke-direct {v0, v1, v4, v2}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->SESSION_ID:LX/0JS;

    .line 39434
    new-instance v0, LX/0JS;

    const-string v1, "VIDEO_HOME_SESSION_ID"

    const-string v2, "video_home_session_id"

    invoke-direct {v0, v1, v5, v2}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->VIDEO_HOME_SESSION_ID:LX/0JS;

    .line 39435
    new-instance v0, LX/0JS;

    const-string v1, "TTI_TYPE"

    const-string v2, "tti_type"

    invoke-direct {v0, v1, v6, v2}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->TTI_TYPE:LX/0JS;

    .line 39436
    new-instance v0, LX/0JS;

    const-string v1, "TTI_PERF_S"

    const-string v2, "value"

    invoke-direct {v0, v1, v7, v2}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->TTI_PERF_S:LX/0JS;

    .line 39437
    new-instance v0, LX/0JS;

    const-string v1, "CLICK_TARGET"

    const-string v2, "click_target"

    invoke-direct {v0, v1, v8, v2}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->CLICK_TARGET:LX/0JS;

    .line 39438
    new-instance v0, LX/0JS;

    const-string v1, "CREATOR_STATUS"

    const/4 v2, 0x5

    const-string v3, "creator_status"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->CREATOR_STATUS:LX/0JS;

    .line 39439
    new-instance v0, LX/0JS;

    const-string v1, "EVENT_TARGET"

    const/4 v2, 0x6

    const-string v3, "event_target"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->EVENT_TARGET:LX/0JS;

    .line 39440
    new-instance v0, LX/0JS;

    const-string v1, "TARGET_ID"

    const/4 v2, 0x7

    const-string v3, "event_target_id"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->TARGET_ID:LX/0JS;

    .line 39441
    new-instance v0, LX/0JS;

    const-string v1, "CHANNEL_ID"

    const/16 v2, 0x8

    const-string v3, "channel_id"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->CHANNEL_ID:LX/0JS;

    .line 39442
    new-instance v0, LX/0JS;

    const-string v1, "REACTION_UNIT_TYPE"

    const/16 v2, 0x9

    const-string v3, "reaction_unit_type"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->REACTION_UNIT_TYPE:LX/0JS;

    .line 39443
    new-instance v0, LX/0JS;

    const-string v1, "TRACKING"

    const/16 v2, 0xa

    const-string v3, "tracking_data"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->TRACKING:LX/0JS;

    .line 39444
    new-instance v0, LX/0JS;

    const-string v1, "SESSION_DURATION"

    const/16 v2, 0xb

    const-string v3, "session_duration"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->SESSION_DURATION:LX/0JS;

    .line 39445
    new-instance v0, LX/0JS;

    const-string v1, "FETCH_REASON"

    const/16 v2, 0xc

    const-string v3, "fetch_reason"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->FETCH_REASON:LX/0JS;

    .line 39446
    new-instance v0, LX/0JS;

    const-string v1, "BADGE_CHECK_INTERVAL"

    const/16 v2, 0xd

    const-string v3, "badge_check_interval"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->BADGE_CHECK_INTERVAL:LX/0JS;

    .line 39447
    new-instance v0, LX/0JS;

    const-string v1, "BADGE_COUNT"

    const/16 v2, 0xe

    const-string v3, "badge_count"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->BADGE_COUNT:LX/0JS;

    .line 39448
    new-instance v0, LX/0JS;

    const-string v1, "NEW_BADGE_COUNT"

    const/16 v2, 0xf

    const-string v3, "new_badge_count"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->NEW_BADGE_COUNT:LX/0JS;

    .line 39449
    new-instance v0, LX/0JS;

    const-string v1, "OLD_BADGE_COUNT"

    const/16 v2, 0x10

    const-string v3, "old_badge_count"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->OLD_BADGE_COUNT:LX/0JS;

    .line 39450
    new-instance v0, LX/0JS;

    const-string v1, "EVENT_TARGET_INFO"

    const/16 v2, 0x11

    const-string v3, "event_target_info"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->EVENT_TARGET_INFO:LX/0JS;

    .line 39451
    new-instance v0, LX/0JS;

    const-string v1, "UNIT_POSITION"

    const/16 v2, 0x12

    const-string v3, "unit_position"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->UNIT_POSITION:LX/0JS;

    .line 39452
    new-instance v0, LX/0JS;

    const-string v1, "POSITION_IN_UNIT"

    const/16 v2, 0x13

    const-string v3, "position_in_unit"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->POSITION_IN_UNIT:LX/0JS;

    .line 39453
    new-instance v0, LX/0JS;

    const-string v1, "DUPLICATE_STORY_ID"

    const/16 v2, 0x14

    const-string v3, "duplicate_story_id"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->DUPLICATE_STORY_ID:LX/0JS;

    .line 39454
    new-instance v0, LX/0JS;

    const-string v1, "VIDEO_SEARCH_EXPERIENCE"

    const/16 v2, 0x15

    const-string v3, "video_search_experience"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->VIDEO_SEARCH_EXPERIENCE:LX/0JS;

    .line 39455
    new-instance v0, LX/0JS;

    const-string v1, "ENTRY_POINT_TYPE"

    const/16 v2, 0x16

    const-string v3, "entry_point_type"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->ENTRY_POINT_TYPE:LX/0JS;

    .line 39456
    new-instance v0, LX/0JS;

    const-string v1, "DATA_PREFETCH_STATUS"

    const/16 v2, 0x17

    const-string v3, "data_prefetch_status"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->DATA_PREFETCH_STATUS:LX/0JS;

    .line 39457
    new-instance v0, LX/0JS;

    const-string v1, "REACTION_COMPONENT_TRACKING_FIELD"

    const/16 v2, 0x18

    const-string v3, "reaction_component_tracking_data"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->REACTION_COMPONENT_TRACKING_FIELD:LX/0JS;

    .line 39458
    new-instance v0, LX/0JS;

    const-string v1, "BROADCAST_STATUS_FIELD"

    const/16 v2, 0x19

    const-string v3, "broadcast_status"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->BROADCAST_STATUS_FIELD:LX/0JS;

    .line 39459
    new-instance v0, LX/0JS;

    const-string v1, "TIME_SINCE_START"

    const/16 v2, 0x1a

    const-string v3, "time_since_start"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->TIME_SINCE_START:LX/0JS;

    .line 39460
    new-instance v0, LX/0JS;

    const-string v1, "TIME_SINCE_PREFETCH"

    const/16 v2, 0x1b

    const-string v3, "time_since_prefetch"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->TIME_SINCE_PREFETCH:LX/0JS;

    .line 39461
    new-instance v0, LX/0JS;

    const-string v1, "VPV_DURATION"

    const/16 v2, 0x1c

    const-string v3, "vpvd_time_delta"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->VPV_DURATION:LX/0JS;

    .line 39462
    new-instance v0, LX/0JS;

    const-string v1, "NUMBER_OF_SECTION"

    const/16 v2, 0x1d

    const-string v3, "number_of_section"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->NUMBER_OF_SECTION:LX/0JS;

    .line 39463
    new-instance v0, LX/0JS;

    const-string v1, "REACTION_UNITS_TYPES"

    const/16 v2, 0x1e

    const-string v3, "reaction_units_types"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->REACTION_UNITS_TYPES:LX/0JS;

    .line 39464
    new-instance v0, LX/0JS;

    const-string v1, "DATA_STALE_INTERVAL"

    const/16 v2, 0x1f

    const-string v3, "data_stale_interval"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->DATA_STALE_INTERVAL:LX/0JS;

    .line 39465
    new-instance v0, LX/0JS;

    const-string v1, "UNSEEN_NOTIFICATIONS_COUNT"

    const/16 v2, 0x20

    const-string v3, "unseen_notifications_count"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->UNSEEN_NOTIFICATIONS_COUNT:LX/0JS;

    .line 39466
    new-instance v0, LX/0JS;

    const-string v1, "PREFETCH_ENABLED"

    const/16 v2, 0x21

    const-string v3, "prefetch_enabled"

    invoke-direct {v0, v1, v2, v3}, LX/0JS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0JS;->PREFETCH_ENABLED:LX/0JS;

    .line 39467
    const/16 v0, 0x22

    new-array v0, v0, [LX/0JS;

    sget-object v1, LX/0JS;->SESSION_ID:LX/0JS;

    aput-object v1, v0, v4

    sget-object v1, LX/0JS;->VIDEO_HOME_SESSION_ID:LX/0JS;

    aput-object v1, v0, v5

    sget-object v1, LX/0JS;->TTI_TYPE:LX/0JS;

    aput-object v1, v0, v6

    sget-object v1, LX/0JS;->TTI_PERF_S:LX/0JS;

    aput-object v1, v0, v7

    sget-object v1, LX/0JS;->CLICK_TARGET:LX/0JS;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0JS;->CREATOR_STATUS:LX/0JS;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0JS;->EVENT_TARGET:LX/0JS;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0JS;->TARGET_ID:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0JS;->CHANNEL_ID:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0JS;->REACTION_UNIT_TYPE:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0JS;->TRACKING:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0JS;->SESSION_DURATION:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0JS;->FETCH_REASON:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0JS;->BADGE_CHECK_INTERVAL:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0JS;->BADGE_COUNT:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0JS;->NEW_BADGE_COUNT:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0JS;->OLD_BADGE_COUNT:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0JS;->EVENT_TARGET_INFO:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/0JS;->UNIT_POSITION:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/0JS;->POSITION_IN_UNIT:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/0JS;->DUPLICATE_STORY_ID:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/0JS;->VIDEO_SEARCH_EXPERIENCE:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/0JS;->ENTRY_POINT_TYPE:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/0JS;->DATA_PREFETCH_STATUS:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/0JS;->REACTION_COMPONENT_TRACKING_FIELD:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/0JS;->BROADCAST_STATUS_FIELD:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/0JS;->TIME_SINCE_START:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/0JS;->TIME_SINCE_PREFETCH:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/0JS;->VPV_DURATION:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/0JS;->NUMBER_OF_SECTION:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/0JS;->REACTION_UNITS_TYPES:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/0JS;->DATA_STALE_INTERVAL:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/0JS;->UNSEEN_NOTIFICATIONS_COUNT:LX/0JS;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/0JS;->PREFETCH_ENABLED:LX/0JS;

    aput-object v2, v0, v1

    sput-object v0, LX/0JS;->$VALUES:[LX/0JS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39468
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39469
    iput-object p3, p0, LX/0JS;->value:Ljava/lang/String;

    .line 39470
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0JS;
    .locals 1

    .prologue
    .line 39471
    const-class v0, LX/0JS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0JS;

    return-object v0
.end method

.method public static values()[LX/0JS;
    .locals 1

    .prologue
    .line 39472
    sget-object v0, LX/0JS;->$VALUES:[LX/0JS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0JS;

    return-object v0
.end method
