.class public final enum LX/00o;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/00o;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/00o;

.field public static final enum FBANDROID_DEBUG:LX/00o;

.field public static final enum FBANDROID_RELEASE:LX/00o;

.field public static final enum MESSENGER_DEBUG:LX/00o;

.field public static final enum MESSENGER_RELEASE:LX/00o;

.field public static final enum SAMPLE_APP:LX/00o;


# instance fields
.field public final bufferSizeBytes:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, 0x400000

    .line 2933
    new-instance v0, LX/00o;

    const-string v1, "FBANDROID_DEBUG"

    const/high16 v2, 0x800000

    invoke-direct {v0, v1, v4, v2}, LX/00o;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/00o;->FBANDROID_DEBUG:LX/00o;

    .line 2934
    new-instance v0, LX/00o;

    const-string v1, "FBANDROID_RELEASE"

    const/high16 v2, 0x800000

    invoke-direct {v0, v1, v5, v2}, LX/00o;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/00o;->FBANDROID_RELEASE:LX/00o;

    .line 2935
    new-instance v0, LX/00o;

    const-string v1, "MESSENGER_DEBUG"

    invoke-direct {v0, v1, v6, v3}, LX/00o;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/00o;->MESSENGER_DEBUG:LX/00o;

    .line 2936
    new-instance v0, LX/00o;

    const-string v1, "MESSENGER_RELEASE"

    invoke-direct {v0, v1, v7, v3}, LX/00o;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/00o;->MESSENGER_RELEASE:LX/00o;

    .line 2937
    new-instance v0, LX/00o;

    const-string v1, "SAMPLE_APP"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/00o;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/00o;->SAMPLE_APP:LX/00o;

    .line 2938
    const/4 v0, 0x5

    new-array v0, v0, [LX/00o;

    sget-object v1, LX/00o;->FBANDROID_DEBUG:LX/00o;

    aput-object v1, v0, v4

    sget-object v1, LX/00o;->FBANDROID_RELEASE:LX/00o;

    aput-object v1, v0, v5

    sget-object v1, LX/00o;->MESSENGER_DEBUG:LX/00o;

    aput-object v1, v0, v6

    sget-object v1, LX/00o;->MESSENGER_RELEASE:LX/00o;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, LX/00o;->SAMPLE_APP:LX/00o;

    aput-object v2, v0, v1

    sput-object v0, LX/00o;->$VALUES:[LX/00o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2939
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2940
    iput p3, p0, LX/00o;->bufferSizeBytes:I

    .line 2941
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/00o;
    .locals 1

    .prologue
    .line 2942
    const-class v0, LX/00o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/00o;

    return-object v0
.end method

.method public static values()[LX/00o;
    .locals 1

    .prologue
    .line 2943
    sget-object v0, LX/00o;->$VALUES:[LX/00o;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/00o;

    return-object v0
.end method
