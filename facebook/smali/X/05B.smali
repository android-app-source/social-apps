.class public abstract LX/05B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;)LX/05B;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "LX/05B",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 15845
    new-instance v0, LX/06e;

    invoke-static {p0}, LX/01n;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, LX/06e;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Object;)LX/05B;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "LX/05B",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 15841
    if-nez p0, :cond_0

    .line 15842
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v0, v0

    .line 15843
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/06e;

    invoke-direct {v0, p0}, LX/06e;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static c()LX/05B;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LX/05B",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 15844
    sget-object v0, LX/06b;->a:LX/06b;

    return-object v0
.end method


# virtual methods
.method public abstract a()Z
.end method

.method public abstract b()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method
