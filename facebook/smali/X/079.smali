.class public LX/079;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:LX/078;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/078;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/078;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 19294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19295
    iput-object p1, p0, LX/079;->a:Ljava/lang/String;

    .line 19296
    iput-object p4, p0, LX/079;->f:Ljava/util/List;

    .line 19297
    iput-object v0, p0, LX/079;->b:Ljava/lang/String;

    .line 19298
    iput-object v0, p0, LX/079;->c:Ljava/lang/String;

    .line 19299
    iput-object p2, p0, LX/079;->d:LX/078;

    .line 19300
    iput-object p3, p0, LX/079;->e:Ljava/lang/String;

    .line 19301
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/078;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/078;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19303
    iput-object p1, p0, LX/079;->a:Ljava/lang/String;

    .line 19304
    iput-object p2, p0, LX/079;->b:Ljava/lang/String;

    .line 19305
    iput-object p3, p0, LX/079;->c:Ljava/lang/String;

    .line 19306
    iput-object p4, p0, LX/079;->d:LX/078;

    .line 19307
    iput-object p5, p0, LX/079;->e:Ljava/lang/String;

    .line 19308
    iput-object p6, p0, LX/079;->f:Ljava/util/List;

    .line 19309
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19310
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{clientIdentifier=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/079;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', willTopic=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/079;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', willMessage=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/079;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', userName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/079;->d:LX/078;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
