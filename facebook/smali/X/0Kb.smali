.class public LX/0Kb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/042;


# direct methods
.method public constructor <init>(LX/042;)V
    .locals 2

    .prologue
    .line 41558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41559
    if-nez p1, :cond_0

    .line 41560
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "videoPlayerServiceListener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41561
    :cond_0
    iput-object p1, p0, LX/0Kb;->a:LX/042;

    .line 41562
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 41556
    iget-object v0, p0, LX/0Kb;->a:LX/042;

    invoke-interface {v0}, LX/042;->a()V

    .line 41557
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 41554
    iget-object v0, p0, LX/0Kb;->a:LX/042;

    invoke-interface {v0, p1}, LX/042;->a(I)V

    .line 41555
    return-void
.end method

.method public final a(IIF)V
    .locals 1

    .prologue
    .line 41552
    iget-object v0, p0, LX/0Kb;->a:LX/042;

    invoke-interface {v0, p1, p2, p3}, LX/042;->a(IIF)V

    .line 41553
    return-void
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    .line 41550
    iget-object v0, p0, LX/0Kb;->a:LX/042;

    invoke-interface {v0, p1, p2, p3}, LX/042;->a(IJ)V

    .line 41551
    return-void
.end method

.method public final a(ILcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V
    .locals 1

    .prologue
    .line 41548
    iget-object v0, p0, LX/0Kb;->a:LX/042;

    invoke-interface {v0, p1, p2}, LX/042;->a(ILcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V

    .line 41549
    return-void
.end method

.method public final a(ILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;IJJ)V
    .locals 8

    .prologue
    .line 41534
    iget-object v0, p0, LX/0Kb;->a:LX/042;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    move-wide v6, p6

    invoke-interface/range {v0 .. v7}, LX/042;->a(ILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;IJJ)V

    .line 41535
    return-void
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/RendererContext;)V
    .locals 1

    .prologue
    .line 41546
    iget-object v0, p0, LX/0Kb;->a:LX/042;

    invoke-interface {v0, p1, p2, p3}, LX/042;->a(Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/RendererContext;)V

    .line 41547
    return-void
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZII)V
    .locals 1

    .prologue
    .line 41544
    iget-object v0, p0, LX/0Kb;->a:LX/042;

    invoke-interface {v0, p1, p2, p3, p4}, LX/042;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZII)V

    .line 41545
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 41542
    iget-object v0, p0, LX/0Kb;->a:LX/042;

    invoke-interface {v0, p1, p2, p3}, LX/042;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 41543
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 41540
    iget-object v0, p0, LX/0Kb;->a:LX/042;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, LX/042;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 41541
    return-void
.end method

.method public final a(Ljava/util/List;J[Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;",
            ">;J[",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41538
    iget-object v0, p0, LX/0Kb;->a:LX/042;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, LX/042;->a(Ljava/util/List;J[Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;)V

    .line 41539
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 41536
    iget-object v0, p0, LX/0Kb;->a:LX/042;

    invoke-interface {v0}, LX/042;->b()V

    .line 41537
    return-void
.end method
