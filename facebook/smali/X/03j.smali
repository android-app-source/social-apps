.class public LX/03j;
.super LX/0bi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0bi",
        "<",
        "LX/03k;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/03k;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 10685
    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x49

    aput v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x48

    aput v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x2f6

    aput v2, v0, v1

    invoke-direct {p0, p1, v0}, LX/0bi;-><init>(LX/0Ot;[I)V

    .line 10686
    return-void
.end method


# virtual methods
.method public final a(LX/0Uh;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 10687
    check-cast p3, LX/03k;

    .line 10688
    invoke-virtual {p1, p2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    .line 10689
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 10690
    invoke-static {}, LX/03k;->a()Landroid/app/Application;

    move-result-object v3

    .line 10691
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    .line 10692
    :goto_0
    if-nez v3, :cond_2

    .line 10693
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Context not available"

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 10694
    :cond_0
    :goto_1
    return-void

    .line 10695
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 10696
    :cond_2
    const-string p0, "breakpad_flags_store"

    invoke-virtual {v3, p0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 10697
    const/16 p0, 0x49

    if-ne p2, p0, :cond_4

    .line 10698
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string p0, "android_crash_breakpad_minidump_size"

    sget-object p1, LX/03R;->YES:LX/03R;

    if-ne v0, p1, :cond_3

    :goto_2
    invoke-interface {v3, p0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    .line 10699
    :cond_4
    const/16 p0, 0x48

    if-ne p2, p0, :cond_6

    .line 10700
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string p0, "android_crash_breakpad_dump_external_process"

    sget-object p1, LX/03R;->YES:LX/03R;

    if-ne v0, p1, :cond_5

    :goto_3
    invoke-interface {v3, p0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_3

    .line 10701
    :cond_6
    const/16 p0, 0x2f6

    if-ne p2, p0, :cond_0

    .line 10702
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string p0, "breakpad_clone_at_install"

    sget-object p1, LX/03R;->YES:LX/03R;

    if-ne v0, p1, :cond_7

    :goto_4
    invoke-interface {v3, p0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    :cond_7
    move v1, v2

    goto :goto_4
.end method
