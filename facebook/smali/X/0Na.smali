.class public final LX/0Na;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0G6;


# instance fields
.field private final a:LX/0G6;

.field private final b:[B

.field private final c:[B

.field private d:Ljavax/crypto/CipherInputStream;


# direct methods
.method public constructor <init>(LX/0G6;[B[B)V
    .locals 0

    .prologue
    .line 51148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51149
    iput-object p1, p0, LX/0Na;->a:LX/0G6;

    .line 51150
    iput-object p2, p0, LX/0Na;->b:[B

    .line 51151
    iput-object p3, p0, LX/0Na;->c:[B

    .line 51152
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 1

    .prologue
    .line 51153
    iget-object v0, p0, LX/0Na;->d:Ljavax/crypto/CipherInputStream;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 51154
    iget-object v0, p0, LX/0Na;->d:Ljavax/crypto/CipherInputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljavax/crypto/CipherInputStream;->read([BII)I

    move-result v0

    .line 51155
    if-gez v0, :cond_0

    .line 51156
    const/4 v0, -0x1

    .line 51157
    :cond_0
    return v0

    .line 51158
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0OA;)J
    .locals 4

    .prologue
    .line 51159
    :try_start_0
    const-string v0, "AES/CBC/PKCS7Padding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 51160
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v2, p0, LX/0Na;->b:[B

    const-string v3, "AES"

    invoke-direct {v1, v2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 51161
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    iget-object v3, p0, LX/0Na;->c:[B

    invoke-direct {v2, v3}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 51162
    const/4 v3, 0x2

    :try_start_1
    invoke-virtual {v0, v3, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_1 .. :try_end_1} :catch_3

    .line 51163
    new-instance v1, Ljavax/crypto/CipherInputStream;

    new-instance v2, LX/0O9;

    iget-object v3, p0, LX/0Na;->a:LX/0G6;

    invoke-direct {v2, v3, p1}, LX/0O9;-><init>(LX/0G6;LX/0OA;)V

    invoke-direct {v1, v2, v0}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    iput-object v1, p0, LX/0Na;->d:Ljavax/crypto/CipherInputStream;

    .line 51164
    const-wide/16 v0, -0x1

    return-wide v0

    .line 51165
    :catch_0
    move-exception v0

    .line 51166
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 51167
    :catch_1
    move-exception v0

    .line 51168
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 51169
    :catch_2
    move-exception v0

    .line 51170
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 51171
    :catch_3
    move-exception v0

    .line 51172
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 51173
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Na;->d:Ljavax/crypto/CipherInputStream;

    .line 51174
    iget-object v0, p0, LX/0Na;->a:LX/0G6;

    invoke-interface {v0}, LX/0G6;->a()V

    .line 51175
    return-void
.end method
