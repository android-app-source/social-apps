.class public final enum LX/0GL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0GL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0GL;

.field public static final enum CANCELED:LX/0GL;

.field public static final enum COMPLETED:LX/0GL;

.field public static final enum FAILED:LX/0GL;

.field public static final enum PENDING:LX/0GL;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34942
    new-instance v0, LX/0GL;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v2}, LX/0GL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0GL;->PENDING:LX/0GL;

    .line 34943
    new-instance v0, LX/0GL;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v3}, LX/0GL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0GL;->COMPLETED:LX/0GL;

    .line 34944
    new-instance v0, LX/0GL;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, LX/0GL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0GL;->FAILED:LX/0GL;

    .line 34945
    new-instance v0, LX/0GL;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v5}, LX/0GL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0GL;->CANCELED:LX/0GL;

    .line 34946
    const/4 v0, 0x4

    new-array v0, v0, [LX/0GL;

    sget-object v1, LX/0GL;->PENDING:LX/0GL;

    aput-object v1, v0, v2

    sget-object v1, LX/0GL;->COMPLETED:LX/0GL;

    aput-object v1, v0, v3

    sget-object v1, LX/0GL;->FAILED:LX/0GL;

    aput-object v1, v0, v4

    sget-object v1, LX/0GL;->CANCELED:LX/0GL;

    aput-object v1, v0, v5

    sput-object v0, LX/0GL;->$VALUES:[LX/0GL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34947
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0GL;
    .locals 1

    .prologue
    .line 34948
    const-class v0, LX/0GL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0GL;

    return-object v0
.end method

.method public static values()[LX/0GL;
    .locals 1

    .prologue
    .line 34949
    sget-object v0, LX/0GL;->$VALUES:[LX/0GL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0GL;

    return-object v0
.end method
