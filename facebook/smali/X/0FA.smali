.class public final enum LX/0FA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0FA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0FA;

.field public static final enum SOURCE_MENU_SHARE:LX/0FA;

.field public static final enum SOURCE_SDK_SHARE:LX/0FA;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32891
    new-instance v0, LX/0FA;

    const-string v1, "SOURCE_SDK_SHARE"

    invoke-direct {v0, v1, v2}, LX/0FA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0FA;->SOURCE_SDK_SHARE:LX/0FA;

    .line 32892
    new-instance v0, LX/0FA;

    const-string v1, "SOURCE_MENU_SHARE"

    invoke-direct {v0, v1, v3}, LX/0FA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0FA;->SOURCE_MENU_SHARE:LX/0FA;

    .line 32893
    const/4 v0, 0x2

    new-array v0, v0, [LX/0FA;

    sget-object v1, LX/0FA;->SOURCE_SDK_SHARE:LX/0FA;

    aput-object v1, v0, v2

    sget-object v1, LX/0FA;->SOURCE_MENU_SHARE:LX/0FA;

    aput-object v1, v0, v3

    sput-object v0, LX/0FA;->$VALUES:[LX/0FA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32894
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0FA;
    .locals 1

    .prologue
    .line 32895
    const-class v0, LX/0FA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0FA;

    return-object v0
.end method

.method public static values()[LX/0FA;
    .locals 1

    .prologue
    .line 32896
    sget-object v0, LX/0FA;->$VALUES:[LX/0FA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0FA;

    return-object v0
.end method
