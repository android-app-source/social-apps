.class public final LX/08v;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field public static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/08y;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Landroid/media/MediaCodecInfo$CodecCapabilities;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21692
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/08v;->a:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 21693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 21694
    const-string v0, "video/avc"

    invoke-static {v0, v1}, LX/08v;->b(Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v0

    .line 21695
    if-nez v0, :cond_0

    .line 21696
    :goto_0
    return v1

    .line 21697
    :cond_0
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaCodecInfo$CodecCapabilities;

    move v2, v1

    .line 21698
    :goto_1
    iget-object v3, v0, Landroid/media/MediaCodecInfo$CodecCapabilities;->profileLevels:[Landroid/media/MediaCodecInfo$CodecProfileLevel;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 21699
    iget-object v3, v0, Landroid/media/MediaCodecInfo$CodecCapabilities;->profileLevels:[Landroid/media/MediaCodecInfo$CodecProfileLevel;

    aget-object v3, v3, v1

    .line 21700
    iget v3, v3, Landroid/media/MediaCodecInfo$CodecProfileLevel;->level:I

    const/high16 v7, 0x200000

    const v6, 0x65400

    const/16 v4, 0x6300

    const v5, 0x18c00

    .line 21701
    sparse-switch v3, :sswitch_data_0

    .line 21702
    const/4 v4, -0x1

    :goto_2
    :sswitch_0
    move v3, v4

    .line 21703
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 21704
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v1, v2

    .line 21705
    goto :goto_0

    :sswitch_1
    move v4, v5

    .line 21706
    goto :goto_2

    :sswitch_2
    move v4, v5

    .line 21707
    goto :goto_2

    :sswitch_3
    move v4, v5

    .line 21708
    goto :goto_2

    .line 21709
    :sswitch_4
    const v4, 0x31800

    goto :goto_2

    :sswitch_5
    move v4, v6

    .line 21710
    goto :goto_2

    :sswitch_6
    move v4, v6

    .line 21711
    goto :goto_2

    .line 21712
    :sswitch_7
    const v4, 0xe1000

    goto :goto_2

    .line 21713
    :sswitch_8
    const/high16 v4, 0x140000

    goto :goto_2

    :sswitch_9
    move v4, v7

    .line 21714
    goto :goto_2

    :sswitch_a
    move v4, v7

    .line 21715
    goto :goto_2

    .line 21716
    :sswitch_b
    const/high16 v4, 0x220000

    goto :goto_2

    .line 21717
    :sswitch_c
    const v4, 0x564000

    goto :goto_2

    .line 21718
    :sswitch_d
    const/high16 v4, 0x900000

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
        0x40 -> :sswitch_4
        0x80 -> :sswitch_5
        0x100 -> :sswitch_6
        0x200 -> :sswitch_7
        0x400 -> :sswitch_8
        0x800 -> :sswitch_9
        0x1000 -> :sswitch_a
        0x2000 -> :sswitch_b
        0x4000 -> :sswitch_c
        0x8000 -> :sswitch_d
    .end sparse-switch
.end method

.method public static a(Ljava/lang/String;Z)LX/08w;
    .locals 3

    .prologue
    .line 21719
    invoke-static {p0, p1}, LX/08v;->b(Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v1

    .line 21720
    if-nez v1, :cond_0

    .line 21721
    const/4 v0, 0x0

    .line 21722
    :goto_0
    return-object v0

    :cond_0
    new-instance v2, LX/08w;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/media/MediaCodecInfo$CodecCapabilities;

    .line 21723
    sget p0, LX/08x;->a:I

    const/16 p1, 0x13

    if-lt p0, p1, :cond_1

    .line 21724
    const-string p0, "adaptive-playback"

    invoke-virtual {v1, p0}, Landroid/media/MediaCodecInfo$CodecCapabilities;->isFeatureSupported(Ljava/lang/String;)Z

    move-result p0

    move p0, p0

    .line 21725
    :goto_1
    move v1, p0

    .line 21726
    invoke-direct {v2, v0, v1}, LX/08w;-><init>(Ljava/lang/String;Z)V

    move-object v0, v2

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_1
.end method

.method private static a(LX/08y;LX/08z;)Landroid/util/Pair;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/08y;",
            "LX/08z;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Landroid/media/MediaCodecInfo$CodecCapabilities;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21727
    :try_start_0
    const/4 v1, 0x0

    .line 21728
    iget-object v4, p0, LX/08y;->a:Ljava/lang/String;

    .line 21729
    invoke-interface {p1}, LX/08z;->a()I

    move-result v5

    .line 21730
    invoke-interface {p1}, LX/08z;->b()Z

    move-result v6

    move v3, v1

    .line 21731
    :goto_0
    if-ge v3, v5, :cond_8

    .line 21732
    invoke-interface {p1, v3}, LX/08z;->a(I)Landroid/media/MediaCodecInfo;

    move-result-object v7

    .line 21733
    invoke-virtual {v7}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v8

    .line 21734
    const/16 v11, 0x12

    const/16 v10, 0x10

    const/4 v0, 0x0

    .line 21735
    invoke-virtual {v7}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v2

    if-nez v2, :cond_0

    if-nez v6, :cond_9

    const-string v2, ".secure"

    invoke-virtual {v8, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 21736
    :cond_0
    :goto_1
    move v0, v0

    .line 21737
    if-eqz v0, :cond_7

    .line 21738
    invoke-virtual {v7}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v9

    move v0, v1

    .line 21739
    :goto_2
    array-length v2, v9

    if-ge v0, v2, :cond_7

    .line 21740
    aget-object v2, v9, v0

    .line 21741
    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 21742
    invoke-virtual {v7, v2}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v10

    .line 21743
    iget-object v2, p0, LX/08y;->a:Ljava/lang/String;

    invoke-interface {p1, v2, v10}, LX/08z;->a(Ljava/lang/String;Landroid/media/MediaCodecInfo$CodecCapabilities;)Z

    move-result v11

    .line 21744
    if-nez v6, :cond_4

    .line 21745
    sget-object v12, LX/08v;->a:Ljava/util/HashMap;

    iget-boolean v2, p0, LX/08y;->b:Z

    if-eqz v2, :cond_2

    new-instance v2, LX/08y;

    invoke-direct {v2, v4, v1}, LX/08y;-><init>(Ljava/lang/String;Z)V

    :goto_3
    invoke-static {v8, v10}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v13

    invoke-virtual {v12, v2, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21746
    if-eqz v11, :cond_1

    .line 21747
    sget-object v11, LX/08v;->a:Ljava/util/HashMap;

    iget-boolean v2, p0, LX/08y;->b:Z

    if-eqz v2, :cond_3

    move-object v2, p0

    :goto_4
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".secure"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v10}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v10

    invoke-virtual {v11, v2, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21748
    :cond_1
    :goto_5
    sget-object v2, LX/08v;->a:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 21749
    sget-object v0, LX/08v;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 21750
    :goto_6
    move-object v0, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 21751
    return-object v0

    .line 21752
    :catch_0
    move-exception v0

    .line 21753
    new-instance v1, LX/090;

    invoke-direct {v1, v0}, LX/090;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    move-object v2, p0

    .line 21754
    goto :goto_3

    .line 21755
    :cond_3
    new-instance v2, LX/08y;

    const/4 v12, 0x1

    invoke-direct {v2, v4, v12}, LX/08y;-><init>(Ljava/lang/String;Z)V

    goto :goto_4

    .line 21756
    :cond_4
    sget-object v12, LX/08v;->a:Ljava/util/HashMap;

    iget-boolean v2, p0, LX/08y;->b:Z

    if-ne v2, v11, :cond_5

    move-object v2, p0

    :goto_7
    invoke-static {v8, v10}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v10

    invoke-virtual {v12, v2, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_5
    new-instance v2, LX/08y;

    invoke-direct {v2, v4, v11}, LX/08y;-><init>(Ljava/lang/String;Z)V

    goto :goto_7

    .line 21757
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 21758
    :cond_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 21759
    :cond_8
    const/4 v0, 0x0

    goto :goto_6

    .line 21760
    :cond_9
    sget v2, LX/08x;->a:I

    const/16 v9, 0x15

    if-ge v2, v9, :cond_a

    const-string v2, "CIPAACDecoder"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_a
    const-string v2, "CIPMP3Decoder"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "CIPVorbisDecoder"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "AACDecoder"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "MP3Decoder"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 21761
    sget v2, LX/08x;->a:I

    if-ge v2, v11, :cond_b

    const-string v2, "OMX.SEC.MP3.Decoder"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 21762
    :cond_b
    sget v2, LX/08x;->a:I

    if-ge v2, v11, :cond_c

    const-string v2, "OMX.MTK.AUDIO.DECODER.AAC"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "a70"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 21763
    :cond_c
    sget v2, LX/08x;->a:I

    if-ne v2, v10, :cond_d

    const-string v2, "OMX.qcom.audio.decoder.mp3"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    const-string v2, "dlxu"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "protou"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "C6602"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "C6603"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "C6606"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "C6616"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "L36h"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SO-02E"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 21764
    :cond_d
    sget v2, LX/08x;->a:I

    if-ne v2, v10, :cond_e

    const-string v2, "OMX.qcom.audio.decoder.aac"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "C1504"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "C1505"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "C1604"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "C1605"

    sget-object v9, LX/08x;->b:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 21765
    :cond_e
    sget v2, LX/08x;->a:I

    const/16 v9, 0x13

    if-gt v2, v9, :cond_10

    sget-object v2, LX/08x;->b:Ljava/lang/String;

    if-eqz v2, :cond_10

    sget-object v2, LX/08x;->b:Ljava/lang/String;

    const-string v9, "d2"

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_f

    sget-object v2, LX/08x;->b:Ljava/lang/String;

    const-string v9, "serrano"

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    :cond_f
    const-string v2, "samsung"

    sget-object v9, LX/08x;->c:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string v2, "OMX.SEC.vp8.dec"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 21766
    :cond_10
    const/4 v0, 0x1

    goto/16 :goto_1
.end method

.method private static declared-synchronized b(Ljava/lang/String;Z)Landroid/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Landroid/media/MediaCodecInfo$CodecCapabilities;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v4, 0x15

    .line 21767
    const-class v2, LX/08v;

    monitor-enter v2

    :try_start_0
    new-instance v1, LX/08y;

    invoke-direct {v1, p0, p1}, LX/08y;-><init>(Ljava/lang/String;Z)V

    .line 21768
    sget-object v0, LX/08v;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21769
    sget-object v0, LX/08v;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 21770
    :cond_0
    :goto_0
    monitor-exit v2

    return-object v0

    .line 21771
    :cond_1
    :try_start_1
    sget v0, LX/08x;->a:I

    if-lt v0, v4, :cond_3

    new-instance v0, LX/091;

    invoke-direct {v0, p1}, LX/091;-><init>(Z)V

    .line 21772
    :goto_1
    invoke-static {v1, v0}, LX/08v;->a(LX/08y;LX/08z;)Landroid/util/Pair;

    move-result-object v0

    .line 21773
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    sget v3, LX/08x;->a:I

    if-gt v4, v3, :cond_0

    sget v3, LX/08x;->a:I

    const/16 v4, 0x17

    if-gt v3, v4, :cond_0

    .line 21774
    new-instance v0, LX/092;

    invoke-direct {v0}, LX/092;-><init>()V

    .line 21775
    invoke-static {v1, v0}, LX/08v;->a(LX/08y;LX/08z;)Landroid/util/Pair;

    move-result-object v1

    .line 21776
    if-eqz v1, :cond_2

    .line 21777
    const-string v3, "MediaCodecUtil"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "MediaCodecList API didn\'t list secure decoder for: "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ". Assuming: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 21778
    :cond_3
    new-instance v0, LX/092;

    invoke-direct {v0}, LX/092;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 21779
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static c(Ljava/lang/String;Z)Landroid/media/MediaCodecInfo$VideoCapabilities;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 21780
    invoke-static {p0, p1}, LX/08v;->b(Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v0

    .line 21781
    if-nez v0, :cond_0

    .line 21782
    const/4 v0, 0x0

    .line 21783
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaCodecInfo$CodecCapabilities;

    invoke-virtual {v0}, Landroid/media/MediaCodecInfo$CodecCapabilities;->getVideoCapabilities()Landroid/media/MediaCodecInfo$VideoCapabilities;

    move-result-object v0

    goto :goto_0
.end method
