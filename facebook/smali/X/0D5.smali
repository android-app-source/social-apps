.class public LX/0D5;
.super LX/0D4;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-java.lang.System.currentTimeMillis"
    }
.end annotation

.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0D3;

.field public c:LX/0Dh;

.field public d:LX/0DI;

.field public e:LX/0Cu;

.field public f:LX/0Ck;

.field public g:Ljava/lang/String;

.field public h:J

.field public i:J

.field public j:J

.field public k:J

.field public l:J

.field public m:J

.field public n:J

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29485
    const-class v0, LX/0D5;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0D5;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, -0x1

    .line 29497
    invoke-direct {p0, p1, p2, p3}, LX/0D4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29498
    const/4 v0, 0x0

    iput-object v0, p0, LX/0D5;->g:Ljava/lang/String;

    .line 29499
    iput-wide v2, p0, LX/0D5;->h:J

    .line 29500
    iput-wide v2, p0, LX/0D5;->i:J

    .line 29501
    iput-wide v2, p0, LX/0D5;->j:J

    .line 29502
    iput-wide v2, p0, LX/0D5;->k:J

    .line 29503
    iput-wide v2, p0, LX/0D5;->l:J

    .line 29504
    iput-wide v2, p0, LX/0D5;->m:J

    .line 29505
    iput-wide v2, p0, LX/0D5;->n:J

    .line 29506
    iput-boolean v1, p0, LX/0D5;->q:Z

    .line 29507
    iput-boolean v1, p0, LX/0D5;->r:Z

    .line 29508
    iput-boolean v1, p0, LX/0D5;->s:Z

    .line 29509
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 29510
    new-instance v0, LX/0Dh;

    invoke-direct {v0, p0}, LX/0Dh;-><init>(LX/0D5;)V

    iput-object v0, p0, LX/0D5;->c:LX/0Dh;

    .line 29511
    return-void
.end method

.method public static a(Landroid/webkit/WebBackForwardList;I)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 29486
    invoke-virtual {p0}, Landroid/webkit/WebBackForwardList;->getSize()I

    move-result v2

    if-le p1, v2, :cond_5

    .line 29487
    invoke-virtual {p0}, Landroid/webkit/WebBackForwardList;->getSize()I

    move-result v2

    .line 29488
    :goto_0
    if-nez v2, :cond_1

    .line 29489
    :cond_0
    :goto_1
    return v0

    .line 29490
    :cond_1
    if-ne v2, v1, :cond_2

    .line 29491
    const-string v2, "about:blank"

    invoke-virtual {p0, v0}, Landroid/webkit/WebBackForwardList;->getItemAtIndex(I)Landroid/webkit/WebHistoryItem;

    move-result-object v3

    invoke-virtual {v3}, Landroid/webkit/WebHistoryItem;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_1

    .line 29492
    :cond_2
    invoke-virtual {p0, v0}, Landroid/webkit/WebBackForwardList;->getItemAtIndex(I)Landroid/webkit/WebHistoryItem;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebHistoryItem;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 29493
    invoke-virtual {p0, v1}, Landroid/webkit/WebBackForwardList;->getItemAtIndex(I)Landroid/webkit/WebHistoryItem;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebHistoryItem;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 29494
    const-string v3, "about:blank"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 29495
    :cond_3
    add-int/lit8 v0, v2, -0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 29496
    goto :goto_1

    :cond_5
    move v2, p1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 29362
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "javascript: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 29363
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 29364
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v1}, LX/0D5;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29365
    :goto_0
    return-void

    .line 29366
    :catch_0
    :cond_0
    invoke-virtual {p0, v0}, LX/0D5;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 29481
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    .line 29482
    sget-object v0, LX/0D5;->a:Ljava/lang/String;

    const-string v1, "Javascript bridge is unsupported for this version"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29483
    :goto_0
    return-void

    .line 29484
    :cond_0
    invoke-super {p0, p1, p2}, LX/0D4;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 14

    .prologue
    .line 29432
    iget-object v0, p0, LX/0D5;->c:LX/0Dh;

    .line 29433
    iget-boolean v1, v0, LX/0Dh;->b:Z

    if-nez v1, :cond_1

    .line 29434
    :cond_0
    :goto_0
    return-void

    .line 29435
    :cond_1
    const-string v1, "FBNavResponseEnd:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 29436
    iget-object v1, v0, LX/0Dh;->a:LX/0D5;

    const/16 v2, 0x11

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0Dh;->b(Ljava/lang/String;)J

    move-result-wide v3

    const/4 v13, 0x0

    .line 29437
    invoke-virtual {v1}, LX/0D5;->b()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 29438
    iget-object v5, v1, LX/0D5;->c:LX/0Dh;

    .line 29439
    iput-boolean v13, v5, LX/0Dh;->b:Z

    .line 29440
    :cond_2
    :goto_1
    goto :goto_0

    .line 29441
    :cond_3
    const-string v1, "FBNavDomContentLoaded:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 29442
    iget-object v1, v0, LX/0Dh;->a:LX/0D5;

    const/16 v2, 0x16

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0Dh;->b(Ljava/lang/String;)J

    move-result-wide v3

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 29443
    invoke-virtual {v1}, LX/0D5;->b()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 29444
    iget-object v5, v1, LX/0D5;->c:LX/0Dh;

    .line 29445
    iput-boolean v11, v5, LX/0Dh;->b:Z

    .line 29446
    :cond_4
    :goto_2
    goto :goto_0

    .line 29447
    :cond_5
    const-string v1, "FBNavLoadEventEnd:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 29448
    iget-object v1, v0, LX/0Dh;->a:LX/0D5;

    const/16 v2, 0x12

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0Dh;->b(Ljava/lang/String;)J

    move-result-wide v3

    const/4 v13, 0x0

    .line 29449
    invoke-virtual {v1}, LX/0D5;->b()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 29450
    iget-object v5, v1, LX/0D5;->c:LX/0Dh;

    .line 29451
    iput-boolean v13, v5, LX/0Dh;->b:Z

    .line 29452
    :cond_6
    :goto_3
    goto :goto_0

    .line 29453
    :cond_7
    const-string v1, "FBNavAmpDetect:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29454
    iget-object v1, v0, LX/0Dh;->a:LX/0D5;

    const/16 v2, 0xf

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    const/4 v5, 0x0

    .line 29455
    invoke-virtual {v1}, LX/0D5;->b()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 29456
    iget-object v3, v1, LX/0D5;->c:LX/0Dh;

    .line 29457
    iput-boolean v5, v3, LX/0Dh;->b:Z

    .line 29458
    :goto_4
    goto/16 :goto_0

    .line 29459
    :cond_8
    iget-wide v5, v1, LX/0D5;->i:J

    cmp-long v5, v5, v3

    if-eqz v5, :cond_2

    .line 29460
    iput-wide v3, v1, LX/0D5;->i:J

    .line 29461
    iget-wide v5, v1, LX/0D5;->l:J

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_2

    .line 29462
    sget-object v5, LX/0D5;->a:Ljava/lang/String;

    const-string v6, "onResponseEnd: %d ms"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    iget-wide v9, v1, LX/0D5;->i:J

    iget-wide v11, v1, LX/0D5;->l:J

    sub-long/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v13

    invoke-static {v5, v6, v7}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 29463
    :cond_9
    iget-wide v5, v1, LX/0D5;->j:J

    cmp-long v5, v5, v3

    if-eqz v5, :cond_4

    .line 29464
    iput-wide v3, v1, LX/0D5;->j:J

    .line 29465
    invoke-virtual {v1}, LX/0D5;->getContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 29466
    if-eqz v5, :cond_a

    const-string v6, "BrowserLiteIntent.EXTRA_JS_TO_EXECUTE"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 29467
    const-string v6, "BrowserLiteIntent.EXTRA_JS_TO_EXECUTE"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/0D5;->a(Ljava/lang/String;)V

    .line 29468
    const-string v6, "BrowserLiteIntent.EXTRA_JS_TO_EXECUTE"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29469
    :cond_a
    iget-wide v5, v1, LX/0D5;->l:J

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_b

    .line 29470
    sget-object v5, LX/0D5;->a:Ljava/lang/String;

    const-string v6, "==DomContentLoaded: %d ms=="

    new-array v7, v12, [Ljava/lang/Object;

    iget-wide v9, v1, LX/0D5;->l:J

    sub-long v9, v3, v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v5, v6, v7}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29471
    :cond_b
    iget-boolean v5, v1, LX/0D5;->s:Z

    if-nez v5, :cond_4

    iget-object v5, v1, LX/0D5;->e:LX/0Cu;

    if-eqz v5, :cond_4

    .line 29472
    iget-object v5, v1, LX/0D5;->e:LX/0Cu;

    invoke-virtual {v5, v1}, LX/0Cu;->a(LX/0D5;)V

    .line 29473
    iput-boolean v12, v1, LX/0D5;->s:Z

    goto/16 :goto_2

    .line 29474
    :cond_c
    iget-wide v5, v1, LX/0D5;->k:J

    cmp-long v5, v5, v3

    if-eqz v5, :cond_6

    .line 29475
    iput-wide v3, v1, LX/0D5;->k:J

    .line 29476
    iget-wide v5, v1, LX/0D5;->l:J

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_6

    .line 29477
    sget-object v5, LX/0D5;->a:Ljava/lang/String;

    const-string v6, "==onLoadEventEnd: %d ms=="

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    iget-wide v9, v1, LX/0D5;->k:J

    iget-wide v11, v1, LX/0D5;->l:J

    sub-long/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v13

    invoke-static {v5, v6, v7}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 29478
    :cond_d
    iget-boolean v3, v1, LX/0D5;->r:Z

    if-nez v3, :cond_e

    if-eqz v2, :cond_e

    .line 29479
    sget-object v3, LX/0D5;->a:Ljava/lang/String;

    const-string v4, "AMP powered page detected!"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29480
    :cond_e
    iput-boolean v2, v1, LX/0D5;->r:Z

    goto/16 :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 29431
    invoke-virtual {p0}, LX/0D5;->canGoBack()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0D5;->canGoForward()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final canGoBack()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 29423
    invoke-super {p0}, LX/0D4;->canGoBack()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 29424
    invoke-virtual {p0}, LX/0D5;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v2

    .line 29425
    invoke-virtual {v2}, Landroid/webkit/WebBackForwardList;->getSize()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_1

    invoke-virtual {v2}, Landroid/webkit/WebBackForwardList;->getCurrentIndex()I

    move-result v3

    if-ne v3, v1, :cond_1

    .line 29426
    invoke-virtual {v2, v0}, Landroid/webkit/WebBackForwardList;->getItemAtIndex(I)Landroid/webkit/WebHistoryItem;

    move-result-object v3

    invoke-virtual {v3}, Landroid/webkit/WebHistoryItem;->getUrl()Ljava/lang/String;

    move-result-object v3

    .line 29427
    invoke-virtual {v2, v1}, Landroid/webkit/WebBackForwardList;->getItemAtIndex(I)Landroid/webkit/WebHistoryItem;

    move-result-object v2

    invoke-virtual {v2}, Landroid/webkit/WebHistoryItem;->getUrl()Ljava/lang/String;

    move-result-object v2

    .line 29428
    const-string v4, "about:blank"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 29429
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 29430
    goto :goto_0
.end method

.method public getDomContentloadedTime()J
    .locals 2

    .prologue
    .line 29422
    iget-wide v0, p0, LX/0D5;->j:J

    return-wide v0
.end method

.method public getFirstScrollReadyTime()J
    .locals 2

    .prologue
    .line 29421
    iget-wide v0, p0, LX/0D5;->h:J

    return-wide v0
.end method

.method public getFirstUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29417
    invoke-virtual {p0}, LX/0D5;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v0

    .line 29418
    invoke-virtual {v0}, Landroid/webkit/WebBackForwardList;->getSize()I

    move-result v1

    if-lez v1, :cond_0

    .line 29419
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebBackForwardList;->getItemAtIndex(I)Landroid/webkit/WebHistoryItem;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebHistoryItem;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 29420
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHitRefreshButton()Z
    .locals 1

    .prologue
    .line 29416
    iget-boolean v0, p0, LX/0D5;->q:Z

    return v0
.end method

.method public getHorizontalScrollRange()I
    .locals 1

    .prologue
    .line 29415
    invoke-virtual {p0}, LX/0D5;->computeHorizontalScrollRange()I

    move-result v0

    return v0
.end method

.method public getIsAmp()Z
    .locals 1

    .prologue
    .line 29512
    iget-boolean v0, p0, LX/0D5;->r:Z

    return v0
.end method

.method public getLoadEventEndTime()J
    .locals 2

    .prologue
    .line 29414
    iget-wide v0, p0, LX/0D5;->k:J

    return-wide v0
.end method

.method public getNonBlankNavigationDepthTotal()I
    .locals 2

    .prologue
    .line 29412
    invoke-virtual {p0}, LX/0D5;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v0

    .line 29413
    invoke-virtual {v0}, Landroid/webkit/WebBackForwardList;->getSize()I

    move-result v1

    invoke-static {v0, v1}, LX/0D5;->a(Landroid/webkit/WebBackForwardList;I)I

    move-result v0

    return v0
.end method

.method public getResponseEndTime()J
    .locals 2

    .prologue
    .line 29411
    iget-wide v0, p0, LX/0D5;->i:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 29407
    invoke-super {p0}, LX/0D4;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 29408
    if-nez v0, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ge v1, v2, :cond_0

    .line 29409
    iget-object v0, p0, LX/0D5;->g:Ljava/lang/String;

    .line 29410
    :cond_0
    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 29398
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    .line 29399
    invoke-super {p0}, LX/0D4;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 29400
    :cond_0
    :goto_0
    return-object v0

    .line 29401
    :cond_1
    const/4 v0, 0x0

    .line 29402
    :try_start_0
    invoke-virtual {p0}, LX/0D5;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v1

    .line 29403
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/webkit/WebBackForwardList;->getCurrentItem()Landroid/webkit/WebHistoryItem;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/webkit/WebBackForwardList;->getCurrentItem()Landroid/webkit/WebHistoryItem;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebHistoryItem;->getUrl()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 29404
    :cond_2
    :goto_1
    move-object v0, v0

    .line 29405
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29406
    :cond_3
    invoke-super {p0}, LX/0D4;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public getVerticalScrollRange()I
    .locals 1

    .prologue
    .line 29397
    invoke-virtual {p0}, LX/0D5;->computeVerticalScrollRange()I

    move-result v0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x30f9ee1a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 29390
    invoke-super {p0}, LX/0D4;->onAttachedToWindow()V

    .line 29391
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ge v1, v2, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 29392
    const-string v1, "accessibility"

    invoke-virtual {p0, v1}, Landroid/webkit/WebView;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 29393
    const-string v1, "accessibilityTraversal"

    invoke-virtual {p0, v1}, Landroid/webkit/WebView;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 29394
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v1, v2, :cond_0

    .line 29395
    const-string v1, "searchBoxJavaBridge_"

    invoke-virtual {p0, v1}, Landroid/webkit/WebView;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 29396
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x16f3a6de

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 29381
    invoke-super {p0, p1}, LX/0D4;->onDraw(Landroid/graphics/Canvas;)V

    .line 29382
    iget-wide v0, p0, LX/0D5;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    invoke-virtual {p0}, LX/0D5;->computeVerticalScrollRange()I

    move-result v0

    invoke-virtual {p0}, LX/0D5;->getHeight()I

    move-result v1

    if-le v0, v1, :cond_1

    iget-boolean v0, p0, LX/0D5;->s:Z

    if-nez v0, :cond_1

    .line 29383
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/0D5;->h:J

    .line 29384
    iget-wide v0, p0, LX/0D5;->l:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 29385
    sget-object v0, LX/0D5;->a:Ljava/lang/String;

    const-string v1, "==onScrollReady: %d ms=="

    new-array v2, v8, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, LX/0D5;->h:J

    iget-wide v6, p0, LX/0D5;->l:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29386
    :cond_0
    iget-object v0, p0, LX/0D5;->e:LX/0Cu;

    if-eqz v0, :cond_1

    .line 29387
    iget-object v0, p0, LX/0D5;->e:LX/0Cu;

    invoke-virtual {v0, p0}, LX/0Cu;->a(LX/0D5;)V

    .line 29388
    iput-boolean v8, p0, LX/0D5;->s:Z

    .line 29389
    :cond_1
    return-void
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 29377
    :try_start_0
    invoke-super {p0}, LX/0D4;->onPause()V

    .line 29378
    iget-object v0, p0, LX/0D5;->b:LX/0D3;

    if-eqz v0, :cond_0

    .line 29379
    iget-object v0, p0, LX/0D5;->b:LX/0D3;

    invoke-virtual {v0}, LX/0D3;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 29380
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 29373
    invoke-super {p0}, LX/0D4;->onResume()V

    .line 29374
    iget-object v0, p0, LX/0D5;->b:LX/0D3;

    if-eqz v0, :cond_0

    .line 29375
    iget-object v0, p0, LX/0D5;->b:LX/0D3;

    invoke-virtual {v0}, LX/0D3;->b()V

    .line 29376
    :cond_0
    return-void
.end method

.method public final onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 29370
    iget-object v0, p0, LX/0D5;->f:LX/0Ck;

    if-eqz v0, :cond_0

    .line 29371
    iget-object v0, p0, LX/0D5;->f:LX/0Ck;

    invoke-virtual {v0, p2, p4}, LX/0Ck;->a(II)V

    .line 29372
    :cond_0
    return-void
.end method

.method public setWebChromeClient(Landroid/webkit/WebChromeClient;)V
    .locals 0

    .prologue
    .line 29367
    invoke-super {p0, p1}, LX/0D4;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 29368
    check-cast p1, LX/0D3;

    iput-object p1, p0, LX/0D5;->b:LX/0D3;

    .line 29369
    return-void
.end method
