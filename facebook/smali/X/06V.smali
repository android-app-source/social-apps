.class public LX/06V;
.super Ljava/util/concurrent/AbstractExecutorService;
.source ""


# instance fields
.field public a:LX/06Z;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 17962
    invoke-direct {p0}, Ljava/util/concurrent/AbstractExecutorService;-><init>()V

    .line 17963
    invoke-static {}, LX/06W;->a()LX/06W;

    move-result-object v0

    const-string v1, "MqttService"

    .line 17964
    iput-object v1, v0, LX/06W;->b:Ljava/lang/String;

    .line 17965
    move-object v0, v0

    .line 17966
    invoke-virtual {v0}, LX/06W;->b()LX/06Z;

    move-result-object v0

    iput-object v0, p0, LX/06V;->a:LX/06Z;

    return-void
.end method


# virtual methods
.method public final awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .prologue
    .line 17961
    const/4 v0, 0x0

    return v0
.end method

.method public final execute(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 17959
    iget-object v0, p0, LX/06V;->a:LX/06Z;

    const v1, -0x37cda904

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 17960
    return-void
.end method

.method public final isShutdown()Z
    .locals 1

    .prologue
    .line 17955
    const/4 v0, 0x0

    return v0
.end method

.method public final isTerminated()Z
    .locals 1

    .prologue
    .line 17958
    const/4 v0, 0x0

    return v0
.end method

.method public final shutdown()V
    .locals 0

    .prologue
    .line 17957
    return-void
.end method

.method public final shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17956
    const/4 v0, 0x0

    return-object v0
.end method
