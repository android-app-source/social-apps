.class public final enum LX/07S;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/07S;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/07S;

.field public static final enum CONNACK:LX/07S;

.field public static final enum CONNECT:LX/07S;

.field public static final enum DISCONNECT:LX/07S;

.field public static final enum PINGREQ:LX/07S;

.field public static final enum PINGRESP:LX/07S;

.field public static final enum PUBACK:LX/07S;

.field public static final enum PUBCOMP:LX/07S;

.field public static final enum PUBLISH:LX/07S;

.field public static final enum PUBREC:LX/07S;

.field public static final enum PUBREL:LX/07S;

.field public static final enum SUBACK:LX/07S;

.field public static final enum SUBSCRIBE:LX/07S;

.field public static final enum UNSUBACK:LX/07S;

.field public static final enum UNSUBSCRIBE:LX/07S;

.field private static final map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/07S;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 19943
    new-instance v0, LX/07S;

    const-string v1, "CONNECT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->CONNECT:LX/07S;

    .line 19944
    new-instance v0, LX/07S;

    const-string v1, "CONNACK"

    invoke-direct {v0, v1, v4, v5}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->CONNACK:LX/07S;

    .line 19945
    new-instance v0, LX/07S;

    const-string v1, "PUBLISH"

    invoke-direct {v0, v1, v5, v6}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->PUBLISH:LX/07S;

    .line 19946
    new-instance v0, LX/07S;

    const-string v1, "PUBACK"

    invoke-direct {v0, v1, v6, v7}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->PUBACK:LX/07S;

    .line 19947
    new-instance v0, LX/07S;

    const-string v1, "PUBREC"

    invoke-direct {v0, v1, v7, v8}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->PUBREC:LX/07S;

    .line 19948
    new-instance v0, LX/07S;

    const-string v1, "PUBREL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->PUBREL:LX/07S;

    .line 19949
    new-instance v0, LX/07S;

    const-string v1, "PUBCOMP"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->PUBCOMP:LX/07S;

    .line 19950
    new-instance v0, LX/07S;

    const-string v1, "SUBSCRIBE"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->SUBSCRIBE:LX/07S;

    .line 19951
    new-instance v0, LX/07S;

    const-string v1, "SUBACK"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->SUBACK:LX/07S;

    .line 19952
    new-instance v0, LX/07S;

    const-string v1, "UNSUBSCRIBE"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->UNSUBSCRIBE:LX/07S;

    .line 19953
    new-instance v0, LX/07S;

    const-string v1, "UNSUBACK"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->UNSUBACK:LX/07S;

    .line 19954
    new-instance v0, LX/07S;

    const-string v1, "PINGREQ"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->PINGREQ:LX/07S;

    .line 19955
    new-instance v0, LX/07S;

    const-string v1, "PINGRESP"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->PINGRESP:LX/07S;

    .line 19956
    new-instance v0, LX/07S;

    const-string v1, "DISCONNECT"

    const/16 v2, 0xd

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, LX/07S;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/07S;->DISCONNECT:LX/07S;

    .line 19957
    const/16 v0, 0xe

    new-array v0, v0, [LX/07S;

    const/4 v1, 0x0

    sget-object v2, LX/07S;->CONNECT:LX/07S;

    aput-object v2, v0, v1

    sget-object v1, LX/07S;->CONNACK:LX/07S;

    aput-object v1, v0, v4

    sget-object v1, LX/07S;->PUBLISH:LX/07S;

    aput-object v1, v0, v5

    sget-object v1, LX/07S;->PUBACK:LX/07S;

    aput-object v1, v0, v6

    sget-object v1, LX/07S;->PUBREC:LX/07S;

    aput-object v1, v0, v7

    sget-object v1, LX/07S;->PUBREL:LX/07S;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, LX/07S;->PUBCOMP:LX/07S;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/07S;->SUBSCRIBE:LX/07S;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/07S;->SUBACK:LX/07S;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/07S;->UNSUBSCRIBE:LX/07S;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/07S;->UNSUBACK:LX/07S;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/07S;->PINGREQ:LX/07S;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/07S;->PINGRESP:LX/07S;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/07S;->DISCONNECT:LX/07S;

    aput-object v2, v0, v1

    sput-object v0, LX/07S;->$VALUES:[LX/07S;

    .line 19958
    new-instance v0, LX/07T;

    invoke-direct {v0}, LX/07T;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, LX/07S;->map:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 19940
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19941
    iput p3, p0, LX/07S;->mValue:I

    .line 19942
    return-void
.end method

.method public static fromInt(I)LX/07S;
    .locals 2

    .prologue
    .line 19959
    sget-object v0, LX/07S;->map:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/07S;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/07S;
    .locals 1

    .prologue
    .line 19939
    const-class v0, LX/07S;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/07S;

    return-object v0
.end method

.method public static values()[LX/07S;
    .locals 1

    .prologue
    .line 19938
    sget-object v0, LX/07S;->$VALUES:[LX/07S;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/07S;

    return-object v0
.end method
