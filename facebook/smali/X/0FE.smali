.class public final enum LX/0FE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0FE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0FE;

.field public static final enum REBOOT:LX/0FE;

.field public static final enum REINSTALL:LX/0FE;

.field public static final enum UNKNOWN:LX/0FE;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32935
    new-instance v0, LX/0FE;

    const-string v1, "REINSTALL"

    invoke-direct {v0, v1, v2}, LX/0FE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0FE;->REINSTALL:LX/0FE;

    .line 32936
    new-instance v0, LX/0FE;

    const-string v1, "REBOOT"

    invoke-direct {v0, v1, v3}, LX/0FE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0FE;->REBOOT:LX/0FE;

    .line 32937
    new-instance v0, LX/0FE;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/0FE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0FE;->UNKNOWN:LX/0FE;

    .line 32938
    const/4 v0, 0x3

    new-array v0, v0, [LX/0FE;

    sget-object v1, LX/0FE;->REINSTALL:LX/0FE;

    aput-object v1, v0, v2

    sget-object v1, LX/0FE;->REBOOT:LX/0FE;

    aput-object v1, v0, v3

    sget-object v1, LX/0FE;->UNKNOWN:LX/0FE;

    aput-object v1, v0, v4

    sput-object v0, LX/0FE;->$VALUES:[LX/0FE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32939
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0FE;
    .locals 1

    .prologue
    .line 32940
    const-class v0, LX/0FE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0FE;

    return-object v0
.end method

.method public static values()[LX/0FE;
    .locals 1

    .prologue
    .line 32941
    sget-object v0, LX/0FE;->$VALUES:[LX/0FE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0FE;

    return-object v0
.end method
