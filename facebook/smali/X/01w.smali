.class public final LX/01w;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/01t;


# direct methods
.method public constructor <init>(LX/01t;)V
    .locals 0

    .prologue
    .line 5770
    iput-object p1, p0, LX/01w;->a:LX/01t;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 5771
    iget v0, p1, Landroid/os/Message;->arg1:I

    const v1, 0x354754c6

    .line 5772
    move v1, v1

    .line 5773
    invoke-static {v0, v1}, Lcom/facebook/loom/logger/api/LoomLogger;->a(II)I

    move-result v1

    .line 5774
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 5775
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5776
    :catchall_0
    move-exception v0

    const v2, -0x4f0eebb2

    .line 5777
    move v2, v2

    .line 5778
    invoke-static {v1, v2}, Lcom/facebook/loom/logger/api/LoomLogger;->b(II)I

    throw v0

    .line 5779
    :pswitch_0
    :try_start_1
    iget-object v0, p0, LX/01w;->a:LX/01t;

    const/4 v2, 0x0

    .line 5780
    iput-boolean v2, v0, LX/01t;->i:Z

    .line 5781
    iget-object v0, p0, LX/01w;->a:LX/01t;

    invoke-virtual {v0}, LX/01t;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5782
    :goto_0
    const v0, 0x1f88ad47

    .line 5783
    move v0, v0

    .line 5784
    invoke-static {v1, v0}, Lcom/facebook/loom/logger/api/LoomLogger;->b(II)I

    .line 5785
    return-void

    .line 5786
    :pswitch_1
    :try_start_2
    iget-object v0, p0, LX/01w;->a:LX/01t;

    const/4 v2, 0x1

    .line 5787
    iput-boolean v2, v0, LX/01t;->j:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5788
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
