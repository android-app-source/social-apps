.class public final enum LX/0Ht;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Ht;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Ht;

.field public static final enum NOT_CONNECTED:LX/0Ht;

.field public static final enum REF_CODE_EXPIRED:LX/0Ht;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38218
    new-instance v0, LX/0Ht;

    const-string v1, "NOT_CONNECTED"

    invoke-direct {v0, v1, v2}, LX/0Ht;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Ht;->NOT_CONNECTED:LX/0Ht;

    .line 38219
    new-instance v0, LX/0Ht;

    const-string v1, "REF_CODE_EXPIRED"

    invoke-direct {v0, v1, v3}, LX/0Ht;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Ht;->REF_CODE_EXPIRED:LX/0Ht;

    .line 38220
    const/4 v0, 0x2

    new-array v0, v0, [LX/0Ht;

    sget-object v1, LX/0Ht;->NOT_CONNECTED:LX/0Ht;

    aput-object v1, v0, v2

    sget-object v1, LX/0Ht;->REF_CODE_EXPIRED:LX/0Ht;

    aput-object v1, v0, v3

    sput-object v0, LX/0Ht;->$VALUES:[LX/0Ht;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38221
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Ht;
    .locals 1

    .prologue
    .line 38222
    const-class v0, LX/0Ht;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Ht;

    return-object v0
.end method

.method public static values()[LX/0Ht;
    .locals 1

    .prologue
    .line 38223
    sget-object v0, LX/0Ht;->$VALUES:[LX/0Ht;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Ht;

    return-object v0
.end method
