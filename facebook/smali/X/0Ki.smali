.class public LX/0Ki;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

.field public final b:[J

.field public c:Z

.field public d:I

.field private e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:J

.field public j:J

.field public k:I

.field public l:I

.field public m:J

.field public n:J


# direct methods
.method public constructor <init>(Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41889
    const/16 v0, 0xa

    new-array v0, v0, [J

    iput-object v0, p0, LX/0Ki;->b:[J

    .line 41890
    iput-object p1, p0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    .line 41891
    iput-boolean v1, p0, LX/0Ki;->c:Z

    .line 41892
    iput v1, p0, LX/0Ki;->h:I

    .line 41893
    return-void
.end method

.method private static a(LX/0Ki;J)J
    .locals 3

    .prologue
    .line 41887
    iget v0, p0, LX/0Ki;->g:I

    if-eqz v0, :cond_0

    iget v0, p0, LX/0Ki;->g:I

    mul-int/lit8 v0, v0, 0x2

    int-to-long v0, v0

    div-long v0, p1, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0Ki;J)J
    .locals 5

    .prologue
    .line 41886
    iget v0, p0, LX/0Ki;->f:I

    if-eqz v0, :cond_0

    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p1

    iget v2, p0, LX/0Ki;->f:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static l(LX/0Ki;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 41881
    iput-wide v2, p0, LX/0Ki;->m:J

    .line 41882
    iput v0, p0, LX/0Ki;->l:I

    .line 41883
    iput v0, p0, LX/0Ki;->k:I

    .line 41884
    iput-wide v2, p0, LX/0Ki;->n:J

    .line 41885
    return-void
.end method

.method public static m(LX/0Ki;)J
    .locals 4

    .prologue
    .line 41880
    iget-object v0, p0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    iget v2, p0, LX/0Ki;->f:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;IIJ)I
    .locals 10

    .prologue
    .line 41855
    const/4 v0, 0x0

    .line 41856
    iget v1, p0, LX/0Ki;->d:I

    if-nez v1, :cond_0

    .line 41857
    iput p3, p0, LX/0Ki;->d:I

    .line 41858
    iput p2, p0, LX/0Ki;->e:I

    .line 41859
    int-to-long v2, p3

    invoke-static {p0, v2, v3}, LX/0Ki;->a(LX/0Ki;J)J

    move-result-wide v2

    .line 41860
    invoke-static {p0, v2, v3}, LX/0Ki;->b(LX/0Ki;J)J

    move-result-wide v2

    .line 41861
    sub-long v2, p4, v2

    .line 41862
    iget v1, p0, LX/0Ki;->h:I

    if-nez v1, :cond_2

    .line 41863
    const-wide/16 v4, 0x0

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, LX/0Ki;->i:J

    .line 41864
    const/4 v1, 0x1

    iput v1, p0, LX/0Ki;->h:I

    .line 41865
    :cond_0
    :goto_0
    iget v1, p0, LX/0Ki;->e:I

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 41866
    iget-object v1, p0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v1, p1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 41867
    iget v2, p0, LX/0Ki;->d:I

    sub-int/2addr v2, v1

    iput v2, p0, LX/0Ki;->d:I

    .line 41868
    iget-wide v2, p0, LX/0Ki;->j:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0Ki;->j:J

    .line 41869
    iget v2, p0, LX/0Ki;->e:I

    add-int/2addr v1, v2

    iput v1, p0, LX/0Ki;->e:I

    .line 41870
    iget v1, p0, LX/0Ki;->d:I

    if-nez v1, :cond_1

    .line 41871
    or-int/lit8 v0, v0, 0x2

    .line 41872
    :cond_1
    return v0

    .line 41873
    :cond_2
    iget-wide v4, p0, LX/0Ki;->i:J

    iget-wide v6, p0, LX/0Ki;->j:J

    invoke-static {p0, v6, v7}, LX/0Ki;->a(LX/0Ki;J)J

    move-result-wide v6

    invoke-static {p0, v6, v7}, LX/0Ki;->b(LX/0Ki;J)J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 41874
    iget v1, p0, LX/0Ki;->h:I

    const/4 v6, 0x1

    if-ne v1, v6, :cond_3

    sub-long v6, v4, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    const-wide/32 v8, 0x30d40

    cmp-long v1, v6, v8

    if-lez v1, :cond_3

    .line 41875
    const/4 v1, 0x2

    iput v1, p0, LX/0Ki;->h:I

    .line 41876
    :cond_3
    iget v1, p0, LX/0Ki;->h:I

    const/4 v6, 0x2

    if-ne v1, v6, :cond_0

    .line 41877
    iget-wide v0, p0, LX/0Ki;->i:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0Ki;->i:J

    .line 41878
    const/4 v0, 0x1

    iput v0, p0, LX/0Ki;->h:I

    .line 41879
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 41854
    iget-boolean v0, p0, LX/0Ki;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 41852
    iget-object v0, p0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->e()V

    .line 41853
    return-void
.end method

.method public final f()Z
    .locals 4

    .prologue
    .line 41851
    invoke-virtual {p0}, LX/0Ki;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/0Ki;->j:J

    invoke-static {p0, v0, v1}, LX/0Ki;->a(LX/0Ki;J)J

    move-result-wide v0

    iget-object v2, p0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    invoke-virtual {v2}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()J
    .locals 14

    .prologue
    .line 41826
    invoke-virtual {p0}, LX/0Ki;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, LX/0Ki;->h:I

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 41827
    if-nez v0, :cond_0

    .line 41828
    const-wide/high16 v0, -0x8000000000000000L

    .line 41829
    :goto_1
    return-wide v0

    .line 41830
    :cond_0
    iget-object v0, p0, LX/0Ki;->a:Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    .line 41831
    iget-object v1, v0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->g:LX/0Ke;

    move-object v0, v1

    .line 41832
    sget-object v1, LX/0Ke;->PLAYING:LX/0Ke;

    if-ne v0, v1, :cond_1

    .line 41833
    const-wide/16 v12, 0x0

    .line 41834
    invoke-static {p0}, LX/0Ki;->m(LX/0Ki;)J

    move-result-wide v4

    .line 41835
    cmp-long v6, v4, v12

    if-nez v6, :cond_4

    .line 41836
    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 41837
    iget v2, p0, LX/0Ki;->l:I

    if-nez v2, :cond_2

    .line 41838
    invoke-static {p0}, LX/0Ki;->m(LX/0Ki;)J

    move-result-wide v0

    iget-wide v2, p0, LX/0Ki;->i:J

    add-long/2addr v0, v2

    goto :goto_1

    .line 41839
    :cond_2
    iget-wide v2, p0, LX/0Ki;->m:J

    add-long/2addr v0, v2

    iget-wide v2, p0, LX/0Ki;->i:J

    add-long/2addr v0, v2

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 41840
    :cond_4
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    .line 41841
    iget-wide v8, p0, LX/0Ki;->n:J

    sub-long v8, v6, v8

    const-wide/16 v10, 0x7530

    cmp-long v8, v8, v10

    if-ltz v8, :cond_1

    .line 41842
    iget-object v8, p0, LX/0Ki;->b:[J

    iget v9, p0, LX/0Ki;->k:I

    sub-long/2addr v4, v6

    aput-wide v4, v8, v9

    .line 41843
    iget v4, p0, LX/0Ki;->k:I

    add-int/lit8 v4, v4, 0x1

    rem-int/lit8 v4, v4, 0xa

    iput v4, p0, LX/0Ki;->k:I

    .line 41844
    iget v4, p0, LX/0Ki;->l:I

    const/16 v5, 0xa

    if-ge v4, v5, :cond_5

    .line 41845
    iget v4, p0, LX/0Ki;->l:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/0Ki;->l:I

    .line 41846
    :cond_5
    iput-wide v6, p0, LX/0Ki;->n:J

    .line 41847
    iput-wide v12, p0, LX/0Ki;->m:J

    .line 41848
    const/4 v4, 0x0

    :goto_2
    iget v5, p0, LX/0Ki;->l:I

    if-ge v4, v5, :cond_1

    .line 41849
    iget-wide v6, p0, LX/0Ki;->m:J

    iget-object v5, p0, LX/0Ki;->b:[J

    aget-wide v8, v5, v4

    iget v5, p0, LX/0Ki;->l:I

    int-to-long v10, v5

    div-long/2addr v8, v10

    add-long/2addr v6, v8

    iput-wide v6, p0, LX/0Ki;->m:J

    .line 41850
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method
