.class public final LX/0OU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0K7;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0K7;",
        "Ljava/util/Comparator",
        "<",
        "LX/0OT;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "LX/0OT;",
            ">;"
        }
    .end annotation
.end field

.field private c:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 53160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53161
    iput-wide p1, p0, LX/0OU;->a:J

    .line 53162
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0, p0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, LX/0OU;->b:Ljava/util/TreeSet;

    .line 53163
    return-void
.end method

.method private b(LX/0OQ;J)V
    .locals 4

    .prologue
    .line 53140
    :goto_0
    iget-wide v0, p0, LX/0OU;->c:J

    add-long/2addr v0, p2

    iget-wide v2, p0, LX/0OU;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 53141
    iget-object v0, p0, LX/0OU;->b:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0OT;

    invoke-interface {p1, v0}, LX/0OQ;->b(LX/0OT;)V

    goto :goto_0

    .line 53142
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0OQ;J)V
    .locals 0

    .prologue
    .line 53158
    invoke-direct {p0, p1, p2, p3}, LX/0OU;->b(LX/0OQ;J)V

    .line 53159
    return-void
.end method

.method public final a(LX/0OQ;LX/0OT;)V
    .locals 4

    .prologue
    .line 53154
    iget-object v0, p0, LX/0OU;->b:Ljava/util/TreeSet;

    invoke-virtual {v0, p2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 53155
    iget-wide v0, p0, LX/0OU;->c:J

    iget-wide v2, p2, LX/0OT;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0OU;->c:J

    .line 53156
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/0OU;->b(LX/0OQ;J)V

    .line 53157
    return-void
.end method

.method public final a(LX/0OQ;LX/0OT;LX/0OT;)V
    .locals 0

    .prologue
    .line 53151
    invoke-virtual {p0, p2}, LX/0OU;->a(LX/0OT;)V

    .line 53152
    invoke-virtual {p0, p1, p3}, LX/0OU;->a(LX/0OQ;LX/0OT;)V

    .line 53153
    return-void
.end method

.method public final a(LX/0OT;)V
    .locals 4

    .prologue
    .line 53148
    iget-object v0, p0, LX/0OU;->b:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 53149
    iget-wide v0, p0, LX/0OU;->c:J

    iget-wide v2, p1, LX/0OT;->c:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/0OU;->c:J

    .line 53150
    return-void
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 53143
    check-cast p1, LX/0OT;

    check-cast p2, LX/0OT;

    .line 53144
    iget-wide v0, p1, LX/0OT;->f:J

    iget-wide v2, p2, LX/0OT;->f:J

    sub-long/2addr v0, v2

    .line 53145
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 53146
    invoke-virtual {p1, p2}, LX/0OT;->a(LX/0OT;)I

    move-result v0

    .line 53147
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p1, LX/0OT;->f:J

    iget-wide v2, p2, LX/0OT;->f:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
