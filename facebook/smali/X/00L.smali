.class public LX/00L;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final LOG_TAG:Ljava/lang/String;

.field public static mANRDetector:LX/00O;

.field public static mANRReport:LX/00N;

.field public static mConfig:LX/00K;

.field public static mReportSender:LX/00M;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2129
    const-class v0, LX/00L;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/00L;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static closeStreamNoException(Ljava/io/Closeable;)V
    .locals 3

    .prologue
    .line 2152
    if-nez p0, :cond_0

    .line 2153
    :goto_0
    return-void

    .line 2154
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2155
    :catch_0
    move-exception v0

    .line 2156
    sget-object v1, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Error while closing stream: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getCachedANRGKValue()Z
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SharedPreferencesUse"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2148
    sget-object v0, LX/00L;->mConfig:LX/00K;

    move-object v0, v0

    .line 2149
    iget-object v1, v0, LX/00K;->mApplicationContext:Landroid/content/Context;

    move-object v0, v1

    .line 2150
    const-string v1, "acra_flags_store"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2151
    const-string v1, "anr_gk_cached"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static initSenderHost(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2158
    :try_start_0
    const-string v0, "report_host.txt"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 2159
    invoke-virtual {v2}, Ljava/io/File;->canRead()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2160
    invoke-static {v1}, LX/00L;->closeStreamNoException(Ljava/io/Closeable;)V

    .line 2161
    :goto_0
    return-void

    .line 2162
    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2163
    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 2164
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ACRA read host from host file "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2165
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2166
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setting crash reporting host to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2167
    sget-object v2, LX/00L;->mReportSender:LX/00M;

    invoke-virtual {v2, v1}, LX/00M;->setHost(Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2168
    :cond_1
    invoke-static {v0}, LX/00L;->closeStreamNoException(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_0
    move-object v0, v1

    :goto_1
    invoke-static {v0}, LX/00L;->closeStreamNoException(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    invoke-static {v1}, LX/00L;->closeStreamNoException(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_2

    :catch_1
    goto :goto_1
.end method

.method public static setReportHost(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2145
    sget-object v0, LX/00L;->mReportSender:LX/00M;

    invoke-virtual {v0, p0}, LX/00M;->setHost(Ljava/lang/String;)Z

    .line 2146
    invoke-static {p0}, LX/00L;->writeSenderHost(Ljava/lang/String;)V

    .line 2147
    return-void
.end method

.method public static startANRDetector()V
    .locals 1

    .prologue
    .line 2143
    sget-object v0, LX/00L;->mANRDetector:LX/00O;

    invoke-virtual {v0}, LX/00O;->start()V

    .line 2144
    return-void
.end method

.method private static writeSenderHost(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2130
    const/4 v2, 0x0

    .line 2131
    :try_start_0
    sget-object v0, LX/00L;->mConfig:LX/00K;

    .line 2132
    iget-object v1, v0, LX/00K;->mApplicationContext:Landroid/content/Context;

    move-object v0, v1

    .line 2133
    const-string v1, "report_host.txt"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    .line 2134
    new-instance v1, Ljava/io/OutputStreamWriter;

    invoke-direct {v1, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2135
    :try_start_1
    invoke-virtual {v1, p0}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 2136
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2137
    invoke-static {v1}, LX/00L;->closeStreamNoException(Ljava/io/Closeable;)V

    .line 2138
    :goto_0
    return-void

    .line 2139
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 2140
    :goto_1
    :try_start_2
    sget-object v2, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v3, "could not write to host file: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2141
    invoke-static {v1}, LX/00L;->closeStreamNoException(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    invoke-static {v2}, LX/00L;->closeStreamNoException(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_2

    .line 2142
    :catch_1
    move-exception v0

    goto :goto_1
.end method
