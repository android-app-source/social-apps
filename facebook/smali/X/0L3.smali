.class public final LX/0L3;
.super Ljava/lang/Exception;
.source ""


# instance fields
.field public final decoderName:Ljava/lang/String;

.field public final diagnosticInfo:Ljava/lang/String;

.field public final mimeType:Ljava/lang/String;

.field public final secureDecoderRequired:Z


# direct methods
.method public constructor <init>(LX/0L4;Ljava/lang/Throwable;ZI)V
    .locals 2

    .prologue
    .line 42878
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Decoder init failed: ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 42879
    iget-object v0, p1, LX/0L4;->b:Ljava/lang/String;

    iput-object v0, p0, LX/0L3;->mimeType:Ljava/lang/String;

    .line 42880
    iput-boolean p3, p0, LX/0L3;->secureDecoderRequired:Z

    .line 42881
    const/4 v0, 0x0

    iput-object v0, p0, LX/0L3;->decoderName:Ljava/lang/String;

    .line 42882
    if-gez p4, :cond_0

    const-string v0, "neg_"

    .line 42883
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string p1, "com.google.android.exoplayer.MediaCodecTrackRenderer_"

    invoke-direct {v1, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p4}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 42884
    iput-object v0, p0, LX/0L3;->diagnosticInfo:Ljava/lang/String;

    .line 42885
    return-void

    .line 42886
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public constructor <init>(LX/0L4;Ljava/lang/Throwable;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 42887
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Decoder init failed: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 42888
    iget-object v0, p1, LX/0L4;->b:Ljava/lang/String;

    iput-object v0, p0, LX/0L3;->mimeType:Ljava/lang/String;

    .line 42889
    iput-boolean p3, p0, LX/0L3;->secureDecoderRequired:Z

    .line 42890
    iput-object p4, p0, LX/0L3;->decoderName:Ljava/lang/String;

    .line 42891
    sget v0, LX/08x;->a:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 42892
    instance-of v0, p2, Landroid/media/MediaCodec$CodecException;

    if-eqz v0, :cond_1

    .line 42893
    check-cast p2, Landroid/media/MediaCodec$CodecException;

    invoke-virtual {p2}, Landroid/media/MediaCodec$CodecException;->getDiagnosticInfo()Ljava/lang/String;

    move-result-object v0

    .line 42894
    :goto_0
    move-object v0, v0

    .line 42895
    :goto_1
    iput-object v0, p0, LX/0L3;->diagnosticInfo:Ljava/lang/String;

    .line 42896
    return-void

    .line 42897
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
