.class public LX/0D4;
.super Landroid/webkit/WebView;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadSuperClassWebView.ProxyWebView",
        "DeprecatedSuperclass"
    }
.end annotation

.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 29359
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 29360
    invoke-direct {p0}, LX/0D4;->a()V

    .line 29361
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 29356
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29357
    invoke-direct {p0}, LX/0D4;->a()V

    .line 29358
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 29353
    invoke-virtual {p0}, LX/0D4;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 29354
    invoke-static {v0}, LX/0G0;->a(Landroid/webkit/WebSettings;)V

    .line 29355
    return-void
.end method
