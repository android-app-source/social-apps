.class public final LX/00U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/00T;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/app/FacebookApplication;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/app/FacebookApplication;)V
    .locals 0

    .prologue
    .line 2370
    iput-object p1, p0, LX/00U;->a:Lcom/facebook/katana/app/FacebookApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCustomData(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2371
    sget-object v0, LX/00w;->b:LX/00w;

    move-object v0, v0

    .line 2372
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2373
    invoke-static {v0}, LX/00w;->g(LX/00w;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    int-to-byte p0, v1

    .line 2374
    iget v1, v0, LX/00w;->g:I

    const/4 p1, 0x2

    if-ge v1, p1, :cond_0

    invoke-static {}, LX/00w;->j()I

    move-result v1

    invoke-static {v1}, LX/00w;->b(I)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_0
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2375
    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    shl-int/lit8 v1, v1, 0x1

    or-int/2addr v1, p0

    int-to-byte p0, v1

    .line 2376
    invoke-static {v0}, LX/00w;->i(LX/00w;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_3
    shl-int/lit8 v1, v1, 0x2

    or-int/2addr v1, p0

    int-to-byte p0, v1

    .line 2377
    invoke-static {v0}, LX/00w;->k(LX/00w;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_4
    shl-int/lit8 v1, v1, 0x3

    or-int/2addr v1, p0

    int-to-byte p0, v1

    .line 2378
    invoke-virtual {v0}, LX/00w;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_5
    shl-int/lit8 v1, v1, 0x4

    or-int/2addr v1, p0

    int-to-byte v1, v1

    .line 2379
    const-string p0, "0x%04X"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {p0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2380
    return-object v0

    :cond_1
    move v1, v3

    .line 2381
    goto :goto_0

    :cond_2
    move v1, v3

    .line 2382
    goto :goto_2

    :cond_3
    move v1, v3

    .line 2383
    goto :goto_3

    :cond_4
    move v1, v3

    .line 2384
    goto :goto_4

    :cond_5
    move v1, v3

    .line 2385
    goto :goto_5

    :cond_6
    const/4 v1, 0x0

    goto :goto_1
.end method
