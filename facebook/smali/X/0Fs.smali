.class public final LX/0Fs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/DoNotOptimize;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Throwable;)I
    .locals 1

    .prologue
    .line 34087
    instance-of v0, p0, Landroid/system/ErrnoException;

    if-eqz v0, :cond_0

    .line 34088
    check-cast p0, Landroid/system/ErrnoException;

    iget v0, p0, Landroid/system/ErrnoException;->errno:I

    .line 34089
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 34090
    invoke-static {p0}, Landroid/system/OsConstants;->errnoName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
