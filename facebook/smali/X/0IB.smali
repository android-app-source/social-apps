.class public abstract LX/0IB;
.super Landroid/app/IntentService;
.source ""


# instance fields
.field public a:LX/04v;

.field public b:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38399
    const-string v0, ""

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 38400
    new-instance v0, LX/04v;

    invoke-direct {v0, p0}, LX/04v;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/0IB;->a:LX/04v;

    .line 38401
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 38402
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 38403
    new-instance v0, LX/04v;

    invoke-direct {v0, p0}, LX/04v;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/0IB;->a:LX/04v;

    .line 38404
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/04v;Landroid/content/SharedPreferences;)V
    .locals 0
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 38405
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 38406
    iput-object p2, p0, LX/0IB;->a:LX/04v;

    .line 38407
    iput-object p3, p0, LX/0IB;->b:Landroid/content/SharedPreferences;

    .line 38408
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Intent;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38409
    return-void
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public final onHandleIntent(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 38410
    if-nez p1, :cond_0

    .line 38411
    :goto_0
    return-void

    .line 38412
    :cond_0
    :try_start_0
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 38413
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 38414
    const-string v1, "com.facebook.rti.fbns.intent.RECEIVE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38415
    :cond_1
    :goto_1
    invoke-static {p1}, LX/0HQ;->a(Landroid/content/Intent;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {p1}, LX/0HQ;->a(Landroid/content/Intent;)Z

    throw v0

    .line 38416
    :cond_2
    const-string v0, "FbnsCallbackHandlerBase"

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38417
    iget-object v0, p0, LX/0IB;->a:LX/04v;

    invoke-virtual {v0, p1}, LX/04v;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 38418
    const-string v0, "INVALID_SENDER"

    invoke-virtual {p0, v4, v0, v4}, LX/0IB;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_1

    .line 38419
    :cond_3
    const-string v0, "receive_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38420
    const-string v1, "message"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 38421
    const-string v0, "token"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38422
    iget-object v1, p0, LX/0IB;->b:Landroid/content/SharedPreferences;

    const-string v2, "token_key"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 38423
    const-string v2, "extra_notification_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 38424
    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 38425
    const-string v0, "FbnsCallbackHandlerBase"

    const-string v1, "Dropping unintended message."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38426
    const-string v0, "TOKEN_MISMATCH"

    invoke-virtual {p0, v2, v0, v4}, LX/0IB;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_1

    .line 38427
    :cond_4
    const-string v0, "FBNS_LITE_NOTIFICATION_RECEIVED"

    invoke-virtual {p0, v2, v0, v4}, LX/0IB;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 38428
    invoke-virtual {p0, p1}, LX/0IB;->a(Landroid/content/Intent;)V

    goto :goto_1

    .line 38429
    :cond_5
    const-string v1, "registered"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 38430
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38431
    iget-object v1, p0, LX/0IB;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "token_key"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 38432
    invoke-virtual {p0, v0}, LX/0IB;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 38433
    :cond_6
    const-string v1, "reg_error"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 38434
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38435
    invoke-virtual {p0, v0}, LX/0IB;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 38436
    :cond_7
    const-string v1, "deleted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 38437
    const-string v1, "unregistered"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 38438
    const-string v0, "FbnsCallbackHandlerBase"

    const-string v1, "Unknown message type"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, -0x1cccc1d3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 38439
    sget-object v1, LX/01p;->p:LX/01q;

    invoke-static {p0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, LX/0IB;->b:Landroid/content/SharedPreferences;

    .line 38440
    invoke-super {p0, p1, p2, p3}, Landroid/app/IntentService;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    const/16 v2, 0x25

    const v3, -0x3346e240    # -9.7054208E7f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method
