.class public LX/0Ic;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/05G;


# instance fields
.field private final a:Lcom/facebook/rti/push/service/FbnsService;

.field private b:LX/06y;

.field private c:LX/0Ih;


# direct methods
.method public constructor <init>(Lcom/facebook/rti/push/service/FbnsService;LX/0Ih;)V
    .locals 8

    .prologue
    .line 38708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38709
    iput-object p1, p0, LX/0Ic;->a:Lcom/facebook/rti/push/service/FbnsService;

    .line 38710
    iput-object p2, p0, LX/0Ic;->c:LX/0Ih;

    .line 38711
    iget-object v0, p0, LX/0Ic;->a:Lcom/facebook/rti/push/service/FbnsService;

    sget-object v1, LX/01p;->g:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 38712
    new-instance v1, LX/06y;

    const-string v2, "/settings/mqtt/id/mqtt_device_id"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/settings/mqtt/id/mqtt_device_secret"

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/settings/mqtt/id/timestamp"

    const-wide v6, 0x7fffffffffffffffL

    invoke-interface {v0, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, LX/06y;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    iput-object v1, p0, LX/0Ic;->b:LX/06y;

    .line 38713
    invoke-direct {p0}, LX/0Ic;->f()V

    .line 38714
    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 38768
    sget-object v0, LX/01p;->g:LX/01q;

    invoke-static {p0, v0}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 38769
    const-string v1, "/settings/mqtt/id/mqtt_device_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 6

    .prologue
    .line 38763
    iget-object v0, p0, LX/0Ic;->b:LX/06y;

    invoke-virtual {v0}, LX/06y;->a()Ljava/lang/String;

    move-result-object v0

    .line 38764
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/0Ic;->a:Lcom/facebook/rti/push/service/FbnsService;

    invoke-virtual {v0}, Lcom/facebook/rti/push/service/FbnsService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/04u;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38765
    new-instance v0, LX/06y;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, LX/06y;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 38766
    invoke-virtual {p0, v0}, LX/0Ic;->a(LX/06y;)Z

    .line 38767
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38762
    const-string v0, "567310203415052"

    return-object v0
.end method

.method public final a(Z)V
    .locals 13

    .prologue
    .line 38732
    if-eqz p1, :cond_0

    .line 38733
    iget-object v0, p0, LX/0Ic;->c:LX/0Ih;

    iget-object v1, p0, LX/0Ic;->b:LX/06y;

    .line 38734
    iget-object v2, v0, LX/0Ih;->c:LX/0Ij;

    const/4 v6, 0x0

    .line 38735
    invoke-static {v2, v1}, LX/0Ij;->b(LX/0Ij;LX/06y;)V

    .line 38736
    invoke-static {v2}, LX/0Ij;->b(LX/0Ij;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 38737
    iget-object v4, v2, LX/0Ij;->d:LX/01o;

    invoke-virtual {v4}, LX/01o;->a()J

    move-result-wide v7

    .line 38738
    const-string v4, "fbns_shared_sync_timestamp"

    const-wide/high16 v9, -0x8000000000000000L

    invoke-interface {v3, v4, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v9

    .line 38739
    sub-long/2addr v9, v7

    invoke-static {v9, v10}, Ljava/lang/Math;->abs(J)J

    move-result-wide v9

    const-wide/32 v11, 0x5265c00

    cmp-long v4, v9, v11

    if-gez v4, :cond_2

    .line 38740
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0Ic;->c:LX/0Ih;

    invoke-virtual {v0}, LX/0Ih;->a()LX/06y;

    move-result-object v0

    .line 38741
    invoke-virtual {v0}, LX/06y;->d()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, LX/0Ic;->a(LX/06y;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38742
    iget-object v0, p0, LX/0Ic;->a:Lcom/facebook/rti/push/service/FbnsService;

    invoke-virtual {v0}, Lcom/facebook/rti/push/service/FbnsService;->o()V

    .line 38743
    :cond_1
    return-void

    .line 38744
    :cond_2
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "fbns_shared_sync_timestamp"

    invoke-interface {v3, v4, v7, v8}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-static {v3}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 38745
    new-instance v4, Landroid/content/Intent;

    const-string v3, "com.facebook.rti.fbns.intent.SHARE_IDS"

    invoke-direct {v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 38746
    iget-object v3, v2, LX/0Ij;->c:LX/04v;

    .line 38747
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 38748
    invoke-virtual {v3, v4}, LX/04v;->e(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v5

    .line 38749
    iget-object v8, v3, LX/04v;->a:Landroid/content/Context;

    invoke-static {v8, v5}, LX/05U;->a(Landroid/content/Context;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v5

    .line 38750
    if-eqz v5, :cond_3

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_4

    :cond_3
    move-object v5, v7

    .line 38751
    :goto_1
    move-object v5, v5

    .line 38752
    iget-object v3, v2, LX/0Ij;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 38753
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 38754
    sget-object v3, LX/0Ij;->a:Ljava/lang/String;

    const-string v7, "requesting device id from %d receivers"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v3, v7, v8}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38755
    iget-object v3, v2, LX/0Ij;->c:LX/04v;

    iget-object v7, v2, LX/0Ij;->e:Landroid/content/BroadcastReceiver;

    const/4 v9, -0x1

    move-object v8, v6

    move-object v10, v6

    move-object v11, v6

    invoke-virtual/range {v3 .. v11}, LX/04v;->a(Landroid/content/Intent;Ljava/util/List;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)I

    goto :goto_0

    .line 38756
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 38757
    if-eqz v5, :cond_5

    iget-object v9, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v9, :cond_5

    iget-object v9, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    if-eqz v9, :cond_5

    iget-object v9, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 38758
    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    .line 38759
    invoke-virtual {v3, v5}, LX/04v;->a(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 38760
    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    move-object v5, v7

    .line 38761
    goto :goto_1
.end method

.method public final declared-synchronized a(LX/06y;)Z
    .locals 6

    .prologue
    .line 38722
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ic;->b:LX/06y;

    invoke-virtual {v0, p1}, LX/06y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 38723
    iget-object v0, p0, LX/0Ic;->a:Lcom/facebook/rti/push/service/FbnsService;

    sget-object v1, LX/01p;->g:LX/01q;

    invoke-static {v0, v1}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 38724
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "/settings/mqtt/id/mqtt_device_id"

    invoke-virtual {p1}, LX/06y;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "/settings/mqtt/id/mqtt_device_secret"

    invoke-virtual {p1}, LX/06y;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "/settings/mqtt/id/timestamp"

    .line 38725
    iget-wide v4, p1, LX/06y;->b:J

    move-wide v2, v4

    .line 38726
    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 38727
    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 38728
    iput-object p1, p0, LX/0Ic;->b:LX/06y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38729
    const/4 v0, 0x1

    .line 38730
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 38731
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38721
    const-string v0, "MQTT"

    return-object v0
.end method

.method public final declared-synchronized c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38720
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ic;->b:LX/06y;

    invoke-virtual {v0}, LX/06y;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38719
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ic;->b:LX/06y;

    invoke-virtual {v0}, LX/06y;->b()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 38715
    iget-object v0, p0, LX/0Ic;->c:LX/0Ih;

    invoke-virtual {v0}, LX/0Ih;->a()LX/06y;

    move-result-object v0

    .line 38716
    invoke-virtual {v0}, LX/06y;->d()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/0Ic;->a(LX/06y;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38717
    iget-object v0, p0, LX/0Ic;->a:Lcom/facebook/rti/push/service/FbnsService;

    invoke-virtual {v0}, Lcom/facebook/rti/push/service/FbnsService;->o()V

    .line 38718
    :cond_0
    return-void
.end method
