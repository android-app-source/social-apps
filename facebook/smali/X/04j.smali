.class public interface abstract LX/04j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)I
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)J
.end method

.method public abstract a()Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;
.end method

.method public abstract a(Ljava/lang/String;)Lcom/facebook/exoplayer/ipc/VideoCacheStatus;
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/VideoPlayRequest;)Lcom/facebook/exoplayer/ipc/VideoPlayerSession;
.end method

.method public abstract a(I)V
.end method

.method public abstract a(II)V
.end method

.method public abstract a(LX/042;)V
.end method

.method public abstract a(Landroid/net/Uri;)V
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;J)V
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/042;)V
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Landroid/net/Uri;)V
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;F)V
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Landroid/view/Surface;)V
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;)V
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/MediaRenderer;)V
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;)V
.end method

.method public abstract a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Z)V
.end method

.method public abstract a(Ljava/util/Map;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()V
.end method

.method public abstract b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/042;)V
.end method

.method public abstract b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Z)V
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Z
.end method

.method public abstract c(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Z)J
.end method

.method public abstract c(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
.end method

.method public abstract c(Ljava/lang/String;)V
.end method

.method public abstract d(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;
.end method

.method public abstract e(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J
.end method

.method public abstract f(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J
.end method

.method public abstract g(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J
.end method

.method public abstract h(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)I
.end method

.method public abstract i(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
.end method

.method public abstract j(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
.end method
