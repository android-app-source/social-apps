.class public final enum LX/0Hs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Hs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Hs;

.field public static final enum FAILED_CONNACK_READ:LX/0Hs;

.field public static final enum FAILED_CONNECTION_REFUSED:LX/0Hs;

.field public static final enum FAILED_CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD:LX/0Hs;

.field public static final enum FAILED_CONNECTION_REFUSED_NOT_AUTHORIZED:LX/0Hs;

.field public static final enum FAILED_CONNECTION_REFUSED_SERVER_SHEDDING_LOAD:LX/0Hs;

.field public static final enum FAILED_CONNECTION_UNKNOWN_CONNECT_HASH:LX/0Hs;

.field public static final enum FAILED_CONNECT_MESSAGE:LX/0Hs;

.field public static final enum FAILED_CREATE_IOSTREAM:LX/0Hs;

.field public static final enum FAILED_DNS_RESOLVE_TIMEOUT:LX/0Hs;

.field public static final enum FAILED_DNS_UNRESOLVED:LX/0Hs;

.field public static final enum FAILED_INVALID_CONACK:LX/0Hs;

.field public static final enum FAILED_MQTT_CONACK_TIMEOUT:LX/0Hs;

.field public static final enum FAILED_SOCKET_CONNECT_ERROR:LX/0Hs;

.field public static final enum FAILED_SOCKET_CONNECT_ERROR_SSL_CLOCK_SKEW:LX/0Hs;

.field public static final enum FAILED_SOCKET_CONNECT_TIMEOUT:LX/0Hs;

.field public static final enum FAILED_SOCKET_ERROR:LX/0Hs;

.field public static final enum FAILED_STOPPED_BEFORE_SSL:LX/0Hs;

.field public static final enum FAILED_UNEXPECTED_DISCONNECT:LX/0Hs;

.field public static final enum MQTT_ERROR:LX/0Hs;

.field public static final enum NETWORK_ERROR:LX/0Hs;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38150
    new-instance v0, LX/0Hs;

    const-string v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v3}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->NETWORK_ERROR:LX/0Hs;

    .line 38151
    new-instance v0, LX/0Hs;

    const-string v1, "MQTT_ERROR"

    invoke-direct {v0, v1, v4}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->MQTT_ERROR:LX/0Hs;

    .line 38152
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_SOCKET_ERROR"

    invoke-direct {v0, v1, v5}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_SOCKET_ERROR:LX/0Hs;

    .line 38153
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_SOCKET_CONNECT_ERROR"

    invoke-direct {v0, v1, v6}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_SOCKET_CONNECT_ERROR:LX/0Hs;

    .line 38154
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_SOCKET_CONNECT_TIMEOUT"

    invoke-direct {v0, v1, v7}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_SOCKET_CONNECT_TIMEOUT:LX/0Hs;

    .line 38155
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_DNS_RESOLVE_TIMEOUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_DNS_RESOLVE_TIMEOUT:LX/0Hs;

    .line 38156
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_MQTT_CONACK_TIMEOUT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_MQTT_CONACK_TIMEOUT:LX/0Hs;

    .line 38157
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_CONNECT_MESSAGE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_CONNECT_MESSAGE:LX/0Hs;

    .line 38158
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_CONNACK_READ"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_CONNACK_READ:LX/0Hs;

    .line 38159
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_INVALID_CONACK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_INVALID_CONACK:LX/0Hs;

    .line 38160
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_DNS_UNRESOLVED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_DNS_UNRESOLVED:LX/0Hs;

    .line 38161
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_CREATE_IOSTREAM"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_CREATE_IOSTREAM:LX/0Hs;

    .line 38162
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_CONNECTION_REFUSED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_CONNECTION_REFUSED:LX/0Hs;

    .line 38163
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_CONNECTION_REFUSED_SERVER_SHEDDING_LOAD"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_CONNECTION_REFUSED_SERVER_SHEDDING_LOAD:LX/0Hs;

    .line 38164
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_UNEXPECTED_DISCONNECT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_UNEXPECTED_DISCONNECT:LX/0Hs;

    .line 38165
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD:LX/0Hs;

    .line 38166
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_CONNECTION_REFUSED_NOT_AUTHORIZED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_CONNECTION_REFUSED_NOT_AUTHORIZED:LX/0Hs;

    .line 38167
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_CONNECTION_UNKNOWN_CONNECT_HASH"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_CONNECTION_UNKNOWN_CONNECT_HASH:LX/0Hs;

    .line 38168
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_SOCKET_CONNECT_ERROR_SSL_CLOCK_SKEW"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_SOCKET_CONNECT_ERROR_SSL_CLOCK_SKEW:LX/0Hs;

    .line 38169
    new-instance v0, LX/0Hs;

    const-string v1, "FAILED_STOPPED_BEFORE_SSL"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/0Hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Hs;->FAILED_STOPPED_BEFORE_SSL:LX/0Hs;

    .line 38170
    const/16 v0, 0x14

    new-array v0, v0, [LX/0Hs;

    sget-object v1, LX/0Hs;->NETWORK_ERROR:LX/0Hs;

    aput-object v1, v0, v3

    sget-object v1, LX/0Hs;->MQTT_ERROR:LX/0Hs;

    aput-object v1, v0, v4

    sget-object v1, LX/0Hs;->FAILED_SOCKET_ERROR:LX/0Hs;

    aput-object v1, v0, v5

    sget-object v1, LX/0Hs;->FAILED_SOCKET_CONNECT_ERROR:LX/0Hs;

    aput-object v1, v0, v6

    sget-object v1, LX/0Hs;->FAILED_SOCKET_CONNECT_TIMEOUT:LX/0Hs;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0Hs;->FAILED_DNS_RESOLVE_TIMEOUT:LX/0Hs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0Hs;->FAILED_MQTT_CONACK_TIMEOUT:LX/0Hs;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0Hs;->FAILED_CONNECT_MESSAGE:LX/0Hs;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0Hs;->FAILED_CONNACK_READ:LX/0Hs;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0Hs;->FAILED_INVALID_CONACK:LX/0Hs;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0Hs;->FAILED_DNS_UNRESOLVED:LX/0Hs;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0Hs;->FAILED_CREATE_IOSTREAM:LX/0Hs;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0Hs;->FAILED_CONNECTION_REFUSED:LX/0Hs;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0Hs;->FAILED_CONNECTION_REFUSED_SERVER_SHEDDING_LOAD:LX/0Hs;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0Hs;->FAILED_UNEXPECTED_DISCONNECT:LX/0Hs;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0Hs;->FAILED_CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD:LX/0Hs;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0Hs;->FAILED_CONNECTION_REFUSED_NOT_AUTHORIZED:LX/0Hs;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0Hs;->FAILED_CONNECTION_UNKNOWN_CONNECT_HASH:LX/0Hs;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/0Hs;->FAILED_SOCKET_CONNECT_ERROR_SSL_CLOCK_SKEW:LX/0Hs;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/0Hs;->FAILED_STOPPED_BEFORE_SSL:LX/0Hs;

    aput-object v2, v0, v1

    sput-object v0, LX/0Hs;->$VALUES:[LX/0Hs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38173
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Hs;
    .locals 1

    .prologue
    .line 38172
    const-class v0, LX/0Hs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Hs;

    return-object v0
.end method

.method public static values()[LX/0Hs;
    .locals 1

    .prologue
    .line 38171
    sget-object v0, LX/0Hs;->$VALUES:[LX/0Hs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Hs;

    return-object v0
.end method
