.class public LX/0La;
.super LX/0LR;
.source ""

# interfaces
.implements LX/0LT;


# instance fields
.field private final k:LX/0LV;

.field private final l:J

.field private final m:I

.field private final n:I

.field private o:LX/0L4;

.field private p:LX/0Lz;

.field private volatile q:I

.field private volatile r:Z


# direct methods
.method public constructor <init>(LX/0G6;LX/0OA;ILX/0AR;JJIJLX/0LV;LX/0L4;IILX/0Lz;ZI)V
    .locals 19

    .prologue
    .line 43990
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move/from16 v10, p3

    move-object/from16 v11, p4

    move-wide/from16 v12, p5

    move-wide/from16 v14, p7

    move/from16 v16, p9

    move/from16 v17, p17

    move/from16 v18, p18

    invoke-direct/range {v7 .. v18}, LX/0LR;-><init>(LX/0G6;LX/0OA;ILX/0AR;JJIZI)V

    .line 43991
    move-object/from16 v0, p12

    move-object/from16 v1, p0

    iput-object v0, v1, LX/0La;->k:LX/0LV;

    .line 43992
    move-wide/from16 v0, p10

    move-object/from16 v2, p0

    iput-wide v0, v2, LX/0La;->l:J

    .line 43993
    move/from16 v0, p14

    move-object/from16 v1, p0

    iput v0, v1, LX/0La;->m:I

    .line 43994
    move/from16 v0, p15

    move-object/from16 v1, p0

    iput v0, v1, LX/0La;->n:I

    .line 43995
    move-object/from16 v0, p13

    move-wide/from16 v1, p10

    move/from16 v3, p14

    move/from16 v4, p15

    invoke-static {v0, v1, v2, v3, v4}, LX/0La;->a(LX/0L4;JII)LX/0L4;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, LX/0La;->o:LX/0L4;

    .line 43996
    move-object/from16 v0, p16

    move-object/from16 v1, p0

    iput-object v0, v1, LX/0La;->p:LX/0Lz;

    .line 43997
    return-void
.end method

.method private static a(LX/0L4;JII)LX/0L4;
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 43998
    if-nez p0, :cond_1

    .line 43999
    const/4 v0, 0x0

    .line 44000
    :cond_0
    :goto_0
    return-object v0

    .line 44001
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_3

    iget-wide v0, p0, LX/0L4;->s:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 44002
    iget-wide v0, p0, LX/0L4;->s:J

    add-long/2addr v0, p1

    invoke-virtual {p0, v0, v1}, LX/0L4;->a(J)LX/0L4;

    move-result-object p0

    move-object v0, p0

    .line 44003
    :goto_1
    if-ne p3, v4, :cond_2

    if-eq p4, v4, :cond_0

    .line 44004
    :cond_2
    invoke-virtual {v0, p3, p4}, LX/0L4;->a(II)LX/0L4;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, p0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0MA;IZ)I
    .locals 1

    .prologue
    .line 44005
    iget-object v0, p0, LX/0LR;->k:LX/0MC;

    move-object v0, v0

    .line 44006
    invoke-virtual {v0, p1, p2, p3}, LX/0MC;->a(LX/0MA;IZ)I

    move-result v0

    return v0
.end method

.method public final a(JIII[B)V
    .locals 9

    .prologue
    .line 44011
    iget-object v0, p0, LX/0LR;->k:LX/0MC;

    move-object v1, v0

    .line 44012
    iget-wide v2, p0, LX/0La;->l:J

    add-long/2addr v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-virtual/range {v1 .. v7}, LX/0MC;->a(JIII[B)V

    .line 44013
    return-void
.end method

.method public final a(LX/0L4;)V
    .locals 4

    .prologue
    .line 44007
    iget-wide v0, p0, LX/0La;->l:J

    iget v2, p0, LX/0La;->m:I

    iget v3, p0, LX/0La;->n:I

    invoke-static {p1, v0, v1, v2, v3}, LX/0La;->a(LX/0L4;JII)LX/0L4;

    move-result-object v0

    iput-object v0, p0, LX/0La;->o:LX/0L4;

    .line 44008
    return-void
.end method

.method public final a(LX/0Lz;)V
    .locals 0

    .prologue
    .line 44009
    iput-object p1, p0, LX/0La;->p:LX/0Lz;

    .line 44010
    return-void
.end method

.method public final a(LX/0M8;)V
    .locals 0

    .prologue
    .line 43968
    return-void
.end method

.method public final a(LX/0Oj;I)V
    .locals 1

    .prologue
    .line 43987
    iget-object v0, p0, LX/0LR;->k:LX/0MC;

    move-object v0, v0

    .line 43988
    invoke-virtual {v0, p1, p2}, LX/0MC;->a(LX/0Oj;I)V

    .line 43989
    return-void
.end method

.method public final b()LX/0L4;
    .locals 1

    .prologue
    .line 43986
    iget-object v0, p0, LX/0La;->o:LX/0L4;

    return-object v0
.end method

.method public final c()LX/0Lz;
    .locals 1

    .prologue
    .line 43985
    iget-object v0, p0, LX/0La;->p:LX/0Lz;

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 43984
    iget v0, p0, LX/0La;->q:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 43982
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0La;->r:Z

    .line 43983
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 43981
    iget-boolean v0, p0, LX/0La;->r:Z

    return v0
.end method

.method public final h()V
    .locals 6

    .prologue
    .line 43969
    iget-object v0, p0, LX/0LP;->e:LX/0OA;

    iget v1, p0, LX/0La;->q:I

    invoke-static {v0, v1}, LX/08x;->a(LX/0OA;I)LX/0OA;

    move-result-object v4

    .line 43970
    :try_start_0
    new-instance v0, LX/0MB;

    iget-object v1, p0, LX/0LP;->g:LX/0G6;

    iget-wide v2, v4, LX/0OA;->c:J

    iget-object v5, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v5, v4}, LX/0G6;->a(LX/0OA;)J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, LX/0MB;-><init>(LX/0G6;JJ)V

    .line 43971
    iget v1, p0, LX/0La;->q:I

    if-nez v1, :cond_0

    .line 43972
    iget-object v1, p0, LX/0La;->k:LX/0LV;

    invoke-virtual {v1, p0}, LX/0LV;->a(LX/0LT;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 43973
    :cond_0
    const/4 v1, 0x0

    .line 43974
    :goto_0
    if-nez v1, :cond_1

    :try_start_1
    iget-boolean v1, p0, LX/0La;->r:Z

    if-nez v1, :cond_1

    .line 43975
    iget-object v1, p0, LX/0La;->k:LX/0LV;

    invoke-virtual {v1, v0}, LX/0LV;->a(LX/0MA;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_0

    .line 43976
    :cond_1
    :try_start_2
    invoke-interface {v0}, LX/0MA;->c()J

    move-result-wide v0

    iget-object v2, p0, LX/0LP;->e:LX/0OA;

    iget-wide v2, v2, LX/0OA;->c:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, LX/0La;->q:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 43977
    iget-object v0, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v0}, LX/0G6;->a()V

    .line 43978
    return-void

    .line 43979
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-interface {v0}, LX/0MA;->c()J

    move-result-wide v2

    iget-object v0, p0, LX/0LP;->e:LX/0OA;

    iget-wide v4, v0, LX/0OA;->c:J

    sub-long/2addr v2, v4

    long-to-int v0, v2

    iput v0, p0, LX/0La;->q:I

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 43980
    :catchall_1
    move-exception v0

    iget-object v1, p0, LX/0LP;->g:LX/0G6;

    invoke-interface {v1}, LX/0G6;->a()V

    throw v0
.end method
