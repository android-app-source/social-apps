.class public final enum LX/03R;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/03R;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/03R;

.field public static final enum NO:LX/03R;

.field public static final enum UNSET:LX/03R;

.field public static final enum YES:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10291
    new-instance v0, LX/03R;

    const-string v1, "YES"

    invoke-direct {v0, v1, v2}, LX/03R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/03R;->YES:LX/03R;

    .line 10292
    new-instance v0, LX/03R;

    const-string v1, "NO"

    invoke-direct {v0, v1, v3}, LX/03R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/03R;->NO:LX/03R;

    .line 10293
    new-instance v0, LX/03R;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v4}, LX/03R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/03R;->UNSET:LX/03R;

    .line 10294
    const/4 v0, 0x3

    new-array v0, v0, [LX/03R;

    sget-object v1, LX/03R;->YES:LX/03R;

    aput-object v1, v0, v2

    sget-object v1, LX/03R;->NO:LX/03R;

    aput-object v1, v0, v3

    sget-object v1, LX/03R;->UNSET:LX/03R;

    aput-object v1, v0, v4

    sput-object v0, LX/03R;->$VALUES:[LX/03R;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10289
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10290
    return-void
.end method

.method public static fromDbValue(I)LX/03R;
    .locals 1

    .prologue
    .line 10285
    packed-switch p0, :pswitch_data_0

    .line 10286
    sget-object v0, LX/03R;->UNSET:LX/03R;

    :goto_0
    return-object v0

    .line 10287
    :pswitch_0
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    .line 10288
    :pswitch_1
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/Boolean;)LX/03R;
    .locals 1

    .prologue
    .line 10284
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/03R;
    .locals 1

    .prologue
    .line 10260
    const-class v0, LX/03R;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/03R;

    return-object v0
.end method

.method public static valueOf(Z)LX/03R;
    .locals 1

    .prologue
    .line 10283
    if-eqz p0, :cond_0

    sget-object v0, LX/03R;->YES:LX/03R;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method

.method public static values()[LX/03R;
    .locals 1

    .prologue
    .line 10282
    sget-object v0, LX/03R;->$VALUES:[LX/03R;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/03R;

    return-object v0
.end method


# virtual methods
.method public final asBoolean()Z
    .locals 3

    .prologue
    .line 10277
    sget-object v0, LX/0Uj;->a:[I

    invoke-virtual {p0}, LX/03R;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 10278
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized TriState value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10279
    :pswitch_0
    const/4 v0, 0x1

    .line 10280
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 10281
    :pswitch_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No boolean equivalent for UNSET"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final asBoolean(Z)Z
    .locals 3

    .prologue
    .line 10272
    sget-object v0, LX/0Uj;->a:[I

    invoke-virtual {p0}, LX/03R;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 10273
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized TriState value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10274
    :pswitch_0
    const/4 p1, 0x1

    .line 10275
    :goto_0
    :pswitch_1
    return p1

    .line 10276
    :pswitch_2
    const/4 p1, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final asBooleanObject()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 10266
    sget-object v0, LX/0Uj;->a:[I

    invoke-virtual {p0}, LX/03R;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 10267
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized TriState value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10268
    :pswitch_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 10269
    :goto_0
    return-object v0

    .line 10270
    :pswitch_1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 10271
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final getDbValue()I
    .locals 2

    .prologue
    .line 10262
    sget-object v0, LX/0Uj;->a:[I

    invoke-virtual {p0}, LX/03R;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 10263
    const/4 v0, 0x3

    :goto_0
    return v0

    .line 10264
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 10265
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final isSet()Z
    .locals 1

    .prologue
    .line 10261
    sget-object v0, LX/03R;->UNSET:LX/03R;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
