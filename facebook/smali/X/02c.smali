.class public abstract LX/02c;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final expectedFiles:[Ljava/lang/String;

.field public final flags:I


# direct methods
.method public constructor <init>(I[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 9016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9017
    iput p1, p0, LX/02c;->flags:I

    .line 9018
    iput-object p2, p0, LX/02c;->expectedFiles:[Ljava/lang/String;

    .line 9019
    return-void
.end method


# virtual methods
.method public abstract configureClassLoader(Ljava/io/File;LX/02f;)V
.end method

.method public abstract getSchemeName()Ljava/lang/String;
.end method

.method public abstract makeCompiler(LX/02U;I)LX/084;
.end method

.method public needOptimization(J)Z
    .locals 1

    .prologue
    .line 9015
    const/4 v0, 0x0

    return v0
.end method

.method public optimize(LX/02U;LX/08Z;LX/0FQ;)V
    .locals 0
    .param p3    # LX/0FQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 9014
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9013
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
