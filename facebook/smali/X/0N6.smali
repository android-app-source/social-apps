.class public final LX/0N6;
.super LX/0N4;
.source ""


# static fields
.field public static final b:[B


# instance fields
.field private final c:LX/0Oi;

.field public final d:LX/0Oj;

.field public final e:LX/0LS;

.field public f:I

.field public g:I

.field public h:I

.field public i:Z

.field private j:Z

.field private k:J

.field public l:I

.field public m:J

.field public n:LX/0LS;

.field public o:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49030
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/0N6;->b:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x49t
        0x44t
        0x33t
    .end array-data
.end method

.method public constructor <init>(LX/0LS;LX/0LS;)V
    .locals 3

    .prologue
    .line 49037
    invoke-direct {p0, p1}, LX/0N4;-><init>(LX/0LS;)V

    .line 49038
    iput-object p2, p0, LX/0N6;->e:LX/0LS;

    .line 49039
    invoke-static {}, LX/0L4;->a()LX/0L4;

    move-result-object v0

    invoke-interface {p2, v0}, LX/0LS;->a(LX/0L4;)V

    .line 49040
    new-instance v0, LX/0Oi;

    const/4 v1, 0x7

    new-array v1, v1, [B

    invoke-direct {v0, v1}, LX/0Oi;-><init>([B)V

    iput-object v0, p0, LX/0N6;->c:LX/0Oi;

    .line 49041
    new-instance v0, LX/0Oj;

    sget-object v1, LX/0N6;->b:[B

    const/16 v2, 0xa

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    invoke-direct {v0, v1}, LX/0Oj;-><init>([B)V

    iput-object v0, p0, LX/0N6;->d:LX/0Oj;

    .line 49042
    invoke-static {p0}, LX/0N6;->c(LX/0N6;)V

    .line 49043
    return-void
.end method

.method public static a(LX/0N6;LX/0LS;JII)V
    .locals 2

    .prologue
    .line 49031
    const/4 v0, 0x3

    iput v0, p0, LX/0N6;->f:I

    .line 49032
    iput p4, p0, LX/0N6;->g:I

    .line 49033
    iput-object p1, p0, LX/0N6;->n:LX/0LS;

    .line 49034
    iput-wide p2, p0, LX/0N6;->o:J

    .line 49035
    iput p5, p0, LX/0N6;->l:I

    .line 49036
    return-void
.end method

.method private a(LX/0Oj;[BI)Z
    .locals 2

    .prologue
    .line 49022
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    iget v1, p0, LX/0N6;->g:I

    sub-int v1, p3, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 49023
    iget v1, p0, LX/0N6;->g:I

    invoke-virtual {p1, p2, v1, v0}, LX/0Oj;->a([BII)V

    .line 49024
    iget v1, p0, LX/0N6;->g:I

    add-int/2addr v0, v1

    iput v0, p0, LX/0N6;->g:I

    .line 49025
    iget v0, p0, LX/0N6;->g:I

    if-ne v0, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/0N6;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49026
    iput v0, p0, LX/0N6;->f:I

    .line 49027
    iput v0, p0, LX/0N6;->g:I

    .line 49028
    const/16 v0, 0x100

    iput v0, p0, LX/0N6;->h:I

    .line 49029
    return-void
.end method

.method private g()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v0, 0x2

    const/4 v11, 0x0

    const/4 v2, -0x1

    const/4 v10, 0x1

    .line 48998
    iget-object v1, p0, LX/0N6;->c:LX/0Oi;

    invoke-virtual {v1, v11}, LX/0Oi;->a(I)V

    .line 48999
    iget-boolean v1, p0, LX/0N6;->j:Z

    if-nez v1, :cond_1

    .line 49000
    iget-object v1, p0, LX/0N6;->c:LX/0Oi;

    invoke-virtual {v1, v0}, LX/0Oi;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 49001
    if-ne v1, v10, :cond_2

    .line 49002
    const-string v1, "AdtsReader"

    const-string v3, "Detected AAC Main audio, but assuming AAC LC."

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 49003
    :goto_0
    iget-object v1, p0, LX/0N6;->c:LX/0Oi;

    invoke-virtual {v1, v12}, LX/0Oi;->c(I)I

    move-result v1

    .line 49004
    iget-object v3, p0, LX/0N6;->c:LX/0Oi;

    invoke-virtual {v3, v10}, LX/0Oi;->b(I)V

    .line 49005
    iget-object v3, p0, LX/0N6;->c:LX/0Oi;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, LX/0Oi;->c(I)I

    move-result v3

    .line 49006
    const/4 v4, 0x2

    new-array v4, v4, [B

    .line 49007
    const/4 v5, 0x0

    shl-int/lit8 v6, v0, 0x3

    and-int/lit16 v6, v6, 0xf8

    shr-int/lit8 v7, v1, 0x1

    and-int/lit8 v7, v7, 0x7

    or-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 49008
    const/4 v5, 0x1

    shl-int/lit8 v6, v1, 0x7

    and-int/lit16 v6, v6, 0x80

    shl-int/lit8 v7, v3, 0x3

    and-int/lit8 v7, v7, 0x78

    or-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 49009
    move-object v8, v4

    .line 49010
    invoke-static {v8}, LX/0OY;->a([B)Landroid/util/Pair;

    move-result-object v7

    .line 49011
    const/4 v0, 0x0

    const-string v1, "audio/mp4a-latm"

    const-wide/16 v4, -0x1

    iget-object v3, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v3, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    move v3, v2

    invoke-static/range {v0 .. v9}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v0

    .line 49012
    const-wide/32 v2, 0x3d090000

    iget v1, v0, LX/0L4;->o:I

    int-to-long v4, v1

    div-long/2addr v2, v4

    iput-wide v2, p0, LX/0N6;->k:J

    .line 49013
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    invoke-interface {v1, v0}, LX/0LS;->a(LX/0L4;)V

    .line 49014
    iput-boolean v10, p0, LX/0N6;->j:Z

    .line 49015
    :goto_1
    iget-object v0, p0, LX/0N6;->c:LX/0Oi;

    invoke-virtual {v0, v12}, LX/0Oi;->b(I)V

    .line 49016
    iget-object v0, p0, LX/0N6;->c:LX/0Oi;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, LX/0Oi;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    add-int/lit8 v5, v0, -0x5

    .line 49017
    iget-boolean v0, p0, LX/0N6;->i:Z

    if-eqz v0, :cond_0

    .line 49018
    add-int/lit8 v5, v5, -0x2

    .line 49019
    :cond_0
    iget-object v1, p0, LX/0N4;->a:LX/0LS;

    iget-wide v2, p0, LX/0N6;->k:J

    move-object v0, p0

    move v4, v11

    invoke-static/range {v0 .. v5}, LX/0N6;->a(LX/0N6;LX/0LS;JII)V

    .line 49020
    return-void

    .line 49021
    :cond_1
    iget-object v0, p0, LX/0N6;->c:LX/0Oi;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, LX/0Oi;->b(I)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 48996
    invoke-static {p0}, LX/0N6;->c(LX/0N6;)V

    .line 48997
    return-void
.end method

.method public final a(JZ)V
    .locals 1

    .prologue
    .line 48994
    iput-wide p1, p0, LX/0N6;->m:J

    .line 48995
    return-void
.end method

.method public final a(LX/0Oj;)V
    .locals 10

    .prologue
    .line 48942
    :cond_0
    :goto_0
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v0

    if-lez v0, :cond_3

    .line 48943
    iget v0, p0, LX/0N6;->f:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 48944
    :pswitch_0
    const/16 v6, 0x200

    const/16 v5, 0x100

    .line 48945
    iget-object v2, p1, LX/0Oj;->a:[B

    .line 48946
    iget v0, p1, LX/0Oj;->b:I

    move v1, v0

    .line 48947
    iget v0, p1, LX/0Oj;->c:I

    move v3, v0

    .line 48948
    move v0, v1

    .line 48949
    :goto_1
    if-ge v0, v3, :cond_6

    .line 48950
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, v2, v0

    and-int/lit16 v0, v0, 0xff

    .line 48951
    iget v4, p0, LX/0N6;->h:I

    if-ne v4, v6, :cond_5

    const/16 v4, 0xf0

    if-lt v0, v4, :cond_5

    const/16 v4, 0xff

    if-eq v0, v4, :cond_5

    .line 48952
    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, LX/0N6;->i:Z

    .line 48953
    const/4 v0, 0x2

    iput v0, p0, LX/0N6;->f:I

    .line 48954
    const/4 v0, 0x0

    iput v0, p0, LX/0N6;->g:I

    .line 48955
    invoke-virtual {p1, v1}, LX/0Oj;->b(I)V

    .line 48956
    :goto_3
    goto :goto_0

    .line 48957
    :pswitch_1
    iget-object v0, p0, LX/0N6;->d:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    const/16 v1, 0xa

    invoke-direct {p0, p1, v0, v1}, LX/0N6;->a(LX/0Oj;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48958
    const/16 v6, 0xa

    .line 48959
    iget-object v2, p0, LX/0N6;->e:LX/0LS;

    iget-object v3, p0, LX/0N6;->d:LX/0Oj;

    invoke-interface {v2, v3, v6}, LX/0LS;->a(LX/0Oj;I)V

    .line 48960
    iget-object v2, p0, LX/0N6;->d:LX/0Oj;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, LX/0Oj;->b(I)V

    .line 48961
    iget-object v3, p0, LX/0N6;->e:LX/0LS;

    const-wide/16 v4, 0x0

    iget-object v2, p0, LX/0N6;->d:LX/0Oj;

    invoke-virtual {v2}, LX/0Oj;->r()I

    move-result v2

    add-int/lit8 v7, v2, 0xa

    move-object v2, p0

    invoke-static/range {v2 .. v7}, LX/0N6;->a(LX/0N6;LX/0LS;JII)V

    .line 48962
    goto :goto_0

    .line 48963
    :pswitch_2
    iget-boolean v0, p0, LX/0N6;->i:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    .line 48964
    :goto_4
    iget-object v1, p0, LX/0N6;->c:LX/0Oi;

    iget-object v1, v1, LX/0Oi;->a:[B

    invoke-direct {p0, p1, v1, v0}, LX/0N6;->a(LX/0Oj;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48965
    invoke-direct {p0}, LX/0N6;->g()V

    goto :goto_0

    .line 48966
    :cond_1
    const/4 v0, 0x5

    goto :goto_4

    .line 48967
    :pswitch_3
    invoke-virtual {p1}, LX/0Oj;->b()I

    move-result v2

    iget v3, p0, LX/0N6;->l:I

    iget v4, p0, LX/0N6;->g:I

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 48968
    iget-object v3, p0, LX/0N6;->n:LX/0LS;

    invoke-interface {v3, p1, v2}, LX/0LS;->a(LX/0Oj;I)V

    .line 48969
    iget v3, p0, LX/0N6;->g:I

    add-int/2addr v2, v3

    iput v2, p0, LX/0N6;->g:I

    .line 48970
    iget v2, p0, LX/0N6;->g:I

    iget v3, p0, LX/0N6;->l:I

    if-ne v2, v3, :cond_2

    .line 48971
    iget-object v3, p0, LX/0N6;->n:LX/0LS;

    iget-wide v4, p0, LX/0N6;->m:J

    const/4 v6, 0x1

    iget v7, p0, LX/0N6;->l:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-interface/range {v3 .. v9}, LX/0LS;->a(JIII[B)V

    .line 48972
    iget-wide v2, p0, LX/0N6;->m:J

    iget-wide v4, p0, LX/0N6;->o:J

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/0N6;->m:J

    .line 48973
    invoke-static {p0}, LX/0N6;->c(LX/0N6;)V

    .line 48974
    :cond_2
    goto/16 :goto_0

    .line 48975
    :cond_3
    return-void

    .line 48976
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 48977
    :cond_5
    iget v4, p0, LX/0N6;->h:I

    or-int/2addr v0, v4

    sparse-switch v0, :sswitch_data_0

    .line 48978
    iget v0, p0, LX/0N6;->h:I

    if-eq v0, v5, :cond_7

    .line 48979
    iput v5, p0, LX/0N6;->h:I

    .line 48980
    add-int/lit8 v0, v1, -0x1

    goto/16 :goto_1

    .line 48981
    :sswitch_0
    iput v6, p0, LX/0N6;->h:I

    move v0, v1

    .line 48982
    goto/16 :goto_1

    .line 48983
    :sswitch_1
    const/16 v0, 0x300

    iput v0, p0, LX/0N6;->h:I

    move v0, v1

    .line 48984
    goto/16 :goto_1

    .line 48985
    :sswitch_2
    const/16 v0, 0x400

    iput v0, p0, LX/0N6;->h:I

    move v0, v1

    .line 48986
    goto/16 :goto_1

    .line 48987
    :sswitch_3
    const/4 v2, 0x0

    .line 48988
    const/4 v0, 0x1

    iput v0, p0, LX/0N6;->f:I

    .line 48989
    sget-object v0, LX/0N6;->b:[B

    array-length v0, v0

    iput v0, p0, LX/0N6;->g:I

    .line 48990
    iput v2, p0, LX/0N6;->l:I

    .line 48991
    iget-object v0, p0, LX/0N6;->d:LX/0Oj;

    invoke-virtual {v0, v2}, LX/0Oj;->b(I)V

    .line 48992
    invoke-virtual {p1, v1}, LX/0Oj;->b(I)V

    goto/16 :goto_3

    .line 48993
    :cond_6
    invoke-virtual {p1, v0}, LX/0Oj;->b(I)V

    goto/16 :goto_3

    :cond_7
    move v0, v1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x149 -> :sswitch_1
        0x1ff -> :sswitch_0
        0x344 -> :sswitch_2
        0x433 -> :sswitch_3
    .end sparse-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 48941
    return-void
.end method
