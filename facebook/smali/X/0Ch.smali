.class public final LX/0Ch;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/BrowserLiteChrome;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteChrome;Z)V
    .locals 0

    .prologue
    .line 27460
    iput-object p1, p0, LX/0Ch;->a:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27461
    iput-boolean p2, p0, LX/0Ch;->b:Z

    .line 27462
    return-void
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 27435
    sparse-switch p0, :sswitch_data_0

    .line 27436
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 27437
    :sswitch_0
    const/16 v0, 0x43

    goto :goto_0

    .line 27438
    :sswitch_1
    const/16 v0, 0x4b

    goto :goto_0

    .line 27439
    :sswitch_2
    const/16 v0, 0x5a

    goto :goto_0

    .line 27440
    :sswitch_3
    const/16 v0, 0x64

    goto :goto_0

    .line 27441
    :sswitch_4
    const/16 v0, 0x6e

    goto :goto_0

    .line 27442
    :sswitch_5
    const/16 v0, 0x7d

    goto :goto_0

    .line 27443
    :sswitch_6
    const/16 v0, 0x96

    goto :goto_0

    .line 27444
    :sswitch_7
    const/16 v0, 0xaf

    goto :goto_0

    .line 27445
    :sswitch_8
    const/16 v0, 0xc8

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x32 -> :sswitch_0
        0x43 -> :sswitch_1
        0x4b -> :sswitch_2
        0x5a -> :sswitch_3
        0x64 -> :sswitch_4
        0x6e -> :sswitch_5
        0x7d -> :sswitch_6
        0x96 -> :sswitch_7
        0xaf -> :sswitch_8
    .end sparse-switch
.end method

.method private static c(I)I
    .locals 1

    .prologue
    .line 27449
    sparse-switch p0, :sswitch_data_0

    .line 27450
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 27451
    :sswitch_0
    const/16 v0, 0xaf

    goto :goto_0

    .line 27452
    :sswitch_1
    const/16 v0, 0x96

    goto :goto_0

    .line 27453
    :sswitch_2
    const/16 v0, 0x7d

    goto :goto_0

    .line 27454
    :sswitch_3
    const/16 v0, 0x6e

    goto :goto_0

    .line 27455
    :sswitch_4
    const/16 v0, 0x64

    goto :goto_0

    .line 27456
    :sswitch_5
    const/16 v0, 0x5a

    goto :goto_0

    .line 27457
    :sswitch_6
    const/16 v0, 0x4b

    goto :goto_0

    .line 27458
    :sswitch_7
    const/16 v0, 0x43

    goto :goto_0

    .line 27459
    :sswitch_8
    const/16 v0, 0x32

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x43 -> :sswitch_8
        0x4b -> :sswitch_7
        0x5a -> :sswitch_6
        0x64 -> :sswitch_5
        0x6e -> :sswitch_4
        0x7d -> :sswitch_3
        0x96 -> :sswitch_2
        0xaf -> :sswitch_1
        0xc8 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 27446
    invoke-static {p1}, LX/0Ch;->c(I)I

    move-result v1

    .line 27447
    if-eq v1, v0, :cond_0

    const/16 v2, 0x64

    if-ge v1, v2, :cond_0

    iget-boolean v2, p0, LX/0Ch;->b:Z

    if-nez v2, :cond_0

    .line 27448
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
