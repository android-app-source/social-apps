.class public LX/07b;
.super LX/07c;
.source ""


# direct methods
.method public constructor <init>(LX/07R;I)V
    .locals 0

    .prologue
    .line 20059
    invoke-direct {p0, p1, p2}, LX/07c;-><init>(LX/07R;I)V

    .line 20060
    return-void
.end method

.method public static d(LX/07b;Ljava/io/DataInputStream;)LX/07U;
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 20061
    invoke-virtual {p0, p1}, LX/07c;->a(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v0

    .line 20062
    const-string v1, "MQIsdp"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20063
    invoke-virtual {p1}, Ljava/io/DataInputStream;->close()V

    .line 20064
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid input - missing header"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20065
    :cond_0
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readByte()B

    move-result v1

    .line 20066
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v10

    .line 20067
    iget v0, p0, LX/07c;->b:I

    add-int/lit8 v0, v0, -0x2

    iput v0, p0, LX/07b;->b:I

    .line 20068
    invoke-virtual {p0, p1}, LX/07c;->b(Ljava/io/DataInputStream;)I

    move-result v8

    .line 20069
    new-instance v0, LX/07U;

    and-int/lit16 v2, v10, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1

    move v2, v7

    :goto_0
    and-int/lit8 v3, v10, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_2

    move v3, v7

    :goto_1
    and-int/lit8 v4, v10, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_3

    move v4, v7

    :goto_2
    and-int/lit8 v5, v10, 0x20

    const/16 v6, 0x20

    if-ne v5, v6, :cond_4

    move v5, v7

    :goto_3
    and-int/lit8 v6, v10, 0x18

    shr-int/lit8 v6, v6, 0x3

    and-int/lit8 v10, v10, 0x2

    const/4 v11, 0x2

    if-ne v10, v11, :cond_5

    :goto_4
    invoke-direct/range {v0 .. v8}, LX/07U;-><init>(IZZZZIZI)V

    return-object v0

    :cond_1
    move v2, v9

    goto :goto_0

    :cond_2
    move v3, v9

    goto :goto_1

    :cond_3
    move v4, v9

    goto :goto_2

    :cond_4
    move v5, v9

    goto :goto_3

    :cond_5
    move v7, v9

    goto :goto_4
.end method
