.class public LX/0Ci;
.super Landroid/os/AsyncTask;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 27974
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 27975
    iput-object p1, p0, LX/0Ci;->a:Landroid/widget/ImageView;

    .line 27976
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 27977
    if-eqz p1, :cond_0

    .line 27978
    iget-object v0, p0, LX/0Ci;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 27979
    iget-object v0, p0, LX/0Ci;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 27980
    :cond_0
    return-void
.end method

.method public final doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 27981
    check-cast p1, [Ljava/lang/String;

    .line 27982
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 27983
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 27984
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 27985
    const v1, 0x6c0dce65

    invoke-static {v0, v1}, LX/04e;->b(Ljava/net/URLConnection;I)Ljava/io/InputStream;

    move-result-object v0

    .line 27986
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 27987
    :goto_0
    return-object v0

    .line 27988
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27989
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, LX/0Ci;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
