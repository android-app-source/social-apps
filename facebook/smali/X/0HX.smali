.class public LX/0HX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37810
    return-void
.end method

.method public static a(Landroid/net/NetworkInfo;)LX/0HW;
    .locals 2

    .prologue
    .line 37811
    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    .line 37812
    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v1

    .line 37813
    packed-switch v0, :pswitch_data_0

    .line 37814
    sget-object v0, LX/0HW;->Other:LX/0HW;

    :goto_0
    return-object v0

    .line 37815
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 37816
    sget-object v0, LX/0HW;->MOBILE_OTHER:LX/0HW;

    goto :goto_0

    .line 37817
    :pswitch_1
    sget-object v0, LX/0HW;->MOBILE_2G:LX/0HW;

    goto :goto_0

    .line 37818
    :pswitch_2
    sget-object v0, LX/0HW;->MOBILE_3G:LX/0HW;

    goto :goto_0

    .line 37819
    :pswitch_3
    sget-object v0, LX/0HW;->MOBILE_4G:LX/0HW;

    goto :goto_0

    .line 37820
    :pswitch_4
    sget-object v0, LX/0HW;->MOBILE_OTHER:LX/0HW;

    goto :goto_0

    .line 37821
    :pswitch_5
    sget-object v0, LX/0HW;->WIFI:LX/0HW;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
