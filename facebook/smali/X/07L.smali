.class public LX/07L;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/07L;


# instance fields
.field private volatile b:J

.field public volatile c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19604
    new-instance v0, LX/07L;

    invoke-direct {v0}, LX/07L;-><init>()V

    sput-object v0, LX/07L;->a:LX/07L;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 19606
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/07L;->b:J

    .line 19607
    return-void
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 19608
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, LX/07L;->b:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x4268

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
