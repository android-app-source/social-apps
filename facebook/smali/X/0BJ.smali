.class public final enum LX/0BJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0BJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0BJ;

.field public static final enum DISCONNECT:LX/0BJ;

.field public static final enum NETWORK_THREAD_LOOP:LX/0BJ;

.field public static final enum PING:LX/0BJ;

.field public static final enum PINGRESP:LX/0BJ;

.field public static final enum PUBACK:LX/0BJ;

.field public static final enum PUBLISH:LX/0BJ;

.field public static final enum SUBSCRIBE:LX/0BJ;

.field public static final enum TIMEOUT:LX/0BJ;

.field public static final enum UNSUBSCRIBE:LX/0BJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26298
    new-instance v0, LX/0BJ;

    const-string v1, "DISCONNECT"

    invoke-direct {v0, v1, v3}, LX/0BJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BJ;->DISCONNECT:LX/0BJ;

    .line 26299
    new-instance v0, LX/0BJ;

    const-string v1, "NETWORK_THREAD_LOOP"

    invoke-direct {v0, v1, v4}, LX/0BJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BJ;->NETWORK_THREAD_LOOP:LX/0BJ;

    .line 26300
    new-instance v0, LX/0BJ;

    const-string v1, "PUBLISH"

    invoke-direct {v0, v1, v5}, LX/0BJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BJ;->PUBLISH:LX/0BJ;

    .line 26301
    new-instance v0, LX/0BJ;

    const-string v1, "PUBACK"

    invoke-direct {v0, v1, v6}, LX/0BJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BJ;->PUBACK:LX/0BJ;

    .line 26302
    new-instance v0, LX/0BJ;

    const-string v1, "PING"

    invoke-direct {v0, v1, v7}, LX/0BJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BJ;->PING:LX/0BJ;

    .line 26303
    new-instance v0, LX/0BJ;

    const-string v1, "PINGRESP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0BJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BJ;->PINGRESP:LX/0BJ;

    .line 26304
    new-instance v0, LX/0BJ;

    const-string v1, "SUBSCRIBE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0BJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BJ;->SUBSCRIBE:LX/0BJ;

    .line 26305
    new-instance v0, LX/0BJ;

    const-string v1, "UNSUBSCRIBE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/0BJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BJ;->UNSUBSCRIBE:LX/0BJ;

    .line 26306
    new-instance v0, LX/0BJ;

    const-string v1, "TIMEOUT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/0BJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0BJ;->TIMEOUT:LX/0BJ;

    .line 26307
    const/16 v0, 0x9

    new-array v0, v0, [LX/0BJ;

    sget-object v1, LX/0BJ;->DISCONNECT:LX/0BJ;

    aput-object v1, v0, v3

    sget-object v1, LX/0BJ;->NETWORK_THREAD_LOOP:LX/0BJ;

    aput-object v1, v0, v4

    sget-object v1, LX/0BJ;->PUBLISH:LX/0BJ;

    aput-object v1, v0, v5

    sget-object v1, LX/0BJ;->PUBACK:LX/0BJ;

    aput-object v1, v0, v6

    sget-object v1, LX/0BJ;->PING:LX/0BJ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0BJ;->PINGRESP:LX/0BJ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0BJ;->SUBSCRIBE:LX/0BJ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0BJ;->UNSUBSCRIBE:LX/0BJ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0BJ;->TIMEOUT:LX/0BJ;

    aput-object v2, v0, v1

    sput-object v0, LX/0BJ;->$VALUES:[LX/0BJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26308
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0BJ;
    .locals 1

    .prologue
    .line 26297
    const-class v0, LX/0BJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0BJ;

    return-object v0
.end method

.method public static values()[LX/0BJ;
    .locals 1

    .prologue
    .line 26296
    sget-object v0, LX/0BJ;->$VALUES:[LX/0BJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0BJ;

    return-object v0
.end method
