.class public final LX/009;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation build Lcom/facebook/annotations/DoNotOptimize;
.end annotation


# static fields
.field public static final ACRA_DIRNAME:Ljava/lang/String; = "acra-reports"

.field public static final CACHED_REPORTFILE_EXTENSION:Ljava/lang/String; = ".cachedreport"

.field public static final CPUSPIN_DIR:Ljava/lang/String; = "traces_cpuspin"

.field public static final CPUSPIN_MAX_REPORT_SIZE:J = 0x80000L

.field public static final CRASH_ATTACHMENT_DUMMY_STACKTRACE:Ljava/lang/String; = "crash attachment"

.field public static final DEFAULT_MAX_REPORT_SIZE:J = 0x100000L

.field public static final DUMPFILE_EXTENSION:Ljava/lang/String; = ".dmp"

.field public static final DUMP_DIR:Ljava/lang/String; = "minidumps"

.field private static final EMPTY_LAST_URL:Ljava/lang/String; = "EMPTY_URL"

.field private static final FILE_IAB_OPEN_TIMES:Ljava/lang/String; = "iab_open_times"

.field private static final FILE_LAST_ACTIVITY:Ljava/lang/String; = "last_activity_opened"

.field private static final FILE_LAST_URL:Ljava/lang/String; = "last_url_opened"

.field private static final HANDLE_EXCEPTION_NEVER_SEND_IMMEDIATELY:I = 0x4

.field private static final HANDLE_EXCEPTION_SEND_IMMEDIATELY:I = 0x1

.field private static final HANDLE_EXCEPTION_SEND_SYNCHRONOUSLY:I = 0x2

.field public static final MAX_REPORT_AGE:J = 0x240c8400L

.field public static final MAX_SEND_REPORTS:I = 0x5

.field private static final MAX_TRACE_COUNT_LIMIT:I = 0x14

.field private static final MAX_TRANSLATION_HOOK_RUNS:I = 0x4

.field public static final NATIVE_MAX_REPORT_SIZE:J = 0x800000L

.field private static final NO_FILE:Ljava/lang/String; = "NO_FILE"

.field public static final PREALLOCATED_FILESIZE:J = 0x100000L

.field public static final PREALLOCATED_REPORTFILE:Ljava/lang/String; = "reportfile.prealloc"

.field public static final REPORTFILE_EXTENSION:Ljava/lang/String; = ".stacktrace"

.field private static final REPORTS_TO_CHECK_ON_STARTUP:[LX/01V;

.field public static final SIGQUIT_DIR:Ljava/lang/String; = "traces"

.field public static final SIGQUIT_MAX_REPORT_SIZE:J = 0x80000L

.field public static final TAG:Ljava/lang/String; = "ErrorReporter"

.field private static final UNCAUGHT_EXCEPTION_LOCK:Ljava/lang/Object;

.field private static final mInternalException:Ljava/lang/String; = "ACRA_INTERNAL=java.lang.Exception: An exception occurred while trying to collect data about an ACRA internal error\n\tat com.facebook.acra.ErrorReporter.handleException(ErrorReporter.java:810)\n\tat com.facebook.acra.ErrorReporter.handleException(ErrorReporter.java:866)\n\tat com.facebook.acra.ErrorReporter.uncaughtException(ErrorReporter.java:666)\n\tat java.lang.ThreadGroup.uncaughtException(ThreadGroup.java:693)\n\tat java.lang.ThreadGroup.uncaughtException(ThreadGroup.java:690)\n"

.field private static sVersionCodeRegex:Ljava/util/regex/Pattern;


# instance fields
.field private mANRDataProvider:LX/09r;

.field private final mActivityLogger:LX/01a;

.field private final mAppStartDate:Landroid/text/format/Time;

.field private mAppVersionCode:Ljava/lang/String;

.field private mAppVersionName:Ljava/lang/String;

.field private volatile mChainedHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private mConfig:LX/00K;

.field private mConstantFields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mContext:Landroid/content/Context;

.field private final mCrashReportedObserver:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/facebook/acra/ErrorReporter$CrashReportedObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final mExceptionTranslationHook:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/00B;",
            ">;"
        }
    .end annotation
.end field

.field private mInitializationComplete:Z

.field private mInstallTime:J

.field private final mInstanceCustomParameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mInstanceLazyCustomParameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/00T;",
            ">;"
        }
    .end annotation
.end field

.field private mLogBridge:LX/03d;

.field private volatile mMaxReportSize:J

.field private mOomReservation:[B

.field private mPreallocFileName:Ljava/io/File;

.field private final mReportSenders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/acra/sender/ReportSender;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mUserId:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1444
    const/4 v0, 0x0

    sput-object v0, LX/009;->sVersionCodeRegex:Ljava/util/regex/Pattern;

    .line 1445
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/009;->UNCAUGHT_EXCEPTION_LOCK:Ljava/lang/Object;

    .line 1446
    const/4 v0, 0x3

    new-array v0, v0, [LX/01V;

    const/4 v1, 0x0

    sget-object v2, LX/01V;->ACRA_CRASH_REPORT:LX/01V;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/01V;->NATIVE_CRASH_REPORT:LX/01V;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/01V;->CPUSPIN_REPORT:LX/01V;

    aput-object v2, v0, v1

    sput-object v0, LX/009;->REPORTS_TO_CHECK_ON_STARTUP:[LX/01V;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1433
    const-wide/32 v0, 0x100000

    iput-wide v0, p0, LX/009;->mMaxReportSize:J

    .line 1434
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/009;->mReportSenders:Ljava/util/ArrayList;

    .line 1435
    iput-object v2, p0, LX/009;->mOomReservation:[B

    .line 1436
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LX/009;->mInstanceCustomParameters:Ljava/util/Map;

    .line 1437
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/009;->mInstanceLazyCustomParameters:Ljava/util/Map;

    .line 1438
    iput-object v2, p0, LX/009;->mPreallocFileName:Ljava/io/File;

    .line 1439
    new-instance v0, LX/01a;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, LX/01a;-><init>(I)V

    iput-object v0, p0, LX/009;->mActivityLogger:LX/01a;

    .line 1440
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, LX/009;->mAppStartDate:Landroid/text/format/Time;

    .line 1441
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LX/009;->mExceptionTranslationHook:Ljava/util/concurrent/atomic/AtomicReference;

    .line 1442
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LX/009;->mCrashReportedObserver:Ljava/util/concurrent/atomic/AtomicReference;

    .line 1443
    return-void
.end method

.method private attachIabInfo(LX/01l;)V
    .locals 3

    .prologue
    .line 1423
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "iab_open_times"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1424
    invoke-static {v0}, LX/009;->readFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 1425
    const-string v2, "NO_FILE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1426
    const-string v1, "IAB_OPEN_TIMES"

    const-string v2, "0"

    invoke-virtual {p1, v1, v2}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1427
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1428
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1429
    :cond_1
    return-void

    .line 1430
    :cond_2
    if-eqz v1, :cond_0

    .line 1431
    const-string v2, "IAB_OPEN_TIMES"

    invoke-virtual {p1, v2, v1}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private attachLastActivityInfo(LX/01l;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1401
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v3, "last_activity_opened"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1402
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1403
    const-string v0, "LAST_ACTIVITY_LOGGED"

    const-string v1, "NO_FILE"

    invoke-virtual {p1, v0, v1}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1404
    :goto_0
    return-void

    .line 1405
    :cond_0
    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    .line 1406
    :try_start_0
    new-instance v4, Ljava/io/BufferedReader;

    const/16 v1, 0x400

    invoke-direct {v4, v3, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1407
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 1408
    if-eqz v1, :cond_1

    .line 1409
    const-string v5, "LAST_ACTIVITY_LOGGED"

    invoke-virtual {p1, v5, v1}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1410
    const-string v1, "LAST_ACTIVITY_LOGGED_TIME"

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v1, v5}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1411
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 1412
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1413
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    goto :goto_0

    .line 1414
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1415
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_1
    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :goto_2
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1416
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1417
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v2, :cond_3

    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :goto_4
    throw v0

    .line 1418
    :catch_2
    move-exception v4

    :try_start_8
    invoke-static {v1, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1419
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 1420
    :cond_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 1421
    :catch_3
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_3
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    goto :goto_4

    .line 1422
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method private attachLastUrlInfo(LX/01l;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1377
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v3, "last_url_opened"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1378
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1379
    const-string v0, "LAST_URL_VISITED"

    const-string v1, "NO_FILE"

    invoke-virtual {p1, v0, v1}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1380
    :goto_0
    return-void

    .line 1381
    :cond_0
    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    .line 1382
    :try_start_0
    new-instance v4, Ljava/io/BufferedReader;

    const/16 v1, 0x400

    invoke-direct {v4, v3, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1383
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 1384
    if-nez v1, :cond_1

    .line 1385
    const-string v1, "LAST_URL_VISITED"

    const-string v5, "EMPTY_URL"

    invoke-virtual {p1, v1, v5}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1386
    :goto_1
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 1387
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1388
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    goto :goto_0

    .line 1389
    :cond_1
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1390
    const-string v5, "LAST_URL_VISITED"

    invoke-virtual {p1, v5, v1}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1391
    const-string v1, "LAST_URL_VISITED_TIME"

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v1, v5}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_1

    .line 1392
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1393
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_2
    if-eqz v1, :cond_2

    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :goto_3
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1394
    :catch_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1395
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_4
    if-eqz v2, :cond_3

    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    :goto_5
    throw v0

    .line 1396
    :catch_2
    move-exception v4

    :try_start_9
    invoke-static {v1, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1397
    :catchall_2
    move-exception v0

    goto :goto_4

    .line 1398
    :cond_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_3

    .line 1399
    :catch_3
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_3
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    goto :goto_5

    .line 1400
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method private checkAndHandleReportsLocked(ILX/01V;)I
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 1353
    invoke-virtual {p2}, LX/01V;->getHandler()LX/01X;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1354
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "ErrorReporter::checkAndHandleReportsLocked report type requires a handler"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1355
    :cond_0
    iget-object v1, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-static {v1}, LX/04R;->getProcessNameFromAms(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 1356
    iget-object v1, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-static {p2, v1}, LX/01V;->getCrashReports(LX/01V;Landroid/content/Context;)LX/01h;

    move-result-object v5

    move v3, v0

    .line 1357
    :goto_0
    :try_start_0
    invoke-virtual {v5}, LX/01h;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    if-ge v3, p1, :cond_2

    .line 1358
    invoke-virtual {v5}, LX/01h;->next()LX/04Z;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v6

    .line 1359
    add-int/lit8 v1, v0, 0x1

    const/4 v7, 0x5

    if-lt v0, v7, :cond_1

    .line 1360
    :try_start_1
    iget-object v0, v6, LX/04Z;->fileName:Ljava/io/File;

    invoke-static {v0}, LX/009;->deleteFile(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 1361
    if-eqz v6, :cond_a

    :try_start_2
    invoke-virtual {v6}, LX/04Z;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move v0, v1

    goto :goto_0

    .line 1362
    :cond_1
    :try_start_3
    invoke-virtual {p2}, LX/01V;->getHandler()LX/01X;

    move-result-object v0

    invoke-interface {v0, p0, v6, v4}, LX/01X;->handleReport(LX/009;LX/04Z;Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result v0

    if-nez v0, :cond_4

    .line 1363
    if-eqz v6, :cond_2

    :try_start_4
    invoke-virtual {v6}, LX/04Z;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1364
    :cond_2
    if-eqz v5, :cond_3

    invoke-virtual {v5}, LX/01h;->close()V

    .line 1365
    :cond_3
    return v3

    .line 1366
    :cond_4
    add-int/lit8 v0, v3, 0x1

    .line 1367
    if-eqz v6, :cond_8

    :try_start_5
    invoke-virtual {v6}, LX/04Z;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move v3, v0

    move v0, v1

    goto :goto_0

    .line 1368
    :catch_0
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1369
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_1
    if-eqz v6, :cond_5

    if-eqz v1, :cond_7

    :try_start_7
    invoke-virtual {v6}, LX/04Z;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :cond_5
    :goto_2
    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 1370
    :catch_1
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1371
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v5, :cond_6

    if-eqz v2, :cond_9

    :try_start_a
    invoke-virtual {v5}, LX/01h;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_3

    :cond_6
    :goto_4
    throw v0

    .line 1372
    :catch_2
    move-exception v3

    :try_start_b
    invoke-static {v1, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1373
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 1374
    :cond_7
    invoke-virtual {v6}, LX/04Z;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto :goto_2

    :cond_8
    move v3, v0

    move v0, v1

    goto :goto_0

    .line 1375
    :catch_3
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_9
    invoke-virtual {v5}, LX/01h;->close()V

    goto :goto_4

    .line 1376
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :cond_a
    move v0, v1

    goto :goto_0
.end method

.method public static deleteFile(Ljava/io/File;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1347
    if-nez p0, :cond_1

    .line 1348
    :cond_0
    :goto_0
    return v0

    .line 1349
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v1

    .line 1350
    if-nez v1, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1351
    :goto_1
    if-nez v0, :cond_0

    .line 1352
    sget-object v1, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not delete error report: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private varargs discardOverlappingReports([LX/01V;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1340
    array-length v2, p1

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 1341
    sget-object v4, LX/01V;->NATIVE_CRASH_REPORT:LX/01V;

    if-eq v3, v4, :cond_0

    sget-object v4, LX/01V;->ACRA_CRASH_REPORT:LX/01V;

    if-ne v3, v4, :cond_2

    .line 1342
    :cond_0
    const/4 v4, 0x1

    new-array v4, v4, [LX/01V;

    aput-object v3, v4, v1

    invoke-virtual {p0, v4}, LX/009;->roughlyCountReportsOfType([LX/01V;)I

    move-result v3

    .line 1343
    if-lez v3, :cond_2

    .line 1344
    iget-object v0, p0, LX/009;->mContext:Landroid/content/Context;

    const-string v2, "traces"

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/009;->purgeDirectory(Ljava/io/File;)V

    .line 1345
    :cond_1
    return-void

    .line 1346
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private dumpCustomDataEntry(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1199
    if-eqz p2, :cond_1

    const-string v1, "\n"

    const-string v2, "\\n"

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 1200
    :goto_0
    if-eqz p3, :cond_0

    const-string v0, "\n"

    const-string v2, "\\n"

    invoke-virtual {p3, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1201
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1202
    return-void

    :cond_1
    move-object v1, v0

    .line 1203
    goto :goto_0
.end method

.method private dumpCustomDataMap(Ljava/lang/StringBuilder;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1333
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1334
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1335
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1336
    invoke-direct {p0, p1, v1, v0}, LX/009;->dumpCustomDataEntry(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1337
    :cond_0
    return-void
.end method

.method private dumpLazyCustomDataMap(Ljava/lang/StringBuilder;Ljava/util/Map;Ljava/lang/Throwable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/00T;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1325
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1326
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1327
    :try_start_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00T;

    invoke-interface {v0, p3}, LX/00T;->getCustomData(Ljava/lang/Throwable;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1328
    if-eqz v0, :cond_0

    .line 1329
    invoke-direct {p0, p1, v1, v0}, LX/009;->dumpCustomDataEntry(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1330
    :catch_0
    move-exception v0

    .line 1331
    sget-object v1, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Caught throwable while getting custom report data"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1332
    :cond_1
    return-void
.end method

.method private genCrashReportFileName(Ljava/lang/Class;Ljava/util/UUID;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/009;->mAppVersionCode:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/009;->mAppVersionCode:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static getInstance()LX/009;
    .locals 1

    .prologue
    .line 1323
    sget-object v0, LX/01Z;->ERROR_REPORTER:LX/009;

    return-object v0
.end method

.method public static getMostSignificantCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 1319
    instance-of v0, p0, LX/03Y;

    if-eqz v0, :cond_1

    .line 1320
    :cond_0
    return-object p0

    .line 1321
    :cond_1
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1322
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0
.end method

.method private static getVersionCodeRegex()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 1316
    sget-object v0, LX/009;->sVersionCodeRegex:Ljava/util/regex/Pattern;

    if-nez v0, :cond_0

    .line 1317
    const-string v0, "^\\d+-[a-zA-Z0-9_\\-]+-(\\d+)\\.(temp_stacktrace|stacktrace)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/009;->sVersionCodeRegex:Ljava/util/regex/Pattern;

    .line 1318
    :cond_0
    sget-object v0, LX/009;->sVersionCodeRegex:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method private handleExceptionInternal(Ljava/lang/Throwable;Ljava/util/Map;Ljava/lang/String;I)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;
    .locals 11
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I)",
            "Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1223
    iget-boolean v0, p0, LX/009;->mInitializationComplete:Z

    if-nez v0, :cond_0

    .line 1224
    :goto_0
    return-object v7

    .line 1225
    :cond_0
    sget-object v1, LX/009;->UNCAUGHT_EXCEPTION_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1226
    :try_start_0
    sget-object v0, LX/009;->UNCAUGHT_EXCEPTION_LOCK:Ljava/lang/Object;

    const v2, -0x7a5bc18a

    invoke-static {v0, v2}, LX/02L;->b(Ljava/lang/Object;I)V

    .line 1227
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1228
    invoke-static {p1}, LX/009;->getMostSignificantCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    .line 1229
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 1230
    new-instance v4, LX/01l;

    invoke-direct {v4}, LX/01l;-><init>()V

    .line 1231
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_1

    .line 1232
    and-int/lit8 p4, p4, -0x4

    .line 1233
    const/4 v0, 0x1

    iput-boolean v0, v4, LX/01l;->throwAwayWrites:Z

    .line 1234
    :cond_1
    instance-of v0, p1, LX/03Y;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, LX/03Y;

    invoke-interface {v0}, LX/03Y;->getExceptionFriendlyName()Ljava/lang/String;

    move-result-object v0

    .line 1235
    :goto_1
    sget-object v1, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Handling exception for "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, p1, p3}, LX/009;->writeToLogBridge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1236
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Generating report file for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1237
    :try_start_1
    sget-object v0, LX/01V;->ACRA_CRASH_REPORT:LX/01V;

    iget-object v1, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/01V;->getSpool(Landroid/content/Context;)LX/01e;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v0

    move-object v2, v0

    .line 1238
    :goto_2
    if-eqz v2, :cond_11

    move-object v8, v7

    move-object v0, v7

    move-object v1, v7

    .line 1239
    :goto_3
    if-nez v8, :cond_3

    .line 1240
    :try_start_2
    invoke-static {}, LX/01c;->generateReportUuid()Ljava/util/UUID;

    move-result-object v1

    .line 1241
    const-string v0, ".stacktrace"

    invoke-direct {p0, v3, v1, v0}, LX/009;->genCrashReportFileName(Ljava/lang/Class;Ljava/util/UUID;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1242
    iget-object v5, p0, LX/009;->mPreallocFileName:Ljava/io/File;

    invoke-virtual {v2, v0, v5}, LX/01e;->produceWithDonorFile(Ljava/lang/String;Ljava/io/File;)LX/04Q;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result-object v8

    goto :goto_3

    .line 1243
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1244
    :cond_2
    const-string v0, "crash"

    goto :goto_1

    .line 1245
    :catch_0
    move-exception v0

    .line 1246
    :try_start_4
    iput-object v0, v4, LX/01l;->generatingIoError:Ljava/lang/Throwable;

    .line 1247
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object v2, v7

    goto :goto_2

    :cond_3
    move-object v2, v0

    .line 1248
    :goto_4
    if-eqz v8, :cond_b

    .line 1249
    :try_start_5
    new-instance v9, Ljava/io/FileOutputStream;

    iget-object v0, v8, LX/04Q;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 1250
    :goto_5
    if-eqz v9, :cond_c

    .line 1251
    :try_start_6
    invoke-static {v9}, LX/01l;->getWriter(Ljava/io/OutputStream;)Ljava/io/Writer;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    move-result-object v5

    .line 1252
    :goto_6
    if-nez v1, :cond_10

    .line 1253
    :try_start_7
    invoke-static {}, LX/01c;->generateReportUuid()Ljava/util/UUID;

    move-result-object v0

    .line 1254
    :goto_7
    const/4 v10, 0x1

    move v1, v10

    .line 1255
    if-eqz v1, :cond_4

    if-eqz v2, :cond_4

    .line 1256
    const-string v1, "ACRA_REPORT_FILENAME"

    invoke-static {v1, v2, v4, v5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V

    .line 1257
    :cond_4
    const/4 v6, 0x1

    move v1, v6

    .line 1258
    if-eqz v1, :cond_5

    .line 1259
    const-string v1, "REPORT_ID"

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v4, v5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V

    .line 1260
    :cond_5
    const/4 v2, 0x1

    move v0, v2

    .line 1261
    if-eqz v0, :cond_6

    .line 1262
    const-string v0, "EXCEPTION_CAUSE"

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4, v5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V

    .line 1263
    :cond_6
    if-nez p3, :cond_f

    .line 1264
    invoke-static {p1}, LX/009;->throwableToString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    .line 1265
    :goto_8
    iget-object v1, p0, LX/009;->mConfig:LX/00K;

    move-object v0, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, LX/04R;->gatherCrashData(LX/009;LX/00K;Ljava/lang/String;Ljava/lang/Throwable;LX/01l;Ljava/io/Writer;Ljava/util/Map;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1266
    :goto_9
    if-eqz v5, :cond_7

    .line 1267
    :try_start_8
    invoke-virtual {v5}, Ljava/io/Writer;->flush()V

    .line 1268
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->flush()V

    .line 1269
    iget-object v0, v8, LX/04Q;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 1270
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/nio/channels/FileChannel;->truncate(J)Ljava/nio/channels/FileChannel;
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1271
    :cond_7
    :goto_a
    :try_start_9
    iget-object v0, v4, LX/01l;->generatingIoError:Ljava/lang/Throwable;

    if-eqz v0, :cond_8

    .line 1272
    const-string v0, "GENERATING_IO_ERROR"

    iget-object v1, v4, LX/01l;->generatingIoError:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4, v5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V

    .line 1273
    :cond_8
    iget-object v0, v4, LX/01l;->generatingIoError:Ljava/lang/Throwable;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v0, :cond_9

    and-int/lit8 v0, p4, 0x4

    if-nez v0, :cond_9

    .line 1274
    or-int/lit8 p4, p4, 0x1

    .line 1275
    :cond_9
    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_e

    .line 1276
    :try_start_a
    new-instance v1, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;

    invoke-direct {v1, p0, v4, v8}, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;-><init>(LX/009;LX/01l;LX/04Q;)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_9
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 1277
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_d

    .line 1278
    :try_start_b
    invoke-virtual {v1}, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->doSend()V

    .line 1279
    iget-object v0, p0, LX/009;->mCrashReportedObserver:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00P;

    .line 1280
    if-eqz v0, :cond_a

    iget-object v2, v1, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mInMemoryReportToSend:LX/01l;

    if-eqz v2, :cond_a

    .line 1281
    iget-object v2, v1, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mInMemoryReportToSend:LX/01l;

    invoke-virtual {v0, v2}, LX/00P;->onCrashReported(LX/01l;)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_8
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :cond_a
    move-object v0, v1

    move-object v7, v8

    .line 1282
    :goto_b
    invoke-static {p0, v5}, LX/009;->safeClose(LX/009;Ljava/io/Closeable;)V

    .line 1283
    invoke-static {p0, v9}, LX/009;->safeClose(LX/009;Ljava/io/Closeable;)V

    .line 1284
    invoke-static {p0, v7}, LX/009;->safeClose(LX/009;Ljava/io/Closeable;)V

    move-object v7, v0

    goto/16 :goto_0

    .line 1285
    :catch_1
    move-exception v0

    .line 1286
    :try_start_c
    iput-object v0, v4, LX/01l;->generatingIoError:Ljava/lang/Throwable;

    .line 1287
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V

    move-object v2, v7

    .line 1288
    goto/16 :goto_4

    .line 1289
    :catch_2
    move-exception v0

    .line 1290
    iput-object v0, v4, LX/01l;->generatingIoError:Ljava/lang/Throwable;

    .line 1291
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :cond_b
    move-object v9, v7

    goto/16 :goto_5

    .line 1292
    :catch_3
    move-exception v0

    .line 1293
    :try_start_d
    iput-object v0, v4, LX/01l;->generatingIoError:Ljava/lang/Throwable;

    .line 1294
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    :cond_c
    move-object v5, v7

    goto/16 :goto_6

    .line 1295
    :catch_4
    move-exception v0

    .line 1296
    :try_start_e
    const-string v1, "gathering crash data"

    invoke-direct {p0, v1, v0}, LX/009;->tryLogInternalError(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 1297
    :try_start_f
    const-string v1, "ACRA_INTERNAL"

    invoke-static {v0}, LX/009;->throwableToString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v4, v5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_5
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_9

    .line 1298
    :catch_5
    move-exception v0

    .line 1299
    :try_start_10
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V

    .line 1300
    const-string v0, "ACRA_INTERNAL"

    const-string v1, "ACRA_INTERNAL=java.lang.Exception: An exception occurred while trying to collect data about an ACRA internal error\n\tat com.facebook.acra.ErrorReporter.handleException(ErrorReporter.java:810)\n\tat com.facebook.acra.ErrorReporter.handleException(ErrorReporter.java:866)\n\tat com.facebook.acra.ErrorReporter.uncaughtException(ErrorReporter.java:666)\n\tat java.lang.ThreadGroup.uncaughtException(ThreadGroup.java:693)\n\tat java.lang.ThreadGroup.uncaughtException(ThreadGroup.java:690)\n"

    invoke-static {v0, v1, v4, v5}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto/16 :goto_9

    .line 1301
    :catchall_1
    move-exception v0

    move-object v7, v9

    :goto_c
    invoke-static {p0, v5}, LX/009;->safeClose(LX/009;Ljava/io/Closeable;)V

    .line 1302
    invoke-static {p0, v7}, LX/009;->safeClose(LX/009;Ljava/io/Closeable;)V

    .line 1303
    invoke-static {p0, v8}, LX/009;->safeClose(LX/009;Ljava/io/Closeable;)V

    throw v0

    .line 1304
    :catch_6
    move-exception v0

    .line 1305
    :try_start_11
    iput-object v0, v4, LX/01l;->generatingIoError:Ljava/lang/Throwable;

    .line 1306
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto/16 :goto_a

    .line 1307
    :cond_d
    :try_start_12
    invoke-virtual {v1}, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->start()V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_7
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    move-object v0, v1

    .line 1308
    goto :goto_b

    .line 1309
    :catch_7
    move-exception v0

    .line 1310
    :try_start_13
    throw v0
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_8
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 1311
    :catch_8
    move-exception v0

    move-object v10, v0

    move-object v0, v1

    move-object v1, v10

    .line 1312
    :goto_d
    :try_start_14
    const-string v2, "sending in-memory report"

    invoke-direct {p0, v2, v1}, LX/009;->tryLogInternalError(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    move-object v7, v8

    goto :goto_b

    :cond_e
    move-object v0, v7

    move-object v7, v8

    .line 1313
    goto :goto_b

    .line 1314
    :catchall_2
    move-exception v0

    move-object v5, v7

    move-object v8, v7

    goto :goto_c

    :catchall_3
    move-exception v0

    move-object v5, v7

    goto :goto_c

    :catchall_4
    move-exception v0

    move-object v5, v7

    move-object v7, v9

    goto :goto_c

    .line 1315
    :catch_9
    move-exception v0

    move-object v1, v0

    move-object v0, v7

    goto :goto_d

    :cond_f
    move-object v2, p3

    goto/16 :goto_8

    :cond_10
    move-object v0, v1

    goto/16 :goto_7

    :cond_11
    move-object v8, v7

    move-object v2, v7

    move-object v1, v7

    goto/16 :goto_4
.end method

.method public static loadAcraCrashReport(LX/009;LX/04Z;)LX/01l;
    .locals 4

    .prologue
    .line 1222
    sget-object v0, LX/01V;->ACRA_CRASH_REPORT:LX/01V;

    iget-wide v2, p0, LX/009;->mMaxReportSize:J

    invoke-direct {p0, p1, v0, v2, v3}, LX/009;->loadCrashReport(LX/04Z;LX/01V;J)LX/01l;

    move-result-object v0

    return-object v0
.end method

.method private loadAttachment(Ljava/io/InputStream;I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1204
    new-array v3, p2, [B

    move v1, v0

    .line 1205
    :goto_0
    sub-int v2, p2, v1

    if-lez v2, :cond_0

    .line 1206
    sub-int v0, p2, v1

    invoke-virtual {p1, v3, v1, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 1207
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1208
    add-int/2addr v1, v0

    goto :goto_0

    .line 1209
    :cond_0
    if-nez v0, :cond_1

    .line 1210
    const-string v0, ""

    .line 1211
    :goto_1
    return-object v0

    .line 1212
    :cond_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1213
    const/4 v2, 0x0

    .line 1214
    :try_start_0
    new-instance v1, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v1, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1215
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v1, v3, v2, p2}, Ljava/util/zip/GZIPOutputStream;->write([BII)V

    .line 1216
    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->finish()V

    .line 1217
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1218
    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->close()V

    goto :goto_1

    .line 1219
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    .line 1220
    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->close()V

    :cond_2
    throw v0

    .line 1221
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method private loadCrashAttachment(LX/04Z;LX/01V;)LX/01l;
    .locals 2

    .prologue
    .line 1472
    iget-wide v0, p2, LX/01V;->defaultMaxSize:J

    invoke-direct {p0, p1, p2, v0, v1}, LX/009;->loadCrashReport(LX/04Z;LX/01V;J)LX/01l;

    move-result-object v0

    return-object v0
.end method

.method private loadCrashReport(LX/04Z;LX/01V;J)LX/01l;
    .locals 17

    .prologue
    .line 1542
    move-object/from16 v0, p1

    iget-object v4, v0, LX/04Z;->fileName:Ljava/io/File;

    .line 1543
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    .line 1544
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 1545
    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v12

    .line 1546
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 1547
    new-instance v3, LX/01l;

    invoke-direct {v3}, LX/01l;-><init>()V

    .line 1548
    const-string v2, "TIME_OF_CRASH"

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1549
    move-object/from16 v0, p0

    iget-wide v14, v0, LX/009;->mInstallTime:J

    cmp-long v2, v12, v14

    if-gez v2, :cond_1

    .line 1550
    const-string v2, "DUMP_WRONG_VERSION"

    .line 1551
    :goto_0
    if-eqz v2, :cond_4

    .line 1552
    sget-object v5, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "deleting crash report "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1553
    invoke-static {v4}, LX/009;->deleteFile(Ljava/io/File;)Z

    .line 1554
    # getter for: LX/01V;->attachmentField:Ljava/lang/String;
    invoke-static/range {p2 .. p2}, LX/01V;->access$1400(LX/01V;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1555
    # getter for: LX/01V;->attachmentField:Ljava/lang/String;
    invoke-static/range {p2 .. p2}, LX/01V;->access$1400(LX/01V;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1556
    :cond_0
    const-string v4, "MINIDUMP_EXCLUDE_REASON"

    invoke-virtual {v3, v4, v2}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1557
    :goto_1
    return-object v3

    .line 1558
    :cond_1
    sub-long/2addr v8, v12

    const-wide/32 v12, 0x240c8400

    cmp-long v2, v8, v12

    if-lez v2, :cond_2

    .line 1559
    const-string v2, "DUMP_TOO_OLD"

    goto :goto_0

    .line 1560
    :cond_2
    cmp-long v2, v6, p3

    if-lez v2, :cond_3

    .line 1561
    const-string v2, "ATTACHMENT_ORIGINAL_SIZE"

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1562
    const-string v2, "DUMP_TOO_BIG"

    goto :goto_0

    .line 1563
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 1564
    :cond_4
    new-instance v11, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    iget-object v2, v0, LX/04Z;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v11, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    const/4 v9, 0x0

    .line 1565
    :try_start_0
    new-instance v4, Ljava/io/BufferedInputStream;

    invoke-direct {v4, v11}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1566
    const/4 v8, 0x0

    .line 1567
    :try_start_1
    sget-object v2, LX/01V;->ACRA_CRASH_REPORT:LX/01V;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_5

    .line 1568
    invoke-virtual {v3, v4}, LX/01l;->load(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 1569
    :goto_2
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    .line 1570
    const-string v2, "ACRA_REPORT_FILENAME"

    invoke-virtual {v3, v2, v10}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1571
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/009;->backfillCrashReportData(LX/01l;)V

    goto :goto_1

    :cond_5
    move-object/from16 v2, p0

    move-object/from16 v5, p2

    .line 1572
    :try_start_3
    invoke-direct/range {v2 .. v7}, LX/009;->slurpAttachment(LX/01l;Ljava/io/InputStream;LX/01V;J)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_2

    .line 1573
    :catch_0
    move-exception v2

    .line 1574
    :try_start_4
    const-string v5, "REPORT_LOAD_THROW"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "throwable: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1575
    sget-object v5, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v6, "Could not load crash report: %s. File will be deleted."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v10, v7, v12

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1576
    move-object/from16 v0, p0

    iget-object v2, v0, LX/009;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v10}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_2

    .line 1577
    :catch_1
    move-exception v2

    :try_start_5
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1578
    :catchall_0
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    :goto_3
    if-eqz v3, :cond_6

    :try_start_6
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :goto_4
    :try_start_7
    throw v2
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1579
    :catch_2
    move-exception v2

    :try_start_8
    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1580
    :catchall_1
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    :goto_5
    if-eqz v3, :cond_7

    :try_start_9
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4

    :goto_6
    throw v2

    :catch_3
    move-exception v4

    :try_start_a
    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :catchall_2
    move-exception v2

    move-object v3, v9

    goto :goto_5

    :cond_6
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    goto :goto_4

    :catch_4
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_7
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    goto :goto_6

    :catchall_3
    move-exception v2

    move-object v3, v8

    goto :goto_3
.end method

.method private makeAttachmentWrapperCrashReport(LX/01V;Ljava/io/Writer;)LX/01l;
    .locals 7

    .prologue
    .line 1581
    new-instance v4, LX/01l;

    invoke-direct {v4}, LX/01l;-><init>()V

    .line 1582
    :try_start_0
    const-string v0, "ACRA_REPORT_TYPE"

    invoke-virtual {p1}, LX/01V;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1, p2}, LX/01l;->put(Ljava/lang/String;Ljava/lang/String;Ljava/io/Writer;)Ljava/lang/String;

    .line 1583
    iget-object v1, p0, LX/009;->mConfig:LX/00K;

    const-string v2, "crash attachment"

    new-instance v3, LX/08q;

    invoke-direct {v3, p0}, LX/08q;-><init>(LX/009;)V

    const/4 v6, 0x0

    move-object v0, p0

    move-object v5, p2

    invoke-static/range {v0 .. v6}, LX/04R;->gatherCrashData(LX/009;LX/00K;Ljava/lang/String;Ljava/lang/Throwable;LX/01l;Ljava/io/Writer;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1584
    :goto_0
    return-object v4

    .line 1585
    :catch_0
    move-exception v0

    .line 1586
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "retrieve exception: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1587
    const-string v1, "REPORT_LOAD_THROW"

    invoke-static {v1, v0, v4, p2}, LX/009;->put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V

    goto :goto_0
.end method

.method private static parseVersionCodeFromFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1588
    if-eqz p0, :cond_0

    .line 1589
    invoke-static {}, LX/009;->getVersionCodeRegex()Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1590
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1591
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 1592
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private populateConstantFields()V
    .locals 6

    .prologue
    .line 1593
    new-instance v0, LX/01b;

    iget-object v1, p0, LX/009;->mContext:Landroid/content/Context;

    sget-object v2, LX/00L;->LOG_TAG:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/01b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1594
    iget-object v1, v0, LX/01b;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/01b;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    move-object v1, v1

    .line 1595
    if-eqz v1, :cond_0

    .line 1596
    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/009;->mAppVersionCode:Ljava/lang/String;

    .line 1597
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-eqz v0, :cond_11

    iget-object v0, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, LX/009;->mAppVersionName:Ljava/lang/String;

    .line 1598
    :cond_0
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 1599
    const/4 v4, 0x1

    move v0, v4

    .line 1600
    if-eqz v0, :cond_1

    .line 1601
    const-string v0, "ANDROID_ID"

    iget-object v3, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "android_id"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1602
    :cond_1
    const/4 v4, 0x1

    move v0, v4

    .line 1603
    if-eqz v0, :cond_2

    .line 1604
    const-string v0, "APP_VERSION_CODE"

    iget-object v3, p0, LX/009;->mAppVersionCode:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1605
    :cond_2
    const/4 v4, 0x1

    move v0, v4

    .line 1606
    if-eqz v0, :cond_3

    .line 1607
    const-string v0, "APP_VERSION_NAME"

    iget-object v3, p0, LX/009;->mAppVersionName:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1608
    :cond_3
    const/4 v4, 0x1

    move v0, v4

    .line 1609
    if-eqz v0, :cond_4

    .line 1610
    const-string v0, "PACKAGE_NAME"

    iget-object v3, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1611
    :cond_4
    const/4 v4, 0x1

    move v0, v4

    .line 1612
    if-eqz v0, :cond_5

    .line 1613
    const-string v0, "PHONE_MODEL"

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1614
    :cond_5
    const/4 v4, 0x1

    move v0, v4

    .line 1615
    if-eqz v0, :cond_6

    .line 1616
    const-string v0, "DEVICE"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1617
    :cond_6
    const/4 v4, 0x1

    move v0, v4

    .line 1618
    if-eqz v0, :cond_7

    .line 1619
    const-string v0, "ANDROID_VERSION"

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1620
    :cond_7
    const/4 v4, 0x1

    move v0, v4

    .line 1621
    if-eqz v0, :cond_8

    .line 1622
    const-string v0, "OS_VERSION"

    const-string v3, "os.version"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1623
    :cond_8
    const/4 v4, 0x1

    move v0, v4

    .line 1624
    if-eqz v0, :cond_9

    .line 1625
    const-string v0, "BUILD_HOST"

    sget-object v3, Landroid/os/Build;->HOST:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1626
    :cond_9
    const/4 v4, 0x1

    move v0, v4

    .line 1627
    if-eqz v0, :cond_a

    .line 1628
    const-string v0, "BRAND"

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1629
    :cond_a
    const/4 v4, 0x1

    move v0, v4

    .line 1630
    if-eqz v0, :cond_b

    .line 1631
    const-string v0, "PRODUCT"

    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1632
    :cond_b
    const/4 v4, 0x1

    move v0, v4

    .line 1633
    if-eqz v0, :cond_d

    .line 1634
    const-string v0, "n/a"

    .line 1635
    iget-object v3, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    .line 1636
    if-eqz v3, :cond_c

    .line 1637
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 1638
    :cond_c
    const-string v3, "FILE_PATH"

    invoke-virtual {v2, v3, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1639
    :cond_d
    const/4 v4, 0x1

    move v0, v4

    .line 1640
    if-eqz v0, :cond_e

    .line 1641
    const-string v0, "SERIAL"

    sget-object v3, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1642
    :cond_e
    const/4 v4, 0x1

    move v0, v4

    .line 1643
    if-eqz v0, :cond_f

    .line 1644
    if-eqz v1, :cond_f

    .line 1645
    const-string v0, "APP_INSTALL_TIME"

    iget-wide v4, v1, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    invoke-static {v4, v5}, LX/01c;->formatTimestamp(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1646
    :cond_f
    const/4 v4, 0x1

    move v0, v4

    .line 1647
    if-eqz v0, :cond_10

    .line 1648
    if-eqz v1, :cond_10

    .line 1649
    const-string v0, "APP_UPGRADE_TIME"

    iget-wide v4, v1, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-static {v4, v5}, LX/01c;->formatTimestamp(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1650
    :cond_10
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/009;->mConstantFields:Ljava/util/Map;

    .line 1651
    return-void

    .line 1652
    :cond_11
    const-string v0, "not set"

    goto/16 :goto_0
.end method

.method private preallocateReportFile(Ljava/io/File;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1653
    const-class v0, LX/009;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    const-string v3, ".stacktrace"

    invoke-direct {p0, v0, v1, v3}, LX/009;->genCrashReportFileName(Ljava/lang/Class;Ljava/util/UUID;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1654
    sget-object v1, LX/01V;->ACRA_CRASH_REPORT:LX/01V;

    iget-object v3, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, LX/01V;->getSpool(Landroid/content/Context;)LX/01e;

    move-result-object v1

    .line 1655
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/01e;->produceWithDonorFile(Ljava/lang/String;Ljava/io/File;)LX/04Q;

    move-result-object v3

    move-object v3, v3

    .line 1656
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v0, v3, LX/04Q;->fileName:Ljava/io/File;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1657
    const/16 v0, 0x2800

    :try_start_1
    new-array v5, v0, [B

    .line 1658
    const-wide/16 v0, 0x0

    .line 1659
    :goto_0
    const-wide/32 v6, 0x100000

    cmp-long v6, v0, v6

    if-gez v6, :cond_0

    .line 1660
    invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V

    .line 1661
    const-wide/16 v6, 0x2800

    add-long/2addr v0, v6

    goto :goto_0

    .line 1662
    :cond_0
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 1663
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 1664
    iget-object v0, v3, LX/04Q;->fileName:Ljava/io/File;

    invoke-static {v0, p1}, LX/009;->renameOrThrow(Ljava/io/File;Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1665
    :try_start_3
    iget-object v0, v3, LX/04Q;->fileName:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1666
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/04Q;->close()V

    .line 1667
    :cond_1
    return-void

    .line 1668
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1669
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_1
    if-eqz v1, :cond_3

    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_2
    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1670
    :catchall_1
    move-exception v0

    :try_start_7
    iget-object v1, v3, LX/04Q;->fileName:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    throw v0
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 1671
    :catch_1
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 1672
    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v3, :cond_2

    if-eqz v2, :cond_4

    :try_start_9
    invoke-virtual {v3}, LX/04Q;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_3

    :cond_2
    :goto_4
    throw v0

    .line 1673
    :catch_2
    move-exception v4

    :try_start_a
    invoke-static {v1, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_2

    .line 1674
    :catch_3
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_4
    invoke-virtual {v3}, LX/04Q;->close()V

    goto :goto_4

    :catchall_3
    move-exception v0

    goto :goto_3

    .line 1675
    :catchall_4
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method private processCrashAttachmentsLocked(ILX/01V;LX/01i;)I
    .locals 18

    .prologue
    .line 1676
    const/4 v4, 0x0

    .line 1677
    const/4 v6, 0x0

    .line 1678
    const/4 v7, 0x0

    .line 1679
    move-object/from16 v0, p0

    iget-object v2, v0, LX/009;->mContext:Landroid/content/Context;

    # getter for: LX/01V;->directory:Ljava/lang/String;
    invoke-static/range {p2 .. p2}, LX/01V;->access$1300(LX/01V;)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    .line 1680
    new-instance v3, Ljava/io/File;

    const-string v5, ".noupload"

    invoke-direct {v3, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1681
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1682
    const/4 v2, 0x0

    .line 1683
    :goto_0
    return v2

    .line 1684
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/009;->mContext:Landroid/content/Context;

    move-object/from16 v0, p2

    invoke-static {v0, v2}, LX/01V;->getCrashReports(LX/01V;Landroid/content/Context;)LX/01h;

    move-result-object v10

    const/4 v9, 0x0

    move v5, v4

    .line 1685
    :goto_1
    :try_start_0
    invoke-virtual {v10}, LX/01h;->hasNext()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move-result v2

    if-eqz v2, :cond_14

    move/from16 v0, p1

    if-ge v7, v0, :cond_14

    .line 1686
    const/4 v3, 0x0

    .line 1687
    :try_start_1
    invoke-virtual {v10}, LX/01h;->next()LX/04Z;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v11

    const/4 v8, 0x0

    .line 1688
    add-int/lit8 v4, v5, 0x1

    const/4 v2, 0x5

    if-lt v5, v2, :cond_2

    .line 1689
    :try_start_2
    iget-object v2, v11, LX/04Z;->fileName:Ljava/io/File;

    invoke-static {v2}, LX/009;->deleteFile(Ljava/io/File;)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 1690
    if-eqz v11, :cond_1

    :try_start_3
    invoke-virtual {v11}, LX/04Z;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :cond_1
    move v5, v4

    goto :goto_1

    .line 1691
    :cond_2
    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v11, v1}, LX/009;->loadCrashAttachment(LX/04Z;LX/01V;)LX/01l;
    :try_end_4
    .catch LX/09b; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v12

    .line 1692
    if-nez v12, :cond_4

    .line 1693
    if-eqz v11, :cond_3

    :try_start_5
    invoke-virtual {v11}, LX/04Z;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :cond_3
    move v5, v4

    goto :goto_1

    .line 1694
    :cond_4
    if-eqz p3, :cond_13

    .line 1695
    :try_start_6
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-virtual/range {p3 .. p3}, LX/01i;->generate()Ljava/io/File;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-static {v2}, LX/01l;->getWriter(Ljava/io/OutputStream;)Ljava/io/Writer;
    :try_end_6
    .catch LX/09b; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v5

    .line 1696
    :goto_2
    :try_start_7
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v5}, LX/009;->makeAttachmentWrapperCrashReport(LX/01V;Ljava/io/Writer;)LX/01l;

    move-result-object v13

    .line 1697
    if-eqz p3, :cond_5

    .line 1698
    const/4 v2, 0x1

    iput-boolean v2, v13, LX/01l;->throwAwayWrites:Z

    .line 1699
    :cond_5
    # getter for: LX/01V;->attachmentField:Ljava/lang/String;
    invoke-static/range {p2 .. p2}, LX/01V;->access$1400(LX/01V;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/01l;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1700
    iget-object v3, v11, LX/04Z;->fileName:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1701
    const-string v14, "REPORT_ID"

    const/4 v15, 0x0

    const/16 v16, 0x2e

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v14, v3, v5}, LX/01l;->put(Ljava/lang/String;Ljava/lang/String;Ljava/io/Writer;)Ljava/lang/String;

    .line 1702
    # getter for: LX/01V;->attachmentField:Ljava/lang/String;
    invoke-static/range {p2 .. p2}, LX/01V;->access$1400(LX/01V;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v3, v2, v5}, LX/01l;->put(Ljava/lang/String;Ljava/lang/String;Ljava/io/Writer;)Ljava/lang/String;

    .line 1703
    invoke-virtual {v13, v12, v5}, LX/01l;->merge(LX/01l;Ljava/io/Writer;)V

    .line 1704
    const-string v2, "EXCEPTION_CAUSE"

    const-string v3, "crash attachment"

    invoke-virtual {v13, v2, v3, v5}, LX/01l;->put(Ljava/lang/String;Ljava/lang/String;Ljava/io/Writer;)Ljava/lang/String;
    :try_end_7
    .catch LX/09b; {:try_start_7 .. :try_end_7} :catch_e
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_c
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    .line 1705
    add-int/lit8 v3, v6, 0x1

    .line 1706
    if-nez v5, :cond_6

    .line 1707
    :try_start_8
    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/009;->sendCrashReport(LX/009;LX/01l;)V

    .line 1708
    :cond_6
    iget-object v2, v11, LX/04Z;->fileName:Ljava/io/File;

    invoke-static {v2}, LX/009;->deleteFile(Ljava/io/File;)Z
    :try_end_8
    .catch LX/09b; {:try_start_8 .. :try_end_8} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_d
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    .line 1709
    add-int/lit8 v2, v7, 0x1

    .line 1710
    if-eqz v5, :cond_7

    .line 1711
    :try_start_9
    invoke-virtual {v5}, Ljava/io/Writer;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_a
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 1712
    :cond_7
    if-eqz v11, :cond_11

    :try_start_a
    invoke-virtual {v11}, LX/04Z;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :goto_3
    move v7, v2

    move v6, v3

    move v5, v4

    .line 1713
    goto/16 :goto_1

    .line 1714
    :catch_0
    move-exception v2

    move v5, v6

    .line 1715
    :goto_4
    :try_start_b
    sget-object v6, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Failed to send crash attachment report "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v13, v11, LX/04Z;->fileName:Ljava/io/File;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v6, v12, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_9

    .line 1716
    if-eqz v3, :cond_8

    .line 1717
    :try_start_c
    invoke-virtual {v3}, Ljava/io/Writer;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_b
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    .line 1718
    :cond_8
    if-eqz v11, :cond_9

    :try_start_d
    invoke-virtual {v11}, LX/04Z;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :cond_9
    move v2, v5

    .line 1719
    :goto_5
    if-eqz v10, :cond_a

    invoke-virtual {v10}, LX/01h;->close()V

    .line 1720
    :cond_a
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "#processCrashAttachmentsLocked - finish, sent: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1721
    :catch_1
    move-exception v2

    move-object v5, v3

    .line 1722
    :goto_6
    :try_start_e
    sget-object v3, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Failed on crash attachment file "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v13, v11, LX/04Z;->fileName:Ljava/io/File;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v3, v12, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1723
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v12, "ANRValidationError<"

    invoke-direct {v3, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v12, "::Non-cached>"

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-static {v2, v3, v12, v0}, LX/009;->reportSoftError(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;LX/009;)V

    .line 1724
    iget-object v2, v11, LX/04Z;->fileName:Ljava/io/File;

    invoke-static {v2}, LX/009;->deleteFile(Ljava/io/File;)Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    .line 1725
    if-eqz v5, :cond_b

    .line 1726
    :try_start_f
    invoke-virtual {v5}, Ljava/io/Writer;->close()V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_2
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    .line 1727
    :cond_b
    if-eqz v11, :cond_c

    :try_start_10
    invoke-virtual {v11}, LX/04Z;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    :cond_c
    move v2, v6

    goto :goto_5

    .line 1728
    :catchall_0
    move-exception v2

    move-object v5, v3

    :goto_7
    if-eqz v5, :cond_d

    .line 1729
    :try_start_11
    invoke-virtual {v5}, Ljava/io/Writer;->close()V

    :cond_d
    throw v2
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_2
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    .line 1730
    :catch_2
    move-exception v2

    move v3, v7

    :goto_8
    :try_start_12
    throw v2
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 1731
    :catchall_1
    move-exception v5

    move v7, v3

    move-object v3, v2

    move-object v2, v5

    :goto_9
    if-eqz v11, :cond_e

    if-eqz v3, :cond_10

    :try_start_13
    invoke-virtual {v11}, LX/04Z;->close()V
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_5
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_3
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    :cond_e
    :goto_a
    :try_start_14
    throw v2
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_4
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    :catch_3
    move-exception v2

    move-object v5, v2

    move v3, v6

    move v2, v7

    .line 1732
    :goto_b
    :try_start_15
    sget-object v6, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v7, "IO Exception"

    invoke-static {v6, v7, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1733
    const-string v6, "ANRValidationError<IOException::Non-cached>"

    invoke-virtual {v5}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-static {v5, v6, v7, v0}, LX/009;->reportSoftError(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;LX/009;)V
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_15 .. :try_end_15} :catch_4
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    goto/16 :goto_3

    .line 1734
    :catch_4
    move-exception v2

    :try_start_16
    throw v2
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_2

    .line 1735
    :catchall_2
    move-exception v3

    move-object/from16 v17, v3

    move-object v3, v2

    move-object/from16 v2, v17

    :goto_c
    if-eqz v10, :cond_f

    if-eqz v3, :cond_12

    :try_start_17
    invoke-virtual {v10}, LX/01h;->close()V
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_6

    :cond_f
    :goto_d
    throw v2

    .line 1736
    :catch_5
    move-exception v5

    :try_start_18
    invoke-static {v3, v5}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_a

    .line 1737
    :catchall_3
    move-exception v2

    move-object v3, v9

    goto :goto_c

    .line 1738
    :cond_10
    invoke-virtual {v11}, LX/04Z;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_18} :catch_4
    .catchall {:try_start_18 .. :try_end_18} :catchall_3

    goto :goto_a

    :cond_11
    move v7, v2

    move v6, v3

    move v5, v4

    .line 1739
    goto/16 :goto_1

    .line 1740
    :catch_6
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_d

    :cond_12
    invoke-virtual {v10}, LX/01h;->close()V

    goto :goto_d

    .line 1741
    :catch_7
    move-exception v2

    move v3, v6

    move v4, v5

    move-object v5, v2

    move v2, v7

    goto :goto_b

    :catch_8
    move-exception v2

    move v3, v5

    move-object v5, v2

    move v2, v7

    goto :goto_b

    :catch_9
    move-exception v5

    goto :goto_b

    :catchall_4
    move-exception v2

    move-object v3, v8

    goto :goto_9

    :catchall_5
    move-exception v5

    move v7, v2

    move v6, v3

    move-object v2, v5

    move-object v3, v8

    goto :goto_9

    :catchall_6
    move-exception v2

    move-object v3, v8

    move v6, v5

    goto :goto_9

    .line 1742
    :catch_a
    move-exception v5

    move v6, v3

    move v3, v2

    move-object v2, v5

    goto :goto_8

    :catch_b
    move-exception v2

    move v3, v7

    move v6, v5

    goto :goto_8

    .line 1743
    :catchall_7
    move-exception v2

    goto :goto_7

    :catchall_8
    move-exception v2

    move v6, v3

    goto :goto_7

    :catchall_9
    move-exception v2

    move v6, v5

    move-object v5, v3

    goto/16 :goto_7

    .line 1744
    :catch_c
    move-exception v2

    goto/16 :goto_6

    :catch_d
    move-exception v2

    move v6, v3

    goto/16 :goto_6

    .line 1745
    :catch_e
    move-exception v2

    move-object v3, v5

    move v5, v6

    goto/16 :goto_4

    :catch_f
    move-exception v2

    move-object/from16 v17, v5

    move v5, v3

    move-object/from16 v3, v17

    goto/16 :goto_4

    :cond_13
    move-object v5, v3

    goto/16 :goto_2

    :cond_14
    move v2, v6

    goto/16 :goto_5
.end method

.method public static purgeDirectory(Ljava/io/File;)V
    .locals 5

    .prologue
    .line 1746
    if-eqz p0, :cond_1

    .line 1747
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 1748
    if-eqz v0, :cond_1

    .line 1749
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1750
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1751
    invoke-static {v3}, LX/009;->purgeDirectory(Ljava/io/File;)V

    .line 1752
    :cond_0
    invoke-static {v3}, LX/009;->deleteFile(Ljava/io/File;)Z

    .line 1753
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1754
    :cond_1
    return-void
.end method

.method public static put(Ljava/lang/String;Ljava/lang/String;LX/01l;Ljava/io/Writer;)V
    .locals 1

    .prologue
    .line 1536
    iget-object v0, p2, LX/01l;->generatingIoError:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    .line 1537
    const/4 p3, 0x0

    .line 1538
    :cond_0
    :try_start_0
    invoke-virtual {p2, p0, p1, p3}, LX/01l;->put(Ljava/lang/String;Ljava/lang/String;Ljava/io/Writer;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1539
    :goto_0
    return-void

    .line 1540
    :catch_0
    move-exception v0

    .line 1541
    iput-object v0, p2, LX/01l;->generatingIoError:Ljava/lang/Throwable;

    goto :goto_0
.end method

.method private static readFile(Ljava/io/File;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1755
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1756
    const-string v0, "NO_FILE"

    .line 1757
    :goto_0
    return-object v0

    .line 1758
    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, p0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1759
    :try_start_1
    new-instance v4, Ljava/io/BufferedReader;

    const/16 v0, 0x400

    invoke-direct {v4, v3, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1760
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result-object v0

    .line 1761
    if-eqz v0, :cond_1

    .line 1762
    :try_start_3
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1763
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    :catch_0
    :goto_1
    move-object v0, v1

    .line 1764
    goto :goto_0

    .line 1765
    :cond_1
    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1766
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_1

    .line 1767
    :catch_1
    move-exception v2

    :try_start_7
    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1768
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v2, :cond_2

    :try_start_8
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :goto_3
    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 1769
    :catch_2
    move-exception v0

    :try_start_a
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 1770
    :catchall_1
    move-exception v2

    move-object v5, v2

    move-object v2, v0

    move-object v0, v5

    :goto_4
    if-eqz v2, :cond_3

    :try_start_b
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    :goto_5
    :try_start_c
    throw v0
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    .line 1771
    :catch_3
    move-exception v4

    :try_start_d
    invoke-static {v2, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1772
    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_4

    .line 1773
    :cond_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    goto :goto_3

    .line 1774
    :catch_4
    move-exception v3

    :try_start_e
    invoke-static {v2, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_3
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0

    goto :goto_5

    .line 1775
    :catchall_3
    move-exception v0

    move-object v2, v1

    goto :goto_2
.end method

.method private static renameOrThrow(Ljava/io/File;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 1533
    invoke-virtual {p0, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1534
    new-instance v0, Ljava/io/IOException;

    const-string v1, "rename of %s to %s failed"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1535
    :cond_0
    return-void
.end method

.method private static reportSoftError(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;LX/009;)V
    .locals 2

    .prologue
    .line 1528
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1529
    const-string v1, "soft_error_category"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1530
    const-string v1, "soft_error_message"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1531
    invoke-virtual {p3, p0, v0}, LX/009;->handleException(Ljava/lang/Throwable;Ljava/util/Map;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;

    .line 1532
    return-void
.end method

.method public static safeClose(LX/009;Ljava/io/Closeable;)V
    .locals 2

    .prologue
    .line 1523
    if-eqz p1, :cond_0

    .line 1524
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1525
    :cond_0
    :goto_0
    return-void

    .line 1526
    :catch_0
    move-exception v0

    .line 1527
    const-string v1, "safeClose"

    invoke-direct {p0, v1, v0}, LX/009;->tryLogInternalError(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static sendCrashReport(LX/009;LX/01l;)V
    .locals 7

    .prologue
    .line 1507
    const/4 v0, 0x0

    .line 1508
    iget-object v1, p0, LX/009;->mReportSenders:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1509
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, LX/009;->mReportSenders:Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1510
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1511
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1512
    new-instance v0, LX/09b;

    const-string v1, "no configured report senders"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/09b;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 1513
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1514
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00M;

    .line 1515
    :try_start_2
    invoke-virtual {v0, p1}, LX/00M;->send(LX/01l;)V
    :try_end_2
    .catch LX/09b; {:try_start_2 .. :try_end_2} :catch_0

    .line 1516
    const/4 v0, 0x1

    move v1, v0

    .line 1517
    goto :goto_0

    .line 1518
    :catch_0
    move-exception v3

    .line 1519
    if-nez v1, :cond_1

    .line 1520
    throw v3

    .line 1521
    :cond_1
    sget-object v4, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ReportSender of class "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " failed but other senders completed their task. ACRA will not send this report again."

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1522
    :cond_2
    return-void
.end method

.method public static declared-synchronized shouldReportANRs(LX/009;)Z
    .locals 1

    .prologue
    .line 1471
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/009;->mANRDataProvider:LX/09r;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/009;->mANRDataProvider:LX/09r;

    invoke-interface {v0}, LX/09r;->shouldANRDetectorRun()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private slurpAttachment(LX/01l;Ljava/io/InputStream;LX/01V;J)V
    .locals 4

    .prologue
    .line 1493
    sget-object v0, LX/01V;->NATIVE_CRASH_REPORT:LX/01V;

    if-ne p3, v0, :cond_0

    .line 1494
    :try_start_0
    invoke-direct {p0, p1}, LX/009;->attachLastUrlInfo(LX/01l;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1495
    :goto_0
    :try_start_1
    invoke-direct {p0, p1}, LX/009;->attachLastActivityInfo(LX/01l;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1496
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, LX/009;->attachIabInfo(LX/01l;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1497
    :cond_0
    :goto_2
    long-to-int v0, p4

    invoke-direct {p0, p2, v0}, LX/009;->loadAttachment(Ljava/io/InputStream;I)Ljava/lang/String;

    move-result-object v0

    .line 1498
    iget-object v1, p3, LX/01V;->attachmentField:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1499
    const-string v0, "ATTACHMENT_ORIGINAL_SIZE"

    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1500
    return-void

    .line 1501
    :catch_0
    move-exception v0

    .line 1502
    const-string v1, "ErrorReporter"

    const-string v2, "error attching URL information"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1503
    :catch_1
    move-exception v0

    .line 1504
    const-string v1, "ErrorReporter"

    const-string v2, "error attaching activity information"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1505
    :catch_2
    move-exception v0

    .line 1506
    const-string v1, "ErrorReporter"

    const-string v2, "error attaching IAB information"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public static throwableToString(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1486
    if-nez p0, :cond_0

    .line 1487
    new-instance p0, Ljava/lang/Exception;

    const-string v0, "Report requested by developer"

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1488
    :cond_0
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 1489
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 1490
    invoke-virtual {p0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 1491
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    .line 1492
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private translateException(Ljava/lang/Throwable;Ljava/util/Map;)Ljava/lang/Throwable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Throwable;"
        }
    .end annotation

    .prologue
    .line 1477
    const/4 v1, 0x0

    .line 1478
    iget-object v0, p0, LX/009;->mExceptionTranslationHook:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00B;

    :goto_0
    move-object v3, v0

    move-object v2, p1

    .line 1479
    :goto_1
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 1480
    :try_start_0
    invoke-virtual {v3, v2, p2}, LX/00B;->translate(Ljava/lang/Throwable;Ljava/util/Map;)Ljava/lang/Throwable;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1481
    :goto_2
    iget-object v3, v3, LX/00B;->next:LX/00B;

    goto :goto_1

    .line 1482
    :catch_0
    move-exception v4

    .line 1483
    sget-object v5, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ignoring error in exception translation hook "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 1484
    :cond_0
    if-eq v2, p1, :cond_1

    add-int/lit8 v1, v1, 0x1

    const/4 v3, 0x4

    if-lt v1, v3, :cond_2

    .line 1485
    :cond_1
    return-object v2

    :cond_2
    move-object p1, v2

    goto :goto_0
.end method

.method private tryLogInternalError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1473
    if-nez p1, :cond_0

    .line 1474
    :try_start_0
    const-string p1, "???"

    .line 1475
    :cond_0
    sget-object v0, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "internal ACRA error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1476
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private tryLogInternalError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1338
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/009;->tryLogInternalError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1339
    return-void
.end method

.method private uncaughtExceptionImpl(Ljava/lang/Thread;Ljava/lang/Throwable;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 1447
    iput-object v0, p0, LX/009;->mOomReservation:[B

    .line 1448
    new-array v0, v4, [LX/01V;

    const/4 v1, 0x0

    sget-object v2, LX/01V;->ACRA_CRASH_REPORT:LX/01V;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, LX/009;->discardOverlappingReports([LX/01V;)V

    .line 1449
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    .line 1450
    invoke-static {}, LX/0Bh;->disableStrictMode()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1451
    :cond_0
    :goto_0
    :try_start_1
    sget-object v0, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ACRA caught a "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exception for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Building report."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 1452
    :goto_1
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    .line 1453
    invoke-direct {p0, p2, v1}, LX/009;->translateException(Ljava/lang/Throwable;Ljava/util/Map;)Ljava/lang/Throwable;

    move-result-object v2

    .line 1454
    if-nez v2, :cond_1

    .line 1455
    :goto_2
    return-void

    .line 1456
    :catch_0
    move-exception v0

    .line 1457
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1458
    :catch_1
    move-exception v0

    .line 1459
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1460
    :cond_1
    if-eqz p3, :cond_3

    .line 1461
    const/4 v0, 0x4

    .line 1462
    :goto_3
    invoke-static {v2}, LX/009;->getMostSignificantCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v3

    instance-of v3, v3, Ljava/lang/OutOfMemoryError;

    if-eqz v3, :cond_2

    .line 1463
    and-int/lit8 v0, v0, -0x2

    .line 1464
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-direct {p0, v2, v1, v3, v0}, LX/009;->handleExceptionInternal(Ljava/lang/Throwable;Ljava/util/Map;Ljava/lang/String;I)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 1465
    :catch_2
    move-exception v0

    .line 1466
    if-eqz p3, :cond_4

    .line 1467
    throw v0

    .line 1468
    :cond_3
    const/4 v0, 0x3

    goto :goto_3

    .line 1469
    :cond_4
    sget-object v1, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v2, "error during error reporting: will attempt to report error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1470
    invoke-direct {p0, p1, v0, v4}, LX/009;->uncaughtExceptionImpl(Ljava/lang/Thread;Ljava/lang/Throwable;Z)V

    goto :goto_2
.end method

.method private writeToLogBridge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 7
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1061
    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    aget-object v1, v1, v0

    .line 1062
    invoke-virtual {p3}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 1063
    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1064
    const-string v0, "ErrorReporter"

    const-string v1, "Unable to log over log bridge due to exception."

    invoke-static {v0, v1, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1065
    :goto_1
    return-void

    .line 1066
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1067
    :cond_1
    invoke-virtual {p0}, LX/009;->getLogBridge()LX/03d;

    move-result-object v0

    .line 1068
    if-eqz v0, :cond_3

    .line 1069
    if-eqz p4, :cond_2

    .line 1070
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, LX/03d;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1071
    :cond_2
    invoke-interface {v0, p1, p2, p3}, LX/03d;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1072
    :cond_3
    if-eqz p4, :cond_4

    .line 1073
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1074
    :cond_4
    invoke-static {p1, p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method


# virtual methods
.method public final addExceptionTranslationHook(LX/00B;)V
    .locals 1

    .prologue
    .line 1059
    iget-object v0, p0, LX/009;->mExceptionTranslationHook:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00B;

    iput-object v0, p1, LX/00B;->next:LX/00B;

    .line 1060
    return-void
.end method

.method public final addReportSender(LX/00M;)V
    .locals 2

    .prologue
    .line 1056
    iget-object v1, p0, LX/009;->mReportSenders:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1057
    :try_start_0
    iget-object v0, p0, LX/009;->mReportSenders:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1058
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final backfillCrashReportData(LX/01l;)V
    .locals 5

    .prologue
    .line 1040
    const-string v0, "ACRA_REPORT_FILENAME"

    invoke-virtual {p1, v0}, LX/01l;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/009;->parseVersionCodeFromFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1041
    iget-object v1, p0, LX/009;->mAppVersionCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v2, v0

    .line 1042
    :goto_0
    const-string v0, "REPORT_ID"

    invoke-virtual {p1, v0}, LX/01l;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1043
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 1044
    :cond_0
    iget-object v0, p0, LX/009;->mConstantFields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1045
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v4, "APP_VERSION_NAME"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1046
    if-nez v2, :cond_1

    .line 1047
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1048
    :cond_2
    const/4 v0, 0x0

    move v2, v0

    goto :goto_0

    .line 1049
    :cond_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/01l;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1050
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1051
    :cond_4
    invoke-virtual {p0}, LX/009;->getUserId()Ljava/lang/String;

    move-result-object v1

    .line 1052
    const-string v0, "UID"

    invoke-virtual {p1, v0}, LX/01l;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1053
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1054
    const-string v0, "UID"

    invoke-virtual {p1, v0, v1}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1055
    :cond_5
    return-void
.end method

.method public final varargs checkReportsOfType([LX/01V;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;
    .locals 1

    .prologue
    .line 1037
    new-instance v0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;

    invoke-direct {v0, p0, p1}, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;-><init>(LX/009;[LX/01V;)V

    .line 1038
    invoke-virtual {v0}, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->start()V

    .line 1039
    return-object v0
.end method

.method public final checkReportsOnApplicationStart()V
    .locals 1

    .prologue
    .line 1034
    sget-object v0, LX/009;->REPORTS_TO_CHECK_ON_STARTUP:[LX/01V;

    invoke-virtual {p0, v0}, LX/009;->roughlyCountReportsOfType([LX/01V;)I

    move-result v0

    if-lez v0, :cond_0

    .line 1035
    sget-object v0, LX/009;->REPORTS_TO_CHECK_ON_STARTUP:[LX/01V;

    invoke-virtual {p0, v0}, LX/009;->checkReportsOfType([LX/01V;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;

    .line 1036
    :cond_0
    return-void
.end method

.method public final dumpCustomDataToString(Ljava/util/Map;Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Throwable;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1015
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1016
    iget-object v1, p0, LX/009;->mInstanceCustomParameters:Ljava/util/Map;

    monitor-enter v1

    .line 1017
    :try_start_0
    iget-object v0, p0, LX/009;->mInstanceCustomParameters:Ljava/util/Map;

    invoke-direct {p0, v3, v0}, LX/009;->dumpCustomDataMap(Ljava/lang/StringBuilder;Ljava/util/Map;)V

    .line 1018
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1019
    if-eqz p1, :cond_0

    .line 1020
    invoke-direct {p0, v3, p1}, LX/009;->dumpCustomDataMap(Ljava/lang/StringBuilder;Ljava/util/Map;)V

    .line 1021
    :cond_0
    const/4 v2, 0x0

    .line 1022
    :try_start_1
    iget-object v4, p0, LX/009;->mInstanceLazyCustomParameters:Ljava/util/Map;

    monitor-enter v4
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 1023
    :try_start_2
    new-instance v1, Ljava/util/TreeMap;

    iget-object v0, p0, LX/009;->mInstanceLazyCustomParameters:Ljava/util/Map;

    invoke-direct {v1, v0}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1024
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1025
    :goto_0
    if-eqz v1, :cond_1

    .line 1026
    invoke-direct {p0, v3, v1, p2}, LX/009;->dumpLazyCustomDataMap(Ljava/lang/StringBuilder;Ljava/util/Map;Ljava/lang/Throwable;)V

    .line 1027
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1028
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 1029
    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_1
    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0

    .line 1030
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 1031
    :goto_2
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V

    move-object v1, v2

    goto :goto_0

    .line 1032
    :catch_1
    move-exception v0

    goto :goto_2

    .line 1033
    :catchall_2
    move-exception v0

    goto :goto_1
.end method

.method public final getActivityLogger()LX/01a;
    .locals 1

    .prologue
    .line 1014
    iget-object v0, p0, LX/009;->mActivityLogger:LX/01a;

    return-object v0
.end method

.method public final getAppStartDate()Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 1013
    iget-object v0, p0, LX/009;->mAppStartDate:Landroid/text/format/Time;

    return-object v0
.end method

.method public final getConstantFields()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1012
    iget-object v0, p0, LX/009;->mConstantFields:Ljava/util/Map;

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1011
    iget-object v0, p0, LX/009;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public final getCustomData(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1008
    iget-object v1, p0, LX/009;->mInstanceCustomParameters:Ljava/util/Map;

    monitor-enter v1

    .line 1009
    :try_start_0
    iget-object v0, p0, LX/009;->mInstanceCustomParameters:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    monitor-exit v1

    return-object v0

    .line 1010
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final getCustomFieldsSnapshot()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1005
    iget-object v1, p0, LX/009;->mInstanceCustomParameters:Ljava/util/Map;

    monitor-enter v1

    .line 1006
    :try_start_0
    new-instance v0, Ljava/util/TreeMap;

    iget-object v2, p0, LX/009;->mInstanceCustomParameters:Ljava/util/Map;

    invoke-direct {v0, v2}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    monitor-exit v1

    return-object v0

    .line 1007
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final getLazyCustomFieldsSnapshot()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/00T;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1002
    iget-object v1, p0, LX/009;->mInstanceLazyCustomParameters:Ljava/util/Map;

    monitor-enter v1

    .line 1003
    :try_start_0
    new-instance v0, Ljava/util/TreeMap;

    iget-object v2, p0, LX/009;->mInstanceLazyCustomParameters:Ljava/util/Map;

    invoke-direct {v0, v2}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    monitor-exit v1

    return-object v0

    .line 1004
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final getLogBridge()LX/03d;
    .locals 1

    .prologue
    .line 1001
    iget-object v0, p0, LX/009;->mLogBridge:LX/03d;

    return-object v0
.end method

.method public final getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1000
    iget-object v0, p0, LX/009;->mUserId:Ljava/lang/String;

    return-object v0
.end method

.method public final handleException(Ljava/lang/Throwable;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;
    .locals 1

    .prologue
    .line 999
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/009;->handleException(Ljava/lang/Throwable;Ljava/util/Map;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;

    move-result-object v0

    return-object v0
.end method

.method public final handleException(Ljava/lang/Throwable;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;"
        }
    .end annotation

    .prologue
    .line 998
    const/4 v0, 0x1

    invoke-direct {p0, p1, p3, p2, v0}, LX/009;->handleExceptionInternal(Ljava/lang/Throwable;Ljava/util/Map;Ljava/lang/String;I)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;

    move-result-object v0

    return-object v0
.end method

.method public final handleException(Ljava/lang/Throwable;Ljava/util/Map;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;"
        }
    .end annotation

    .prologue
    .line 997
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, LX/009;->handleExceptionInternal(Ljava/lang/Throwable;Ljava/util/Map;Ljava/lang/String;I)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;

    move-result-object v0

    return-object v0
.end method

.method public final handleExceptionDelayed(Ljava/lang/Throwable;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1129
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, LX/009;->handleExceptionInternal(Ljava/lang/Throwable;Ljava/util/Map;Ljava/lang/String;I)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;

    .line 1130
    return-void
.end method

.method public final init(LX/00K;)V
    .locals 4

    .prologue
    .line 1185
    iget-boolean v0, p0, LX/009;->mInitializationComplete:Z

    if-eqz v0, :cond_0

    .line 1186
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ErrorReporter already initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1187
    :cond_0
    iget-object v0, p1, LX/00K;->mApplicationContext:Landroid/content/Context;

    move-object v0, v0

    .line 1188
    iput-object v0, p0, LX/009;->mContext:Landroid/content/Context;

    .line 1189
    iget-object v0, p0, LX/009;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 1190
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "context must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1191
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, LX/009;->mInstallTime:J

    .line 1192
    iget-wide v0, p0, LX/009;->mInstallTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 1193
    sget-object v0, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v1, "could not retrieve APK mod time"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1194
    :cond_2
    iput-object p1, p0, LX/009;->mConfig:LX/00K;

    .line 1195
    iget-object v0, p1, LX/00K;->mExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    move-object v0, v0

    .line 1196
    iput-object v0, p0, LX/009;->mChainedHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 1197
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/009;->mInitializationComplete:Z

    .line 1198
    return-void
.end method

.method public final initFallible()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1172
    const/high16 v0, 0x10000

    new-array v0, v0, [B

    iput-object v0, p0, LX/009;->mOomReservation:[B

    .line 1173
    iget-object v0, p0, LX/009;->mOomReservation:[B

    const/4 v1, 0x1

    aput-byte v1, v0, v2

    .line 1174
    iget-object v0, p0, LX/009;->mAppStartDate:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 1175
    invoke-direct {p0}, LX/009;->populateConstantFields()V

    .line 1176
    iget-object v0, p0, LX/009;->mContext:Landroid/content/Context;

    const-string v1, "acra-reports"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 1177
    new-instance v0, Ljava/io/File;

    const-string v2, "reportfile.prealloc"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1178
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/32 v4, 0x100000

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1179
    :try_start_0
    invoke-direct {p0, v0}, LX/009;->preallocateReportFile(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1180
    :cond_0
    :goto_0
    iput-object v0, p0, LX/009;->mPreallocFileName:Ljava/io/File;

    .line 1181
    return-void

    .line 1182
    :catch_0
    move-exception v0

    .line 1183
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V

    .line 1184
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs prepareReports(ILX/01i;[LX/01V;)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1160
    sget-object v1, LX/009;->UNCAUGHT_EXCEPTION_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1161
    :try_start_0
    sget-object v0, LX/009;->UNCAUGHT_EXCEPTION_LOCK:Ljava/lang/Object;

    const v3, 0x883628f

    invoke-static {v0, v3}, LX/02L;->b(Ljava/lang/Object;I)V

    .line 1162
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1163
    invoke-direct {p0, p3}, LX/009;->discardOverlappingReports([LX/01V;)V

    .line 1164
    array-length v3, p3

    move v1, v2

    move v0, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, p3, v1

    .line 1165
    sub-int v5, p1, v0

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1166
    invoke-virtual {v4}, LX/01V;->getHandler()LX/01X;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1167
    invoke-direct {p0, v5, v4}, LX/009;->checkAndHandleReportsLocked(ILX/01V;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1168
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1169
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1170
    :cond_0
    invoke-direct {p0, v5, v4, p2}, LX/009;->processCrashAttachmentsLocked(ILX/01V;LX/01i;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    .line 1171
    :cond_1
    return v0
.end method

.method public final putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1152
    if-nez p1, :cond_0

    .line 1153
    const/4 v0, 0x0

    .line 1154
    :goto_0
    return-object v0

    .line 1155
    :cond_0
    if-eqz p2, :cond_1

    .line 1156
    iget-object v1, p0, LX/009;->mInstanceCustomParameters:Ljava/util/Map;

    monitor-enter v1

    .line 1157
    :try_start_0
    iget-object v0, p0, LX/009;->mInstanceCustomParameters:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    monitor-exit v1

    goto :goto_0

    .line 1158
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1159
    :cond_1
    invoke-virtual {p0, p1}, LX/009;->removeCustomData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final putLazyCustomData(Ljava/lang/String;LX/00T;)V
    .locals 2

    .prologue
    .line 1149
    iget-object v1, p0, LX/009;->mInstanceLazyCustomParameters:Ljava/util/Map;

    monitor-enter v1

    .line 1150
    :try_start_0
    iget-object v0, p0, LX/009;->mInstanceLazyCustomParameters:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final registerActivity(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1146
    if-eqz p1, :cond_0

    .line 1147
    iget-object v0, p0, LX/009;->mActivityLogger:LX/01a;

    invoke-virtual {v0, p1}, LX/01a;->append(Ljava/lang/String;)V

    .line 1148
    :cond_0
    return-void
.end method

.method public final removeAllReportSenders()V
    .locals 2

    .prologue
    .line 1143
    iget-object v1, p0, LX/009;->mReportSenders:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1144
    :try_start_0
    iget-object v0, p0, LX/009;->mReportSenders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1145
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final removeCustomData(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1137
    if-nez p1, :cond_0

    .line 1138
    const/4 v0, 0x0

    .line 1139
    :goto_0
    return-object v0

    .line 1140
    :cond_0
    iget-object v1, p0, LX/009;->mInstanceCustomParameters:Ljava/util/Map;

    monitor-enter v1

    .line 1141
    :try_start_0
    iget-object v0, p0, LX/009;->mInstanceCustomParameters:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    monitor-exit v1

    goto :goto_0

    .line 1142
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final removeLazyCustomData(Ljava/lang/String;)LX/00T;
    .locals 2

    .prologue
    .line 1131
    if-nez p1, :cond_0

    .line 1132
    const/4 v0, 0x0

    .line 1133
    :goto_0
    return-object v0

    .line 1134
    :cond_0
    iget-object v1, p0, LX/009;->mInstanceLazyCustomParameters:Ljava/util/Map;

    monitor-enter v1

    .line 1135
    :try_start_0
    iget-object v0, p0, LX/009;->mInstanceLazyCustomParameters:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00T;

    monitor-exit v1

    goto :goto_0

    .line 1136
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final varargs roughlyCountReportsOfType([LX/01V;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1075
    iget-object v1, p0, LX/009;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 1076
    sget-object v1, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Trying to get ACRA reports but ACRA is not initialized."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1077
    :cond_0
    return v0

    .line 1078
    :cond_1
    array-length v3, p1

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, p1, v1

    .line 1079
    iget-object v4, p0, LX/009;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, LX/01V;->getCrashReports(LX/01V;Landroid/content/Context;)LX/01h;

    move-result-object v2

    .line 1080
    iget-object v4, v2, LX/01h;->mDescriptors:[LX/01g;

    array-length v4, v4

    move v2, v4

    .line 1081
    add-int/2addr v2, v0

    .line 1082
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0
.end method

.method public final declared-synchronized setANRDataProvider(LX/09r;)V
    .locals 1

    .prologue
    .line 1126
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/009;->mANRDataProvider:LX/09r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1127
    monitor-exit p0

    return-void

    .line 1128
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final setCrashReportedObserver(LX/00P;)V
    .locals 1

    .prologue
    .line 1124
    iget-object v0, p0, LX/009;->mCrashReportedObserver:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 1125
    return-void
.end method

.method public final setLogBridge(LX/03d;)V
    .locals 0
    .param p1    # LX/03d;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1122
    iput-object p1, p0, LX/009;->mLogBridge:LX/03d;

    .line 1123
    return-void
.end method

.method public final setMaxReportSize(J)V
    .locals 1

    .prologue
    .line 1120
    iput-wide p1, p0, LX/009;->mMaxReportSize:J

    .line 1121
    return-void
.end method

.method public final setReportProxy(Ljava/net/Proxy;)V
    .locals 4

    .prologue
    .line 1113
    iget-object v1, p0, LX/009;->mReportSenders:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1114
    :try_start_0
    iget-object v0, p0, LX/009;->mReportSenders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00M;

    .line 1115
    instance-of v3, v0, LX/00M;

    if-eqz v3, :cond_0

    .line 1116
    check-cast v0, LX/00M;

    .line 1117
    iput-object p1, v0, LX/00M;->mProxy:Ljava/net/Proxy;

    .line 1118
    goto :goto_0

    .line 1119
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final setReportSender(LX/00M;)V
    .locals 2

    .prologue
    .line 1109
    iget-object v1, p0, LX/009;->mReportSenders:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1110
    :try_start_0
    invoke-virtual {p0}, LX/009;->removeAllReportSenders()V

    .line 1111
    invoke-virtual {p0, p1}, LX/009;->addReportSender(LX/00M;)V

    .line 1112
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final setUserId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1107
    iput-object p1, p0, LX/009;->mUserId:Ljava/lang/String;

    .line 1108
    return-void
.end method

.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1090
    sget-object v1, LX/009;->UNCAUGHT_EXCEPTION_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1091
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, LX/009;->uncaughtExceptionImpl(Ljava/lang/Thread;Ljava/lang/Throwable;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1092
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/009;->mChainedHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 1093
    if-eqz v0, :cond_0

    .line 1094
    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1095
    :cond_0
    :goto_1
    :try_start_2
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1096
    :goto_2
    const/16 v0, 0xa

    :try_start_3
    invoke-static {v0}, Ljava/lang/System;->exit(I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1097
    :goto_3
    goto/32 :goto_3

    .line 1098
    :catch_0
    move-exception v0

    .line 1099
    :try_start_4
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1100
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 1101
    :catch_1
    move-exception v0

    .line 1102
    :try_start_5
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1103
    :catch_2
    move-exception v0

    .line 1104
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1105
    :catch_3
    move-exception v0

    .line 1106
    invoke-direct {p0, v0}, LX/009;->tryLogInternalError(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3
.end method

.method public final writeReportToStream(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    .locals 7

    .prologue
    .line 1083
    new-instance v4, LX/01l;

    invoke-direct {v4}, LX/01l;-><init>()V

    .line 1084
    invoke-static {p2}, LX/01l;->getWriter(Ljava/io/OutputStream;)Ljava/io/Writer;

    move-result-object v5

    .line 1085
    invoke-static {p1}, LX/009;->throwableToString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    .line 1086
    invoke-static {}, LX/01c;->generateReportUuid()Ljava/util/UUID;

    move-result-object v0

    .line 1087
    const-string v1, "REPORT_ID"

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0, v5}, LX/01l;->put(Ljava/lang/String;Ljava/lang/String;Ljava/io/Writer;)Ljava/lang/String;

    .line 1088
    iget-object v1, p0, LX/009;->mConfig:LX/00K;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v3, p1

    invoke-static/range {v0 .. v6}, LX/04R;->gatherCrashData(LX/009;LX/00K;Ljava/lang/String;Ljava/lang/Throwable;LX/01l;Ljava/io/Writer;Ljava/util/Map;)V

    .line 1089
    return-void
.end method
