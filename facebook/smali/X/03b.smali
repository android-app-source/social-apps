.class public LX/03b;
.super Ljava/lang/ref/WeakReference;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/WeakReference",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:LX/03b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/03b",
            "<TT;>;"
        }
    .end annotation
.end field

.field public b:LX/03b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/03b",
            "<TT;>;"
        }
    .end annotation
.end field

.field public c:LX/03b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/03b",
            "<TT;>;"
        }
    .end annotation
.end field

.field public d:LX/03b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/03b",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 10568
    invoke-direct {p0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 10569
    iput-object p0, p0, LX/03b;->d:LX/03b;

    iput-object p0, p0, LX/03b;->c:LX/03b;

    iput-object p0, p0, LX/03b;->b:LX/03b;

    iput-object p0, p0, LX/03b;->a:LX/03b;

    .line 10570
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/ref/ReferenceQueue",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 10565
    invoke-direct {p0, p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 10566
    iput-object p0, p0, LX/03b;->d:LX/03b;

    iput-object p0, p0, LX/03b;->c:LX/03b;

    iput-object p0, p0, LX/03b;->b:LX/03b;

    iput-object p0, p0, LX/03b;->a:LX/03b;

    .line 10567
    return-void
.end method

.method public static a()LX/03b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LX/03b",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 10564
    new-instance v0, LX/03b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/03b;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(LX/03b;Ljava/lang/Object;)LX/03b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<O:",
            "Ljava/lang/Object;",
            ">(",
            "LX/03b",
            "<TO;>;TO;)",
            "LX/03b",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 10559
    iget-object v0, p0, LX/03b;->d:LX/03b;

    .line 10560
    :goto_0
    if-eq v0, p0, :cond_0

    .line 10561
    invoke-virtual {v0}, LX/03b;->get()Ljava/lang/Object;

    move-result-object v1

    if-eq v1, p1, :cond_0

    .line 10562
    iget-object v0, v0, LX/03b;->d:LX/03b;

    goto :goto_0

    .line 10563
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(LX/03b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/03b",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 10554
    iget-object v0, p0, LX/03b;->b:LX/03b;

    iput-object p1, v0, LX/03b;->a:LX/03b;

    .line 10555
    iget-object v0, p0, LX/03b;->b:LX/03b;

    iput-object v0, p1, LX/03b;->b:LX/03b;

    .line 10556
    iput-object p0, p1, LX/03b;->a:LX/03b;

    .line 10557
    iput-object p1, p0, LX/03b;->b:LX/03b;

    .line 10558
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 10540
    iget-object v0, p0, LX/03b;->a:LX/03b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/03b;->b:LX/03b;

    if-eqz v0, :cond_0

    .line 10541
    iget-object v0, p0, LX/03b;->a:LX/03b;

    iget-object v1, p0, LX/03b;->b:LX/03b;

    iput-object v1, v0, LX/03b;->b:LX/03b;

    .line 10542
    iget-object v0, p0, LX/03b;->b:LX/03b;

    iget-object v1, p0, LX/03b;->a:LX/03b;

    iput-object v1, v0, LX/03b;->a:LX/03b;

    .line 10543
    iput-object p0, p0, LX/03b;->b:LX/03b;

    iput-object p0, p0, LX/03b;->a:LX/03b;

    .line 10544
    :cond_0
    iget-object v0, p0, LX/03b;->c:LX/03b;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/03b;->d:LX/03b;

    if-eqz v0, :cond_1

    .line 10545
    iget-object v0, p0, LX/03b;->c:LX/03b;

    iget-object v1, p0, LX/03b;->d:LX/03b;

    iput-object v1, v0, LX/03b;->d:LX/03b;

    .line 10546
    iget-object v0, p0, LX/03b;->d:LX/03b;

    iget-object v1, p0, LX/03b;->c:LX/03b;

    iput-object v1, v0, LX/03b;->c:LX/03b;

    .line 10547
    iput-object p0, p0, LX/03b;->d:LX/03b;

    iput-object p0, p0, LX/03b;->c:LX/03b;

    .line 10548
    :cond_1
    return-void
.end method

.method public final b(LX/03b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/03b",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 10549
    iget-object v0, p0, LX/03b;->d:LX/03b;

    iput-object p1, v0, LX/03b;->c:LX/03b;

    .line 10550
    iget-object v0, p0, LX/03b;->d:LX/03b;

    iput-object v0, p1, LX/03b;->d:LX/03b;

    .line 10551
    iput-object p0, p1, LX/03b;->c:LX/03b;

    .line 10552
    iput-object p1, p0, LX/03b;->d:LX/03b;

    .line 10553
    return-void
.end method
