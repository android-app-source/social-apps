.class public LX/08X;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/08X;


# instance fields
.field private final b:LX/0Xl;

.field public final c:LX/04v;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21268
    const-class v0, LX/08X;

    sput-object v0, LX/08X;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Xl;Landroid/content/Context;)V
    .locals 1
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/GlobalFbBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 21264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21265
    iput-object p1, p0, LX/08X;->b:LX/0Xl;

    .line 21266
    new-instance v0, LX/04v;

    invoke-direct {v0, p2}, LX/04v;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/08X;->c:LX/04v;

    .line 21267
    return-void
.end method

.method public static a(LX/0QB;)LX/08X;
    .locals 3

    .prologue
    .line 21244
    sget-object v0, LX/08X;->d:LX/08X;

    if-nez v0, :cond_1

    .line 21245
    const-class v1, LX/08X;

    monitor-enter v1

    .line 21246
    :try_start_0
    sget-object v0, LX/08X;->d:LX/08X;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 21247
    if-eqz v2, :cond_0

    .line 21248
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/08X;->b(LX/0QB;)LX/08X;

    move-result-object v0

    sput-object v0, LX/08X;->d:LX/08X;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 21249
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 21250
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 21251
    :cond_1
    sget-object v0, LX/08X;->d:LX/08X;

    return-object v0

    .line 21252
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 21253
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/08X;
    .locals 3

    .prologue
    .line 21262
    new-instance v2, LX/08X;

    invoke-static {p0}, LX/0aW;->a(LX/0QB;)LX/0aW;

    move-result-object v0

    check-cast v0, LX/0Xl;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v1}, LX/08X;-><init>(LX/0Xl;Landroid/content/Context;)V

    .line 21263
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 21254
    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    .line 21255
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 21256
    const-string v0, "android.intent.action.PACKAGE_FULLY_REMOVED"

    .line 21257
    :cond_0
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 21258
    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 21259
    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 21260
    iget-object v2, p0, LX/08X;->b:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    new-instance v3, LX/07q;

    invoke-direct {v3, p0, p1}, LX/07q;-><init>(LX/08X;Ljava/lang/Runnable;)V

    invoke-interface {v2, v0, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/content/IntentFilter;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 21261
    return-void
.end method
