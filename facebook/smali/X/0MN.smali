.class public final LX/0MN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:[J

.field public c:[I

.field private d:[I

.field private e:[J

.field private f:[[B

.field public g:I

.field public h:I

.field public i:I

.field public j:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45830
    const/16 v0, 0x3e8

    iput v0, p0, LX/0MN;->a:I

    .line 45831
    iget v0, p0, LX/0MN;->a:I

    new-array v0, v0, [J

    iput-object v0, p0, LX/0MN;->b:[J

    .line 45832
    iget v0, p0, LX/0MN;->a:I

    new-array v0, v0, [J

    iput-object v0, p0, LX/0MN;->e:[J

    .line 45833
    iget v0, p0, LX/0MN;->a:I

    new-array v0, v0, [I

    iput-object v0, p0, LX/0MN;->d:[I

    .line 45834
    iget v0, p0, LX/0MN;->a:I

    new-array v0, v0, [I

    iput-object v0, p0, LX/0MN;->c:[I

    .line 45835
    iget v0, p0, LX/0MN;->a:I

    new-array v0, v0, [[B

    iput-object v0, p0, LX/0MN;->f:[[B

    .line 45836
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(J)J
    .locals 9

    .prologue
    const/4 v5, -0x1

    const-wide/16 v0, -0x1

    .line 45810
    monitor-enter p0

    :try_start_0
    iget v2, p0, LX/0MN;->g:I

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/0MN;->e:[J

    iget v3, p0, LX/0MN;->i:I

    aget-wide v2, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v2, p1, v2

    if-gez v2, :cond_1

    .line 45811
    :cond_0
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 45812
    :cond_1
    :try_start_1
    iget v2, p0, LX/0MN;->j:I

    if-nez v2, :cond_3

    iget v2, p0, LX/0MN;->a:I

    :goto_1
    add-int/lit8 v2, v2, -0x1

    .line 45813
    iget-object v3, p0, LX/0MN;->e:[J

    aget-wide v2, v3, v2

    .line 45814
    cmp-long v2, p1, v2

    if-gtz v2, :cond_0

    .line 45815
    const/4 v3, 0x0

    .line 45816
    iget v2, p0, LX/0MN;->i:I

    move v4, v2

    move v2, v5

    .line 45817
    :goto_2
    iget v6, p0, LX/0MN;->j:I

    if-eq v4, v6, :cond_4

    .line 45818
    iget-object v6, p0, LX/0MN;->e:[J

    aget-wide v6, v6, v4

    cmp-long v6, v6, p1

    if-gtz v6, :cond_4

    .line 45819
    iget-object v6, p0, LX/0MN;->d:[I

    aget v6, v6, v4

    and-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_2

    move v2, v3

    .line 45820
    :cond_2
    add-int/lit8 v4, v4, 0x1

    iget v6, p0, LX/0MN;->a:I

    rem-int/2addr v4, v6

    .line 45821
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 45822
    :cond_3
    iget v2, p0, LX/0MN;->j:I

    goto :goto_1

    .line 45823
    :cond_4
    if-eq v2, v5, :cond_0

    .line 45824
    iget v0, p0, LX/0MN;->g:I

    sub-int/2addr v0, v2

    iput v0, p0, LX/0MN;->g:I

    .line 45825
    iget v0, p0, LX/0MN;->i:I

    add-int/2addr v0, v2

    iget v1, p0, LX/0MN;->a:I

    rem-int/2addr v0, v1

    iput v0, p0, LX/0MN;->i:I

    .line 45826
    iget v0, p0, LX/0MN;->h:I

    add-int/2addr v0, v2

    iput v0, p0, LX/0MN;->h:I

    .line 45827
    iget-object v0, p0, LX/0MN;->b:[J

    iget v1, p0, LX/0MN;->i:I

    aget-wide v0, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 45828
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JIJI[B)V
    .locals 10

    .prologue
    .line 45771
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0MN;->e:[J

    iget v1, p0, LX/0MN;->j:I

    aput-wide p1, v0, v1

    .line 45772
    iget-object v0, p0, LX/0MN;->b:[J

    iget v1, p0, LX/0MN;->j:I

    aput-wide p4, v0, v1

    .line 45773
    iget-object v0, p0, LX/0MN;->c:[I

    iget v1, p0, LX/0MN;->j:I

    aput p6, v0, v1

    .line 45774
    iget-object v0, p0, LX/0MN;->d:[I

    iget v1, p0, LX/0MN;->j:I

    aput p3, v0, v1

    .line 45775
    iget-object v0, p0, LX/0MN;->f:[[B

    iget v1, p0, LX/0MN;->j:I

    aput-object p7, v0, v1

    .line 45776
    iget v0, p0, LX/0MN;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0MN;->g:I

    .line 45777
    iget v0, p0, LX/0MN;->g:I

    iget v1, p0, LX/0MN;->a:I

    if-ne v0, v1, :cond_1

    .line 45778
    iget v0, p0, LX/0MN;->a:I

    add-int/lit16 v0, v0, 0x3e8

    .line 45779
    new-array v1, v0, [J

    .line 45780
    new-array v2, v0, [J

    .line 45781
    new-array v3, v0, [I

    .line 45782
    new-array v4, v0, [I

    .line 45783
    new-array v5, v0, [[B

    .line 45784
    iget v6, p0, LX/0MN;->a:I

    iget v7, p0, LX/0MN;->i:I

    sub-int/2addr v6, v7

    .line 45785
    iget-object v7, p0, LX/0MN;->b:[J

    iget v8, p0, LX/0MN;->i:I

    const/4 v9, 0x0

    invoke-static {v7, v8, v1, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45786
    iget-object v7, p0, LX/0MN;->e:[J

    iget v8, p0, LX/0MN;->i:I

    const/4 v9, 0x0

    invoke-static {v7, v8, v2, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45787
    iget-object v7, p0, LX/0MN;->d:[I

    iget v8, p0, LX/0MN;->i:I

    const/4 v9, 0x0

    invoke-static {v7, v8, v3, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45788
    iget-object v7, p0, LX/0MN;->c:[I

    iget v8, p0, LX/0MN;->i:I

    const/4 v9, 0x0

    invoke-static {v7, v8, v4, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45789
    iget-object v7, p0, LX/0MN;->f:[[B

    iget v8, p0, LX/0MN;->i:I

    const/4 v9, 0x0

    invoke-static {v7, v8, v5, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45790
    iget v7, p0, LX/0MN;->i:I

    .line 45791
    iget-object v8, p0, LX/0MN;->b:[J

    const/4 v9, 0x0

    invoke-static {v8, v9, v1, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45792
    iget-object v8, p0, LX/0MN;->e:[J

    const/4 v9, 0x0

    invoke-static {v8, v9, v2, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45793
    iget-object v8, p0, LX/0MN;->d:[I

    const/4 v9, 0x0

    invoke-static {v8, v9, v3, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45794
    iget-object v8, p0, LX/0MN;->c:[I

    const/4 v9, 0x0

    invoke-static {v8, v9, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45795
    iget-object v8, p0, LX/0MN;->f:[[B

    const/4 v9, 0x0

    invoke-static {v8, v9, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45796
    iput-object v1, p0, LX/0MN;->b:[J

    .line 45797
    iput-object v2, p0, LX/0MN;->e:[J

    .line 45798
    iput-object v3, p0, LX/0MN;->d:[I

    .line 45799
    iput-object v4, p0, LX/0MN;->c:[I

    .line 45800
    iput-object v5, p0, LX/0MN;->f:[[B

    .line 45801
    const/4 v1, 0x0

    iput v1, p0, LX/0MN;->i:I

    .line 45802
    iget v1, p0, LX/0MN;->a:I

    iput v1, p0, LX/0MN;->j:I

    .line 45803
    iget v1, p0, LX/0MN;->a:I

    iput v1, p0, LX/0MN;->g:I

    .line 45804
    iput v0, p0, LX/0MN;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45805
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 45806
    :cond_1
    :try_start_1
    iget v0, p0, LX/0MN;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0MN;->j:I

    .line 45807
    iget v0, p0, LX/0MN;->j:I

    iget v1, p0, LX/0MN;->a:I

    if-ne v0, v1, :cond_0

    .line 45808
    const/4 v0, 0x0

    iput v0, p0, LX/0MN;->j:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 45809
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0L7;LX/0MO;)Z
    .locals 2

    .prologue
    .line 45753
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0MN;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 45754
    const/4 v0, 0x0

    .line 45755
    :goto_0
    monitor-exit p0

    return v0

    .line 45756
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0MN;->e:[J

    iget v1, p0, LX/0MN;->i:I

    aget-wide v0, v0, v1

    iput-wide v0, p1, LX/0L7;->e:J

    .line 45757
    iget-object v0, p0, LX/0MN;->c:[I

    iget v1, p0, LX/0MN;->i:I

    aget v0, v0, v1

    iput v0, p1, LX/0L7;->c:I

    .line 45758
    iget-object v0, p0, LX/0MN;->d:[I

    iget v1, p0, LX/0MN;->i:I

    aget v0, v0, v1

    iput v0, p1, LX/0L7;->d:I

    .line 45759
    iget-object v0, p0, LX/0MN;->b:[J

    iget v1, p0, LX/0MN;->i:I

    aget-wide v0, v0, v1

    iput-wide v0, p2, LX/0MO;->a:J

    .line 45760
    iget-object v0, p0, LX/0MN;->f:[[B

    iget v1, p0, LX/0MN;->i:I

    aget-object v0, v0, v1

    iput-object v0, p2, LX/0MO;->b:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 45761
    const/4 v0, 0x1

    goto :goto_0

    .line 45762
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 45770
    iget v0, p0, LX/0MN;->h:I

    iget v1, p0, LX/0MN;->g:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final declared-synchronized d()J
    .locals 4

    .prologue
    .line 45763
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0MN;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0MN;->g:I

    .line 45764
    iget v0, p0, LX/0MN;->i:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/0MN;->i:I

    .line 45765
    iget v1, p0, LX/0MN;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0MN;->h:I

    .line 45766
    iget v1, p0, LX/0MN;->i:I

    iget v2, p0, LX/0MN;->a:I

    if-ne v1, v2, :cond_0

    .line 45767
    const/4 v1, 0x0

    iput v1, p0, LX/0MN;->i:I

    .line 45768
    :cond_0
    iget v1, p0, LX/0MN;->g:I

    if-lez v1, :cond_1

    iget-object v0, p0, LX/0MN;->b:[J

    iget v1, p0, LX/0MN;->i:I

    aget-wide v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_1
    :try_start_1
    iget-object v1, p0, LX/0MN;->c:[I

    aget v1, v1, v0

    int-to-long v2, v1

    iget-object v1, p0, LX/0MN;->b:[J

    aget-wide v0, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-long/2addr v0, v2

    goto :goto_0

    .line 45769
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
