.class public Lcom/facebook/cpuprofiler/CPUProfiler;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static volatile a:Z

.field private static volatile b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10050
    sput-boolean v0, Lcom/facebook/cpuprofiler/CPUProfiler;->a:Z

    .line 10051
    sput v0, Lcom/facebook/cpuprofiler/CPUProfiler;->b:I

    .line 10052
    const-string v0, "loom"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 10053
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 10098
    sget-boolean v0, Lcom/facebook/cpuprofiler/CPUProfiler;->a:Z

    if-nez v0, :cond_0

    .line 10099
    :goto_0
    return-void

    .line 10100
    :cond_0
    invoke-static {}, Lcom/facebook/cpuprofiler/CPUProfiler;->nativeCollectTrace()V

    goto :goto_0
.end method

.method public static declared-synchronized a(I)Z
    .locals 2

    .prologue
    .line 10097
    const-class v1, Lcom/facebook/cpuprofiler/CPUProfiler;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/facebook/cpuprofiler/CPUProfiler;->a:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/cpuprofiler/CPUProfiler;->nativeStartProfiling(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    .line 10085
    const-class v1, Lcom/facebook/cpuprofiler/CPUProfiler;

    monitor-enter v1

    :try_start_0
    check-cast p0, Landroid/content/ContextWrapper;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    .line 10086
    sget-boolean v2, Lcom/facebook/cpuprofiler/CPUProfiler;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    .line 10087
    const/4 v0, 0x1

    .line 10088
    :goto_0
    monitor-exit v1

    return v0

    .line 10089
    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/facebook/cpuprofiler/CPUProfiler;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 10090
    if-nez v2, :cond_1

    .line 10091
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "No metadata found"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 10092
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 10093
    :cond_1
    :try_start_2
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 10094
    invoke-static {v0}, Lcom/facebook/cpuprofiler/CPUProfiler;->b(Landroid/content/Context;)I

    move-result v4

    .line 10095
    sput v4, Lcom/facebook/cpuprofiler/CPUProfiler;->b:I

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/cpuprofiler/CPUProfiler;->nativeInitialize(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 10096
    sput-boolean v0, Lcom/facebook/cpuprofiler/CPUProfiler;->a:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)I
    .locals 6

    .prologue
    const/16 v5, 0x15

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 10077
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v2, v5, :cond_2

    .line 10078
    :goto_0
    const-string v1, "os.arch"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 10079
    if-eqz v1, :cond_0

    const-string v2, "arm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v5, :cond_1

    .line 10080
    :cond_0
    or-int/lit8 v0, v0, 0x4

    .line 10081
    :cond_1
    return v0

    .line 10082
    :cond_2
    invoke-static {p0}, Lcom/facebook/cpuprofiler/ArtCompatibility;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 10083
    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_3
    move v0, v2

    :goto_1
    packed-switch v0, :pswitch_data_0

    :cond_4
    move v0, v1

    goto :goto_0

    :sswitch_0
    const-string v0, "6.0"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string v4, "6.0.1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    .line 10084
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xd078 -> :sswitch_0
        0x30e983b -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static declared-synchronized b()V
    .locals 2

    .prologue
    .line 10073
    const-class v1, Lcom/facebook/cpuprofiler/CPUProfiler;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/facebook/cpuprofiler/CPUProfiler;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 10074
    :goto_0
    monitor-exit v1

    return-void

    .line 10075
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/facebook/cpuprofiler/CPUProfiler;->nativeStopProfiling()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 10076
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 10054
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v2, "secondary-program-dex-jars/metadata.txt"

    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v3

    .line 10055
    :try_start_1
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v4, "metadatacopy.txt"

    invoke-direct {v0, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 10056
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 10057
    const/16 v2, 0x400

    :try_start_2
    new-array v2, v2, [B

    .line 10058
    :goto_0
    invoke-virtual {v3, v2}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    .line 10059
    const/4 v6, 0x0

    invoke-virtual {v4, v2, v6, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    goto :goto_0

    .line 10060
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 10061
    :catchall_0
    move-exception v2

    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    :goto_1
    if-eqz v2, :cond_3

    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :goto_2
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 10062
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 10063
    :catchall_1
    move-exception v2

    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    :goto_3
    if-eqz v3, :cond_0

    if-eqz v2, :cond_4

    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    :cond_0
    :goto_4
    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    .line 10064
    :catch_2
    move-object v0, v1

    :cond_1
    :goto_5
    return-object v0

    .line 10065
    :cond_2
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 10066
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    move-result-object v0

    .line 10067
    if-eqz v3, :cond_1

    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_5

    .line 10068
    :catch_3
    move-exception v4

    :try_start_b
    invoke-static {v2, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 10069
    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_3

    .line 10070
    :cond_3
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto :goto_2

    .line 10071
    :catch_4
    move-exception v3

    :try_start_c
    invoke-static {v2, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2

    goto :goto_4

    .line 10072
    :catchall_3
    move-exception v0

    move-object v2, v1

    goto :goto_1
.end method

.method private static native nativeCollectTrace()V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private static native nativeInitialize(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private static native nativeStartProfiling(I)Z
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private static native nativeStopProfiling()V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method
