.class public final Lcom/facebook/common/udppriming/client/ColdStartPrimingInformation$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/00a;


# direct methods
.method public constructor <init>(LX/00a;)V
    .locals 0

    .prologue
    .line 14944
    iput-object p1, p0, Lcom/facebook/common/udppriming/client/ColdStartPrimingInformation$1;->a:LX/00a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 14945
    iget-object v0, p0, Lcom/facebook/common/udppriming/client/ColdStartPrimingInformation$1;->a:LX/00a;

    iget-object v1, p0, Lcom/facebook/common/udppriming/client/ColdStartPrimingInformation$1;->a:LX/00a;

    iget-wide v2, v1, LX/00a;->g:J

    .line 14946
    iget-object v1, v0, LX/00a;->c:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    .line 14947
    iget-object v1, v0, LX/00a;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 14948
    const-string v4, "COLD_START_PRIME_INFO/LAST_UDP_PRIMING_TIME"

    invoke-interface {v1, v4, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 14949
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 14950
    :cond_0
    return-void
.end method
