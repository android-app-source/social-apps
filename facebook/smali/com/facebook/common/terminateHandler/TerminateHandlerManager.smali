.class public Lcom/facebook/common/terminateHandler/TerminateHandlerManager;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2744
    const-string v0, "terminate_handler_manager"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 2745
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2739
    const-string v0, "terminate_handler_flags_store"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2740
    const-string v1, "android_enable_terminate_handler"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2741
    if-eqz v0, :cond_0

    .line 2742
    invoke-static {}, Lcom/facebook/common/terminateHandler/TerminateHandlerManager;->nativeInstallTerminateHandler()V

    .line 2743
    :cond_0
    return-void
.end method

.method private static native nativeInstallTerminateHandler()V
.end method
