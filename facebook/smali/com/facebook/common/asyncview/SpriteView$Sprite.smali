.class public final Lcom/facebook/common/asyncview/SpriteView$Sprite;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final a:Landroid/graphics/Bitmap;

.field public final b:F

.field public final c:F

.field public d:Lcom/facebook/common/asyncview/SpriteView;

.field public e:F

.field public f:F

.field public g:F

.field public h:F

.field public i:F

.field public j:F

.field public k:F

.field public l:F

.field public m:F

.field public n:F

.field public o:F

.field public p:Z

.field public q:Z

.field public r:I

.field public s:I

.field public t:[F

.field public u:Landroid/graphics/Matrix;

.field public v:Landroid/graphics/Paint;

.field public w:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;FF)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 7457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7458
    iput v1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->e:F

    .line 7459
    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->f:F

    .line 7460
    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->g:F

    .line 7461
    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->h:F

    .line 7462
    iput v1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->i:F

    .line 7463
    iput v1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->j:F

    .line 7464
    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->k:F

    .line 7465
    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->l:F

    .line 7466
    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->m:F

    .line 7467
    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->n:F

    .line 7468
    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->o:F

    .line 7469
    if-nez p1, :cond_0

    .line 7470
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bitmap cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7471
    :cond_0
    iput-object p1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a:Landroid/graphics/Bitmap;

    .line 7472
    iput p2, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->b:F

    .line 7473
    iput p3, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->c:F

    .line 7474
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 7454
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->d:Lcom/facebook/common/asyncview/SpriteView;

    if-eqz v0, :cond_0

    .line 7455
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->d:Lcom/facebook/common/asyncview/SpriteView;

    invoke-virtual {v0}, LX/02H;->f()V

    .line 7456
    :cond_0
    return-void
.end method


# virtual methods
.method public setAlpha(F)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 7449
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->e:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 7450
    iput p1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->e:F

    .line 7451
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->q:Z

    .line 7452
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a()V

    .line 7453
    :cond_0
    return-void
.end method

.method public setPivotX(F)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 7444
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->n:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 7445
    iput p1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->n:F

    .line 7446
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 7447
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a()V

    .line 7448
    :cond_0
    return-void
.end method

.method public setPivotY(F)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 7439
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->o:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 7440
    iput p1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->o:F

    .line 7441
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 7442
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a()V

    .line 7443
    :cond_0
    return-void
.end method

.method public setRotation(F)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 7434
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->m:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 7435
    iput p1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->m:F

    .line 7436
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 7437
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a()V

    .line 7438
    :cond_0
    return-void
.end method

.method public setRotationX(F)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 7429
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->k:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 7430
    iput p1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->k:F

    .line 7431
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 7432
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a()V

    .line 7433
    :cond_0
    return-void
.end method

.method public setRotationY(F)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 7424
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->l:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 7425
    iput p1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->l:F

    .line 7426
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 7427
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a()V

    .line 7428
    :cond_0
    return-void
.end method

.method public setScaleX(F)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 7419
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->i:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 7420
    iput p1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->i:F

    .line 7421
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 7422
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a()V

    .line 7423
    :cond_0
    return-void
.end method

.method public setScaleY(F)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 7414
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->j:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 7415
    iput p1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->j:F

    .line 7416
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 7417
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a()V

    .line 7418
    :cond_0
    return-void
.end method

.method public setTranslationX(F)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 7409
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->f:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 7410
    iput p1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->f:F

    .line 7411
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 7412
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a()V

    .line 7413
    :cond_0
    return-void
.end method

.method public setTranslationY(F)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 7404
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->g:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 7405
    iput p1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->g:F

    .line 7406
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 7407
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a()V

    .line 7408
    :cond_0
    return-void
.end method

.method public setTranslationZ(F)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 7399
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->h:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 7400
    iput p1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->h:F

    .line 7401
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 7402
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a()V

    .line 7403
    :cond_0
    return-void
.end method
