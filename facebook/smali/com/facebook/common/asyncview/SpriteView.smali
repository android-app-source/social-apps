.class public Lcom/facebook/common/asyncview/SpriteView;
.super LX/02H;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final a:[I

.field public final b:[F

.field public final c:[F

.field public final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/common/asyncview/SpriteView$Sprite;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:I

.field private g:B

.field private h:Landroid/graphics/Matrix;

.field private i:Z

.field private j:LX/0FC;

.field public k:Ljavax/microedition/khronos/egl/EGL10;

.field public l:Ljavax/microedition/khronos/egl/EGLDisplay;

.field private m:Ljavax/microedition/khronos/egl/EGLConfig;

.field private n:Ljavax/microedition/khronos/egl/EGLContext;

.field public o:Ljavax/microedition/khronos/egl/EGLSurface;

.field private p:Ljavax/microedition/khronos/opengles/GL10;

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field public u:[F

.field private v:I

.field private w:I

.field private x:I

.field private y:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 6879
    invoke-direct {p0, p1}, LX/02H;-><init>(Landroid/content/Context;)V

    .line 6880
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->d:Ljava/util/ArrayList;

    .line 6881
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/facebook/common/asyncview/SpriteView;->g:B

    .line 6882
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->a:[I

    .line 6883
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->b:[F

    .line 6884
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->c:[F

    .line 6885
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->u:[F

    .line 6886
    return-void
.end method

.method private a(I[F)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6887
    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->a:[I

    .line 6888
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 6889
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6890
    const/4 v2, 0x0

    aget v1, v0, v2

    .line 6891
    invoke-static {p1, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 6892
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6893
    array-length v0, p2

    mul-int/lit8 v0, v0, 0x4

    invoke-static {p2}, Ljava/nio/FloatBuffer;->wrap([F)Ljava/nio/FloatBuffer;

    move-result-object v2

    const v3, 0x88e4

    invoke-static {p1, v0, v2, v3}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 6894
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6895
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 6896
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6897
    return v1

    .line 6898
    :catchall_0
    move-exception v0

    .line 6899
    invoke-direct {p0, v1}, Lcom/facebook/common/asyncview/SpriteView;->a(I)V

    throw v0
.end method

.method private a(Landroid/graphics/Bitmap;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 6900
    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->a:[I

    .line 6901
    iget-object v2, p0, Lcom/facebook/common/asyncview/SpriteView;->p:Ljavax/microedition/khronos/opengles/GL10;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {v2, v3, v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 6902
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6903
    const/4 v2, 0x0

    aget v1, v0, v2

    .line 6904
    const/16 v0, 0xde1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 6905
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6906
    const/16 v0, 0xde1

    const/16 v2, 0x2802

    const v3, 0x812f

    invoke-static {v0, v2, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 6907
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6908
    const/16 v0, 0xde1

    const/16 v2, 0x2803

    const v3, 0x812f

    invoke-static {v0, v2, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 6909
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6910
    const/16 v0, 0xde1

    const/16 v2, 0x2801

    const/16 v3, 0x2601

    invoke-static {v0, v2, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 6911
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6912
    const/16 v0, 0xde1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v2, p1, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 6913
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6914
    const/16 v0, 0xde1

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 6915
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6916
    return v1

    .line 6917
    :catchall_0
    move-exception v0

    .line 6918
    invoke-static {p0, v1}, Lcom/facebook/common/asyncview/SpriteView;->b(Lcom/facebook/common/asyncview/SpriteView;I)V

    throw v0
.end method

.method private static a(Lcom/facebook/common/asyncview/SpriteView;ILjava/lang/String;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 6919
    :try_start_0
    invoke-static {p1}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    .line 6920
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6921
    invoke-static {v1, p2}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 6922
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6923
    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 6924
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6925
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->a:[I

    .line 6926
    const v2, 0x8b81

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 6927
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6928
    const/4 v2, 0x0

    aget v0, v0, v2

    if-nez v0, :cond_0

    .line 6929
    invoke-static {v1}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v0

    .line 6930
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6931
    const-string v2, "fb-SpriteView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "shader compilation failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 6932
    new-instance v0, Landroid/opengl/GLException;

    const/16 v2, 0x501

    const-string v3, "shader compilation failed"

    invoke-direct {v0, v2, v3}, Landroid/opengl/GLException;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6933
    :catchall_0
    move-exception v0

    .line 6934
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    throw v0

    :cond_0
    return v1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 6935
    :try_start_0
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 6936
    :try_start_1
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6937
    const v0, 0x8b31

    invoke-static {p0, v0, p1}, Lcom/facebook/common/asyncview/SpriteView;->a(Lcom/facebook/common/asyncview/SpriteView;ILjava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    .line 6938
    :try_start_2
    invoke-static {v1, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 6939
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6940
    const v0, 0x8b30

    invoke-static {p0, v0, p2}, Lcom/facebook/common/asyncview/SpriteView;->a(Lcom/facebook/common/asyncview/SpriteView;ILjava/lang/String;)I

    move-result v2

    .line 6941
    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 6942
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6943
    invoke-static {v1}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 6944
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 6945
    invoke-static {v3}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 6946
    invoke-static {v2}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 6947
    return v1

    .line 6948
    :catchall_0
    move-exception v0

    move v1, v2

    move v3, v2

    :goto_0
    invoke-static {v3}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 6949
    invoke-static {v2}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 6950
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    throw v0

    .line 6951
    :catchall_1
    move-exception v0

    move v3, v2

    goto :goto_0

    :catchall_2
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljavax/microedition/khronos/egl/EGLConfig;[III)I
    .locals 2

    .prologue
    .line 6952
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1, p1, p3, p2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6953
    const/4 v0, 0x0

    aget p4, p2, v0

    .line 6954
    :cond_0
    return p4
.end method

.method private static a([F)Landroid/graphics/Matrix;
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 6969
    array-length v0, p0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 6970
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "bad GL matrix"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 6971
    :cond_0
    aget v0, p0, v8

    .line 6972
    const/4 v1, 0x5

    aget v1, p0, v1

    .line 6973
    const/16 v2, 0xa

    aget v2, p0, v2

    .line 6974
    const/16 v3, 0xc

    aget v3, p0, v3

    .line 6975
    const/16 v4, 0xd

    aget v4, p0, v4

    .line 6976
    const/16 v5, 0xe

    aget v5, p0, v5

    .line 6977
    cmpl-float v5, v5, v6

    if-nez v5, :cond_2

    cmpl-float v5, v2, v7

    if-eqz v5, :cond_1

    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v2, v2, v5

    if-nez v2, :cond_2

    :cond_1
    const/4 v2, 0x1

    aget v2, p0, v2

    cmpl-float v2, v2, v6

    if-nez v2, :cond_2

    aget v2, p0, v9

    cmpl-float v2, v2, v6

    if-nez v2, :cond_2

    aget v2, p0, v10

    cmpl-float v2, v2, v6

    if-nez v2, :cond_2

    const/4 v2, 0x6

    aget v2, p0, v2

    cmpl-float v2, v2, v6

    if-nez v2, :cond_2

    const/16 v2, 0x8

    aget v2, p0, v2

    cmpl-float v2, v2, v6

    if-nez v2, :cond_2

    const/16 v2, 0x9

    aget v2, p0, v2

    cmpl-float v2, v2, v6

    if-nez v2, :cond_2

    const/4 v2, 0x3

    aget v2, p0, v2

    cmpl-float v2, v2, v6

    if-nez v2, :cond_2

    const/4 v2, 0x7

    aget v2, p0, v2

    cmpl-float v2, v2, v6

    if-nez v2, :cond_2

    const/16 v2, 0xb

    aget v2, p0, v2

    cmpl-float v2, v2, v6

    if-nez v2, :cond_2

    const/16 v2, 0xf

    aget v2, p0, v2

    cmpl-float v2, v2, v7

    if-eqz v2, :cond_3

    .line 6978
    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "GL matrix is not 2D-compatible"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6979
    :cond_3
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 6980
    const/16 v5, 0x9

    new-array v5, v5, [F

    .line 6981
    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->getValues([F)V

    .line 6982
    aput v3, v5, v9

    .line 6983
    const/4 v3, 0x5

    aput v4, v5, v3

    .line 6984
    aput v0, v5, v8

    .line 6985
    aput v1, v5, v10

    .line 6986
    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->setValues([F)V

    .line 6987
    return-object v2
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6757
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->a:[I

    .line 6758
    aput p1, v0, v2

    .line 6759
    const/4 v1, 0x1

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    .line 6760
    return-void
.end method

.method private a(II)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/high16 v2, 0x40000000    # 2.0f

    .line 6955
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    if-eqz v0, :cond_0

    .line 6956
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "cannot start software rendeirng with EGL active"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 6957
    :cond_0
    int-to-float v0, p1

    div-float/2addr v0, v2

    .line 6958
    int-to-float v1, p2

    div-float/2addr v1, v2

    .line 6959
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 6960
    neg-float v3, v1

    invoke-virtual {v2, v0, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 6961
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 6962
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->u:[F

    invoke-static {v0}, Lcom/facebook/common/asyncview/SpriteView;->a([F)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 6963
    iput-object v2, p0, Lcom/facebook/common/asyncview/SpriteView;->h:Landroid/graphics/Matrix;

    .line 6964
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/asyncview/SpriteView$Sprite;

    .line 6965
    iput-boolean v4, v0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 6966
    iput-boolean v4, v0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->q:Z

    .line 6967
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 6968
    :cond_1
    return-void
.end method

.method private a(Landroid/view/SurfaceHolder;)V
    .locals 10

    .prologue
    .line 7035
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView;->g()V

    .line 7036
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    .line 7037
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 7038
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->k(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7039
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-ne v0, v1, :cond_0

    .line 7040
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 7041
    new-instance v0, LX/0FC;

    const-string v1, "eglGetDisplay failed"

    invoke-direct {v0, v1}, LX/0FC;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7042
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 7043
    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 7044
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->k(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7045
    new-instance v0, LX/0FC;

    const-string v1, "eglInitialize failed"

    invoke-direct {v0, v1}, LX/0FC;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7046
    :cond_1
    const/16 v0, 0xf

    new-array v2, v0, [I

    fill-array-data v2, :array_0

    .line 7047
    const/4 v0, 0x1

    new-array v5, v0, [I

    .line 7048
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 7049
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->k(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7050
    new-instance v0, LX/0FC;

    const-string v1, "eglChooseConfig failed"

    invoke-direct {v0, v1}, LX/0FC;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7051
    :cond_2
    const/4 v0, 0x0

    aget v4, v5, v0

    .line 7052
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 7053
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 7054
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->k(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7055
    new-instance v0, LX/0FC;

    const-string v1, "eglChooseConfig failed"

    invoke-direct {v0, v1}, LX/0FC;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7056
    :cond_3
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_4

    aget-object v1, v3, v0

    .line 7057
    const/16 v2, 0x3025

    const/4 v6, 0x0

    invoke-direct {p0, v1, v5, v2, v6}, Lcom/facebook/common/asyncview/SpriteView;->a(Ljavax/microedition/khronos/egl/EGLConfig;[III)I

    move-result v2

    .line 7058
    const/16 v6, 0x3026

    const/4 v7, 0x0

    invoke-direct {p0, v1, v5, v6, v7}, Lcom/facebook/common/asyncview/SpriteView;->a(Ljavax/microedition/khronos/egl/EGLConfig;[III)I

    move-result v6

    .line 7059
    if-ltz v2, :cond_5

    if-ltz v6, :cond_5

    .line 7060
    const/16 v2, 0x3024

    const/4 v6, 0x0

    invoke-direct {p0, v1, v5, v2, v6}, Lcom/facebook/common/asyncview/SpriteView;->a(Ljavax/microedition/khronos/egl/EGLConfig;[III)I

    move-result v2

    .line 7061
    const/16 v6, 0x3023

    const/4 v7, 0x0

    invoke-direct {p0, v1, v5, v6, v7}, Lcom/facebook/common/asyncview/SpriteView;->a(Ljavax/microedition/khronos/egl/EGLConfig;[III)I

    move-result v6

    .line 7062
    const/16 v7, 0x3022

    const/4 v8, 0x0

    invoke-direct {p0, v1, v5, v7, v8}, Lcom/facebook/common/asyncview/SpriteView;->a(Ljavax/microedition/khronos/egl/EGLConfig;[III)I

    move-result v7

    .line 7063
    const/16 v8, 0x3021

    const/4 v9, 0x0

    invoke-direct {p0, v1, v5, v8, v9}, Lcom/facebook/common/asyncview/SpriteView;->a(Ljavax/microedition/khronos/egl/EGLConfig;[III)I

    move-result v8

    .line 7064
    const/16 v9, 0x8

    if-ne v2, v9, :cond_5

    const/16 v2, 0x8

    if-ne v6, v2, :cond_5

    const/16 v2, 0x8

    if-ne v7, v2, :cond_5

    const/16 v2, 0x8

    if-ne v8, v2, :cond_5

    .line 7065
    iput-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->m:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 7066
    :cond_4
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->m:Ljavax/microedition/khronos/egl/EGLConfig;

    if-nez v0, :cond_6

    .line 7067
    new-instance v0, LX/0FC;

    const-string v1, "could not find suitable EGL config"

    invoke-direct {v0, v1}, LX/0FC;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7068
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7069
    :cond_6
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 7070
    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/facebook/common/asyncview/SpriteView;->m:Ljavax/microedition/khronos/egl/EGLConfig;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v1, v2, v3, v4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->n:Ljavax/microedition/khronos/egl/EGLContext;

    .line 7071
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->n:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->n:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    if-ne v0, v1, :cond_8

    .line 7072
    :cond_7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->n:Ljavax/microedition/khronos/egl/EGLContext;

    .line 7073
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->k(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7074
    new-instance v0, LX/0FC;

    const-string v1, "eglCreateContext failed"

    invoke-direct {v0, v1}, LX/0FC;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7075
    :cond_8
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->n:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v0}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL10;

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->p:Ljavax/microedition/khronos/opengles/GL10;

    .line 7076
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/facebook/common/asyncview/SpriteView;->m:Ljavax/microedition/khronos/egl/EGLConfig;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, p1, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->o:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 7077
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->o:Ljavax/microedition/khronos/egl/EGLSurface;

    if-nez v0, :cond_9

    .line 7078
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->k(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7079
    new-instance v0, LX/0FC;

    const-string v1, "eglCreateWindowSurface failed"

    invoke-direct {v0, v1}, LX/0FC;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7080
    :cond_9
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/facebook/common/asyncview/SpriteView;->o:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v3, p0, Lcom/facebook/common/asyncview/SpriteView;->o:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, Lcom/facebook/common/asyncview/SpriteView;->n:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 7081
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->k(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7082
    new-instance v0, LX/0FC;

    const-string v1, "eglMakeCurrent failed"

    invoke-direct {v0, v1}, LX/0FC;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7083
    :cond_a
    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 7084
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7085
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 7086
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7087
    const/4 v0, 0x1

    const/16 v1, 0x303

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 7088
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7089
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 7090
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7091
    const-string v0, "precision mediump float;attribute vec4 vPosition;attribute vec2 vTexCoord;uniform mat4 mModel;uniform mat4 mView;varying vec2 texCoord;void main() {  gl_Position = mView * mModel * vPosition;  texCoord = vTexCoord; }"

    const-string v1, "precision mediump float;varying vec2 texCoord;uniform float alpha;uniform sampler2D tex;void main() {  gl_FragColor = texture2D(tex, vec2(texCoord.x, 1.0-texCoord.y)) * alpha;}"

    invoke-direct {p0, v0, v1}, Lcom/facebook/common/asyncview/SpriteView;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView;->q:I

    .line 7092
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->q:I

    const-string v1, "alpha"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView;->v:I

    .line 7093
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7094
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->q:I

    const-string v1, "tex"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView;->w:I

    .line 7095
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7096
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->q:I

    const-string v1, "vPosition"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView;->r:I

    .line 7097
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7098
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->q:I

    const-string v1, "vTexCoord"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView;->x:I

    .line 7099
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7100
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->q:I

    const-string v1, "mModel"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView;->s:I

    .line 7101
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7102
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->q:I

    const-string v1, "mView"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView;->t:I

    .line 7103
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 7104
    const v0, 0x8892

    const/16 v1, 0x8

    new-array v1, v1, [F

    fill-array-data v1, :array_2

    invoke-direct {p0, v0, v1}, Lcom/facebook/common/asyncview/SpriteView;->a(I[F)I

    move-result v0

    iput v0, p0, Lcom/facebook/common/asyncview/SpriteView;->y:I

    .line 7105
    iget-object v2, p0, Lcom/facebook/common/asyncview/SpriteView;->d:Ljava/util/ArrayList;

    .line 7106
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 7107
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_b

    .line 7108
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/asyncview/SpriteView$Sprite;

    invoke-direct {p0, v0}, Lcom/facebook/common/asyncview/SpriteView;->b(Lcom/facebook/common/asyncview/SpriteView$Sprite;)V

    .line 7109
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 7110
    :cond_b
    return-void

    .line 7111
    :array_0
    .array-data 4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3025
        0x0
        0x3026
        0x0
        0x3040
        0x4
        0x3038
    .end array-data

    .line 7112
    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data

    .line 7113
    :array_2
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private static a(Lcom/facebook/common/asyncview/SpriteView$Sprite;Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 7114
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->v:Landroid/graphics/Paint;

    .line 7115
    iget-boolean v1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->q:Z

    if-eqz v1, :cond_1

    .line 7116
    if-nez v0, :cond_0

    .line 7117
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->v:Landroid/graphics/Paint;

    .line 7118
    :cond_0
    iget v1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->e:F

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 7119
    :cond_1
    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->u:Landroid/graphics/Matrix;

    .line 7120
    iget-boolean v2, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    if-eqz v2, :cond_3

    .line 7121
    if-nez v1, :cond_2

    .line 7122
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->u:Landroid/graphics/Matrix;

    .line 7123
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 7124
    iget v2, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->f:F

    iget v3, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->g:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 7125
    iget v2, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->m:F

    iget v3, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->n:F

    iget v4, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->o:F

    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    .line 7126
    iget v2, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->i:F

    iget v3, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->j:F

    iget v4, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->n:F

    iget v5, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->o:F

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Matrix;->preScale(FFFF)Z

    .line 7127
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 7128
    :cond_3
    iget-object v2, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->w:Landroid/graphics/RectF;

    .line 7129
    if-nez v2, :cond_4

    .line 7130
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->w:Landroid/graphics/RectF;

    .line 7131
    iget v3, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->b:F

    neg-float v3, v3

    div-float/2addr v3, v6

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 7132
    iget v3, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->c:F

    neg-float v3, v3

    div-float/2addr v3, v6

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 7133
    iget v3, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->b:F

    div-float/2addr v3, v6

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 7134
    iget v3, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->c:F

    div-float/2addr v3, v6

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 7135
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 7136
    :try_start_0
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 7137
    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3, v2, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7138
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 7139
    return-void

    .line 7140
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    throw v0
.end method

.method public static a([FIFFF)V
    .locals 11

    .prologue
    const v2, 0x3c8efa34

    const/4 v10, 0x0

    .line 7007
    mul-float v0, p2, v2

    .line 7008
    mul-float v1, p3, v2

    .line 7009
    mul-float/2addr v2, p4

    .line 7010
    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    .line 7011
    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v4, v4

    .line 7012
    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v5, v6

    .line 7013
    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v0, v6

    .line 7014
    float-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v1, v6

    .line 7015
    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v2, v6

    .line 7016
    mul-float v6, v0, v4

    .line 7017
    mul-float v7, v3, v4

    .line 7018
    add-int/lit8 v8, p1, 0x0

    mul-float v9, v1, v2

    aput v9, p0, v8

    .line 7019
    add-int/lit8 v8, p1, 0x1

    neg-float v9, v1

    mul-float/2addr v9, v5

    aput v9, p0, v8

    .line 7020
    add-int/lit8 v8, p1, 0x2

    aput v4, p0, v8

    .line 7021
    add-int/lit8 v4, p1, 0x3

    aput v10, p0, v4

    .line 7022
    add-int/lit8 v4, p1, 0x4

    mul-float v8, v7, v2

    mul-float v9, v0, v5

    add-float/2addr v8, v9

    aput v8, p0, v4

    .line 7023
    add-int/lit8 v4, p1, 0x5

    neg-float v7, v7

    mul-float/2addr v7, v5

    mul-float v8, v0, v2

    add-float/2addr v7, v8

    aput v7, p0, v4

    .line 7024
    add-int/lit8 v4, p1, 0x6

    neg-float v7, v3

    mul-float/2addr v7, v1

    aput v7, p0, v4

    .line 7025
    add-int/lit8 v4, p1, 0x7

    aput v10, p0, v4

    .line 7026
    add-int/lit8 v4, p1, 0x8

    neg-float v7, v6

    mul-float/2addr v7, v2

    mul-float v8, v3, v5

    add-float/2addr v7, v8

    aput v7, p0, v4

    .line 7027
    add-int/lit8 v4, p1, 0x9

    mul-float/2addr v5, v6

    mul-float/2addr v2, v3

    add-float/2addr v2, v5

    aput v2, p0, v4

    .line 7028
    add-int/lit8 v2, p1, 0xa

    mul-float/2addr v0, v1

    aput v0, p0, v2

    .line 7029
    add-int/lit8 v0, p1, 0xb

    aput v10, p0, v0

    .line 7030
    add-int/lit8 v0, p1, 0xc

    aput v10, p0, v0

    .line 7031
    add-int/lit8 v0, p1, 0xd

    aput v10, p0, v0

    .line 7032
    add-int/lit8 v0, p1, 0xe

    aput v10, p0, v0

    .line 7033
    add-int/lit8 v0, p1, 0xf

    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, p0, v0

    .line 7034
    return-void
.end method

.method private b(Landroid/view/SurfaceHolder;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6988
    iget-boolean v1, p0, Lcom/facebook/common/asyncview/SpriteView;->i:Z

    if-eqz v1, :cond_1

    .line 6989
    :cond_0
    :goto_0
    return-void

    .line 6990
    :cond_1
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v2

    .line 6991
    if-nez v2, :cond_2

    .line 6992
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/asyncview/SpriteView;->i:Z

    .line 6993
    const-string v0, "fb-SpriteView"

    const-string v1, "failure to lock canvas: stopping all drawing!"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 6994
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->j:LX/0FC;

    if-eqz v0, :cond_0

    .line 6995
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->j:LX/0FC;

    iget v0, v0, LX/0FC;->error:I

    const/16 v1, 0x300d

    if-eq v0, v1, :cond_0

    .line 6996
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->j:LX/0FC;

    throw v0

    .line 6997
    :cond_2
    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    .line 6998
    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 6999
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 7000
    iget-object v3, p0, Lcom/facebook/common/asyncview/SpriteView;->d:Ljava/util/ArrayList;

    .line 7001
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v0

    .line 7002
    :goto_1
    if-ge v1, v4, :cond_3

    .line 7003
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/asyncview/SpriteView$Sprite;

    invoke-static {v0, v2}, Lcom/facebook/common/asyncview/SpriteView;->a(Lcom/facebook/common/asyncview/SpriteView$Sprite;Landroid/graphics/Canvas;)V

    .line 7004
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 7005
    :cond_3
    invoke-virtual {v2}, Landroid/graphics/Canvas;->restore()V

    .line 7006
    invoke-interface {p1, v2}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private b(Lcom/facebook/common/asyncview/SpriteView$Sprite;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/high16 v2, 0x40000000    # 2.0f

    .line 6865
    iget v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->r:I

    if-nez v0, :cond_0

    iget v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->s:I

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->t:[F

    if-eqz v0, :cond_1

    .line 6866
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 6867
    :cond_1
    iput-boolean v1, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 6868
    :try_start_0
    iget-object v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->a:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/facebook/common/asyncview/SpriteView;->a(Landroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->r:I

    .line 6869
    iget v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->b:F

    div-float/2addr v0, v2

    .line 6870
    iget v1, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->c:F

    div-float/2addr v1, v2

    .line 6871
    const v2, 0x8892

    const/16 v3, 0x8

    new-array v3, v3, [F

    const/4 v4, 0x0

    neg-float v5, v0

    aput v5, v3, v4

    const/4 v4, 0x1

    neg-float v5, v1

    aput v5, v3, v4

    const/4 v4, 0x2

    aput v0, v3, v4

    const/4 v4, 0x3

    neg-float v5, v1

    aput v5, v3, v4

    const/4 v4, 0x4

    neg-float v5, v0

    aput v5, v3, v4

    const/4 v4, 0x5

    aput v1, v3, v4

    const/4 v4, 0x6

    aput v0, v3, v4

    const/4 v0, 0x7

    aput v1, v3, v0

    invoke-direct {p0, v2, v3}, Lcom/facebook/common/asyncview/SpriteView;->a(I[F)I

    move-result v0

    iput v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->s:I

    .line 6872
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->t:[F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6873
    return-void

    :catchall_0
    move-exception v0

    .line 6874
    invoke-direct {p0, p1}, Lcom/facebook/common/asyncview/SpriteView;->c(Lcom/facebook/common/asyncview/SpriteView$Sprite;)V

    throw v0
.end method

.method private static b(Lcom/facebook/common/asyncview/SpriteView;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6875
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->a:[I

    .line 6876
    aput p1, v0, v2

    .line 6877
    const/4 v1, 0x1

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 6878
    return-void
.end method

.method private static b(Lcom/facebook/common/asyncview/SpriteView;I[F)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6667
    const/4 v0, 0x1

    invoke-static {p1, v0, v1, p2, v1}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 6668
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6669
    return-void
.end method

.method private c(Lcom/facebook/common/asyncview/SpriteView$Sprite;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6670
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->t:[F

    .line 6671
    iget v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->s:I

    invoke-direct {p0, v0}, Lcom/facebook/common/asyncview/SpriteView;->a(I)V

    .line 6672
    iput v1, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->s:I

    .line 6673
    iget v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->r:I

    invoke-static {p0, v0}, Lcom/facebook/common/asyncview/SpriteView;->b(Lcom/facebook/common/asyncview/SpriteView;I)V

    .line 6674
    iput v1, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->r:I

    .line 6675
    return-void
.end method

.method private static c(Lcom/facebook/common/asyncview/SpriteView;II)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 6676
    const v0, 0x8892

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 6677
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6678
    invoke-static {p2}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 6679
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6680
    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/16 v4, 0x8

    move v0, p2

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 6681
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6682
    return-void
.end method

.method public static e(Lcom/facebook/common/asyncview/SpriteView;Lcom/facebook/common/asyncview/SpriteView$Sprite;)V
    .locals 13

    .prologue
    const/16 v3, 0xde1

    const/4 v2, 0x0

    .line 6683
    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/4 v5, 0x0

    .line 6684
    iget-boolean v4, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    if-eqz v4, :cond_3

    .line 6685
    iget-object v4, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->t:[F

    .line 6686
    invoke-static {v4, v5}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 6687
    iget-object v6, p0, Lcom/facebook/common/asyncview/SpriteView;->b:[F

    .line 6688
    iget-object v8, p0, Lcom/facebook/common/asyncview/SpriteView;->c:[F

    .line 6689
    invoke-static {v6, v5}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 6690
    iget v7, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->f:F

    iget v9, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->n:F

    sub-float/2addr v7, v9

    iget v9, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->g:F

    iget v10, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->o:F

    sub-float/2addr v9, v10

    iget v10, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->h:F

    invoke-static {v6, v5, v7, v9, v10}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 6691
    iget v7, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->k:F

    cmpl-float v7, v7, v11

    if-nez v7, :cond_0

    iget v7, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->l:F

    cmpl-float v7, v7, v11

    if-nez v7, :cond_0

    iget v7, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->m:F

    cmpl-float v7, v7, v11

    if-eqz v7, :cond_4

    .line 6692
    :cond_0
    iget v7, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->k:F

    iget v9, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->l:F

    iget v10, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->m:F

    neg-float v10, v10

    invoke-static {v8, v5, v7, v9, v10}, Lcom/facebook/common/asyncview/SpriteView;->a([FIFFF)V

    move v7, v5

    move v9, v5

    .line 6693
    invoke-static/range {v4 .. v9}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 6694
    :goto_0
    iget v6, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->i:F

    cmpl-float v6, v6, v12

    if-nez v6, :cond_1

    iget v6, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->j:F

    cmpl-float v6, v6, v12

    if-eqz v6, :cond_2

    .line 6695
    :cond_1
    iget v6, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->i:F

    iget v7, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->j:F

    invoke-static {v4, v5, v6, v7, v12}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 6696
    :cond_2
    iget v6, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->n:F

    iget v7, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->o:F

    invoke-static {v4, v5, v6, v7, v11}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 6697
    iput-boolean v5, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 6698
    :cond_3
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->q:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 6699
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6700
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->t:I

    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->u:[F

    invoke-static {p0, v0, v1}, Lcom/facebook/common/asyncview/SpriteView;->b(Lcom/facebook/common/asyncview/SpriteView;I[F)V

    .line 6701
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->s:I

    iget-object v1, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->t:[F

    invoke-static {p0, v0, v1}, Lcom/facebook/common/asyncview/SpriteView;->b(Lcom/facebook/common/asyncview/SpriteView;I[F)V

    .line 6702
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->v:I

    iget v1, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->e:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 6703
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6704
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 6705
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6706
    iget v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->r:I

    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 6707
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6708
    iget v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->s:I

    iget v1, p0, Lcom/facebook/common/asyncview/SpriteView;->r:I

    invoke-static {p0, v0, v1}, Lcom/facebook/common/asyncview/SpriteView;->c(Lcom/facebook/common/asyncview/SpriteView;II)V

    .line 6709
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->y:I

    iget v1, p0, Lcom/facebook/common/asyncview/SpriteView;->x:I

    invoke-static {p0, v0, v1}, Lcom/facebook/common/asyncview/SpriteView;->c(Lcom/facebook/common/asyncview/SpriteView;II)V

    .line 6710
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->w:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 6711
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6712
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-static {v0, v2, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 6713
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6714
    invoke-static {v3, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 6715
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6716
    invoke-static {v2}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 6717
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6718
    return-void

    .line 6719
    :cond_4
    const/4 v8, 0x0

    .line 6720
    const/16 v7, 0x10

    invoke-static {v6, v8, v4, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6721
    goto :goto_0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 6722
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->m:Ljavax/microedition/khronos/egl/EGLConfig;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->n:Ljavax/microedition/khronos/egl/EGLContext;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->o:Ljavax/microedition/khronos/egl/EGLSurface;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->p:Ljavax/microedition/khronos/opengles/GL10;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->q:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->r:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->s:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->t:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->v:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->w:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->x:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->y:I

    if-eqz v0, :cond_1

    .line 6723
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 6724
    :cond_1
    return-void
.end method

.method private h()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 6725
    iget-object v3, p0, Lcom/facebook/common/asyncview/SpriteView;->d:Ljava/util/ArrayList;

    .line 6726
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 6727
    :goto_0
    if-ge v1, v4, :cond_0

    .line 6728
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/asyncview/SpriteView$Sprite;

    invoke-direct {p0, v0}, Lcom/facebook/common/asyncview/SpriteView;->c(Lcom/facebook/common/asyncview/SpriteView$Sprite;)V

    .line 6729
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 6730
    :cond_0
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->y:I

    invoke-direct {p0, v0}, Lcom/facebook/common/asyncview/SpriteView;->a(I)V

    .line 6731
    iput v2, p0, Lcom/facebook/common/asyncview/SpriteView;->y:I

    .line 6732
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->q:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 6733
    iput v2, p0, Lcom/facebook/common/asyncview/SpriteView;->q:I

    .line 6734
    iput v2, p0, Lcom/facebook/common/asyncview/SpriteView;->x:I

    .line 6735
    iput v2, p0, Lcom/facebook/common/asyncview/SpriteView;->s:I

    .line 6736
    iput v2, p0, Lcom/facebook/common/asyncview/SpriteView;->t:I

    .line 6737
    iput v2, p0, Lcom/facebook/common/asyncview/SpriteView;->w:I

    .line 6738
    iput v2, p0, Lcom/facebook/common/asyncview/SpriteView;->v:I

    .line 6739
    iput v2, p0, Lcom/facebook/common/asyncview/SpriteView;->r:I

    .line 6740
    iput-object v5, p0, Lcom/facebook/common/asyncview/SpriteView;->p:Ljavax/microedition/khronos/opengles/GL10;

    .line 6741
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->o:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v0, :cond_2

    .line 6742
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-nez v0, :cond_1

    .line 6743
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 6744
    :cond_1
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 6745
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/facebook/common/asyncview/SpriteView;->o:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 6746
    iput-object v5, p0, Lcom/facebook/common/asyncview/SpriteView;->o:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 6747
    :cond_2
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->n:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_4

    .line 6748
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/facebook/common/asyncview/SpriteView;->n:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 6749
    const-string v0, "fb-SpriteView"

    const-string v1, "eglDestroyContext failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 6750
    :cond_3
    iput-object v5, p0, Lcom/facebook/common/asyncview/SpriteView;->n:Ljavax/microedition/khronos/egl/EGLContext;

    .line 6751
    :cond_4
    iput-object v5, p0, Lcom/facebook/common/asyncview/SpriteView;->m:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 6752
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eqz v0, :cond_5

    .line 6753
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    .line 6754
    iput-object v5, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 6755
    :cond_5
    iput-object v5, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    .line 6756
    return-void
.end method

.method public static j(Lcom/facebook/common/asyncview/SpriteView;)V
    .locals 2

    .prologue
    .line 6761
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->p:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetError()I

    move-result v0

    .line 6762
    if-eqz v0, :cond_0

    .line 6763
    new-instance v1, Landroid/opengl/GLException;

    invoke-direct {v1, v0}, Landroid/opengl/GLException;-><init>(I)V

    throw v1

    .line 6764
    :cond_0
    return-void
.end method

.method public static k(Lcom/facebook/common/asyncview/SpriteView;)V
    .locals 3

    .prologue
    .line 6765
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v1

    .line 6766
    packed-switch v1, :pswitch_data_0

    .line 6767
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unknown EGL error "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6768
    :goto_0
    new-instance v2, LX/0FC;

    invoke-direct {v2, v1, v0}, LX/0FC;-><init>(ILjava/lang/String;)V

    throw v2

    .line 6769
    :pswitch_0
    const-string v0, "EGL_NOT_INITIALIZED"

    goto :goto_0

    .line 6770
    :pswitch_1
    const-string v0, "EGL_BAD_ACCESS"

    goto :goto_0

    .line 6771
    :pswitch_2
    const-string v0, "EGL_BAD_ALLOC"

    goto :goto_0

    .line 6772
    :pswitch_3
    const-string v0, "EGL_BAD_ATTRIBUTE"

    goto :goto_0

    .line 6773
    :pswitch_4
    const-string v0, "EGL_BAD_CONFIG"

    goto :goto_0

    .line 6774
    :pswitch_5
    const-string v0, "EGL_BAD_CONTEXT"

    goto :goto_0

    .line 6775
    :pswitch_6
    const-string v0, "EGL_BAD_CURRENT_SURFACE"

    goto :goto_0

    .line 6776
    :pswitch_7
    const-string v0, "EGL_BAD_DISPLAY"

    goto :goto_0

    .line 6777
    :pswitch_8
    const-string v0, "EGL_BAD_MATCH"

    goto :goto_0

    .line 6778
    :pswitch_9
    const-string v0, "EGL_BAD_NATIVE_PIXMAP"

    goto :goto_0

    .line 6779
    :pswitch_a
    const-string v0, "EGL_BAD_NATIVE_WINDOW"

    goto :goto_0

    .line 6780
    :pswitch_b
    const-string v0, "EGL_BAD_PARAMETER"

    goto :goto_0

    .line 6781
    :pswitch_c
    const-string v0, "EGL_BAD_SURFACE"

    goto :goto_0

    .line 6782
    :pswitch_d
    const-string v0, "EGL_CONTEXT_LOST"

    goto :goto_0

    .line 6783
    :pswitch_e
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3000
        :pswitch_e
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/SurfaceHolder;II)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    .line 6784
    invoke-super {p0, p1, p2, p3}, LX/02H;->a(Landroid/view/SurfaceHolder;II)V

    .line 6785
    iget-byte v0, p0, Lcom/facebook/common/asyncview/SpriteView;->g:B

    if-nez v0, :cond_0

    .line 6786
    :try_start_0
    invoke-direct {p0, p1}, Lcom/facebook/common/asyncview/SpriteView;->a(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Landroid/opengl/GLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/0FC; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6787
    :cond_0
    :goto_0
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->e:I

    if-ne v0, p2, :cond_1

    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->f:I

    if-eq v0, p3, :cond_2

    .line 6788
    :cond_1
    iput p2, p0, Lcom/facebook/common/asyncview/SpriteView;->e:I

    .line 6789
    iput p3, p0, Lcom/facebook/common/asyncview/SpriteView;->f:I

    .line 6790
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->u:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 6791
    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 6792
    int-to-float v4, p2

    div-float v7, v4, v6

    .line 6793
    int-to-float v4, p3

    div-float v9, v4, v6

    .line 6794
    neg-float v6, v7

    .line 6795
    neg-float v8, v9

    .line 6796
    iget-object v4, p0, Lcom/facebook/common/asyncview/SpriteView;->u:[F

    invoke-static {v4, v5}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 6797
    iget-object v4, p0, Lcom/facebook/common/asyncview/SpriteView;->u:[F

    const/high16 v10, -0x40800000    # -1.0f

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-static/range {v4 .. v11}, Landroid/opengl/Matrix;->orthoM([FIFFFFFF)V

    .line 6798
    :cond_2
    iget-byte v0, p0, Lcom/facebook/common/asyncview/SpriteView;->g:B

    if-ne v0, v3, :cond_3

    .line 6799
    invoke-direct {p0, p2, p3}, Lcom/facebook/common/asyncview/SpriteView;->a(II)V

    .line 6800
    :cond_3
    return-void

    .line 6801
    :catch_0
    move-exception v0

    .line 6802
    :goto_1
    :try_start_1
    const-string v1, "fb-SpriteView"

    const-string v2, "failure to initialize OpenGL: switching to software"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6803
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/facebook/common/asyncview/SpriteView;->g:B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6804
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView;->h()V

    goto :goto_0

    .line 6805
    :catchall_0
    move-exception v0

    .line 6806
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView;->h()V

    throw v0

    .line 6807
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Landroid/view/SurfaceHolder;J)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 6808
    invoke-super {p0, p1, p2, p3}, LX/02H;->a(Landroid/view/SurfaceHolder;J)V

    .line 6809
    iget-byte v0, p0, Lcom/facebook/common/asyncview/SpriteView;->g:B

    .line 6810
    if-nez v0, :cond_3

    .line 6811
    :cond_0
    const/4 v2, 0x0

    .line 6812
    :try_start_0
    const/16 v3, 0x4000

    invoke-static {v3}, Landroid/opengl/GLES20;->glClear(I)V

    .line 6813
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->j(Lcom/facebook/common/asyncview/SpriteView;)V

    .line 6814
    iget-object v5, p0, Lcom/facebook/common/asyncview/SpriteView;->d:Ljava/util/ArrayList;

    .line 6815
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result p2

    .line 6816
    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, p2, :cond_1

    .line 6817
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/common/asyncview/SpriteView$Sprite;

    invoke-static {p0, v3}, Lcom/facebook/common/asyncview/SpriteView;->e(Lcom/facebook/common/asyncview/SpriteView;Lcom/facebook/common/asyncview/SpriteView$Sprite;)V

    .line 6818
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 6819
    :cond_1
    iget-object v3, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v4, p0, Lcom/facebook/common/asyncview/SpriteView;->l:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v5, p0, Lcom/facebook/common/asyncview/SpriteView;->o:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v3, v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 6820
    invoke-static {p0}, Lcom/facebook/common/asyncview/SpriteView;->k(Lcom/facebook/common/asyncview/SpriteView;)V
    :try_end_0
    .catch LX/0FC; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/opengl/GLException; {:try_start_0 .. :try_end_0} :catch_1

    .line 6821
    :goto_1
    if-eqz v2, :cond_2

    .line 6822
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView;->h()V

    .line 6823
    invoke-direct {p0, p1}, Lcom/facebook/common/asyncview/SpriteView;->a(Landroid/view/SurfaceHolder;)V
    :try_end_1
    .catch Landroid/opengl/GLException; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/0FC; {:try_start_1 .. :try_end_1} :catch_2

    .line 6824
    :cond_2
    if-nez v2, :cond_0

    .line 6825
    :cond_3
    :goto_2
    if-ne v0, v1, :cond_7

    .line 6826
    invoke-direct {p0, p1}, Lcom/facebook/common/asyncview/SpriteView;->b(Landroid/view/SurfaceHolder;)V

    .line 6827
    :cond_4
    return-void

    .line 6828
    :catch_0
    move-exception v2

    .line 6829
    :try_start_2
    iget v3, v2, LX/0FC;->error:I

    const/16 v4, 0x300e

    if-ne v3, v4, :cond_5

    move v2, v1

    .line 6830
    goto :goto_1

    .line 6831
    :cond_5
    throw v2
    :try_end_2
    .catch Landroid/opengl/GLException; {:try_start_2 .. :try_end_2} :catch_1
    .catch LX/0FC; {:try_start_2 .. :try_end_2} :catch_2

    .line 6832
    :catch_1
    move-exception v0

    .line 6833
    :goto_3
    const-string v2, "fb-SpriteView"

    const-string v3, "OpenGL error: falling back to software"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6834
    instance-of v2, v0, LX/0FC;

    if-eqz v2, :cond_6

    .line 6835
    check-cast v0, LX/0FC;

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->j:LX/0FC;

    .line 6836
    :cond_6
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView;->h()V

    .line 6837
    iput-byte v1, p0, Lcom/facebook/common/asyncview/SpriteView;->g:B

    .line 6838
    iget v0, p0, Lcom/facebook/common/asyncview/SpriteView;->e:I

    iget v2, p0, Lcom/facebook/common/asyncview/SpriteView;->f:I

    invoke-direct {p0, v0, v2}, Lcom/facebook/common/asyncview/SpriteView;->a(II)V

    move v0, v1

    goto :goto_2

    .line 6839
    :cond_7
    if-eqz v0, :cond_4

    .line 6840
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "unknown draw style"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 6841
    :catch_2
    move-exception v0

    goto :goto_3
.end method

.method public final a(Lcom/facebook/common/asyncview/SpriteView$Sprite;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 6842
    invoke-virtual {p0}, LX/02H;->e()V

    .line 6843
    iget-object v0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->d:Lcom/facebook/common/asyncview/SpriteView;

    if-eqz v0, :cond_0

    .line 6844
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "sprite already in scene"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6845
    :cond_0
    iput-object p0, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->d:Lcom/facebook/common/asyncview/SpriteView;

    .line 6846
    iput-boolean v1, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->p:Z

    .line 6847
    iput-boolean v1, p1, Lcom/facebook/common/asyncview/SpriteView$Sprite;->q:Z

    .line 6848
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->k:Ljavax/microedition/khronos/egl/EGL10;

    if-eqz v0, :cond_1

    .line 6849
    invoke-direct {p0, p1}, Lcom/facebook/common/asyncview/SpriteView;->b(Lcom/facebook/common/asyncview/SpriteView$Sprite;)V

    .line 6850
    :cond_1
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6851
    invoke-virtual {p0}, LX/02H;->f()V

    .line 6852
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6853
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView;->g()V

    .line 6854
    iget-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 6855
    iput-boolean v1, p0, Lcom/facebook/common/asyncview/SpriteView;->i:Z

    .line 6856
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/common/asyncview/SpriteView;->j:LX/0FC;

    .line 6857
    iput v1, p0, Lcom/facebook/common/asyncview/SpriteView;->e:I

    .line 6858
    iput v1, p0, Lcom/facebook/common/asyncview/SpriteView;->f:I

    .line 6859
    invoke-super {p0}, LX/02H;->c()V

    .line 6860
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 6861
    iget-byte v0, p0, Lcom/facebook/common/asyncview/SpriteView;->g:B

    if-nez v0, :cond_0

    .line 6862
    invoke-direct {p0}, Lcom/facebook/common/asyncview/SpriteView;->h()V

    .line 6863
    :cond_0
    invoke-super {p0}, LX/02H;->d()V

    .line 6864
    return-void
.end method
