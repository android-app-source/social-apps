.class public Lcom/facebook/common/lyra/LyraManager;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2728
    const-string v0, "lyramanager"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 2729
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2731
    const-string v0, "lyra_flags_store"

    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2732
    const-string v1, "android_crash_lyra_hook_cxa_throw"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 2733
    const-string v2, "android_crash_lyra_enable_backtraces"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2734
    if-eqz v1, :cond_0

    .line 2735
    invoke-static {v0}, Lcom/facebook/common/lyra/LyraManager;->nativeInstallLyraHook(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2736
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Installing lyra hook failed."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2737
    :cond_0
    return-void
.end method

.method private static native nativeInstallLyraHook(Z)Z
.end method
