.class public final Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field private final mHeldLock:LX/02W;

.field private final mNewStatus:J

.field public final synthetic this$0:LX/02U;


# direct methods
.method public constructor <init>(LX/02U;LX/02W;J)V
    .locals 3

    .prologue
    .line 33325
    iput-object p1, p0, Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;->this$0:LX/02U;

    .line 33326
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TxFlush-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, LX/02U;->root:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 33327
    iput-object p2, p0, Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;->mHeldLock:LX/02W;

    .line 33328
    iput-wide p3, p0, Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;->mNewStatus:J

    .line 33329
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 33330
    :try_start_0
    const-string v0, "running syncer thread"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33331
    :try_start_1
    iget-object v0, p0, Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;->this$0:LX/02U;

    iget-object v0, v0, LX/02U;->root:Ljava/io/File;

    invoke-static {}, LX/08D;->unchanged()LX/08D;

    move-result-object v1

    invoke-static {v0, v1}, LX/02Q;->fsyncRecursive(Ljava/io/File;LX/08D;)V

    .line 33332
    iget-object v0, p0, Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;->this$0:LX/02U;

    iget-wide v2, p0, Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;->mNewStatus:J

    invoke-virtual {v0, v2, v3}, LX/02U;->writeStatusLocked(J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 33333
    iget-object v0, p0, Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;->mHeldLock:LX/02W;

    invoke-virtual {v0}, LX/02W;->close()V

    .line 33334
    const-string v0, "finished syncer thread: initial regeneration of dex store %s complete"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;->this$0:LX/02U;

    iget-object v2, v2, LX/02U;->root:Ljava/io/File;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 33335
    return-void

    .line 33336
    :catch_0
    move-exception v0

    .line 33337
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 33338
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/common/dextricks/DexStore$FinishRegenerationThread;->mHeldLock:LX/02W;

    invoke-virtual {v1}, LX/02W;->close()V

    throw v0
.end method
