.class public final Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;
.super Lcom/facebook/common/dextricks/MultiDexClassLoader;
.source ""


# static fields
.field public static final AVG_DEX_SIZE:I = 0x700000

.field public static final MAX_COLDSTART_OAT_SIZE:I = 0x1e00000

.field public static final MB:I = 0x100000

.field public static final OAT_HEADER_SIZE:I = 0x100000

.field public static final PROT_EXEC_OAT_GK_ENABLED_FILENAME:Ljava/lang/String; = "fb4a_prot_exec_for_oat_file_enabled"

.field public static final PROT_EXEC_OAT_GK_NAME:Ljava/lang/String; = "fb4a_prot_exec_for_oat_file"

.field public static final RANDOM_ACCESS_OAT_GK_ENABLED_FILENAME:Ljava/lang/String; = "fb4a_random_access_mode_for_oat_file_enabled"

.field public static final RANDOM_ACCESS_OAT_GK_NAME:Ljava/lang/String; = "fb4a_random_access_mode_for_oat_file"

.field public static final SEQUENTIAL_ACCESS_OAT_GK_ENABLED_FILENAME:Ljava/lang/String; = "fb4a_sequential_access_mode_for_oat_file_enabled"

.field public static final SEQUENTIAL_ACCESS_OAT_GK_NAME:Ljava/lang/String; = "fb4a_sequential_access_mode_for_oat_file"

.field private static final USE_LOW_LEVEL_DALVIK_HOOKS:Z = true

.field private static final USE_O1_DALVIK_LOCATOR_HACK:Z = true

.field private static final WANT_PROC_EXEC:I = 0x1

.field public static sIsIntialized:Z


# instance fields
.field private final mAuxDexes:[Ldalvik/system/DexFile;

.field private mDirectLookupsEnabled:Z

.field private mHacksAttempted:Z

.field private mO1HackEnabled:Z

.field private final mPrimaryDexes:[Ldalvik/system/DexFile;

.field private final mPutativeLoader:Ljava/lang/ClassLoader;

.field private mSecondaryDexes:[Ldalvik/system/DexFile;

.field private mStoreLocators:[I
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3613
    const/4 v0, 0x0

    sput-boolean v0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->sIsIntialized:Z

    .line 3614
    const-string v0, "dextricks"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 3615
    return-void
.end method

.method public constructor <init>(Ljava/lang/ClassLoader;Ljava/lang/ClassLoader;Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3556
    invoke-direct {p0, p1}, Lcom/facebook/common/dextricks/MultiDexClassLoader;-><init>(Ljava/lang/ClassLoader;)V

    .line 3557
    iput-boolean v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mDirectLookupsEnabled:Z

    .line 3558
    iput-boolean v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mO1HackEnabled:Z

    .line 3559
    iput-boolean v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mHacksAttempted:Z

    .line 3560
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3561
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 3562
    :try_start_0
    invoke-static {p3, p2, v2, v3}, Lcom/facebook/common/dextricks/MultiDexClassLoader;->learnApplicationDexFiles(Landroid/content/Context;Ljava/lang/ClassLoader;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 3563
    :goto_0
    if-eqz v0, :cond_0

    .line 3564
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ldalvik/system/DexFile;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldalvik/system/DexFile;

    iput-object v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mPrimaryDexes:[Ldalvik/system/DexFile;

    .line 3565
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ldalvik/system/DexFile;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldalvik/system/DexFile;

    iput-object v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mAuxDexes:[Ldalvik/system/DexFile;

    .line 3566
    :goto_1
    iput-object p2, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mPutativeLoader:Ljava/lang/ClassLoader;

    .line 3567
    sget-object v0, Lcom/facebook/common/dextricks/MultiDexClassLoader;->sPrefabException:Ljava/lang/ClassNotFoundException;

    iget-object v2, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mAuxDexes:[Ldalvik/system/DexFile;

    invoke-direct {p0, p2, v0, v2}, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->nativeInitialize(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 3568
    sput-boolean v1, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->sIsIntialized:Z

    .line 3569
    return-void

    .line 3570
    :catch_0
    move-exception v4

    .line 3571
    const-string v5, "MultiDexClassLoader"

    const-string v6, "cannot enable dex hooks: failure to locate primary/aux dexes"

    invoke-static {v5, v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 3572
    :cond_0
    iput-object v7, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mPrimaryDexes:[Ldalvik/system/DexFile;

    .line 3573
    iput-object v7, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mAuxDexes:[Ldalvik/system/DexFile;

    goto :goto_1
.end method

.method public static native getNumClassLoadAttempts()I
.end method

.method public static native getNumDexQueries()I
.end method

.method private static native nativeConfigure([Ljava/lang/Object;[Ljava/lang/Object;IIIII[I)V
    .param p7    # [I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method private native nativeEnableDirectLookupHooks()V
.end method

.method private static native nativeEnableO1Hack()V
.end method

.method private native nativeInitialize(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V
.end method

.method private static native unlockAllMemory()V
.end method


# virtual methods
.method public final declared-synchronized doConfigure(LX/02f;)V
    .locals 8

    .prologue
    .line 3590
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, LX/02f;->mDexFiles:Ljava/util/ArrayList;

    .line 3591
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ldalvik/system/DexFile;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ldalvik/system/DexFile;

    .line 3592
    sget v4, LX/02e;->FB_REDEX_COLD_START_SET_DEX_COUNT:I

    .line 3593
    iget-object v0, p1, LX/02f;->dexExperiment:LX/02d;

    .line 3594
    if-eqz v0, :cond_1

    iget v5, v0, LX/02d;->lockDexNum:I

    .line 3595
    :goto_0
    iget-object v0, p1, LX/02f;->storeLocators:[I

    iput-object v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mStoreLocators:[I

    .line 3596
    iget-object v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mPrimaryDexes:[Ldalvik/system/DexFile;

    .line 3597
    iget v2, p1, LX/02f;->configFlags:I

    move v2, v2

    .line 3598
    sget v3, LX/02e;->FB_REDEX_COLD_START_SET_DEX_COUNT:I

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mStoreLocators:[I

    invoke-static/range {v0 .. v7}, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->nativeConfigure([Ljava/lang/Object;[Ljava/lang/Object;IIIII[I)V

    .line 3599
    iput-object v1, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mSecondaryDexes:[Ldalvik/system/DexFile;

    .line 3600
    iget-boolean v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mHacksAttempted:Z

    if-nez v0, :cond_0

    .line 3601
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mHacksAttempted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3602
    :try_start_1
    invoke-virtual {p0}, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->enableDirectLookupHooks()V

    .line 3603
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mDirectLookupsEnabled:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3604
    :goto_1
    :try_start_2
    invoke-virtual {p0}, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->enableO1Hack()V

    .line 3605
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mO1HackEnabled:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3606
    :cond_0
    :goto_2
    monitor-exit p0

    return-void

    .line 3607
    :cond_1
    const/4 v5, -0x1

    goto :goto_0

    .line 3608
    :catch_0
    move-exception v0

    .line 3609
    :try_start_3
    const-string v1, "MultiDexClassLoader"

    const-string v2, "unable to install direct Dalvik class-lookup hooks; continuing with classloader API"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 3610
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3611
    :catch_1
    move-exception v0

    .line 3612
    :try_start_4
    const-string v1, "MultiDexClassLoader"

    const-string v2, "unable to enable O1 Dalvik hack"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method

.method public final doGetConfiguredDexFiles()[Ldalvik/system/DexFile;
    .locals 1

    .prologue
    .line 3589
    iget-object v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mSecondaryDexes:[Ldalvik/system/DexFile;

    return-object v0
.end method

.method public final enableDirectLookupHooks()V
    .locals 2

    .prologue
    .line 3583
    iget-object v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mPrimaryDexes:[Ldalvik/system/DexFile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mAuxDexes:[Ldalvik/system/DexFile;

    if-nez v0, :cond_1

    .line 3584
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "cannot enable direct hooks: we could not locate primary and aux dexes"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3585
    :cond_1
    iget-object v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mPrimaryDexes:[Ldalvik/system/DexFile;

    array-length v0, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 3586
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "cannot enable direct hooks: must have found exactly one primary dex"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3587
    :cond_2
    invoke-direct {p0}, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->nativeEnableDirectLookupHooks()V

    .line 3588
    return-void
.end method

.method public final enableO1Hack()V
    .locals 4

    .prologue
    .line 3577
    iget-object v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mPrimaryDexes:[Ldalvik/system/DexFile;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 3578
    :goto_0
    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 3579
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "To use the O(1) class lookup hack, must have exactly one primary dex: have "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3580
    :cond_0
    iget-object v0, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mPrimaryDexes:[Ldalvik/system/DexFile;

    array-length v0, v0

    goto :goto_0

    .line 3581
    :cond_1
    invoke-static {}, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->nativeEnableO1Hack()V

    .line 3582
    return-void
.end method

.method public final native findClass(Ljava/lang/String;)Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end method

.method public final native loadClass(Ljava/lang/String;Z)Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end method

.method public final onColdstartDone()V
    .locals 0

    .prologue
    .line 3575
    invoke-static {}, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->unlockAllMemory()V

    .line 3576
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 3574
    const-string v0, "MultiDexClassLoaderDalvikNative(lookups=%s,o1=%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mDirectLookupsEnabled:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;->mO1HackEnabled:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
