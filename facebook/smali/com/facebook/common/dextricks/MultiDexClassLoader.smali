.class public abstract Lcom/facebook/common/dextricks/MultiDexClassLoader;
.super Ljava/lang/ClassLoader;
.source ""


# static fields
.field private static final mInstallLock:Ljava/lang/Object;

.field public static volatile mInstalledClassLoader:Lcom/facebook/common/dextricks/MultiDexClassLoader;

.field public static final sPrefabException:Ljava/lang/ClassNotFoundException;


# instance fields
.field public mConfig:LX/02f;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile mHasArtLocked:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3705
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mInstalledClassLoader:Lcom/facebook/common/dextricks/MultiDexClassLoader;

    .line 3706
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mInstallLock:Ljava/lang/Object;

    .line 3707
    new-instance v0, Ljava/lang/ClassNotFoundException;

    const-string v1, "[MultiDexclassLoader prefab]"

    invoke-direct {v0, v1}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/common/dextricks/MultiDexClassLoader;->sPrefabException:Ljava/lang/ClassNotFoundException;

    return-void
.end method

.method public constructor <init>(Ljava/lang/ClassLoader;)V
    .locals 0

    .prologue
    .line 3712
    invoke-direct {p0, p1}, Ljava/lang/ClassLoader;-><init>(Ljava/lang/ClassLoader;)V

    .line 3713
    return-void
.end method

.method public static getConfiguredDexFiles()[Ldalvik/system/DexFile;
    .locals 1

    .prologue
    .line 3708
    sget-object v0, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mInstalledClassLoader:Lcom/facebook/common/dextricks/MultiDexClassLoader;

    .line 3709
    if-nez v0, :cond_0

    .line 3710
    const/4 v0, 0x0

    new-array v0, v0, [Ldalvik/system/DexFile;

    .line 3711
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/common/dextricks/MultiDexClassLoader;->doGetConfiguredDexFiles()[Ldalvik/system/DexFile;

    move-result-object v0

    goto :goto_0
.end method

.method public static install(Landroid/content/Context;)Lcom/facebook/common/dextricks/MultiDexClassLoader;
    .locals 8

    .prologue
    .line 3631
    :try_start_0
    sget-object v0, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mInstalledClassLoader:Lcom/facebook/common/dextricks/MultiDexClassLoader;

    .line 3632
    if-nez v0, :cond_0

    .line 3633
    sget-object v3, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mInstallLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    .line 3634
    :try_start_1
    sget-object v2, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mInstalledClassLoader:Lcom/facebook/common/dextricks/MultiDexClassLoader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3635
    if-nez v2, :cond_3

    .line 3636
    :try_start_2
    const-string v0, "com.facebook.common.dextricks.DexFileLoadOld"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 3637
    const-string v0, "com.facebook.common.dextricks.DexFileLoadNew"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 3638
    const-string v0, "com.facebook.common.dextricks.stats.ClassLoadingStats"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 3639
    const-string v0, "com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3640
    :try_start_3
    const-string v0, "com.facebook.loom.logger.api.LoomLogger"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 3641
    const-string v0, "com.facebook.loom.core.TraceEvents"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 3642
    const-string v0, "com.facebook.loom.logger.LogEntry$EntryType"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 3643
    const-string v0, "com.facebook.loom.logger.ClassLoadLogger"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 3644
    const-string v0, "com.facebook.loom.logger.Logger"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3645
    :goto_0
    :try_start_4
    const-class v0, Lcom/facebook/common/dextricks/MultiDexClassLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    .line 3646
    const-class v0, Ljava/lang/ClassLoader;

    const-string v1, "parent"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    .line 3647
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 3648
    invoke-virtual {v5, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ClassLoader;

    .line 3649
    sget-boolean v1, LX/00g;->yes:Z

    if-nez v1, :cond_1

    const-string v1, "Amazon"

    sget-object v6, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 3650
    :try_start_5
    new-instance v1, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;

    invoke-direct {v1, v0, v4, p0}, Lcom/facebook/common/dextricks/MultiDexClassLoaderDalvikNative;-><init>(Ljava/lang/ClassLoader;Ljava/lang/ClassLoader;Landroid/content/Context;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3651
    :goto_1
    if-nez v1, :cond_2

    .line 3652
    :try_start_6
    new-instance v1, LX/0FY;

    invoke-direct {v1, v0, v4, p0}, LX/0FY;-><init>(Ljava/lang/ClassLoader;Ljava/lang/ClassLoader;Landroid/content/Context;)V

    move-object v0, v1

    .line 3653
    :goto_2
    invoke-virtual {v5, v4, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3654
    sput-object v0, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mInstalledClassLoader:Lcom/facebook/common/dextricks/MultiDexClassLoader;

    .line 3655
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v1

    const-string v2, "recentClassLoadFailures"

    new-instance v4, LX/02h;

    invoke-direct {v4}, LX/02h;-><init>()V

    invoke-virtual {v1, v2, v4}, LX/009;->putLazyCustomData(Ljava/lang/String;LX/00T;)V

    .line 3656
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v1

    const-string v2, "multiDexClassLoader"

    new-instance v4, LX/02i;

    invoke-direct {v4}, LX/02i;-><init>()V

    invoke-virtual {v1, v2, v4}, LX/009;->putLazyCustomData(Ljava/lang/String;LX/00T;)V

    .line 3657
    :goto_3
    monitor-exit v3

    .line 3658
    :cond_0
    return-object v0

    .line 3659
    :catch_0
    move-exception v0

    .line 3660
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 3661
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/NoSuchFieldException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_7 .. :try_end_7} :catch_3

    .line 3662
    :catch_1
    move-exception v0

    .line 3663
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 3664
    :catch_2
    move-exception v1

    .line 3665
    :goto_4
    :try_start_8
    const-string v6, "MultiDexClassLoader"

    const-string v7, "unable to use Dalvik native MDCL: falling back to Java impl"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_1
    move-object v1, v2

    goto :goto_1

    .line 3666
    :catch_3
    move-exception v0

    .line 3667
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 3668
    :catch_4
    move-exception v1

    goto :goto_4

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v0, v2

    goto :goto_3

    :catch_5
    goto :goto_0
.end method

.method public static isArt()Z
    .locals 2

    .prologue
    .line 3704
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static learnApplicationDexFiles(Landroid/content/Context;Ljava/lang/ClassLoader;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/ClassLoader;",
            "Ljava/util/ArrayList",
            "<",
            "Ldalvik/system/DexFile;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ldalvik/system/DexFile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3669
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 3670
    :try_start_0
    const-string v0, "dalvik.system.BaseDexClassLoader"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 3671
    const-string v1, "pathList"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 3672
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 3673
    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 3674
    const-string v1, "dalvik.system.DexPathList"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 3675
    const-string v2, "dexElements"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 3676
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 3677
    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 3678
    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 3679
    const-string v1, "dalvik.system.DexPathList$Element"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 3680
    const-string v2, "dexFile"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 3681
    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 3682
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v6, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 3683
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "primary dex name: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3684
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3685
    array-length v7, v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v7, :cond_1

    aget-object v1, v0, v2

    .line 3686
    invoke-virtual {v3, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 3687
    check-cast v1, Ldalvik/system/DexFile;

    .line 3688
    invoke-virtual {v1}, Ldalvik/system/DexFile;->getName()Ljava/lang/String;

    move-result-object v8

    .line 3689
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 3690
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Found primary dex "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3691
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3692
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 3693
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Found system/other dex "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3694
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 3695
    :catch_0
    move-exception v0

    .line 3696
    :goto_2
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3697
    :catchall_0
    move-exception v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    .line 3698
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Setup multi dex took "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3699
    throw v0

    .line 3700
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v4

    .line 3701
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setup multi dex took "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3702
    return-void

    .line 3703
    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public configure(LX/02f;)V
    .locals 0

    .prologue
    .line 3714
    iput-object p1, p0, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mConfig:LX/02f;

    .line 3715
    invoke-virtual {p0, p1}, Lcom/facebook/common/dextricks/MultiDexClassLoader;->doConfigure(LX/02f;)V

    .line 3716
    return-void
.end method

.method public abstract doConfigure(LX/02f;)V
.end method

.method public abstract doGetConfiguredDexFiles()[Ldalvik/system/DexFile;
.end method

.method public getRecentFailedClasses()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 3616
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    return-object v0
.end method

.method public abstract onColdstartDone()V
.end method

.method public setMadvAndMprotForOatFile(LX/02f;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 3617
    iget-boolean v0, p1, LX/02f;->enableExecProtForOat:Z

    if-eqz v0, :cond_3

    move v0, v1

    .line 3618
    :goto_0
    iget-boolean v2, p1, LX/02f;->enableOatSequentialAccessMode:Z

    if-eqz v2, :cond_4

    move v2, v1

    .line 3619
    :goto_1
    iget-boolean v3, p1, LX/02f;->enableOatRandomAccessMode:Z

    if-eqz v3, :cond_5

    .line 3620
    :goto_2
    iget v3, p1, LX/02f;->coldstartSetSize:I

    .line 3621
    const/4 v4, 0x0

    .line 3622
    if-eqz p1, :cond_0

    iget-object v6, p1, LX/02f;->dexExperiment:LX/02d;

    if-nez v6, :cond_6

    .line 3623
    :cond_0
    :goto_3
    move v4, v4

    .line 3624
    if-nez v0, :cond_1

    if-nez v2, :cond_1

    if-nez v1, :cond_1

    if-eqz v4, :cond_2

    :cond_1
    invoke-static {}, Lcom/facebook/common/dextricks/MultiDexClassLoader;->isArt()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 3625
    iput-boolean v4, p0, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mHasArtLocked:Z

    .line 3626
    invoke-static/range {v0 .. v5}, Lcom/facebook/common/dextricks/DalvikInternals;->mstarOatFile(ZZZIZZ)V

    .line 3627
    :cond_2
    return-void

    :cond_3
    move v0, v5

    .line 3628
    goto :goto_0

    :cond_4
    move v2, v5

    .line 3629
    goto :goto_1

    :cond_5
    move v1, v5

    .line 3630
    goto :goto_2

    :cond_6
    iget-object v6, p1, LX/02f;->dexExperiment:LX/02d;

    iget-boolean v6, v6, LX/02d;->lockArt:Z

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/facebook/common/dextricks/MultiDexClassLoader;->isArt()Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v4, 0x1

    goto :goto_3
.end method
