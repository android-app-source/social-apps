.class public final Lcom/facebook/common/dextricks/stats/ClassLoadingStats;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;


# instance fields
.field private volatile b:I

.field private c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3741
    new-instance v0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    invoke-direct {v0}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;-><init>()V

    sput-object v0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->a:Lcom/facebook/common/dextricks/stats/ClassLoadingStats;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3738
    iput v0, p0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->b:I

    .line 3739
    iput v0, p0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->c:I

    .line 3740
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;)Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;
    .locals 4

    .prologue
    .line 3736
    new-instance v0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

    iget v1, p0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->b:I

    iget v2, p1, Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;->a:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->c:I

    iget v3, p1, Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;->b:I

    sub-int/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;-><init>(II)V

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 3734
    iget v0, p0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->c:I

    .line 3735
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 3729
    iget v0, p0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->b:I

    .line 3730
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 3732
    iget v0, p0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->c:I

    .line 3733
    return-void
.end method

.method public final d()Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;
    .locals 3

    .prologue
    .line 3731
    new-instance v0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;

    iget v1, p0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->b:I

    iget v2, p0, Lcom/facebook/common/dextricks/stats/ClassLoadingStats;->c:I

    invoke-direct {v0, v1, v2}, Lcom/facebook/common/dextricks/stats/ClassLoadingStats$SnapshotStats;-><init>(II)V

    return-object v0
.end method
