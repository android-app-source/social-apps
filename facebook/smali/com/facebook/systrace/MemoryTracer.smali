.class public final Lcom/facebook/systrace/MemoryTracer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public volatile a:Z

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25255
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/systrace/MemoryTracer;->a:Z

    return-void
.end method

.method private c()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x400

    .line 25256
    invoke-static {}, Landroid/os/Debug;->getGlobalAllocCount()I

    move-result v0

    .line 25257
    invoke-static {}, Landroid/os/Debug;->getGlobalAllocSize()I

    move-result v1

    .line 25258
    invoke-static {}, Landroid/os/Debug;->getLoadedClassCount()I

    move-result v2

    .line 25259
    invoke-static {}, Landroid/os/Debug;->getGlobalClassInitCount()I

    move-result v3

    .line 25260
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 25261
    iget-wide v6, p0, Lcom/facebook/systrace/MemoryTracer;->f:J

    sub-long v6, v4, v6

    long-to-int v6, v6

    .line 25262
    if-nez v6, :cond_0

    .line 25263
    :goto_0
    return-void

    .line 25264
    :cond_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    .line 25265
    invoke-virtual {v6}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v8

    .line 25266
    invoke-virtual {v6}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v6

    .line 25267
    const-string v10, "Java bytes allocated"

    iget v11, p0, Lcom/facebook/systrace/MemoryTracer;->c:I

    sub-int v11, v1, v11

    invoke-static {v12, v13, v10, v11}, LX/018;->a(JLjava/lang/String;I)V

    .line 25268
    const-string v10, "# Java allocations"

    iget v11, p0, Lcom/facebook/systrace/MemoryTracer;->b:I

    sub-int v11, v0, v11

    invoke-static {v12, v13, v10, v11}, LX/018;->a(JLjava/lang/String;I)V

    .line 25269
    const-string v10, "classinits"

    iget v11, p0, Lcom/facebook/systrace/MemoryTracer;->e:I

    sub-int v11, v3, v11

    invoke-static {v12, v13, v10, v11}, LX/018;->a(JLjava/lang/String;I)V

    .line 25270
    const-string v10, "classloads"

    iget v11, p0, Lcom/facebook/systrace/MemoryTracer;->d:I

    sub-int v11, v2, v11

    invoke-static {v12, v13, v10, v11}, LX/018;->a(JLjava/lang/String;I)V

    .line 25271
    const-string v10, "free Java heap"

    long-to-int v11, v6

    invoke-static {v12, v13, v10, v11}, LX/018;->a(JLjava/lang/String;I)V

    .line 25272
    const-string v10, "used Java heap"

    sub-long v6, v8, v6

    long-to-int v6, v6

    invoke-static {v12, v13, v10, v6}, LX/018;->a(JLjava/lang/String;I)V

    .line 25273
    const-string v6, "total Java heap"

    long-to-int v7, v8

    invoke-static {v12, v13, v6, v7}, LX/018;->a(JLjava/lang/String;I)V

    .line 25274
    const-string v6, "initialized classes"

    invoke-static {v12, v13, v6, v3}, LX/018;->a(JLjava/lang/String;I)V

    .line 25275
    const-string v6, "loaded classes"

    invoke-static {v12, v13, v6, v2}, LX/018;->a(JLjava/lang/String;I)V

    .line 25276
    iput v0, p0, Lcom/facebook/systrace/MemoryTracer;->b:I

    .line 25277
    iput v1, p0, Lcom/facebook/systrace/MemoryTracer;->c:I

    .line 25278
    iput v2, p0, Lcom/facebook/systrace/MemoryTracer;->d:I

    .line 25279
    iput v3, p0, Lcom/facebook/systrace/MemoryTracer;->e:I

    .line 25280
    iput-wide v4, p0, Lcom/facebook/systrace/MemoryTracer;->f:J

    goto :goto_0
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 25281
    invoke-static {}, Landroid/os/Debug;->startAllocCounting()V

    .line 25282
    :try_start_0
    invoke-static {}, Landroid/os/Debug;->getGlobalAllocCount()I

    move-result v0

    iput v0, p0, Lcom/facebook/systrace/MemoryTracer;->b:I

    .line 25283
    invoke-static {}, Landroid/os/Debug;->getGlobalAllocSize()I

    move-result v0

    iput v0, p0, Lcom/facebook/systrace/MemoryTracer;->c:I

    .line 25284
    invoke-static {}, Landroid/os/Debug;->getLoadedClassCount()I

    move-result v0

    iput v0, p0, Lcom/facebook/systrace/MemoryTracer;->d:I

    .line 25285
    invoke-static {}, Landroid/os/Debug;->getGlobalClassInitCount()I

    move-result v0

    iput v0, p0, Lcom/facebook/systrace/MemoryTracer;->e:I

    .line 25286
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/systrace/MemoryTracer;->f:J

    .line 25287
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/systrace/MemoryTracer;->a:Z

    if-nez v0, :cond_0

    .line 25288
    const-wide/16 v0, 0x400

    const-string v2, "MemoryTracer"

    invoke-static {v0, v1, v2}, LX/018;->a(JLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25289
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/systrace/MemoryTracer;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 25290
    const-wide/16 v0, 0x400

    :try_start_2
    invoke-static {v0, v1}, LX/018;->a(J)V

    .line 25291
    const-wide/16 v4, 0x64
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 25292
    :goto_1
    goto :goto_0

    .line 25293
    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/os/Debug;->stopAllocCounting()V

    throw v0

    .line 25294
    :catchall_1
    move-exception v0

    const-wide/16 v2, 0x400

    :try_start_4
    invoke-static {v2, v3}, LX/018;->a(J)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 25295
    :cond_0
    invoke-static {}, Landroid/os/Debug;->stopAllocCounting()V

    .line 25296
    return-void

    :catch_0
    goto :goto_1
.end method
