.class public final Lcom/facebook/systrace/TraceListenerNotifier$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/013;


# direct methods
.method public constructor <init>(LX/013;J)V
    .locals 0

    .prologue
    .line 39097
    iput-object p1, p0, Lcom/facebook/systrace/TraceListenerNotifier$1;->b:LX/013;

    iput-wide p2, p0, Lcom/facebook/systrace/TraceListenerNotifier$1;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 39098
    iget-object v0, p0, Lcom/facebook/systrace/TraceListenerNotifier$1;->b:LX/013;

    iget-object v1, v0, LX/013;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 39099
    :try_start_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-ge v2, v3, :cond_1

    .line 39100
    :cond_0
    sget-object v2, LX/013;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/systrace/TraceListenerNotifier$1;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 39101
    :goto_0
    iget-object v0, p0, Lcom/facebook/systrace/TraceListenerNotifier$1;->b:LX/013;

    invoke-virtual {v0}, LX/013;->b()V

    .line 39102
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 39103
    :cond_1
    :try_start_1
    const-wide/16 v2, 0x64
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 39104
    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method
