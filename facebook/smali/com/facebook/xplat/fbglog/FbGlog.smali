.class public Lcom/facebook/xplat/fbglog/FbGlog;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static a:LX/03E;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6557
    const-string v0, "fb"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 6558
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6560
    return-void
.end method

.method public static declared-synchronized ensureSubscribedToBLogLevelChanges()V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 6561
    const-class v1, Lcom/facebook/xplat/fbglog/FbGlog;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/facebook/xplat/fbglog/FbGlog;->a:LX/03E;

    if-nez v0, :cond_0

    .line 6562
    new-instance v0, LX/03E;

    invoke-direct {v0}, LX/03E;-><init>()V

    .line 6563
    sput-object v0, Lcom/facebook/xplat/fbglog/FbGlog;->a:LX/03E;

    invoke-static {v0}, LX/01m;->a(LX/03E;)V

    .line 6564
    invoke-static {}, LX/01m;->a()I

    move-result v0

    invoke-static {v0}, Lcom/facebook/xplat/fbglog/FbGlog;->setLogLevel(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6565
    :cond_0
    monitor-exit v1

    return-void

    .line 6566
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static native setLogLevel(I)V
.end method
