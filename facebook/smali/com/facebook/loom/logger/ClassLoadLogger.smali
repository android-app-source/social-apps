.class public Lcom/facebook/loom/logger/ClassLoadLogger;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Ljava/lang/reflect/Field;

.field private static c:Ljava/lang/reflect/Field;

.field private static d:Ljava/lang/reflect/Method;

.field private static final e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 9148
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v1, 0x0

    const v2, 0x3f666666    # 0.9f

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IF)V

    sput-object v0, Lcom/facebook/loom/logger/ClassLoadLogger;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 9149
    invoke-static {}, Lcom/facebook/loom/logger/ClassLoadLogger;->a()Z

    move-result v0

    sput-boolean v0, Lcom/facebook/loom/logger/ClassLoadLogger;->e:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(ILjava/lang/Class;I)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<*>;I)I"
        }
    .end annotation

    .prologue
    .line 9111
    sget-boolean v0, Lcom/facebook/loom/logger/ClassLoadLogger;->e:Z

    if-nez v0, :cond_0

    .line 9112
    const/4 v0, -0x1

    .line 9113
    :goto_0
    return v0

    .line 9114
    :cond_0
    invoke-static {p1}, Lcom/facebook/loom/logger/ClassLoadLogger;->a(Ljava/lang/Class;)I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    invoke-static {p1}, Lcom/facebook/loom/logger/ClassLoadLogger;->b(Ljava/lang/Class;)I

    move-result v2

    int-to-long v2, v2

    or-long v4, v0, v2

    .line 9115
    const/16 v1, 0x48

    const/4 v3, 0x0

    move v0, p0

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-result v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 9144
    :try_start_0
    sget-object v0, Lcom/facebook/loom/logger/ClassLoadLogger;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 9145
    :catch_0
    move-exception v0

    .line 9146
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 9140
    sget-object v0, Lcom/facebook/loom/logger/ClassLoadLogger;->d:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 9141
    check-cast v0, Lcom/android/dex/Dex;

    .line 9142
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/dex/Dex;->open(I)Lcom/android/dex/Dex$Section;

    move-result-object v0

    .line 9143
    invoke-virtual {v0}, Lcom/android/dex/Dex$Section;->readInt()I

    move-result v0

    return v0
.end method

.method private static declared-synchronized a()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 9125
    const-class v2, Lcom/facebook/loom/logger/ClassLoadLogger;

    monitor-enter v2

    :try_start_0
    const-class v3, Ljava/lang/Class;

    .line 9126
    const-string v4, "dexClassDefIndex"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 9127
    const-string v5, "dexCache"

    invoke-virtual {v3, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 9128
    const-string v5, "java.lang.DexCache"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    .line 9129
    const-string v6, "getDex"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 9130
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 9131
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 9132
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 9133
    sput-object v4, Lcom/facebook/loom/logger/ClassLoadLogger;->b:Ljava/lang/reflect/Field;

    .line 9134
    sput-object v3, Lcom/facebook/loom/logger/ClassLoadLogger;->c:Ljava/lang/reflect/Field;

    .line 9135
    sput-object v5, Lcom/facebook/loom/logger/ClassLoadLogger;->d:Ljava/lang/reflect/Method;

    .line 9136
    const-class v3, Lcom/facebook/loom/logger/ClassLoadLogger;

    invoke-static {v3}, Lcom/facebook/loom/logger/ClassLoadLogger;->a(Ljava/lang/Class;)I

    .line 9137
    const-class v3, Lcom/facebook/loom/logger/ClassLoadLogger;

    invoke-static {v3}, Lcom/facebook/loom/logger/ClassLoadLogger;->b(Ljava/lang/Class;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9138
    :goto_0
    monitor-exit v2

    return v0

    :catch_0
    move v0, v1

    goto :goto_0

    .line 9139
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static b(Ljava/lang/Class;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 9116
    :try_start_0
    sget-object v0, Lcom/facebook/loom/logger/ClassLoadLogger;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 9117
    sget-object v0, Lcom/facebook/loom/logger/ClassLoadLogger;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 9118
    if-nez v0, :cond_0

    .line 9119
    invoke-static {v1}, Lcom/facebook/loom/logger/ClassLoadLogger;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 9120
    sget-object v2, Lcom/facebook/loom/logger/ClassLoadLogger;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9121
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 9122
    :catch_0
    move-exception v0

    .line 9123
    :goto_0
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 9124
    :catch_1
    move-exception v0

    goto :goto_0
.end method
