.class public final Lcom/facebook/loom/logger/Logger;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation

.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field public static final a:Ljava/util/concurrent/BlockingQueue;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:Lcom/facebook/loom/logger/NativeRingBuffer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private static volatile c:Z

.field private static d:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/facebook/loom/logger/LoggerWorkerThread;",
            ">;"
        }
    .end annotation
.end field

.field private static e:Ljava/io/File;

.field private static f:LX/02o;

.field private static g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6259
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/facebook/loom/logger/Logger;->a:Ljava/util/concurrent/BlockingQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(III)I
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 6261
    sget-boolean v0, Lcom/facebook/loom/logger/Logger;->c:Z

    if-nez v0, :cond_0

    .line 6262
    const/4 v0, -0x1

    .line 6263
    :goto_0
    return v0

    :cond_0
    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move v1, p0

    move v3, p1

    move v4, p2

    move-object v8, v2

    invoke-static/range {v1 .. v8}, Lcom/facebook/loom/logger/Logger;->a(ILcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJLjava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(IIII)I
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 6264
    sget-boolean v0, Lcom/facebook/loom/logger/Logger;->c:Z

    if-nez v0, :cond_0

    .line 6265
    const/4 v0, -0x1

    .line 6266
    :goto_0
    return v0

    :cond_0
    const-wide/16 v6, 0x0

    move v1, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move-object v8, v2

    invoke-static/range {v1 .. v8}, Lcom/facebook/loom/logger/Logger;->a(ILcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJLjava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(IIIIJ)I
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 6267
    sget-boolean v0, Lcom/facebook/loom/logger/Logger;->c:Z

    if-nez v0, :cond_0

    .line 6268
    const/4 v0, -0x1

    .line 6269
    :goto_0
    return v0

    :cond_0
    move v1, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move-wide v6, p4

    move-object v8, v2

    invoke-static/range {v1 .. v8}, Lcom/facebook/loom/logger/Logger;->a(ILcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJLjava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(IIIIJLjava/lang/String;Ljava/lang/String;)I
    .locals 12
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 6270
    sget-boolean v2, Lcom/facebook/loom/logger/Logger;->c:Z

    if-eqz v2, :cond_0

    const/4 v2, -0x1

    if-eq p0, v2, :cond_1

    invoke-static {p0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 6271
    :cond_0
    const/4 v2, -0x1

    .line 6272
    :goto_0
    return v2

    .line 6273
    :cond_1
    const/4 v4, 0x0

    const/4 v10, 0x0

    move v3, p0

    move v5, p1

    move v6, p2

    move v7, p3

    move-wide/from16 v8, p4

    invoke-static/range {v3 .. v10}, Lcom/facebook/loom/logger/Logger;->a(ILcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJLjava/lang/String;)I

    move-result v2

    .line 6274
    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-static {p0, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IILjava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public static a(IIIJ)I
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 6292
    sget-boolean v0, Lcom/facebook/loom/logger/Logger;->c:Z

    if-nez v0, :cond_0

    .line 6293
    const/4 v0, -0x1

    .line 6294
    :goto_0
    return v0

    :cond_0
    const/4 v5, 0x0

    move v1, p0

    move v3, p1

    move v4, p2

    move-wide v6, p3

    move-object v8, v2

    invoke-static/range {v1 .. v8}, Lcom/facebook/loom/logger/Logger;->a(ILcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJLjava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(IIIJJ)I
    .locals 9

    .prologue
    const/4 v0, -0x1

    .line 6275
    sget-boolean v1, Lcom/facebook/loom/logger/Logger;->c:Z

    if-eqz v1, :cond_0

    if-eq p0, v0, :cond_1

    invoke-static {p0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 6276
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v3, 0x0

    move v1, p1

    move v2, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-static/range {v1 .. v7}, Lcom/facebook/loom/logger/Logger;->loggerWriteWithMonotonicTime(IIIJJ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(IIIJLjava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 6277
    sget-boolean v1, Lcom/facebook/loom/logger/Logger;->c:Z

    if-eqz v1, :cond_0

    if-eq p0, v0, :cond_1

    invoke-static {p0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 6278
    :cond_0
    :goto_0
    return v0

    .line 6279
    :cond_1
    const/4 v5, 0x0

    move v1, p0

    move v3, p1

    move v4, p2

    move-wide v6, p3

    move-object v8, v2

    invoke-static/range {v1 .. v8}, Lcom/facebook/loom/logger/Logger;->a(ILcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJLjava/lang/String;)I

    move-result v0

    .line 6280
    invoke-static {p0, v0, p5, p6}, Lcom/facebook/loom/logger/Logger;->a(IILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(IIIJLjava/lang/String;Ljava/lang/String;J)I
    .locals 9
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 6281
    sget-boolean v0, Lcom/facebook/loom/logger/Logger;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-eq p0, v0, :cond_1

    invoke-static {p0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 6282
    :cond_0
    const/4 v0, -0x1

    .line 6283
    :goto_0
    return v0

    .line 6284
    :cond_1
    const/4 v3, 0x0

    move v1, p1

    move v2, p2

    move-wide v4, p3

    move-wide/from16 v6, p7

    invoke-static/range {v1 .. v7}, Lcom/facebook/loom/logger/Logger;->loggerWriteWithMonotonicTime(IIIJJ)I

    move-result v0

    .line 6285
    invoke-static {p0, v0, p5, p6}, Lcom/facebook/loom/logger/Logger;->a(IILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static a(IIILjava/lang/String;)I
    .locals 9

    .prologue
    .line 6286
    sget-boolean v0, Lcom/facebook/loom/logger/Logger;->c:Z

    if-nez v0, :cond_0

    .line 6287
    const/4 v0, -0x1

    .line 6288
    :goto_0
    return v0

    :cond_0
    const/4 v2, 0x0

    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    move v1, p0

    move v3, p1

    move v5, p2

    move-object v8, p3

    invoke-static/range {v1 .. v8}, Lcom/facebook/loom/logger/Logger;->a(ILcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJLjava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static a(IILjava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 6289
    if-eqz p2, :cond_0

    .line 6290
    const/16 v0, 0x3f

    invoke-static {p0, v0, p1, p2}, Lcom/facebook/loom/logger/Logger;->a(IIILjava/lang/String;)I

    move-result p1

    .line 6291
    :cond_0
    const/16 v0, 0x40

    invoke-static {p0, v0, p1, p3}, Lcom/facebook/loom/logger/Logger;->a(IIILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static a(ILcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJLjava/lang/String;)I
    .locals 3
    .param p1    # Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, -0x1

    .line 6255
    if-eq p0, v0, :cond_0

    invoke-static {p0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6256
    :goto_0
    return v0

    :cond_0
    invoke-static/range {p1 .. p7}, Lcom/facebook/loom/logger/Logger;->a(Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJLjava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJLjava/lang/String;)I
    .locals 2
    .param p0    # Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 6214
    if-eqz p6, :cond_0

    .line 6215
    invoke-static {p1, p3, p6}, Lcom/facebook/loom/logger/Logger;->loggerWriteString(IILjava/lang/String;)I

    move-result v0

    .line 6216
    :goto_0
    return v0

    .line 6217
    :cond_0
    if-nez p0, :cond_1

    .line 6218
    invoke-static {p1, p2, p3, p4, p5}, Lcom/facebook/loom/logger/Logger;->loggerWrite(IIIJ)I

    move-result v0

    goto :goto_0

    .line 6219
    :cond_1
    invoke-static/range {p0 .. p5}, Lcom/facebook/loom/logger/Logger;->loggerWriteAndGetCursor(Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJ)I

    move-result v0

    goto :goto_0
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 6257
    const/16 v0, 0x44

    invoke-static {v0}, Lcom/facebook/loom/logger/Logger;->a(I)V

    .line 6258
    return-void
.end method

.method private static a(I)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 6211
    sget-boolean v0, Lcom/facebook/loom/logger/Logger;->c:Z

    if-eqz v0, :cond_0

    .line 6212
    const/4 v1, -0x1

    const-wide/16 v6, 0x0

    move v3, p0

    move v5, v4

    move-object v8, v2

    invoke-static/range {v1 .. v8}, Lcom/facebook/loom/logger/Logger;->a(ILcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJLjava/lang/String;)I

    .line 6213
    :cond_0
    return-void
.end method

.method public static a(ILjava/io/File;LX/02o;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 6220
    const-string v0, "loom"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 6221
    invoke-static {p0}, Lcom/facebook/loom/logger/NativeRingBuffer;->a(I)Lcom/facebook/loom/logger/NativeRingBuffer;

    move-result-object v0

    sput-object v0, Lcom/facebook/loom/logger/Logger;->b:Lcom/facebook/loom/logger/NativeRingBuffer;

    .line 6222
    sput-boolean v1, Lcom/facebook/loom/logger/Logger;->c:Z

    .line 6223
    sput-boolean v1, Lcom/facebook/loom/core/TraceEvents;->a:Z

    .line 6224
    sput-object p1, Lcom/facebook/loom/logger/Logger;->e:Ljava/io/File;

    .line 6225
    sput-object p2, Lcom/facebook/loom/logger/Logger;->f:LX/02o;

    .line 6226
    sput p0, Lcom/facebook/loom/logger/Logger;->g:I

    .line 6227
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/facebook/loom/logger/Logger;->d:Ljava/util/concurrent/atomic/AtomicReference;

    .line 6228
    return-void
.end method

.method public static a(JI)V
    .locals 6

    .prologue
    .line 6229
    sget-boolean v0, Lcom/facebook/loom/logger/Logger;->c:Z

    if-eqz v0, :cond_0

    .line 6230
    const/4 v0, -0x1

    const/16 v1, 0x31

    const/4 v3, 0x0

    move v2, p2

    move-wide v4, p0

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->b(IIIIJ)I

    .line 6231
    :cond_0
    return-void
.end method

.method public static a(JII)V
    .locals 6

    .prologue
    .line 6232
    sget-boolean v0, Lcom/facebook/loom/logger/Logger;->c:Z

    if-eqz v0, :cond_0

    .line 6233
    const/4 v0, -0x1

    const/16 v1, 0x30

    move v2, p3

    move v3, p2

    move-wide v4, p0

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->b(IIIIJ)I

    .line 6234
    :cond_0
    return-void
.end method

.method private static b(IIIIJ)I
    .locals 10

    .prologue
    .line 6235
    sget-object v0, Lcom/facebook/loom/logger/Logger;->b:Lcom/facebook/loom/logger/NativeRingBuffer;

    .line 6236
    if-nez v0, :cond_0

    .line 6237
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Logger enabled but buffer is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6238
    :cond_0
    sget-object v1, Lcom/facebook/loom/logger/Logger;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 6239
    invoke-static {}, Lcom/facebook/loom/logger/Logger;->f()V

    .line 6240
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/loom/logger/NativeRingBuffer;->a()Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;

    move-result-object v2

    .line 6241
    const/4 v8, 0x0

    move v1, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move-wide v6, p4

    invoke-static/range {v1 .. v8}, Lcom/facebook/loom/logger/Logger;->a(ILcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJLjava/lang/String;)I

    move-result v0

    .line 6242
    sget-object v1, Lcom/facebook/loom/logger/Logger;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v2}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 6243
    return v0
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 6244
    const/16 v0, 0x2f

    invoke-static {v0}, Lcom/facebook/loom/logger/Logger;->a(I)V

    .line 6245
    return-void
.end method

.method public static c()V
    .locals 1

    .prologue
    .line 6246
    const/16 v0, 0x2e

    invoke-static {v0}, Lcom/facebook/loom/logger/Logger;->a(I)V

    .line 6247
    return-void
.end method

.method public static d()V
    .locals 1

    .prologue
    .line 6248
    const/16 v0, 0x32

    invoke-static {v0}, Lcom/facebook/loom/logger/Logger;->a(I)V

    .line 6249
    return-void
.end method

.method private static f()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .prologue
    .line 6250
    sget-object v0, Lcom/facebook/loom/logger/Logger;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 6251
    :cond_0
    :goto_0
    return-void

    .line 6252
    :cond_1
    new-instance v0, Lcom/facebook/loom/logger/LoggerWorkerThread;

    sget v1, Lcom/facebook/loom/logger/Logger;->g:I

    invoke-static {v1}, Lcom/facebook/loom/logger/NativeRingBuffer;->a(I)Lcom/facebook/loom/logger/NativeRingBuffer;

    move-result-object v1

    sget-object v2, Lcom/facebook/loom/logger/Logger;->e:Ljava/io/File;

    sget-object v3, Lcom/facebook/loom/logger/Logger;->f:LX/02o;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/loom/logger/LoggerWorkerThread;-><init>(Lcom/facebook/loom/logger/NativeRingBuffer;Ljava/io/File;LX/02o;)V

    .line 6253
    sget-object v1, Lcom/facebook/loom/logger/Logger;->d:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6254
    invoke-virtual {v0}, Lcom/facebook/loom/logger/LoggerWorkerThread;->start()V

    goto :goto_0
.end method

.method private static native loggerWrite(IIIJ)I
.end method

.method private static native loggerWriteAndGetCursor(Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;IIIJ)I
.end method

.method private static native loggerWriteForThread(IIIIJ)I
.end method

.method private static native loggerWriteString(IILjava/lang/String;)I
.end method

.method private static native loggerWriteWithMonotonicTime(IIIJJ)I
.end method
