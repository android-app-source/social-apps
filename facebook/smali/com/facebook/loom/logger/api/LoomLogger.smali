.class public final Lcom/facebook/loom/logger/api/LoomLogger;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static volatile a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5823
    const/4 v0, 0x0

    sput-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 2

    .prologue
    .line 5827
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    .line 5828
    const/4 v0, 0x1

    const/16 v1, 0xe

    invoke-static {v0, v1, p0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 5829
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(II)I
    .locals 2

    .prologue
    .line 5824
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    .line 5825
    const/4 v0, 0x1

    const/16 v1, 0xc

    invoke-static {v0, v1, p1, p0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move-result v0

    .line 5826
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(IJLjava/lang/String;)I
    .locals 9

    .prologue
    .line 5793
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    .line 5794
    const/16 v0, 0x100

    const/4 v1, 0x5

    const/4 v2, 0x0

    const-string v6, "network_request_name"

    move v3, p0

    move-wide v4, p1

    move-object v7, p3

    invoke-static/range {v0 .. v7}, Lcom/facebook/loom/logger/Logger;->a(IIIIJLjava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 5795
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(J)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5820
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    .line 5821
    const/16 v0, 0x100

    const/4 v1, 0x5

    move v3, v2

    move-wide v4, p0

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-result v0

    .line 5822
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)I"
        }
    .end annotation

    .prologue
    const/16 v1, 0x800

    .line 5817
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5818
    const/4 v0, 0x0

    invoke-static {v1, p0, v0}, Lcom/facebook/loom/logger/ClassLoadLogger;->a(ILjava/lang/Class;I)I

    move-result v0

    .line 5819
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(IIIIIIIIIJ)V
    .locals 7

    .prologue
    .line 5831
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    .line 5832
    const/16 v0, 0x100

    const/16 v1, 0xb

    const v2, 0x9d0001

    move v3, p0

    move-wide/from16 v4, p9

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 5833
    const/16 v0, 0x100

    const/16 v1, 0xb

    const v2, 0x9d0002

    move v3, p1

    move-wide/from16 v4, p9

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 5834
    const/16 v0, 0x100

    const/16 v1, 0xb

    const v2, 0x9d0003

    move v3, p2

    move-wide/from16 v4, p9

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 5835
    const/16 v0, 0x100

    const/16 v1, 0xb

    const v2, 0x9d0004

    move v3, p3

    move-wide/from16 v4, p9

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 5836
    const/16 v0, 0x100

    const/16 v1, 0xb

    const v2, 0x9d0005

    move v3, p4

    move-wide/from16 v4, p9

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 5837
    const/16 v0, 0x100

    const/16 v1, 0xb

    const v2, 0x9d0006

    move v3, p5

    move-wide/from16 v4, p9

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 5838
    const/16 v0, 0x100

    const/16 v1, 0xb

    const v2, 0x9d0007

    move v3, p6

    move-wide/from16 v4, p9

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 5839
    const/16 v0, 0x100

    const/16 v1, 0xb

    const v2, 0x9d0008

    move v3, p7

    move-wide/from16 v4, p9

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 5840
    const/16 v0, 0x100

    const/16 v1, 0xb

    const v2, 0x9d0009

    move v3, p8

    move-wide/from16 v4, p9

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 5841
    :cond_0
    return-void
.end method

.method public static b(II)I
    .locals 2

    .prologue
    .line 5814
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    .line 5815
    const/4 v0, 0x1

    const/16 v1, 0xd

    invoke-static {v0, v1, p1, p0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move-result v0

    .line 5816
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static b(J)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5811
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    .line 5812
    const/16 v0, 0x100

    const/16 v1, 0xa

    move v3, v2

    move-wide v4, p0

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-result v0

    .line 5813
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static c(II)I
    .locals 2

    .prologue
    .line 5808
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    .line 5809
    const/4 v0, 0x1

    const/16 v1, 0x3c

    invoke-static {v0, v1, p1, p0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move-result v0

    .line 5810
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static c(J)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5805
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    .line 5806
    const/16 v0, 0x100

    const/16 v1, 0x8

    move v3, v2

    move-wide v4, p0

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-result v0

    .line 5807
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static d(II)I
    .locals 2

    .prologue
    .line 5802
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    .line 5803
    const/4 v0, 0x1

    const/16 v1, 0x3d

    invoke-static {v0, v1, p1, p0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move-result v0

    .line 5804
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static d(J)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5799
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    .line 5800
    const/16 v0, 0x100

    const/4 v1, 0x7

    move v3, v2

    move-wide v4, p0

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-result v0

    .line 5801
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static e(J)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5796
    sget-boolean v0, Lcom/facebook/loom/logger/api/LoomLogger;->a:Z

    if-eqz v0, :cond_0

    .line 5797
    const/16 v0, 0x100

    const/4 v1, 0x6

    move v3, v2

    move-wide v4, p0

    invoke-static/range {v0 .. v5}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    move-result v0

    .line 5798
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method
