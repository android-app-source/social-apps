.class public final Lcom/facebook/loom/logger/LogEntry$EntryType;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 9109
    const/16 v0, 0x4b

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_TYPE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "UI_INPUT_START"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "UI_INPUT_END"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "UI_UPDATE_START"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "UI_UPDATE_END"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "NET_ADDED"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "NET_ERROR"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "NET_END"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "NET_RESPONSE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "NET_RETRY"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "NET_START"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "NET_COUNTER"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "CALL_START"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "CALL_END"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "ASYNC_CALL"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SERV_CONN"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "SERV_DISCONN"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "SERV_END"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "ADAPTER_NOTIFY"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ASYNC_TASK_PRE"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "ASYNC_TASK_BACKGROUND"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "ASYNC_TASK_POST"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "ASYNC_TASK_END"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "NETWORK_OP_START"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "NETWORK_OP_END"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "MARK_FLAG"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "MARK_START"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "MARK_STOP"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "MARK_FAIL"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "MARK_CANCEL"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "MARK_PUSH"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "MARK_POP"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "LIFECYCLE_APPLICATION_START"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "LIFECYCLE_APPLICATION_END"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "LIFECYCLE_ACTIVITY_START"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "LIFECYCLE_ACTIVITY_END"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "LIFECYCLE_SERVICE_START"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "LIFECYCLE_SERVICE_END"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "LIFECYCLE_BROADCAST_RECEIVER_START"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "LIFECYCLE_BROADCAST_RECEIVER_END"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "LIFECYCLE_CONTENT_PROVIDER_START"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "LIFECYCLE_CONTENT_PROVIDER_END"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "LIFECYCLE_FRAGMENT_START"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "LIFECYCLE_FRAGMENT_END"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "LIFECYCLE_VIEW_START"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "LIFECYCLE_VIEW_END"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "TRACE_ABORT"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "TRACE_END"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "TRACE_START"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "TRACE_BACKWARDS"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "TRACE_TIMEOUT"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "COUNTER"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "STACK_FRAME"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "TEST_CLOCK_SYNC_START"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "TEST_CLOCK_SYNC_END"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "QPL_START"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "QPL_END"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "QPL_CANCEL"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "QPL_NOTE"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "TRACE_ANNOTATION"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "WAIT_START"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "WAIT_END"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "WAIT_SIGNAL"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "STRING_KEY"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "STRING_VALUE"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "QPL_TAG"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "QPL_ANNOTATION"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "TRACE_THREAD_NAME"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "TRACE_PRE_END"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "TRACE_THREAD_PRI"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "MAJOR_FAULT"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "YARN_LOST_RECORDS"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "CLASS_LOAD"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "NATIVE_STACK_FRAME"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "NATIVE_LIB_MAPPING"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/loom/logger/LogEntry$EntryType;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
