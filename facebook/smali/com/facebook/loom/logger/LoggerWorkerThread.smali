.class public Lcom/facebook/loom/logger/LoggerWorkerThread;
.super Ljava/lang/Thread;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation


# instance fields
.field private final a:Ljava/io/File;

.field private volatile b:Z

.field private volatile c:Lcom/facebook/loom/logger/NativeRingBuffer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Lcom/facebook/loom/logger/LogEntry;

.field private final e:LX/02o;

.field private final f:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/loom/logger/NativeRingBuffer;Ljava/io/File;LX/02o;)V
    .locals 1

    .prologue
    .line 22579
    const-string v0, "dextr-worker"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 22580
    new-instance v0, LX/09c;

    invoke-direct {v0, p0}, LX/09c;-><init>(Lcom/facebook/loom/logger/LoggerWorkerThread;)V

    invoke-virtual {p0, v0}, Lcom/facebook/loom/logger/LoggerWorkerThread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 22581
    new-instance v0, Lcom/facebook/loom/logger/LogEntry;

    invoke-direct {v0}, Lcom/facebook/loom/logger/LogEntry;-><init>()V

    iput-object v0, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->d:Lcom/facebook/loom/logger/LogEntry;

    .line 22582
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->b:Z

    .line 22583
    iput-object p1, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->c:Lcom/facebook/loom/logger/NativeRingBuffer;

    .line 22584
    iput-object p3, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->e:LX/02o;

    .line 22585
    iput-object p2, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->a:Ljava/io/File;

    .line 22586
    sget-object v0, Lcom/facebook/loom/logger/Logger;->a:Ljava/util/concurrent/BlockingQueue;

    move-object v0, v0

    .line 22587
    iput-object v0, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->f:Ljava/util/concurrent/BlockingQueue;

    .line 22588
    return-void
.end method

.method private a(JLjava/io/File;I)LX/038;
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 22589
    invoke-static {p1, p2}, LX/0Ps;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 22590
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd\'T\'HH-mm-ssZ"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 22591
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v3, "%s-%s%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v0, v4, v1

    const/4 v1, 0x2

    const-string v5, ".tmp"

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 22592
    const-string v2, "[^\\p{Alnum}]"

    const-string v3, "_"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 22593
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 22594
    new-instance v5, LX/09d;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v0, v1}, LX/09d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 22595
    new-instance v1, LX/038;

    iget-object v7, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->e:LX/02o;

    move-wide v2, p1

    move v6, p4

    invoke-direct/range {v1 .. v7}, LX/038;-><init>(JLjava/io/File;LX/09e;ILX/02o;)V

    return-object v1
.end method

.method private static a(LX/038;S)V
    .locals 0
    .param p0    # LX/038;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 22596
    if-eqz p0, :cond_0

    .line 22597
    invoke-virtual {p0, p1}, LX/038;->a(S)V

    .line 22598
    invoke-static {p0}, LX/09f;->a(Ljava/io/Closeable;)V

    .line 22599
    :cond_0
    return-void
.end method

.method private a(LX/09e;Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 22600
    invoke-virtual {p2}, Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;

    .line 22601
    invoke-virtual {v0, v3}, Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;->moveBackward(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 22602
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cursor %s could not move backward"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 22603
    :cond_0
    iget-object v1, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->d:Lcom/facebook/loom/logger/LogEntry;

    invoke-virtual {v0, v1}, Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;->tryReadBackward(Lcom/facebook/loom/logger/LogEntry;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 22604
    iget-object v2, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->d:Lcom/facebook/loom/logger/LogEntry;

    invoke-virtual {p1, v2}, LX/09e;->a(Lcom/facebook/loom/logger/LogEntry;)J

    .line 22605
    if-nez v1, :cond_0

    .line 22606
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 22607
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->b:Z

    .line 22608
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->c:Lcom/facebook/loom/logger/NativeRingBuffer;

    .line 22609
    return-void
.end method

.method public final run()V
    .locals 10

    .prologue
    const/16 v9, 0x31

    const/4 v2, 0x0

    .line 22610
    const/16 v0, 0x9

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 22611
    iget-object v0, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->c:Lcom/facebook/loom/logger/NativeRingBuffer;

    .line 22612
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->b:Z

    if-nez v0, :cond_2

    .line 22613
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/loom/logger/LoggerWorkerThread;->a()V

    .line 22614
    :cond_1
    return-void

    .line 22615
    :cond_2
    iget-object v3, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->f:Ljava/util/concurrent/BlockingQueue;

    move-object v1, v2

    .line 22616
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->b:Z

    if-eqz v0, :cond_1

    .line 22617
    :try_start_0
    invoke-interface {v3}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22618
    :cond_3
    iget-object v4, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->d:Lcom/facebook/loom/logger/LogEntry;

    invoke-virtual {v0, v4}, Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;->waitAndTryReadForward(Lcom/facebook/loom/logger/LogEntry;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 22619
    iget-object v5, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->d:Lcom/facebook/loom/logger/LogEntry;

    .line 22620
    iget v6, v5, Lcom/facebook/loom/logger/LogEntry;->mEntryType:I

    move v5, v6

    .line 22621
    const/16 v6, 0x30

    if-eq v5, v6, :cond_4

    if-ne v5, v9, :cond_5

    .line 22622
    :cond_4
    const/16 v6, 0x6f

    invoke-static {v1, v6}, Lcom/facebook/loom/logger/LoggerWorkerThread;->a(LX/038;S)V

    .line 22623
    iget-object v1, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->d:Lcom/facebook/loom/logger/LogEntry;

    invoke-virtual {v1}, Lcom/facebook/loom/logger/LogEntry;->f()J

    move-result-wide v6

    .line 22624
    iget-object v1, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->d:Lcom/facebook/loom/logger/LogEntry;

    invoke-virtual {v1}, Lcom/facebook/loom/logger/LogEntry;->g()I

    move-result v1

    .line 22625
    iget-object v8, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->a:Ljava/io/File;

    invoke-direct {p0, v6, v7, v8, v1}, Lcom/facebook/loom/logger/LoggerWorkerThread;->a(JLjava/io/File;I)LX/038;

    move-result-object v1

    .line 22626
    :cond_5
    if-eqz v1, :cond_9

    .line 22627
    iget-object v6, v1, LX/038;->e:LX/09e;

    move-object v6, v6

    .line 22628
    if-ne v5, v9, :cond_b

    .line 22629
    invoke-direct {p0, v6, v0}, Lcom/facebook/loom/logger/LoggerWorkerThread;->a(LX/09e;Lcom/facebook/loom/logger/NativeRingBuffer$Cursor;)V

    .line 22630
    :goto_1
    const/16 v6, 0x2e

    if-ne v5, v6, :cond_6

    .line 22631
    const/16 v6, 0x72

    invoke-static {v1, v6}, Lcom/facebook/loom/logger/LoggerWorkerThread;->a(LX/038;S)V

    move-object v1, v2

    .line 22632
    :cond_6
    const/16 v6, 0x32

    if-ne v5, v6, :cond_7

    .line 22633
    const/16 v6, 0x71

    invoke-static {v1, v6}, Lcom/facebook/loom/logger/LoggerWorkerThread;->a(LX/038;S)V

    move-object v1, v2

    .line 22634
    :cond_7
    const/16 v6, 0x2f

    if-ne v5, v6, :cond_8

    .line 22635
    invoke-static {v1}, LX/09f;->a(Ljava/io/Closeable;)V

    move-object v1, v2

    .line 22636
    :cond_8
    if-nez v1, :cond_3

    :cond_9
    move-object v0, v1

    .line 22637
    if-nez v4, :cond_a

    .line 22638
    const/16 v1, 0x70

    invoke-static {v0, v1}, Lcom/facebook/loom/logger/LoggerWorkerThread;->a(LX/038;S)V

    move-object v0, v2

    :cond_a
    move-object v1, v0

    .line 22639
    goto :goto_0

    .line 22640
    :catch_0
    new-instance v0, LX/09g;

    const-string v1, "Interrupted take() call"

    invoke-direct {v0, v1}, LX/09g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22641
    :cond_b
    iget-object v7, p0, Lcom/facebook/loom/logger/LoggerWorkerThread;->d:Lcom/facebook/loom/logger/LogEntry;

    invoke-virtual {v6, v7}, LX/09e;->a(Lcom/facebook/loom/logger/LogEntry;)J

    goto :goto_1
.end method
