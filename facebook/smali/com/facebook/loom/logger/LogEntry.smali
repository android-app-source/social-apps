.class public Lcom/facebook/loom/logger/LogEntry;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private mByteBuffer:Ljava/nio/ByteBuffer;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mBytes:[B
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mContentType:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mEntryID:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mEntryType:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21226
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/facebook/loom/logger/LogEntry;->mBytes:[B

    .line 21227
    iget-object v0, p0, Lcom/facebook/loom/logger/LogEntry;->mBytes:[B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/loom/logger/LogEntry;->mByteBuffer:Ljava/nio/ByteBuffer;

    .line 21228
    return-void
.end method

.method private static a(ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 21240
    if-nez p0, :cond_0

    .line 21241
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " access is not allowed for current contentType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21242
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 21239
    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mEntryID:I

    return v0
.end method

.method public final a([J)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 21229
    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "frames"

    invoke-static {v0, v2}, Lcom/facebook/loom/logger/LogEntry;->a(ZLjava/lang/String;)V

    .line 21230
    invoke-virtual {p0}, Lcom/facebook/loom/logger/LogEntry;->i()I

    move-result v2

    .line 21231
    const/16 v0, 0xd

    .line 21232
    :goto_1
    if-ge v1, v2, :cond_1

    .line 21233
    iget-object v3, p0, Lcom/facebook/loom/logger/LogEntry;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v4

    .line 21234
    add-int/lit8 v0, v0, 0x8

    .line 21235
    aput-wide v4, p1, v1

    .line 21236
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 21237
    goto :goto_0

    .line 21238
    :cond_1
    return v2
.end method

.method public final a([B)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 21217
    iget v2, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    if-ne v2, v0, :cond_0

    :goto_0
    const-string v2, "bytes"

    invoke-static {v0, v2}, Lcom/facebook/loom/logger/LogEntry;->a(ZLjava/lang/String;)V

    .line 21218
    invoke-virtual {p0}, Lcom/facebook/loom/logger/LogEntry;->i()I

    move-result v0

    array-length v2, p1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 21219
    iget-object v2, p0, Lcom/facebook/loom/logger/LogEntry;->mByteBuffer:Ljava/nio/ByteBuffer;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 21220
    iget-object v2, p0, Lcom/facebook/loom/logger/LogEntry;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, p1, v1, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 21221
    iget-object v0, p0, Lcom/facebook/loom/logger/LogEntry;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 21222
    return-void

    :cond_0
    move v0, v1

    .line 21223
    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 21224
    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mEntryType:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 21243
    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    return v0
.end method

.method public final d()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 21197
    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "tid"

    invoke-static {v0, v2}, Lcom/facebook/loom/logger/LogEntry;->a(ZLjava/lang/String;)V

    .line 21198
    iget-object v0, p0, Lcom/facebook/loom/logger/LogEntry;->mByteBuffer:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    if-nez v2, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    return v0

    :cond_1
    move v0, v1

    .line 21199
    goto :goto_0

    .line 21200
    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public final e()J
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 21201
    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "time"

    invoke-static {v0, v2}, Lcom/facebook/loom/logger/LogEntry;->a(ZLjava/lang/String;)V

    .line 21202
    iget-object v0, p0, Lcom/facebook/loom/logger/LogEntry;->mByteBuffer:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    if-nez v2, :cond_1

    const/4 v1, 0x4

    :cond_1
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    return-wide v0

    :cond_2
    move v0, v1

    .line 21203
    goto :goto_0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 21204
    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "longExtra"

    invoke-static {v0, v1}, Lcom/facebook/loom/logger/LogEntry;->a(ZLjava/lang/String;)V

    .line 21205
    iget-object v0, p0, Lcom/facebook/loom/logger/LogEntry;->mByteBuffer:Ljava/nio/ByteBuffer;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    return-wide v0

    .line 21206
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 21207
    iget v2, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    if-ne v2, v0, :cond_2

    :cond_0
    :goto_0
    const-string v2, "matchID"

    invoke-static {v0, v2}, Lcom/facebook/loom/logger/LogEntry;->a(ZLjava/lang/String;)V

    .line 21208
    iget-object v0, p0, Lcom/facebook/loom/logger/LogEntry;->mByteBuffer:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    if-nez v2, :cond_1

    const/16 v1, 0x10

    :cond_1
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    return v0

    :cond_2
    move v0, v1

    .line 21209
    goto :goto_0
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 21210
    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "callID"

    invoke-static {v0, v1}, Lcom/facebook/loom/logger/LogEntry;->a(ZLjava/lang/String;)V

    .line 21211
    iget-object v0, p0, Lcom/facebook/loom/logger/LogEntry;->mByteBuffer:Ljava/nio/ByteBuffer;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    return v0

    .line 21212
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 21213
    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "length"

    invoke-static {v0, v2}, Lcom/facebook/loom/logger/LogEntry;->a(ZLjava/lang/String;)V

    .line 21214
    iget-object v2, p0, Lcom/facebook/loom/logger/LogEntry;->mByteBuffer:Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/facebook/loom/logger/LogEntry;->mContentType:I

    if-ne v0, v1, :cond_2

    const/4 v0, 0x4

    :goto_1
    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    return v0

    .line 21215
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 21216
    :cond_2
    const/16 v0, 0xc

    goto :goto_1
.end method
