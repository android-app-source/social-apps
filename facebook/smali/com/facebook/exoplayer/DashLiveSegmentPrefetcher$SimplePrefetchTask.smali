.class public final Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0GN;

.field private final b:I


# direct methods
.method public constructor <init>(LX/0GN;I)V
    .locals 0

    .prologue
    .line 35033
    iput-object p1, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35034
    iput p2, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->b:I

    .line 35035
    return-void
.end method

.method private a(LX/0GM;)I
    .locals 11

    .prologue
    const/4 v3, 0x1

    .line 35036
    new-instance v0, LX/0Gg;

    iget-object v1, p1, LX/0GM;->d:Ljava/lang/String;

    iget-object v2, p1, LX/0GM;->i:LX/0Gk;

    const/4 v4, 0x0

    iget-object v5, p1, LX/0GM;->a:LX/0GK;

    invoke-virtual {v5}, LX/0GK;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget-boolean v6, v6, LX/0GN;->r:Z

    if-eqz v6, :cond_0

    .line 35037
    sget-object v6, LX/0Gi;->a:LX/0OC;

    move-object v6, v6

    .line 35038
    :goto_0
    iget-object v7, p1, LX/0GM;->j:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, LX/0Gg;-><init>(Ljava/lang/String;LX/0Gk;ZZLjava/lang/String;LX/04m;Ljava/lang/String;)V

    .line 35039
    new-instance v4, LX/0Gv;

    iget-object v5, p1, LX/0GM;->d:Ljava/lang/String;

    iget-object v6, p1, LX/0GM;->f:Landroid/net/Uri;

    const-string v8, "ExoDashLive"

    iget-object v1, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget-object v9, v1, LX/0GN;->e:LX/0GB;

    move v7, v3

    move-object v10, v0

    invoke-direct/range {v4 .. v10}, LX/0Gv;-><init>(Ljava/lang/String;Landroid/net/Uri;ZLjava/lang/String;LX/0GB;LX/0Gg;)V

    .line 35040
    new-instance v0, LX/0OA;

    iget-object v1, p1, LX/0GM;->e:Landroid/net/Uri;

    invoke-direct {v0, v1}, LX/0OA;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v4, v0}, LX/0Gv;->b(LX/0OA;)I

    move-result v0

    return v0

    .line 35041
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private a(Ljava/net/URL;)I
    .locals 5

    .prologue
    .line 35019
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 35020
    iget-object v1, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget v1, v1, LX/0GN;->k:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 35021
    iget-object v1, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget v1, v1, LX/0GN;->l:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 35022
    const v1, -0x4ab7f4f

    invoke-static {v0, v1}, LX/04e;->a(Ljava/net/URLConnection;I)V

    .line 35023
    const v1, 0x4ec82334

    invoke-static {v0, v1}, LX/04e;->b(Ljava/net/URLConnection;I)Ljava/io/InputStream;

    move-result-object v1

    .line 35024
    const/4 v0, 0x0

    .line 35025
    const/high16 v2, 0x10000

    :try_start_0
    new-array v2, v2, [B

    .line 35026
    :goto_0
    invoke-virtual {v1, v2}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 35027
    add-int/2addr v0, v3

    goto :goto_0

    .line 35028
    :cond_0
    if-eqz v1, :cond_1

    .line 35029
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 35030
    :cond_1
    return v0

    .line 35031
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 35032
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_2
    throw v0
.end method

.method private b(LX/0GM;)I
    .locals 13

    .prologue
    const/4 v4, 0x0

    .line 35007
    new-instance v0, LX/0Gg;

    iget-object v1, p1, LX/0GM;->d:Ljava/lang/String;

    iget-object v2, p1, LX/0GM;->i:LX/0Gk;

    const/4 v3, 0x1

    iget-object v5, p1, LX/0GM;->a:LX/0GK;

    invoke-virtual {v5}, LX/0GK;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget-boolean v6, v6, LX/0GN;->r:Z

    if-eqz v6, :cond_0

    .line 35008
    sget-object v6, LX/0Gi;->a:LX/0OC;

    move-object v6, v6

    .line 35009
    :goto_0
    iget-object v7, p1, LX/0GM;->j:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, LX/0Gg;-><init>(Ljava/lang/String;LX/0Gk;ZZLjava/lang/String;LX/04m;Ljava/lang/String;)V

    .line 35010
    new-instance v5, LX/0Gd;

    iget-object v6, p1, LX/0GM;->d:Ljava/lang/String;

    iget-object v7, p1, LX/0GM;->f:Landroid/net/Uri;

    const-string v1, "ExoDashLive"

    iget-object v2, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget v2, v2, LX/0GN;->k:I

    iget-object v3, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget v3, v3, LX/0GN;->l:I

    invoke-static {v1, v0, v2, v3}, LX/0Gt;->b(Ljava/lang/String;LX/04n;II)LX/0Ge;

    move-result-object v8

    iget-object v1, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget-object v9, v1, LX/0GN;->e:LX/0GB;

    move v10, v4

    move v11, v4

    move-object v12, v0

    invoke-direct/range {v5 .. v12}, LX/0Gd;-><init>(Ljava/lang/String;Landroid/net/Uri;LX/0Ge;LX/0GB;ZZLX/0Gg;)V

    .line 35011
    :try_start_0
    new-instance v0, LX/0OA;

    iget-object v1, p1, LX/0GM;->e:Landroid/net/Uri;

    invoke-direct {v0, v1}, LX/0OA;-><init>(Landroid/net/Uri;)V

    .line 35012
    invoke-virtual {v5, v0}, LX/0Gd;->a(LX/0OA;)J

    .line 35013
    const/high16 v0, 0x10000

    new-array v1, v0, [B

    move v0, v4

    .line 35014
    :goto_1
    const/4 v2, -0x1

    if-eq v4, v2, :cond_1

    .line 35015
    const/4 v2, 0x0

    const/high16 v3, 0x10000

    invoke-virtual {v5, v1, v2, v3}, LX/0Gd;->a([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 35016
    add-int/2addr v0, v4

    goto :goto_1

    .line 35017
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 35018
    :cond_1
    invoke-virtual {v5}, LX/0Gd;->a()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {v5}, LX/0Gd;->a()V

    throw v0
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 34976
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget-object v0, v0, LX/0GN;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GM;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 34977
    iget-object v1, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget-boolean v1, v1, LX/0GN;->q:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget-object v1, v1, LX/0GN;->e:LX/0GB;

    iget-object v2, v0, LX/0GM;->d:Ljava/lang/String;

    iget-object v3, v0, LX/0GM;->e:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, LX/0GB;->a(Ljava/lang/String;Landroid/net/Uri;)[B

    move-result-object v1

    if-eqz v1, :cond_1

    .line 34978
    sget-object v1, LX/0GL;->COMPLETED:LX/0GL;

    invoke-virtual {v0, v1}, LX/0GM;->a(LX/0GL;)V

    goto :goto_0

    .line 34979
    :cond_1
    iget-object v1, v0, LX/0GM;->l:LX/0GL;

    sget-object v2, LX/0GL;->PENDING:LX/0GL;

    if-ne v1, v2, :cond_0

    .line 34980
    :try_start_1
    iget-object v1, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget-boolean v1, v1, LX/0GN;->i:Z

    if-eqz v1, :cond_2

    .line 34981
    invoke-direct {p0, v0}, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a(LX/0GM;)I

    move-result v1

    .line 34982
    :goto_1
    sget-object v2, LX/0GL;->COMPLETED:LX/0GL;

    invoke-virtual {v0, v2}, LX/0GM;->a(LX/0GL;)V

    .line 34983
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x2

    iget-object v5, v0, LX/0GM;->e:Landroid/net/Uri;

    aput-object v5, v4, v1

    const/4 v5, 0x3

    iget-object v1, v0, LX/0GM;->g:Ljava/lang/String;

    if-nez v1, :cond_4

    const-string v1, "null"

    :goto_2
    aput-object v1, v4, v5
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    .line 34984
    goto :goto_0

    .line 34985
    :catch_0
    move-exception v1

    .line 34986
    :goto_3
    instance-of v2, v1, LX/0OL;

    if-eqz v2, :cond_5

    .line 34987
    sget-object v2, LX/0GN;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid response happens while fetching "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, LX/0GM;->e:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 34988
    sget-object v2, LX/0GL;->FAILED:LX/0GL;

    invoke-virtual {v0, v2}, LX/0GM;->a(LX/0GL;)V

    .line 34989
    check-cast v1, LX/0OL;

    iget v1, v1, LX/0OL;->responseCode:I

    .line 34990
    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_0

    .line 34991
    :sswitch_0
    iget-object v1, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget-boolean v1, v1, LX/0GN;->j:Z

    if-eqz v1, :cond_0

    .line 34992
    invoke-virtual {v0}, LX/0GM;->c()V

    goto/16 :goto_0

    .line 34993
    :cond_2
    :try_start_2
    iget v1, v0, LX/0GM;->h:I

    if-lez v1, :cond_3

    .line 34994
    invoke-direct {p0, v0}, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->b(LX/0GM;)I

    move-result v1

    goto :goto_1

    .line 34995
    :cond_3
    new-instance v1, Ljava/net/URL;

    iget-object v2, v0, LX/0GM;->e:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a(Ljava/net/URL;)I

    move-result v1

    goto :goto_1

    .line 34996
    :cond_4
    iget-object v1, v0, LX/0GM;->g:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 34997
    :sswitch_1
    iget-object v1, v0, LX/0GM;->i:LX/0Gk;

    if-eqz v1, :cond_0

    .line 34998
    iget-object v1, v0, LX/0GM;->i:LX/0Gk;

    sget-object v2, LX/0H9;->MANIFEST_FETECH_END:LX/0H9;

    new-instance v3, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;

    iget-object v0, v0, LX/0GM;->d:Ljava/lang/String;

    const/16 v4, 0x19a

    invoke-direct {v3, v0, v7, v4}, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v1, v2, v3}, LX/0Gk;->a(LX/0H9;Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V

    goto/16 :goto_0

    .line 34999
    :cond_5
    iget v2, v0, LX/0GM;->k:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, LX/0GM;->k:I

    move v2, v2

    .line 35000
    if-lez v2, :cond_6

    .line 35001
    sget-object v2, LX/0GN;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error happens while fetching "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, LX/0GM;->e:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " retry remain: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, LX/0GM;->k:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 35002
    iget-object v1, p0, Lcom/facebook/exoplayer/DashLiveSegmentPrefetcher$SimplePrefetchTask;->a:LX/0GN;

    iget-object v1, v1, LX/0GN;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v0}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 35003
    :cond_6
    sget-object v2, LX/0GL;->FAILED:LX/0GL;

    invoke-virtual {v0, v2}, LX/0GM;->a(LX/0GL;)V

    .line 35004
    sget-object v2, LX/0GN;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error happens while fetching "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, LX/0GM;->e:Landroid/net/Uri;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 35005
    :catch_1
    return-void

    .line 35006
    :catch_2
    move-exception v1

    goto/16 :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x194 -> :sswitch_0
        0x19a -> :sswitch_1
    .end sparse-switch
.end method
