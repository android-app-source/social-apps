.class public final Lcom/facebook/exoplayer/PrefetchScheduler$StatusCheckTask;
.super Ljava/util/TimerTask;
.source ""


# instance fields
.field public final synthetic a:LX/0Gh;


# direct methods
.method public constructor <init>(LX/0Gh;)V
    .locals 0

    .prologue
    .line 36230
    iput-object p1, p0, Lcom/facebook/exoplayer/PrefetchScheduler$StatusCheckTask;->a:LX/0Gh;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 36231
    iget-object v1, p0, Lcom/facebook/exoplayer/PrefetchScheduler$StatusCheckTask;->a:LX/0Gh;

    monitor-enter v1

    .line 36232
    :try_start_0
    iget-object v0, p0, Lcom/facebook/exoplayer/PrefetchScheduler$StatusCheckTask;->a:LX/0Gh;

    iget-object v0, v0, LX/0Gh;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 36233
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36234
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GC;

    .line 36235
    invoke-virtual {v0}, LX/0GC;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 36236
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Prefetch is complete "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, LX/0GC;->a:Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    iget-object v0, v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36237
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 36238
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 36239
    :cond_1
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/exoplayer/PrefetchScheduler$StatusCheckTask;->a:LX/0Gh;

    iget-object v0, v0, LX/0Gh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/exoplayer/PrefetchScheduler$StatusCheckTask;->a:LX/0Gh;

    invoke-static {v0}, LX/0Gh;->f(LX/0Gh;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 36240
    iget-object v0, p0, Lcom/facebook/exoplayer/PrefetchScheduler$StatusCheckTask;->a:LX/0Gh;

    iget-object v0, v0, LX/0Gh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GC;

    .line 36241
    iget-object v2, p0, Lcom/facebook/exoplayer/PrefetchScheduler$StatusCheckTask;->a:LX/0Gh;

    iget-object v2, v2, LX/0Gh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v0}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 36242
    iget-object v2, p0, Lcom/facebook/exoplayer/PrefetchScheduler$StatusCheckTask;->a:LX/0Gh;

    iget-object v2, v2, LX/0Gh;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36243
    invoke-virtual {v0}, LX/0GC;->a()V

    goto :goto_1

    .line 36244
    :cond_2
    iget-object v0, p0, Lcom/facebook/exoplayer/PrefetchScheduler$StatusCheckTask;->a:LX/0Gh;

    invoke-static {v0}, LX/0Gh;->e(LX/0Gh;)V

    .line 36245
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
