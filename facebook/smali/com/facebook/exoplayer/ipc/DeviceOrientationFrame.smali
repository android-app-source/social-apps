.class public final Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:F

.field public b:F

.field public c:F

.field public d:[F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36809
    new-instance v0, LX/0Gx;

    invoke-direct {v0}, LX/0Gx;-><init>()V

    sput-object v0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(FFF[F)V
    .locals 0

    .prologue
    .line 36810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36811
    iput p1, p0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->a:F

    .line 36812
    iput p2, p0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->b:F

    .line 36813
    iput p3, p0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->c:F

    .line 36814
    iput-object p4, p0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->d:[F

    .line 36815
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 36816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36817
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->a:F

    .line 36818
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->b:F

    .line 36819
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->c:F

    .line 36820
    invoke-virtual {p1}, Landroid/os/Parcel;->createFloatArray()[F

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->d:[F

    .line 36821
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 36822
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 36823
    iget v0, p0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 36824
    iget v0, p0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 36825
    iget v0, p0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 36826
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;->d:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    .line 36827
    return-void
.end method
