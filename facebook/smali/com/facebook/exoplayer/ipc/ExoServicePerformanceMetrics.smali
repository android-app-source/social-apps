.class public final Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23913
    new-instance v0, LX/0Gy;

    invoke-direct {v0}, LX/0Gy;-><init>()V

    sput-object v0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23915
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 23916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23917
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->a:J

    .line 23918
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->b:J

    .line 23919
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->c:J

    .line 23920
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->d:J

    .line 23921
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 23922
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 23923
    iget-wide v0, p0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 23924
    iget-wide v0, p0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 23925
    iget-wide v0, p0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 23926
    iget-wide v0, p0, Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 23927
    return-void
.end method
