.class public final Lcom/facebook/exoplayer/ipc/VideoPlayRequest;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Closeable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.util.Log.e"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final m:Ljava/lang/String;


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Landroid/net/Uri;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Landroid/net/Uri;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Landroid/os/ParcelFileDescriptor;

.field public i:LX/0H5;

.field public j:Z

.field public k:Z

.field public l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37001
    const-class v0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->m:Ljava/lang/String;

    .line 37002
    new-instance v0, LX/0H4;

    invoke-direct {v0}, LX/0H4;-><init>()V

    sput-object v0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37000
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;LX/0H5;ZZLjava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Landroid/os/ParcelFileDescriptor;",
            "LX/0H5;",
            "ZZ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36986
    iput-object p1, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->a:Landroid/net/Uri;

    .line 36987
    iput-object p2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->b:Landroid/net/Uri;

    .line 36988
    iput-object p3, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->c:Ljava/lang/String;

    .line 36989
    iput-object p4, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->d:Ljava/lang/String;

    .line 36990
    iput-object p5, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->e:Ljava/lang/String;

    .line 36991
    iput-object p6, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->f:Landroid/net/Uri;

    .line 36992
    iput-object p7, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->g:Ljava/lang/String;

    .line 36993
    iput-object p8, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->h:Landroid/os/ParcelFileDescriptor;

    .line 36994
    iput-object p9, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->i:LX/0H5;

    .line 36995
    iput-boolean p10, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->j:Z

    .line 36996
    iput-boolean p11, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->k:Z

    .line 36997
    iput-object p12, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->l:Ljava/util/Map;

    .line 36998
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 36939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36940
    const-class v0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    .line 36941
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->a:Landroid/net/Uri;

    .line 36942
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->b:Landroid/net/Uri;

    .line 36943
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->c:Ljava/lang/String;

    .line 36944
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->d:Ljava/lang/String;

    .line 36945
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->e:Ljava/lang/String;

    .line 36946
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->f:Landroid/net/Uri;

    .line 36947
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->g:Ljava/lang/String;

    .line 36948
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->h:Landroid/os/ParcelFileDescriptor;

    .line 36949
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0H5;->valueOf(Ljava/lang/String;)LX/0H5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->i:LX/0H5;

    .line 36950
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->j:Z

    .line 36951
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->k:Z

    .line 36952
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 36953
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->l:Ljava/util/Map;

    .line 36954
    :goto_2
    if-ge v2, v0, :cond_2

    .line 36955
    iget-object v1, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->l:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36956
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_0
    move v0, v2

    .line 36957
    goto :goto_0

    :cond_1
    move v1, v2

    .line 36958
    goto :goto_1

    .line 36959
    :cond_2
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 3

    .prologue
    .line 36979
    :try_start_0
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->h:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    .line 36980
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->h:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 36981
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->h:Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36982
    :cond_0
    :goto_0
    return-void

    .line 36983
    :catch_0
    move-exception v0

    .line 36984
    sget-object v1, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->m:Ljava/lang/String;

    const-string v2, "Failed to close manifestFd"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 36978
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 36960
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 36961
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 36962
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36963
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36964
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36965
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 36966
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36967
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->h:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 36968
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->i:LX/0H5;

    invoke-virtual {v0}, LX/0H5;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36969
    iget-boolean v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->j:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 36970
    iget-boolean v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->k:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 36971
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 36972
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 36973
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36974
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    :cond_0
    move v0, v2

    .line 36975
    goto :goto_0

    :cond_1
    move v1, v2

    .line 36976
    goto :goto_1

    .line 36977
    :cond_2
    return-void
.end method
