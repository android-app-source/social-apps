.class public final Lcom/facebook/exoplayer/ipc/VideoCacheStatus;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoCacheStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public b:J

.field public c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36906
    new-instance v0, LX/0H3;

    invoke-direct {v0}, LX/0H3;-><init>()V

    sput-object v0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36908
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 36909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36910
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->a:Z

    .line 36911
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->b:J

    .line 36912
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->c:J

    .line 36913
    return-void

    .line 36914
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ZJJ)V
    .locals 0

    .prologue
    .line 36915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36916
    iput-boolean p1, p0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->a:Z

    .line 36917
    iput-wide p2, p0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->b:J

    .line 36918
    iput-wide p4, p0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->c:J

    .line 36919
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 36920
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 36921
    iget-boolean v0, p0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 36922
    iget-wide v0, p0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 36923
    iget-wide v0, p0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 36924
    return-void

    .line 36925
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
