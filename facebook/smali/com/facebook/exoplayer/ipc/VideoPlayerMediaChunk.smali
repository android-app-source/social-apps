.class public final Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public b:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

.field public final c:J

.field public final d:J

.field public final e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37026
    new-instance v0, LX/0H6;

    invoke-direct {v0}, LX/0H6;-><init>()V

    sput-object v0, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    .prologue
    const-wide/16 v4, -0x1

    .line 37024
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v8, -0x1

    move-object v1, p0

    move-wide v6, v4

    invoke-direct/range {v1 .. v8}, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;-><init>(ILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;JJI)V

    .line 37025
    return-void
.end method

.method private constructor <init>(ILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;JJI)V
    .locals 1

    .prologue
    .line 37017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37018
    iput p1, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->a:I

    .line 37019
    iput-object p2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    .line 37020
    iput-wide p3, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->c:J

    .line 37021
    iput-wide p5, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->d:J

    .line 37022
    iput p7, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->e:I

    .line 37023
    return-void
.end method

.method public constructor <init>(LX/0LQ;)V
    .locals 9

    .prologue
    .line 37015
    iget v2, p1, LX/0LP;->c:I

    new-instance v3, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iget-object v0, p1, LX/0LP;->d:LX/0AR;

    invoke-direct {v3, v0}, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;-><init>(LX/0AR;)V

    iget-wide v4, p1, LX/0LQ;->h:J

    iget-wide v6, p1, LX/0LQ;->i:J

    iget v8, p1, LX/0LQ;->j:I

    move-object v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;-><init>(ILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;JJI)V

    .line 37016
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 9

    .prologue
    .line 37006
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const-class v0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    move-object v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;-><init>(ILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;JJI)V

    .line 37007
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 37014
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 37008
    iget v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 37009
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 37010
    iget-wide v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 37011
    iget-wide v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 37012
    iget v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 37013
    return-void
.end method
