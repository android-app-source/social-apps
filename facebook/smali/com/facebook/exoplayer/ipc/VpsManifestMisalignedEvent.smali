.class public Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;
.super Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 24119
    invoke-direct {p0}, Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;-><init>()V

    .line 24120
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->a:Ljava/lang/String;

    .line 24121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->b:Ljava/lang/String;

    .line 24122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->c:Ljava/lang/String;

    .line 24123
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->d:Ljava/lang/String;

    .line 24124
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24132
    invoke-direct {p0}, Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;-><init>()V

    .line 24133
    iput-object p1, p0, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->a:Ljava/lang/String;

    .line 24134
    iput-object p2, p0, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->b:Ljava/lang/String;

    .line 24135
    iput-object p3, p0, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->c:Ljava/lang/String;

    .line 24136
    iput-object p4, p0, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->d:Ljava/lang/String;

    .line 24137
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 24131
    sget-object v0, LX/0H9;->MANIFEST_MISALIGNED:LX/0H9;

    iget v0, v0, LX/0H9;->mValue:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 24125
    invoke-super {p0, p1, p2}, Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 24126
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24127
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24128
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24129
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24130
    return-void
.end method
