.class public Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;
.super Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:I

.field public final f:J

.field public final g:J

.field public final h:I

.field public final i:LX/0AC;

.field public final j:Z

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 24138
    invoke-direct {p0}, Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;-><init>()V

    .line 24139
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->a:Ljava/lang/String;

    .line 24140
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->b:Ljava/lang/String;

    .line 24141
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->c:Ljava/lang/String;

    .line 24142
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->d:Z

    .line 24143
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->e:I

    .line 24144
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->f:J

    .line 24145
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->g:J

    .line 24146
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->h:I

    .line 24147
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/0AC;->fromValue(I)LX/0AC;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->i:LX/0AC;

    .line 24148
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->j:Z

    .line 24149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->k:Ljava/lang/String;

    .line 24150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->l:Ljava/lang/String;

    .line 24151
    return-void

    :cond_0
    move v0, v2

    .line 24152
    goto :goto_0

    :cond_1
    move v1, v2

    .line 24153
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIJJILX/0AC;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24154
    invoke-direct {p0}, Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;-><init>()V

    .line 24155
    iput-object p1, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->a:Ljava/lang/String;

    .line 24156
    iput-object p2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->b:Ljava/lang/String;

    .line 24157
    iput-object p3, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->c:Ljava/lang/String;

    .line 24158
    iput-boolean p4, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->d:Z

    .line 24159
    iput p5, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->e:I

    .line 24160
    iput-wide p6, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->f:J

    .line 24161
    iput-wide p8, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->g:J

    .line 24162
    iput p10, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->h:I

    .line 24163
    iput-object p11, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->i:LX/0AC;

    .line 24164
    iput-boolean p12, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->j:Z

    .line 24165
    iput-object p13, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->k:Ljava/lang/String;

    .line 24166
    iput-object p14, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->l:Ljava/lang/String;

    .line 24167
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 24168
    sget-object v0, LX/0H9;->HTTP_TRANSFER_END:LX/0H9;

    iget v0, v0, LX/0H9;->mValue:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 24169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 24170
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "videoId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24171
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "url="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24172
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "exception="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24173
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isPrefetch="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24174
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "readBytes="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24175
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startDuration="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->f:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24176
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "endDuration="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->g:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24177
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "seqNum="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24178
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cacheType="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->i:LX/0AC;

    iget v2, v2, LX/0AC;->value:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24179
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isFirstPlay="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->j:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24180
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "playOrigin="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24181
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "debugInfo="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24182
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 24183
    invoke-super {p0, p1, p2}, Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 24184
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24185
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24186
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24187
    iget-boolean v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 24188
    iget v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 24189
    iget-wide v4, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->f:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 24190
    iget-wide v4, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->g:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 24191
    iget v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 24192
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->i:LX/0AC;

    if-nez v0, :cond_1

    sget-object v0, LX/0AC;->NOT_APPLY:LX/0AC;

    iget v0, v0, LX/0AC;->value:I

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 24193
    iget-boolean v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->j:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 24194
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24195
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24196
    return-void

    :cond_0
    move v0, v2

    .line 24197
    goto :goto_0

    .line 24198
    :cond_1
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;->i:LX/0AC;

    iget v0, v0, LX/0AC;->value:I

    goto :goto_1

    :cond_2
    move v1, v2

    .line 24199
    goto :goto_2
.end method
