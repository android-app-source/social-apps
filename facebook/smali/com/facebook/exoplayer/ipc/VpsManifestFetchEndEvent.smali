.class public Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;
.super Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:I


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 37389
    invoke-direct {p0}, Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;-><init>()V

    .line 37390
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;->a:Ljava/lang/String;

    .line 37391
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;->b:Z

    .line 37392
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;->c:I

    .line 37393
    return-void

    .line 37394
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;ZI)V
    .locals 0

    .prologue
    .line 37395
    invoke-direct {p0}, Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;-><init>()V

    .line 37396
    iput-object p1, p0, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;->a:Ljava/lang/String;

    .line 37397
    iput-boolean p2, p0, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;->b:Z

    .line 37398
    iput p3, p0, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;->c:I

    .line 37399
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 37400
    sget-object v0, LX/0H9;->MANIFEST_FETECH_END:LX/0H9;

    iget v0, v0, LX/0H9;->mValue:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 37401
    invoke-super {p0, p1, p2}, Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 37402
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37403
    iget-boolean v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 37404
    iget v0, p0, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 37405
    return-void

    .line 37406
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
