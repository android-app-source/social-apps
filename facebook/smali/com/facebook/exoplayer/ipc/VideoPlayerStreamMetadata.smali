.class public Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37386
    new-instance v0, LX/0HF;

    invoke-direct {v0}, LX/0HF;-><init>()V

    sput-object v0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(IILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37380
    iput p1, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->a:I

    .line 37381
    iput p2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->b:I

    .line 37382
    iput-object p3, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    .line 37383
    iput-object p4, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->d:Ljava/lang/String;

    .line 37384
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 37387
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const-class v0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;-><init>(IILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;Ljava/lang/String;)V

    .line 37388
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37377
    iput-object p1, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->d:Ljava/lang/String;

    .line 37378
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 37371
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 37372
    iget v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 37373
    iget v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 37374
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 37375
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37376
    return-void
.end method
