.class public final Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:D

.field public final c:D


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36885
    new-instance v0, LX/0H1;

    invoke-direct {v0}, LX/0H1;-><init>()V

    sput-object v0, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0H2;)V
    .locals 2

    .prologue
    .line 36886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36887
    iget-boolean v0, p1, LX/0H2;->a:Z

    iput-boolean v0, p0, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->a:Z

    .line 36888
    iget-wide v0, p1, LX/0H2;->b:D

    iput-wide v0, p0, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->b:D

    .line 36889
    iget-wide v0, p1, LX/0H2;->c:D

    iput-wide v0, p0, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->c:D

    .line 36890
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 36891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36892
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->a:Z

    .line 36893
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->b:D

    .line 36894
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->c:D

    .line 36895
    return-void

    .line 36896
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 36897
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 36898
    iget-boolean v0, p0, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 36899
    iget-wide v0, p0, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->b:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 36900
    iget-wide v0, p0, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;->c:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 36901
    return-void

    .line 36902
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
