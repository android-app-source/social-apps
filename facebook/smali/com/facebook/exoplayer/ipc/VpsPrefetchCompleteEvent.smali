.class public Lcom/facebook/exoplayer/ipc/VpsPrefetchCompleteEvent;
.super Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/exoplayer/ipc/VideoCacheStatus;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 37414
    invoke-direct {p0}, Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;-><init>()V

    .line 37415
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsPrefetchCompleteEvent;->a:Ljava/lang/String;

    .line 37416
    new-instance v0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;

    invoke-direct {v0, p1}, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsPrefetchCompleteEvent;->b:Lcom/facebook/exoplayer/ipc/VideoCacheStatus;

    .line 37417
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoCacheStatus;)V
    .locals 0

    .prologue
    .line 37418
    invoke-direct {p0}, Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;-><init>()V

    .line 37419
    iput-object p1, p0, Lcom/facebook/exoplayer/ipc/VpsPrefetchCompleteEvent;->a:Ljava/lang/String;

    .line 37420
    iput-object p2, p0, Lcom/facebook/exoplayer/ipc/VpsPrefetchCompleteEvent;->b:Lcom/facebook/exoplayer/ipc/VideoCacheStatus;

    .line 37421
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 37422
    sget-object v0, LX/0H9;->PREFETCH_COMPLETE:LX/0H9;

    iget v0, v0, LX/0H9;->mValue:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 37423
    invoke-super {p0, p1, p2}, Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 37424
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsPrefetchCompleteEvent;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37425
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VpsPrefetchCompleteEvent;->b:Lcom/facebook/exoplayer/ipc/VideoCacheStatus;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->writeToParcel(Landroid/os/Parcel;I)V

    .line 37426
    return-void
.end method
