.class public final Lcom/facebook/exoplayer/ipc/VideoPlayerSession;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/net/Uri;

.field public c:Landroid/net/Uri;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/exoplayer/ipc/MediaRenderer;

.field public f:Lcom/facebook/exoplayer/ipc/MediaRenderer;

.field public g:Landroid/net/Uri;

.field public h:LX/0H5;

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37297
    new-instance v0, LX/0HB;

    invoke-direct {v0}, LX/0HB;-><init>()V

    sput-object v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37296
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    .line 37280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37281
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->a:Ljava/lang/String;

    .line 37282
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->b:Landroid/net/Uri;

    .line 37283
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->c:Landroid/net/Uri;

    .line 37284
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->d:Ljava/lang/String;

    .line 37285
    const-class v0, Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/exoplayer/ipc/MediaRenderer;

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->e:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    .line 37286
    const-class v0, Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/exoplayer/ipc/MediaRenderer;

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->f:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    .line 37287
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    .line 37288
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0H5;->valueOf(Ljava/lang/String;)LX/0H5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->h:LX/0H5;

    .line 37289
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 37290
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->i:Ljava/util/Map;

    .line 37291
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 37292
    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->i:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37293
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37294
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/MediaRenderer;Landroid/net/Uri;LX/0H5;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Lcom/facebook/exoplayer/ipc/MediaRenderer;",
            "Lcom/facebook/exoplayer/ipc/MediaRenderer;",
            "Landroid/net/Uri;",
            "LX/0H5;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37299
    iput-object p1, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->a:Ljava/lang/String;

    .line 37300
    iput-object p2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->b:Landroid/net/Uri;

    .line 37301
    iput-object p3, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->c:Landroid/net/Uri;

    .line 37302
    iput-object p4, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->d:Ljava/lang/String;

    .line 37303
    iput-object p5, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->e:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    .line 37304
    iput-object p6, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->f:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    .line 37305
    iput-object p7, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    .line 37306
    iput-object p8, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->h:LX/0H5;

    .line 37307
    iput-object p9, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->i:Ljava/util/Map;

    .line 37308
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 37279
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 37269
    instance-of v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    if-nez v2, :cond_0

    .line 37270
    :goto_0
    return v1

    .line 37271
    :cond_0
    check-cast p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 37272
    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->c:Landroid/net/Uri;

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->b:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->b:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 37273
    :goto_1
    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->a:Ljava/lang/String;

    if-nez v2, :cond_5

    :cond_1
    move v2, v0

    .line 37274
    :goto_2
    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    if-nez v2, :cond_6

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    if-nez v2, :cond_7

    :cond_2
    :goto_3
    move v1, v0

    .line 37275
    goto :goto_0

    .line 37276
    :cond_3
    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->c:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_1

    .line 37277
    :cond_4
    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_5
    move v2, v1

    goto :goto_2

    .line 37278
    :cond_6
    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 37250
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->c:Landroid/net/Uri;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    .line 37251
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 37252
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 37253
    return v0

    .line 37254
    :cond_1
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 37255
    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 37256
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37257
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 37258
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 37259
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37260
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->e:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 37261
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->f:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 37262
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->g:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 37263
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->h:LX/0H5;

    invoke-virtual {v0}, LX/0H5;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37264
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 37265
    iget-object v0, p0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 37266
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37267
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 37268
    :cond_0
    return-void
.end method
