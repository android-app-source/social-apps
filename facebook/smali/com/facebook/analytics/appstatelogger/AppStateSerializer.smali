.class public Lcom/facebook/analytics/appstatelogger/AppStateSerializer;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4743
    const/4 v0, 0x0

    sput-boolean v0, Lcom/facebook/analytics/appstatelogger/AppStateSerializer;->a:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 4741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4742
    return-void
.end method

.method public static a(Ljava/io/OutputStream;LX/01F;)V
    .locals 9

    .prologue
    .line 4707
    new-instance v0, Ljava/io/OutputStreamWriter;

    const-string v1, "UTF-8"

    invoke-direct {v0, p0, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 4708
    new-instance v3, Landroid/util/JsonWriter;

    invoke-direct {v3, v0}, Landroid/util/JsonWriter;-><init>(Ljava/io/Writer;)V

    .line 4709
    invoke-virtual {v3}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 4710
    iget-object v0, p1, LX/01F;->a:Ljava/lang/String;

    move-object v0, v0

    .line 4711
    const-string v1, "processName"

    invoke-virtual {v3, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 4712
    iget-object v0, p1, LX/01F;->b:Ljava/lang/String;

    move-object v0, v0

    .line 4713
    const-string v1, "appVersionName"

    invoke-virtual {v3, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 4714
    iget v0, p1, LX/01F;->c:I

    move v0, v0

    .line 4715
    const-string v1, "appVersionCode"

    invoke-virtual {v3, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v1

    int-to-long v4, v0

    invoke-virtual {v1, v4, v5}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 4716
    iget-wide v7, p1, LX/01F;->g:J

    move-wide v0, v7

    .line 4717
    const-string v2, "aslCreationTime"

    invoke-virtual {v3, v2}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 4718
    iget-boolean v0, p1, LX/01F;->e:Z

    move v0, v0

    .line 4719
    const-string v1, "startedInBackground"

    invoke-virtual {v3, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/util/JsonWriter;->value(Z)Landroid/util/JsonWriter;

    .line 4720
    iget-object v0, p1, LX/01F;->d:LX/01J;

    move-object v4, v0

    .line 4721
    const-string v0, "activities"

    invoke-virtual {v3, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/JsonWriter;->beginArray()Landroid/util/JsonWriter;

    .line 4722
    invoke-virtual {v4}, LX/01J;->size()I

    move-result v5

    .line 4723
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_0

    .line 4724
    invoke-virtual {v4, v2}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 4725
    invoke-virtual {v4, v2}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 4726
    invoke-virtual {v3}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 4727
    const-string v6, "name"

    invoke-virtual {v3, v6}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 4728
    const-string v0, "state"

    invoke-virtual {v3, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    invoke-static {v1}, LX/01K;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 4729
    invoke-virtual {v3}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    .line 4730
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 4731
    :cond_0
    invoke-virtual {v3}, Landroid/util/JsonWriter;->endArray()Landroid/util/JsonWriter;

    .line 4732
    const-string v0, "freeInternalDiskSpace"

    invoke-virtual {v3, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    .line 4733
    iget-wide v7, p1, LX/01F;->f:J

    move-wide v4, v7

    .line 4734
    invoke-virtual {v0, v4, v5}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 4735
    sget-boolean v0, Lcom/facebook/analytics/appstatelogger/AppStateSerializer;->a:Z

    if-eqz v0, :cond_1

    .line 4736
    invoke-virtual {v3}, Landroid/util/JsonWriter;->flush()V

    .line 4737
    invoke-static {p0}, Lcom/facebook/analytics/appstatelogger/AppStateSerializer;->appendSmapInfo(Ljava/io/OutputStream;)V

    .line 4738
    :cond_1
    invoke-virtual {v3}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    .line 4739
    invoke-virtual {v3}, Landroid/util/JsonWriter;->flush()V

    .line 4740
    return-void
.end method

.method private static native appendSmapInfo(Ljava/io/OutputStream;)V
.end method
