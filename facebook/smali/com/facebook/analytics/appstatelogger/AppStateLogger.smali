.class public Lcom/facebook/analytics/appstatelogger/AppStateLogger;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "StringFormatUse",
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation

.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:Lcom/facebook/analytics/appstatelogger/AppStateLogger;


# instance fields
.field public final c:Ljava/io/File;

.field private final d:LX/01F;

.field private final e:Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;

.field private f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2837
    const-class v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(LX/00G;Ljava/lang/String;IZLjava/io/File;)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .prologue
    .line 2829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2830
    iput-object p5, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->c:Ljava/io/File;

    .line 2831
    iget-object v0, p1, LX/00G;->b:Ljava/lang/String;

    move-object v2, v0

    .line 2832
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long v6, v0, v4

    .line 2833
    new-instance v1, LX/01F;

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v7}, LX/01F;-><init>(Ljava/lang/String;Ljava/lang/String;IZJ)V

    iput-object v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->d:LX/01F;

    .line 2834
    new-instance v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;

    iget-object v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->d:LX/01F;

    invoke-direct {v0, v1, p5}, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;-><init>(LX/01F;Ljava/io/File;)V

    iput-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->e:Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;

    .line 2835
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->e:Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;

    invoke-virtual {v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->start()V

    .line 2836
    return-void
.end method

.method private a(Landroid/app/Application;)V
    .locals 1

    .prologue
    .line 2826
    new-instance v0, LX/01G;

    invoke-direct {v0, p0}, LX/01G;-><init>(Lcom/facebook/analytics/appstatelogger/AppStateLogger;)V

    .line 2827
    invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 2828
    return-void
.end method

.method public static a(Landroid/app/Application;LX/00G;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2746
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->b:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    if-eqz v0, :cond_0

    .line 2747
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "An application has already been registered with AppStateLogger"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2748
    :cond_0
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 2749
    new-instance v1, Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Ljava/util/UUID;-><init>(JJ)V

    .line 2750
    const-string v0, "state_logs"

    invoke-virtual {p0, v0, v6}, Landroid/app/Application;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 2751
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".txt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2752
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2753
    invoke-virtual {p0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2754
    invoke-virtual {p0}, Landroid/app/Application;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 2755
    new-instance v2, LX/01H;

    invoke-direct {v2, v0, v1}, LX/01H;-><init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)V

    .line 2756
    invoke-virtual {p0}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 2757
    invoke-virtual {v2, v0, v6}, LX/01H;->d(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 2758
    if-eqz v0, :cond_1

    .line 2759
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 2760
    iget v3, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 2761
    :goto_0
    new-instance v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    move-object v1, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;-><init>(LX/00G;Ljava/lang/String;IZLjava/io/File;)V

    .line 2762
    invoke-direct {v0, p0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a(Landroid/app/Application;)V

    .line 2763
    invoke-static {v0}, LX/01I;->a(Lcom/facebook/analytics/appstatelogger/AppStateLogger;)V

    .line 2764
    sput-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->b:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    .line 2765
    return-void

    .line 2766
    :cond_1
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v1, "Could not find package info"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2767
    const-string v2, "UNKNOWN"

    .line 2768
    const/4 v3, -0x1

    goto :goto_0
.end method

.method private static a(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 2823
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 2824
    invoke-static {v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->registerWithNativeCrashHandler(Ljava/lang/String;)V

    .line 2825
    return-void
.end method

.method public static a(Z)V
    .locals 2

    .prologue
    .line 2819
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->b:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    if-eqz v0, :cond_0

    .line 2820
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->b:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    iget-object v0, v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->e:Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;

    invoke-virtual {v0, p0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->a(Z)V

    .line 2821
    :goto_0
    return-void

    .line 2822
    :cond_0
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v1, "AppStateLogger is not ready yet"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/analytics/appstatelogger/AppStateLogger;Landroid/app/Activity;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2806
    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    .line 2807
    iget-object v3, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->d:LX/01F;

    .line 2808
    iget-object v4, v3, LX/01F;->d:LX/01J;

    move-object v3, v4

    .line 2809
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2810
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v0

    invoke-static {p2}, LX/01K;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    .line 2811
    iget v2, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->f:I

    .line 2812
    const/4 v3, 0x3

    if-ne p2, v3, :cond_3

    .line 2813
    iget v3, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->f:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->f:I

    .line 2814
    :cond_0
    :goto_0
    iget v3, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->f:I

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->f:I

    if-lez v3, :cond_2

    if-nez v2, :cond_2

    :cond_1
    move v0, v1

    .line 2815
    :cond_2
    iget-object v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->e:Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;

    iget-object v2, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->d:LX/01F;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->a(LX/01F;Z)V

    .line 2816
    return-void

    .line 2817
    :cond_3
    const/4 v3, 0x4

    if-ne p2, v3, :cond_0

    .line 2818
    iget v3, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->f:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->f:I

    goto :goto_0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 2802
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->b:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    if-nez v0, :cond_0

    .line 2803
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v1, "No application has been registered with AppStateLogger"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2804
    :goto_0
    return-void

    .line 2805
    :cond_0
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->b:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    iget-object v0, v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->e:Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;

    invoke-virtual {v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->c()V

    goto :goto_0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 2801
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->b:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()Ljava/io/File;
    .locals 2

    .prologue
    .line 2796
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->b:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    if-nez v0, :cond_0

    .line 2797
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No application has been registered with AppStateLogger"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2798
    :cond_0
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->b:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    .line 2799
    iget-object v1, v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->c:Ljava/io/File;

    move-object v0, v1

    .line 2800
    return-object v0
.end method

.method public static e()V
    .locals 2

    .prologue
    .line 2792
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->b:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    if-nez v0, :cond_0

    .line 2793
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Application needs to be registered before native crash reporting"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2794
    :cond_0
    sget-object v0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->b:Lcom/facebook/analytics/appstatelogger/AppStateLogger;

    invoke-direct {v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->i()V

    .line 2795
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2786
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->c:Ljava/io/File;

    .line 2787
    const-string v1, "appstatelogger"

    invoke-static {v1}, LX/01L;->a(Ljava/lang/String;)V

    .line 2788
    invoke-static {v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a(Ljava/io/File;)V

    .line 2789
    invoke-static {}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->k()V

    .line 2790
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->e:Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;

    invoke-virtual {v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->b()V

    .line 2791
    return-void
.end method

.method public static j()J
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 2778
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    .line 2779
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 2780
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v0, v2, :cond_0

    .line 2781
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v0

    .line 2782
    :goto_0
    return-wide v0

    .line 2783
    :cond_0
    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v2, v0

    .line 2784
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v0, v0

    .line 2785
    mul-long/2addr v0, v2

    goto :goto_0
.end method

.method private static k()V
    .locals 3

    .prologue
    .line 2774
    :try_start_0
    invoke-static {}, Lcom/facebook/breakpad/BreakpadManager;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/System;->mapLibraryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->registerStreamWithBreakpad(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2775
    :goto_0
    return-void

    .line 2776
    :catch_0
    move-exception v0

    .line 2777
    sget-object v1, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v2, "registerAppStateLoggerStreamWithBreakpad failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static native registerStreamWithBreakpad(Ljava/lang/String;)V
.end method

.method private static native registerWithNativeCrashHandler(Ljava/lang/String;)V
.end method

.method public static native setBreakpadStreamData([B)V
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2769
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->e:Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;

    invoke-virtual {v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->a()V

    .line 2770
    :try_start_0
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->e:Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;

    invoke-virtual {v0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2771
    :goto_0
    return-void

    .line 2772
    :catch_0
    move-exception v0

    .line 2773
    sget-object v1, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v2, "Interrupted joining worker thread"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
