.class public final Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field private final a:Ljava/io/File;

.field private b:LX/01F;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/Object;

.field private g:Z

.field private h:Z

.field private i:J


# direct methods
.method public constructor <init>(LX/01F;Ljava/io/File;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4071
    const-string v0, "PersistToDiskThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 4072
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->c:Z

    .line 4073
    iput-boolean v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->d:Z

    .line 4074
    iput-boolean v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->e:Z

    .line 4075
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->f:Ljava/lang/Object;

    .line 4076
    iget-boolean v0, p1, LX/01F;->e:Z

    move v0, v0

    .line 4077
    iput-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->g:Z

    .line 4078
    iput-object p2, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->a:Ljava/io/File;

    .line 4079
    new-instance v0, LX/01F;

    invoke-direct {v0, p1}, LX/01F;-><init>(LX/01F;)V

    iput-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->b:LX/01F;

    .line 4080
    const-wide/32 v0, 0x15f90

    iput-wide v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->i:J

    .line 4081
    return-void
.end method

.method private static d(Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;)V
    .locals 2

    .prologue
    .line 4067
    iget-object v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 4068
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->g:Z

    .line 4069
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->f:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 4070
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 4059
    monitor-enter p0

    .line 4060
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->d:Z

    .line 4061
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->c:Z

    .line 4062
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 4063
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4064
    invoke-static {p0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->d(Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;)V

    .line 4065
    return-void

    .line 4066
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/01F;Z)V
    .locals 1

    .prologue
    .line 3956
    monitor-enter p0

    .line 3957
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->c:Z

    .line 3958
    new-instance v0, LX/01F;

    invoke-direct {v0, p1}, LX/01F;-><init>(LX/01F;)V

    iput-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->b:LX/01F;

    .line 3959
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 3960
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3961
    if-eqz p2, :cond_0

    .line 3962
    invoke-static {p0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->d(Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;)V

    .line 3963
    :cond_0
    return-void

    .line 3964
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 4050
    iget-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->e:Z

    if-ne p1, v0, :cond_0

    .line 4051
    :goto_0
    return-void

    .line 4052
    :cond_0
    monitor-enter p0

    .line 4053
    :try_start_0
    iput-boolean p1, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->e:Z

    .line 4054
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->c:Z

    .line 4055
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 4056
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4057
    invoke-static {p0}, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->d(Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;)V

    goto :goto_0

    .line 4058
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 4046
    iget-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->h:Z

    if-eqz v0, :cond_0

    .line 4047
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Native crash reporting is already initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4048
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->h:Z

    .line 4049
    return-void
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 4043
    monitor-enter p0

    const-wide/16 v0, 0x3e8

    :try_start_0
    iput-wide v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4044
    monitor-exit p0

    return-void

    .line 4045
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final run()V
    .locals 11

    .prologue
    .line 3965
    :try_start_0
    sget-object v0, LX/0TP;->NORMAL:LX/0TP;

    invoke-virtual {v0}, LX/0TP;->getAndroidThreadPriority()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_b

    .line 3966
    :goto_0
    const/4 v1, 0x0

    .line 3967
    :try_start_1
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 3968
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3969
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3970
    sget-object v2, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v3, "Unable to create app state log directory: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3971
    :cond_0
    :goto_1
    return-void

    .line 3972
    :cond_1
    new-instance v2, LX/01N;

    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->a:Ljava/io/File;

    invoke-direct {v2, v0}, LX/01N;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 3973
    const/4 v0, 0x1

    :try_start_2
    invoke-virtual {v2, v0}, LX/01N;->a(I)V

    .line 3974
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_a
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3975
    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 3976
    :goto_2
    iget-object v3, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->f:Ljava/lang/Object;

    monitor-enter v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3977
    :goto_3
    :try_start_4
    iget-boolean v4, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->g:Z

    .line 3978
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->g:Z

    .line 3979
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 3980
    sub-long/2addr v6, v0

    .line 3981
    iget-wide v8, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->i:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    sub-long v6, v8, v6

    .line 3982
    if-nez v4, :cond_2

    const-wide/16 v4, 0x0

    cmp-long v4, v6, v4

    if-lez v4, :cond_2

    .line 3983
    :try_start_5
    iget-object v4, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->f:Ljava/lang/Object;

    invoke-virtual {v4, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 3984
    :catch_0
    move-exception v0

    .line 3985
    :try_start_6
    sget-object v1, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v4, "Interrupted while sleeping"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3986
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 3987
    :try_start_7
    invoke-virtual {v2}, LX/01N;->b()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_1

    .line 3988
    :catch_1
    move-exception v0

    .line 3989
    sget-object v1, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v2, "Failed to close log file"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 3990
    :catch_2
    move-exception v0

    .line 3991
    :goto_4
    :try_start_8
    sget-object v2, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v3, "Error opening app state logging file"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    .line 3992
    if-eqz v1, :cond_0

    .line 3993
    :try_start_9
    invoke-virtual {v1}, LX/01N;->b()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_1

    .line 3994
    :catch_3
    move-exception v0

    .line 3995
    sget-object v1, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v2, "Failed to close log file"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 3996
    :cond_2
    :try_start_a
    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 3997
    :try_start_b
    monitor-enter p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 3998
    :goto_5
    :try_start_c
    iget-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->c:Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    if-nez v0, :cond_4

    .line 3999
    :try_start_d
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    goto :goto_5

    .line 4000
    :catch_4
    move-exception v0

    .line 4001
    :try_start_e
    sget-object v1, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v3, "Interrupted while waiting for updated app state"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4002
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 4003
    :try_start_f
    invoke-virtual {v2}, LX/01N;->b()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_5

    goto :goto_1

    .line 4004
    :catch_5
    move-exception v0

    .line 4005
    sget-object v1, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v2, "Failed to close log file"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 4006
    :catchall_0
    move-exception v0

    :try_start_10
    monitor-exit v3
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :try_start_11
    throw v0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 4007
    :catchall_1
    move-exception v0

    :goto_6
    if-eqz v2, :cond_3

    .line 4008
    :try_start_12
    invoke-virtual {v2}, LX/01N;->b()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_9

    .line 4009
    :cond_3
    :goto_7
    throw v0

    .line 4010
    :cond_4
    :try_start_13
    new-instance v0, LX/01F;

    iget-object v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->b:LX/01F;

    invoke-direct {v0, v1}, LX/01F;-><init>(LX/01F;)V

    .line 4011
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->c:Z

    .line 4012
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 4013
    :try_start_14
    iget-boolean v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->e:Z

    if-nez v1, :cond_6

    .line 4014
    const/4 v1, 0x2

    invoke-virtual {v2, v1}, LX/01N;->a(I)V

    .line 4015
    new-instance v1, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 4016
    iget-boolean v3, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->h:Z

    if-eqz v3, :cond_5

    .line 4017
    new-instance v3, LX/01O;

    invoke-direct {v3}, LX/01O;-><init>()V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4018
    :cond_5
    invoke-virtual {v2}, LX/01N;->a()Ljava/io/OutputStream;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4019
    new-instance v3, LX/01P;

    invoke-direct {v3, v1}, LX/01P;-><init>(Ljava/util/ArrayList;)V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_6
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 4020
    :try_start_15
    invoke-static {}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->j()J

    move-result-wide v4

    .line 4021
    iput-wide v4, v0, LX/01F;->f:J

    .line 4022
    invoke-static {v3, v0}, Lcom/facebook/analytics/appstatelogger/AppStateSerializer;->a(Ljava/io/OutputStream;LX/01F;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    .line 4023
    :try_start_16
    invoke-virtual {v3}, LX/01P;->close()V

    .line 4024
    const/4 v0, 0x3

    invoke-virtual {v2, v0}, LX/01N;->a(I)V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_6
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    .line 4025
    :goto_8
    :try_start_17
    iget-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->d:Z

    if-nez v0, :cond_7

    .line 4026
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    move-result-wide v0

    goto/16 :goto_2

    .line 4027
    :catchall_2
    move-exception v0

    :try_start_18
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    :try_start_19
    throw v0
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    .line 4028
    :catchall_3
    move-exception v0

    :try_start_1a
    invoke-virtual {v3}, LX/01P;->close()V

    throw v0
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_6
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    .line 4029
    :catch_6
    move-exception v0

    .line 4030
    :try_start_1b
    sget-object v1, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v3, "Error dumping app state to log file"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    goto :goto_8

    .line 4031
    :cond_6
    const/16 v0, 0x14

    :try_start_1c
    invoke-virtual {v2, v0}, LX/01N;->a(I)V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_6
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    goto :goto_8

    .line 4032
    :cond_7
    :try_start_1d
    iget-boolean v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateLogger$PersistToDiskThread;->d:Z
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_1

    if-eqz v0, :cond_8

    .line 4033
    const/4 v0, 0x4

    :try_start_1e
    invoke-virtual {v2, v0}, LX/01N;->a(I)V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_8
    .catchall {:try_start_1e .. :try_end_1e} :catchall_1

    .line 4034
    :cond_8
    :goto_9
    :try_start_1f
    invoke-virtual {v2}, LX/01N;->b()V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_7

    goto/16 :goto_1

    .line 4035
    :catch_7
    move-exception v0

    .line 4036
    sget-object v1, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v2, "Failed to close log file"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 4037
    :catch_8
    move-exception v0

    .line 4038
    :try_start_20
    sget-object v1, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v3, "Error updating log file state when application crashed"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_1

    goto :goto_9

    .line 4039
    :catch_9
    move-exception v1

    .line 4040
    sget-object v2, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a:Ljava/lang/String;

    const-string v3, "Failed to close log file"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_7

    .line 4041
    :catchall_4
    move-exception v0

    move-object v2, v1

    goto/16 :goto_6

    :catchall_5
    move-exception v0

    move-object v2, v1

    goto/16 :goto_6

    .line 4042
    :catch_a
    move-exception v0

    move-object v1, v2

    goto/16 :goto_4

    :catch_b
    goto/16 :goto_0
.end method
