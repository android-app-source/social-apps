.class public Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25952
    new-instance v0, LX/0Bu;

    invoke-direct {v0}, LX/0Bu;-><init>()V

    sput-object v0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 25953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25954
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->a:Ljava/lang/String;

    .line 25955
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->b:Ljava/lang/String;

    .line 25956
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->c:Ljava/lang/String;

    .line 25957
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->d:Ljava/lang/String;

    .line 25958
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->e:J

    .line 25959
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->f:[B

    .line 25960
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J[B)V
    .locals 1

    .prologue
    .line 25961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25962
    iput-object p1, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->a:Ljava/lang/String;

    .line 25963
    iput-object p2, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->b:Ljava/lang/String;

    .line 25964
    iput-object p3, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->c:Ljava/lang/String;

    .line 25965
    iput-object p4, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->d:Ljava/lang/String;

    .line 25966
    iput-wide p5, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->e:J

    .line 25967
    iput-object p7, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->f:[B

    .line 25968
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 25969
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 25970
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 25971
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 25972
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 25973
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 25974
    iget-wide v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 25975
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->f:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 25976
    return-void
.end method
