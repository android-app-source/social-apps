.class public final Lcom/facebook/jni/NativeSoftErrorReporterProxy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/03Z;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;

.field private static e:Ljava/lang/String;

.field private static f:Ljava/lang/String;

.field private static g:LX/0VL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6546
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->b:Ljava/util/LinkedList;

    .line 6547
    const-string v0, "<level:warning> "

    sput-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->c:Ljava/lang/String;

    .line 6548
    const-string v0, "<level:mustfix> "

    sput-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->d:Ljava/lang/String;

    .line 6549
    const-string v0, "<level:assert> "

    sput-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->e:Ljava/lang/String;

    .line 6550
    const-string v0, "<level:unknown> "

    sput-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->f:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 6545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()LX/03R;
    .locals 1

    .prologue
    .line 6544
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->g:LX/0VL;

    if-nez v0, :cond_0

    sget-object v0, LX/03R;->UNSET:LX/03R;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->g:LX/0VL;

    invoke-virtual {v0}, LX/0VL;->a()LX/03R;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 6522
    packed-switch p0, :pswitch_data_0

    .line 6523
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->f:Ljava/lang/String;

    :goto_0
    return-object v0

    .line 6524
    :pswitch_0
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->c:Ljava/lang/String;

    goto :goto_0

    .line 6525
    :pswitch_1
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->d:Ljava/lang/String;

    goto :goto_0

    .line 6526
    :pswitch_2
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(ILjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 6543
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[Native] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a(LX/03V;LX/0VL;)V
    .locals 2

    .prologue
    .line 6551
    const-class v1, Lcom/facebook/jni/NativeSoftErrorReporterProxy;

    monitor-enter v1

    :try_start_0
    sput-object p1, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->g:LX/0VL;

    .line 6552
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    .line 6553
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a:Ljava/lang/ref/WeakReference;

    .line 6554
    invoke-static {}, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6555
    :cond_0
    monitor-exit v1

    return-void

    .line 6556
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V
    .locals 3

    .prologue
    .line 6538
    const-class v1, Lcom/facebook/jni/NativeSoftErrorReporterProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0x32

    if-ne v0, v2, :cond_0

    .line 6539
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 6540
    :cond_0
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->b:Ljava/util/LinkedList;

    new-instance v2, LX/03Z;

    invoke-direct {v2, p0, p1, p2, p3}, LX/03Z;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6541
    monitor-exit v1

    return-void

    .line 6542
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized b()V
    .locals 8

    .prologue
    .line 6527
    const-class v2, Lcom/facebook/jni/NativeSoftErrorReporterProxy;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 6528
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 6529
    if-eqz v0, :cond_1

    .line 6530
    invoke-static {}, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a()LX/03R;

    move-result-object v3

    .line 6531
    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v3, v1, :cond_0

    .line 6532
    sget-object v1, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03Z;

    .line 6533
    iget-object v5, v1, LX/03Z;->a:Ljava/lang/String;

    iget-object v6, v1, LX/03Z;->b:Ljava/lang/String;

    iget-object v7, v1, LX/03Z;->d:Ljava/lang/Throwable;

    iget v1, v1, LX/03Z;->c:I

    invoke-virtual {v0, v5, v6, v7, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 6534
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 6535
    :cond_0
    :try_start_1
    sget-object v0, LX/03R;->UNSET:LX/03R;

    if-eq v3, v0, :cond_1

    .line 6536
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6537
    :cond_1
    monitor-exit v2

    return-void
.end method

.method public static native generateNativeSoftError()V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public static softReport(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 6520
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->softReport(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 6521
    return-void
.end method

.method public static softReport(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V
    .locals 3
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 6512
    const/4 v0, 0x0

    .line 6513
    sget-object v1, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 6514
    sget-object v0, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 6515
    :cond_0
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a()LX/03R;

    move-result-object v1

    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-ne v1, v2, :cond_3

    .line 6516
    :cond_1
    invoke-static {p0, p1}, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2, p3, p4}, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 6517
    :cond_2
    :goto_0
    return-void

    .line 6518
    :cond_3
    invoke-static {}, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a()LX/03R;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_2

    .line 6519
    invoke-static {p0, p1}, Lcom/facebook/jni/NativeSoftErrorReporterProxy;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    goto :goto_0
.end method
