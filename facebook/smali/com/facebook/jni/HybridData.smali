.class public Lcom/facebook/jni/HybridData;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private mNativePointer:J
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6496
    const-string v0, "fb"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 6497
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 6498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6499
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/jni/HybridData;->mNativePointer:J

    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 0

    .prologue
    .line 6500
    invoke-virtual {p0}, Lcom/facebook/jni/HybridData;->resetNative()V

    .line 6501
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 6502
    return-void
.end method

.method public isValid()Z
    .locals 4

    .prologue
    .line 6503
    iget-wide v0, p0, Lcom/facebook/jni/HybridData;->mNativePointer:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public native resetNative()V
.end method
