.class public Lcom/facebook/jni/ThreadScopeSupport;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6507
    const-string v0, "fb"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 6508
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static runStdFunction(J)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 6509
    invoke-static {p0, p1}, Lcom/facebook/jni/ThreadScopeSupport;->runStdFunctionImpl(J)V

    .line 6510
    return-void
.end method

.method private static native runStdFunctionImpl(J)V
.end method
