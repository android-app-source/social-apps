.class public final Lcom/facebook/debug/logcat/raw/LogcatProcess$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0G4;


# direct methods
.method public constructor <init>(LX/0G4;)V
    .locals 0

    .prologue
    .line 34204
    iput-object p1, p0, Lcom/facebook/debug/logcat/raw/LogcatProcess$1;->a:LX/0G4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 34205
    :try_start_0
    iget-object v0, p0, Lcom/facebook/debug/logcat/raw/LogcatProcess$1;->a:LX/0G4;

    iget-object v0, v0, LX/0G4;->b:Ljava/lang/Process;

    invoke-virtual {v0}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    .line 34206
    const/16 v1, 0x1000

    new-array v1, v1, [B

    .line 34207
    :goto_0
    invoke-virtual {v0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-ltz v2, :cond_0

    .line 34208
    iget-object v2, p0, Lcom/facebook/debug/logcat/raw/LogcatProcess$1;->a:LX/0G4;

    monitor-enter v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34209
    :try_start_1
    iget-object v3, p0, Lcom/facebook/debug/logcat/raw/LogcatProcess$1;->a:LX/0G4;

    iget-object v3, v3, LX/0G4;->d:LX/0G3;

    sget-object v4, LX/0G3;->RUNNING:LX/0G3;

    if-eq v3, v4, :cond_1

    .line 34210
    monitor-exit v2

    .line 34211
    :cond_0
    :goto_1
    return-void

    .line 34212
    :cond_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 34213
    :catch_0
    iget-object v0, p0, Lcom/facebook/debug/logcat/raw/LogcatProcess$1;->a:LX/0G4;

    iget-object v0, v0, LX/0G4;->b:Ljava/lang/Process;

    invoke-virtual {v0}, Ljava/lang/Process;->destroy()V

    .line 34214
    iget-object v0, p0, Lcom/facebook/debug/logcat/raw/LogcatProcess$1;->a:LX/0G4;

    sget-object v1, LX/0G3;->KILLED:LX/0G3;

    .line 34215
    iput-object v1, v0, LX/0G4;->d:LX/0G3;

    .line 34216
    goto :goto_1
.end method
