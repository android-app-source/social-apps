.class public final Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field private exception:Ljava/lang/Throwable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mGenerator:LX/01i;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mInMemoryReportToSend:LX/01l;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mReportFileUnderConstruction:LX/04Q;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mReportTypesToSend:[LX/01V;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final synthetic this$0:LX/009;


# direct methods
.method public constructor <init>(LX/009;LX/01l;LX/04Q;)V
    .locals 1
    .param p3    # LX/04Q;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 13244
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;-><init>(LX/009;LX/01l;LX/04Q;[LX/01V;)V

    .line 13245
    return-void
.end method

.method private constructor <init>(LX/009;LX/01l;LX/04Q;[LX/01V;)V
    .locals 1
    .param p2    # LX/01l;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/04Q;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [LX/01V;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 13204
    iput-object p1, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->this$0:LX/009;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 13205
    iput-object v0, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->exception:Ljava/lang/Throwable;

    .line 13206
    iput-object v0, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mGenerator:LX/01i;

    .line 13207
    iput-object p2, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mInMemoryReportToSend:LX/01l;

    .line 13208
    iput-object p3, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mReportFileUnderConstruction:LX/04Q;

    .line 13209
    iput-object p4, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mReportTypesToSend:[LX/01V;

    .line 13210
    return-void
.end method

.method public varargs constructor <init>(LX/009;[LX/01V;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13242
    invoke-direct {p0, p1, v0, v0, p2}, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;-><init>(LX/009;LX/01l;LX/04Q;[LX/01V;)V

    .line 13243
    return-void
.end method


# virtual methods
.method public final doSend()V
    .locals 5

    .prologue
    .line 13220
    const/4 v1, 0x0

    .line 13221
    :try_start_0
    new-instance v0, LX/01b;

    iget-object v2, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->this$0:LX/009;

    iget-object v2, v2, LX/009;->mContext:Landroid/content/Context;

    sget-object v3, LX/00L;->LOG_TAG:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, LX/01b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 13222
    const-string v2, "android.permission.WAKE_LOCK"

    invoke-virtual {v0, v2}, LX/01b;->hasPermission(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 13223
    const/4 v0, 0x0

    .line 13224
    :goto_0
    move-object v1, v0

    .line 13225
    iget-object v0, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mInMemoryReportToSend:LX/01l;

    if-nez v0, :cond_2

    .line 13226
    iget-object v0, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->this$0:LX/009;

    const v2, 0x7fffffff

    iget-object v3, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mGenerator:LX/01i;

    iget-object v4, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mReportTypesToSend:[LX/01V;

    invoke-virtual {v0, v2, v3, v4}, LX/009;->prepareReports(ILX/01i;[LX/01V;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13227
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13228
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 13229
    :cond_1
    return-void

    .line 13230
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mInMemoryReportToSend:LX/01l;

    .line 13231
    iget-object v2, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->this$0:LX/009;

    iget-object v2, v2, LX/009;->mContext:Landroid/content/Context;

    invoke-static {v2}, LX/04R;->getProcessNameFromAms(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 13232
    const-string v3, "UPLOADED_BY_PROCESS"

    invoke-virtual {v0, v3, v2}, LX/01l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13233
    iget-object v2, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->this$0:LX/009;

    invoke-static {v2, v0}, LX/009;->sendCrashReport(LX/009;LX/01l;)V

    .line 13234
    iget-object v0, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mReportFileUnderConstruction:LX/04Q;

    if-eqz v0, :cond_0

    .line 13235
    iget-object v0, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mReportFileUnderConstruction:LX/04Q;

    iget-object v0, v0, LX/04Q;->fileName:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 13236
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 13237
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_3
    throw v0

    .line 13238
    :cond_4
    iget-object v0, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->this$0:LX/009;

    iget-object v0, v0, LX/009;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 13239
    const/4 v2, 0x1

    const-string v3, "ACRA wakelock"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 13240
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 13241
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_0
.end method

.method public final getException()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 13219
    iget-object v0, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->exception:Ljava/lang/Throwable;

    return-object v0
.end method

.method public final routeReportToFile(LX/01i;)V
    .locals 0

    .prologue
    .line 13217
    iput-object p1, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mGenerator:LX/01i;

    .line 13218
    return-void
.end method

.method public final run()V
    .locals 3

    .prologue
    .line 13211
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->doSend()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13212
    iget-object v0, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->this$0:LX/009;

    iget-object v1, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mReportFileUnderConstruction:LX/04Q;

    invoke-static {v0, v1}, LX/009;->safeClose(LX/009;Ljava/io/Closeable;)V

    .line 13213
    :goto_0
    return-void

    .line 13214
    :catch_0
    move-exception v0

    .line 13215
    :try_start_1
    iput-object v0, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->exception:Ljava/lang/Throwable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 13216
    iget-object v0, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->this$0:LX/009;

    iget-object v1, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mReportFileUnderConstruction:LX/04Q;

    invoke-static {v0, v1}, LX/009;->safeClose(LX/009;Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->this$0:LX/009;

    iget-object v2, p0, Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;->mReportFileUnderConstruction:LX/04Q;

    invoke-static {v1, v2}, LX/009;->safeClose(LX/009;Ljava/io/Closeable;)V

    throw v0
.end method
