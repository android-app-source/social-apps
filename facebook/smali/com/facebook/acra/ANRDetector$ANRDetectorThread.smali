.class public final Lcom/facebook/acra/ANRDetector$ANRDetectorThread;
.super Ljava/lang/Thread;
.source ""


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static mInstanceSingleton:Lcom/facebook/acra/ANRDetector$ANRDetectorThread;


# instance fields
.field private final mANRDetector:LX/00O;

.field private mDelay:J

.field private mErrorReporter:LX/009;

.field private mIsPaused:Z

.field private mIsRunning:Z

.field private mSendCachedReports:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23981
    const-class v0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(LX/00O;J)V
    .locals 2

    .prologue
    .line 23975
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 23976
    iput-object p1, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mANRDetector:LX/00O;

    .line 23977
    iput-wide p2, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mDelay:J

    .line 23978
    const-string v0, "ANRDetector"

    invoke-virtual {p0, v0}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->setName(Ljava/lang/String;)V

    .line 23979
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->setPriority(I)V

    .line 23980
    return-void
.end method

.method public static getInstance(LX/00O;)Lcom/facebook/acra/ANRDetector$ANRDetectorThread;
    .locals 4

    .prologue
    .line 23972
    sget-object v0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mInstanceSingleton:Lcom/facebook/acra/ANRDetector$ANRDetectorThread;

    if-nez v0, :cond_0

    .line 23973
    new-instance v0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;

    const-wide/16 v2, 0x5

    invoke-direct {v0, p0, v2, v3}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;-><init>(LX/00O;J)V

    sput-object v0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mInstanceSingleton:Lcom/facebook/acra/ANRDetector$ANRDetectorThread;

    .line 23974
    :cond_0
    sget-object v0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mInstanceSingleton:Lcom/facebook/acra/ANRDetector$ANRDetectorThread;

    return-object v0
.end method

.method private onDetectorError(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 23967
    invoke-virtual {p0}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->stopDetector()V

    .line 23968
    iget-object v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mANRDetector:LX/00O;

    const-string v1, "ANRDetectorThread.onDetectorError"

    .line 23969
    iget-object p0, v0, LX/00O;->mANRDataProvider:LX/09r;

    if-eqz p0, :cond_0

    .line 23970
    iget-object p0, v0, LX/00O;->mANRDataProvider:LX/09r;

    invoke-interface {p0, v1, p1}, LX/09r;->reportSoftError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 23971
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized pauseDetector()V
    .locals 1

    .prologue
    .line 23964
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mIsPaused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23965
    monitor-exit p0

    return-void

    .line 23966
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized run()V
    .locals 6

    .prologue
    .line 23950
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mIsRunning:Z

    if-eqz v0, :cond_2

    .line 23951
    iget-boolean v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mSendCachedReports:Z

    if-eqz v0, :cond_0

    .line 23952
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mSendCachedReports:Z

    .line 23953
    iget-object v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mErrorReporter:LX/009;

    const v1, 0x7fffffff

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [LX/01V;

    const/4 v4, 0x0

    sget-object v5, LX/01V;->CACHED_ANR_REPORT:LX/01V;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, LX/009;->prepareReports(ILX/01i;[LX/01V;)I

    .line 23954
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mIsPaused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 23955
    :try_start_1
    iget-object v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mANRDetector:LX/00O;

    iget-wide v2, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mDelay:J

    invoke-virtual {v0, v2, v3}, LX/00O;->detectANR(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23956
    :goto_1
    :try_start_2
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mDelay:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    const v2, 0x77fc5024

    invoke-static {p0, v0, v1, v2}, LX/02L;->a(Ljava/lang/Object;JI)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 23957
    :catch_0
    goto :goto_0

    .line 23958
    :catch_1
    move-exception v0

    .line 23959
    :try_start_3
    invoke-direct {p0, v0}, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->onDetectorError(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 23960
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 23961
    :cond_1
    const v0, -0x39f5d061

    :try_start_4
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 23962
    :catch_2
    goto :goto_0

    .line 23963
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized sendCachedANRReports(LX/009;)V
    .locals 1

    .prologue
    .line 23928
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mErrorReporter:LX/009;

    .line 23929
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mSendCachedReports:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23930
    monitor-exit p0

    return-void

    .line 23931
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setDelay(J)V
    .locals 1

    .prologue
    .line 23947
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mDelay:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23948
    monitor-exit p0

    return-void

    .line 23949
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized start()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .prologue
    .line 23936
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mIsPaused:Z

    .line 23937
    iget-boolean v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mIsRunning:Z

    if-eqz v0, :cond_0

    .line 23938
    const v0, -0x7023d793

    invoke-static {p0, v0}, LX/02L;->b(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23939
    :goto_0
    monitor-exit p0

    return-void

    .line 23940
    :cond_0
    :try_start_1
    invoke-super {p0}, Ljava/lang/Thread;->start()V

    .line 23941
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mIsRunning:Z

    .line 23942
    iget-object v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mANRDetector:LX/00O;

    .line 23943
    const/4 v1, -0x1

    iput v1, v0, LX/00O;->mLastTick:I

    .line 23944
    invoke-static {v0}, LX/00O;->sendMessageToHandler(LX/00O;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23945
    goto :goto_0

    .line 23946
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopDetector()V
    .locals 1

    .prologue
    .line 23932
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/facebook/acra/ANRDetector$ANRDetectorThread;->mIsRunning:Z

    .line 23933
    const v0, 0x47ea7718

    invoke-static {p0, v0}, LX/02L;->b(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23934
    monitor-exit p0

    return-void

    .line 23935
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
