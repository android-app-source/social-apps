.class public final Lcom/facebook/katana/app/FacebookSplashScreenActivity;
.super Lcom/facebook/base/init/GenericLogoSplashScreenActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2598
    const v0, 0x7f020702

    invoke-direct {p0, v0}, Lcom/facebook/base/init/GenericLogoSplashScreenActivity;-><init>(I)V

    .line 2599
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2592
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 2593
    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookSplashScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 2594
    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 2595
    const/high16 v1, 0x33000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 2596
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x500

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 2597
    :cond_0
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 2589
    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookSplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/app/FacebookApplication;

    iget-object v0, v0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    .line 2590
    const-string v1, "ColdStart/SplashScreenDisplay"

    invoke-virtual {v0, v1}, LX/005;->b(Ljava/lang/String;)V

    .line 2591
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0x20fa7924

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2581
    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookSplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/app/FacebookApplication;

    iget-object v0, v0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    .line 2582
    const-string v1, "ColdStart/SplashScreenCreate"

    const v3, 0x70000f

    invoke-virtual {v0, v1, v3}, LX/006;->a(Ljava/lang/String;I)LX/00X;

    move-result-object v3

    const/4 v1, 0x0

    .line 2583
    :try_start_0
    invoke-super {p0, p1}, Lcom/facebook/base/init/GenericLogoSplashScreenActivity;->onCreate(Landroid/os/Bundle;)V

    .line 2584
    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookSplashScreenActivity;->d()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2585
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/00X;->close()V

    .line 2586
    :cond_0
    const v0, 0x34d23b11

    invoke-static {v0, v2}, LX/02F;->c(II)V

    return-void

    .line 2587
    :catch_0
    move-exception v0

    const v1, -0x12a257f8

    :try_start_1
    invoke-static {v1, v2}, LX/02F;->c(II)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2588
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v3, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v3}, LX/00X;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    const v1, 0x39e7d62e

    invoke-static {v1, v2}, LX/02F;->c(II)V

    throw v0

    :catch_1
    move-exception v3

    invoke-static {v1, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, LX/00X;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method
