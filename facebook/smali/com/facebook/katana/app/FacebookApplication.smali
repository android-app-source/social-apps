.class public Lcom/facebook/katana/app/FacebookApplication;
.super LX/000;
.source ""

# interfaces
.implements LX/004;


# instance fields
.field public final m:LX/005;

.field private n:LX/00H;

.field private o:J

.field private p:LX/00q;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, LX/000;-><init>()V

    .line 35
    new-instance v0, LX/005;

    invoke-direct {v0}, LX/005;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    .line 36
    return-void
.end method

.method private a(ILX/005;)V
    .locals 6
    .param p2    # LX/005;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1
    or-int/lit8 v0, p1, 0x4

    .line 2
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 3
    if-eqz v1, :cond_0

    .line 4
    or-int/lit8 v0, v0, 0x1

    .line 5
    :cond_0
    invoke-static {p0, v0, p2}, LX/008;->loadAll(Landroid/content/Context;ILX/006;)V

    .line 6
    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_2

    .line 7
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v0

    new-instance v1, LX/00A;

    invoke-direct {v1}, LX/00A;-><init>()V

    invoke-virtual {v0, v1}, LX/009;->addExceptionTranslationHook(LX/00B;)V

    .line 8
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 9
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 10
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 11
    instance-of v2, v0, Ldalvik/system/PathClassLoader;

    if-eqz v2, :cond_3

    .line 12
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    check-cast v0, Ldalvik/system/PathClassLoader;

    .line 13
    const-class v2, Ldalvik/system/BaseDexClassLoader;

    const-string v3, "pathList"

    invoke-static {v0, v2, v3}, LX/00C;->getField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    move-object v2, v2

    .line 14
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "dexElements"

    invoke-static {v2, v3, v4}, LX/00C;->getField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    .line 15
    invoke-static {v5}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result p1

    .line 16
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, p1, :cond_5

    .line 17
    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p2

    .line 18
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "file"

    invoke-static {p2, v2, v3}, LX/00C;->getField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 19
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string p0, "zipFile"

    invoke-static {p2, v3, p0}, LX/00C;->getField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/zip/ZipFile;

    .line 20
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 21
    if-eqz v3, :cond_1

    .line 22
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "zipFile"

    const/4 v4, 0x0

    invoke-static {p2, v2, v3, v4}, LX/00C;->setField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 23
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Zeroed out zipFile entry corresponding to path "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v2, v3, :cond_2

    .line 25
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "initialized"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {p2, v2, v3, v4}, LX/00C;->setField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 26
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Marked as initialized entry corresponding to path "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2

    .line 27
    :cond_2
    :goto_1
    return-void

    .line 28
    :cond_3
    :try_start_1
    const-string v0, "MemoryReductionHack"

    const-string v1, "system classloader of unexpected type"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 29
    :catch_0
    const-string v0, "MemoryReductionHack"

    const-string v1, "Couldn\'t retrieve the application info"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 30
    :catch_1
    const-string v0, "MemoryReductionHack"

    const-string v1, "Couldn\'t update the Loader"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 31
    :catch_2
    const-string v0, "MemoryReductionHack"

    const-string v1, "Couldn\'t update the Loader"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 32
    :cond_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    .line 33
    :cond_5
    const-string v2, "MemoryReductionHack"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not find zipFile entry corresponding to path "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SharedPreferencesUse"
        }
    .end annotation

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 196
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "nodex"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    :cond_0
    const/4 v0, 0x2

    .line 198
    :cond_1
    :try_start_0
    const-class v1, LX/00D;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/00F;->a(Landroid/content/Context;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    return-void

    .line 200
    :catch_0
    move-exception v0

    .line 201
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b(I)V
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.util.Log.w",
            "SharedPreferencesUse"
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 202
    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v0

    invoke-virtual {v0}, LX/00G;->c()Ljava/lang/String;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_9

    .line 204
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "videoplayer"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 205
    :goto_0
    const-string v2, ""

    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v4

    invoke-virtual {v4}, LX/00G;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v3

    .line 206
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplication;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "breakpad_flags_store"

    invoke-virtual {v4, v5, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 207
    invoke-static {}, Lcom/facebook/breakpad/BreakpadManager;->c()Z

    move-result v5

    if-nez v5, :cond_1

    .line 208
    if-nez v2, :cond_0

    const-string v5, "breakpad_clone_at_install"

    invoke-interface {v4, v5, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 209
    invoke-static {}, Lcom/facebook/breakpad/BreakpadManager;->a()V

    .line 210
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/breakpad/BreakpadManager;->a(Landroid/content/Context;I)V

    .line 211
    :cond_1
    invoke-static {}, Lcom/facebook/breakpad/BreakpadManager;->c()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 212
    invoke-static {}, Lcom/facebook/breakpad/BreakpadManager;->getMinidumpFlags()J

    move-result-wide v6

    .line 213
    if-nez v2, :cond_5

    .line 214
    const-string v0, "android_crash_breakpad_minidump_size"

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 215
    if-eqz v0, :cond_4

    .line 216
    or-long v0, v6, v8

    move v10, v3

    move-wide v2, v0

    move v1, v10

    .line 217
    :goto_2
    :try_start_0
    invoke-static {v1}, Lcom/facebook/common/dextricks/DalvikInternals;->setJvmStreamEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_1

    move-wide v0, v2

    .line 218
    :goto_3
    invoke-static {v0, v1}, Lcom/facebook/breakpad/BreakpadManager;->setMinidumpFlags(J)V

    .line 219
    :cond_2
    return-void

    :cond_3
    move v2, v1

    .line 220
    goto :goto_1

    .line 221
    :cond_4
    const-wide/16 v2, -0x3

    and-long/2addr v2, v6

    .line 222
    goto :goto_2

    .line 223
    :catch_0
    move-exception v0

    .line 224
    :goto_4
    const-string v1, "FacebookApplication"

    const-string v4, "error enabling jvm stream"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-wide v0, v2

    .line 225
    goto :goto_3

    .line 226
    :cond_5
    const-string v2, "android_crash_breakpad_dump_external_process"

    invoke-interface {v4, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 227
    if-eqz v2, :cond_8

    .line 228
    const-string v2, "android_crash_breakpad_collect_stack_for_vps"

    invoke-interface {v4, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 229
    const-wide/16 v2, 0x8

    or-long/2addr v2, v6

    .line 230
    if-eqz v1, :cond_6

    if-nez v0, :cond_7

    .line 231
    :cond_6
    const-wide/16 v0, 0x20

    or-long/2addr v0, v2

    goto :goto_3

    .line 232
    :cond_7
    or-long v0, v2, v8

    .line 233
    goto :goto_3

    .line 234
    :cond_8
    const-wide/16 v0, 0x10

    or-long/2addr v0, v6

    goto :goto_3

    .line 235
    :catch_1
    move-exception v0

    goto :goto_4

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method private k()V
    .locals 5

    .prologue
    .line 236
    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookApplication;->m()LX/00H;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->n:LX/00H;

    .line 237
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 238
    if-eqz v0, :cond_4

    const-string v0, "https://b-www.facebook.com/mobile/android_beta_crash_logs/"

    .line 239
    :goto_0
    sget-boolean v1, LX/007;->j:Z

    move v1, v1

    .line 240
    if-eqz v1, :cond_0

    .line 241
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->n:LX/00H;

    invoke-virtual {v0}, LX/00H;->c()Ljava/lang/String;

    move-result-object v0

    .line 242
    const-string v1, "https://www.facebook.com/mobile/generic_android_crash_logs/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    move-object v0, v1

    .line 243
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 244
    :cond_0
    new-instance v1, LX/00J;

    .line 245
    sget-boolean v2, LX/007;->i:Z

    move v2, v2

    .line 246
    const/4 v3, 0x1

    invoke-direct {v1, p0, v0, v2, v3}, LX/00J;-><init>(Landroid/app/Application;Ljava/lang/String;ZZ)V

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "ACRA init; reportURL: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 248
    iget-object v2, v1, LX/00K;->mCrashReportUrl:Ljava/lang/String;

    move-object v2, v2

    .line 249
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v2

    .line 251
    sget-object v0, LX/00L;->mConfig:LX/00K;

    if-nez v0, :cond_1

    .line 252
    sput-object v1, LX/00L;->mConfig:LX/00K;

    .line 253
    iget-object v0, v1, LX/00K;->mApplicationContext:Landroid/content/Context;

    move-object v3, v0

    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "ACRA is enabled for "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", intializing..."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    sget-object v0, LX/00L;->mConfig:LX/00K;

    invoke-virtual {v2, v0}, LX/009;->init(LX/00K;)V

    .line 256
    const/4 v0, 0x0

    .line 257
    :try_start_0
    invoke-virtual {v2}, LX/009;->initFallible()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :goto_1
    sget-object v4, LX/00L;->mConfig:LX/00K;

    .line 259
    new-instance v1, LX/00M;

    invoke-direct {v1, v4}, LX/00M;-><init>(LX/00K;)V

    move-object v4, v1

    .line 260
    sput-object v4, LX/00L;->mReportSender:LX/00M;

    .line 261
    sget-object v4, LX/00L;->mReportSender:LX/00M;

    invoke-virtual {v2, v4}, LX/009;->setReportSender(LX/00M;)V

    .line 262
    invoke-static {v3}, LX/00L;->initSenderHost(Landroid/content/Context;)V

    .line 263
    const-string v4, "skip_cert_checks.txt"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 264
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    .line 265
    sget-object v1, LX/00L;->mReportSender:LX/00M;

    .line 266
    iput-boolean v4, v1, LX/00M;->mSkipSslCertChecks:Z

    .line 267
    invoke-virtual {v2}, LX/009;->checkReportsOnApplicationStart()V

    .line 268
    invoke-static {v2}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 269
    if-eqz v0, :cond_1

    .line 270
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LX/009;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 271
    :cond_1
    sget-object v0, LX/00L;->mConfig:LX/00K;

    move-object v0, v0

    .line 272
    iget-object v3, v0, LX/00K;->mApplicationContext:Landroid/content/Context;

    move-object v0, v3

    .line 273
    new-instance v3, LX/00N;

    invoke-direct {v3, v0, v2}, LX/00N;-><init>(Landroid/content/Context;LX/009;)V

    sput-object v3, LX/00L;->mANRReport:LX/00N;

    .line 274
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 275
    new-instance v3, LX/00O;

    new-instance v4, Lcom/facebook/acra/ANRDetectorRunnable;

    invoke-direct {v4}, Lcom/facebook/acra/ANRDetectorRunnable;-><init>()V

    sget-object v1, LX/00L;->mANRReport:LX/00N;

    invoke-direct {v3, v4, v1, v0}, LX/00O;-><init>(Lcom/facebook/acra/ANRDetectorRunnable;LX/00N;Landroid/os/Handler;)V

    .line 276
    sput-object v3, LX/00L;->mANRDetector:LX/00O;

    sget-object v0, LX/00L;->mConfig:LX/00K;

    .line 277
    iget-boolean v4, v0, LX/00K;->mIsInternalBuild:Z

    move v0, v4

    .line 278
    iput-boolean v0, v3, LX/00O;->mIsInternalBuild:Z

    .line 279
    invoke-static {}, LX/00L;->getCachedANRGKValue()Z

    move-result v3

    .line 280
    if-eqz v3, :cond_2

    .line 281
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "ANRDetector"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v3, :cond_5

    const-string v0, " "

    :goto_2
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "started in "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v3, :cond_6

    const-string v0, "production "

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "mode."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    sget-object v0, LX/00L;->mANRDetector:LX/00O;

    invoke-virtual {v0}, LX/00O;->start()V

    .line 283
    :cond_2
    move-object v0, v2

    .line 284
    new-instance v1, LX/00P;

    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v2

    .line 285
    iget-object v3, v2, LX/00G;->b:Ljava/lang/String;

    move-object v2, v3

    .line 286
    invoke-direct {v1, p0, v2}, LX/00P;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/009;->setCrashReportedObserver(LX/00P;)V

    .line 287
    new-instance v1, LX/00Q;

    invoke-direct {v1}, LX/00Q;-><init>()V

    invoke-virtual {v0, v1}, LX/009;->addExceptionTranslationHook(LX/00B;)V

    .line 288
    new-instance v1, LX/00R;

    invoke-direct {v1}, LX/00R;-><init>()V

    invoke-virtual {v0, v1}, LX/009;->addExceptionTranslationHook(LX/00B;)V

    .line 289
    const-string v1, "app"

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->n:LX/00H;

    .line 290
    iget-object v3, v2, LX/00H;->b:Ljava/lang/String;

    move-object v2, v3

    .line 291
    invoke-virtual {v0, v1, v2}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 292
    const-string v1, "fb_app_id"

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->n:LX/00H;

    invoke-virtual {v2}, LX/00H;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 293
    const-string v1, "app_backgrounded"

    new-instance v2, LX/00S;

    invoke-direct {v2, p0}, LX/00S;-><init>(Lcom/facebook/katana/app/FacebookApplication;)V

    invoke-virtual {v0, v1, v2}, LX/009;->putLazyCustomData(Ljava/lang/String;LX/00T;)V

    .line 294
    const-string v1, "app_background_stats"

    new-instance v2, LX/00U;

    invoke-direct {v2, p0}, LX/00U;-><init>(Lcom/facebook/katana/app/FacebookApplication;)V

    invoke-virtual {v0, v1, v2}, LX/009;->putLazyCustomData(Ljava/lang/String;LX/00T;)V

    .line 295
    const-string v1, "react_bundle_version"

    new-instance v2, LX/00V;

    invoke-direct {v2, p0}, LX/00V;-><init>(Lcom/facebook/katana/app/FacebookApplication;)V

    invoke-virtual {v0, v1, v2}, LX/009;->putLazyCustomData(Ljava/lang/String;LX/00T;)V

    .line 296
    const-string v1, "persisted_uid"

    new-instance v2, LX/00W;

    invoke-direct {v2, p0}, LX/00W;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, LX/009;->putLazyCustomData(Ljava/lang/String;LX/00T;)V

    .line 297
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 298
    if-eqz v1, :cond_3

    .line 299
    const-wide/32 v2, 0x100000

    invoke-virtual {v0, v2, v3}, LX/009;->setMaxReportSize(J)V

    .line 300
    :cond_3
    return-void

    .line 301
    :cond_4
    const-string v0, "https://b-www.facebook.com/mobile/android_crash_logs/"

    goto/16 :goto_0

    .line 302
    :catch_0
    move-exception v0

    goto/16 :goto_1

    .line 303
    :cond_5
    const-string v0, " not "

    goto/16 :goto_2

    :cond_6
    const-string v0, "unit test "

    goto/16 :goto_3
.end method

.method private static l()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.util.Log.w"
        }
    .end annotation

    .prologue
    .line 311
    sget-boolean v0, LX/007;->k:Z

    move v0, v0

    .line 312
    if-eqz v0, :cond_0

    .line 313
    :try_start_0
    const-string v0, "com.facebook.debug.artfix.ArtDebugFix"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 315
    :catch_0
    const-string v0, "FacebookApplication"

    const-string v1, "Unable to load ART debugger fix"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private m()LX/00H;
    .locals 3

    .prologue
    .line 304
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v1, "FacebookApplication#getAppType"

    const v2, 0x70000e

    invoke-virtual {v0, v1, v2}, LX/006;->a(Ljava/lang/String;I)LX/00X;

    .line 305
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 306
    if-eqz v0, :cond_0

    .line 307
    sget-object v0, LX/IYS;->a:LX/00H;

    sget-object v1, LX/IYS;->b:LX/00H;

    sget-object v2, LX/IYS;->c:LX/00H;

    invoke-static {v0, v1, v2}, LX/00Z;->a(LX/00H;LX/00H;LX/00H;)LX/00H;

    move-result-object v0

    .line 308
    :goto_0
    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v2, "FacebookApplication#getAppType"

    invoke-virtual {v1, v2}, LX/005;->a(Ljava/lang/String;)V

    .line 309
    return-object v0

    .line 310
    :cond_0
    sget-object v0, LX/00Y;->a:LX/00H;

    sget-object v1, LX/00Y;->b:LX/00H;

    sget-object v2, LX/00Y;->c:LX/00H;

    invoke-static {v0, v1, v2}, LX/00Z;->a(LX/00H;LX/00H;LX/00H;)LX/00H;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/app/SplashScreenActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 316
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 317
    const-string v1, "com.facebook.katana.LoginActivity"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.facebook.katana.activity.FbMainTabActivity"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    :cond_0
    sget-object v0, LX/00a;->a:LX/00a;

    move-object v0, v0

    .line 319
    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 320
    iget-object p0, v0, LX/00a;->o:LX/00b;

    sget-object p1, LX/00b;->NONE:LX/00b;

    if-ne p0, p1, :cond_1

    invoke-virtual {v0, v1}, LX/00a;->a(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_1

    iget-boolean p0, v0, LX/00a;->m:Z

    if-eqz p0, :cond_1

    .line 321
    sget-object p0, LX/00c;->a:LX/00c;

    move-object p0, p0

    .line 322
    invoke-virtual {p0, v1}, LX/00c;->a(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 323
    sget-object p0, LX/00d;->a:LX/00d;

    move-object p0, p0

    .line 324
    invoke-virtual {p0, v1}, LX/00d;->a(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 325
    invoke-static {v0}, LX/00a;->j(LX/00a;)V

    .line 326
    :cond_1
    const-class v0, Lcom/facebook/katana/app/FacebookSplashScreenActivity;

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 180
    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v0

    invoke-virtual {v0}, LX/00G;->c()Ljava/lang/String;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_0

    const-string v1, "bsod"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    :cond_0
    const v1, 0x7f0301fc

    const v2, 0x7f020701

    const v3, 0x7f08118e

    const v4, 0x7f08118f

    const v5, 0x7f08118a

    const v0, 0x7f0800c7

    invoke-virtual {p0, v0}, Lcom/facebook/katana/app/FacebookApplication;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/facebook/sosource/bsod/BSODActivity;->a(Landroid/content/Context;IIIIILjava/lang/String;)V

    .line 183
    invoke-super {p0, p1}, LX/000;->a(Ljava/lang/Throwable;)V

    .line 184
    :cond_1
    return-void
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 185
    sget-object v1, LX/00e;->b:Ljava/lang/Boolean;

    .line 186
    if-eqz v1, :cond_3

    .line 187
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 188
    :goto_0
    move v1, v1

    .line 189
    if-nez v1, :cond_1

    .line 190
    :cond_0
    :goto_1
    return v0

    .line 191
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    iget v2, p0, LX/000;->a:I

    if-eq v1, v2, :cond_2

    iget v1, p1, Landroid/os/Message;->what:I

    iget v2, p0, LX/000;->b:I

    if-eq v1, v2, :cond_2

    iget v1, p1, Landroid/os/Message;->what:I

    iget v2, p0, LX/000;->j:I

    if-eq v1, v2, :cond_2

    iget v1, p1, Landroid/os/Message;->what:I

    iget v2, p0, LX/000;->k:I

    if-eq v1, v2, :cond_2

    iget v1, p1, Landroid/os/Message;->what:I

    iget v2, p0, LX/000;->l:I

    if-ne v1, v2, :cond_0

    .line 192
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 193
    :cond_3
    const-string v1, "fb4a_should_delay_service_gk_enabled"

    invoke-static {p0, v1}, LX/00f;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 194
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, LX/00e;->b:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public final b()Lcom/facebook/base/app/ApplicationLike;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 103
    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, LX/00G;->c()Ljava/lang/String;

    move-result-object v2

    .line 105
    const-string v3, "bsod"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 106
    new-instance v0, Lcom/facebook/base/app/ApplicationLike;

    invoke-direct {v0}, Lcom/facebook/base/app/ApplicationLike;-><init>()V

    .line 107
    :goto_0
    return-object v0

    .line 108
    :cond_0
    iget-object v3, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v4, "soloader_init"

    const v5, 0x70000d

    invoke-virtual {v3, v4, v5}, LX/006;->a(Ljava/lang/String;I)LX/00X;

    move-result-object v3

    .line 109
    const/4 v4, 0x2

    :try_start_0
    invoke-virtual {p0, v4}, LX/001;->a(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/00X;->close()V

    .line 111
    :cond_1
    invoke-static {}, Lcom/facebook/katana/app/FacebookApplication;->l()V

    .line 112
    iget-object v3, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v4, "BreakpadManager_Setup"

    const v5, 0x860002

    invoke-virtual {v3, v4, v5}, LX/006;->a(Ljava/lang/String;I)LX/00X;

    move-result-object v3

    .line 113
    const/4 v4, 0x0

    :try_start_1
    invoke-direct {p0, v4}, Lcom/facebook/katana/app/FacebookApplication;->b(I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 114
    if-eqz v3, :cond_2

    invoke-virtual {v3}, LX/00X;->close()V

    .line 115
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplication;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/lyra/LyraManager;->a(Landroid/content/Context;)V

    .line 116
    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplication;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/terminateHandler/TerminateHandlerManager;->a(Landroid/content/Context;)V

    .line 117
    invoke-static {}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->e()V

    .line 118
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/crash_log"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/dextricks/DalvikInternals;->integrateWithCrashLog(Ljava/lang/String;)V

    .line 119
    sget-boolean v3, LX/00g;->yes:Z

    if-nez v3, :cond_3

    .line 120
    :try_start_2
    new-instance v3, LX/00h;

    invoke-direct {v3}, LX/00h;-><init>()V

    invoke-static {v3}, Lcom/facebook/common/dextricks/DalvikInternals;->setClassInitFailureHook(Lcom/facebook/common/dextricks/DalvikInternals$ClassInitFailureHook;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_5

    .line 121
    :cond_3
    :goto_1
    new-instance v3, LX/00i;

    invoke-direct {v3}, LX/00i;-><init>()V

    invoke-static {v3}, LX/00k;->a(LX/00j;)V

    .line 122
    if-eqz v2, :cond_4

    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 123
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplication;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    .line 124
    new-instance v3, Lcom/facebook/http/config/PreconnectionConfig$1;

    invoke-direct {v3, v2}, Lcom/facebook/http/config/PreconnectionConfig$1;-><init>(Landroid/content/Context;)V

    const v4, 0x3c3e33e6

    invoke-static {v3, v4}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 125
    :goto_2
    sget-boolean v2, LX/007;->i:Z

    move v2, v2

    .line 126
    if-eqz v2, :cond_5

    .line 127
    new-instance v2, Ljava/util/WeakHashMap;

    invoke-direct {v2}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v2, LX/00m;->a:Ljava/util/WeakHashMap;

    .line 128
    :cond_5
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_6

    .line 129
    :try_start_3
    const/4 v6, 0x0

    .line 130
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    if-nez v2, :cond_6

    .line 131
    const-string v2, "dalvik.system.VMRuntime"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 132
    const-string v3, "clearGrowthLimit"

    new-array v4, v6, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    const-string v4, "getRuntime"

    new-array v5, v6, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v4, 0x0

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_4

    .line 133
    :cond_6
    :goto_3
    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v3, "ColdStart/FBApp.createDelegate"

    const v4, 0x700007

    invoke-virtual {v2, v3, v4}, LX/006;->a(Ljava/lang/String;I)LX/00X;

    .line 134
    :try_start_4
    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v3, "ColdStart/EnsureDexsLoaded"

    const v4, 0x70000a

    invoke-virtual {v2, v3, v4}, LX/006;->a(Ljava/lang/String;I)LX/00X;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 135
    const/4 v2, 0x0

    :try_start_5
    iget-object v3, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    invoke-direct {p0, v2, v3}, Lcom/facebook/katana/app/FacebookApplication;->a(ILX/005;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 136
    :try_start_6
    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v3, "ColdStart/EnsureDexsLoaded"

    invoke-virtual {v2, v3}, LX/005;->a(Ljava/lang/String;)V

    .line 137
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-gt v2, v3, :cond_7

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 138
    invoke-static {p0}, LX/00n;->a(Landroid/content/Context;)V

    .line 139
    :cond_7
    sget-boolean v2, LX/007;->i:Z

    move v2, v2

    .line 140
    if-eqz v2, :cond_10

    sget-object v2, LX/00o;->FBANDROID_DEBUG:LX/00o;

    .line 141
    :goto_4
    iget-object v3, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v4, "ColdStart/ReplaceLinearAllocBuffer"

    const v5, 0x70000b

    invoke-virtual {v3, v4, v5}, LX/006;->a(Ljava/lang/String;I)LX/00X;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 142
    :try_start_7
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_11

    .line 143
    const/4 v3, 0x0

    .line 144
    :goto_5
    move v2, v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 145
    :try_start_8
    iget-object v3, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v4, "ColdStart/ReplaceLinearAllocBuffer"

    invoke-virtual {v3, v4}, LX/005;->a(Ljava/lang/String;)V

    .line 146
    if-nez v2, :cond_8

    .line 147
    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v3, "ColdStart/FallbackReplaceLinearAllocBuffer"

    const v4, 0x70000c

    invoke-virtual {v2, v3, v4}, LX/006;->a(Ljava/lang/String;I)LX/00X;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 148
    :try_start_9
    iget-object v2, v0, LX/00G;->b:Ljava/lang/String;

    move-object v0, v2

    .line 149
    invoke-static {p0, v0}, LX/00p;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 150
    :try_start_a
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v2, "ColdStart/FallbackReplaceLinearAllocBuffer"

    invoke-virtual {v0, v2}, LX/005;->a(Ljava/lang/String;)V

    .line 151
    :cond_8
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, LX/00H;

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/katana/app/FacebookApplication;->n:LX/00H;

    aput-object v3, v0, v2

    const/4 v2, 0x2

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v0, v2

    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/facebook/katana/app/FacebookApplication;->o:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-class v3, LX/00q;

    aput-object v3, v0, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/facebook/katana/app/FacebookApplication;->p:LX/00q;

    aput-object v3, v0, v2

    const/4 v2, 0x6

    const-class v3, LX/005;

    aput-object v3, v0, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    aput-object v3, v0, v2

    invoke-virtual {p0, v0}, LX/001;->a([Ljava/lang/Object;)Lcom/facebook/base/app/ApplicationLike;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    move-result-object v0

    .line 152
    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v3, "ColdStart/FBApp.createDelegate"

    invoke-virtual {v2, v3}, LX/005;->a(Ljava/lang/String;)V

    .line 153
    iput-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->p:LX/00q;

    goto/16 :goto_0

    .line 154
    :catch_0
    move-exception v1

    :try_start_b
    throw v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 155
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_9

    if-eqz v1, :cond_a

    :try_start_c
    invoke-virtual {v3}, LX/00X;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_1

    :cond_9
    :goto_6
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_a
    invoke-virtual {v3}, LX/00X;->close()V

    goto :goto_6

    .line 156
    :catch_2
    move-exception v1

    :try_start_d
    throw v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 157
    :catchall_1
    move-exception v0

    if-eqz v3, :cond_b

    if-eqz v1, :cond_c

    :try_start_e
    invoke-virtual {v3}, LX/00X;->close()V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_3

    :cond_b
    :goto_7
    throw v0

    :catch_3
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_7

    :cond_c
    invoke-virtual {v3}, LX/00X;->close()V

    goto :goto_7

    .line 158
    :cond_d
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 159
    if-eqz v0, :cond_e

    const-string v0, "startupstress"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 160
    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplication;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/00G;->a(Ljava/lang/String;)LX/00G;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, LX/00G;->c()Ljava/lang/String;

    .line 162
    sput-object v0, LX/00G;->a:LX/00G;

    .line 163
    goto/16 :goto_2

    .line 164
    :cond_e
    const-string v0, "optsvc"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 165
    const/16 v0, 0xa

    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/app/FacebookApplication;->a(ILX/005;)V

    .line 166
    new-instance v0, Lcom/facebook/base/app/ApplicationLike;

    invoke-direct {v0}, Lcom/facebook/base/app/ApplicationLike;-><init>()V

    goto/16 :goto_0

    .line 167
    :cond_f
    new-instance v0, Lcom/facebook/base/app/ApplicationLike;

    invoke-direct {v0}, Lcom/facebook/base/app/ApplicationLike;-><init>()V

    goto/16 :goto_0

    .line 168
    :catch_4
    move-exception v2

    .line 169
    const-string v3, "FacebookApplication"

    const-string v4, "Unable to set large heap mode"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3

    .line 170
    :catchall_2
    move-exception v0

    :try_start_f
    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v3, "ColdStart/EnsureDexsLoaded"

    invoke-virtual {v2, v3}, LX/005;->a(Ljava/lang/String;)V

    throw v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    .line 171
    :catchall_3
    move-exception v0

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v3, "ColdStart/FBApp.createDelegate"

    invoke-virtual {v2, v3}, LX/005;->a(Ljava/lang/String;)V

    .line 172
    iput-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->p:LX/00q;

    throw v0

    .line 173
    :cond_10
    :try_start_10
    sget-object v2, LX/00o;->FBANDROID_RELEASE:LX/00o;

    goto/16 :goto_4

    .line 174
    :catchall_4
    move-exception v0

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v3, "ColdStart/ReplaceLinearAllocBuffer"

    invoke-virtual {v2, v3}, LX/005;->a(Ljava/lang/String;)V

    throw v0

    .line 175
    :catchall_5
    move-exception v0

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v3, "ColdStart/FallbackReplaceLinearAllocBuffer"

    invoke-virtual {v2, v3}, LX/005;->a(Ljava/lang/String;)V

    throw v0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    .line 176
    :catch_5
    move-exception v3

    .line 177
    sget-object v4, LX/00h;->TAG:Ljava/lang/String;

    const-string v5, "failed to install class failure stapler; proceeding"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 178
    :cond_11
    invoke-static {v2}, LX/00s;->replaceBuffer(LX/00o;)V

    .line 179
    const/4 v3, 0x1

    goto/16 :goto_5
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 98
    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v0

    invoke-virtual {v0}, LX/00G;->c()Ljava/lang/String;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_0

    const-string v1, "bsod"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    :cond_0
    const v1, 0x7f0301fc

    const v2, 0x7f020701

    const v3, 0x7f08118e

    const v4, 0x7f081191

    const v5, 0x7f08118c

    const v0, 0x7f0800c7

    invoke-virtual {p0, v0}, Lcom/facebook/katana/app/FacebookApplication;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/facebook/sosource/bsod/BSODActivity;->a(Landroid/content/Context;IIIIILjava/lang/String;)V

    .line 101
    invoke-super {p0, p1}, LX/000;->a(Ljava/lang/Throwable;)V

    .line 102
    :cond_1
    return-void
.end method

.method public final b(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 94
    invoke-static {p1}, LX/00t;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_0

    .line 96
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/00u;->a(Landroid/content/Context;)LX/00u;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/00u;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :cond_0
    :goto_0
    invoke-super {p0, p1}, LX/000;->b(Landroid/content/Intent;)Z

    move-result v0

    return v0

    :catch_0
    goto :goto_0
.end method

.method public final c(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 89
    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v0

    invoke-virtual {v0}, LX/00G;->c()Ljava/lang/String;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_0

    const-string v1, "bsod"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 91
    :cond_0
    const v1, 0x7f0301fc

    const v2, 0x7f020701

    const v3, 0x7f08118e

    const v4, 0x7f081190

    const v5, 0x7f08118d

    const v0, 0x7f0800c7

    invoke-virtual {p0, v0}, Lcom/facebook/katana/app/FacebookApplication;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/facebook/sosource/bsod/BSODActivity;->a(Landroid/content/Context;IIIIILjava/lang/String;)V

    .line 92
    invoke-super {p0, p1}, LX/000;->a(Ljava/lang/Throwable;)V

    .line 93
    :cond_1
    return-void
.end method

.method public final getCacheDir()Ljava/io/File;
    .locals 2

    .prologue
    .line 82
    invoke-super {p0}, LX/000;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 83
    sget-boolean v1, LX/00v;->a:Z

    if-nez v1, :cond_1

    .line 84
    :cond_0
    :goto_0
    move-object v0, v0

    .line 85
    return-object v0

    .line 86
    :cond_1
    new-instance v1, Ljava/io/File;

    const-string p0, "browser_proc"

    invoke-direct {v1, v0, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 87
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result p0

    if-nez p0, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result p0

    if-eqz p0, :cond_0

    :cond_2
    move-object v0, v1

    .line 88
    goto :goto_0
.end method

.method public final getDir(Ljava/lang/String;I)Ljava/io/File;
    .locals 1

    .prologue
    .line 78
    sget-boolean v0, LX/00v;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "webview"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string p1, "browser_proc_webview"

    .line 80
    :cond_0
    move-object v0, p1

    .line 81
    invoke-super {p0, v0, p2}, LX/000;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final h()Z
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v1, "ColdStart/SplashScreenSetup"

    const v2, 0x700010

    invoke-virtual {v0, v1, v2}, LX/006;->a(Ljava/lang/String;I)LX/00X;

    move-result-object v2

    const/4 v1, 0x0

    .line 74
    :try_start_0
    invoke-super {p0}, LX/000;->h()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 75
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/00X;->close()V

    :cond_0
    return v0

    .line 76
    :catch_0
    move-exception v1

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/00X;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_0
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, LX/00X;->close()V

    goto :goto_0
.end method

.method public final i()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 37
    new-instance v1, LX/00q;

    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplication;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "power"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-direct {v1, v0}, LX/00q;-><init>(Landroid/os/PowerManager;)V

    iput-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->p:LX/00q;

    .line 38
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/katana/app/FacebookApplication;->o:J

    .line 39
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->p:LX/00q;

    invoke-virtual {v0}, LX/00q;->n()V

    .line 40
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v1, "ColdStart/ApplicationCreate"

    const v3, 0x700015

    invoke-virtual {v0, v1, v3}, LX/005;->c(Ljava/lang/String;I)V

    .line 41
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v1, "ColdStart/FBAppImpl.onCreate"

    const v3, 0x700006

    invoke-virtual {v0, v1, v3}, LX/005;->c(Ljava/lang/String;I)V

    .line 42
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v1, "ColdStart/SplashScreenDisplay"

    const v3, 0x700014

    invoke-virtual {v0, v1, v3}, LX/005;->c(Ljava/lang/String;I)V

    .line 43
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v1, "ColdStart/FBApp.onBaseContextAttached"

    const v3, 0x700009

    invoke-virtual {v0, v1, v3}, LX/006;->a(Ljava/lang/String;I)LX/00X;

    move-result-object v3

    .line 44
    :try_start_0
    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, LX/00G;->c()Ljava/lang/String;

    move-result-object v1

    .line 46
    sget-object v4, LX/00w;->b:LX/00w;

    move-object v4, v4

    .line 47
    invoke-virtual {v4}, LX/00w;->b()Z

    .line 48
    iget-object v4, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v5, "CatchMeIfYouCan_Setup"

    const v6, 0x860003

    invoke-virtual {v4, v5, v6}, LX/006;->a(Ljava/lang/String;I)LX/00X;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v4

    .line 49
    :try_start_1
    invoke-direct {p0, v1}, Lcom/facebook/katana/app/FacebookApplication;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    .line 50
    if-eqz v4, :cond_0

    :try_start_2
    invoke-virtual {v4}, LX/00X;->close()V

    .line 51
    :cond_0
    sget-object v1, LX/00w;->b:LX/00w;

    move-object v1, v1

    .line 52
    iget-object v4, v1, LX/00w;->e:Ljava/lang/Boolean;

    move-object v1, v4

    .line 53
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 54
    invoke-static {p0, v0, v1}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->a(Landroid/app/Application;LX/00G;Z)V

    .line 55
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->m:LX/005;

    const-string v1, "ACRA_Setup"

    const v4, 0x860001

    invoke-virtual {v0, v1, v4}, LX/006;->a(Ljava/lang/String;I)LX/00X;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v4

    .line 56
    :try_start_3
    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookApplication;->k()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 57
    if-eqz v4, :cond_1

    :try_start_4
    invoke-virtual {v4}, LX/00X;->close()V

    .line 58
    :cond_1
    invoke-static {p0}, LX/00x;->a(Landroid/content/Context;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 59
    if-eqz v3, :cond_2

    invoke-virtual {v3}, LX/00X;->close()V

    .line 60
    :cond_2
    return-void

    .line 61
    :catch_0
    move-exception v1

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 62
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v4, :cond_3

    if-eqz v1, :cond_5

    :try_start_6
    invoke-virtual {v4}, LX/00X;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :cond_3
    :goto_1
    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 63
    :catch_1
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 64
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    if-eqz v3, :cond_4

    if-eqz v2, :cond_8

    :try_start_9
    invoke-virtual {v3}, LX/00X;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_5

    :cond_4
    :goto_3
    throw v0

    .line 65
    :catch_2
    move-exception v4

    :try_start_a
    invoke-static {v1, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 66
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 67
    :cond_5
    invoke-virtual {v4}, LX/00X;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    goto :goto_1

    .line 68
    :catch_3
    move-exception v1

    :try_start_b
    throw v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 69
    :catchall_3
    move-exception v0

    :goto_4
    if-eqz v4, :cond_6

    if-eqz v1, :cond_7

    :try_start_c
    invoke-virtual {v4}, LX/00X;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :cond_6
    :goto_5
    :try_start_d
    throw v0

    :catch_4
    move-exception v4

    invoke-static {v1, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_7
    invoke-virtual {v4}, LX/00X;->close()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    goto :goto_5

    .line 70
    :catch_5
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_8
    invoke-virtual {v3}, LX/00X;->close()V

    goto :goto_3

    .line 71
    :catchall_4
    move-exception v0

    move-object v1, v2

    goto :goto_4

    .line 72
    :catchall_5
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method
