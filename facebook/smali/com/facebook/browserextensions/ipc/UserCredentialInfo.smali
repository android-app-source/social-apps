.class public Lcom/facebook/browserextensions/ipc/UserCredentialInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/ipc/UserCredentialInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/facebook/browserextensions/ipc/MailingAddressInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32637
    const-class v0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->f:Ljava/lang/String;

    .line 32638
    new-instance v0, LX/0Ev;

    invoke-direct {v0}, LX/0Ev;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Ew;)V
    .locals 1

    .prologue
    .line 32639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32640
    iget-object v0, p1, LX/0Ew;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->a:Ljava/lang/String;

    .line 32641
    iget-object v0, p1, LX/0Ew;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->b:Ljava/lang/String;

    .line 32642
    iget-object v0, p1, LX/0Ew;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->c:Ljava/lang/String;

    .line 32643
    iget-object v0, p1, LX/0Ew;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->d:Ljava/lang/String;

    .line 32644
    iget-object v0, p1, LX/0Ew;->e:Lcom/facebook/browserextensions/ipc/MailingAddressInfo;

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->e:Lcom/facebook/browserextensions/ipc/MailingAddressInfo;

    .line 32645
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 32646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32647
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->a:Ljava/lang/String;

    .line 32648
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->b:Ljava/lang/String;

    .line 32649
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->c:Ljava/lang/String;

    .line 32650
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->d:Ljava/lang/String;

    .line 32651
    const-class v0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->e:Lcom/facebook/browserextensions/ipc/MailingAddressInfo;

    .line 32652
    return-void
.end method


# virtual methods
.method public final a()Lorg/json/JSONObject;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 32653
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 32654
    :try_start_0
    const-string v1, "name"

    iget-object v2, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32655
    const-string v1, "email"

    iget-object v2, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32656
    const-string v1, "cardType"

    iget-object v2, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32657
    const-string v1, "cardLastFourDigits"

    iget-object v2, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32658
    iget-object v1, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->e:Lcom/facebook/browserextensions/ipc/MailingAddressInfo;

    if-eqz v1, :cond_0

    .line 32659
    const-string v1, "shippingAddress"

    iget-object v2, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->e:Lcom/facebook/browserextensions/ipc/MailingAddressInfo;

    .line 32660
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 32661
    :try_start_1
    const-string v4, "name"

    iget-object p0, v2, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32662
    const-string v4, "street1"

    iget-object p0, v2, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32663
    const-string v4, "street2"

    iget-object p0, v2, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32664
    const-string v4, "city"

    iget-object p0, v2, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32665
    const-string v4, "region"

    iget-object p0, v2, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32666
    const-string v4, "postalCode"

    iget-object p0, v2, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->f:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32667
    const-string v4, "country"

    iget-object p0, v2, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->g:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 32668
    :goto_0
    :try_start_2
    move-object v2, v3

    .line 32669
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 32670
    :cond_0
    :goto_1
    return-object v0

    .line 32671
    :catch_0
    sget-object v0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->f:Ljava/lang/String;

    const-string v1, "Error while serializing address!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32672
    const/4 v0, 0x0

    goto :goto_1

    .line 32673
    :catch_1
    sget-object v3, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->h:Ljava/lang/String;

    const-string v4, "Error while serializing address!"

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {v3, v4, p0}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32674
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 32675
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 32676
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32677
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32678
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32679
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32680
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->e:Lcom/facebook/browserextensions/ipc/MailingAddressInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 32681
    return-void
.end method
