.class public Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;
.super Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;
.source ""


# static fields
.field public static final CREATOR:LX/0EY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0EY",
            "<",
            "Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32583
    new-instance v0, LX/0Ep;

    invoke-direct {v0}, LX/0Ep;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;->CREATOR:LX/0EY;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 32581
    const-string v4, "requestCredentials"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 32582
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 32574
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;-><init>(Landroid/os/Parcel;)V

    .line 32575
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/browserextensions/ipc/UserCredentialInfo;)Landroid/os/Bundle;
    .locals 2
    .param p1    # Lcom/facebook/browserextensions/ipc/UserCredentialInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 32577
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 32578
    const-string v1, "callbackID"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32579
    const-string v1, "userInfo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 32580
    return-object v0
.end method


# virtual methods
.method public final h()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 32576
    const-string v0, "JS_BRIDGE_PAGE_POLICY_URL"

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
