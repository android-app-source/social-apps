.class public Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;
.super Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;
.source ""


# static fields
.field public static final CREATOR:LX/0EY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0EY",
            "<",
            "Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32616
    new-instance v0, LX/0Et;

    invoke-direct {v0}, LX/0Et;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;->CREATOR:LX/0EY;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 32617
    const-string v4, "requestUserInfoField"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 32618
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 32619
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;-><init>(Landroid/os/Parcel;)V

    .line 32620
    return-void
.end method


# virtual methods
.method public final h()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 32621
    const-string v0, "JS_BRIDGE_APP_ID"

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
