.class public Lcom/facebook/browserextensions/ipc/CardCredentialInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/ipc/CardCredentialInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32412
    const-class v0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->f:Ljava/lang/String;

    .line 32413
    new-instance v0, LX/0Ec;

    invoke-direct {v0}, LX/0Ec;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Ed;)V
    .locals 1

    .prologue
    .line 32398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32399
    iget-object v0, p1, LX/0Ed;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->a:Ljava/lang/String;

    .line 32400
    iget-object v0, p1, LX/0Ed;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->b:Ljava/lang/String;

    .line 32401
    iget-object v0, p1, LX/0Ed;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->c:Ljava/lang/String;

    .line 32402
    iget-object v0, p1, LX/0Ed;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->d:Ljava/lang/String;

    .line 32403
    iget-object v0, p1, LX/0Ed;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->e:Ljava/lang/String;

    .line 32404
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 32405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32406
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->a:Ljava/lang/String;

    .line 32407
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->b:Ljava/lang/String;

    .line 32408
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->c:Ljava/lang/String;

    .line 32409
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->d:Ljava/lang/String;

    .line 32410
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->e:Ljava/lang/String;

    .line 32411
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 32397
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 32391
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32392
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32393
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32394
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32395
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32396
    return-void
.end method
