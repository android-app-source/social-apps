.class public Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/0FA;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32897
    new-instance v0, LX/0F9;

    invoke-direct {v0}, LX/0F9;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 32898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32899
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->a:Ljava/lang/String;

    .line 32900
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0FA;->valueOf(Ljava/lang/String;)LX/0FA;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->b:LX/0FA;

    .line 32901
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->c:Ljava/lang/String;

    .line 32902
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->d:Ljava/lang/String;

    .line 32903
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->e:Ljava/lang/String;

    .line 32904
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->f:Ljava/lang/String;

    .line 32905
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->g:Ljava/lang/String;

    .line 32906
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->h:Ljava/lang/String;

    .line 32907
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 32908
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 32909
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32910
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->b:LX/0FA;

    invoke-virtual {v0}, LX/0FA;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32911
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32912
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32913
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32914
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32915
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32916
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/model/MessengerPlatformWebviewShareContentFields;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32917
    return-void
.end method
