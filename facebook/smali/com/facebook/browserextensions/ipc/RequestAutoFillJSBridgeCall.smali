.class public Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;
.super Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;
.source ""


# static fields
.field public static final CREATOR:LX/0EY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0EY",
            "<",
            "Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32556
    new-instance v0, LX/0En;

    invoke-direct {v0}, LX/0En;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;->CREATOR:LX/0EY;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 32557
    const-string v4, "requestAutoFill"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 32558
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 32559
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;-><init>(Landroid/os/Parcel;)V

    .line 32560
    return-void
.end method
