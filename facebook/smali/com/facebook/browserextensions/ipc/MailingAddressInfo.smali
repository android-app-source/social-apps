.class public Lcom/facebook/browserextensions/ipc/MailingAddressInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/ipc/MailingAddressInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32438
    const-class v0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->h:Ljava/lang/String;

    .line 32439
    new-instance v0, LX/0Eg;

    invoke-direct {v0}, LX/0Eg;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Eh;)V
    .locals 1

    .prologue
    .line 32440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32441
    iget-object v0, p1, LX/0Eh;->a:Ljava/lang/String;

    move-object v0, v0

    .line 32442
    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->a:Ljava/lang/String;

    .line 32443
    iget-object v0, p1, LX/0Eh;->b:Ljava/lang/String;

    move-object v0, v0

    .line 32444
    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->b:Ljava/lang/String;

    .line 32445
    iget-object v0, p1, LX/0Eh;->c:Ljava/lang/String;

    move-object v0, v0

    .line 32446
    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->c:Ljava/lang/String;

    .line 32447
    iget-object v0, p1, LX/0Eh;->d:Ljava/lang/String;

    move-object v0, v0

    .line 32448
    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->d:Ljava/lang/String;

    .line 32449
    iget-object v0, p1, LX/0Eh;->e:Ljava/lang/String;

    move-object v0, v0

    .line 32450
    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->e:Ljava/lang/String;

    .line 32451
    iget-object v0, p1, LX/0Eh;->f:Ljava/lang/String;

    move-object v0, v0

    .line 32452
    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->f:Ljava/lang/String;

    .line 32453
    iget-object v0, p1, LX/0Eh;->g:Ljava/lang/String;

    move-object v0, v0

    .line 32454
    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->g:Ljava/lang/String;

    .line 32455
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 32456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32457
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->a:Ljava/lang/String;

    .line 32458
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->b:Ljava/lang/String;

    .line 32459
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->c:Ljava/lang/String;

    .line 32460
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->d:Ljava/lang/String;

    .line 32461
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->e:Ljava/lang/String;

    .line 32462
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->f:Ljava/lang/String;

    .line 32463
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->g:Ljava/lang/String;

    .line 32464
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 32465
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 32466
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32467
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32468
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32469
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32470
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32471
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32472
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/MailingAddressInfo;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32473
    return-void
.end method
