.class public final Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;
.super Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Eb;

.field private c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32150
    const-class v0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a:Ljava/lang/String;

    .line 32151
    new-instance v0, LX/0Ea;

    invoke-direct {v0}, LX/0Ea;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32152
    const-string v0, "_FBExtensions"

    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;-><init>(Ljava/lang/String;)V

    .line 32153
    new-instance v0, LX/0Eb;

    invoke-direct {v0, p0}, LX/0Eb;-><init>(Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;)V

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->b:LX/0Eb;

    .line 32154
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 31999
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;-><init>(Landroid/os/Parcel;)V

    .line 32000
    new-instance v0, LX/0Eb;

    invoke-direct {v0, p0}, LX/0Eb;-><init>(Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;)V

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->b:LX/0Eb;

    .line 32001
    return-void
.end method

.method private a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 6

    .prologue
    .line 32155
    :try_start_0
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->b:LX/0Eb;

    .line 32156
    invoke-static {}, LX/0CQ;->a()LX/0CQ;

    move-result-object v1

    .line 32157
    new-instance v2, LX/0CF;

    invoke-direct {v2, v1, p1, v0}, LX/0CF;-><init>(LX/0CQ;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;LX/0Da;)V

    invoke-static {v1, v2}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 32158
    return-void

    .line 32159
    :catch_0
    move-exception v0

    .line 32160
    sget-object v1, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a:Ljava/lang/String;

    const-string v2, "Exception when invoking %s call!"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 32161
    iget-object v5, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v5, v5

    .line 32162
    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32163
    throw v0
.end method

.method public static a$redex0(Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;ILjava/lang/String;)V
    .locals 6
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 32164
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 32165
    if-nez p2, :cond_0

    .line 32166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Error code is needed for onErrorCallback"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32167
    :cond_0
    :try_start_0
    const-string v1, "errorCode"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 32168
    if-eqz p3, :cond_1

    .line 32169
    const-string v1, "errorMessage"

    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 32170
    :cond_1
    :goto_0
    const-string v1, "%s(%s, \'%s\', \'%s\');"

    iget-object v2, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 32171
    invoke-virtual {p0, p1, v0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    .line 32172
    return-void

    .line 32173
    :catch_0
    sget-object v1, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a:Ljava/lang/String;

    const-string v2, "Exception when handling error callback for %s!"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 32174
    iget-object v4, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v4, v4

    .line 32175
    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32176
    const/4 v0, 0x0

    .line 32177
    iget-object v1, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v4, v1

    .line 32178
    const/4 v1, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 32179
    :goto_1
    if-nez v0, :cond_2

    .line 32180
    new-array v0, v3, [Ljava/lang/Object;

    .line 32181
    iget-object v1, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v1, v1

    .line 32182
    aput-object v1, v0, v2

    .line 32183
    :goto_2
    return-void

    .line 32184
    :sswitch_0
    const-string v5, "requestCredentials"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    goto :goto_0

    :sswitch_1
    const-string v5, "requestAuthorizedCredentials"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v3

    goto :goto_0

    :sswitch_2
    const-string v5, "processPayment"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v5, "updateCart"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v5, "resetCart"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v5, "purchase_complete"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v5, "paymentsCheckout"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v5, "paymentsCheckoutShippingAddressReturn"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v5, "requestUserInfoField"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :sswitch_9
    const-string v5, "requestCurrentPosition"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v1, 0x9

    goto :goto_0

    :sswitch_a
    const-string v5, "getUserID"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v1, 0xa

    goto :goto_0

    :sswitch_b
    const-string v5, "requestCloseBrowser"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v1, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v5, "hasCapability"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v1, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v5, "requestAutoFill"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v5, "requestFillOfferCode"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v1, 0xe

    goto/16 :goto_0

    .line 32185
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    .line 32186
    const-class v1, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 32187
    const-string v1, "callbackID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 32188
    const-string v1, "userInfo"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;

    .line 32189
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/browserextensions/ipc/UserCredentialInfo;->a()Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    .line 32190
    :goto_3
    const-string v6, "%s(%s, \'%s\', \'%s\');"

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_4
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v6, v0, v1, v5, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 32191
    goto/16 :goto_1

    .line 32192
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    .line 32193
    const-class v1, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 32194
    const-string v1, "callbackID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 32195
    const-string v1, "cardToken"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;

    .line 32196
    if-eqz v1, :cond_5

    .line 32197
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 32198
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 32199
    :try_start_0
    const-string v7, "month"

    iget-object p2, v1, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->c:Ljava/lang/String;

    invoke-virtual {v6, v7, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32200
    const-string v7, "year"

    iget-object p2, v1, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->d:Ljava/lang/String;

    invoke-virtual {v6, v7, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32201
    const-string v7, "token"

    iget-object p2, v1, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->a:Ljava/lang/String;

    invoke-virtual {v4, v7, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32202
    const-string v7, "cardVerifier"

    iget-object p2, v1, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->b:Ljava/lang/String;

    invoke-virtual {v4, v7, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32203
    const-string v7, "token_expiry"

    invoke-virtual {v4, v7, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32204
    const-string v6, "zip_code"

    iget-object v7, v1, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->e:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 32205
    :goto_5
    move-object v4, v4

    .line 32206
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    .line 32207
    :goto_6
    const-string v6, "%s(%s, \'%s\', \'%s\');"

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :goto_7
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v6, v0, v1, v5, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 32208
    goto/16 :goto_1

    .line 32209
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    .line 32210
    const-class v1, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 32211
    const-string v1, "callbackID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 32212
    const-string v1, "payment_result"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    .line 32213
    if-nez v1, :cond_8

    .line 32214
    :goto_8
    move-object v5, v5

    .line 32215
    const-string v6, "%s(%s, \'%s\', \'%s\');"

    if-eqz v5, :cond_7

    const/4 v1, 0x1

    :goto_9
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v6, v0, v1, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 32216
    goto/16 :goto_1

    .line 32217
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    .line 32218
    const-string v1, "callbackID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32219
    if-nez v1, :cond_9

    .line 32220
    const/4 v1, 0x0

    .line 32221
    :goto_a
    move-object v0, v1

    .line 32222
    goto/16 :goto_1

    .line 32223
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    .line 32224
    const-string v1, "callbackID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32225
    if-nez v1, :cond_a

    .line 32226
    const/4 v1, 0x0

    .line 32227
    :goto_b
    move-object v0, v1

    .line 32228
    goto/16 :goto_1

    .line 32229
    :pswitch_5
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    .line 32230
    const-string v1, "callbackID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32231
    if-nez v1, :cond_b

    .line 32232
    const/4 v1, 0x0

    .line 32233
    :goto_c
    move-object v0, v1

    .line 32234
    goto/16 :goto_1

    .line 32235
    :pswitch_6
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    .line 32236
    const-string v1, "callbackID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32237
    if-nez v1, :cond_c

    .line 32238
    const/4 v1, 0x0

    .line 32239
    :goto_d
    move-object v0, v1

    .line 32240
    goto/16 :goto_1

    .line 32241
    :pswitch_7
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    .line 32242
    const-string v1, "callbackID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32243
    if-nez v1, :cond_d

    .line 32244
    const/4 v1, 0x0

    .line 32245
    :goto_e
    move-object v0, v1

    .line 32246
    goto/16 :goto_1

    .line 32247
    :pswitch_8
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    .line 32248
    const-string v1, "callbackID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32249
    if-nez v1, :cond_e

    .line 32250
    const/4 v1, 0x0

    .line 32251
    :goto_f
    move-object v0, v1

    .line 32252
    goto/16 :goto_1

    .line 32253
    :pswitch_9
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    const/4 v1, 0x0

    .line 32254
    const-string v4, "callbackID"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 32255
    if-nez v4, :cond_f

    .line 32256
    :goto_10
    move-object v0, v1

    .line 32257
    goto/16 :goto_1

    .line 32258
    :pswitch_a
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 32259
    const-string v5, "callbackID"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 32260
    if-nez v5, :cond_10

    move-object v1, v4

    .line 32261
    :goto_11
    move-object v0, v1

    .line 32262
    goto/16 :goto_1

    .line 32263
    :pswitch_b
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    .line 32264
    const-string v1, "callbackID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32265
    if-nez v1, :cond_12

    .line 32266
    const/4 v1, 0x0

    .line 32267
    :goto_12
    move-object v0, v1

    .line 32268
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->c()LX/0D5;

    move-result-object v1

    .line 32269
    if-eqz v1, :cond_1

    .line 32270
    new-instance v4, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy$2;

    invoke-direct {v4, p0, v1}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy$2;-><init>(Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;LX/0D5;)V

    invoke-virtual {v1, v4}, LX/0D5;->post(Ljava/lang/Runnable;)Z

    .line 32271
    :cond_1
    goto/16 :goto_1

    .line 32272
    :pswitch_c
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    .line 32273
    const-string v1, "callbackID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32274
    if-nez v1, :cond_13

    .line 32275
    const/4 v1, 0x0

    .line 32276
    :goto_13
    move-object v0, v1

    .line 32277
    goto/16 :goto_1

    .line 32278
    :pswitch_d
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    const/4 v4, 0x0

    .line 32279
    const-string v1, "callbackID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 32280
    if-nez v5, :cond_14

    move-object v1, v4

    .line 32281
    :goto_14
    move-object v0, v1

    .line 32282
    goto/16 :goto_1

    .line 32283
    :pswitch_e
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;

    const/4 v1, 0x0

    .line 32284
    const-string v4, "callbackID"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 32285
    if-nez v4, :cond_16

    .line 32286
    :goto_15
    move-object v0, v1

    .line 32287
    goto/16 :goto_1

    .line 32288
    :cond_2
    invoke-virtual {p0, p1, v0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 32289
    :cond_3
    const-string v4, ""

    goto/16 :goto_3

    .line 32290
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 32291
    :cond_5
    const-string v4, ""

    goto/16 :goto_6

    .line 32292
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_7

    .line 32293
    :catch_0
    sget-object v4, Lcom/facebook/browserextensions/ipc/CardCredentialInfo;->f:Ljava/lang/String;

    const-string v6, "Error while serializing credential info!"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4, v6, v7}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32294
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_9

    .line 32295
    :cond_8
    :try_start_1
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 32296
    const-string v7, "payment_result"

    invoke-virtual {v6, v7, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32297
    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    goto/16 :goto_8

    .line 32298
    :catch_1
    const-string v6, "ProcessPaymentJSBridgeCall"

    const-string v7, "Error while serializing payment token!"

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {v6, v7, p2}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_8

    :cond_9
    const-string v4, "%s(%s, \'%s\');"

    const-string v5, "callback_result"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4, v0, v5, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_a

    :cond_a
    const-string v4, "%s(%s, \'%s\');"

    const-string v5, "callback_result"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4, v0, v5, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_b

    :cond_b
    const-string v4, "%s(%s, \'%s\');"

    const-string v5, "callback_result"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4, v0, v5, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_c

    .line 32299
    :cond_c
    const-string v4, "result"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 32300
    const-string v5, "%s(%s, \'%s\', \'%s\');"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v5, v0, v6, v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_d

    .line 32301
    :cond_d
    const-string v4, "result"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 32302
    const-string v5, "%s(%s, \'%s\', \'%s\');"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v5, v0, v6, v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_e

    .line 32303
    :cond_e
    const-string v4, "result"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 32304
    const-string v5, "%s(%s, \'%s\', \'%s\');"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v5, v0, v6, v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_f

    .line 32305
    :cond_f
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 32306
    :try_start_2
    const-string v6, "callbackID"

    invoke-virtual {v5, v6, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32307
    const-string v6, "latitude"

    const-string v7, "latitude"

    invoke-virtual {p2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32308
    const-string v6, "longitude"

    const-string v7, "longitude"

    invoke-virtual {p2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32309
    const-string v6, "accuracy"

    const-string v7, "accuracy"

    invoke-virtual {p2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 32310
    const-string v1, "%s(%s, \'%s\', \'%s\');"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v0, v6, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_10

    .line 32311
    :catch_2
    const-string v4, "requestCurrentPosition"

    const-string v5, "Exception serializing return params!"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_10

    .line 32312
    :cond_10
    const-string v6, "asid"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 32313
    const-string v7, "psid"

    invoke-virtual {p2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 32314
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 32315
    :try_start_3
    const-string v9, "asid"

    invoke-virtual {v8, v9, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32316
    const-string v6, "psid"

    invoke-virtual {v8, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 32317
    const-string v4, "%s(%s, \'%s\', \'%s\');"

    if-eqz v7, :cond_11

    const/4 v1, 0x1

    :cond_11
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v0, v1, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_11

    .line 32318
    :catch_3
    const-string v5, "getUserID"

    const-string v6, "Exception serializing return params!"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v5, v6, v1}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v4

    .line 32319
    goto/16 :goto_11

    :cond_12
    const-string v4, "%s(%s, \'%s\');"

    const-string v5, "true"

    invoke-static {v4, v0, v5, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_12

    .line 32320
    :cond_13
    const-string v4, "result"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 32321
    const-string v5, "%s(%s, \'%s\', \'%s\');"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v5, v0, v6, v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_13

    .line 32322
    :cond_14
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 32323
    :try_start_4
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_16
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 32324
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v1, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_16

    .line 32325
    :catch_4
    const-string v1, "requestAutoFill"

    const-string v5, "Exception serializing return params!"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1, v5, v6}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v4

    .line 32326
    goto/16 :goto_14

    .line 32327
    :cond_15
    const-string v1, "%s(%s, \'%s\', \'%s\');"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v0, v4, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_14

    .line 32328
    :cond_16
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 32329
    :try_start_5
    const-string v6, "offer_code"

    const-string v7, "offer_code"

    invoke-virtual {p2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5

    .line 32330
    const-string v1, "%s(%s, \'%s\', \'%s\');"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v0, v6, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_15

    .line 32331
    :catch_5
    const-string v4, "requestFillOfferCode"

    const-string v5, "Exception serializing return params!"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_15

    :sswitch_data_0
    .sparse-switch
        -0x725e90ee -> :sswitch_8
        -0x6b5f58cd -> :sswitch_9
        -0x69bd59c1 -> :sswitch_b
        -0x4c9f9149 -> :sswitch_5
        -0x4597bc6d -> :sswitch_6
        -0x34988629 -> :sswitch_e
        -0x33f39f69 -> :sswitch_2
        -0x11ab6477 -> :sswitch_3
        -0xff3d07f -> :sswitch_d
        -0x59d64ae -> :sswitch_c
        0x3123f8ed -> :sswitch_0
        0x3342511c -> :sswitch_a
        0x61dab312 -> :sswitch_1
        0x6be89163 -> :sswitch_7
        0x7898cd0f -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 32332
    invoke-static {p0, p1, p2}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->b(Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Landroid/os/Bundle;)V

    .line 32333
    return-void
.end method

.method public final beginShareFlow(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32334
    new-instance v0, Lcom/facebook/browserextensions/ipc/BeginShareFlowJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32335
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32336
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32337
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32338
    const-string v7, "title"

    const-string p1, "title"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32339
    const-string v7, "subtitle"

    const-string p1, "subtitle"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32340
    const-string v7, "image_url"

    const-string p1, "image_url"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32341
    const-string v7, "item_url"

    const-string p1, "item_url"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32342
    const-string v7, "button_title"

    const-string p1, "button_title"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32343
    const-string v7, "button_url"

    const-string p1, "button_url"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32344
    move-object v5, v6

    .line 32345
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/BeginShareFlowJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32346
    return-void
.end method

.method public final getUserID(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32347
    new-instance v0, Lcom/facebook/browserextensions/ipc/GetUserIDJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32348
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32349
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32350
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32351
    const-string v7, "callbackID"

    const-string p1, "callbackID"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32352
    move-object v5, v6

    .line 32353
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/GetUserIDJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32354
    return-void
.end method

.method public final hasCapability(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32355
    new-instance v0, Lcom/facebook/browserextensions/ipc/HasCapabilityJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32356
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32357
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32358
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32359
    const-string v7, "callbackID"

    const-string p1, "callbackID"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32360
    const-string v7, "capabilities"

    const-string p1, "capabilities"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32361
    move-object v5, v6

    .line 32362
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/HasCapabilityJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32363
    return-void
.end method

.method public final hideAutoFillBar(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32364
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->d()Lcom/facebook/browser/lite/BrowserLiteFragment;

    move-result-object v0

    .line 32365
    if-nez v0, :cond_0

    .line 32366
    :goto_0
    return-void

    .line 32367
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a()V

    goto :goto_0
.end method

.method public final initializeCallbackHandler(Ljava/lang/String;)V
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32368
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32369
    const-string v1, "name"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 32370
    return-void

    .line 32371
    :catch_0
    move-exception v0

    .line 32372
    sget-object v1, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a:Ljava/lang/String;

    const-string v2, "Exception when invoking setupCallbackHandler call!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32373
    throw v0
.end method

.method public final paymentsCheckout(Ljava/lang/String;)V
    .locals 7
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32374
    new-instance v0, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32375
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32376
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/0FB;->PAYMENTS_CHECKOUT:LX/0FB;

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v6}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->a(LX/0FB;Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32377
    return-void
.end method

.method public final paymentsCheckoutChargeRequestErrorReturn(Ljava/lang/String;)V
    .locals 7
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32378
    new-instance v0, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32379
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32380
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/0FB;->PAYMENTS_CHECKOUT_CHARGE_REQUEST_ERROR_RETURN:LX/0FB;

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v6}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->a(LX/0FB;Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32381
    return-void
.end method

.method public final paymentsCheckoutChargeRequestSuccessReturn(Ljava/lang/String;)V
    .locals 7
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32146
    new-instance v0, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32147
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32148
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/0FB;->PAYMENTS_CHECKOUT_CHARGE_REQUEST_SUCCESS_RETURN:LX/0FB;

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v6}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->a(LX/0FB;Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32149
    return-void
.end method

.method public final paymentsCheckoutDummyReturn(Ljava/lang/String;)V
    .locals 7
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32382
    new-instance v0, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32383
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32384
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/0FB;->PAYMENTS_CHECKOUT_DUMMY_RETURN:LX/0FB;

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v6}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->a(LX/0FB;Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32385
    return-void
.end method

.method public final paymentsCheckoutShippingAddressReturn(Ljava/lang/String;)V
    .locals 7
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 31995
    new-instance v0, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 31996
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 31997
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/0FB;->PAYMENTS_CHECKOUT_SHIPPING_ADDRESS_RETURN:LX/0FB;

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v6}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->a(LX/0FB;Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 31998
    return-void
.end method

.method public final processPayment(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32002
    new-instance v0, Lcom/facebook/browserextensions/ipc/ProcessPaymentJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32003
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32004
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32005
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32006
    const-string v7, "callbackID"

    const-string p1, "callbackID"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32007
    const-string v7, "amount"

    const-string p1, "amount"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32008
    move-object v5, v6

    .line 32009
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/ProcessPaymentJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32010
    return-void
.end method

.method public final purchaseComplete(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32011
    new-instance v0, Lcom/facebook/browserextensions/ipc/commerce/PurchaseCompleteJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32012
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32013
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32014
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32015
    const-string v7, "callbackID"

    const-string p1, "callbackID"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32016
    const-string v7, "amount"

    const-string p1, "amount"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32017
    move-object v5, v6

    .line 32018
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/commerce/PurchaseCompleteJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32019
    return-void
.end method

.method public final requestAuthorizedCredentials(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32020
    new-instance v0, Lcom/facebook/browserextensions/ipc/RequestAuthorizedCredentialsJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32021
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32022
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32023
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32024
    const-string v7, "callbackID"

    const-string p1, "callbackID"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32025
    const-string v7, "amount"

    const-string p1, "amount"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32026
    move-object v5, v6

    .line 32027
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/RequestAuthorizedCredentialsJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32028
    return-void
.end method

.method public final requestAutoFill(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32029
    new-instance v0, Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32030
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32031
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32032
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32033
    const-string v7, "callbackID"

    const-string p1, "callbackID"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32034
    const-string v7, "autofillFields"

    const-string p1, "autofillFields"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32035
    const-string v7, "selectedAutoCompleteTag"

    const-string p1, "selectedAutoCompleteTag"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32036
    move-object v5, v6

    .line 32037
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32038
    return-void
.end method

.method public final requestCloseBrowser(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32039
    new-instance v0, Lcom/facebook/browserextensions/ipc/RequestCloseBrowserJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32040
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32041
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32042
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32043
    const-string v7, "callbackID"

    const-string p1, "callbackID"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32044
    move-object v5, v6

    .line 32045
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/RequestCloseBrowserJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32046
    return-void
.end method

.method public final requestCredentials(Ljava/lang/String;)V
    .locals 10
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32047
    new-instance v0, Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32048
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32049
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32050
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32051
    const-string v7, "callbackID"

    const-string v8, "callbackID"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32052
    const-string v7, "title"

    const-string v8, "title"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32053
    const-string v7, "imageURL"

    const-string v8, "imageURL"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32054
    const-string v7, "amount"

    const-string v8, "amount"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32055
    const-string v7, "requestedUserInfo"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "requestedUserInfo"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 32056
    :try_start_0
    const-string v7, "requestedUserInfo"

    const-string v8, "requestedUserInfo"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 32057
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 32058
    const/4 v9, 0x0

    :goto_0
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v9, v5, :cond_0

    .line 32059
    invoke-virtual {v8, v9}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32060
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 32061
    :cond_0
    move-object v8, p1

    .line 32062
    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 32063
    :cond_1
    :goto_1
    move-object v5, v6

    .line 32064
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32065
    return-void

    .line 32066
    :catch_0
    const-string v7, "requestCredentials"

    const-string v8, "Exception de-serializing requested user params!"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final requestCurrentPosition(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32067
    new-instance v0, Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32068
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32069
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32070
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32071
    const-string v7, "callbackID"

    const-string p1, "callbackID"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32072
    move-object v5, v6

    .line 32073
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32074
    return-void
.end method

.method public final requestFillOfferCode(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32075
    new-instance v0, Lcom/facebook/browserextensions/ipc/RequestOfferCodeJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32076
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32077
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32078
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32079
    const-string v7, "callbackID"

    const-string p1, "callbackID"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32080
    move-object v5, v6

    .line 32081
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/RequestOfferCodeJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32082
    return-void
.end method

.method public final requestUpdateProductHistory(Ljava/lang/String;)V
    .locals 9
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32083
    new-instance v0, Lcom/facebook/browserextensions/ipc/RequestUpdateProductHistoryJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32084
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32085
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32086
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32087
    const-string v7, "callbackID"

    const-string v8, "callbackID"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32088
    const-string v7, "productUrl"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 32089
    const-string v8, "browserLocale"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 32090
    const-string p1, "productUrl"

    invoke-virtual {v6, p1, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32091
    const-string v7, "browserLocale"

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32092
    move-object v5, v6

    .line 32093
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/RequestUpdateProductHistoryJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32094
    return-void
.end method

.method public final requestUserInfoField(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32095
    new-instance v0, Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32096
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32097
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32098
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32099
    const-string v7, "callbackID"

    const-string p1, "callbackID"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32100
    const-string v7, "field"

    const-string p1, "field"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32101
    move-object v5, v6

    .line 32102
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32103
    return-void
.end method

.method public final resetCart(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32104
    new-instance v0, Lcom/facebook/browserextensions/ipc/commerce/ResetCartJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32105
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32106
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32107
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32108
    const-string v7, "callbackID"

    const-string p1, "callbackID"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32109
    move-object v5, v6

    .line 32110
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/commerce/ResetCartJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32111
    return-void
.end method

.method public final saveAutofillData(Ljava/lang/String;)V
    .locals 14
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32112
    new-instance v0, Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32113
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32114
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32115
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 32116
    const-string v6, "callbackID"

    const-string v7, "callbackID"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32117
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 32118
    const-string v6, "raw_autofill_data"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 32119
    if-eqz v10, :cond_2

    .line 32120
    invoke-virtual {v10}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v11

    .line 32121
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 32122
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 32123
    invoke-virtual {v10, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v12

    .line 32124
    new-instance v13, Ljava/util/ArrayList;

    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I

    move-result v7

    invoke-direct {v13, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 32125
    const/4 v7, 0x0

    :goto_1
    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I

    move-result p1

    if-ge v7, p1, :cond_1

    .line 32126
    invoke-virtual {v12, v7}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    .line 32127
    if-eqz p1, :cond_0

    .line 32128
    invoke-virtual {v12, v7}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v13, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32129
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 32130
    :cond_1
    invoke-virtual {v9, v6, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 32131
    :cond_2
    const-string v6, "autofill_data_bundle_key"

    invoke-virtual {v8, v6, v9}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 32132
    move-object v5, v8

    .line 32133
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32134
    return-void
.end method

.method public final updateCart(Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32135
    new-instance v0, Lcom/facebook/browserextensions/ipc/commerce/UpdateCartJSBridgeCall;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->f()Landroid/content/Context;

    move-result-object v1

    .line 32136
    iget-object v2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v2

    .line 32137
    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->e()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 32138
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 32139
    const-string v7, "callbackID"

    const-string p1, "callbackID"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32140
    const-string v7, "itemCount"

    const-string p1, "itemCount"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32141
    const-string v7, "cartURL"

    const-string p1, "cartURL"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32142
    const-string v7, "expiry"

    const-string p1, "expiry"

    invoke-virtual {v5, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32143
    move-object v5, v6

    .line 32144
    invoke-direct/range {v0 .. v5}, Lcom/facebook/browserextensions/ipc/commerce/UpdateCartJSBridgeCall;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 32145
    return-void
.end method
