.class public Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;
.super Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData",
        "<",
        "Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32725
    new-instance v0, LX/0Ex;

    invoke-direct {v0}, LX/0Ex;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->b:Ljava/util/Set;

    .line 32726
    new-instance v0, LX/0Ey;

    invoke-direct {v0}, LX/0Ey;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 32727
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;-><init>(Landroid/os/Parcel;)V

    .line 32728
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32731
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;-><init>(Ljava/util/Map;)V

    .line 32732
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .prologue
    .line 32729
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;-><init>(Lorg/json/JSONObject;)V

    .line 32730
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 32724
    sget-object v0, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 32698
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 32699
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 32700
    const-string v0, " "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32701
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32702
    const/4 v0, 0x1

    .line 32703
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Set;)Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;
    .locals 5

    .prologue
    .line 32719
    new-instance v1, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;

    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    invoke-direct {v1, v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;-><init>(Ljava/util/Map;)V

    .line 32720
    iget-object v0, v1, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 32721
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 32722
    iget-object v3, v1, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 32723
    :cond_1
    return-object v1
.end method

.method public final a()Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32718
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 32705
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 32706
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const-string v2, "street-address"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32707
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const-string v2, "address-line1"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    .line 32708
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const-string v2, "address-line2"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    .line 32709
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const-string v2, "address-line3"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    .line 32710
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const-string v2, "address-level4"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    .line 32711
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const-string v2, "address-level3"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    .line 32712
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const-string v2, "address-level2"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    .line 32713
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const-string v2, "address-level1"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    .line 32714
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const-string v2, "postal-code"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    .line 32715
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const-string v2, "country"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 32716
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const-string v2, "country-name"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    .line 32717
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32704
    const-string v0, "address-autofill-data"

    return-object v0
.end method
