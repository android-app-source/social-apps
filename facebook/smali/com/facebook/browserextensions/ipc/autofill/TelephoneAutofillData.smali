.class public Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;
.super Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData",
        "<",
        "Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32829
    new-instance v0, LX/0F3;

    invoke-direct {v0}, LX/0F3;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->b:Ljava/util/Set;

    .line 32830
    new-instance v0, LX/0F4;

    invoke-direct {v0}, LX/0F4;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 32831
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;-><init>(Landroid/os/Parcel;)V

    .line 32832
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->c:Ljava/lang/String;

    .line 32833
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32834
    invoke-direct {p0}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;-><init>()V

    .line 32835
    iput-object p1, p0, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->a:Ljava/util/Map;

    .line 32836
    iput-object p2, p0, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->c:Ljava/lang/String;

    .line 32837
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 32838
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;-><init>(Lorg/json/JSONObject;)V

    .line 32839
    :try_start_0
    const-string v0, "display_number"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 32840
    :goto_0
    iput-object v0, p0, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->c:Ljava/lang/String;

    .line 32841
    return-void

    .line 32842
    :catch_0
    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const-string v1, "tel"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 32843
    sget-object v0, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/util/Set;)Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;
    .locals 5

    .prologue
    .line 32844
    new-instance v1, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;

    iget-object v0, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->c:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;-><init>(Ljava/util/Map;Ljava/lang/String;)V

    .line 32845
    iget-object v0, v1, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 32846
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 32847
    iget-object v3, v1, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 32848
    :cond_1
    return-object v1
.end method

.method public final a()Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32849
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32850
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32851
    const-string v0, "telephone-autofill-data"

    return-object v0
.end method

.method public final d()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 32852
    invoke-super {p0}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->d()Lorg/json/JSONObject;

    move-result-object v0

    .line 32853
    const-string v1, "display_number"

    iget-object v2, p0, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32854
    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 32855
    invoke-super {p0, p1, p2}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 32856
    iget-object v0, p0, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32857
    return-void
.end method
