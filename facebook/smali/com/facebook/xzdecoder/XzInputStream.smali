.class public Lcom/facebook/xzdecoder/XzInputStream;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field private a:[B

.field private b:[B

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Ljava/io/InputStream;

.field private h:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42280
    const-string v0, "fb_xzdecoder"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 42281
    invoke-static {}, Lcom/facebook/xzdecoder/XzInputStream;->initializeLibrary()V

    .line 42282
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    const v1, 0x8000

    .line 42274
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 42275
    iput-object p1, p0, Lcom/facebook/xzdecoder/XzInputStream;->g:Ljava/io/InputStream;

    .line 42276
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->a:[B

    .line 42277
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->b:[B

    .line 42278
    invoke-static {}, Lcom/facebook/xzdecoder/XzInputStream;->initializeState()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->h:J

    .line 42279
    return-void
.end method

.method private a([BII)I
    .locals 3

    .prologue
    .line 42270
    iget v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->e:I

    iget v1, p0, Lcom/facebook/xzdecoder/XzInputStream;->f:I

    sub-int/2addr v0, v1

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 42271
    iget-object v1, p0, Lcom/facebook/xzdecoder/XzInputStream;->b:[B

    iget v2, p0, Lcom/facebook/xzdecoder/XzInputStream;->f:I

    invoke-static {v1, v2, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 42272
    iget v1, p0, Lcom/facebook/xzdecoder/XzInputStream;->f:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/facebook/xzdecoder/XzInputStream;->f:I

    .line 42273
    return v0
.end method

.method private a()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 42261
    iput v1, p0, Lcom/facebook/xzdecoder/XzInputStream;->d:I

    .line 42262
    iput v1, p0, Lcom/facebook/xzdecoder/XzInputStream;->c:I

    .line 42263
    :goto_0
    iget v2, p0, Lcom/facebook/xzdecoder/XzInputStream;->d:I

    const v3, 0x8000

    if-ge v2, v3, :cond_0

    .line 42264
    iget-object v2, p0, Lcom/facebook/xzdecoder/XzInputStream;->g:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/facebook/xzdecoder/XzInputStream;->a:[B

    iget v4, p0, Lcom/facebook/xzdecoder/XzInputStream;->d:I

    iget-object v5, p0, Lcom/facebook/xzdecoder/XzInputStream;->a:[B

    array-length v5, v5

    iget v6, p0, Lcom/facebook/xzdecoder/XzInputStream;->d:I

    sub-int/2addr v5, v6

    invoke-virtual {v2, v3, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 42265
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 42266
    iget v2, p0, Lcom/facebook/xzdecoder/XzInputStream;->d:I

    if-lez v2, :cond_1

    .line 42267
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 42268
    goto :goto_1

    .line 42269
    :cond_2
    iget v3, p0, Lcom/facebook/xzdecoder/XzInputStream;->d:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/facebook/xzdecoder/XzInputStream;->d:I

    goto :goto_0
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 42256
    iget-wide v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->h:J

    iget-object v2, p0, Lcom/facebook/xzdecoder/XzInputStream;->a:[B

    iget v3, p0, Lcom/facebook/xzdecoder/XzInputStream;->c:I

    iget v4, p0, Lcom/facebook/xzdecoder/XzInputStream;->d:I

    iget-object v5, p0, Lcom/facebook/xzdecoder/XzInputStream;->b:[B

    iget-object v7, p0, Lcom/facebook/xzdecoder/XzInputStream;->b:[B

    array-length v7, v7

    invoke-static/range {v0 .. v7}, Lcom/facebook/xzdecoder/XzInputStream;->decompressStream(J[BII[BII)J

    move-result-wide v0

    .line 42257
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    long-to-int v2, v2

    iput v2, p0, Lcom/facebook/xzdecoder/XzInputStream;->c:I

    .line 42258
    long-to-int v0, v0

    iput v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->e:I

    .line 42259
    iput v6, p0, Lcom/facebook/xzdecoder/XzInputStream;->f:I

    .line 42260
    return-void
.end method

.method private static native decompressStream(J[BII[BII)J
.end method

.method private static native end(J)V
.end method

.method private static native initializeLibrary()V
.end method

.method private static native initializeState()J
.end method


# virtual methods
.method public final close()V
    .locals 4

    .prologue
    .line 42252
    iget-object v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->g:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 42253
    iget-wide v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 42254
    iget-wide v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->h:J

    invoke-static {v0, v1}, Lcom/facebook/xzdecoder/XzInputStream;->end(J)V

    .line 42255
    :cond_0
    return-void
.end method

.method public final read()I
    .locals 4

    .prologue
    .line 42244
    iget-wide v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 42245
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42246
    :cond_0
    iget v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->f:I

    iget v1, p0, Lcom/facebook/xzdecoder/XzInputStream;->e:I

    if-ne v0, v1, :cond_2

    .line 42247
    iget v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->c:I

    iget v1, p0, Lcom/facebook/xzdecoder/XzInputStream;->d:I

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/facebook/xzdecoder/XzInputStream;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 42248
    const/4 v0, -0x1

    .line 42249
    :goto_0
    return v0

    .line 42250
    :cond_1
    invoke-direct {p0}, Lcom/facebook/xzdecoder/XzInputStream;->b()V

    .line 42251
    :cond_2
    iget-object v0, p0, Lcom/facebook/xzdecoder/XzInputStream;->b:[B

    iget v1, p0, Lcom/facebook/xzdecoder/XzInputStream;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/facebook/xzdecoder/XzInputStream;->f:I

    aget-byte v0, v0, v1

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 42229
    if-ltz p3, :cond_0

    if-ltz p2, :cond_0

    add-int v1, p2, p3

    array-length v2, p1

    if-le v1, v2, :cond_1

    .line 42230
    :cond_0
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    const-string v2, "buf.length = %d, off = %d, len = %d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    array-length v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 42231
    :cond_1
    iget-wide v2, p0, Lcom/facebook/xzdecoder/XzInputStream;->h:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 42232
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v1, p3

    .line 42233
    :goto_0
    if-ge v0, p3, :cond_4

    .line 42234
    iget v2, p0, Lcom/facebook/xzdecoder/XzInputStream;->f:I

    iget v3, p0, Lcom/facebook/xzdecoder/XzInputStream;->e:I

    if-ge v2, v3, :cond_3

    .line 42235
    invoke-direct {p0, p1, p2, v1}, Lcom/facebook/xzdecoder/XzInputStream;->a([BII)I

    move-result v2

    .line 42236
    sub-int/2addr v1, v2

    .line 42237
    add-int/2addr p2, v2

    .line 42238
    add-int/2addr v0, v2

    .line 42239
    goto :goto_0

    .line 42240
    :cond_3
    iget v2, p0, Lcom/facebook/xzdecoder/XzInputStream;->c:I

    iget v3, p0, Lcom/facebook/xzdecoder/XzInputStream;->d:I

    if-ne v2, v3, :cond_5

    invoke-direct {p0}, Lcom/facebook/xzdecoder/XzInputStream;->a()Z

    move-result v2

    if-nez v2, :cond_5

    .line 42241
    if-nez v0, :cond_4

    const/4 v0, -0x1

    .line 42242
    :cond_4
    return v0

    .line 42243
    :cond_5
    invoke-direct {p0}, Lcom/facebook/xzdecoder/XzInputStream;->b()V

    goto :goto_0
.end method
