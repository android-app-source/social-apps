.class public Lcom/facebook/rti/orca/UpdateQeBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:LX/07y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26028
    const-class v0, Lcom/facebook/rti/orca/UpdateQeBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/rti/orca/UpdateQeBroadcastReceiver;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26018
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/rti/orca/UpdateQeBroadcastReceiver;

    invoke-static {v0}, LX/07y;->a(LX/0QB;)LX/07y;

    move-result-object v0

    check-cast v0, LX/07y;

    iput-object v0, p0, Lcom/facebook/rti/orca/UpdateQeBroadcastReceiver;->a:LX/07y;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x6f8b9648

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 26019
    invoke-static {p1}, LX/1mU;->a(Landroid/content/Context;)V

    .line 26020
    invoke-static {p0, p1}, Lcom/facebook/rti/orca/UpdateQeBroadcastReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 26021
    if-nez p2, :cond_0

    .line 26022
    const/16 v1, 0x27

    const v2, 0x61e522af

    invoke-static {p2, v3, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 26023
    :goto_0
    return-void

    .line 26024
    :cond_0
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "android.intent.action.MY_PACKAGE_REPLACED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 26025
    const v1, -0x63234bb7

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 26026
    :cond_1
    iget-object v1, p0, Lcom/facebook/rti/orca/UpdateQeBroadcastReceiver;->a:LX/07y;

    invoke-virtual {v1}, LX/07y;->a()V

    .line 26027
    const v1, 0x665e92dd

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0
.end method
