.class public Lcom/facebook/rti/orca/MainService;
.super Landroid/app/Service;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x8
.end annotation


# instance fields
.field public a:LX/09k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24405
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/rti/orca/MainService;

    invoke-static {v0}, LX/09k;->a(LX/0QB;)LX/09k;

    move-result-object v0

    check-cast v0, LX/09k;

    iput-object v0, p0, Lcom/facebook/rti/orca/MainService;->a:LX/09k;

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 24394
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x68aa81d6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 24401
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 24402
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 24403
    invoke-static {p0, p0}, Lcom/facebook/rti/orca/MainService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 24404
    const/16 v1, 0x25

    const v2, -0x105d6b13

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x2a40c10b    # 1.7120001E-13f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 24395
    iget-object v2, p0, Lcom/facebook/rti/orca/MainService;->a:LX/09k;

    .line 24396
    iget-boolean v3, v2, LX/09k;->g:Z

    move v2, v3

    .line 24397
    if-nez v2, :cond_0

    .line 24398
    invoke-virtual {p0}, Lcom/facebook/rti/orca/MainService;->stopSelf()V

    .line 24399
    const/16 v2, 0x25

    const v3, -0x68c7ead7

    invoke-static {v0, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 24400
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    const v2, 0x2ac8c448

    invoke-static {v2, v1}, LX/02F;->d(II)V

    goto :goto_0
.end method
