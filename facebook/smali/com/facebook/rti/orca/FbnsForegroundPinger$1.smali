.class public final Lcom/facebook/rti/orca/FbnsForegroundPinger$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/09n;


# direct methods
.method public constructor <init>(LX/09n;)V
    .locals 0

    .prologue
    .line 25916
    iput-object p1, p0, Lcom/facebook/rti/orca/FbnsForegroundPinger$1;->a:LX/09n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 25917
    iget-object v0, p0, Lcom/facebook/rti/orca/FbnsForegroundPinger$1;->a:LX/09n;

    iget-object v0, v0, LX/09n;->e:LX/09m;

    const-string v1, "Orca.PING"

    .line 25918
    invoke-static {v0}, LX/09m;->e(LX/09m;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "shared_flag"

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 25919
    invoke-static {v0, v2}, LX/09m;->a(LX/09m;I)Ljava/lang/String;

    move-result-object v2

    .line 25920
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 25921
    invoke-static {v2}, Lcom/facebook/rti/push/service/FbnsService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 25922
    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25923
    move-object v0, v3

    .line 25924
    const-string v1, "caller"

    const-string v2, "FB_PING"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25925
    iget-object v1, p0, Lcom/facebook/rti/orca/FbnsForegroundPinger$1;->a:LX/09n;

    iget-object v1, v1, LX/09n;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    .line 25926
    if-nez v0, :cond_0

    .line 25927
    sget-object v0, LX/09n;->a:Ljava/lang/String;

    const-string v1, "Missing %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-class v4, Lcom/facebook/rti/push/service/FbnsService;

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25928
    :cond_0
    return-void
.end method
