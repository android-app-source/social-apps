.class public Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25977
    const-class v0, Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25999
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 11

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, -0x1

    const/4 v7, 0x1

    .line 26000
    sget-object v0, LX/01p;->f:LX/01q;

    invoke-static {p1, v0}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 26001
    const-string v1, "sharing_state_enabled"

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26002
    invoke-virtual {p0, v7}, Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 26003
    const-string v1, "/settings/mqtt/id/mqtt_device_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 26004
    const-string v1, "/settings/mqtt/id/mqtt_device_secret"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 26005
    const-string v1, "/settings/mqtt/id/timestamp"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 26006
    invoke-virtual {p0, v8, v10, v0}, Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;->setResult(ILjava/lang/String;Landroid/os/Bundle;)V

    .line 26007
    :goto_0
    return-void

    .line 26008
    :cond_0
    sget-object v0, LX/01p;->g:LX/01q;

    invoke-static {p1, v0}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 26009
    const-string v1, "/settings/mqtt/id/mqtt_device_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 26010
    const-string v2, "/settings/mqtt/id/mqtt_device_secret"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 26011
    const-string v3, "/settings/mqtt/id/timestamp"

    invoke-interface {v0, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 26012
    sget-object v0, Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;->a:Ljava/lang/String;

    const-string v3, "sharing device id %s"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v1, v6, v9

    invoke-static {v0, v3, v6}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26013
    invoke-virtual {p0, v7}, Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 26014
    const-string v3, "/settings/mqtt/id/mqtt_device_id"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 26015
    const-string v1, "/settings/mqtt/id/mqtt_device_secret"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 26016
    const-string v1, "/settings/mqtt/id/timestamp"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 26017
    invoke-virtual {p0, v8, v10, v0}, Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;->setResult(ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 25991
    const-string v0, "pkg_name"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 25992
    if-nez v0, :cond_0

    .line 25993
    :goto_0
    return-void

    .line 25994
    :cond_0
    sget-object v0, LX/01p;->f:LX/01q;

    invoke-static {p1, v0}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 25995
    const-string v1, "shared_qe_flag"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 25996
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v1

    .line 25997
    const-string v2, "shared_qe_flag"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 25998
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0, v1}, Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;->setResult(ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x13c0ea45

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 25978
    if-nez p2, :cond_0

    .line 25979
    const/16 v1, 0x27

    const v2, 0x3091e6b4

    invoke-static {p2, v3, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 25980
    :goto_0
    return-void

    .line 25981
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 25982
    new-instance v2, LX/04v;

    invoke-direct {v2, p1}, LX/04v;-><init>(Landroid/content/Context;)V

    .line 25983
    invoke-virtual {v2, p2}, LX/04v;->a(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 25984
    sget-object v1, Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;->a:Ljava/lang/String;

    const-string v2, "Rejecting device credentials sharing request due to failed auth"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25985
    const v1, 0x626561b1

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 25986
    :cond_1
    const-string v2, "com.facebook.rti.fbns.intent.SHARE_IDS"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 25987
    invoke-direct {p0, p1}, Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;->a(Landroid/content/Context;)V

    .line 25988
    :cond_2
    :goto_1
    const v1, -0x6dadb093

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 25989
    :cond_3
    const-string v2, "com.facebook.rti.intent.SHARED_QE_FLAG_REQUEST"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 25990
    invoke-direct {p0, p1, p2}, Lcom/facebook/rti/push/service/idsharing/FbnsSharingStateReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_1
.end method
