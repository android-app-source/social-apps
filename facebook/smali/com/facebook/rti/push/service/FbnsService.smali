.class public Lcom/facebook/rti/push/service/FbnsService;
.super LX/051;
.source ""


# static fields
.field public static final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;"
        }
    .end annotation
.end field

.field public static final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;"
        }
    .end annotation
.end field

.field private static v:Lcom/facebook/rti/push/service/FbnsService;


# instance fields
.field public r:LX/0Ib;
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation
.end field

.field public s:LX/0Ig;
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation
.end field

.field public t:LX/0IP;

.field public u:LX/04v;

.field public w:LX/0IK;

.field private x:LX/0IT;

.field private y:LX/0Ic;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25698
    new-instance v0, LX/0AE;

    invoke-direct {v0}, LX/0AE;-><init>()V

    sput-object v0, Lcom/facebook/rti/push/service/FbnsService;->o:Ljava/util/List;

    .line 25699
    new-instance v0, LX/0AG;

    invoke-direct {v0}, LX/0AG;-><init>()V

    sput-object v0, Lcom/facebook/rti/push/service/FbnsService;->p:Ljava/util/List;

    .line 25700
    new-instance v0, LX/0AH;

    invoke-direct {v0}, LX/0AH;-><init>()V

    sput-object v0, Lcom/facebook/rti/push/service/FbnsService;->q:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25701
    invoke-direct {p0}, LX/051;-><init>()V

    .line 25702
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25703
    invoke-static {p0}, LX/04u;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25704
    const-string v0, "com.facebook.oxygen.services.fbns.PreloadedFbnsService"

    .line 25705
    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lcom/facebook/rti/push/service/FbnsService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/0IS;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 25706
    iget-object v0, p1, LX/0IS;->a:Ljava/lang/String;

    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25707
    const-string v0, "FbnsService"

    const-string v1, "service/register/response/invalid"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25708
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0II;->FAILURE_SERVER_RESPOND_WITH_INVALID_PACKAGE_NAME:LX/0II;

    invoke-virtual {v0, v1, v4}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    .line 25709
    :goto_0
    return-void

    .line 25710
    :cond_0
    iget-object v0, p1, LX/0IS;->b:Ljava/lang/String;

    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 25711
    const-string v0, "FbnsService"

    const-string v1, "service/register/response/empty_token"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25712
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0II;->FAILURE_SERVER_RESPOND_WITH_INVALID_TOKEN:LX/0II;

    invoke-virtual {v0, v1, v4}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    goto :goto_0

    .line 25713
    :cond_1
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    iget-object v1, p1, LX/0IS;->a:Ljava/lang/String;

    iget-object v2, p1, LX/0IS;->b:Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 25714
    const-string v5, "RegistrationState"

    const-string v8, "updateTokenCache %s %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v1, v9, v7

    aput-object v2, v9, v6

    invoke-static {v5, v8, v9}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25715
    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    :goto_1
    invoke-static {v5}, LX/01n;->a(Z)V

    .line 25716
    invoke-static {v2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    :goto_2
    invoke-static {v6}, LX/01n;->a(Z)V

    .line 25717
    invoke-static {v0}, LX/0Ig;->h(LX/0Ig;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "auto_reg_retry"

    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-static {v5}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 25718
    invoke-static {v0}, LX/0Ig;->g(LX/0Ig;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 25719
    invoke-static {v1, v5}, LX/0Ig;->a(Ljava/lang/String;Landroid/content/SharedPreferences;)LX/0If;

    move-result-object v6

    .line 25720
    if-nez v6, :cond_5

    .line 25721
    const-string v5, "RegistrationState"

    const-string v6, "Missing entry"

    new-array v8, v7, [Ljava/lang/Object;

    invoke-static {v5, v6, v8}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25722
    :goto_3
    move v0, v7

    .line 25723
    if-eqz v0, :cond_2

    .line 25724
    iget-object v0, p1, LX/0IS;->a:Ljava/lang/String;

    iget-object v1, p1, LX/0IS;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/facebook/rti/push/service/FbnsService;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 25725
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0II;->RESPONSE_RECEIVED:LX/0II;

    invoke-virtual {v0, v1, v4}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    goto :goto_0

    .line 25726
    :cond_2
    const-string v0, "FbnsService"

    const-string v1, "service/register/response/cache_update_failed"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25727
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0II;->FAILURE_CACHE_UPDATE:LX/0II;

    iget-object v2, p1, LX/0IS;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    move v5, v7

    .line 25728
    goto :goto_1

    :cond_4
    move v6, v7

    .line 25729
    goto :goto_2

    .line 25730
    :cond_5
    iput-object v2, v6, LX/0If;->c:Ljava/lang/String;

    .line 25731
    iget-object v7, v0, LX/0Ig;->b:LX/01o;

    invoke-virtual {v7}, LX/01o;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v6, LX/0If;->d:Ljava/lang/Long;

    .line 25732
    invoke-static {v1, v6, v5}, LX/0Ig;->a(Ljava/lang/String;LX/0If;Landroid/content/SharedPreferences;)Z

    move-result v7

    goto :goto_3
.end method

.method private a(LX/0Ig;LX/0IK;LX/0IT;LX/04v;LX/0Ic;LX/0IP;)V
    .locals 1

    .prologue
    .line 25733
    iput-object p1, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    .line 25734
    iput-object p2, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    .line 25735
    iput-object p3, p0, Lcom/facebook/rti/push/service/FbnsService;->x:LX/0IT;

    .line 25736
    new-instance v0, LX/0Ib;

    invoke-direct {v0}, LX/0Ib;-><init>()V

    iput-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->r:LX/0Ib;

    .line 25737
    iput-object p4, p0, Lcom/facebook/rti/push/service/FbnsService;->u:LX/04v;

    .line 25738
    iput-object p5, p0, Lcom/facebook/rti/push/service/FbnsService;->y:LX/0Ic;

    .line 25739
    iput-object p6, p0, Lcom/facebook/rti/push/service/FbnsService;->t:LX/0IP;

    .line 25740
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 25741
    const-string v0, "FbnsService"

    const-string v1, "service/registered; package=%s, token=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25742
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->x:LX/0IT;

    invoke-virtual {v0, p1}, LX/0IT;->a(Ljava/lang/String;)V

    .line 25743
    const-string v0, "registered"

    invoke-static {p1, v0, p2}, Lcom/facebook/rti/push/service/FbnsService;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 25744
    invoke-direct {p0, v0}, Lcom/facebook/rti/push/service/FbnsService;->c(Landroid/content/Intent;)V

    .line 25745
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 25655
    invoke-static {p1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 25656
    :cond_0
    const-string v0, "FbnsService"

    const-string v1, "service/register/invalid_input"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25657
    :cond_1
    :goto_0
    return-void

    .line 25658
    :cond_2
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->x:LX/0IT;

    invoke-virtual {v0, p1, p2, p3}, LX/0IT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 25659
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 25660
    const-string v7, "RegistrationState"

    const-string v10, "add app %s %s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    aput-object p1, v11, v9

    aput-object p2, v11, v8

    invoke-static {v7, v10, v11}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25661
    invoke-static {p1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    move v7, v8

    :goto_1
    invoke-static {v7}, LX/01n;->a(Z)V

    .line 25662
    invoke-static {p2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    :goto_2
    invoke-static {v8}, LX/01n;->a(Z)V

    .line 25663
    new-instance v7, LX/0If;

    invoke-direct {v7}, LX/0If;-><init>()V

    .line 25664
    iput-object p1, v7, LX/0If;->b:Ljava/lang/String;

    .line 25665
    iput-object p2, v7, LX/0If;->a:Ljava/lang/String;

    .line 25666
    iget-object v8, v0, LX/0Ig;->b:LX/01o;

    invoke-virtual {v8}, LX/01o;->a()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    iput-object v8, v7, LX/0If;->d:Ljava/lang/Long;

    .line 25667
    invoke-static {v0}, LX/0Ig;->g(LX/0Ig;)Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-static {p1, v7, v8}, LX/0Ig;->a(Ljava/lang/String;LX/0If;Landroid/content/SharedPreferences;)Z

    .line 25668
    new-instance v0, LX/0IR;

    invoke-direct {v0, p1, p2}, LX/0IR;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 25669
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 25670
    const-string v4, "pkg_name"

    iget-object v5, v0, LX/0IR;->a:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 25671
    const-string v4, "appid"

    iget-object v5, v0, LX/0IR;->b:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 25672
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25673
    const-string v2, "FbnsService"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25674
    new-instance v2, LX/0Ia;

    invoke-direct {v2, p0}, LX/0Ia;-><init>(Lcom/facebook/rti/push/service/FbnsService;)V

    .line 25675
    :try_start_1
    iget-object v3, p0, LX/051;->c:LX/056;

    const-string v4, "/fbns_reg_req"

    invoke-static {v0}, LX/05V;->b(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v5, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    invoke-virtual {v3, v4, v0, v5, v2}, LX/056;->a(Ljava/lang/String;[BLX/0AL;LX/0AM;)I
    :try_end_1
    .catch LX/0BK; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 25676
    :goto_3
    if-ne v0, v1, :cond_1

    .line 25677
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0II;->FAILURE_MQTT_NOT_CONNECTED:LX/0II;

    invoke-virtual {v0, v1, v6}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 25678
    :catch_0
    move-exception v0

    .line 25679
    const-string v1, "FbnsService"

    const-string v2, "service/register/serialize_exception"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25680
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0II;->FAILURE_UNKNOWN_CLIENT_ERROR:LX/0II;

    invoke-virtual {v0, v1, v6}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move v0, v1

    goto :goto_3

    :cond_3
    move v7, v9

    .line 25681
    goto :goto_1

    :cond_4
    move v8, v9

    .line 25682
    goto :goto_2
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 25746
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.rti.fbns.intent.RECEIVE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 25747
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 25748
    invoke-virtual {v0, p0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 25749
    const-string v1, "receive_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25750
    if-eqz p2, :cond_0

    .line 25751
    const-string v1, "data"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25752
    :cond_0
    return-object v0
.end method

.method private b(LX/0IS;)V
    .locals 9

    .prologue
    .line 25753
    iget-object v0, p1, LX/0IS;->a:Ljava/lang/String;

    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25754
    const-string v0, "FbnsService"

    const-string v1, "service/register/response/empty_package"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25755
    :goto_0
    return-void

    .line 25756
    :cond_0
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    iget-object v1, p1, LX/0IS;->a:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 25757
    const-string v5, "RegistrationState"

    const-string v6, "invalidateTokenCache %s"

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v1, v7, v4

    invoke-static {v5, v6, v7}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25758
    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    :goto_1
    invoke-static {v3}, LX/01n;->a(Z)V

    .line 25759
    invoke-static {v0}, LX/0Ig;->g(LX/0Ig;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 25760
    invoke-static {v1, v3}, LX/0Ig;->a(Ljava/lang/String;Landroid/content/SharedPreferences;)LX/0If;

    move-result-object v5

    .line 25761
    if-nez v5, :cond_2

    .line 25762
    const-string v3, "RegistrationState"

    const-string v5, "Missing entry"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v5, v4}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25763
    :goto_2
    goto :goto_0

    :cond_1
    move v3, v4

    .line 25764
    goto :goto_1

    .line 25765
    :cond_2
    const-string v4, ""

    iput-object v4, v5, LX/0If;->c:Ljava/lang/String;

    .line 25766
    iget-object v4, v0, LX/0Ig;->b:LX/01o;

    invoke-virtual {v4}, LX/01o;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v5, LX/0If;->d:Ljava/lang/Long;

    .line 25767
    invoke-static {v1, v5, v3}, LX/0Ig;->a(Ljava/lang/String;LX/0If;Landroid/content/SharedPreferences;)Z

    goto :goto_2
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 25768
    new-instance v0, LX/0IS;

    invoke-direct {v0}, LX/0IS;-><init>()V

    .line 25769
    if-nez p1, :cond_1

    .line 25770
    :goto_0
    move-object v0, v0

    .line 25771
    iget-object v1, v0, LX/0IS;->c:Ljava/lang/String;

    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25772
    invoke-direct {p0, v0}, Lcom/facebook/rti/push/service/FbnsService;->a(LX/0IS;)V

    .line 25773
    :goto_1
    return-void

    .line 25774
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/rti/push/service/FbnsService;->b(LX/0IS;)V

    .line 25775
    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v2, LX/0II;->FAILURE_SERVER_RESPOND_WITH_ERROR:LX/0II;

    iget-object v0, v0, LX/0IS;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    goto :goto_1

    .line 25776
    :cond_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 25777
    const-string v2, "pkg_name"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/0IS;->a:Ljava/lang/String;

    .line 25778
    const-string v2, "token"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/0IS;->b:Ljava/lang/String;

    .line 25779
    const-string v2, "error"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/0IS;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, -0x1

    .line 25780
    new-instance v0, LX/0Id;

    invoke-direct {v0, p1, p2}, LX/0Id;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 25781
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 25782
    const-string v3, "tk"

    iget-object v4, v0, LX/0Id;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 25783
    const-string v3, "pn"

    iget-object v4, v0, LX/0Id;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 25784
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25785
    new-instance v2, LX/0IU;

    invoke-direct {v2, p0}, LX/0IU;-><init>(Lcom/facebook/rti/push/service/FbnsService;)V

    .line 25786
    :try_start_1
    iget-object v3, p0, LX/051;->c:LX/056;

    const-string v4, "/fbns_unreg_req"

    invoke-static {v0}, LX/05V;->b(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v5, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    invoke-virtual {v3, v4, v0, v5, v2}, LX/056;->a(Ljava/lang/String;[BLX/0AL;LX/0AM;)I
    :try_end_1
    .catch LX/0BK; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 25787
    :goto_0
    if-ne v0, v1, :cond_0

    .line 25788
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0II;->UNREGISTER_FAILURE_MQTT_NOT_CONNECTED:LX/0II;

    invoke-virtual {v0, v1, v6}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    .line 25789
    :cond_0
    :goto_1
    return-void

    .line 25790
    :catch_0
    move-exception v0

    .line 25791
    const-string v1, "FbnsService"

    const-string v2, "service/unregister/serialization_exception"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25792
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0II;->FAILURE_UNKNOWN_CLIENT_ERROR:LX/0II;

    invoke-virtual {v0, v1, v6}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move v0, v1

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 25793
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 25794
    const-string v0, "com.facebook.rti.fbns.intent.REGISTER"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.facebook.rti.fbns.intent.REGISTER_RETRY"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.facebook.rti.fbns.intent.UNREGISTER"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 25795
    :cond_0
    const-string v0, "pkg_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 25796
    invoke-static {p1}, LX/04v;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    .line 25797
    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 25798
    const-string v5, "FbnsService"

    const-string v6, "Empty package name for %s from %s"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v3, v7, v1

    aput-object v4, v7, v2

    invoke-static {v5, v6, v7}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25799
    iget-object v2, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v5, LX/0II;->FAILURE_EMPTY_PACKAGE_NAME:LX/0II;

    invoke-virtual {v2, v5, v3, v4, v0}, LX/0IK;->a(LX/0II;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 25800
    :goto_0
    return v0

    .line 25801
    :cond_1
    const-string v5, "com.facebook.rti.fbns.intent.REGISTER_RETRY"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 25802
    invoke-virtual {p0}, Lcom/facebook/rti/push/service/FbnsService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 25803
    :cond_2
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 25804
    const-string v5, "FbnsService"

    const-string v6, "Package mismatch for %s from %s: packageName %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v3, v7, v1

    aput-object v4, v7, v2

    aput-object v0, v7, v8

    invoke-static {v5, v6, v7}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25805
    iget-object v2, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v5, LX/0II;->FAILURE_PACKAGE_DOES_NOT_MATCH_INTENT:LX/0II;

    invoke-virtual {v2, v5, v3, v4, v0}, LX/0IK;->a(LX/0II;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 25806
    goto :goto_0

    :cond_3
    move v0, v2

    .line 25807
    goto :goto_0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 25808
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    .line 25809
    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 25810
    :cond_0
    :goto_0
    return-void

    .line 25811
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/rti/push/service/FbnsService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 25812
    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->u:LX/04v;

    invoke-virtual {v1, p1, v0}, LX/04v;->a(Landroid/content/Intent;Ljava/lang/String;)Z

    goto :goto_0

    .line 25813
    :cond_2
    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->u:LX/04v;

    invoke-virtual {v1, v0}, LX/04v;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 25814
    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    invoke-virtual {v1, v0}, LX/0Ig;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 25815
    if-eqz v1, :cond_0

    .line 25816
    invoke-direct {p0, v1, v0}, Lcom/facebook/rti/push/service/FbnsService;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 25817
    :cond_3
    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->u:LX/04v;

    invoke-virtual {v1, p1, v0}, LX/04v;->a(Landroid/content/Intent;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 25818
    new-instance v0, LX/0IQ;

    invoke-direct {v0}, LX/0IQ;-><init>()V

    .line 25819
    if-nez p1, :cond_5

    .line 25820
    :goto_0
    move-object v10, v0

    .line 25821
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->r:LX/0Ib;

    invoke-static {v0, v10}, LX/0Ib;->b(LX/0Ib;LX/0IQ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25822
    const-string v0, "FbnsService"

    const-string v1, "receive/message; duplicatedNotif=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v10, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25823
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0IH;->DUPLICATED_NOTIFICATION:LX/0IH;

    iget-object v2, v10, LX/0IQ;->f:Ljava/lang/String;

    iget-object v3, v10, LX/0IQ;->g:Ljava/lang/String;

    iget-object v4, v10, LX/0IQ;->c:Ljava/lang/String;

    iget-wide v5, p0, LX/051;->l:J

    iget-object v7, p0, LX/051;->i:LX/05b;

    invoke-virtual {v7}, LX/05b;->b()Z

    move-result v7

    iget-object v8, p0, LX/051;->i:LX/05b;

    invoke-virtual {v8}, LX/05b;->c()J

    move-result-wide v8

    invoke-virtual/range {v0 .. v9}, LX/0IK;->a(LX/0IH;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZJ)V

    .line 25824
    :goto_1
    return-void

    .line 25825
    :cond_0
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->r:LX/0Ib;

    invoke-static {v0, v10}, LX/0Ib;->a(LX/0Ib;LX/0IQ;)V

    .line 25826
    iget-object v0, v10, LX/0IQ;->c:Ljava/lang/String;

    const-string v1, "message"

    iget-object v2, v10, LX/0IQ;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/rti/push/service/FbnsService;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 25827
    iget-object v1, v10, LX/0IQ;->a:Ljava/lang/String;

    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 25828
    const-string v1, "token"

    iget-object v2, v10, LX/0IQ;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25829
    :cond_1
    iget-object v1, v10, LX/0IQ;->d:Ljava/lang/String;

    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 25830
    const-string v1, "collapse_key"

    iget-object v2, v10, LX/0IQ;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25831
    :cond_2
    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->t:LX/0IP;

    iget-object v2, v10, LX/0IQ;->f:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 25832
    invoke-virtual {v0}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v5

    .line 25833
    invoke-static {v2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-static {v5}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    sget-object v6, LX/0IP;->h:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    :cond_3
    move v3, v4

    .line 25834
    :goto_2
    move v1, v3

    .line 25835
    if-nez v1, :cond_4

    .line 25836
    invoke-direct {p0, v0}, Lcom/facebook/rti/push/service/FbnsService;->c(Landroid/content/Intent;)V

    .line 25837
    :cond_4
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0IH;->NOTIFICATION_RECEIVED:LX/0IH;

    iget-object v2, v10, LX/0IQ;->f:Ljava/lang/String;

    iget-object v3, v10, LX/0IQ;->g:Ljava/lang/String;

    iget-object v4, v10, LX/0IQ;->c:Ljava/lang/String;

    iget-wide v5, p0, LX/051;->l:J

    iget-object v7, p0, LX/051;->i:LX/05b;

    invoke-virtual {v7}, LX/05b;->b()Z

    move-result v7

    iget-object v8, p0, LX/051;->i:LX/05b;

    invoke-virtual {v8}, LX/05b;->c()J

    move-result-wide v8

    invoke-virtual/range {v0 .. v9}, LX/0IK;->a(LX/0IH;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZJ)V

    .line 25838
    iget-object v0, p0, LX/051;->h:LX/05i;

    iget-object v1, v10, LX/0IQ;->c:Ljava/lang/String;

    .line 25839
    iget-object v2, v0, LX/05i;->m:Ljava/util/concurrent/ConcurrentMap;

    new-instance v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    invoke-interface {v2, v1, v3}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25840
    iget-object v2, v0, LX/05i;->m:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 25841
    goto/16 :goto_1

    .line 25842
    :cond_5
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 25843
    const-string v2, "token"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/0IQ;->a:Ljava/lang/String;

    .line 25844
    const-string v2, "ck"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/0IQ;->b:Ljava/lang/String;

    .line 25845
    const-string v2, "pn"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/0IQ;->c:Ljava/lang/String;

    .line 25846
    const-string v2, "cp"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/0IQ;->d:Ljava/lang/String;

    .line 25847
    const-string v2, "fbpushnotif"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/0IQ;->e:Ljava/lang/String;

    .line 25848
    const-string v2, "nid"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/0IQ;->f:Ljava/lang/String;

    .line 25849
    const-string v2, "bu"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/0IQ;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 25850
    :cond_6
    const-string v6, "extra_notification_sender"

    iget-object v7, v1, LX/05I;->a:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25851
    const-string v6, "extra_notification_id"

    invoke-virtual {v0, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25852
    invoke-static {v1, v0, v5}, LX/0IP;->a(LX/0IP;Landroid/content/Intent;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 25853
    invoke-virtual {v1}, LX/05I;->e()LX/0BE;

    move-result-object v5

    invoke-interface {v5, v2, v0}, LX/0BE;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 25854
    const-string v5, "FbnsLiteNotificationDeliveryHelper"

    const-string v6, "deliverFbnsLiteNotification %s"

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v2, v7, v4

    invoke-static {v5, v6, v7}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_7
    move v3, v4

    .line 25855
    goto/16 :goto_2
.end method

.method private d(Landroid/content/Intent;)V
    .locals 7
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 25856
    const-string v0, "pkg_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 25857
    const-string v1, "appid"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 25858
    iget-object v2, p0, Lcom/facebook/rti/push/service/FbnsService;->x:LX/0IT;

    invoke-virtual {v2, v0}, LX/0IT;->a(Ljava/lang/String;)V

    .line 25859
    iget-object v2, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_0

    .line 25860
    const-string v2, "FbnsService"

    const-string v3, "service/register/not_started"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25861
    iget-object v2, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v3, LX/0II;->FAILURE_SERVICE_NOT_STARTED:LX/0II;

    invoke-virtual {v2, v3, v6}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    .line 25862
    :cond_0
    const-string v2, "FbnsService"

    const-string v3, "service/register; appId=%s, package=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25863
    iget-object v2, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v3, LX/0II;->REGISTER:LX/0II;

    invoke-virtual {v2, v3, v0}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    .line 25864
    iget-object v2, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    invoke-virtual {v2, v0}, LX/0Ig;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 25865
    invoke-static {v2}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 25866
    invoke-direct {p0, v0, v2}, Lcom/facebook/rti/push/service/FbnsService;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 25867
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0II;->CACHE_HIT:LX/0II;

    invoke-virtual {v0, v1, v6}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    .line 25868
    :goto_0
    return-void

    .line 25869
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/rti/push/service/FbnsService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private e(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 25870
    const-string v0, "pkg_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 25871
    const-string v1, "appid"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 25872
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/rti/push/service/FbnsService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 25873
    return-void
.end method

.method private f(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 25683
    const-string v0, "pkg_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 25684
    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    invoke-virtual {v1, v0}, LX/0Ig;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 25685
    iget-object v2, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 25686
    const-string v6, "RegistrationState"

    const-string v7, "remove app %s"

    new-array p1, v3, [Ljava/lang/Object;

    aput-object v0, p1, v5

    invoke-static {v6, v7, p1}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25687
    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    :goto_0
    invoke-static {v3}, LX/01n;->a(Z)V

    .line 25688
    invoke-static {v2}, LX/0Ig;->g(LX/0Ig;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 25689
    invoke-interface {v3, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 25690
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-static {v3}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 25691
    :cond_0
    const-string v2, "unregistered"

    invoke-static {v0, v2, v4}, Lcom/facebook/rti/push/service/FbnsService;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 25692
    invoke-direct {p0, v2}, Lcom/facebook/rti/push/service/FbnsService;->c(Landroid/content/Intent;)V

    .line 25693
    iget-object v2, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v3, LX/0II;->UNREGISTER_CALLED:LX/0II;

    invoke-virtual {v2, v3, v4}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    .line 25694
    if-eqz v1, :cond_1

    .line 25695
    invoke-direct {p0, v1, v0}, Lcom/facebook/rti/push/service/FbnsService;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 25696
    :cond_1
    return-void

    :cond_2
    move v3, v5

    .line 25697
    goto :goto_0
.end method

.method public static q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25517
    const-string v0, "FBNS"

    return-object v0
.end method


# virtual methods
.method public final a(LX/0AX;)Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0AX;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 25518
    invoke-static {p0}, LX/04u;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25519
    const-string v0, "FbnsService"

    const-string v1, "service/FBNS_STOPPED"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25520
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->u:LX/04v;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.rti.intent.ACTION_FBNS_STOPPED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/04v;->d(Landroid/content/Intent;)V

    .line 25521
    :cond_0
    invoke-super {p0, p1}, LX/051;->a(LX/0AX;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/06f;)V
    .locals 3

    .prologue
    .line 25522
    iget-object v0, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    .line 25523
    invoke-super {p0, p1}, LX/051;->a(LX/06f;)V

    .line 25524
    if-nez v0, :cond_0

    invoke-static {p0}, LX/04u;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25525
    const-string v0, "FbnsService"

    const-string v1, "service/FBNS_STARTED"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25526
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->u:LX/04v;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.rti.intent.ACTION_FBNS_STARTED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/04v;->d(Landroid/content/Intent;)V

    .line 25527
    :cond_0
    return-void
.end method

.method public final a(LX/07W;)V
    .locals 4

    .prologue
    .line 25592
    invoke-super {p0, p1}, LX/051;->a(LX/07W;)V

    .line 25593
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->t:LX/0IP;

    invoke-virtual {v0}, LX/05I;->c()I

    move-result v1

    .line 25594
    iget-object v0, p0, LX/051;->h:LX/05i;

    const-class v2, LX/0BF;

    invoke-virtual {v0, v2}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/0BF;

    sget-object v2, LX/0BG;->FbnsLiteNotificationDeliveryRetried:LX/0BG;

    invoke-virtual {v0, v2}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 25595
    return-void
.end method

.method public final a(LX/0Hs;)V
    .locals 9
    .param p1    # LX/0Hs;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 25528
    sget-object v0, LX/0Hs;->FAILED_CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD:LX/0Hs;

    invoke-virtual {v0, p1}, LX/0Hs;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    .line 25529
    invoke-static {v0}, LX/0Ig;->h(LX/0Ig;)Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "auto_reg_retry"

    const-wide/16 v7, 0x0

    invoke-interface {v5, v6, v7, v8}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    .line 25530
    iget-object v7, v0, LX/0Ig;->b:LX/01o;

    invoke-virtual {v7}, LX/01o;->a()J

    move-result-wide v7

    sub-long v5, v7, v5

    const-wide/32 v7, 0x5265c00

    cmp-long v5, v5, v7

    if-lez v5, :cond_1

    const/4 v5, 0x1

    :goto_0
    move v0, v5

    .line 25531
    if-eqz v0, :cond_0

    .line 25532
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    .line 25533
    invoke-static {v0}, LX/0Ig;->h(LX/0Ig;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "auto_reg_retry"

    iget-object v7, v0, LX/0Ig;->b:LX/01o;

    invoke-virtual {v7}, LX/01o;->a()J

    move-result-wide v7

    invoke-interface {v5, v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-static {v5}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 25534
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    invoke-virtual {v0}, LX/0Ig;->b()Ljava/util/List;

    move-result-object v0

    .line 25535
    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    invoke-virtual {v1}, LX/0Ig;->a()V

    .line 25536
    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v2, LX/0II;->AUTHFAIL_AUTO_REGISTER:LX/0II;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    .line 25537
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0If;

    .line 25538
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.rti.fbns.intent.REGISTER"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 25539
    const-string v3, "pkg_name"

    iget-object v4, v0, LX/0If;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25540
    const-string v3, "appid"

    iget-object v0, v0, LX/0If;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25541
    invoke-virtual {p0}, Lcom/facebook/rti/push/service/FbnsService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25542
    invoke-direct {p0, v2}, Lcom/facebook/rti/push/service/FbnsService;->d(Landroid/content/Intent;)V

    goto :goto_1

    .line 25543
    :cond_0
    return-void

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 25544
    invoke-super {p0, p1}, LX/051;->a(Landroid/content/Intent;)V

    .line 25545
    invoke-direct {p0, p1}, Lcom/facebook/rti/push/service/FbnsService;->b(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 25546
    :cond_0
    :goto_0
    return-void

    .line 25547
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 25548
    const-string v1, "com.facebook.rti.fbns.intent.REGISTER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 25549
    sget-object v0, LX/06f;->FBNS_REGISTER:LX/06f;

    invoke-virtual {p0, v0}, Lcom/facebook/rti/push/service/FbnsService;->a(LX/06f;)V

    .line 25550
    invoke-direct {p0, p1}, Lcom/facebook/rti/push/service/FbnsService;->d(Landroid/content/Intent;)V

    goto :goto_0

    .line 25551
    :cond_2
    const-string v1, "com.facebook.rti.fbns.intent.REGISTER_RETRY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 25552
    sget-object v0, LX/06f;->FBNS_REGISTER_RETRY:LX/06f;

    invoke-virtual {p0, v0}, Lcom/facebook/rti/push/service/FbnsService;->a(LX/06f;)V

    .line 25553
    invoke-direct {p0, p1}, Lcom/facebook/rti/push/service/FbnsService;->e(Landroid/content/Intent;)V

    goto :goto_0

    .line 25554
    :cond_3
    const-string v1, "com.facebook.rti.fbns.intent.UNREGISTER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25555
    sget-object v0, LX/06f;->FBNS_UNREGISTER:LX/06f;

    invoke-virtual {p0, v0}, Lcom/facebook/rti/push/service/FbnsService;->a(LX/06f;)V

    .line 25556
    invoke-direct {p0, p1}, Lcom/facebook/rti/push/service/FbnsService;->f(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;II)V
    .locals 2

    .prologue
    .line 25557
    if-eqz p1, :cond_1

    .line 25558
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->u:LX/04v;

    invoke-virtual {v0, p1}, LX/04v;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 25559
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v1

    .line 25560
    const-string p0, "verify_sender_failed"

    .line 25561
    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/String;

    const/4 p2, 0x0

    const-string p3, "event_type"

    aput-object p3, p1, p2

    const/4 p2, 0x1

    aput-object p0, p1, p2

    invoke-static {p1}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object p0

    .line 25562
    invoke-static {v1}, LX/05V;->a(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 25563
    const-string p1, "event_extra_info"

    invoke-interface {p0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25564
    :cond_0
    const-string p1, "fbns_auth_intent_event"

    invoke-static {v0, p1, p0}, LX/0IK;->a(LX/0IK;Ljava/lang/String;Ljava/util/Map;)V

    .line 25565
    :goto_0
    return-void

    .line 25566
    :cond_1
    invoke-super {p0, p1, p2, p3}, LX/051;->a(Landroid/content/Intent;II)V

    goto :goto_0
.end method

.method public final a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 25567
    :try_start_0
    const-string v0, "[ FbnsService ]"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 25568
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "validCompatibleApps="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 25569
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 25570
    sget-object v1, LX/04u;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 25571
    const/16 v4, 0x40

    invoke-static {p0, v1, v4}, LX/05U;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 25572
    invoke-static {p0, v4}, LX/05U;->a(Landroid/content/Context;Landroid/content/pm/PackageInfo;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {p0, v1}, LX/0Hk;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 25573
    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 25574
    :cond_1
    move-object v1, v2

    .line 25575
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 25576
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "registeredApps="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    .line 25577
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 25578
    invoke-virtual {v1}, LX/0Ig;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0If;

    .line 25579
    iget-object v2, v2, LX/0If;->b:Ljava/lang/String;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 25580
    :cond_2
    move-object v1, v3

    .line 25581
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 25582
    sget-object v0, LX/01p;->f:LX/01q;

    invoke-static {p0, v0}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 25583
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "leaderPackage="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "leader_package"

    const-string v3, "N/A"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 25584
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sharedStatus="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "shared_status"

    const-string v3, "N/A"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 25585
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sharingEnabled="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "sharing_state_enabled"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 25586
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "fbnsSharedVersion="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/rti/push/service/FbnsService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, LX/0Hk;->b(Landroid/content/Context;Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 25587
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "notificationCounter="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/051;->h:LX/05i;

    .line 25588
    iget-object v2, v1, LX/05i;->m:Ljava/util/concurrent/ConcurrentMap;

    move-object v1, v2

    .line 25589
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 25590
    :goto_2
    invoke-super {p0, p1, p2, p3}, LX/051;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 25591
    return-void

    :catch_0
    goto :goto_2
.end method

.method public final a(Ljava/lang/String;[BJ)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 25501
    if-nez p2, :cond_0

    .line 25502
    const-string v0, "FbnsService"

    const-string v1, "receive/publish/empty_payload; topic=%s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25503
    :goto_0
    return-void

    .line 25504
    :cond_0
    sget v0, LX/05D;->a:I

    move v0, v0

    .line 25505
    const/4 v1, 0x3

    if-gt v0, v1, :cond_1

    .line 25506
    const-string v0, "FbnsService"

    const-string v1, "receive/publish; topic=%s, payload=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {p2}, LX/05V;->a([B)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25507
    :cond_1
    :try_start_0
    invoke-static {p2}, LX/05V;->a([B)Ljava/lang/String;

    move-result-object v0

    .line 25508
    const-string v1, "/fbns_msg"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 25509
    invoke-direct {p0, v0}, Lcom/facebook/rti/push/service/FbnsService;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 25510
    :catch_0
    move-exception v0

    .line 25511
    const-string v1, "FbnsService"

    const-string v2, "receive/publish/payload_exception; topic=%s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25512
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0IJ;->JSON_PARSE_ERROR:LX/0IJ;

    invoke-virtual {v0, v1, p1}, LX/0IK;->a(LX/0IJ;Ljava/lang/String;)V

    goto :goto_0

    .line 25513
    :cond_2
    :try_start_1
    const-string v1, "/fbns_reg_resp"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 25514
    invoke-direct {p0, v0}, Lcom/facebook/rti/push/service/FbnsService;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 25515
    :cond_3
    const-string v0, "FbnsService"

    const-string v1, "receive/publish/wrong_topic; topic=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25516
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v1, LX/0IJ;->UNEXPECTED_TOPIC:LX/0IJ;

    invoke-virtual {v0, v1, p1}, LX/0IK;->a(LX/0IJ;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 25596
    invoke-super {p0}, LX/051;->d()V

    .line 25597
    sget-object v0, Lcom/facebook/rti/push/service/FbnsService;->v:Lcom/facebook/rti/push/service/FbnsService;

    if-ne v0, p0, :cond_0

    .line 25598
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/rti/push/service/FbnsService;->v:Lcom/facebook/rti/push/service/FbnsService;

    .line 25599
    :cond_0
    return-void
.end method

.method public final e()LX/05Q;
    .locals 19

    .prologue
    .line 25600
    sget-object v2, Lcom/facebook/rti/push/service/FbnsService;->v:Lcom/facebook/rti/push/service/FbnsService;

    if-eqz v2, :cond_0

    .line 25601
    sget-object v2, Lcom/facebook/rti/push/service/FbnsService;->v:Lcom/facebook/rti/push/service/FbnsService;

    invoke-virtual {v2}, LX/051;->j()V

    .line 25602
    :cond_0
    sput-object p0, Lcom/facebook/rti/push/service/FbnsService;->v:Lcom/facebook/rti/push/service/FbnsService;

    .line 25603
    new-instance v11, LX/0IV;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, LX/0IV;-><init>(Lcom/facebook/rti/push/service/FbnsService;)V

    .line 25604
    new-instance v12, LX/0IW;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, LX/0IW;-><init>(Lcom/facebook/rti/push/service/FbnsService;)V

    .line 25605
    new-instance v13, LX/0IX;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, LX/0IX;-><init>(Lcom/facebook/rti/push/service/FbnsService;)V

    .line 25606
    new-instance v14, LX/04v;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, LX/04v;-><init>(Landroid/content/Context;)V

    .line 25607
    new-instance v2, LX/0Ij;

    invoke-static {}, LX/01o;->b()LX/01o;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v14, v3}, LX/0Ij;-><init>(Landroid/content/Context;LX/04v;LX/01o;)V

    .line 25608
    new-instance v3, LX/0Ih;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, LX/0Ih;-><init>(Landroid/content/Context;LX/0Ij;)V

    .line 25609
    new-instance v15, LX/0Ic;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v3}, LX/0Ic;-><init>(Lcom/facebook/rti/push/service/FbnsService;LX/0Ih;)V

    .line 25610
    new-instance v16, LX/0IM;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, LX/0IM;-><init>(Landroid/content/Context;)V

    .line 25611
    new-instance v17, LX/05P;

    invoke-direct/range {v17 .. v17}, LX/05P;-><init>()V

    .line 25612
    new-instance v18, LX/0IL;

    invoke-direct/range {v18 .. v18}, LX/0IL;-><init>()V

    .line 25613
    sget-object v2, LX/01p;->b:LX/01q;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 25614
    const-string v2, "logging_health_stats_sample_rate"

    const/4 v3, 0x1

    invoke-interface {v4, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 25615
    if-ltz v2, :cond_1

    const/16 v3, 0x2710

    if-le v2, v3, :cond_2

    .line 25616
    :cond_1
    const-string v3, "FbnsService"

    const-string v5, "Wrong health stats sampling rate found in shared preferences: %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v7

    invoke-static {v3, v5, v6}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25617
    const/4 v2, 0x1

    .line 25618
    :cond_2
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    const/16 v5, 0x2710

    invoke-virtual {v3, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    if-ge v3, v2, :cond_3

    const/4 v2, 0x1

    move v10, v2

    .line 25619
    :goto_0
    const-string v2, "log_analytic_events"

    const/4 v3, 0x0

    invoke-interface {v4, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 25620
    new-instance v3, LX/0IY;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, LX/0IY;-><init>(Lcom/facebook/rti/push/service/FbnsService;Z)V

    invoke-static {}, Lcom/facebook/rti/push/service/FbnsService;->q()Ljava/lang/String;

    move-result-object v5

    new-instance v6, LX/0IZ;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v6, v0, v1}, LX/0IZ;-><init>(Lcom/facebook/rti/push/service/FbnsService;LX/0IM;)V

    invoke-virtual {v15}, LX/0Ic;->c()Ljava/lang/String;

    move-result-object v7

    const-string v8, "567310203415052"

    invoke-virtual {v15}, LX/0Ic;->b()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v9}, LX/0HP;->a(Landroid/content/Context;LX/0IY;Landroid/content/SharedPreferences;Ljava/lang/String;LX/05N;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0HP;

    move-result-object v2

    .line 25621
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, LX/05P;->a(Landroid/content/Context;)LX/05P;

    move-result-object v3

    invoke-static {}, Lcom/facebook/rti/push/service/FbnsService;->q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/05P;->a(Ljava/lang/String;)LX/05P;

    move-result-object v3

    new-instance v4, LX/056;

    invoke-direct {v4}, LX/056;-><init>()V

    invoke-virtual {v3, v4}, LX/05P;->a(LX/056;)LX/05P;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/051;->n:LX/055;

    invoke-virtual {v3, v4}, LX/05P;->a(LX/055;)LX/05P;

    move-result-object v3

    invoke-virtual {v3, v15}, LX/05P;->a(LX/05G;)LX/05P;

    move-result-object v3

    new-instance v4, LX/0Ie;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, LX/0Ie;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4}, LX/05P;->a(LX/05F;)LX/05P;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/05P;->b(LX/05G;)LX/05P;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/05P;->b(LX/05F;)LX/05P;

    move-result-object v3

    new-instance v4, LX/0I5;

    invoke-direct {v4}, LX/0I5;-><init>()V

    invoke-virtual {v3, v4}, LX/05P;->a(LX/05L;)LX/05P;

    move-result-object v3

    new-instance v4, LX/05K;

    invoke-direct {v4}, LX/05K;-><init>()V

    invoke-virtual {v3, v4}, LX/05P;->a(LX/05K;)LX/05P;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, LX/05P;->a(LX/04p;)LX/05P;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/05P;->a(LX/05N;)LX/05P;

    move-result-object v3

    invoke-virtual {v3, v11}, LX/05P;->b(LX/05N;)LX/05P;

    move-result-object v3

    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v3, v4}, LX/05P;->a(Landroid/os/Handler;)LX/05P;

    move-result-object v3

    new-instance v4, LX/0HR;

    invoke-direct {v4}, LX/0HR;-><init>()V

    invoke-virtual {v3, v4}, LX/05P;->a(LX/05R;)LX/05P;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/05P;->a(LX/05O;)LX/05P;

    move-result-object v2

    invoke-virtual {v2, v14}, LX/05P;->a(LX/04v;)LX/05P;

    move-result-object v2

    invoke-virtual {v2, v13}, LX/05P;->c(LX/05N;)LX/05P;

    move-result-object v2

    invoke-virtual {v2, v13}, LX/05P;->f(LX/05N;)LX/05P;

    move-result-object v2

    invoke-virtual {v2, v13}, LX/05P;->g(LX/05N;)LX/05P;

    move-result-object v2

    invoke-virtual {v2, v13}, LX/05P;->h(LX/05N;)LX/05P;

    move-result-object v2

    invoke-virtual {v2, v12}, LX/05P;->d(LX/05N;)LX/05P;

    move-result-object v2

    invoke-virtual {v2, v13}, LX/05P;->e(LX/05N;)LX/05P;

    move-result-object v2

    new-instance v3, LX/0IN;

    move-object/from16 v0, v16

    invoke-direct {v3, v0}, LX/0IN;-><init>(LX/04p;)V

    invoke-virtual {v2, v3}, LX/05P;->a(LX/05H;)LX/05P;

    move-result-object v2

    new-instance v3, LX/05M;

    invoke-direct {v3}, LX/05M;-><init>()V

    invoke-virtual {v2, v3}, LX/05P;->a(LX/05M;)LX/05P;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/05P;->a(Ljava/util/concurrent/atomic/AtomicReference;)LX/05P;

    move-result-object v2

    const-string v3, "567310203415052"

    invoke-virtual {v2, v3}, LX/05P;->b(Ljava/lang/String;)LX/05P;

    move-result-object v2

    invoke-virtual {v2, v13}, LX/05P;->i(LX/05N;)LX/05P;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/05P;->e(Z)LX/05P;

    move-result-object v2

    invoke-virtual {v2}, LX/05P;->a()LX/05S;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v15, v2}, LX/0IL;->a(LX/0Ic;LX/05S;)V

    .line 25622
    return-object v18

    .line 25623
    :cond_3
    const/4 v2, 0x0

    move v10, v2

    goto/16 :goto_0
.end method

.method public final f()V
    .locals 8

    .prologue
    .line 25624
    invoke-super {p0}, LX/051;->f()V

    .line 25625
    iget-object v0, p0, LX/051;->b:LX/05Q;

    check-cast v0, LX/0IL;

    .line 25626
    iget-object v1, v0, LX/0IL;->E:LX/0Ig;

    iget-object v2, v0, LX/0IL;->G:LX/0IK;

    iget-object v3, v0, LX/0IL;->F:LX/0IT;

    iget-object v4, p0, LX/051;->b:LX/05Q;

    iget-object v4, v4, LX/05Q;->B:LX/04v;

    iget-object v5, v0, LX/0IL;->H:LX/0Ic;

    new-instance v6, LX/0IP;

    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v0, v0, LX/05Q;->B:LX/04v;

    iget-object v7, p0, LX/051;->b:LX/05Q;

    iget-object v7, v7, LX/05Q;->i:LX/01o;

    invoke-direct {v6, p0, v0, v7}, LX/0IP;-><init>(Lcom/facebook/rti/push/service/FbnsService;LX/04v;LX/01o;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/rti/push/service/FbnsService;->a(LX/0Ig;LX/0IK;LX/0IT;LX/04v;LX/0Ic;LX/0IP;)V

    .line 25627
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 25628
    invoke-super {p0}, LX/051;->g()V

    .line 25629
    iget-object v0, p0, LX/051;->h:LX/05i;

    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->t:LX/0IP;

    .line 25630
    invoke-virtual {v1}, LX/05I;->e()LX/0BE;

    move-result-object v2

    invoke-interface {v2}, LX/0BE;->a()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 25631
    iput-object v1, v0, LX/05i;->p:Ljava/lang/String;

    .line 25632
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->y:LX/0Ic;

    if-eqz v0, :cond_0

    .line 25633
    sget-object v0, LX/01p;->f:LX/01q;

    invoke-static {p0, v0}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 25634
    const-string v1, "sharing_state_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 25635
    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->y:LX/0Ic;

    invoke-virtual {v1, v0}, LX/0Ic;->a(Z)V

    .line 25636
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 25637
    invoke-super {p0}, LX/051;->h()V

    .line 25638
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->t:LX/0IP;

    invoke-virtual {v0}, LX/05I;->a()V

    .line 25639
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 25640
    invoke-super {p0}, LX/051;->i()V

    .line 25641
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->t:LX/0IP;

    invoke-virtual {v0}, LX/05I;->b()V

    .line 25642
    return-void
.end method

.method public final o()V
    .locals 5

    .prologue
    .line 25643
    iget-object v0, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    invoke-virtual {v0}, LX/0Ig;->b()Ljava/util/List;

    move-result-object v0

    .line 25644
    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->s:LX/0Ig;

    invoke-virtual {v1}, LX/0Ig;->a()V

    .line 25645
    iget-object v1, p0, Lcom/facebook/rti/push/service/FbnsService;->w:LX/0IK;

    sget-object v2, LX/0II;->CREDENTIALS_UPDATED:LX/0II;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0IK;->a(LX/0II;Ljava/lang/String;)V

    .line 25646
    sget-object v1, LX/06f;->CREDENTIALS_UPDATED:LX/06f;

    invoke-virtual {p0, v1}, Lcom/facebook/rti/push/service/FbnsService;->a(LX/06f;)V

    .line 25647
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0If;

    .line 25648
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.rti.fbns.intent.REGISTER"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 25649
    const-string v3, "pkg_name"

    iget-object v4, v0, LX/0If;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25650
    const-string v3, "appid"

    iget-object v0, v0, LX/0If;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25651
    invoke-virtual {p0}, Lcom/facebook/rti/push/service/FbnsService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25652
    invoke-direct {p0, v2}, Lcom/facebook/rti/push/service/FbnsService;->d(Landroid/content/Intent;)V

    goto :goto_0

    .line 25653
    :cond_0
    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25654
    const-string v0, "FBNS_ALWAYS"

    return-object v0
.end method
