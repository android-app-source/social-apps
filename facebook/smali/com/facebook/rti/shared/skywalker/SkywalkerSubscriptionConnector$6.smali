.class public final Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/0lF;

.field public final synthetic c:LX/09G;


# direct methods
.method public constructor <init>(LX/09G;Ljava/lang/String;LX/0lF;)V
    .locals 0

    .prologue
    .line 22324
    iput-object p1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$6;->c:LX/09G;

    iput-object p2, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$6;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$6;->b:LX/0lF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 22325
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$6;->c:LX/09G;

    iget-object v0, v0, LX/09G;->f:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->e()LX/0m9;

    move-result-object v0

    .line 22326
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$6;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$6;->b:LX/0lF;

    invoke-virtual {v2}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 22327
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$6;->c:LX/09G;

    invoke-static {v1, v3, v3, v0}, LX/09G;->a(LX/09G;LX/162;LX/162;LX/0lF;)LX/0m9;

    move-result-object v0

    .line 22328
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$6;->c:LX/09G;

    iget-object v1, v1, LX/09G;->d:LX/2Hu;

    invoke-virtual {v1}, LX/2Hu;->a()LX/2gV;

    move-result-object v1

    .line 22329
    :try_start_0
    const-string v2, "/pubsub"

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v0, v4, v5}, LX/2gV;->a(Ljava/lang/String;LX/0lF;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22330
    invoke-virtual {v1}, LX/2gV;->f()V

    .line 22331
    :goto_0
    return-void

    .line 22332
    :catch_0
    move-exception v0

    .line 22333
    :try_start_1
    sget-object v2, LX/09G;->a:Ljava/lang/Class;

    const-string v3, "Remote exception for publish"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 22334
    invoke-virtual {v1}, LX/2gV;->f()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0
.end method
