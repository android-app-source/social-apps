.class public final Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/09G;


# direct methods
.method public constructor <init>(LX/09G;)V
    .locals 0

    .prologue
    .line 22268
    iput-object p1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$7;->a:LX/09G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 22269
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$7;->a:LX/09G;

    monitor-enter v1

    .line 22270
    :try_start_0
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$7;->a:LX/09G;

    iget-object v0, v0, LX/09G;->i:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v2

    .line 22271
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$7;->a:LX/09G;

    iget-object v0, v0, LX/09G;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 22272
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22273
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$7;->a:LX/09G;

    iget-object v0, v0, LX/09G;->f:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->f()LX/162;

    move-result-object v1

    .line 22274
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 22275
    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 22276
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 22277
    :cond_0
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$7;->a:LX/09G;

    invoke-static {v0, v1}, LX/09G;->a$redex0(LX/09G;LX/162;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22278
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$7;->a:LX/09G;

    monitor-enter v1

    .line 22279
    :try_start_2
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$7;->a:LX/09G;

    iget-object v0, v0, LX/09G;->h:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 22280
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$7;->a:LX/09G;

    iget-object v0, v0, LX/09G;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 22281
    monitor-exit v1

    .line 22282
    :cond_1
    return-void

    .line 22283
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
