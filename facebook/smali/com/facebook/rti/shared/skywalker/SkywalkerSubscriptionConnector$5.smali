.class public final Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/09G;


# direct methods
.method public constructor <init>(LX/09G;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22284
    iput-object p1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->b:LX/09G;

    iput-object p2, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 22285
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->b:LX/09G;

    iget-object v0, v0, LX/09G;->f:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->f()LX/162;

    move-result-object v0

    .line 22286
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 22287
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->b:LX/09G;

    invoke-static {v1, v2, v0, v2}, LX/09G;->a(LX/09G;LX/162;LX/162;LX/0lF;)LX/0m9;

    move-result-object v0

    .line 22288
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->b:LX/09G;

    iget-object v1, v1, LX/09G;->d:LX/2Hu;

    invoke-virtual {v1}, LX/2Hu;->a()LX/2gV;

    move-result-object v1

    .line 22289
    :try_start_0
    const-string v2, "/pubsub"

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v0, v4, v5}, LX/2gV;->a(Ljava/lang/String;LX/0lF;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22290
    invoke-virtual {v1}, LX/2gV;->f()V

    .line 22291
    :goto_0
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->b:LX/09G;

    monitor-enter v1

    .line 22292
    :try_start_1
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->b:LX/09G;

    iget-object v0, v0, LX/09G;->h:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22293
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->b:LX/09G;

    iget-object v0, v0, LX/09G;->h:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22294
    :cond_0
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->b:LX/09G;

    iget-object v0, v0, LX/09G;->i:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22295
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->b:LX/09G;

    iget-object v0, v0, LX/09G;->i:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$5;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22296
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 22297
    :catch_0
    move-exception v0

    .line 22298
    :try_start_2
    sget-object v2, LX/09G;->a:Ljava/lang/Class;

    const-string v3, "Remote exception for unsubscribe"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 22299
    invoke-virtual {v1}, LX/2gV;->f()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0

    .line 22300
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
