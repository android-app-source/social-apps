.class public final Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/0TF;

.field public final synthetic c:LX/09K;

.field public final synthetic d:LX/09G;


# direct methods
.method public constructor <init>(LX/09G;Ljava/lang/String;LX/0TF;LX/09K;)V
    .locals 0

    .prologue
    .line 22301
    iput-object p1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->d:LX/09G;

    iput-object p2, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->b:LX/0TF;

    iput-object p4, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->c:LX/09K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 22302
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->d:LX/09G;

    monitor-enter v1

    .line 22303
    :try_start_0
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->d:LX/09G;

    iget-object v0, v0, LX/09G;->h:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22304
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->d:LX/09G;

    iget-object v0, v0, LX/09G;->h:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->b:LX/0TF;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22305
    monitor-exit v1

    .line 22306
    :cond_0
    :goto_0
    return-void

    .line 22307
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22308
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->d:LX/09G;

    iget-object v0, v0, LX/09G;->f:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->f()LX/162;

    move-result-object v0

    .line 22309
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 22310
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->d:LX/09G;

    invoke-static {v1, v0}, LX/09G;->a$redex0(LX/09G;LX/162;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 22311
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->d:LX/09G;

    monitor-enter v1

    .line 22312
    :try_start_1
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->d:LX/09G;

    iget-object v0, v0, LX/09G;->h:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->b:LX/0TF;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22313
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 22314
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->c:LX/09K;

    if-eqz v0, :cond_0

    .line 22315
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->c:LX/09K;

    invoke-interface {v0}, LX/09K;->a()V

    goto :goto_0

    .line 22316
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 22317
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 22318
    :cond_2
    iget-object v1, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->d:LX/09G;

    monitor-enter v1

    .line 22319
    :try_start_4
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->d:LX/09G;

    iget-object v0, v0, LX/09G;->i:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->b:LX/0TF;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22320
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 22321
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->c:LX/09K;

    if-eqz v0, :cond_0

    .line 22322
    iget-object v0, p0, Lcom/facebook/rti/shared/skywalker/SkywalkerSubscriptionConnector$4;->c:LX/09K;

    invoke-interface {v0}, LX/09K;->b()V

    goto :goto_0

    .line 22323
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method
