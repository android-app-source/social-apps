.class public final Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/07k;

.field public final synthetic b:LX/077;


# direct methods
.method public constructor <init>(LX/077;LX/07k;)V
    .locals 0

    .prologue
    .line 20233
    iput-object p1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iput-object p2, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->a:LX/07k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 20234
    const-string v0, "FbnsConnectionManager"

    const-string v1, "connection/established/runnable"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20235
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-boolean v0, v0, LX/077;->c:Z

    if-nez v0, :cond_0

    .line 20236
    const-string v0, "FbnsConnectionManager"

    const-string v1, "FastSend disabled"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20237
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    invoke-static {v0}, LX/077;->d(LX/077;)V

    .line 20238
    :cond_0
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->b:LX/072;

    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v1, v1, LX/077;->b:LX/072;

    if-ne v0, v1, :cond_7

    .line 20239
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->a:LX/07k;

    iget-object v0, v0, LX/07k;->e:LX/05B;

    invoke-virtual {v0}, LX/05B;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 20240
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v1, v0, LX/056;->u:LX/05F;

    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->a:LX/07k;

    iget-object v0, v0, LX/07k;->e:LX/05B;

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/06k;

    invoke-interface {v1, v0}, LX/05F;->a(LX/06k;)Z

    move-result v0

    or-int/lit8 v1, v0, 0x0

    .line 20241
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->w:LX/05F;

    if-eqz v0, :cond_1

    .line 20242
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v3, v0, LX/056;->w:LX/05F;

    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->a:LX/07k;

    iget-object v0, v0, LX/07k;->e:LX/05B;

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/06k;

    invoke-interface {v3, v0}, LX/05F;->a(LX/06k;)Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    .line 20243
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->G:Ljava/lang/String;

    invoke-static {v0}, LX/04u;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->a:LX/07k;

    iget-object v0, v0, LX/07k;->f:LX/05B;

    invoke-virtual {v0}, LX/05B;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 20244
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v3, v0, LX/056;->v:LX/05G;

    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->a:LX/07k;

    iget-object v0, v0, LX/07k;->f:LX/05B;

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/06y;

    invoke-interface {v3, v0}, LX/05G;->a(LX/06y;)Z

    move-result v0

    or-int/2addr v1, v0

    .line 20245
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->x:LX/05G;

    if-eqz v0, :cond_2

    .line 20246
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v3, v0, LX/056;->x:LX/05G;

    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->a:LX/07k;

    iget-object v0, v0, LX/07k;->f:LX/05B;

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/06y;

    invoke-interface {v3, v0}, LX/05G;->a(LX/06y;)Z

    move-result v0

    or-int/2addr v1, v0

    .line 20247
    :cond_2
    if-eqz v1, :cond_3

    .line 20248
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->D:LX/055;

    .line 20249
    iget-object v1, v0, LX/055;->a:LX/051;

    invoke-virtual {v1}, LX/051;->o()V

    .line 20250
    :cond_3
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-boolean v0, v0, LX/077;->c:Z

    if-nez v0, :cond_4

    .line 20251
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    invoke-virtual {v0, v8, v8}, LX/056;->a(Ljava/util/List;Ljava/util/List;)V

    .line 20252
    :cond_4
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    invoke-virtual {v0}, LX/056;->f()V

    .line 20253
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->j:LX/05i;

    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->W:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, LX/05i;->c(J)V

    .line 20254
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-boolean v0, v0, LX/056;->S:Z

    if-eqz v0, :cond_5

    .line 20255
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->j:LX/05i;

    invoke-virtual {v1}, LX/05i;->a()LX/0Ha;

    move-result-object v1

    invoke-static {v0, v1, v2}, LX/056;->a$redex0(LX/056;LX/0Ha;Z)V

    .line 20256
    :cond_5
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->j:LX/05i;

    const-class v1, LX/06p;

    invoke-virtual {v0, v1}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/06p;

    sget-object v1, LX/06w;->LastConnectFailureReason:LX/06w;

    invoke-virtual {v0, v1, v8}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 20257
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-boolean v0, v0, LX/077;->c:Z

    if-nez v0, :cond_6

    .line 20258
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->D:LX/055;

    invoke-virtual {v0}, LX/055;->d()V

    .line 20259
    :cond_6
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    iget-object v3, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v3, v3, LX/077;->a:LX/056;

    iget-object v3, v3, LX/056;->t:LX/06I;

    .line 20260
    iget-wide v9, v3, LX/06I;->k:J

    move-wide v4, v9

    .line 20261
    sub-long/2addr v0, v4

    .line 20262
    iget-object v3, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v3, v3, LX/077;->a:LX/056;

    iget-object v3, v3, LX/056;->d:LX/05h;

    iget-object v4, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v4, v4, LX/077;->a:LX/056;

    iget-object v4, v4, LX/056;->t:LX/06I;

    .line 20263
    iget v5, v4, LX/06I;->j:I

    move v4, v5

    .line 20264
    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "retry_count"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "retry_duration_ms"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v5}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 20265
    const-string v6, "mqtt_connection_retries"

    invoke-virtual {v3, v6, v5}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 20266
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    .line 20267
    iput-boolean v2, v0, LX/056;->B:Z

    .line 20268
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v1, v1, LX/077;->b:LX/072;

    invoke-virtual {v1}, LX/072;->j()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/056;->f(Ljava/util/List;)V

    .line 20269
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/056;->e(Ljava/util/List;)V

    .line 20270
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    iput-wide v4, v0, LX/056;->o:J

    .line 20271
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$2;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iput-boolean v2, v0, LX/056;->i:Z

    .line 20272
    :cond_7
    return-void

    :cond_8
    move v1, v2

    goto/16 :goto_0
.end method
