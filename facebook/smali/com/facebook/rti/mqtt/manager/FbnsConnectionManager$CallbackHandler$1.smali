.class public final Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/077;


# direct methods
.method public constructor <init>(LX/077;)V
    .locals 0

    .prologue
    .line 38008
    iput-object p1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$1;->a:LX/077;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 38009
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$1;->a:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->D:LX/055;

    .line 38010
    iget-object v1, v0, LX/055;->a:LX/051;

    .line 38011
    const-string v2, "MqttPushService"

    const-string v3, "connection/connect_sent"

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v0}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38012
    sget-object v2, LX/053;->CONNECT_SENT:LX/053;

    invoke-virtual {v1, v2}, LX/051;->a(LX/053;)Z

    .line 38013
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$1;->a:LX/077;

    iget-boolean v0, v0, LX/077;->c:Z

    if-eqz v0, :cond_0

    .line 38014
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$1;->a:LX/077;

    invoke-static {v0}, LX/077;->d(LX/077;)V

    .line 38015
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$1;->a:LX/077;

    const/4 p0, 0x0

    .line 38016
    iget-object v1, v0, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->b:LX/072;

    iget-object v2, v0, LX/077;->b:LX/072;

    if-ne v1, v2, :cond_0

    .line 38017
    const-string v1, "FbnsConnectionManager"

    const-string v2, "FastSend enabled"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38018
    iget-object v1, v0, LX/077;->a:LX/056;

    invoke-virtual {v1, p0, p0}, LX/056;->a(Ljava/util/List;Ljava/util/List;)V

    .line 38019
    iget-object v1, v0, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->D:LX/055;

    invoke-virtual {v1}, LX/055;->d()V

    .line 38020
    iget-object v1, v0, LX/077;->a:LX/056;

    iget-object v2, v0, LX/077;->b:LX/072;

    invoke-virtual {v2}, LX/072;->k()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/056;->e(Ljava/util/List;)V

    .line 38021
    :cond_0
    return-void
.end method
