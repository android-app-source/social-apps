.class public final Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/07k;

.field public final synthetic b:LX/077;


# direct methods
.method public constructor <init>(LX/077;LX/07k;)V
    .locals 0

    .prologue
    .line 38036
    iput-object p1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->b:LX/077;

    iput-object p2, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->a:LX/07k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 38022
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->a:LX/07k;

    iget-object v0, v0, LX/07k;->b:LX/05B;

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Hs;

    .line 38023
    const-string v1, "FbnsConnectionManager"

    const-string v2, "connection/failed; reason=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38024
    sget-object v1, LX/0Hs;->FAILED_CONNECTION_REFUSED_SERVER_SHEDDING_LOAD:LX/0Hs;

    invoke-virtual {v0, v1}, LX/0Hs;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/0Hs;->FAILED_CONNECTION_REFUSED_NOT_AUTHORIZED:LX/0Hs;

    invoke-virtual {v0, v1}, LX/0Hs;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 38025
    :cond_0
    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->t:LX/06I;

    invoke-virtual {v1}, LX/06I;->h()V

    .line 38026
    :cond_1
    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->b:LX/072;

    iget-object v2, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->b:LX/077;

    iget-object v2, v2, LX/077;->b:LX/072;

    if-ne v1, v2, :cond_4

    .line 38027
    sget-object v1, LX/0Hs;->FAILED_CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD:LX/0Hs;

    invoke-virtual {v0, v1}, LX/0Hs;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 38028
    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->u:LX/05F;

    invoke-interface {v1}, LX/05F;->c()V

    .line 38029
    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->w:LX/05F;

    if-eqz v1, :cond_2

    .line 38030
    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->w:LX/05F;

    invoke-interface {v1}, LX/05F;->c()V

    .line 38031
    :cond_2
    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    sget-object v2, LX/0BA;->CONNECT_FAILED:LX/0BA;

    invoke-static {v0}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/056;->a$redex0(LX/056;LX/0BA;LX/05B;)V

    .line 38032
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->j:LX/05i;

    const-class v2, LX/06p;

    invoke-virtual {v1, v2}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v1

    check-cast v1, LX/06p;

    sget-object v2, LX/06w;->LastConnectFailureReason:LX/06w;

    invoke-virtual {v0}, LX/0Hs;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/06q;->a(LX/06x;Ljava/lang/Object;)V

    .line 38033
    return-void

    .line 38034
    :cond_4
    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    iget-object v1, v1, LX/056;->m:LX/072;

    iget-object v2, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->b:LX/077;

    iget-object v2, v2, LX/077;->b:LX/072;

    if-ne v1, v2, :cond_3

    .line 38035
    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$3;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    invoke-static {v1}, LX/056;->x(LX/056;)V

    goto :goto_0
.end method
