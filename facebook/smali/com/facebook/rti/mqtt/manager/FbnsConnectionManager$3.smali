.class public final Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/056;


# direct methods
.method public constructor <init>(LX/056;)V
    .locals 0

    .prologue
    .line 15721
    iput-object p1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$3;->a:LX/056;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 15722
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$3;->a:LX/056;

    iget-object v0, v0, LX/056;->D:LX/055;

    invoke-virtual {v0}, LX/055;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 15723
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$3;->a:LX/056;

    invoke-virtual {v0}, LX/056;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15724
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$3;->a:LX/056;

    invoke-virtual {v0}, LX/056;->d()V

    .line 15725
    :cond_0
    :goto_0
    return-void

    .line 15726
    :cond_1
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$3;->a:LX/056;

    invoke-virtual {v0}, LX/056;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 15727
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$3;->a:LX/056;

    invoke-virtual {v0}, LX/056;->j()V

    .line 15728
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$3;->a:LX/056;

    iget-object v0, v0, LX/056;->t:LX/06I;

    invoke-virtual {v0}, LX/06I;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15729
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$3;->a:LX/056;

    iget-object v0, v0, LX/056;->j:LX/05i;

    sget-object v1, LX/06f;->KEEPALIVE:LX/06f;

    .line 15730
    iput-object v1, v0, LX/05i;->n:LX/06f;

    .line 15731
    goto :goto_0

    .line 15732
    :cond_2
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$3;->a:LX/056;

    sget-object v1, LX/0AX;->KEEPALIVE_SHOULD_NOT_CONNECT:LX/0AX;

    invoke-virtual {v0, v1}, LX/056;->a(LX/0AX;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method
