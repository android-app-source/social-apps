.class public final Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/07W;

.field public final synthetic b:LX/077;


# direct methods
.method public constructor <init>(LX/077;LX/07W;)V
    .locals 0

    .prologue
    .line 26163
    iput-object p1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->b:LX/077;

    iput-object p2, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->a:LX/07W;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 26164
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->b:LX/072;

    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->b:LX/077;

    iget-object v1, v1, LX/077;->b:LX/072;

    if-ne v0, v1, :cond_2

    .line 26165
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v1, v0

    .line 26166
    sget-object v0, LX/0B9;->b:[I

    iget-object v2, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->a:LX/07W;

    invoke-virtual {v2}, LX/07W;->e()LX/07S;

    move-result-object v2

    invoke-virtual {v2}, LX/07S;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 26167
    :cond_0
    :goto_0
    invoke-virtual {v1}, LX/05B;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 26168
    const-string v0, "FbnsConnectionManager"

    const-string v1, "receive/%s"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->a:LX/07W;

    invoke-virtual {v3}, LX/07W;->e()LX/07S;

    move-result-object v3

    invoke-virtual {v3}, LX/07S;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26169
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->D:LX/055;

    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->a:LX/07W;

    .line 26170
    iget-object v2, v0, LX/055;->a:LX/051;

    invoke-virtual {v2, v1}, LX/051;->a(LX/07W;)V

    .line 26171
    :cond_2
    return-void

    .line 26172
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->a:LX/07W;

    invoke-virtual {v0}, LX/07W;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0B6;

    iget v0, v0, LX/0B6;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v0

    move-object v1, v0

    .line 26173
    goto :goto_0

    .line 26174
    :pswitch_1
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v0

    move-object v1, v0

    .line 26175
    goto :goto_0

    .line 26176
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    invoke-virtual {v0}, LX/056;->f()V

    .line 26177
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    .line 26178
    iget-boolean v7, v0, LX/056;->S:Z

    if-eqz v7, :cond_3

    .line 26179
    iget-object v7, v0, LX/056;->b:LX/072;

    .line 26180
    invoke-static {v7}, LX/056;->d(LX/072;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 26181
    iget-wide v11, v7, LX/072;->C:J

    move-wide v7, v11

    .line 26182
    iget-object v9, v0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v9}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v9

    .line 26183
    sub-long v7, v9, v7

    .line 26184
    iget-object v9, v0, LX/056;->j:LX/05i;

    invoke-virtual {v9, v7, v8}, LX/05i;->b(J)LX/0Ha;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v0, v7, v8}, LX/056;->a$redex0(LX/056;LX/0Ha;Z)V

    .line 26185
    :cond_3
    goto :goto_0

    .line 26186
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->a:LX/07W;

    check-cast v0, LX/07X;

    .line 26187
    iget-object v2, v0, LX/07W;->a:LX/07R;

    move-object v0, v2

    .line 26188
    iget v0, v0, LX/07R;->c:I

    .line 26189
    sget-object v2, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    iget v2, v2, LX/0AL;->mValue:I

    if-ne v0, v2, :cond_0

    .line 26190
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    invoke-virtual {v0}, LX/056;->f()V

    goto/16 :goto_0

    .line 26191
    :cond_4
    const-string v0, "FbnsConnectionManager"

    const-string v2, "receive/%s; id=%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->a:LX/07W;

    invoke-virtual {v4}, LX/07W;->e()LX/07S;

    move-result-object v4

    invoke-virtual {v4}, LX/07S;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v0, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26192
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v2, v0, LX/056;->k:LX/06J;

    invoke-virtual {v1}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, LX/06J;->a(I)LX/0B1;

    .line 26193
    invoke-virtual {v1}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->b:LX/077;

    iget-object v1, v1, LX/077;->a:LX/056;

    iget v1, v1, LX/056;->n:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->m:LX/072;

    if-eqz v0, :cond_1

    .line 26194
    const-string v0, "FbnsConnectionManager"

    const-string v1, "Attempting to stop preemptive reconnect %d"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->b:LX/077;

    iget-object v3, v3, LX/077;->a:LX/056;

    iget v3, v3, LX/056;->n:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26195
    iget-object v0, p0, Lcom/facebook/rti/mqtt/manager/FbnsConnectionManager$CallbackHandler$6;->b:LX/077;

    iget-object v0, v0, LX/077;->a:LX/056;

    iget-object v0, v0, LX/056;->m:LX/072;

    .line 26196
    iget-object v1, v0, LX/072;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/rti/mqtt/protocol/MqttClient$3;

    invoke-direct {v2, v0}, Lcom/facebook/rti/mqtt/protocol/MqttClient$3;-><init>(LX/072;)V

    const v3, 0x7393ceaa

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 26197
    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
