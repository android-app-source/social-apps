.class public Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;
.super LX/05s;
.source ""

# interfaces
.implements LX/05u;
.implements Ljava/lang/Runnable;
.implements Ljava/util/concurrent/ScheduledFuture;


# annotations
.annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "LX/05s",
        "<TV;>;",
        "LX/05u",
        "<TV;>;",
        "Ljava/lang/Runnable;",
        "Ljava/util/concurrent/ScheduledFuture",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/05q;

.field public final b:LX/0B3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0B3",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/05q;Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 16985
    iput-object p1, p0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;->a:LX/05q;

    invoke-direct {p0}, LX/05s;-><init>()V

    .line 16986
    invoke-static {p2, p3}, LX/0B3;->a(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0B3;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;->b:LX/0B3;

    .line 16987
    return-void
.end method

.method public constructor <init>(LX/05q;Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 16982
    iput-object p1, p0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;->a:LX/05q;

    invoke-direct {p0}, LX/05s;-><init>()V

    .line 16983
    invoke-static {p2}, LX/0B3;->a(Ljava/util/concurrent/Callable;)LX/0B3;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;->b:LX/0B3;

    .line 16984
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 16980
    iget-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;->b:LX/0B3;

    move-object v0, v0

    .line 16981
    return-object v0
.end method

.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 16977
    iget-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;->b:LX/0B3;

    move-object v0, v0

    .line 16978
    invoke-virtual {v0, p1, p2}, LX/0B3;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 16979
    return-void
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16967
    iget-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;->b:LX/0B3;

    move-object v0, v0

    .line 16968
    return-object v0
.end method

.method public final cancel(Z)Z
    .locals 1

    .prologue
    .line 16974
    iget-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;->a:LX/05q;

    invoke-static {v0, p0}, LX/05q;->a$redex0(LX/05q;Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;)V

    .line 16975
    iget-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;->b:LX/0B3;

    move-object v0, v0

    .line 16976
    invoke-virtual {v0, p1}, LX/0B3;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 16973
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final getDelay(Ljava/util/concurrent/TimeUnit;)J
    .locals 1

    .prologue
    .line 16972
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 16969
    iget-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/WakingExecutorService$ListenableScheduledFuture;->b:LX/0B3;

    move-object v0, v0

    .line 16970
    invoke-virtual {v0}, LX/0B3;->run()V

    .line 16971
    return-void
.end method
