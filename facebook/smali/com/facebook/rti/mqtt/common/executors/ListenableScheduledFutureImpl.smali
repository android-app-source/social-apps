.class public Lcom/facebook/rti/mqtt/common/executors/ListenableScheduledFutureImpl;
.super LX/0Hg;
.source ""

# interfaces
.implements LX/0Hh;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Hg",
        "<TV;>;",
        "LX/0Hh",
        "<TV;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field public final a:LX/0B3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0B3",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Ljava/lang/Runnable;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 37975
    invoke-direct {p0, p1}, LX/0Hg;-><init>(Landroid/os/Handler;)V

    .line 37976
    invoke-static {p2, p3}, LX/0B3;->a(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0B3;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/ListenableScheduledFutureImpl;->a:LX/0B3;

    .line 37977
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 37978
    invoke-direct {p0, p1}, LX/0Hg;-><init>(Landroid/os/Handler;)V

    .line 37979
    invoke-static {p2}, LX/0B3;->a(Ljava/util/concurrent/Callable;)LX/0B3;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/ListenableScheduledFutureImpl;->a:LX/0B3;

    .line 37980
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 37970
    iget-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/ListenableScheduledFutureImpl;->a:LX/0B3;

    move-object v0, v0

    .line 37971
    return-object v0
.end method

.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 37972
    iget-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/ListenableScheduledFutureImpl;->a:LX/0B3;

    move-object v0, v0

    .line 37973
    invoke-virtual {v0, p1, p2}, LX/0B3;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 37974
    return-void
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37963
    iget-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/ListenableScheduledFutureImpl;->a:LX/0B3;

    move-object v0, v0

    .line 37964
    return-object v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 37969
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final getDelay(Ljava/util/concurrent/TimeUnit;)J
    .locals 1

    .prologue
    .line 37965
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 37966
    iget-object v0, p0, Lcom/facebook/rti/mqtt/common/executors/ListenableScheduledFutureImpl;->a:LX/0B3;

    move-object v0, v0

    .line 37967
    invoke-virtual {v0}, LX/0B3;->run()V

    .line 37968
    return-void
.end method
