.class public final Lcom/facebook/rti/mqtt/protocol/MqttClient$10;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/072;


# direct methods
.method public constructor <init>(LX/072;)V
    .locals 0

    .prologue
    .line 38174
    iput-object p1, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$10;->a:LX/072;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 38175
    :try_start_0
    iget-object v0, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$10;->a:LX/072;

    iget-boolean v0, v0, LX/072;->t:Z

    if-nez v0, :cond_0

    .line 38176
    iget-object v0, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$10;->a:LX/072;

    iget-object v0, v0, LX/072;->m:LX/071;

    invoke-interface {v0}, LX/071;->d()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 38177
    :cond_0
    :goto_0
    return-void

    .line 38178
    :catch_0
    move-exception v0

    .line 38179
    const-string v1, "MqttClient"

    const-string v2, "exception/ping"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38180
    iget-object v1, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$10;->a:LX/072;

    invoke-static {v0}, LX/0AX;->getFromWriteException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v2

    sget-object v3, LX/0BJ;->PINGRESP:LX/0BJ;

    invoke-static {v1, v2, v3, v0}, LX/072;->b(LX/072;LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
