.class public final Lcom/facebook/rti/mqtt/protocol/MqttClient$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:I

.field public final synthetic c:LX/072;


# direct methods
.method public constructor <init>(LX/072;Ljava/util/List;I)V
    .locals 0

    .prologue
    .line 38196
    iput-object p1, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$5;->c:LX/072;

    iput-object p2, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$5;->a:Ljava/util/List;

    iput p3, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$5;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 38197
    iget-object v0, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$5;->c:LX/072;

    iget-object v1, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$5;->a:Ljava/util/List;

    iget v2, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$5;->b:I

    .line 38198
    :try_start_0
    invoke-static {v0}, LX/072;->p(LX/072;)V

    .line 38199
    invoke-virtual {v0}, LX/072;->d()Z

    move-result v3

    if-nez v3, :cond_1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 38200
    :cond_0
    :goto_0
    return-void

    .line 38201
    :cond_1
    :try_start_1
    iget-object v3, v0, LX/072;->m:LX/071;

    invoke-interface {v3, v1, v2}, LX/071;->b(Ljava/util/List;I)V

    .line 38202
    iget-object v3, v0, LX/072;->E:LX/077;

    .line 38203
    if-eqz v3, :cond_0

    .line 38204
    sget-object v4, LX/07S;->UNSUBSCRIBE:LX/07S;

    invoke-virtual {v4}, LX/07S;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, LX/077;->a(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 38205
    :catch_0
    move-exception v3

    .line 38206
    const-string v4, "MqttClient"

    const-string v5, "exception/unsubscribe"

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {v4, v3, v5, p0}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38207
    invoke-static {v3}, LX/0AX;->getFromWriteException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v4

    sget-object v5, LX/0BJ;->UNSUBSCRIBE:LX/0BJ;

    invoke-static {v0, v4, v5, v3}, LX/072;->b(LX/072;LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
