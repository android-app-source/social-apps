.class public final Lcom/facebook/rti/mqtt/protocol/MqttClient$9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Ljava/lang/Object;

.field public final synthetic c:LX/072;


# direct methods
.method public constructor <init>(LX/072;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 26290
    iput-object p1, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$9;->c:LX/072;

    iput p2, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$9;->a:I

    iput-object p3, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$9;->b:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 26291
    :try_start_0
    iget-object v0, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$9;->c:LX/072;

    iget-object v0, v0, LX/072;->m:LX/071;

    iget v1, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$9;->a:I

    iget-object v2, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$9;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, LX/071;->a(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 26292
    :goto_0
    return-void

    .line 26293
    :catch_0
    move-exception v0

    .line 26294
    iget-object v1, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$9;->c:LX/072;

    iget-object v1, v1, LX/072;->J:LX/076;

    const-string v2, "Mqtt Uncaught Exception"

    const-string v3, "sendPubAck"

    invoke-virtual {v1, v0, v2, v3}, LX/076;->a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    .line 26295
    iget-object v1, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$9;->c:LX/072;

    invoke-static {v0}, LX/0AX;->getFromWriteException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v2

    sget-object v3, LX/0BJ;->PUBACK:LX/0BJ;

    invoke-static {v1, v2, v3, v0}, LX/072;->b(LX/072;LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
