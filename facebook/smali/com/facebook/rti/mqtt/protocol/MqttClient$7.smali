.class public final Lcom/facebook/rti/mqtt/protocol/MqttClient$7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/072;


# direct methods
.method public constructor <init>(LX/072;)V
    .locals 0

    .prologue
    .line 38208
    iput-object p1, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$7;->a:LX/072;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 38209
    iget-object v0, p0, Lcom/facebook/rti/mqtt/protocol/MqttClient$7;->a:LX/072;

    const/4 v4, 0x0

    .line 38210
    :try_start_0
    invoke-virtual {v0}, LX/072;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 38211
    const-string v1, "MqttClient"

    const-string v2, "send/ping/not_connected"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 38212
    :goto_0
    return-void

    .line 38213
    :cond_0
    :try_start_1
    iget-object v1, v0, LX/072;->m:LX/071;

    invoke-interface {v1}, LX/071;->c()V

    .line 38214
    iget-object v1, v0, LX/072;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v1

    iput-wide v1, v0, LX/072;->v:J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 38215
    :catch_0
    move-exception v1

    .line 38216
    const-string v2, "MqttClient"

    const-string v3, "exception/ping"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38217
    invoke-static {v1}, LX/0AX;->getFromWriteException(Ljava/lang/Throwable;)LX/0AX;

    move-result-object v2

    sget-object v3, LX/0BJ;->PING:LX/0BJ;

    invoke-static {v0, v2, v3, v1}, LX/072;->b(LX/072;LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
