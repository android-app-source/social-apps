.class public final Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:Z

.field public final synthetic d:LX/079;

.field public final synthetic e:I

.field public final synthetic f:Z

.field public final synthetic g:LX/070;


# direct methods
.method public constructor <init>(LX/070;Ljava/lang/String;IZLX/079;IZ)V
    .locals 0

    .prologue
    .line 19311
    iput-object p1, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->g:LX/070;

    iput-object p2, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->a:Ljava/lang/String;

    iput p3, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->b:I

    iput-boolean p4, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->c:Z

    iput-object p5, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->d:LX/079;

    iput p6, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->e:I

    iput-boolean p7, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->f:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 19312
    :try_start_0
    iget-object v0, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->g:LX/070;

    iget-object v1, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->a:Ljava/lang/String;

    iget v2, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->b:I

    iget-boolean v3, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->c:Z

    iget-object v4, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->d:LX/079;

    iget v5, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->e:I

    iget-boolean v6, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->f:Z

    .line 19313
    invoke-static/range {v0 .. v6}, LX/070;->b(LX/070;Ljava/lang/String;IZLX/079;IZ)LX/07k;

    move-result-object v7

    move-object v0, v7

    .line 19314
    iget-boolean v1, v0, LX/07k;->a:Z

    if-nez v1, :cond_0

    .line 19315
    iget-object v1, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->g:LX/070;

    invoke-virtual {v1}, LX/070;->a()V

    .line 19316
    :cond_0
    iget-object v1, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->g:LX/070;

    iget-object v1, v1, LX/070;->p:LX/076;

    invoke-virtual {v1, v0}, LX/076;->a(LX/07k;)V

    .line 19317
    iget-object v0, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->g:LX/070;

    invoke-static {v0}, LX/070;->h(LX/070;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 19318
    :goto_0
    return-void

    .line 19319
    :catch_0
    move-exception v0

    .line 19320
    const-string v1, "DefaultMqttClientCore"

    const-string v2, "exception/networkThreadLoop"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19321
    iget-object v1, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->g:LX/070;

    iget-object v1, v1, LX/070;->p:LX/076;

    .line 19322
    iget-object v2, v1, LX/076;->a:LX/072;

    iget-object v2, v2, LX/072;->E:LX/077;

    .line 19323
    if-eqz v2, :cond_1

    .line 19324
    :cond_1
    iget-object v1, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->g:LX/070;

    iget-object v1, v1, LX/070;->p:LX/076;

    sget-object v2, LX/0AX;->UNKNOWN_RUNTIME:LX/0AX;

    sget-object v3, LX/0BJ;->NETWORK_THREAD_LOOP:LX/0BJ;

    invoke-virtual {v1, v2, v3, v0}, LX/076;->a(LX/0AX;LX/0BJ;Ljava/lang/Throwable;)V

    .line 19325
    iget-object v1, p0, Lcom/facebook/rti/mqtt/protocol/DefaultMqttClientCore$1;->g:LX/070;

    iget-object v1, v1, LX/070;->p:LX/076;

    const-string v2, "Mqtt Unhandled Exception"

    const-string v3, "Unhandled exception in Mqtt networkThreadLoop"

    invoke-virtual {v1, v0, v2, v3}, LX/076;->a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
