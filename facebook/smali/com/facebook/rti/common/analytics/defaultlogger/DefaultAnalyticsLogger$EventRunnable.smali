.class public final Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0HP;

.field private b:LX/06d;


# direct methods
.method public constructor <init>(LX/0HP;LX/06d;)V
    .locals 0

    .prologue
    .line 37650
    iput-object p1, p0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;->a:LX/0HP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37651
    iput-object p2, p0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;->b:LX/06d;

    .line 37652
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 37653
    iget-object v0, p0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;->a:LX/0HP;

    iget-object v1, p0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;->b:LX/06d;

    .line 37654
    iget-object v2, v0, LX/0HP;->p:Landroid/content/SharedPreferences;

    const-string v3, "user_id"

    const-string v5, ""

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0HP;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 37655
    const-string v3, "pk"

    invoke-static {v1, v3, v2}, LX/06d;->a(LX/06d;Ljava/lang/String;Ljava/lang/String;)LX/06d;

    move-result-object v3

    .line 37656
    iget-object v0, p0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;->a:LX/0HP;

    iget-object v0, v0, LX/0HP;->h:LX/0HK;

    iget-object v1, p0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;->b:LX/06d;

    .line 37657
    iget-object v2, v0, LX/0HK;->j:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37658
    iget-object v0, p0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;->a:LX/0HP;

    iget-object v0, v0, LX/0HP;->i:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 37659
    iget-object v0, p0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;->a:LX/0HP;

    iget-object v0, v0, LX/0HP;->h:LX/0HK;

    .line 37660
    iget-object v1, v0, LX/0HK;->j:Ljava/util/List;

    move-object v0, v1

    .line 37661
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_0

    .line 37662
    iget-object v0, p0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;->a:LX/0HP;

    invoke-static {v0}, LX/0HP;->e$redex0(LX/0HP;)V

    .line 37663
    :goto_0
    return-void

    .line 37664
    :cond_0
    iget-object v0, p0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$EventRunnable;->a:LX/0HP;

    iget-object v0, v0, LX/0HP;->i:Landroid/os/Handler;

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
