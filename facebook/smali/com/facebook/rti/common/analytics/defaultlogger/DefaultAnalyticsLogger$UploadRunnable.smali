.class public final Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$UploadRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0HP;


# direct methods
.method public constructor <init>(LX/0HP;)V
    .locals 0

    .prologue
    .line 37668
    iput-object p1, p0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$UploadRunnable;->a:LX/0HP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 37669
    iget-object v0, p0, Lcom/facebook/rti/common/analytics/defaultlogger/DefaultAnalyticsLogger$UploadRunnable;->a:LX/0HP;

    iget-object v0, v0, LX/0HP;->n:LX/0HM;

    const/4 v2, 0x0

    .line 37670
    sget-object v3, LX/0HM;->a:Ljava/lang/String;

    const-string v4, "Attempting to upload analytics"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37671
    iget-object v3, v0, LX/0HM;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 37672
    sget-object v3, LX/0HM;->a:Ljava/lang/String;

    const-string v4, "No analytics directory exists, nothing to do"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37673
    :cond_0
    :goto_0
    return-void

    .line 37674
    :cond_1
    iget-object v3, v0, LX/0HM;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 37675
    if-nez v4, :cond_4

    .line 37676
    sget-object v3, LX/0HM;->a:Ljava/lang/String;

    const-string v4, "Analytics directory error"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37677
    iget-object v3, v0, LX/0HM;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 37678
    sget-object v3, LX/0HM;->a:Ljava/lang/String;

    const-string v4, "directory_not_found"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 37679
    :cond_2
    iget-object v3, v0, LX/0HM;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 37680
    sget-object v3, LX/0HM;->a:Ljava/lang/String;

    const-string v4, "directory_is_file"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 37681
    :cond_3
    sget-object v3, LX/0HM;->a:Ljava/lang/String;

    const-string v4, "directory_unknown_error"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 37682
    :cond_4
    array-length v5, v4

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_0

    aget-object p0, v4, v3

    .line 37683
    invoke-static {v0, p0}, LX/0HM;->a(LX/0HM;Ljava/io/File;)Z

    move-result p0

    .line 37684
    if-nez p0, :cond_5

    .line 37685
    goto :goto_0

    .line 37686
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method
