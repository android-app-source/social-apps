.class public final Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/06Z;

.field private final b:Ljava/lang/Runnable;

.field private final c:J

.field private volatile d:J

.field private volatile e:J


# direct methods
.method public constructor <init>(LX/06Z;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 26029
    iput-object p1, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->a:LX/06Z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26030
    iput-object p2, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->b:Ljava/lang/Runnable;

    .line 26031
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->c:J

    .line 26032
    iput-wide v2, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->d:J

    .line 26033
    iput-wide v2, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->e:J

    .line 26034
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 26035
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->d:J

    .line 26036
    iget-object v0, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->a:LX/06Z;

    iget v0, v0, LX/06Z;->e:I

    if-eq v0, v7, :cond_0

    iget-wide v0, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->d:J

    iget-wide v2, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->c:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->a:LX/06Z;

    iget v2, v2, LX/06Z;->e:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 26037
    const-string v0, "SerialExecutor"

    const-string v1, "dispatch time exceeded limit: %s"

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->a:LX/06Z;

    iget-object v3, v3, LX/06Z;->a:Ljava/lang/String;

    aput-object v3, v2, v8

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26038
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    .line 26039
    iget-object v2, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->b:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 26040
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    .line 26041
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 26042
    iget-object v6, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->a:LX/06Z;

    iget v6, v6, LX/06Z;->c:I

    if-eq v6, v7, :cond_1

    sub-long v0, v2, v0

    iget-object v2, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->a:LX/06Z;

    iget v2, v2, LX/06Z;->c:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 26043
    const-string v0, "SerialExecutor"

    const-string v1, "compute time exceeded limit: %s"

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->a:LX/06Z;

    iget-object v3, v3, LX/06Z;->a:Ljava/lang/String;

    aput-object v3, v2, v8

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26044
    :cond_1
    iget-object v0, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->a:LX/06Z;

    iget v0, v0, LX/06Z;->d:I

    if-eq v0, v7, :cond_2

    iget-wide v0, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->d:J

    sub-long v0, v4, v0

    iget-object v2, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->a:LX/06Z;

    iget v2, v2, LX/06Z;->d:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 26045
    const-string v0, "SerialExecutor"

    const-string v1, "wall clock runtime exceeded limit: %s"

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->a:LX/06Z;

    iget-object v3, v3, LX/06Z;->a:Ljava/lang/String;

    aput-object v3, v2, v8

    invoke-static {v0, v1, v2}, LX/05D;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26046
    :cond_2
    iget-object v0, p0, Lcom/facebook/rti/common/concurrent/SerialExecutor$RunnableWrapper;->a:LX/06Z;

    invoke-static {v0}, LX/06Z;->a(LX/06Z;)V

    .line 26047
    return-void
.end method
