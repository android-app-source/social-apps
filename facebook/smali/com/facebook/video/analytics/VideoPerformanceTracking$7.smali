.class public final Lcom/facebook/video/analytics/VideoPerformanceTracking$7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Z

.field public final synthetic c:LX/0A1;


# direct methods
.method public constructor <init>(LX/0A1;JZ)V
    .locals 0

    .prologue
    .line 24100
    iput-object p1, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->c:LX/0A1;

    iput-wide p2, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->a:J

    iput-boolean p4, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 24101
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->c:LX/0A1;

    iget-wide v2, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->a:J

    invoke-static {v0, v2, v3}, LX/0A1;->a(LX/0A1;J)J

    .line 24102
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->c:LX/0A1;

    iget-wide v2, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->a:J

    .line 24103
    iget-wide v4, v0, LX/0A1;->j:J

    add-long/2addr v4, v2

    iput-wide v4, v0, LX/0A1;->j:J

    .line 24104
    iget-boolean v0, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->b:Z

    if-eqz v0, :cond_0

    .line 24105
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->c:LX/0A1;

    iget-wide v2, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->a:J

    .line 24106
    iget-wide v4, v0, LX/0A1;->k:J

    add-long/2addr v4, v2

    iput-wide v4, v0, LX/0A1;->k:J

    .line 24107
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->c:LX/0A1;

    invoke-static {v0}, LX/0A1;->e(LX/0A1;)V

    .line 24108
    return-void

    .line 24109
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->c:LX/0A1;

    iget-wide v2, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->a:J

    invoke-static {v0, v2, v3}, LX/0A1;->b(LX/0A1;J)J

    .line 24110
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->c:LX/0A1;

    iget-wide v2, p0, Lcom/facebook/video/analytics/VideoPerformanceTracking$7;->a:J

    .line 24111
    iget-wide v4, v0, LX/0A1;->l:J

    add-long/2addr v4, v2

    iput-wide v4, v0, LX/0A1;->l:J

    .line 24112
    goto :goto_0
.end method
