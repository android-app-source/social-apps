.class public Lcom/facebook/video/analytics/VideoFeedStoryInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/analytics/VideoFeedStoryInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/162;

.field public b:LX/04g;

.field public c:LX/04H;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24022
    new-instance v0, LX/0Ao;

    invoke-direct {v0}, LX/0Ao;-><init>()V

    sput-object v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24024
    sget-object v0, LX/04g;->UNSET:LX/04g;

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    .line 24025
    sget-object v0, LX/04H;->NO_INFO:LX/04H;

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    .line 24026
    return-void
.end method

.method public constructor <init>(LX/0AW;)V
    .locals 1

    .prologue
    .line 24027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24028
    sget-object v0, LX/04g;->UNSET:LX/04g;

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    .line 24029
    sget-object v0, LX/04H;->NO_INFO:LX/04H;

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    .line 24030
    iget-object v0, p1, LX/0AW;->a:LX/162;

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a:LX/162;

    .line 24031
    iget-object v0, p1, LX/0AW;->b:LX/04g;

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    .line 24032
    iget-object v0, p1, LX/0AW;->c:LX/04H;

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    .line 24033
    iget-boolean v0, p1, LX/0AW;->d:Z

    iput-boolean v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->d:Z

    .line 24034
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 24005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24006
    sget-object v0, LX/04g;->UNSET:LX/04g;

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    .line 24007
    sget-object v0, LX/04H;->NO_INFO:LX/04H;

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    .line 24008
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    .line 24009
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 24010
    invoke-virtual {v0, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 24011
    check-cast v0, LX/162;

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a:LX/162;

    .line 24012
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/04g;->asEventTriggerType(Ljava/lang/String;)LX/04g;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    .line 24013
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/04H;->valueOf(Ljava/lang/String;)LX/04H;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    .line 24014
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->d:Z

    .line 24015
    return-void

    .line 24016
    :catch_0
    move-exception v0

    .line 24017
    new-instance v1, Landroid/os/ParcelFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not parse parcel "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/28F;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/ParcelFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 24018
    :catch_1
    move-exception v0

    .line 24019
    new-instance v1, Landroid/os/ParcelFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not parse parcel "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/ParcelFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 24020
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/162;
    .locals 1

    .prologue
    .line 24021
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a:LX/162;

    return-object v0
.end method

.method public final b()LX/04g;
    .locals 1

    .prologue
    .line 23995
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    return-object v0
.end method

.method public final c()LX/04H;
    .locals 1

    .prologue
    .line 23996
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 23997
    iget-boolean v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->d:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 24004
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 23998
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a:LX/162;

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 23999
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    iget-object v0, v0, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24000
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    iget-object v0, v0, LX/04H;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24001
    iget-boolean v0, p0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 24002
    return-void

    .line 24003
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
