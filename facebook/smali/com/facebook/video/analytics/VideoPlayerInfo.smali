.class public Lcom/facebook/video/analytics/VideoPlayerInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/analytics/VideoPlayerInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/04G;

.field public b:LX/04D;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25009
    new-instance v0, LX/0Ar;

    invoke-direct {v0}, LX/0Ar;-><init>()V

    sput-object v0, Lcom/facebook/video/analytics/VideoPlayerInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/04G;)V
    .locals 1

    .prologue
    .line 25010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25011
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    .line 25012
    iput-object p1, p0, Lcom/facebook/video/analytics/VideoPlayerInfo;->a:LX/04G;

    .line 25013
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 25014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25015
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    .line 25016
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/04G;->asPlayerType(Ljava/lang/String;)LX/04G;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoPlayerInfo;->a:LX/04G;

    .line 25017
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/04D;->asPlayerOrigin(Ljava/lang/String;)LX/04D;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    .line 25018
    return-void
.end method


# virtual methods
.method public final a()LX/04D;
    .locals 1

    .prologue
    .line 25019
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    return-object v0
.end method

.method public final b()LX/04G;
    .locals 1

    .prologue
    .line 25020
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoPlayerInfo;->a:LX/04G;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 25021
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 25022
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoPlayerInfo;->a:LX/04G;

    iget-object v0, v0, LX/04G;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 25023
    iget-object v0, p0, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    invoke-virtual {v0}, LX/04D;->asString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 25024
    return-void
.end method
