.class public final Lcom/facebook/video/analytics/TimedMicroStorage$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0A8;

.field public final synthetic b:LX/0A2;


# direct methods
.method public constructor <init>(LX/0A2;LX/0A8;)V
    .locals 0

    .prologue
    .line 24081
    iput-object p1, p0, Lcom/facebook/video/analytics/TimedMicroStorage$1;->b:LX/0A2;

    iput-object p2, p0, Lcom/facebook/video/analytics/TimedMicroStorage$1;->a:LX/0A8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 24082
    iget-object v0, p0, Lcom/facebook/video/analytics/TimedMicroStorage$1;->b:LX/0A2;

    iget-object v0, v0, LX/0A2;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 24083
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/analytics/TimedMicroStorage$1;->b:LX/0A2;

    iget-object v0, v0, LX/0A2;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 24084
    iget-object v0, p0, Lcom/facebook/video/analytics/TimedMicroStorage$1;->a:LX/0A8;

    const/4 v1, 0x0

    new-array v1, v1, [B

    invoke-virtual {v0, v1}, LX/0A8;->a([B)V

    .line 24085
    :goto_0
    return-void

    .line 24086
    :cond_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v0, p0, Lcom/facebook/video/analytics/TimedMicroStorage$1;->b:LX/0A2;

    iget-object v0, v0, LX/0A2;->b:Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24087
    :try_start_1
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v0}, LX/0hW;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 24088
    iget-object v2, p0, Lcom/facebook/video/analytics/TimedMicroStorage$1;->a:LX/0A8;

    invoke-virtual {v2, v0}, LX/0A8;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 24089
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 24090
    :catch_0
    move-exception v0

    .line 24091
    sget-object v1, LX/0A2;->a:Ljava/lang/String;

    const-string v2, "Cannot read from storage file"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24092
    iget-object v0, p0, Lcom/facebook/video/analytics/TimedMicroStorage$1;->a:LX/0A8;

    new-array v1, v4, [B

    invoke-virtual {v0, v1}, LX/0A8;->a([B)V

    goto :goto_0

    .line 24093
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    throw v0
.end method
