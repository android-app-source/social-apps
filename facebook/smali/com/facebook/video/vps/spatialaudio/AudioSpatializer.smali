.class public Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:I

.field private final c:J

.field private final d:LX/0Ka;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:LX/03z;

.field private f:F

.field public g:LX/0Ke;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41630
    const-string v0, "spatialaudio"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 41631
    return-void
.end method

.method public constructor <init>(LX/03z;IILX/0Ka;)V
    .locals 2
    .param p4    # LX/0Ka;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 41632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41633
    sget-object v0, LX/03z;->UNKNOWN:LX/03z;

    iput-object v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->e:LX/03z;

    .line 41634
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->f:F

    .line 41635
    sget-object v0, LX/0Ke;->UNKNOWN:LX/0Ke;

    iput-object v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->g:LX/0Ke;

    .line 41636
    iput-object p1, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->e:LX/03z;

    .line 41637
    iput p2, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a:I

    .line 41638
    iput p3, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->b:I

    .line 41639
    invoke-static {}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeCreate()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    .line 41640
    iput-object p4, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->d:LX/0Ka;

    .line 41641
    return-void
.end method

.method private static native nativeConfigure(JFIIZ)V
.end method

.method private static native nativeCreate()J
.end method

.method private static native nativeDestroy(J)V
.end method

.method private static native nativeEnableFocus(JZ)V
.end method

.method private static native nativeFinalize(J)V
.end method

.method private static native nativeGetAudioMix(JLjava/nio/ByteBuffer;)I
.end method

.method private static native nativeGetBufferUnderrunCount(J)I
.end method

.method private static native nativeGetPlaybackHeadPosition(J)J
.end method

.method private static native nativeHandleEndOfStream(J)V
.end method

.method private static native nativeInitialize(J)Z
.end method

.method private static native nativeIsInitialized(J)Z
.end method

.method private static native nativePause(J)V
.end method

.method private static native nativePlay(J)V
.end method

.method private static native nativeReset(J)V
.end method

.method private static native nativeSetFocusProperties(JFF)V
.end method

.method private static native nativeSetListenerOrientation(J[F)V
.end method

.method private static native nativeSetVolume(JF)V
.end method

.method private static native nativeWrite(JLjava/nio/ByteBuffer;IILjava/lang/String;)I
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)I
    .locals 6

    .prologue
    .line 41587
    iget-object v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->e:LX/03z;

    iget-boolean v0, v0, LX/03z;->isSpatial:Z

    if-eqz v0, :cond_0

    .line 41588
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    iget-object v2, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->e:LX/03z;

    iget-object v5, v2, LX/03z;->key:Ljava/lang/String;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeWrite(JLjava/nio/ByteBuffer;IILjava/lang/String;)I

    move-result v0

    return v0

    .line 41589
    :cond_0
    new-instance v0, LX/0Kf;

    iget-object v1, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->e:LX/03z;

    invoke-direct {v0, v1}, LX/0Kf;-><init>(LX/03z;)V

    throw v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 41590
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeInitialize(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 41591
    new-instance v0, LX/0Kd;

    invoke-direct {v0}, LX/0Kd;-><init>()V

    throw v0

    .line 41592
    :cond_0
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    iget v2, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->f:F

    invoke-static {v0, v1, v2}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeSetVolume(JF)V

    .line 41593
    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 41594
    iput p1, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->f:F

    .line 41595
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeSetVolume(JF)V

    .line 41596
    return-void
.end method

.method public final a(FF)V
    .locals 2

    .prologue
    .line 41597
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1, p1, p2}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeSetFocusProperties(JFF)V

    .line 41598
    return-void
.end method

.method public final a(FZ)V
    .locals 6

    .prologue
    .line 41599
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    iget v3, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->a:I

    iget v4, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->b:I

    move v2, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeConfigure(JFIIZ)V

    .line 41600
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 41601
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeEnableFocus(JZ)V

    .line 41602
    return-void
.end method

.method public final a([F)V
    .locals 2

    .prologue
    .line 41603
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeSetListenerOrientation(J[F)V

    .line 41604
    return-void
.end method

.method public final b(Ljava/nio/ByteBuffer;)I
    .locals 2

    .prologue
    .line 41605
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1, p1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeGetAudioMix(JLjava/nio/ByteBuffer;)I

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 41606
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeIsInitialized(J)Z

    move-result v0

    return v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 41607
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeGetPlaybackHeadPosition(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 41608
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeHandleEndOfStream(J)V

    .line 41609
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 41610
    invoke-virtual {p0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->g:LX/0Ke;

    sget-object v1, LX/0Ke;->PLAYING:LX/0Ke;

    if-eq v0, v1, :cond_0

    .line 41611
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativePlay(J)V

    .line 41612
    sget-object v0, LX/0Ke;->PLAYING:LX/0Ke;

    iput-object v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->g:LX/0Ke;

    .line 41613
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 41614
    invoke-virtual {p0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->g:LX/0Ke;

    sget-object v1, LX/0Ke;->PAUSED:LX/0Ke;

    if-eq v0, v1, :cond_0

    .line 41615
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativePause(J)V

    .line 41616
    sget-object v0, LX/0Ke;->PAUSED:LX/0Ke;

    iput-object v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->g:LX/0Ke;

    .line 41617
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeGetBufferUnderrunCount(J)I

    move-result v0

    .line 41618
    iget-object v1, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->d:LX/0Ka;

    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    .line 41619
    iget-object v1, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->d:LX/0Ka;

    invoke-virtual {v1, v0}, LX/0Ka;->a(I)V

    .line 41620
    :cond_0
    return-void
.end method

.method public final finalize()V
    .locals 2

    .prologue
    .line 41621
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeFinalize(J)V

    .line 41622
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 41623
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 41624
    invoke-virtual {p0}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->f()V

    .line 41625
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeReset(J)V

    .line 41626
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 41627
    iget-wide v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->c:J

    invoke-static {v0, v1}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->nativeDestroy(J)V

    .line 41628
    return-void
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 41629
    iget v0, p0, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;->b:I

    mul-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x2

    return v0
.end method
