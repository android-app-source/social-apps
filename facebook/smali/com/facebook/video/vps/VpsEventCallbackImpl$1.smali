.class public final Lcom/facebook/video/vps/VpsEventCallbackImpl$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0H9;

.field public final synthetic b:Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;

.field public final synthetic c:LX/0Kc;


# direct methods
.method public constructor <init>(LX/0Kc;LX/0H9;Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V
    .locals 0

    .prologue
    .line 41563
    iput-object p1, p0, Lcom/facebook/video/vps/VpsEventCallbackImpl$1;->c:LX/0Kc;

    iput-object p2, p0, Lcom/facebook/video/vps/VpsEventCallbackImpl$1;->a:LX/0H9;

    iput-object p3, p0, Lcom/facebook/video/vps/VpsEventCallbackImpl$1;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 41564
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VpsEventCallbackImpl$1;->c:LX/0Kc;

    iget-object v0, v0, LX/0Kc;->b:LX/0Kb;

    iget-object v1, p0, Lcom/facebook/video/vps/VpsEventCallbackImpl$1;->a:LX/0H9;

    iget v1, v1, LX/0H9;->mValue:I

    iget-object v2, p0, Lcom/facebook/video/vps/VpsEventCallbackImpl$1;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;

    invoke-virtual {v0, v1, v2}, LX/0Kb;->a(ILcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41565
    :goto_0
    return-void

    .line 41566
    :catch_0
    move-exception v0

    .line 41567
    sget-object v1, LX/0Kc;->a:Ljava/lang/String;

    const-string v2, "Exception in VPS listener callback event %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/video/vps/VpsEventCallbackImpl$1;->a:LX/0H9;

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/0Gj;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
