.class public Lcom/facebook/video/vps/VideoPlayerService;
.super Landroid/app/Service;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "StringFormatUse",
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e",
        "BadMethodUse-java.lang.String.length",
        "HardcodedIPAddressUse"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public volatile A:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final B:LX/0BS;

.field private final C:Ljava/util/TimerTask;

.field public final b:Ljava/lang/Object;

.field public c:Landroid/net/Uri;

.field public d:LX/043;

.field public e:LX/0K9;

.field public final f:LX/0Gh;

.field public final g:LX/0KC;

.field public final h:LX/0GE;

.field public final i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            "LX/0Kb;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mListenerHashMapInner"
    .end annotation
.end field

.field public final j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/exoplayer/ipc/MediaRenderer;",
            "LX/0GT;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mTrackRendererHashMapInner"
    .end annotation
.end field

.field public final k:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            "Lcom/facebook/exoplayer/ipc/RendererContext;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mRendererContextHashMapInner"
    .end annotation
.end field

.field private final l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mVideoMetadataHashMapInner"
    .end annotation
.end field

.field public final m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            "LX/0Kx;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mExoPlayerHashMapInner"
    .end annotation
.end field

.field public final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            "Lcom/facebook/exoplayer/ipc/VideoPlayRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mDashLiveStartPositions"
    .end annotation
.end field

.field private final p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mToBeReleasedInner"
    .end annotation
.end field

.field public final q:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0Kb;",
            ">;"
        }
    .end annotation
.end field

.field private volatile r:Landroid/os/Handler;

.field public volatile s:I

.field public volatile t:I

.field public volatile u:Z

.field public final v:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mTurnedOffNonAudioTracks"
    .end annotation
.end field

.field public final w:Ljava/lang/Runnable;

.field public final x:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            ">;"
        }
    .end annotation
.end field

.field public y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11809
    const-class v0, Lcom/facebook/video/vps/VideoPlayerService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/vps/VideoPlayerService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 11782
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 11783
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->b:Ljava/lang/Object;

    .line 11784
    iput-object v3, p0, Lcom/facebook/video/vps/VideoPlayerService;->c:Landroid/net/Uri;

    .line 11785
    new-instance v0, LX/0Gh;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Gh;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->f:LX/0Gh;

    .line 11786
    new-instance v0, LX/0KC;

    invoke-direct {v0}, LX/0KC;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->g:LX/0KC;

    .line 11787
    new-instance v0, LX/0GE;

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/video/vps/VideoPlayerService;->f:LX/0Gh;

    invoke-direct {v0, v1, p0, v2}, LX/0GE;-><init>(ILandroid/content/Context;LX/0Gh;)V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->h:LX/0GE;

    .line 11788
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->i:Ljava/util/HashMap;

    .line 11789
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    .line 11790
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->k:Ljava/util/HashMap;

    .line 11791
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->l:Ljava/util/HashMap;

    .line 11792
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    .line 11793
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->n:Ljava/util/Map;

    .line 11794
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->o:Ljava/util/HashMap;

    .line 11795
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    .line 11796
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->q:Ljava/util/concurrent/atomic/AtomicReference;

    .line 11797
    iput v4, p0, Lcom/facebook/video/vps/VideoPlayerService;->s:I

    .line 11798
    iput v4, p0, Lcom/facebook/video/vps/VideoPlayerService;->t:I

    .line 11799
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->u:Z

    .line 11800
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->v:Ljava/util/HashMap;

    .line 11801
    new-instance v0, Lcom/facebook/video/vps/VideoPlayerService$1;

    invoke-direct {v0, p0}, Lcom/facebook/video/vps/VideoPlayerService$1;-><init>(Lcom/facebook/video/vps/VideoPlayerService;)V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->w:Ljava/lang/Runnable;

    .line 11802
    new-instance v0, LX/0KO;

    const/4 v1, 0x3

    invoke-direct {v0, p0, v1}, LX/0KO;-><init>(Lcom/facebook/video/vps/VideoPlayerService;I)V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->x:Landroid/util/LruCache;

    .line 11803
    iput-object v3, p0, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 11804
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->z:Z

    .line 11805
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 11806
    new-instance v0, LX/0KP;

    invoke-direct {v0, p0}, LX/0KP;-><init>(Lcom/facebook/video/vps/VideoPlayerService;)V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->B:LX/0BS;

    .line 11807
    new-instance v0, Lcom/facebook/video/vps/VideoPlayerService$4;

    invoke-direct {v0, p0}, Lcom/facebook/video/vps/VideoPlayerService$4;-><init>(Lcom/facebook/video/vps/VideoPlayerService;)V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->C:Ljava/util/TimerTask;

    .line 11808
    return-void
.end method

.method private a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;JZ)J
    .locals 10

    .prologue
    .line 11766
    invoke-static {p0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v3

    .line 11767
    if-eqz v3, :cond_5

    if-eqz p1, :cond_5

    .line 11768
    iget-object v0, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->h:LX/0H5;

    invoke-static {v0}, LX/0H5;->isLive(LX/0H5;)Z

    move-result v4

    .line 11769
    if-eqz p4, :cond_2

    iget-object v0, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->h:LX/0H5;

    invoke-static {v0}, LX/0H5;->isLive(LX/0H5;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v2, v0

    .line 11770
    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v3}, LX/0Kx;->g()J

    move-result-wide v0

    .line 11771
    :goto_1
    const/4 v5, 0x1

    move v5, v5

    .line 11772
    if-eqz v5, :cond_0

    if-eqz v4, :cond_0

    .line 11773
    const-string v4, "StartPos:%d, RelativePos: %d, AbsolutePos: %d, bufferedPos: %d, useRelativePos: %b"

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-interface {v3}, LX/0Kx;->g()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-interface {v3}, LX/0Kx;->f()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    invoke-interface {v3}, LX/0Kx;->h()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x4

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v5, v3

    invoke-static {p0, p1, v4, v5}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11774
    :cond_0
    const-wide/16 v2, -0x1

    cmp-long v2, p2, v2

    if-eqz v2, :cond_1

    .line 11775
    cmp-long v2, v0, p2

    if-lez v2, :cond_4

    sub-long/2addr v0, p2

    .line 11776
    :cond_1
    :goto_2
    return-wide v0

    .line 11777
    :cond_2
    const/4 v0, 0x0

    move v2, v0

    goto :goto_0

    .line 11778
    :cond_3
    invoke-interface {v3}, LX/0Kx;->f()J

    move-result-wide v0

    goto :goto_1

    .line 11779
    :cond_4
    const-wide/16 v0, 0x0

    goto :goto_2

    .line 11780
    :cond_5
    const-string v0, "no available player to getCurrentPositionMs"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11781
    const-wide/16 v0, -0x1

    goto :goto_2
.end method

.method private a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZZ)J
    .locals 5

    .prologue
    .line 11755
    const-wide/16 v2, -0x1

    .line 11756
    if-nez p2, :cond_1

    if-eqz p3, :cond_1

    .line 11757
    iget-object v4, p0, Lcom/facebook/video/vps/VideoPlayerService;->o:Ljava/util/HashMap;

    monitor-enter v4

    .line 11758
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 11759
    if-eqz v0, :cond_0

    .line 11760
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 11761
    :goto_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11762
    :goto_1
    iget-object v2, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v2

    .line 11763
    :try_start_1
    invoke-direct {p0, p1, v0, v1, p2}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;JZ)J

    move-result-wide v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-wide v0

    .line 11764
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 11765
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_0
    move-wide v0, v2

    goto :goto_0

    :cond_1
    move-wide v0, v2

    goto :goto_1
.end method

.method public static synthetic a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZZ)J
    .locals 2

    .prologue
    .line 11754
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZZ)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Lcom/facebook/exoplayer/ipc/MediaRenderer;)LX/0GT;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 11751
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    monitor-enter v1

    .line 11752
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0GT;

    monitor-exit v1

    return-object v0

    .line 11753
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0Ah;LX/03z;JLX/0Gb;LX/0Ka;)LX/0GT;
    .locals 8
    .param p2    # LX/0Ah;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 11724
    if-nez p2, :cond_0

    .line 11725
    const/4 v0, 0x0

    .line 11726
    :goto_0
    return-object v0

    .line 11727
    :cond_0
    iget-object v0, p2, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    .line 11728
    const-string v1, "audio/webm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 11729
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->e:LX/0K9;

    iget-object v1, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/video/vps/VideoPlayerService;->c:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->i:Ljava/util/Map;

    invoke-static {p0}, Lcom/facebook/video/vps/VideoPlayerService;->f(Lcom/facebook/video/vps/VideoPlayerService;)LX/04m;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/0K9;->a(Ljava/lang/String;Landroid/net/Uri;IZZLjava/util/Map;LX/04m;)LX/0G6;

    move-result-object v2

    .line 11730
    new-instance v0, LX/0Lq;

    invoke-static {}, LX/0Lt;->a()LX/0Lt;

    move-result-object v1

    new-instance v3, LX/0Le;

    invoke-direct {v3}, LX/0Le;-><init>()V

    const/4 v6, 0x1

    const/4 v4, 0x1

    new-array v7, v4, [LX/0Ah;

    const/4 v4, 0x0

    aput-object p2, v7, v4

    move-wide v4, p4

    invoke-direct/range {v0 .. v7}, LX/0Lq;-><init>(LX/0Lr;LX/0G6;LX/04o;JI[LX/0Ah;)V

    .line 11731
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 11732
    new-instance v2, LX/0LY;

    invoke-static {v1}, LX/040;->a(Ljava/util/Map;)I

    move-result v3

    invoke-static {v1}, LX/040;->c(Ljava/util/Map;)I

    move-result v4

    mul-int/2addr v3, v4

    invoke-direct {v2, v0, p6, v3}, LX/0LY;-><init>(LX/0LZ;LX/0Gb;I)V

    .line 11733
    new-instance v3, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;

    .line 11734
    sget-object v0, LX/040;->ai:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 11735
    sget-object v0, LX/040;->ai:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 11736
    :goto_1
    move v0, v0

    .line 11737
    sget-object v4, LX/040;->aj:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 11738
    sget-object v4, LX/040;->aj:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 11739
    :goto_2
    move v4, v4

    .line 11740
    invoke-direct {v3, p3, v0, v4, p7}, Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;-><init>(LX/03z;IILX/0Ka;)V

    .line 11741
    const/4 v4, 0x0

    .line 11742
    sget-object v0, LX/040;->ah:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 11743
    sget-object v0, LX/040;->ah:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    .line 11744
    :goto_3
    move v0, v0

    .line 11745
    if-eqz v0, :cond_1

    .line 11746
    new-instance v0, LX/0Kh;

    invoke-static {p0}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1, p7}, LX/0Kh;-><init>(LX/0L9;Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;Landroid/os/Handler;LX/0Ka;)V

    goto/16 :goto_0

    .line 11747
    :cond_1
    new-instance v0, LX/0Kj;

    invoke-static {p0}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1, p7}, LX/0Kj;-><init>(LX/0L9;Lcom/facebook/video/vps/spatialaudio/AudioSpatializer;Landroid/os/Handler;LX/0Ka;)V

    goto/16 :goto_0

    .line 11748
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected mime type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    const/16 v0, 0x2000

    goto :goto_1

    :cond_4
    const/16 v4, 0x400

    goto :goto_2

    :cond_5
    move v0, v4

    .line 11749
    goto :goto_3

    :cond_6
    move v0, v4

    .line 11750
    goto :goto_3
.end method

.method public static synthetic a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/MediaRenderer;)LX/0GT;
    .locals 1

    .prologue
    .line 11723
    invoke-direct {p0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/MediaRenderer;)LX/0GT;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0Ah;JLX/0Gb;LX/0Ka;)LX/0GX;
    .locals 9
    .param p2    # LX/0Ah;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 11626
    if-nez p2, :cond_0

    .line 11627
    const/4 v0, 0x0

    .line 11628
    :goto_0
    return-object v0

    .line 11629
    :cond_0
    iget-object v0, p2, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    .line 11630
    const-string v1, "audio/mp4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "audio/mp4a-latm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 11631
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Creating Audio Sample Source "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11632
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->e:LX/0K9;

    iget-object v1, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/video/vps/VideoPlayerService;->c:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->i:Ljava/util/Map;

    invoke-static {p0}, Lcom/facebook/video/vps/VideoPlayerService;->f(Lcom/facebook/video/vps/VideoPlayerService;)LX/04m;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/0K9;->a(Ljava/lang/String;Landroid/net/Uri;IZZLjava/util/Map;LX/04m;)LX/0G6;

    move-result-object v2

    .line 11633
    new-instance v0, LX/0Lq;

    invoke-static {}, LX/0Lt;->a()LX/0Lt;

    move-result-object v1

    new-instance v3, LX/0Le;

    invoke-direct {v3}, LX/0Le;-><init>()V

    const/4 v6, 0x1

    const/4 v4, 0x1

    new-array v7, v4, [LX/0Ah;

    const/4 v4, 0x0

    aput-object p2, v7, v4

    move-wide v4, p3

    invoke-direct/range {v0 .. v7}, LX/0Lq;-><init>(LX/0Lr;LX/0G6;LX/04o;JI[LX/0Ah;)V

    .line 11634
    iget-object v2, p0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 11635
    new-instance v1, LX/0LY;

    invoke-static {v2}, LX/040;->a(Ljava/util/Map;)I

    move-result v3

    invoke-static {v2}, LX/040;->c(Ljava/util/Map;)I

    move-result v2

    mul-int/2addr v2, v3

    invoke-direct {v1, v0, p5, v2}, LX/0LY;-><init>(LX/0LZ;LX/0Gb;I)V

    .line 11636
    new-instance v0, LX/0GX;

    sget-object v2, LX/0L1;->a:LX/0L1;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {p0}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, LX/0GX;-><init>(LX/0L9;LX/0L1;LX/0M3;ZLandroid/os/Handler;LX/0Jr;)V

    goto :goto_0

    .line 11637
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected mime type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0AY;LX/0Gb;LX/0Ka;)LX/0Jw;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v10, -0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 11701
    invoke-virtual {p2}, LX/0AY;->b()I

    move-result v0

    if-gtz v0, :cond_1

    .line 11702
    :cond_0
    :goto_0
    return-object v1

    .line 11703
    :cond_1
    invoke-virtual {p2, v3}, LX/0AY;->a(I)LX/0Am;

    move-result-object v0

    .line 11704
    invoke-virtual {v0, v3}, LX/0Am;->a(I)I

    move-result v2

    .line 11705
    if-eq v2, v10, :cond_4

    .line 11706
    iget-object v0, v0, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    .line 11707
    :goto_1
    if-eqz v0, :cond_0

    .line 11708
    :try_start_0
    iget-object v2, v0, LX/0Ak;->c:Ljava/util/List;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {p0, v2, v4, v5}, LX/0Li;->a(Landroid/content/Context;Ljava/util/List;[Ljava/lang/String;Z)[I
    :try_end_0
    .catch LX/090; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 11709
    if-eqz v2, :cond_0

    array-length v2, v2

    if-eqz v2, :cond_0

    .line 11710
    iget-object v0, v0, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    iget-object v0, v0, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    .line 11711
    const-string v1, "video/avc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "video/mp4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 11712
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Creating Video Sample Source: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11713
    iget-object v8, p0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 11714
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->e:LX/0K9;

    iget-object v1, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/video/vps/VideoPlayerService;->c:Landroid/net/Uri;

    iget-object v6, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->i:Ljava/util/Map;

    invoke-static {p0}, Lcom/facebook/video/vps/VideoPlayerService;->f(Lcom/facebook/video/vps/VideoPlayerService;)LX/04m;

    move-result-object v7

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v7}, LX/0K9;->a(Ljava/lang/String;Landroid/net/Uri;IZZLjava/util/Map;LX/04m;)LX/0G6;

    move-result-object v0

    .line 11715
    new-instance v1, LX/0Lq;

    invoke-static {p0, v3, v3}, LX/0Lt;->a(Landroid/content/Context;ZZ)LX/0Lt;

    move-result-object v2

    new-instance v3, LX/0KF;

    invoke-direct {v3, p0, p1}, LX/0KF;-><init>(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    invoke-direct {v1, p2, v2, v0, v3}, LX/0Lq;-><init>(LX/0AY;LX/0Lr;LX/0G6;LX/04o;)V

    .line 11716
    new-instance v0, LX/0LY;

    invoke-static {v8}, LX/040;->b(Ljava/util/Map;)I

    move-result v2

    invoke-static {v8}, LX/040;->c(Ljava/util/Map;)I

    move-result v3

    mul-int/2addr v3, v2

    invoke-static {p0}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v4

    move-object v2, p3

    move-object v5, p4

    move v6, v9

    invoke-direct/range {v0 .. v6}, LX/0LY;-><init>(LX/0LZ;LX/0Gb;ILandroid/os/Handler;LX/0KZ;I)V

    .line 11717
    new-instance v1, LX/0Jw;

    sget-object v4, LX/0L1;->a:LX/0L1;

    const-wide/16 v6, 0x0

    invoke-static {p0}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v8

    move-object v2, p0

    move-object v3, v0

    move v5, v9

    move-object v9, p4

    invoke-direct/range {v1 .. v10}, LX/0Jw;-><init>(Landroid/content/Context;LX/0L9;LX/0L1;IJLandroid/os/Handler;LX/0Ju;I)V

    goto/16 :goto_0

    .line 11718
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected mime type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 11719
    :catch_0
    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto/16 :goto_1
.end method

.method private a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/util/Map;)LX/0Kx;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerSession;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Kx;"
        }
    .end annotation

    .prologue
    .line 11678
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 11679
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Kx;

    .line 11680
    if-eqz v0, :cond_0

    .line 11681
    const-string v2, "Found ExoPlayer instance"

    invoke-static {p0, v2, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11682
    :goto_0
    monitor-exit v1

    return-object v0

    .line 11683
    :cond_0
    const-string v0, "ExoPlayer.Factory.newInstance"

    invoke-static {p0, v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11684
    const/4 v0, 0x2

    .line 11685
    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->h:LX/0H5;

    invoke-static {v2}, LX/0H5;->isLive(LX/0H5;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "video.live.min_buffer_ms"

    invoke-interface {p2, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 11686
    const-string v2, "video.live.min_buffer_ms"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 11687
    :goto_1
    move v2, v2

    .line 11688
    iget-object v3, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->h:LX/0H5;

    invoke-static {v3}, LX/0H5;->isLive(LX/0H5;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "video.live.min_rebuffer_ms"

    invoke-interface {p2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 11689
    const-string v3, "video.live.min_rebuffer_ms"

    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 11690
    :goto_2
    move v3, v3

    .line 11691
    invoke-static {v0, v2, v3}, LX/0Kw;->a(III)LX/0Kx;

    move-result-object v0

    .line 11692
    new-instance v2, LX/0KX;

    invoke-direct {v2, p0, p1}, LX/0KX;-><init>(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    invoke-interface {v0, v2}, LX/0Kx;->a(LX/0KW;)V

    .line 11693
    iget-object v2, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 11694
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 11695
    :cond_1
    :try_start_1
    sget-object v2, LX/040;->H:Ljava/lang/String;

    invoke-interface {p2, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 11696
    sget-object v2, LX/040;->H:Ljava/lang/String;

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 11697
    :cond_2
    const/16 v2, 0x1f4

    goto :goto_1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 11698
    :cond_3
    sget-object v3, LX/040;->I:Ljava/lang/String;

    invoke-interface {p2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 11699
    sget-object v3, LX/040;->I:Ljava/lang/String;

    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    goto :goto_2

    .line 11700
    :cond_4
    const/16 v3, 0x7d0

    goto :goto_2
.end method

.method public static synthetic a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/util/Map;)LX/0Kx;
    .locals 1

    .prologue
    .line 11677
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/util/Map;)LX/0Kx;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11676
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, LX/0Kn;

    if-eqz v0, :cond_0

    sget-object v0, LX/0KD;->BEHIND_LIVE_WINDOW_ERROR:LX/0KD;

    iget-object v0, v0, LX/0KD;->value:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/0Kx;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 4

    .prologue
    .line 11658
    if-nez p1, :cond_0

    .line 11659
    :goto_0
    return-void

    .line 11660
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 11661
    invoke-direct {p0, p2}, Lcom/facebook/video/vps/VideoPlayerService;->e(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v1

    .line 11662
    invoke-static {v0}, LX/040;->W(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 11663
    invoke-interface {p1}, LX/0Kx;->c()V

    .line 11664
    if-eqz v1, :cond_1

    .line 11665
    const/4 v0, 0x1

    :try_start_0
    const-string v2, "cleanUpSession - Start onPlayerStateChanged call"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2, v0, v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 11666
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, p2, v0, v2, v3}, LX/0Kb;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11667
    :cond_1
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "cleanUpSession - Finished onPlayerStateChanged call. VideoPlayerService listener was "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_4

    const-string v0, "called"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11668
    :cond_2
    invoke-direct {p0, p2}, Lcom/facebook/video/vps/VideoPlayerService;->r(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Ljava/lang/Long;

    .line 11669
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->n:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;

    .line 11670
    if-eqz v0, :cond_3

    iget-object v1, v0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->h:Landroid/os/ParcelFileDescriptor;

    if-eqz v1, :cond_3

    .line 11671
    :try_start_1
    iget-object v0, v0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->h:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 11672
    :cond_3
    :goto_3
    invoke-static {p0, p2}, Lcom/facebook/video/vps/VideoPlayerService;->p(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0

    .line 11673
    :catch_0
    const-string v0, "Error while updating release player state change"

    invoke-static {v0, p2}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_1

    .line 11674
    :cond_4
    const-string v0, "not called because listener was null"

    goto :goto_2

    .line 11675
    :catch_1
    const-string v0, "ParcelFileDescriptor close fail, nothing we can do"

    invoke-static {v0, p2}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_3
.end method

.method private a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0GT;LX/0GT;)V
    .locals 3
    .param p2    # LX/0GT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0GT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 11644
    const/4 v0, 0x1

    move v0, v0

    .line 11645
    if-eqz v0, :cond_0

    .line 11646
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "hashcode of v: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->e:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {v2}, Lcom/facebook/exoplayer/ipc/MediaRenderer;->hashCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " rendererImpl: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p2, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->d(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11647
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "hashcode of a: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->f:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {v2}, Lcom/facebook/exoplayer/ipc/MediaRenderer;->hashCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " rendererImpl: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-nez p3, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->d(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11648
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    monitor-enter v1

    .line 11649
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->e:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11650
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->f:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {v0, v2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11651
    const/4 v0, 0x1

    move v0, v0

    .line 11652
    if-eqz v0, :cond_1

    .line 11653
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mTrackRendererHashMapInner.size() = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->d(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11654
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 11655
    :cond_2
    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    .line 11656
    :cond_3
    invoke-virtual {p3}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    .line 11657
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static varargs a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZLjava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 11640
    const/4 v0, 0x1

    move v0, v0

    .line 11641
    if-eqz v0, :cond_0

    .line 11642
    invoke-static {p0, p1, p2, p3}, Lcom/facebook/video/vps/VideoPlayerService;->b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 11643
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0AY;LX/0K4;LX/0Ka;)V
    .locals 0

    .prologue
    .line 11639
    invoke-static {p0, p1, p2, p3, p4}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0AY;LX/0K4;LX/0Ka;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0GT;LX/0GT;ZLcom/facebook/exoplayer/ipc/RendererContext;)V
    .locals 0

    .prologue
    .line 11638
    invoke-static/range {p0 .. p5}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0GT;LX/0GT;ZLcom/facebook/exoplayer/ipc/RendererContext;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 11824
    return-void
.end method

.method public static synthetic a(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Ljava/lang/Throwable;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 0

    .prologue
    .line 11823
    invoke-static {p0, p1, p2, p3}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Ljava/lang/Throwable;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    return-void
.end method

.method private static varargs a(Ljava/util/HashMap;LX/0KR;[Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/HashMap",
            "<TK;TV;>;",
            "LX/0KR",
            "<TV;>;[TK;)V"
        }
    .end annotation

    .prologue
    .line 11942
    const/4 v0, 0x0

    .line 11943
    if-eqz p2, :cond_1

    .line 11944
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 11945
    array-length v2, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p2, v1

    .line 11946
    invoke-virtual {p0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 11947
    if-eqz v4, :cond_0

    .line 11948
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11949
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 11950
    :cond_1
    if-eqz p1, :cond_2

    .line 11951
    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 11952
    invoke-interface {p1, v2}, LX/0KR;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 11953
    :cond_2
    invoke-virtual {p0}, Ljava/util/HashMap;->clear()V

    .line 11954
    if-eqz v0, :cond_3

    .line 11955
    invoke-virtual {p0, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 11956
    :cond_3
    return-void
.end method

.method private static a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0AY;LX/0K4;LX/0Ka;)V
    .locals 17
    .param p2    # LX/0AY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 11908
    invoke-virtual/range {p2 .. p2}, LX/0AY;->b()I

    move-result v4

    if-gtz v4, :cond_0

    .line 11909
    :goto_0
    return-void

    .line 11910
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, LX/0AY;->a(I)LX/0Am;

    move-result-object v5

    .line 11911
    const/4 v4, 0x0

    .line 11912
    const/4 v12, 0x0

    .line 11913
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 11914
    iget-object v5, v5, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v13, v4

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Ak;

    .line 11915
    iget v5, v4, LX/0Ak;->b:I

    packed-switch v5, :pswitch_data_0

    move-object v5, v12

    :cond_1
    move-object v12, v5

    .line 11916
    goto :goto_1

    .line 11917
    :pswitch_0
    iget-object v4, v4, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v5, v13

    :cond_2
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Ah;

    .line 11918
    if-nez v5, :cond_2

    .line 11919
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Add audio representation "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v4, LX/0Ah;->c:LX/0AR;

    iget-object v8, v8, LX/0AR;->b:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v5, v1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    move-object v5, v4

    .line 11920
    goto :goto_2

    :cond_3
    move-object v13, v5

    .line 11921
    goto :goto_1

    .line 11922
    :pswitch_1
    if-nez v12, :cond_b

    move-object v5, v4

    .line 11923
    :goto_3
    iget-object v4, v4, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Ah;

    .line 11924
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_4

    .line 11925
    const-string v8, ","

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11926
    :cond_4
    iget-object v8, v4, LX/0Ah;->c:LX/0AR;

    iget-object v8, v8, LX/0AR;->d:Ljava/lang/String;

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11927
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Add video representation "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v4, LX/0Ah;->c:LX/0AR;

    iget-object v4, v4, LX/0AR;->b:Ljava/lang/String;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v4, v1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_4

    .line 11928
    :cond_5
    if-eqz p3, :cond_6

    invoke-virtual/range {p3 .. p3}, LX/0K4;->a()LX/03z;

    move-result-object v4

    move-object v14, v4

    .line 11929
    :goto_5
    invoke-static/range {p0 .. p1}, Lcom/facebook/video/vps/VideoPlayerService;->h(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;

    move-result-object v4

    invoke-virtual {v14}, LX/03z;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->a(Ljava/lang/String;)V

    .line 11930
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 11931
    invoke-static {v11}, LX/040;->U(Ljava/util/Map;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 11932
    new-instance v4, LX/0Gc;

    new-instance v5, LX/0OB;

    invoke-static {v11}, LX/040;->c(Ljava/util/Map;)I

    move-result v6

    invoke-direct {v5, v6}, LX/0OB;-><init>(I)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v11}, LX/040;->M(Ljava/util/Map;)I

    move-result v8

    invoke-static {v11}, LX/040;->N(Ljava/util/Map;)I

    move-result v9

    invoke-static {v11}, LX/040;->Q(Ljava/util/Map;)F

    move-result v10

    invoke-static {v11}, LX/040;->R(Ljava/util/Map;)F

    move-result v11

    invoke-direct/range {v4 .. v11}, LX/0Gc;-><init>(LX/0O1;Landroid/os/Handler;LX/0GZ;IIFF)V

    move-object v10, v4

    .line 11933
    :goto_6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v10, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0AY;LX/0Gb;LX/0Ka;)LX/0Jw;

    move-result-object v16

    .line 11934
    iget-boolean v4, v14, LX/03z;->isSpatial:Z

    if-eqz v4, :cond_8

    move-object/from16 v0, p2

    iget-wide v8, v0, LX/0AY;->c:J

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object v6, v13

    move-object v7, v14

    move-object/from16 v11, p4

    invoke-direct/range {v4 .. v11}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0Ah;LX/03z;JLX/0Gb;LX/0Ka;)LX/0GT;

    move-result-object v7

    .line 11935
    :goto_7
    if-eqz v12, :cond_9

    iget-object v4, v12, LX/0Ak;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    .line 11936
    :goto_8
    const/4 v8, 0x0

    new-instance v9, Lcom/facebook/exoplayer/ipc/RendererContext;

    sget-object v5, LX/0HE;->DASH:LX/0HE;

    invoke-virtual {v5}, LX/0HE;->toString()Ljava/lang/String;

    move-result-object v6

    if-eqz v13, :cond_a

    iget-object v5, v13, LX/0Ah;->c:LX/0AR;

    iget v5, v5, LX/0AR;->c:I

    :goto_9
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v6, v5, v4, v10}, Lcom/facebook/exoplayer/ipc/RendererContext;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, v16

    invoke-static/range {v4 .. v9}, Lcom/facebook/video/vps/VideoPlayerService;->a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0GT;LX/0GT;ZLcom/facebook/exoplayer/ipc/RendererContext;)V

    goto/16 :goto_0

    .line 11937
    :cond_6
    sget-object v4, LX/03z;->UNKNOWN:LX/03z;

    move-object v14, v4

    goto/16 :goto_5

    .line 11938
    :cond_7
    new-instance v4, LX/0Kt;

    new-instance v5, LX/0OB;

    invoke-static {v11}, LX/040;->c(Ljava/util/Map;)I

    move-result v6

    invoke-direct {v5, v6}, LX/0OB;-><init>(I)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v11}, LX/040;->M(Ljava/util/Map;)I

    move-result v8

    invoke-static {v11}, LX/040;->N(Ljava/util/Map;)I

    move-result v9

    invoke-static {v11}, LX/040;->Q(Ljava/util/Map;)F

    move-result v10

    invoke-static {v11}, LX/040;->R(Ljava/util/Map;)F

    move-result v11

    invoke-direct/range {v4 .. v11}, LX/0Kt;-><init>(LX/0O1;Landroid/os/Handler;LX/0Kr;IIFF)V

    move-object v10, v4

    goto :goto_6

    .line 11939
    :cond_8
    move-object/from16 v0, p2

    iget-wide v8, v0, LX/0AY;->c:J

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object v7, v13

    move-object/from16 v11, p4

    invoke-direct/range {v5 .. v11}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0Ah;JLX/0Gb;LX/0Ka;)LX/0GX;

    move-result-object v7

    goto :goto_7

    .line 11940
    :cond_9
    const/4 v4, 0x0

    goto :goto_8

    .line 11941
    :cond_a
    const/4 v5, 0x0

    goto :goto_9

    :cond_b
    move-object v5, v12

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0GT;LX/0GT;ZLcom/facebook/exoplayer/ipc/RendererContext;)V
    .locals 2

    .prologue
    .line 11905
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 11906
    :try_start_0
    invoke-direct/range {p0 .. p5}, Lcom/facebook/video/vps/VideoPlayerService;->b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0GT;LX/0GT;ZLcom/facebook/exoplayer/ipc/RendererContext;)V

    .line 11907
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 2

    .prologue
    .line 11902
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 11903
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/vps/VideoPlayerService;->b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11904
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/lang/Long;)V
    .locals 2

    .prologue
    .line 11899
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->o:Ljava/util/HashMap;

    monitor-enter v1

    .line 11900
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11901
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static varargs a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 11897
    const/4 v0, 0x0

    invoke-static {p1, v0, p2, p3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 11898
    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Ljava/lang/Throwable;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 6

    .prologue
    const/16 v2, 0x3e8

    .line 11884
    invoke-virtual {p0, p3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v0

    .line 11885
    invoke-static {p0, p3}, Lcom/facebook/video/vps/VideoPlayerService;->b(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J

    move-result-wide v4

    .line 11886
    invoke-static {p0, p3}, Lcom/facebook/video/vps/VideoPlayerService;->c$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11887
    if-eqz v0, :cond_1

    .line 11888
    const/4 v3, 0x0

    .line 11889
    if-eqz p3, :cond_0

    :try_start_0
    iget-object v1, p3, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->h:LX/0H5;

    invoke-static {v1}, LX/0H5;->isLive(LX/0H5;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11890
    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    .line 11891
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 11892
    const/4 v1, 0x0

    const/16 v2, 0x3e8

    invoke-virtual {v3, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 11893
    :cond_0
    invoke-static {p2}, Lcom/facebook/video/vps/VideoPlayerService;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/0Kb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11894
    :cond_1
    :goto_0
    return-void

    .line 11895
    :catch_0
    move-exception v0

    .line 11896
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Caught exception when sending error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J
    .locals 2

    .prologue
    .line 11881
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 11882
    invoke-static {v0}, LX/040;->Z(Ljava/util/Map;)Z

    move-result v0

    .line 11883
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static b(LX/042;)LX/0Kb;
    .locals 1

    .prologue
    .line 11880
    new-instance v0, LX/0Kb;

    invoke-direct {v0, p0}, LX/0Kb;-><init>(LX/042;)V

    return-object v0
.end method

.method private b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0GT;LX/0GT;ZLcom/facebook/exoplayer/ipc/RendererContext;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 11861
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/0GT;LX/0GT;)V

    .line 11862
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->k:Ljava/util/HashMap;

    monitor-enter v1

    .line 11863
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11864
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11865
    const/4 v0, 0x1

    move v0, v0

    .line 11866
    if-eqz v0, :cond_0

    .line 11867
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "size of ExoPlayers is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/facebook/video/vps/VideoPlayerService;->i()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->d(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11868
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v0

    .line 11869
    if-eqz v0, :cond_1

    .line 11870
    iget-object v1, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->e:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->f:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {v0, v1, v2, p5}, LX/0Kb;->a(Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/RendererContext;)V

    .line 11871
    :cond_1
    invoke-static {p0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v1

    .line 11872
    if-nez v1, :cond_3

    .line 11873
    :cond_2
    :goto_0
    return-void

    .line 11874
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 11875
    :cond_3
    if-nez p4, :cond_4

    .line 11876
    const/4 v0, 0x2

    new-array v0, v0, [LX/0GT;

    aput-object p2, v0, v3

    const/4 v2, 0x1

    aput-object p3, v0, v2

    invoke-interface {v1, v0}, LX/0Kx;->a([LX/0GT;)V

    goto :goto_0

    .line 11877
    :cond_4
    if-eqz v0, :cond_2

    .line 11878
    const/4 v2, 0x0

    :try_start_2
    invoke-interface {v1}, LX/0Kx;->a()I

    move-result v3

    invoke-interface {v1}, LX/0Kx;->i()I

    move-result v1

    invoke-virtual {v0, p1, v2, v3, v1}, LX/0Kb;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 11879
    :catch_0
    const-string v0, "error update player state change"

    invoke-static {v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0
.end method

.method private b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 5

    .prologue
    .line 11857
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Marked video player session with key "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to be evicted."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11858
    invoke-direct {p0, p2}, Lcom/facebook/video/vps/VideoPlayerService;->m(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11859
    invoke-static {p0}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->C:Ljava/util/TimerTask;

    const-wide/16 v2, 0x3e8

    const v4, -0x5f3dc815

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 11860
    return-void
.end method

.method private static varargs b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZLjava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 11850
    if-nez p0, :cond_1

    const-string v0, "null"

    .line 11851
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "session["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p3

    if-nez v1, :cond_2

    :goto_1
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11852
    if-eqz p1, :cond_0

    .line 11853
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Show Stack Trace[Not Error]"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    .line 11854
    :cond_0
    return-void

    .line 11855
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 11856
    :cond_2
    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_1
.end method

.method public static b(Ljava/lang/Throwable;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 2

    .prologue
    .line 11844
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "session["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p2, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 11845
    if-eqz p0, :cond_1

    .line 11846
    sget-object v1, Lcom/facebook/video/vps/VideoPlayerService;->a:Ljava/lang/String;

    invoke-static {v1, v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 11847
    :goto_1
    return-void

    .line 11848
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 11849
    :cond_1
    sget-object v1, Lcom/facebook/video/vps/VideoPlayerService;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static c(Lcom/facebook/video/vps/VideoPlayerService;)V
    .locals 2

    .prologue
    .line 11833
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 11834
    invoke-static {v0}, LX/040;->ah(Ljava/util/Map;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11835
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 11836
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/video/vps/VideoPlayerService;->e()V

    .line 11837
    monitor-exit v1

    .line 11838
    :goto_0
    return-void

    .line 11839
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 11840
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 11841
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/video/vps/VideoPlayerService;->d()V

    .line 11842
    monitor-exit v1

    goto :goto_0

    .line 11843
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method public static c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11831
    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p2, v1, p1, v0}, Lcom/facebook/video/vps/VideoPlayerService;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 11832
    return-void
.end method

.method public static c$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 1

    .prologue
    .line 11825
    const-string v0, "Release now"

    invoke-static {p0, v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11826
    invoke-direct {p0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->j(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v0

    .line 11827
    if-eqz v0, :cond_0

    .line 11828
    invoke-direct {p0, v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->a(LX/0Kx;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11829
    invoke-interface {v0}, LX/0Kx;->d()V

    .line 11830
    :cond_0
    return-void
.end method

.method private d(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;
    .locals 1
    .param p1    # Lcom/facebook/exoplayer/ipc/VideoPlayerSession;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 11720
    if-nez p1, :cond_0

    .line 11721
    const/4 v0, 0x0

    .line 11722
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Kb;

    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 11810
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->x:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 11811
    invoke-static {p0}, Lcom/facebook/video/vps/VideoPlayerService;->k(Lcom/facebook/video/vps/VideoPlayerService;)[Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    move-result-object v1

    .line 11812
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 11813
    invoke-static {p0, v3}, Lcom/facebook/video/vps/VideoPlayerService;->c$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11814
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11815
    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/vps/VideoPlayerService;->l()V

    .line 11816
    invoke-direct {p0}, Lcom/facebook/video/vps/VideoPlayerService;->g()V

    .line 11817
    invoke-direct {p0}, Lcom/facebook/video/vps/VideoPlayerService;->n()V

    .line 11818
    invoke-direct {p0}, Lcom/facebook/video/vps/VideoPlayerService;->h()Ljava/util/Collection;

    move-result-object v0

    .line 11819
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Kx;

    .line 11820
    if-eqz v0, :cond_1

    .line 11821
    invoke-interface {v0}, LX/0Kx;->d()V

    goto :goto_1

    .line 11822
    :cond_2
    return-void
.end method

.method public static d(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11544
    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p1, v1, p0, v0}, Lcom/facebook/video/vps/VideoPlayerService;->b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 11545
    return-void
.end method

.method private e(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;
    .locals 2
    .param p1    # Lcom/facebook/exoplayer/ipc/VideoPlayerSession;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 11538
    if-nez p1, :cond_0

    .line 11539
    const/4 v0, 0x0

    .line 11540
    :goto_0
    return-object v0

    .line 11541
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->i:Ljava/util/HashMap;

    monitor-enter v1

    .line 11542
    :try_start_0
    invoke-direct {p0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->f(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 11543
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 11531
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->x:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 11532
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {p0, v0}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11533
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-direct {p0, v0}, Lcom/facebook/video/vps/VideoPlayerService;->q(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11534
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-direct {p0, v0}, Lcom/facebook/video/vps/VideoPlayerService;->g(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11535
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-direct {p0, v0}, Lcom/facebook/video/vps/VideoPlayerService;->s(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11536
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-direct {p0, v0}, Lcom/facebook/video/vps/VideoPlayerService;->l(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11537
    return-void
.end method

.method public static e(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 1

    .prologue
    .line 11529
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->b(Ljava/lang/Throwable;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11530
    return-void
.end method

.method public static f(Lcom/facebook/video/vps/VideoPlayerService;)LX/04m;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 11526
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    invoke-static {v0}, LX/040;->p(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11527
    sget-object v0, LX/0Gi;->a:LX/0OC;

    move-object v0, v0

    .line 11528
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 11525
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Kb;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 11522
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->i:Ljava/util/HashMap;

    monitor-enter v1

    .line 11523
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 11524
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private g(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 5

    .prologue
    .line 11519
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->i:Ljava/util/HashMap;

    monitor-enter v1

    .line 11520
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->i:Ljava/util/HashMap;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Ljava/util/HashMap;LX/0KR;[Ljava/lang/Object;)V

    .line 11521
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static h(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;
    .locals 3

    .prologue
    .line 11512
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->l:Ljava/util/HashMap;

    monitor-enter v1

    .line 11513
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;

    .line 11514
    if-nez v0, :cond_0

    .line 11515
    new-instance v0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;

    invoke-direct {v0}, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;-><init>()V

    .line 11516
    iget-object v2, p0, Lcom/facebook/video/vps/VideoPlayerService;->l:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11517
    :cond_0
    monitor-exit v1

    return-object v0

    .line 11518
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private h()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LX/0Kx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 11506
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 11507
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 11508
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 11509
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 11510
    monitor-exit v1

    return-object v2

    .line 11511
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private i()I
    .locals 2

    .prologue
    .line 11503
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 11504
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 11505
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static i(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 11500
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 11501
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Kx;

    monitor-exit v1

    return-object v0

    .line 11502
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static j(Lcom/facebook/video/vps/VideoPlayerService;)I
    .locals 2

    .prologue
    .line 11497
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    monitor-enter v1

    .line 11498
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 11499
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private j(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 11494
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 11495
    :try_start_0
    invoke-direct {p0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->k(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 11496
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private k(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kx;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 11493
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Kx;

    return-object v0
.end method

.method public static synthetic k(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 11492
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->c:Landroid/net/Uri;

    return-object v0
.end method

.method public static k(Lcom/facebook/video/vps/VideoPlayerService;)[Lcom/facebook/exoplayer/ipc/VideoPlayerSession;
    .locals 5

    .prologue
    .line 11484
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    monitor-enter v1

    .line 11485
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 11486
    new-array v2, v0, [Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 11487
    iget-object v3, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 11488
    iget-object v3, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 11489
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Clearing all "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " session(s) from the ToBeReleased list."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11490
    monitor-exit v1

    return-object v2

    .line 11491
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static synthetic l(Lcom/facebook/video/vps/VideoPlayerService;)I
    .locals 1

    .prologue
    .line 11483
    iget v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->s:I

    return v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 11480
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    monitor-enter v1

    .line 11481
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 11482
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private l(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 5

    .prologue
    .line 11584
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 11585
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    new-instance v2, LX/0KS;

    invoke-direct {v2, p0}, LX/0KS;-><init>(Lcom/facebook/video/vps/VideoPlayerService;)V

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Ljava/util/HashMap;LX/0KR;[Ljava/lang/Object;)V

    .line 11586
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private m()I
    .locals 2

    .prologue
    .line 11623
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    monitor-enter v1

    .line 11624
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 11625
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static synthetic m(Lcom/facebook/video/vps/VideoPlayerService;)I
    .locals 1

    .prologue
    .line 11622
    iget v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->t:I

    return v0
.end method

.method private m(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 3

    .prologue
    .line 11618
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    monitor-enter v1

    .line 11619
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Add session "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to the ToBeReleased list."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11620
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 11621
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 11615
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->o:Ljava/util/HashMap;

    monitor-enter v1

    .line 11616
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->o:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 11617
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static n(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 3

    .prologue
    .line 11611
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    monitor-enter v1

    .line 11612
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Remove session "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " from the ToBeReleased list."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 11613
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 11614
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static o(Lcom/facebook/video/vps/VideoPlayerService;)Landroid/os/Handler;
    .locals 2

    .prologue
    .line 11601
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->r:Landroid/os/Handler;

    .line 11602
    if-nez v0, :cond_1

    .line 11603
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 11604
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->r:Landroid/os/Handler;

    .line 11605
    if-nez v0, :cond_0

    .line 11606
    invoke-static {}, Lcom/facebook/video/vps/VideoPlayerService;->p()Landroid/os/Handler;

    move-result-object v0

    .line 11607
    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->r:Landroid/os/Handler;

    .line 11608
    :cond_0
    monitor-exit v1

    .line 11609
    :cond_1
    return-object v0

    .line 11610
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static o(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 5

    .prologue
    .line 11588
    iget-object v2, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    monitor-enter v2

    .line 11589
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v3

    .line 11590
    const/4 v0, 0x0

    .line 11591
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 11592
    if-ne v0, p1, :cond_0

    .line 11593
    const/4 v0, 0x1

    move v1, v0

    goto :goto_0

    .line 11594
    :cond_0
    invoke-static {p0, v0}, Lcom/facebook/video/vps/VideoPlayerService;->c$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto :goto_0

    .line 11595
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 11596
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 11597
    if-eqz v1, :cond_2

    .line 11598
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 11599
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Clearing "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " session(s) from the ToBeReleased list."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11600
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private static p()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 11587
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-object v0
.end method

.method public static p(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 3

    .prologue
    .line 11546
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    monitor-enter v1

    .line 11547
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->e:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 11548
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->f:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 11549
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private q(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 6

    .prologue
    .line 11578
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    monitor-enter v1

    .line 11579
    if-nez p1, :cond_0

    .line 11580
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-static {v0, v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Ljava/util/HashMap;LX/0KR;[Ljava/lang/Object;)V

    .line 11581
    :goto_0
    monitor-exit v1

    return-void

    .line 11582
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->j:Ljava/util/HashMap;

    const/4 v2, 0x0

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/facebook/exoplayer/ipc/MediaRenderer;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->e:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p1, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->f:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Ljava/util/HashMap;LX/0KR;[Ljava/lang/Object;)V

    goto :goto_0

    .line 11583
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static synthetic q(Lcom/facebook/video/vps/VideoPlayerService;)Z
    .locals 1

    .prologue
    .line 11576
    const/4 v0, 0x1

    move v0, v0

    .line 11577
    return v0
.end method

.method public static synthetic r(Lcom/facebook/video/vps/VideoPlayerService;)I
    .locals 1

    .prologue
    .line 11575
    invoke-direct {p0}, Lcom/facebook/video/vps/VideoPlayerService;->m()I

    move-result v0

    return v0
.end method

.method private r(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 11572
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->o:Ljava/util/HashMap;

    monitor-enter v1

    .line 11573
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    monitor-exit v1

    return-object v0

    .line 11574
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private s(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V
    .locals 5

    .prologue
    .line 11569
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->o:Ljava/util/HashMap;

    monitor-enter v1

    .line 11570
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->o:Ljava/util/HashMap;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v2, v3}, Lcom/facebook/video/vps/VideoPlayerService;->a(Ljava/util/HashMap;LX/0KR;[Ljava/lang/Object;)V

    .line 11571
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;
    .locals 2
    .param p1    # Lcom/facebook/exoplayer/ipc/VideoPlayerSession;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 11563
    if-nez p1, :cond_0

    .line 11564
    const/4 v0, 0x0

    .line 11565
    :goto_0
    return-object v0

    .line 11566
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->i:Ljava/util/HashMap;

    monitor-enter v1

    .line 11567
    :try_start_0
    invoke-direct {p0, p1}, Lcom/facebook/video/vps/VideoPlayerService;->d(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)LX/0Kb;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 11568
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    .line 11558
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bind by intent "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11559
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->e:LX/0K9;

    if-nez v0, :cond_0

    .line 11560
    new-instance v0, LX/043;

    invoke-direct {v0, p1}, LX/043;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->d:LX/043;

    .line 11561
    new-instance v0, LX/0K9;

    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->d:LX/043;

    new-instance v2, LX/0KQ;

    invoke-direct {v2, p0}, LX/0KQ;-><init>(Lcom/facebook/video/vps/VideoPlayerService;)V

    invoke-direct {v0, p0, v1, v2}, LX/0K9;-><init>(Landroid/content/Context;LX/043;LX/0KQ;)V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->e:LX/0K9;

    .line 11562
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService;->B:LX/0BS;

    return-object v0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x21604235

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 11556
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 11557
    const/16 v1, 0x25

    const v2, 0x68c339f0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, 0x6d43a516

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 11550
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 11551
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    if-eqz v1, :cond_0

    .line 11552
    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->x:Landroid/util/LruCache;

    iget-object v2, p0, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v3, p0, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v1, v2, v3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11553
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 11554
    :cond_0
    invoke-static {p0}, Lcom/facebook/video/vps/VideoPlayerService;->c(Lcom/facebook/video/vps/VideoPlayerService;)V

    .line 11555
    const/16 v1, 0x25

    const v2, 0x1cb71414

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
