.class public final Lcom/facebook/video/vps/VideoPlayerService$4;
.super Ljava/util/TimerTask;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/vps/VideoPlayerService;

.field private final b:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Lcom/facebook/video/vps/VideoPlayerService;)V
    .locals 2

    .prologue
    .line 41359
    iput-object p1, p0, Lcom/facebook/video/vps/VideoPlayerService$4;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 41360
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService$4;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 41361
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService$4;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v0, v0, Lcom/facebook/video/vps/VideoPlayerService;->A:Ljava/util/Map;

    .line 41362
    invoke-static {v0}, LX/040;->ah(Ljava/util/Map;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41363
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService$4;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, p0, Lcom/facebook/video/vps/VideoPlayerService$4;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v1, Lcom/facebook/video/vps/VideoPlayerService;->y:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {v0, v1}, Lcom/facebook/video/vps/VideoPlayerService;->o(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41364
    :cond_0
    return-void

    .line 41365
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService$4;->a:Lcom/facebook/video/vps/VideoPlayerService;

    invoke-static {v0}, Lcom/facebook/video/vps/VideoPlayerService;->k(Lcom/facebook/video/vps/VideoPlayerService;)[Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    move-result-object v1

    .line 41366
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 41367
    iget-object v4, p0, Lcom/facebook/video/vps/VideoPlayerService$4;->a:Lcom/facebook/video/vps/VideoPlayerService;

    .line 41368
    invoke-static {v4, v3}, Lcom/facebook/video/vps/VideoPlayerService;->c$redex0(Lcom/facebook/video/vps/VideoPlayerService;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    .line 41369
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 41370
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService$4;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41371
    :goto_0
    return-void

    .line 41372
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService$4;->a:Lcom/facebook/video/vps/VideoPlayerService;

    iget-object v1, v0, Lcom/facebook/video/vps/VideoPlayerService;->m:Ljava/util/HashMap;

    monitor-enter v1

    .line 41373
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/video/vps/VideoPlayerService$4;->a()V

    .line 41374
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41375
    iget-object v0, p0, Lcom/facebook/video/vps/VideoPlayerService$4;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 41376
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
