.class public Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;
.super Ljava/lang/Thread;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-java.lang.Thread.run",
        "BadMethodUse-java.lang.Thread.start"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;I)V
    .locals 0

    .prologue
    .line 22831
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 22832
    iput p2, p0, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;->a:I

    .line 22833
    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 22834
    invoke-direct {p0, p1, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 22835
    iput p3, p0, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;->a:I

    .line 22836
    return-void
.end method

.method public constructor <init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;JI)V
    .locals 0

    .prologue
    .line 22828
    invoke-direct/range {p0 .. p5}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;J)V

    .line 22829
    iput p6, p0, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;->a:I

    .line 22830
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/16 v4, 0xd

    const/4 v3, 0x1

    .line 22819
    const/16 v0, 0xc

    iget v1, p0, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;->a:I

    iget v2, p0, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;->b:I

    invoke-static {v3, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move-result v0

    .line 22820
    :try_start_0
    invoke-super {p0}, Ljava/lang/Thread;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22821
    iget v1, p0, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;->a:I

    invoke-static {v3, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 22822
    return-void

    .line 22823
    :catchall_0
    move-exception v1

    iget v2, p0, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;->a:I

    invoke-static {v3, v4, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1
.end method

.method public final declared-synchronized start()V
    .locals 3

    .prologue
    .line 22824
    monitor-enter p0

    const/4 v0, 0x1

    const/16 v1, 0xe

    :try_start_0
    iget v2, p0, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;->a:I

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/tools/dextr/runtime/detour/ThreadWrapper;->b:I

    .line 22825
    invoke-super {p0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22826
    monitor-exit p0

    return-void

    .line 22827
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
