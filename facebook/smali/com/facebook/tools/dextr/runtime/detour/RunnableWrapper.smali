.class public Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Ljava/lang/Runnable;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;II)V
    .locals 0

    .prologue
    .line 22786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22787
    iput-object p1, p0, Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;->a:Ljava/lang/Runnable;

    .line 22788
    iput p2, p0, Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;->c:I

    .line 22789
    iput p3, p0, Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;->b:I

    .line 22790
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/16 v4, 0xd

    const/4 v3, 0x1

    .line 22791
    const/16 v0, 0xc

    iget v1, p0, Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;->b:I

    iget v2, p0, Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;->c:I

    invoke-static {v3, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move-result v1

    .line 22792
    :try_start_0
    iget-object v0, p0, Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22793
    iget v0, p0, Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;->b:I

    invoke-static {v3, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 22794
    return-void

    .line 22795
    :catchall_0
    move-exception v0

    iget v2, p0, Lcom/facebook/tools/dextr/runtime/detour/RunnableWrapper;->b:I

    invoke-static {v3, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0
.end method
