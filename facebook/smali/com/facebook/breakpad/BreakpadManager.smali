.class public Lcom/facebook/breakpad/BreakpadManager;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/io/File;

.field private static c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2046
    const/4 v0, 0x0

    sput-boolean v0, Lcom/facebook/breakpad/BreakpadManager;->c:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()V
    .locals 2

    .prologue
    .line 2050
    const-class v0, Lcom/facebook/breakpad/BreakpadManager;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    sput-boolean v1, Lcom/facebook/breakpad/BreakpadManager;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2051
    monitor-exit v0

    return-void

    .line 2052
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 2053
    const v0, 0x177000

    invoke-static {p0, p1, v0}, Lcom/facebook/breakpad/BreakpadManager;->a(Landroid/content/Context;II)V

    .line 2054
    return-void
.end method

.method private static declared-synchronized a(Landroid/content/Context;II)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2055
    const-class v1, Lcom/facebook/breakpad/BreakpadManager;

    monitor-enter v1

    and-int/lit8 v2, p1, 0x2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    :try_start_0
    invoke-static {v0}, Lcom/facebook/breakpad/BreakpadManager;->a(Z)V

    .line 2056
    sget-object v0, Lcom/facebook/breakpad/BreakpadManager;->b:Ljava/io/File;

    if-nez v0, :cond_2

    .line 2057
    const-string v0, "minidumps"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 2058
    if-nez v0, :cond_1

    .line 2059
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Breakpad init failed to create crash directory: minidumps"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2060
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 2061
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    sget-boolean v3, Lcom/facebook/breakpad/BreakpadManager;->c:Z

    invoke-static {v2, v3, p2}, Lcom/facebook/breakpad/BreakpadManager;->install(Ljava/lang/String;ZI)V

    .line 2062
    sput-object v0, Lcom/facebook/breakpad/BreakpadManager;->b:Ljava/io/File;

    .line 2063
    invoke-static {}, Lcom/facebook/breakpad/BreakpadManager;->getMinidumpFlags()J

    move-result-wide v2

    const-wide/16 v4, 0x2

    or-long/2addr v2, v4

    const-wide/16 v4, 0x4

    or-long/2addr v2, v4

    invoke-static {v2, v3}, Lcom/facebook/breakpad/BreakpadManager;->setMinidumpFlags(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2064
    :cond_2
    monitor-exit v1

    return-void
.end method

.method private static a(Z)V
    .locals 1

    .prologue
    .line 2065
    sget-object v0, Lcom/facebook/breakpad/BreakpadManager;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2066
    if-eqz p0, :cond_1

    .line 2067
    const-string v0, "breakpad_static"

    .line 2068
    :goto_0
    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 2069
    sput-object v0, Lcom/facebook/breakpad/BreakpadManager;->a:Ljava/lang/String;

    .line 2070
    :cond_0
    return-void

    .line 2071
    :cond_1
    const-string v0, "breakpad"

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 2072
    invoke-static {}, Lcom/facebook/breakpad/BreakpadManager;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2073
    const/4 v0, 0x0

    .line 2074
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/breakpad/BreakpadManager;->enableCoreDumpingImpl(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2075
    sget-object v0, Lcom/facebook/breakpad/BreakpadManager;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2076
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Breakpad not installed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2077
    :cond_0
    sget-object v0, Lcom/facebook/breakpad/BreakpadManager;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 2078
    sget-object v0, Lcom/facebook/breakpad/BreakpadManager;->b:Ljava/io/File;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static native crashThisProcess()V
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 2047
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/facebook/breakpad/BreakpadManager;->a(Z)V

    .line 2048
    invoke-static {}, Lcom/facebook/breakpad/BreakpadManager;->disableCoreDumpingImpl()Z

    move-result v0

    return v0
.end method

.method private static native disableCoreDumpingImpl()Z
.end method

.method public static e()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2039
    invoke-static {v0}, Lcom/facebook/breakpad/BreakpadManager;->a(Z)V

    .line 2040
    invoke-static {}, Lcom/facebook/breakpad/BreakpadManager;->f()Ljava/io/File;

    move-result-object v1

    .line 2041
    if-nez v1, :cond_0

    .line 2042
    :goto_0
    return v0

    .line 2043
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->isAbsolute()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2044
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not write permissions into "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2045
    :cond_1
    invoke-static {}, Lcom/facebook/breakpad/BreakpadManager;->isCoreResouceHardUnlimited()Z

    move-result v0

    goto :goto_0
.end method

.method private static native enableCoreDumpingImpl(Ljava/lang/String;)Z
.end method

.method private static f()Ljava/io/File;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2027
    new-instance v1, Ljava/io/File;

    const-string v2, "/proc/sys/kernel/core_pattern"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2028
    :try_start_0
    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2029
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 2030
    :try_start_1
    new-instance v1, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 2031
    :goto_0
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2032
    :goto_1
    return-object v0

    .line 2033
    :catch_0
    move-exception v1

    .line 2034
    const-string v2, "BreakpadManager"

    const-string v3, "Core pattern file not found"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 2035
    :catch_1
    move-exception v1

    .line 2036
    const-string v2, "BreakpadManager"

    const-string v4, "There was a problem reading core pattern file"

    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 2037
    :catch_2
    move-exception v1

    .line 2038
    const-string v2, "BreakpadManager"

    const-string v3, "There was a problem closing core pattern file"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static native getMinidumpFlags()J
.end method

.method private static native install(Ljava/lang/String;ZI)V
.end method

.method private static native isCoreResouceHardUnlimited()Z
.end method

.method private static native nativeSetJvmStreamEnabled(ZZ)Z
.end method

.method public static native setMinidumpFlags(J)V
.end method

.method public static native uninstall()V
.end method
