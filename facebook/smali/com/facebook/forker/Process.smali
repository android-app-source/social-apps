.class public final Lcom/facebook/forker/Process;
.super Ljava/lang/Process;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final FD_STREAM_INPUT:I = 0x0

.field private static final FD_STREAM_OUTPUT:I = 0x1

.field public static final SD_DEVNULL:I = -0x3

.field public static final SD_INHERIT:I = -0x2

.field public static final SD_PIPE:I = -0x4

.field public static final SD_STDOUT:I = -0x5

.field public static final SIGCONT:I = 0x12

.field public static final SIGKILL:I = 0x9

.field public static final SIGSTOP:I = 0x13

.field public static final SIGTERM:I = 0xf

.field public static final SIGTSTP:I = 0x14

.field public static final STATUS_EXITED:I = 0x4

.field public static final STATUS_RUNNING:I = 0x1

.field public static final STATUS_STOPPED:I = 0x2

.field public static final STDERR:I = 0x2

.field public static final STDIN:I = 0x0

.field public static final STDOUT:I = 0x1

.field public static final WAIT_RESULT_RUNNING:I = -0x7ffffffe

.field public static final WAIT_RESULT_STOPPED:I = -0x7fffffff

.field public static final WAIT_RESULT_TIMEOUT:I = -0x80000000


# instance fields
.field private mChildStderr:Ljava/io/InputStream;

.field private mChildStdin:Ljava/io/OutputStream;

.field private mChildStdout:Ljava/io/InputStream;

.field private mExitStatus:I

.field private mPid:I

.field private mProcessStatus:I

.field private mWaiterThread:Lcom/facebook/forker/Process$WaiterThread;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25481
    const-string v0, "forker"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 25482
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Ljava/lang/String;[B[I[I)V
    .locals 9
    .param p3    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x6

    const/4 v8, -0x1

    const/4 v2, 0x0

    .line 25421
    invoke-direct {p0}, Ljava/lang/Process;-><init>()V

    .line 25422
    const/4 v1, 0x0

    .line 25423
    iput v8, p0, Lcom/facebook/forker/Process;->mPid:I

    .line 25424
    new-instance v0, Lcom/facebook/forker/Process$WaiterThread;

    invoke-direct {v0, p0}, Lcom/facebook/forker/Process$WaiterThread;-><init>(Lcom/facebook/forker/Process;)V

    iput-object v0, p0, Lcom/facebook/forker/Process;->mWaiterThread:Lcom/facebook/forker/Process$WaiterThread;

    .line 25425
    const/4 v0, 0x6

    :try_start_0
    new-array v7, v0, [I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move v0, v2

    .line 25426
    :goto_0
    if-ge v0, v5, :cond_0

    .line 25427
    const/4 v1, -0x1

    :try_start_1
    aput v1, v7, v0

    .line 25428
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v2

    .line 25429
    :goto_1
    if-ge v0, v5, :cond_3

    .line 25430
    const/4 v1, 0x2

    new-array v1, v1, [I

    invoke-static {v1}, Lcom/facebook/forker/Process;->nativeUnixPipe([I)[I

    move-result-object v1

    .line 25431
    if-nez v0, :cond_1

    .line 25432
    const/4 v3, 0x0

    const/4 v4, 0x1

    aget v4, v1, v4

    aput v4, v7, v3

    .line 25433
    const/4 v3, 0x1

    const/4 v4, 0x0

    aget v1, v1, v4

    aput v1, v7, v3

    .line 25434
    :goto_2
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 25435
    :cond_1
    add-int/lit8 v3, v0, 0x0

    const/4 v4, 0x0

    aget v4, v1, v4

    aput v4, v7, v3

    .line 25436
    add-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    aget v1, v1, v4

    aput v1, v7, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 25437
    :catchall_0
    move-exception v0

    move-object v1, v7

    move v6, v8

    :goto_3
    invoke-static {v6}, Lcom/facebook/forker/Process;->unixClose(I)V

    .line 25438
    invoke-static {v1}, Lcom/facebook/forker/Process;->unixClose([I)V

    .line 25439
    iget-object v1, p0, Lcom/facebook/forker/Process;->mChildStdin:Ljava/io/OutputStream;

    invoke-static {v1}, Lcom/facebook/forker/Process;->safeClose(Ljava/io/Closeable;)V

    .line 25440
    iget-object v1, p0, Lcom/facebook/forker/Process;->mChildStdout:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/facebook/forker/Process;->safeClose(Ljava/io/Closeable;)V

    .line 25441
    iget-object v1, p0, Lcom/facebook/forker/Process;->mChildStderr:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/facebook/forker/Process;->safeClose(Ljava/io/Closeable;)V

    .line 25442
    iget v1, p0, Lcom/facebook/forker/Process;->mPid:I

    if-eq v1, v8, :cond_2

    .line 25443
    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lcom/facebook/forker/Process;->kill(I)V

    .line 25444
    invoke-direct {p0}, Lcom/facebook/forker/Process;->nativeWait()V

    :cond_2
    throw v0

    .line 25445
    :cond_3
    const/4 v0, 0x6

    :try_start_2
    new-array v4, v0, [I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v1, v2

    move v6, v8

    .line 25446
    :goto_4
    const/4 v0, 0x3

    if-ge v1, v0, :cond_5

    .line 25447
    :try_start_3
    aget v0, p5, v1

    packed-switch v0, :pswitch_data_0

    .line 25448
    aget v0, p5, v1

    if-gez v0, :cond_4

    .line 25449
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "illegal stream disposition %s for fd %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aget v5, p5, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 25450
    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_3

    :pswitch_0
    move v0, v1

    .line 25451
    :goto_5
    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v3, v3, 0x0

    aput v0, v4, v3

    .line 25452
    mul-int/lit8 v0, v1, 0x2

    add-int/lit8 v0, v0, 0x1

    aput v1, v4, v0

    .line 25453
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 25454
    :pswitch_1
    if-ne v6, v8, :cond_8

    .line 25455
    const-string v0, "/dev/null"

    invoke-static {v0}, Lcom/facebook/forker/Process;->nativeUnixOpen(Ljava/lang/String;)I

    move-result v0

    :goto_6
    move v6, v0

    .line 25456
    goto :goto_5

    .line 25457
    :pswitch_2
    mul-int/lit8 v0, v1, 0x2

    add-int/lit8 v0, v0, 0x1

    aget v0, v7, v0

    goto :goto_5

    .line 25458
    :cond_4
    aget v0, p5, v1

    goto :goto_5

    :cond_5
    move v0, v2

    .line 25459
    :goto_7
    const/4 v1, 0x3

    if-ge v0, v1, :cond_7

    .line 25460
    aget v1, p5, v0

    const/4 v2, -0x5

    if-ne v1, v2, :cond_6

    .line 25461
    mul-int/lit8 v1, v0, 0x2

    add-int/lit8 v1, v1, 0x0

    const/4 v2, 0x2

    aget v2, v4, v2

    aput v2, v4, v1

    .line 25462
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_7
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    .line 25463
    invoke-direct/range {v0 .. v5}, Lcom/facebook/forker/Process;->nativeLaunch(Ljava/lang/String;[Ljava/lang/String;[B[I[I)I

    move-result v0

    iput v0, p0, Lcom/facebook/forker/Process;->mPid:I

    .line 25464
    iget-object v0, p0, Lcom/facebook/forker/Process;->mWaiterThread:Lcom/facebook/forker/Process$WaiterThread;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PidWaiter:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/forker/Process;->mPid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/forker/Process$WaiterThread;->setName(Ljava/lang/String;)V

    .line 25465
    const/4 v0, 0x0

    aget v0, v7, v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/forker/Process;->openFdStream(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    iput-object v0, p0, Lcom/facebook/forker/Process;->mChildStdin:Ljava/io/OutputStream;

    .line 25466
    const/4 v0, 0x2

    aget v0, v7, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/forker/Process;->openFdStream(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, Lcom/facebook/forker/Process;->mChildStdout:Ljava/io/InputStream;

    .line 25467
    const/4 v0, 0x4

    aget v0, v7, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/forker/Process;->openFdStream(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, Lcom/facebook/forker/Process;->mChildStderr:Ljava/io/InputStream;

    .line 25468
    iget-object v0, p0, Lcom/facebook/forker/Process;->mWaiterThread:Lcom/facebook/forker/Process$WaiterThread;

    invoke-virtual {v0}, Lcom/facebook/forker/Process$WaiterThread;->start()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 25469
    invoke-static {v6}, Lcom/facebook/forker/Process;->unixClose(I)V

    .line 25470
    invoke-static {v7}, Lcom/facebook/forker/Process;->unixClose([I)V

    .line 25471
    return-void

    .line 25472
    :catchall_2
    move-exception v0

    move v6, v8

    goto/16 :goto_3

    :cond_8
    move v0, v6

    goto :goto_6

    :pswitch_data_0
    .packed-switch -0x5
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static synthetic access$000(Lcom/facebook/forker/Process;)V
    .locals 0

    .prologue
    .line 25473
    invoke-direct {p0}, Lcom/facebook/forker/Process;->nativeWait()V

    return-void
.end method

.method public static describeStatus(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 25474
    if-gez p0, :cond_0

    .line 25475
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "killed by signal "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    neg-int v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 25476
    :goto_0
    return-object v0

    .line 25477
    :cond_0
    if-lez p0, :cond_1

    .line 25478
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "exited with status "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 25479
    :cond_1
    const-string v0, "exited successfully"

    goto :goto_0
.end method

.method private static fdMagicName(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 25480
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "/proc/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/task/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/fd/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private native nativeKill(I)V
.end method

.method private native nativeLaunch(Ljava/lang/String;[Ljava/lang/String;[B[I[I)I
    .param p3    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method private static native nativeUnixClose(I)V
.end method

.method private static native nativeUnixOpen(Ljava/lang/String;)I
.end method

.method private static native nativeUnixPipe([I)[I
.end method

.method private native nativeWait()V
.end method

.method private static openFdStream(II)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 25483
    invoke-static {p0}, Lcom/facebook/forker/Process;->fdMagicName(I)Ljava/lang/String;

    move-result-object v1

    .line 25484
    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 25485
    :goto_0
    return-object v0

    .line 25486
    :cond_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 25487
    :catch_0
    invoke-static {p0}, Landroid/os/ParcelFileDescriptor;->fromFd(I)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 25488
    if-nez p1, :cond_1

    new-instance v0, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    invoke-direct {v0, v1}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    invoke-direct {v0, v1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method

.method private static safeClose(Ljava/io/Closeable;)V
    .locals 1
    .param p0    # Ljava/io/Closeable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 25489
    if-eqz p0, :cond_0

    .line 25490
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25491
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private static unixClose(I)V
    .locals 1

    .prologue
    .line 25418
    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    .line 25419
    invoke-static {p0}, Lcom/facebook/forker/Process;->nativeUnixClose(I)V

    .line 25420
    :cond_0
    return-void
.end method

.method private static unixClose([I)V
    .locals 2
    .param p0    # [I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 25413
    if-eqz p0, :cond_0

    .line 25414
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 25415
    aget v1, p0, v0

    invoke-static {v1}, Lcom/facebook/forker/Process;->unixClose(I)V

    .line 25416
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 25417
    :cond_0
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 0

    .prologue
    .line 25411
    invoke-virtual {p0}, Lcom/facebook/forker/Process;->destroy()V

    .line 25412
    return-void
.end method

.method public final destroy()V
    .locals 2

    .prologue
    .line 25397
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/facebook/forker/Process;->kill(I)V

    .line 25398
    const/4 v0, 0x0

    .line 25399
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/facebook/forker/Process;->mWaiterThread:Lcom/facebook/forker/Process$WaiterThread;

    invoke-virtual {v1}, Lcom/facebook/forker/Process$WaiterThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25400
    monitor-enter p0

    .line 25401
    :try_start_1
    iget-object v1, p0, Lcom/facebook/forker/Process;->mChildStdin:Ljava/io/OutputStream;

    invoke-static {v1}, Lcom/facebook/forker/Process;->safeClose(Ljava/io/Closeable;)V

    .line 25402
    iget-object v1, p0, Lcom/facebook/forker/Process;->mChildStdout:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/facebook/forker/Process;->safeClose(Ljava/io/Closeable;)V

    .line 25403
    iget-object v1, p0, Lcom/facebook/forker/Process;->mChildStderr:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/facebook/forker/Process;->safeClose(Ljava/io/Closeable;)V

    .line 25404
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 25405
    if-eqz v0, :cond_0

    .line 25406
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 25407
    :cond_0
    return-void

    .line 25408
    :catch_0
    const/4 v0, 0x1

    .line 25409
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    goto :goto_0

    .line 25410
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public final exitValue()I
    .locals 1

    .prologue
    .line 25395
    invoke-virtual {p0}, Lcom/facebook/forker/Process;->exitValueEx()I

    move-result v0

    .line 25396
    if-gez v0, :cond_0

    neg-int v0, v0

    add-int/lit16 v0, v0, 0x80

    :cond_0
    return v0
.end method

.method public final declared-synchronized exitValueEx()I
    .locals 3

    .prologue
    .line 25333
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/facebook/forker/Process;->mProcessStatus:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 25334
    new-instance v0, Ljava/lang/IllegalThreadStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Process has not yet terminated: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/forker/Process;->mPid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25335
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 25336
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/facebook/forker/Process;->mExitStatus:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v0
.end method

.method public final getErrorStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 25394
    iget-object v0, p0, Lcom/facebook/forker/Process;->mChildStderr:Ljava/io/InputStream;

    return-object v0
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 25393
    iget-object v0, p0, Lcom/facebook/forker/Process;->mChildStdout:Ljava/io/InputStream;

    return-object v0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 25392
    iget-object v0, p0, Lcom/facebook/forker/Process;->mChildStdin:Ljava/io/OutputStream;

    return-object v0
.end method

.method public final getPid()I
    .locals 1

    .prologue
    .line 25391
    iget v0, p0, Lcom/facebook/forker/Process;->mPid:I

    return v0
.end method

.method public final kill(I)V
    .locals 0

    .prologue
    .line 25389
    invoke-direct {p0, p1}, Lcom/facebook/forker/Process;->nativeKill(I)V

    .line 25390
    return-void
.end method

.method public final declared-synchronized waitFor()I
    .locals 2

    .prologue
    .line 25385
    monitor-enter p0

    :goto_0
    :try_start_0
    iget v0, p0, Lcom/facebook/forker/Process;->mProcessStatus:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 25386
    const v0, -0x368ddda3

    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 25387
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 25388
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/facebook/forker/Process;->exitValue()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    monitor-exit p0

    return v0
.end method

.method public final declared-synchronized waitFor(II)I
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 25362
    monitor-enter p0

    .line 25363
    :cond_0
    :goto_0
    :try_start_0
    iget v2, p0, Lcom/facebook/forker/Process;->mProcessStatus:I

    and-int/2addr v2, p2

    if-nez v2, :cond_3

    iget v2, p0, Lcom/facebook/forker/Process;->mProcessStatus:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_3

    if-eqz p1, :cond_3

    .line 25364
    if-lez p1, :cond_1

    .line 25365
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 25366
    int-to-long v2, p1

    const v4, 0x3510423

    invoke-static {p0, v2, v3, v4}, LX/02L;->a(Ljava/lang/Object;JI)V

    .line 25367
    :goto_1
    if-lez p1, :cond_0

    .line 25368
    const-wide/16 v2, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 25369
    int-to-long v4, p1

    cmp-long v4, v4, v2

    if-gez v4, :cond_2

    .line 25370
    const/4 p1, 0x0

    goto :goto_0

    .line 25371
    :cond_1
    const v2, -0x39c9b1

    invoke-static {p0, v2}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 25372
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 25373
    :cond_2
    long-to-int v2, v2

    sub-int/2addr p1, v2

    .line 25374
    goto :goto_0

    .line 25375
    :cond_3
    :try_start_1
    iget v0, p0, Lcom/facebook/forker/Process;->mProcessStatus:I

    and-int/2addr v0, p2

    if-eqz v0, :cond_4

    .line 25376
    iget v0, p0, Lcom/facebook/forker/Process;->mProcessStatus:I

    packed-switch v0, :pswitch_data_0

    .line 25377
    :pswitch_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 25378
    :pswitch_1
    const v0, -0x7ffffffe

    .line 25379
    :goto_2
    monitor-exit p0

    return v0

    .line 25380
    :pswitch_2
    const v0, -0x7fffffff

    goto :goto_2

    .line 25381
    :pswitch_3
    iget v0, p0, Lcom/facebook/forker/Process;->mExitStatus:I

    goto :goto_2

    .line 25382
    :cond_4
    if-nez p1, :cond_5

    .line 25383
    const/high16 v0, -0x80000000

    goto :goto_2

    .line 25384
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "process entered unexpected state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/forker/Process;->mProcessStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final waitForUninterruptibly()I
    .locals 2

    .prologue
    .line 25355
    const/4 v0, 0x0

    .line 25356
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/forker/Process;->waitFor()I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 25357
    if-eqz v0, :cond_0

    .line 25358
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 25359
    :cond_0
    return v1

    .line 25360
    :catch_0
    const/4 v0, 0x1

    .line 25361
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    goto :goto_0
.end method

.method public final waitForUninterruptibly(II)I
    .locals 10

    .prologue
    .line 25337
    const/4 v1, 0x0

    .line 25338
    const/high16 v0, -0x80000000

    .line 25339
    const-wide/16 v4, 0x0

    move v2, p1

    .line 25340
    :cond_0
    if-lez v2, :cond_1

    .line 25341
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 25342
    :cond_1
    :try_start_0
    invoke-virtual {p0, v2, p2}, Lcom/facebook/forker/Process;->waitFor(II)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 25343
    const/high16 v3, -0x80000000

    if-eq v0, v3, :cond_3

    .line 25344
    :goto_0
    if-eqz v1, :cond_2

    .line 25345
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 25346
    :cond_2
    return v0

    .line 25347
    :catch_0
    const/4 v1, 0x1

    .line 25348
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 25349
    :cond_3
    if-lez v2, :cond_4

    .line 25350
    const-wide/16 v6, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 25351
    int-to-long v8, v2

    cmp-long v3, v8, v6

    if-gez v3, :cond_5

    .line 25352
    const/4 v2, 0x0

    .line 25353
    :cond_4
    :goto_1
    if-nez v2, :cond_0

    goto :goto_0

    .line 25354
    :cond_5
    long-to-int v3, v6

    sub-int/2addr v2, v3

    goto :goto_1
.end method
