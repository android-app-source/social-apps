.class public final Lcom/facebook/browser/lite/bugreport/RageShakeListenerFragment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0DO;


# direct methods
.method public constructor <init>(LX/0DO;)V
    .locals 0

    .prologue
    .line 29917
    iput-object p1, p0, Lcom/facebook/browser/lite/bugreport/RageShakeListenerFragment$1;->a:LX/0DO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 29918
    iget-object v0, p0, Lcom/facebook/browser/lite/bugreport/RageShakeListenerFragment$1;->a:LX/0DO;

    invoke-virtual {v0}, LX/0DO;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v2, 0x0

    .line 29919
    const/4 v1, 0x0

    .line 29920
    new-instance v3, Ljava/io/File;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    const-string v5, "browser_lite"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 29921
    :try_start_0
    invoke-static {v3}, LX/04M;->a(Ljava/io/File;)V
    :try_end_0
    .catch LX/0Fu; {:try_start_0 .. :try_end_0} :catch_1

    .line 29922
    :goto_0
    move-object v4, v3

    .line 29923
    if-nez v4, :cond_5

    .line 29924
    :goto_1
    move-object v4, v1

    .line 29925
    if-nez v4, :cond_1

    move-object v1, v2

    .line 29926
    :goto_2
    move-object v0, v1

    .line 29927
    if-eqz v0, :cond_0

    .line 29928
    invoke-static {}, LX/0CQ;->a()LX/0CQ;

    move-result-object v1

    .line 29929
    iget-object v2, v1, LX/0CQ;->d:LX/0DT;

    if-eqz v2, :cond_0

    .line 29930
    :try_start_1
    iget-object v2, v1, LX/0CQ;->d:LX/0DT;

    invoke-interface {v2, v0}, LX/0DT;->a(Ljava/util/Map;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_4

    .line 29931
    :cond_0
    :goto_3
    return-void

    .line 29932
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 29933
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_3

    move-object v1, v0

    .line 29934
    check-cast v1, Landroid/app/Activity;

    .line 29935
    invoke-virtual {v1}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 29936
    invoke-virtual {v1}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object v1

    .line 29937
    :cond_2
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    .line 29938
    const/4 v6, 0x0

    .line 29939
    :try_start_2
    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v5

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v5

    .line 29940
    :try_start_3
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 29941
    invoke-virtual {v7, v6}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 29942
    :goto_4
    move-object v1, v5

    .line 29943
    if-eqz v1, :cond_3

    .line 29944
    invoke-static {v1, v4}, LX/0DL;->a(Landroid/graphics/Bitmap;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 29945
    const-string v4, "screenshot_uri"

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29946
    :cond_3
    const-string v1, "raw_view_description_file_uri"

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29947
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 29948
    instance-of v2, v0, Landroid/app/Activity;

    if-eqz v2, :cond_4

    .line 29949
    check-cast v0, Landroid/app/Activity;

    .line 29950
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 29951
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 29952
    const-string v4, "intent_extras"

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29953
    :cond_4
    move-object v1, v1

    .line 29954
    const-string v2, "debug_info_map"

    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v3

    .line 29955
    goto/16 :goto_2

    .line 29956
    :cond_5
    new-instance v3, Ljava/io/File;

    const-string v5, "bugreport"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 29957
    :try_start_4
    invoke-static {v3}, LX/04M;->a(Ljava/io/File;)V
    :try_end_4
    .catch LX/0Fu; {:try_start_4 .. :try_end_4} :catch_0

    move-object v1, v3

    .line 29958
    goto/16 :goto_1

    .line 29959
    :catch_0
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v5

    goto/16 :goto_1

    .line 29960
    :catch_1
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v5

    .line 29961
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 29962
    :catch_2
    move-exception v5

    move-object p0, v5

    move-object v5, v6

    move-object v6, p0

    .line 29963
    :goto_5
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v7, v8

    goto :goto_4

    .line 29964
    :catch_3
    move-exception v6

    goto :goto_5

    :catch_4
    goto/16 :goto_3
.end method
