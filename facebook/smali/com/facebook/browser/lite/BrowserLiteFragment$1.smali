.class public final Lcom/facebook/browser/lite/BrowserLiteFragment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/content/Intent;

.field public final synthetic b:Lcom/facebook/browser/lite/BrowserLiteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/BrowserLiteFragment;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 28026
    iput-object p1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$1;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iput-object p2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$1;->a:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 28027
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$1;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$1;->a:Landroid/content/Intent;

    const-string v2, "OFFERS_BUNDLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 28028
    const-string v2, "IS_OMNI_CHANNEL"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 28029
    iget-object v3, v0, LX/0Dw;->r:Landroid/view/View;

    invoke-static {v0, v3}, LX/0Dw;->a(LX/0Dw;Landroid/view/View;)V

    .line 28030
    iget-object p0, v0, LX/0Dw;->r:Landroid/view/View;

    if-eqz v2, :cond_1

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 28031
    const-string v2, "CLAIM_STATUS"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "UNIQUE_CODE"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 28032
    const-string p0, "claim_failed"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 28033
    iget-object p0, v0, LX/0Dw;->k:Landroid/widget/TextView;

    iget-object v1, v0, LX/0Dw;->n:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28034
    iget-object p0, v0, LX/0Dw;->l:Landroid/widget/TextView;

    sget v1, LX/0E7;->r:I

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 28035
    iget-object p0, v0, LX/0Dw;->g:Landroid/view/View;

    invoke-static {v0, p0}, LX/0Dw;->a(LX/0Dw;Landroid/view/View;)V

    .line 28036
    invoke-static {v0}, LX/0Dw;->j(LX/0Dw;)V

    .line 28037
    :cond_0
    :goto_1
    return-void

    .line 28038
    :cond_1
    const/16 v3, 0x8

    goto :goto_0

    .line 28039
    :cond_2
    const-string p0, "claim_limit_hit"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 28040
    iget-object p0, v0, LX/0Dw;->k:Landroid/widget/TextView;

    iget-object v1, v0, LX/0Dw;->n:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28041
    iget-object p0, v0, LX/0Dw;->l:Landroid/widget/TextView;

    sget v1, LX/0E7;->q:I

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 28042
    iget-object p0, v0, LX/0Dw;->g:Landroid/view/View;

    invoke-static {v0, p0}, LX/0Dw;->a(LX/0Dw;Landroid/view/View;)V

    .line 28043
    invoke-static {v0}, LX/0Dw;->j(LX/0Dw;)V

    goto :goto_1

    .line 28044
    :cond_3
    const-string p0, "expired"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 28045
    iget-object p0, v0, LX/0Dw;->k:Landroid/widget/TextView;

    iget-object v1, v0, LX/0Dw;->n:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28046
    iget-object p0, v0, LX/0Dw;->l:Landroid/widget/TextView;

    sget v1, LX/0E7;->p:I

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 28047
    iget-object p0, v0, LX/0Dw;->g:Landroid/view/View;

    invoke-static {v0, p0}, LX/0Dw;->a(LX/0Dw;Landroid/view/View;)V

    .line 28048
    invoke-static {v0}, LX/0Dw;->j(LX/0Dw;)V

    goto :goto_1

    .line 28049
    :cond_4
    const-string p0, "unique_code_success"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_0

    .line 28050
    invoke-static {v0, v3}, LX/0Dw;->a(LX/0Dw;Ljava/lang/String;)V

    .line 28051
    invoke-static {v0}, LX/0Dw;->i(LX/0Dw;)V

    goto :goto_1
.end method
