.class public Lcom/facebook/browser/lite/widget/QuoteBar;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field public b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 31957
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 31958
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 31955
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31956
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31953
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31954
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 31947
    sget v0, LX/0E9;->c:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/QuoteBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/browser/lite/widget/QuoteBar;->a:Landroid/widget/TextView;

    .line 31948
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/QuoteBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, LX/0E0;->a:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v0, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 31949
    sget v0, LX/0E9;->d:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/QuoteBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 31950
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 31951
    sget v0, LX/0E9;->e:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/QuoteBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/widget/QuoteBar;->b:Landroid/view/View;

    .line 31952
    return-void
.end method


# virtual methods
.method public getActionButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 31946
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/QuoteBar;->b:Landroid/view/View;

    return-object v0
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4731a31f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 31943
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 31944
    invoke-direct {p0}, Lcom/facebook/browser/lite/widget/QuoteBar;->a()V

    .line 31945
    const/16 v1, 0x2d

    const v2, 0x4b8f78f6    # 1.8805228E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setQuoteText(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 31941
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/QuoteBar;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/QuoteBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LX/0E9;->f:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31942
    return-void
.end method
