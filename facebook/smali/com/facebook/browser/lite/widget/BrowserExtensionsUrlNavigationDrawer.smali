.class public Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;
.super Landroid/widget/FrameLayout;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field private d:Z

.field public e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field public j:Landroid/view/View;

.field public k:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31622
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31623
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31624
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31625
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31626
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31627
    invoke-direct {p0, p1, p2}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31628
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 31642
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 31643
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    div-int/lit16 v0, v0, 0xa0

    mul-int/2addr v0, p1

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 31629
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget-object v3, LX/0E2;->n:[I

    invoke-virtual {v2, p2, v3, v0, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 31630
    sget v3, LX/0E2;->o:I

    invoke-virtual {v2, v3, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    .line 31631
    iput-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a:Z

    .line 31632
    iput-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->b:Z

    .line 31633
    if-ne v3, v1, :cond_0

    move v0, v1

    :cond_0
    iput-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->c:Z

    .line 31634
    iput-boolean v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->d:Z

    .line 31635
    const/16 v0, 0x96

    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->e:I

    .line 31636
    invoke-direct {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->c()V

    .line 31637
    invoke-direct {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->d()V

    .line 31638
    invoke-direct {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->e()V

    .line 31639
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 31640
    invoke-virtual {p0, v1}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->setFocusableInTouchMode(Z)V

    .line 31641
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 31644
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 31645
    sget v1, LX/0E2;->q:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->f:I

    .line 31646
    sget v1, LX/0E2;->p:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->g:I

    .line 31647
    sget v1, LX/0E2;->s:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->h:I

    .line 31648
    sget v1, LX/0E2;->r:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->i:I

    .line 31649
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 31592
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 31593
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->j:Landroid/view/View;

    .line 31594
    iget-object v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->j:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 31595
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->j:Landroid/view/View;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->h:I

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 31596
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->j:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 31597
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->j:Landroid/view/View;

    new-instance v1, LX/0ED;

    invoke-direct {v1, p0}, LX/0ED;-><init>(Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31598
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->j:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->addView(Landroid/view/View;)V

    .line 31599
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 31607
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 31608
    iget-boolean v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->c:Z

    if-eqz v1, :cond_0

    .line 31609
    iget v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->f:I

    invoke-virtual {v0, v3, v3, v1, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 31610
    :goto_0
    new-instance v1, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    .line 31611
    iget-object v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 31612
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    iget v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->g:I

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 31613
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->i:I

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 31614
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 31615
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    new-instance v1, LX/0EI;

    invoke-direct {v1, p0, p0}, LX/0EI;-><init>(Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 31616
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 31617
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->addView(Landroid/view/View;)V

    .line 31618
    return-void

    .line 31619
    :cond_0
    iget v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->f:I

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0
.end method

.method public static getEffectiveContentViewWidth(Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)I
    .locals 2

    .prologue
    .line 31620
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->f:I

    invoke-direct {p0, v1}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 31621
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1c2

    const/4 v2, 0x0

    .line 31600
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getX()F

    move-result v0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 31601
    :goto_0
    return-void

    .line 31602
    :cond_0
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    .line 31603
    iget-object v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, LX/0EE;

    invoke-direct {v2, p0}, LX/0EE;-><init>(Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 31604
    iget-object v2, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->j:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v2, LX/0EF;

    invoke-direct {v2, p0}, LX/0EF;-><init>(Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 31605
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 31606
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1c2

    .line 31583
    iget-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a:Z

    if-nez v0, :cond_0

    .line 31584
    :goto_0
    return-void

    .line 31585
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->c:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 31586
    :goto_1
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    .line 31587
    iget-object v2, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v2, LX/0EG;

    invoke-direct {v2, p0}, LX/0EG;-><init>(Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 31588
    iget-object v2, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->j:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, LX/0EH;

    invoke-direct {v2, p0}, LX/0EH;-><init>(Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 31589
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 31590
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0

    .line 31591
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->getWidth()I

    move-result v0

    int-to-float v0, v0

    goto :goto_1
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 31580
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a:Z

    if-eqz v0, :cond_0

    .line 31581
    const/4 v0, 0x1

    .line 31582
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 31576
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a:Z

    if-eqz v0, :cond_0

    .line 31577
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->b()V

    .line 31578
    const/4 v0, 0x1

    .line 31579
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 31569
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 31570
    iget-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->d:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->a:Z

    if-nez v0, :cond_1

    .line 31571
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->c:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 31572
    :goto_0
    iget-object v1, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setX(F)V

    .line 31573
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->d:Z

    .line 31574
    :cond_1
    return-void

    .line 31575
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->getWidth()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 31567
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 31568
    return-void
.end method

.method public setOnItemClickedListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1

    .prologue
    .line 31565
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserExtensionsUrlNavigationDrawer;->k:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 31566
    return-void
.end method
