.class public Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView;
.super Landroid/widget/RelativeLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 31656
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31657
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 31658
    sget v1, LX/0E3;->b:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 31659
    return-void
.end method


# virtual methods
.method public setUpView(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 31660
    sget v0, LX/0E3;->c:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 31661
    const-string v1, "lead_gen_continued_flow_title"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31662
    sget v0, LX/0E3;->d:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 31663
    const-string v1, "lead_gen_continued_flow_text"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31664
    sget v0, LX/0E3;->e:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 31665
    new-instance v1, LX/0EJ;

    invoke-direct {v1, p0}, LX/0EJ;-><init>(Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView;)V

    .line 31666
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31667
    invoke-virtual {p0, v1}, Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31668
    new-instance v0, Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView$2;

    invoke-direct {v0, p0}, Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView$2;-><init>(Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView;)V

    const-wide/16 v2, 0xfa0

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 31669
    return-void
.end method
