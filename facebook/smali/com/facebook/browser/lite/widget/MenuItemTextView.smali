.class public Lcom/facebook/browser/lite/widget/MenuItemTextView;
.super Landroid/widget/LinearLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31893
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/browser/lite/widget/MenuItemTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31894
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31895
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/browser/lite/widget/MenuItemTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31896
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31897
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31898
    return-void
.end method


# virtual methods
.method public final a(LX/0EK;LX/0Cf;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 31899
    if-nez p1, :cond_0

    .line 31900
    :goto_0
    return-void

    .line 31901
    :cond_0
    const v0, 0x7f0d07cf

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/MenuItemTextView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 31902
    iget-object v2, p1, LX/0EK;->c:Ljava/lang/String;

    move-object v2, v2

    .line 31903
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31904
    iget v2, p1, LX/0EK;->d:I

    move v2, v2

    .line 31905
    if-lez v2, :cond_1

    .line 31906
    iget v2, p1, LX/0EK;->d:I

    move v2, v2

    .line 31907
    invoke-virtual {v0, v2, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 31908
    :cond_1
    new-instance v0, LX/0EU;

    invoke-direct {v0, p0, p2, p1}, LX/0EU;-><init>(Lcom/facebook/browser/lite/widget/MenuItemTextView;LX/0Cf;LX/0EK;)V

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/MenuItemTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31909
    const v0, 0x7f0d07d0

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/MenuItemTextView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz p3, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method
