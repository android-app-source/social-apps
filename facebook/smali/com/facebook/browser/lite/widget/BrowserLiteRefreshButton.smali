.class public Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;

.field private b:Landroid/graphics/drawable/Drawable;

.field public c:Z

.field public d:LX/0CT;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31823
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31824
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31821
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31822
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 31808
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31809
    sget v0, LX/0E1;->d:I

    if-nez v0, :cond_0

    .line 31810
    :goto_0
    return-void

    .line 31811
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, LX/0E1;->d:I

    invoke-static {v0, v1}, LX/0DR;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->a:Landroid/graphics/drawable/Drawable;

    .line 31812
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f020182

    invoke-static {v0, v1}, LX/0DR;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->b:Landroid/graphics/drawable/Drawable;

    .line 31813
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a062f

    invoke-static {v1, v2}, LX/0DR;->b(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 31814
    iget-object v1, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 31815
    iget-object v1, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 31816
    new-instance v0, LX/0EP;

    invoke-direct {v0, p0}, LX/0EP;-><init>(Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;)V

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31817
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081c8e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->e:Ljava/lang/String;

    .line 31818
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081c8f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->f:Ljava/lang/String;

    .line 31819
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 31820
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 31825
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 31826
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 31827
    return-void
.end method

.method public setOnClickListener(LX/0CT;)V
    .locals 0

    .prologue
    .line 31806
    iput-object p1, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->d:LX/0CT;

    .line 31807
    return-void
.end method

.method public setProgress(I)V
    .locals 1

    .prologue
    .line 31795
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 31796
    :cond_0
    :goto_0
    return-void

    .line 31797
    :cond_1
    const/16 v0, 0x64

    if-ne p1, v0, :cond_2

    .line 31798
    iget-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->c:Z

    if-nez v0, :cond_0

    .line 31799
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 31800
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 31801
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->c:Z

    goto :goto_0

    .line 31802
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->c:Z

    if-eqz v0, :cond_0

    .line 31803
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 31804
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 31805
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->c:Z

    goto :goto_0
.end method
