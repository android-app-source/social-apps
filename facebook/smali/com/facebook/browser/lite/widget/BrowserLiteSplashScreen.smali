.class public Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;
.super Landroid/widget/FrameLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 31859
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 31860
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 31857
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31858
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31861
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31862
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 31848
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 31849
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 31850
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 31851
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 31852
    new-instance v1, LX/0EQ;

    invoke-direct {v1, p0}, LX/0EQ;-><init>(Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 31853
    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->startAnimation(Landroid/view/animation/Animation;)V

    .line 31854
    :cond_0
    :goto_0
    return-void

    .line 31855
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 31856
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 31843
    packed-switch p1, :pswitch_data_0

    .line 31844
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31845
    :pswitch_0
    const v0, 0x7f0d07ec

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 31846
    :goto_0
    return-void

    .line 31847
    :pswitch_1
    const v0, 0x7f0d07ed

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/widget/ImageView;)V
    .locals 4

    .prologue
    .line 31837
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 31838
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 31839
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 31840
    new-instance v1, LX/0ER;

    invoke-direct {v1, p0}, LX/0ER;-><init>(Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 31841
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 31842
    return-void
.end method
