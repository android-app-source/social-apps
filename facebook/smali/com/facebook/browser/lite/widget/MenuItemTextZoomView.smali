.class public Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:Landroid/graphics/ColorFilter;

.field private b:Landroid/graphics/ColorFilter;

.field public c:LX/0EL;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31914
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31915
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31916
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31917
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 31918
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31919
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a063a

    invoke-static {v1, v2}, LX/0DR;->b(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v0, p0, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->a:Landroid/graphics/ColorFilter;

    .line 31920
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0631

    invoke-static {v1, v2}, LX/0DR;->b(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v0, p0, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->b:Landroid/graphics/ColorFilter;

    .line 31921
    return-void
.end method

.method private a(LX/0EK;Landroid/widget/ImageButton;LX/0Cf;)V
    .locals 2

    .prologue
    .line 31922
    iget-boolean v0, p1, LX/0EK;->e:Z

    move v0, v0

    .line 31923
    invoke-virtual {p2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 31924
    invoke-virtual {p2}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 31925
    iget-boolean v0, p1, LX/0EK;->e:Z

    move v0, v0

    .line 31926
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->b:Landroid/graphics/ColorFilter;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 31927
    iget-boolean v0, p1, LX/0EK;->e:Z

    move v0, v0

    .line 31928
    if-eqz v0, :cond_0

    .line 31929
    new-instance v0, LX/0EV;

    invoke-direct {v0, p0, p3, p1}, LX/0EV;-><init>(Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;LX/0Cf;LX/0EK;)V

    invoke-virtual {p2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31930
    :cond_0
    return-void

    .line 31931
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->a:Landroid/graphics/ColorFilter;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0EK;LX/0Cf;LX/0EL;Z)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 31932
    iput-object p3, p0, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->c:LX/0EL;

    .line 31933
    iget-object v0, p1, LX/0EK;->a:Ljava/util/ArrayList;

    move-object v3, v0

    .line 31934
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0EK;

    const v1, 0x7f0d07d3

    invoke-virtual {p0, v1}, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v1, p2}, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->a(LX/0EK;Landroid/widget/ImageButton;LX/0Cf;)V

    .line 31935
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0EK;

    const v1, 0x7f0d07d5

    invoke-virtual {p0, v1}, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v1, p2}, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->a(LX/0EK;Landroid/widget/ImageButton;LX/0Cf;)V

    .line 31936
    const v0, 0x7f0d07d0

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p4, :cond_0

    move v0, v2

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 31937
    const v0, 0x7f0d07d4

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 31938
    invoke-virtual {p0}, Lcom/facebook/browser/lite/widget/MenuItemTextZoomView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f081c8d

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {p2}, LX/0Cf;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31939
    return-void

    .line 31940
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
