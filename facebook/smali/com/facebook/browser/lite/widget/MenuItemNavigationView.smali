.class public Lcom/facebook/browser/lite/widget/MenuItemNavigationView;
.super Landroid/widget/LinearLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31888
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/browser/lite/widget/MenuItemNavigationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31889
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31886
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/browser/lite/widget/MenuItemNavigationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31887
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31872
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31873
    return-void
.end method

.method private a(LX/0EK;Landroid/widget/ImageButton;LX/0Cf;)V
    .locals 1

    .prologue
    .line 31880
    iget-boolean v0, p1, LX/0EK;->e:Z

    move v0, v0

    .line 31881
    invoke-virtual {p2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 31882
    iget-boolean v0, p1, LX/0EK;->e:Z

    move v0, v0

    .line 31883
    if-eqz v0, :cond_0

    .line 31884
    new-instance v0, LX/0ET;

    invoke-direct {v0, p0, p3, p1}, LX/0ET;-><init>(Lcom/facebook/browser/lite/widget/MenuItemNavigationView;LX/0Cf;LX/0EK;)V

    invoke-virtual {p2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31885
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0EK;LX/0Cf;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 31874
    iget-object v0, p1, LX/0EK;->a:Ljava/util/ArrayList;

    move-object v3, v0

    .line 31875
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0EK;

    const v1, 0x7f0d07d1

    invoke-virtual {p0, v1}, Lcom/facebook/browser/lite/widget/MenuItemNavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v1, p2}, Lcom/facebook/browser/lite/widget/MenuItemNavigationView;->a(LX/0EK;Landroid/widget/ImageButton;LX/0Cf;)V

    .line 31876
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0EK;

    const v1, 0x7f0d07d2

    invoke-virtual {p0, v1}, Lcom/facebook/browser/lite/widget/MenuItemNavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v1, p2}, Lcom/facebook/browser/lite/widget/MenuItemNavigationView;->a(LX/0EK;Landroid/widget/ImageButton;LX/0Cf;)V

    .line 31877
    const v0, 0x7f0d07d0

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/widget/MenuItemNavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p3, :cond_0

    move v0, v2

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 31878
    return-void

    .line 31879
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
