.class public Lcom/facebook/browser/lite/BrowserLiteIntentService;
.super Landroid/app/IntentService;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field public static a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26348
    const-class v0, Lcom/facebook/browser/lite/BrowserLiteIntentService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browser/lite/BrowserLiteIntentService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26374
    const-string v0, "BrowserLiteIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 26375
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteIntentService;->setIntentRedelivery(Z)V

    .line 26376
    return-void
.end method


# virtual methods
.method public final onHandleIntent(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 26349
    if-nez p1, :cond_1

    .line 26350
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 26351
    :cond_1
    invoke-static {}, LX/048;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 26352
    const/4 v0, 0x0

    .line 26353
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteIntentService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;

    move-result-object v4

    iget-object v0, v4, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26354
    :goto_1
    invoke-static {v0}, LX/0DQ;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 26355
    sput-boolean v2, LX/00v;->a:Z

    .line 26356
    :cond_2
    sput-boolean v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->a:Z

    .line 26357
    const-string v0, "EXTRA_ACTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 26358
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26359
    const-string v0, "BrowserLiteIntent.EXTRA_LOGCAT"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 26360
    sput-boolean v0, LX/0Dg;->a:Z

    .line 26361
    new-array v0, v2, [Ljava/lang/Object;

    aput-object v4, v0, v1

    .line 26362
    const/4 v0, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_3
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 26363
    invoke-static {}, LX/0D1;->a()LX/0D1;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0D1;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 26364
    :sswitch_0
    const-string v5, "ACTION_CLEAR_DATA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v1

    goto :goto_2

    :sswitch_1
    const-string v5, "ACTION_INJECT_COOKIES"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v2

    goto :goto_2

    :sswitch_2
    const-string v5, "ACTION_WARM_UP"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v3

    goto :goto_2

    :sswitch_3
    const-string v5, "ACTION_EXTRACT_HTML_RESOURCE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x3

    goto :goto_2

    .line 26365
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/048;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 26366
    :pswitch_2
    const-string v0, "BrowserLiteIntent.EXTRA_COOKIES"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 26367
    if-eqz v0, :cond_0

    .line 26368
    const-string v4, "EXTRA_FLUSH_COOKIES"

    invoke-virtual {p1, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 26369
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v3, v2

    .line 26370
    invoke-static {p0, v0, v4}, LX/048;->a(Landroid/content/Context;Ljava/util/Map;Z)V

    goto/16 :goto_0

    .line 26371
    :pswitch_3
    const-string v0, "BrowserLiteIntent.EXTRA_PREFETCH_INFO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    .line 26372
    if-eqz v0, :cond_0

    .line 26373
    invoke-static {p0}, LX/0Do;->a(Landroid/content/Context;)LX/0Do;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Do;->a(Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;)V

    goto/16 :goto_0

    :catch_0
    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x710ec3db -> :sswitch_0
        0x1023597b -> :sswitch_3
        0x47ee7aea -> :sswitch_1
        0x4b009d8c -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
