.class public Lcom/facebook/browser/lite/BrowserLitePreview;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/view/ViewStub;

.field private h:Landroid/view/ViewStub;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29210
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29211
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29208
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29209
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 29197
    sget v0, LX/0E8;->b:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLitePreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->a:Landroid/widget/TextView;

    .line 29198
    sget v0, LX/0E8;->c:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLitePreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->c:Landroid/widget/TextView;

    .line 29199
    sget v0, LX/0E8;->d:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLitePreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->d:Landroid/widget/TextView;

    .line 29200
    sget v0, LX/0E8;->e:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLitePreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->e:Landroid/widget/ImageView;

    .line 29201
    sget v0, LX/0E8;->f:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLitePreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->f:Landroid/widget/ImageView;

    .line 29202
    sget v0, LX/0E8;->g:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLitePreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->b:Landroid/widget/TextView;

    .line 29203
    sget v0, LX/0E8;->h:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLitePreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->g:Landroid/view/ViewStub;

    .line 29204
    sget v0, LX/0E8;->i:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLitePreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->h:Landroid/view/ViewStub;

    .line 29205
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLitePreview;->c()V

    .line 29206
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLitePreview;->d()V

    .line 29207
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 29194
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->g:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->i:Landroid/widget/ImageView;

    .line 29195
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 29196
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 29192
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->f:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLitePreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LX/0E8;->k:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 29193
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 29185
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLitePreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LX/0E8;->l:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29186
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLitePreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LX/0E8;->j:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 29187
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 29188
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 29189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->i:Landroid/widget/ImageView;

    .line 29190
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->h:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->j:Landroid/widget/ImageView;

    .line 29191
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 29212
    if-eqz p1, :cond_0

    .line 29213
    new-instance v0, LX/0Ci;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->f:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, LX/0Ci;-><init>(Landroid/widget/ImageView;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, LX/0Ci;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 29214
    :cond_0
    return-void
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1f8af24e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 29182
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 29183
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLitePreview;->b()V

    .line 29184
    const/16 v1, 0x2d

    const v2, 0x34954426

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setPreviewBodyMaxLines(I)V
    .locals 1

    .prologue
    .line 29180
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 29181
    return-void
.end method

.method public setPreviewCloseButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 29178
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29179
    return-void
.end method

.method public setPreviewLoadedStateListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 29175
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29176
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29177
    return-void
.end method

.method public setPreviewSubtitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29173
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29174
    return-void
.end method

.method public setPreviewText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29171
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29172
    return-void
.end method

.method public setPreviewTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29169
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLitePreview;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29170
    return-void
.end method
