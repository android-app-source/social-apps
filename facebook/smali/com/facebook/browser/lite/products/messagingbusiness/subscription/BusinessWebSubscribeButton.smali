.class public Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:LX/0Dq;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 31225
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31226
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31223
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31224
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 31216
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31217
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, LX/0E6;->b:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 31218
    sget v0, LX/0E6;->g:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->b:Landroid/widget/TextView;

    .line 31219
    sget v0, LX/0E6;->h:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->c:Landroid/widget/ImageView;

    .line 31220
    invoke-virtual {p0, p0}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31221
    sget-object v0, LX/0Du;->SUBSCRIBE:LX/0Du;

    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->a(LX/0Du;)V

    .line 31222
    return-void
.end method

.method private a(LX/0Du;)V
    .locals 0

    .prologue
    .line 31191
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->b(LX/0Du;)V

    .line 31192
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->c(LX/0Du;)V

    .line 31193
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->d(LX/0Du;)V

    .line 31194
    return-void
.end method

.method private b(LX/0Du;)V
    .locals 2

    .prologue
    .line 31211
    sget-object v0, LX/0Du;->SUBSCRIBE:LX/0Du;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    .line 31212
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->setEnabled(Z)V

    .line 31213
    iget-object v1, p0, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 31214
    return-void

    .line 31215
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LX/0Du;)V
    .locals 3

    .prologue
    .line 31208
    iget-object v1, p0, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->b:Landroid/widget/TextView;

    sget-object v0, LX/0Du;->SUBSCRIBE:LX/0Du;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, LX/0E6;->i:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 31209
    return-void

    .line 31210
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, LX/0E6;->j:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private d(LX/0Du;)V
    .locals 4

    .prologue
    .line 31201
    sget-object v0, LX/0Du;->SUBSCRIBE:LX/0Du;

    if-ne p1, v0, :cond_0

    sget v0, LX/0E6;->i:I

    .line 31202
    :goto_0
    sget-object v1, LX/0Du;->SUBSCRIBE:LX/0Du;

    if-ne p1, v1, :cond_1

    sget v1, LX/0E6;->k:I

    .line 31203
    :goto_1
    iget-object v2, p0, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->c:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 31204
    iget-object v1, p0, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->c:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 31205
    return-void

    .line 31206
    :cond_0
    sget v0, LX/0E6;->j:I

    goto :goto_0

    .line 31207
    :cond_1
    sget v1, LX/0E6;->l:I

    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x6408e822

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 31197
    sget-object v1, LX/0Du;->SUBSCRIBED:LX/0Du;

    invoke-direct {p0, v1}, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->a(LX/0Du;)V

    .line 31198
    iget-object v1, p0, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->a:LX/0Dq;

    if-eqz v1, :cond_0

    .line 31199
    iget-object v1, p0, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->a:LX/0Dq;

    invoke-interface {v1}, LX/0Dq;->a()V

    .line 31200
    :cond_0
    const v1, 0x7220438e

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setSubscribeButtonListener(LX/0Dq;)V
    .locals 0

    .prologue
    .line 31195
    iput-object p1, p0, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->a:LX/0Dq;

    .line 31196
    return-void
.end method
