.class public final Lcom/facebook/browser/lite/BrowserLiteFragment$WebViewSelectionChangeListener$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/0D0;


# direct methods
.method public constructor <init>(LX/0D0;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28338
    iput-object p1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$WebViewSelectionChangeListener$1;->b:LX/0D0;

    iput-object p2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$WebViewSelectionChangeListener$1;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 28339
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$WebViewSelectionChangeListener$1;->b:LX/0D0;

    iget-object v0, v0, LX/0D0;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->A:Lcom/facebook/browser/lite/widget/QuoteBar;

    if-nez v0, :cond_0

    .line 28340
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$WebViewSelectionChangeListener$1;->b:LX/0D0;

    iget-object v0, v0, LX/0D0;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-static {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->k(Lcom/facebook/browser/lite/BrowserLiteFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28341
    :goto_0
    return-void

    .line 28342
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$WebViewSelectionChangeListener$1;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28343
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$WebViewSelectionChangeListener$1;->b:LX/0D0;

    iget-object v0, v0, LX/0D0;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->A:Lcom/facebook/browser/lite/widget/QuoteBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/QuoteBar;->setVisibility(I)V

    goto :goto_0

    .line 28344
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$WebViewSelectionChangeListener$1;->b:LX/0D0;

    iget-object v0, v0, LX/0D0;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->A:Lcom/facebook/browser/lite/widget/QuoteBar;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$WebViewSelectionChangeListener$1;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/QuoteBar;->setQuoteText(Ljava/lang/String;)V

    .line 28345
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment$WebViewSelectionChangeListener$1;->b:LX/0D0;

    iget-object v0, v0, LX/0D0;->a:Lcom/facebook/browser/lite/BrowserLiteFragment;

    iget-object v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->A:Lcom/facebook/browser/lite/widget/QuoteBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/QuoteBar;->setVisibility(I)V

    goto :goto_0
.end method
