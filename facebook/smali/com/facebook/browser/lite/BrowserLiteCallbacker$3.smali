.class public final Lcom/facebook/browser/lite/BrowserLiteCallbacker$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0C7;

.field public final synthetic b:LX/0CQ;


# direct methods
.method public constructor <init>(LX/0CQ;LX/0C7;)V
    .locals 0

    .prologue
    .line 27169
    iput-object p1, p0, Lcom/facebook/browser/lite/BrowserLiteCallbacker$3;->b:LX/0CQ;

    iput-object p2, p0, Lcom/facebook/browser/lite/BrowserLiteCallbacker$3;->a:LX/0C7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 27170
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteCallbacker$3;->b:LX/0CQ;

    .line 27171
    const/16 v3, 0x12c

    .line 27172
    :goto_0
    :try_start_0
    iget-object v4, v0, LX/0CQ;->d:LX/0DT;

    if-nez v4, :cond_0

    add-int/lit8 v4, v3, -0x1

    if-lez v3, :cond_0

    .line 27173
    const-wide/16 v5, 0xa

    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v4

    goto :goto_0

    .line 27174
    :catch_0
    :cond_0
    iget-object v3, v0, LX/0CQ;->d:LX/0DT;

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 27175
    if-nez v0, :cond_1

    .line 27176
    sget-object v0, LX/0CQ;->a:Ljava/lang/String;

    const-string v1, "Callback service is not available."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27177
    :goto_2
    return-void

    .line 27178
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteCallbacker$3;->a:LX/0C7;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteCallbacker$3;->b:LX/0CQ;

    iget-object v1, v1, LX/0CQ;->d:LX/0DT;

    invoke-virtual {v0, v1}, LX/0C7;->a(LX/0DT;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 27179
    :catch_1
    move-exception v0

    .line 27180
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteCallbacker$3;->a:LX/0C7;

    invoke-virtual {v1, v0}, LX/0C7;->a(Ljava/lang/Exception;)V

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method
