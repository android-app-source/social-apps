.class public final Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0D5;

.field public final synthetic b:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;


# direct methods
.method public constructor <init>(Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;LX/0D5;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 29527
    iput-object p1, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy$2;->d:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    iput-object p2, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy$2;->a:LX/0D5;

    iput-object p3, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy$2;->b:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    iput-object p4, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy$2;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 29528
    iget-object v0, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy$2;->a:LX/0D5;

    iget-object v1, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy$2;->b:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    const/4 v3, 0x0

    .line 29529
    invoke-virtual {v0}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 29530
    :goto_0
    iget-object v4, v1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v4, v4

    .line 29531
    if-eqz v4, :cond_0

    .line 29532
    iget-object v3, v1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v3, v3

    .line 29533
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 29534
    :cond_0
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 29535
    if-eqz v0, :cond_1

    .line 29536
    iget-object v0, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy$2;->a:LX/0D5;

    iget-object v1, p0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy$2;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0D5;->a(Ljava/lang/String;)V

    .line 29537
    :goto_2
    return-void

    .line 29538
    :cond_1
    sget-object v0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->a:Ljava/lang/String;

    const-string v1, "Could not invoke js callback due to domain change"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    move-object v2, v3

    .line 29539
    goto :goto_0

    .line 29540
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method
