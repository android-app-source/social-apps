.class public Lcom/facebook/browser/lite/BrowserLiteChrome;
.super Landroid/widget/RelativeLayout;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:Ljava/lang/String;

.field public B:LX/0Di;

.field public C:Landroid/os/Bundle;

.field public b:Landroid/content/Context;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field public f:LX/0D5;

.field private g:LX/0EO;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/view/ViewStub;

.field private j:Landroid/widget/ImageView;

.field private k:Landroid/view/View;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/view/View;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/view/ViewStub;

.field public p:Landroid/content/Intent;

.field private q:Landroid/view/View$OnClickListener;

.field public r:LX/0Cd;

.field private s:Landroid/graphics/drawable/Drawable;

.field public t:LX/0CQ;

.field private u:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

.field public v:I

.field private w:Z

.field private x:Z

.field public y:LX/0Ch;

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27663
    const-class v0, Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27664
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27665
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 27666
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27667
    iput-object p1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    .line 27668
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->d()V

    .line 27669
    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 27670
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_ACTION_BUTTON"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 27671
    if-nez v0, :cond_1

    .line 27672
    :cond_0
    :goto_0
    return-void

    .line 27673
    :cond_1
    const-string v1, "KEY_ICON_RES"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 27674
    if-eqz v0, :cond_0

    .line 27675
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 27676
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 27677
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 27678
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    goto :goto_0
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 27679
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 27680
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27681
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27682
    return-void
.end method

.method private a(LX/0EK;)V
    .locals 3

    .prologue
    .line 27626
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->canGoBack()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->canGoForward()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27627
    :cond_0
    new-instance v0, LX/0EK;

    const-string v1, "navigation"

    invoke-direct {v0, v1}, LX/0EK;-><init>(Ljava/lang/String;)V

    .line 27628
    invoke-virtual {p1, v0}, LX/0EK;->a(LX/0EK;)V

    .line 27629
    new-instance v1, LX/0EK;

    const-string v2, "ACTION_GO_BACK"

    invoke-direct {v1, v2}, LX/0EK;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v2}, LX/0D5;->canGoBack()Z

    move-result v2

    .line 27630
    iput-boolean v2, v1, LX/0EK;->e:Z

    .line 27631
    move-object v1, v1

    .line 27632
    invoke-virtual {v0, v1}, LX/0EK;->a(LX/0EK;)V

    .line 27633
    new-instance v1, LX/0EK;

    const-string v2, "ACTION_GO_FORWARD"

    invoke-direct {v1, v2}, LX/0EK;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v2}, LX/0D5;->canGoForward()Z

    move-result v2

    .line 27634
    iput-boolean v2, v1, LX/0EK;->e:Z

    .line 27635
    move-object v1, v1

    .line 27636
    invoke-virtual {v0, v1}, LX/0EK;->a(LX/0EK;)V

    .line 27637
    :cond_1
    return-void
.end method

.method private a(LX/0EK;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0EK;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27683
    invoke-direct {p0, p1, p2}, Lcom/facebook/browser/lite/BrowserLiteChrome;->b(LX/0EK;Ljava/util/ArrayList;)V

    .line 27684
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getAppInstallMenuItem()LX/0EK;

    move-result-object v0

    .line 27685
    if-eqz v0, :cond_0

    .line 27686
    invoke-virtual {p1, v0}, LX/0EK;->a(LX/0EK;)V

    .line 27687
    :cond_0
    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 27688
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27689
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 27690
    :goto_0
    return-void

    .line 27691
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 27692
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LX/0E0;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 27693
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->k:Landroid/view/View;

    sget v1, LX/0E1;->c:I

    invoke-static {v0, v1}, LX/0DR;->a(Landroid/view/View;I)V

    .line 27694
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LX/0E0;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 27695
    const-string v1, "BrowserLiteIntent.URL_TEXT_COLOR_DARK"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 27696
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/16 v2, 0x9d

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 27697
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0634

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 27698
    :cond_0
    :goto_0
    const-string v1, "BrowserLiteIntent.CLOSE_BUTTON_ICON_BACK_ARROW"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27699
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, LX/0E1;->e:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 27700
    :cond_1
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27701
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27702
    sget v1, LX/0E0;->b:I

    invoke-direct {p0, v1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(I)V

    .line 27703
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27704
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->u:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v0, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 27705
    return-void

    .line 27706
    :cond_2
    const-string v1, "BrowserLiteIntent.URL_TEXT_COLOR_BRIGHT"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27707
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27708
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 27709
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 27710
    if-nez p1, :cond_0

    .line 27711
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 27712
    :goto_0
    if-eqz p2, :cond_1

    .line 27713
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 27714
    :goto_1
    return-void

    .line 27715
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 27716
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 27717
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/browser/lite/BrowserLiteChrome;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 27718
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27719
    :cond_0
    :goto_0
    return-void

    .line 27720
    :cond_1
    new-instance v0, LX/0EK;

    invoke-direct {v0}, LX/0EK;-><init>()V

    .line 27721
    invoke-direct {p0, v0, p1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->b(LX/0EK;Ljava/util/ArrayList;)V

    .line 27722
    invoke-virtual {v0}, LX/0EK;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27723
    new-instance v1, LX/0EO;

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    .line 27724
    iget-object v3, v0, LX/0EK;->a:Ljava/util/ArrayList;

    move-object v0, v3

    .line 27725
    new-instance v3, LX/0Cg;

    invoke-direct {v3, p0}, LX/0Cg;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V

    invoke-direct {v1, v2, v0, v3}, LX/0EO;-><init>(Landroid/content/Context;Ljava/util/ArrayList;LX/0Cf;)V

    iput-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    .line 27726
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->a()V

    .line 27727
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, LX/0EO;->setAnchorView(Landroid/view/View;)V

    .line 27728
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    new-instance v1, LX/0Cb;

    invoke-direct {v1, p0}, LX/0Cb;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V

    invoke-virtual {v0, v1}, LX/0EO;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 27729
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->show()V

    .line 27730
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOverScrollMode(I)V

    .line 27731
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 27732
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 27733
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->getListView()Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, LX/0Cc;

    invoke-direct {v1, p0}, LX/0Cc;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0
.end method

.method private b(I)LX/0EK;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 27734
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27735
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getOpenInSpecificAppMenuItem()LX/0EK;

    move-result-object v0

    .line 27736
    if-eqz v0, :cond_0

    .line 27737
    :goto_0
    return-object v0

    .line 27738
    :cond_0
    invoke-static {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->z(Lcom/facebook/browser/lite/BrowserLiteChrome;)Landroid/content/Intent;

    move-result-object v0

    .line 27739
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-static {v2, v0}, LX/0DQ;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 27740
    if-nez v0, :cond_1

    move-object v0, v1

    .line 27741
    goto :goto_0

    .line 27742
    :cond_1
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v2, :cond_4

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-boolean v2, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    if-eqz v2, :cond_4

    .line 27743
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    const-string v2, "android"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 27744
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    const v1, 0x7f081c8c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 27745
    :goto_1
    new-instance v1, LX/0EK;

    const-string v2, "ACTION_OPEN_WITH"

    invoke-direct {v1, v2}, LX/0EK;-><init>(Ljava/lang/String;)V

    .line 27746
    iput-object v0, v1, LX/0EK;->c:Ljava/lang/String;

    .line 27747
    if-gez p1, :cond_5

    .line 27748
    const v0, 0x7f020178

    .line 27749
    iput v0, v1, LX/0EK;->d:I

    .line 27750
    :cond_2
    :goto_2
    move-object v0, v1

    .line 27751
    goto :goto_0

    .line 27752
    :cond_3
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    const v2, 0x7f081c8a

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 27753
    goto :goto_0

    .line 27754
    :cond_5
    if-lez p1, :cond_2

    .line 27755
    iput p1, v1, LX/0EK;->d:I

    .line 27756
    goto :goto_2
.end method

.method private b(LX/0EK;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, -0x1

    .line 27757
    iget-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->w:Z

    if-nez v0, :cond_0

    .line 27758
    :goto_0
    return-void

    .line 27759
    :cond_0
    new-instance v3, LX/0EK;

    const-string v0, "zoom"

    invoke-direct {v3, v0}, LX/0EK;-><init>(Ljava/lang/String;)V

    .line 27760
    invoke-virtual {p1, v3}, LX/0EK;->a(LX/0EK;)V

    .line 27761
    new-instance v4, LX/0EK;

    const-string v0, "ZOOM_OUT"

    invoke-direct {v4, v0}, LX/0EK;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->y:LX/0Ch;

    iget v5, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    invoke-virtual {v0, v5}, LX/0Ch;->a(I)I

    move-result v0

    if-eq v0, v6, :cond_1

    move v0, v1

    .line 27762
    :goto_1
    iput-boolean v0, v4, LX/0EK;->e:Z

    .line 27763
    move-object v0, v4

    .line 27764
    invoke-virtual {v3, v0}, LX/0EK;->a(LX/0EK;)V

    .line 27765
    new-instance v0, LX/0EK;

    const-string v4, "ZOOM_IN"

    invoke-direct {v0, v4}, LX/0EK;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    invoke-static {v4}, LX/0Ch;->b(I)I

    move-result v4

    if-eq v4, v6, :cond_2

    .line 27766
    :goto_2
    iput-boolean v1, v0, LX/0EK;->e:Z

    .line 27767
    move-object v0, v0

    .line 27768
    invoke-virtual {v3, v0}, LX/0EK;->a(LX/0EK;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 27769
    goto :goto_1

    :cond_2
    move v1, v2

    .line 27770
    goto :goto_2
.end method

.method private b(LX/0EK;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0EK;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27773
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 27774
    const-string v2, "KEY_LABEL"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 27775
    const-string v3, "action"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 27776
    const-string v4, "KEY_ICON_RES"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 27777
    const/4 v0, 0x0

    .line 27778
    if-nez v3, :cond_2

    .line 27779
    const-string v3, "MENU_OPEN_WITH"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 27780
    invoke-direct {p0, v4}, Lcom/facebook/browser/lite/BrowserLiteChrome;->b(I)LX/0EK;

    move-result-object v0

    .line 27781
    :cond_1
    :goto_1
    if-eqz v0, :cond_0

    .line 27782
    invoke-virtual {p1, v0}, LX/0EK;->a(LX/0EK;)V

    goto :goto_0

    .line 27783
    :cond_2
    new-instance v0, LX/0EK;

    invoke-direct {v0, v3}, LX/0EK;-><init>(Ljava/lang/String;)V

    .line 27784
    iput-object v2, v0, LX/0EK;->c:Ljava/lang/String;

    .line 27785
    if-lez v4, :cond_1

    .line 27786
    iput v4, v0, LX/0EK;->d:I

    .line 27787
    goto :goto_1

    .line 27788
    :cond_3
    return-void
.end method

.method private b(Landroid/net/Uri;)Z
    .locals 5
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 27957
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 27958
    :cond_0
    :goto_0
    return v0

    .line 27959
    :cond_1
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.EXTRA_ACTION_BUTTON"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 27960
    if-eqz v1, :cond_0

    .line 27961
    const-string v2, "KEY_BLACKLIST_DOMAIN"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 27962
    if-eqz v1, :cond_0

    .line 27963
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27964
    invoke-virtual {v2, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p0

    .line 27965
    if-nez p0, :cond_3

    .line 27966
    :cond_2
    :goto_1
    move v1, v3

    .line 27967
    if-eqz v1, :cond_0

    .line 27968
    const/4 v0, 0x1

    goto :goto_0

    .line 27969
    :cond_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result p0

    .line 27970
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result p1

    .line 27971
    if-ne p0, p1, :cond_4

    move v3, v4

    .line 27972
    goto :goto_1

    .line 27973
    :cond_4
    sub-int p0, p1, p0

    add-int/lit8 p0, p0, -0x1

    invoke-virtual {v2, p0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    const/16 p1, 0x2e

    if-ne p0, p1, :cond_2

    move v3, v4

    goto :goto_1
.end method

.method public static b$redex0(Lcom/facebook/browser/lite/BrowserLiteChrome;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 27939
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27940
    :cond_0
    :goto_0
    return-void

    .line 27941
    :cond_1
    new-instance v0, LX/0EK;

    invoke-direct {v0}, LX/0EK;-><init>()V

    .line 27942
    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(LX/0EK;)V

    .line 27943
    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->b(LX/0EK;)V

    .line 27944
    invoke-direct {p0, v0, p1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(LX/0EK;Ljava/util/ArrayList;)V

    .line 27945
    invoke-virtual {v0}, LX/0EK;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27946
    new-instance v1, LX/0EO;

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    .line 27947
    iget-object v3, v0, LX/0EK;->a:Ljava/util/ArrayList;

    move-object v0, v3

    .line 27948
    new-instance v3, LX/0Cg;

    invoke-direct {v3, p0}, LX/0Cg;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V

    invoke-direct {v1, v2, v0, v3}, LX/0EO;-><init>(Landroid/content/Context;Ljava/util/ArrayList;LX/0Cf;)V

    iput-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    .line 27949
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->a()V

    .line 27950
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, LX/0EO;->setAnchorView(Landroid/view/View;)V

    .line 27951
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    new-instance v1, LX/0CR;

    invoke-direct {v1, p0}, LX/0CR;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V

    invoke-virtual {v0, v1}, LX/0EO;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 27952
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->show()V

    .line 27953
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOverScrollMode(I)V

    .line 27954
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 27955
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 27956
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->getListView()Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, LX/0CS;

    invoke-direct {v1, p0}, LX/0CS;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 27902
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    .line 27903
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_TRACKING"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->C:Landroid/os/Bundle;

    .line 27904
    invoke-static {}, LX/0Di;->a()LX/0Di;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->B:LX/0Di;

    .line 27905
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_TEXT_ZOOM_ENABLED"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->w:Z

    .line 27906
    iget-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->w:Z

    if-eqz v0, :cond_0

    .line 27907
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_SAVED_TEXT_ZOOM_LEVEL"

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    .line 27908
    new-instance v0, LX/0Ch;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.EXTRA_ULTRA_TEXT_ZOOM_OUT_ENABLED"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-direct {v0, p0, v1}, LX/0Ch;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;Z)V

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->y:LX/0Ch;

    .line 27909
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_TEXT_AUTOSIZING_ENABLED"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->x:Z

    .line 27910
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0301e3

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 27911
    const v0, 0x7f0d07b1

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->c:Landroid/widget/TextView;

    .line 27912
    const v0, 0x7f0d07b2

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->d:Landroid/widget/TextView;

    .line 27913
    new-instance v0, LX/0CV;

    invoke-direct {v0, p0}, LX/0CV;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->q:Landroid/view/View$OnClickListener;

    .line 27914
    const v0, 0x7f0d05f0

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    .line 27915
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 27916
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020264

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 27917
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27918
    const v0, 0x7f0d07ac

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->n:Landroid/widget/ImageView;

    .line 27919
    const v0, 0x7f0d07a8

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    .line 27920
    const v0, 0x7f0d07aa

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    .line 27921
    const v0, 0x7f0d07a9

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->i:Landroid/view/ViewStub;

    .line 27922
    const v0, 0x7f0d07b0

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->m:Landroid/view/View;

    .line 27923
    const v0, 0x7f0d07ae

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->l:Landroid/widget/TextView;

    .line 27924
    const v0, 0x7f0d07ad

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->k:Landroid/view/View;

    .line 27925
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020185

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    .line 27926
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0x7f

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 27927
    const v0, 0x7f0d07af

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->u:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    .line 27928
    const v0, 0x7f0d07a7

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->o:Landroid/view/ViewStub;

    .line 27929
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->e()V

    .line 27930
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->f()V

    .line 27931
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->k()V

    .line 27932
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->u()V

    .line 27933
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->r()V

    .line 27934
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->h()V

    .line 27935
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->g()V

    .line 27936
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->i()V

    .line 27937
    invoke-static {}, LX/0CQ;->a()LX/0CQ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->t:LX/0CQ;

    .line 27938
    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 27887
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_ACTION_BUTTON"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 27888
    if-nez v0, :cond_1

    .line 27889
    :cond_0
    :goto_0
    return-void

    .line 27890
    :cond_1
    const-string v1, "KEY_LABEL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 27891
    const-string v2, "action"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 27892
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v2, :cond_0

    .line 27893
    const v3, 0x7f0a062f

    invoke-direct {p0, v3}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(I)V

    .line 27894
    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 27895
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 27896
    const-string v1, "ACTION_BUTTON_ONLY"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 27897
    if-eqz v0, :cond_2

    .line 27898
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 27899
    const/high16 v1, 0x41600000    # 14.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 27900
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v4, v0, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 27901
    :cond_2
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    new-instance v1, LX/0CW;

    invoke-direct {v1, p0, v2}, LX/0CW;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 27878
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_SUB_ACTION_BUTTON"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 27879
    if-nez v0, :cond_1

    .line 27880
    :cond_0
    :goto_0
    return-void

    .line 27881
    :cond_1
    const-string v1, "KEY_ICON_RES"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 27882
    const-string v2, "action"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 27883
    if-eqz v1, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27884
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->o:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 27885
    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 27886
    new-instance v1, LX/0CX;

    invoke-direct {v1, p0, v2}, LX/0CX;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private g()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 27865
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_PROFILE_ICON"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 27866
    if-nez v0, :cond_1

    .line 27867
    :cond_0
    :goto_0
    return-void

    .line 27868
    :cond_1
    const-string v1, "KEY_ICON_URL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 27869
    const-string v2, "action"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 27870
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27871
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->i:Landroid/view/ViewStub;

    sget v3, LX/0E5;->a:I

    invoke-virtual {v0, v3}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 27872
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->i:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 27873
    sget v3, LX/0E5;->b:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 27874
    :try_start_0
    new-instance v3, LX/0Ci;

    invoke-direct {v3, v0}, LX/0Ci;-><init>(Landroid/widget/ImageView;)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, LX/0Ci;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 27875
    :goto_1
    new-instance v1, LX/0CY;

    invoke-direct {v1, p0, v2}, LX/0CY;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 27876
    :catch_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 27877
    sget-object v1, Lcom/facebook/browser/lite/BrowserLiteChrome;->a:Ljava/lang/String;

    const-string v3, "Failed downloading page icon"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private getAppInstallMenuItem()LX/0EK;
    .locals 6

    .prologue
    .line 27850
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "extra_install_intent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 27851
    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->x()Z

    move-result v1

    if-nez v1, :cond_1

    .line 27852
    :cond_0
    const/4 v0, 0x0

    .line 27853
    :goto_0
    return-object v0

    .line 27854
    :cond_1
    const-string v1, "extra_app_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 27855
    new-instance v0, LX/0EK;

    const-string v2, "ACTION_INSTALL_APP"

    invoke-direct {v0, v2}, LX/0EK;-><init>(Ljava/lang/String;)V

    .line 27856
    const v2, 0x7f020171

    .line 27857
    iput v2, v0, LX/0EK;->d:I

    .line 27858
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 27859
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081c88

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 27860
    iput-object v1, v0, LX/0EK;->c:Ljava/lang/String;

    .line 27861
    goto :goto_0

    .line 27862
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081c89

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 27863
    iput-object v1, v0, LX/0EK;->c:Ljava/lang/String;

    .line 27864
    goto :goto_0
.end method

.method private getOpenInSpecificAppMenuItem()LX/0EK;
    .locals 7

    .prologue
    .line 27835
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "extra_app_intent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 27836
    if-nez v0, :cond_0

    .line 27837
    const/4 v0, 0x0

    .line 27838
    :goto_0
    return-object v0

    .line 27839
    :cond_0
    const v1, 0x7f020177

    .line 27840
    const-string v2, "extra_app_name"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 27841
    new-instance v0, LX/0EK;

    const-string v3, "ACTION_LAUNCH_APP"

    invoke-direct {v0, v3}, LX/0EK;-><init>(Ljava/lang/String;)V

    .line 27842
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 27843
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f081c8a

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 27844
    iput-object v2, v0, LX/0EK;->c:Ljava/lang/String;

    .line 27845
    :goto_1
    iput v1, v0, LX/0EK;->d:I

    .line 27846
    goto :goto_0

    .line 27847
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081c8b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 27848
    iput-object v2, v0, LX/0EK;->c:Ljava/lang/String;

    .line 27849
    goto :goto_1
.end method

.method private h()V
    .locals 3

    .prologue
    .line 27771
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_SHOW_CLOSE_BUTTON"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->setCloseButtonVisibility(Z)V

    .line 27772
    return-void
.end method

.method private i()V
    .locals 6

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 27802
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_THEME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 27803
    const-string v1, "THEME_INSTANT_EXPERIENCE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27804
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    sget v2, LX/0E2;->c:I

    invoke-static {v1, v2}, LX/0DR;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 27805
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->n:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27806
    const v0, 0x106000d

    sget v1, LX/0E2;->a:I

    invoke-direct {p0, v0, v1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(II)V

    .line 27807
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 27808
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 27809
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->n:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LX/0E2;->e:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 27810
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->n:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LX/0E2;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27811
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 27812
    iput v5, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 27813
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LX/0E2;->d:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 27814
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 27815
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 27816
    iput v5, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 27817
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 27818
    :cond_0
    :goto_0
    return-void

    .line 27819
    :cond_1
    const-string v1, "THEME_MESSENGER_IAB"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 27820
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.DISPLAY_HEIGHT_RATIO"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v0

    .line 27821
    cmpl-double v0, v0, v2

    if-nez v0, :cond_2

    .line 27822
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    sget v2, LX/0E4;->d:I

    invoke-static {v1, v2}, LX/0DR;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 27823
    :cond_2
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    sget v2, LX/0E4;->a:I

    invoke-static {v1, v2}, LX/0DR;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 27824
    :cond_3
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    sget v3, LX/0E1;->b:I

    invoke-static {v2, v3}, LX/0DR;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 27825
    const-string v1, "THEME_MESSENGER_FB4A"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 27826
    sget v0, LX/0E4;->b:I

    sget v1, LX/0E4;->c:I

    invoke-direct {p0, v0, v1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(II)V

    goto :goto_0

    .line 27827
    :cond_4
    const-string v1, "THEME_WORK_CHAT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 27828
    sget v0, LX/0EA;->a:I

    sget v1, LX/0EA;->b:I

    invoke-direct {p0, v0, v1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(II)V

    goto :goto_0

    .line 27829
    :cond_5
    const-string v1, "THEME_INSTAGRAM_WATCHBROWSE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27830
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 27831
    iput v5, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 27832
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 27833
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 27834
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto/16 :goto_0
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27798
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_REFRESH_BUTTON_ENABLED"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27799
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->u:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    invoke-virtual {v0, v2}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setVisibility(I)V

    .line 27800
    :goto_0
    return-void

    .line 27801
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->u:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private k()V
    .locals 4

    .prologue
    .line 27656
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_MENU_ITEMS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 27657
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27658
    :cond_0
    :goto_0
    return-void

    .line 27659
    :cond_1
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v2, "extra_menu_button_icon"

    sget v3, LX/0E1;->a:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 27660
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-static {v3, v1}, LX/0DR;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 27661
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    new-instance v2, LX/0CZ;

    invoke-direct {v2, p0, v0}, LX/0CZ;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27662
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_SHOW_MENU_ITEM"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->setMenuButtonVisibility(Z)V

    goto :goto_0
.end method

.method private l()V
    .locals 3

    .prologue
    .line 27789
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 27790
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 27791
    const v0, 0x7f0d05f0

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 27792
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 27793
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020264

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 27794
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27795
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->l:Landroid/widget/TextView;

    new-instance v1, LX/0Ca;

    invoke-direct {v1, p0}, LX/0Ca;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27796
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->j()V

    .line 27797
    return-void
.end method

.method private m()V
    .locals 5

    .prologue
    const/16 v4, 0x9d

    .line 27463
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LX/0E0;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 27464
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->k:Landroid/view/View;

    sget v1, LX/0E1;->c:I

    invoke-static {v0, v1}, LX/0DR;->a(Landroid/view/View;I)V

    .line 27465
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 27466
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LX/0E0;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 27467
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27468
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27469
    sget v1, LX/0E0;->b:I

    invoke-direct {p0, v1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(I)V

    .line 27470
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27471
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->u:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v0, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 27472
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 27473
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0634

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 27474
    return-void
.end method

.method private n()V
    .locals 5

    .prologue
    .line 27475
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0636

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 27476
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, LX/0E1;->c:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 27477
    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v1, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LayerDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 27478
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->u:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    new-instance v3, Landroid/graphics/PorterDuffColorFilter;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v1, v4}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 27479
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->k:Landroid/view/View;

    invoke-static {v2, v0}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 27480
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27481
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27482
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27483
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 27484
    return-void
.end method

.method private o()V
    .locals 4

    .prologue
    .line 27485
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LX/0E0;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 27486
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->k:Landroid/view/View;

    sget v1, LX/0E1;->c:I

    invoke-static {v0, v1}, LX/0DR;->a(Landroid/view/View;I)V

    .line 27487
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LX/0E0;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 27488
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27489
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, LX/0E1;->e:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 27490
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27491
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27492
    sget v1, LX/0E0;->b:I

    invoke-direct {p0, v1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(I)V

    .line 27493
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27494
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->u:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v0, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 27495
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 27496
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 27497
    return-void
.end method

.method private p()V
    .locals 5

    .prologue
    .line 27498
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0636

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 27499
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, LX/0E1;->c:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 27500
    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v1, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LayerDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 27501
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->u:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    new-instance v3, Landroid/graphics/PorterDuffColorFilter;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v1, v4}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 27502
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->k:Landroid/view/View;

    invoke-static {v2, v0}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 27503
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27504
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27505
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27506
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 27507
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, LX/0E1;->e:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 27508
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27509
    return-void
.end method

.method private q()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 27510
    const-string v2, "THEME_MESSENGER_FB4A"

    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v4, "BrowserLiteIntent.EXTRA_THEME"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 27511
    :cond_0
    :goto_0
    return v0

    .line 27512
    :cond_1
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_IS_BURD_V1_ENABLED"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 27513
    goto :goto_0

    .line 27514
    :cond_2
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_IS_BURD_V1_WHITE_CHROME_ENABLED"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 27515
    goto :goto_0

    .line 27516
    :cond_3
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_IS_BURD_V1_WITH_BACK_ARROW_ENABLED"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 27517
    goto :goto_0

    .line 27518
    :cond_4
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_IS_BURD_V1_WHITE_WITH_BACK_ARROW_ENABLED"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    move v0, v1

    .line 27519
    goto :goto_0

    .line 27520
    :cond_5
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_IS_BURD_BLUE_ENABLED"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 27521
    goto :goto_0
.end method

.method private r()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27522
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->q()Z

    move-result v0

    if-nez v0, :cond_1

    .line 27523
    :cond_0
    :goto_0
    return-void

    .line 27524
    :cond_1
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->l()V

    .line 27525
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_BURD_V1_ENABLED"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 27526
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->m()V

    goto :goto_0

    .line 27527
    :cond_2
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_BURD_V1_WHITE_CHROME_ENABLED"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 27528
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->n()V

    goto :goto_0

    .line 27529
    :cond_3
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_BURD_V1_WITH_BACK_ARROW_ENABLED"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 27530
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->o()V

    goto :goto_0

    .line 27531
    :cond_4
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_BURD_V1_WHITE_WITH_BACK_ARROW_ENABLED"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 27532
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->p()V

    goto :goto_0

    .line 27533
    :cond_5
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_BURD_BLUE_ENABLED"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27534
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_CLOSE_BUTTON_ICON"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.EXTRA_URL_TEXT_COLOR"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private s()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27535
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->s:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1, v2, v2, v2}, LX/0DR;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 27536
    return-void
.end method

.method private setDomain(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 27537
    if-nez p1, :cond_0

    move-object v0, v1

    .line 27538
    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(Landroid/net/Uri;)V

    .line 27539
    if-nez v0, :cond_1

    .line 27540
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(Ljava/lang/String;Z)V

    .line 27541
    :goto_1
    return-void

    .line 27542
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 27543
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public static setTextZoom(Lcom/facebook/browser/lite/BrowserLiteChrome;I)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 27544
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 27545
    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    .line 27546
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_1

    const/16 v1, 0x64

    if-le p1, v1, :cond_1

    iget-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->x:Z

    if-eqz v1, :cond_1

    .line 27547
    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getLayoutAlgorithm()Landroid/webkit/WebSettings$LayoutAlgorithm;

    move-result-object v1

    sget-object v2, Landroid/webkit/WebSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Landroid/webkit/WebSettings$LayoutAlgorithm;

    if-eq v1, v2, :cond_0

    .line 27548
    sget-object v1, Landroid/webkit/WebSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    .line 27549
    :cond_0
    :goto_0
    return-void

    .line 27550
    :cond_1
    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getLayoutAlgorithm()Landroid/webkit/WebSettings$LayoutAlgorithm;

    move-result-object v1

    sget-object v2, Landroid/webkit/WebSettings$LayoutAlgorithm;->NORMAL:Landroid/webkit/WebSettings$LayoutAlgorithm;

    if-eq v1, v2, :cond_0

    .line 27551
    sget-object v1, Landroid/webkit/WebSettings$LayoutAlgorithm;->NORMAL:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    goto :goto_0
.end method

.method private setTitleBarColorScheme(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 27552
    packed-switch p1, :pswitch_data_0

    .line 27553
    :goto_0
    return-void

    .line 27554
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->w()V

    goto :goto_0

    .line 27555
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->w()V

    .line 27556
    new-instance v0, Landroid/text/SpannableString;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 27557
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 27558
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setUrl(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 27559
    if-nez p1, :cond_0

    .line 27560
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->t()V

    .line 27561
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->l:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 27562
    :goto_0
    return-void

    .line 27563
    :cond_0
    const-string v0, "https://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27564
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->s()V

    .line 27565
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->l:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 27566
    :cond_1
    const-string v0, "http://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 27567
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->t()V

    .line 27568
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->l:Landroid/widget/TextView;

    const/4 v1, 0x7

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 27569
    :cond_2
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->t()V

    .line 27570
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private t()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27571
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->l:Landroid/widget/TextView;

    invoke-static {v0, v1, v1, v1, v1}, LX/0DR;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 27572
    return-void
.end method

.method private u()V
    .locals 3

    .prologue
    .line 27573
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27574
    :cond_0
    :goto_0
    return-void

    .line 27575
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 27576
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BrowserLiteIntent.EXTRA_USE_ALTERNATIVE_TITLE_BAR_COLOR_SCHEME"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 27577
    if-eqz v0, :cond_0

    .line 27578
    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->setTitleBarColorScheme(I)V

    goto :goto_0
.end method

.method private v()Z
    .locals 2

    .prologue
    .line 27579
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_THEME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 27580
    const-string v1, "THEME_MESSENGER_FB4A"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "THEME_WORK_CHAT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "THEME_MESSENGER_IAB"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()V
    .locals 4

    .prologue
    .line 27581
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LX/0E0;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 27582
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LX/0E0;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 27583
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27584
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0638

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27585
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27586
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27587
    sget v1, LX/0E0;->b:I

    invoke-direct {p0, v1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(I)V

    .line 27588
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 27589
    return-void
.end method

.method private x()Z
    .locals 3

    .prologue
    .line 27590
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 27591
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v1}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 27592
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->y()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private y()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 27593
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->r:LX/0Cd;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->r:LX/0Cd;

    invoke-interface {v1}, LX/0Cd;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27594
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v1}, LX/0D5;->canGoBack()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static z(Lcom/facebook/browser/lite/BrowserLiteChrome;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 27595
    const/4 v0, 0x0

    .line 27596
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->x()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27597
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.OPEN_WITH_URL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 27598
    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 27599
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 27600
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 27601
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 27602
    return-object v1
.end method


# virtual methods
.method public final a(LX/0D5;)V
    .locals 2

    .prologue
    .line 27603
    iput-object p1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    .line 27604
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->setTitle(Ljava/lang/String;)V

    .line 27605
    iget-object v0, p1, LX/0D5;->b:LX/0D3;

    move-object v0, v0

    .line 27606
    invoke-virtual {v0}, LX/0D3;->c()V

    .line 27607
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v0}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(Ljava/lang/String;)V

    .line 27608
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->u:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    new-instance v1, LX/0CU;

    invoke-direct {v1, p0}, LX/0CU;-><init>(Lcom/facebook/browser/lite/BrowserLiteChrome;)V

    .line 27609
    iput-object v1, v0, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->d:LX/0CT;

    .line 27610
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->u:Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->f:LX/0D5;

    invoke-virtual {v1}, LX/0D5;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/BrowserLiteRefreshButton;->setProgress(I)V

    .line 27611
    iget-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->w:Z

    if-eqz v0, :cond_0

    .line 27612
    iget v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->v:I

    invoke-static {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->setTextZoom(Lcom/facebook/browser/lite/BrowserLiteChrome;I)V

    .line 27613
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 27614
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->setDomain(Ljava/lang/String;)V

    .line 27615
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27616
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->setUrl(Ljava/lang/String;)V

    .line 27617
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "about:blank"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 27618
    iput-object p1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->A:Ljava/lang/String;

    .line 27619
    :cond_1
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 27620
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    if-nez v1, :cond_1

    .line 27621
    :cond_0
    :goto_0
    return v0

    .line 27622
    :cond_1
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.EXTRA_MENU_ITEMS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 27623
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 27624
    invoke-static {p0, v1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->b$redex0(Lcom/facebook/browser/lite/BrowserLiteChrome;Ljava/util/ArrayList;)V

    .line 27625
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 27638
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27639
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    invoke-virtual {v0}, LX/0EO;->dismiss()V

    .line 27640
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->g:LX/0EO;

    .line 27641
    const/4 v0, 0x1

    .line 27642
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBrowserChromeDelegate(LX/0Cd;)V
    .locals 0

    .prologue
    .line 27643
    iput-object p1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->r:LX/0Cd;

    .line 27644
    return-void
.end method

.method public setCloseButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 27645
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->j:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 27646
    return-void

    .line 27647
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setMenuButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 27648
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->h:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 27649
    return-void

    .line 27650
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 27651
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->p:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_SHOW_TITLE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 27652
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 27653
    :goto_0
    return-void

    .line 27654
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 27655
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteChrome;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
