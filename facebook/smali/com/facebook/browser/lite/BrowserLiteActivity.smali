.class public Lcom/facebook/browser/lite/BrowserLiteActivity;
.super Landroid/app/Activity;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field public static a:Ljava/lang/String;


# instance fields
.field private b:Lcom/facebook/browser/lite/BrowserLiteFragment;

.field private c:LX/0CQ;

.field private d:LX/0Di;

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24393
    const-class v0, Lcom/facebook/browser/lite/BrowserLiteActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browser/lite/BrowserLiteActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24388
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24389
    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->e:Z

    .line 24390
    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->f:Z

    .line 24391
    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->g:Z

    .line 24392
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 24379
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 24380
    if-nez v1, :cond_1

    .line 24381
    :cond_0
    :goto_0
    return-void

    .line 24382
    :cond_1
    const-string v0, "lead_gen_continued_flow_title"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24383
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24384
    const v0, 0x7f0d07ce

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 24385
    sget v2, LX/0E3;->a:I

    invoke-virtual {v0, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 24386
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView;

    .line 24387
    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/BrowserLiteLeadGenContinuedFlowView;->setUpView(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 24373
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "BrowserLiteIntent.EXTRA_IS_RAGE_SHAKE_AVAILABLE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24374
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->c()LX/0DO;

    move-result-object v0

    .line 24375
    if-nez v0, :cond_0

    .line 24376
    new-instance v0, LX/0DO;

    invoke-direct {v0}, LX/0DO;-><init>()V

    .line 24377
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "rageshake_listener_fragment"

    invoke-virtual {v1, v3, v0, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->disallowAddToBackStack()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 24378
    :cond_0
    return-void
.end method

.method private c()LX/0DO;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 24371
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "rageshake_listener_fragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 24372
    check-cast v0, LX/0DO;

    return-object v0
.end method

.method public static d(Lcom/facebook/browser/lite/BrowserLiteActivity;)V
    .locals 3

    .prologue
    .line 24364
    iget-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->f:Z

    if-eqz v0, :cond_1

    .line 24365
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->c:LX/0CQ;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getApplicationContext()Landroid/content/Context;

    .line 24366
    iget-object v1, v0, LX/0CQ;->f:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/0CQ;->d:LX/0DT;

    if-nez v1, :cond_2

    .line 24367
    :cond_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/Runtime;->exit(I)V

    .line 24368
    :goto_0
    return-void

    .line 24369
    :cond_1
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->f()V

    goto :goto_0

    .line 24370
    :cond_2
    iget-object v1, v0, LX/0CQ;->f:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/browser/lite/BrowserLiteCallbacker$13;

    invoke-direct {v2, v0}, Lcom/facebook/browser/lite/BrowserLiteCallbacker$13;-><init>(LX/0CQ;)V

    const p0, -0x77e62887

    invoke-static {v1, v2, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method private e()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 24359
    sget v2, LX/0C6;->b:I

    move v2, v2

    .line 24360
    if-nez v2, :cond_0

    move v2, v0

    .line 24361
    :goto_0
    if-eqz v2, :cond_1

    invoke-static {p0}, LX/0DQ;->a(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 24362
    goto :goto_0

    :cond_1
    move v0, v1

    .line 24363
    goto :goto_1
.end method

.method private f()V
    .locals 4

    .prologue
    .line 24330
    :try_start_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 24331
    const-class v1, Landroid/view/inputmethod/InputMethodManager;

    const-string v2, "finishInputLocked"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 24332
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 24333
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24334
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 24350
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "BrowserLiteIntent.EXTRA_LOCALE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 24351
    if-nez v0, :cond_1

    .line 24352
    :cond_0
    :goto_0
    return-void

    .line 24353
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 24354
    iget-object v2, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 24355
    invoke-virtual {v0, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 24356
    iput-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 24357
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 24358
    invoke-static {v0}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    goto :goto_0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 24346
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "BrowserLiteIntent.EXTRA_ANIMATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    .line 24347
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 24348
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-virtual {p0, v1, v0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->overridePendingTransition(II)V

    .line 24349
    :cond_0
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 24342
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "BrowserLiteIntent.EXTRA_ANIMATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    .line 24343
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 24344
    const/4 v1, 0x2

    aget v1, v0, v1

    const/4 v2, 0x3

    aget v0, v0, v2

    invoke-virtual {p0, v1, v0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->overridePendingTransition(II)V

    .line 24345
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 24335
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->e:Z

    .line 24336
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(I)V

    .line 24337
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 24338
    if-nez p1, :cond_1

    const/4 v0, -0x1

    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "url"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "last_tap_point"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/browser/lite/BrowserLiteActivity;->setResult(ILandroid/content/Intent;)V

    .line 24339
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->finish()V

    .line 24340
    return-void

    .line 24341
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final finish()V
    .locals 3

    .prologue
    .line 24260
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 24261
    invoke-static {}, LX/0C6;->b()V

    .line 24262
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->f:Z

    .line 24263
    iget-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->f:Z

    if-eqz v0, :cond_0

    .line 24264
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 24265
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 24266
    if-eqz v1, :cond_0

    .line 24267
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 24268
    :cond_0
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->i()V

    .line 24269
    return-void
.end method

.method public final onActionModeFinished(Landroid/view/ActionMode;)V
    .locals 1

    .prologue
    .line 24270
    invoke-super {p0, p1}, Landroid/app/Activity;->onActionModeFinished(Landroid/view/ActionMode;)V

    .line 24271
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->c()V

    .line 24272
    return-void
.end method

.method public final onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 1

    .prologue
    .line 24273
    invoke-super {p0, p1}, Landroid/app/Activity;->onActionModeStarted(Landroid/view/ActionMode;)V

    .line 24274
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b()V

    .line 24275
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 24276
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 24277
    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/browser/lite/BrowserLiteActivity;->a(ILjava/lang/String;)V

    .line 24278
    :cond_1
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 24279
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 24280
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->g()V

    .line 24281
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const/4 v8, -0x1

    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0x4e64304f    # -4.5347055E-9f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 24282
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 24283
    invoke-static {}, LX/048;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24284
    invoke-static {p0}, LX/0DQ;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24285
    sput-boolean v4, LX/00v;->a:Z

    .line 24286
    iput-boolean v4, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->g:Z

    .line 24287
    :cond_0
    if-nez p1, :cond_1

    .line 24288
    invoke-static {}, LX/0C6;->a()V

    .line 24289
    :cond_1
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->h()V

    .line 24290
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->g()V

    .line 24291
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "BrowserLiteIntent.EXTRA_LOGCAT"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 24292
    sput-boolean v0, LX/0Dg;->a:Z

    .line 24293
    sget-object v0, LX/0Do;->a:LX/0Do;

    if-nez v0, :cond_3

    .line 24294
    :goto_0
    const v0, 0x7f0301ec

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->setContentView(I)V

    .line 24295
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v2, 0x7f0d07cd

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/BrowserLiteFragment;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 24296
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    new-instance v2, LX/0C5;

    invoke-direct {v2, p0}, LX/0C5;-><init>(Lcom/facebook/browser/lite/BrowserLiteActivity;)V

    .line 24297
    iput-object v2, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->k:LX/0C4;

    .line 24298
    invoke-static {}, LX/0CQ;->a()LX/0CQ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->c:LX/0CQ;

    .line 24299
    invoke-static {}, LX/0Di;->a()LX/0Di;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->d:LX/0Di;

    .line 24300
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->b()V

    .line 24301
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->a()V

    .line 24302
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "BrowserLiteIntent.EXTRA_IS_QUOTE_SHARE_ENTRY_POINT_ENABLED"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 24303
    sput-boolean v0, LX/0Dy;->a:Z

    .line 24304
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "BrowserLiteIntent.DISPLAY_HEIGHT_RATIO"

    invoke-virtual {v0, v2, v6, v7}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v2

    .line 24305
    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_2

    cmpg-double v0, v2, v6

    if-gez v0, :cond_2

    .line 24306
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 24307
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v6, v0

    mul-double/2addr v2, v6

    double-to-int v0, v2

    invoke-virtual {v4, v8, v0}, Landroid/view/Window;->setLayout(II)V

    .line 24308
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v2, 0x57

    invoke-virtual {v0, v2}, Landroid/view/Window;->setGravity(I)V

    .line 24309
    :goto_1
    const v0, 0x68b5afaa

    invoke-static {v0, v1}, LX/02F;->c(II)V

    return-void

    .line 24310
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v8, v8}, Landroid/view/Window;->setLayout(II)V

    goto :goto_1

    .line 24311
    :cond_3
    sget-object v0, LX/0Do;->a:LX/0Do;

    invoke-static {v0}, LX/0Do;->b(LX/0Do;)V

    goto/16 :goto_0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 24312
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->b:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24313
    const/4 v0, 0x1

    .line 24314
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x22

    const v1, 0x70f7590

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 24315
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 24316
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->c()LX/0DO;

    move-result-object v1

    .line 24317
    if-eqz v1, :cond_0

    .line 24318
    invoke-virtual {v1}, LX/0DO;->onPause()V

    .line 24319
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->e:Z

    if-eqz v1, :cond_1

    .line 24320
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/facebook/browser/lite/BrowserLiteActivity$2;

    invoke-direct {v2, p0}, Lcom/facebook/browser/lite/BrowserLiteActivity$2;-><init>(Lcom/facebook/browser/lite/BrowserLiteActivity;)V

    const-wide/16 v4, 0x1f4

    const v3, -0x29735042

    invoke-static {v1, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 24321
    :cond_1
    const/16 v1, 0x23

    const v2, -0x3ef86fc6

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x49eadb30

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 24322
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 24323
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteActivity;->c()LX/0DO;

    move-result-object v1

    .line 24324
    if-eqz v1, :cond_0

    .line 24325
    invoke-virtual {v1}, LX/0DO;->onResume()V

    .line 24326
    :cond_0
    const/16 v1, 0x23

    const v2, -0xf7546a6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onUserInteraction()V
    .locals 1

    .prologue
    .line 24327
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteActivity;->d:LX/0Di;

    invoke-virtual {v0}, LX/0Di;->b()V

    .line 24328
    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    .line 24329
    return-void
.end method
