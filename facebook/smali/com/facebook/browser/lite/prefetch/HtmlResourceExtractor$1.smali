.class public final Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

.field public final synthetic b:LX/0Do;


# direct methods
.method public constructor <init>(LX/0Do;Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;)V
    .locals 0

    .prologue
    .line 31059
    iput-object p1, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->b:LX/0Do;

    iput-object p2, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->a:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 31060
    iget-object v0, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->b:LX/0Do;

    iget-object v0, v0, LX/0Do;->d:Landroid/webkit/WebView;

    if-nez v0, :cond_0

    .line 31061
    iget-object v0, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->b:LX/0Do;

    iget-object v1, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->b:LX/0Do;

    const/4 v5, 0x1

    .line 31062
    new-instance v2, LX/0D4;

    iget-object v3, v1, LX/0Do;->c:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/0D4;-><init>(Landroid/content/Context;)V

    .line 31063
    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    .line 31064
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 31065
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 31066
    :goto_0
    invoke-virtual {v3, v5}, Landroid/webkit/WebSettings;->setBlockNetworkLoads(Z)V

    .line 31067
    new-instance v3, LX/0Dn;

    invoke-direct {v3, v1}, LX/0Dn;-><init>(LX/0Do;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 31068
    move-object v1, v2

    .line 31069
    iput-object v1, v0, LX/0Do;->d:Landroid/webkit/WebView;

    .line 31070
    :goto_1
    iget-object v0, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->b:LX/0Do;

    iget-object v1, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->a:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    .line 31071
    iget-object v2, v1, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;->a:Ljava/lang/String;

    move-object v1, v2

    .line 31072
    iput-object v1, v0, LX/0Do;->e:Ljava/lang/String;

    .line 31073
    iget-object v0, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->b:LX/0Do;

    iget-object v1, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->a:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    .line 31074
    iput-object v1, v0, LX/0Do;->f:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    .line 31075
    iget-object v0, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->b:LX/0Do;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 31076
    iput-wide v2, v0, LX/0Do;->k:J

    .line 31077
    iget-object v0, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->b:LX/0Do;

    iget-object v0, v0, LX/0Do;->d:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->b:LX/0Do;

    iget-object v1, v1, LX/0Do;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 31078
    return-void

    .line 31079
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/prefetch/HtmlResourceExtractor$1;->b:LX/0Do;

    iget-object v0, v0, LX/0Do;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    goto :goto_1

    :catch_0
    goto :goto_0
.end method
