.class public Lcom/facebook/browser/lite/BrowserLiteFragment;
.super Landroid/app/Fragment;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "EmptyCatchBlock"
    }
.end annotation

.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field public static a:Z

.field public static b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;


# instance fields
.field public A:Lcom/facebook/browser/lite/widget/QuoteBar;

.field public B:LX/0Dt;

.field private C:Z

.field public D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:LX/0Dw;

.field public F:I

.field public G:I

.field public H:I

.field public I:I

.field public J:Z

.field public K:Z

.field private L:I

.field private M:I

.field public N:J

.field public O:Z

.field private P:Ljava/lang/String;

.field private Q:J

.field private R:Z

.field private S:Z

.field public T:Z

.field private U:Z

.field private V:Landroid/widget/TextView;

.field public final d:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "LX/0D5;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/net/Uri;

.field public f:Landroid/content/Intent;

.field private g:Landroid/widget/FrameLayout;

.field public h:Lcom/facebook/browser/lite/BrowserLiteChrome;

.field public i:LX/0Dm;

.field public j:LX/0CQ;

.field public k:LX/0C4;

.field public l:LX/0DA;

.field public m:Lcom/facebook/browser/lite/BrowserLitePreview;

.field public n:Landroid/view/View$OnClickListener;

.field public o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

.field public p:Landroid/widget/RelativeLayout;

.field private q:LX/0DI;

.field private r:Landroid/view/View;

.field public s:I

.field private t:J

.field public u:Ljava/lang/String;

.field private v:Z

.field public w:Z

.field public x:Z

.field private y:Z

.field public z:LX/0D0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28911
    const/4 v0, 0x0

    sput-boolean v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->a:Z

    .line 28912
    const-class v0, Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    .line 28913
    const-string v0, "http://m.facebook.com"

    sput-object v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 28896
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 28897
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    .line 28898
    iput v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->s:I

    .line 28899
    iput-wide v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->t:J

    .line 28900
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->v:Z

    .line 28901
    iput-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->x:Z

    .line 28902
    iput-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->C:Z

    .line 28903
    iput-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->J:Z

    .line 28904
    iput-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->K:Z

    .line 28905
    iput-wide v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->N:J

    .line 28906
    iput-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->O:Z

    .line 28907
    iput-wide v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->Q:J

    .line 28908
    iput-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->S:Z

    .line 28909
    iput-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->U:Z

    .line 28910
    return-void
.end method

.method private a(LX/0D5;)V
    .locals 3

    .prologue
    .line 28882
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-virtual {v0, p1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a(LX/0D5;)V

    .line 28883
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->l:LX/0DA;

    if-eqz v0, :cond_0

    .line 28884
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->l:LX/0DA;

    .line 28885
    iput-object p1, v0, LX/0DA;->e:LX/0D5;

    .line 28886
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->q:LX/0DI;

    if-eqz v0, :cond_1

    .line 28887
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->q:LX/0DI;

    .line 28888
    iput-object p1, v0, LX/0DI;->e:LX/0D5;

    .line 28889
    iput-object v0, p1, LX/0D5;->d:LX/0DI;

    .line 28890
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    if-eqz v0, :cond_2

    .line 28891
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    invoke-virtual {v0, p1}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->a(LX/0D5;)V

    .line 28892
    :cond_2
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    if-eqz v0, :cond_3

    .line 28893
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_PREVIEW_JUMP_DESTINATION"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 28894
    new-instance v1, LX/0Cs;

    invoke-direct {v1, p0, v0, p1}, LX/0Cs;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;ILX/0D5;)V

    iput-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->n:Landroid/view/View$OnClickListener;

    .line 28895
    :cond_3
    return-void
.end method

.method private a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 28877
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "BrowserLiteIntent.EXTRA_LOGCAT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 28878
    sput-boolean v0, LX/0Dg;->a:Z

    .line 28879
    invoke-static {}, LX/0CQ;->a()LX/0CQ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    .line 28880
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0CQ;->a(Landroid/content/Context;)V

    .line 28881
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 28867
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_DEBUG_OVERLAY_ENABLED"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28868
    :goto_0
    return-void

    .line 28869
    :cond_0
    const v0, 0x7f0d07c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 28870
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->V:Landroid/widget/TextView;

    .line 28871
    sput-boolean v5, LX/0Df;->b:Z

    .line 28872
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->V:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    .line 28873
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->V:Landroid/widget/TextView;

    new-instance v1, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v1}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 28874
    invoke-static {}, LX/0Df;->a()LX/0Df;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->V:Landroid/widget/TextView;

    .line 28875
    iput-object v1, v0, LX/0Df;->c:Landroid/widget/TextView;

    .line 28876
    sget-object v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v1, "debug overlay. separate data dir: %s, click source %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->U:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v4, "iab_click_source"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 4

    .prologue
    .line 28855
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    if-nez v0, :cond_1

    .line 28856
    :cond_0
    :goto_0
    return-void

    .line 28857
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    .line 28858
    iget-object v1, v0, LX/0Dw;->q:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/0Dw;->q:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, LX/0Dw;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 28859
    :cond_2
    const/4 v1, 0x0

    .line 28860
    :goto_1
    move-object v0, v1

    .line 28861
    if-eqz v0, :cond_0

    .line 28862
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    invoke-virtual {v1, p1, v0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Landroid/os/Bundle;)V

    goto :goto_0

    .line 28863
    :cond_3
    iget-object v1, v0, LX/0Dw;->a:Landroid/app/Activity;

    new-instance v2, Lcom/facebook/browser/lite/products/offers/OfferBrowserBarController$2;

    invoke-direct {v2, v0}, Lcom/facebook/browser/lite/products/offers/OfferBrowserBarController$2;-><init>(LX/0Dw;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 28864
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 28865
    const-string v2, "callbackID"

    invoke-virtual {p1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28866
    const-string v2, "offer_code"

    iget-object v3, v0, LX/0Dw;->q:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/util/List;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28846
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    if-nez v0, :cond_1

    .line 28847
    :cond_0
    :goto_0
    return-void

    .line 28848
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->l:LX/0DA;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    .line 28849
    iget-object v2, v0, LX/0DA;->a:LX/0D9;

    .line 28850
    iget-object v9, v2, LX/0D9;->a:Landroid/app/Activity;

    new-instance v3, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;

    move-object v4, v2

    move-object v5, p2

    move-object v6, v1

    move-object v7, p1

    move-object v8, p3

    invoke-direct/range {v3 .. v8}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;-><init>(LX/0D9;Ljava/util/List;Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 28851
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    if-eqz v0, :cond_0

    .line 28852
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    .line 28853
    iget-object v1, v0, LX/0Dw;->a:Landroid/app/Activity;

    new-instance v2, Lcom/facebook/browser/lite/products/offers/OfferBrowserBarController$3;

    invoke-direct {v2, v0}, Lcom/facebook/browser/lite/products/offers/OfferBrowserBarController$3;-><init>(LX/0Dw;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 28854
    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 28842
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    .line 28843
    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 28844
    :cond_0
    :goto_0
    return-void

    .line 28845
    :cond_1
    new-instance v1, Lcom/facebook/browser/lite/BrowserLiteFragment$4;

    invoke-direct {v1, p0, p2, v0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment$4;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;Ljava/lang/String;LX/0D5;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0D5;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private a(IIIZ)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 28735
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v2}, Lcom/facebook/browser/lite/BrowserLitePreview;->getHeight()I

    move-result v2

    .line 28736
    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v3}, Lcom/facebook/browser/lite/BrowserLitePreview;->getWidth()I

    move-result v3

    .line 28737
    if-le p1, v2, :cond_0

    .line 28738
    const-string v1, "swipe_away"

    invoke-static {p0, v1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->e(Lcom/facebook/browser/lite/BrowserLiteFragment;Ljava/lang/String;)V

    .line 28739
    :goto_0
    return v0

    .line 28740
    :cond_0
    sub-int v4, p2, p3

    .line 28741
    if-le p3, p1, :cond_1

    .line 28742
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v0, v1, v1, v3, v2}, Lcom/facebook/browser/lite/BrowserLitePreview;->layout(IIII)V

    move v0, v1

    .line 28743
    goto :goto_0

    .line 28744
    :cond_1
    if-eqz p4, :cond_3

    .line 28745
    sub-int v4, p1, p3

    .line 28746
    int-to-float v5, v2

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v5, v6

    .line 28747
    int-to-float v4, v4

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_2

    .line 28748
    const-string v1, "swipe_away"

    invoke-static {p0, v1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->e(Lcom/facebook/browser/lite/BrowserLiteFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 28749
    :cond_2
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v0, v1, v1, v3, v2}, Lcom/facebook/browser/lite/BrowserLitePreview;->layout(IIII)V

    :goto_1
    move v0, v1

    .line 28750
    goto :goto_0

    .line 28751
    :cond_3
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    neg-int v2, v4

    invoke-virtual {v0, v2}, Lcom/facebook/browser/lite/BrowserLitePreview;->offsetTopAndBottom(I)V

    goto :goto_1
.end method

.method private static a(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 28820
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 28821
    if-eqz p0, :cond_1

    .line 28822
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 28823
    if-eqz v1, :cond_1

    const-string p1, "our.intern."

    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, ".facebook.com"

    invoke-virtual {v1, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 28824
    if-eqz p0, :cond_4

    const-string v1, "http"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "https"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 28825
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 28826
    :cond_1
    move v0, v0

    .line 28827
    if-nez v0, :cond_3

    .line 28828
    if-eqz p0, :cond_5

    const-string v0, "fb"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "fb-messenger"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "fbinternal"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "fb-work"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "dialtone"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 28829
    if-nez v0, :cond_3

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/URLUtil;->isHttpsUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Landroid/os/Bundle;)Z
    .locals 2
    .param p0    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 28816
    if-nez p0, :cond_1

    .line 28817
    :cond_0
    :goto_0
    return v0

    .line 28818
    :cond_1
    const-string v1, "splash_screen_color"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "splash_throbber"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28819
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 28814
    invoke-virtual {p0}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 28815
    if-eqz v0, :cond_0

    const-string v1, "about:blank"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/browser/lite/BrowserLiteFragment;IIIZ)Z
    .locals 1

    .prologue
    .line 28813
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(IIIZ)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/facebook/browser/lite/BrowserLiteFragment;Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 28812
    invoke-static {p1, p2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a$redex0(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;Landroid/net/Uri;Ljava/util/Map;Ljava/lang/String;)V
    .locals 7
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0D5;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 28785
    iget-wide v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->t:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 28786
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->t:J

    .line 28787
    iget-wide v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->t:J

    .line 28788
    iput-wide v0, p1, LX/0D5;->l:J

    .line 28789
    :cond_0
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 28790
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 28791
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p4, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/0D5;->postUrl(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28792
    :goto_0
    return-void

    .line 28793
    :catch_0
    sget-object v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v1, "Failed postUrl"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 28794
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->e:Landroid/net/Uri;

    if-ne p2, v0, :cond_5

    .line 28795
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->i:LX/0Dm;

    .line 28796
    iget-object v2, v0, LX/0Dm;->b:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    if-eqz v2, :cond_6

    .line 28797
    iget-object v2, v0, LX/0Dm;->b:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    .line 28798
    iget-object v0, v2, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;->a:Ljava/lang/String;

    move-object v2, v0

    .line 28799
    :goto_1
    move-object v0, v2

    .line 28800
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 28801
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 28802
    sget-object v2, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v3, "Prefetch resolved final url %s -> %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28803
    :cond_2
    :goto_2
    if-eqz p3, :cond_4

    invoke-interface {p3}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 28804
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ne v1, v2, :cond_3

    .line 28805
    sget-object v1, Lcom/facebook/browser/lite/BrowserLiteFragment;->b:Ljava/lang/String;

    .line 28806
    :try_start_1
    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 28807
    const-string v3, "<!DOCTYPE HTML>\n<html lang=\"en-US\">\n    <head>\n        <meta charset=\"UTF-8\">\n        <script type=\"text/javascript\">\n            window.location.href = decodeURIComponent(escape(atob(\"%s\")));\n        </script>\n    </head>\n    <body/>\n</html>"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 28808
    :goto_3
    move-object v2, v2

    .line 28809
    const-string v3, "text/html"

    const-string v4, "UTF-8"

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/0D5;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 28810
    :cond_3
    invoke-virtual {p1, v0, p3}, LX/0D5;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 28811
    :cond_4
    invoke-virtual {p1, v0}, LX/0D5;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    goto :goto_1

    :catch_1
    const/4 v2, 0x0

    goto :goto_3
.end method

.method public static a$redex0(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 28591
    invoke-static {p1, p2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(LX/0D5;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28592
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->t()V

    .line 28593
    :cond_0
    return-void
.end method

.method private b(LX/0D5;)Ljava/util/HashMap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0D5;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 28769
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 28770
    iget-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->R:Z

    if-eqz v1, :cond_0

    .line 28771
    const-string v1, "preview_tap_point"

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->P:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28772
    const-string v1, "preview_dismiss_ms"

    iget-wide v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->Q:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28773
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->T:Z

    if-eqz v1, :cond_3

    .line 28774
    const-wide/16 v8, -0x1

    .line 28775
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 28776
    iget-wide v6, p1, LX/0D5;->m:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    .line 28777
    const-string v5, "fbevents_ms"

    iget-wide v6, p1, LX/0D5;->m:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28778
    const-string v5, "fbevents_prefetched"

    iget-boolean v6, p1, LX/0D5;->o:Z

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28779
    :cond_1
    iget-wide v6, p1, LX/0D5;->n:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    .line 28780
    const-string v5, "tr_ms"

    iget-wide v6, p1, LX/0D5;->n:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28781
    const-string v5, "tr_prefetched"

    iget-boolean v6, p1, LX/0D5;->p:Z

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28782
    :cond_2
    move-object v1, v4

    .line 28783
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 28784
    :cond_3
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v0, 0x0

    :cond_4
    return-object v0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 28753
    const-string v0, "web_view_number"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28754
    sget-object v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v2, "The fragment is reconstructed but without webview state number info!"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, LX/0Dg;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28755
    :goto_0
    return-void

    .line 28756
    :cond_0
    const-string v0, "web_view_number"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 28757
    if-nez v2, :cond_1

    .line 28758
    sget-object v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v2, "0 webview saved!"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, LX/0Dg;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 28759
    :goto_1
    if-ge v0, v2, :cond_3

    .line 28760
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "web_view_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 28761
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 28762
    sget-object v3, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v4, "Info for webview %d (total %d) not found!"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v6, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, LX/0Dg;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28763
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 28764
    :cond_2
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 28765
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->u()LX/0D5;

    move-result-object v4

    .line 28766
    invoke-virtual {v4, v3}, LX/0D5;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 28767
    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v3, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 28768
    :cond_3
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0D5;

    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(LX/0D5;)V

    goto :goto_0
.end method

.method private static b(LX/0D5;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 28752
    invoke-virtual {p0}, LX/0D5;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    .line 29117
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    const/4 v0, 0x0

    .line 29118
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 29119
    :cond_0
    :goto_0
    move v0, v0

    .line 29120
    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".facebook.com"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".paypal.com"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static synthetic b(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 28914
    invoke-static {p1, p2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(LX/0D5;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(Lcom/facebook/browser/lite/BrowserLiteFragment;Landroid/webkit/WebView;)Z
    .locals 1

    .prologue
    .line 29121
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 29116
    const-string v0, "fr="

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 29110
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 29111
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v2, 0x3c

    if-le v0, v2, :cond_0

    const/4 v0, 0x1

    .line 29112
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 29113
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 29114
    return-void

    .line 29115
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/webkit/WebView;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 29105
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 29106
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 29107
    if-eqz v0, :cond_0

    .line 29108
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/webkit/CookieManager;->setAcceptThirdPartyCookies(Landroid/webkit/WebView;Z)V

    .line 29109
    :cond_0
    return-void
.end method

.method public static c(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;)V
    .locals 9

    .prologue
    .line 29079
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->B:LX/0Dt;

    if-eqz v0, :cond_0

    .line 29080
    :goto_0
    return-void

    .line 29081
    :cond_0
    new-instance v0, LX/0Dt;

    invoke-direct {v0}, LX/0Dt;-><init>()V

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->B:LX/0Dt;

    .line 29082
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->B:LX/0Dt;

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->r:Landroid/view/View;

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->r:Landroid/view/View;

    const v4, 0x7f0d07be

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    new-instance v4, LX/0Cm;

    invoke-direct {v4, p0, p1}, LX/0Cm;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;)V

    .line 29083
    if-nez v2, :cond_2

    .line 29084
    :cond_1
    :goto_1
    goto :goto_0

    .line 29085
    :cond_2
    const-string v5, "content_subscription_page_id"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, LX/0Dt;->f:Ljava/lang/String;

    .line 29086
    const-string v5, "content_subscription_title"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 29087
    const-string v5, "content_subscription_content"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 29088
    iget-object v5, v1, LX/0Dt;->f:Ljava/lang/String;

    if-eqz v5, :cond_1

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 29089
    sget v5, LX/0E6;->c:I

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 29090
    if-nez v5, :cond_3

    .line 29091
    sget v5, LX/0E6;->a:I

    invoke-virtual {v0, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 29092
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v5

    move-object v8, v5

    .line 29093
    :goto_2
    sget v5, LX/0E6;->d:I

    invoke-virtual {v8, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 29094
    sget v6, LX/0E6;->e:I

    invoke-virtual {v8, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 29095
    sget v7, LX/0E6;->f:I

    invoke-virtual {v8, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;

    .line 29096
    invoke-virtual {v5, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29097
    invoke-virtual {v6, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29098
    iput-object v8, v1, LX/0Dp;->c:Landroid/view/View;

    .line 29099
    iput-object v4, v1, LX/0Dp;->e:LX/0Cl;

    .line 29100
    invoke-virtual {v1}, LX/0Dp;->a()V

    .line 29101
    iput-object v4, v1, LX/0Dt;->e:LX/0Cm;

    .line 29102
    new-instance v5, LX/0Dr;

    invoke-direct {v5, v1, v4}, LX/0Dr;-><init>(LX/0Dt;LX/0Cm;)V

    .line 29103
    iput-object v5, v7, Lcom/facebook/browser/lite/products/messagingbusiness/subscription/BusinessWebSubscribeButton;->a:LX/0Dq;

    .line 29104
    iget-object v5, v1, LX/0Dt;->e:LX/0Cm;

    iget-object v6, v1, LX/0Dt;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/0Cm;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v8, v5

    goto :goto_2
.end method

.method private c(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 29076
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->k:LX/0C4;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->C:Z

    if-nez v0, :cond_0

    .line 29077
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->k:LX/0C4;

    iget v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->s:I

    invoke-interface {v0, v1, p1}, LX/0C4;->a(ILjava/lang/String;)V

    .line 29078
    :cond_0
    return-void
.end method

.method private static d(Landroid/webkit/WebView;)V
    .locals 2

    .prologue
    .line 29122
    if-eqz p0, :cond_1

    .line 29123
    const-string v0, "about:blank"

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 29124
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setTag(Ljava/lang/Object;)V

    .line 29125
    invoke-virtual {p0}, Landroid/webkit/WebView;->clearHistory()V

    .line 29126
    invoke-virtual {p0}, Landroid/webkit/WebView;->removeAllViews()V

    .line 29127
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    .line 29128
    invoke-virtual {p0}, Landroid/webkit/WebView;->clearView()V

    .line 29129
    :cond_0
    invoke-virtual {p0}, Landroid/webkit/WebView;->onPause()V

    .line 29130
    invoke-virtual {p0}, Landroid/webkit/WebView;->destroy()V

    .line 29131
    :cond_1
    return-void
.end method

.method public static d(Lcom/facebook/browser/lite/BrowserLiteFragment;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 29035
    new-array v2, v0, [Ljava/lang/Object;

    aput-object p1, v2, v1

    .line 29036
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    .line 29037
    const/4 v3, 0x0

    .line 29038
    iget-object v4, v2, LX/0CQ;->d:LX/0DT;

    if-eqz v4, :cond_0

    .line 29039
    :try_start_0
    iget-object v4, v2, LX/0CQ;->d:LX/0DT;

    invoke-interface {v4, p1}, LX/0DT;->a(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 29040
    :cond_0
    :goto_0
    move v2, v3

    .line 29041
    packed-switch v2, :pswitch_data_0

    .line 29042
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 29043
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 29044
    :cond_1
    :goto_1
    move v0, v1

    .line 29045
    if-nez v0, :cond_2

    .line 29046
    const v1, 0x7f081c84

    invoke-direct {p0, v1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->c(I)V

    .line 29047
    :cond_2
    :goto_2
    :pswitch_0
    iget-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->x:Z

    if-nez v1, :cond_3

    .line 29048
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->e()V

    .line 29049
    :cond_3
    :goto_3
    return v0

    .line 29050
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->c(Ljava/lang/String;)V

    goto :goto_3

    .line 29051
    :pswitch_2
    const v0, 0x7f081c84

    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->c(I)V

    move v0, v1

    .line 29052
    goto :goto_2

    :catch_0
    goto :goto_0

    .line 29053
    :cond_4
    const/4 v2, 0x1

    :try_start_1
    invoke-static {p1, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 29054
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 29055
    const-string v3, "android.intent.category.BROWSABLE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 29056
    invoke-virtual {v2, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 29057
    invoke-virtual {v2, v4}, Landroid/content/Intent;->setSelector(Landroid/content/Intent;)V

    .line 29058
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v4, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 29059
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 29060
    const/16 v3, 0x40

    invoke-virtual {v5, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 29061
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 29062
    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 29063
    iget-object v3, v3, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 29064
    if-eq v4, v3, :cond_6

    invoke-virtual {v5, v4, v3}, Landroid/content/pm/PackageManager;->checkSignatures(II)I

    move-result v3

    if-nez v3, :cond_5

    .line 29065
    :cond_6
    const/4 v3, 0x1

    .line 29066
    :goto_4
    move v3, v3

    .line 29067
    if-nez v3, :cond_1

    .line 29068
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-nez v1, :cond_7

    .line 29069
    invoke-virtual {v2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    .line 29070
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 29071
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "market"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "details"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "id"

    invoke-virtual {v2, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 29072
    invoke-static {v0, v2}, LX/0DQ;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v2

    move v1, v2

    .line 29073
    goto/16 :goto_1

    .line 29074
    :cond_7
    invoke-static {v0, v2}, LX/0DQ;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    goto/16 :goto_1

    .line 29075
    :catch_1
    goto/16 :goto_1

    :cond_8
    const/4 v3, 0x0

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static e(Lcom/facebook/browser/lite/BrowserLiteFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 29031
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLitePreview;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v1}, Lcom/facebook/browser/lite/BrowserLitePreview;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/0Co;

    invoke-direct {v1, p0}, LX/0Co;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 29032
    iput-object p1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->P:Ljava/lang/String;

    .line 29033
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->Q:J

    .line 29034
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 29029
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/facebook/browser/lite/BrowserLiteFragment$2;

    invoke-direct {v1, p0}, Lcom/facebook/browser/lite/BrowserLiteFragment$2;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 29030
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 29027
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/facebook/browser/lite/BrowserLiteFragment$3;

    invoke-direct {v1, p0}, Lcom/facebook/browser/lite/BrowserLiteFragment$3;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 29028
    return-void
.end method

.method public static k(Lcom/facebook/browser/lite/BrowserLiteFragment;)Z
    .locals 2

    .prologue
    .line 29016
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 29017
    if-nez v0, :cond_0

    .line 29018
    const/4 v0, 0x0

    .line 29019
    :goto_0
    return v0

    .line 29020
    :cond_0
    const v1, 0x7f0d07c0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 29021
    sget v1, LX/0E9;->a:I

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 29022
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/widget/QuoteBar;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->A:Lcom/facebook/browser/lite/widget/QuoteBar;

    .line 29023
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->A:Lcom/facebook/browser/lite/widget/QuoteBar;

    .line 29024
    iget-object v1, v0, Lcom/facebook/browser/lite/widget/QuoteBar;->b:Landroid/view/View;

    move-object v0, v1

    .line 29025
    new-instance v1, LX/0Cp;

    invoke-direct {v1, p0}, LX/0Cp;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29026
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private l()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 28998
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 28999
    invoke-static {v1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 29000
    :cond_0
    :goto_0
    return-void

    .line 29001
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0d07c6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 29002
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    .line 29003
    const-string v0, "splash_throbber"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 29004
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->a(I)V

    .line 29005
    :cond_2
    :goto_1
    const-string v0, "splash_screen_color"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29006
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const-string v3, "splash_screen_color"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v2}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 29007
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->p:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 29008
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, LX/0E2;->g:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 29009
    const-string v2, "splash_screen_color"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->OVERLAY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 29010
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->p:Landroid/widget/RelativeLayout;

    invoke-static {v1, v0}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 29011
    :cond_3
    const-string v0, "splash_icon_url"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 29012
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    invoke-virtual {v0, v3}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->a(I)V

    .line 29013
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    const v2, 0x7f0d07ee

    invoke-virtual {v0, v2}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 29014
    :try_start_0
    new-instance v2, LX/0Cy;

    invoke-direct {v2, p0, v0}, LX/0Cy;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;Landroid/widget/ImageView;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "splash_icon_url"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v3

    invoke-virtual {v2, v0}, LX/0Cy;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 29015
    :catch_0
    sget-object v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v2, "Failed downloading splash icon"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LX/0Dg;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private m()V
    .locals 3

    .prologue
    .line 28994
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_TOAST_RES_ID"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 28995
    if-lez v0, :cond_0

    .line 28996
    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->c(I)V

    .line 28997
    :cond_0
    return-void
.end method

.method private n()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 28935
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v3, "offer_view_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 28936
    :cond_0
    :goto_0
    return-void

    .line 28937
    :cond_1
    new-instance v2, LX/0Dw;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->r:Landroid/view/View;

    invoke-direct {v2, v3, v4}, LX/0Dw;-><init>(Landroid/app/Activity;Landroid/view/View;)V

    iput-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    .line 28938
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    new-instance v3, LX/0Cr;

    invoke-direct {v3, p0}, LX/0Cr;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    const/16 v7, 0x8

    .line 28939
    iput-object v3, v2, LX/0Dw;->b:LX/0Cr;

    .line 28940
    iget-object v4, v2, LX/0Dw;->c:Landroid/view/View;

    sget v5, LX/0E7;->a:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v2, LX/0Dw;->d:Landroid/view/View;

    .line 28941
    iget-object v4, v2, LX/0Dw;->c:Landroid/view/View;

    const v5, 0x7f0d07bb

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    .line 28942
    iget-object v5, v2, LX/0Dw;->d:Landroid/view/View;

    if-eqz v5, :cond_2

    .line 28943
    iget-object v5, v2, LX/0Dw;->d:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 28944
    :cond_2
    iget-object v5, v2, LX/0Dw;->a:Landroid/app/Activity;

    invoke-static {v5}, LX/0Dw;->a(Landroid/app/Activity;)Landroid/os/Bundle;

    move-result-object v5

    .line 28945
    if-nez v5, :cond_8

    .line 28946
    invoke-static {v2}, LX/0Dw;->g(LX/0Dw;)V

    .line 28947
    :goto_1
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 28948
    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v4, "BrowserLiteIntent.EXTRA_THEME"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 28949
    if-eqz v2, :cond_6

    const-string v4, "watch_and_browse_is_in_watch_and_browse"

    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v0

    .line 28950
    :goto_2
    if-eqz v3, :cond_7

    const-string v4, "THEME_INSTANT_EXPERIENCE"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 28951
    :goto_3
    if-nez v2, :cond_3

    if-eqz v0, :cond_0

    .line 28952
    :cond_3
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    .line 28953
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 28954
    iget-object v2, v1, LX/0Dw;->a:Landroid/app/Activity;

    invoke-static {v2}, LX/0Dw;->a(Landroid/app/Activity;)Landroid/os/Bundle;

    move-result-object v4

    .line 28955
    iget-object v2, v1, LX/0Dw;->a:Landroid/app/Activity;

    invoke-static {v2}, LX/0Dw;->a(Landroid/app/Activity;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 28956
    sget-object v6, LX/0Dx;->a:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 28957
    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v2, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 28958
    :cond_5
    move-object v1, v3

    .line 28959
    new-instance v2, LX/0CB;

    invoke-direct {v2, v0, v1}, LX/0CB;-><init>(LX/0CQ;Landroid/os/Bundle;)V

    invoke-static {v0, v2}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 28960
    goto/16 :goto_0

    :cond_6
    move v2, v1

    .line 28961
    goto :goto_2

    :cond_7
    move v0, v1

    .line 28962
    goto :goto_3

    .line 28963
    :cond_8
    const-string v6, "offer_code"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, LX/0Dw;->q:Ljava/lang/String;

    .line 28964
    const-string v6, "title"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, LX/0Dw;->n:Ljava/lang/String;

    .line 28965
    const-string v6, "offer_view_id"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, LX/0Dw;->o:Ljava/lang/String;

    .line 28966
    const-string v6, "share_id"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, LX/0Dw;->p:Ljava/lang/String;

    .line 28967
    iget-object v5, v2, LX/0Dw;->q:Ljava/lang/String;

    if-nez v5, :cond_9

    iget-object v5, v2, LX/0Dw;->n:Ljava/lang/String;

    if-eqz v5, :cond_d

    .line 28968
    :cond_9
    iget-object v5, v2, LX/0Dw;->d:Landroid/view/View;

    if-nez v5, :cond_a

    .line 28969
    sget v5, LX/0E7;->m:I

    invoke-virtual {v4, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 28970
    invoke-virtual {v4}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v4

    iput-object v4, v2, LX/0Dw;->d:Landroid/view/View;

    .line 28971
    :cond_a
    iget-object v4, v2, LX/0Dw;->d:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 28972
    iget-object v4, v2, LX/0Dw;->d:Landroid/view/View;

    sget v5, LX/0E7;->b:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v2, LX/0Dw;->e:Landroid/view/View;

    .line 28973
    iget-object v4, v2, LX/0Dw;->d:Landroid/view/View;

    sget v5, LX/0E7;->c:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v2, LX/0Dw;->f:Landroid/view/View;

    .line 28974
    iget-object v4, v2, LX/0Dw;->d:Landroid/view/View;

    sget v5, LX/0E7;->d:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v2, LX/0Dw;->g:Landroid/view/View;

    .line 28975
    iget-object v4, v2, LX/0Dw;->g:Landroid/view/View;

    sget v5, LX/0E7;->e:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v2, LX/0Dw;->k:Landroid/widget/TextView;

    .line 28976
    iget-object v4, v2, LX/0Dw;->g:Landroid/view/View;

    sget v5, LX/0E7;->f:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v2, LX/0Dw;->l:Landroid/widget/TextView;

    .line 28977
    iget-object v4, v2, LX/0Dw;->q:Ljava/lang/String;

    if-eqz v4, :cond_b

    iget-object v4, v2, LX/0Dw;->q:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 28978
    :cond_b
    const/16 v6, 0x8

    .line 28979
    iget-object v4, v2, LX/0Dw;->e:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 28980
    iget-object v4, v2, LX/0Dw;->f:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 28981
    iget-object v4, v2, LX/0Dw;->g:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 28982
    iget-object v4, v2, LX/0Dw;->e:Landroid/view/View;

    sget v5, LX/0E7;->g:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v2, LX/0Dw;->j:Landroid/widget/TextView;

    .line 28983
    iget-object v4, v2, LX/0Dw;->j:Landroid/widget/TextView;

    iget-object v5, v2, LX/0Dw;->n:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28984
    iget-object v4, v2, LX/0Dw;->e:Landroid/view/View;

    invoke-static {v2, v4}, LX/0Dw;->a(LX/0Dw;Landroid/view/View;)V

    .line 28985
    :goto_5
    iget-object v4, v2, LX/0Dw;->d:Landroid/view/View;

    sget v5, LX/0E7;->k:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 28986
    invoke-static {v2, v4}, LX/0Dw;->a(LX/0Dw;Landroid/view/View;)V

    .line 28987
    iget-object v4, v2, LX/0Dw;->d:Landroid/view/View;

    sget v5, LX/0E7;->l:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v2, LX/0Dw;->r:Landroid/view/View;

    .line 28988
    iget-object v4, v2, LX/0Dw;->r:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 28989
    :goto_6
    invoke-static {v2}, LX/0Dw;->g(LX/0Dw;)V

    goto/16 :goto_1

    .line 28990
    :cond_c
    iget-object v4, v2, LX/0Dw;->q:Ljava/lang/String;

    invoke-static {v2, v4}, LX/0Dw;->a(LX/0Dw;Ljava/lang/String;)V

    .line 28991
    invoke-static {v2}, LX/0Dw;->i(LX/0Dw;)V

    .line 28992
    iget-object v4, v2, LX/0Dw;->d:Landroid/view/View;

    sget v5, LX/0E7;->j:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v2, LX/0Dw;->m:Landroid/view/View;

    goto :goto_5

    .line 28993
    :cond_d
    invoke-static {v2}, LX/0Dw;->j(LX/0Dw;)V

    goto :goto_6
.end method

.method private o()V
    .locals 4

    .prologue
    .line 28830
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    const/4 v1, 0x1

    .line 28831
    iget-boolean v2, v0, LX/0Dw;->s:Z

    if-nez v2, :cond_1

    .line 28832
    iput-boolean v1, v0, LX/0Dw;->s:Z

    .line 28833
    :goto_0
    move v0, v1

    .line 28834
    if-eqz v0, :cond_0

    .line 28835
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    .line 28836
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 28837
    const-string v3, "offer_view_id"

    iget-object p0, v1, LX/0Dw;->o:Ljava/lang/String;

    invoke-virtual {v2, v3, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28838
    const-string v3, "share_id"

    iget-object p0, v1, LX/0Dw;->p:Ljava/lang/String;

    invoke-virtual {v2, v3, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28839
    move-object v1, v2

    .line 28840
    new-instance v2, LX/0CC;

    invoke-direct {v2, v0, v1}, LX/0CC;-><init>(LX/0CQ;Landroid/os/Bundle;)V

    invoke-static {v0, v2}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 28841
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private p()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 28915
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_THEME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 28916
    if-eqz v0, :cond_2

    const-string v1, "THEME_INSTANT_EXPERIENCE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 28917
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 28918
    if-eqz v0, :cond_0

    .line 28919
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LX/0E2;->b:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 28920
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-virtual {v1, v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 28921
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v1, "show_instant_experiences_webview_chrome"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 28922
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28923
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d07ba

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 28924
    sget v1, LX/0E2;->f:I

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 28925
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->p:Landroid/widget/RelativeLayout;

    .line 28926
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->p:Landroid/widget/RelativeLayout;

    sget v1, LX/0E2;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 28927
    new-instance v1, LX/0DI;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getView()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->p:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    invoke-direct {v1, v2, v3, v0, v4}, LX/0DI;-><init>(Landroid/view/View;Landroid/view/View;Landroid/widget/LinearLayout;Landroid/content/Intent;)V

    iput-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->q:LX/0DI;

    .line 28928
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v1, "show_instant_experiences_product_history"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 28929
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 28930
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_2

    .line 28931
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 28932
    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 28933
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 28934
    :cond_2
    return-void
.end method

.method public static synthetic q(Lcom/facebook/browser/lite/BrowserLiteFragment;)I
    .locals 2

    .prologue
    .line 28587
    iget v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->L:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->L:I

    return v0
.end method

.method private q()V
    .locals 4

    .prologue
    .line 28583
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_ENABLE_AUTOFILL"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 28584
    if-eqz v0, :cond_0

    .line 28585
    new-instance v0, LX/0DA;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getView()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    invoke-direct {v0, v1, v2, v3}, LX/0DA;-><init>(Landroid/app/Activity;Landroid/view/View;Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->l:LX/0DA;

    .line 28586
    :cond_0
    return-void
.end method

.method private r()V
    .locals 5

    .prologue
    .line 28574
    invoke-static {}, LX/0Dm;->a()LX/0Dm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->i:LX/0Dm;

    .line 28575
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_PREFETCH_INFO"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    .line 28576
    if-eqz v0, :cond_1

    .line 28577
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->i:LX/0Dm;

    .line 28578
    iput-object v0, v1, LX/0Dm;->b:Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    .line 28579
    :cond_0
    :goto_0
    return-void

    .line 28580
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_NO_PREFETCH_REASON"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 28581
    if-eqz v0, :cond_0

    .line 28582
    sget-object v1, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v2, "No prefetch reason: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static synthetic s(Lcom/facebook/browser/lite/BrowserLiteFragment;)I
    .locals 2

    .prologue
    .line 28573
    iget v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->M:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->M:I

    return v0
.end method

.method private s()LX/0D5;
    .locals 2

    .prologue
    .line 28372
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    .line 28373
    if-eqz v0, :cond_0

    .line 28374
    invoke-virtual {v0}, LX/0D5;->onPause()V

    .line 28375
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/0D5;->setVisibility(I)V

    .line 28376
    :cond_0
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->u()LX/0D5;

    move-result-object v0

    .line 28377
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 28378
    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(LX/0D5;)V

    .line 28379
    return-object v0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 28544
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28545
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->e()V

    .line 28546
    :goto_0
    return-void

    .line 28547
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0D5;

    .line 28548
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/0D5;->setVisibility(I)V

    .line 28549
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 28550
    invoke-static {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d(Landroid/webkit/WebView;)V

    .line 28551
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    .line 28552
    if-nez v0, :cond_1

    .line 28553
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->e()V

    goto :goto_0

    .line 28554
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0D5;->setVisibility(I)V

    .line 28555
    invoke-virtual {v0}, LX/0D5;->onResume()V

    .line 28556
    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(LX/0D5;)V

    goto :goto_0
.end method

.method private u()LX/0D5;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 28480
    new-instance v1, LX/0D5;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x1010085

    invoke-direct {v1, v0, v8, v2}, LX/0D5;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28481
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 28482
    if-eqz v0, :cond_8

    const-string v2, "watch_and_browse_is_in_watch_and_browse"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "watch_and_browse_browser_height"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 28483
    const-string v2, "watch_and_browse_browser_height"

    invoke-virtual {v0, v2, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 28484
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v7, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 28485
    :goto_0
    invoke-virtual {v1, v0}, LX/0D5;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 28486
    invoke-virtual {v1, v5}, LX/0D5;->setFocusable(Z)V

    .line 28487
    invoke-virtual {v1, v5}, LX/0D5;->setFocusableInTouchMode(Z)V

    .line 28488
    invoke-virtual {v1, v5}, LX/0D5;->setScrollbarFadingEnabled(Z)V

    .line 28489
    const/high16 v0, 0x2000000

    invoke-virtual {v1, v0}, LX/0D5;->setScrollBarStyle(I)V

    .line 28490
    new-instance v0, LX/0Ct;

    invoke-direct {v0, p0, v1}, LX/0Ct;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;)V

    invoke-virtual {v1, v0}, LX/0D5;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    .line 28491
    invoke-virtual {v1}, LX/0D5;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 28492
    invoke-static {v0}, LX/0G0;->a(Landroid/webkit/WebSettings;)V

    .line 28493
    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 28494
    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 28495
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 28496
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 28497
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 28498
    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 28499
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 28500
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 28501
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28502
    :goto_1
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_UA"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 28503
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 28504
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 28505
    :cond_0
    new-instance v2, LX/0Cx;

    invoke-direct {v2, p0}, LX/0Cx;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    invoke-virtual {v1, v2}, LX/0D5;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 28506
    new-instance v2, LX/0D3;

    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v4, "BrowserLiteIntent.EXTRA_THEME"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, p0, v3}, LX/0D3;-><init>(LX/0D5;Lcom/facebook/browser/lite/BrowserLiteFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0D5;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 28507
    new-instance v2, LX/0Cu;

    invoke-direct {v2, p0}, LX/0Cu;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    .line 28508
    iput-object v2, v1, LX/0D5;->e:LX/0Cu;

    .line 28509
    new-instance v2, LX/0D6;

    invoke-direct {v2}, LX/0D6;-><init>()V

    .line 28510
    new-instance v3, LX/0Cv;

    invoke-direct {v3, p0}, LX/0Cv;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    invoke-virtual {v2, v3}, LX/0D6;->a(Landroid/view/View$OnTouchListener;)V

    .line 28511
    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v4, "extra_enable_swipe_down_to_dismiss"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 28512
    new-instance v3, LX/0Cz;

    invoke-direct {v3, p0}, LX/0Cz;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    invoke-virtual {v2, v3}, LX/0D6;->a(Landroid/view/View$OnTouchListener;)V

    .line 28513
    :cond_1
    invoke-virtual {v1, v2}, LX/0D5;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 28514
    invoke-virtual {v1, v6}, LX/0D5;->setHapticFeedbackEnabled(Z)V

    .line 28515
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 28516
    const-wide/32 v2, 0x500000

    invoke-virtual {v0, v2, v3}, Landroid/webkit/WebSettings;->setAppCacheMaxSize(J)V

    .line 28517
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 28518
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 28519
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_2

    .line 28520
    iget-boolean v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->y:Z

    invoke-static {v2}, LX/0D5;->setWebContentsDebuggingEnabled(Z)V

    .line 28521
    :cond_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_3

    .line 28522
    invoke-direct {p0, v1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->c(Landroid/webkit/WebView;)V

    .line 28523
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setMixedContentMode(I)V

    .line 28524
    :cond_3
    sget-boolean v0, LX/0Dy;->a:Z

    move v0, v0

    .line 28525
    if-eqz v0, :cond_5

    .line 28526
    new-instance v0, LX/0D0;

    invoke-direct {v0, p0}, LX/0D0;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->z:LX/0D0;

    .line 28527
    new-instance v0, LX/0De;

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->z:LX/0D0;

    invoke-direct {v0, v2}, LX/0De;-><init>(LX/0D0;)V

    .line 28528
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_4

    .line 28529
    iput-object v1, v0, LX/0De;->b:Landroid/webkit/WebView;

    .line 28530
    iget-object v2, v0, LX/0De;->b:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 28531
    iget-object v2, v0, LX/0De;->b:Landroid/webkit/WebView;

    const-string v3, "FbQuoteShareJSInterface"

    invoke-virtual {v2, v0, v3}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28532
    :cond_4
    new-instance v0, LX/0Cj;

    invoke-direct {v0, p0, v1}, LX/0Cj;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;)V

    invoke-virtual {v1, v0}, LX/0D5;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 28533
    :cond_5
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    if-eqz v0, :cond_6

    .line 28534
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    .line 28535
    iget-object v3, v2, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->b:Ljava/lang/String;

    move-object v2, v3

    .line 28536
    invoke-virtual {v1, v0, v2}, LX/0D5;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28537
    :cond_6
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->v()V

    .line 28538
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.EXTRA_WEBVIEW_LAYTER_TYPE"

    invoke-virtual {v0, v2, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 28539
    if-ltz v0, :cond_7

    const/4 v2, 0x2

    if-gt v0, v2, :cond_7

    .line 28540
    invoke-virtual {v1, v0, v8}, LX/0D5;->setLayerType(ILandroid/graphics/Paint;)V

    .line 28541
    :cond_7
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 28542
    return-object v1

    .line 28543
    :cond_8
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    goto/16 :goto_0

    :catch_0
    goto/16 :goto_1
.end method

.method private v()V
    .locals 7

    .prologue
    .line 28462
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 28463
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v1

    .line 28464
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->e:Landroid/net/Uri;

    invoke-static {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(Landroid/net/Uri;)Z

    move-result v2

    .line 28465
    if-nez v2, :cond_0

    .line 28466
    invoke-virtual {v1}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 28467
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 28468
    const-string v3, "BrowserLiteIntent.EXTRA_COOKIES"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 28469
    if-nez v0, :cond_1

    .line 28470
    :goto_1
    return-void

    .line 28471
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->w:Z

    goto :goto_0

    .line 28472
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 28473
    const-string v4, "KEY_URL"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 28474
    const-string v5, "KEY_STRING_ARRAY"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 28475
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    if-eqz v0, :cond_2

    .line 28476
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 28477
    if-nez v2, :cond_4

    invoke-static {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 28478
    :cond_4
    invoke-virtual {v1, v4, v0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 28479
    :cond_5
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    goto :goto_1
.end method

.method public static w(Lcom/facebook/browser/lite/BrowserLiteFragment;)V
    .locals 1

    .prologue
    .line 28457
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 28458
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 28459
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 28460
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 28461
    return-void
.end method

.method private x()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 28437
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_PREVIEW_TITLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 28438
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.EXTRA_PREVIEW_SUBTITLE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 28439
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_PREVIEW_BODY"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 28440
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v4, "BrowserLiteIntent.EXTRA_NO_PREVIEW_REASON"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 28441
    iget-object v4, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v5, "BrowserLiteIntent.EXTRA_PREVIEW_MAX_LINES"

    const/16 v6, 0x10

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 28442
    iget-object v5, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v6, "BrowserLiteIntent.EXTRA_PREVIEW_FAVICON_URL"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 28443
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 28444
    iput-boolean v8, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->R:Z

    .line 28445
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v6, 0x7f0d07c3

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 28446
    sget v6, LX/0E8;->a:I

    invoke-virtual {v0, v6}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 28447
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/BrowserLitePreview;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    .line 28448
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLitePreview;->bringToFront()V

    .line 28449
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/BrowserLitePreview;->setPreviewTitle(Ljava/lang/String;)V

    .line 28450
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v0, v5}, Lcom/facebook/browser/lite/BrowserLitePreview;->a(Ljava/lang/String;)V

    .line 28451
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v0, v2}, Lcom/facebook/browser/lite/BrowserLitePreview;->setPreviewSubtitle(Ljava/lang/String;)V

    .line 28452
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v0, v3}, Lcom/facebook/browser/lite/BrowserLitePreview;->setPreviewText(Ljava/lang/String;)V

    .line 28453
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    invoke-virtual {v0, v4}, Lcom/facebook/browser/lite/BrowserLitePreview;->setPreviewBodyMaxLines(I)V

    .line 28454
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    new-instance v1, LX/0Cn;

    invoke-direct {v1, p0}, LX/0Cn;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/BrowserLitePreview;->setPreviewCloseButtonListener(Landroid/view/View$OnClickListener;)V

    .line 28455
    :goto_0
    return-void

    .line 28456
    :cond_0
    sget-object v1, Lcom/facebook/browser/lite/BrowserLiteFragment;->c:Ljava/lang/String;

    const-string v2, "Not showing the preview, reason: %s"

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, LX/0Dg;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private y()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 28354
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 28355
    if-eqz v1, :cond_0

    const-string v0, "watch_and_browse_is_in_watch_and_browse"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 28356
    :cond_0
    :goto_0
    return-void

    .line 28357
    :cond_1
    const-string v0, "watch_and_browse_dummy_video_view_height"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 28358
    const-string v0, "watch_and_browse_dummy_video_view_height"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 28359
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 28360
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setClipToPadding(Z)V

    .line 28361
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setClipChildren(Z)V

    .line 28362
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    if-eqz v0, :cond_2

    .line 28363
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 28364
    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 28365
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->o:Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;

    invoke-virtual {v2, v0}, Lcom/facebook/browser/lite/widget/BrowserLiteSplashScreen;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 28366
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0d07b7

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 28367
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v2}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 28368
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v2}, LX/0DR;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 28369
    const-string v0, "watch_and_browse_is_in_watch_and_install"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->O:Z

    .line 28370
    iget-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->O:Z

    if-eqz v0, :cond_0

    .line 28371
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->setVisibility(I)V

    goto :goto_0
.end method

.method public static z(Lcom/facebook/browser/lite/BrowserLiteFragment;)I
    .locals 4

    .prologue
    .line 28380
    const/4 v0, 0x0

    .line 28381
    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0D5;

    .line 28382
    invoke-virtual {v0}, LX/0D5;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v3

    .line 28383
    invoke-virtual {v3}, Landroid/webkit/WebBackForwardList;->getCurrentIndex()I

    move-result p0

    add-int/lit8 p0, p0, 0x1

    invoke-static {v3, p0}, LX/0D5;->a(Landroid/webkit/WebBackForwardList;I)I

    move-result v3

    move v0, v3

    .line 28384
    add-int/2addr v0, v1

    move v1, v0

    .line 28385
    goto :goto_0

    .line 28386
    :cond_0
    return v1
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 28387
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->l:LX/0DA;

    if-nez v0, :cond_1

    .line 28388
    :cond_0
    :goto_0
    return-void

    .line 28389
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->l:LX/0DA;

    .line 28390
    iget-object v1, v0, LX/0DA;->a:LX/0D9;

    .line 28391
    iget-object v2, v1, LX/0D9;->a:Landroid/app/Activity;

    new-instance v0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$4;

    invoke-direct {v0, v1}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$4;-><init>(LX/0D9;)V

    invoke-virtual {v2, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 28392
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    if-eqz v0, :cond_0

    .line 28393
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    .line 28394
    iget-object v1, v0, LX/0Dw;->a:Landroid/app/Activity;

    new-instance v2, Lcom/facebook/browser/lite/products/offers/OfferBrowserBarController$4;

    invoke-direct {v2, v0}, Lcom/facebook/browser/lite/products/offers/OfferBrowserBarController$4;-><init>(LX/0Dw;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 28395
    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 28396
    const-string v0, "EXTRA_ACTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 28397
    if-nez v2, :cond_1

    .line 28398
    :cond_0
    :goto_0
    return-void

    .line 28399
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 28400
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->j()V

    goto :goto_0

    .line 28401
    :sswitch_0
    const-string v3, "ACTION_SHOW_QUOTE_SHARE_NUX"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string v3, "ACTION_HANDLE_AUTO_FILL"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v3, "ACTION_HANDLE_AUTOFILL_SAVE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v3, "ACTION_COMPLETE_WEB_SHARE_DIALOG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string v3, "ACTION_UPDATE_OFFERS_BAR"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_5
    const-string v3, "ACTION_HANDLE_OFFER_CODE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_6
    const-string v3, "ACTION_HANDLE_IX_UPDATE_PRODUCT_HISTORY"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_7
    const-string v3, "ACTION_CLOSE_BROWSER"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x7

    goto :goto_1

    .line 28402
    :pswitch_1
    const-string v0, "EXTRA_AUTO_FILL_FIELDS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 28403
    const-string v0, "EXTRA_AUTO_FILL_JS_BRIDGE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    const-string v2, "EXTRA_AUTO_FILL_FIELD_FULL_NAME"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    .line 28404
    :pswitch_2
    const-string v0, "EXTRA_AUTOFILL_SAVE_DATA"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 28405
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->l:LX/0DA;

    if-eqz v0, :cond_0

    .line 28406
    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->l:LX/0DA;

    const-string v0, "EXTRA_AUTO_FILL_JS_BRIDGE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    const-string v4, "EXTRA_AUTOFILL_SAVE_BANNER_IS_EXPANDED"

    invoke-virtual {p1, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 28407
    iget-object v4, v3, LX/0DA;->f:LX/0DD;

    if-nez v4, :cond_3

    .line 28408
    new-instance v4, LX/0DD;

    iget-object p0, v3, LX/0DA;->c:Landroid/app/Activity;

    iget-object p1, v3, LX/0DA;->d:Landroid/view/View;

    invoke-direct {v4, p0, p1, v1}, LX/0DD;-><init>(Landroid/app/Activity;Landroid/view/View;Z)V

    iput-object v4, v3, LX/0DA;->f:LX/0DD;

    .line 28409
    :cond_3
    iget-object v4, v3, LX/0DA;->f:LX/0DD;

    .line 28410
    iget-object p0, v4, LX/0DD;->a:Landroid/app/Activity;

    new-instance p1, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;

    invoke-direct {p1, v4, v2, v0}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;-><init>(LX/0DD;Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 28411
    goto/16 :goto_0

    .line 28412
    :pswitch_3
    const-string v0, "BrowserLiteIntent.EXTRA_JS_TO_EXECUTE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrowserLiteIntent.EXTRA_REFERER"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 28413
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->E:LX/0Dw;

    if-eqz v0, :cond_0

    .line 28414
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/facebook/browser/lite/BrowserLiteFragment$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment$1;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 28415
    :pswitch_5
    const-string v0, "EXTRA_OFFER_CODE_JS_BRIDGE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    .line 28416
    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    goto/16 :goto_0

    .line 28417
    :pswitch_6
    const-string v0, "EXTRA_IX_PRODUCT_URL"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 28418
    const-string v0, "EXTRA_IX_PRODUCT_LOCALE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 28419
    :pswitch_7
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->i()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7b9fe6a8 -> :sswitch_7
        -0x3bf5c3d0 -> :sswitch_6
        -0x25a0b942 -> :sswitch_5
        0x1098b3a5 -> :sswitch_1
        0x2c6491f5 -> :sswitch_0
        0x44f6327c -> :sswitch_2
        0x69e4ae70 -> :sswitch_3
        0x75dbacb8 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Landroid/webkit/WebView;)V
    .locals 1

    .prologue
    .line 28420
    invoke-static {p0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(Lcom/facebook/browser/lite/BrowserLiteFragment;Landroid/webkit/WebView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28421
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->t()V

    .line 28422
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 28423
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    if-eqz v0, :cond_0

    .line 28424
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-virtual {v0, p1}, Lcom/facebook/browser/lite/BrowserLiteChrome;->setTitle(Ljava/lang/String;)V

    .line 28425
    :cond_0
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 28426
    packed-switch p1, :pswitch_data_0

    .line 28427
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 28428
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    if-eqz v0, :cond_0

    .line 28429
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->a()Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/webkit/WebView;ZLandroid/os/Message;)Z
    .locals 2

    .prologue
    .line 28430
    invoke-static {p0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(Lcom/facebook/browser/lite/BrowserLiteFragment;Landroid/webkit/WebView;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 28431
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->s()LX/0D5;

    move-result-object v1

    .line 28432
    iget-object v0, p3, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/webkit/WebView$WebViewTransport;

    .line 28433
    invoke-virtual {v0, v1}, Landroid/webkit/WebView$WebViewTransport;->setWebView(Landroid/webkit/WebView;)V

    .line 28434
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    .line 28435
    const/4 v0, 0x1

    .line 28436
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 28654
    sget-boolean v0, LX/0Dy;->a:Z

    move v0, v0

    .line 28655
    if-eqz v0, :cond_0

    .line 28656
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->A:Lcom/facebook/browser/lite/widget/QuoteBar;

    if-nez v0, :cond_1

    .line 28657
    invoke-static {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->k(Lcom/facebook/browser/lite/BrowserLiteFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 28658
    :cond_0
    :goto_0
    return-void

    .line 28659
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->A:Lcom/facebook/browser/lite/widget/QuoteBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/QuoteBar;->setVisibility(I)V

    .line 28660
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->A:Lcom/facebook/browser/lite/widget/QuoteBar;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/widget/QuoteBar;->bringToFront()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 28730
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    if-eqz v0, :cond_0

    .line 28731
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->a(LX/0D5;)V

    .line 28732
    :cond_0
    iput p1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->s:I

    .line 28733
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->C:Z

    .line 28734
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 28726
    sget-boolean v0, LX/0Dy;->a:Z

    move v0, v0

    .line 28727
    if-eqz v0, :cond_0

    .line 28728
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->A:Lcom/facebook/browser/lite/widget/QuoteBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/widget/QuoteBar;->setVisibility(I)V

    .line 28729
    :cond_0
    return-void
.end method

.method public final d()LX/0D5;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 28725
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0D5;

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 28723
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->c(Ljava/lang/String;)V

    .line 28724
    return-void
.end method

.method public final f()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 28557
    const/4 v2, 0x2

    iput v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->s:I

    .line 28558
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v2

    .line 28559
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a()V

    .line 28560
    if-nez v2, :cond_1

    .line 28561
    :cond_0
    :goto_0
    return v0

    .line 28562
    :cond_1
    iget-object v3, v2, LX/0D5;->b:LX/0D3;

    move-object v3, v3

    .line 28563
    if-eqz v3, :cond_2

    .line 28564
    iget-object v3, v2, LX/0D5;->b:LX/0D3;

    move-object v3, v3

    .line 28565
    invoke-virtual {v3}, LX/0D3;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 28566
    goto :goto_0

    .line 28567
    :cond_2
    invoke-virtual {v2}, LX/0D5;->canGoBack()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 28568
    invoke-virtual {v2}, LX/0D5;->goBack()V

    move v0, v1

    .line 28569
    goto :goto_0

    .line 28570
    :cond_3
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    if-le v2, v1, :cond_0

    .line 28571
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->t()V

    move v0, v1

    .line 28572
    goto :goto_0
.end method

.method public final g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28719
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 28720
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 28721
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->requestLayout()V

    .line 28722
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 28669
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 28670
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    .line 28671
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->e:Landroid/net/Uri;

    .line 28672
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->e:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->e:Landroid/net/Uri;

    invoke-static {v0}, LX/047;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 28673
    :cond_0
    :goto_0
    return-void

    .line 28674
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, LX/0DQ;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-boolean v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->a:Z

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->S:Z

    .line 28675
    sput-boolean v1, Lcom/facebook/browser/lite/BrowserLiteFragment;->a:Z

    .line 28676
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 28677
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.JS_BRIDGE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    .line 28678
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    if-eqz v0, :cond_2

    .line 28679
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->D:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    invoke-virtual {v0, p0}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->a(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    .line 28680
    :cond_2
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.EXTRA_IS_IN_APP_BROWSER_PROFILING_ENABLED"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->y:Z

    .line 28681
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.EXTRA_VIDEO_TIME_SPENT_INTERVAL"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 28682
    if-lez v0, :cond_3

    .line 28683
    sput v0, LX/0Dj;->a:I

    .line 28684
    :cond_3
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.EXTRA_LOG_FB_TRACKING_REQUEST"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->T:Z

    .line 28685
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->u:Ljava/lang/String;

    .line 28686
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->r()V

    .line 28687
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0d07b9

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/BrowserLiteChrome;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    .line 28688
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    new-instance v2, LX/0Cw;

    invoke-direct {v2, p0}, LX/0Cw;-><init>(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    .line 28689
    iput-object v2, v0, Lcom/facebook/browser/lite/BrowserLiteChrome;->r:LX/0Cd;

    .line 28690
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->bringToFront()V

    .line 28691
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->p()V

    .line 28692
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->l()V

    .line 28693
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->q()V

    .line 28694
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0d06c3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    .line 28695
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->n()V

    .line 28696
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->y()V

    .line 28697
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->x()V

    .line 28698
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->m()V

    .line 28699
    if-nez p1, :cond_8

    .line 28700
    invoke-direct {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->s()LX/0D5;

    move-result-object v2

    .line 28701
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_POST_DATA"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 28702
    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->e:Landroid/net/Uri;

    invoke-static {v3, v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 28703
    :goto_2
    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v4, "BrowserLiteIntent.EXTRA_REFERER"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 28704
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 28705
    sput-object v3, Lcom/facebook/browser/lite/BrowserLiteFragment;->b:Ljava/lang/String;

    .line 28706
    :cond_4
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 28707
    const-string v4, "Referer"

    sget-object v5, Lcom/facebook/browser/lite/BrowserLiteFragment;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28708
    iget-object v4, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->e:Landroid/net/Uri;

    invoke-static {p0, v2, v4, v3, v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a$redex0(Lcom/facebook/browser/lite/BrowserLiteFragment;LX/0D5;Landroid/net/Uri;Ljava/util/Map;Ljava/lang/String;)V

    .line 28709
    :goto_3
    invoke-static {}, LX/0D1;->a()LX/0D1;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0D1;->a(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    .line 28710
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.EXTRA_TRACKING"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 28711
    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    iget-object v3, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->e:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 28712
    new-instance v4, LX/0CK;

    invoke-direct {v4, v2, v3, v0}, LX/0CK;-><init>(LX/0CQ;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v2, v4}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 28713
    invoke-static {}, LX/048;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, LX/0DQ;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 28714
    iput-boolean v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->U:Z

    .line 28715
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 28716
    goto/16 :goto_1

    .line 28717
    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    .line 28718
    :cond_8
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(Landroid/os/Bundle;)V

    goto :goto_3
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 28665
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    .line 28666
    iget-object p0, v0, LX/0D5;->b:LX/0D3;

    move-object v0, p0

    .line 28667
    invoke-virtual {v0, p1, p2, p3}, LX/0D3;->a(IILandroid/content/Intent;)Z

    .line 28668
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 28661
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 28662
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    .line 28663
    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Landroid/app/Activity;)V

    .line 28664
    :cond_0
    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 28588
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 28589
    check-cast p1, Landroid/app/Activity;

    invoke-direct {p0, p1}, Lcom/facebook/browser/lite/BrowserLiteFragment;->a(Landroid/app/Activity;)V

    .line 28590
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 28650
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 28651
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    if-eqz v0, :cond_0

    .line 28652
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteChrome;->b()Z

    .line 28653
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 28648
    const v0, 0x7f0301e9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->r:Landroid/view/View;

    .line 28649
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->r:Landroid/view/View;

    return-object v0
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 28640
    iget-boolean v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->C:Z

    if-nez v0, :cond_0

    .line 28641
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0CQ;->b(Landroid/content/Context;)V

    .line 28642
    :cond_0
    invoke-static {}, LX/0D1;->a()LX/0D1;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0D1;->b(Lcom/facebook/browser/lite/BrowserLiteFragment;)V

    .line 28643
    :goto_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 28644
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0D5;

    .line 28645
    invoke-static {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d(Landroid/webkit/WebView;)V

    goto :goto_0

    .line 28646
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 28647
    return-void
.end method

.method public final onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28631
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 28632
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 28633
    iput-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->g:Landroid/widget/FrameLayout;

    .line 28634
    :cond_0
    iput-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->r:Landroid/view/View;

    .line 28635
    iput-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->h:Lcom/facebook/browser/lite/BrowserLiteChrome;

    .line 28636
    iput-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->A:Lcom/facebook/browser/lite/widget/QuoteBar;

    .line 28637
    iput-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->m:Lcom/facebook/browser/lite/BrowserLitePreview;

    .line 28638
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 28639
    return-void
.end method

.method public final onPause()V
    .locals 24

    .prologue
    .line 28609
    invoke-super/range {p0 .. p0}, Landroid/app/Fragment;->onPause()V

    .line 28610
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v3

    .line 28611
    if-eqz v3, :cond_3

    invoke-virtual {v3}, LX/0D5;->getUrl()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v23, v2

    .line 28612
    :goto_0
    if-eqz v3, :cond_4

    invoke-virtual {v3}, LX/0D5;->getTitle()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v22, v2

    .line 28613
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->C:Z

    move-object/from16 v0, v23

    invoke-virtual {v2, v0, v4}, LX/0CQ;->a(Ljava/lang/String;Z)V

    .line 28614
    invoke-direct/range {p0 .. p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->o()V

    .line 28615
    if-eqz v3, :cond_0

    .line 28616
    invoke-virtual {v3}, LX/0D5;->onPause()V

    .line 28617
    invoke-virtual {v3}, LX/0D5;->pauseTimers()V

    .line 28618
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->v:Z

    if-eqz v2, :cond_0

    .line 28619
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->v:Z

    .line 28620
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->firstElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0D5;

    .line 28621
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(LX/0D5;)Ljava/util/HashMap;

    move-result-object v20

    .line 28622
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2}, LX/0D5;->getFirstUrl()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->t:J

    invoke-virtual {v2}, LX/0D5;->getResponseEndTime()J

    move-result-wide v8

    invoke-virtual {v2}, LX/0D5;->getDomContentloadedTime()J

    move-result-wide v10

    invoke-virtual {v2}, LX/0D5;->getLoadEventEndTime()J

    move-result-wide v12

    invoke-virtual {v2}, LX/0D5;->getFirstScrollReadyTime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->F:I

    move/from16 v16, v0

    invoke-virtual {v2}, LX/0D5;->getHitRefreshButton()Z

    move-result v17

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->C:Z

    move/from16 v18, v0

    invoke-virtual {v2}, LX/0D5;->getIsAmp()Z

    move-result v19

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->S:Z

    move/from16 v21, v0

    invoke-virtual/range {v3 .. v21}, LX/0CQ;->a(Landroid/content/Context;Ljava/lang/String;JJJJJIZZZLjava/util/HashMap;Z)V

    .line 28623
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->C:Z

    if-eqz v2, :cond_1

    .line 28624
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->s:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->L:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->M:I

    move-object/from16 v3, v23

    move-object/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/0CQ;->a(Ljava/lang/String;Ljava/lang/String;III)V

    .line 28625
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0CQ;->c(Landroid/content/Context;)V

    .line 28626
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->C:Z

    if-eqz v2, :cond_2

    .line 28627
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0CQ;->b(Landroid/content/Context;)V

    .line 28628
    :cond_2
    return-void

    .line 28629
    :cond_3
    const/4 v2, 0x0

    move-object/from16 v23, v2

    goto/16 :goto_0

    .line 28630
    :cond_4
    const/4 v2, 0x0

    move-object/from16 v22, v2

    goto/16 :goto_1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    .line 28601
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 28602
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->j:LX/0CQ;

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->u:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->f:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_TRACKING"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 28603
    new-instance v3, LX/0CL;

    invoke-direct {v3, v0, v1, v2}, LX/0CL;-><init>(LX/0CQ;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v0, v3}, LX/0CQ;->a(LX/0CQ;LX/0C7;)V

    .line 28604
    invoke-virtual {p0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    .line 28605
    if-eqz v0, :cond_0

    .line 28606
    invoke-virtual {v0}, LX/0D5;->onResume()V

    .line 28607
    invoke-virtual {v0}, LX/0D5;->resumeTimers()V

    .line 28608
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 28594
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 28595
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 28596
    iget-object v0, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0D5;

    invoke-virtual {v0, v2}, LX/0D5;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 28597
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "web_view_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 28598
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 28599
    :cond_0
    const-string v0, "web_view_number"

    iget-object v1, p0, Lcom/facebook/browser/lite/BrowserLiteFragment;->d:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 28600
    return-void
.end method
