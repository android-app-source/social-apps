.class public final Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

.field public final synthetic c:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/0D9;


# direct methods
.method public constructor <init>(LX/0D9;Ljava/util/List;Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 29587
    iput-object p1, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->e:LX/0D9;

    iput-object p2, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->b:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    iput-object p4, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->c:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    iput-object p5, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 29588
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 29589
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 29590
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 29591
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 29592
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 29593
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->e:LX/0D9;

    iget-object v0, v0, LX/0D9;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 29594
    :goto_1
    return-void

    .line 29595
    :cond_2
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->e:LX/0D9;

    iget-boolean v0, v0, LX/0D9;->c:Z

    if-eqz v0, :cond_4

    .line 29596
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->e:LX/0D9;

    iget-object v2, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->b:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    iget-object v3, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->c:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    .line 29597
    iget-object v4, v0, LX/0D9;->e:Landroid/view/View;

    sget v5, LX/0Dz;->j:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 29598
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 29599
    iget-object v5, v0, LX/0D9;->d:Landroid/content/res/Resources;

    sget v6, LX/0E0;->a:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 29600
    new-instance v6, Landroid/graphics/PorterDuffColorFilter;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v5, v7}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 29601
    iget-object v5, v0, LX/0D9;->e:Landroid/view/View;

    sget v7, LX/0Dz;->m:I

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 29602
    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 29603
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 29604
    iget-object v6, v0, LX/0D9;->a:Landroid/app/Activity;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    sget v7, LX/0Dz;->c:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout;

    .line 29605
    sget v7, LX/0Dz;->k:I

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 29606
    invoke-virtual {v5}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29607
    sget v7, LX/0Dz;->l:I

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-static {v0, v7, v5, v2, v3}, LX/0D9;->a(LX/0D9;Landroid/view/View;Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 29608
    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 29609
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->e:LX/0D9;

    iget-object v0, v0, LX/0D9;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 29610
    :cond_4
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->e:LX/0D9;

    iget-object v2, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->b:Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;

    iget-object v4, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsAutofillBarHolder$2;->c:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    .line 29611
    const/4 v5, 0x0

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 29612
    iget-object v6, v0, LX/0D9;->e:Landroid/view/View;

    sget v7, LX/0Dz;->i:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 29613
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29614
    iget-object v6, v0, LX/0D9;->e:Landroid/view/View;

    sget v7, LX/0Dz;->h:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 29615
    invoke-virtual {v5}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29616
    iget-object v6, v0, LX/0D9;->d:Landroid/content/res/Resources;

    sget v7, LX/0E0;->a:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 29617
    new-instance v7, Landroid/graphics/PorterDuffColorFilter;

    sget-object v8, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v7, v6, v8}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 29618
    iget-object v6, v0, LX/0D9;->e:Landroid/view/View;

    sget v8, LX/0Dz;->g:I

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 29619
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 29620
    iget-object v6, v0, LX/0D9;->e:Landroid/view/View;

    sget v7, LX/0Dz;->e:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 29621
    invoke-static {v0, v6, v5, v3, v4}, LX/0D9;->a(LX/0D9;Landroid/view/View;Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 29622
    goto :goto_3
.end method
