.class public final Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0DD;


# direct methods
.method public constructor <init>(LX/0DD;)V
    .locals 0

    .prologue
    .line 29656
    iput-object p1, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$1;->a:LX/0DD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 29657
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$1;->a:LX/0DD;

    iget-object v0, v0, LX/0DD;->b:Landroid/view/View;

    const v1, 0x7f0d07bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 29658
    sget v1, LX/0Dz;->d:I

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 29659
    iget-object v1, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$1;->a:LX/0DD;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;

    .line 29660
    iput-object v0, v1, LX/0DD;->d:Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;

    .line 29661
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$1;->a:LX/0DD;

    iget-boolean v0, v0, LX/0DD;->e:Z

    if-eqz v0, :cond_0

    .line 29662
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$1;->a:LX/0DD;

    iget-object v0, v0, LX/0DD;->d:Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->setInfoVisibility(I)V

    .line 29663
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$1;->a:LX/0DD;

    iget-object v0, v0, LX/0DD;->d:Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;

    invoke-virtual {v0, v2}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->setDetailsTextVisibility(I)V

    .line 29664
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$1;->a:LX/0DD;

    iget-object v0, v0, LX/0DD;->d:Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;

    invoke-virtual {v0, v2}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->setVisibility(I)V

    .line 29665
    return-void
.end method
