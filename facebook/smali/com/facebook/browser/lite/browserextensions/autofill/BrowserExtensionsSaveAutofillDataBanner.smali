.class public Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field public b:Landroid/widget/LinearLayout;

.field private c:Landroid/widget/LinearLayout;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29723
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29724
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29721
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29722
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 29714
    sget v0, LX/0Dz;->n:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->a:Landroid/widget/TextView;

    .line 29715
    sget v0, LX/0Dz;->p:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->b:Landroid/widget/LinearLayout;

    .line 29716
    sget v0, LX/0Dz;->o:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->c:Landroid/widget/LinearLayout;

    .line 29717
    sget v0, LX/0Dz;->q:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->d:Landroid/widget/Button;

    .line 29718
    sget v0, LX/0Dz;->r:I

    invoke-virtual {p0, v0}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->e:Landroid/widget/Button;

    .line 29719
    invoke-static {p0}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->b$redex0(Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;)V

    .line 29720
    return-void
.end method

.method public static b$redex0(Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;)V
    .locals 2

    .prologue
    .line 29694
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->b:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 29695
    :cond_0
    :goto_0
    return-void

    .line 29696
    :cond_1
    iget-object v1, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->a:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    sget v0, LX/0Dz;->y:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 29697
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->a:Landroid/widget/TextView;

    new-instance v1, LX/0DE;

    invoke-direct {v1, p0}, LX/0DE;-><init>(Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 29698
    :cond_2
    sget v0, LX/0Dz;->x:I

    goto :goto_1
.end method


# virtual methods
.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x66577b0a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 29711
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 29712
    invoke-direct {p0}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->a()V

    .line 29713
    const/16 v1, 0x2d

    const v2, -0x1bfa321d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAutofillData(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29725
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 29726
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 29727
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 29728
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f081c93

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 29729
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29730
    invoke-virtual {p0}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, LX/0Dz;->z:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 29731
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 29732
    :cond_0
    return-void
.end method

.method public setDetailsTextVisibility(I)V
    .locals 1

    .prologue
    .line 29708
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    if-eq p1, v0, :cond_0

    if-nez p1, :cond_1

    .line 29709
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 29710
    :cond_1
    return-void
.end method

.method public setInfoVisibility(I)V
    .locals 1

    .prologue
    .line 29705
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->b:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    if-eq p1, v0, :cond_0

    if-nez p1, :cond_1

    .line 29706
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 29707
    :cond_1
    return-void
.end method

.method public setOnAcceptListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 29702
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->d:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 29703
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->d:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29704
    :cond_0
    return-void
.end method

.method public setOnDeclineListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 29699
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->e:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 29700
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->e:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29701
    :cond_0
    return-void
.end method
