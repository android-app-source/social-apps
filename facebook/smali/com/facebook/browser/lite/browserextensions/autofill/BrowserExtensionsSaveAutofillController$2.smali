.class public final Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

.field public final synthetic c:LX/0DD;


# direct methods
.method public constructor <init>(LX/0DD;Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 0

    .prologue
    .line 29680
    iput-object p1, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;->c:LX/0DD;

    iput-object p2, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;->b:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 29675
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;->c:LX/0DD;

    iget-object v0, v0, LX/0DD;->d:Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;

    iget-object v1, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->setAutofillData(Ljava/util/List;)V

    .line 29676
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;->c:LX/0DD;

    iget-object v0, v0, LX/0DD;->d:Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;

    new-instance v1, LX/0DB;

    invoke-direct {v1, p0}, LX/0DB;-><init>(Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;)V

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->setOnAcceptListener(Landroid/view/View$OnClickListener;)V

    .line 29677
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;->c:LX/0DD;

    iget-object v0, v0, LX/0DD;->d:Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;

    new-instance v1, LX/0DC;

    invoke-direct {v1, p0}, LX/0DC;-><init>(Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;)V

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->setOnDeclineListener(Landroid/view/View$OnClickListener;)V

    .line 29678
    iget-object v0, p0, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillController$2;->c:LX/0DD;

    iget-object v0, v0, LX/0DD;->d:Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/browserextensions/autofill/BrowserExtensionsSaveAutofillDataBanner;->setVisibility(I)V

    .line 29679
    return-void
.end method
