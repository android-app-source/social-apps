.class public Lcom/facebook/base/init/GenericLogoSplashScreenActivity;
.super Lcom/facebook/base/app/SplashScreenActivity;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public f:LX/02G;

.field public final g:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 2600
    invoke-direct {p0}, Lcom/facebook/base/app/SplashScreenActivity;-><init>()V

    .line 2601
    iput p1, p0, Lcom/facebook/base/init/GenericLogoSplashScreenActivity;->g:I

    .line 2602
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v4, -0x1

    const/16 v0, 0x22

    const v1, 0x4be7a03d    # 3.0359674E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2603
    invoke-super {p0, p1}, Lcom/facebook/base/app/SplashScreenActivity;->onCreate(Landroid/os/Bundle;)V

    .line 2604
    invoke-virtual {p0}, Lcom/facebook/base/init/GenericLogoSplashScreenActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2605
    const/16 v0, 0x23

    const v2, -0x77706959

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2606
    :goto_0
    return-void

    .line 2607
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/base/init/GenericLogoSplashScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/facebook/base/init/GenericLogoSplashScreenActivity;->g:I

    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2608
    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-direct {v2, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2609
    new-instance v3, LX/02G;

    invoke-direct {v3, p0, v0}, LX/02G;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    iput-object v3, p0, Lcom/facebook/base/init/GenericLogoSplashScreenActivity;->f:LX/02G;

    .line 2610
    iget-object v0, p0, Lcom/facebook/base/init/GenericLogoSplashScreenActivity;->f:LX/02G;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2611
    invoke-virtual {p0, v2}, Lcom/facebook/base/init/GenericLogoSplashScreenActivity;->setContentView(Landroid/view/View;)V

    .line 2612
    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2613
    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2614
    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2615
    iget-object v0, p0, Lcom/facebook/base/init/GenericLogoSplashScreenActivity;->f:LX/02G;

    invoke-virtual {v0}, LX/02G;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2616
    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2617
    const v0, 0x49372a3b

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x6096bf91

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2618
    iget-object v1, p0, Lcom/facebook/base/init/GenericLogoSplashScreenActivity;->f:LX/02G;

    if-eqz v1, :cond_0

    .line 2619
    iget-object v1, p0, Lcom/facebook/base/init/GenericLogoSplashScreenActivity;->f:LX/02G;

    invoke-virtual {v1}, LX/02H;->a()V

    .line 2620
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/app/SplashScreenActivity;->onDestroy()V

    .line 2621
    const/16 v1, 0x23

    const v2, -0x1ddccdc6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
