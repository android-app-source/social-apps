.class public Lcom/facebook/base/app/SplashScreenActivity;
.super Landroid/app/Activity;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "StringFormatUse",
        "BadMethodUse-android.util.Log.v",
        "BadMethodUse-android.util.Log.d",
        "BadMethodUse-android.util.Log.i",
        "BadMethodUse-android.util.Log.w",
        "BadMethodUse-android.util.Log.e"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public c:J

.field public d:J

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2676
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 2677
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2673
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenActivity;->finish()V

    .line 2674
    const/4 v0, 0x0

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/facebook/base/app/SplashScreenActivity;->overridePendingTransition(II)V

    .line 2675
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 2672
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 3

    .prologue
    .line 2622
    invoke-super {p0}, Landroid/app/Activity;->onAttachedToWindow()V

    .line 2623
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 2624
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 2625
    invoke-static {v0, p0}, LX/02I;->a(Landroid/view/ViewTreeObserver;Lcom/facebook/base/app/SplashScreenActivity;)V

    .line 2626
    :goto_0
    return-void

    .line 2627
    :cond_0
    new-instance v1, LX/0AQ;

    invoke-direct {v1, p0, v0}, LX/0AQ;-><init>(Lcom/facebook/base/app/SplashScreenActivity;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 2668
    const v0, 0x3f59cb94

    invoke-virtual {p0, v0}, Lcom/facebook/base/app/SplashScreenActivity;->setResult(I)V

    .line 2669
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 2670
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/000;

    const/4 v1, 0x7

    invoke-virtual {v0, p0, v1}, LX/000;->a(Lcom/facebook/base/app/SplashScreenActivity;I)V

    .line 2671
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/16 v0, 0x22

    const v1, -0x3e80cdf0

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2659
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 2660
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2661
    const-string v2, "com.facebook.base.app.splashId"

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/base/app/SplashScreenActivity;->c:J

    .line 2662
    const-string v2, "com.facebook.base.app.rhaId"

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/base/app/SplashScreenActivity;->d:J

    .line 2663
    new-array v0, v6, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenActivity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    iget-wide v2, p0, Lcom/facebook/base/app/SplashScreenActivity;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v7

    .line 2664
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/000;

    invoke-virtual {v0, p0}, LX/000;->a(Lcom/facebook/base/app/SplashScreenActivity;)V

    .line 2665
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2666
    iput v7, p0, Lcom/facebook/base/app/SplashScreenActivity;->e:I

    .line 2667
    :cond_0
    const/16 v0, 0x23

    const v2, 0x77c88733

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDestroy()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x2

    const/16 v0, 0x22

    const v1, -0x4fe64053

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2653
    iput v4, p0, Lcom/facebook/base/app/SplashScreenActivity;->e:I

    .line 2654
    new-array v0, v6, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/facebook/base/app/SplashScreenActivity;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v4

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenActivity;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    .line 2655
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/000;

    .line 2656
    iget-object v2, v0, LX/000;->y:Ljava/util/ArrayList;

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2657
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 2658
    const/16 v0, 0x23

    const v2, -0x8c4587f

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x22

    const v1, 0x64aaf9a0

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2648
    iput v6, p0, Lcom/facebook/base/app/SplashScreenActivity;->e:I

    .line 2649
    new-array v0, v6, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenActivity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenActivity;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    .line 2650
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/000;

    const/4 v2, 0x4

    invoke-virtual {v0, p0, v2}, LX/000;->a(Lcom/facebook/base/app/SplashScreenActivity;I)V

    .line 2651
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 2652
    const/16 v0, 0x23

    const v2, 0x283c36dc

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestart()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x22

    const v1, 0x37caed1

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2644
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 2645
    new-array v0, v6, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenActivity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenActivity;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    .line 2646
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/000;

    const/4 v2, 0x6

    invoke-virtual {v0, p0, v2}, LX/000;->a(Lcom/facebook/base/app/SplashScreenActivity;I)V

    .line 2647
    const/16 v0, 0x23

    const v2, 0x7fb62eef

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/16 v0, 0x22

    const v1, -0x5ea6dbf3

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2639
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 2640
    new-array v0, v6, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenActivity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenActivity;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    .line 2641
    iput v7, p0, Lcom/facebook/base/app/SplashScreenActivity;->e:I

    .line 2642
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/000;

    invoke-virtual {v0, p0, v7}, LX/000;->a(Lcom/facebook/base/app/SplashScreenActivity;I)V

    .line 2643
    const/16 v0, 0x23

    const v2, -0x5d1ad8e9

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x22

    const v1, 0x1c22ce20

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2634
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 2635
    new-array v0, v6, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenActivity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenActivity;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    .line 2636
    iput v6, p0, Lcom/facebook/base/app/SplashScreenActivity;->e:I

    .line 2637
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/000;

    invoke-virtual {v0, p0, v6}, LX/000;->a(Lcom/facebook/base/app/SplashScreenActivity;I)V

    .line 2638
    const/16 v0, 0x23

    const v2, -0x68ce68e1

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v0, 0x22

    const v1, -0x5bd49d68

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2628
    iput v6, p0, Lcom/facebook/base/app/SplashScreenActivity;->e:I

    .line 2629
    new-array v0, v7, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenActivity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    iget-wide v2, p0, Lcom/facebook/base/app/SplashScreenActivity;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v6

    .line 2630
    iput-boolean v6, p0, Lcom/facebook/base/app/SplashScreenActivity;->b:Z

    .line 2631
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 2632
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/000;

    const/4 v2, 0x5

    invoke-virtual {v0, p0, v2}, LX/000;->a(Lcom/facebook/base/app/SplashScreenActivity;I)V

    .line 2633
    const/16 v0, 0x23

    const v2, -0x14701606

    invoke-static {v7, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
