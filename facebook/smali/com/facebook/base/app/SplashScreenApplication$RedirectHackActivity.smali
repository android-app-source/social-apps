.class public final Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;
.super Landroid/app/Activity;
.source ""


# instance fields
.field public a:Landroid/os/Bundle;

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field public c:J

.field public d:Z

.field public e:Z

.field public final synthetic f:LX/000;


# direct methods
.method public constructor <init>(LX/000;)V
    .locals 0

    .prologue
    .line 5896
    iput-object p1, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->f:LX/000;

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 5848
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 5849
    const v0, 0x2e21e1eb

    if-ne p1, v0, :cond_0

    const v0, 0x3f59cb94

    if-ne p2, v0, :cond_0

    .line 5850
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->onBackPressed()V

    .line 5851
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v4, 0x0

    const/16 v0, 0x22

    const v1, 0xd848113

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 5877
    const/4 v1, 0x0

    invoke-super {p0, v1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 5878
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->isTaskRoot()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 5879
    if-eqz v1, :cond_0

    .line 5880
    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->finish()V

    .line 5881
    :cond_0
    iget-object v1, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->f:LX/000;

    invoke-virtual {v1}, LX/000;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->c:J

    .line 5882
    new-array v1, v8, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    .line 5883
    iget-object v1, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->f:LX/000;

    iget-object v1, v1, LX/000;->z:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5884
    invoke-virtual {p0, v4}, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->setVisible(Z)V

    .line 5885
    iget-object v1, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->f:LX/000;

    invoke-virtual {v1}, LX/000;->j()J

    move-result-wide v2

    .line 5886
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->f:LX/000;

    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/000;->a(Landroid/content/Intent;)Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5887
    const-string v4, "com.facebook.showSplashScreen"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 5888
    const/high16 v4, 0x40000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 5889
    const-string v4, "com.facebook.base.app.originalIntent"

    invoke-virtual {p0}, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 5890
    const-string v4, "com.facebook.base.app.rhaId"

    iget-wide v6, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->c:J

    invoke-virtual {v1, v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 5891
    const-string v4, "com.facebook.base.app.splashId"

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 5892
    iget-object v4, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->f:LX/000;

    iget-object v4, v4, LX/000;->x:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5893
    const v2, 0x2e21e1eb

    invoke-virtual {p0, v1, v2}, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 5894
    iput-boolean v8, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->d:Z

    .line 5895
    const/16 v1, 0x23

    const v2, -0x4893679c

    invoke-static {v9, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v0, 0x22

    const v1, 0x53f54a83

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 5872
    new-array v1, v6, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 5873
    iput-boolean v6, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->e:Z

    .line 5874
    iget-object v1, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->f:LX/000;

    iget-object v1, v1, LX/000;->z:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 5875
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 5876
    const/16 v1, 0x23

    const v2, -0x7045fc3e

    invoke-static {v7, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onNewIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 5866
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 5867
    iget-object v0, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 5868
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->b:Ljava/util/ArrayList;

    .line 5869
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 5870
    iget-object v0, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5871
    return-void
.end method

.method public final onPause()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x22

    const v1, -0x6d9892ac

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 5863
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 5864
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 5865
    const/16 v1, 0x23

    const v2, -0x5c76c0e9

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x22

    const v1, 0x3c96d4e8

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 5860
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 5861
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 5862
    const/16 v1, 0x23

    const v2, 0x54b63c0b

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x22

    const v1, 0x4ba032b0    # 2.0997472E7f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 5856
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 5857
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    .line 5858
    iput-boolean v4, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->d:Z

    .line 5859
    const/16 v1, 0x23

    const v2, -0x2618efb4

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/16 v0, 0x22

    const v1, -0x55de4cd4

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 5852
    iput-boolean v2, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->d:Z

    .line 5853
    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/facebook/base/app/SplashScreenApplication$RedirectHackActivity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 5854
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 5855
    const/16 v1, 0x23

    const v2, -0x5c9cc51f

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
