.class public final Lcom/google/android/exoplayer/ext/opus/OpusDecoder;
.super Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/exoplayer/util/extensions/SimpleDecoder",
        "<",
        "LX/08M;",
        "LX/0M7;",
        "LX/0M4;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Z


# instance fields
.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:J

.field private f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45083
    :try_start_0
    const-string v0, "opus"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 45084
    const-string v0, "opusJNI"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 45085
    const/4 v0, 0x1

    .line 45086
    :goto_0
    sput-boolean v0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->a:Z

    .line 45087
    return-void

    .line 45088
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(IIILjava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    const/16 v6, 0xa

    const/16 v9, 0x8

    const/4 v10, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 45089
    new-array v0, p1, [LX/08M;

    new-array v3, p2, [LX/0M7;

    invoke-direct {p0, v0, v3}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;-><init>([LX/08M;[LX/0M6;)V

    .line 45090
    invoke-interface {p4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 45091
    array-length v3, v0

    const/16 v4, 0x13

    if-ge v3, v4, :cond_0

    .line 45092
    new-instance v0, LX/0M4;

    const-string v1, "Header size is too small."

    invoke-direct {v0, v1}, LX/0M4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45093
    :cond_0
    const/16 v3, 0x9

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    iput v3, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->b:I

    .line 45094
    invoke-static {v0, v6}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->a([BI)I

    move-result v7

    .line 45095
    const/16 v3, 0x10

    invoke-static {v0, v3}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->a([BI)I

    move-result v5

    .line 45096
    new-array v6, v6, [B

    .line 45097
    const/16 v3, 0x12

    aget-byte v3, v0, v3

    if-nez v3, :cond_5

    .line 45098
    iget v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->b:I

    if-le v0, v10, :cond_1

    .line 45099
    new-instance v0, LX/0M4;

    const-string v1, "Invalid Header, missing stream map."

    invoke-direct {v0, v1}, LX/0M4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45100
    :cond_1
    iget v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->b:I

    if-ne v0, v10, :cond_4

    move v0, v1

    .line 45101
    :goto_0
    aput-byte v2, v6, v2

    .line 45102
    aput-byte v1, v6, v1

    move v4, v0

    move v3, v1

    .line 45103
    :cond_2
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_8

    .line 45104
    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    array-length v0, v0

    if-ne v0, v9, :cond_3

    invoke-interface {p4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    array-length v0, v0

    if-eq v0, v9, :cond_7

    .line 45105
    :cond_3
    new-instance v0, LX/0M4;

    const-string v1, "Invalid Codec Delay or Seek Preroll"

    invoke-direct {v0, v1}, LX/0M4;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v2

    .line 45106
    goto :goto_0

    .line 45107
    :cond_5
    array-length v3, v0

    iget v4, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->b:I

    add-int/lit8 v4, v4, 0x15

    if-ge v3, v4, :cond_6

    .line 45108
    new-instance v0, LX/0M4;

    const-string v1, "Header size is too small."

    invoke-direct {v0, v1}, LX/0M4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45109
    :cond_6
    const/16 v3, 0x13

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    .line 45110
    const/16 v4, 0x14

    aget-byte v4, v0, v4

    and-int/lit16 v4, v4, 0xff

    .line 45111
    :goto_1
    iget v8, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->b:I

    if-ge v2, v8, :cond_2

    .line 45112
    add-int/lit8 v8, v2, 0x15

    aget-byte v8, v0, v8

    aput-byte v8, v6, v2

    .line 45113
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 45114
    :cond_7
    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v8

    .line 45115
    invoke-interface {p4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    .line 45116
    invoke-static {v8, v9}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->a(J)I

    move-result v2

    iput v2, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->c:I

    .line 45117
    invoke-static {v0, v1}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->a(J)I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->d:I

    .line 45118
    :goto_2
    const v1, 0xbb80

    iget v2, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->b:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->opusInit(IIIII[B)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->e:J

    .line 45119
    iget-wide v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 45120
    new-instance v0, LX/0M4;

    const-string v1, "Failed to initialize decoder"

    invoke-direct {v0, v1}, LX/0M4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45121
    :cond_8
    iput v7, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->c:I

    .line 45122
    const/16 v0, 0xf00

    iput v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->d:I

    goto :goto_2

    .line 45123
    :cond_9
    const/4 v1, 0x0

    .line 45124
    iget v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f:I

    iget-object v2, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->d:[LX/08M;

    array-length v2, v2

    if-ne v0, v2, :cond_a

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45125
    :goto_4
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->d:[LX/08M;

    array-length v0, v0

    if-ge v1, v0, :cond_b

    .line 45126
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->d:[LX/08M;

    aget-object v0, v0, v1

    iget-object v0, v0, LX/08M;->a:LX/0L7;

    invoke-virtual {v0, p3}, LX/0L7;->a(I)V

    .line 45127
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_a
    move v0, v1

    .line 45128
    goto :goto_3

    .line 45129
    :cond_b
    return-void
.end method

.method private static a(J)I
    .locals 4

    .prologue
    .line 45082
    const-wide/32 v0, 0xbb80

    mul-long/2addr v0, p0

    const-wide/32 v2, 0x3b9aca00

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static a([BI)I
    .locals 2

    .prologue
    .line 45130
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    .line 45131
    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    .line 45132
    return v0
.end method

.method private a(LX/08M;LX/0M7;)LX/0M4;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 45133
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/0M5;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45134
    iget-wide v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->e:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->opusReset(J)V

    .line 45135
    iget-object v0, p1, LX/08M;->a:LX/0L7;

    iget-wide v0, v0, LX/0L7;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->c:I

    :goto_0
    iput v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->f:I

    .line 45136
    :cond_0
    iget-object v0, p1, LX/08M;->a:LX/0L7;

    .line 45137
    iget-wide v2, v0, LX/0L7;->e:J

    iput-wide v2, p2, LX/0M7;->a:J

    .line 45138
    iget-object v1, v0, LX/0L7;->b:Ljava/nio/ByteBuffer;

    iget-object v2, v0, LX/0L7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    iget v3, v0, LX/0L7;->c:I

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 45139
    iget-object v1, v0, LX/0L7;->b:Ljava/nio/ByteBuffer;

    iget v2, v0, LX/0L7;->c:I

    const v3, 0xbb80

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->opusGetRequiredOutputBufferSize(Ljava/nio/ByteBuffer;II)I

    move-result v1

    .line 45140
    if-gez v1, :cond_2

    .line 45141
    new-instance v0, LX/0M4;

    const-string v1, "Error when computing required output buffer size."

    invoke-direct {v0, v1}, LX/0M4;-><init>(Ljava/lang/String;)V

    .line 45142
    :goto_1
    return-object v0

    .line 45143
    :cond_1
    iget v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->d:I

    goto :goto_0

    .line 45144
    :cond_2
    iget-object v2, p2, LX/0M7;->b:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_3

    iget-object v2, p2, LX/0M7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    if-ge v2, v1, :cond_4

    .line 45145
    :cond_3
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, p2, LX/0M7;->b:Ljava/nio/ByteBuffer;

    .line 45146
    :cond_4
    iget-object v2, p2, LX/0M7;->b:Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 45147
    iget-object v2, p2, LX/0M7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 45148
    iget-wide v2, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->e:J

    iget-object v4, v0, LX/0L7;->b:Ljava/nio/ByteBuffer;

    iget v5, v0, LX/0L7;->c:I

    iget-object v6, p2, LX/0M7;->b:Ljava/nio/ByteBuffer;

    iget-object v0, p2, LX/0M7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v7

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->opusDecode(JLjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;I)I

    move-result v1

    .line 45149
    if-gez v1, :cond_5

    .line 45150
    new-instance v0, LX/0M4;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Decode error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->opusGetErrorMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0M4;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 45151
    :cond_5
    iget-object v0, p2, LX/0M7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 45152
    iget-object v0, p2, LX/0M7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 45153
    iget v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->f:I

    if-lez v0, :cond_6

    .line 45154
    iget v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->b:I

    mul-int/lit8 v0, v0, 0x2

    .line 45155
    iget v2, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->f:I

    mul-int/2addr v2, v0

    .line 45156
    if-gt v1, v2, :cond_7

    .line 45157
    iget v2, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->f:I

    div-int v0, v1, v0

    sub-int v0, v2, v0

    iput v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->f:I

    .line 45158
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, LX/0M5;->a(I)V

    .line 45159
    iget-object v0, p2, LX/0M7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 45160
    :cond_6
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    .line 45161
    :cond_7
    iput v8, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->f:I

    .line 45162
    iget-object v0, p2, LX/0M7;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_2
.end method

.method private g()LX/0M7;
    .locals 1

    .prologue
    .line 45163
    new-instance v0, LX/0M7;

    invoke-direct {v0, p0}, LX/0M7;-><init>(Lcom/google/android/exoplayer/ext/opus/OpusDecoder;)V

    return-object v0
.end method

.method public static native getLibopusVersion()Ljava/lang/String;
.end method

.method private native opusClose(J)V
.end method

.method private native opusDecode(JLjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;I)I
.end method

.method private native opusGetErrorMessage(I)Ljava/lang/String;
.end method

.method private native opusGetRequiredOutputBufferSize(Ljava/nio/ByteBuffer;II)I
.end method

.method private native opusInit(IIIII[B)J
.end method

.method private native opusReset(J)V
.end method


# virtual methods
.method public final bridge synthetic a(LX/08M;LX/0M6;)Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 45081
    check-cast p2, LX/0M7;

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->a(LX/08M;LX/0M7;)LX/0M4;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/0M6;)V
    .locals 0

    .prologue
    .line 45078
    check-cast p1, LX/0M7;

    .line 45079
    invoke-super {p0, p1}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a(LX/0M6;)V

    .line 45080
    return-void
.end method

.method public final a(LX/0M7;)V
    .locals 0

    .prologue
    .line 45076
    invoke-super {p0, p1}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a(LX/0M6;)V

    .line 45077
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 45073
    invoke-super {p0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->d()V

    .line 45074
    iget-wide v0, p0, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->e:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->opusClose(J)V

    .line 45075
    return-void
.end method

.method public final e()LX/08M;
    .locals 1

    .prologue
    .line 45072
    new-instance v0, LX/08M;

    invoke-direct {v0}, LX/08M;-><init>()V

    return-object v0
.end method

.method public final synthetic f()LX/0M6;
    .locals 1

    .prologue
    .line 45071
    invoke-direct {p0}, Lcom/google/android/exoplayer/ext/opus/OpusDecoder;->g()LX/0M7;

    move-result-object v0

    return-object v0
.end method
