.class public final Lcom/google/android/exoplayer/upstream/Loader$LoadTask;
.super Landroid/os/Handler;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0ON;

.field public final b:LX/0LO;

.field private final c:LX/0LX;

.field public volatile d:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(LX/0ON;Landroid/os/Looper;LX/0LO;LX/0LX;)V
    .locals 0

    .prologue
    .line 52957
    iput-object p1, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->a:LX/0ON;

    .line 52958
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 52959
    iput-object p3, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->b:LX/0LO;

    .line 52960
    iput-object p4, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->c:LX/0LX;

    .line 52961
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 52945
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 52946
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 52947
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->a:LX/0ON;

    const/4 v1, 0x0

    .line 52948
    iput-boolean v1, v0, LX/0ON;->c:Z

    .line 52949
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->a:LX/0ON;

    const/4 v1, 0x0

    .line 52950
    iput-object v1, v0, LX/0ON;->b:Lcom/google/android/exoplayer/upstream/Loader$LoadTask;

    .line 52951
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->b:LX/0LO;

    invoke-interface {v0}, LX/0LO;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52952
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->c:LX/0LX;

    invoke-interface {v0}, LX/0LX;->f()V

    .line 52953
    :goto_0
    return-void

    .line 52954
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 52955
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->c:LX/0LX;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->b:LX/0LO;

    invoke-interface {v0, v1}, LX/0LX;->a(LX/0LO;)V

    goto :goto_0

    .line 52956
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->c:LX/0LX;

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->b:LX/0LO;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/io/IOException;

    invoke-interface {v1, v2, v0}, LX/0LX;->a(LX/0LO;Ljava/io/IOException;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 52927
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->d:Ljava/lang/Thread;

    .line 52928
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->b:LX/0LO;

    invoke-interface {v0}, LX/0LO;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52929
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->b:LX/0LO;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".load()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/08K;->a(Ljava/lang/String;)V

    .line 52930
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->b:LX/0LO;

    invoke-interface {v0}, LX/0LO;->h()V

    .line 52931
    invoke-static {}, LX/08K;->a()V

    .line 52932
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_3

    .line 52933
    :goto_0
    return-void

    .line 52934
    :catch_0
    move-exception v0

    .line 52935
    invoke-virtual {p0, v3, v0}, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 52936
    :catch_1
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->b:LX/0LO;

    invoke-interface {v0}, LX/0LO;->g()Z

    move-result v0

    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 52937
    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 52938
    :catch_2
    move-exception v0

    .line 52939
    const-string v1, "LoadTask"

    const-string v2, "Unexpected exception loading stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 52940
    new-instance v1, LX/0OM;

    invoke-direct {v1, v0}, LX/0OM;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {p0, v3, v1}, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 52941
    :catch_3
    move-exception v0

    .line 52942
    const-string v1, "LoadTask"

    const-string v2, "Unexpected error loading stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 52943
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/exoplayer/upstream/Loader$LoadTask;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 52944
    throw v0
.end method
