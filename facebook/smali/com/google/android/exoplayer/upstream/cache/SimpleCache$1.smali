.class public final Lcom/google/android/exoplayer/upstream/cache/SimpleCache$1;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field public final synthetic a:Landroid/os/ConditionVariable;

.field public final synthetic b:LX/0OV;


# direct methods
.method public constructor <init>(LX/0OV;Landroid/os/ConditionVariable;)V
    .locals 0

    .prologue
    .line 53164
    iput-object p1, p0, Lcom/google/android/exoplayer/upstream/cache/SimpleCache$1;->b:LX/0OV;

    iput-object p2, p0, Lcom/google/android/exoplayer/upstream/cache/SimpleCache$1;->a:Landroid/os/ConditionVariable;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 53165
    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/SimpleCache$1;->b:LX/0OV;

    monitor-enter v1

    .line 53166
    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/SimpleCache$1;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 53167
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/SimpleCache$1;->b:LX/0OV;

    .line 53168
    iget-object v2, v0, LX/0OV;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 53169
    iget-object v2, v0, LX/0OV;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 53170
    :cond_0
    iget-object v2, v0, LX/0OV;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 53171
    if-nez v3, :cond_2

    .line 53172
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 53173
    :cond_2
    const/4 v2, 0x0

    :goto_0
    array-length v4, v3

    if-ge v2, v4, :cond_1

    .line 53174
    aget-object v4, v3, v2

    .line 53175
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_3

    .line 53176
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 53177
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 53178
    :cond_3
    invoke-static {v4}, LX/0OT;->b(Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    .line 53179
    invoke-static {v4}, LX/0OT;->a(Ljava/io/File;)LX/0OT;

    move-result-object v5

    .line 53180
    if-nez v5, :cond_4

    .line 53181
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 53182
    :cond_4
    invoke-static {v0, v5}, LX/0OV;->e(LX/0OV;LX/0OT;)V

    goto :goto_1
.end method
