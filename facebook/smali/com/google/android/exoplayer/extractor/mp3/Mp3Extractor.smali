.class public final Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ME;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# instance fields
.field private final d:J

.field private final e:LX/0Oj;

.field private final f:LX/0Oe;

.field private g:LX/0LU;

.field private h:LX/0LS;

.field private i:I

.field private j:LX/0ML;

.field private k:LX/0MX;

.field private l:J

.field private m:I

.field private n:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46387
    const-string v0, "Xing"

    invoke-static {v0}, LX/08x;->e(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->a:I

    .line 46388
    const-string v0, "Info"

    invoke-static {v0}, LX/08x;->e(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->b:I

    .line 46389
    const-string v0, "VBRI"

    invoke-static {v0}, LX/08x;->e(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 46511
    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;-><init>(J)V

    .line 46512
    return-void
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 46505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46506
    iput-wide p1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->d:J

    .line 46507
    new-instance v0, LX/0Oj;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    .line 46508
    new-instance v0, LX/0Oe;

    invoke-direct {v0}, LX/0Oe;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    .line 46509
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->l:J

    .line 46510
    return-void
.end method

.method private a(LX/0MA;Z)Z
    .locals 11

    .prologue
    const/4 v10, 0x4

    const v9, -0x1f400

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46474
    invoke-interface {p1}, LX/0MA;->a()V

    .line 46475
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_b

    .line 46476
    invoke-static {p1}, LX/0MZ;->a(LX/0MA;)LX/0ML;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->j:LX/0ML;

    .line 46477
    invoke-interface {p1}, LX/0MA;->b()J

    move-result-wide v0

    long-to-int v0, v0

    .line 46478
    if-nez p2, :cond_0

    .line 46479
    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    :cond_0
    move v4, v0

    move v1, v2

    move v5, v2

    move v6, v2

    .line 46480
    :goto_0
    if-eqz p2, :cond_1

    const/16 v0, 0x1000

    if-ne v6, v0, :cond_1

    move v0, v2

    .line 46481
    :goto_1
    return v0

    .line 46482
    :cond_1
    if-nez p2, :cond_2

    const/high16 v0, 0x20000

    if-ne v6, v0, :cond_2

    .line 46483
    new-instance v0, LX/0L6;

    const-string v1, "Searched too many bytes."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46484
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    invoke-interface {p1, v0, v2, v10, v3}, LX/0MA;->b([BIIZ)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 46485
    goto :goto_1

    .line 46486
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    invoke-virtual {v0, v2}, LX/0Oj;->b(I)V

    .line 46487
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->m()I

    move-result v0

    .line 46488
    if-eqz v1, :cond_4

    and-int v7, v0, v9

    and-int v8, v1, v9

    if-ne v7, v8, :cond_5

    :cond_4
    invoke-static {v0}, LX/0Oe;->a(I)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_7

    .line 46489
    :cond_5
    add-int/lit8 v0, v6, 0x1

    .line 46490
    if-eqz p2, :cond_6

    .line 46491
    invoke-interface {p1}, LX/0MA;->a()V

    .line 46492
    add-int v1, v4, v0

    invoke-interface {p1, v1}, LX/0MA;->c(I)V

    move v1, v2

    move v5, v2

    move v6, v0

    goto :goto_0

    .line 46493
    :cond_6
    invoke-interface {p1, v3}, LX/0MA;->b(I)V

    move v1, v2

    move v5, v2

    move v6, v0

    goto :goto_0

    .line 46494
    :cond_7
    add-int/lit8 v5, v5, 0x1

    .line 46495
    if-ne v5, v3, :cond_8

    .line 46496
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    invoke-static {v0, v1}, LX/0Oe;->a(ILX/0Oe;)Z

    .line 46497
    :goto_2
    add-int/lit8 v1, v7, -0x4

    invoke-interface {p1, v1}, LX/0MA;->c(I)V

    move v1, v0

    .line 46498
    goto :goto_0

    .line 46499
    :cond_8
    if-eq v5, v10, :cond_9

    move v0, v1

    goto :goto_2

    .line 46500
    :cond_9
    if-eqz p2, :cond_a

    .line 46501
    add-int v0, v4, v6

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    .line 46502
    :goto_3
    iput v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->i:I

    move v0, v3

    .line 46503
    goto :goto_1

    .line 46504
    :cond_a
    invoke-interface {p1}, LX/0MA;->a()V

    goto :goto_3

    :cond_b
    move v4, v2

    move v1, v2

    move v5, v2

    move v6, v2

    goto :goto_0
.end method

.method private b(LX/0MA;)I
    .locals 12

    .prologue
    const-wide/16 v8, -0x1

    const/4 v4, 0x1

    const/4 v0, -0x1

    const/4 v6, 0x0

    .line 46456
    iget v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->n:I

    if-nez v1, :cond_3

    .line 46457
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->c(LX/0MA;)Z

    move-result v1

    if-nez v1, :cond_1

    move v6, v0

    .line 46458
    :cond_0
    :goto_0
    return v6

    .line 46459
    :cond_1
    iget-wide v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->l:J

    cmp-long v1, v2, v8

    if-nez v1, :cond_2

    .line 46460
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->k:LX/0MX;

    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, LX/0MX;->a(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->l:J

    .line 46461
    iget-wide v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->d:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_2

    .line 46462
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->k:LX/0MX;

    const-wide/16 v2, 0x0

    invoke-interface {v1, v2, v3}, LX/0MX;->a(J)J

    move-result-wide v2

    .line 46463
    iget-wide v8, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->l:J

    iget-wide v10, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->d:J

    sub-long v2, v10, v2

    add-long/2addr v2, v8

    iput-wide v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->l:J

    .line 46464
    :cond_2
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v1, v1, LX/0Oe;->c:I

    iput v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->n:I

    .line 46465
    :cond_3
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->h:LX/0LS;

    iget v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->n:I

    invoke-interface {v1, p1, v2, v4}, LX/0LS;->a(LX/0MA;IZ)I

    move-result v1

    .line 46466
    if-ne v1, v0, :cond_4

    move v6, v0

    .line 46467
    goto :goto_0

    .line 46468
    :cond_4
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->n:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->n:I

    .line 46469
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->n:I

    if-gtz v0, :cond_0

    .line 46470
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->l:J

    iget v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->m:I

    int-to-long v2, v2

    const-wide/32 v8, 0xf4240

    mul-long/2addr v2, v8

    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v5, v5, LX/0Oe;->d:I

    int-to-long v8, v5

    div-long/2addr v2, v8

    add-long/2addr v2, v0

    .line 46471
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->h:LX/0LS;

    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v5, v0, LX/0Oe;->c:I

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    .line 46472
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->m:I

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v1, v1, LX/0Oe;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->m:I

    .line 46473
    iput v6, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->n:I

    goto :goto_0
.end method

.method private c(LX/0MA;)Z
    .locals 6

    .prologue
    const v5, -0x1f400

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 46443
    invoke-interface {p1}, LX/0MA;->a()V

    .line 46444
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    const/4 v3, 0x4

    invoke-interface {p1, v2, v0, v3, v1}, LX/0MA;->b([BIIZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 46445
    :goto_0
    return v0

    .line 46446
    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    invoke-virtual {v2, v0}, LX/0Oj;->b(I)V

    .line 46447
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    invoke-virtual {v2}, LX/0Oj;->m()I

    move-result v2

    .line 46448
    and-int v3, v2, v5

    iget v4, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->i:I

    and-int/2addr v4, v5

    if-ne v3, v4, :cond_1

    .line 46449
    invoke-static {v2}, LX/0Oe;->a(I)I

    move-result v3

    .line 46450
    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 46451
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    invoke-static {v2, v0}, LX/0Oe;->a(ILX/0Oe;)Z

    move v0, v1

    .line 46452
    goto :goto_0

    .line 46453
    :cond_1
    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->i:I

    .line 46454
    invoke-interface {p1, v1}, LX/0MA;->b(I)V

    .line 46455
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->d(LX/0MA;)Z

    move-result v0

    goto :goto_0
.end method

.method private d(LX/0MA;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 46513
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->a(LX/0MA;Z)Z
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 46514
    :goto_0
    return v0

    :catch_0
    goto :goto_0
.end method

.method private e(LX/0MA;)V
    .locals 10

    .prologue
    const/16 v0, 0x24

    const/16 v6, 0x15

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 46410
    new-instance v1, LX/0Oj;

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v2, v2, LX/0Oe;->c:I

    invoke-direct {v1, v2}, LX/0Oj;-><init>(I)V

    .line 46411
    iget-object v2, v1, LX/0Oj;->a:[B

    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v3, v3, LX/0Oe;->c:I

    invoke-interface {p1, v2, v9, v3}, LX/0MA;->c([BII)V

    .line 46412
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v2

    .line 46413
    invoke-interface {p1}, LX/0MA;->d()J

    move-result-wide v4

    .line 46414
    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v7, v7, LX/0Oe;->a:I

    and-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v7, v7, LX/0Oe;->e:I

    if-eq v7, v8, :cond_0

    move v6, v0

    .line 46415
    :cond_0
    :goto_0
    invoke-virtual {v1, v6}, LX/0Oj;->b(I)V

    .line 46416
    invoke-virtual {v1}, LX/0Oj;->m()I

    move-result v7

    .line 46417
    sget v8, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->a:I

    if-eq v7, v8, :cond_1

    sget v8, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->b:I

    if-ne v7, v8, :cond_6

    .line 46418
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    invoke-static/range {v0 .. v5}, LX/0Mb;->a(LX/0Oe;LX/0Oj;JJ)LX/0Mb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->k:LX/0MX;

    .line 46419
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->k:LX/0MX;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->j:LX/0ML;

    if-nez v0, :cond_2

    .line 46420
    invoke-interface {p1}, LX/0MA;->a()V

    .line 46421
    add-int/lit16 v0, v6, 0x8d

    invoke-interface {p1, v0}, LX/0MA;->c(I)V

    .line 46422
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    const/4 v1, 0x3

    invoke-interface {p1, v0, v9, v1}, LX/0MA;->c([BII)V

    .line 46423
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    invoke-virtual {v0, v9}, LX/0Oj;->b(I)V

    .line 46424
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->j()I

    move-result v0

    .line 46425
    shr-int/lit8 v2, v0, 0xc

    .line 46426
    and-int/lit16 v3, v0, 0xfff

    .line 46427
    if-nez v2, :cond_7

    if-nez v3, :cond_7

    const/4 v1, 0x0

    :goto_1
    move-object v0, v1

    .line 46428
    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->j:LX/0ML;

    .line 46429
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v0, v0, LX/0Oe;->c:I

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    .line 46430
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->k:LX/0MX;

    if-nez v0, :cond_4

    .line 46431
    invoke-interface {p1}, LX/0MA;->a()V

    .line 46432
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    const/4 v1, 0x4

    invoke-interface {p1, v0, v9, v1}, LX/0MA;->c([BII)V

    .line 46433
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    invoke-virtual {v0, v9}, LX/0Oj;->b(I)V

    .line 46434
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->m()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    invoke-static {v0, v1}, LX/0Oe;->a(ILX/0Oe;)Z

    .line 46435
    new-instance v0, LX/0MY;

    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v3, v3, LX/0Oe;->f:I

    invoke-direct/range {v0 .. v5}, LX/0MY;-><init>(JIJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->k:LX/0MX;

    .line 46436
    :cond_4
    return-void

    .line 46437
    :cond_5
    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v7, v7, LX/0Oe;->e:I

    if-ne v7, v8, :cond_0

    const/16 v6, 0xd

    goto/16 :goto_0

    .line 46438
    :cond_6
    invoke-virtual {v1, v0}, LX/0Oj;->b(I)V

    .line 46439
    invoke-virtual {v1}, LX/0Oj;->m()I

    move-result v0

    .line 46440
    sget v6, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->c:I

    if-ne v0, v6, :cond_3

    .line 46441
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    invoke-static/range {v0 .. v5}, LX/0Ma;->a(LX/0Oe;LX/0Oj;JJ)LX/0Ma;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->k:LX/0MX;

    .line 46442
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v0, v0, LX/0Oe;->c:I

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    goto :goto_2

    :cond_7
    new-instance v1, LX/0ML;

    invoke-direct {v1, v2, v3}, LX/0ML;-><init>(II)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0MA;LX/0MM;)I
    .locals 10

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    .line 46400
    iget v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->i:I

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->d(LX/0MA;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 46401
    :goto_0
    return v2

    .line 46402
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->k:LX/0MX;

    if-nez v1, :cond_2

    .line 46403
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->e(LX/0MA;)V

    .line 46404
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->g:LX/0LU;

    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->k:LX/0MX;

    invoke-interface {v1, v3}, LX/0LU;->a(LX/0M8;)V

    .line 46405
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget-object v1, v1, LX/0Oe;->b:Ljava/lang/String;

    const/16 v3, 0x1000

    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->k:LX/0MX;

    invoke-interface {v4}, LX/0MX;->b()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v6, v6, LX/0Oe;->e:I

    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->f:LX/0Oe;

    iget v7, v7, LX/0Oe;->d:I

    move-object v8, v0

    move-object v9, v0

    invoke-static/range {v0 .. v9}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v0

    .line 46406
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->j:LX/0ML;

    if-eqz v1, :cond_1

    .line 46407
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->j:LX/0ML;

    iget v1, v1, LX/0ML;->a:I

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->j:LX/0ML;

    iget v2, v2, LX/0ML;->b:I

    invoke-virtual {v0, v1, v2}, LX/0L4;->b(II)LX/0L4;

    move-result-object v0

    .line 46408
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->h:LX/0LS;

    invoke-interface {v1, v0}, LX/0LS;->a(LX/0L4;)V

    .line 46409
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->b(LX/0MA;)I

    move-result v2

    goto :goto_0
.end method

.method public final a(LX/0LU;)V
    .locals 1

    .prologue
    .line 46396
    iput-object p1, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->g:LX/0LU;

    .line 46397
    const/4 v0, 0x0

    invoke-interface {p1, v0}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->h:LX/0LS;

    .line 46398
    invoke-interface {p1}, LX/0LU;->a()V

    .line 46399
    return-void
.end method

.method public final a(LX/0MA;)Z
    .locals 1

    .prologue
    .line 46395
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->a(LX/0MA;Z)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46390
    iput v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->i:I

    .line 46391
    iput v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->m:I

    .line 46392
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->l:J

    .line 46393
    iput v2, p0, Lcom/google/android/exoplayer/extractor/mp3/Mp3Extractor;->n:I

    .line 46394
    return-void
.end method
