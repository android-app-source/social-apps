.class public final Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ME;


# static fields
.field private static final a:[B


# instance fields
.field private final b:I

.field private final c:LX/0Mo;

.field private final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0Mk;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Oj;

.field private final f:LX/0Oj;

.field private final g:LX/0Oj;

.field private final h:LX/0Oj;

.field private final i:[B

.field private final j:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "LX/0Md;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:I

.field private m:J

.field private n:I

.field private o:LX/0Oj;

.field private p:J

.field private q:LX/0Mk;

.field private r:I

.field private s:I

.field private t:I

.field private u:LX/0LU;

.field private v:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47636
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a:[B

    return-void

    :array_0
    .array-data 1
        -0x5et
        0x39t
        0x4ft
        0x52t
        0x5at
        -0x65t
        0x4ft
        0x14t
        -0x5et
        0x44t
        0x6ct
        0x42t
        0x7ct
        0x64t
        -0x73t
        -0xct
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47637
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;-><init>(I)V

    .line 47638
    return-void
.end method

.method private constructor <init>(I)V
    .locals 1

    .prologue
    .line 47622
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;-><init>(ILX/0Mo;)V

    .line 47623
    return-void
.end method

.method private constructor <init>(ILX/0Mo;)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v1, 0x4

    .line 47639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47640
    iput-object p2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->c:LX/0Mo;

    .line 47641
    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    or-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->b:I

    .line 47642
    new-instance v0, LX/0Oj;

    invoke-direct {v0, v3}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->h:LX/0Oj;

    .line 47643
    new-instance v0, LX/0Oj;

    sget-object v2, LX/0Oh;->a:[B

    invoke-direct {v0, v2}, LX/0Oj;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->e:LX/0Oj;

    .line 47644
    new-instance v0, LX/0Oj;

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->f:LX/0Oj;

    .line 47645
    new-instance v0, LX/0Oj;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->g:LX/0Oj;

    .line 47646
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->i:[B

    .line 47647
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->j:Ljava/util/Stack;

    .line 47648
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    .line 47649
    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a()V

    .line 47650
    return-void

    .line 47651
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/0Mk;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 47652
    iget-object v0, p1, LX/0Mk;->a:LX/0Mq;

    .line 47653
    iget-object v2, v0, LX/0Mq;->l:LX/0Oj;

    .line 47654
    iget-object v3, v0, LX/0Mq;->a:LX/0Mj;

    iget v3, v3, LX/0Mj;->a:I

    .line 47655
    iget-object v4, p1, LX/0Mk;->c:LX/0Mo;

    iget-object v4, v4, LX/0Mo;->l:[LX/0Mp;

    aget-object v3, v4, v3

    .line 47656
    iget v3, v3, LX/0Mp;->b:I

    .line 47657
    iget-object v0, v0, LX/0Mq;->j:[Z

    iget v4, p1, LX/0Mk;->e:I

    aget-boolean v4, v0, v4

    .line 47658
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->g:LX/0Oj;

    iget-object v5, v0, LX/0Oj;->a:[B

    if-eqz v4, :cond_0

    const/16 v0, 0x80

    :goto_0
    or-int/2addr v0, v3

    int-to-byte v0, v0

    aput-byte v0, v5, v1

    .line 47659
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->g:LX/0Oj;

    invoke-virtual {v0, v1}, LX/0Oj;->b(I)V

    .line 47660
    iget-object v0, p1, LX/0Mk;->b:LX/0LS;

    .line 47661
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->g:LX/0Oj;

    const/4 v5, 0x1

    invoke-interface {v0, v1, v5}, LX/0LS;->a(LX/0Oj;I)V

    .line 47662
    invoke-interface {v0, v2, v3}, LX/0LS;->a(LX/0Oj;I)V

    .line 47663
    if-nez v4, :cond_1

    .line 47664
    add-int/lit8 v0, v3, 0x1

    .line 47665
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 47666
    goto :goto_0

    .line 47667
    :cond_1
    invoke-virtual {v2}, LX/0Oj;->g()I

    move-result v1

    .line 47668
    const/4 v4, -0x2

    invoke-virtual {v2, v4}, LX/0Oj;->c(I)V

    .line 47669
    mul-int/lit8 v1, v1, 0x6

    add-int/lit8 v1, v1, 0x2

    .line 47670
    invoke-interface {v0, v2, v1}, LX/0LS;->a(LX/0Oj;I)V

    .line 47671
    add-int/lit8 v0, v3, 0x1

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method private static a(LX/0Oj;J)LX/0M9;
    .locals 23

    .prologue
    .line 47506
    const/16 v4, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/0Oj;->b(I)V

    .line 47507
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->m()I

    move-result v4

    .line 47508
    invoke-static {v4}, LX/0Mc;->a(I)I

    move-result v4

    .line 47509
    const/4 v5, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, LX/0Oj;->c(I)V

    .line 47510
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->k()J

    move-result-wide v8

    .line 47511
    if-nez v4, :cond_0

    .line 47512
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->k()J

    move-result-wide v4

    .line 47513
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->k()J

    move-result-wide v6

    add-long v6, v6, p1

    move-wide v10, v6

    .line 47514
    :goto_0
    const/4 v6, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/0Oj;->c(I)V

    .line 47515
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->g()I

    move-result v16

    .line 47516
    move/from16 v0, v16

    new-array v0, v0, [I

    move-object/from16 v17, v0

    .line 47517
    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v18, v0

    .line 47518
    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v19, v0

    .line 47519
    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v20, v0

    .line 47520
    const-wide/32 v6, 0xf4240

    invoke-static/range {v4 .. v9}, LX/08x;->a(JJJ)J

    move-result-wide v12

    .line 47521
    const/4 v6, 0x0

    move-wide v14, v10

    move v10, v6

    move-wide v6, v4

    move-wide v4, v12

    :goto_1
    move/from16 v0, v16

    if-ge v10, v0, :cond_2

    .line 47522
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->m()I

    move-result v11

    .line 47523
    const/high16 v12, -0x80000000

    and-int/2addr v12, v11

    .line 47524
    if-eqz v12, :cond_1

    .line 47525
    new-instance v4, LX/0L6;

    const-string v5, "Unhandled indirect reference"

    invoke-direct {v4, v5}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v4

    .line 47526
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->u()J

    move-result-wide v4

    .line 47527
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->u()J

    move-result-wide v6

    add-long v6, v6, p1

    move-wide v10, v6

    goto :goto_0

    .line 47528
    :cond_1
    invoke-virtual/range {p0 .. p0}, LX/0Oj;->k()J

    move-result-wide v12

    .line 47529
    const v21, 0x7fffffff

    and-int v11, v11, v21

    aput v11, v17, v10

    .line 47530
    aput-wide v14, v18, v10

    .line 47531
    aput-wide v4, v20, v10

    .line 47532
    add-long v4, v6, v12

    .line 47533
    const-wide/32 v6, 0xf4240

    invoke-static/range {v4 .. v9}, LX/08x;->a(JJJ)J

    move-result-wide v12

    .line 47534
    aget-wide v6, v20, v10

    sub-long v6, v12, v6

    aput-wide v6, v19, v10

    .line 47535
    const/4 v6, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/0Oj;->c(I)V

    .line 47536
    aget v6, v17, v10

    int-to-long v6, v6

    add-long/2addr v14, v6

    .line 47537
    add-int/lit8 v6, v10, 0x1

    move v10, v6

    move-wide v6, v4

    move-wide v4, v12

    goto :goto_1

    .line 47538
    :cond_2
    new-instance v4, LX/0M9;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-direct {v4, v0, v1, v2, v3}, LX/0M9;-><init>([I[J[J[J)V

    return-object v4
.end method

.method private static a(LX/0Oj;Landroid/util/SparseArray;I)LX/0Mk;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Oj;",
            "Landroid/util/SparseArray",
            "<",
            "LX/0Mk;",
            ">;I)",
            "LX/0Mk;"
        }
    .end annotation

    .prologue
    .line 47672
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 47673
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    .line 47674
    invoke-static {v0}, LX/0Mc;->b(I)I

    move-result v5

    .line 47675
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    .line 47676
    and-int/lit8 v1, p2, 0x4

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Mk;

    .line 47677
    if-nez v0, :cond_1

    .line 47678
    const/4 v0, 0x0

    .line 47679
    :goto_1
    return-object v0

    .line 47680
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 47681
    :cond_1
    and-int/lit8 v1, v5, 0x1

    if-eqz v1, :cond_2

    .line 47682
    invoke-virtual {p0}, LX/0Oj;->u()J

    move-result-wide v2

    .line 47683
    iget-object v1, v0, LX/0Mk;->a:LX/0Mq;

    iput-wide v2, v1, LX/0Mq;->b:J

    .line 47684
    iget-object v1, v0, LX/0Mk;->a:LX/0Mq;

    iput-wide v2, v1, LX/0Mq;->c:J

    .line 47685
    :cond_2
    iget-object v6, v0, LX/0Mk;->d:LX/0Mj;

    .line 47686
    and-int/lit8 v1, v5, 0x2

    if-eqz v1, :cond_3

    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v4, v1

    .line 47687
    :goto_2
    and-int/lit8 v1, v5, 0x8

    if-eqz v1, :cond_4

    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v1

    move v3, v1

    .line 47688
    :goto_3
    and-int/lit8 v1, v5, 0x10

    if-eqz v1, :cond_5

    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v1

    move v2, v1

    .line 47689
    :goto_4
    and-int/lit8 v1, v5, 0x20

    if-eqz v1, :cond_6

    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v1

    .line 47690
    :goto_5
    iget-object v5, v0, LX/0Mk;->a:LX/0Mq;

    new-instance v6, LX/0Mj;

    invoke-direct {v6, v4, v3, v2, v1}, LX/0Mj;-><init>(IIII)V

    iput-object v6, v5, LX/0Mq;->a:LX/0Mj;

    goto :goto_1

    .line 47691
    :cond_3
    iget v1, v6, LX/0Mj;->a:I

    move v4, v1

    goto :goto_2

    .line 47692
    :cond_4
    iget v1, v6, LX/0Mj;->b:I

    move v3, v1

    goto :goto_3

    .line 47693
    :cond_5
    iget v1, v6, LX/0Mj;->c:I

    move v2, v1

    goto :goto_4

    .line 47694
    :cond_6
    iget v1, v6, LX/0Mj;->d:I

    goto :goto_5
.end method

.method private static a(Landroid/util/SparseArray;)LX/0Mk;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "LX/0Mk;",
            ">;)",
            "LX/0Mk;"
        }
    .end annotation

    .prologue
    .line 47695
    const/4 v1, 0x0

    .line 47696
    const-wide v4, 0x7fffffffffffffffL

    .line 47697
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v7

    .line 47698
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_0

    .line 47699
    invoke-virtual {p0, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Mk;

    .line 47700
    iget v2, v0, LX/0Mk;->e:I

    iget-object v3, v0, LX/0Mk;->a:LX/0Mq;

    iget v3, v3, LX/0Mq;->d:I

    if-eq v2, v3, :cond_1

    .line 47701
    iget-object v2, v0, LX/0Mk;->a:LX/0Mq;

    iget-wide v2, v2, LX/0Mq;->b:J

    .line 47702
    cmp-long v8, v2, v4

    if-gez v8, :cond_1

    move-wide v9, v2

    move-object v2, v0

    move-wide v0, v9

    .line 47703
    :goto_1
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move-wide v4, v0

    move-object v1, v2

    goto :goto_0

    .line 47704
    :cond_0
    return-object v1

    :cond_1
    move-object v2, v1

    move-wide v0, v4

    goto :goto_1
.end method

.method private static a(LX/0Oj;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Oj;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "LX/0Mj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47705
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 47706
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    .line 47707
    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 47708
    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v2

    .line 47709
    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v3

    .line 47710
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v4

    .line 47711
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v5, LX/0Mj;

    invoke-direct {v5, v1, v2, v3, v4}, LX/0Mj;-><init>(IIII)V

    invoke-static {v0, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47830
    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->k:I

    .line 47831
    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->n:I

    .line 47832
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 47712
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Md;

    iget-wide v0, v0, LX/0Md;->aB:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 47713
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Md;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Md;)V

    goto :goto_0

    .line 47714
    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a()V

    .line 47715
    return-void
.end method

.method private a(LX/0Md;)V
    .locals 2

    .prologue
    .line 47823
    iget v0, p1, LX/0Mc;->aA:I

    sget v1, LX/0Mc;->y:I

    if-ne v0, v1, :cond_1

    .line 47824
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->b(LX/0Md;)V

    .line 47825
    :cond_0
    :goto_0
    return-void

    .line 47826
    :cond_1
    iget v0, p1, LX/0Mc;->aA:I

    sget v1, LX/0Mc;->H:I

    if-ne v0, v1, :cond_2

    .line 47827
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->c(LX/0Md;)V

    goto :goto_0

    .line 47828
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 47829
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Md;

    invoke-virtual {v0, p1}, LX/0Md;->a(LX/0Md;)V

    goto :goto_0
.end method

.method private static a(LX/0Md;Landroid/util/SparseArray;I[B)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Md;",
            "Landroid/util/SparseArray",
            "<",
            "LX/0Mk;",
            ">;I[B)V"
        }
    .end annotation

    .prologue
    .line 47816
    iget-object v0, p0, LX/0Md;->aD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 47817
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 47818
    iget-object v0, p0, LX/0Md;->aD:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Md;

    .line 47819
    iget v3, v0, LX/0Mc;->aA:I

    sget v4, LX/0Mc;->I:I

    if-ne v3, v4, :cond_0

    .line 47820
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->b(LX/0Md;Landroid/util/SparseArray;I[B)V

    .line 47821
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 47822
    :cond_1
    return-void
.end method

.method private a(LX/0Me;J)V
    .locals 2

    .prologue
    .line 47809
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 47810
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Md;

    invoke-virtual {v0, p1}, LX/0Md;->a(LX/0Me;)V

    .line 47811
    :cond_0
    :goto_0
    return-void

    .line 47812
    :cond_1
    iget v0, p1, LX/0Mc;->aA:I

    sget v1, LX/0Mc;->x:I

    if-ne v0, v1, :cond_0

    .line 47813
    iget-object v0, p1, LX/0Me;->aB:LX/0Oj;

    invoke-static {v0, p2, p3}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Oj;J)LX/0M9;

    move-result-object v0

    .line 47814
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->u:LX/0LU;

    invoke-interface {v1, v0}, LX/0LU;->a(LX/0M8;)V

    .line 47815
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->v:Z

    goto :goto_0
.end method

.method private static a(LX/0Mk;JILX/0Oj;)V
    .locals 29

    .prologue
    .line 47757
    const/16 v2, 0x8

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, LX/0Oj;->b(I)V

    .line 47758
    invoke-virtual/range {p4 .. p4}, LX/0Oj;->m()I

    move-result v2

    .line 47759
    invoke-static {v2}, LX/0Mc;->b(I)I

    move-result v3

    .line 47760
    move-object/from16 v0, p0

    iget-object v12, v0, LX/0Mk;->c:LX/0Mo;

    .line 47761
    move-object/from16 v0, p0

    iget-object v0, v0, LX/0Mk;->a:LX/0Mq;

    move-object/from16 v21, v0

    .line 47762
    move-object/from16 v0, v21

    iget-object v0, v0, LX/0Mq;->a:LX/0Mj;

    move-object/from16 v22, v0

    .line 47763
    invoke-virtual/range {p4 .. p4}, LX/0Oj;->s()I

    move-result v23

    .line 47764
    and-int/lit8 v2, v3, 0x1

    if-eqz v2, :cond_0

    .line 47765
    move-object/from16 v0, v21

    iget-wide v4, v0, LX/0Mq;->b:J

    invoke-virtual/range {p4 .. p4}, LX/0Oj;->m()I

    move-result v2

    int-to-long v6, v2

    add-long/2addr v4, v6

    move-object/from16 v0, v21

    iput-wide v4, v0, LX/0Mq;->b:J

    .line 47766
    :cond_0
    and-int/lit8 v2, v3, 0x4

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    move v8, v2

    .line 47767
    :goto_0
    move-object/from16 v0, v22

    iget v14, v0, LX/0Mj;->d:I

    .line 47768
    if-eqz v8, :cond_1

    .line 47769
    invoke-virtual/range {p4 .. p4}, LX/0Oj;->s()I

    move-result v14

    .line 47770
    :cond_1
    and-int/lit16 v2, v3, 0x100

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    move/from16 v20, v2

    .line 47771
    :goto_1
    and-int/lit16 v2, v3, 0x200

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    move/from16 v19, v2

    .line 47772
    :goto_2
    and-int/lit16 v2, v3, 0x400

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    move/from16 v18, v2

    .line 47773
    :goto_3
    and-int/lit16 v2, v3, 0x800

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    move v9, v2

    .line 47774
    :goto_4
    const-wide/16 v2, 0x0

    .line 47775
    iget-object v4, v12, LX/0Mo;->m:[J

    if-eqz v4, :cond_10

    iget-object v4, v12, LX/0Mo;->m:[J

    array-length v4, v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_10

    iget-object v4, v12, LX/0Mo;->m:[J

    const/4 v5, 0x0

    aget-wide v4, v4, v5

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_10

    .line 47776
    iget-object v2, v12, LX/0Mo;->n:[J

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    const-wide/16 v4, 0x3e8

    iget-wide v6, v12, LX/0Mo;->h:J

    invoke-static/range {v2 .. v7}, LX/08x;->a(JJJ)J

    move-result-wide v2

    move-wide v10, v2

    .line 47777
    :goto_5
    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, LX/0Mq;->a(I)V

    .line 47778
    move-object/from16 v0, v21

    iget-object v0, v0, LX/0Mq;->e:[I

    move-object/from16 v24, v0

    .line 47779
    move-object/from16 v0, v21

    iget-object v0, v0, LX/0Mq;->f:[I

    move-object/from16 v25, v0

    .line 47780
    move-object/from16 v0, v21

    iget-object v0, v0, LX/0Mq;->g:[J

    move-object/from16 v26, v0

    .line 47781
    move-object/from16 v0, v21

    iget-object v0, v0, LX/0Mq;->h:[Z

    move-object/from16 v27, v0

    .line 47782
    iget-wide v6, v12, LX/0Mo;->h:J

    .line 47783
    iget v2, v12, LX/0Mo;->g:I

    sget v3, LX/0Mo;->a:I

    if-ne v2, v3, :cond_8

    and-int/lit8 v2, p3, 0x1

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    move v12, v2

    .line 47784
    :goto_6
    const/4 v2, 0x0

    move/from16 v17, v2

    move-wide/from16 v2, p1

    :goto_7
    move/from16 v0, v17

    move/from16 v1, v23

    if-ge v0, v1, :cond_f

    .line 47785
    if-eqz v20, :cond_9

    invoke-virtual/range {p4 .. p4}, LX/0Oj;->s()I

    move-result v4

    move/from16 v16, v4

    .line 47786
    :goto_8
    if-eqz v19, :cond_a

    invoke-virtual/range {p4 .. p4}, LX/0Oj;->s()I

    move-result v4

    move v15, v4

    .line 47787
    :goto_9
    if-nez v17, :cond_b

    if-eqz v8, :cond_b

    move v13, v14

    .line 47788
    :goto_a
    if-eqz v9, :cond_d

    .line 47789
    invoke-virtual/range {p4 .. p4}, LX/0Oj;->m()I

    move-result v4

    .line 47790
    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    div-long/2addr v4, v6

    long-to-int v4, v4

    aput v4, v25, v17

    .line 47791
    :goto_b
    const-wide/16 v4, 0x3e8

    invoke-static/range {v2 .. v7}, LX/08x;->a(JJJ)J

    move-result-wide v4

    sub-long/2addr v4, v10

    aput-wide v4, v26, v17

    .line 47792
    aput v15, v24, v17

    .line 47793
    shr-int/lit8 v4, v13, 0x10

    and-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_e

    if-eqz v12, :cond_2

    if-nez v17, :cond_e

    :cond_2
    const/4 v4, 0x1

    :goto_c
    aput-boolean v4, v27, v17

    .line 47794
    move/from16 v0, v16

    int-to-long v4, v0

    add-long p1, v2, v4

    .line 47795
    add-int/lit8 v2, v17, 0x1

    move/from16 v17, v2

    move-wide/from16 v2, p1

    goto :goto_7

    .line 47796
    :cond_3
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_0

    .line 47797
    :cond_4
    const/4 v2, 0x0

    move/from16 v20, v2

    goto/16 :goto_1

    .line 47798
    :cond_5
    const/4 v2, 0x0

    move/from16 v19, v2

    goto/16 :goto_2

    .line 47799
    :cond_6
    const/4 v2, 0x0

    move/from16 v18, v2

    goto/16 :goto_3

    .line 47800
    :cond_7
    const/4 v2, 0x0

    move v9, v2

    goto/16 :goto_4

    .line 47801
    :cond_8
    const/4 v2, 0x0

    move v12, v2

    goto :goto_6

    .line 47802
    :cond_9
    move-object/from16 v0, v22

    iget v4, v0, LX/0Mj;->b:I

    move/from16 v16, v4

    goto :goto_8

    .line 47803
    :cond_a
    move-object/from16 v0, v22

    iget v4, v0, LX/0Mj;->c:I

    move v15, v4

    goto :goto_9

    .line 47804
    :cond_b
    if-eqz v18, :cond_c

    invoke-virtual/range {p4 .. p4}, LX/0Oj;->m()I

    move-result v4

    move v13, v4

    goto :goto_a

    :cond_c
    move-object/from16 v0, v22

    iget v4, v0, LX/0Mj;->d:I

    move v13, v4

    goto :goto_a

    .line 47805
    :cond_d
    const/4 v4, 0x0

    aput v4, v25, v17

    goto :goto_b

    .line 47806
    :cond_e
    const/4 v4, 0x0

    goto :goto_c

    .line 47807
    :cond_f
    move-object/from16 v0, v21

    iput-wide v2, v0, LX/0Mq;->n:J

    .line 47808
    return-void

    :cond_10
    move-wide v10, v2

    goto/16 :goto_5
.end method

.method private static a(LX/0Mp;LX/0Oj;LX/0Mq;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47733
    iget v5, p0, LX/0Mp;->b:I

    .line 47734
    invoke-virtual {p1, v3}, LX/0Oj;->b(I)V

    .line 47735
    invoke-virtual {p1}, LX/0Oj;->m()I

    move-result v0

    .line 47736
    invoke-static {v0}, LX/0Mc;->b(I)I

    move-result v0

    .line 47737
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 47738
    invoke-virtual {p1, v3}, LX/0Oj;->c(I)V

    .line 47739
    :cond_0
    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v0

    .line 47740
    invoke-virtual {p1}, LX/0Oj;->s()I

    move-result v6

    .line 47741
    iget v3, p2, LX/0Mq;->d:I

    if-eq v6, v3, :cond_1

    .line 47742
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Length mismatch: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, LX/0Mq;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47743
    :cond_1
    if-nez v0, :cond_3

    .line 47744
    iget-object v7, p2, LX/0Mq;->j:[Z

    move v3, v2

    move v0, v2

    .line 47745
    :goto_0
    if-ge v3, v6, :cond_4

    .line 47746
    invoke-virtual {p1}, LX/0Oj;->f()I

    move-result v8

    .line 47747
    add-int v4, v0, v8

    .line 47748
    if-le v8, v5, :cond_2

    move v0, v1

    :goto_1
    aput-boolean v0, v7, v3

    .line 47749
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v4

    goto :goto_0

    :cond_2
    move v0, v2

    .line 47750
    goto :goto_1

    .line 47751
    :cond_3
    if-le v0, v5, :cond_5

    .line 47752
    :goto_2
    mul-int/2addr v0, v6

    add-int/lit8 v0, v0, 0x0

    .line 47753
    iget-object v3, p2, LX/0Mq;->j:[Z

    invoke-static {v3, v2, v6, v1}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 47754
    :cond_4
    invoke-virtual {p2, v0}, LX/0Mq;->b(I)V

    .line 47755
    return-void

    :cond_5
    move v1, v2

    .line 47756
    goto :goto_2
.end method

.method private static a(LX/0Oj;ILX/0Mq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 47716
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 47717
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    .line 47718
    invoke-static {v0}, LX/0Mc;->b(I)I

    move-result v0

    .line 47719
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_0

    .line 47720
    new-instance v0, LX/0L6;

    const-string v1, "Overriding TrackEncryptionBox parameters is unsupported."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47721
    :cond_0
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 47722
    :goto_0
    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v2

    .line 47723
    iget v3, p2, LX/0Mq;->d:I

    if-eq v2, v3, :cond_2

    .line 47724
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Length mismatch: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, LX/0Mq;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v1

    .line 47725
    goto :goto_0

    .line 47726
    :cond_2
    iget-object v3, p2, LX/0Mq;->j:[Z

    invoke-static {v3, v1, v2, v0}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 47727
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v0

    invoke-virtual {p2, v0}, LX/0Mq;->b(I)V

    .line 47728
    const/4 v2, 0x0

    .line 47729
    iget-object v0, p2, LX/0Mq;->l:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    iget v1, p2, LX/0Mq;->k:I

    invoke-virtual {p0, v0, v2, v1}, LX/0Oj;->a([BII)V

    .line 47730
    iget-object v0, p2, LX/0Mq;->l:LX/0Oj;

    invoke-virtual {v0, v2}, LX/0Oj;->b(I)V

    .line 47731
    iput-boolean v2, p2, LX/0Mq;->m:Z

    .line 47732
    return-void
.end method

.method private static a(LX/0Oj;LX/0Mq;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 47624
    invoke-virtual {p0, v3}, LX/0Oj;->b(I)V

    .line 47625
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    .line 47626
    invoke-static {v0}, LX/0Mc;->b(I)I

    move-result v1

    .line 47627
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 47628
    invoke-virtual {p0, v3}, LX/0Oj;->c(I)V

    .line 47629
    :cond_0
    invoke-virtual {p0}, LX/0Oj;->s()I

    move-result v1

    .line 47630
    if-eq v1, v2, :cond_1

    .line 47631
    new-instance v0, LX/0L6;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected saio entry count: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47632
    :cond_1
    invoke-static {v0}, LX/0Mc;->a(I)I

    move-result v0

    .line 47633
    iget-wide v2, p1, LX/0Mq;->c:J

    if-nez v0, :cond_2

    invoke-virtual {p0}, LX/0Oj;->k()J

    move-result-wide v0

    :goto_0
    add-long/2addr v0, v2

    iput-wide v0, p1, LX/0Mq;->c:J

    .line 47634
    return-void

    .line 47635
    :cond_2
    invoke-virtual {p0}, LX/0Oj;->u()J

    move-result-wide v0

    goto :goto_0
.end method

.method private static a(LX/0Oj;LX/0Mq;[B)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 47318
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 47319
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0, v1}, LX/0Oj;->a([BII)V

    .line 47320
    sget-object v0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a:[B

    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47321
    :goto_0
    return-void

    .line 47322
    :cond_0
    invoke-static {p0, v1, p1}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Oj;ILX/0Mq;)V

    goto :goto_0
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 47323
    sget v0, LX/0Mc;->P:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->O:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->z:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->x:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->Q:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->t:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->u:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->L:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->v:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->w:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->R:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->Z:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->aa:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->ac:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->ab:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->N:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->K:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0Oj;)J
    .locals 2

    .prologue
    .line 47324
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 47325
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    .line 47326
    invoke-static {v0}, LX/0Mc;->a(I)I

    move-result v0

    .line 47327
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0Oj;->k()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, LX/0Oj;->u()J

    move-result-wide v0

    goto :goto_0
.end method

.method private b(LX/0Md;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47328
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->c:LX/0Mo;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Unexpected moov box."

    invoke-static {v0, v3}, LX/0Av;->b(ZLjava/lang/Object;)V

    .line 47329
    iget-object v5, p1, LX/0Md;->aC:Ljava/util/List;

    .line 47330
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    .line 47331
    const/4 v3, 0x0

    move v4, v2

    .line 47332
    :goto_1
    if-ge v4, v6, :cond_4

    .line 47333
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Me;

    .line 47334
    iget v7, v0, LX/0Mc;->aA:I

    sget v8, LX/0Mc;->R:I

    if-ne v7, v8, :cond_1

    .line 47335
    if-nez v3, :cond_0

    .line 47336
    new-instance v3, LX/0M0;

    invoke-direct {v3}, LX/0M0;-><init>()V

    .line 47337
    :cond_0
    iget-object v0, v0, LX/0Me;->aB:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    .line 47338
    invoke-static {v0}, LX/0Mm;->a([B)Ljava/util/UUID;

    move-result-object v7

    .line 47339
    if-nez v7, :cond_3

    .line 47340
    const-string v0, "FragmentedMp4Extractor"

    const-string v7, "Skipped pssh atom (failed to extract uuid)"

    invoke-static {v0, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 47341
    :cond_1
    :goto_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 47342
    goto :goto_0

    .line 47343
    :cond_3
    invoke-static {v0}, LX/0Mm;->a([B)Ljava/util/UUID;

    move-result-object v7

    new-instance v8, LX/0M1;

    const-string v9, "video/mp4"

    invoke-direct {v8, v9, v0}, LX/0M1;-><init>(Ljava/lang/String;[B)V

    invoke-virtual {v3, v7, v8}, LX/0M0;->a(Ljava/util/UUID;LX/0M1;)V

    goto :goto_2

    .line 47344
    :cond_4
    if-eqz v3, :cond_5

    .line 47345
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->u:LX/0LU;

    invoke-interface {v0, v3}, LX/0LU;->a(LX/0Lz;)V

    .line 47346
    :cond_5
    sget v0, LX/0Mc;->J:I

    invoke-virtual {p1, v0}, LX/0Md;->e(I)LX/0Md;

    move-result-object v6

    .line 47347
    new-instance v7, Landroid/util/SparseArray;

    invoke-direct {v7}, Landroid/util/SparseArray;-><init>()V

    .line 47348
    const-wide/16 v4, -0x1

    .line 47349
    iget-object v0, v6, LX/0Md;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    move v3, v2

    .line 47350
    :goto_3
    if-ge v3, v8, :cond_8

    .line 47351
    iget-object v0, v6, LX/0Md;->aC:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Me;

    .line 47352
    iget v9, v0, LX/0Mc;->aA:I

    sget v10, LX/0Mc;->v:I

    if-ne v9, v10, :cond_7

    .line 47353
    iget-object v0, v0, LX/0Me;->aB:LX/0Oj;

    invoke-static {v0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Oj;)Landroid/util/Pair;

    move-result-object v9

    .line 47354
    iget-object v0, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v9, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v7, v0, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 47355
    :cond_6
    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 47356
    :cond_7
    iget v9, v0, LX/0Mc;->aA:I

    sget v10, LX/0Mc;->K:I

    if-ne v9, v10, :cond_6

    .line 47357
    iget-object v0, v0, LX/0Me;->aB:LX/0Oj;

    invoke-static {v0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->b(LX/0Oj;)J

    move-result-wide v4

    goto :goto_4

    .line 47358
    :cond_8
    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    .line 47359
    iget-object v0, p1, LX/0Md;->aD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    move v3, v2

    .line 47360
    :goto_5
    if-ge v3, v8, :cond_a

    .line 47361
    iget-object v0, p1, LX/0Md;->aD:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Md;

    .line 47362
    iget v9, v0, LX/0Mc;->aA:I

    sget v10, LX/0Mc;->A:I

    if-ne v9, v10, :cond_9

    .line 47363
    sget v9, LX/0Mc;->z:I

    invoke-virtual {p1, v9}, LX/0Md;->d(I)LX/0Me;

    move-result-object v9

    invoke-static {v0, v9, v4, v5, v2}, LX/0Mi;->a(LX/0Md;LX/0Me;JZ)LX/0Mo;

    move-result-object v0

    .line 47364
    if-eqz v0, :cond_9

    .line 47365
    iget v9, v0, LX/0Mo;->f:I

    invoke-virtual {v6, v9, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 47366
    :cond_9
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 47367
    :cond_a
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v4

    .line 47368
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_c

    move v1, v2

    .line 47369
    :goto_6
    if-ge v1, v4, :cond_b

    .line 47370
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Mo;

    iget v0, v0, LX/0Mo;->f:I

    new-instance v5, LX/0Mk;

    iget-object v8, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->u:LX/0LU;

    invoke-interface {v8, v1}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v8

    invoke-direct {v5, v8}, LX/0Mk;-><init>(LX/0LS;)V

    invoke-virtual {v3, v0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 47371
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 47372
    :cond_b
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->u:LX/0LU;

    invoke-interface {v0}, LX/0LU;->a()V

    :goto_7
    move v3, v2

    .line 47373
    :goto_8
    if-ge v3, v4, :cond_e

    .line 47374
    invoke-virtual {v6, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Mo;

    .line 47375
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    iget v2, v0, LX/0Mo;->f:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Mk;

    iget v2, v0, LX/0Mo;->f:I

    invoke-virtual {v7, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Mj;

    invoke-virtual {v1, v0, v2}, LX/0Mk;->a(LX/0Mo;LX/0Mj;)V

    .line 47376
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_8

    .line 47377
    :cond_c
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ne v0, v4, :cond_d

    :goto_9
    invoke-static {v1}, LX/0Av;->b(Z)V

    goto :goto_7

    :cond_d
    move v1, v2

    goto :goto_9

    .line 47378
    :cond_e
    return-void
.end method

.method private static b(LX/0Md;Landroid/util/SparseArray;I[B)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Md;",
            "Landroid/util/SparseArray",
            "<",
            "LX/0Mk;",
            ">;I[B)V"
        }
    .end annotation

    .prologue
    .line 47379
    sget v0, LX/0Mc;->w:I

    const/4 v3, 0x0

    .line 47380
    iget-object v1, p0, LX/0Md;->aC:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    move v4, v3

    move v2, v3

    .line 47381
    :goto_0
    if-ge v4, v5, :cond_0

    .line 47382
    iget-object v1, p0, LX/0Md;->aC:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Me;

    .line 47383
    iget v1, v1, LX/0Mc;->aA:I

    if-ne v1, v0, :cond_b

    .line 47384
    add-int/lit8 v1, v2, 0x1

    .line 47385
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto :goto_0

    .line 47386
    :cond_0
    iget-object v1, p0, LX/0Md;->aD:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    .line 47387
    :goto_2
    if-ge v3, v4, :cond_1

    .line 47388
    iget-object v1, p0, LX/0Md;->aD:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Md;

    .line 47389
    iget v1, v1, LX/0Mc;->aA:I

    if-ne v1, v0, :cond_a

    .line 47390
    add-int/lit8 v1, v2, 0x1

    .line 47391
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_2

    .line 47392
    :cond_1
    move v0, v2

    .line 47393
    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 47394
    new-instance v0, LX/0L6;

    const-string v1, "Trun count in traf != 1 (unsupported)."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47395
    :cond_2
    sget v0, LX/0Mc;->u:I

    invoke-virtual {p0, v0}, LX/0Md;->d(I)LX/0Me;

    move-result-object v0

    .line 47396
    iget-object v0, v0, LX/0Me;->aB:LX/0Oj;

    invoke-static {v0, p1, p2}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Oj;Landroid/util/SparseArray;I)LX/0Mk;

    move-result-object v2

    .line 47397
    if-nez v2, :cond_4

    .line 47398
    :cond_3
    return-void

    .line 47399
    :cond_4
    iget-object v3, v2, LX/0Mk;->a:LX/0Mq;

    .line 47400
    iget-wide v0, v3, LX/0Mq;->n:J

    .line 47401
    invoke-virtual {v2}, LX/0Mk;->a()V

    .line 47402
    sget v4, LX/0Mc;->t:I

    invoke-virtual {p0, v4}, LX/0Md;->d(I)LX/0Me;

    move-result-object v4

    .line 47403
    if-eqz v4, :cond_5

    and-int/lit8 v4, p2, 0x2

    if-nez v4, :cond_5

    .line 47404
    sget v0, LX/0Mc;->t:I

    invoke-virtual {p0, v0}, LX/0Md;->d(I)LX/0Me;

    move-result-object v0

    iget-object v0, v0, LX/0Me;->aB:LX/0Oj;

    invoke-static {v0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->c(LX/0Oj;)J

    move-result-wide v0

    .line 47405
    :cond_5
    sget v4, LX/0Mc;->w:I

    invoke-virtual {p0, v4}, LX/0Md;->d(I)LX/0Me;

    move-result-object v4

    .line 47406
    iget-object v4, v4, LX/0Me;->aB:LX/0Oj;

    invoke-static {v2, v0, v1, p2, v4}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Mk;JILX/0Oj;)V

    .line 47407
    sget v0, LX/0Mc;->Z:I

    invoke-virtual {p0, v0}, LX/0Md;->d(I)LX/0Me;

    move-result-object v0

    .line 47408
    if-eqz v0, :cond_6

    .line 47409
    iget-object v1, v2, LX/0Mk;->c:LX/0Mo;

    iget-object v1, v1, LX/0Mo;->l:[LX/0Mp;

    iget-object v2, v3, LX/0Mq;->a:LX/0Mj;

    iget v2, v2, LX/0Mj;->a:I

    aget-object v1, v1, v2

    .line 47410
    iget-object v0, v0, LX/0Me;->aB:LX/0Oj;

    invoke-static {v1, v0, v3}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Mp;LX/0Oj;LX/0Mq;)V

    .line 47411
    :cond_6
    sget v0, LX/0Mc;->aa:I

    invoke-virtual {p0, v0}, LX/0Md;->d(I)LX/0Me;

    move-result-object v0

    .line 47412
    if-eqz v0, :cond_7

    .line 47413
    iget-object v0, v0, LX/0Me;->aB:LX/0Oj;

    invoke-static {v0, v3}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Oj;LX/0Mq;)V

    .line 47414
    :cond_7
    sget v0, LX/0Mc;->ac:I

    invoke-virtual {p0, v0}, LX/0Md;->d(I)LX/0Me;

    move-result-object v0

    .line 47415
    if-eqz v0, :cond_8

    .line 47416
    iget-object v0, v0, LX/0Me;->aB:LX/0Oj;

    invoke-static {v0, v3}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->b(LX/0Oj;LX/0Mq;)V

    .line 47417
    :cond_8
    iget-object v0, p0, LX/0Md;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 47418
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v2, :cond_3

    .line 47419
    iget-object v0, p0, LX/0Md;->aC:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Me;

    .line 47420
    iget v4, v0, LX/0Mc;->aA:I

    sget v5, LX/0Mc;->ab:I

    if-ne v4, v5, :cond_9

    .line 47421
    iget-object v0, v0, LX/0Me;->aB:LX/0Oj;

    invoke-static {v0, v3, p3}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Oj;LX/0Mq;[B)V

    .line 47422
    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_a
    move v1, v2

    goto/16 :goto_3

    :cond_b
    move v1, v2

    goto/16 :goto_1
.end method

.method private static b(LX/0Oj;LX/0Mq;)V
    .locals 1

    .prologue
    .line 47423
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Oj;ILX/0Mq;)V

    .line 47424
    return-void
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 47425
    sget v0, LX/0Mc;->y:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->A:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->B:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->C:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->D:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->H:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->I:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->J:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->M:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/0MA;)Z
    .locals 12

    .prologue
    const-wide/32 v10, 0x7fffffff

    const/4 v9, 0x0

    const/16 v8, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 47426
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->n:I

    if-nez v0, :cond_1

    .line 47427
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->h:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    invoke-interface {p1, v0, v1, v8, v2}, LX/0MA;->a([BIIZ)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 47428
    :goto_0
    return v0

    .line 47429
    :cond_0
    iput v8, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->n:I

    .line 47430
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->h:LX/0Oj;

    invoke-virtual {v0, v1}, LX/0Oj;->b(I)V

    .line 47431
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->h:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->k()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->m:J

    .line 47432
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->h:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->m()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->l:I

    .line 47433
    :cond_1
    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->m:J

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    .line 47434
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->h:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    invoke-interface {p1, v0, v8, v8}, LX/0MA;->b([BII)V

    .line 47435
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->n:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->n:I

    .line 47436
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->h:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->u()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->m:J

    .line 47437
    :cond_2
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v4

    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->n:I

    int-to-long v6, v0

    sub-long/2addr v4, v6

    .line 47438
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->l:I

    sget v3, LX/0Mc;->H:I

    if-ne v0, v3, :cond_3

    .line 47439
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v6

    move v3, v1

    .line 47440
    :goto_1
    if-ge v3, v6, :cond_3

    .line 47441
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Mk;

    iget-object v0, v0, LX/0Mk;->a:LX/0Mq;

    .line 47442
    iput-wide v4, v0, LX/0Mq;->c:J

    .line 47443
    iput-wide v4, v0, LX/0Mq;->b:J

    .line 47444
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 47445
    :cond_3
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->l:I

    sget v3, LX/0Mc;->h:I

    if-ne v0, v3, :cond_5

    .line 47446
    iput-object v9, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    .line 47447
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->m:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->p:J

    .line 47448
    iget-boolean v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->v:Z

    if-nez v0, :cond_4

    .line 47449
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->u:LX/0LU;

    sget-object v1, LX/0M8;->f:LX/0M8;

    invoke-interface {v0, v1}, LX/0LU;->a(LX/0M8;)V

    .line 47450
    iput-boolean v2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->v:Z

    .line 47451
    :cond_4
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->k:I

    move v0, v2

    .line 47452
    goto :goto_0

    .line 47453
    :cond_5
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->l:I

    invoke-static {v0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->b(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 47454
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->m:J

    add-long/2addr v0, v4

    const-wide/16 v4, 0x8

    sub-long/2addr v0, v4

    .line 47455
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->j:Ljava/util/Stack;

    new-instance v4, LX/0Md;

    iget v5, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->l:I

    invoke-direct {v4, v5, v0, v1}, LX/0Md;-><init>(IJ)V

    invoke-virtual {v3, v4}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 47456
    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->m:J

    iget v3, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->n:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-nez v3, :cond_6

    .line 47457
    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(J)V

    :goto_2
    move v0, v2

    .line 47458
    goto/16 :goto_0

    .line 47459
    :cond_6
    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a()V

    goto :goto_2

    .line 47460
    :cond_7
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->l:I

    invoke-static {v0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 47461
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->n:I

    if-eq v0, v8, :cond_8

    .line 47462
    new-instance v0, LX/0L6;

    const-string v1, "Leaf atom defines extended atom size (unsupported)."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47463
    :cond_8
    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->m:J

    cmp-long v0, v4, v10

    if-lez v0, :cond_9

    .line 47464
    new-instance v0, LX/0L6;

    const-string v1, "Leaf atom with length > 2147483647 (unsupported)."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47465
    :cond_9
    new-instance v0, LX/0Oj;

    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->m:J

    long-to-int v3, v4

    invoke-direct {v0, v3}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->o:LX/0Oj;

    .line 47466
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->h:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->o:LX/0Oj;

    iget-object v3, v3, LX/0Oj;->a:[B

    invoke-static {v0, v1, v3, v1, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47467
    iput v2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->k:I

    goto :goto_2

    .line 47468
    :cond_a
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->m:J

    cmp-long v0, v0, v10

    if-lez v0, :cond_b

    .line 47469
    new-instance v0, LX/0L6;

    const-string v1, "Skipping atom with length > 2147483647 (unsupported)."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47470
    :cond_b
    iput-object v9, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->o:LX/0Oj;

    .line 47471
    iput v2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->k:I

    goto :goto_2
.end method

.method private static c(LX/0Oj;)J
    .locals 2

    .prologue
    .line 47472
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/0Oj;->b(I)V

    .line 47473
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v0

    .line 47474
    invoke-static {v0}, LX/0Mc;->a(I)I

    move-result v0

    .line 47475
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, LX/0Oj;->u()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, LX/0Oj;->k()J

    move-result-wide v0

    goto :goto_0
.end method

.method private c(LX/0MA;)V
    .locals 4

    .prologue
    .line 47476
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->m:J

    long-to-int v0, v0

    iget v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->n:I

    sub-int/2addr v0, v1

    .line 47477
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->o:LX/0Oj;

    if-eqz v1, :cond_0

    .line 47478
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->o:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    const/16 v2, 0x8

    invoke-interface {p1, v1, v2, v0}, LX/0MA;->b([BII)V

    .line 47479
    new-instance v0, LX/0Me;

    iget v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->l:I

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->o:LX/0Oj;

    invoke-direct {v0, v1, v2}, LX/0Me;-><init>(ILX/0Oj;)V

    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Me;J)V

    .line 47480
    :goto_0
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(J)V

    .line 47481
    return-void

    .line 47482
    :cond_0
    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    goto :goto_0
.end method

.method private c(LX/0Md;)V
    .locals 3

    .prologue
    .line 47483
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->i:[B

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Md;Landroid/util/SparseArray;I[B)V

    .line 47484
    return-void
.end method

.method private d(LX/0MA;)V
    .locals 10

    .prologue
    .line 47485
    const/4 v1, 0x0

    .line 47486
    const-wide v2, 0x7fffffffffffffffL

    .line 47487
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v5

    .line 47488
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_0

    .line 47489
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Mk;

    iget-object v0, v0, LX/0Mk;->a:LX/0Mq;

    .line 47490
    iget-boolean v6, v0, LX/0Mq;->m:Z

    if-eqz v6, :cond_3

    iget-wide v6, v0, LX/0Mq;->c:J

    cmp-long v6, v6, v2

    if-gez v6, :cond_3

    .line 47491
    iget-wide v2, v0, LX/0Mq;->c:J

    .line 47492
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Mk;

    move-wide v8, v2

    move-object v2, v0

    move-wide v0, v8

    .line 47493
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-wide v8, v0

    move-object v1, v2

    move-wide v2, v8

    goto :goto_0

    .line 47494
    :cond_0
    if-nez v1, :cond_1

    .line 47495
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->k:I

    .line 47496
    :goto_2
    return-void

    .line 47497
    :cond_1
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v0, v2

    .line 47498
    if-gez v0, :cond_2

    .line 47499
    new-instance v0, LX/0L6;

    const-string v1, "Offset to encryption data was negative."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47500
    :cond_2
    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    .line 47501
    iget-object v0, v1, LX/0Mk;->a:LX/0Mq;

    const/4 v3, 0x0

    .line 47502
    iget-object v1, v0, LX/0Mq;->l:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    iget v2, v0, LX/0Mq;->k:I

    invoke-interface {p1, v1, v3, v2}, LX/0MA;->b([BII)V

    .line 47503
    iget-object v1, v0, LX/0Mq;->l:LX/0Oj;

    invoke-virtual {v1, v3}, LX/0Oj;->b(I)V

    .line 47504
    iput-boolean v3, v0, LX/0Mq;->m:Z

    .line 47505
    goto :goto_2

    :cond_3
    move-wide v8, v2

    move-object v2, v1

    move-wide v0, v8

    goto :goto_1
.end method

.method private e(LX/0MA;)Z
    .locals 13

    .prologue
    const/4 v11, 0x4

    const/4 v12, 0x3

    const/4 v0, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 47539
    iget v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->k:I

    if-ne v1, v12, :cond_4

    .line 47540
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    if-nez v1, :cond_3

    .line 47541
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    invoke-static {v1}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(Landroid/util/SparseArray;)LX/0Mk;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    .line 47542
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    if-nez v1, :cond_1

    .line 47543
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->p:J

    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 47544
    if-gez v0, :cond_0

    .line 47545
    new-instance v0, LX/0L6;

    const-string v1, "Offset to end of mdat was negative."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47546
    :cond_0
    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    .line 47547
    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a()V

    .line 47548
    :goto_0
    return v6

    .line 47549
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    iget-object v1, v1, LX/0Mk;->a:LX/0Mq;

    iget-wide v2, v1, LX/0Mq;->b:J

    .line 47550
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v1, v2

    .line 47551
    if-gez v1, :cond_2

    .line 47552
    new-instance v0, LX/0L6;

    const-string v1, "Offset to sample data was negative."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47553
    :cond_2
    invoke-interface {p1, v1}, LX/0MA;->b(I)V

    .line 47554
    :cond_3
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    iget-object v1, v1, LX/0Mk;->a:LX/0Mq;

    iget-object v1, v1, LX/0Mq;->e:[I

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    iget v2, v2, LX/0Mk;->e:I

    aget v1, v1, v2

    iput v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->r:I

    .line 47555
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    iget-object v1, v1, LX/0Mk;->a:LX/0Mq;

    iget-boolean v1, v1, LX/0Mq;->i:Z

    if-eqz v1, :cond_5

    .line 47556
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a(LX/0Mk;)I

    move-result v1

    iput v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->s:I

    .line 47557
    iget v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->r:I

    iget v2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->s:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->r:I

    .line 47558
    :goto_1
    iput v11, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->k:I

    .line 47559
    iput v6, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->t:I

    .line 47560
    :cond_4
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    iget-object v9, v1, LX/0Mk;->a:LX/0Mq;

    .line 47561
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    iget-object v5, v1, LX/0Mk;->c:LX/0Mo;

    .line 47562
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    iget-object v1, v1, LX/0Mk;->b:LX/0LS;

    .line 47563
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    iget v4, v2, LX/0Mk;->e:I

    .line 47564
    iget v2, v5, LX/0Mo;->o:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_7

    .line 47565
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->f:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    .line 47566
    aput-byte v6, v2, v6

    .line 47567
    aput-byte v6, v2, v8

    .line 47568
    aput-byte v6, v2, v0

    .line 47569
    iget v2, v5, LX/0Mo;->o:I

    .line 47570
    iget v3, v5, LX/0Mo;->o:I

    rsub-int/lit8 v3, v3, 0x4

    .line 47571
    :goto_2
    iget v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->s:I

    iget v10, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->r:I

    if-ge v7, v10, :cond_8

    .line 47572
    iget v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->t:I

    if-nez v7, :cond_6

    .line 47573
    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->f:LX/0Oj;

    iget-object v7, v7, LX/0Oj;->a:[B

    invoke-interface {p1, v7, v3, v2}, LX/0MA;->b([BII)V

    .line 47574
    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->f:LX/0Oj;

    invoke-virtual {v7, v6}, LX/0Oj;->b(I)V

    .line 47575
    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->f:LX/0Oj;

    invoke-virtual {v7}, LX/0Oj;->s()I

    move-result v7

    iput v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->t:I

    .line 47576
    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->e:LX/0Oj;

    invoke-virtual {v7, v6}, LX/0Oj;->b(I)V

    .line 47577
    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->e:LX/0Oj;

    invoke-interface {v1, v7, v11}, LX/0LS;->a(LX/0Oj;I)V

    .line 47578
    iget v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->s:I

    add-int/lit8 v7, v7, 0x4

    iput v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->s:I

    .line 47579
    iget v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->r:I

    add-int/2addr v7, v3

    iput v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->r:I

    goto :goto_2

    .line 47580
    :cond_5
    iput v6, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->s:I

    goto :goto_1

    .line 47581
    :cond_6
    iget v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->t:I

    invoke-interface {v1, p1, v7, v6}, LX/0LS;->a(LX/0MA;IZ)I

    move-result v7

    .line 47582
    iget v10, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->s:I

    add-int/2addr v10, v7

    iput v10, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->s:I

    .line 47583
    iget v10, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->t:I

    sub-int v7, v10, v7

    iput v7, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->t:I

    goto :goto_2

    .line 47584
    :cond_7
    :goto_3
    iget v2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->s:I

    iget v3, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->r:I

    if-ge v2, v3, :cond_8

    .line 47585
    iget v2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->r:I

    iget v3, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->s:I

    sub-int/2addr v2, v3

    invoke-interface {v1, p1, v2, v6}, LX/0LS;->a(LX/0MA;IZ)I

    move-result v2

    .line 47586
    iget v3, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->s:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->s:I

    goto :goto_3

    .line 47587
    :cond_8
    invoke-virtual {v9, v4}, LX/0Mq;->c(I)J

    move-result-wide v2

    const-wide/16 v10, 0x3e8

    mul-long/2addr v2, v10

    .line 47588
    iget-boolean v7, v9, LX/0Mq;->i:Z

    if-eqz v7, :cond_a

    :goto_4
    iget-object v7, v9, LX/0Mq;->h:[Z

    aget-boolean v4, v7, v4

    if-eqz v4, :cond_b

    move v4, v8

    :goto_5
    or-int/2addr v4, v0

    .line 47589
    iget-object v0, v9, LX/0Mq;->a:LX/0Mj;

    iget v0, v0, LX/0Mj;->a:I

    .line 47590
    iget-boolean v7, v9, LX/0Mq;->i:Z

    if-eqz v7, :cond_c

    iget-object v5, v5, LX/0Mo;->l:[LX/0Mp;

    aget-object v0, v5, v0

    iget-object v7, v0, LX/0Mp;->c:[B

    .line 47591
    :goto_6
    iget v5, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->r:I

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    .line 47592
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    iget v1, v0, LX/0Mk;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Mk;->e:I

    .line 47593
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    iget v0, v0, LX/0Mk;->e:I

    iget v1, v9, LX/0Mq;->d:I

    if-ne v0, v1, :cond_9

    .line 47594
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->q:LX/0Mk;

    .line 47595
    :cond_9
    iput v12, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->k:I

    move v6, v8

    .line 47596
    goto/16 :goto_0

    :cond_a
    move v0, v6

    .line 47597
    goto :goto_4

    :cond_b
    move v4, v6

    goto :goto_5

    .line 47598
    :cond_c
    const/4 v7, 0x0

    goto :goto_6
.end method


# virtual methods
.method public final a(LX/0MA;LX/0MM;)I
    .locals 1

    .prologue
    .line 47599
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->k:I

    packed-switch v0, :pswitch_data_0

    .line 47600
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->e(LX/0MA;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47601
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 47602
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->b(LX/0MA;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47603
    const/4 v0, -0x1

    goto :goto_1

    .line 47604
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->c(LX/0MA;)V

    goto :goto_0

    .line 47605
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d(LX/0MA;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/0LU;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 47606
    iput-object p1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->u:LX/0LU;

    .line 47607
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->c:LX/0Mo;

    if-eqz v0, :cond_0

    .line 47608
    new-instance v0, LX/0Mk;

    invoke-interface {p1, v3}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0Mk;-><init>(LX/0LS;)V

    .line 47609
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->c:LX/0Mo;

    new-instance v2, LX/0Mj;

    invoke-direct {v2, v3, v3, v3, v3}, LX/0Mj;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, LX/0Mk;->a(LX/0Mo;LX/0Mj;)V

    .line 47610
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 47611
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->u:LX/0LU;

    invoke-interface {v0}, LX/0LU;->a()V

    .line 47612
    :cond_0
    return-void
.end method

.method public final a(LX/0MA;)Z
    .locals 1

    .prologue
    .line 47613
    const/16 v0, 0x1000

    const/4 p0, 0x1

    invoke-static {p1, v0, p0}, LX/0Mn;->a(LX/0MA;IZ)Z

    move-result v0

    move v0, v0

    .line 47614
    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 47615
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 47616
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 47617
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Mk;

    invoke-virtual {v0}, LX/0Mk;->a()V

    .line 47618
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 47619
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 47620
    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/mp4/FragmentedMp4Extractor;->a()V

    .line 47621
    return-void
.end method
