.class public final Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ME;
.implements LX/0M8;


# static fields
.field private static final a:I


# instance fields
.field private final b:LX/0Oj;

.field private final c:LX/0Oj;

.field private final d:LX/0Oj;

.field private final e:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "LX/0Md;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:J

.field private j:I

.field private k:LX/0Oj;

.field private l:I

.field private m:I

.field private n:I

.field private o:LX/0LU;

.field private p:[LX/0Ml;

.field private q:Z

.field private final r:Z

.field private s:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48063
    const-string v0, "qt  "

    invoke-static {v0}, LX/08x;->e(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48061
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;-><init>(Z)V

    .line 48062
    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2

    .prologue
    .line 48053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48054
    new-instance v0, LX/0Oj;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->d:LX/0Oj;

    .line 48055
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->e:Ljava/util/Stack;

    .line 48056
    new-instance v0, LX/0Oj;

    sget-object v1, LX/0Oh;->a:[B

    invoke-direct {v0, v1}, LX/0Oj;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->b:LX/0Oj;

    .line 48057
    new-instance v0, LX/0Oj;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->c:LX/0Oj;

    .line 48058
    iput-boolean p1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->r:Z

    .line 48059
    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->c()V

    .line 48060
    return-void
.end method

.method private a(J)V
    .locals 5

    .prologue
    const/4 v3, 0x3

    .line 48042
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Md;

    iget-wide v0, v0, LX/0Md;->aB:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_2

    .line 48043
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Md;

    .line 48044
    iget v1, v0, LX/0Mc;->aA:I

    sget v2, LX/0Mc;->y:I

    if-ne v1, v2, :cond_1

    .line 48045
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->a(LX/0Md;)V

    .line 48046
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 48047
    iput v3, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->g:I

    goto :goto_0

    .line 48048
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->e:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 48049
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->e:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Md;

    invoke-virtual {v1, v0}, LX/0Md;->a(LX/0Md;)V

    goto :goto_0

    .line 48050
    :cond_2
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->g:I

    if-eq v0, v3, :cond_3

    .line 48051
    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->c()V

    .line 48052
    :cond_3
    return-void
.end method

.method private a(LX/0Md;)V
    .locals 14

    .prologue
    const/4 v3, 0x0

    .line 47964
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 47965
    const/4 v0, 0x0

    .line 47966
    sget v1, LX/0Mc;->at:I

    invoke-virtual {p1, v1}, LX/0Md;->d(I)LX/0Me;

    move-result-object v1

    .line 47967
    if-eqz v1, :cond_4

    .line 47968
    iget-boolean v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->q:Z

    const/4 v2, 0x0

    const/16 v9, 0x8

    .line 47969
    if-eqz v0, :cond_5

    .line 47970
    :cond_0
    :goto_0
    move-object v0, v2

    .line 47971
    move-object v1, v0

    :goto_1
    move v2, v3

    .line 47972
    :goto_2
    iget-object v0, p1, LX/0Md;->aD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 47973
    iget-object v0, p1, LX/0Md;->aD:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Md;

    .line 47974
    iget v5, v0, LX/0Mc;->aA:I

    sget v6, LX/0Mc;->A:I

    if-ne v5, v6, :cond_2

    .line 47975
    sget v5, LX/0Mc;->z:I

    invoke-virtual {p1, v5}, LX/0Md;->d(I)LX/0Me;

    move-result-object v5

    const-wide/16 v6, -0x1

    iget-boolean v8, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->q:Z

    invoke-static {v0, v5, v6, v7, v8}, LX/0Mi;->a(LX/0Md;LX/0Me;JZ)LX/0Mo;

    move-result-object v5

    .line 47976
    if-eqz v5, :cond_2

    .line 47977
    sget v6, LX/0Mc;->B:I

    invoke-virtual {v0, v6}, LX/0Md;->e(I)LX/0Md;

    move-result-object v0

    sget v6, LX/0Mc;->C:I

    invoke-virtual {v0, v6}, LX/0Md;->e(I)LX/0Md;

    move-result-object v0

    sget v6, LX/0Mc;->D:I

    invoke-virtual {v0, v6}, LX/0Md;->e(I)LX/0Md;

    move-result-object v0

    .line 47978
    invoke-static {v5, v0}, LX/0Mi;->a(LX/0Mo;LX/0Md;)LX/0Mr;

    move-result-object v0

    .line 47979
    iget v6, v0, LX/0Mr;->a:I

    if-eqz v6, :cond_2

    .line 47980
    new-instance v6, LX/0Ml;

    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->o:LX/0LU;

    invoke-interface {v7, v2}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v7

    invoke-direct {v6, v5, v0, v7}, LX/0Ml;-><init>(LX/0Mo;LX/0Mr;LX/0LS;)V

    .line 47981
    iget v0, v0, LX/0Mr;->d:I

    add-int/lit8 v0, v0, 0x1e

    .line 47982
    iget-object v5, v5, LX/0Mo;->k:LX/0L4;

    invoke-virtual {v5, v0}, LX/0L4;->a(I)LX/0L4;

    move-result-object v0

    .line 47983
    if-eqz v1, :cond_1

    .line 47984
    iget v5, v1, LX/0ML;->a:I

    iget v7, v1, LX/0ML;->b:I

    invoke-virtual {v0, v5, v7}, LX/0L4;->b(II)LX/0L4;

    move-result-object v0

    .line 47985
    :cond_1
    iget-object v5, v6, LX/0Ml;->c:LX/0LS;

    invoke-interface {v5, v0}, LX/0LS;->a(LX/0L4;)V

    .line 47986
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47987
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 47988
    :cond_3
    new-array v0, v3, [LX/0Ml;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Ml;

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->p:[LX/0Ml;

    .line 47989
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->o:LX/0LU;

    invoke-interface {v0}, LX/0LU;->a()V

    .line 47990
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->o:LX/0LU;

    invoke-interface {v0, p0}, LX/0LU;->a(LX/0M8;)V

    .line 47991
    return-void

    :cond_4
    move-object v1, v0

    goto/16 :goto_1

    .line 47992
    :cond_5
    iget-object v5, v1, LX/0Me;->aB:LX/0Oj;

    .line 47993
    invoke-virtual {v5, v9}, LX/0Oj;->b(I)V

    .line 47994
    :goto_3
    invoke-virtual {v5}, LX/0Oj;->b()I

    move-result v6

    if-lt v6, v9, :cond_0

    .line 47995
    invoke-virtual {v5}, LX/0Oj;->m()I

    move-result v6

    .line 47996
    invoke-virtual {v5}, LX/0Oj;->m()I

    move-result v7

    .line 47997
    sget v8, LX/0Mc;->au:I

    if-ne v7, v8, :cond_c

    .line 47998
    iget v2, v5, LX/0Oj;->b:I

    move v2, v2

    .line 47999
    add-int/lit8 v2, v2, -0x8

    invoke-virtual {v5, v2}, LX/0Oj;->b(I)V

    .line 48000
    iget v2, v5, LX/0Oj;->b:I

    move v2, v2

    .line 48001
    add-int/2addr v2, v6

    invoke-virtual {v5, v2}, LX/0Oj;->a(I)V

    .line 48002
    const/16 v2, 0xc

    invoke-virtual {v5, v2}, LX/0Oj;->c(I)V

    .line 48003
    new-instance v6, LX/0Oj;

    invoke-direct {v6}, LX/0Oj;-><init>()V

    .line 48004
    :goto_4
    invoke-virtual {v5}, LX/0Oj;->b()I

    move-result v2

    const/16 v7, 0x8

    if-lt v2, v7, :cond_e

    .line 48005
    invoke-virtual {v5}, LX/0Oj;->m()I

    move-result v2

    add-int/lit8 v7, v2, -0x8

    .line 48006
    invoke-virtual {v5}, LX/0Oj;->m()I

    move-result v2

    .line 48007
    sget v8, LX/0Mc;->av:I

    if-ne v2, v8, :cond_d

    .line 48008
    iget-object v2, v5, LX/0Oj;->a:[B

    .line 48009
    iget v8, v5, LX/0Oj;->b:I

    move v8, v8

    .line 48010
    add-int/2addr v8, v7

    invoke-virtual {v6, v2, v8}, LX/0Oj;->a([BI)V

    .line 48011
    iget v2, v5, LX/0Oj;->b:I

    move v2, v2

    .line 48012
    invoke-virtual {v6, v2}, LX/0Oj;->b(I)V

    .line 48013
    const/4 v0, 0x4

    const/4 v8, 0x0

    .line 48014
    :cond_6
    :goto_5
    invoke-virtual {v6}, LX/0Oj;->b()I

    move-result v2

    if-lez v2, :cond_b

    .line 48015
    iget v2, v6, LX/0Oj;->b:I

    move v2, v2

    .line 48016
    invoke-virtual {v6}, LX/0Oj;->m()I

    move-result v9

    add-int v11, v2, v9

    .line 48017
    invoke-virtual {v6}, LX/0Oj;->m()I

    move-result v2

    .line 48018
    sget v9, LX/0Mc;->az:I

    if-ne v2, v9, :cond_f

    move-object v2, v8

    move-object v9, v8

    move-object v10, v8

    .line 48019
    :goto_6
    iget v12, v6, LX/0Oj;->b:I

    move v12, v12

    .line 48020
    if-ge v12, v11, :cond_a

    .line 48021
    invoke-virtual {v6}, LX/0Oj;->m()I

    move-result v12

    add-int/lit8 v12, v12, -0xc

    .line 48022
    invoke-virtual {v6}, LX/0Oj;->m()I

    move-result v13

    .line 48023
    invoke-virtual {v6, v0}, LX/0Oj;->c(I)V

    .line 48024
    sget v1, LX/0Mc;->aw:I

    if-ne v13, v1, :cond_7

    .line 48025
    invoke-virtual {v6, v12}, LX/0Oj;->d(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_6

    .line 48026
    :cond_7
    sget v1, LX/0Mc;->ax:I

    if-ne v13, v1, :cond_8

    .line 48027
    invoke-virtual {v6, v12}, LX/0Oj;->d(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_6

    .line 48028
    :cond_8
    sget v1, LX/0Mc;->ay:I

    if-ne v13, v1, :cond_9

    .line 48029
    invoke-virtual {v6, v0}, LX/0Oj;->c(I)V

    .line 48030
    add-int/lit8 v2, v12, -0x4

    invoke-virtual {v6, v2}, LX/0Oj;->d(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 48031
    :cond_9
    invoke-virtual {v6, v12}, LX/0Oj;->c(I)V

    goto :goto_6

    .line 48032
    :cond_a
    if-eqz v9, :cond_6

    if-eqz v2, :cond_6

    const-string v11, "com.apple.iTunes"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 48033
    invoke-static {v9, v2}, LX/0ML;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ML;

    move-result-object v8

    .line 48034
    :cond_b
    move-object v2, v8

    .line 48035
    if-eqz v2, :cond_d

    .line 48036
    :goto_7
    move-object v2, v2

    .line 48037
    goto/16 :goto_0

    .line 48038
    :cond_c
    add-int/lit8 v6, v6, -0x8

    invoke-virtual {v5, v6}, LX/0Oj;->c(I)V

    goto/16 :goto_3

    .line 48039
    :cond_d
    invoke-virtual {v5, v7}, LX/0Oj;->c(I)V

    goto/16 :goto_4

    .line 48040
    :cond_e
    const/4 v2, 0x0

    goto :goto_7

    .line 48041
    :cond_f
    invoke-virtual {v6, v11}, LX/0Oj;->b(I)V

    goto :goto_5
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 47963
    sget v0, LX/0Mc;->O:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->z:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->P:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->Q:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->ah:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->ai:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->aj:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->N:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->ak:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->al:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->am:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->an:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->L:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->a:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->at:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/0Oj;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 47955
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, LX/0Oj;->b(I)V

    .line 47956
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v1

    .line 47957
    sget v2, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->a:I

    if-ne v1, v2, :cond_0

    .line 47958
    :goto_0
    return v0

    .line 47959
    :cond_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, LX/0Oj;->c(I)V

    .line 47960
    :cond_1
    invoke-virtual {p0}, LX/0Oj;->b()I

    move-result v1

    if-lez v1, :cond_2

    .line 47961
    invoke-virtual {p0}, LX/0Oj;->m()I

    move-result v1

    sget v2, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->a:I

    if-ne v1, v2, :cond_1

    goto :goto_0

    .line 47962
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 47954
    sget v0, LX/0Mc;->y:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->A:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->B:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->C:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->D:I

    if-eq p0, v0, :cond_0

    sget v0, LX/0Mc;->M:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/0MA;)Z
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x1

    const/16 v8, 0x8

    const/4 v2, 0x0

    .line 47926
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->j:I

    if-nez v0, :cond_1

    .line 47927
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->d:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    invoke-interface {p1, v0, v2, v8, v1}, LX/0MA;->a([BIIZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47928
    :goto_0
    return v2

    .line 47929
    :cond_0
    iput v8, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->j:I

    .line 47930
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->d:LX/0Oj;

    invoke-virtual {v0, v2}, LX/0Oj;->b(I)V

    .line 47931
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->d:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->k()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->i:J

    .line 47932
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->d:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->m()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->h:I

    .line 47933
    :cond_1
    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->i:J

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    .line 47934
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->d:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    invoke-interface {p1, v0, v8, v8}, LX/0MA;->b([BII)V

    .line 47935
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->j:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->j:I

    .line 47936
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->d:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->u()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->i:J

    .line 47937
    :cond_2
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->h:I

    invoke-static {v0}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 47938
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->i:J

    add-long/2addr v2, v4

    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->j:I

    int-to-long v4, v0

    sub-long/2addr v2, v4

    .line 47939
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->e:Ljava/util/Stack;

    new-instance v4, LX/0Md;

    iget v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->h:I

    invoke-direct {v4, v5, v2, v3}, LX/0Md;-><init>(IJ)V

    invoke-virtual {v0, v4}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 47940
    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->i:J

    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->j:I

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    .line 47941
    invoke-direct {p0, v2, v3}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->a(J)V

    :goto_1
    move v2, v1

    .line 47942
    goto :goto_0

    .line 47943
    :cond_3
    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->c()V

    goto :goto_1

    .line 47944
    :cond_4
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->h:I

    invoke-static {v0}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 47945
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->j:I

    if-ne v0, v8, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 47946
    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->i:J

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-gtz v0, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 47947
    new-instance v0, LX/0Oj;

    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->i:J

    long-to-int v3, v4

    invoke-direct {v0, v3}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->k:LX/0Oj;

    .line 47948
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->d:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->k:LX/0Oj;

    iget-object v3, v3, LX/0Oj;->a:[B

    invoke-static {v0, v2, v3, v2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47949
    iput v9, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->g:I

    goto :goto_1

    :cond_5
    move v0, v2

    .line 47950
    goto :goto_2

    :cond_6
    move v0, v2

    .line 47951
    goto :goto_3

    .line 47952
    :cond_7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->k:LX/0Oj;

    .line 47953
    iput v9, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->g:I

    goto :goto_1
.end method

.method private b(LX/0MA;LX/0MM;)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 47838
    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->i:J

    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->j:I

    int-to-long v6, v0

    sub-long/2addr v4, v6

    .line 47839
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v6

    add-long/2addr v6, v4

    .line 47840
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->k:LX/0Oj;

    if-eqz v0, :cond_1

    .line 47841
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->k:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    iget v3, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->j:I

    long-to-int v4, v4

    invoke-interface {p1, v0, v3, v4}, LX/0MA;->b([BII)V

    .line 47842
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->h:I

    sget v3, LX/0Mc;->a:I

    if-ne v0, v3, :cond_0

    .line 47843
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->k:LX/0Oj;

    invoke-static {v0}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->a(LX/0Oj;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->q:Z

    move v0, v1

    .line 47844
    :goto_0
    invoke-direct {p0, v6, v7}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->a(J)V

    .line 47845
    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->g:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_3

    :goto_1
    return v2

    .line 47846
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 47847
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Md;

    new-instance v3, LX/0Me;

    iget v4, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->h:I

    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->k:LX/0Oj;

    invoke-direct {v3, v4, v5}, LX/0Me;-><init>(ILX/0Oj;)V

    invoke-virtual {v0, v3}, LX/0Md;->a(LX/0Me;)V

    move v0, v1

    goto :goto_0

    .line 47848
    :cond_1
    const-wide/32 v8, 0x40000

    cmp-long v0, v4, v8

    if-gez v0, :cond_2

    .line 47849
    long-to-int v0, v4

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    move v0, v1

    goto :goto_0

    .line 47850
    :cond_2
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v8

    add-long/2addr v4, v8

    iput-wide v4, p2, LX/0MM;->a:J

    move v0, v2

    .line 47851
    goto :goto_0

    :cond_3
    move v2, v1

    .line 47852
    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private c(LX/0MA;LX/0MM;)I
    .locals 10

    .prologue
    .line 48064
    iget-boolean v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->r:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->d()I

    move-result v0

    .line 48065
    :goto_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 48066
    const/4 v0, -0x1

    .line 48067
    :goto_1
    return v0

    .line 48068
    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->e()I

    move-result v0

    goto :goto_0

    .line 48069
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->p:[LX/0Ml;

    aget-object v0, v1, v0

    .line 48070
    iget-object v1, v0, LX/0Ml;->c:LX/0LS;

    .line 48071
    iget v4, v0, LX/0Ml;->d:I

    .line 48072
    iget-object v2, v0, LX/0Ml;->b:LX/0Mr;

    iget-object v2, v2, LX/0Mr;->b:[J

    aget-wide v2, v2, v4

    .line 48073
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v6

    sub-long v6, v2, v6

    iget v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->m:I

    int-to-long v8, v5

    add-long/2addr v6, v8

    .line 48074
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_2

    const-wide/32 v8, 0x40000

    cmp-long v5, v6, v8

    if-ltz v5, :cond_3

    .line 48075
    :cond_2
    iput-wide v2, p2, LX/0MM;->a:J

    .line 48076
    const/4 v0, 0x1

    goto :goto_1

    .line 48077
    :cond_3
    long-to-int v2, v6

    invoke-interface {p1, v2}, LX/0MA;->b(I)V

    .line 48078
    iget-object v2, v0, LX/0Ml;->b:LX/0Mr;

    iget-object v2, v2, LX/0Mr;->c:[I

    aget v2, v2, v4

    iput v2, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->l:I

    .line 48079
    iget-object v2, v0, LX/0Ml;->a:LX/0Mo;

    iget v2, v2, LX/0Mo;->o:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_5

    .line 48080
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->c:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    .line 48081
    const/4 v3, 0x0

    const/4 v5, 0x0

    aput-byte v5, v2, v3

    .line 48082
    const/4 v3, 0x1

    const/4 v5, 0x0

    aput-byte v5, v2, v3

    .line 48083
    const/4 v3, 0x2

    const/4 v5, 0x0

    aput-byte v5, v2, v3

    .line 48084
    iget-object v2, v0, LX/0Ml;->a:LX/0Mo;

    iget v2, v2, LX/0Mo;->o:I

    .line 48085
    iget-object v3, v0, LX/0Ml;->a:LX/0Mo;

    iget v3, v3, LX/0Mo;->o:I

    rsub-int/lit8 v3, v3, 0x4

    .line 48086
    :goto_2
    iget v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->m:I

    iget v6, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->l:I

    if-ge v5, v6, :cond_6

    .line 48087
    iget v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->n:I

    if-nez v5, :cond_4

    .line 48088
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->c:LX/0Oj;

    iget-object v5, v5, LX/0Oj;->a:[B

    invoke-interface {p1, v5, v3, v2}, LX/0MA;->b([BII)V

    .line 48089
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->c:LX/0Oj;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/0Oj;->b(I)V

    .line 48090
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->c:LX/0Oj;

    invoke-virtual {v5}, LX/0Oj;->s()I

    move-result v5

    iput v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->n:I

    .line 48091
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->b:LX/0Oj;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/0Oj;->b(I)V

    .line 48092
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->b:LX/0Oj;

    const/4 v6, 0x4

    invoke-interface {v1, v5, v6}, LX/0LS;->a(LX/0Oj;I)V

    .line 48093
    iget v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->m:I

    add-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->m:I

    .line 48094
    iget v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->l:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->l:I

    goto :goto_2

    .line 48095
    :cond_4
    iget v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->n:I

    const/4 v6, 0x0

    invoke-interface {v1, p1, v5, v6}, LX/0LS;->a(LX/0MA;IZ)I

    move-result v5

    .line 48096
    iget v6, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->m:I

    add-int/2addr v6, v5

    iput v6, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->m:I

    .line 48097
    iget v6, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->n:I

    sub-int v5, v6, v5

    iput v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->n:I

    goto :goto_2

    .line 48098
    :cond_5
    :goto_3
    iget v2, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->m:I

    iget v3, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->l:I

    if-ge v2, v3, :cond_6

    .line 48099
    iget v2, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->l:I

    iget v3, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->m:I

    sub-int/2addr v2, v3

    const/4 v3, 0x0

    invoke-interface {v1, p1, v2, v3}, LX/0LS;->a(LX/0MA;IZ)I

    move-result v2

    .line 48100
    iget v3, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->m:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->m:I

    .line 48101
    iget v3, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->n:I

    sub-int v2, v3, v2

    iput v2, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->n:I

    goto :goto_3

    .line 48102
    :cond_6
    iget-object v2, v0, LX/0Ml;->b:LX/0Mr;

    iget-object v2, v2, LX/0Mr;->e:[J

    aget-wide v2, v2, v4

    iget-object v5, v0, LX/0Ml;->b:LX/0Mr;

    iget-object v5, v5, LX/0Mr;->f:[I

    aget v4, v5, v4

    iget v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->l:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    .line 48103
    iget v1, v0, LX/0Ml;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0Ml;->d:I

    .line 48104
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->m:I

    .line 48105
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->n:I

    .line 48106
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method private c()V
    .locals 1

    .prologue
    .line 47923
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->g:I

    .line 47924
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->j:I

    .line 47925
    return-void
.end method

.method private d()I
    .locals 7

    .prologue
    .line 47913
    const/4 v1, -0x1

    .line 47914
    const-wide v2, 0x7fffffffffffffffL

    .line 47915
    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->p:[LX/0Ml;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 47916
    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->p:[LX/0Ml;

    aget-object v4, v4, v0

    .line 47917
    iget v5, v4, LX/0Ml;->d:I

    .line 47918
    iget-object v6, v4, LX/0Ml;->b:LX/0Mr;

    iget v6, v6, LX/0Mr;->a:I

    if-eq v5, v6, :cond_0

    .line 47919
    iget-object v4, v4, LX/0Ml;->b:LX/0Mr;

    iget-object v4, v4, LX/0Mr;->b:[J

    aget-wide v4, v4, v5

    .line 47920
    cmp-long v6, v4, v2

    if-gez v6, :cond_0

    move-wide v2, v4

    move v1, v0

    .line 47921
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 47922
    :cond_1
    return v1
.end method

.method private e()I
    .locals 10

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    const/4 v4, -0x1

    .line 47894
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->p:[LX/0Ml;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 47895
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->s:I

    .line 47896
    :goto_0
    return v0

    .line 47897
    :cond_0
    const/4 v0, 0x0

    move v1, v4

    move-wide v6, v2

    :goto_1
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->p:[LX/0Ml;

    array-length v5, v5

    if-ge v0, v5, :cond_4

    .line 47898
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->p:[LX/0Ml;

    aget-object v5, v5, v0

    .line 47899
    iget v8, v5, LX/0Ml;->d:I

    .line 47900
    iget-object v9, v5, LX/0Ml;->b:LX/0Mr;

    iget v9, v9, LX/0Mr;->a:I

    if-ne v8, v9, :cond_2

    .line 47901
    iget v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->s:I

    if-ne v0, v5, :cond_1

    .line 47902
    iput v4, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->s:I

    .line 47903
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 47904
    :cond_2
    iget-object v5, v5, LX/0Ml;->b:LX/0Mr;

    iget-object v5, v5, LX/0Mr;->e:[J

    aget-wide v8, v5, v8

    .line 47905
    cmp-long v5, v8, v2

    if-gez v5, :cond_3

    move-wide v2, v8

    move v1, v0

    .line 47906
    :cond_3
    iget v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->s:I

    if-ne v0, v5, :cond_1

    move-wide v6, v8

    .line 47907
    goto :goto_2

    .line 47908
    :cond_4
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->s:I

    if-ne v0, v4, :cond_6

    .line 47909
    iput v1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->s:I

    .line 47910
    :cond_5
    :goto_3
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->s:I

    goto :goto_0

    .line 47911
    :cond_6
    sub-long v2, v6, v2

    const-wide/32 v4, 0x2dc6c0

    cmp-long v0, v2, v4

    if-lez v0, :cond_5

    .line 47912
    iput v1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->s:I

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/0MA;LX/0MM;)I
    .locals 4

    .prologue
    .line 47885
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->g:I

    packed-switch v0, :pswitch_data_0

    .line 47886
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->c(LX/0MA;LX/0MM;)I

    move-result v0

    :goto_1
    return v0

    .line 47887
    :pswitch_0
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 47888
    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->c()V

    goto :goto_0

    .line 47889
    :cond_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->g:I

    goto :goto_0

    .line 47890
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->b(LX/0MA;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47891
    const/4 v0, -0x1

    goto :goto_1

    .line 47892
    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->b(LX/0MA;LX/0MM;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47893
    const/4 v0, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/0LU;)V
    .locals 0

    .prologue
    .line 47883
    iput-object p1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->o:LX/0LU;

    .line 47884
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 47882
    const/4 v0, 0x1

    return v0
.end method

.method public final a(LX/0MA;)Z
    .locals 1

    .prologue
    .line 47880
    const/16 v0, 0x80

    const/4 p0, 0x0

    invoke-static {p1, v0, p0}, LX/0Mn;->a(LX/0MA;IZ)Z

    move-result v0

    move v0, v0

    .line 47881
    return v0
.end method

.method public final b(J)J
    .locals 7

    .prologue
    .line 47859
    const-wide v2, 0x7fffffffffffffffL

    .line 47860
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->p:[LX/0Ml;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 47861
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->p:[LX/0Ml;

    aget-object v1, v1, v0

    iget-object v4, v1, LX/0Ml;->b:LX/0Mr;

    .line 47862
    iget-object v1, v4, LX/0Mr;->e:[J

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {v1, p1, p2, v5, v6}, LX/08x;->a([JJZZ)I

    move-result v1

    .line 47863
    :goto_1
    if-ltz v1, :cond_4

    .line 47864
    iget-object v5, v4, LX/0Mr;->f:[I

    aget v5, v5, v1

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_3

    .line 47865
    :goto_2
    move v1, v1

    .line 47866
    const/4 v5, -0x1

    if-ne v1, v5, :cond_0

    .line 47867
    iget-object v1, v4, LX/0Mr;->e:[J

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {v1, p1, p2, v5, v6}, LX/08x;->b([JJZZ)I

    move-result v1

    .line 47868
    :goto_3
    iget-object v5, v4, LX/0Mr;->e:[J

    array-length v5, v5

    if-ge v1, v5, :cond_6

    .line 47869
    iget-object v5, v4, LX/0Mr;->f:[I

    aget v5, v5, v1

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_5

    .line 47870
    :goto_4
    move v1, v1

    .line 47871
    :cond_0
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->p:[LX/0Ml;

    aget-object v5, v5, v0

    iput v1, v5, LX/0Ml;->d:I

    .line 47872
    iget-object v4, v4, LX/0Mr;->b:[J

    aget-wide v4, v4, v1

    .line 47873
    cmp-long v1, v4, v2

    if-gez v1, :cond_1

    move-wide v2, v4

    .line 47874
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 47875
    :cond_2
    return-wide v2

    .line 47876
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 47877
    :cond_4
    const/4 v1, -0x1

    goto :goto_2

    .line 47878
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 47879
    :cond_6
    const/4 v1, -0x1

    goto :goto_4
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47853
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 47854
    iput v1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->j:I

    .line 47855
    iput v1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->m:I

    .line 47856
    iput v1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->n:I

    .line 47857
    iput v1, p0, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;->g:I

    .line 47858
    return-void
.end method
