.class public final Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ME;
.implements LX/0M8;


# static fields
.field private static final d:I


# instance fields
.field public a:I

.field public b:I

.field public c:J

.field private final e:LX/0Oj;

.field private final g:LX/0Oj;

.field private final h:LX/0Oj;

.field private final i:LX/0Oj;

.field private j:LX/0LU;

.field private k:I

.field private l:I

.field private m:LX/0MS;

.field private n:LX/0MW;

.field private o:LX/0MT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46093
    const-string v0, "FLV"

    invoke-static {v0}, LX/08x;->e(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 46086
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46087
    new-instance v0, LX/0Oj;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    .line 46088
    new-instance v0, LX/0Oj;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->g:LX/0Oj;

    .line 46089
    new-instance v0, LX/0Oj;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->h:LX/0Oj;

    .line 46090
    new-instance v0, LX/0Oj;

    invoke-direct {v0}, LX/0Oj;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->i:LX/0Oj;

    .line 46091
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->k:I

    .line 46092
    return-void
.end method

.method private b(LX/0MA;)Z
    .locals 6

    .prologue
    const/16 v5, 0x9

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 46067
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->g:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    invoke-interface {p1, v2, v0, v5, v1}, LX/0MA;->a([BIIZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 46068
    :goto_0
    return v0

    .line 46069
    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->g:LX/0Oj;

    invoke-virtual {v2, v0}, LX/0Oj;->b(I)V

    .line 46070
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->g:LX/0Oj;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, LX/0Oj;->c(I)V

    .line 46071
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->g:LX/0Oj;

    invoke-virtual {v2}, LX/0Oj;->f()I

    move-result v3

    .line 46072
    and-int/lit8 v2, v3, 0x4

    if-eqz v2, :cond_5

    move v2, v1

    .line 46073
    :goto_1
    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    move v0, v1

    .line 46074
    :cond_1
    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->m:LX/0MS;

    if-nez v2, :cond_2

    .line 46075
    new-instance v2, LX/0MS;

    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->j:LX/0LU;

    const/16 v4, 0x8

    invoke-interface {v3, v4}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0MS;-><init>(LX/0LS;)V

    iput-object v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->m:LX/0MS;

    .line 46076
    :cond_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->n:LX/0MW;

    if-nez v0, :cond_3

    .line 46077
    new-instance v0, LX/0MW;

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->j:LX/0LU;

    invoke-interface {v2, v5}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0MW;-><init>(LX/0LS;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->n:LX/0MW;

    .line 46078
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->o:LX/0MT;

    if-nez v0, :cond_4

    .line 46079
    new-instance v0, LX/0MT;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, LX/0MT;-><init>(LX/0LS;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->o:LX/0MT;

    .line 46080
    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->j:LX/0LU;

    invoke-interface {v0}, LX/0LU;->a()V

    .line 46081
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->j:LX/0LU;

    invoke-interface {v0, p0}, LX/0LU;->a(LX/0M8;)V

    .line 46082
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->g:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->m()I

    move-result v0

    add-int/lit8 v0, v0, -0x9

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->l:I

    .line 46083
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->k:I

    move v0, v1

    .line 46084
    goto :goto_0

    :cond_5
    move v2, v0

    .line 46085
    goto :goto_1
.end method

.method private c(LX/0MA;)V
    .locals 1

    .prologue
    .line 46063
    iget v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->l:I

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    .line 46064
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->l:I

    .line 46065
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->k:I

    .line 46066
    return-void
.end method

.method private d(LX/0MA;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 46053
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->h:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    const/16 v3, 0xb

    invoke-interface {p1, v2, v0, v3, v1}, LX/0MA;->a([BIIZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 46054
    :goto_0
    return v0

    .line 46055
    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->h:LX/0Oj;

    invoke-virtual {v2, v0}, LX/0Oj;->b(I)V

    .line 46056
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->h:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->a:I

    .line 46057
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->h:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->j()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->b:I

    .line 46058
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->h:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->j()I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->c:J

    .line 46059
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->h:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->f()I

    move-result v0

    shl-int/lit8 v0, v0, 0x18

    int-to-long v2, v0

    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->c:J

    or-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->c:J

    .line 46060
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->h:LX/0Oj;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, LX/0Oj;->c(I)V

    .line 46061
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->k:I

    move v0, v1

    .line 46062
    goto :goto_0
.end method

.method private e(LX/0MA;)Z
    .locals 8

    .prologue
    .line 45994
    const/4 v0, 0x1

    .line 45995
    iget v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->a:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->m:LX/0MS;

    if-eqz v1, :cond_1

    .line 45996
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->m:LX/0MS;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->f(LX/0MA;)LX/0Oj;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->c:J

    invoke-virtual {v1, v2, v4, v5}, LX/0MR;->b(LX/0Oj;J)V

    .line 45997
    :cond_0
    :goto_0
    const/4 v1, 0x4

    iput v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->l:I

    .line 45998
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->k:I

    .line 45999
    return v0

    .line 46000
    :cond_1
    iget v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->a:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->n:LX/0MW;

    if-eqz v1, :cond_2

    .line 46001
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->n:LX/0MW;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->f(LX/0MA;)LX/0Oj;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->c:J

    invoke-virtual {v1, v2, v4, v5}, LX/0MR;->b(LX/0Oj;J)V

    goto :goto_0

    .line 46002
    :cond_2
    iget v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->a:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->o:LX/0MT;

    if-eqz v1, :cond_4

    .line 46003
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->o:LX/0MT;

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->f(LX/0MA;)LX/0Oj;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->c:J

    invoke-virtual {v1, v2, v4, v5}, LX/0MR;->b(LX/0Oj;J)V

    .line 46004
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->o:LX/0MT;

    .line 46005
    iget-wide v6, v1, LX/0MR;->b:J

    move-wide v2, v6

    .line 46006
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 46007
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->m:LX/0MS;

    if-eqz v1, :cond_3

    .line 46008
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->m:LX/0MS;

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->o:LX/0MT;

    .line 46009
    iget-wide v6, v2, LX/0MR;->b:J

    move-wide v2, v6

    .line 46010
    iput-wide v2, v1, LX/0MR;->b:J

    .line 46011
    :cond_3
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->n:LX/0MW;

    if-eqz v1, :cond_0

    .line 46012
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->n:LX/0MW;

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->o:LX/0MT;

    .line 46013
    iget-wide v6, v2, LX/0MR;->b:J

    move-wide v2, v6

    .line 46014
    iput-wide v2, v1, LX/0MR;->b:J

    .line 46015
    goto :goto_0

    .line 46016
    :cond_4
    iget v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->b:I

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    .line 46017
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(LX/0MA;)LX/0Oj;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 46047
    iget v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->b:I

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->i:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->e()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 46048
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->i:LX/0Oj;

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->i:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->e()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->b:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-array v1, v1, [B

    invoke-virtual {v0, v1, v3}, LX/0Oj;->a([BI)V

    .line 46049
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->i:LX/0Oj;

    iget v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->b:I

    invoke-virtual {v0, v1}, LX/0Oj;->a(I)V

    .line 46050
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->i:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    iget v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->b:I

    invoke-interface {p1, v0, v3, v1}, LX/0MA;->b([BII)V

    .line 46051
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->i:LX/0Oj;

    return-object v0

    .line 46052
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->i:LX/0Oj;

    invoke-virtual {v0, v3}, LX/0Oj;->b(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0MA;LX/0MM;)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 46040
    :cond_0
    :goto_0
    iget v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->k:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 46041
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->b(LX/0MA;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 46042
    :goto_1
    return v0

    .line 46043
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->c(LX/0MA;)V

    goto :goto_0

    .line 46044
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->d(LX/0MA;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 46045
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e(LX/0MA;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46046
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/0LU;)V
    .locals 0

    .prologue
    .line 46038
    iput-object p1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->j:LX/0LU;

    .line 46039
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 46037
    const/4 v0, 0x0

    return v0
.end method

.method public final a(LX/0MA;)Z
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v0, 0x0

    .line 46022
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    const/4 v2, 0x3

    invoke-interface {p1, v1, v0, v2}, LX/0MA;->c([BII)V

    .line 46023
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    invoke-virtual {v1, v0}, LX/0Oj;->b(I)V

    .line 46024
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->j()I

    move-result v1

    sget v2, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->d:I

    if-eq v1, v2, :cond_1

    .line 46025
    :cond_0
    :goto_0
    return v0

    .line 46026
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    const/4 v2, 0x2

    invoke-interface {p1, v1, v0, v2}, LX/0MA;->c([BII)V

    .line 46027
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    invoke-virtual {v1, v0}, LX/0Oj;->b(I)V

    .line 46028
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->g()I

    move-result v1

    and-int/lit16 v1, v1, 0xfa

    if-nez v1, :cond_0

    .line 46029
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    invoke-interface {p1, v1, v0, v3}, LX/0MA;->c([BII)V

    .line 46030
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    invoke-virtual {v1, v0}, LX/0Oj;->b(I)V

    .line 46031
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->m()I

    move-result v1

    .line 46032
    invoke-interface {p1}, LX/0MA;->a()V

    .line 46033
    invoke-interface {p1, v1}, LX/0MA;->c(I)V

    .line 46034
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    invoke-interface {p1, v1, v0, v3}, LX/0MA;->c([BII)V

    .line 46035
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    invoke-virtual {v1, v0}, LX/0Oj;->b(I)V

    .line 46036
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->e:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->m()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(J)J
    .locals 2

    .prologue
    .line 46021
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 46018
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->k:I

    .line 46019
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/extractor/flv/FlvExtractor;->l:I

    .line 46020
    return-void
.end method
