.class public final Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ME;
.implements LX/0M8;


# instance fields
.field private final a:LX/0Oj;

.field private final b:LX/0Ms;

.field private c:LX/0LS;

.field private d:LX/0Mx;

.field private e:I

.field private g:J

.field private h:Z

.field private final i:LX/0Mt;

.field private j:J

.field private k:LX/0LU;

.field private l:LX/0N2;

.field private m:LX/0N0;

.field private n:J

.field private o:J

.field private p:J

.field private q:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 48503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48504
    new-instance v0, LX/0Oj;

    const v1, 0xfe01

    new-array v1, v1, [B

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/0Oj;-><init>([BI)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    .line 48505
    new-instance v0, LX/0Ms;

    invoke-direct {v0}, LX/0Ms;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->b:LX/0Ms;

    .line 48506
    new-instance v0, LX/0Mt;

    invoke-direct {v0}, LX/0Mt;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->i:LX/0Mt;

    .line 48507
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->j:J

    .line 48508
    return-void
.end method

.method private static a(BLX/0Mx;)I
    .locals 5

    .prologue
    .line 48497
    iget v0, p1, LX/0Mx;->e:I

    const/4 v1, 0x1

    .line 48498
    shr-int v2, p0, v1

    const/16 v3, 0xff

    rsub-int/lit8 v4, v0, 0x8

    ushr-int/2addr v3, v4

    and-int/2addr v2, v3

    move v0, v2

    .line 48499
    iget-object v1, p1, LX/0Mx;->d:[LX/0N1;

    aget-object v0, v1, v0

    iget-boolean v0, v0, LX/0N1;->a:Z

    if-nez v0, :cond_0

    .line 48500
    iget-object v0, p1, LX/0Mx;->a:LX/0N2;

    iget v0, v0, LX/0N2;->g:I

    .line 48501
    :goto_0
    return v0

    .line 48502
    :cond_0
    iget-object v0, p1, LX/0Mx;->a:LX/0N2;

    iget v0, v0, LX/0N2;->h:I

    goto :goto_0
.end method

.method private a(LX/0MA;LX/0Oj;)LX/0Mx;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 48479
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->l:LX/0N2;

    if-nez v0, :cond_0

    .line 48480
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->b:LX/0Ms;

    invoke-virtual {v0, p1, p2}, LX/0Ms;->a(LX/0MA;LX/0Oj;)Z

    .line 48481
    invoke-static {p2}, LX/0N3;->a(LX/0Oj;)LX/0N2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->l:LX/0N2;

    .line 48482
    invoke-virtual {p2}, LX/0Oj;->a()V

    .line 48483
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->m:LX/0N0;

    if-nez v0, :cond_1

    .line 48484
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->b:LX/0Ms;

    invoke-virtual {v0, p1, p2}, LX/0Ms;->a(LX/0MA;LX/0Oj;)Z

    .line 48485
    invoke-static {p2}, LX/0N3;->b(LX/0Oj;)LX/0N0;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->m:LX/0N0;

    .line 48486
    invoke-virtual {p2}, LX/0Oj;->a()V

    .line 48487
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->b:LX/0Ms;

    invoke-virtual {v0, p1, p2}, LX/0Ms;->a(LX/0MA;LX/0Oj;)Z

    .line 48488
    iget v0, p2, LX/0Oj;->c:I

    move v0, v0

    .line 48489
    new-array v3, v0, [B

    .line 48490
    iget-object v0, p2, LX/0Oj;->a:[B

    .line 48491
    iget v1, p2, LX/0Oj;->c:I

    move v1, v1

    .line 48492
    invoke-static {v0, v2, v3, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 48493
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->l:LX/0N2;

    iget v0, v0, LX/0N2;->b:I

    invoke-static {p2, v0}, LX/0N3;->a(LX/0Oj;I)[LX/0N1;

    move-result-object v4

    .line 48494
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, LX/0N3;->a(I)I

    move-result v5

    .line 48495
    invoke-virtual {p2}, LX/0Oj;->a()V

    .line 48496
    new-instance v0, LX/0Mx;

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->l:LX/0N2;

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->m:LX/0N0;

    invoke-direct/range {v0 .. v5}, LX/0Mx;-><init>(LX/0N2;LX/0N0;[B[LX/0N1;I)V

    return-object v0
.end method

.method private static a(LX/0Oj;J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0xff

    .line 48387
    iget v0, p0, LX/0Oj;->c:I

    move v0, v0

    .line 48388
    add-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, LX/0Oj;->a(I)V

    .line 48389
    iget-object v0, p0, LX/0Oj;->a:[B

    .line 48390
    iget v1, p0, LX/0Oj;->c:I

    move v1, v1

    .line 48391
    add-int/lit8 v1, v1, -0x4

    and-long v2, p1, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 48392
    iget-object v0, p0, LX/0Oj;->a:[B

    .line 48393
    iget v1, p0, LX/0Oj;->c:I

    move v1, v1

    .line 48394
    add-int/lit8 v1, v1, -0x3

    const/16 v2, 0x8

    ushr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 48395
    iget-object v0, p0, LX/0Oj;->a:[B

    .line 48396
    iget v1, p0, LX/0Oj;->c:I

    move v1, v1

    .line 48397
    add-int/lit8 v1, v1, -0x2

    const/16 v2, 0x10

    ushr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 48398
    iget-object v0, p0, LX/0Oj;->a:[B

    .line 48399
    iget v1, p0, LX/0Oj;->c:I

    move v1, v1

    .line 48400
    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0x18

    ushr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 48401
    return-void
.end method


# virtual methods
.method public final a(LX/0MA;LX/0MM;)I
    .locals 16

    .prologue
    .line 48430
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->p:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 48431
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->d:LX/0Mx;

    if-nez v2, :cond_0

    .line 48432
    invoke-interface/range {p1 .. p1}, LX/0MA;->d()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->n:J

    .line 48433
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a(LX/0MA;LX/0Oj;)LX/0Mx;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->d:LX/0Mx;

    .line 48434
    invoke-interface/range {p1 .. p1}, LX/0MA;->c()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->o:J

    .line 48435
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->k:LX/0LU;

    move-object/from16 v0, p0

    invoke-interface {v2, v0}, LX/0LU;->a(LX/0M8;)V

    .line 48436
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->n:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 48437
    invoke-interface/range {p1 .. p1}, LX/0MA;->d()J

    move-result-wide v2

    const-wide/16 v4, 0x1f40

    sub-long/2addr v2, v4

    move-object/from16 v0, p2

    iput-wide v2, v0, LX/0MM;->a:J

    .line 48438
    const/4 v2, 0x1

    .line 48439
    :goto_0
    return v2

    .line 48440
    :cond_0
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->n:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    const-wide/16 v2, -0x1

    :goto_1
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->p:J

    .line 48441
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 48442
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->d:LX/0Mx;

    iget-object v2, v2, LX/0Mx;->a:LX/0N2;

    iget-object v2, v2, LX/0N2;->j:[B

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48443
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->d:LX/0Mx;

    iget-object v2, v2, LX/0Mx;->c:[B

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48444
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->n:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    const-wide/16 v2, -0x1

    :goto_2
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->q:J

    .line 48445
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->c:LX/0LS;

    const/4 v2, 0x0

    const-string v3, "audio/vorbis"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->d:LX/0Mx;

    iget-object v4, v4, LX/0Mx;->a:LX/0N2;

    iget v4, v4, LX/0N2;->e:I

    const v5, 0xfe01

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->q:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->d:LX/0Mx;

    iget-object v8, v8, LX/0Mx;->a:LX/0N2;

    iget v8, v8, LX/0N2;->b:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->d:LX/0Mx;

    iget-object v9, v9, LX/0Mx;->a:LX/0N2;

    iget-wide v14, v9, LX/0N2;->c:J

    long-to-int v9, v14

    const/4 v11, 0x0

    invoke-static/range {v2 .. v11}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v2

    invoke-interface {v12, v2}, LX/0LS;->a(LX/0L4;)V

    .line 48446
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->n:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 48447
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->i:LX/0Mt;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->n:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->o:J

    sub-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->p:J

    invoke-virtual {v2, v4, v5, v6, v7}, LX/0Mt;->a(JJ)V

    .line 48448
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->o:J

    move-object/from16 v0, p2

    iput-wide v2, v0, LX/0MM;->a:J

    .line 48449
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 48450
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->b:LX/0Ms;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/0Ms;->a(LX/0MA;)J

    move-result-wide v2

    goto/16 :goto_1

    .line 48451
    :cond_2
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->p:J

    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->d:LX/0Mx;

    iget-object v4, v4, LX/0Mx;->a:LX/0N2;

    iget-wide v4, v4, LX/0N2;->c:J

    div-long/2addr v2, v4

    goto :goto_2

    .line 48452
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->h:Z

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->j:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    .line 48453
    invoke-static/range {p1 .. p1}, LX/0Mw;->a(LX/0MA;)V

    .line 48454
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->i:LX/0Mt;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->j:J

    move-object/from16 v0, p1

    invoke-virtual {v2, v4, v5, v0}, LX/0Mt;->a(JLX/0MA;)J

    move-result-wide v2

    .line 48455
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_4

    .line 48456
    move-object/from16 v0, p2

    iput-wide v2, v0, LX/0MM;->a:J

    .line 48457
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 48458
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->b:LX/0Ms;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->j:J

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v4, v5}, LX/0Ms;->a(LX/0MA;J)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->g:J

    .line 48459
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->l:LX/0N2;

    iget v2, v2, LX/0N2;->g:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->e:I

    .line 48460
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->h:Z

    .line 48461
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->i:LX/0Mt;

    invoke-virtual {v2}, LX/0Mt;->a()V

    .line 48462
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->b:LX/0Ms;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v3}, LX/0Ms;->a(LX/0MA;LX/0Oj;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 48463
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-eq v2, v3, :cond_7

    .line 48464
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->d:LX/0Mx;

    invoke-static {v2, v3}, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a(BLX/0Mx;)I

    move-result v10

    .line 48465
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->h:Z

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->e:I

    add-int/2addr v2, v10

    div-int/lit8 v2, v2, 0x4

    .line 48466
    :goto_3
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->g:J

    int-to-long v6, v2

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->j:J

    cmp-long v3, v4, v6

    if-ltz v3, :cond_6

    .line 48467
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    int-to-long v4, v2

    invoke-static {v3, v4, v5}, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a(LX/0Oj;J)V

    .line 48468
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->g:J

    const-wide/32 v6, 0xf4240

    mul-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->d:LX/0Mx;

    iget-object v3, v3, LX/0Mx;->a:LX/0N2;

    iget-wide v6, v3, LX/0N2;->c:J

    div-long/2addr v4, v6

    .line 48469
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->c:LX/0LS;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    invoke-virtual {v7}, LX/0Oj;->c()I

    move-result v7

    invoke-interface {v3, v6, v7}, LX/0LS;->a(LX/0Oj;I)V

    .line 48470
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->c:LX/0LS;

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    invoke-virtual {v7}, LX/0Oj;->c()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-interface/range {v3 .. v9}, LX/0LS;->a(JIII[B)V

    .line 48471
    const-wide/16 v4, -0x1

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->j:J

    .line 48472
    :cond_6
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->h:Z

    .line 48473
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->g:J

    int-to-long v2, v2

    add-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->g:J

    .line 48474
    move-object/from16 v0, p0

    iput v10, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->e:I

    .line 48475
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    invoke-virtual {v2}, LX/0Oj;->a()V

    .line 48476
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 48477
    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    .line 48478
    :cond_9
    const/4 v2, -0x1

    goto/16 :goto_0
.end method

.method public final a(LX/0LU;)V
    .locals 1

    .prologue
    .line 48426
    const/4 v0, 0x0

    invoke-interface {p1, v0}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->c:LX/0LS;

    .line 48427
    invoke-interface {p1}, LX/0LU;->a()V

    .line 48428
    iput-object p1, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->k:LX/0LU;

    .line 48429
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 48425
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->d:LX/0Mx;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->n:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0MA;)Z
    .locals 5

    .prologue
    const/4 v4, 0x7

    const/4 v0, 0x0

    .line 48417
    :try_start_0
    new-instance v1, LX/0Mv;

    invoke-direct {v1}, LX/0Mv;-><init>()V

    .line 48418
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    const/4 v3, 0x1

    invoke-static {p1, v1, v2, v3}, LX/0Mw;->a(LX/0MA;LX/0Mv;LX/0Oj;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v1, LX/0Mv;->b:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget v1, v1, LX/0Mv;->i:I
    :try_end_0
    .catch LX/0L6; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v1, v4, :cond_1

    .line 48419
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->a()V

    .line 48420
    :goto_0
    return v0

    .line 48421
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->a()V

    .line 48422
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    const/4 v2, 0x0

    const/4 v3, 0x7

    invoke-interface {p1, v1, v2, v3}, LX/0MA;->c([BII)V

    .line 48423
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, LX/0N3;->a(ILX/0Oj;Z)Z
    :try_end_1
    .catch LX/0L6; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 48424
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->a()V

    goto :goto_0

    :catch_0
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->a()V

    throw v0
.end method

.method public final b(J)J
    .locals 7

    .prologue
    .line 48411
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 48412
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->j:J

    .line 48413
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->o:J

    .line 48414
    :goto_0
    return-wide v0

    .line 48415
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->d:LX/0Mx;

    iget-object v0, v0, LX/0Mx;->a:LX/0N2;

    iget-wide v0, v0, LX/0N2;->c:J

    mul-long/2addr v0, p1

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->j:J

    .line 48416
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->o:J

    iget-wide v2, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->n:J

    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->o:J

    sub-long/2addr v2, v4

    mul-long/2addr v2, p1

    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->q:J

    div-long/2addr v2, v4

    const-wide/16 v4, 0xfa0

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48402
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->b:LX/0Ms;

    .line 48403
    iget-object v1, v0, LX/0Ms;->a:LX/0Mv;

    invoke-virtual {v1}, LX/0Mv;->a()V

    .line 48404
    iget-object v1, v0, LX/0Ms;->b:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->a()V

    .line 48405
    const/4 v1, -0x1

    iput v1, v0, LX/0Ms;->d:I

    .line 48406
    iput v2, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->e:I

    .line 48407
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->g:J

    .line 48408
    iput-boolean v2, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->h:Z

    .line 48409
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ogg/OggVorbisExtractor;->a:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->a()V

    .line 48410
    return-void
.end method
