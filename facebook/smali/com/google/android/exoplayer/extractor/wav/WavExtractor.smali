.class public final Lcom/google/android/exoplayer/extractor/wav/WavExtractor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ME;
.implements LX/0M8;


# instance fields
.field private a:LX/0LU;

.field private b:LX/0LS;

.field private c:LX/0NP;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0MA;LX/0MM;)I
    .locals 13

    .prologue
    const v3, 0x8000

    const/4 v12, 0x1

    const/4 v10, -0x1

    const/4 v0, 0x0

    .line 50251
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    if-nez v1, :cond_1

    .line 50252
    invoke-static {p1}, LX/0NR;->a(LX/0MA;)LX/0NP;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    .line 50253
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    if-nez v1, :cond_0

    .line 50254
    new-instance v0, LX/0L6;

    const-string v1, "Error initializing WavHeader. Did you sniff first?"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50255
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    .line 50256
    iget v2, v1, LX/0NP;->d:I

    move v1, v2

    .line 50257
    iput v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->d:I

    .line 50258
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    invoke-virtual {v1}, LX/0NP;->f()Z

    move-result v1

    if-nez v1, :cond_2

    .line 50259
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    invoke-static {p1, v1}, LX/0NR;->a(LX/0MA;LX/0NP;)V

    .line 50260
    iget-object v11, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->b:LX/0LS;

    const-string v1, "audio/raw"

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    .line 50261
    iget v4, v2, LX/0NP;->b:I

    iget v5, v2, LX/0NP;->e:I

    mul-int/2addr v4, v5

    iget v5, v2, LX/0NP;->a:I

    mul-int/2addr v4, v5

    move v2, v4

    .line 50262
    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    invoke-virtual {v4}, LX/0NP;->a()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    .line 50263
    iget v7, v6, LX/0NP;->a:I

    move v6, v7

    .line 50264
    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    .line 50265
    iget v8, v7, LX/0NP;->b:I

    move v7, v8

    .line 50266
    move-object v8, v0

    move-object v9, v0

    invoke-static/range {v0 .. v9}, LX/0L4;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)LX/0L4;

    move-result-object v1

    invoke-interface {v11, v1}, LX/0LS;->a(LX/0L4;)V

    .line 50267
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->a:LX/0LU;

    invoke-interface {v1, p0}, LX/0LU;->a(LX/0M8;)V

    .line 50268
    :cond_2
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->b:LX/0LS;

    iget v2, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->e:I

    sub-int v2, v3, v2

    invoke-interface {v1, p1, v2, v12}, LX/0LS;->a(LX/0MA;IZ)I

    move-result v8

    .line 50269
    if-eq v8, v10, :cond_3

    .line 50270
    iget v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->e:I

    add-int/2addr v1, v8

    iput v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->e:I

    .line 50271
    :cond_3
    iget v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->e:I

    iget v2, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->d:I

    div-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->d:I

    mul-int v5, v1, v2

    .line 50272
    if-lez v5, :cond_4

    .line 50273
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v2

    iget v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->e:I

    int-to-long v6, v1

    sub-long/2addr v2, v6

    .line 50274
    iget v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->e:I

    sub-int/2addr v1, v5

    iput v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->e:I

    .line 50275
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->b:LX/0LS;

    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    invoke-virtual {v4, v2, v3}, LX/0NP;->b(J)J

    move-result-wide v2

    iget v6, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->e:I

    move v4, v12

    move-object v7, v0

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    .line 50276
    :cond_4
    if-ne v8, v10, :cond_5

    move v0, v10

    .line 50277
    :goto_0
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0LU;)V
    .locals 1

    .prologue
    .line 50246
    iput-object p1, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->a:LX/0LU;

    .line 50247
    const/4 v0, 0x0

    invoke-interface {p1, v0}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->b:LX/0LS;

    .line 50248
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    .line 50249
    invoke-interface {p1}, LX/0LU;->a()V

    .line 50250
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 50238
    const/4 v0, 0x1

    return v0
.end method

.method public final a(LX/0MA;)Z
    .locals 1

    .prologue
    .line 50245
    invoke-static {p1}, LX/0NR;->a(LX/0MA;)LX/0NP;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(J)J
    .locals 8

    .prologue
    .line 50241
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->c:LX/0NP;

    .line 50242
    iget v3, v0, LX/0NP;->c:I

    int-to-long v3, v3

    mul-long/2addr v3, p1

    const-wide/32 v5, 0xf4240

    div-long/2addr v3, v5

    .line 50243
    iget v5, v0, LX/0NP;->a:I

    int-to-long v5, v5

    div-long/2addr v3, v5

    iget v5, v0, LX/0NP;->a:I

    int-to-long v5, v5

    mul-long/2addr v3, v5

    iget-wide v5, v0, LX/0NP;->f:J

    add-long/2addr v3, v5

    move-wide v0, v3

    .line 50244
    return-wide v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 50239
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/extractor/wav/WavExtractor;->e:I

    .line 50240
    return-void
.end method
