.class public final Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ME;


# static fields
.field private static final a:I


# instance fields
.field private final b:J

.field private final c:LX/0Oj;

.field private d:LX/0N6;

.field private e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48891
    const-string v0, "ID3"

    invoke-static {v0}, LX/08x;->e(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48892
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;-><init>(J)V

    .line 48893
    return-void
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 48894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48895
    iput-wide p1, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->b:J

    .line 48896
    new-instance v0, LX/0Oj;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->c:LX/0Oj;

    .line 48897
    return-void
.end method


# virtual methods
.method public final a(LX/0MA;LX/0MM;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 48898
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->c:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    const/16 v3, 0xc8

    invoke-interface {p1, v2, v1, v3}, LX/0MA;->a([BII)I

    move-result v2

    .line 48899
    if-ne v2, v0, :cond_0

    .line 48900
    :goto_0
    return v0

    .line 48901
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->c:LX/0Oj;

    invoke-virtual {v0, v1}, LX/0Oj;->b(I)V

    .line 48902
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->c:LX/0Oj;

    invoke-virtual {v0, v2}, LX/0Oj;->a(I)V

    .line 48903
    iget-boolean v0, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->e:Z

    if-nez v0, :cond_1

    .line 48904
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->d:LX/0N6;

    iget-wide v2, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->b:J

    invoke-virtual {v0, v2, v3, v4}, LX/0N6;->a(JZ)V

    .line 48905
    iput-boolean v4, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->e:Z

    .line 48906
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->d:LX/0N6;

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->c:LX/0Oj;

    invoke-virtual {v0, v2}, LX/0N6;->a(LX/0Oj;)V

    move v0, v1

    .line 48907
    goto :goto_0
.end method

.method public final a(LX/0LU;)V
    .locals 3

    .prologue
    .line 48908
    new-instance v0, LX/0N6;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {p1, v2}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/0N6;-><init>(LX/0LS;LX/0LS;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->d:LX/0N6;

    .line 48909
    invoke-interface {p1}, LX/0LU;->a()V

    .line 48910
    sget-object v0, LX/0M8;->f:LX/0M8;

    invoke-interface {p1, v0}, LX/0LU;->a(LX/0M8;)V

    .line 48911
    return-void
.end method

.method public final a(LX/0MA;)Z
    .locals 10

    .prologue
    const/16 v7, 0xa

    const/4 v9, 0x4

    const/4 v1, 0x0

    .line 48912
    new-instance v5, LX/0Oj;

    invoke-direct {v5, v7}, LX/0Oj;-><init>(I)V

    .line 48913
    new-instance v6, LX/0Oi;

    iget-object v0, v5, LX/0Oj;->a:[B

    invoke-direct {v6, v0}, LX/0Oi;-><init>([B)V

    move v0, v1

    .line 48914
    :goto_0
    iget-object v2, v5, LX/0Oj;->a:[B

    invoke-interface {p1, v2, v1, v7}, LX/0MA;->c([BII)V

    .line 48915
    invoke-virtual {v5, v1}, LX/0Oj;->b(I)V

    .line 48916
    invoke-virtual {v5}, LX/0Oj;->j()I

    move-result v2

    sget v3, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->a:I

    if-ne v2, v3, :cond_0

    .line 48917
    iget-object v2, v5, LX/0Oj;->a:[B

    const/4 v3, 0x6

    aget-byte v2, v2, v3

    and-int/lit8 v2, v2, 0x7f

    shl-int/lit8 v2, v2, 0x15

    iget-object v3, v5, LX/0Oj;->a:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x7f

    shl-int/lit8 v3, v3, 0xe

    or-int/2addr v2, v3

    iget-object v3, v5, LX/0Oj;->a:[B

    const/16 v4, 0x8

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x7f

    shl-int/lit8 v3, v3, 0x7

    or-int/2addr v2, v3

    iget-object v3, v5, LX/0Oj;->a:[B

    const/16 v4, 0x9

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x7f

    or-int/2addr v2, v3

    .line 48918
    add-int/lit8 v3, v2, 0xa

    add-int/2addr v0, v3

    .line 48919
    invoke-interface {p1, v2}, LX/0MA;->c(I)V

    goto :goto_0

    .line 48920
    :cond_0
    invoke-interface {p1}, LX/0MA;->a()V

    .line 48921
    invoke-interface {p1, v0}, LX/0MA;->c(I)V

    move v2, v1

    move v3, v1

    move v4, v0

    .line 48922
    :goto_1
    iget-object v7, v5, LX/0Oj;->a:[B

    const/4 v8, 0x2

    invoke-interface {p1, v7, v1, v8}, LX/0MA;->c([BII)V

    .line 48923
    invoke-virtual {v5, v1}, LX/0Oj;->b(I)V

    .line 48924
    invoke-virtual {v5}, LX/0Oj;->g()I

    move-result v7

    .line 48925
    const v8, 0xfff6

    and-int/2addr v7, v8

    const v8, 0xfff0

    if-eq v7, v8, :cond_2

    .line 48926
    invoke-interface {p1}, LX/0MA;->a()V

    .line 48927
    add-int/lit8 v2, v4, 0x1

    sub-int v3, v2, v0

    const/16 v4, 0x2000

    if-lt v3, v4, :cond_1

    .line 48928
    :goto_2
    return v1

    .line 48929
    :cond_1
    invoke-interface {p1, v2}, LX/0MA;->c(I)V

    move v3, v1

    move v4, v2

    move v2, v1

    goto :goto_1

    .line 48930
    :cond_2
    add-int/lit8 v2, v2, 0x1

    if-lt v2, v9, :cond_3

    const/16 v7, 0xbc

    if-le v3, v7, :cond_3

    .line 48931
    const/4 v1, 0x1

    goto :goto_2

    .line 48932
    :cond_3
    iget-object v7, v5, LX/0Oj;->a:[B

    invoke-interface {p1, v7, v1, v9}, LX/0MA;->c([BII)V

    .line 48933
    const/16 v7, 0xe

    invoke-virtual {v6, v7}, LX/0Oi;->a(I)V

    .line 48934
    const/16 v7, 0xd

    invoke-virtual {v6, v7}, LX/0Oi;->c(I)I

    move-result v7

    .line 48935
    add-int/lit8 v8, v7, -0x6

    invoke-interface {p1, v8}, LX/0MA;->c(I)V

    .line 48936
    add-int/2addr v3, v7

    .line 48937
    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 48938
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->e:Z

    .line 48939
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/AdtsExtractor;->d:LX/0N6;

    invoke-virtual {v0}, LX/0N6;->a()V

    .line 48940
    return-void
.end method
