.class public final Lcom/google/android/exoplayer/extractor/ts/TsExtractor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ME;


# static fields
.field public static final d:J

.field public static final e:J

.field public static final f:J


# instance fields
.field public final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0NL;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/util/SparseBooleanArray;

.field public c:LX/0NF;

.field public final g:LX/0NJ;

.field public final h:I

.field private final i:LX/0Oj;

.field private final j:LX/0Oi;

.field private k:LX/0LU;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50235
    const-string v0, "AC-3"

    invoke-static {v0}, LX/08x;->e(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->d:J

    .line 50236
    const-string v0, "EAC3"

    invoke-static {v0}, LX/08x;->e(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->e:J

    .line 50237
    const-string v0, "HEVC"

    invoke-static {v0}, LX/08x;->e(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->f:J

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 50233
    new-instance v0, LX/0NJ;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, LX/0NJ;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;-><init>(LX/0NJ;)V

    .line 50234
    return-void
.end method

.method private constructor <init>(LX/0NJ;)V
    .locals 1

    .prologue
    .line 50182
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;-><init>(LX/0NJ;I)V

    .line 50183
    return-void
.end method

.method public constructor <init>(LX/0NJ;I)V
    .locals 3

    .prologue
    .line 50224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50225
    iput-object p1, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->g:LX/0NJ;

    .line 50226
    iput p2, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->h:I

    .line 50227
    new-instance v0, LX/0Oj;

    const/16 v1, 0xbc

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->i:LX/0Oj;

    .line 50228
    new-instance v0, LX/0Oi;

    const/4 v1, 0x3

    new-array v1, v1, [B

    invoke-direct {v0, v1}, LX/0Oi;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->j:LX/0Oi;

    .line 50229
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->a:Landroid/util/SparseArray;

    .line 50230
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    new-instance v2, LX/0NM;

    invoke-direct {v2, p0}, LX/0NM;-><init>(Lcom/google/android/exoplayer/extractor/ts/TsExtractor;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 50231
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->b:Landroid/util/SparseBooleanArray;

    .line 50232
    return-void
.end method


# virtual methods
.method public final a(LX/0MA;LX/0MM;)I
    .locals 6

    .prologue
    const/16 v2, 0xbc

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 50200
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->i:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    invoke-interface {p1, v0, v1, v2, v4}, LX/0MA;->a([BIIZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 50201
    const/4 v0, -0x1

    .line 50202
    :goto_0
    return v0

    .line 50203
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->i:LX/0Oj;

    invoke-virtual {v0, v1}, LX/0Oj;->b(I)V

    .line 50204
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->i:LX/0Oj;

    invoke-virtual {v0, v2}, LX/0Oj;->a(I)V

    .line 50205
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->i:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->f()I

    move-result v0

    .line 50206
    const/16 v2, 0x47

    if-eq v0, v2, :cond_1

    move v0, v1

    .line 50207
    goto :goto_0

    .line 50208
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->i:LX/0Oj;

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->j:LX/0Oi;

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, LX/0Oj;->a(LX/0Oi;I)V

    .line 50209
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->j:LX/0Oi;

    invoke-virtual {v0, v4}, LX/0Oi;->b(I)V

    .line 50210
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->j:LX/0Oi;

    invoke-virtual {v0}, LX/0Oi;->b()Z

    move-result v2

    .line 50211
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->j:LX/0Oi;

    invoke-virtual {v0, v4}, LX/0Oi;->b(I)V

    .line 50212
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->j:LX/0Oi;

    const/16 v3, 0xd

    invoke-virtual {v0, v3}, LX/0Oi;->c(I)I

    move-result v0

    .line 50213
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->j:LX/0Oi;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/0Oi;->b(I)V

    .line 50214
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->j:LX/0Oi;

    invoke-virtual {v3}, LX/0Oi;->b()Z

    move-result v3

    .line 50215
    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->j:LX/0Oi;

    invoke-virtual {v4}, LX/0Oi;->b()Z

    move-result v4

    .line 50216
    if-eqz v3, :cond_2

    .line 50217
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->i:LX/0Oj;

    invoke-virtual {v3}, LX/0Oj;->f()I

    move-result v3

    .line 50218
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->i:LX/0Oj;

    invoke-virtual {v5, v3}, LX/0Oj;->c(I)V

    .line 50219
    :cond_2
    if-eqz v4, :cond_3

    .line 50220
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->a:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0NL;

    .line 50221
    if-eqz v0, :cond_3

    .line 50222
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->i:LX/0Oj;

    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->k:LX/0LU;

    invoke-virtual {v0, v3, v2, v4}, LX/0NL;->a(LX/0Oj;ZLX/0LU;)V

    :cond_3
    move v0, v1

    .line 50223
    goto :goto_0
.end method

.method public final a(LX/0LU;)V
    .locals 1

    .prologue
    .line 50197
    iput-object p1, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->k:LX/0LU;

    .line 50198
    sget-object v0, LX/0M8;->f:LX/0M8;

    invoke-interface {p1, v0}, LX/0LU;->a(LX/0M8;)V

    .line 50199
    return-void
.end method

.method public final a(LX/0MA;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 50189
    new-array v3, v1, [B

    move v2, v0

    .line 50190
    :goto_0
    const/4 v4, 0x5

    if-ge v2, v4, :cond_1

    .line 50191
    invoke-interface {p1, v3, v0, v1}, LX/0MA;->c([BII)V

    .line 50192
    aget-byte v4, v3, v0

    and-int/lit16 v4, v4, 0xff

    const/16 v5, 0x47

    if-eq v4, v5, :cond_0

    .line 50193
    :goto_1
    return v0

    .line 50194
    :cond_0
    const/16 v4, 0xbb

    invoke-interface {p1, v4}, LX/0MA;->c(I)V

    .line 50195
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 50196
    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 50184
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->g:LX/0NJ;

    invoke-virtual {v0}, LX/0NJ;->a()V

    .line 50185
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 50186
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/TsExtractor;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0NL;

    invoke-virtual {v0}, LX/0NL;->a()V

    .line 50187
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 50188
    :cond_0
    return-void
.end method
