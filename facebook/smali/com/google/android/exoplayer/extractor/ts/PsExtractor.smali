.class public final Lcom/google/android/exoplayer/extractor/ts/PsExtractor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ME;


# instance fields
.field private final a:LX/0NJ;

.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0NI;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Oj;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:LX/0LU;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 49936
    new-instance v0, LX/0NJ;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, LX/0NJ;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;-><init>(LX/0NJ;)V

    .line 49937
    return-void
.end method

.method private constructor <init>(LX/0NJ;)V
    .locals 2

    .prologue
    .line 49876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49877
    iput-object p1, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->a:LX/0NJ;

    .line 49878
    new-instance v0, LX/0Oj;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    .line 49879
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->b:Landroid/util/SparseArray;

    .line 49880
    return-void
.end method


# virtual methods
.method public final a(LX/0MA;LX/0MM;)I
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, -0x1

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 49881
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    const/4 v3, 0x4

    invoke-interface {p1, v2, v1, v3, v6}, LX/0MA;->b([BIIZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 49882
    :cond_0
    :goto_0
    return v0

    .line 49883
    :cond_1
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    invoke-virtual {v2, v1}, LX/0Oj;->b(I)V

    .line 49884
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    invoke-virtual {v2}, LX/0Oj;->m()I

    move-result v2

    .line 49885
    const/16 v3, 0x1b9

    if-eq v2, v3, :cond_0

    .line 49886
    const/16 v0, 0x1ba

    if-ne v2, v0, :cond_2

    .line 49887
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    const/16 v2, 0xa

    invoke-interface {p1, v0, v1, v2}, LX/0MA;->c([BII)V

    .line 49888
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    invoke-virtual {v0, v1}, LX/0Oj;->b(I)V

    .line 49889
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    const/16 v2, 0x9

    invoke-virtual {v0, v2}, LX/0Oj;->c(I)V

    .line 49890
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->f()I

    move-result v0

    and-int/lit8 v0, v0, 0x7

    .line 49891
    add-int/lit8 v0, v0, 0xe

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    move v0, v1

    .line 49892
    goto :goto_0

    .line 49893
    :cond_2
    const/16 v0, 0x1bb

    if-ne v2, v0, :cond_3

    .line 49894
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    invoke-interface {p1, v0, v1, v7}, LX/0MA;->c([BII)V

    .line 49895
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    invoke-virtual {v0, v1}, LX/0Oj;->b(I)V

    .line 49896
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->g()I

    move-result v0

    .line 49897
    add-int/lit8 v0, v0, 0x6

    invoke-interface {p1, v0}, LX/0MA;->b(I)V

    move v0, v1

    .line 49898
    goto :goto_0

    .line 49899
    :cond_3
    and-int/lit16 v0, v2, -0x100

    shr-int/lit8 v0, v0, 0x8

    if-eq v0, v6, :cond_4

    .line 49900
    invoke-interface {p1, v6}, LX/0MA;->b(I)V

    move v0, v1

    .line 49901
    goto :goto_0

    .line 49902
    :cond_4
    and-int/lit16 v3, v2, 0xff

    .line 49903
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0NI;

    .line 49904
    iget-boolean v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->d:Z

    if-nez v2, :cond_9

    .line 49905
    if-nez v0, :cond_6

    .line 49906
    const/4 v2, 0x0

    .line 49907
    iget-boolean v4, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->e:Z

    if-nez v4, :cond_a

    const/16 v4, 0xbd

    if-ne v3, v4, :cond_a

    .line 49908
    new-instance v2, LX/0N5;

    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->g:LX/0LU;

    invoke-interface {v4, v3}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v4

    invoke-direct {v2, v4, v1}, LX/0N5;-><init>(LX/0LS;Z)V

    .line 49909
    iput-boolean v6, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->e:Z

    .line 49910
    :cond_5
    :goto_1
    if-eqz v2, :cond_6

    .line 49911
    new-instance v0, LX/0NI;

    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->a:LX/0NJ;

    invoke-direct {v0, v2, v4}, LX/0NI;-><init>(LX/0N4;LX/0NJ;)V

    .line 49912
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 49913
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->e:Z

    if-eqz v2, :cond_7

    iget-boolean v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->f:Z

    if-nez v2, :cond_8

    :cond_7
    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v2

    const-wide/32 v4, 0x100000

    cmp-long v2, v2, v4

    if-lez v2, :cond_9

    .line 49914
    :cond_8
    iput-boolean v6, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->d:Z

    .line 49915
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->g:LX/0LU;

    invoke-interface {v2}, LX/0LU;->a()V

    .line 49916
    :cond_9
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    invoke-interface {p1, v2, v1, v7}, LX/0MA;->c([BII)V

    .line 49917
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    invoke-virtual {v2, v1}, LX/0Oj;->b(I)V

    .line 49918
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    invoke-virtual {v2}, LX/0Oj;->g()I

    move-result v2

    .line 49919
    add-int/lit8 v2, v2, 0x6

    .line 49920
    if-nez v0, :cond_c

    .line 49921
    invoke-interface {p1, v2}, LX/0MA;->b(I)V

    :goto_2
    move v0, v1

    .line 49922
    goto/16 :goto_0

    .line 49923
    :cond_a
    iget-boolean v4, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->e:Z

    if-nez v4, :cond_b

    and-int/lit16 v4, v3, 0xe0

    const/16 v5, 0xc0

    if-ne v4, v5, :cond_b

    .line 49924
    new-instance v2, LX/0NG;

    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->g:LX/0LU;

    invoke-interface {v4, v3}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v4

    invoke-direct {v2, v4}, LX/0NG;-><init>(LX/0LS;)V

    .line 49925
    iput-boolean v6, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->e:Z

    goto :goto_1

    .line 49926
    :cond_b
    iget-boolean v4, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->f:Z

    if-nez v4, :cond_5

    and-int/lit16 v4, v3, 0xf0

    const/16 v5, 0xe0

    if-ne v4, v5, :cond_5

    .line 49927
    new-instance v2, LX/0N9;

    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->g:LX/0LU;

    invoke-interface {v4, v3}, LX/0LU;->a_(I)LX/0LS;

    move-result-object v4

    invoke-direct {v2, v4}, LX/0N9;-><init>(LX/0LS;)V

    .line 49928
    iput-boolean v6, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->f:Z

    goto :goto_1

    .line 49929
    :cond_c
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    invoke-virtual {v3}, LX/0Oj;->e()I

    move-result v3

    if-ge v3, v2, :cond_d

    .line 49930
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    new-array v4, v2, [B

    invoke-virtual {v3, v4, v2}, LX/0Oj;->a([BI)V

    .line 49931
    :cond_d
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    iget-object v3, v3, LX/0Oj;->a:[B

    invoke-interface {p1, v3, v1, v2}, LX/0MA;->b([BII)V

    .line 49932
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, LX/0Oj;->b(I)V

    .line 49933
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    invoke-virtual {v3, v2}, LX/0Oj;->a(I)V

    .line 49934
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    invoke-virtual {v0, v2}, LX/0NI;->a(LX/0Oj;)V

    .line 49935
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->c:LX/0Oj;

    invoke-virtual {v2}, LX/0Oj;->e()I

    move-result v2

    invoke-virtual {v0, v2}, LX/0Oj;->a(I)V

    goto :goto_2
.end method

.method public final a(LX/0LU;)V
    .locals 1

    .prologue
    .line 49938
    iput-object p1, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->g:LX/0LU;

    .line 49939
    sget-object v0, LX/0M8;->f:LX/0M8;

    invoke-interface {p1, v0}, LX/0LU;->a(LX/0M8;)V

    .line 49940
    return-void
.end method

.method public final a(LX/0MA;)Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 49856
    const/16 v2, 0xe

    new-array v2, v2, [B

    .line 49857
    const/16 v3, 0xe

    invoke-interface {p1, v2, v0, v3}, LX/0MA;->c([BII)V

    .line 49858
    const/16 v3, 0x1ba

    aget-byte v4, v2, v0

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x18

    aget-byte v5, v2, v1

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    aget-byte v5, v2, v8

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    aget-byte v5, v2, v6

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v4, v5

    if-eq v3, v4, :cond_1

    .line 49859
    :cond_0
    :goto_0
    return v0

    .line 49860
    :cond_1
    aget-byte v3, v2, v7

    and-int/lit16 v3, v3, 0xc4

    const/16 v4, 0x44

    if-ne v3, v4, :cond_0

    .line 49861
    const/4 v3, 0x6

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v7, :cond_0

    .line 49862
    const/16 v3, 0x8

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v7, :cond_0

    .line 49863
    const/16 v3, 0x9

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_0

    .line 49864
    const/16 v3, 0xc

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x3

    if-ne v3, v6, :cond_0

    .line 49865
    const/16 v3, 0xd

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x7

    .line 49866
    invoke-interface {p1, v3}, LX/0MA;->c(I)V

    .line 49867
    invoke-interface {p1, v2, v0, v6}, LX/0MA;->c([BII)V

    .line 49868
    aget-byte v3, v2, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    aget-byte v4, v2, v1

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v3, v4

    aget-byte v2, v2, v8

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v2, v3

    if-ne v1, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 49869
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->a:LX/0NJ;

    invoke-virtual {v0}, LX/0NJ;->a()V

    .line 49870
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 49871
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/ts/PsExtractor;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0NI;

    .line 49872
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/0NI;->f:Z

    .line 49873
    iget-object v2, v0, LX/0NI;->a:LX/0N4;

    invoke-virtual {v2}, LX/0N4;->a()V

    .line 49874
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 49875
    :cond_0
    return-void
.end method
