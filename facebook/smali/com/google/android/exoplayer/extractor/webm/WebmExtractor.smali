.class public final Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ME;


# static fields
.field private static final a:[B

.field private static final b:[B

.field public static final c:Ljava/util/UUID;


# instance fields
.field private A:J

.field private B:LX/0Oa;

.field private C:LX/0Oa;

.field private D:Z

.field private E:I

.field private F:J

.field private G:J

.field private H:I

.field private I:I

.field private J:[I

.field private K:I

.field private L:I

.field private M:I

.field private N:I

.field private O:Z

.field private P:I

.field private Q:I

.field private R:Z

.field private S:Z

.field private T:LX/0LU;

.field private final d:LX/0NT;

.field private final e:LX/0NX;

.field private final f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0NZ;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Oj;

.field private final h:LX/0Oj;

.field private final i:LX/0Oj;

.field private final j:LX/0Oj;

.field private final k:LX/0Oj;

.field private final l:LX/0Oj;

.field private final m:LX/0Oj;

.field private n:J

.field private o:J

.field private p:J

.field private q:J

.field private r:J

.field private s:LX/0NZ;

.field private t:Z

.field private u:Z

.field private v:I

.field private w:J

.field private x:Z

.field private y:J

.field private z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 51006
    const/16 v0, 0x20

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a:[B

    .line 51007
    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->b:[B

    .line 51008
    new-instance v0, Ljava/util/UUID;

    const-wide v2, 0x100000000001000L

    const-wide v4, -0x7fffff55ffc7648fL    # -3.607411173533E-312

    invoke-direct {v0, v2, v3, v4, v5}, Ljava/util/UUID;-><init>(JJ)V

    sput-object v0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->c:Ljava/util/UUID;

    return-void

    .line 51009
    :array_0
    .array-data 1
        0x31t
        0xat
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
        0x30t
        0x30t
        0x30t
        0x20t
        0x2dt
        0x2dt
        0x3et
        0x20t
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
        0x30t
        0x30t
        0x30t
        0xat
    .end array-data

    .line 51010
    :array_1
    .array-data 1
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51011
    new-instance v0, LX/0NU;

    invoke-direct {v0}, LX/0NU;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;-><init>(LX/0NT;)V

    .line 51012
    return-void
.end method

.method private constructor <init>(LX/0NT;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const-wide/16 v0, -0x1

    .line 51013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51014
    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->n:J

    .line 51015
    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->o:J

    .line 51016
    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->p:J

    .line 51017
    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->q:J

    .line 51018
    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->r:J

    .line 51019
    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->y:J

    .line 51020
    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->z:J

    .line 51021
    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->A:J

    .line 51022
    iput-object p1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->d:LX/0NT;

    .line 51023
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->d:LX/0NT;

    new-instance v1, LX/0NY;

    invoke-direct {v1, p0}, LX/0NY;-><init>(Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;)V

    invoke-interface {v0, v1}, LX/0NT;->a(LX/0NV;)V

    .line 51024
    new-instance v0, LX/0NX;

    invoke-direct {v0}, LX/0NX;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->e:LX/0NX;

    .line 51025
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->f:Landroid/util/SparseArray;

    .line 51026
    new-instance v0, LX/0Oj;

    invoke-direct {v0, v3}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    .line 51027
    new-instance v0, LX/0Oj;

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-direct {v0, v1}, LX/0Oj;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->j:LX/0Oj;

    .line 51028
    new-instance v0, LX/0Oj;

    invoke-direct {v0, v3}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->k:LX/0Oj;

    .line 51029
    new-instance v0, LX/0Oj;

    sget-object v1, LX/0Oh;->a:[B

    invoke-direct {v0, v1}, LX/0Oj;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->g:LX/0Oj;

    .line 51030
    new-instance v0, LX/0Oj;

    invoke-direct {v0, v3}, LX/0Oj;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->h:LX/0Oj;

    .line 51031
    new-instance v0, LX/0Oj;

    invoke-direct {v0}, LX/0Oj;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->l:LX/0Oj;

    .line 51032
    new-instance v0, LX/0Oj;

    invoke-direct {v0}, LX/0Oj;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->m:LX/0Oj;

    .line 51033
    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 51034
    sparse-switch p0, :sswitch_data_0

    .line 51035
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 51036
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 51037
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 51038
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 51039
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 51040
    :sswitch_4
    const/4 v0, 0x5

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x83 -> :sswitch_1
        0x86 -> :sswitch_2
        0x9b -> :sswitch_1
        0x9f -> :sswitch_1
        0xa0 -> :sswitch_0
        0xa1 -> :sswitch_3
        0xa3 -> :sswitch_3
        0xae -> :sswitch_0
        0xb0 -> :sswitch_1
        0xb3 -> :sswitch_1
        0xb5 -> :sswitch_4
        0xb7 -> :sswitch_0
        0xba -> :sswitch_1
        0xbb -> :sswitch_0
        0xd7 -> :sswitch_1
        0xe0 -> :sswitch_0
        0xe1 -> :sswitch_0
        0xe7 -> :sswitch_1
        0xf1 -> :sswitch_1
        0xfb -> :sswitch_1
        0x4254 -> :sswitch_1
        0x4255 -> :sswitch_3
        0x4282 -> :sswitch_2
        0x4285 -> :sswitch_1
        0x42f7 -> :sswitch_1
        0x4489 -> :sswitch_4
        0x47e1 -> :sswitch_1
        0x47e2 -> :sswitch_3
        0x47e7 -> :sswitch_0
        0x47e8 -> :sswitch_1
        0x4dbb -> :sswitch_0
        0x5031 -> :sswitch_1
        0x5032 -> :sswitch_1
        0x5034 -> :sswitch_0
        0x5035 -> :sswitch_0
        0x53ab -> :sswitch_3
        0x53ac -> :sswitch_1
        0x54b0 -> :sswitch_1
        0x54b2 -> :sswitch_1
        0x54ba -> :sswitch_1
        0x56aa -> :sswitch_1
        0x56bb -> :sswitch_1
        0x6240 -> :sswitch_0
        0x6264 -> :sswitch_1
        0x63a2 -> :sswitch_3
        0x6d80 -> :sswitch_0
        0x22b59c -> :sswitch_2
        0x23e383 -> :sswitch_1
        0x2ad7b1 -> :sswitch_1
        0x114d9b74 -> :sswitch_0
        0x1549a966 -> :sswitch_0
        0x1654ae6b -> :sswitch_0
        0x18538067 -> :sswitch_0
        0x1a45dfa3 -> :sswitch_0
        0x1c53bb6b -> :sswitch_0
        0x1f43b675 -> :sswitch_0
    .end sparse-switch
.end method

.method private a(LX/0MA;LX/0LS;I)I
    .locals 2

    .prologue
    .line 51041
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->l:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->b()I

    move-result v0

    .line 51042
    if-lez v0, :cond_0

    .line 51043
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 51044
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->l:LX/0Oj;

    invoke-interface {p2, v1, v0}, LX/0LS;->a(LX/0Oj;I)V

    .line 51045
    :goto_0
    iget v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->N:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->N:I

    .line 51046
    iget v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->Q:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->Q:I

    .line 51047
    return v0

    .line 51048
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p2, p1, p3, v0}, LX/0LS;->a(LX/0MA;IZ)I

    move-result v0

    goto :goto_0
.end method

.method private a(J)J
    .locals 7

    .prologue
    .line 51049
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->p:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 51050
    new-instance v0, LX/0L6;

    const-string v1, "Can\'t scale timecode prior to timecodeScale being set."

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51051
    :cond_0
    iget-wide v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->p:J

    const-wide/16 v4, 0x3e8

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, LX/08x;->a(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(LX/0MA;I)V
    .locals 4

    .prologue
    .line 51052
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    .line 51053
    iget v1, v0, LX/0Oj;->c:I

    move v0, v1

    .line 51054
    if-lt v0, p2, :cond_0

    .line 51055
    :goto_0
    return-void

    .line 51056
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->e()I

    move-result v0

    if-ge v0, p2, :cond_1

    .line 51057
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    .line 51058
    iget v3, v2, LX/0Oj;->c:I

    move v2, v3

    .line 51059
    invoke-virtual {v0, v1, v2}, LX/0Oj;->a([BI)V

    .line 51060
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    .line 51061
    iget v2, v1, LX/0Oj;->c:I

    move v1, v2

    .line 51062
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    .line 51063
    iget v3, v2, LX/0Oj;->c:I

    move v2, v3

    .line 51064
    sub-int v2, p2, v2

    invoke-interface {p1, v0, v1, v2}, LX/0MA;->b([BII)V

    .line 51065
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    invoke-virtual {v0, p2}, LX/0Oj;->a(I)V

    goto :goto_0
.end method

.method private a(LX/0MA;LX/0NZ;I)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 51066
    const-string v0, "S_TEXT/UTF8"

    iget-object v1, p2, LX/0NZ;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 51067
    sget-object v0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a:[B

    array-length v0, v0

    add-int/2addr v0, p3

    .line 51068
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->m:LX/0Oj;

    invoke-virtual {v1}, LX/0Oj;->e()I

    move-result v1

    if-ge v1, v0, :cond_0

    .line 51069
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->m:LX/0Oj;

    sget-object v2, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a:[B

    add-int v3, v0, p3

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    iput-object v2, v1, LX/0Oj;->a:[B

    .line 51070
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->m:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    sget-object v2, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a:[B

    array-length v2, v2

    invoke-interface {p1, v1, v2, p3}, LX/0MA;->b([BII)V

    .line 51071
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->m:LX/0Oj;

    invoke-virtual {v1, v7}, LX/0Oj;->b(I)V

    .line 51072
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->m:LX/0Oj;

    invoke-virtual {v1, v0}, LX/0Oj;->a(I)V

    .line 51073
    :cond_1
    :goto_0
    return-void

    .line 51074
    :cond_2
    iget-object v0, p2, LX/0NZ;->s:LX/0LS;

    .line 51075
    iget-boolean v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->O:Z

    if-nez v1, :cond_5

    .line 51076
    iget-boolean v1, p2, LX/0NZ;->e:Z

    if-eqz v1, :cond_7

    .line 51077
    iget v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->M:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->M:I

    .line 51078
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    invoke-interface {p1, v1, v7, v4}, LX/0MA;->b([BII)V

    .line 51079
    iget v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->N:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->N:I

    .line 51080
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    aget-byte v1, v1, v7

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_3

    .line 51081
    new-instance v0, LX/0L6;

    const-string v1, "Extension bit is set in signal byte"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51082
    :cond_3
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    aget-byte v1, v1, v7

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_4

    .line 51083
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v1, v1, LX/0Oj;->a:[B

    const/16 v2, 0x8

    aput-byte v2, v1, v7

    .line 51084
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    invoke-virtual {v1, v7}, LX/0Oj;->b(I)V

    .line 51085
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    invoke-interface {v0, v1, v4}, LX/0LS;->a(LX/0Oj;I)V

    .line 51086
    iget v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->Q:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->Q:I

    .line 51087
    iget v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->M:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->M:I

    .line 51088
    :cond_4
    :goto_1
    iput-boolean v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->O:Z

    .line 51089
    :cond_5
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->l:LX/0Oj;

    .line 51090
    iget v2, v1, LX/0Oj;->c:I

    move v1, v2

    .line 51091
    add-int/2addr v1, p3

    .line 51092
    const-string v2, "V_MPEG4/ISO/AVC"

    iget-object v3, p2, LX/0NZ;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "V_MPEGH/ISO/HEVC"

    iget-object v3, p2, LX/0NZ;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 51093
    :cond_6
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->h:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    .line 51094
    aput-byte v7, v2, v7

    .line 51095
    aput-byte v7, v2, v4

    .line 51096
    const/4 v3, 0x2

    aput-byte v7, v2, v3

    .line 51097
    iget v3, p2, LX/0NZ;->t:I

    .line 51098
    iget v4, p2, LX/0NZ;->t:I

    rsub-int/lit8 v4, v4, 0x4

    .line 51099
    :goto_2
    iget v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->N:I

    if-ge v5, v1, :cond_a

    .line 51100
    iget v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->P:I

    if-nez v5, :cond_8

    .line 51101
    invoke-direct {p0, p1, v2, v4, v3}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0MA;[BII)V

    .line 51102
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->h:LX/0Oj;

    invoke-virtual {v5, v7}, LX/0Oj;->b(I)V

    .line 51103
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->h:LX/0Oj;

    invoke-virtual {v5}, LX/0Oj;->s()I

    move-result v5

    iput v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->P:I

    .line 51104
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->g:LX/0Oj;

    invoke-virtual {v5, v7}, LX/0Oj;->b(I)V

    .line 51105
    iget-object v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->g:LX/0Oj;

    invoke-interface {v0, v5, v8}, LX/0LS;->a(LX/0Oj;I)V

    .line 51106
    iget v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->Q:I

    add-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->Q:I

    goto :goto_2

    .line 51107
    :cond_7
    iget-object v1, p2, LX/0NZ;->f:[B

    if-eqz v1, :cond_4

    .line 51108
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->l:LX/0Oj;

    iget-object v2, p2, LX/0NZ;->f:[B

    iget-object v3, p2, LX/0NZ;->f:[B

    array-length v3, v3

    invoke-virtual {v1, v2, v3}, LX/0Oj;->a([BI)V

    goto :goto_1

    .line 51109
    :cond_8
    iget v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->P:I

    iget v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->P:I

    invoke-direct {p0, p1, v0, v6}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0MA;LX/0LS;I)I

    move-result v6

    sub-int/2addr v5, v6

    iput v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->P:I

    goto :goto_2

    .line 51110
    :cond_9
    :goto_3
    iget v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->N:I

    if-ge v2, v1, :cond_a

    .line 51111
    iget v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->N:I

    sub-int v2, v1, v2

    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0MA;LX/0LS;I)I

    goto :goto_3

    .line 51112
    :cond_a
    const-string v1, "A_VORBIS"

    iget-object v2, p2, LX/0NZ;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 51113
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->j:LX/0Oj;

    invoke-virtual {v1, v7}, LX/0Oj;->b(I)V

    .line 51114
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->j:LX/0Oj;

    invoke-interface {v0, v1, v8}, LX/0LS;->a(LX/0Oj;I)V

    .line 51115
    iget v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->Q:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->Q:I

    goto/16 :goto_0
.end method

.method private a(LX/0MA;[BII)V
    .locals 3

    .prologue
    .line 51116
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->l:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->b()I

    move-result v0

    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 51117
    add-int v1, p3, v0

    sub-int v2, p4, v0

    invoke-interface {p1, p2, v1, v2}, LX/0MA;->b([BII)V

    .line 51118
    if-lez v0, :cond_0

    .line 51119
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->l:LX/0Oj;

    invoke-virtual {v1, p2, p3, v0}, LX/0Oj;->a([BII)V

    .line 51120
    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->N:I

    add-int/2addr v0, p4

    iput v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->N:I

    .line 51121
    return-void
.end method

.method private a(LX/0NZ;)V
    .locals 4

    .prologue
    .line 51122
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->m:LX/0Oj;

    iget-object v0, v0, LX/0Oj;->a:[B

    iget-wide v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->G:J

    invoke-static {v0, v2, v3}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a([BJ)V

    .line 51123
    iget-object v0, p1, LX/0NZ;->s:LX/0LS;

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->m:LX/0Oj;

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->m:LX/0Oj;

    .line 51124
    iget v3, v2, LX/0Oj;->c:I

    move v2, v3

    .line 51125
    invoke-interface {v0, v1, v2}, LX/0LS;->a(LX/0Oj;I)V

    .line 51126
    iget v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->Q:I

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->m:LX/0Oj;

    .line 51127
    iget v2, v1, LX/0Oj;->c:I

    move v1, v2

    .line 51128
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->Q:I

    .line 51129
    return-void
.end method

.method private a(LX/0NZ;J)V
    .locals 8

    .prologue
    .line 51130
    const-string v0, "S_TEXT/UTF8"

    iget-object v1, p1, LX/0NZ;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51131
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0NZ;)V

    .line 51132
    :cond_0
    iget-object v1, p1, LX/0NZ;->s:LX/0LS;

    iget v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->M:I

    iget v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->Q:I

    const/4 v6, 0x0

    iget-object v7, p1, LX/0NZ;->g:[B

    move-wide v2, p2

    invoke-interface/range {v1 .. v7}, LX/0LS;->a(JIII[B)V

    .line 51133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->R:Z

    .line 51134
    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->c()V

    .line 51135
    return-void
.end method

.method private static a([BJ)V
    .locals 9

    .prologue
    const-wide v4, 0xd693a400L

    const/4 v8, 0x0

    .line 51136
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 51137
    sget-object v0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->b:[B

    .line 51138
    :goto_0
    const/16 v1, 0x13

    const/16 v2, 0xc

    invoke-static {v0, v8, p0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 51139
    return-void

    .line 51140
    :cond_0
    div-long v0, p1, v4

    long-to-int v0, v0

    .line 51141
    int-to-long v2, v0

    mul-long/2addr v2, v4

    sub-long v2, p1, v2

    .line 51142
    const-wide/32 v4, 0x3938700

    div-long v4, v2, v4

    long-to-int v1, v4

    .line 51143
    const v4, 0x3938700

    mul-int/2addr v4, v1

    int-to-long v4, v4

    sub-long/2addr v2, v4

    .line 51144
    const-wide/32 v4, 0xf4240

    div-long v4, v2, v4

    long-to-int v4, v4

    .line 51145
    const v5, 0xf4240

    mul-int/2addr v5, v4

    int-to-long v6, v5

    sub-long/2addr v2, v6

    .line 51146
    const-wide/16 v6, 0x3e8

    div-long/2addr v2, v6

    long-to-int v2, v2

    .line 51147
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%02d:%02d:%02d,%03d"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v8

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v3, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/0MM;J)Z
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 50996
    iget-boolean v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->x:Z

    if-eqz v2, :cond_0

    .line 50997
    iput-wide p2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->z:J

    .line 50998
    iget-wide v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->y:J

    iput-wide v2, p1, LX/0MM;->a:J

    .line 50999
    iput-boolean v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->x:Z

    .line 51000
    :goto_0
    return v0

    .line 51001
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->u:Z

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->z:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 51002
    iget-wide v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->z:J

    iput-wide v2, p1, LX/0MM;->a:J

    .line 51003
    iput-wide v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->z:J

    goto :goto_0

    :cond_1
    move v0, v1

    .line 51004
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 51005
    const-string v0, "V_VP8"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_VP9"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG2"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/SP"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/ASP"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/AP"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/AVC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEGH/ISO/HEVC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MS/VFW/FOURCC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_OPUS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_VORBIS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_AAC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_MPEG/L3"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_AC3"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_EAC3"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_TRUEHD"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_DTS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_DTS/EXPRESS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_DTS/LOSSLESS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_FLAC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_MS/ACM"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_PCM/INT/LIT"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "S_TEXT/UTF8"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "S_VOBSUB"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "S_HDMV/PGS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([II)[I
    .locals 1

    .prologue
    .line 50759
    if-nez p0, :cond_1

    .line 50760
    new-array p0, p1, [I

    .line 50761
    :cond_0
    :goto_0
    return-object p0

    .line 50762
    :cond_1
    array-length v0, p0

    if-ge v0, p1, :cond_0

    .line 50763
    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array p0, v0, [I

    goto :goto_0
.end method

.method public static b(I)Z
    .locals 1

    .prologue
    .line 50764
    const v0, 0x1549a966

    if-eq p0, v0, :cond_0

    const v0, 0x1f43b675

    if-eq p0, v0, :cond_0

    const v0, 0x1c53bb6b

    if-eq p0, v0, :cond_0

    const v0, 0x1654ae6b

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50765
    iput v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->N:I

    .line 50766
    iput v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->Q:I

    .line 50767
    iput v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->P:I

    .line 50768
    iput-boolean v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->O:Z

    .line 50769
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->l:LX/0Oj;

    invoke-virtual {v0}, LX/0Oj;->a()V

    .line 50770
    return-void
.end method

.method private d()LX/0M8;
    .locals 13

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    const/4 v12, 0x0

    .line 50771
    iget-wide v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->n:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->r:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->B:LX/0Oa;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->B:LX/0Oa;

    .line 50772
    iget v2, v1, LX/0Oa;->a:I

    move v1, v2

    .line 50773
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->C:LX/0Oa;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->C:LX/0Oa;

    .line 50774
    iget v2, v1, LX/0Oa;->a:I

    move v1, v2

    .line 50775
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->B:LX/0Oa;

    .line 50776
    iget v3, v2, LX/0Oa;->a:I

    move v2, v3

    .line 50777
    if-eq v1, v2, :cond_1

    .line 50778
    :cond_0
    iput-object v12, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->B:LX/0Oa;

    .line 50779
    iput-object v12, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->C:LX/0Oa;

    .line 50780
    sget-object v0, LX/0M8;->f:LX/0M8;

    .line 50781
    :goto_0
    return-object v0

    .line 50782
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->B:LX/0Oa;

    .line 50783
    iget v2, v1, LX/0Oa;->a:I

    move v2, v2

    .line 50784
    new-array v3, v2, [I

    .line 50785
    new-array v4, v2, [J

    .line 50786
    new-array v5, v2, [J

    .line 50787
    new-array v6, v2, [J

    move v1, v0

    .line 50788
    :goto_1
    if-ge v1, v2, :cond_2

    .line 50789
    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->B:LX/0Oa;

    invoke-virtual {v7, v1}, LX/0Oa;->a(I)J

    move-result-wide v8

    aput-wide v8, v6, v1

    .line 50790
    iget-wide v8, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->n:J

    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->C:LX/0Oa;

    invoke-virtual {v7, v1}, LX/0Oa;->a(I)J

    move-result-wide v10

    add-long/2addr v8, v10

    aput-wide v8, v4, v1

    .line 50791
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 50792
    :cond_2
    :goto_2
    add-int/lit8 v1, v2, -0x1

    if-ge v0, v1, :cond_3

    .line 50793
    add-int/lit8 v1, v0, 0x1

    aget-wide v8, v4, v1

    aget-wide v10, v4, v0

    sub-long/2addr v8, v10

    long-to-int v1, v8

    aput v1, v3, v0

    .line 50794
    add-int/lit8 v1, v0, 0x1

    aget-wide v8, v6, v1

    aget-wide v10, v6, v0

    sub-long/2addr v8, v10

    aput-wide v8, v5, v0

    .line 50795
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 50796
    :cond_3
    add-int/lit8 v0, v2, -0x1

    iget-wide v8, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->n:J

    iget-wide v10, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->o:J

    add-long/2addr v8, v10

    add-int/lit8 v1, v2, -0x1

    aget-wide v10, v4, v1

    sub-long/2addr v8, v10

    long-to-int v1, v8

    aput v1, v3, v0

    .line 50797
    add-int/lit8 v0, v2, -0x1

    iget-wide v8, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->r:J

    add-int/lit8 v1, v2, -0x1

    aget-wide v10, v6, v1

    sub-long/2addr v8, v10

    aput-wide v8, v5, v0

    .line 50798
    iput-object v12, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->B:LX/0Oa;

    .line 50799
    iput-object v12, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->C:LX/0Oa;

    .line 50800
    new-instance v0, LX/0M9;

    invoke-direct {v0, v3, v4, v5, v6}, LX/0M9;-><init>([I[J[J[J)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0MA;LX/0MM;)I
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 50801
    iput-boolean v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->R:Z

    move v2, v0

    .line 50802
    :cond_0
    if-eqz v2, :cond_1

    iget-boolean v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->R:Z

    if-nez v3, :cond_1

    .line 50803
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->d:LX/0NT;

    invoke-interface {v2, p1}, LX/0NT;->a(LX/0MA;)Z

    move-result v2

    .line 50804
    if-eqz v2, :cond_0

    invoke-interface {p1}, LX/0MA;->c()J

    move-result-wide v4

    invoke-direct {p0, p2, v4, v5}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0MM;J)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 50805
    :goto_0
    return v0

    :cond_1
    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(ID)V
    .locals 2

    .prologue
    .line 50992
    sparse-switch p1, :sswitch_data_0

    .line 50993
    :goto_0
    return-void

    .line 50994
    :sswitch_0
    double-to-long v0, p2

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->q:J

    goto :goto_0

    .line 50995
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    double-to-int v1, p2

    iput v1, v0, LX/0NZ;->p:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xb5 -> :sswitch_1
        0x4489 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(IILX/0MA;)V
    .locals 15

    .prologue
    .line 50806
    sparse-switch p1, :sswitch_data_0

    .line 50807
    new-instance v2, LX/0L6;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v2

    .line 50808
    :sswitch_0
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->k:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([BB)V

    .line 50809
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->k:LX/0Oj;

    iget-object v2, v2, LX/0Oj;->a:[B

    rsub-int/lit8 v3, p2, 0x4

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, LX/0MA;->b([BII)V

    .line 50810
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->k:LX/0Oj;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Oj;->b(I)V

    .line 50811
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->k:LX/0Oj;

    invoke-virtual {v2}, LX/0Oj;->k()J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->v:I

    .line 50812
    :goto_0
    return-void

    .line 50813
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, LX/0NZ;->h:[B

    .line 50814
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget-object v2, v2, LX/0NZ;->h:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, LX/0MA;->b([BII)V

    goto :goto_0

    .line 50815
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, LX/0NZ;->f:[B

    .line 50816
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget-object v2, v2, LX/0NZ;->f:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, LX/0MA;->b([BII)V

    goto :goto_0

    .line 50817
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, LX/0NZ;->g:[B

    .line 50818
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget-object v2, v2, LX/0NZ;->g:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, LX/0MA;->b([BII)V

    goto :goto_0

    .line 50819
    :sswitch_4
    iget v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->E:I

    if-nez v2, :cond_0

    .line 50820
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->e:LX/0NX;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/16 v5, 0x8

    move-object/from16 v0, p3

    invoke-virtual {v2, v0, v3, v4, v5}, LX/0NX;->a(LX/0MA;ZZI)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->K:I

    .line 50821
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->e:LX/0NX;

    invoke-virtual {v2}, LX/0NX;->b()I

    move-result v2

    iput v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->L:I

    .line 50822
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->G:J

    .line 50823
    const/4 v2, 0x1

    iput v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->E:I

    .line 50824
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    invoke-virtual {v2}, LX/0Oj;->a()V

    .line 50825
    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->f:Landroid/util/SparseArray;

    iget v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->K:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0NZ;

    .line 50826
    if-nez v2, :cond_1

    .line 50827
    iget v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->L:I

    sub-int v2, p2, v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, LX/0MA;->b(I)V

    .line 50828
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->E:I

    goto :goto_0

    .line 50829
    :cond_1
    iget v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->E:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 50830
    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v3}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0MA;I)V

    .line 50831
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v3, v3, LX/0Oj;->a:[B

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x6

    shr-int/lit8 v3, v3, 0x1

    .line 50832
    if-nez v3, :cond_4

    .line 50833
    const/4 v3, 0x1

    iput v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->I:I

    .line 50834
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a([II)[I

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    .line 50835
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->L:I

    sub-int v5, p2, v5

    add-int/lit8 v5, v5, -0x3

    aput v5, v3, v4

    .line 50836
    :goto_1
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v3, v3, LX/0Oj;->a:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    shl-int/lit8 v3, v3, 0x8

    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v4, v4, LX/0Oj;->a:[B

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    .line 50837
    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->A:J

    int-to-long v6, v3

    invoke-direct {p0, v6, v7}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(J)J

    move-result-wide v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->F:J

    .line 50838
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v3, v3, LX/0Oj;->a:[B

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_13

    const/4 v3, 0x1

    .line 50839
    :goto_2
    iget v4, v2, LX/0NZ;->c:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_2

    const/16 v4, 0xa3

    move/from16 v0, p1

    if-ne v0, v4, :cond_14

    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v4, v4, LX/0Oj;->a:[B

    const/4 v5, 0x2

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_14

    :cond_2
    const/4 v4, 0x1

    .line 50840
    :goto_3
    if-eqz v4, :cond_15

    const/4 v4, 0x1

    :goto_4
    if-eqz v3, :cond_16

    const/high16 v3, 0x8000000

    :goto_5
    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->M:I

    .line 50841
    const/4 v3, 0x2

    iput v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->E:I

    .line 50842
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->H:I

    .line 50843
    :cond_3
    const/16 v3, 0xa3

    move/from16 v0, p1

    if-ne v0, v3, :cond_18

    .line 50844
    :goto_6
    iget v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->H:I

    iget v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->I:I

    if-ge v3, v4, :cond_17

    .line 50845
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    iget v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->H:I

    aget v3, v3, v4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0MA;LX/0NZ;I)V

    .line 50846
    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->F:J

    iget v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->H:I

    iget v6, v2, LX/0NZ;->d:I

    mul-int/2addr v3, v6

    div-int/lit16 v3, v3, 0x3e8

    int-to-long v6, v3

    add-long/2addr v4, v6

    .line 50847
    invoke-direct {p0, v2, v4, v5}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0NZ;J)V

    .line 50848
    iget v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->H:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->H:I

    goto :goto_6

    .line 50849
    :cond_4
    const/16 v4, 0xa3

    move/from16 v0, p1

    if-eq v0, v4, :cond_5

    .line 50850
    new-instance v2, LX/0L6;

    const-string v3, "Lacing only supported in SimpleBlocks."

    invoke-direct {v2, v3}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v2

    .line 50851
    :cond_5
    const/4 v4, 0x4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0MA;I)V

    .line 50852
    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v4, v4, LX/0Oj;->a:[B

    const/4 v5, 0x3

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->I:I

    .line 50853
    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    iget v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->I:I

    invoke-static {v4, v5}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a([II)[I

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    .line 50854
    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 50855
    iget v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->L:I

    sub-int v3, p2, v3

    add-int/lit8 v3, v3, -0x4

    iget v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->I:I

    div-int/2addr v3, v4

    .line 50856
    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->I:I

    invoke-static {v4, v5, v6, v3}, Ljava/util/Arrays;->fill([IIII)V

    goto/16 :goto_1

    .line 50857
    :cond_6
    const/4 v4, 0x1

    if-ne v3, v4, :cond_9

    .line 50858
    const/4 v5, 0x0

    .line 50859
    const/4 v4, 0x4

    .line 50860
    const/4 v3, 0x0

    :goto_7
    iget v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->I:I

    add-int/lit8 v6, v6, -0x1

    if-ge v3, v6, :cond_8

    .line 50861
    iget-object v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    const/4 v7, 0x0

    aput v7, v6, v3

    .line 50862
    :cond_7
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0MA;I)V

    .line 50863
    iget-object v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v6, v6, LX/0Oj;->a:[B

    add-int/lit8 v7, v4, -0x1

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    .line 50864
    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    aget v8, v7, v3

    add-int/2addr v8, v6

    aput v8, v7, v3

    .line 50865
    const/16 v7, 0xff

    if-eq v6, v7, :cond_7

    .line 50866
    iget-object v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    aget v6, v6, v3

    add-int/2addr v5, v6

    .line 50867
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 50868
    :cond_8
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    iget v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->I:I

    add-int/lit8 v6, v6, -0x1

    iget v7, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->L:I

    sub-int v7, p2, v7

    sub-int v4, v7, v4

    sub-int/2addr v4, v5

    aput v4, v3, v6

    goto/16 :goto_1

    .line 50869
    :cond_9
    const/4 v4, 0x3

    if-ne v3, v4, :cond_12

    .line 50870
    const/4 v5, 0x0

    .line 50871
    const/4 v4, 0x4

    .line 50872
    const/4 v3, 0x0

    :goto_8
    iget v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->I:I

    add-int/lit8 v6, v6, -0x1

    if-ge v3, v6, :cond_11

    .line 50873
    iget-object v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    const/4 v7, 0x0

    aput v7, v6, v3

    .line 50874
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0MA;I)V

    .line 50875
    iget-object v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v6, v6, LX/0Oj;->a:[B

    add-int/lit8 v7, v4, -0x1

    aget-byte v6, v6, v7

    if-nez v6, :cond_a

    .line 50876
    new-instance v2, LX/0L6;

    const-string v3, "No valid varint length mask found"

    invoke-direct {v2, v3}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v2

    .line 50877
    :cond_a
    const-wide/16 v6, 0x0

    .line 50878
    const/4 v8, 0x0

    move v10, v8

    :goto_9
    const/16 v8, 0x8

    if-ge v10, v8, :cond_c

    .line 50879
    const/4 v8, 0x1

    rsub-int/lit8 v9, v10, 0x7

    shl-int/2addr v8, v9

    .line 50880
    iget-object v9, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v9, v9, LX/0Oj;->a:[B

    add-int/lit8 v11, v4, -0x1

    aget-byte v9, v9, v11

    and-int/2addr v9, v8

    if-eqz v9, :cond_e

    .line 50881
    add-int/lit8 v7, v4, -0x1

    .line 50882
    add-int/2addr v4, v10

    .line 50883
    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0MA;I)V

    .line 50884
    iget-object v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v9, v6, LX/0Oj;->a:[B

    add-int/lit8 v6, v7, 0x1

    aget-byte v7, v9, v7

    and-int/lit16 v7, v7, 0xff

    xor-int/lit8 v8, v8, -0x1

    and-int/2addr v7, v8

    int-to-long v8, v7

    move v14, v6

    move-wide v6, v8

    move v8, v14

    .line 50885
    :goto_a
    if-ge v8, v4, :cond_b

    .line 50886
    const/16 v9, 0x8

    shl-long v12, v6, v9

    .line 50887
    iget-object v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->i:LX/0Oj;

    iget-object v7, v6, LX/0Oj;->a:[B

    add-int/lit8 v6, v8, 0x1

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    int-to-long v8, v7

    or-long/2addr v8, v12

    move v14, v6

    move-wide v6, v8

    move v8, v14

    goto :goto_a

    .line 50888
    :cond_b
    if-lez v3, :cond_c

    .line 50889
    const-wide/16 v8, 0x1

    mul-int/lit8 v10, v10, 0x7

    add-int/lit8 v10, v10, 0x6

    shl-long/2addr v8, v10

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    sub-long/2addr v6, v8

    .line 50890
    :cond_c
    const-wide/32 v8, -0x80000000

    cmp-long v8, v6, v8

    if-ltz v8, :cond_d

    const-wide/32 v8, 0x7fffffff

    cmp-long v8, v6, v8

    if-lez v8, :cond_f

    .line 50891
    :cond_d
    new-instance v2, LX/0L6;

    const-string v3, "EBML lacing sample size out of range."

    invoke-direct {v2, v3}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v2

    .line 50892
    :cond_e
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    goto :goto_9

    .line 50893
    :cond_f
    long-to-int v6, v6

    .line 50894
    iget-object v7, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    if-nez v3, :cond_10

    :goto_b
    aput v6, v7, v3

    .line 50895
    iget-object v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    aget v6, v6, v3

    add-int/2addr v5, v6

    .line 50896
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    .line 50897
    :cond_10
    iget-object v8, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    add-int/lit8 v9, v3, -0x1

    aget v8, v8, v9

    add-int/2addr v6, v8

    goto :goto_b

    .line 50898
    :cond_11
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    iget v6, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->I:I

    add-int/lit8 v6, v6, -0x1

    iget v7, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->L:I

    sub-int v7, p2, v7

    sub-int v4, v7, v4

    sub-int/2addr v4, v5

    aput v4, v3, v6

    goto/16 :goto_1

    .line 50899
    :cond_12
    new-instance v2, LX/0L6;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unexpected lacing value: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v2

    .line 50900
    :cond_13
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 50901
    :cond_14
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 50902
    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_16
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 50903
    :cond_17
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->E:I

    goto/16 :goto_0

    .line 50904
    :cond_18
    iget-object v3, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->J:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0MA;LX/0NZ;I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0xa1 -> :sswitch_4
        0xa3 -> :sswitch_4
        0x4255 -> :sswitch_2
        0x47e2 -> :sswitch_3
        0x53ab -> :sswitch_0
        0x63a2 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(IJ)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const-wide/16 v0, 0x1

    .line 50905
    sparse-switch p1, :sswitch_data_0

    .line 50906
    :cond_0
    :goto_0
    return-void

    .line 50907
    :sswitch_0
    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 50908
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EBMLReadVersion "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50909
    :sswitch_1
    cmp-long v0, p2, v0

    if-ltz v0, :cond_1

    const-wide/16 v0, 0x2

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    .line 50910
    :cond_1
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DocTypeReadVersion "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50911
    :sswitch_2
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->n:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->w:J

    goto :goto_0

    .line 50912
    :sswitch_3
    iput-wide p2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->p:J

    goto :goto_0

    .line 50913
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    long-to-int v1, p2

    iput v1, v0, LX/0NZ;->i:I

    goto :goto_0

    .line 50914
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    long-to-int v1, p2

    iput v1, v0, LX/0NZ;->j:I

    goto :goto_0

    .line 50915
    :sswitch_6
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    long-to-int v1, p2

    iput v1, v0, LX/0NZ;->k:I

    goto :goto_0

    .line 50916
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    long-to-int v1, p2

    iput v1, v0, LX/0NZ;->l:I

    goto :goto_0

    .line 50917
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    long-to-int v1, p2

    iput v1, v0, LX/0NZ;->m:I

    goto :goto_0

    .line 50918
    :sswitch_9
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    long-to-int v1, p2

    iput v1, v0, LX/0NZ;->b:I

    goto :goto_0

    .line 50919
    :sswitch_a
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    long-to-int v1, p2

    iput v1, v0, LX/0NZ;->c:I

    goto :goto_0

    .line 50920
    :sswitch_b
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    long-to-int v1, p2

    iput v1, v0, LX/0NZ;->d:I

    goto :goto_0

    .line 50921
    :sswitch_c
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iput-wide p2, v0, LX/0NZ;->q:J

    goto/16 :goto_0

    .line 50922
    :sswitch_d
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iput-wide p2, v0, LX/0NZ;->r:J

    goto/16 :goto_0

    .line 50923
    :sswitch_e
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    long-to-int v1, p2

    iput v1, v0, LX/0NZ;->n:I

    goto/16 :goto_0

    .line 50924
    :sswitch_f
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    long-to-int v1, p2

    iput v1, v0, LX/0NZ;->o:I

    goto/16 :goto_0

    .line 50925
    :sswitch_10
    iput-boolean v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->S:Z

    goto/16 :goto_0

    .line 50926
    :sswitch_11
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 50927
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentEncodingOrder "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50928
    :sswitch_12
    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 50929
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentEncodingScope "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50930
    :sswitch_13
    const-wide/16 v0, 0x3

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 50931
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentCompAlgo "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50932
    :sswitch_14
    const-wide/16 v0, 0x5

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 50933
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentEncAlgo "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50934
    :sswitch_15
    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 50935
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AESSettingsCipherMode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50936
    :sswitch_16
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->B:LX/0Oa;

    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/0Oa;->a(J)V

    goto/16 :goto_0

    .line 50937
    :sswitch_17
    iget-boolean v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->D:Z

    if-nez v0, :cond_0

    .line 50938
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->C:LX/0Oa;

    invoke-virtual {v0, p2, p3}, LX/0Oa;->a(J)V

    .line 50939
    iput-boolean v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->D:Z

    goto/16 :goto_0

    .line 50940
    :sswitch_18
    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->A:J

    goto/16 :goto_0

    .line 50941
    :sswitch_19
    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->G:J

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x83 -> :sswitch_a
        0x9b -> :sswitch_19
        0x9f -> :sswitch_e
        0xb0 -> :sswitch_4
        0xb3 -> :sswitch_16
        0xba -> :sswitch_5
        0xd7 -> :sswitch_9
        0xe7 -> :sswitch_18
        0xf1 -> :sswitch_17
        0xfb -> :sswitch_10
        0x4254 -> :sswitch_13
        0x4285 -> :sswitch_1
        0x42f7 -> :sswitch_0
        0x47e1 -> :sswitch_14
        0x47e8 -> :sswitch_15
        0x5031 -> :sswitch_11
        0x5032 -> :sswitch_12
        0x53ac -> :sswitch_2
        0x54b0 -> :sswitch_6
        0x54b2 -> :sswitch_8
        0x54ba -> :sswitch_7
        0x56aa -> :sswitch_c
        0x56bb -> :sswitch_d
        0x6264 -> :sswitch_f
        0x23e383 -> :sswitch_b
        0x2ad7b1 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(IJJ)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 50740
    sparse-switch p1, :sswitch_data_0

    .line 50741
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 50742
    :sswitch_1
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->n:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->n:J

    cmp-long v0, v0, p2

    if-eqz v0, :cond_1

    .line 50743
    new-instance v0, LX/0L6;

    const-string v1, "Multiple Segment elements not supported"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50744
    :cond_1
    iput-wide p2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->n:J

    .line 50745
    iput-wide p4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->o:J

    goto :goto_0

    .line 50746
    :sswitch_2
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->v:I

    .line 50747
    iput-wide v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->w:J

    goto :goto_0

    .line 50748
    :sswitch_3
    new-instance v0, LX/0Oa;

    invoke-direct {v0}, LX/0Oa;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->B:LX/0Oa;

    .line 50749
    new-instance v0, LX/0Oa;

    invoke-direct {v0}, LX/0Oa;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->C:LX/0Oa;

    goto :goto_0

    .line 50750
    :sswitch_4
    iput-boolean v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->D:Z

    goto :goto_0

    .line 50751
    :sswitch_5
    iget-boolean v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->u:Z

    if-nez v0, :cond_0

    .line 50752
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->y:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 50753
    iput-boolean v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->x:Z

    goto :goto_0

    .line 50754
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->T:LX/0LU;

    sget-object v1, LX/0M8;->f:LX/0M8;

    invoke-interface {v0, v1}, LX/0LU;->a(LX/0M8;)V

    .line 50755
    iput-boolean v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->u:Z

    goto :goto_0

    .line 50756
    :sswitch_6
    iput-boolean v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->S:Z

    goto :goto_0

    .line 50757
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iput-boolean v2, v0, LX/0NZ;->e:Z

    goto :goto_0

    .line 50758
    :sswitch_8
    new-instance v0, LX/0NZ;

    invoke-direct {v0}, LX/0NZ;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xa0 -> :sswitch_6
        0xae -> :sswitch_8
        0xbb -> :sswitch_4
        0x4dbb -> :sswitch_2
        0x5035 -> :sswitch_7
        0x6240 -> :sswitch_0
        0x18538067 -> :sswitch_1
        0x1c53bb6b -> :sswitch_3
        0x1f43b675 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 50942
    sparse-switch p1, :sswitch_data_0

    .line 50943
    :cond_0
    :goto_0
    return-void

    .line 50944
    :sswitch_0
    const-string v0, "webm"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "matroska"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 50945
    new-instance v0, LX/0L6;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DocType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50946
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iput-object p2, v0, LX/0NZ;->a:Ljava/lang/String;

    goto :goto_0

    .line 50947
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    .line 50948
    iput-object p2, v0, LX/0NZ;->u:Ljava/lang/String;

    .line 50949
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x86 -> :sswitch_1
        0x4282 -> :sswitch_0
        0x22b59c -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(LX/0LU;)V
    .locals 0

    .prologue
    .line 50950
    iput-object p1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->T:LX/0LU;

    .line 50951
    return-void
.end method

.method public final a(LX/0MA;)Z
    .locals 1

    .prologue
    .line 50952
    new-instance v0, LX/0NW;

    invoke-direct {v0}, LX/0NW;-><init>()V

    invoke-virtual {v0, p1}, LX/0NW;->a(LX/0MA;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 50953
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->A:J

    .line 50954
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->E:I

    .line 50955
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->d:LX/0NT;

    invoke-interface {v0}, LX/0NT;->a()V

    .line 50956
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->e:LX/0NX;

    invoke-virtual {v0}, LX/0NX;->a()V

    .line 50957
    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->c()V

    .line 50958
    return-void
.end method

.method public final c(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const-wide/16 v2, -0x1

    .line 50959
    sparse-switch p1, :sswitch_data_0

    .line 50960
    :cond_0
    :goto_0
    return-void

    .line 50961
    :sswitch_0
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->p:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 50962
    const-wide/32 v0, 0xf4240

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->p:J

    .line 50963
    :cond_1
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->q:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 50964
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->q:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->r:J

    goto :goto_0

    .line 50965
    :sswitch_1
    iget v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->v:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->w:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 50966
    :cond_2
    new-instance v0, LX/0L6;

    const-string v1, "Mandatory element SeekID or SeekPosition not found"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50967
    :cond_3
    iget v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->v:I

    const v1, 0x1c53bb6b

    if-ne v0, v1, :cond_0

    .line 50968
    iget-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->w:J

    iput-wide v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->y:J

    goto :goto_0

    .line 50969
    :sswitch_2
    iget-boolean v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->u:Z

    if-nez v0, :cond_0

    .line 50970
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->T:LX/0LU;

    invoke-direct {p0}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->d()LX/0M8;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0LU;->a(LX/0M8;)V

    .line 50971
    iput-boolean v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->u:Z

    goto :goto_0

    .line 50972
    :sswitch_3
    iget v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->E:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 50973
    iget-boolean v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->S:Z

    if-nez v0, :cond_4

    .line 50974
    iget v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->M:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->M:I

    .line 50975
    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->f:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->K:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0NZ;

    iget-wide v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->F:J

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(LX/0NZ;J)V

    .line 50976
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->E:I

    goto :goto_0

    .line 50977
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget-boolean v0, v0, LX/0NZ;->e:Z

    if-eqz v0, :cond_0

    .line 50978
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget-object v0, v0, LX/0NZ;->g:[B

    if-nez v0, :cond_5

    .line 50979
    new-instance v0, LX/0L6;

    const-string v1, "Encrypted Track found but ContentEncKeyID was not found"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50980
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->t:Z

    if-nez v0, :cond_0

    .line 50981
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->T:LX/0LU;

    new-instance v1, LX/0M2;

    new-instance v2, LX/0M1;

    const-string v3, "video/webm"

    iget-object v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget-object v4, v4, LX/0NZ;->g:[B

    invoke-direct {v2, v3, v4}, LX/0M1;-><init>(Ljava/lang/String;[B)V

    invoke-direct {v1, v2}, LX/0M2;-><init>(LX/0M1;)V

    invoke-interface {v0, v1}, LX/0LU;->a(LX/0Lz;)V

    .line 50982
    iput-boolean v5, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->t:Z

    goto/16 :goto_0

    .line 50983
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget-boolean v0, v0, LX/0NZ;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget-object v0, v0, LX/0NZ;->f:[B

    if-eqz v0, :cond_0

    .line 50984
    new-instance v0, LX/0L6;

    const-string v1, "Combining encryption and compression is not supported"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50985
    :sswitch_6
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->f:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget v1, v1, LX/0NZ;->b:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget-object v0, v0, LX/0NZ;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 50986
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->T:LX/0LU;

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget v2, v2, LX/0NZ;->b:I

    iget-wide v4, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->r:J

    invoke-virtual {v0, v1, v2, v4, v5}, LX/0NZ;->a(LX/0LU;IJ)V

    .line 50987
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->f:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    iget v1, v1, LX/0NZ;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 50988
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->s:LX/0NZ;

    goto/16 :goto_0

    .line 50989
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 50990
    new-instance v0, LX/0L6;

    const-string v1, "No valid tracks were found"

    invoke-direct {v0, v1}, LX/0L6;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50991
    :cond_7
    iget-object v0, p0, Lcom/google/android/exoplayer/extractor/webm/WebmExtractor;->T:LX/0LU;

    invoke-interface {v0}, LX/0LU;->a()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xa0 -> :sswitch_3
        0xae -> :sswitch_6
        0x4dbb -> :sswitch_1
        0x6240 -> :sswitch_4
        0x6d80 -> :sswitch_5
        0x1549a966 -> :sswitch_0
        0x1654ae6b -> :sswitch_7
        0x1c53bb6b -> :sswitch_2
    .end sparse-switch
.end method
