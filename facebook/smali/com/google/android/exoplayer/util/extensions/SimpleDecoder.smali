.class public abstract Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;
.super Ljava/lang/Thread;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "LX/08M;",
        "O:",
        "LX/0M6;",
        "E:",
        "Ljava/lang/Exception;",
        ">",
        "Ljava/lang/Thread;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<TI;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<TO;>;"
        }
    .end annotation
.end field

.field public final d:[LX/08M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TI;"
        }
    .end annotation
.end field

.field private final e:[LX/0M6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TO;"
        }
    .end annotation
.end field

.field public f:I

.field private g:I

.field private h:LX/08M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TI;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>([LX/08M;[LX/0M6;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TI;[TO;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 45011
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 45012
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    .line 45013
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->b:Ljava/util/LinkedList;

    .line 45014
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->c:Ljava/util/LinkedList;

    .line 45015
    iput-object p1, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->d:[LX/08M;

    .line 45016
    array-length v0, p1

    iput v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f:I

    move v0, v1

    .line 45017
    :goto_0
    iget v2, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f:I

    if-ge v0, v2, :cond_0

    .line 45018
    iget-object v2, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->d:[LX/08M;

    invoke-virtual {p0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->e()LX/08M;

    move-result-object v3

    aput-object v3, v2, v0

    .line 45019
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45020
    :cond_0
    iput-object p2, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->e:[LX/0M6;

    .line 45021
    array-length v0, p2

    iput v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g:I

    .line 45022
    :goto_1
    iget v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g:I

    if-ge v1, v0, :cond_1

    .line 45023
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->e:[LX/0M6;

    invoke-virtual {p0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f()LX/0M6;

    move-result-object v2

    aput-object v2, v0, v1

    .line 45024
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 45025
    :cond_1
    return-void
.end method

.method private static g(Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V^TE;"
        }
    .end annotation

    .prologue
    .line 45059
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->i:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 45060
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->i:Ljava/lang/Exception;

    throw v0

    .line 45061
    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 45056
    invoke-static {p0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->j(Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45057
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    const v1, -0x51dd89af

    invoke-static {v0, v1}, LX/02L;->b(Ljava/lang/Object;I)V

    .line 45058
    :cond_0
    return-void
.end method

.method private i()Z
    .locals 7

    .prologue
    const/high16 v6, 0x8000000

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 45027
    iget-object v3, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 45028
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->k:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->j(Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45029
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    const v4, 0x1e86cbf8

    invoke-static {v0, v4}, LX/02L;->a(Ljava/lang/Object;I)V

    goto :goto_0

    .line 45030
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 45031
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->k:Z

    if-eqz v0, :cond_1

    .line 45032
    monitor-exit v3

    move v0, v1

    .line 45033
    :goto_1
    return v0

    .line 45034
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/08M;

    .line 45035
    iget-object v4, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->e:[LX/0M6;

    iget v5, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g:I

    aget-object v4, v4, v5

    .line 45036
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->j:Z

    .line 45037
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 45038
    invoke-virtual {v4}, LX/0M5;->a()V

    .line 45039
    invoke-virtual {v0, v2}, LX/0M5;->b(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 45040
    invoke-virtual {v4, v2}, LX/0M5;->a(I)V

    .line 45041
    :cond_2
    iget-object v1, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 45042
    :try_start_2
    iget-boolean v3, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->j:Z

    if-nez v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {v4, v3}, LX/0M5;->b(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 45043
    :cond_3
    iget-object v3, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->e:[LX/0M6;

    iget v5, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g:I

    aput-object v4, v3, v5

    .line 45044
    :goto_2
    iget-object v3, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->d:[LX/08M;

    iget v4, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f:I

    aput-object v0, v3, v4

    .line 45045
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move v0, v2

    .line 45046
    goto :goto_1

    .line 45047
    :cond_4
    invoke-virtual {v0, v6}, LX/0M5;->b(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 45048
    invoke-virtual {v4, v6}, LX/0M5;->a(I)V

    .line 45049
    :cond_5
    invoke-virtual {p0, v0, v4}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a(LX/08M;LX/0M6;)Ljava/lang/Exception;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->i:Ljava/lang/Exception;

    .line 45050
    iget-object v3, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->i:Ljava/lang/Exception;

    if-eqz v3, :cond_2

    .line 45051
    iget-object v2, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    monitor-exit v2

    move v0, v1

    .line 45052
    goto :goto_1

    .line 45053
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 45054
    :cond_6
    :try_start_4
    iget-object v3, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->c:Ljava/util/LinkedList;

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_2

    .line 45055
    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0
.end method

.method private static j(Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;)Z
    .locals 1

    .prologue
    .line 45026
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/08M;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TI;^TE;"
        }
    .end annotation

    .prologue
    .line 44999
    iget-object v1, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 45000
    :try_start_0
    invoke-static {p0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g(Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;)V

    .line 45001
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->h:LX/08M;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->b(Z)V

    .line 45002
    iget v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f:I

    if-nez v0, :cond_1

    .line 45003
    const/4 v0, 0x0

    monitor-exit v1

    .line 45004
    :goto_1
    return-object v0

    .line 45005
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 45006
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->d:[LX/08M;

    iget v2, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f:I

    aget-object v0, v0, v2

    .line 45007
    invoke-virtual {v0}, LX/0M5;->a()V

    .line 45008
    iput-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->h:LX/08M;

    .line 45009
    monitor-exit v1

    goto :goto_1

    .line 45010
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract a(LX/08M;LX/0M6;)Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TI;TO;)TE;"
        }
    .end annotation
.end method

.method public final a(LX/08M;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TI;)V^TE;"
        }
    .end annotation

    .prologue
    .line 45062
    iget-object v1, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 45063
    :try_start_0
    invoke-static {p0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g(Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;)V

    .line 45064
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->h:LX/08M;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Av;->a(Z)V

    .line 45065
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 45066
    invoke-direct {p0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->h()V

    .line 45067
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->h:LX/08M;

    .line 45068
    monitor-exit v1

    return-void

    .line 45069
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 45070
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(LX/0M6;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TO;)V"
        }
    .end annotation

    .prologue
    .line 44966
    iget-object v1, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 44967
    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->e:[LX/0M6;

    iget v2, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g:I

    aput-object p1, v0, v2

    .line 44968
    invoke-direct {p0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->h()V

    .line 44969
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()LX/0M6;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TO;^TE;"
        }
    .end annotation

    .prologue
    .line 44970
    iget-object v1, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 44971
    :try_start_0
    invoke-static {p0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g(Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;)V

    .line 44972
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44973
    const/4 v0, 0x0

    monitor-exit v1

    .line 44974
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0M6;

    monitor-exit v1

    goto :goto_0

    .line 44975
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 44976
    iget-object v1, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 44977
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->j:Z

    .line 44978
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->h:LX/08M;

    if-eqz v0, :cond_0

    .line 44979
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->d:[LX/08M;

    iget v2, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f:I

    iget-object v3, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->h:LX/08M;

    aput-object v3, v0, v2

    .line 44980
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->h:LX/08M;

    .line 44981
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 44982
    iget-object v2, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->d:[LX/08M;

    iget v3, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f:I

    add-int/lit8 v0, v3, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->f:I

    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/08M;

    aput-object v0, v2, v3

    goto :goto_0

    .line 44983
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 44984
    :cond_1
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 44985
    iget-object v2, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->e:[LX/0M6;

    iget v3, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g:I

    add-int/lit8 v0, v3, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->g:I

    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0M6;

    aput-object v0, v2, v3

    goto :goto_1

    .line 44986
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 44987
    iget-object v1, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 44988
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->k:Z

    .line 44989
    iget-object v0, p0, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->a:Ljava/lang/Object;

    const v2, -0x6127abfb

    invoke-static {v0, v2}, LX/02L;->b(Ljava/lang/Object;I)V

    .line 44990
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44991
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 44992
    :goto_0
    return-void

    .line 44993
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 44994
    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public abstract e()LX/08M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TI;"
        }
    .end annotation
.end method

.method public abstract f()LX/0M6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TO;"
        }
    .end annotation
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 44995
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/exoplayer/util/extensions/SimpleDecoder;->i()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_0

    .line 44996
    return-void

    .line 44997
    :catch_0
    move-exception v0

    .line 44998
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
