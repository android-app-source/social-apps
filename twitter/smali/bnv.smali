.class public Lbnv;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(Lavz;)Lcom/twitter/database/lru/schema/LruSchema;
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Lavz;->c()Lcom/twitter/database/lru/schema/LruSchema;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/twitter/library/provider/h;)Lcom/twitter/database/schema/DraftsSchema;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/twitter/library/provider/h;->c()Lcom/twitter/database/schema/DraftsSchema;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/twitter/library/provider/t;)Lcom/twitter/database/schema/TwitterSchema;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;Lcnz;)Lcom/twitter/library/provider/t;
    .locals 5

    .prologue
    .line 30
    invoke-virtual {p1}, Lcnz;->b()J

    move-result-wide v0

    .line 31
    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->l(J)Ljava/lang/String;

    move-result-object v2

    .line 32
    new-instance v3, Lcom/twitter/library/provider/t;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v2, v0, v1}, Lcom/twitter/library/provider/t;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    return-object v3
.end method

.method static b(Landroid/content/Context;Lcnz;)Lcom/twitter/library/provider/h;
    .locals 4

    .prologue
    .line 47
    new-instance v0, Lcom/twitter/library/provider/h;

    invoke-virtual {p1}, Lcnz;->b()J

    move-result-wide v2

    invoke-direct {v0, p0, v2, v3}, Lcom/twitter/library/provider/h;-><init>(Landroid/content/Context;J)V

    return-object v0
.end method

.method static c(Landroid/content/Context;Lcnz;)Lcom/twitter/library/scribe/ScribeDatabaseHelper;
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lcom/twitter/library/scribe/ScribeDatabaseHelper;

    invoke-virtual {p1}, Lcnz;->b()J

    move-result-wide v2

    invoke-direct {v0, p0, v2, v3}, Lcom/twitter/library/scribe/ScribeDatabaseHelper;-><init>(Landroid/content/Context;J)V

    return-object v0
.end method

.method static d(Landroid/content/Context;Lcnz;)Lavz;
    .locals 4

    .prologue
    .line 70
    new-instance v0, Lavz;

    invoke-virtual {p1}, Lcnz;->b()J

    move-result-wide v2

    invoke-direct {v0, p0, v2, v3}, Lavz;-><init>(Landroid/content/Context;J)V

    return-object v0
.end method

.method static e(Landroid/content/Context;Lcnz;)Lcom/twitter/library/database/dm/a;
    .locals 4

    .prologue
    .line 85
    new-instance v0, Lcom/twitter/library/database/dm/a;

    invoke-virtual {p1}, Lcnz;->b()J

    move-result-wide v2

    invoke-direct {v0, p0, v2, v3}, Lcom/twitter/library/database/dm/a;-><init>(Landroid/content/Context;J)V

    return-object v0
.end method
