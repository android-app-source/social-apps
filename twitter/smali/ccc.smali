.class public Lccc;
.super Lccb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccc$b;,
        Lccc$a;
    }
.end annotation


# static fields
.field public static final c:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lccc;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lccc$b;

    invoke-direct {v0}, Lccc$b;-><init>()V

    sput-object v0, Lccc;->c:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lccc$a;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lccb;-><init>(Lccb$a;)V

    .line 24
    const-string/jumbo v0, "b2c_nps_feedback_in_dm_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lccc;->d:Z

    .line 25
    return-void
.end method

.method synthetic constructor <init>(Lccc$a;Lccc$1;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lccc;-><init>(Lccc$a;)V

    return-void
.end method


# virtual methods
.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const-string/jumbo v0, "2586390716:feedback_nps"

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Lccb;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lccc;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
