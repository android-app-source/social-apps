.class public Lbgq;
.super Lbgv;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;JILbgp;)V
    .locals 11

    .prologue
    .line 16
    const-class v0, Lbgq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const/16 v6, 0x10

    const/4 v9, 0x0

    sget-object v10, Lbgw;->a:Lbgw;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p3

    move/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v10}, Lbgv;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;JIILbgp;Ljava/lang/String;Lbgw;)V

    .line 18
    return-void
.end method


# virtual methods
.method public ba_()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 32
    invoke-virtual {p0}, Lbgq;->x()I

    move-result v1

    .line 33
    if-eq v1, v0, :cond_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 23
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "timeline"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "favorites"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lbgq;->b:J

    .line 24
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 23
    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method
