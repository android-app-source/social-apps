.class public Lahk;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field a:Laii;

.field private final b:Lcom/twitter/app/common/base/BaseFragmentActivity;

.field private final c:Lbqg;

.field private final d:Lcom/twitter/android/geo/a;

.field private final e:Laht;

.field private final f:Lcom/twitter/android/search/AdvancedSearchFiltersActivity$a;

.field private final g:Lahx;


# direct methods
.method constructor <init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Laii;Lahx;Lcom/twitter/android/search/AdvancedSearchFiltersActivity$a;Laht;Lbqg;Lcom/twitter/android/geo/a;)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lahk;->b:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 65
    iput-object p6, p0, Lahk;->c:Lbqg;

    .line 66
    iput-object p7, p0, Lahk;->d:Lcom/twitter/android/geo/a;

    .line 67
    iput-object p3, p0, Lahk;->g:Lahx;

    .line 68
    iput-object p4, p0, Lahk;->f:Lcom/twitter/android/search/AdvancedSearchFiltersActivity$a;

    .line 69
    iput-object p2, p0, Lahk;->a:Laii;

    .line 70
    iput-object p5, p0, Lahk;->e:Laht;

    .line 71
    iget-object v0, p0, Lahk;->g:Lahx;

    iget-object v1, p0, Lahk;->a:Laii;

    invoke-virtual {v1}, Laii;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lahx;->a(Z)V

    .line 72
    iget-object v0, p0, Lahk;->g:Lahx;

    iget-object v1, p0, Lahk;->a:Laii;

    invoke-virtual {v1}, Laii;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lahx;->b(Z)V

    .line 73
    iget-object v0, p0, Lahk;->g:Lahx;

    new-instance v1, Lahk$1;

    invoke-direct {v1, p0}, Lahk$1;-><init>(Lahk;)V

    invoke-virtual {v0, v1}, Lahx;->a(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v0, p0, Lahk;->g:Lahx;

    new-instance v1, Lahk$2;

    invoke-direct {v1, p0}, Lahk$2;-><init>(Lahk;)V

    invoke-virtual {v0, v1}, Lahx;->b(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v0, p0, Lahk;->g:Lahx;

    new-instance v1, Lahk$3;

    invoke-direct {v1, p0}, Lahk$3;-><init>(Lahk;)V

    invoke-virtual {v0, v1}, Lahx;->a(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Landroid/content/Intent;Lahx;Lcom/twitter/android/search/AdvancedSearchFiltersActivity$a;Laht;)V
    .locals 8

    .prologue
    .line 49
    const-string/jumbo v0, "extra_advanced_filters"

    sget-object v1, Laii;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p2, v0, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Laii;->b:Laii;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laii;

    .line 52
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v6

    new-instance v7, Lcom/twitter/android/geo/a;

    const-string/jumbo v0, "search_activity_location_dialog"

    .line 54
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v1

    const/16 v3, 0x47

    invoke-direct {v7, p1, v0, v1, v3}, Lcom/twitter/android/geo/a;-><init>(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Lcom/twitter/util/android/f;I)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 49
    invoke-direct/range {v0 .. v7}, Lahk;-><init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Laii;Lahx;Lcom/twitter/android/search/AdvancedSearchFiltersActivity$a;Laht;Lbqg;Lcom/twitter/android/geo/a;)V

    .line 55
    return-void
.end method

.method static synthetic a(Lahk;)Lcom/twitter/android/search/AdvancedSearchFiltersActivity$a;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lahk;->f:Lcom/twitter/android/search/AdvancedSearchFiltersActivity$a;

    return-object v0
.end method

.method static synthetic b(Lahk;)Laht;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lahk;->e:Laht;

    return-object v0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lahk;->c:Lbqg;

    invoke-virtual {v0}, Lbqg;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahk;->c:Lbqg;

    invoke-virtual {v0}, Lbqg;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lahk;)Z
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lahk;->b()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lahk;)Lcom/twitter/android/geo/a;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lahk;->d:Lcom/twitter/android/geo/a;

    return-object v0
.end method


# virtual methods
.method public a(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    .line 115
    const/16 v0, 0x47

    if-ne p1, v0, :cond_0

    .line 116
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    const-string/jumbo v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/util/android/f;->a(Ljava/lang/String;[Ljava/lang/String;[I)Z

    move-result v0

    .line 118
    new-instance v1, Laii$a;

    iget-object v2, p0, Lahk;->a:Laii;

    invoke-direct {v1, v2}, Laii$a;-><init>(Laii;)V

    .line 119
    invoke-virtual {v1, v0}, Laii$a;->b(Z)Laii$a;

    .line 120
    invoke-virtual {v1}, Laii$a;->e()Laii;

    move-result-object v1

    iput-object v1, p0, Lahk;->a:Laii;

    .line 121
    iget-object v1, p0, Lahk;->g:Lahx;

    invoke-virtual {v1, v0}, Lahx;->b(Z)V

    .line 122
    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, Lahk;->b:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-static {v0}, Lcom/twitter/android/geo/a;->b(Landroid/content/Context;)V

    .line 124
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbqg;->a(Z)V

    .line 127
    :cond_0
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lahk;->g:Lahx;

    invoke-virtual {v0}, Lahx;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
