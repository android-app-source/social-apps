.class public final Lcgb$d$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcgb$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcgb$d;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcgb$e;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcgb$b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcgb$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcgb$d$a;)Lcgb$e;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcgb$d$a;->a:Lcgb$e;

    return-object v0
.end method

.method static synthetic b(Lcgb$d$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcgb$d$a;->b:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(Lcgb$d$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcgb$d$a;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcgb$d$a;->a:Lcgb$e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcgb$d$a;->b:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcgb$d$a;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcgb$e;)Lcgb$d$a;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcgb$d$a;->a:Lcgb$e;

    .line 81
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcgb$d$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcgb$c;",
            ">;)",
            "Lcgb$d$a;"
        }
    .end annotation

    .prologue
    .line 94
    iput-object p1, p0, Lcgb$d$a;->c:Ljava/util/List;

    .line 95
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcgb$d$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcgb$b;",
            ">;)",
            "Lcgb$d$a;"
        }
    .end annotation

    .prologue
    .line 87
    iput-object p1, p0, Lcgb$d$a;->b:Ljava/util/Map;

    .line 88
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcgb$d$a;->e()Lcgb$d;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcgb$d;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Lcgb$d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcgb$d;-><init>(Lcgb$d$a;Lcgb$1;)V

    return-object v0
.end method
