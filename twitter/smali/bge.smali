.class public abstract Lbge;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TPR:",
        "Lcom/twitter/library/service/c;",
        ">",
        "Lbao",
        "<TTPR;>;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:J

.field private h:I

.field private i:Ljava/lang/String;

.field private j:I

.field private k:Lcom/twitter/model/timeline/u;

.field private l:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lbge;->h:I

    .line 78
    const-string/jumbo v0, ""

    iput-object v0, p0, Lbge;->i:Ljava/lang/String;

    .line 90
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1, p2, p3}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lbge;->h:I

    .line 78
    const-string/jumbo v0, ""

    iput-object v0, p0, Lbge;->i:Ljava/lang/String;

    .line 94
    return-void
.end method


# virtual methods
.method public final D()J
    .locals 2

    .prologue
    .line 148
    iget-wide v0, p0, Lbge;->c:J

    return-wide v0
.end method

.method public final E()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lbge;->h:I

    return v0
.end method

.method public final F()J
    .locals 2

    .prologue
    .line 168
    iget-wide v0, p0, Lbge;->b:J

    return-wide v0
.end method

.method public final G()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lbge;->a:I

    return v0
.end method

.method public final H()Lcom/twitter/model/timeline/u;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lbge;->k:Lcom/twitter/model/timeline/u;

    return-object v0
.end method

.method public I()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lbge;->j:I

    return v0
.end method

.method public final a(Lcom/twitter/model/timeline/u;)Lbge;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lbge;",
            ">(",
            "Lcom/twitter/model/timeline/u;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 189
    iput-object p1, p0, Lbge;->k:Lcom/twitter/model/timeline/u;

    .line 190
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbge;

    return-object v0
.end method

.method public final b(J)Lbge;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lbge;",
            ">(J)TT;"
        }
    .end annotation

    .prologue
    .line 153
    iput-wide p1, p0, Lbge;->c:J

    .line 154
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lbge;
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lbge;->l:Ljava/lang/String;

    .line 99
    return-object p0
.end method

.method protected final b()Lcom/twitter/library/service/d$a;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 107
    invoke-virtual {p0}, Lbge;->g()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 108
    iget v1, p0, Lbge;->h:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 109
    const-string/jumbo v1, "count"

    iget v2, p0, Lbge;->h:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 111
    :cond_0
    iget-wide v2, p0, Lbge;->c:J

    cmp-long v1, v2, v6

    if-nez v1, :cond_1

    iget-wide v2, p0, Lbge;->b:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 112
    const-string/jumbo v1, "since_id"

    iget-wide v2, p0, Lbge;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 114
    :cond_1
    iget-wide v2, p0, Lbge;->c:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_2

    .line 115
    const-string/jumbo v1, "max_id"

    iget-wide v2, p0, Lbge;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 117
    :cond_2
    iget-object v1, p0, Lbge;->l:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/network/apache/util/d;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 118
    const-string/jumbo v1, "Twitter-Display-Size"

    iget-object v2, p0, Lbge;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 120
    :cond_3
    iget-object v1, p0, Lbge;->i:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 121
    const-string/jumbo v1, "request_context"

    iget-object v2, p0, Lbge;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 129
    :cond_4
    invoke-virtual {p0}, Lbge;->I()I

    move-result v1

    .line 131
    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 132
    const-string/jumbo v1, "autoplay_enabled"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 137
    :cond_5
    :goto_0
    const-string/jumbo v1, "include_entities"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->f()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->e()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_media_features"

    .line 141
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_user_entities"

    .line 142
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 143
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->d()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 137
    return-object v0

    .line 133
    :cond_6
    if-ne v1, v4, :cond_5

    .line 134
    const-string/jumbo v1, "autoplay_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    goto :goto_0
.end method

.method public final c(I)Lbge;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lbge;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 163
    iput p1, p0, Lbge;->h:I

    .line 164
    return-object p0
.end method

.method public final c(J)Lbge;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lbge;",
            ">(J)TT;"
        }
    .end annotation

    .prologue
    .line 173
    iput-wide p1, p0, Lbge;->b:J

    .line 174
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lbge;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lbge;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 200
    iput-object p1, p0, Lbge;->i:Ljava/lang/String;

    .line 201
    return-object p0
.end method

.method public final d(I)Lbge;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lbge;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 183
    iput p1, p0, Lbge;->a:I

    .line 184
    return-object p0
.end method

.method public final e(I)Lbge;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lbge;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 211
    iput p1, p0, Lbge;->j:I

    .line 212
    return-object p0
.end method

.method protected abstract g()Lcom/twitter/library/service/d$a;
.end method
