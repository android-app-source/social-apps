.class public Lbxw;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(ZLcom/twitter/library/media/widget/AdaptiveTweetMediaView;Lcom/twitter/library/media/widget/TweetMediaView$a;Lcom/twitter/model/core/Tweet;IIZZ)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 64
    if-eqz p2, :cond_0

    .line 65
    invoke-virtual {p1, p2}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setOnImageLoadedListener(Lcom/twitter/library/media/widget/TweetMediaView$a;)V

    .line 67
    :cond_0
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/twitter/util/ui/a;->a(Landroid/view/View;I)V

    .line 69
    invoke-virtual {p3}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v4

    .line 71
    invoke-virtual {p1, p4}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setMediaDividerSize(I)V

    .line 72
    invoke-virtual {p1, p5}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setMediaPlaceholder(I)V

    .line 73
    if-eqz p6, :cond_3

    .line 74
    invoke-static {}, Lbpi;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    move v3, v2

    .line 75
    :goto_0
    if-eqz p7, :cond_4

    .line 76
    invoke-static {}, Lcmj;->h()Z

    move-result v0

    .line 75
    :goto_1
    invoke-virtual {p1, v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->c(Z)V

    .line 77
    if-nez v3, :cond_1

    .line 78
    invoke-static {p3}, Lcom/twitter/android/av/h;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    .line 77
    :goto_2
    invoke-virtual {p1, v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->a(Z)V

    .line 79
    invoke-virtual {p1, v3}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->b(Z)V

    .line 80
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setSingleImageMinAspectRatio(F)V

    .line 83
    invoke-virtual {p3}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    .line 84
    invoke-static {}, Lbpi;->d()Lcom/twitter/util/math/Size;

    move-result-object v3

    .line 83
    invoke-static {v0, v3}, Lcom/twitter/model/util/c;->a(Ljava/lang/Iterable;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v0

    .line 86
    if-eqz p0, :cond_6

    .line 87
    iget-object v0, p3, Lcom/twitter/model/core/Tweet;->V:Ljava/util/List;

    .line 88
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 89
    invoke-virtual {p1, v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setEditableMedia(Ljava/util/List;)V

    .line 99
    :cond_2
    :goto_3
    invoke-virtual {p1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->g()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 100
    invoke-virtual {p1, v1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setVisibility(I)V

    .line 106
    :goto_4
    invoke-virtual {p3}, Lcom/twitter/model/core/Tweet;->ao()Z

    move-result v0

    if-nez v0, :cond_a

    :goto_5
    invoke-virtual {p1, v2}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setClickable(Z)V

    .line 107
    return-void

    :cond_3
    move v3, v1

    .line 74
    goto :goto_0

    :cond_4
    move v0, v1

    .line 76
    goto :goto_1

    :cond_5
    move v0, v1

    .line 78
    goto :goto_2

    .line 91
    :cond_6
    if-eqz v4, :cond_7

    .line 92
    invoke-virtual {p1, v4}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setCard(Lcax;)V

    goto :goto_3

    .line 93
    :cond_7
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 94
    iget-object v3, p3, Lcom/twitter/model/core/Tweet;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->a(Ljava/lang/Iterable;Ljava/lang/String;)V

    goto :goto_3

    .line 96
    :cond_8
    invoke-virtual {p1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->d()V

    goto :goto_3

    .line 102
    :cond_9
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setVisibility(I)V

    goto :goto_4

    :cond_a
    move v2, v1

    .line 106
    goto :goto_5
.end method
