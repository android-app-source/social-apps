.class public Lbsk;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/database/model/i;


# direct methods
.method public constructor <init>(Lcom/twitter/database/model/i;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lbsk;->a:Lcom/twitter/database/model/i;

    .line 32
    return-void
.end method

.method public static a(Lcom/twitter/database/model/h;Lcac;II)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/model/h",
            "<",
            "Lawm$a;",
            ">;",
            "Lcac;",
            "II)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 67
    iget-object v0, p0, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v0, Lawm$a;

    .line 68
    invoke-interface {v0, p3}, Lawm$a;->a(I)Lawm$a;

    move-result-object v0

    .line 69
    invoke-interface {v0, p2}, Lawm$a;->b(I)Lawm$a;

    move-result-object v0

    .line 70
    invoke-virtual {p1}, Lcac;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lawm$a;->a(J)Lawm$a;

    move-result-object v0

    .line 71
    invoke-virtual {p1}, Lcac;->c()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lawm$a;->b(J)Lawm$a;

    move-result-object v0

    iget-wide v2, p1, Lcac;->b:J

    .line 72
    invoke-interface {v0, v2, v3}, Lawm$a;->c(J)Lawm$a;

    move-result-object v0

    iget-wide v2, p1, Lcac;->c:J

    .line 73
    invoke-interface {v0, v2, v3}, Lawm$a;->d(J)Lawm$a;

    move-result-object v0

    .line 74
    invoke-interface {v0, v1}, Lawm$a;->a(Z)Lawm$a;

    .line 75
    invoke-virtual {p0}, Lcom/twitter/database/model/h;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 5

    .prologue
    .line 58
    iget-object v0, p0, Lbsk;->a:Lcom/twitter/database/model/i;

    const-class v1, Lawk;

    invoke-interface {v0, v1}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Lawk;

    invoke-interface {v0}, Lawk;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    sget-object v1, Lawk;->b:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 59
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/l;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v0

    .line 58
    return v0
.end method

.method public a(IJ)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 87
    iget-object v0, p0, Lbsk;->a:Lcom/twitter/database/model/i;

    const-class v1, Lawk;

    .line 88
    invoke-interface {v0, v1}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Lawk;

    invoke-interface {v0}, Lawk;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    .line 90
    sget-object v1, Lawk;->a:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    .line 91
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/l;->b(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 92
    int-to-long v2, v1

    cmp-long v2, v2, p2

    if-gtz v2, :cond_0

    .line 117
    :goto_0
    return-void

    .line 96
    :cond_0
    new-instance v2, Lcom/twitter/database/model/f$a;

    invoke-direct {v2}, Lcom/twitter/database/model/f$a;-><init>()V

    sget-object v3, Lawk;->a:Ljava/lang/String;

    new-array v4, v4, [Ljava/lang/String;

    .line 97
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v2

    const-string/jumbo v3, "sort_id ASC"

    .line 98
    invoke-virtual {v2, v3}, Lcom/twitter/database/model/f$a;->b(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v2

    int-to-long v4, v1

    const-wide/16 v6, 0x2

    div-long v6, p2, v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    .line 99
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/database/model/f$a;->d(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v1

    .line 100
    invoke-virtual {v1}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v1

    .line 96
    invoke-interface {v0, v1}, Lcom/twitter/database/model/l;->a(Lcom/twitter/database/model/f;)Lcom/twitter/database/model/g;

    move-result-object v1

    .line 101
    iget-object v0, p0, Lbsk;->a:Lcom/twitter/database/model/i;

    invoke-interface {v0}, Lcom/twitter/database/model/i;->h()Lcom/twitter/database/model/o;

    move-result-object v2

    .line 103
    :try_start_0
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lawk$a;

    invoke-interface {v0}, Lawk$a;->b()J

    move-result-wide v4

    .line 105
    iget-object v0, p0, Lbsk;->a:Lcom/twitter/database/model/i;

    const-class v3, Lawe;

    invoke-interface {v0, v3}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    const-string/jumbo v3, "type=? AND max_position<=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 107
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    .line 106
    invoke-interface {v0, v3, v6}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/String;)I

    .line 108
    iget-object v0, p0, Lbsk;->a:Lcom/twitter/database/model/i;

    const-class v3, Lawm;

    invoke-interface {v0, v3}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    const-string/jumbo v3, "type=? AND max_position<=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 110
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v7

    .line 109
    invoke-interface {v0, v3, v6}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/String;)I

    .line 112
    :cond_1
    invoke-interface {v2}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    .line 115
    invoke-interface {v2}, Lcom/twitter/database/model/o;->close()V

    goto/16 :goto_0

    .line 114
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    .line 115
    invoke-interface {v2}, Lcom/twitter/database/model/o;->close()V

    throw v0
.end method

.method public a(ILjava/lang/Iterable;Laut;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Laut;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lbsk;->a:Lcom/twitter/database/model/i;

    const-class v1, Lawm;

    .line 37
    invoke-interface {v0, v1}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    .line 38
    invoke-interface {v0}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v9

    .line 39
    invoke-interface {v0}, Lcom/twitter/database/model/m;->c()Lcom/twitter/database/model/o;

    move-result-object v10

    .line 40
    const/4 v0, 0x0

    .line 42
    :try_start_0
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v8, v0

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    .line 43
    new-instance v1, Lcal;

    iget-wide v2, v0, Lcom/twitter/model/core/ac;->g:J

    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->a()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->a()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcal;-><init>(JJJ)V

    .line 44
    const/4 v0, 0x2

    invoke-static {v9, v1, v0, p1}, Lbsk;->a(Lcom/twitter/database/model/h;Lcac;II)I

    move-result v0

    add-int/2addr v0, v8

    move v8, v0

    .line 46
    goto :goto_0

    .line 47
    :cond_0
    invoke-interface {v10}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    invoke-interface {v10}, Lcom/twitter/database/model/o;->close()V

    .line 52
    if-lez v8, :cond_1

    .line 53
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Lcom/twitter/database/schema/a$p;->a:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-virtual {p3, v0}, Laut;->a([Landroid/net/Uri;)V

    .line 55
    :cond_1
    return-void

    .line 49
    :catchall_0
    move-exception v0

    invoke-interface {v10}, Lcom/twitter/database/model/o;->close()V

    throw v0
.end method

.method public a(JJJILaut;)V
    .locals 11

    .prologue
    .line 142
    const/4 v3, 0x0

    .line 143
    const-wide/16 v4, 0x1

    sub-long v4, p1, v4

    .line 144
    const-wide/16 v6, 0x1

    sub-long v6, p5, v6

    .line 146
    iget-object v2, p0, Lbsk;->a:Lcom/twitter/database/model/i;

    const-class v8, Lawm;

    .line 147
    invoke-interface {v2, v8}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v2

    invoke-interface {v2}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v8

    .line 148
    iget-object v2, p0, Lbsk;->a:Lcom/twitter/database/model/i;

    invoke-interface {v2}, Lcom/twitter/database/model/i;->h()Lcom/twitter/database/model/o;

    move-result-object v9

    .line 150
    :try_start_0
    iget-object v2, v8, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v2, Lawm$a;

    .line 151
    move/from16 v0, p7

    invoke-interface {v2, v0}, Lawm$a;->a(I)Lawm$a;

    move-result-object v2

    const/4 v10, 0x3

    .line 152
    invoke-interface {v2, v10}, Lawm$a;->b(I)Lawm$a;

    move-result-object v2

    .line 153
    invoke-interface {v2, v4, v5}, Lawm$a;->a(J)Lawm$a;

    move-result-object v2

    .line 154
    invoke-interface {v2, v4, v5}, Lawm$a;->c(J)Lawm$a;

    move-result-object v2

    .line 155
    invoke-interface {v2, p3, p4}, Lawm$a;->d(J)Lawm$a;

    move-result-object v2

    .line 156
    invoke-interface {v2, v6, v7}, Lawm$a;->b(J)Lawm$a;

    .line 157
    int-to-long v2, v3

    invoke-virtual {v8}, Lcom/twitter/database/model/h;->b()J

    move-result-wide v4

    add-long/2addr v2, v4

    long-to-int v2, v2

    .line 158
    invoke-interface {v9}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    invoke-interface {v9}, Lcom/twitter/database/model/o;->close()V

    .line 162
    if-eqz p8, :cond_0

    if-lez v2, :cond_0

    .line 163
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/net/Uri;

    const/4 v3, 0x0

    sget-object v4, Lcom/twitter/database/schema/a$p;->a:Landroid/net/Uri;

    aput-object v4, v2, v3

    move-object/from16 v0, p8

    invoke-virtual {v0, v2}, Laut;->a([Landroid/net/Uri;)V

    .line 165
    :cond_0
    return-void

    .line 160
    :catchall_0
    move-exception v2

    invoke-interface {v9}, Lcom/twitter/database/model/o;->close()V

    throw v2
.end method

.method public a(JILaut;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 173
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gtz v2, :cond_0

    .line 191
    :goto_0
    return v1

    .line 177
    :cond_0
    iget-object v2, p0, Lbsk;->a:Lcom/twitter/database/model/i;

    invoke-interface {v2}, Lcom/twitter/database/model/i;->h()Lcom/twitter/database/model/o;

    move-result-object v2

    .line 179
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "data_type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "max_position"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 182
    iget-object v4, p0, Lbsk;->a:Lcom/twitter/database/model/i;

    const-class v5, Lawm;

    invoke-interface {v4, v5}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    .line 183
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-interface {v4, v3, v5}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 184
    invoke-interface {v2}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    invoke-interface {v2}, Lcom/twitter/database/model/o;->close()V

    .line 188
    if-eqz p4, :cond_1

    if-lez v3, :cond_1

    .line 189
    new-array v2, v0, [Landroid/net/Uri;

    sget-object v4, Lcom/twitter/database/schema/a$p;->a:Landroid/net/Uri;

    aput-object v4, v2, v1

    invoke-virtual {p4, v2}, Laut;->a([Landroid/net/Uri;)V

    .line 191
    :cond_1
    if-lez v3, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    .line 186
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Lcom/twitter/database/model/o;->close()V

    throw v0

    :cond_2
    move v0, v1

    .line 191
    goto :goto_1
.end method

.method public b(I)J
    .locals 4

    .prologue
    .line 121
    iget-object v0, p0, Lbsk;->a:Lcom/twitter/database/model/i;

    const-class v1, Lawk;

    invoke-interface {v0, v1}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Lawk;

    invoke-interface {v0}, Lawk;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    .line 122
    new-instance v1, Lcom/twitter/database/model/f$a;

    invoke-direct {v1}, Lcom/twitter/database/model/f$a;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 123
    invoke-virtual {v1, v2}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v1

    const-string/jumbo v2, "max_position DESC"

    .line 124
    invoke-virtual {v1, v2}, Lcom/twitter/database/model/f$a;->b(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v1

    const-string/jumbo v2, "1"

    .line 125
    invoke-virtual {v1, v2}, Lcom/twitter/database/model/f$a;->d(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v1

    .line 126
    invoke-virtual {v1}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v1

    .line 122
    invoke-interface {v0, v1}, Lcom/twitter/database/model/l;->a(Lcom/twitter/database/model/f;)Lcom/twitter/database/model/g;

    move-result-object v2

    .line 128
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, v2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lawk$a;

    invoke-interface {v0}, Lawk$a;->b()J

    move-result-wide v0

    .line 133
    :goto_0
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->close()V

    .line 134
    return-wide v0

    .line 131
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
