.class public Lcib;
.super Lcho;
.source "Twttr"

# interfaces
.implements Lcho$a;


# instance fields
.field public final a:Lcia;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLcom/twitter/model/timeline/r;Lchj;Lcia;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct/range {p0 .. p5}, Lcho;-><init>(Ljava/lang/String;JLcom/twitter/model/timeline/r;Lchj;)V

    .line 31
    iput-object p6, p0, Lcib;->a:Lcia;

    .line 32
    return-void
.end method


# virtual methods
.method public synthetic a(Lcgx;Lchc;)Lcom/twitter/model/timeline/y$a;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1, p2}, Lcib;->d(Lcgx;Lchc;)Lcom/twitter/model/timeline/ah$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcgx;Lchc;)Lcom/twitter/model/timeline/y;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1, p2}, Lcib;->c(Lcgx;Lchc;)Lcom/twitter/model/timeline/ah;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcgx;Lchc;)Lcom/twitter/model/timeline/ah;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1, p2}, Lcib;->d(Lcgx;Lchc;)Lcom/twitter/model/timeline/ah$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/timeline/ah$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah;

    return-object v0
.end method

.method public d(Lcgx;Lchc;)Lcom/twitter/model/timeline/ah$a;
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v2, 0x0

    .line 45
    iget-object v0, p0, Lcib;->a:Lcia;

    iget-object v0, v0, Lcia;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcgx;->a(Ljava/lang/String;)Lcom/twitter/model/core/ac;

    move-result-object v3

    .line 47
    iget-object v0, p0, Lcib;->e:Lchj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcib;->e:Lchj;

    iget-object v0, v0, Lchj;->b:Ljava/lang/String;

    move-object v1, v0

    .line 48
    :goto_0
    iget-object v0, p0, Lcib;->e:Lchj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcib;->e:Lchj;

    iget-object v0, v0, Lchj;->a:Ljava/util/List;

    .line 49
    invoke-virtual {p2, v0}, Lchc;->a(Ljava/util/List;)Lcom/twitter/model/timeline/k;

    move-result-object v2

    .line 51
    :cond_0
    new-instance v0, Lcom/twitter/model/timeline/ah$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/ah$a;-><init>()V

    .line 52
    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/timeline/ah$a;

    move-result-object v0

    iget-object v3, p0, Lcib;->a:Lcia;

    .line 53
    invoke-virtual {v3}, Lcia;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/ah$a;->e(Ljava/lang/String;)Lcom/twitter/model/timeline/ah$a;

    move-result-object v0

    iget-object v3, p0, Lcib;->a:Lcia;

    iget-object v3, v3, Lcia;->c:Lcgi;

    .line 54
    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/ah$a;->a(Lcgi;)Lcom/twitter/model/timeline/ah$a;

    move-result-object v0

    iget-wide v4, p0, Lcib;->c:J

    .line 55
    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/timeline/ah$a;->a(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    iget-object v3, p0, Lcib;->b:Ljava/lang/String;

    .line 56
    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/ah$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    iget-object v3, p0, Lcib;->d:Lcom/twitter/model/timeline/r;

    .line 57
    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    .line 58
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ah$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    .line 59
    invoke-virtual {v0, v2}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/timeline/k;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    iget-object v1, p0, Lcib;->a:Lcia;

    iget-object v1, v1, Lcia;->d:Lcom/twitter/model/core/TwitterSocialProof;

    .line 60
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/core/TwitterSocialProof;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    iget-object v1, p0, Lcib;->a:Lcia;

    iget-object v1, v1, Lcia;->e:Lcom/twitter/model/revenue/d;

    .line 61
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/revenue/d;)Lcom/twitter/model/timeline/ah$a;

    move-result-object v1

    .line 63
    iget-object v0, p0, Lcib;->a:Lcia;

    invoke-virtual {v0}, Lcia;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "MomentTweet"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcib;->a:Lcia;

    invoke-virtual {v0}, Lcia;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lcib;->a:Lcia;

    iget-object v0, v0, Lcia;->d:Lcom/twitter/model/core/TwitterSocialProof;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterSocialProof;->m:Ljava/lang/String;

    .line 65
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string/jumbo v3, "moment_id"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 66
    invoke-static {v2, v6, v7}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 68
    cmp-long v4, v2, v6

    if-eqz v4, :cond_1

    .line 69
    const-string/jumbo v4, "Moments"

    invoke-virtual {v1, v4}, Lcom/twitter/model/timeline/ah$a;->b(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    .line 70
    new-instance v4, Lcom/twitter/model/moments/x;

    const/4 v5, 0x0

    new-instance v6, Lcom/twitter/model/moments/Moment$a;

    invoke-direct {v6}, Lcom/twitter/model/moments/Moment$a;-><init>()V

    .line 72
    invoke-virtual {v6, v2, v3}, Lcom/twitter/model/moments/Moment$a;->a(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    iget-object v3, p0, Lcib;->a:Lcia;

    iget-object v3, v3, Lcia;->d:Lcom/twitter/model/core/TwitterSocialProof;

    iget-object v3, v3, Lcom/twitter/model/core/TwitterSocialProof;->l:Ljava/lang/String;

    .line 73
    invoke-virtual {v2, v3}, Lcom/twitter/model/moments/Moment$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 74
    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/Moment$a;->g(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/twitter/model/moments/Moment$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    invoke-direct {v4, v5, v0}, Lcom/twitter/model/moments/x;-><init>(ILcom/twitter/model/moments/Moment;)V

    .line 70
    invoke-virtual {v1, v4}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/moments/x;)Lcom/twitter/model/timeline/y$a;

    .line 79
    :cond_1
    return-object v1

    :cond_2
    move-object v1, v2

    .line 47
    goto/16 :goto_0
.end method
