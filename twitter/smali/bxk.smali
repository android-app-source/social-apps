.class public Lbxk;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbxk$a;
    }
.end annotation


# instance fields
.field private final a:Lbxk$a;

.field private final b:[J

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final d:J

.field private final e:J

.field private final f:J


# direct methods
.method public constructor <init>(Lbxk$a;[JLjava/util/List;JJJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxk$a;",
            "[J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;JJJ)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lbxk;->a:Lbxk$a;

    .line 22
    iput-object p2, p0, Lbxk;->b:[J

    .line 23
    iput-object p3, p0, Lbxk;->c:Ljava/util/List;

    .line 24
    iput-wide p4, p0, Lbxk;->d:J

    .line 25
    iput-wide p6, p0, Lbxk;->e:J

    .line 26
    iput-wide p8, p0, Lbxk;->f:J

    .line 27
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    .line 31
    iget-object v1, p0, Lbxk;->a:Lbxk$a;

    iget-object v2, p0, Lbxk;->b:[J

    iget-object v3, p0, Lbxk;->c:Ljava/util/List;

    iget-wide v4, p0, Lbxk;->d:J

    iget-wide v6, p0, Lbxk;->e:J

    iget-wide v8, p0, Lbxk;->f:J

    invoke-interface/range {v1 .. v9}, Lbxk$a;->a([JLjava/util/List;JJJ)V

    .line 32
    return-void
.end method
