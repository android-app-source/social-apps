.class public Lckk;
.super Lckm;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lckk$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/graphics/PointF;

.field private final b:Landroid/graphics/PointF;

.field private final c:Landroid/graphics/PointF;

.field private d:F

.field private e:Lckk$a;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lckm;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lckk;->a:Landroid/graphics/PointF;

    .line 26
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lckk;->b:Landroid/graphics/PointF;

    .line 27
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lckk;->c:Landroid/graphics/PointF;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lckk;->f:Z

    .line 34
    invoke-virtual {p0}, Lckk;->a()V

    .line 35
    return-void
.end method

.method private a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 136
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lckk;->d:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 137
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lckk;->d:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 136
    :goto_0
    return v0

    .line 137
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p0}, Lckk;->d()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lckk;->d:F

    .line 39
    invoke-virtual {p0}, Lckk;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 41
    invoke-virtual {p0}, Lckk;->c()Landroid/view/View;

    move-result-object v0

    sget v1, Lckx$b;->dock_content_touchable:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 42
    invoke-virtual {p0}, Lckk;->b()Landroid/view/ViewGroup;

    move-result-object v0

    sget v1, Lckx$b;->dock_content_presenter:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setId(I)V

    .line 43
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Lckk;->b()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 52
    if-eqz p1, :cond_0

    .line 53
    invoke-virtual {p0}, Lckk;->b()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 55
    :cond_0
    return-void
.end method

.method public a(Lckk$a;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lckk;->e:Lckk$a;

    .line 59
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 73
    .line 75
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 127
    :cond_0
    :goto_0
    iget-object v1, p0, Lckk;->a:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iput v2, v1, Landroid/graphics/PointF;->x:F

    .line 128
    iget-object v1, p0, Lckk;->a:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iput v2, v1, Landroid/graphics/PointF;->y:F

    .line 130
    return v0

    .line 77
    :pswitch_0
    iget-object v2, p0, Lckk;->b:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 78
    iput-boolean v1, p0, Lckk;->f:Z

    goto :goto_0

    .line 83
    :pswitch_1
    iget-boolean v1, p0, Lckk;->f:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lckk;->b:Landroid/graphics/PointF;

    invoke-direct {p0, v1, p2}, Lckk;->a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 84
    iget-object v1, p0, Lckk;->e:Lckk$a;

    if-eqz v1, :cond_1

    .line 85
    iget-object v1, p0, Lckk;->e:Lckk$a;

    invoke-interface {v1, p0}, Lckk$a;->b(Lckk;)V

    .line 88
    :cond_1
    iget-object v1, p0, Lckk;->c:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p0, Lckk;->a:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/PointF;->x:F

    .line 89
    iget-object v1, p0, Lckk;->c:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lckk;->a:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/PointF;->y:F

    .line 90
    iput-boolean v0, p0, Lckk;->f:Z

    goto :goto_0

    .line 91
    :cond_2
    iget-boolean v1, p0, Lckk;->f:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lckk;->e:Lckk$a;

    if-eqz v1, :cond_0

    .line 92
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iget-object v2, p0, Lckk;->a:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    .line 93
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget-object v3, p0, Lckk;->a:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    .line 94
    iget-object v3, p0, Lckk;->c:Landroid/graphics/PointF;

    invoke-virtual {v3, v6, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 95
    invoke-virtual {p0}, Lckk;->g()Lcom/twitter/ui/anim/AnimatableParams;

    move-result-object v3

    .line 96
    invoke-virtual {v3}, Lcom/twitter/ui/anim/AnimatableParams;->b()Landroid/graphics/PointF;

    move-result-object v3

    .line 97
    iget v4, v3, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lckk;->c:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v1

    add-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/PointF;->x:F

    .line 98
    iget v4, v3, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lckk;->c:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    add-float/2addr v5, v2

    add-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/PointF;->y:F

    .line 99
    iget-object v3, p0, Lckk;->c:Landroid/graphics/PointF;

    invoke-virtual {v3, v6, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 100
    invoke-virtual {p0}, Lckk;->g()Lcom/twitter/ui/anim/AnimatableParams;

    move-result-object v3

    invoke-virtual {p0}, Lckk;->g()Lcom/twitter/ui/anim/AnimatableParams;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/ui/anim/AnimatableParams;->b()Landroid/graphics/PointF;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/ui/anim/AnimatableParams;->c(Landroid/graphics/PointF;)V

    .line 101
    invoke-virtual {p0}, Lckk;->e()V

    .line 103
    invoke-virtual {p0}, Lckk;->f()V

    .line 104
    iget-object v3, p0, Lckk;->e:Lckk$a;

    invoke-interface {v3, p0, v1, v2}, Lckk$a;->a(Lckk;FF)V

    goto/16 :goto_0

    .line 111
    :pswitch_2
    iget-object v2, p0, Lckk;->e:Lckk$a;

    if-eqz v2, :cond_3

    .line 112
    iget-boolean v2, p0, Lckk;->f:Z

    if-eqz v2, :cond_4

    .line 113
    iget-object v2, p0, Lckk;->e:Lckk$a;

    invoke-interface {v2, p0}, Lckk$a;->c(Lckk;)V

    .line 118
    :cond_3
    :goto_1
    iput-boolean v1, p0, Lckk;->f:Z

    goto/16 :goto_0

    .line 115
    :cond_4
    iget-object v2, p0, Lckk;->e:Lckk$a;

    invoke-interface {v2, p0}, Lckk$a;->a(Lckk;)V

    goto :goto_1

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
