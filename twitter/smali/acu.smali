.class public Lacu;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajq",
        "<",
        "Ladg;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/m;

.field private final b:Lcom/twitter/android/moments/ui/maker/q;

.field private final c:Lcom/twitter/android/util/q;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/m;Lcom/twitter/android/moments/ui/maker/q;Lcom/twitter/android/util/q;Lcom/twitter/android/moments/ui/maker/navigation/ah;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lacu;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/m;

    .line 27
    iput-object p2, p0, Lacu;->b:Lcom/twitter/android/moments/ui/maker/q;

    .line 28
    iput-object p3, p0, Lacu;->c:Lcom/twitter/android/util/q;

    .line 29
    return-void
.end method

.method static synthetic a(Lacu;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lacu;->b()V

    return-void
.end method

.method static synthetic b(Lacu;)Lcom/twitter/android/util/q;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lacu;->c:Lcom/twitter/android/util/q;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lacu;->b:Lcom/twitter/android/moments/ui/maker/q;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/q;->a()V

    .line 51
    return-void
.end method


# virtual methods
.method public a(Ladg;)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lacu;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/m;

    new-instance v1, Lacu$1;

    invoke-direct {v1, p0}, Lacu$1;-><init>(Lacu;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/m;->a(Landroid/view/View$OnClickListener;)V

    .line 47
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Ladg;

    invoke-virtual {p0, p1}, Lacu;->a(Ladg;)V

    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lacu;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/m;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/m;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
