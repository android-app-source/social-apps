.class public Labm;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/animation/Interpolator;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/ImageButton;

.field private final e:Landroid/view/View;

.field private final f:Landroid/widget/ImageButton;

.field private final g:Landroid/view/View;

.field private final h:Landroid/view/View;

.field private final i:I


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Landroid/widget/ImageButton;Landroid/view/View;Landroid/view/View;Landroid/content/res/Resources;Landroid/widget/ImageButton;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Lcom/twitter/ui/anim/h;->b()Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Labm;->a:Landroid/view/animation/Interpolator;

    .line 41
    iput-object p1, p0, Labm;->b:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Labm;->c:Landroid/view/View;

    .line 43
    iput-object p3, p0, Labm;->e:Landroid/view/View;

    .line 44
    iput-object p4, p0, Labm;->f:Landroid/widget/ImageButton;

    .line 45
    iput-object p8, p0, Labm;->d:Landroid/widget/ImageButton;

    .line 46
    iput-object p5, p0, Labm;->g:Landroid/view/View;

    .line 47
    iget-object v0, p0, Labm;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 48
    iput-object p6, p0, Labm;->h:Landroid/view/View;

    .line 49
    const v0, 0x7f0f0034

    invoke-virtual {p7, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Labm;->i:I

    .line 51
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Labm;
    .locals 9

    .prologue
    .line 32
    new-instance v0, Labm;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f130402

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f130508

    .line 33
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f130510

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    const v5, 0x7f130484

    .line 34
    invoke-virtual {p0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f130515

    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 35
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f130506

    invoke-virtual {p0, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    invoke-direct/range {v0 .. v8}, Labm;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Landroid/widget/ImageButton;Landroid/view/View;Landroid/view/View;Landroid/content/res/Resources;Landroid/widget/ImageButton;)V

    .line 32
    return-object v0
.end method

.method private a(F)V
    .locals 4

    .prologue
    .line 106
    iget-object v0, p0, Labm;->c:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->withLayer()Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 108
    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->alpha(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    iget v1, p0, Labm;->i:I

    int-to-long v2, v1

    .line 109
    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    iget-object v1, p0, Labm;->a:Landroid/view/animation/Interpolator;

    .line 110
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 112
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Labm;->d:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 73
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Labm;->d:Landroid/widget/ImageButton;

    invoke-static {v0}, Lcrf;->d(Landroid/view/View;)Lrx/c;

    move-result-object v0

    new-instance v1, Labm$1;

    invoke-direct {v1, p0, p1}, Labm$1;-><init>(Labm;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 87
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 54
    iget-object v0, p0, Labm;->c:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 55
    if-eqz p1, :cond_0

    .line 56
    invoke-direct {p0, v2}, Labm;->a(F)V

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Labm;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Labm;->d:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 77
    return-void
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Labm;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    return-void
.end method

.method public b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63
    iget-object v0, p0, Labm;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 64
    if-eqz p1, :cond_0

    .line 65
    invoke-direct {p0, v2}, Labm;->a(F)V

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Labm;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Labm;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 95
    return-void
.end method

.method public c(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Labm;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    return-void
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 90
    iget-object v1, p0, Labm;->d:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const v0, 0x7f020497

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 91
    return-void

    .line 90
    :cond_0
    const v0, 0x7f020496

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Labm;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 103
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Labm;->f:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 116
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Labm;->f:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 120
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Labm;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 136
    return-void
.end method
