.class final Lary$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lasa;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lary;

.field private final b:Lant;

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/support/v4/app/FragmentManager;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/smartfollowstep/b;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/model/onboarding/TaskContext;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laqs;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/onboarding/Subtask;",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcje;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcjg;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcjd;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/smartfollowstep/c;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lary;Lant;)V
    .locals 1

    .prologue
    .line 691
    iput-object p1, p0, Lary$b;->a:Lary;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 692
    invoke-static {p2}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lant;

    iput-object v0, p0, Lary$b;->b:Lant;

    .line 693
    invoke-direct {p0}, Lary$b;->c()V

    .line 694
    return-void
.end method

.method synthetic constructor <init>(Lary;Lant;Lary$1;)V
    .locals 0

    .prologue
    .line 660
    invoke-direct {p0, p1, p2}, Lary$b;-><init>(Lary;Lant;)V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 699
    iget-object v0, p0, Lary$b;->b:Lant;

    .line 701
    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 700
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lary$b;->c:Lcta;

    .line 703
    iget-object v0, p0, Lary$b;->c:Lcta;

    .line 705
    invoke-static {v0}, Lanx;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 704
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lary$b;->d:Lcta;

    .line 707
    iget-object v0, p0, Lary$b;->c:Lcta;

    .line 709
    invoke-static {v0}, Lanv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 708
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lary$b;->e:Lcta;

    .line 712
    iget-object v0, p0, Lary$b;->e:Lcta;

    .line 714
    invoke-static {v0}, Laob;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 713
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lary$b;->f:Lcta;

    .line 717
    iget-object v0, p0, Lary$b;->d:Lcta;

    iget-object v1, p0, Lary$b;->f:Lcta;

    .line 719
    invoke-static {v0, v1}, Lasc;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 718
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lary$b;->g:Lcta;

    .line 722
    iget-object v0, p0, Lary$b;->c:Lcta;

    .line 724
    invoke-static {v0}, Laqv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 723
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lary$b;->h:Lcta;

    .line 727
    iget-object v0, p0, Lary$b;->a:Lary;

    .line 729
    invoke-static {v0}, Lary;->a(Lary;)Lcta;

    move-result-object v0

    .line 728
    invoke-static {v0}, Laqt;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lary$b;->i:Lcta;

    .line 731
    iget-object v0, p0, Lary$b;->i:Lcta;

    .line 732
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lary$b;->j:Lcta;

    .line 734
    iget-object v0, p0, Lary$b;->j:Lcta;

    .line 736
    invoke-static {v0}, Lcjf;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 735
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lary$b;->k:Lcta;

    .line 738
    iget-object v0, p0, Lary$b;->h:Lcta;

    iget-object v1, p0, Lary$b;->k:Lcta;

    iget-object v2, p0, Lary$b;->a:Lary;

    .line 743
    invoke-static {v2}, Lary;->b(Lary;)Lcta;

    move-result-object v2

    .line 744
    invoke-static {}, Lcjc;->c()Ldagger/internal/c;

    move-result-object v3

    .line 740
    invoke-static {v0, v1, v2, v3}, Lcjh;->a(Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 739
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lary$b;->l:Lcta;

    .line 746
    iget-object v0, p0, Lary$b;->l:Lcta;

    .line 747
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lary$b;->m:Lcta;

    .line 752
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lary$b;->g:Lcta;

    iget-object v2, p0, Lary$b;->m:Lcta;

    .line 751
    invoke-static {v0, v1, v2}, Lcom/twitter/app/onboarding/smartfollowstep/d;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 750
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lary$b;->n:Lcta;

    .line 756
    iget-object v0, p0, Lary$b;->n:Lcta;

    .line 757
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lary$b;->o:Lcta;

    .line 758
    return-void
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lary$b;->o:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 762
    iget-object v0, p0, Lary$b;->a:Lary;

    invoke-static {v0}, Lary;->c(Lary;)Lcta;

    move-result-object v0

    .line 763
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 762
    return-object v0
.end method
