.class Lajy$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lajy;->a(Lcom/twitter/library/widget/renderablecontent/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/core/MediaEntity;

.field final synthetic b:Lcom/twitter/model/core/Tweet;

.field final synthetic c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field final synthetic d:Landroid/app/Activity;

.field final synthetic e:Lajy;


# direct methods
.method constructor <init>(Lajy;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lajy$1;->e:Lajy;

    iput-object p2, p0, Lajy$1;->a:Lcom/twitter/model/core/MediaEntity;

    iput-object p3, p0, Lajy$1;->b:Lcom/twitter/model/core/Tweet;

    iput-object p4, p0, Lajy$1;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object p5, p0, Lajy$1;->d:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 82
    iget-object v0, p0, Lajy$1;->a:Lcom/twitter/model/core/MediaEntity;

    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->l:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lajy$1;->a:Lcom/twitter/model/core/MediaEntity;

    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->l:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v2, p0, Lajy$1;->b:Lcom/twitter/model/core/Tweet;

    iget-object v3, p0, Lajy$1;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v4, p0, Lajy$1;->d:Landroid/app/Activity;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/android/profiles/v;->a(JLcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/app/Activity;)V

    .line 85
    :cond_0
    return-void
.end method
