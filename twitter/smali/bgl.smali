.class public Lbgl;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbgp;


# instance fields
.field private final a:Lcom/twitter/library/provider/t;

.field private final b:I

.field private final c:J

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Lcom/twitter/android/timeline/ce;


# direct methods
.method private constructor <init>(Lcom/twitter/library/provider/t;IJLjava/lang/String;ILcom/twitter/android/timeline/ce;)V
    .locals 5

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lbgl;->a:Lcom/twitter/library/provider/t;

    .line 56
    iput p2, p0, Lbgl;->b:I

    .line 57
    iput-wide p3, p0, Lbgl;->c:J

    .line 58
    iput-object p5, p0, Lbgl;->d:Ljava/lang/String;

    .line 59
    iput p6, p0, Lbgl;->e:I

    .line 60
    iput-object p7, p0, Lbgl;->f:Lcom/twitter/android/timeline/ce;

    .line 62
    iget v0, p0, Lbgl;->e:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lbgl;->e:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "DatababaseBackedURTCursorProvider currently only supports CursorTypes TOP and BOTTOM. You provided CursorType: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lbgl;->e:I

    .line 64
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 63
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    return-void
.end method

.method public static a(Lcom/twitter/library/provider/t;IJLjava/lang/String;Lcom/twitter/android/timeline/ce;)Lbgp;
    .locals 10

    .prologue
    .line 40
    new-instance v1, Lbgl;

    const/4 v7, 0x2

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-object v6, p4

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lbgl;-><init>(Lcom/twitter/library/provider/t;IJLjava/lang/String;ILcom/twitter/android/timeline/ce;)V

    return-object v1
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/android/timeline/cf;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 99
    iget-object v0, p0, Lbgl;->a:Lcom/twitter/library/provider/t;

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string/jumbo v1, "timeline_view"

    sget-object v2, Lbue;->a:[Ljava/lang/String;

    const-string/jumbo v7, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v3, p1

    move-object v4, p2

    move-object v6, v5

    move-object v8, v5

    .line 100
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 102
    new-instance v2, Lcom/twitter/android/timeline/bl;

    iget-object v0, p0, Lbgl;->f:Lcom/twitter/android/timeline/ce;

    .line 103
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbp;

    invoke-direct {v2, v1, v0}, Lcom/twitter/android/timeline/bl;-><init>(Landroid/database/Cursor;Lcbp;)V

    .line 105
    :try_start_0
    iget v0, p0, Lbgl;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 117
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 113
    :goto_0
    return-object v5

    .line 107
    :pswitch_0
    :try_start_1
    invoke-virtual {v2}, Lcom/twitter/android/timeline/bl;->d()Lcom/twitter/android/timeline/cf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 117
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 110
    :pswitch_1
    :try_start_2
    invoke-virtual {v2}, Lcom/twitter/android/timeline/bl;->e()Lcom/twitter/android/timeline/cf;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    .line 117
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 105
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Lcom/twitter/library/provider/t;IJLjava/lang/String;Lcom/twitter/android/timeline/ce;)Lbgp;
    .locals 10

    .prologue
    .line 48
    new-instance v1, Lbgl;

    const/4 v7, 0x3

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-object v6, p4

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lbgl;-><init>(Lcom/twitter/library/provider/t;IJLjava/lang/String;ILcom/twitter/android/timeline/ce;)V

    return-object v1
.end method


# virtual methods
.method public a()Lcom/twitter/android/timeline/cf;
    .locals 8

    .prologue
    const/4 v2, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 80
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 83
    iget-object v0, p0, Lbgl;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 84
    new-array v0, v2, [Ljava/lang/String;

    const-string/jumbo v1, "timeline_owner_id=?"

    aput-object v1, v0, v4

    const-string/jumbo v1, "timeline_type=?"

    aput-object v1, v0, v5

    const-string/jumbo v1, "timeline_data_type=?"

    aput-object v1, v0, v6

    const-string/jumbo v1, "timeline_timeline_tag=?"

    aput-object v1, v0, v7

    invoke-static {v0}, Laux;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 86
    new-array v0, v2, [Ljava/lang/String;

    iget-wide v2, p0, Lbgl;->c:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    iget v2, p0, Lbgl;->b:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    const/16 v2, 0xe

    .line 87
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    iget-object v2, p0, Lbgl;->d:Ljava/lang/String;

    aput-object v2, v0, v7

    .line 94
    :goto_0
    invoke-direct {p0, v1, v0}, Lbgl;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/android/timeline/cf;

    move-result-object v0

    return-object v0

    .line 89
    :cond_0
    new-array v0, v2, [Ljava/lang/String;

    const-string/jumbo v1, "timeline_owner_id=?"

    aput-object v1, v0, v4

    const-string/jumbo v1, "timeline_type=?"

    aput-object v1, v0, v5

    const-string/jumbo v1, "timeline_data_type=?"

    aput-object v1, v0, v6

    const-string/jumbo v1, "timeline_timeline_tag is NULL"

    aput-object v1, v0, v7

    invoke-static {v0}, Laux;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 91
    new-array v0, v7, [Ljava/lang/String;

    iget-wide v2, p0, Lbgl;->c:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    iget v2, p0, Lbgl;->b:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    const/16 v2, 0xe

    .line 92
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    goto :goto_0
.end method
