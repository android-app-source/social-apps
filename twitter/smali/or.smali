.class public Lor;
.super Lot;
.source "Twttr"


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lot;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V

    .line 24
    return-void
.end method

.method private a(Z)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-virtual {p0}, Lor;->a()Lcom/twitter/model/av/VideoCta;

    move-result-object v1

    .line 40
    if-nez v1, :cond_0

    .line 66
    :goto_0
    return-object v0

    .line 44
    :cond_0
    sget-object v2, Lor$1;->a:[I

    invoke-virtual {v1}, Lcom/twitter/model/av/VideoCta;->a()Lcom/twitter/model/av/VideoCtaType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/av/VideoCtaType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 46
    :pswitch_0
    if-eqz p1, :cond_1

    .line 47
    const-string/jumbo v0, "cta_app_open_click"

    goto :goto_0

    .line 49
    :cond_1
    const-string/jumbo v0, "cta_app_install_click"

    goto :goto_0

    .line 54
    :pswitch_1
    const-string/jumbo v0, "cta_url_click"

    goto :goto_0

    .line 58
    :pswitch_2
    const-string/jumbo v0, "cta_watch_click"

    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public processCtaClickedEvent(Lbjj;)V
    .locals 3
    .annotation runtime Lbiz;
        a = Lbjj;
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p1}, Lbjj;->a()Z

    move-result v0

    invoke-direct {p0, v0}, Lor;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    iget-object v1, p0, Lor;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {p0}, Lor;->b()Lcom/twitter/library/av/m;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/m;)Lcom/twitter/library/av/m$a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/m;)V

    .line 32
    :cond_0
    return-void
.end method
