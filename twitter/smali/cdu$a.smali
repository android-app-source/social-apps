.class public final Lcdu$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcdu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcdu;",
        ">;"
    }
.end annotation


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:J

.field private h:J

.field private i:Lcea;

.field private j:J

.field private k:Ljava/lang/String;

.field private l:J

.field private m:J

.field private n:J

.field private o:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcdu$a;)Lcea;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcdu$a;->i:Lcea;

    return-object v0
.end method

.method static synthetic b(Lcdu$a;)J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcdu$a;->g:J

    return-wide v0
.end method

.method static synthetic c(Lcdu$a;)J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcdu$a;->h:J

    return-wide v0
.end method

.method static synthetic d(Lcdu$a;)J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcdu$a;->f:J

    return-wide v0
.end method

.method static synthetic e(Lcdu$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcdu$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcdu$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcdu$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcdu$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcdu$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcdu$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcdu$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcdu$a;)J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcdu$a;->a:J

    return-wide v0
.end method

.method static synthetic j(Lcdu$a;)J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcdu$a;->j:J

    return-wide v0
.end method

.method static synthetic k(Lcdu$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcdu$a;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcdu$a;)J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcdu$a;->l:J

    return-wide v0
.end method

.method static synthetic m(Lcdu$a;)J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcdu$a;->m:J

    return-wide v0
.end method

.method static synthetic n(Lcdu$a;)J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcdu$a;->n:J

    return-wide v0
.end method

.method static synthetic o(Lcdu$a;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcdu$a;->o:Z

    return v0
.end method


# virtual methods
.method public R_()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 79
    iget-object v0, p0, Lcdu$a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcdu$a;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcdu$a;->i:Lcea;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcdu$a;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcdu$a;->g:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcdu$a;
    .locals 1

    .prologue
    .line 91
    iput-wide p1, p0, Lcdu$a;->a:J

    .line 92
    return-object p0
.end method

.method public a(Lcea;)Lcdu$a;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcdu$a;->i:Lcea;

    .line 140
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcdu$a;
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcdu$a;->b:Ljava/lang/String;

    .line 98
    return-object p0
.end method

.method public a(Z)Lcdu$a;
    .locals 0

    .prologue
    .line 175
    iput-boolean p1, p0, Lcdu$a;->o:Z

    .line 176
    return-object p0
.end method

.method public b(J)Lcdu$a;
    .locals 1

    .prologue
    .line 121
    iput-wide p1, p0, Lcdu$a;->f:J

    .line 122
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcdu$a;
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcdu$a;->c:Ljava/lang/String;

    .line 104
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcdu$a;->e()Lcdu;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcdu$a;
    .locals 1

    .prologue
    .line 127
    iput-wide p1, p0, Lcdu$a;->g:J

    .line 128
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcdu$a;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcdu$a;->d:Ljava/lang/String;

    .line 110
    return-object p0
.end method

.method public d(J)Lcdu$a;
    .locals 1

    .prologue
    .line 133
    iput-wide p1, p0, Lcdu$a;->h:J

    .line 134
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcdu$a;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcdu$a;->e:Ljava/lang/String;

    .line 116
    return-object p0
.end method

.method public e(J)Lcdu$a;
    .locals 1

    .prologue
    .line 145
    iput-wide p1, p0, Lcdu$a;->j:J

    .line 146
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcdu$a;
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcdu$a;->k:Ljava/lang/String;

    .line 152
    return-object p0
.end method

.method protected e()Lcdu;
    .locals 2

    .prologue
    .line 86
    new-instance v0, Lcdu;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcdu;-><init>(Lcdu$a;Lcdu$1;)V

    return-object v0
.end method

.method public f(J)Lcdu$a;
    .locals 1

    .prologue
    .line 157
    iput-wide p1, p0, Lcdu$a;->l:J

    .line 158
    return-object p0
.end method

.method public g(J)Lcdu$a;
    .locals 1

    .prologue
    .line 163
    iput-wide p1, p0, Lcdu$a;->m:J

    .line 164
    return-object p0
.end method

.method public h(J)Lcdu$a;
    .locals 1

    .prologue
    .line 169
    iput-wide p1, p0, Lcdu$a;->n:J

    .line 170
    return-object p0
.end method
