.class public Lahx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/RadioGroup;

.field private final e:Landroid/widget/RadioGroup;

.field private final f:Landroid/widget/RadioButton;

.field private final g:Landroid/widget/RadioButton;

.field private final h:Landroid/widget/RadioButton;

.field private final i:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 3

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const v0, 0x7f04039c

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lahx;->a:Landroid/view/View;

    .line 33
    iget-object v0, p0, Lahx;->a:Landroid/view/View;

    const v1, 0x7f1307ab

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lahx;->b:Landroid/view/View;

    .line 34
    iget-object v0, p0, Lahx;->a:Landroid/view/View;

    const v1, 0x7f1307dd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lahx;->c:Landroid/view/View;

    .line 35
    iget-object v0, p0, Lahx;->a:Landroid/view/View;

    const v1, 0x7f1307de

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lahx;->d:Landroid/widget/RadioGroup;

    .line 36
    iget-object v0, p0, Lahx;->a:Landroid/view/View;

    const v1, 0x7f1307df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lahx;->e:Landroid/widget/RadioGroup;

    .line 37
    iget-object v0, p0, Lahx;->a:Landroid/view/View;

    const v1, 0x7f1307c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lahx;->f:Landroid/widget/RadioButton;

    .line 38
    iget-object v0, p0, Lahx;->a:Landroid/view/View;

    const v1, 0x7f1307c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lahx;->g:Landroid/widget/RadioButton;

    .line 39
    iget-object v0, p0, Lahx;->a:Landroid/view/View;

    const v1, 0x7f1307e0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lahx;->h:Landroid/widget/RadioButton;

    .line 40
    iget-object v0, p0, Lahx;->a:Landroid/view/View;

    const v1, 0x7f1307e1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lahx;->i:Landroid/widget/RadioButton;

    .line 41
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lahx;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    return-void
.end method

.method public a(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lahx;->d:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 53
    iget-object v0, p0, Lahx;->e:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 54
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 63
    iget-object v1, p0, Lahx;->f:Landroid/widget/RadioButton;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 64
    iget-object v0, p0, Lahx;->g:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 65
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lahx;->a:Landroid/view/View;

    return-object v0
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lahx;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 68
    iget-object v1, p0, Lahx;->h:Landroid/widget/RadioButton;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 69
    iget-object v0, p0, Lahx;->i:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 70
    return-void

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
