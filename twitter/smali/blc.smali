.class public Lblc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/playback/AVMediaPlayer$a;


# instance fields
.field final a:Lcom/twitter/model/av/AVMedia;

.field volatile b:J

.field final c:Lcom/twitter/library/av/playback/AVPlayer;

.field private final d:Lcom/twitter/library/av/playback/o;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lblc;->c:Lcom/twitter/library/av/playback/AVPlayer;

    .line 37
    iput-object p2, p0, Lblc;->a:Lcom/twitter/model/av/AVMedia;

    .line 39
    sget-object v0, Lcom/twitter/library/av/playback/o;->a:Lcom/twitter/library/av/playback/o;

    iput-object v0, p0, Lblc;->d:Lcom/twitter/library/av/playback/o;

    .line 40
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lblc;->c:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v0

    new-instance v1, Lbkc;

    iget-object v2, p0, Lblc;->a:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v1, v2, p1}, Lbkc;-><init>(Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V

    invoke-virtual {v0, v1}, Lbix;->a(Lbiw;)V

    .line 53
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Lblc;->c:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-wide v0, p0, Lblc;->b:J

    const-wide/16 v2, 0xa

    add-long/2addr v0, v2

    iput-wide v0, p0, Lblc;->b:J

    .line 46
    iget-object v0, p0, Lblc;->c:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v0

    new-instance v1, Lbki;

    iget-object v2, p0, Lblc;->a:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v1, v2, p1}, Lbki;-><init>(Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/playback/aa;)V

    invoke-virtual {v0, v1}, Lbix;->a(Lbiw;)V

    .line 48
    :cond_0
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public c()Lcom/twitter/library/av/playback/o;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lblc;->d:Lcom/twitter/library/av/playback/o;

    return-object v0
.end method
