.class public Lacc;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/al;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/view/ViewGroup;

.field private final g:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lacc;->a:Landroid/view/ViewGroup;

    .line 44
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/al;

    invoke-direct {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/al;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lacc;->b:Lcom/twitter/android/moments/ui/fullscreen/al;

    .line 45
    const v0, 0x7f130300

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lacc;->c:Landroid/view/View;

    .line 46
    const v0, 0x7f130551

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lacc;->d:Landroid/view/View;

    .line 47
    const v0, 0x7f130277

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lacc;->e:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f1304c3

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lacc;->f:Landroid/view/ViewGroup;

    .line 49
    const v0, 0x7f1304c4

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lacc;->g:Landroid/widget/TextView;

    .line 50
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;)Lacc;
    .locals 3

    .prologue
    .line 38
    const v0, 0x7f0401f4

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 39
    new-instance v1, Lacc;

    invoke-direct {v1, v0}, Lacc;-><init>(Landroid/view/ViewGroup;)V

    return-object v1
.end method


# virtual methods
.method public a()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lacc;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lacc;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    return-void
.end method

.method public a(Lcom/twitter/model/moments/w;)V
    .locals 2

    .prologue
    .line 57
    iget v0, p1, Lcom/twitter/model/moments/w;->c:I

    .line 58
    iget-object v1, p0, Lacc;->b:Lcom/twitter/android/moments/ui/fullscreen/al;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/fullscreen/al;->e()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 59
    iget-object v1, p0, Lacc;->b:Lcom/twitter/android/moments/ui/fullscreen/al;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/fullscreen/al;->f()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 62
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-static {v0, v1}, Lcom/twitter/util/ui/g;->g(IF)I

    move-result v0

    .line 63
    iget-object v1, p0, Lacc;->b:Lcom/twitter/android/moments/ui/fullscreen/al;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/fullscreen/al;->g()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 64
    iget-object v1, p0, Lacc;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 65
    iget-object v0, p0, Lacc;->a:Landroid/view/ViewGroup;

    iget v1, p1, Lcom/twitter/model/moments/w;->b:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 66
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lacc;->b:Lcom/twitter/android/moments/ui/fullscreen/al;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/al;->e()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lacc;->e:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/twitter/util/ui/j;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 90
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/twitter/media/request/a$b;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lacc;->b:Lcom/twitter/android/moments/ui/fullscreen/al;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/al;->a()Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;Lcom/twitter/media/request/a$b;)Z

    .line 86
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 53
    iget-object v1, p0, Lacc;->c:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 54
    return-void

    .line 53
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public b()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lacc;->b:Lcom/twitter/android/moments/ui/fullscreen/al;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/al;->e()Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lacc;->b:Lcom/twitter/android/moments/ui/fullscreen/al;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/al;->a()Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v0, p0, Lacc;->b:Lcom/twitter/android/moments/ui/fullscreen/al;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/al;->f()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object v0, p0, Lacc;->b:Lcom/twitter/android/moments/ui/fullscreen/al;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/al;->g()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lacc;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lacc;->b:Lcom/twitter/android/moments/ui/fullscreen/al;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/al;->f()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lacc;->f:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lacc;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    return-void
.end method

.method public b(Z)V
    .locals 3

    .prologue
    const/16 v2, 0xc8

    .line 109
    if-eqz p1, :cond_0

    .line 110
    iget-object v0, p0, Lacc;->d:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/twitter/util/e;->b(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    .line 115
    :goto_0
    return-void

    .line 112
    :cond_0
    invoke-static {}, Lcom/twitter/ui/anim/h;->b()Landroid/view/animation/Interpolator;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lacc;->d:Landroid/view/View;

    invoke-static {v1, v2, v0}, Lcom/twitter/util/e;->a(Landroid/view/View;ILandroid/view/animation/Interpolator;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lacc;->b:Lcom/twitter/android/moments/ui/fullscreen/al;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/al;->g()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    return-void
.end method
