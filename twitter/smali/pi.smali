.class public Lpi;
.super Lbiy;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpi$a;
    }
.end annotation


# instance fields
.field a:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field b:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private c:Lpi$a;


# direct methods
.method public constructor <init>(Lpi$a;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lbiy;-><init>()V

    .line 21
    iput-boolean v0, p0, Lpi;->a:Z

    .line 23
    iput-boolean v0, p0, Lpi;->b:Z

    .line 29
    iput-object p1, p0, Lpi;->c:Lpi$a;

    .line 30
    return-void
.end method


# virtual methods
.method public a(Lbiw;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lpi;->a:Z

    if-nez v0, :cond_0

    instance-of v0, p1, Lbja;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processTick(Lbki;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p1, Lbki;->a:Lcom/twitter/model/av/AVMedia;

    invoke-static {v0}, Lcom/twitter/model/av/b;->a(Lcom/twitter/model/av/AVMedia;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpi;->b:Z

    .line 51
    :cond_0
    return-void
.end method

.method public processViewThreshold(Lbkk;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbkk;
        c = true
    .end annotation

    .prologue
    .line 34
    iget-object v0, p1, Lbkk;->a:Lcom/twitter/model/av/AVMedia;

    invoke-static {v0}, Lcom/twitter/model/av/b;->a(Lcom/twitter/model/av/AVMedia;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    iget-boolean v0, p0, Lpi;->b:Z

    if-eqz v0, :cond_1

    .line 37
    iget-object v0, p0, Lpi;->c:Lpi$a;

    invoke-interface {v0}, Lpi$a;->b()V

    .line 42
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpi;->a:Z

    .line 44
    :cond_0
    return-void

    .line 40
    :cond_1
    iget-object v0, p0, Lpi;->c:Lpi$a;

    invoke-interface {v0}, Lpi$a;->a()V

    goto :goto_0
.end method
