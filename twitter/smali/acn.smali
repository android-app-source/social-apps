.class public Lacn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lacm;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lacn$a;
    }
.end annotation


# instance fields
.field private final a:Lcnw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcnw",
            "<",
            "Lacn$a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lakr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakr",
            "<",
            "Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lzp;

.field private e:Lrx/j;


# direct methods
.method public constructor <init>(Lcnw;Lrx/c;Lakr;Lzp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcnw",
            "<",
            "Lacn$a;",
            ">;",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/h;",
            ">;>;",
            "Lakr",
            "<",
            "Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;",
            ">;",
            "Lzp;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lacn;->a:Lcnw;

    .line 39
    iput-object p2, p0, Lacn;->c:Lrx/c;

    .line 40
    iput-object p3, p0, Lacn;->b:Lakr;

    .line 41
    iput-object p4, p0, Lacn;->d:Lzp;

    .line 42
    return-void
.end method

.method static synthetic a(Lacn;)Lcnw;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lacn;->a:Lcnw;

    return-object v0
.end method

.method static synthetic b(Lacn;)Lakr;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lacn;->b:Lakr;

    return-object v0
.end method

.method static synthetic c(Lacn;)Lzp;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lacn;->d:Lzp;

    return-object v0
.end method

.method static synthetic d()Lcpp;
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lacn;->e()Lcpp;

    move-result-object v0

    return-object v0
.end method

.method private static e()Lcpp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcpp",
            "<",
            "Lcom/twitter/model/moments/h;",
            "Lacn$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lacn$1;

    invoke-direct {v0}, Lacn$1;-><init>()V

    return-object v0
.end method

.method private static f()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/h;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lacn$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, Lacn$2;

    invoke-direct {v0}, Lacn$2;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 68
    invoke-virtual {p0}, Lacn;->c()V

    .line 69
    iget-object v0, p0, Lacn;->c:Lrx/c;

    const/4 v1, 0x1

    .line 70
    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    .line 71
    invoke-static {}, Lacn;->f()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lacn$3;

    invoke-direct {v1, p0}, Lacn$3;-><init>(Lacn;)V

    .line 72
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lacn;->e:Lrx/j;

    .line 87
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lacn;->a:Lcnw;

    invoke-interface {v0}, Lcnw;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lacn;->e:Lrx/j;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lacn;->e:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lacn;->e:Lrx/j;

    .line 95
    :cond_0
    return-void
.end method
