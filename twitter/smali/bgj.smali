.class public Lbgj;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/provider/t;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p2, p3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    iput-object v0, p0, Lbgj;->a:Lcom/twitter/library/provider/t;

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbgj;->b:Landroid/content/Context;

    .line 36
    return-void
.end method


# virtual methods
.method public a(Lcgs;Lcgx;Lchc;Lchx$b;)Lcgz;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 45
    iget-object v0, p4, Lchx$b;->i:Lcgu;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgu;

    .line 46
    iget-object v1, p1, Lcgs;->a:Lchg;

    iget-object v2, v0, Lcgu;->a:Ljava/lang/String;

    iget-wide v4, v0, Lcgu;->b:J

    .line 47
    invoke-virtual {v1, v2, v4, v5}, Lchg;->a(Ljava/lang/String;J)Lchg;

    move-result-object v0

    .line 48
    invoke-virtual {v0, p2, p3}, Lchg;->a(Lcgx;Lchc;)Lcom/twitter/model/timeline/x;

    move-result-object v0

    .line 49
    new-instance v1, Laut;

    iget-object v2, p0, Lbgj;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 50
    iget-object v2, p4, Lchx$b;->d:Ljava/lang/String;

    .line 51
    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 52
    iget-object v3, p0, Lbgj;->a:Lcom/twitter/library/provider/t;

    .line 53
    invoke-static {v0}, Lcom/twitter/library/provider/s$a;->a(Ljava/util/List;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    iget-wide v4, p4, Lchx$b;->b:J

    .line 54
    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/s$a;->a(J)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    iget v4, p4, Lchx$b;->c:I

    .line 55
    invoke-virtual {v0, v4}, Lcom/twitter/library/provider/s$a;->a(I)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 56
    invoke-virtual {v0, v2}, Lcom/twitter/library/provider/s$a;->a(Ljava/lang/String;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 57
    invoke-virtual {v0, v6}, Lcom/twitter/library/provider/s$a;->d(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 58
    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/s$a;->a(Laut;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    iget-boolean v2, p4, Lchx$b;->h:Z

    .line 59
    invoke-virtual {v0, v2}, Lcom/twitter/library/provider/s$a;->e(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 60
    invoke-virtual {v0, v6}, Lcom/twitter/library/provider/s$a;->f(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/twitter/library/provider/s$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/s;

    .line 52
    invoke-virtual {v3, v0}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/library/provider/s;)I

    move-result v0

    .line 62
    if-lez v0, :cond_0

    .line 63
    invoke-virtual {v1}, Laut;->a()V

    .line 65
    :cond_0
    new-instance v1, Lcgs$a;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcgs$a;-><init>(II)V

    return-object v1
.end method
