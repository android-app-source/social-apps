.class public Lbyr;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lbyr;->a:[B

    return-void

    :array_0
    .array-data 1
        0x21t
        -0x7t
        0x4t
        0x0t
        0xat
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public static a(Lcom/twitter/media/model/MediaFile;Ljava/lang/String;)Lbyu;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 35
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 37
    instance-of v0, p0, Lcom/twitter/media/model/ImageFile;

    if-eqz v0, :cond_1

    .line 38
    iget-object v0, p0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    invoke-static {v0}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/lang/String;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/decoder/ImageDecoder;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 39
    if-nez v1, :cond_0

    move-object v0, v2

    .line 89
    :goto_0
    return-object v0

    .line 42
    :cond_0
    new-instance v0, Lbyv;

    check-cast p0, Lcom/twitter/media/model/ImageFile;

    invoke-direct {v0, p0, v1}, Lbyv;-><init>(Lcom/twitter/media/model/ImageFile;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_1
    move-object v0, p0

    .line 45
    check-cast v0, Lcom/twitter/media/model/AnimatedGifFile;

    .line 46
    new-instance v4, Lbys;

    invoke-direct {v4, v0}, Lbys;-><init>(Lcom/twitter/media/model/AnimatedGifFile;)V

    .line 47
    iget-object v1, v4, Lbys;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x1

    if-gt v1, v3, :cond_3

    .line 48
    iget-object v1, p0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    invoke-static {v1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/lang/String;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/media/decoder/ImageDecoder;->b()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 49
    if-nez v3, :cond_2

    move-object v0, v2

    .line 50
    goto :goto_0

    .line 52
    :cond_2
    new-instance v1, Lbyv;

    invoke-static {v0}, Lcom/twitter/media/model/ImageFile;->a(Lcom/twitter/media/model/AnimatedGifFile;)Lcom/twitter/media/model/ImageFile;

    move-result-object v0

    invoke-direct {v1, v0, v3}, Lbyv;-><init>(Lcom/twitter/media/model/ImageFile;Landroid/graphics/Bitmap;)V

    move-object v0, v1

    goto :goto_0

    .line 56
    :cond_3
    iget-boolean v1, v4, Lbys;->c:Z

    if-eqz v1, :cond_8

    .line 58
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v1

    invoke-virtual {v1}, Lcqq;->c()Lcqr;

    move-result-object v1

    sget-object v3, Lcom/twitter/media/model/MediaType;->c:Lcom/twitter/media/model/MediaType;

    iget-object v3, v3, Lcom/twitter/media/model/MediaType;->extension:Ljava/lang/String;

    invoke-interface {v1, v3}, Lcqr;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 59
    if-nez v3, :cond_4

    move-object v0, v2

    .line 60
    goto :goto_0

    .line 63
    :cond_4
    :try_start_0
    iget-object v1, p0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    invoke-static {v4, v1, v3}, Lbyr;->a(Lbys;Ljava/io/File;Ljava/io/File;)V

    .line 64
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Movie;->decodeFile(Ljava/lang/String;)Landroid/graphics/Movie;

    move-result-object v1

    .line 65
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/graphics/Movie;->duration()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_6

    .line 69
    :cond_5
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-object v0, v2

    .line 66
    goto :goto_0

    .line 69
    :cond_6
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 89
    :cond_7
    :goto_1
    new-instance v2, Lbyt;

    invoke-direct {v2, v0, v4, v1}, Lbyt;-><init>(Lcom/twitter/media/model/AnimatedGifFile;Lbys;Landroid/graphics/Movie;)V

    move-object v0, v2

    goto :goto_0

    .line 69
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    throw v0

    .line 72
    :cond_8
    iget-object v1, p0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Movie;->decodeFile(Ljava/lang/String;)Landroid/graphics/Movie;

    move-result-object v1

    .line 73
    if-nez v1, :cond_9

    move-object v0, v2

    .line 74
    goto/16 :goto_0

    .line 76
    :cond_9
    invoke-virtual {v1}, Landroid/graphics/Movie;->duration()I

    move-result v3

    iget v5, v4, Lbys;->b:I

    if-eq v3, v5, :cond_7

    .line 79
    :try_start_1
    new-instance v3, Lbyq;

    iget-object v1, v4, Lbys;->a:Ljava/util/List;

    invoke-direct {v3, v0, v1}, Lbyq;-><init>(Lcom/twitter/media/model/AnimatedGifFile;Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 80
    :try_start_2
    invoke-static {v3}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v1

    .line 81
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Landroid/graphics/Movie;->duration()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v5

    if-nez v5, :cond_b

    .line 85
    :cond_a
    invoke-static {v3}, Lcqc;->a(Ljava/io/Closeable;)V

    move-object v0, v2

    .line 82
    goto/16 :goto_0

    .line 85
    :cond_b
    invoke-static {v3}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_1
    move-exception v0

    :goto_2
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_2
.end method

.method private static a(Lbys;Ljava/io/File;Ljava/io/File;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 97
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 99
    const/16 v0, 0x1000

    :try_start_0
    new-array v4, v0, [B

    .line 100
    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Lbys;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbys$a;

    .line 102
    iget-boolean v6, v0, Lbys$a;->e:Z

    if-eqz v6, :cond_1

    .line 103
    iget v6, v0, Lbys$a;->d:I

    sub-int v1, v6, v1

    invoke-static {v2, v3, v4, v1}, Lbyr;->a(Ljava/io/InputStream;Ljava/io/OutputStream;[BI)V

    .line 104
    iget v0, v0, Lbys$a;->d:I

    .line 105
    sget-object v1, Lbyr;->a:[B

    invoke-virtual {v3, v1}, Ljava/io/OutputStream;->write([B)V

    :goto_1
    move v1, v0

    .line 107
    goto :goto_0

    .line 108
    :cond_0
    const/16 v0, 0x1000

    invoke-static {v2, v3, v0}, Lcqc;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    invoke-static {v3}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 111
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 113
    return-void

    .line 110
    :catchall_0
    move-exception v0

    invoke-static {v3}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 111
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static a(Ljava/io/InputStream;Ljava/io/OutputStream;[BI)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 119
    array-length v0, p2

    .line 120
    :goto_0
    if-lez p3, :cond_0

    .line 121
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p0, p2, v2, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 122
    invoke-virtual {p1, p2, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 123
    sub-int/2addr p3, v1

    .line 124
    goto :goto_0

    .line 125
    :cond_0
    return-void
.end method
