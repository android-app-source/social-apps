.class public Lpj;
.super Lbjb;
.source "Twttr"


# instance fields
.field protected a:J

.field protected b:Z

.field c:Ljava/lang/String;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field d:J
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field e:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field f:J
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field g:J
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final l:Lcom/twitter/library/av/model/a;

.field private final m:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

.field private final n:Ltv/periscope/android/api/ApiManager;

.field private final o:Lpg;

.field private final p:Lde/greenrobot/event/c;

.field private q:Z

.field private r:Z


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 8

    .prologue
    .line 73
    new-instance v3, Lcom/twitter/library/av/model/a;

    invoke-direct {v3}, Lcom/twitter/library/av/model/a;-><init>()V

    .line 74
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayer;->e()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/PeriscopeDataSource;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/PeriscopeDataSource;->q()Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    move-result-object v4

    .line 75
    invoke-static {}, Ltv/periscope/android/library/d;->a()Ltv/periscope/android/library/d;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/library/d;->c()Ltv/periscope/android/library/c;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/library/c;->d()Ltv/periscope/android/api/ApiManager;

    move-result-object v5

    .line 76
    invoke-static {}, Ltv/periscope/android/library/d;->a()Ltv/periscope/android/library/d;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/library/d;->c()Ltv/periscope/android/library/c;

    move-result-object v0

    invoke-interface {v0}, Ltv/periscope/android/library/c;->c()Lde/greenrobot/event/c;

    move-result-object v6

    new-instance v7, Lpg;

    invoke-direct {v7}, Lpg;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 73
    invoke-direct/range {v0 .. v7}, Lpj;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/model/a;Lcom/twitter/library/api/periscope/PeriscopeCapiModel;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Lpg;)V

    .line 77
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/model/a;Lcom/twitter/library/api/periscope/PeriscopeCapiModel;Ltv/periscope/android/api/ApiManager;Lde/greenrobot/event/c;Lpg;)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 41
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lpj;->a:J

    .line 85
    iput-object p3, p0, Lpj;->l:Lcom/twitter/library/av/model/a;

    .line 86
    iput-object p4, p0, Lpj;->m:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    .line 87
    iput-object p5, p0, Lpj;->n:Ltv/periscope/android/api/ApiManager;

    .line 88
    iput-object p6, p0, Lpj;->p:Lde/greenrobot/event/c;

    .line 89
    iget-object v0, p0, Lpj;->p:Lde/greenrobot/event/c;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->a(Ljava/lang/Object;)V

    .line 90
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayer;->i()Z

    move-result v0

    iput-boolean v0, p0, Lpj;->q:Z

    .line 91
    iput-object p7, p0, Lpj;->o:Lpg;

    .line 92
    return-void
.end method

.method private a(J)J
    .locals 5

    .prologue
    .line 139
    iget-wide v0, p0, Lpj;->a:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 140
    const-wide/16 v0, 0x0

    .line 142
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lpj;->a:J

    sub-long v0, p1, v0

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 184
    iget-object v0, p0, Lpj;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 185
    iget-object v1, p0, Lpj;->n:Ltv/periscope/android/api/ApiManager;

    iget-object v2, p0, Lpj;->c:Ljava/lang/String;

    move-wide v6, v4

    invoke-interface/range {v1 .. v7}, Ltv/periscope/android/api/ApiManager;->endWatching(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;

    .line 191
    :goto_0
    iput-object v3, p0, Lpj;->c:Ljava/lang/String;

    .line 192
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lpj;->a:J

    .line 193
    iput-wide v4, p0, Lpj;->d:J

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lpj;->b:Z

    .line 195
    return-void

    .line 189
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpj;->r:Z

    goto :goto_0
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 147
    sget-object v0, Lpj$1;->a:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 162
    :goto_0
    iget-boolean v0, p0, Lpj;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lpj;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 163
    iget-object v1, p0, Lpj;->n:Ltv/periscope/android/api/ApiManager;

    iget-object v2, p0, Lpj;->c:Ljava/lang/String;

    move-wide v6, v4

    invoke-interface/range {v1 .. v7}, Ltv/periscope/android/api/ApiManager;->endWatching(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;

    .line 164
    iput-boolean v8, p0, Lpj;->r:Z

    .line 165
    iput-object v3, p0, Lpj;->c:Ljava/lang/String;

    .line 166
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lpj;->a:J

    .line 167
    iput-wide v4, p0, Lpj;->d:J

    .line 168
    iput-boolean v8, p0, Lpj;->b:Z

    .line 170
    :cond_0
    return-void

    .line 149
    :pswitch_0
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/api/StartWatchingResponse;

    .line 151
    iget-object v0, v0, Ltv/periscope/android/api/StartWatchingResponse;->session:Ljava/lang/String;

    iput-object v0, p0, Lpj;->c:Ljava/lang/String;

    goto :goto_0

    .line 153
    :cond_1
    iput-object v3, p0, Lpj;->c:Ljava/lang/String;

    goto :goto_0

    .line 147
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public processMediaReleaseEvent(Lbjh;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbjh;
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lpj;->p:Lde/greenrobot/event/c;

    invoke-virtual {v0, p0}, Lde/greenrobot/event/c;->c(Ljava/lang/Object;)V

    .line 175
    invoke-virtual {p0}, Lpj;->a()V

    .line 176
    return-void
.end method

.method public processPlaybackFinishedEvent(Lbkb;)V
    .locals 0
    .annotation runtime Lbiz;
        a = Lbkb;
    .end annotation

    .prologue
    .line 180
    invoke-virtual {p0}, Lpj;->a()V

    .line 181
    return-void
.end method

.method public processTickEvent(Lbki;)V
    .locals 9
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    .line 96
    invoke-virtual {p0}, Lpj;->b()Lcom/twitter/library/av/m;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lpj;->o:Lpg;

    invoke-virtual {v1}, Lpg;->a()V

    .line 99
    iget-object v1, p0, Lpj;->o:Lpg;

    invoke-virtual {v1}, Lpg;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lpj;->g:J

    .line 101
    iget-boolean v1, p0, Lpj;->e:Z

    if-nez v1, :cond_0

    .line 106
    iput-boolean v8, p0, Lpj;->e:Z

    .line 107
    iget-object v1, p0, Lpj;->l:Lcom/twitter/library/av/model/a;

    iget-object v2, p0, Lpj;->k:Lcom/twitter/model/av/AVMedia;

    iget-object v3, p1, Lbki;->b:Lcom/twitter/library/av/playback/aa;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/av/model/a;->a(Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/playback/aa;)J

    move-result-wide v2

    iput-wide v2, p0, Lpj;->f:J

    .line 110
    :cond_0
    iget-boolean v1, p0, Lpj;->q:Z

    .line 111
    iget-object v0, v0, Lcom/twitter/library/av/m;->o:Lbyf;

    invoke-interface {v0}, Lbyf;->e()Z

    move-result v0

    iput-boolean v0, p0, Lpj;->q:Z

    .line 113
    iget-boolean v0, p0, Lpj;->q:Z

    if-eqz v0, :cond_2

    .line 117
    if-nez v1, :cond_1

    .line 119
    invoke-virtual {p0}, Lpj;->a()V

    .line 136
    :cond_1
    :goto_0
    return-void

    .line 122
    :cond_2
    iget-wide v0, p0, Lpj;->a:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 123
    iget-wide v0, p0, Lpj;->g:J

    iput-wide v0, p0, Lpj;->a:J

    .line 126
    :cond_3
    iget-wide v0, p0, Lpj;->g:J

    invoke-direct {p0, v0, v1}, Lpj;->a(J)J

    move-result-wide v0

    iget-wide v2, p0, Lpj;->f:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 127
    iget-object v0, p0, Lpj;->m:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v0}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->g()Ljava/lang/String;

    move-result-object v0

    .line 128
    iget-object v1, p0, Lpj;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-wide v2, p0, Lpj;->d:J

    const-wide/16 v6, 0x7530

    add-long/2addr v2, v6

    iget-wide v6, p0, Lpj;->g:J

    cmp-long v1, v2, v6

    if-gtz v1, :cond_4

    .line 129
    iget-object v1, p0, Lpj;->n:Ltv/periscope/android/api/ApiManager;

    iget-object v2, p0, Lpj;->c:Ljava/lang/String;

    const/4 v3, 0x0

    move-wide v6, v4

    invoke-interface/range {v1 .. v7}, Ltv/periscope/android/api/ApiManager;->pingWatching(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;

    .line 130
    iget-wide v0, p0, Lpj;->g:J

    iput-wide v0, p0, Lpj;->d:J

    goto :goto_0

    .line 131
    :cond_4
    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lpj;->b:Z

    if-nez v1, :cond_1

    .line 132
    iget-object v1, p0, Lpj;->n:Ltv/periscope/android/api/ApiManager;

    invoke-interface {v1, v0, v8}, Ltv/periscope/android/api/ApiManager;->startWatching(Ljava/lang/String;Z)Ljava/lang/String;

    .line 133
    iput-boolean v8, p0, Lpj;->b:Z

    goto :goto_0
.end method
