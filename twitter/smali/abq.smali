.class public Labq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/cu;


# instance fields
.field private final b:Landroid/widget/FrameLayout;

.field private final c:Lcom/twitter/android/widget/q;

.field private final d:Lcom/twitter/android/moments/ui/fullscreen/z;

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/View;

.field private final g:Landroid/view/View;

.field private final h:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/view/ViewGroup;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/view/View;

.field private final l:Landroid/widget/TextView;

.field private final m:Lbpl;


# direct methods
.method constructor <init>(Landroid/widget/FrameLayout;Lcom/twitter/android/widget/q;Lcom/twitter/android/moments/ui/fullscreen/z;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/TextView;Lbpl;Landroid/view/View;Landroid/widget/TextView;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Labq;->b:Landroid/widget/FrameLayout;

    .line 77
    iput-object p2, p0, Labq;->c:Lcom/twitter/android/widget/q;

    .line 78
    iput-object p3, p0, Labq;->d:Lcom/twitter/android/moments/ui/fullscreen/z;

    .line 79
    iput-object p4, p0, Labq;->e:Landroid/view/View;

    .line 80
    iput-object p5, p0, Labq;->f:Landroid/view/View;

    .line 81
    iput-object p6, p0, Labq;->g:Landroid/view/View;

    .line 82
    iput-object p7, p0, Labq;->i:Landroid/view/ViewGroup;

    .line 83
    iput-object p8, p0, Labq;->j:Landroid/widget/TextView;

    .line 84
    iput-object p9, p0, Labq;->m:Lbpl;

    .line 85
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Labq;->h:Lrx/subjects/PublishSubject;

    .line 86
    iput-object p10, p0, Labq;->k:Landroid/view/View;

    .line 87
    iput-object p11, p0, Labq;->l:Landroid/widget/TextView;

    .line 88
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;)Labq;
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 44
    const v0, 0x7f0401f7

    .line 45
    invoke-virtual {p0, v0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 46
    new-instance v2, Lcom/twitter/android/widget/q;

    invoke-direct {v2, v1}, Lcom/twitter/android/widget/q;-><init>(Landroid/view/ViewGroup;)V

    .line 47
    const v0, 0x7f130519

    .line 48
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 49
    const v3, 0x7f13041b

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 51
    invoke-static {}, Lbsd;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v9, Lbpl;

    invoke-direct {v9, v0}, Lbpl;-><init>(Landroid/widget/FrameLayout;)V

    .line 53
    :cond_0
    new-instance v3, Lcom/twitter/android/moments/ui/fullscreen/cw;

    .line 54
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v3, v5, v0, v4}, Lcom/twitter/android/moments/ui/fullscreen/cw;-><init>(Landroid/content/res/Resources;Landroid/view/View;Landroid/view/View;)V

    .line 56
    const v0, 0x7f130300

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 57
    const v0, 0x7f13051b

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 58
    const v0, 0x7f1304f1

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 59
    const v0, 0x7f130518

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 60
    const v0, 0x7f130277

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 61
    const v0, 0x7f1304c3

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 62
    const v0, 0x7f1304c4

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 63
    new-instance v0, Labq;

    invoke-direct/range {v0 .. v11}, Labq;-><init>(Landroid/widget/FrameLayout;Lcom/twitter/android/widget/q;Lcom/twitter/android/moments/ui/fullscreen/z;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/TextView;Lbpl;Landroid/view/View;Landroid/widget/TextView;)V

    return-object v0
.end method


# virtual methods
.method public a()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Labq;->c:Lcom/twitter/android/widget/q;

    invoke-virtual {v0}, Lcom/twitter/android/widget/q;->e()Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 175
    iget-object v0, p0, Labq;->b:Landroid/widget/FrameLayout;

    const v1, 0x7f13051a

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 176
    invoke-virtual {v0, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 177
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 178
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Labq;->f:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Labq;->c:Lcom/twitter/android/widget/q;

    invoke-virtual {v0}, Lcom/twitter/android/widget/q;->e()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/util/ui/j;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 107
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Labq;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Labq;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 118
    iget-object v1, p0, Labq;->e:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 119
    return-void

    .line 118
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public b()Lcom/twitter/android/moments/ui/fullscreen/z;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Labq;->d:Lcom/twitter/android/moments/ui/fullscreen/z;

    return-object v0
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Labq;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Labq;->c:Lcom/twitter/android/widget/q;

    invoke-virtual {v0}, Lcom/twitter/android/widget/q;->f()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    return-void
.end method

.method public c()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Labq;->i:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Labq;->c:Lcom/twitter/android/widget/q;

    invoke-virtual {v0}, Lcom/twitter/android/widget/q;->g()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    return-void
.end method

.method public d()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Labq;->b:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public d(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Labq;->j:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/twitter/util/ui/j;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 127
    return-void
.end method

.method public e()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Labq;->g:Landroid/view/View;

    invoke-static {v0}, Lcrf;->a(Landroid/view/View;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Labq;->g:Landroid/view/View;

    .line 151
    invoke-virtual {v0, v1}, Lrx/c;->d(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Labq;->h:Lrx/subjects/PublishSubject;

    .line 152
    invoke-virtual {v0, v1}, Lrx/c;->g(Lrx/c;)Lrx/c;

    move-result-object v0

    new-instance v1, Labq$1;

    invoke-direct {v1, p0}, Labq$1;-><init>(Labq;)V

    .line 153
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 171
    invoke-virtual {v0}, Lrx/c;->j()Lrx/c;

    move-result-object v0

    .line 150
    return-object v0
.end method

.method public f()Lbpl;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Labq;->m:Lbpl;

    return-object v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Labq;->h:Lrx/subjects/PublishSubject;

    iget-object v1, p0, Labq;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 145
    return-void
.end method
