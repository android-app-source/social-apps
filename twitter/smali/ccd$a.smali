.class public final Lccd$a;
.super Lcbx$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lccd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbx$a",
        "<",
        "Lccd;",
        "Lccd$a;",
        ">;"
    }
.end annotation


# instance fields
.field private f:Lcom/twitter/model/geo/b;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcbx$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lccd$a;)Lcom/twitter/model/geo/b;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lccd$a;->f:Lcom/twitter/model/geo/b;

    return-object v0
.end method

.method static synthetic b(Lccd$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lccd$a;->g:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 82
    invoke-super {p0}, Lcbx$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccd$a;->f:Lcom/twitter/model/geo/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/geo/b;)Lccd$a;
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lccd$a;->f:Lcom/twitter/model/geo/b;

    .line 71
    return-object p0
.end method

.method public a(Ljava/util/List;)Lccd$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;)",
            "Lccd$a;"
        }
    .end annotation

    .prologue
    .line 76
    iput-object p1, p0, Lccd$a;->g:Ljava/util/List;

    .line 77
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lccd$a;->e()Lccd;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lccd;
    .locals 2

    .prologue
    .line 88
    new-instance v0, Lccd;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lccd;-><init>(Lccd$a;Lccd$1;)V

    return-object v0
.end method
