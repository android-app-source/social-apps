.class public final Lcdf$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcdf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcdf;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcdd$a;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lccz;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lccv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcdf$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcdf$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcdf$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcdf$a;->c:Ljava/util/Map;

    return-object v0
.end method

.method private b(Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcda;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lccv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v3

    .line 226
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcda;

    .line 227
    iget-object v0, v0, Lcda;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdb;

    .line 228
    iget-object v6, v0, Lcdb;->a:Ljava/lang/String;

    .line 229
    iget-object v2, v0, Lcdb;->b:Ljava/lang/Object;

    .line 230
    iget-object v1, v0, Lcdb;->c:Ljava/util/List;

    .line 232
    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    invoke-virtual {v3, v6}, Lcom/twitter/util/collection/i;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 233
    invoke-virtual {v3, v6}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccv;

    .line 234
    iget-object v7, v0, Lccv;->c:Ljava/lang/Object;

    invoke-static {v2, v7}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 235
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v0, v0, Lccv;->d:Ljava/util/List;

    :goto_1
    move-object v1, v0

    move-object v0, v2

    .line 238
    :goto_2
    new-instance v2, Lccv$a;

    invoke-direct {v2}, Lccv$a;-><init>()V

    .line 240
    invoke-virtual {v2, v6}, Lccv$a;->a(Ljava/lang/String;)Lccv$a;

    move-result-object v2

    .line 241
    invoke-virtual {v2, v0}, Lccv$a;->a(Ljava/lang/Object;)Lccv$a;

    move-result-object v0

    .line 242
    invoke-virtual {v0, v1}, Lccv$a;->a(Ljava/util/List;)Lccv$a;

    move-result-object v0

    .line 243
    invoke-interface {p2, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lccv$a;->a(Z)Lccv$a;

    move-result-object v0

    .line 244
    invoke-virtual {v0}, Lccv$a;->q()Ljava/lang/Object;

    move-result-object v0

    .line 238
    invoke-virtual {v3, v6, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 235
    goto :goto_1

    .line 247
    :cond_3
    invoke-virtual {v3}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0

    :cond_4
    move-object v0, v2

    goto :goto_2
.end method

.method static synthetic c(Lcdf$a;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcdf$a;->d:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic d(Lcdf$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcdf$a;->e:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic e(Lcdf$a;)Lcdd$a;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcdf$a;->a:Lcdd$a;

    return-object v0
.end method


# virtual methods
.method public a(Lcdd$a;)Lcdf$a;
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcdf$a;->a:Lcdd$a;

    .line 174
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcdf$a;
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcdf$a;->b:Ljava/lang/String;

    .line 180
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcdf$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lccz;",
            ">;)",
            "Lcdf$a;"
        }
    .end annotation

    .prologue
    .line 185
    iput-object p1, p0, Lcdf$a;->c:Ljava/util/Map;

    .line 186
    return-object p0
.end method

.method public a(Ljava/util/Map;Ljava/util/Set;)Lcdf$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcda;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcdf$a;"
        }
    .end annotation

    .prologue
    .line 198
    invoke-direct {p0, p1, p2}, Lcdf$a;->b(Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcdf$a;->e:Ljava/util/Map;

    .line 199
    return-object p0
.end method

.method public a(Ljava/util/Set;)Lcdf$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcdf$a;"
        }
    .end annotation

    .prologue
    .line 191
    iput-object p1, p0, Lcdf$a;->d:Ljava/util/Set;

    .line 192
    return-object p0
.end method

.method public b(Ljava/util/Map;)Lcdf$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lccv;",
            ">;)",
            "Lcdf$a;"
        }
    .end annotation

    .prologue
    .line 204
    iput-object p1, p0, Lcdf$a;->e:Ljava/util/Map;

    .line 205
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lcdf$a;->e()Lcdf;

    move-result-object v0

    return-object v0
.end method

.method protected c_()V
    .locals 1

    .prologue
    .line 210
    invoke-super {p0}, Lcom/twitter/util/object/i;->c_()V

    .line 211
    iget-object v0, p0, Lcdf$a;->a:Lcdd$a;

    if-nez v0, :cond_0

    .line 212
    new-instance v0, Lcdd$a;

    invoke-direct {v0}, Lcdd$a;-><init>()V

    iput-object v0, p0, Lcdf$a;->a:Lcdd$a;

    .line 214
    :cond_0
    return-void
.end method

.method protected e()Lcdf;
    .locals 2

    .prologue
    .line 219
    new-instance v0, Lcdf;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcdf;-><init>(Lcdf$a;Lcdf$1;)V

    return-object v0
.end method
