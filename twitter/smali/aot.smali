.class public Laot;
.super Laoq;
.source "Twttr"

# interfaces
.implements Laoz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laoq",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Laoz;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Landroid/support/v4/app/LoaderManager;

.field private final c:Lcom/twitter/util/object/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/j",
            "<",
            "Lcom/twitter/util/android/d;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v4/app/LoaderManager;ILcom/twitter/util/object/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/LoaderManager;",
            "I",
            "Lcom/twitter/util/object/j",
            "<",
            "Lcom/twitter/util/android/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Laoq;-><init>()V

    .line 26
    new-instance v0, Laot$1;

    invoke-direct {v0, p0}, Laot$1;-><init>(Laot;)V

    iput-object v0, p0, Laot;->d:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 47
    iput-object p1, p0, Laot;->b:Landroid/support/v4/app/LoaderManager;

    .line 48
    iput p2, p0, Laot;->a:I

    .line 49
    iput-object p3, p0, Laot;->c:Lcom/twitter/util/object/j;

    .line 50
    return-void
.end method

.method static synthetic a(Laot;)Lcom/twitter/util/object/j;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Laot;->c:Lcom/twitter/util/object/j;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 60
    iget-object v0, p0, Laot;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Laot;->a:I

    const/4 v2, 0x0

    iget-object v3, p0, Laot;->d:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 61
    return-void
.end method

.method public a(Laow;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laow",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-super {p0, p1}, Laoq;->a(Laow;)V

    .line 55
    iget-object v0, p0, Laot;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Laot;->a:I

    const/4 v2, 0x0

    iget-object v3, p0, Laot;->d:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 56
    return-void
.end method
