.class Lcaf$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcaf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcaf;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcaf$1;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcaf$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcaf;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Lcaf$a;

    invoke-direct {v0}, Lcaf$a;-><init>()V

    .line 102
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcaf$a;->a(Ljava/lang/String;)Lcaf$a;

    move-result-object v0

    .line 103
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcaf$a;->a(I)Lcaf$a;

    move-result-object v0

    .line 104
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcaf$a;->b(Ljava/lang/String;)Lcaf$a;

    move-result-object v0

    new-instance v1, Lcad;

    .line 105
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcad;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcaf$a;->a(Lcad;)Lcaf$a;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcaf$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcaf;

    .line 101
    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcaf;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p2, Lcaf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcaf;->c:I

    .line 92
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcaf;->d:Ljava/lang/String;

    .line 93
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v1

    iget-object v0, p2, Lcaf;->e:Lcad;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcaf;->e:Lcad;

    iget-object v0, v0, Lcad;->a:Ljava/lang/String;

    .line 94
    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 95
    return-void

    .line 93
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    check-cast p2, Lcaf;

    invoke-virtual {p0, p1, p2}, Lcaf$b;->a(Lcom/twitter/util/serialization/o;Lcaf;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p0, p1, p2}, Lcaf$b;->a(Lcom/twitter/util/serialization/n;I)Lcaf;

    move-result-object v0

    return-object v0
.end method
