.class public Laje$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laje;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/support/v4/app/FragmentActivity;

.field private b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private c:Lcom/twitter/model/core/Tweet;

.field private d:J


# direct methods
.method constructor <init>(Landroid/support/v4/app/FragmentActivity;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Laje$a;->a:Landroid/support/v4/app/FragmentActivity;

    .line 43
    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;)Laje$a;
    .locals 1

    .prologue
    .line 70
    new-instance v0, Laje$a;

    invoke-direct {v0, p0}, Laje$a;-><init>(Landroid/support/v4/app/FragmentActivity;)V

    return-object v0
.end method

.method static synthetic a(Laje$a;)Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Laje$a;->a:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method static synthetic b(Laje$a;)J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Laje$a;->d:J

    return-wide v0
.end method

.method static synthetic c(Laje$a;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Laje$a;->c:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method static synthetic d(Laje$a;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Laje$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method


# virtual methods
.method public a(J)Laje$a;
    .locals 1

    .prologue
    .line 47
    iput-wide p1, p0, Laje$a;->d:J

    .line 48
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Laje$a;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Laje$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 60
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Laje$a;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Laje$a;->c:Lcom/twitter/model/core/Tweet;

    .line 54
    return-object p0
.end method

.method public a()Lajj;
    .locals 1

    .prologue
    .line 65
    new-instance v0, Laje;

    invoke-direct {v0, p0}, Laje;-><init>(Laje$a;)V

    return-object v0
.end method
