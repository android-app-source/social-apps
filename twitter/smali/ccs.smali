.class public Lccs;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccs$d;,
        Lccs$b;,
        Lccs$c;,
        Lccs$a;
    }
.end annotation


# static fields
.field public static final d:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lccs;",
            "Lccs$c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lccs$d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lccs$d;-><init>(Lccs$1;)V

    sput-object v0, Lccs;->d:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method protected constructor <init>(Lccs$a;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lccs$a;->a(Lccs$a;)Ljava/lang/String;

    move-result-object v0

    .line 37
    iput-object v0, p0, Lccs;->e:Ljava/lang/String;

    .line 38
    invoke-static {p1}, Lccs$a;->b(Lccs$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lccs;->f:Ljava/lang/String;

    .line 39
    return-void
.end method

.method private a(Lccs;)Z
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lccs;->e:Ljava/lang/String;

    iget-object v1, p1, Lccs;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccs;->f:Ljava/lang/String;

    iget-object v1, p1, Lccs;->f:Ljava/lang/String;

    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 47
    :goto_0
    return v0

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 43
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lccs;

    if-eqz v0, :cond_1

    check-cast p1, Lccs;

    invoke-direct {p0, p1}, Lccs;->a(Lccs;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lccs;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 54
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lccs;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    return v0
.end method
