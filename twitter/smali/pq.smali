.class public Lpq;
.super Lbjb;
.source "Twttr"


# instance fields
.field a:Z

.field b:Z

.field private final c:Lcom/twitter/library/av/playback/AVPlayer;

.field private final d:Lpg;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lpg;

    invoke-direct {v0}, Lpg;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lpq;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Lpg;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Lpg;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 24
    iput-boolean v0, p0, Lpq;->a:Z

    .line 25
    iput-boolean v0, p0, Lpq;->b:Z

    .line 39
    iput-object p1, p0, Lpq;->c:Lcom/twitter/library/av/playback/AVPlayer;

    .line 40
    iput-object p3, p0, Lpq;->d:Lpg;

    .line 41
    return-void
.end method


# virtual methods
.method public a(Lbiw;)Z
    .locals 2

    .prologue
    .line 68
    instance-of v0, p1, Lbji;

    .line 69
    invoke-super {p0, p1}, Lbjb;->a(Lbiw;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 70
    iget-boolean v0, p0, Lpq;->a:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 70
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processPlayerReleaseEvent(Lbji;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbji;
    .end annotation

    .prologue
    .line 55
    iget-boolean v0, p0, Lpq;->b:Z

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lpq;->c:Lcom/twitter/library/av/playback/AVPlayer;

    .line 57
    invoke-virtual {p0}, Lpq;->d()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/m;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    iget-object v2, p0, Lpq;->k:Lcom/twitter/model/av/AVMedia;

    .line 58
    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/model/av/AVMedia;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    const-string/jumbo v2, "playback_lapse"

    .line 59
    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    iget-object v2, p0, Lpq;->d:Lpg;

    .line 60
    invoke-virtual {v2}, Lpg;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->b(Ljava/lang/Long;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    .line 61
    invoke-virtual {v1}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v1

    .line 56
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/m;)V

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpq;->a:Z

    .line 64
    :cond_0
    return-void
.end method

.method public processTick(Lbki;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lpq;->d:Lpg;

    invoke-virtual {v0}, Lpg;->a()V

    .line 46
    return-void
.end method

.method public processVideoViewEvent(Lbkj;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbkj;
        b = true
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpq;->b:Z

    .line 51
    return-void
.end method
