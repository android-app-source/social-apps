.class public Lash;
.super Laou;
.source "Twttr"


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Laou;-><init>(Landroid/content/Context;)V

    .line 18
    iput-wide p2, p0, Lash;->a:J

    .line 19
    return-void
.end method

.method public static a(J)Lapb;
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lapb$a;

    invoke-direct {v0}, Lapb$a;-><init>()V

    sget-object v1, Lcom/twitter/database/schema/a$c;->a:Landroid/net/Uri;

    .line 30
    invoke-static {v1, p0, p1}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lapb$a;->a(Landroid/net/Uri;)Lapb$a;

    move-result-object v0

    const-string/jumbo v1, "ads_type IN(\"tweet\",\"carousel\")"

    .line 31
    invoke-virtual {v0, v1}, Lapb$a;->a(Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    const-string/jumbo v1, "_id ASC"

    .line 33
    invoke-virtual {v0, v1}, Lapb$a;->b(Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    sget-object v1, Lbtg;->a:[Ljava/lang/String;

    .line 34
    invoke-virtual {v0, v1}, Lapb$a;->b([Ljava/lang/String;)Lapb$a;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lapb$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapb;

    .line 29
    return-object v0
.end method


# virtual methods
.method protected a()Lapb;
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lash;->a:J

    invoke-static {v0, v1}, Lash;->a(J)Lapb;

    move-result-object v0

    return-object v0
.end method
