.class public final Lcjo$f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcjo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation


# static fields
.field public static final days:I = 0x7f0c0005

.field public static final in_reply_to_name_and_count:I = 0x7f0c000b

.field public static final months:I = 0x7f0c0015

.field public static final social_follow_and_follow_and_others:I = 0x7f0c0027

.field public static final social_like_and_retweets_count:I = 0x7f0c0028

.field public static final social_like_count:I = 0x7f0c0029

.field public static final social_like_count_with_user:I = 0x7f0c002a

.field public static final social_like_count_with_user_accessibility_description:I = 0x7f0c002b

.field public static final social_proof_in_reply_multiple_names_and_count:I = 0x7f0c002c

.field public static final social_retweet_and_likes_count:I = 0x7f0c002d

.field public static final social_retweet_count:I = 0x7f0c002e

.field public static final time_days:I = 0x7f0c0038

.field public static final time_days_ago:I = 0x7f0c0039

.field public static final time_hours:I = 0x7f0c003a

.field public static final time_hours_ago:I = 0x7f0c003b

.field public static final time_mins:I = 0x7f0c003c

.field public static final time_mins_ago:I = 0x7f0c003d

.field public static final time_secs:I = 0x7f0c003e

.field public static final weeks:I = 0x7f0c0042

.field public static final years:I = 0x7f0c0043
