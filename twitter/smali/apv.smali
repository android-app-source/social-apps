.class public abstract Lapv;
.super Lapn;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lapv$b;,
        Lapv$a;,
        Lapv$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<H:",
        "Lapv$c;",
        ">",
        "Lapn",
        "<",
        "Lcom/twitter/model/dms/e;",
        ">;"
    }
.end annotation


# instance fields
.field private final A:Lcom/twitter/library/view/QuoteView;

.field private final B:Lcom/twitter/media/ui/image/MediaImageView;

.field private final C:Landroid/view/View;

.field private final D:Landroid/widget/Button;

.field private final E:Landroid/widget/Button;

.field private final F:Landroid/view/View;

.field private final G:Z

.field private final H:Z

.field private final I:Lcom/twitter/library/network/t;

.field private final J:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final K:Ljava/text/SimpleDateFormat;

.field private final L:Ljava/text/SimpleDateFormat;

.field private final M:Ljava/text/SimpleDateFormat;

.field private final N:Ljava/lang/String;

.field private final O:Lcom/twitter/android/card/d;

.field private final P:Lcdu;

.field private final Q:Lcom/twitter/app/dm/p;

.field private final R:Lcom/twitter/app/dm/o;

.field final k:Landroid/widget/TextView;

.field final l:Landroid/view/View;

.field final m:Lcom/twitter/app/dm/v;

.field final n:Lcne;

.field final o:Lcom/twitter/app/dm/f;

.field final p:Lcom/twitter/android/bn;

.field final q:Z

.field final r:Landroid/view/View;

.field final s:Landroid/view/View;

.field final t:Lcom/twitter/media/ui/image/MediaImageView;

.field final u:Landroid/view/ViewGroup;

.field private final v:Landroid/view/View;

.field private final w:Landroid/view/ViewGroup;

.field private final x:Landroid/view/ViewGroup;

.field private final y:Lcom/twitter/library/av/DMVideoThumbnailView;

.field private final z:Lcom/twitter/android/av/video/VideoContainerHost;


# direct methods
.method protected constructor <init>(Lapv$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lapv$a",
            "<TH;*>;)V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lapn;-><init>(Lapn$a;)V

    .line 143
    invoke-static {p1}, Lapv$a;->a(Lapv$a;)Z

    move-result v0

    iput-boolean v0, p0, Lapv;->q:Z

    .line 144
    invoke-static {p1}, Lapv$a;->b(Lapv$a;)Z

    move-result v0

    iput-boolean v0, p0, Lapv;->G:Z

    .line 145
    invoke-static {p1}, Lapv$a;->c(Lapv$a;)Lcom/twitter/app/dm/v;

    move-result-object v0

    iput-object v0, p0, Lapv;->m:Lcom/twitter/app/dm/v;

    .line 146
    invoke-static {p1}, Lapv$a;->d(Lapv$a;)Lcom/twitter/app/dm/f;

    move-result-object v0

    iput-object v0, p0, Lapv;->o:Lcom/twitter/app/dm/f;

    .line 147
    invoke-static {p1}, Lapv$a;->e(Lapv$a;)Lcne;

    move-result-object v0

    iput-object v0, p0, Lapv;->n:Lcne;

    .line 148
    invoke-static {p1}, Lapv$a;->f(Lapv$a;)Lcom/twitter/library/network/t;

    move-result-object v0

    iput-object v0, p0, Lapv;->I:Lcom/twitter/library/network/t;

    .line 149
    invoke-static {p1}, Lapv$a;->g(Lapv$a;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    iput-object v0, p0, Lapv;->J:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 150
    invoke-static {p1}, Lapv$a;->h(Lapv$a;)Ljava/text/SimpleDateFormat;

    move-result-object v0

    iput-object v0, p0, Lapv;->K:Ljava/text/SimpleDateFormat;

    .line 151
    invoke-static {p1}, Lapv$a;->i(Lapv$a;)Ljava/text/SimpleDateFormat;

    move-result-object v0

    iput-object v0, p0, Lapv;->L:Ljava/text/SimpleDateFormat;

    .line 152
    invoke-static {p1}, Lapv$a;->j(Lapv$a;)Ljava/text/SimpleDateFormat;

    move-result-object v0

    iput-object v0, p0, Lapv;->M:Ljava/text/SimpleDateFormat;

    .line 153
    invoke-static {p1}, Lapv$a;->k(Lapv$a;)Z

    move-result v0

    iput-boolean v0, p0, Lapv;->H:Z

    .line 155
    iget-object v0, p1, Lapv$a;->a:Lapn$b;

    check-cast v0, Lapv$c;

    .line 156
    invoke-static {v0}, Lapv$c;->a(Lapv$c;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lapv;->v:Landroid/view/View;

    .line 157
    iget-object v1, v0, Lapv$c;->a:Landroid/view/ViewGroup;

    iput-object v1, p0, Lapv;->u:Landroid/view/ViewGroup;

    .line 158
    invoke-static {v0}, Lapv$c;->b(Lapv$c;)Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lapv;->w:Landroid/view/ViewGroup;

    .line 160
    invoke-static {v0}, Lapv$c;->c(Lapv$c;)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lapv;->k:Landroid/widget/TextView;

    .line 161
    invoke-static {v0}, Lapv$c;->d(Lapv$c;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lapv;->l:Landroid/view/View;

    .line 162
    invoke-static {v0}, Lapv$c;->e(Lapv$c;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lapv;->s:Landroid/view/View;

    .line 164
    invoke-static {v0}, Lapv$c;->f(Lapv$c;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lapv;->C:Landroid/view/View;

    .line 165
    invoke-static {v0}, Lapv$c;->g(Lapv$c;)Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, Lapv;->D:Landroid/widget/Button;

    .line 166
    invoke-static {v0}, Lapv$c;->h(Lapv$c;)Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, Lapv;->E:Landroid/widget/Button;

    .line 169
    invoke-static {v0}, Lapv$c;->i(Lapv$c;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lapv;->r:Landroid/view/View;

    .line 170
    invoke-static {v0}, Lapv$c;->j(Lapv$c;)Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v1

    iput-object v1, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    .line 171
    invoke-static {v0}, Lapv$c;->k(Lapv$c;)Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v1

    iput-object v1, p0, Lapv;->B:Lcom/twitter/media/ui/image/MediaImageView;

    .line 172
    invoke-static {v0}, Lapv$c;->l(Lapv$c;)Lcom/twitter/library/av/DMVideoThumbnailView;

    move-result-object v1

    iput-object v1, p0, Lapv;->y:Lcom/twitter/library/av/DMVideoThumbnailView;

    .line 173
    invoke-static {v0}, Lapv$c;->m(Lapv$c;)Lcom/twitter/android/av/video/VideoContainerHost;

    move-result-object v1

    iput-object v1, p0, Lapv;->z:Lcom/twitter/android/av/video/VideoContainerHost;

    .line 174
    invoke-static {v0}, Lapv$c;->n(Lapv$c;)Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lapv;->x:Landroid/view/ViewGroup;

    .line 176
    invoke-static {v0}, Lapv$c;->o(Lapv$c;)Lcom/twitter/library/view/QuoteView;

    move-result-object v1

    iput-object v1, p0, Lapv;->A:Lcom/twitter/library/view/QuoteView;

    .line 177
    invoke-static {v0}, Lapv$c;->p(Lapv$c;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lapv;->F:Landroid/view/View;

    .line 179
    invoke-virtual {p0}, Lapv;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapv;->N:Ljava/lang/String;

    .line 181
    invoke-static {p1}, Lapv$a;->l(Lapv$a;)Lcom/twitter/android/bn;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bn;

    iput-object v0, p0, Lapv;->p:Lcom/twitter/android/bn;

    .line 183
    invoke-static {p1}, Lapv$a;->m(Lapv$a;)Lcdu;

    move-result-object v0

    iput-object v0, p0, Lapv;->P:Lcdu;

    .line 185
    invoke-direct {p0}, Lapv;->B()Lcom/twitter/android/card/d;

    move-result-object v0

    iput-object v0, p0, Lapv;->O:Lcom/twitter/android/card/d;

    .line 187
    invoke-static {p1}, Lapv$a;->n(Lapv$a;)Lcom/twitter/app/dm/p;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/p;

    iput-object v0, p0, Lapv;->Q:Lcom/twitter/app/dm/p;

    .line 188
    invoke-static {p1}, Lapv$a;->o(Lapv$a;)Lcom/twitter/app/dm/o;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/o;

    iput-object v0, p0, Lapv;->R:Lcom/twitter/app/dm/o;

    .line 189
    return-void
.end method

.method private A()Z
    .locals 2

    .prologue
    .line 816
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->y()I

    move-result v0

    const/16 v1, 0xa

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private B()Lcom/twitter/android/card/d;
    .locals 2

    .prologue
    .line 821
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    instance-of v0, v0, Lcom/twitter/model/dms/a;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lapv;->H:Z

    if-eqz v0, :cond_0

    .line 822
    new-instance v1, Lcom/twitter/android/card/f;

    iget-object v0, p0, Lapv;->g:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    .line 823
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/a;

    invoke-static {v0}, Lcom/twitter/library/card/CardContextFactory;->a(Lcom/twitter/model/dms/a;)Lcom/twitter/library/card/CardContext;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/card/CardContext;)V

    .line 824
    iget-object v0, p0, Lapv;->J:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-interface {v1, v0}, Lcom/twitter/android/card/d;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    move-object v0, v1

    .line 827
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lapv;)Lcom/twitter/app/dm/o;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lapv;->R:Lcom/twitter/app/dm/o;

    return-object v0
.end method

.method static synthetic a(Lapv;Lccf;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lapv;->b(Lccf;)V

    return-void
.end method

.method private a(Lcdu;)V
    .locals 4

    .prologue
    .line 517
    iget-object v0, p0, Lapv;->B:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p1, Lcdu;->j:Lcea;

    iget v1, v1, Lcea;->b:F

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 518
    iget-object v0, p0, Lapv;->B:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 520
    iget-object v0, p0, Lapv;->B:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p1, Lcdu;->j:Lcea;

    iget-object v1, v1, Lcea;->c:Lcdw;

    iget-object v1, v1, Lcdw;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    new-instance v2, Lcom/twitter/media/util/r;

    iget-object v3, p1, Lcdu;->j:Lcea;

    invoke-direct {v2, v3}, Lcom/twitter/media/util/r;-><init>(Lcea;)V

    .line 521
    invoke-virtual {v1, v2}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/a$c;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    .line 520
    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 522
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/model/dms/e;Lapv$b;)V
    .locals 4

    .prologue
    const v3, 0x7f0e01e7

    .line 394
    iget-wide v0, p0, Lapv;->i:J

    invoke-virtual {p2, v0, v1}, Lcom/twitter/model/dms/e;->b(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lapv;->F:Landroid/view/View;

    const v1, 0x7f020129

    const v2, 0x7f110089

    .line 396
    invoke-direct {p0, v1, v2}, Lapv;->b(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 395
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 401
    :goto_0
    iget-object v0, p0, Lapv;->h:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 402
    iget-object v1, p0, Lapv;->h:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 403
    iget-object v2, p0, Lapv;->F:Landroid/view/View;

    invoke-virtual {v2, v0, v1, v0, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 404
    iget-object v0, p0, Lapv;->F:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 405
    iget-object v0, p0, Lapv;->F:Landroid/view/View;

    new-instance v1, Lapv$10;

    invoke-direct {v1, p0, p1, p3}, Lapv$10;-><init>(Lapv;Ljava/lang/String;Lapv$b;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 413
    iget-object v0, p0, Lapv;->s:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 414
    return-void

    .line 398
    :cond_0
    iget-object v0, p0, Lapv;->F:Landroid/view/View;

    const v1, 0x7f020127

    const v2, 0x7f110087

    .line 399
    invoke-direct {p0, v1, v2}, Lapv;->b(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 398
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(Lcbz;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 498
    iget-object v2, p0, Lapv;->x:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 502
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcbz;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lapv;->x:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private b(II)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 359
    iget-object v0, p0, Lapv;->g:Landroid/content/Context;

    invoke-static {v0, p1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 360
    iget-object v1, p0, Lapv;->g:Landroid/content/Context;

    invoke-static {v1, p2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-static {v0, v1}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 361
    return-object v0
.end method

.method static synthetic b(Lapv;)Landroid/view/View;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lapv;->F:Landroid/view/View;

    return-object v0
.end method

.method private b(Lccf;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 476
    iget-object v1, p0, Lapv;->y:Lcom/twitter/library/av/DMVideoThumbnailView;

    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccf;

    invoke-virtual {v0}, Lccf;->j()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/av/DMVideoThumbnailView;->a(Ljava/lang/String;Z)V

    .line 477
    iget-object v0, p0, Lapv;->y:Lcom/twitter/library/av/DMVideoThumbnailView;

    invoke-virtual {v0}, Lcom/twitter/library/av/DMVideoThumbnailView;->a()V

    .line 478
    iget-object v0, p0, Lapv;->y:Lcom/twitter/library/av/DMVideoThumbnailView;

    new-instance v1, Lapv$14;

    invoke-direct {v1, p0}, Lapv$14;-><init>(Lapv;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/DMVideoThumbnailView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 487
    iget-object v0, p0, Lapv;->y:Lcom/twitter/library/av/DMVideoThumbnailView;

    new-instance v1, Lapv$15;

    invoke-direct {v1, p0}, Lapv$15;-><init>(Lapv;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/DMVideoThumbnailView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 493
    iget-object v0, p0, Lapv;->y:Lcom/twitter/library/av/DMVideoThumbnailView;

    invoke-virtual {v0, v3}, Lcom/twitter/library/av/DMVideoThumbnailView;->setVisibility(I)V

    .line 494
    iget-object v0, p0, Lapv;->s:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 495
    return-void
.end method

.method static synthetic c(Lapv;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lapv;->o()V

    return-void
.end method

.method private c(Lccf;)Z
    .locals 2

    .prologue
    .line 697
    invoke-virtual {p1}, Lccf;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lapv;->G:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lapv;->R:Lcom/twitter/app/dm/o;

    .line 698
    invoke-virtual {p1}, Lccf;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/o;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 697
    :goto_0
    return v0

    .line 698
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lapv;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lapv;->J:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method

.method private n()V
    .locals 3

    .prologue
    .line 425
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    .line 426
    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccf;

    .line 428
    invoke-direct {p0, v0}, Lapv;->c(Lccf;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 429
    invoke-virtual {v0}, Lccf;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    new-instance v2, Lapv$11;

    invoke-direct {v2, p0}, Lapv$11;-><init>(Lapv;)V

    invoke-direct {p0, v1, v0, v2}, Lapv;->a(Ljava/lang/String;Lcom/twitter/model/dms/e;Lapv$b;)V

    .line 438
    :goto_0
    invoke-virtual {p0}, Lapv;->l()V

    .line 439
    iget-object v0, p0, Lapv;->r:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 440
    return-void

    .line 436
    :cond_0
    invoke-direct {p0}, Lapv;->o()V

    goto :goto_0
.end method

.method private o()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 443
    new-instance v0, Lcom/twitter/android/av/video/f;

    new-instance v1, Lcom/twitter/library/av/playback/DMAVDataSource;

    new-instance v3, Lcom/twitter/library/av/playback/DMAVDataSource$a;

    invoke-direct {v3}, Lcom/twitter/library/av/playback/DMAVDataSource$a;-><init>()V

    iget-object v2, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v2, Lcom/twitter/model/dms/e;

    .line 444
    invoke-virtual {v3, v2}, Lcom/twitter/library/av/playback/DMAVDataSource$a;->a(Lcom/twitter/model/dms/e;)Lcom/twitter/library/av/playback/DMAVDataSource$a;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/library/av/playback/DMAVDataSource;-><init>(Lcom/twitter/library/av/playback/DMAVDataSource$a;)V

    iget-object v2, p0, Lapv;->J:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    sget-object v3, Lbyo;->f:Lbyf;

    sget-object v4, Lcom/twitter/library/av/VideoPlayerView$Mode;->a:Lcom/twitter/library/av/VideoPlayerView$Mode;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/av/video/f;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lbyf;Lcom/twitter/library/av/VideoPlayerView$Mode;Landroid/view/View$OnClickListener;)V

    .line 446
    iget-object v1, p0, Lapv;->z:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v1, v0}, Lcom/twitter/android/av/video/VideoContainerHost;->setVideoContainerConfig(Lcom/twitter/android/av/video/f;)V

    .line 447
    iget-object v0, p0, Lapv;->z:Lcom/twitter/android/av/video/VideoContainerHost;

    new-instance v1, Lapv$12;

    invoke-direct {v1, p0}, Lapv$12;-><init>(Lapv;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/VideoContainerHost;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 453
    iget-object v0, p0, Lapv;->z:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v0, v6}, Lcom/twitter/android/av/video/VideoContainerHost;->setVisibility(I)V

    .line 454
    iget-object v0, p0, Lapv;->s:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 455
    return-void
.end method

.method private p()V
    .locals 4

    .prologue
    .line 458
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    .line 459
    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccf;

    .line 461
    invoke-direct {p0, v0}, Lapv;->c(Lccf;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 462
    invoke-virtual {v0}, Lccf;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v1, Lcom/twitter/model/dms/e;

    new-instance v3, Lapv$13;

    invoke-direct {v3, p0, v0}, Lapv$13;-><init>(Lapv;Lccf;)V

    invoke-direct {p0, v2, v1, v3}, Lapv;->a(Ljava/lang/String;Lcom/twitter/model/dms/e;Lapv$b;)V

    .line 471
    :goto_0
    invoke-virtual {p0}, Lapv;->l()V

    .line 472
    iget-object v0, p0, Lapv;->r:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 473
    return-void

    .line 469
    :cond_0
    invoke-direct {p0, v0}, Lapv;->b(Lccf;)V

    goto :goto_0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lapv;->P:Lcdu;

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lapv;->P:Lcdu;

    invoke-direct {p0, v0}, Lapv;->a(Lcdu;)V

    .line 512
    :goto_0
    invoke-virtual {p0}, Lapv;->l()V

    .line 513
    iget-object v0, p0, Lapv;->r:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 514
    return-void

    .line 509
    :cond_0
    invoke-direct {p0}, Lapv;->r()V

    goto :goto_0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 525
    iget-object v0, p0, Lapv;->B:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 526
    iget-object v0, p0, Lapv;->B:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 527
    return-void
.end method

.method private s()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 530
    iget-boolean v0, p0, Lapv;->H:Z

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbz;

    .line 532
    invoke-direct {p0, v0}, Lapv;->a(Lcbz;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 533
    iget-object v1, p0, Lapv;->m:Lcom/twitter/app/dm/v;

    iget-object v2, p0, Lapv;->b:Lcom/twitter/model/dms/m;

    invoke-interface {v1, v2}, Lcom/twitter/app/dm/v;->c(Lcom/twitter/model/dms/m;)Landroid/view/View;

    move-result-object v1

    .line 534
    if-eqz v1, :cond_0

    .line 535
    invoke-virtual {v0}, Lcbz;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 536
    iget-object v2, p0, Lapv;->x:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 537
    iget-object v2, p0, Lapv;->x:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 538
    iget-object v1, p0, Lapv;->x:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 539
    invoke-virtual {p0}, Lapv;->l()V

    .line 540
    iget-object v1, p0, Lapv;->r:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 541
    iget-object v1, p0, Lapv;->s:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 542
    iget-object v1, p0, Lapv;->O:Lcom/twitter/android/card/d;

    if-eqz v1, :cond_0

    .line 543
    iget-object v1, p0, Lapv;->O:Lcom/twitter/android/card/d;

    const-string/jumbo v2, "show"

    const-string/jumbo v3, "platform_card"

    .line 544
    invoke-virtual {v0}, Lcbz;->k()Ljava/lang/String;

    move-result-object v0

    .line 543
    invoke-interface {v1, v2, v3, v0}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    :cond_0
    :goto_0
    return-void

    .line 548
    :cond_1
    iget-object v0, p0, Lapv;->x:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 549
    invoke-virtual {p0}, Lapv;->l()V

    .line 550
    iget-object v0, p0, Lapv;->r:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 551
    iget-object v0, p0, Lapv;->s:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private t()V
    .locals 4

    .prologue
    .line 557
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccf;

    .line 559
    invoke-direct {p0, v0}, Lapv;->c(Lccf;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 560
    invoke-virtual {v0}, Lccf;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v1, Lcom/twitter/model/dms/e;

    new-instance v3, Lapv$2;

    invoke-direct {v3, p0, v0}, Lapv$2;-><init>(Lapv;Lccf;)V

    invoke-direct {p0, v2, v1, v3}, Lapv;->a(Ljava/lang/String;Lcom/twitter/model/dms/e;Lapv$b;)V

    .line 570
    :goto_0
    invoke-virtual {p0}, Lapv;->l()V

    .line 571
    iget-object v0, p0, Lapv;->r:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 572
    return-void

    .line 567
    :cond_0
    invoke-virtual {p0, v0}, Lapv;->a(Lccf;)V

    goto :goto_0
.end method

.method private u()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 610
    iget-wide v2, p0, Lapv;->i:J

    .line 612
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    check-cast v0, Lcci;

    .line 614
    iget-object v4, p0, Lapv;->Q:Lcom/twitter/app/dm/p;

    iget-object v1, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v1, Lcom/twitter/model/dms/e;

    invoke-virtual {v4, v1}, Lcom/twitter/app/dm/p;->a(Lcom/twitter/model/dms/e;)V

    .line 616
    iget-object v1, p0, Lapv;->A:Lcom/twitter/library/view/QuoteView;

    iget-boolean v4, p0, Lapv;->G:Z

    invoke-virtual {v1, v4}, Lcom/twitter/library/view/QuoteView;->setDisplaySensitiveMedia(Z)V

    .line 617
    iget-object v1, p0, Lapv;->A:Lcom/twitter/library/view/QuoteView;

    iget-object v4, v0, Lcci;->d:Lcom/twitter/model/core/r;

    invoke-virtual {v1, v4}, Lcom/twitter/library/view/QuoteView;->setQuoteData(Lcom/twitter/model/core/r;)V

    .line 618
    iget-object v1, p0, Lapv;->A:Lcom/twitter/library/view/QuoteView;

    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/twitter/library/view/QuoteView;->setRenderRtl(Z)V

    .line 619
    iget-object v1, p0, Lapv;->A:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v1, v6}, Lcom/twitter/library/view/QuoteView;->setVisibility(I)V

    .line 620
    iget-object v1, p0, Lapv;->A:Lcom/twitter/library/view/QuoteView;

    iget-object v4, p0, Lapv;->h:Landroid/content/res/Resources;

    const v5, 0x7f0e0077

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v4}, Lcom/twitter/library/view/QuoteView;->setBorderCornerRadius(I)V

    .line 622
    iget-object v1, p0, Lapv;->A:Lcom/twitter/library/view/QuoteView;

    new-instance v4, Lapv$5;

    invoke-direct {v4, p0, v2, v3, v0}, Lapv$5;-><init>(Lapv;JLcci;)V

    invoke-virtual {v1, v4}, Lcom/twitter/library/view/QuoteView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 639
    iget-object v0, p0, Lapv;->A:Lcom/twitter/library/view/QuoteView;

    new-instance v1, Lapv$6;

    invoke-direct {v1, p0}, Lapv$6;-><init>(Lapv;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/QuoteView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 646
    invoke-virtual {p0}, Lapv;->l()V

    .line 647
    iget-object v0, p0, Lapv;->r:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 648
    iget-object v0, p0, Lapv;->s:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 649
    return-void
.end method

.method private v()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 653
    iget-object v0, p0, Lapv;->w:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lapv;->m:Lcom/twitter/app/dm/v;

    iget-object v2, p0, Lapv;->b:Lcom/twitter/model/dms/m;

    invoke-interface {v0, v2}, Lcom/twitter/app/dm/v;->b(Lcom/twitter/model/dms/m;)Landroid/view/View;

    move-result-object v0

    .line 655
    if-eqz v0, :cond_0

    .line 656
    iget-object v2, p0, Lapv;->w:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 657
    iget-object v2, p0, Lapv;->w:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 661
    iget-boolean v0, p0, Lapv;->d:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0200a2

    .line 663
    :goto_0
    iget-boolean v2, p0, Lapv;->d:Z

    if-eqz v2, :cond_2

    .line 666
    :goto_1
    iget-object v2, p0, Lapv;->w:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 667
    iget-object v0, p0, Lapv;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 668
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 670
    iget-object v1, p0, Lapv;->w:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 671
    const/4 v1, 0x1

    .line 674
    :cond_0
    return v1

    .line 661
    :cond_1
    const v0, 0x7f0200a1

    goto :goto_0

    .line 663
    :cond_2
    iget-object v1, p0, Lapv;->h:Landroid/content/res/Resources;

    const v2, 0x7f0e01ec

    .line 664
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_1
.end method

.method private w()Ljava/lang/CharSequence;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 703
    iget-object v0, p0, Lapv;->b:Lcom/twitter/model/dms/m;

    iget-object v4, v0, Lcom/twitter/model/dms/m;->b:Ljava/lang/String;

    .line 704
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    iget-wide v6, p0, Lapv;->i:J

    invoke-virtual {v0, v6, v7}, Lcom/twitter/model/dms/e;->b(J)Z

    move-result v0

    if-nez v0, :cond_1

    move v1, v2

    .line 706
    :goto_0
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->z()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 707
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccf;

    iget-object v0, v0, Lccf;->d:Lcom/twitter/model/core/MediaEntity;

    invoke-virtual {v0}, Lcom/twitter/model/core/MediaEntity;->bv_()Ljava/lang/String;

    move-result-object v0

    .line 708
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 709
    if-eqz v1, :cond_2

    .line 710
    iget-object v5, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v6, p0, Lapv;->h:Landroid/content/res/Resources;

    const v7, 0x7f0a02ce

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v4, v8, v3

    aput-object v0, v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/twitter/media/ui/image/MediaImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 751
    :cond_0
    :goto_1
    iget-object v0, p0, Lapv;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 752
    if-eqz v1, :cond_d

    .line 753
    iget-object v1, p0, Lapv;->h:Landroid/content/res/Resources;

    const v5, 0x7f0a02d3

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    .line 754
    invoke-static {v4}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    aput-object v0, v6, v2

    iget-object v0, p0, Lapv;->N:Ljava/lang/String;

    aput-object v0, v6, v9

    .line 753
    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 759
    :goto_2
    return-object v0

    :cond_1
    move v1, v3

    .line 704
    goto :goto_0

    .line 713
    :cond_2
    iget-object v5, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v6, p0, Lapv;->h:Landroid/content/res/Resources;

    const v7, 0x7f0a02e1

    new-array v8, v2, [Ljava/lang/Object;

    aput-object v0, v8, v3

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/twitter/media/ui/image/MediaImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 717
    :cond_3
    if-eqz v1, :cond_4

    .line 718
    iget-object v0, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v5, p0, Lapv;->h:Landroid/content/res/Resources;

    const v6, 0x7f0a02cd

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v4, v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/media/ui/image/MediaImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 720
    :cond_4
    iget-object v0, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v5, p0, Lapv;->h:Landroid/content/res/Resources;

    const v6, 0x7f0a02e0

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/media/ui/image/MediaImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 723
    :cond_5
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->B()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724
    if-eqz v1, :cond_6

    .line 725
    iget-object v0, p0, Lapv;->y:Lcom/twitter/library/av/DMVideoThumbnailView;

    iget-object v5, p0, Lapv;->h:Landroid/content/res/Resources;

    const v6, 0x7f0a02d0

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v4, v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/library/av/DMVideoThumbnailView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 728
    :cond_6
    iget-object v0, p0, Lapv;->y:Lcom/twitter/library/av/DMVideoThumbnailView;

    iget-object v5, p0, Lapv;->h:Landroid/content/res/Resources;

    const v6, 0x7f0a02e3

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/library/av/DMVideoThumbnailView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 730
    :cond_7
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->C()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 731
    if-eqz v1, :cond_8

    .line 732
    iget-object v0, p0, Lapv;->z:Lcom/twitter/android/av/video/VideoContainerHost;

    iget-object v5, p0, Lapv;->h:Landroid/content/res/Resources;

    const v6, 0x7f0a02cc

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v4, v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/android/av/video/VideoContainerHost;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 734
    :cond_8
    iget-object v0, p0, Lapv;->z:Lcom/twitter/android/av/video/VideoContainerHost;

    iget-object v5, p0, Lapv;->h:Landroid/content/res/Resources;

    const v6, 0x7f0a02df

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/android/av/video/VideoContainerHost;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 736
    :cond_9
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->e()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 737
    if-eqz v1, :cond_a

    .line 738
    iget-object v0, p0, Lapv;->x:Landroid/view/ViewGroup;

    iget-object v5, p0, Lapv;->h:Landroid/content/res/Resources;

    const v6, 0x7f0a02d2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 740
    :cond_a
    iget-object v0, p0, Lapv;->x:Landroid/view/ViewGroup;

    iget-object v5, p0, Lapv;->h:Landroid/content/res/Resources;

    const v6, 0x7f0a02e4

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 742
    :cond_b
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 743
    if-eqz v1, :cond_c

    .line 744
    iget-object v0, p0, Lapv;->B:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v5, p0, Lapv;->h:Landroid/content/res/Resources;

    const v6, 0x7f0a02cf

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v4, v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/media/ui/image/MediaImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 746
    :cond_c
    iget-object v0, p0, Lapv;->B:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v5, p0, Lapv;->h:Landroid/content/res/Resources;

    const v6, 0x7f0a02e2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/media/ui/image/MediaImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 756
    :cond_d
    iget-object v1, p0, Lapv;->h:Landroid/content/res/Resources;

    const v4, 0x7f0a02e5

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v0, v5, v3

    iget-object v0, p0, Lapv;->N:Ljava/lang/String;

    aput-object v0, v5, v2

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2
.end method

.method private x()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 764
    iget-object v0, p0, Lapv;->c:Lcom/twitter/model/dms/c;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c;

    invoke-virtual {v0}, Lcom/twitter/model/dms/c;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv;->c:Lcom/twitter/model/dms/c;

    .line 765
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 766
    iget-object v0, p0, Lapv;->h:Landroid/content/res/Resources;

    const v1, 0x7f0e01f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v1, v0

    .line 773
    :goto_0
    iget-object v0, p0, Lapv;->C:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 774
    iget-object v2, p0, Lapv;->h:Landroid/content/res/Resources;

    const v3, 0x7f0e01f0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 775
    invoke-virtual {v0, v4, v2, v4, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 776
    iget-object v1, p0, Lapv;->C:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 777
    return-void

    .line 767
    :cond_0
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    iget-object v1, p0, Lapv;->c:Lcom/twitter/model/dms/c;

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/e;->a(Lcom/twitter/model/dms/d;)J

    move-result-wide v0

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 768
    iget-object v0, p0, Lapv;->h:Landroid/content/res/Resources;

    const v1, 0x7f0e01ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 770
    :cond_1
    iget-object v0, p0, Lapv;->h:Landroid/content/res/Resources;

    const v1, 0x7f0e01ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v1, v0

    goto :goto_0
.end method

.method private y()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 780
    iget-object v0, p0, Lapv;->h:Landroid/content/res/Resources;

    const v1, 0x7f0e01d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 781
    iget-object v1, p0, Lapv;->h:Landroid/content/res/Resources;

    const v2, 0x7f0e0301

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 782
    iget-object v2, p0, Lapv;->h:Landroid/content/res/Resources;

    const v3, 0x7f0e0300

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 783
    iget-object v3, p0, Lapv;->k:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v1, v0, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 784
    invoke-direct {p0}, Lapv;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 785
    invoke-direct {p0}, Lapv;->z()V

    .line 786
    iget-object v1, p0, Lapv;->Q:Lcom/twitter/app/dm/p;

    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/p;->b(Lcom/twitter/model/dms/e;)V

    .line 787
    iget-object v0, p0, Lapv;->l:Landroid/view/View;

    iget-object v1, p0, Lapv;->g:Landroid/content/Context;

    const v2, 0x7f11004e

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 788
    iget-object v0, p0, Lapv;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lapv;->h:Landroid/content/res/Resources;

    const v2, 0x7f0e01dc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v4, v4, v4, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 789
    iget-object v0, p0, Lapv;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lapv;->g:Landroid/content/Context;

    const v2, 0x7f110088

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 790
    iget-object v1, p0, Lapv;->k:Landroid/widget/TextView;

    iget-object v2, p0, Lapv;->h:Landroid/content/res/Resources;

    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    .line 791
    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->y()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    invoke-static {}, Lcom/twitter/library/dm/d;->o()Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f0e01de

    .line 790
    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v4, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 794
    :cond_0
    return-void

    .line 791
    :cond_1
    const v0, 0x7f0e01dd

    goto :goto_0
.end method

.method private z()V
    .locals 2

    .prologue
    .line 797
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v0

    .line 798
    iget-object v1, p0, Lapv;->m:Lcom/twitter/app/dm/v;

    invoke-interface {v1, v0}, Lcom/twitter/app/dm/v;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lapv;->m:Lcom/twitter/app/dm/v;

    invoke-interface {v0}, Lcom/twitter/app/dm/v;->c()V

    .line 800
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->y()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 801
    iget-object v0, p0, Lapv;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lapv$7;

    invoke-direct {v1, p0}, Lapv$7;-><init>(Lapv;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 813
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 7
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/16 v6, 0x8

    const/4 v3, 0x0

    .line 194
    iget-object v0, p0, Lapv;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lapv;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lapv;->s:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 198
    invoke-virtual {p0}, Lapv;->b()V

    .line 200
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 202
    invoke-virtual {p0}, Lapv;->m()V

    .line 203
    invoke-virtual {p0}, Lapv;->k()V

    .line 204
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lapv;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Lapv;->s:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 211
    :cond_0
    iget-object v0, p0, Lapv;->k:Landroid/widget/TextView;

    sget v4, Lcni;->a:F

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 212
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    iget-wide v4, p0, Lapv;->i:J

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/dms/e;->b(J)Z

    move-result v4

    .line 213
    iget-object v5, p0, Lapv;->g:Landroid/content/Context;

    if-eqz v4, :cond_3

    const v0, 0x7f11008a

    :goto_0
    invoke-static {v5, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v5

    .line 216
    invoke-static {v1}, Lcnf;->a(Ljava/lang/CharSequence;)Lcnf;

    move-result-object v1

    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    .line 217
    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->t()Lcom/twitter/model/core/v;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcnf;->a(Lcom/twitter/model/core/v;)Lcnf;

    move-result-object v0

    iget-object v1, p0, Lapv;->n:Lcne;

    .line 218
    invoke-virtual {v0, v1}, Lcnf;->a(Lcne;)Lcnf;

    move-result-object v0

    .line 219
    invoke-virtual {v0, v5}, Lcnf;->a(I)Lcnf;

    move-result-object v0

    .line 220
    invoke-virtual {v0, v2}, Lcnf;->a(Z)Lcnf;

    move-result-object v0

    .line 221
    invoke-virtual {v0, v4}, Lcnf;->b(Z)Lcnf;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Lcnf;->a()Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 224
    invoke-virtual {p0}, Lapv;->d()V

    .line 226
    invoke-direct {p0}, Lapv;->y()V

    .line 227
    iget-object v1, p0, Lapv;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v1, p0, Lapv;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setLinkTextColor(I)V

    .line 229
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lapv;->a(Z)V

    .line 230
    iget-object v0, p0, Lapv;->k:Landroid/widget/TextView;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 232
    iget-object v0, p0, Lapv;->C:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lapv;->l:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 235
    invoke-direct {p0}, Lapv;->v()Z

    move-result v0

    .line 237
    if-nez v0, :cond_5

    .line 238
    invoke-virtual {p0}, Lapv;->j()V

    .line 239
    iget-object v0, p0, Lapv;->c:Lcom/twitter/model/dms/c;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lapv;->c:Lcom/twitter/model/dms/c;

    invoke-virtual {v0}, Lcom/twitter/model/dms/c;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    iget-object v1, p0, Lapv;->c:Lcom/twitter/model/dms/c;

    .line 240
    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/e;

    invoke-virtual {p0, v0, v1}, Lapv;->a(Lcom/twitter/model/dms/e;Lcom/twitter/model/dms/e;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    .line 242
    :goto_1
    iget-boolean v1, p0, Lapv;->d:Z

    if-nez v1, :cond_1

    .line 243
    invoke-direct {p0}, Lapv;->x()V

    .line 249
    :cond_1
    :goto_2
    if-eqz v0, :cond_6

    .line 250
    invoke-virtual {p0}, Lapv;->f()V

    .line 255
    :goto_3
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    iget-object v0, p0, Lapv;->C:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 257
    iget-object v0, p0, Lapv;->D:Landroid/widget/Button;

    new-instance v1, Lapv$1;

    invoke-direct {v1, p0}, Lapv$1;-><init>(Lapv;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    iget-object v0, p0, Lapv;->E:Landroid/widget/Button;

    new-instance v1, Lapv$8;

    invoke-direct {v1, p0}, Lapv$8;-><init>(Lapv;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    :cond_2
    iget-object v0, p0, Lapv;->v:Landroid/view/View;

    invoke-direct {p0}, Lapv;->w()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 273
    new-instance v0, Lapv$9;

    invoke-direct {v0, p0}, Lapv$9;-><init>(Lapv;)V

    .line 279
    iget-object v1, p0, Lapv;->l:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 280
    iget-object v1, p0, Lapv;->u:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 281
    return-void

    .line 213
    :cond_3
    const v0, 0x7f1100c8

    goto/16 :goto_0

    :cond_4
    move v0, v3

    .line 240
    goto :goto_1

    :cond_5
    move v0, v3

    .line 246
    goto :goto_2

    .line 252
    :cond_6
    invoke-virtual {p0}, Lapv;->g()V

    goto :goto_3
.end method

.method a(II)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 348
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lapv;->l:Landroid/view/View;

    invoke-direct {p0, p1, p2}, Lapv;->b(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 350
    iget-object v0, p0, Lapv;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lapv;->h:Landroid/content/res/Resources;

    const v2, 0x7f0e0068

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 355
    :goto_0
    return-void

    .line 352
    :cond_0
    iget-object v0, p0, Lapv;->l:Landroid/view/View;

    iget-object v1, p0, Lapv;->g:Landroid/content/Context;

    invoke-static {v1, p2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 353
    iget-object v0, p0, Lapv;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lapv;->h:Landroid/content/res/Resources;

    const v2, 0x7f0e0067

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_0
.end method

.method a(Lccf;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 575
    invoke-direct {p0, p1}, Lapv;->c(Lccf;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 577
    invoke-virtual {p1}, Lccf;->k()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Lccf;->l()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    const/high16 v2, 0x3f400000    # 0.75f

    const/high16 v3, 0x40400000    # 3.0f

    invoke-static {v0, v2, v3}, Lcom/twitter/util/math/b;->a(FFF)F

    move-result v0

    .line 579
    iget-object v2, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v2, v0}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 580
    iget-object v0, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget-object v3, p0, Lapv;->g:Landroid/content/Context;

    const v4, 0x7f11000b

    invoke-static {v3, v4}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 581
    iget-object v2, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-static {p1}, Lcom/twitter/media/util/k;->a(Lccf;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    iget-object v3, p0, Lapv;->I:Lcom/twitter/library/network/t;

    invoke-virtual {v0, v3}, Lcom/twitter/media/request/a$a;->a(Ljava/lang/Object;)Lcom/twitter/media/request/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a$a;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->a(Lcom/twitter/media/request/a$a;Z)Z

    .line 582
    iget-object v0, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 584
    iget-object v0, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    new-instance v2, Lapv$3;

    invoke-direct {v2, p0, p1}, Lapv$3;-><init>(Lapv;Lccf;)V

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 598
    iget-object v0, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    new-instance v2, Lapv$4;

    invoke-direct {v2, p0}, Lapv$4;-><init>(Lapv;)V

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 605
    iget-object v0, p0, Lapv;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 606
    return-void

    :cond_0
    move v0, v1

    .line 575
    goto :goto_0
.end method

.method abstract a(Z)V
.end method

.method a(Lcom/twitter/model/dms/e;Lcom/twitter/model/dms/e;)Z
    .locals 4

    .prologue
    .line 286
    iget-object v0, p0, Lapv;->j:Lcom/twitter/app/dm/m;

    iget-wide v2, p2, Lcom/twitter/model/dms/e;->e:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/dm/m;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Lcom/twitter/model/dms/e;->a(Lcom/twitter/model/dms/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Z)V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 295
    invoke-virtual {p0}, Lapv;->h()Lcom/twitter/app/dm/widget/MessageBylineView;

    move-result-object v1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/widget/MessageBylineView;->a(Z)V

    .line 296
    return-void

    .line 295
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method abstract d()V
.end method

.method e()V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 300
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 301
    invoke-virtual {p0}, Lapv;->h()Lcom/twitter/app/dm/widget/MessageBylineView;

    move-result-object v0

    iget-object v1, p0, Lapv;->N:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/MessageBylineView;->setTimestampText(Ljava/lang/CharSequence;)V

    .line 302
    return-void

    .line 300
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method f()V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 306
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lapv;->A()Z

    move-result v0

    if-nez v0, :cond_0

    .line 307
    invoke-virtual {p0}, Lapv;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308
    const v0, 0x7f020129

    const v1, 0x7f110089

    invoke-virtual {p0, v0, v1}, Lapv;->a(II)V

    .line 313
    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lapv;->b(Z)V

    .line 314
    return-void

    .line 310
    :cond_1
    const v0, 0x7f020127

    const v1, 0x7f110087

    invoke-virtual {p0, v0, v1}, Lapv;->a(II)V

    goto :goto_0
.end method

.method g()V
    .locals 4
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 318
    invoke-virtual {p0}, Lapv;->h()Lcom/twitter/app/dm/widget/MessageBylineView;

    move-result-object v0

    iget-object v1, p0, Lapv;->h:Landroid/content/res/Resources;

    const v2, 0x7f0e01db

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/MessageBylineView;->a(I)V

    .line 319
    invoke-virtual {p0}, Lapv;->h()Lcom/twitter/app/dm/widget/MessageBylineView;

    move-result-object v0

    iget-object v1, p0, Lapv;->h:Landroid/content/res/Resources;

    const v2, 0x7f0e01f0

    .line 320
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lapv;->h:Landroid/content/res/Resources;

    const v3, 0x7f0e01ef

    .line 321
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 319
    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/dm/widget/MessageBylineView;->a(II)V

    .line 324
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lapv;->b(Z)V

    .line 325
    return-void
.end method

.method abstract h()Lcom/twitter/app/dm/widget/MessageBylineView;
.end method

.method i()Ljava/lang/String;
    .locals 7

    .prologue
    .line 332
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    iget-wide v0, v0, Lcom/twitter/model/dms/e;->g:J

    .line 333
    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 334
    iget-object v2, p0, Lapv;->K:Ljava/text/SimpleDateFormat;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 344
    :goto_0
    return-object v0

    .line 335
    :cond_0
    const-wide/32 v2, 0x5265c00

    add-long/2addr v2, v0

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 336
    iget-object v2, p0, Lapv;->h:Landroid/content/res/Resources;

    const v3, 0x7f0a0a43

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lapv;->K:Ljava/text/SimpleDateFormat;

    .line 337
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 336
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 338
    :cond_1
    invoke-static {v0, v1}, Lcom/twitter/util/aa;->c(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 339
    iget-object v2, p0, Lapv;->L:Ljava/text/SimpleDateFormat;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 341
    :cond_2
    iget-object v2, p0, Lapv;->M:Ljava/text/SimpleDateFormat;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected j()V
    .locals 6

    .prologue
    .line 366
    iget-object v0, p0, Lapv;->u:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 367
    iget-object v1, p0, Lapv;->h:Landroid/content/res/Resources;

    const v2, 0x7f0e01ec

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 368
    iget-object v2, p0, Lapv;->h:Landroid/content/res/Resources;

    const v3, 0x7f0e0304

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 369
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget-boolean v5, p0, Lapv;->d:Z

    if-eqz v5, :cond_0

    :goto_0
    invoke-virtual {v0, v3, v2, v4, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 371
    iget-object v1, p0, Lapv;->u:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 372
    return-void

    :cond_0
    move v1, v2

    .line 369
    goto :goto_0
.end method

.method k()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 376
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    invoke-direct {p0}, Lapv;->t()V

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 378
    :cond_1
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->B()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 379
    invoke-direct {p0}, Lapv;->p()V

    goto :goto_0

    .line 380
    :cond_2
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->C()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 381
    invoke-direct {p0}, Lapv;->n()V

    goto :goto_0

    .line 382
    :cond_3
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->A()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 383
    invoke-direct {p0}, Lapv;->u()V

    goto :goto_0

    .line 384
    :cond_4
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 386
    invoke-direct {p0}, Lapv;->s()V

    goto :goto_0

    .line 387
    :cond_5
    iget-object v0, p0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    invoke-direct {p0}, Lapv;->q()V

    goto :goto_0
.end method

.method l()V
    .locals 5

    .prologue
    .line 417
    iget-object v0, p0, Lapv;->h:Landroid/content/res/Resources;

    const v1, 0x7f0e0076

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 418
    invoke-virtual {p0}, Lapv;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 419
    :goto_0
    invoke-virtual {p0}, Lapv;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 420
    :goto_1
    iget-object v2, p0, Lapv;->r:Landroid/view/View;

    iget-object v3, p0, Lapv;->r:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    iget-object v4, p0, Lapv;->r:Landroid/view/View;

    .line 421
    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    .line 420
    invoke-virtual {v2, v0, v3, v1, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 422
    return-void

    :cond_0
    move v0, v1

    .line 418
    goto :goto_0

    .line 419
    :cond_1
    iget-object v1, p0, Lapv;->r:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    goto :goto_1
.end method

.method m()V
    .locals 6
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 679
    iget-object v0, p0, Lapv;->r:Landroid/view/View;

    iget-object v1, p0, Lapv;->r:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lapv;->r:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    invoke-virtual {v0, v4, v1, v4, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 680
    iget-object v0, p0, Lapv;->r:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 681
    iget-object v0, p0, Lapv;->A:Lcom/twitter/library/view/QuoteView;

    invoke-virtual {v0, v3}, Lcom/twitter/library/view/QuoteView;->setVisibility(I)V

    .line 682
    iget-object v0, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v5}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 683
    iget-object v0, p0, Lapv;->t:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v3}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 684
    iget-object v0, p0, Lapv;->F:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 685
    iget-object v0, p0, Lapv;->y:Lcom/twitter/library/av/DMVideoThumbnailView;

    invoke-virtual {v0, v3}, Lcom/twitter/library/av/DMVideoThumbnailView;->setVisibility(I)V

    .line 686
    iget-object v0, p0, Lapv;->z:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v0, v3}, Lcom/twitter/android/av/video/VideoContainerHost;->setVisibility(I)V

    .line 687
    iget-object v0, p0, Lapv;->x:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 688
    iget-object v0, p0, Lapv;->w:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 689
    iget-object v0, p0, Lapv;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 690
    iget-object v0, p0, Lapv;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 692
    :cond_0
    iget-object v0, p0, Lapv;->B:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v3}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 693
    iget-object v0, p0, Lapv;->B:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v5}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 694
    return-void
.end method
