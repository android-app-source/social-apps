.class public Lbrh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/TransferListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbrh$a;
    }
.end annotation


# instance fields
.field private final a:Lbix;

.field private b:J

.field private c:Z

.field private d:Z

.field private final e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lbix;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbrh;->e:Ljava/lang/Object;

    .line 38
    iput-object p1, p0, Lbrh;->a:Lbix;

    .line 39
    iget-object v0, p0, Lbrh;->a:Lbix;

    new-instance v1, Lbrh$a;

    invoke-direct {v1, p0}, Lbrh$a;-><init>(Lbrh;)V

    invoke-virtual {v0, v1}, Lbix;->a(Lbiy;)Lbix;

    .line 40
    return-void
.end method

.method static synthetic a(Lbrh;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lbrh;->e:Ljava/lang/Object;

    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 65
    iget-boolean v0, p0, Lbrh;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbrh;->c:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lbrh;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lbrh;->a:Lbix;

    new-instance v1, Lbjq;

    iget-wide v2, p0, Lbrh;->b:J

    invoke-direct {v1, v2, v3}, Lbjq;-><init>(J)V

    invoke-virtual {v0, v1}, Lbix;->a(Lbiw;)V

    .line 67
    iput-wide v4, p0, Lbrh;->b:J

    .line 69
    :cond_0
    return-void
.end method

.method static synthetic a(Lbrh;Z)Z
    .locals 0

    .prologue
    .line 18
    iput-boolean p1, p0, Lbrh;->d:Z

    return p1
.end method

.method static synthetic b(Lbrh;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lbrh;->a()V

    return-void
.end method


# virtual methods
.method public onBytesTransferred(I)V
    .locals 6

    .prologue
    .line 51
    iget-object v1, p0, Lbrh;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 52
    :try_start_0
    iget-wide v2, p0, Lbrh;->b:J

    int-to-long v4, p1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lbrh;->b:J

    .line 53
    monitor-exit v1

    .line 54
    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onTransferEnd()V
    .locals 2

    .prologue
    .line 58
    iget-object v1, p0, Lbrh;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 59
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lbrh;->c:Z

    .line 60
    invoke-direct {p0}, Lbrh;->a()V

    .line 61
    monitor-exit v1

    .line 62
    return-void

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onTransferStart()V
    .locals 2

    .prologue
    .line 44
    iget-object v1, p0, Lbrh;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 45
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lbrh;->c:Z

    .line 46
    monitor-exit v1

    .line 47
    return-void

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
