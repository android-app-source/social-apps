.class Lcmg$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcmg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcmg;


# direct methods
.method private constructor <init>(Lcmg;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcmg$b;->a:Lcmg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcmg;Lcmg$1;)V
    .locals 0

    .prologue
    .line 229
    invoke-direct {p0, p1}, Lcmg$b;-><init>(Lcmg;)V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 245
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 237
    iget-object v0, p0, Lcmg$b;->a:Lcmg;

    invoke-virtual {v0}, Lcmg;->g()Lcmg$a;

    move-result-object v0

    .line 238
    if-eqz v0, :cond_0

    .line 239
    iget-object v1, p0, Lcmg$b;->a:Lcmg;

    iget-object v2, p0, Lcmg$b;->a:Lcmg;

    invoke-static {v2}, Lcmg;->a(Lcmg;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcmg$a;->a(Lcmg;Z)V

    .line 241
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 249
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 253
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 254
    iget-object v1, p0, Lcmg$b;->a:Lcmg;

    invoke-virtual {v1, v0}, Lcmg;->b(F)V

    .line 255
    iget-object v0, p0, Lcmg$b;->a:Lcmg;

    invoke-virtual {v0}, Lcmg;->invalidateSelf()V

    .line 256
    return-void
.end method
