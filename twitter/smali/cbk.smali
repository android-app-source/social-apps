.class public final Lcbk;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcbi;Lcbi;)Lcbi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcbi",
            "<+TT;>;",
            "Lcbi",
            "<+TT;>;)",
            "Lcbi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Lcbk$1;

    invoke-direct {v0, p0, p1}, Lcbk$1;-><init>(Lcbi;Lcbi;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Lcbi;)Lcbi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcbi",
            "<+TT;>;)",
            "Lcbi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v1, Lcbl;

    invoke-virtual {p1}, Lcbi;->be_()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-direct {v1, v0}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    return-object v1
.end method

.method public static a()Lrx/functions/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/e",
            "<",
            "Lcbi",
            "<TT;>;",
            "Lcbi",
            "<TT;>;",
            "Lcbi",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Lcbk$2;

    invoke-direct {v0}, Lcbk$2;-><init>()V

    return-object v0
.end method

.method public static a(Lcbi;)Z
    .locals 1

    .prologue
    .line 27
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcbi;->be_()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Lrx/functions/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/e",
            "<",
            "Lcbi",
            "<TT;>;TT;",
            "Lcbi",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, Lcbk$3;

    invoke-direct {v0}, Lcbk$3;-><init>()V

    return-object v0
.end method
