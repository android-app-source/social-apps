.class public Lof;
.super Lbjb;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/av/playback/AVPlayer;

.field private final b:Lcom/twitter/library/av/playback/av;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/library/av/playback/av;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/av;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lof;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/playback/av;)V

    .line 36
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/playback/av;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 42
    iput-object p1, p0, Lof;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 43
    iput-object p3, p0, Lof;->b:Lcom/twitter/library/av/playback/av;

    .line 44
    return-void
.end method

.method static a(JJZ)Z
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 135
    sub-long v4, p0, p2

    .line 136
    if-eqz p4, :cond_1

    cmp-long v2, v4, v2

    if-gtz v2, :cond_0

    move v2, v0

    .line 137
    :goto_0
    if-eqz v2, :cond_3

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-gtz v2, :cond_3

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 136
    goto :goto_0

    :cond_1
    cmp-long v2, v4, v2

    if-ltz v2, :cond_2

    move v2, v0

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 137
    goto :goto_1
.end method


# virtual methods
.method a(Lcom/twitter/library/av/playback/aa;Lcom/twitter/library/av/playback/av;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 76
    if-nez p1, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    iget-wide v4, p1, Lcom/twitter/library/av/playback/aa;->c:J

    .line 81
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gez v0, :cond_7

    move v0, v1

    .line 82
    :goto_1
    const-wide/16 v6, 0x4

    div-long v6, v4, v6

    .line 83
    iget-wide v8, p1, Lcom/twitter/library/av/playback/aa;->b:J

    .line 85
    iget-boolean v3, p2, Lcom/twitter/library/av/playback/av;->a:Z

    if-nez v3, :cond_2

    .line 86
    iput-boolean v1, p2, Lcom/twitter/library/av/playback/av;->a:Z

    .line 87
    iget-object v3, p0, Lof;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v3}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v3

    new-instance v10, Loq;

    iget-object v11, p0, Lof;->k:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v10, v11}, Loq;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {p0}, Lof;->b()Lcom/twitter/library/av/m;

    move-result-object v11

    invoke-virtual {v3, v10, v11}, Lbix;->a(Lbiw;Lcom/twitter/library/av/m;)V

    .line 90
    :cond_2
    if-nez v0, :cond_0

    .line 91
    iget-boolean v0, p2, Lcom/twitter/library/av/playback/av;->b:Z

    if-nez v0, :cond_3

    .line 92
    invoke-static {v8, v9, v6, v7, v2}, Lof;->a(JJZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 93
    iput-boolean v1, p2, Lcom/twitter/library/av/playback/av;->b:Z

    .line 94
    iget-object v0, p0, Lof;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v0

    new-instance v3, Lol;

    iget-object v10, p0, Lof;->k:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v3, v10}, Lol;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {p0}, Lof;->b()Lcom/twitter/library/av/m;

    move-result-object v10

    invoke-virtual {v0, v3, v10}, Lbix;->a(Lbiw;Lcom/twitter/library/av/m;)V

    .line 97
    :cond_3
    iget-boolean v0, p2, Lcom/twitter/library/av/playback/av;->c:Z

    if-nez v0, :cond_4

    const-wide/16 v10, 0x2

    mul-long/2addr v10, v6

    .line 98
    invoke-static {v8, v9, v10, v11, v2}, Lof;->a(JJZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 99
    iput-boolean v1, p2, Lcom/twitter/library/av/playback/av;->c:Z

    .line 100
    iget-object v0, p0, Lof;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v0

    new-instance v3, Lom;

    iget-object v10, p0, Lof;->k:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v3, v10}, Lom;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {p0}, Lof;->b()Lcom/twitter/library/av/m;

    move-result-object v10

    invoke-virtual {v0, v3, v10}, Lbix;->a(Lbiw;Lcom/twitter/library/av/m;)V

    .line 103
    :cond_4
    iget-boolean v0, p2, Lcom/twitter/library/av/playback/av;->d:Z

    if-nez v0, :cond_5

    const-wide/16 v10, 0x3

    mul-long/2addr v6, v10

    .line 104
    invoke-static {v8, v9, v6, v7, v2}, Lof;->a(JJZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 105
    iput-boolean v1, p2, Lcom/twitter/library/av/playback/av;->d:Z

    .line 106
    iget-object v0, p0, Lof;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v0

    new-instance v2, Lon;

    iget-object v3, p0, Lof;->k:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v2, v3}, Lon;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {p0}, Lof;->b()Lcom/twitter/library/av/m;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lbix;->a(Lbiw;Lcom/twitter/library/av/m;)V

    .line 109
    :cond_5
    long-to-double v2, v4

    const-wide v6, 0x3fee666666666666L    # 0.95

    mul-double/2addr v2, v6

    double-to-long v2, v2

    .line 110
    iget-boolean v0, p2, Lcom/twitter/library/av/playback/av;->e:Z

    if-nez v0, :cond_6

    .line 111
    invoke-static {v8, v9, v2, v3, v1}, Lof;->a(JJZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 112
    iput-boolean v1, p2, Lcom/twitter/library/av/playback/av;->e:Z

    .line 113
    iget-object v0, p0, Lof;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v0

    new-instance v2, Loo;

    iget-object v3, p0, Lof;->k:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v2, v3}, Loo;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {p0}, Lof;->b()Lcom/twitter/library/av/m;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lbix;->a(Lbiw;Lcom/twitter/library/av/m;)V

    .line 116
    :cond_6
    iget-boolean v0, p2, Lcom/twitter/library/av/playback/av;->f:Z

    if-nez v0, :cond_0

    .line 117
    invoke-static {v8, v9, v4, v5, v1}, Lof;->a(JJZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iput-boolean v1, p2, Lcom/twitter/library/av/playback/av;->f:Z

    .line 119
    iget-object v0, p0, Lof;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v0

    new-instance v1, Lop;

    iget-object v2, p0, Lof;->k:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v1, v2}, Lop;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {p0}, Lof;->b()Lcom/twitter/library/av/m;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbix;->a(Lbiw;Lcom/twitter/library/av/m;)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 81
    goto/16 :goto_1
.end method

.method public processLoop(Lbjt;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbjt;
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lof;->k:Lcom/twitter/model/av/AVMedia;

    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lof;->b:Lcom/twitter/library/av/playback/av;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/av;->a()V

    .line 66
    :cond_0
    return-void
.end method

.method public processPlay(Lbjx;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbjx;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lof;->b:Lcom/twitter/library/av/playback/av;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/av;->a()V

    .line 59
    return-void
.end method

.method public processReplay(Lbkd;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbkd;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lof;->b:Lcom/twitter/library/av/playback/av;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/av;->a()V

    .line 54
    return-void
.end method

.method public processTick(Lbki;)V
    .locals 2
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p1, Lbki;->b:Lcom/twitter/library/av/playback/aa;

    iget-object v1, p0, Lof;->b:Lcom/twitter/library/av/playback/av;

    invoke-virtual {p0, v0, v1}, Lof;->a(Lcom/twitter/library/av/playback/aa;Lcom/twitter/library/av/playback/av;)V

    .line 49
    return-void
.end method
