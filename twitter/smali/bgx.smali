.class public Lbgx;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:J

.field private final c:Lcom/twitter/library/provider/t;


# direct methods
.method public constructor <init>(Landroid/content/Context;JLcom/twitter/library/provider/t;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbgx;->a:Landroid/content/Context;

    .line 28
    iput-wide p2, p0, Lbgx;->b:J

    .line 29
    iput-object p4, p0, Lbgx;->c:Lcom/twitter/library/provider/t;

    .line 30
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/timeline/cf;)V
    .locals 7

    .prologue
    .line 36
    if-eqz p1, :cond_0

    .line 37
    iget-object v0, p1, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget v0, v0, Lcom/twitter/model/timeline/ar;->c:I

    packed-switch v0, :pswitch_data_0

    .line 79
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 40
    :pswitch_1
    invoke-virtual {p1}, Lcom/twitter/android/timeline/cf;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v0

    .line 41
    new-instance v6, Laut;

    iget-object v1, p0, Lbgx;->a:Landroid/content/Context;

    .line 42
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v6, v1}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 43
    invoke-static {v6}, Lawc;->a(Laut;)V

    .line 44
    iget-object v1, p0, Lbgx;->c:Lcom/twitter/library/provider/t;

    iget-wide v2, p0, Lbgx;->b:J

    iget v4, v0, Lcom/twitter/android/timeline/bg;->g:I

    iget-object v5, v0, Lcom/twitter/android/timeline/bg;->a:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/t;->b(JILjava/lang/String;Laut;)I

    move-result v0

    .line 46
    if-lez v0, :cond_0

    .line 47
    invoke-virtual {v6}, Laut;->a()V

    goto :goto_0

    .line 52
    :pswitch_2
    invoke-virtual {p1}, Lcom/twitter/android/timeline/cf;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v3

    .line 53
    new-instance v6, Laut;

    iget-object v0, p0, Lbgx;->a:Landroid/content/Context;

    .line 54
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {v6, v0}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 55
    invoke-static {v6}, Lawc;->a(Laut;)V

    .line 56
    iget-object v0, p0, Lbgx;->c:Lcom/twitter/library/provider/t;

    iget-wide v1, p0, Lbgx;->b:J

    iget v3, v3, Lcom/twitter/android/timeline/bg;->g:I

    iget-wide v4, p1, Lcom/twitter/android/timeline/cf;->d:J

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/provider/t;->a(JIJLaut;)I

    move-result v0

    .line 58
    if-lez v0, :cond_0

    .line 59
    invoke-virtual {v6}, Laut;->a()V

    goto :goto_0

    .line 37
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
