.class public abstract Lcfd;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final h:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcfd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 12
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/twitter/util/serialization/j;

    const/4 v1, 0x0

    const-class v2, Lcex;

    new-instance v3, Lcex$b;

    invoke-direct {v3}, Lcex$b;-><init>()V

    .line 13
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lces;

    new-instance v3, Lces$a;

    invoke-direct {v3}, Lces$a;-><init>()V

    .line 14
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcfc;

    new-instance v3, Lcfc$b;

    invoke-direct {v3}, Lcfc$b;-><init>()V

    .line 15
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcew;

    new-instance v3, Lcew$b;

    invoke-direct {v3}, Lcew$b;-><init>()V

    .line 16
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcev;

    new-instance v3, Lcev$b;

    invoke-direct {v3}, Lcev$b;-><init>()V

    .line 18
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lcfa;

    new-instance v3, Lcfa$a;

    invoke-direct {v3}, Lcfa$a;-><init>()V

    .line 20
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-class v2, Lcfj;

    new-instance v3, Lcfj$a;

    invoke-direct {v3}, Lcfj$a;-><init>()V

    .line 22
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-class v2, Lcet;

    new-instance v3, Lcet$a;

    invoke-direct {v3}, Lcet$a;-><init>()V

    .line 24
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-class v2, Lceu;

    new-instance v3, Lceu$a;

    invoke-direct {v3}, Lceu$a;-><init>()V

    .line 25
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-class v2, Lcff;

    new-instance v3, Lcff$a;

    invoke-direct {v3}, Lcff$a;-><init>()V

    .line 26
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    .line 12
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a([Lcom/twitter/util/serialization/j;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcfd;->h:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
