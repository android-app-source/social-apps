.class public Labk;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Lcom/twitter/ui/widget/TwitterButton;

.field private final c:Landroid/view/ViewGroup;

.field private final d:Landroid/view/View;

.field private final e:Lcom/twitter/ui/widget/ToggleTwitterButton;

.field private final f:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;I)V
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Labk;->a:Landroid/view/ViewGroup;

    .line 41
    iget-object v0, p0, Labk;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130505

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Labk;->d:Landroid/view/View;

    .line 42
    iget-object v0, p0, Labk;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130506

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/ToggleTwitterButton;

    iput-object v0, p0, Labk;->e:Lcom/twitter/ui/widget/ToggleTwitterButton;

    .line 43
    iget-object v0, p0, Labk;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130507

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Labk;->f:Landroid/widget/TextView;

    .line 44
    iget-object v0, p0, Labk;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130508

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Labk;->b:Lcom/twitter/ui/widget/TwitterButton;

    .line 45
    iget-object v0, p0, Labk;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130516

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Labk;->c:Landroid/view/ViewGroup;

    .line 46
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;)Labk;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Labk;

    const v1, 0x7f0401e9

    invoke-direct {v0, p0, v1}, Labk;-><init>(Landroid/view/LayoutInflater;I)V

    return-object v0
.end method

.method public static b(Landroid/view/LayoutInflater;)Labk;
    .locals 2

    .prologue
    .line 34
    new-instance v1, Labk;

    invoke-static {}, Lbsd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0401ef

    :goto_0
    invoke-direct {v1, p0, v0}, Labk;-><init>(Landroid/view/LayoutInflater;I)V

    return-object v1

    :cond_0
    const v0, 0x7f0401ee

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Labk;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Labk;->e:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-static {v0}, Lcrf;->d(Landroid/view/View;)Lrx/c;

    move-result-object v0

    new-instance v1, Labk$1;

    invoke-direct {v1, p0, p1}, Labk$1;-><init>(Labk;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 86
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Labk;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Labk;->e:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setToggledOn(Z)V

    .line 71
    iget-object v1, p0, Labk;->e:Lcom/twitter/ui/widget/ToggleTwitterButton;

    if-eqz p1, :cond_0

    const v0, 0x7f0a0557

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setText(I)V

    .line 72
    return-void

    .line 71
    :cond_0
    const v0, 0x7f0a0555

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Labk;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 59
    return-void
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Labk;->f:Landroid/widget/TextView;

    invoke-static {v0}, Lcrf;->d(Landroid/view/View;)Lrx/c;

    move-result-object v0

    new-instance v1, Labk$2;

    invoke-direct {v1, p0, p1}, Labk$2;-><init>(Labk;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 112
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Labk;->e:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setEnabled(Z)V

    .line 76
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Labk;->e:Lcom/twitter/ui/widget/ToggleTwitterButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setVisibility(I)V

    .line 63
    return-void
.end method

.method public c(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Labk;->b:Lcom/twitter/ui/widget/TwitterButton;

    invoke-static {v0}, Lcrf;->d(Landroid/view/View;)Lrx/c;

    move-result-object v0

    new-instance v1, Labk$3;

    invoke-direct {v1, p0, p1}, Labk$3;-><init>(Labk;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 130
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Labk;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Labk;->f:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 94
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Labk;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Labk;->b:Lcom/twitter/ui/widget/TwitterButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 120
    return-void
.end method

.method public h()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Labk;->c:Landroid/view/ViewGroup;

    return-object v0
.end method
