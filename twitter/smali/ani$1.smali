.class Lani$1;
.super Lcom/twitter/app/common/util/b$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lani;-><init>(Lcom/twitter/app/common/util/j;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lani;


# direct methods
.method constructor <init>(Lani;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lani$1;->a:Lani;

    invoke-direct {p0}, Lcom/twitter/app/common/util/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lani$1;->a:Lani;

    invoke-virtual {v0, p2}, Lani;->a(Landroid/content/res/Configuration;)V

    .line 89
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lani$1;->a:Lani;

    invoke-virtual {v0}, Lani;->e()V

    .line 83
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    invoke-interface {v0, p0}, Lcom/twitter/app/common/util/j;->b(Lcom/twitter/app/common/util/b$a;)V

    .line 84
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lani$1;->a:Lani;

    invoke-virtual {v0}, Lani;->c()V

    .line 68
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lani$1;->a:Lani;

    invoke-virtual {v0}, Lani;->b()V

    .line 63
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lani$1;->a:Lani;

    invoke-virtual {v0, p2}, Lani;->a(Landroid/os/Bundle;)V

    .line 78
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lani$1;->a:Lani;

    invoke-virtual {v0}, Lani;->a()V

    .line 58
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lani$1;->a:Lani;

    invoke-virtual {v0}, Lani;->d()V

    .line 73
    return-void
.end method
