.class public Lcks;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:Landroid/graphics/Point;

.field public b:I

.field public c:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x1

    iput v0, p0, Lcks;->b:I

    .line 19
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcks;->c:Landroid/graphics/Rect;

    .line 22
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcks;->a:Landroid/graphics/Point;

    .line 23
    return-void
.end method

.method private a(ILandroid/graphics/Point;Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 40
    iget v0, p2, Landroid/graphics/Point;->x:I

    if-ltz v0, :cond_0

    iget v0, p2, Landroid/graphics/Point;->y:I

    if-gez v0, :cond_1

    .line 41
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Only fixed size views are currently supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "DockLayoutParams.logicalPosition needs to be one of BOTTOM_LEFT or BOTTOM_RIGHT. See "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 60
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :pswitch_0
    iget v0, p3, Landroid/graphics/Rect;->left:I

    iget v1, p4, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    .line 49
    iget v0, p3, Landroid/graphics/Rect;->bottom:I

    iget v2, p2, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v2

    iget v2, p4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v2

    .line 64
    :goto_0
    new-instance v2, Landroid/graphics/PointF;

    int-to-float v1, v1

    int-to-float v0, v0

    invoke-direct {v2, v1, v0}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v2

    .line 53
    :pswitch_1
    iget v0, p3, Landroid/graphics/Rect;->right:I

    iget v1, p2, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    iget v1, p4, Landroid/graphics/Rect;->right:I

    sub-int v1, v0, v1

    .line 54
    iget v0, p3, Landroid/graphics/Rect;->bottom:I

    iget v2, p2, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v2

    iget v2, p4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v2

    .line 55
    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(Landroid/graphics/Rect;)Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 34
    iget v0, p0, Lcks;->b:I

    iget-object v1, p0, Lcks;->a:Landroid/graphics/Point;

    iget-object v2, p0, Lcks;->c:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1, p1, v2}, Lcks;->a(ILandroid/graphics/Point;Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method
