.class public Lbwp;
.super Lcom/twitter/library/service/j;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/timeline/bk;

.field private final b:Z

.field private final c:I

.field private final g:Lcom/twitter/library/provider/g;

.field private final i:I

.field private j:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;ZIILcom/twitter/library/provider/g;)V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lbwp;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/j;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 41
    iput-object p3, p0, Lbwp;->a:Lcom/twitter/android/timeline/bk;

    .line 42
    iput-boolean p4, p0, Lbwp;->b:Z

    .line 43
    iput p5, p0, Lbwp;->c:I

    .line 44
    iput p6, p0, Lbwp;->i:I

    .line 46
    invoke-virtual {p0}, Lbwp;->s()Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/provider/g;->a(Lcom/twitter/library/provider/t;)Lcom/twitter/library/provider/g;

    move-result-object v0

    .line 45
    invoke-static {p7, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/g;

    iput-object v0, p0, Lbwp;->g:Lcom/twitter/library/provider/g;

    .line 47
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->c:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {p0, v0}, Lbwp;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 48
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;IZII)Lbwp;
    .locals 8

    .prologue
    .line 74
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v7}, Lbwp;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;IZIILcom/twitter/library/provider/g;)Lbwp;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;IZIILcom/twitter/library/provider/g;)Lbwp;
    .locals 8

    .prologue
    .line 82
    if-eqz p7, :cond_0

    .line 83
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 85
    :cond_0
    new-instance v0, Lbwp;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p4

    move v5, p5

    move v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lbwp;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;ZIILcom/twitter/library/provider/g;)V

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 6

    .prologue
    .line 63
    invoke-virtual {p0}, Lbwp;->b()Laut;

    move-result-object v5

    .line 64
    iget-object v0, p0, Lbwp;->g:Lcom/twitter/library/provider/g;

    iget-object v1, p0, Lbwp;->a:Lcom/twitter/android/timeline/bk;

    iget-boolean v2, p0, Lbwp;->b:Z

    iget v3, p0, Lbwp;->c:I

    iget v4, p0, Lbwp;->i:I

    .line 65
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/g;->a(Lcom/twitter/android/timeline/bk;ZIILaut;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbwp;->j:Ljava/lang/String;

    .line 67
    invoke-virtual {v5}, Laut;->a()V

    .line 68
    return-void
.end method

.method b()Laut;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Lbwp;->t()Laut;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lbwp;->j:Ljava/lang/String;

    return-object v0
.end method
