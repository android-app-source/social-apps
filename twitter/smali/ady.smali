.class public Lady;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcak;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/i;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/List",
            "<",
            "Lcaf;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Ladt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 31
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lady;->a:Lcom/twitter/util/serialization/l;

    .line 32
    sget-object v0, Lcom/twitter/model/core/i;->a:Lcom/twitter/util/serialization/l;

    .line 33
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lady;->b:Lcom/twitter/util/serialization/l;

    .line 34
    sget-object v0, Lcaf;->a:Lcom/twitter/util/serialization/l;

    .line 35
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lady;->c:Lcom/twitter/util/serialization/l;

    .line 34
    return-void
.end method

.method public constructor <init>(Ladt;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcbp;-><init>()V

    .line 40
    iput-object p1, p0, Lady;->d:Ladt;

    .line 41
    return-void
.end method

.method private static a(Landroid/database/Cursor;II)Lcag;
    .locals 3

    .prologue
    .line 96
    new-instance v0, Lcag$a;

    invoke-direct {v0}, Lcag$a;-><init>()V

    .line 97
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcag$a;->a(Ljava/lang/String;)Lcag$a;

    move-result-object v1

    .line 99
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v2, Lady;->b:Lcom/twitter/util/serialization/l;

    .line 98
    invoke-static {v0, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcag$a;->a(Ljava/util/List;)Lcag$a;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcag$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcag;

    .line 96
    return-object v0
.end method

.method private static c(Landroid/database/Cursor;)Lcae;
    .locals 3

    .prologue
    .line 84
    new-instance v0, Lcae$a;

    invoke-direct {v0}, Lcae$a;-><init>()V

    const/16 v1, 0x1e

    .line 85
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcae$a;->a(Ljava/lang/String;)Lcae$a;

    move-result-object v1

    const/16 v0, 0x20

    .line 87
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v2, Lady;->c:Lcom/twitter/util/serialization/l;

    .line 86
    invoke-static {v0, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcae$a;->a(Ljava/util/List;)Lcae$a;

    move-result-object v0

    new-instance v1, Lcad;

    const/16 v2, 0x1f

    .line 90
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcad;-><init>(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v0, v1}, Lcae$a;->a(Lcad;)Lcae$a;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcae$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcae;

    .line 84
    return-object v0
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lady;->b(Landroid/database/Cursor;)Lcak;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 45
    invoke-static {p1}, Ladt;->b(Landroid/database/Cursor;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/database/Cursor;)Lcak;
    .locals 6

    .prologue
    .line 51
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lads;

    .line 52
    iget-object v1, p0, Lady;->d:Ladt;

    invoke-virtual {v1, p1}, Ladt;->a(Landroid/database/Cursor;)Lbzy;

    move-result-object v2

    .line 54
    const/16 v1, 0x21

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 55
    new-instance v1, Lcaj$a;

    invoke-direct {v1}, Lcaj$a;-><init>()V

    const/16 v4, 0x15

    const/16 v5, 0x16

    .line 56
    invoke-static {p1, v4, v5}, Lady;->a(Landroid/database/Cursor;II)Lcag;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcaj$a;->a(Lcag;)Lcaj$a;

    move-result-object v1

    const/16 v4, 0x22

    const/16 v5, 0x23

    .line 58
    invoke-static {p1, v4, v5}, Lady;->a(Landroid/database/Cursor;II)Lcag;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcaj$a;->b(Lcag;)Lcaj$a;

    move-result-object v1

    const/16 v4, 0x14

    .line 60
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcaj$a;->a(Ljava/lang/String;)Lcaj$a;

    move-result-object v1

    const/16 v4, 0x17

    .line 61
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcaj$a;->b(Ljava/lang/String;)Lcaj$a;

    move-result-object v4

    const/16 v1, 0x19

    .line 62
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    sget-object v5, Lady;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v1, v5}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-virtual {v0, v1}, Lads;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcaj$a;->a(Ljava/util/List;)Lcaj$a;

    move-result-object v4

    const/16 v1, 0x1b

    .line 64
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    sget-object v5, Lady;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v1, v5}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-virtual {v0, v1}, Lads;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcaj$a;->c(Ljava/util/List;)Lcaj$a;

    move-result-object v4

    const/16 v1, 0x1a

    .line 66
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    sget-object v5, Lady;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v1, v5}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-virtual {v0, v1}, Lads;->b(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcaj$a;->b(Ljava/util/List;)Lcaj$a;

    move-result-object v0

    const/16 v1, 0x26

    .line 68
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcaj$a;->f(Ljava/lang/String;)Lcaj$a;

    move-result-object v0

    const/16 v1, 0x1c

    .line 69
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcaj$a;->a(J)Lcaj$a;

    move-result-object v0

    iget-wide v4, v2, Lbzy;->b:J

    .line 70
    invoke-virtual {v0, v4, v5}, Lcaj$a;->b(J)Lcaj$a;

    move-result-object v0

    iget-wide v4, v2, Lbzy;->c:J

    .line 71
    invoke-virtual {v0, v4, v5}, Lcaj$a;->c(J)Lcaj$a;

    move-result-object v0

    const/16 v1, 0x1d

    .line 72
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcaj$a;->c(Ljava/lang/String;)Lcaj$a;

    move-result-object v0

    new-instance v1, Lcan;

    invoke-direct {v1, v3}, Lcan;-><init>(I)V

    .line 73
    invoke-virtual {v0, v1}, Lcaj$a;->a(Lcan;)Lcaj$a;

    move-result-object v0

    .line 74
    invoke-static {p1}, Lady;->c(Landroid/database/Cursor;)Lcae;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcaj$a;->a(Lcae;)Lcaj$a;

    move-result-object v0

    const/16 v1, 0x24

    .line 75
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcaj$a;->d(Ljava/lang/String;)Lcaj$a;

    move-result-object v0

    const/16 v1, 0x25

    .line 76
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcaj$a;->e(Ljava/lang/String;)Lcaj$a;

    move-result-object v0

    const/16 v1, 0x27

    .line 77
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcaj$a;->g(Ljava/lang/String;)Lcaj$a;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcaj$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcaj;

    .line 79
    new-instance v1, Lcak;

    invoke-direct {v1, v2, v0}, Lcak;-><init>(Lbzy;Lcaj;)V

    return-object v1
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 29
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lady;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
