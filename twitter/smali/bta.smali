.class public Lbta;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/database/model/i;


# direct methods
.method public constructor <init>(Lcom/twitter/database/model/i;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lbta;->a:Lcom/twitter/database/model/i;

    .line 46
    return-void
.end method

.method private a(Lcom/twitter/database/model/f;)Lcbi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/model/f;",
            ")",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lbta;->a:Lcom/twitter/database/model/i;

    invoke-static {v0}, Lcom/twitter/database/hydrator/c;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/c;

    move-result-object v0

    const-class v1, Lawz;

    const-class v2, Lcom/twitter/model/dms/q;

    .line 139
    invoke-virtual {v0, v1, p1, v2}, Lcom/twitter/database/hydrator/c;->b(Ljava/lang/Class;Lcom/twitter/database/model/f;Ljava/lang/Class;)Lcbi;

    move-result-object v0

    .line 138
    return-object v0
.end method

.method private b(Lcom/twitter/database/model/f;)Lcbi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/model/f;",
            ")",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lbta;->a:Lcom/twitter/database/model/i;

    invoke-static {v0}, Lcom/twitter/database/hydrator/c;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/c;

    move-result-object v0

    const-class v1, Laww;

    const-class v2, Lcom/twitter/model/dms/Participant;

    .line 220
    invoke-virtual {v0, v1, p1, v2}, Lcom/twitter/database/hydrator/c;->b(Ljava/lang/Class;Lcom/twitter/database/model/f;Ljava/lang/Class;)Lcbi;

    move-result-object v0

    .line 219
    return-object v0
.end method

.method private b(Ljava/util/Set;)Lcbi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation

    .prologue
    .line 272
    new-instance v0, Lcom/twitter/database/model/f$a;

    invoke-direct {v0}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v1, "conversation_participants_user_id"

    .line 273
    invoke-static {v1, p1}, Laux;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    .line 275
    invoke-virtual {v0}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v0

    .line 277
    invoke-direct {p0, v0}, Lbta;->b(Lcom/twitter/database/model/f;)Lcbi;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Iterable;Z)Lcbi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    const-string/jumbo v0, "conversations_conversation_id"

    invoke-static {v0, p1}, Laux;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 119
    if-nez p2, :cond_0

    .line 125
    :goto_0
    new-instance v1, Lcom/twitter/database/model/f$a;

    invoke-direct {v1}, Lcom/twitter/database/model/f$a;-><init>()V

    .line 126
    invoke-virtual {v1, v0}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v0

    .line 129
    invoke-direct {p0, v0}, Lbta;->a(Lcom/twitter/database/model/f;)Lcbi;

    move-result-object v0

    return-object v0

    .line 122
    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    const-string/jumbo v2, "conversations_title"

    invoke-static {v2}, Laux;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, Laux;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcbi;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    new-instance v0, Lcom/twitter/database/model/f$a;

    invoke-direct {v0}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v1, "conversation_participants_participant_type,conversation_participants_join_time ASC,CAST(conversation_participants_user_id AS INT)"

    .line 190
    invoke-virtual {v0, v1}, Lcom/twitter/database/model/f$a;->b(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    .line 191
    if-eqz p1, :cond_0

    .line 192
    sget-object v1, Laww;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    .line 194
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lbta;->b(Lcom/twitter/database/model/f;)Lcbi;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Collection;)Lcbi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    new-instance v0, Lcom/twitter/database/model/f$a;

    invoke-direct {v0}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v1, "conversation_participants_conversation_id"

    .line 207
    invoke-static {v1, p1}, Laux;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 206
    invoke-virtual {v0, v1}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v0

    .line 209
    invoke-direct {p0, v0}, Lbta;->b(Lcom/twitter/database/model/f;)Lcbi;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lbta;->a(Ljava/lang/String;J)Ljava/util/Map;

    move-result-object v0

    .line 146
    invoke-virtual {p0}, Lbta;->b()Lcbi;

    move-result-object v1

    .line 147
    invoke-static {v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    .line 148
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 150
    new-instance v1, Lbta$1;

    invoke-direct {v1, p0, v0}, Lbta$1;-><init>(Lbta;Ljava/util/Map;)V

    invoke-static {v2, v1}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Set;)Ljava/lang/Iterable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    invoke-direct {p0, p1}, Lbta;->b(Ljava/util/Set;)Lcbi;

    move-result-object v0

    .line 233
    new-instance v1, Lbta$2;

    invoke-direct {v1, p0}, Lbta$2;-><init>(Lbta;)V

    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v1

    .line 242
    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 245
    invoke-virtual {p0, v1}, Lbta;->a(Ljava/util/Collection;)Lcbi;

    move-result-object v2

    .line 246
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v3

    .line 247
    invoke-virtual {v2}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/Participant;

    .line 248
    iget-object v1, v0, Lcom/twitter/model/dms/Participant;->f:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 249
    iget-object v1, v0, Lcom/twitter/model/dms/Participant;->f:Ljava/lang/String;

    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v3, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    :cond_0
    iget-object v1, v0, Lcom/twitter/model/dms/Participant;->f:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    iget-wide v6, v0, Lcom/twitter/model/dms/Participant;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 253
    :cond_1
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 255
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lbta$3;

    invoke-direct {v1, p0, v3, p1}, Lbta$3;-><init>(Lbta;Ljava/util/Map;Ljava/util/Set;)V

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/dms/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Lcom/twitter/database/model/f$a;

    invoke-direct {v0}, Lcom/twitter/database/model/f$a;-><init>()V

    .line 81
    invoke-virtual {v0}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lbta;->a:Lcom/twitter/database/model/i;

    .line 84
    invoke-static {v1}, Lcom/twitter/database/hydrator/c;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/c;

    move-result-object v1

    const-class v2, Laxa;

    const-class v3, Lcom/twitter/model/dms/m;

    .line 85
    invoke-virtual {v1, v2, v0, v3}, Lcom/twitter/database/hydrator/c;->b(Ljava/lang/Class;Lcom/twitter/database/model/f;Ljava/lang/Class;)Lcbi;

    move-result-object v1

    .line 87
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v2

    .line 88
    invoke-virtual {v1}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/m;

    .line 89
    invoke-virtual {v0}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v4

    iget-object v4, v4, Lcom/twitter/model/dms/c;->f:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    goto :goto_0

    .line 92
    :cond_0
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 94
    invoke-virtual {v2}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public a(Ljava/lang/String;J)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 177
    .line 178
    invoke-virtual {p0, p1}, Lbta;->a(Ljava/lang/String;)Lcbi;

    move-result-object v0

    invoke-static {v0}, Lbtb;->a(Lcbi;)Ljava/util/Map;

    move-result-object v0

    .line 177
    invoke-static {v0, p2, p3}, Lbtb;->a(Ljava/util/Map;J)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcbi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    new-instance v0, Lcom/twitter/database/model/f$a;

    invoke-direct {v0}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v1, "conversations_sort_event_id DESC"

    .line 103
    invoke-virtual {v0, v1}, Lcom/twitter/database/model/f$a;->b(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v0

    .line 105
    invoke-direct {p0, v0}, Lbta;->a(Lcom/twitter/database/model/f;)Lcbi;

    move-result-object v0

    return-object v0
.end method
