.class public Lbsa;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:J


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lbsa;->a:Landroid/content/Context;

    .line 21
    iput-wide p2, p0, Lbsa;->b:J

    .line 22
    return-void
.end method

.method private b(J)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 47
    sget-object v0, Lcom/twitter/database/schema/a$v;->m:Landroid/net/Uri;

    .line 49
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 50
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iget-wide v2, p0, Lbsa;->b:J

    .line 47
    invoke-static {v0, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/util/android/d;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 41
    sget-object v0, Lcom/twitter/library/provider/m$a;->a:Landroid/net/Uri;

    iget-wide v2, p0, Lbsa;->b:J

    invoke-static {v0, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 42
    new-instance v0, Lcom/twitter/util/android/d;

    iget-object v1, p0, Lbsa;->a:Landroid/content/Context;

    sget-object v3, Lcom/twitter/library/provider/m$a;->b:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(ILjava/lang/String;)Lcom/twitter/util/android/d;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 27
    .line 28
    invoke-static {p1, p2}, Lcom/twitter/library/provider/m$b;->a(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-wide v2, p0, Lbsa;->b:J

    invoke-static {v0, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 29
    new-instance v0, Lcom/twitter/util/android/d;

    iget-object v1, p0, Lbsa;->a:Landroid/content/Context;

    sget-object v3, Lcom/twitter/library/provider/m$b;->b:[Ljava/lang/String;

    const-string/jumbo v6, "moments_guide_section_id ASC, _id"

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(J)Lcom/twitter/util/android/d;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 35
    invoke-direct {p0, p1, p2}, Lbsa;->b(J)Landroid/net/Uri;

    move-result-object v2

    .line 36
    new-instance v0, Lcom/twitter/util/android/d;

    iget-object v1, p0, Lbsa;->a:Landroid/content/Context;

    sget-object v3, Lbuj;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
