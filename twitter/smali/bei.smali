.class public Lbei;
.super Lbea;
.source "Twttr"


# instance fields
.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 31
    new-instance v2, Lcom/twitter/library/service/v;

    invoke-direct {v2, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lbei;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;ILjava/lang/String;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 36
    const-string/jumbo v2, "UpdatePushDevice"

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lbea;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    iput p4, p0, Lbei;->c:I

    .line 38
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 48
    invoke-virtual {p0}, Lbei;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "push_destinations"

    aput-object v3, v1, v2

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 50
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 51
    invoke-virtual {p0, v0}, Lbei;->a(Lcom/twitter/library/service/d$a;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(JI)V
    .locals 7

    .prologue
    .line 81
    invoke-virtual {p0}, Lbei;->S()Laut;

    move-result-object v6

    .line 82
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v1

    const/4 v5, 0x1

    move-wide v2, p1

    move v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/j;->a(JIZLaut;)I

    .line 83
    invoke-virtual {v6}, Laut;->a()V

    .line 86
    iget-object v0, p0, Lbei;->p:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v0

    .line 87
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/platform/notifications/t;->a(J)V

    .line 88
    return-void
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfz;",
            "Lcfy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-super {p0, p1, p2, p3}, Lbea;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    .line 69
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v1, 0x130

    if-ne v0, v1, :cond_2

    .line 70
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/library/service/u;->a(Z)V

    .line 71
    invoke-static {p3}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/i;

    invoke-virtual {p0, v0}, Lbei;->a(Lcom/twitter/library/api/i;)Lcfz;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lcfz;->a:Z

    if-eqz v1, :cond_1

    iget v0, v0, Lcfz;->b:I

    :goto_0
    iput v0, p0, Lbei;->h:I

    .line 73
    invoke-virtual {p0}, Lbei;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/v;

    iget-wide v0, v0, Lcom/twitter/library/service/v;->c:J

    iget v2, p0, Lbei;->h:I

    invoke-virtual {p0, v0, v1, v2}, Lbei;->a(JI)V

    .line 78
    :goto_1
    return-void

    .line 72
    :cond_1
    iget v0, p0, Lbei;->c:I

    goto :goto_0

    .line 75
    :cond_2
    invoke-static {p3}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/i;

    invoke-virtual {p0, v0}, Lbei;->b(Lcom/twitter/library/api/i;)Lcfy;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_3

    iget v0, v0, Lcfy;->b:I

    :goto_2
    iput v0, p0, Lbei;->g:I

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 25
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbei;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    const-string/jumbo v0, "app:twitter_service:gcm_registration:update_request"

    return-object v0
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfz;",
            "Lcfy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    const-class v0, Lcfz;

    const-class v1, Lcfy;

    invoke-static {v0, v1}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lbei;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
