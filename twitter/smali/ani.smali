.class public Lani;
.super Lanh;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lanh;-><init>()V

    .line 38
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lani;->a:Ljava/util/List;

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/twitter/app/common/base/BaseFragment;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Lanh;-><init>()V

    .line 38
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lani;->a:Ljava/util/List;

    .line 97
    new-instance v0, Lani$2;

    invoke-direct {v0, p0}, Lani$2;-><init>(Lani;)V

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/base/BaseFragment;->a(Lcom/twitter/app/common/util/d;)V

    .line 134
    return-void
.end method

.method public constructor <init>(Lcom/twitter/app/common/util/j;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lanh;-><init>()V

    .line 38
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lani;->a:Ljava/util/List;

    .line 54
    new-instance v0, Lani$1;

    invoke-direct {v0, p0}, Lani$1;-><init>(Lani;)V

    invoke-interface {p1, v0}, Lcom/twitter/app/common/util/j;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 91
    return-void
.end method

.method private f(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Lani;->b:Z

    if-eqz v0, :cond_0

    instance-of v0, p1, Lanl;

    if-eqz v0, :cond_0

    .line 241
    check-cast p1, Lanl;

    invoke-interface {p1}, Lanl;->aB_()V

    .line 243
    :cond_0
    return-void
.end method

.method private g(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lani;->c:Z

    if-eqz v0, :cond_0

    instance-of v0, p1, Lanf;

    if-eqz v0, :cond_0

    .line 247
    check-cast p1, Lanf;

    invoke-interface {p1}, Lanf;->aO_()V

    .line 249
    :cond_0
    return-void
.end method

.method private h(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lani;->c:Z

    if-eqz v0, :cond_0

    instance-of v0, p1, Lanf;

    if-eqz v0, :cond_0

    .line 253
    check-cast p1, Lanf;

    invoke-interface {p1}, Lanf;->ak_()V

    .line 255
    :cond_0
    return-void
.end method

.method private i(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lani;->b:Z

    if-eqz v0, :cond_0

    instance-of v0, p1, Lanl;

    if-eqz v0, :cond_0

    .line 259
    check-cast p1, Lanl;

    invoke-interface {p1}, Lanl;->d()V

    .line 261
    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Lanh;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lani;->c(Ljava/lang/Object;)Lani;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lani;->b:Z

    .line 177
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 178
    instance-of v2, v0, Lanl;

    if-eqz v2, :cond_0

    .line 179
    check-cast v0, Lanl;

    invoke-interface {v0}, Lanl;->aB_()V

    goto :goto_0

    .line 182
    :cond_1
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 232
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 233
    instance-of v2, v0, Lane;

    if-eqz v2, :cond_0

    .line 234
    check-cast v0, Lane;

    invoke-interface {v0, p1}, Lane;->a(Landroid/content/res/Configuration;)V

    goto :goto_0

    .line 237
    :cond_1
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 214
    invoke-static {p1}, Lank;->a(Landroid/os/Bundle;)Lank;

    move-result-object v1

    .line 215
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 216
    instance-of v3, v0, Lang;

    if-eqz v3, :cond_0

    .line 217
    check-cast v0, Lang;

    invoke-virtual {v1, v0}, Lank;->a(Lang;)V

    goto :goto_0

    .line 220
    :cond_1
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Lanh;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lani;->d(Ljava/lang/Object;)Lani;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lani;->c:Z

    .line 186
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 187
    instance-of v2, v0, Lanf;

    if-eqz v2, :cond_0

    .line 188
    check-cast v0, Lanf;

    invoke-interface {v0}, Lanf;->aO_()V

    goto :goto_0

    .line 191
    :cond_1
    return-void
.end method

.method public c(Ljava/lang/Object;)Lani;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    invoke-direct {p0, p1}, Lani;->f(Ljava/lang/Object;)V

    .line 141
    invoke-direct {p0, p1}, Lani;->g(Ljava/lang/Object;)V

    .line 142
    return-object p0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lani;->c:Z

    .line 195
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 196
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 197
    instance-of v2, v0, Lanf;

    if-eqz v2, :cond_0

    .line 198
    check-cast v0, Lanf;

    invoke-interface {v0}, Lanf;->ak_()V

    .line 195
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 201
    :cond_1
    return-void
.end method

.method public d(Ljava/lang/Object;)Lani;
    .locals 1

    .prologue
    .line 157
    invoke-virtual {p0, p1}, Lani;->e(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 158
    invoke-direct {p0, p1}, Lani;->h(Ljava/lang/Object;)V

    .line 159
    invoke-direct {p0, p1}, Lani;->i(Ljava/lang/Object;)V

    .line 161
    :cond_0
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 162
    return-object p0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 204
    const/4 v0, 0x0

    iput-boolean v0, p0, Lani;->b:Z

    .line 205
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 206
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 207
    instance-of v2, v0, Lanl;

    if-eqz v2, :cond_0

    .line 208
    check-cast v0, Lanl;

    invoke-interface {v0}, Lanl;->d()V

    .line 205
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 211
    :cond_1
    return-void
.end method

.method public e(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 223
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 224
    iget-object v0, p0, Lani;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 225
    instance-of v2, v0, Lanj;

    if-eqz v2, :cond_0

    .line 226
    check-cast v0, Lanj;

    invoke-interface {v0}, Lanj;->am_()V

    .line 223
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 229
    :cond_1
    return-void
.end method
