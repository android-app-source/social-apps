.class public Lacq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/twitter/media/ui/image/MediaImageView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Lcom/twitter/ui/widget/BadgeView;

.field private final f:Lacr;

.field private final g:Lcom/twitter/android/moments/ui/guide/k;

.field private final h:I

.field private final i:I


# direct methods
.method public constructor <init>(Landroid/view/View;Lacr;Lcom/twitter/android/moments/ui/guide/k;II)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lacq;->a:Landroid/view/View;

    .line 47
    const v0, 0x7f13052a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lacq;->b:Lcom/twitter/media/ui/image/MediaImageView;

    .line 48
    const v0, 0x7f1304f2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lacq;->c:Landroid/widget/TextView;

    .line 49
    const v0, 0x7f1304f4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lacq;->d:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f130527

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/BadgeView;

    iput-object v0, p0, Lacq;->e:Lcom/twitter/ui/widget/BadgeView;

    .line 51
    iput-object p2, p0, Lacq;->f:Lacr;

    .line 52
    iput-object p3, p0, Lacq;->g:Lcom/twitter/android/moments/ui/guide/k;

    .line 53
    iput p4, p0, Lacq;->i:I

    .line 54
    iput p5, p0, Lacq;->h:I

    .line 55
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lacq;
    .locals 7

    .prologue
    .line 34
    const v0, 0x7f040208

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 36
    invoke-static {v1}, Lacr;->a(Landroid/view/View;)Lacr;

    move-result-object v2

    .line 37
    new-instance v0, Lacq;

    new-instance v3, Lcom/twitter/android/moments/ui/guide/k;

    const v4, 0x7f130529

    .line 38
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/twitter/android/moments/ui/guide/k;-><init>(Landroid/view/View;)V

    .line 39
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f110177

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 40
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f1100e4

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lacq;-><init>(Landroid/view/View;Lacr;Lcom/twitter/android/moments/ui/guide/k;II)V

    .line 37
    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lacq;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lacq;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lacq;->a:Landroid/view/View;

    return-object v0
.end method

.method public b()Lcom/twitter/ui/widget/BadgeView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lacq;->e:Lcom/twitter/ui/widget/BadgeView;

    return-object v0
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lacq;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v0, p0, Lacq;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lacq;->d:Landroid/widget/TextView;

    iget v1, p0, Lacq;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 97
    return-void
.end method

.method public c()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lacq;->b:Lcom/twitter/media/ui/image/MediaImageView;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lacq;->c:Landroid/widget/TextView;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lacq;->d:Landroid/widget/TextView;

    aput-object v3, v1, v2

    .line 70
    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->a([Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    move-result-object v0

    iget-object v1, p0, Lacq;->f:Lacr;

    .line 71
    invoke-virtual {v1}, Lacr;->d()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 69
    return-object v0
.end method

.method public d()Lcom/twitter/media/ui/image/MediaImageView;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lacq;->b:Lcom/twitter/media/ui/image/MediaImageView;

    return-object v0
.end method

.method public e()Lcom/twitter/android/moments/ui/a;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lacq;->g:Lcom/twitter/android/moments/ui/guide/k;

    return-object v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lacq;->e:Lcom/twitter/ui/widget/BadgeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/BadgeView;->setVisibility(I)V

    .line 87
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lacq;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lacq;->d:Landroid/widget/TextView;

    const v1, 0x7f0a0554

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 115
    iget-object v0, p0, Lacq;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lacq;->d:Landroid/widget/TextView;

    iget v1, p0, Lacq;->h:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 117
    return-void
.end method
