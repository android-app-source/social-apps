.class public final Lbbl$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbbl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/av/a;",
            ">;"
        }
    .end annotation
.end field

.field final b:Landroid/content/Context;

.field final c:Lcom/twitter/library/client/Session;

.field d:Lcom/twitter/model/av/DynamicAd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbbl$a;->a:Ljava/util/List;

    .line 178
    iput-object p1, p0, Lbbl$a;->b:Landroid/content/Context;

    .line 179
    iput-object p2, p0, Lbbl$a;->c:Lcom/twitter/library/client/Session;

    .line 180
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/a;)Lbbl$a;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lbbl$a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    return-object p0
.end method

.method public a()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbbl;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v0, 0xa

    .line 203
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 204
    iget-object v1, p0, Lbbl$a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 205
    const/4 v2, 0x0

    .line 206
    if-le v1, v0, :cond_0

    :goto_0
    move v8, v0

    move v0, v2

    move v2, v8

    .line 207
    :goto_1
    if-ge v0, v1, :cond_2

    .line 208
    new-instance v4, Lbbl;

    iget-object v5, p0, Lbbl$a;->b:Landroid/content/Context;

    iget-object v6, p0, Lbbl$a;->c:Lcom/twitter/library/client/Session;

    iget-object v7, p0, Lbbl$a;->d:Lcom/twitter/model/av/DynamicAd;

    invoke-direct {v4, v5, v6, v7}, Lbbl;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/av/DynamicAd;)V

    .line 209
    iget-object v5, p0, Lbbl$a;->a:Ljava/util/List;

    invoke-interface {v5, v0, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbbl;->a(Ljava/util/List;)V

    .line 210
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    add-int/lit8 v0, v2, 0xa

    .line 213
    if-le v1, v0, :cond_1

    :goto_2
    move v8, v0

    move v0, v2

    move v2, v8

    .line 214
    goto :goto_1

    :cond_0
    move v0, v1

    .line 206
    goto :goto_0

    :cond_1
    move v0, v1

    .line 213
    goto :goto_2

    .line 216
    :cond_2
    return-object v3
.end method

.method public a(Lcom/twitter/model/av/DynamicAd;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lbbl$a;->d:Lcom/twitter/model/av/DynamicAd;

    .line 196
    return-void
.end method
