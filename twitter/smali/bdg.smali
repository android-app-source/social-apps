.class public Lbdg;
.super Lbfy;
.source "Twttr"


# instance fields
.field private final j:J

.field private final k:Lcom/twitter/model/core/TwitterUser;

.field private final l:Lrx/subjects/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/a",
            "<",
            "Lbdf;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lbdh;

.field private r:Lbdf;

.field private s:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;JILbdh;)V
    .locals 6

    .prologue
    .line 67
    const-class v0, Lbdg;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/service/v;

    invoke-direct {v3, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lbfy;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;I)V

    .line 49
    invoke-static {}, Lrx/subjects/a;->r()Lrx/subjects/a;

    move-result-object v0

    iput-object v0, p0, Lbdg;->l:Lrx/subjects/a;

    .line 68
    iput-wide p4, p0, Lbdg;->j:J

    .line 69
    iput-object p3, p0, Lbdg;->k:Lcom/twitter/model/core/TwitterUser;

    .line 70
    iput-object p7, p0, Lbdg;->m:Lbdh;

    .line 71
    new-instance v0, Lcom/twitter/library/service/f;

    invoke-direct {v0}, Lcom/twitter/library/service/f;-><init>()V

    new-instance v1, Lcom/twitter/library/service/g;

    invoke-direct {v1, p1}, Lcom/twitter/library/service/g;-><init>(Landroid/content/Context;)V

    .line 72
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/service/n;

    invoke-direct {v1}, Lcom/twitter/library/service/n;-><init>()V

    .line 73
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v0

    .line 71
    invoke-virtual {p0, v0}, Lbdg;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 74
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lbdg;
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lbdg;->s:Ljava/lang/String;

    .line 101
    return-object p0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 3

    .prologue
    .line 130
    invoke-super {p0, p1, p2, p3}, Lbfy;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    .line 131
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-static {p3}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/y;

    invoke-virtual {v0}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdf;

    iput-object v0, p0, Lbdg;->r:Lbdf;

    .line 133
    iget-object v0, p0, Lbdg;->l:Lrx/subjects/a;

    iget-object v1, p0, Lbdg;->r:Lbdf;

    invoke-virtual {v0, v1}, Lrx/subjects/a;->a(Ljava/lang/Object;)V

    .line 134
    iget-object v0, p0, Lbdg;->l:Lrx/subjects/a;

    invoke-virtual {v0}, Lrx/subjects/a;->by_()V

    .line 138
    :goto_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 139
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lbdg;->l:Lrx/subjects/a;

    new-instance v1, Landroid/accounts/NetworkErrorException;

    invoke-virtual {p2}, Lcom/twitter/library/service/u;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/accounts/NetworkErrorException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/subjects/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 44
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lbdg;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method public e()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lbdf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lbdg;->l:Lrx/subjects/a;

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lbdg;->h()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected g()Lcom/twitter/library/service/d$a;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 108
    invoke-super {p0}, Lbfy;->g()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->a:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 109
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "1.1"

    .line 110
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const-string/jumbo v2, "live_video"

    aput-object v2, v1, v4

    .line 111
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const-string/jumbo v2, "1"

    aput-object v2, v1, v4

    .line 112
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    iget-wide v2, p0, Lbdg;->j:J

    .line 113
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const-string/jumbo v2, "timeline"

    aput-object v2, v1, v4

    .line 114
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lbdg;->s:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 116
    const-string/jumbo v1, "timeline_id"

    iget-object v2, p0, Lbdg;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 118
    :cond_0
    return-object v0
.end method

.method protected h()Lcom/twitter/library/api/y;
    .locals 2

    .prologue
    .line 124
    const/16 v0, 0x2d

    iget-object v1, p0, Lbdg;->k:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0, v1}, Lcom/twitter/library/api/y;->a(ILcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected s()I
    .locals 1

    .prologue
    .line 144
    const/16 v0, 0xd

    return v0
.end method

.method protected t()Ljava/lang/String;
    .locals 4

    .prologue
    .line 150
    iget-object v0, p0, Lbdg;->m:Lbdh;

    iget-wide v2, p0, Lbdg;->j:J

    iget-object v1, p0, Lbdg;->s:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1}, Lbdh;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
