.class public Lbip;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbip$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lbiq;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lbiq;

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/av/i;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field private final g:Z

.field private final h:[J

.field private final i:[J

.field private final j:Z

.field private final k:Z

.field private final l:Lcom/twitter/library/provider/t;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;[J)V
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lbip;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;[JLcom/twitter/library/provider/t;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;[JLcom/twitter/library/provider/t;)V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lbip$a;

    invoke-direct {v0}, Lbip$a;-><init>()V

    invoke-virtual {v0, p1}, Lbip$a;->a(Landroid/content/Context;)Lbip$a;

    move-result-object v0

    invoke-virtual {v0, p2}, Lbip$a;->a(Lcom/twitter/library/client/Session;)Lbip$a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lbip$a;->a([J)Lbip$a;

    move-result-object v0

    .line 47
    invoke-virtual {v0, p4}, Lbip$a;->a(Lcom/twitter/library/provider/t;)Lbip$a;

    move-result-object v0

    .line 46
    invoke-direct {p0, v0}, Lbip;-><init>(Lbip$a;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lbip$a;)V
    .locals 3

    .prologue
    .line 61
    invoke-static {p1}, Lbip$a;->a(Lbip$a;)Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbip;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lbip$a;->b(Lbip$a;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 63
    invoke-static {p1}, Lbip$a;->c(Lbip$a;)Z

    move-result v0

    iput-boolean v0, p0, Lbip;->c:Z

    .line 64
    invoke-static {p1}, Lbip$a;->d(Lbip$a;)Z

    move-result v0

    iput-boolean v0, p0, Lbip;->g:Z

    .line 65
    invoke-static {p1}, Lbip$a;->e(Lbip$a;)[J

    move-result-object v0

    iput-object v0, p0, Lbip;->h:[J

    .line 66
    invoke-static {p1}, Lbip$a;->f(Lbip$a;)[J

    move-result-object v0

    iput-object v0, p0, Lbip;->i:[J

    .line 67
    invoke-static {p1}, Lbip$a;->g(Lbip$a;)Z

    move-result v0

    iput-boolean v0, p0, Lbip;->j:Z

    .line 68
    invoke-static {p1}, Lbip$a;->h(Lbip$a;)Z

    move-result v0

    iput-boolean v0, p0, Lbip;->k:Z

    .line 69
    invoke-static {p1}, Lbip$a;->i(Lbip$a;)Lcom/twitter/library/provider/t;

    move-result-object v0

    iput-object v0, p0, Lbip;->l:Lcom/twitter/library/provider/t;

    .line 71
    new-instance v0, Lbiq;

    invoke-direct {v0}, Lbiq;-><init>()V

    iput-object v0, p0, Lbip;->a:Lbiq;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbip;->b:Ljava/util/List;

    .line 73
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0}, Lbip;->g()Lcom/twitter/library/service/d$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lbiq;)V
    .locals 17

    .prologue
    .line 84
    invoke-super/range {p0 .. p3}, Lcom/twitter/library/service/b;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    .line 85
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    invoke-virtual/range {p3 .. p3}, Lbiq;->b()Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lbip;->b:Ljava/util/List;

    .line 88
    move-object/from16 v0, p0

    iget-object v1, v0, Lbip;->b:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 89
    invoke-virtual/range {p0 .. p0}, Lbip;->M()Lcom/twitter/library/service/v;

    move-result-object v15

    .line 90
    move-object/from16 v0, p0

    iget-object v1, v0, Lbip;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/twitter/model/av/i;

    .line 92
    move-object/from16 v0, p0

    iget-object v1, v0, Lbip;->l:Lcom/twitter/library/provider/t;

    invoke-virtual {v2}, Lcom/twitter/model/av/i;->b()Ljava/util/List;

    move-result-object v2

    iget-wide v3, v15, Lcom/twitter/library/service/v;->c:J

    const/16 v5, 0xa

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {v1 .. v14}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJZZZLjava/lang/String;ZLaut;Z)Ljava/util/Collection;

    goto :goto_0

    .line 97
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 27
    check-cast p3, Lbiq;

    invoke-virtual {p0, p1, p2, p3}, Lbip;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lbiq;)V

    return-void
.end method

.method protected b()Lbiq;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lbiq;

    invoke-direct {v0}, Lbiq;-><init>()V

    return-object v0
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/av/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lbip;->b:Ljava/util/List;

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lbip;->b()Lbiq;

    move-result-object v0

    return-object v0
.end method

.method g()Lcom/twitter/library/service/d$a;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 114
    invoke-virtual {p0}, Lbip;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->a:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 115
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "videos/suggestions"

    aput-object v3, v1, v2

    .line 116
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 118
    const-string/jumbo v1, "seed_status_ids"

    iget-object v2, p0, Lbip;->h:[J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;[J)Lcom/twitter/library/service/d$a;

    .line 120
    const-string/jumbo v1, "pc"

    iget-boolean v2, p0, Lbip;->c:Z

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 122
    const-string/jumbo v1, "allow_nsfw"

    iget-boolean v2, p0, Lbip;->g:Z

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 124
    iget-object v1, p0, Lbip;->i:[J

    if-eqz v1, :cond_0

    .line 125
    const-string/jumbo v1, "excluded_status_ids"

    iget-object v2, p0, Lbip;->i:[J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;[J)Lcom/twitter/library/service/d$a;

    .line 128
    :cond_0
    const-string/jumbo v1, "include_pro_video"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 130
    const-string/jumbo v1, "include_consumer_video"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 132
    const-string/jumbo v1, "include_vine"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 134
    const-string/jumbo v1, "include_gif"

    iget-boolean v2, p0, Lbip;->j:Z

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 136
    const-string/jumbo v1, "include_periscope"

    iget-boolean v2, p0, Lbip;->k:Z

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 138
    const-string/jumbo v1, "include_entities"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_media_features"

    .line 140
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_user_entities"

    .line 141
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 143
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->d()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 138
    return-object v0
.end method
