.class public Lcez;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcez$b;,
        Lcez$a;
    }
.end annotation


# static fields
.field public static final a:Lcez$b;


# instance fields
.field public final b:Lcom/twitter/model/moments/r;

.field public final c:Lcom/twitter/model/moments/e;

.field public final d:Lcom/twitter/model/moments/w;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcez$b;

    invoke-direct {v0}, Lcez$b;-><init>()V

    sput-object v0, Lcez;->a:Lcez$b;

    return-void
.end method

.method public constructor <init>(Lcez$a;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iget-object v0, p1, Lcez$a;->a:Lcom/twitter/model/moments/r;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/r;

    iput-object v0, p0, Lcez;->b:Lcom/twitter/model/moments/r;

    .line 32
    iget-object v0, p1, Lcez$a;->b:Lcom/twitter/model/moments/e;

    iput-object v0, p0, Lcez;->c:Lcom/twitter/model/moments/e;

    .line 33
    iget-object v0, p1, Lcez$a;->c:Lcom/twitter/model/moments/w;

    iput-object v0, p0, Lcez;->d:Lcom/twitter/model/moments/w;

    .line 34
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcez;->c:Lcom/twitter/model/moments/e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
