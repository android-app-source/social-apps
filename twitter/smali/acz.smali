.class public Lacz;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

.field private final b:Lcom/twitter/library/av/playback/ac;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;Lcom/twitter/library/av/playback/ac;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    .line 33
    iput-object p2, p0, Lacz;->b:Lcom/twitter/library/av/playback/ac;

    .line 34
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;)Lacz;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lacz;

    new-instance v1, Lcom/twitter/library/av/playback/ac;

    invoke-direct {v1}, Lcom/twitter/library/av/playback/ac;-><init>()V

    invoke-direct {v0, p0, v1}, Lacz;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;Lcom/twitter/library/av/playback/ac;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/g;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 37
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/g;->b()Lcom/twitter/model/moments/MomentPageDisplayMode;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/MomentPageDisplayMode;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-eq v0, v1, :cond_0

    .line 39
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->c()V

    .line 103
    :goto_0
    return-void

    .line 42
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 43
    sget-object v1, Lacz$1;->b:[I

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 99
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->c()V

    goto :goto_0

    .line 45
    :pswitch_0
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 46
    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 47
    iget-object v1, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b()V

    .line 48
    iget-object v1, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    const v2, 0x7f020575

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b(I)V

    .line 49
    iget-object v1, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :cond_1
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->c()V

    goto :goto_0

    .line 56
    :pswitch_1
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b()V

    .line 57
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    const v1, 0x7f020285

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b(I)V

    .line 58
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0, v3}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :pswitch_2
    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    .line 63
    sget-object v1, Lacz$1;->a:[I

    iget-object v2, v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 92
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->c()V

    goto :goto_0

    .line 65
    :pswitch_3
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    const v1, 0x7f0203f6

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b(I)V

    .line 66
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0, v3}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->a(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b()V

    goto :goto_0

    .line 71
    :pswitch_4
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    const v1, 0x7f0206b6

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b(I)V

    .line 72
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0, v3}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->a(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b()V

    goto/16 :goto_0

    .line 78
    :pswitch_5
    iget-object v1, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v1, v4}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b(I)V

    .line 79
    iget-object v1, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    iget-object v2, p0, Lacz;->b:Lcom/twitter/library/av/playback/ac;

    .line 81
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 80
    invoke-virtual {v2, v0}, Lcom/twitter/library/av/playback/ac;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->a(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b()V

    goto/16 :goto_0

    .line 86
    :pswitch_6
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0, v4}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b(I)V

    .line 87
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    const v1, 0x7f0a0561

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->a(I)V

    .line 88
    iget-object v0, p0, Lacz;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b()V

    goto/16 :goto_0

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 63
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
