.class public Lcet$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcet;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcet;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 38
    new-instance v1, Lcet;

    sget-object v0, Lcom/twitter/model/moments/w;->a:Lcom/twitter/util/serialization/l;

    .line 39
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/w;

    invoke-direct {v1, v0}, Lcet;-><init>(Lcom/twitter/model/moments/w;)V

    .line 38
    return-object v1
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p2, Lcet;->a:Lcom/twitter/model/moments/w;

    sget-object v1, Lcom/twitter/model/moments/w;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 32
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    check-cast p2, Lcet;

    invoke-virtual {p0, p1, p2}, Lcet$a;->a(Lcom/twitter/util/serialization/o;Lcet;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p0, p1, p2}, Lcet$a;->a(Lcom/twitter/util/serialization/n;I)Lcet;

    move-result-object v0

    return-object v0
.end method
