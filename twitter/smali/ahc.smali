.class public Lahc;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# instance fields
.field private final a:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 25
    iput-object p1, p0, Lahc;->a:Landroid/app/Activity;

    .line 26
    return-void
.end method

.method static synthetic a(Lahc;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lahc;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private a(Lcom/twitter/android/dogfood/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lahc;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v0

    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Thanks for submitting a bad search!\n\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "What (user, tweet, image, etc): \n\n"

    .line 46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "Expected results: \n\n"

    .line 47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "Actual results: \n\n\n\n"

    .line 48
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "-------------------------\n\n"

    .line 49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 50
    invoke-virtual {p1}, Lcom/twitter/android/dogfood/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n\n"

    .line 51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/network/ab;->c:Lcom/twitter/library/network/ae;

    .line 52
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 45
    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 29
    iget-object v0, p0, Lahc;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/twitter/android/dogfood/a;->a(Landroid/content/Context;)Lcom/twitter/android/dogfood/a;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lahc;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0c1c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 31
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Bad search for ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "] from Android"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 32
    invoke-direct {p0, v0, p1}, Lahc;->a(Lcom/twitter/android/dogfood/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 33
    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/dogfood/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lrx/g;

    move-result-object v0

    new-instance v1, Lahc$1;

    invoke-direct {v1, p0}, Lahc$1;-><init>(Lahc;)V

    .line 34
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 40
    return-void
.end method
