.class Laez$b;
.super Ltv/periscope/android/ui/chat/j;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laez;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field final a:Ltv/periscope/android/view/MaskImageView;

.field final b:Landroid/widget/TextView;

.field final c:Landroid/widget/TextView;

.field final d:Landroid/view/ViewGroup;

.field final e:Ldae;

.field final f:Landroid/content/res/Resources;

.field private g:Laap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laap",
            "<",
            "Laba;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;Ldae;)V
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ltv/periscope/android/ui/chat/j;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    .line 157
    const v0, 0x7f130626

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/view/MaskImageView;

    iput-object v0, p0, Laez$b;->a:Ltv/periscope/android/view/MaskImageView;

    .line 158
    const v0, 0x7f13009a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laez$b;->b:Landroid/widget/TextView;

    .line 159
    const v0, 0x7f1301e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laez$b;->c:Landroid/widget/TextView;

    .line 160
    const v0, 0x7f130627

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Laez$b;->d:Landroid/view/ViewGroup;

    .line 161
    iput-object p2, p0, Laez$b;->e:Ldae;

    .line 162
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Laez$b;->f:Landroid/content/res/Resources;

    .line 163
    return-void
.end method

.method static synthetic a(Laez$b;)Laap;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Laez$b;->g:Laap;

    return-object v0
.end method

.method static synthetic a(Laez$b;Laap;)Laap;
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Laez$b;->g:Laap;

    return-object p1
.end method


# virtual methods
.method a(Ltv/periscope/android/ui/chat/h;)V
    .locals 6

    .prologue
    .line 166
    iget-object v0, p0, Laez$b;->b:Landroid/widget/TextView;

    iget-object v1, p1, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v1}, Ltv/periscope/model/chat/Message;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v0, p0, Laez$b;->c:Landroid/widget/TextView;

    iget-object v1, p0, Laez$b;->f:Landroid/content/res/Resources;

    const v2, 0x7f0a063a

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    .line 168
    invoke-virtual {v5}, Ltv/periscope/model/chat/Message;->j()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 167
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v0, p0, Laez$b;->a:Ltv/periscope/android/view/MaskImageView;

    invoke-virtual {v0}, Ltv/periscope/android/view/MaskImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 170
    iget-object v1, p0, Laez$b;->e:Ldae;

    iget-object v2, p1, Ltv/periscope/android/ui/chat/h;->a:Ltv/periscope/model/chat/Message;

    invoke-virtual {v2}, Ltv/periscope/model/chat/Message;->m()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Laez$b;->a:Ltv/periscope/android/view/MaskImageView;

    invoke-interface {v1, v0, v2, v3}, Ldae;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 171
    return-void
.end method
