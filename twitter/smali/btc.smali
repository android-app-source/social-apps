.class public Lbtc;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcom/twitter/model/core/Tweet$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lbtc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lbtc;

    invoke-direct {v0}, Lbtc;-><init>()V

    sput-object v0, Lbtc;->a:Lbtc;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcbp;-><init>()V

    .line 39
    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet$a;
    .locals 12

    .prologue
    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 44
    new-instance v10, Lcom/twitter/model/core/Tweet$a;

    invoke-direct {v10}, Lcom/twitter/model/core/Tweet$a;-><init>()V

    .line 45
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 47
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0}, Lcpd;->b()Lcpa;

    move-result-object v11

    .line 48
    invoke-virtual {v11}, Lcpa;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 49
    const-string/jumbo v0, "tweet_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v11, v0, v5}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    .line 54
    :goto_0
    :try_start_0
    invoke-virtual {v10, v8, v9}, Lcom/twitter/model/core/Tweet$a;->g(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    .line 55
    invoke-virtual {v0, v8, v9}, Lcom/twitter/model/core/Tweet$a;->e(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x17

    .line 56
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/twitter/model/core/Tweet$a;->h(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x15

    .line 57
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/twitter/model/core/Tweet$a;->f(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x16

    .line 58
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x18

    .line 59
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/4 v8, 0x5

    .line 60
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/twitter/model/core/Tweet$a;->d(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/4 v8, 0x3

    .line 61
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->h(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/4 v8, 0x4

    .line 62
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->d(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/4 v8, 0x2

    .line 63
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x1a

    .line 64
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->i(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    .line 65
    const/16 v0, 0x12

    .line 66
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v8, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 65
    invoke-static {v0, v8}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x19

    .line 67
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/twitter/model/core/Tweet$a;->a(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/4 v8, 0x7

    .line 68
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->e(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x8

    .line 69
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/twitter/model/core/Tweet$a;->b(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0xa

    .line 70
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->f(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x9

    .line 71
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/twitter/model/core/Tweet$a;->c(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0xb

    .line 72
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    .line 73
    const/16 v0, 0x14

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 74
    if-ne v8, v4, :cond_6

    move v0, v4

    :goto_1
    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->c(Z)Lcom/twitter/model/core/Tweet$a;

    move-result-object v9

    const/16 v0, 0x2b

    .line 75
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v4, :cond_7

    move v0, v4

    :goto_2
    invoke-virtual {v9, v0}, Lcom/twitter/model/core/Tweet$a;->d(Z)Lcom/twitter/model/core/Tweet$a;

    .line 77
    const-string/jumbo v0, "rt_status_groups_ref_id"

    .line 78
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 80
    const/4 v0, 0x4

    if-ne v8, v0, :cond_8

    move v0, v4

    :goto_3
    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->f(Z)Lcom/twitter/model/core/Tweet$a;

    move-result-object v9

    const/4 v0, 0x7

    if-ne v8, v0, :cond_9

    move v0, v4

    .line 81
    :goto_4
    invoke-virtual {v9, v0}, Lcom/twitter/model/core/Tweet$a;->i(Z)Lcom/twitter/model/core/Tweet$a;

    move-result-object v8

    const/16 v0, 0xd

    .line 82
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v4, :cond_a

    move v0, v4

    :goto_5
    invoke-virtual {v8, v0}, Lcom/twitter/model/core/Tweet$a;->a(Z)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x23

    .line 83
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->h(I)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x22

    .line 84
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->d(I)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x26

    .line 85
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/twitter/model/core/Tweet$a;->m(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x20

    .line 86
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->g(I)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x21

    .line 87
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/twitter/model/core/Tweet$a;->k(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x24

    .line 88
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->j(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x25

    .line 89
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->k(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x1b

    .line 90
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->a(I)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x1d

    .line 91
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->b(I)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0x13

    .line 92
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->c(I)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v8, 0xc

    .line 93
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/Tweet$a;->f(I)Lcom/twitter/model/core/Tweet$a;

    move-result-object v8

    iget v0, v10, Lcom/twitter/model/core/Tweet$a;->M:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_b

    move v0, v4

    .line 94
    :goto_6
    invoke-virtual {v8, v0}, Lcom/twitter/model/core/Tweet$a;->b(Z)Lcom/twitter/model/core/Tweet$a;

    move-result-object v8

    iget v0, v10, Lcom/twitter/model/core/Tweet$a;->M:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_c

    move v0, v4

    .line 95
    :goto_7
    invoke-virtual {v8, v0}, Lcom/twitter/model/core/Tweet$a;->g(Z)Lcom/twitter/model/core/Tweet$a;

    move-result-object v8

    const/16 v0, 0x1c

    .line 97
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v9, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 96
    invoke-static {v0, v9}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    invoke-virtual {v8, v0}, Lcom/twitter/model/core/Tweet$a;->a(Lcgi;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v8

    const/16 v0, 0xe

    .line 98
    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_d

    const/16 v0, 0xf

    .line 99
    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v4

    .line 98
    :goto_8
    invoke-virtual {v8, v0}, Lcom/twitter/model/core/Tweet$a;->e(Z)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    iget-boolean v8, v10, Lcom/twitter/model/core/Tweet$a;->u:Z

    if-eqz v8, :cond_e

    const/16 v8, 0xe

    .line 100
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v8

    :goto_9
    invoke-virtual {v0, v8, v9}, Lcom/twitter/model/core/Tweet$a;->a(D)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    iget-boolean v8, v10, Lcom/twitter/model/core/Tweet$a;->u:Z

    if-eqz v8, :cond_0

    const/16 v6, 0xf

    .line 101
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    :cond_0
    invoke-virtual {v0, v6, v7}, Lcom/twitter/model/core/Tweet$a;->b(D)Lcom/twitter/model/core/Tweet$a;

    move-result-object v6

    const/16 v0, 0x27

    .line 103
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v7, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    .line 102
    invoke-static {v0, v7}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/TwitterPlace;

    invoke-virtual {v6, v0}, Lcom/twitter/model/core/Tweet$a;->a(Lcom/twitter/model/geo/TwitterPlace;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v6

    const/16 v0, 0x28

    .line 105
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v7, Lcom/twitter/model/core/r;->a:Lcom/twitter/util/serialization/l;

    .line 104
    invoke-static {v0, v7}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/r;

    invoke-virtual {v6, v0}, Lcom/twitter/model/core/Tweet$a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v6, 0x29

    .line 106
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/twitter/model/core/Tweet$a;->l(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v6

    const/16 v0, 0x1f

    .line 108
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v7, Lcax;->a:Lcom/twitter/util/serialization/l;

    .line 107
    invoke-static {v0, v7}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    invoke-virtual {v6, v0}, Lcom/twitter/model/core/Tweet$a;->a(Lcax;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v6, 0x2e

    .line 109
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/twitter/model/core/Tweet$a;->i(I)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v6, 0x2f

    .line 110
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/twitter/model/core/Tweet$a;->e(I)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v6, 0x30

    .line 111
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/twitter/model/core/Tweet$a;->j(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v6, 0x31

    .line 112
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/twitter/model/core/Tweet$a;->o(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/16 v6, 0x32

    .line 113
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/twitter/model/core/Tweet$a;->o(I)Lcom/twitter/model/core/Tweet$a;

    move-result-object v6

    const/16 v0, 0x2d

    .line 114
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v7, Lcaq;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v7}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcaq;

    invoke-virtual {v6, v0}, Lcom/twitter/model/core/Tweet$a;->a(Lcaq;)Lcom/twitter/model/core/Tweet$a;

    .line 118
    const/16 v0, 0x2a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 119
    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-lez v0, :cond_f

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_a
    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->a(Ljava/lang/Long;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v6

    const/16 v0, 0x2c

    .line 120
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v7, Lcom/twitter/model/media/EditableMedia;->j:Lcom/twitter/util/serialization/l;

    .line 121
    invoke-static {v7}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v7

    .line 120
    invoke-static {v0, v7}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v6, v0}, Lcom/twitter/model/core/Tweet$a;->a(Ljava/util/List;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    const/4 v6, 0x0

    .line 122
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/twitter/model/core/Tweet$a;->i(J)Lcom/twitter/model/core/Tweet$a;

    move-result-object v6

    const/16 v0, 0x1e

    .line 123
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v4, :cond_10

    move v0, v4

    :goto_b
    invoke-virtual {v6, v0}, Lcom/twitter/model/core/Tweet$a;->h(Z)Lcom/twitter/model/core/Tweet$a;

    .line 125
    const-string/jumbo v0, "status_metadata_soc_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 127
    if-eq v0, v3, :cond_11

    .line 128
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 127
    :goto_c
    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->j(I)Lcom/twitter/model/core/Tweet$a;

    .line 130
    const-string/jumbo v0, "status_metadata_soc_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 131
    if-eq v0, v3, :cond_12

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_d
    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->l(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    .line 133
    const-string/jumbo v0, "status_metadata_soc_fav_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 135
    if-eq v0, v3, :cond_13

    .line 136
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 135
    :goto_e
    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->k(I)Lcom/twitter/model/core/Tweet$a;

    .line 138
    const-string/jumbo v0, "status_metadata_soc_rt_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 139
    if-eq v0, v3, :cond_14

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :goto_f
    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->m(I)Lcom/twitter/model/core/Tweet$a;

    .line 141
    const-string/jumbo v0, "status_metadata_soc_second_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 143
    if-eq v0, v3, :cond_15

    .line 144
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 143
    :goto_10
    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->m(Ljava/lang/String;)Lcom/twitter/model/core/Tweet$a;

    .line 146
    const-string/jumbo v0, "status_metadata_soc_others_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 148
    if-eq v0, v3, :cond_16

    .line 149
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 148
    :goto_11
    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->l(I)Lcom/twitter/model/core/Tweet$a;

    .line 151
    const-string/jumbo v0, "status_metadata_highlights"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 152
    if-eq v0, v3, :cond_1

    .line 154
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v4, Lcom/twitter/model/core/i;->a:Lcom/twitter/util/serialization/l;

    .line 155
    invoke-static {v4}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v4

    .line 153
    invoke-static {v0, v4}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 156
    if-nez v0, :cond_17

    move-object v0, v2

    :goto_12
    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->a([Lcom/twitter/model/core/d;)Lcom/twitter/model/core/Tweet$a;

    .line 160
    :cond_1
    const-string/jumbo v0, "timeline_flags"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 161
    if-eq v0, v3, :cond_18

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :goto_13
    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->n(I)Lcom/twitter/model/core/Tweet$a;

    .line 166
    const-string/jumbo v0, "timeline_scribe_content"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 167
    if-eq v0, v3, :cond_2

    .line 168
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    invoke-virtual {v10, v0}, Lcom/twitter/model/core/Tweet$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/core/Tweet$a;

    .line 171
    :cond_2
    const-string/jumbo v0, "timeline_sort_index"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 172
    if-eq v0, v3, :cond_3

    .line 173
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v10, v0, v1}, Lcom/twitter/model/core/Tweet$a;->n(J)Lcom/twitter/model/core/Tweet$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    :cond_3
    invoke-virtual {v11}, Lcpa;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 177
    const-string/jumbo v0, "tweet_id"

    invoke-virtual {v11, v0, v5}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    :cond_4
    return-object v10

    :cond_5
    move-object v5, v2

    .line 51
    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 74
    goto/16 :goto_1

    :cond_7
    move v0, v1

    .line 75
    goto/16 :goto_2

    :cond_8
    move v0, v1

    .line 80
    goto/16 :goto_3

    :cond_9
    move v0, v1

    goto/16 :goto_4

    :cond_a
    move v0, v1

    .line 82
    goto/16 :goto_5

    :cond_b
    move v0, v1

    .line 93
    goto/16 :goto_6

    :cond_c
    move v0, v1

    .line 94
    goto/16 :goto_7

    :cond_d
    move v0, v1

    .line 99
    goto/16 :goto_8

    :cond_e
    move-wide v8, v6

    .line 100
    goto/16 :goto_9

    :cond_f
    move-object v0, v2

    .line 119
    goto/16 :goto_a

    :cond_10
    move v0, v1

    .line 123
    goto/16 :goto_b

    :cond_11
    move v0, v3

    .line 128
    goto/16 :goto_c

    :cond_12
    move-object v0, v2

    .line 131
    goto/16 :goto_d

    :cond_13
    move v0, v1

    .line 136
    goto/16 :goto_e

    :cond_14
    move v0, v1

    .line 139
    goto/16 :goto_f

    :cond_15
    move-object v0, v2

    .line 144
    goto/16 :goto_10

    :cond_16
    move v0, v1

    .line 149
    goto/16 :goto_11

    .line 157
    :cond_17
    :try_start_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/twitter/model/core/d;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/model/core/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_12

    :cond_18
    move v0, v1

    .line 161
    goto :goto_13

    .line 176
    :catchall_0
    move-exception v0

    invoke-virtual {v11}, Lcpa;->d()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 177
    const-string/jumbo v1, "tweet_id"

    invoke-virtual {v11, v1, v5}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_19
    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lbtc;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    return-object v0
.end method
