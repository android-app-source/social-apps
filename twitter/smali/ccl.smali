.class public abstract Lccl;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccl$b;,
        Lccl$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lccl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/util/serialization/j;

    const/4 v1, 0x0

    const-class v2, Lccq;

    new-instance v3, Lccq$b;

    invoke-direct {v3}, Lccq$b;-><init>()V

    .line 25
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcct;

    new-instance v3, Lcct$b;

    invoke-direct {v3}, Lcct$b;-><init>()V

    .line 27
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    .line 24
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a([Lcom/twitter/util/serialization/j;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lccl;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method protected constructor <init>(Lccl$a;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lccl$a;->a(Lccl$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lccl;->b:Ljava/lang/String;

    .line 34
    return-void
.end method

.method private a(Lccl;)Z
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lccl;->b:Ljava/lang/String;

    iget-object v1, p1, Lccl;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lccl;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 51
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lccl;

    if-eqz v0, :cond_1

    check-cast p1, Lccl;

    .line 52
    invoke-direct {p0, p1}, Lccl;->a(Lccl;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 51
    :goto_0
    return v0

    .line 52
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lccl;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
