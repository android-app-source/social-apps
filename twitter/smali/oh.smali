.class public Loh;
.super Lbiy;
.source "Twttr"


# instance fields
.field a:Z

.field b:J

.field private final c:Lcom/twitter/library/av/playback/AVPlayer;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lbiy;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Loh;->a:Z

    .line 35
    iput-object p1, p0, Loh;->c:Lcom/twitter/library/av/playback/AVPlayer;

    .line 36
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 96
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Loh;->b:J

    .line 97
    iput-boolean p1, p0, Loh;->a:Z

    .line 98
    return-void
.end method


# virtual methods
.method public a(Lbiw;)Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    return v0
.end method

.method public processLoop(Lbjt;)V
    .locals 3
    .annotation runtime Lbiz;
        a = Lbjt;
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Loh;->c:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {p0}, Loh;->b()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/m;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    const-string/jumbo v2, "loop"

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    iget-object v2, p1, Lbjt;->a:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/model/av/AVMedia;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/m;)V

    .line 41
    return-void
.end method

.method public processPlayback25(Lol;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lol;
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Loh;->c:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v1, "playback_25"

    const/4 v2, 0x0

    iget-object v3, p1, Lol;->a:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V

    .line 68
    return-void
.end method

.method public processPlayback50(Lom;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lom;
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Loh;->c:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v1, "playback_50"

    const/4 v2, 0x0

    iget-object v3, p1, Lom;->a:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V

    .line 73
    return-void
.end method

.method public processPlayback75(Lon;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lon;
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Loh;->c:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v1, "playback_75"

    const/4 v2, 0x0

    iget-object v3, p1, Lon;->a:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V

    .line 78
    return-void
.end method

.method public processPlayback95(Loo;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Loo;
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Loh;->c:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v1, "playback_95"

    const/4 v2, 0x0

    iget-object v3, p1, Loo;->a:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V

    .line 83
    return-void
.end method

.method public processPlaybackComplete(Lop;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lop;
    .end annotation

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Loh;->a(Z)V

    .line 88
    iget-object v0, p0, Loh;->c:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v1, "playback_complete"

    const/4 v2, 0x0

    iget-object v3, p1, Lop;->a:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V

    .line 89
    return-void
.end method

.method public processPlaybackStart(Loq;)V
    .locals 8
    .annotation runtime Lbiz;
        a = Loq;
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 55
    new-instance v0, Lcom/twitter/library/av/m$a;

    invoke-direct {v0}, Lcom/twitter/library/av/m$a;-><init>()V

    .line 56
    iget-wide v2, p0, Loh;->b:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_0

    .line 57
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    iget-wide v4, p0, Loh;->b:J

    sub-long/2addr v2, v4

    .line 58
    iget-boolean v1, p0, Loh;->a:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/Boolean;)Lcom/twitter/library/av/m$a;

    .line 59
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/Long;)Lcom/twitter/library/av/m$a;

    .line 60
    iput-wide v6, p0, Loh;->b:J

    .line 62
    :cond_0
    iget-object v1, p0, Loh;->c:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v2, "playback_start"

    const/4 v3, 0x0

    iget-object v4, p1, Loq;->a:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/m$a;)V

    .line 63
    return-void
.end method

.method public processReplay(Lbkd;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbkd;
    .end annotation

    .prologue
    .line 45
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Loh;->a(Z)V

    .line 46
    return-void
.end method

.method public processShow(Lbkg;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbkg;
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Loh;->a(Z)V

    .line 51
    return-void
.end method
