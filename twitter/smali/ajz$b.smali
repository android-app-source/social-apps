.class Lajz$b;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lajz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Ljava/util/List",
        "<",
        "Lafu$a;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/revenue/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/revenue/h",
            "<",
            "Lafu;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/model/core/Tweet;

.field private final c:Lcom/twitter/library/widget/renderablecontent/DisplayMode;


# direct methods
.method constructor <init>(Lcom/twitter/android/revenue/h;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/revenue/h",
            "<",
            "Lafu;",
            ">;",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/library/widget/renderablecontent/DisplayMode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 212
    invoke-direct {p0}, Lcqw;-><init>()V

    .line 213
    iput-object p1, p0, Lajz$b;->a:Lcom/twitter/android/revenue/h;

    .line 214
    iput-object p2, p0, Lajz$b;->b:Lcom/twitter/model/core/Tweet;

    .line 215
    iput-object p3, p0, Lajz$b;->c:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 216
    return-void
.end method

.method static synthetic a(Lajz$b;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lajz$b;->b:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 206
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lajz$b;->a(Ljava/util/List;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lafu$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 220
    new-instance v0, Lajz$b$1;

    invoke-direct {v0, p0}, Lajz$b$1;-><init>(Lajz$b;)V

    .line 221
    invoke-static {p1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v0

    .line 230
    new-instance v2, Lcbl$a;

    invoke-direct {v2}, Lcbl$a;-><init>()V

    .line 231
    invoke-virtual {v2, v0}, Lcbl$a;->a(Ljava/lang/Iterable;)Lcbl$a;

    move-result-object v0

    invoke-virtual {v0}, Lcbl$a;->a()Lcbl;

    move-result-object v2

    .line 233
    iget-object v0, p0, Lajz$b;->a:Lcom/twitter/android/revenue/h;

    invoke-interface {v0, v2}, Lcom/twitter/android/revenue/h;->a(Lcbi;)V

    .line 234
    iget-object v0, p0, Lajz$b;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->af:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v3, p0, Lajz$b;->c:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-static {v0, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    .line 235
    invoke-static {}, Lajz;->f()Landroid/util/LruCache;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 236
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2}, Lcbl;->be_()I

    move-result v2

    if-ge v3, v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 237
    :goto_0
    iget-object v1, p0, Lajz$b;->a:Lcom/twitter/android/revenue/h;

    invoke-interface {v1, v0}, Lcom/twitter/android/revenue/h;->a(I)V

    .line 238
    return-void

    :cond_0
    move v0, v1

    .line 236
    goto :goto_0
.end method
