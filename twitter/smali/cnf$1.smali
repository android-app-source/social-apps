.class Lcnf$1;
.super Lcom/twitter/ui/view/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcnf;->a(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/ref/WeakReference;

.field final synthetic b:Lcom/twitter/model/core/ad;

.field final synthetic c:Lcnf;


# direct methods
.method constructor <init>(Lcnf;ILjava/lang/Integer;ZZLjava/lang/ref/WeakReference;Lcom/twitter/model/core/ad;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcnf$1;->c:Lcnf;

    iput-object p6, p0, Lcnf$1;->a:Ljava/lang/ref/WeakReference;

    iput-object p7, p0, Lcnf$1;->b:Lcom/twitter/model/core/ad;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/twitter/ui/view/a;-><init>(ILjava/lang/Integer;ZZ)V

    return-void
.end method


# virtual methods
.method public b()Z
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcnf$1;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcne;

    .line 205
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcnf$1;->b:Lcom/twitter/model/core/ad;

    invoke-interface {v0, v1}, Lcne;->b(Lcom/twitter/model/core/ad;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcnf$1;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcne;

    .line 197
    if-eqz v0, :cond_0

    .line 198
    iget-object v1, p0, Lcnf$1;->b:Lcom/twitter/model/core/ad;

    invoke-interface {v0, v1}, Lcne;->a(Lcom/twitter/model/core/ad;)V

    .line 200
    :cond_0
    return-void
.end method
