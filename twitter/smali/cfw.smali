.class public Lcfw;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcfw$b;,
        Lcfw$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcfw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcfv;

.field public final c:Lcfv;

.field public final d:Lcfv;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcfs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcfw$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcfw$b;-><init>(Lcfw$1;)V

    sput-object v0, Lcfw;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcfw$a;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcfw$a;->a(Lcfw$a;)Lcfv;

    move-result-object v0

    iput-object v0, p0, Lcfw;->b:Lcfv;

    .line 30
    invoke-static {p1}, Lcfw$a;->b(Lcfw$a;)Lcfv;

    move-result-object v0

    iput-object v0, p0, Lcfw;->c:Lcfv;

    .line 31
    invoke-static {p1}, Lcfw$a;->c(Lcfw$a;)Lcfv;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcfw$a;->b(Lcfw$a;)Lcfv;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcfw;->d:Lcfv;

    .line 32
    invoke-static {p1}, Lcfw$a;->d(Lcfw$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcfw;->e:Ljava/util/List;

    .line 33
    return-void

    .line 31
    :cond_0
    invoke-static {p1}, Lcfw$a;->c(Lcfw$a;)Lcfv;

    move-result-object v0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcfw$a;Lcfw$1;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcfw;-><init>(Lcfw$a;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcfw;->d:Lcfv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcfw;->d:Lcfv;

    iget-object v0, v0, Lcfv;->f:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
