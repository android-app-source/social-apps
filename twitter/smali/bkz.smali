.class public Lbkz;
.super Lbks;
.source "Twttr"


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:J


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lbks;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Ljava/lang/String;)V

    .line 27
    iput-object p3, p0, Lbkz;->e:Ljava/lang/String;

    .line 28
    iput-wide p4, p0, Lbkz;->f:J

    .line 29
    return-void
.end method


# virtual methods
.method protected b(Lcom/twitter/network/j;Lcom/twitter/network/HttpOperation;Ljava/util/Map;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/j;",
            "Lcom/twitter/network/HttpOperation;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ")",
            "Lcom/twitter/model/av/AVMediaPlaylist;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Lcom/twitter/model/av/VinePlaylist;

    invoke-virtual {p0}, Lbkz;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbkz;->e:Ljava/lang/String;

    iget-wide v4, p0, Lbkz;->f:J

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/twitter/model/av/VinePlaylist;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    return-object v0
.end method
