.class public Lbuo;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lrx/f;

.field private final b:Lrx/f;


# direct methods
.method public constructor <init>(Lrx/f;Lrx/f;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lbuo;->a:Lrx/f;

    .line 49
    iput-object p2, p0, Lbuo;->b:Lrx/f;

    .line 50
    return-void
.end method

.method public static a()Lbuo;
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->S()Lbuo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(JILjava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    invoke-static {p0, p1, p2, p3}, Lbuo;->b(JILjava/lang/String;)V

    return-void
.end method

.method private static b(JILjava/lang/String;)V
    .locals 6

    .prologue
    .line 99
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v1, "app"

    const-string/jumbo v2, ""

    const-string/jumbo v3, "feature_switches"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "fetch_listener_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 100
    invoke-static {v1, v2, v3, v4, p3}, Lcom/twitter/analytics/model/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(JLcom/twitter/analytics/model/a;)V

    .line 99
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 101
    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 3

    .prologue
    .line 53
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->b()Lcqs;

    move-result-object v0

    const-string/jumbo v1, "did_listen_for_fs_config_fetch"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcqs;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x5

    .line 55
    invoke-virtual {p0, p1, p2, v0}, Lbuo;->a(JI)V

    .line 57
    :cond_0
    return-void
.end method

.method protected a(JI)V
    .locals 5

    .prologue
    .line 60
    invoke-static {}, Lcon;->a()Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    .line 61
    invoke-virtual {v0, v1}, Lrx/c;->c(I)Lrx/c;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lrx/c;->k()Lrx/c;

    move-result-object v0

    int-to-long v2, p3

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lbuo;->b:Lrx/f;

    .line 63
    invoke-virtual {v0, v2, v3, v1, v4}, Lrx/c;->g(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lbuo$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lbuo$3;-><init>(Lbuo;JI)V

    .line 64
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/a;)Lrx/c;

    move-result-object v0

    new-instance v1, Lbuo$2;

    invoke-direct {v1, p0}, Lbuo$2;-><init>(Lbuo;)V

    .line 70
    invoke-virtual {v0, v1}, Lrx/c;->c(Lrx/functions/a;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lbuo;->a:Lrx/f;

    .line 78
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lbuo$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lbuo$1;-><init>(Lbuo;JI)V

    .line 79
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 95
    return-void
.end method
