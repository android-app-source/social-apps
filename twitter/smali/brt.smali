.class public Lbrt;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/twitter/util/math/Size;

.field private static final b:Lcom/twitter/util/math/Size;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x400

    .line 74
    const/16 v0, 0x500

    invoke-static {v0, v1}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v0

    sput-object v0, Lbrt;->a:Lcom/twitter/util/math/Size;

    .line 81
    invoke-static {v1}, Lcom/twitter/util/math/Size;->a(I)Lcom/twitter/util/math/Size;

    move-result-object v0

    sput-object v0, Lbrt;->b:Lcom/twitter/util/math/Size;

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/model/media/EditableImage;)Lcom/twitter/media/model/MediaFile;
    .locals 16

    .prologue
    .line 126
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 128
    new-instance v14, Lcom/twitter/media/util/b;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Lcom/twitter/media/util/b;-><init>(Lcom/twitter/model/media/EditableImage;)V

    .line 129
    invoke-virtual {v14}, Lcom/twitter/media/util/b;->c()Ljava/io/File;

    move-result-object v1

    .line 130
    if-eqz v1, :cond_1

    .line 131
    sget-object v2, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    invoke-static {v1, v2}, Lcom/twitter/media/model/MediaFile;->a(Ljava/io/File;Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/model/MediaFile;

    move-result-object v10

    .line 231
    :cond_0
    :goto_0
    return-object v10

    .line 133
    :cond_1
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    move-object v10, v1

    check-cast v10, Lcom/twitter/media/model/ImageFile;

    .line 134
    iget-object v11, v10, Lcom/twitter/media/model/ImageFile;->e:Ljava/io/File;

    .line 139
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    .line 140
    if-eqz v1, :cond_7

    move-object/from16 v0, p1

    iget v2, v0, Lcom/twitter/model/media/EditableImage;->e:I

    .line 142
    invoke-static {v2}, Lcom/twitter/media/util/ImageOrientation;->a(I)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v2

    .line 143
    invoke-virtual {v2}, Lcom/twitter/media/util/ImageOrientation;->b()Lcom/twitter/media/util/ImageOrientation;

    move-result-object v2

    .line 144
    invoke-virtual {v2, v1}, Lcom/twitter/media/util/ImageOrientation;->a(Lcom/twitter/util/math/c;)Lcom/twitter/util/math/c;

    move-result-object v1

    .line 146
    :goto_1
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v2

    invoke-virtual {v2}, Lcqq;->c()Lcqr;

    move-result-object v15

    .line 147
    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/twitter/util/math/c;->a()Z

    move-result v2

    if-nez v2, :cond_10

    .line 148
    sget-object v2, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    iget-object v2, v2, Lcom/twitter/media/model/MediaType;->extension:Ljava/lang/String;

    invoke-interface {v15, v2}, Lcqr;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 149
    if-eqz v2, :cond_10

    .line 152
    sget-object v3, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    iget-object v3, v3, Lcom/twitter/media/model/MediaType;->extension:Ljava/lang/String;

    invoke-interface {v15, v3}, Lcqr;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 153
    invoke-virtual {v10}, Lcom/twitter/media/model/ImageFile;->a()Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v0, v4, v3}, Lcqc;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/io/File;)Z

    .line 154
    new-instance v4, Lcom/twitter/media/service/tasks/CropTask;

    .line 155
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    iget v6, v0, Lcom/twitter/model/media/EditableImage;->e:I

    invoke-direct {v4, v5, v2, v1, v6}, Lcom/twitter/media/service/tasks/CropTask;-><init>(Ljava/lang/String;Ljava/io/File;Lcom/twitter/util/math/c;I)V

    .line 156
    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/twitter/media/service/tasks/CropTask;->d(Landroid/content/Context;)Z

    .line 157
    invoke-virtual {v4}, Lcom/twitter/media/service/tasks/CropTask;->b()Z

    move-result v1

    if-eqz v1, :cond_f

    move-object v1, v2

    .line 160
    :goto_2
    invoke-interface {v15, v3}, Lcqr;->b(Ljava/io/File;)Z

    move-object v12, v1

    .line 164
    :goto_3
    const/4 v13, 0x0

    .line 165
    invoke-static/range {p0 .. p0}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 166
    move-object/from16 v0, p1

    iget-boolean v1, v0, Lcom/twitter/model/media/EditableImage;->b:Z

    if-nez v1, :cond_2

    move-object/from16 v0, p1

    iget v1, v0, Lcom/twitter/model/media/EditableImage;->c:I

    invoke-static {v1}, Lcom/twitter/media/filters/c;->a(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 169
    :cond_2
    :try_start_0
    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    iget-object v1, v1, Lcom/twitter/media/model/MediaType;->extension:Ljava/lang/String;

    invoke-interface {v15, v1}, Lcqr;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 170
    if-eqz v3, :cond_9

    .line 171
    invoke-static {v12}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget v6, v0, Lcom/twitter/model/media/EditableImage;->c:I

    move-object/from16 v0, p1

    iget-boolean v7, v0, Lcom/twitter/model/media/EditableImage;->b:Z

    const/high16 v8, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    iget v9, v0, Lcom/twitter/model/media/EditableImage;->d:F

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v9}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/io/File;IIIZFF)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_8

    .line 173
    const/4 v1, 0x1

    move-object v12, v3

    :goto_4
    move v13, v1

    .line 185
    :cond_3
    :goto_5
    invoke-virtual {v12, v11}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 186
    invoke-static {v12}, Lcom/twitter/media/util/f;->a(Ljava/io/File;)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v1

    .line 188
    if-eqz v13, :cond_4

    sget-object v1, Lcom/twitter/media/util/ImageOrientation;->b:Lcom/twitter/media/util/ImageOrientation;

    :cond_4
    invoke-static {v11, v12, v1}, Lcom/twitter/media/util/f;->a(Ljava/io/File;Ljava/io/File;Lcom/twitter/media/util/ImageOrientation;)Z

    .line 191
    :cond_5
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/twitter/model/media/EditableImage;->h:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 193
    invoke-static {v12}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    const/4 v2, 0x1

    .line 194
    invoke-virtual {v1, v2}, Lcom/twitter/media/decoder/ImageDecoder;->b(Z)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    .line 195
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/media/EditableImage;->e()Lcom/twitter/util/math/Size;

    move-result-object v2

    .line 196
    invoke-virtual {v2}, Lcom/twitter/util/math/Size;->a()I

    move-result v3

    const/16 v4, 0x400

    if-ge v3, v4, :cond_6

    .line 197
    invoke-virtual {v2}, Lcom/twitter/util/math/Size;->b()I

    move-result v3

    const/16 v4, 0x400

    if-ge v3, v4, :cond_6

    .line 198
    sget-object v3, Lbrt;->b:Lcom/twitter/util/math/Size;

    invoke-virtual {v2, v3}, Lcom/twitter/util/math/Size;->c(Lcom/twitter/util/math/Size;)Lcom/twitter/util/math/Size;

    move-result-object v2

    .line 199
    const/4 v3, 0x1

    .line 200
    invoke-virtual {v1, v3}, Lcom/twitter/media/decoder/ImageDecoder;->a(Z)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v3

    .line 201
    invoke-virtual {v3, v2}, Lcom/twitter/media/decoder/ImageDecoder;->c(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/ImageDecoder;

    .line 203
    :cond_6
    invoke-virtual {v1}, Lcom/twitter/media/decoder/ImageDecoder;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 204
    if-nez v1, :cond_a

    .line 205
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 144
    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 176
    :cond_8
    :try_start_1
    invoke-interface {v15, v3}, Lcqr;->b(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    :cond_9
    move v1, v13

    goto :goto_4

    .line 179
    :catch_0
    move-exception v1

    .line 180
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_5

    .line 207
    :cond_a
    new-instance v2, Lbrf;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lbrf;-><init>(Lcom/twitter/model/media/EditableImage;)V

    .line 208
    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v1}, Lbrf;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 209
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 211
    :cond_b
    invoke-virtual {v12, v11}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 212
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v2

    invoke-virtual {v2}, Lcqq;->c()Lcqr;

    move-result-object v2

    invoke-interface {v2, v12}, Lcqr;->c(Ljava/io/File;)Lrx/g;

    .line 214
    :cond_c
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x5f

    invoke-static {v1, v2, v3}, Lcom/twitter/media/util/a;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)Ljava/io/File;

    move-result-object v12

    .line 215
    if-nez v12, :cond_d

    .line 216
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 220
    :cond_d
    invoke-virtual {v12, v11}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 224
    invoke-static/range {p0 .. p0}, Lcom/twitter/media/util/o;->a(Landroid/content/Context;)Lcom/twitter/media/util/o;

    move-result-object v1

    .line 225
    invoke-virtual {v1, v14, v12}, Lcom/twitter/media/util/o;->a(Lcom/twitter/media/util/o$a;Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    .line 226
    if-eqz v1, :cond_e

    .line 227
    invoke-interface {v15, v12}, Lcqr;->b(Ljava/io/File;)Z

    move-object v12, v1

    .line 230
    :cond_e
    invoke-interface {v15, v11}, Lcqr;->b(Ljava/io/File;)Z

    .line 231
    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    invoke-static {v12, v1}, Lcom/twitter/media/model/MediaFile;->a(Ljava/io/File;Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/model/MediaFile;

    move-result-object v10

    goto/16 :goto_0

    :cond_f
    move-object v1, v11

    goto/16 :goto_2

    :cond_10
    move-object v12, v11

    goto/16 :goto_3
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/media/EditableMedia;)Lcom/twitter/media/model/MediaFile;
    .locals 2

    .prologue
    .line 86
    sget-object v0, Lbrt$2;->a:[I

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 100
    iget-object v0, p1, Lcom/twitter/model/media/EditableMedia;->k:Lcom/twitter/media/model/MediaFile;

    :goto_0
    return-object v0

    .line 88
    :pswitch_0
    check-cast p1, Lcom/twitter/model/media/EditableAnimatedGif;

    invoke-static {p1}, Lbrt;->a(Lcom/twitter/model/media/EditableAnimatedGif;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    goto :goto_0

    .line 91
    :pswitch_1
    check-cast p1, Lcom/twitter/model/media/EditableImage;

    invoke-static {p0, p1}, Lbrt;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableImage;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    goto :goto_0

    .line 94
    :pswitch_2
    check-cast p1, Lcom/twitter/model/media/EditableVideo;

    invoke-static {p1}, Lbrt;->a(Lcom/twitter/model/media/EditableVideo;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    goto :goto_0

    .line 97
    :pswitch_3
    check-cast p1, Lcom/twitter/model/media/EditableSegmentedVideo;

    invoke-static {p0, p1}, Lbrt;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableSegmentedVideo;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/model/media/EditableSegmentedVideo;)Lcom/twitter/media/model/MediaFile;
    .locals 3

    .prologue
    .line 276
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 277
    invoke-static {p0}, Lcom/twitter/media/util/o;->a(Landroid/content/Context;)Lcom/twitter/media/util/o;

    move-result-object v0

    .line 278
    new-instance v2, Lcom/twitter/media/util/c;

    invoke-direct {v2, p1}, Lcom/twitter/media/util/c;-><init>(Lcom/twitter/model/media/EditableSegmentedVideo;)V

    .line 279
    invoke-virtual {v2}, Lcom/twitter/media/util/c;->c()Ljava/io/File;

    move-result-object v1

    .line 280
    if-eqz v1, :cond_0

    .line 281
    sget-object v0, Lcom/twitter/media/model/MediaType;->d:Lcom/twitter/media/model/MediaType;

    invoke-static {v1, v0}, Lcom/twitter/media/model/MediaFile;->a(Ljava/io/File;Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    .line 292
    :goto_0
    return-object v0

    .line 284
    :cond_0
    invoke-static {p0, p1}, Lbrt;->b(Landroid/content/Context;Lcom/twitter/model/media/EditableSegmentedVideo;)Ljava/io/File;

    move-result-object v1

    .line 285
    if-nez v1, :cond_1

    .line 286
    const/4 v0, 0x0

    goto :goto_0

    .line 288
    :cond_1
    invoke-virtual {v0, v2, v1}, Lcom/twitter/media/util/o;->a(Lcom/twitter/media/util/o$a;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 289
    if-eqz v0, :cond_2

    .line 292
    :goto_1
    sget-object v1, Lcom/twitter/media/model/MediaType;->d:Lcom/twitter/media/model/MediaType;

    invoke-static {v0, v1}, Lcom/twitter/media/model/MediaFile;->a(Ljava/io/File;Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private static a(Lcom/twitter/model/media/EditableAnimatedGif;)Lcom/twitter/media/model/MediaFile;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/twitter/model/media/EditableAnimatedGif;->k:Lcom/twitter/media/model/MediaFile;

    return-object v0
.end method

.method private static a(Lcom/twitter/model/media/EditableVideo;)Lcom/twitter/media/model/MediaFile;
    .locals 5

    .prologue
    .line 237
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 238
    invoke-static {}, Lcom/twitter/media/util/t;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 239
    const/4 v0, 0x0

    .line 270
    :goto_0
    return-object v0

    .line 242
    :cond_0
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 244
    const-string/jumbo v0, "android_video_bypass_transcode_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    invoke-static {p0}, Lbrt;->d(Lcom/twitter/model/media/EditableVideo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-instance v1, Lcom/twitter/library/scribe/ScribeSectionImportedVideo;

    invoke-direct {v1, p0}, Lcom/twitter/library/scribe/ScribeSectionImportedVideo;-><init>(Lcom/twitter/model/media/EditableVideo;)V

    const-string/jumbo v2, "video_trimmer::video:transcode:bypass"

    .line 247
    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeSectionImportedVideo;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeSectionImportedVideo;

    move-result-object v1

    .line 246
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeSection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 248
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 249
    iget-object v0, p0, Lcom/twitter/model/media/EditableVideo;->k:Lcom/twitter/media/model/MediaFile;

    goto :goto_0

    .line 252
    :cond_1
    invoke-static {p0}, Lbrt;->b(Lcom/twitter/model/media/EditableVideo;)Lcom/twitter/media/model/VideoFile;

    move-result-object v1

    .line 253
    new-instance v4, Lcom/twitter/library/scribe/ScribeSectionImportedVideo;

    invoke-direct {v4, p0}, Lcom/twitter/library/scribe/ScribeSectionImportedVideo;-><init>(Lcom/twitter/model/media/EditableVideo;)V

    if-eqz v1, :cond_2

    const-string/jumbo v0, "video_trimmer::video:transcode:success"

    .line 254
    :goto_1
    invoke-virtual {v4, v0}, Lcom/twitter/library/scribe/ScribeSectionImportedVideo;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeSectionImportedVideo;

    move-result-object v0

    .line 257
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v4, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    invoke-virtual {v4, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeSection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 258
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 260
    if-nez v1, :cond_3

    const-string/jumbo v0, "android_video_transcode_fallback_enabled"

    .line 261
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 262
    invoke-static {p0}, Lbrt;->d(Lcom/twitter/model/media/EditableVideo;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 263
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-instance v1, Lcom/twitter/library/scribe/ScribeSectionImportedVideo;

    invoke-direct {v1, p0}, Lcom/twitter/library/scribe/ScribeSectionImportedVideo;-><init>(Lcom/twitter/model/media/EditableVideo;)V

    const-string/jumbo v2, "video_trimmer::video:transcode:fallback"

    .line 264
    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeSectionImportedVideo;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeSectionImportedVideo;

    move-result-object v1

    .line 263
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeSection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 265
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 267
    iget-object v0, p0, Lcom/twitter/model/media/EditableVideo;->k:Lcom/twitter/media/model/MediaFile;

    goto/16 :goto_0

    .line 253
    :cond_2
    const-string/jumbo v0, "video_trimmer::video:transcode:failure"

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 270
    goto/16 :goto_0
.end method

.method private static b(Lcom/twitter/model/media/EditableVideo;)Lcom/twitter/media/model/VideoFile;
    .locals 18
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 300
    const/4 v7, 0x0

    .line 301
    const/4 v6, 0x3

    .line 302
    const/4 v5, 0x3

    .line 303
    const v3, 0x3567e0

    .line 304
    const v2, 0x3f666666    # 0.9f

    .line 305
    const/4 v4, 0x0

    .line 307
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 308
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v8

    invoke-virtual {v8}, Lcqq;->c()Lcqr;

    move-result-object v9

    move v8, v6

    move v6, v3

    move-object v3, v7

    move v7, v5

    move v5, v2

    .line 309
    :goto_0
    if-lez v8, :cond_5

    if-lez v7, :cond_5

    .line 310
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 311
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/model/media/EditableVideo;->c:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/twitter/model/media/EditableVideo;->b:I

    sub-int/2addr v2, v14

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v14, v2

    .line 312
    new-instance v16, Lcom/twitter/library/media/util/transcode/d;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/model/media/EditableVideo;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v2, Lcom/twitter/media/model/VideoFile;

    iget-object v2, v2, Lcom/twitter/media/model/VideoFile;->e:Ljava/io/File;

    .line 314
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lcom/twitter/library/media/util/transcode/d;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x26

    .line 315
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/twitter/library/media/util/transcode/d;->b(I)Lcom/twitter/library/media/util/transcode/d;

    move-result-object v2

    sget-object v16, Lbrt;->a:Lcom/twitter/util/math/Size;

    .line 316
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/twitter/library/media/util/transcode/d;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/library/media/util/transcode/d;

    move-result-object v2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/model/media/EditableVideo;->b:I

    move/from16 v16, v0

    move/from16 v0, v16

    mul-int/lit16 v0, v0, 0x3e8

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 317
    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1, v14, v15}, Lcom/twitter/library/media/util/transcode/d;->a(JJ)Lcom/twitter/library/media/util/transcode/d;

    move-result-object v2

    .line 318
    invoke-virtual {v2, v6}, Lcom/twitter/library/media/util/transcode/d;->a(I)Lcom/twitter/library/media/util/transcode/d;

    move-result-object v2

    .line 320
    :try_start_0
    invoke-virtual {v2}, Lcom/twitter/library/media/util/transcode/d;->b()Ljava/io/File;

    move-result-object v3

    .line 321
    invoke-virtual {v2}, Lcom/twitter/library/media/util/transcode/d;->a()Lcom/twitter/util/math/Size;
    :try_end_0
    .catch Lcom/twitter/library/media/util/transcode/TranscoderException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 334
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v14

    const-wide/32 v16, 0x4b00000

    cmp-long v2, v14, v16

    if-lez v2, :cond_2

    .line 336
    const/high16 v2, 0x4b00000

    mul-int/2addr v2, v6

    int-to-long v12, v2

    .line 337
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v14

    div-long/2addr v12, v14

    long-to-float v2, v12

    mul-float/2addr v2, v5

    float-to-int v2, v2

    .line 338
    const v6, 0x30d40

    const v12, 0x30d40

    div-int/2addr v2, v12

    mul-int/2addr v6, v2

    .line 339
    add-int/lit8 v7, v7, -0x1

    .line 340
    const v2, 0x3dcccccd    # 0.1f

    sub-float v2, v5, v2

    .line 342
    invoke-interface {v9, v3}, Lcqr;->b(Ljava/io/File;)Z

    move v5, v2

    .line 343
    goto/16 :goto_0

    .line 322
    :catch_0
    move-exception v2

    .line 323
    const-string/jumbo v12, "VideoFile"

    const-string/jumbo v13, "Error transcoding file"

    invoke-static {v12, v13, v2}, Lcqj;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 324
    invoke-static {v2}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 325
    invoke-virtual {v2}, Lcom/twitter/library/media/util/transcode/TranscoderException;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v2, v4

    .line 357
    :goto_1
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    if-nez v2, :cond_4

    .line 359
    :cond_0
    const-string/jumbo v4, "VideoFile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Can\'t transcode video "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/model/media/EditableVideo;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v2, Lcom/twitter/media/model/VideoFile;

    iget-object v2, v2, Lcom/twitter/media/model/VideoFile;->e:Ljava/io/File;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcqj;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    invoke-interface {v9, v3}, Lcqr;->b(Ljava/io/File;)Z

    .line 361
    const/4 v2, 0x0

    .line 367
    :goto_2
    return-object v2

    .line 328
    :cond_1
    add-int/lit8 v2, v8, -0x1

    move v8, v2

    .line 329
    goto/16 :goto_0

    .line 330
    :catch_1
    move-exception v2

    move-object v2, v4

    .line 331
    goto :goto_1

    .line 346
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v2, v14, v16

    if-nez v2, :cond_3

    .line 348
    add-int/lit8 v2, v8, -0x1

    move v8, v2

    .line 349
    goto/16 :goto_0

    .line 352
    :cond_3
    const-string/jumbo v5, "VideoFile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Video "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/model/media/EditableVideo;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v2, Lcom/twitter/media/model/VideoFile;

    iget-object v2, v2, Lcom/twitter/media/model/VideoFile;->e:Ljava/io/File;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, " transcoded in "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 353
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v12

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "ms "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 352
    invoke-static {v5, v2}, Lcqj;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    .line 354
    goto/16 :goto_1

    .line 364
    :cond_4
    const-string/jumbo v4, "VideoFile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Video "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/model/media/EditableVideo;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v2, Lcom/twitter/media/model/VideoFile;

    iget-object v2, v2, Lcom/twitter/media/model/VideoFile;->e:Ljava/io/File;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, " muxed in "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 365
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v10

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, "ms"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 364
    invoke-static {v4, v2}, Lcqj;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    sget-object v2, Lcom/twitter/media/model/MediaType;->d:Lcom/twitter/media/model/MediaType;

    invoke-static {v3, v2}, Lcom/twitter/media/model/MediaFile;->a(Ljava/io/File;Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/model/MediaFile;

    move-result-object v2

    check-cast v2, Lcom/twitter/media/model/VideoFile;

    goto/16 :goto_2

    :cond_5
    move-object v2, v4

    goto/16 :goto_1
.end method

.method private static b(Landroid/content/Context;Lcom/twitter/model/media/EditableSegmentedVideo;)Ljava/io/File;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 373
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 374
    iget-object v0, p1, Lcom/twitter/model/media/EditableSegmentedVideo;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/SegmentedVideoFile;

    iget-object v0, v0, Lcom/twitter/media/model/SegmentedVideoFile;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 375
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->c()Lcqr;

    move-result-object v0

    sget-object v2, Lcom/twitter/media/model/MediaType;->d:Lcom/twitter/media/model/MediaType;

    iget-object v2, v2, Lcom/twitter/media/model/MediaType;->extension:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcqr;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 376
    if-nez v2, :cond_1

    move-object v0, v1

    .line 384
    :goto_1
    return-object v0

    .line 374
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 380
    :cond_1
    new-instance v3, Lcom/twitter/media/service/tasks/VideoStitchTask;

    iget-object v0, p1, Lcom/twitter/model/media/EditableSegmentedVideo;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/SegmentedVideoFile;

    iget-object v0, v0, Lcom/twitter/media/model/SegmentedVideoFile;->i:Ljava/util/List;

    invoke-direct {v3, v0, v2}, Lcom/twitter/media/service/tasks/VideoStitchTask;-><init>(Ljava/util/List;Ljava/io/File;)V

    .line 381
    invoke-virtual {v3, p0}, Lcom/twitter/media/service/tasks/VideoStitchTask;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v1

    .line 382
    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 384
    goto :goto_1
.end method

.method public static b(Landroid/content/Context;Lcom/twitter/model/media/EditableMedia;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/model/media/EditableMedia;",
            ")",
            "Lrx/g",
            "<",
            "Lcom/twitter/media/model/MediaFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    new-instance v0, Lbrt$1;

    invoke-direct {v0, p0, p1}, Lbrt$1;-><init>(Landroid/content/Context;Lcom/twitter/model/media/EditableMedia;)V

    sget-object v1, Lcom/twitter/media/model/MediaFile;->d:Lcom/twitter/util/concurrent/d;

    invoke-static {v0, v1}, Lcre;->a(Ljava/util/concurrent/Callable;Lcom/twitter/util/concurrent/d;)Lrx/g;

    move-result-object v0

    .line 113
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v0

    .line 108
    return-object v0
.end method

.method private static c(Lcom/twitter/model/media/EditableVideo;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 388
    const/4 v3, 0x0

    .line 390
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v0, p0, Lcom/twitter/model/media/EditableVideo;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    iget-object v0, v0, Lcom/twitter/media/model/VideoFile;->e:Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :try_start_1
    new-instance v3, Landroid/media/MediaExtractor;

    invoke-direct {v3}, Landroid/media/MediaExtractor;-><init>()V

    .line 392
    invoke-virtual {v2}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/media/MediaExtractor;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 393
    invoke-virtual {v3}, Landroid/media/MediaExtractor;->getTrackCount()I

    move-result v4

    move v0, v1

    .line 394
    :goto_0
    if-ge v0, v4, :cond_1

    .line 395
    invoke-virtual {v3, v0}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v5

    .line 396
    const-string/jumbo v6, "mime"

    invoke-virtual {v5, v6}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 397
    const-string/jumbo v6, "video/avc"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string/jumbo v6, "audio/ac3"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v5

    if-nez v5, :cond_0

    .line 406
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    move v0, v1

    .line 404
    :goto_1
    return v0

    .line 394
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 401
    :cond_1
    const/4 v0, 0x1

    .line 406
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 402
    :catch_0
    move-exception v0

    move-object v2, v3

    .line 403
    :goto_2
    :try_start_2
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    move v0, v1

    .line 404
    goto :goto_1

    .line 406
    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_3
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 402
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private static d(Lcom/twitter/model/media/EditableVideo;)Z
    .locals 4

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/twitter/model/media/EditableVideo;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/media/EditableVideo;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    iget-object v0, v0, Lcom/twitter/media/model/VideoFile;->e:Ljava/io/File;

    .line 412
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/32 v2, 0x4b00000

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 413
    invoke-static {p0}, Lbrt;->c(Lcom/twitter/model/media/EditableVideo;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 411
    :goto_0
    return v0

    .line 413
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
