.class public Laeq;
.super Laum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laum",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;",
        "Lcom/twitter/model/people/j;",
        "Laeo;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:Lcom/twitter/database/lru/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/j;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/android/people/ah;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/database/lru/l;Lcom/twitter/android/people/ah;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/j;",
            ">;",
            "Lcom/twitter/android/people/ah;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Laum;-><init>()V

    .line 33
    iput-object p1, p0, Laeq;->a:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Laeq;->b:Lcom/twitter/library/client/Session;

    .line 35
    iput-object p3, p0, Laeq;->c:Lcom/twitter/database/lru/l;

    .line 36
    iput-object p4, p0, Laeq;->d:Lcom/twitter/android/people/ah;

    .line 37
    return-void
.end method


# virtual methods
.method protected a(Ljava/util/Map;)Laeo;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Laeo;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Laeo;

    iget-object v1, p0, Laeq;->a:Landroid/content/Context;

    iget-object v2, p0, Laeq;->b:Lcom/twitter/library/client/Session;

    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    iget-object v4, p0, Laeq;->c:Lcom/twitter/database/lru/l;

    iget-object v5, p0, Laeq;->d:Lcom/twitter/android/people/ah;

    invoke-direct/range {v0 .. v5}, Laeo;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/util/Map;Lcom/twitter/database/lru/l;Lcom/twitter/android/people/ah;)V

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/library/service/s;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Laeq;->a(Ljava/util/Map;)Laeo;

    move-result-object v0

    return-object v0
.end method

.method protected a(Laeo;)Lcom/twitter/model/people/j;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p1, Laeo;->a:Lcom/twitter/model/people/j;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/j;

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/library/service/s;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Laeo;

    invoke-virtual {p0, p1}, Laeq;->a(Laeo;)Lcom/twitter/model/people/j;

    move-result-object v0

    return-object v0
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    return v0
.end method
