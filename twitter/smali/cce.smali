.class public final Lcce;
.super Lcby;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcce$b;,
        Lcce$a;
    }
.end annotation


# static fields
.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcce;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lcom/twitter/model/geo/TwitterPlace;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcce$b;

    invoke-direct {v0}, Lcce$b;-><init>()V

    sput-object v0, Lcce;->b:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcce$a;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcby;-><init>(Lcbx$a;)V

    .line 25
    invoke-static {p1}, Lcce$a;->a(Lcce$a;)Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/TwitterPlace;

    iput-object v0, p0, Lcce;->c:Lcom/twitter/model/geo/TwitterPlace;

    .line 26
    return-void
.end method

.method synthetic constructor <init>(Lcce$a;Lcce$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcce;-><init>(Lcce$a;)V

    return-void
.end method

.method static synthetic a(Lcce;)Lcom/twitter/model/geo/TwitterPlace;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcce;->c:Lcom/twitter/model/geo/TwitterPlace;

    return-object v0
.end method

.method private b(Lcce;)Z
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcce;->c:Lcom/twitter/model/geo/TwitterPlace;

    iget-object v1, p1, Lcce;->c:Lcom/twitter/model/geo/TwitterPlace;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 35
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcce;

    if-eqz v0, :cond_1

    check-cast p1, Lcce;

    .line 36
    invoke-direct {p0, p1}, Lcce;->b(Lcce;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    .line 36
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 30
    invoke-super {p0}, Lcby;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcce;->c:Lcom/twitter/model/geo/TwitterPlace;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
