.class public Lpp;
.super Lpo;
.source "Twttr"


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lpo;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V

    .line 23
    return-void
.end method


# virtual methods
.method public processTick(Lbki;)V
    .locals 5
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p0}, Lpp;->b()Lcom/twitter/library/av/m;

    move-result-object v0

    .line 28
    iget-object v1, v0, Lcom/twitter/library/av/m;->o:Lbyf;

    invoke-interface {v1}, Lbyf;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29
    const/4 v1, 0x1

    iput-boolean v1, p0, Lpp;->b:Z

    .line 30
    iget-object v1, p0, Lpp;->a:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v2, "play_from_tap"

    const/4 v3, 0x0

    iget-object v4, p0, Lpp;->k:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V

    .line 31
    iget-object v1, p0, Lpp;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v1

    new-instance v2, Lbjy;

    iget-object v3, p0, Lpp;->k:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v2, v3}, Lbjy;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {v1, v2, v0}, Lbix;->a(Lbiw;Lcom/twitter/library/av/m;)V

    .line 33
    :cond_0
    return-void
.end method
