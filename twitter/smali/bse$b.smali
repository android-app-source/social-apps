.class public Lbse$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/database/model/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/model/l",
            "<",
            "Laxz$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/database/model/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/model/l",
            "<",
            "Laxz$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p1, p0, Lbse$b;->a:Lcom/twitter/database/model/l;

    .line 137
    return-void
.end method

.method public static a(Lcom/twitter/database/model/i;)Lbse$b;
    .locals 2

    .prologue
    .line 131
    const-class v0, Laxz;

    invoke-interface {p0, v0}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Laxz;

    invoke-interface {v0}, Laxz;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    .line 132
    new-instance v1, Lbse$b;

    invoke-direct {v1, v0}, Lbse$b;-><init>(Lcom/twitter/database/model/l;)V

    return-object v1
.end method


# virtual methods
.method public a(Ljava/util/List;)Lrx/c;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 148
    new-instance v0, Lcom/twitter/database/model/f$a;

    invoke-direct {v0}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v1, "section_id"

    .line 149
    invoke-static {v1, p1}, Laux;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lbse$b;->a:Lcom/twitter/database/model/l;

    invoke-interface {v1, v0}, Lcom/twitter/database/model/l;->a(Lcom/twitter/database/model/f;)Lcom/twitter/database/model/g;

    move-result-object v1

    .line 153
    :try_start_0
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v2

    .line 154
    :goto_0
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Laxz$a;

    invoke-interface {v0}, Laxz$a;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Laxz$a;

    invoke-interface {v0}, Laxz$a;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 160
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    throw v0

    .line 157
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 158
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lrx/c;->a(Ljava/lang/Iterable;)Lrx/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 160
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    .line 158
    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 125
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lbse$b;->a(Ljava/util/List;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    return-void
.end method
