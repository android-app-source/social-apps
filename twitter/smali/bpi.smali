.class public Lbpi;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbpi$a;
    }
.end annotation


# static fields
.field private static final a:Lcok$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcok$b",
            "<",
            "Lbpi$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lbpi$1;

    invoke-direct {v0}, Lbpi$1;-><init>()V

    .line 33
    invoke-static {v0}, Lcok;->a(Lcom/twitter/util/object/j;)Lcok$b;

    move-result-object v0

    sput-object v0, Lbpi;->a:Lcok$b;

    .line 32
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lbpi;->a:Lcok$b;

    invoke-virtual {v0}, Lcok$b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpi$a;

    iget v0, v0, Lbpi$a;->c:I

    return v0
.end method

.method public static a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 50
    check-cast p0, Landroid/app/Activity;

    invoke-static {p0}, Lcom/twitter/util/ui/k;->a(Landroid/app/Activity;)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3f333333    # 0.7f

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->rint(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public static a(Z)Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 67
    if-eqz p0, :cond_0

    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/util/math/Size;

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;Z)Z
    .locals 1

    .prologue
    .line 72
    .line 73
    invoke-static {p1}, Lbpi;->a(Z)Lcom/twitter/util/math/Size;

    move-result-object v0

    .line 72
    invoke-static {p0, v0}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Z

    move-result v0

    return v0
.end method

.method public static b()I
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lbpi;->a:Lcok$b;

    invoke-virtual {v0}, Lcok$b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpi$a;

    iget v0, v0, Lbpi$a;->d:I

    return v0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lbpi;->a:Lcok$b;

    invoke-virtual {v0}, Lcok$b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpi$a;

    iget-boolean v0, v0, Lbpi$a;->e:Z

    return v0
.end method

.method public static d()Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 62
    invoke-static {}, Lbpi;->c()Z

    move-result v0

    invoke-static {v0}, Lbpi;->a(Z)Lcom/twitter/util/math/Size;

    move-result-object v0

    return-object v0
.end method

.method public static e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lbpi;->a:Lcok$b;

    invoke-virtual {v0}, Lcok$b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpi$a;

    iget-object v0, v0, Lbpi$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lbpi;->a:Lcok$b;

    invoke-virtual {v0}, Lcok$b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpi$a;

    iget-boolean v0, v0, Lbpi$a;->b:Z

    return v0
.end method
