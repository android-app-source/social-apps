.class public Lns;
.super Lbjb;
.source "Twttr"


# instance fields
.field a:Z

.field private final b:Lcom/twitter/library/av/playback/AVPlayer;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lns;->a:Z

    .line 27
    iput-object p1, p0, Lns;->b:Lcom/twitter/library/av/playback/AVPlayer;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lbiw;)Z
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 39
    invoke-super {p0, p1}, Lbjb;->a(Lbiw;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lns;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processTick(Lbki;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lns;->a:Z

    .line 33
    iget-object v0, p0, Lns;->b:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v1, "marketplace_ad_impression"

    const/4 v2, 0x0

    iget-object v3, p0, Lns;->k:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V

    .line 34
    return-void
.end method
