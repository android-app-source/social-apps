.class public Lcnr;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/support/v4/widget/SwipeRefreshLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcno;)V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcnr;-><init>(Landroid/content/Context;Lcno;Landroid/support/v4/widget/SwipeRefreshLayout;)V

    .line 19
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcno;Landroid/support/v4/widget/SwipeRefreshLayout;)V
    .locals 7
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-interface {p2}, Lcno;->a()Landroid/view/ViewGroup;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 27
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 28
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 31
    if-eqz p3, :cond_0

    .line 36
    :goto_0
    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lckh$c;->twitter_blue:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    aput v5, v3, v4

    invoke-virtual {p3, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    .line 37
    invoke-virtual {p3, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->addView(Landroid/view/View;)V

    .line 38
    invoke-virtual {v0, p3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 39
    iput-object p3, p0, Lcnr;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 42
    new-instance v0, Lcnr$1;

    invoke-direct {v0, p0}, Lcnr$1;-><init>(Lcnr;)V

    invoke-interface {p2, v0}, Lcno;->a(Lcno$c;)V

    .line 53
    return-void

    .line 34
    :cond_0
    new-instance p3, Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-direct {p3, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method static synthetic a(Lcnr;)Landroid/support/v4/widget/SwipeRefreshLayout;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcnr;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcnr;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V

    .line 57
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcnr;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 61
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcnr;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->isRefreshing()Z

    move-result v0

    return v0
.end method
