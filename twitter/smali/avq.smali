.class public Lavq;
.super Lcom/twitter/database/hydrator/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/database/hydrator/a",
        "<",
        "Lcom/twitter/model/dms/Participant;",
        "Lawv$b$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/database/hydrator/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/dms/Participant;Lawv$b$a;)Lawv$b$a;
    .locals 4

    .prologue
    .line 18
    iget-object v0, p1, Lcom/twitter/model/dms/Participant;->f:Ljava/lang/String;

    .line 19
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p2, v0}, Lawv$b$a;->a(Ljava/lang/String;)Lawv$b$a;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/model/dms/Participant;->b:J

    .line 20
    invoke-interface {v0, v2, v3}, Lawv$b$a;->a(J)Lawv$b$a;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/model/dms/Participant;->d:J

    .line 21
    invoke-interface {v0, v2, v3}, Lawv$b$a;->c(J)Lawv$b$a;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/model/dms/Participant;->c:J

    .line 22
    invoke-interface {v0, v2, v3}, Lawv$b$a;->b(J)Lawv$b$a;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/model/dms/Participant;->e:J

    .line 23
    invoke-interface {v0, v2, v3}, Lawv$b$a;->d(J)Lawv$b$a;

    move-result-object v0

    iget v1, p1, Lcom/twitter/model/dms/Participant;->h:I

    .line 24
    invoke-interface {v0, v1}, Lawv$b$a;->a(I)Lawv$b$a;

    .line 25
    return-object p2
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    check-cast p1, Lcom/twitter/model/dms/Participant;

    check-cast p2, Lawv$b$a;

    invoke-virtual {p0, p1, p2}, Lavq;->a(Lcom/twitter/model/dms/Participant;Lawv$b$a;)Lawv$b$a;

    move-result-object v0

    return-object v0
.end method
