.class public Laqe;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/drafts/d;


# instance fields
.field private final a:Lcom/twitter/app/drafts/e;

.field private final b:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/drafts/a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/app/drafts/e;Lcom/twitter/app/drafts/h;J)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Laqe;->a:Lcom/twitter/app/drafts/e;

    .line 25
    iget-object v0, p0, Laqe;->a:Lcom/twitter/app/drafts/e;

    invoke-interface {v0, p3, p4}, Lcom/twitter/app/drafts/e;->a(J)Lrx/c;

    move-result-object v0

    iput-object v0, p0, Laqe;->b:Lrx/c;

    .line 26
    invoke-virtual {p2}, Lcom/twitter/app/drafts/h;->a()V

    .line 27
    return-void
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/drafts/a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Laqe;->b:Lrx/c;

    return-object v0
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    .line 32
    iget-object v0, p0, Laqe;->a:Lcom/twitter/app/drafts/e;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 34
    :cond_0
    return-void
.end method
