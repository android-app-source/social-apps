.class public Lant;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lank;

.field private final c:Laod;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lank;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/app/Activity;",
            ":",
            "Lcom/twitter/app/common/util/i;",
            ">(TT;",
            "Lank;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    sget-object v0, Laod;->a:Laod;

    invoke-direct {p0, p1, p2, v0}, Lant;-><init>(Landroid/app/Activity;Lank;Laod;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lank;Laod;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/app/Activity;",
            ":",
            "Lcom/twitter/app/common/util/i;",
            ">(TT;",
            "Lank;",
            "Laod;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lant;->a:Landroid/app/Activity;

    .line 54
    iput-object p2, p0, Lant;->b:Lank;

    .line 55
    iput-object p3, p0, Lant;->c:Laod;

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Laod;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/app/Activity;",
            ":",
            "Lcom/twitter/app/common/util/i;",
            ">(TT;",
            "Laod;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    sget-object v0, Lank;->a:Lank;

    invoke-direct {p0, p1, v0, p2}, Lant;-><init>(Landroid/app/Activity;Lank;Laod;)V

    .line 49
    return-void
.end method

.method public static a(Lcom/twitter/app/common/base/BaseFragmentActivity;)Landroid/support/v4/app/FragmentManager;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/app/Activity;)Lcom/twitter/app/common/util/j;
    .locals 1

    .prologue
    .line 69
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    return-object v0
.end method

.method public static d(Landroid/app/Activity;)Lcom/twitter/app/common/base/BaseFragmentActivity;
    .locals 1

    .prologue
    .line 92
    const-class v0, Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-static {p0, v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/base/BaseFragmentActivity;

    return-object v0
.end method

.method public static e(Landroid/app/Activity;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 106
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method public static f(Landroid/app/Activity;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public static g(Landroid/app/Activity;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public g()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lant;->a:Landroid/app/Activity;

    return-object v0
.end method

.method public h()Lank;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lant;->b:Lank;

    return-object v0
.end method

.method public i()Laod;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lant;->c:Laod;

    return-object v0
.end method
