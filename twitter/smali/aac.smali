.class public Laac;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:Lcom/twitter/library/client/p;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Laac;->d:Ljava/util/List;

    .line 31
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Laac;->e:Ljava/util/Set;

    .line 35
    iput-object p1, p0, Laac;->a:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Laac;->b:Lcom/twitter/library/client/Session;

    .line 37
    iput-object p3, p0, Laac;->c:Lcom/twitter/library/client/p;

    .line 38
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 3

    .prologue
    .line 91
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    .line 92
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    .line 93
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 94
    invoke-virtual {v0, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 91
    return-object v0
.end method

.method private a(Lcom/twitter/library/api/PromotedEvent;JLandroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 4

    .prologue
    .line 99
    invoke-virtual {p4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 100
    iget-object v1, p0, Laac;->c:Lcom/twitter/library/client/p;

    new-instance v2, Lbel;

    invoke-direct {v2, v0, p5, p1}, Lbel;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/PromotedEvent;)V

    .line 101
    invoke-virtual {v2, p2, p3}, Lbel;->a(J)Lbel;

    move-result-object v0

    .line 100
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 102
    return-void
.end method

.method private static c(Lcom/twitter/model/moments/i;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 106
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/twitter/model/moments/i;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 108
    const/16 v1, 0x8

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 109
    iget v1, p0, Lcom/twitter/model/moments/i;->e:I

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 110
    iget-object v1, p0, Lcom/twitter/model/moments/i;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 111
    iget-object v1, p0, Lcom/twitter/model/moments/i;->k:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->k:Ljava/lang/String;

    .line 112
    iget-wide v2, p0, Lcom/twitter/model/moments/i;->l:J

    iput-wide v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 114
    iget-boolean v1, p0, Lcom/twitter/model/moments/i;->f:Z

    if-eqz v1, :cond_0

    .line 115
    iget-wide v2, p0, Lcom/twitter/model/moments/i;->h:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->e:Ljava/lang/String;

    .line 117
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 48
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "trendsplus"

    aput-object v3, v2, v5

    const-string/jumbo v3, "modern_guide"

    aput-object v3, v2, v4

    const/4 v3, 0x2

    aput-object v6, v2, v3

    const/4 v3, 0x3

    aput-object v6, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "impression"

    aput-object v4, v2, v3

    .line 49
    invoke-static {v2}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 48
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 51
    return-void
.end method

.method public a(Lcom/twitter/model/moments/i;)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Laac;->e:Ljava/util/Set;

    iget-object v1, p1, Lcom/twitter/model/moments/i;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Laac;->d:Ljava/util/List;

    invoke-static {p1}, Laac;->c(Lcom/twitter/model/moments/i;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v0, p0, Laac;->e:Ljava/util/Set;

    iget-object v1, p1, Lcom/twitter/model/moments/i;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    :cond_0
    return-void
.end method

.method public b()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 54
    iget-object v0, p0, Laac;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "trendsplus"

    aput-object v3, v2, v5

    const-string/jumbo v3, "modern_guide"

    aput-object v3, v2, v4

    const/4 v3, 0x2

    aput-object v6, v2, v3

    const/4 v3, 0x3

    aput-object v6, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "results"

    aput-object v4, v2, v3

    .line 56
    invoke-static {v2}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Laac;->d:Ljava/util/List;

    .line 58
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 55
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 60
    :cond_0
    iget-object v0, p0, Laac;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 61
    return-void
.end method

.method public b(Lcom/twitter/model/moments/i;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 64
    iget-boolean v0, p1, Lcom/twitter/model/moments/i;->f:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "promoted_trend"

    .line 65
    :goto_0
    invoke-static {p1}, Laac;->c(Lcom/twitter/model/moments/i;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 68
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "trendsplus"

    aput-object v3, v2, v4

    const-string/jumbo v3, "modern_guide"

    aput-object v3, v2, v5

    const/4 v3, 0x0

    aput-object v3, v2, v6

    aput-object v0, v2, v7

    const-string/jumbo v3, "click"

    aput-object v3, v2, v8

    invoke-static {v2}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 70
    iget-object v3, p1, Lcom/twitter/model/moments/i;->c:Ljava/lang/String;

    invoke-direct {p0, v3, v2, v1}, Laac;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v2

    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 73
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "trendsplus"

    aput-object v3, v2, v4

    const-string/jumbo v3, "modern_guide"

    aput-object v3, v2, v5

    const/4 v3, 0x0

    aput-object v3, v2, v6

    aput-object v0, v2, v7

    const-string/jumbo v0, "search"

    aput-object v0, v2, v8

    invoke-static {v2}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    iget-object v2, p1, Lcom/twitter/model/moments/i;->c:Ljava/lang/String;

    invoke-direct {p0, v2, v0, v1}, Laac;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 77
    iget-boolean v0, p1, Lcom/twitter/model/moments/i;->f:Z

    if-eqz v0, :cond_0

    .line 78
    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->h:Lcom/twitter/library/api/PromotedEvent;

    iget-wide v2, p1, Lcom/twitter/model/moments/i;->h:J

    iget-object v4, p0, Laac;->a:Landroid/content/Context;

    iget-object v5, p0, Laac;->b:Lcom/twitter/library/client/Session;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Laac;->a(Lcom/twitter/library/api/PromotedEvent;JLandroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 80
    :cond_0
    return-void

    .line 64
    :cond_1
    const-string/jumbo v0, "trend"

    goto :goto_0
.end method

.method public c()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 83
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "trendsplus"

    aput-object v3, v2, v5

    const-string/jumbo v3, "modern_guide"

    aput-object v3, v2, v4

    const/4 v3, 0x2

    aput-object v6, v2, v3

    const/4 v3, 0x3

    aput-object v6, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "navigate"

    aput-object v4, v2, v3

    .line 84
    invoke-static {v2}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 83
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 86
    return-void
.end method
