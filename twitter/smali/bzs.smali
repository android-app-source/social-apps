.class Lbzs;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbzq;


# static fields
.field private static a:Lbzs;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lbzr;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lbzo;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lbzr;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbzp;

    invoke-direct {v0}, Lbzp;-><init>()V

    :goto_0
    invoke-direct {p0, v0}, Lbzs;-><init>(Lbzo;)V

    .line 26
    return-void

    .line 25
    :cond_0
    new-instance v0, Lbzt;

    invoke-direct {v0}, Lbzt;-><init>()V

    goto :goto_0
.end method

.method constructor <init>(Lbzo;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lbzs;->b:Ljava/util/Map;

    .line 30
    iput-object p1, p0, Lbzs;->c:Lbzo;

    .line 31
    iget-object v0, p0, Lbzs;->c:Lbzo;

    invoke-virtual {v0, p0}, Lbzo;->a(Lbzq;)V

    .line 32
    return-void
.end method

.method public static declared-synchronized a()Lbzs;
    .locals 2

    .prologue
    .line 36
    const-class v1, Lbzs;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbzs;->a:Lbzs;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lbzs;

    invoke-direct {v0}, Lbzs;-><init>()V

    sput-object v0, Lbzs;->a:Lbzs;

    .line 38
    const-class v0, Lbzs;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 40
    :cond_0
    sget-object v0, Lbzs;->a:Lbzs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(J)V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lbzs;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzq;

    .line 66
    invoke-interface {v0, p1, p2}, Lbzq;->a(J)V

    goto :goto_0

    .line 68
    :cond_0
    return-void
.end method

.method declared-synchronized a(Lbzr;)V
    .locals 2

    .prologue
    .line 49
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbzs;->b:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    iget-object v0, p0, Lbzs;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 51
    iget-object v0, p0, Lbzs;->c:Lbzo;

    invoke-virtual {v0}, Lbzo;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :cond_0
    monitor-exit p0

    return-void

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b(Lbzr;)V
    .locals 2

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbzs;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 58
    iget-object v0, p0, Lbzs;->c:Lbzo;

    invoke-virtual {v0}, Lbzo;->b()V

    .line 60
    :cond_0
    iget-object v0, p0, Lbzs;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    monitor-exit p0

    return-void

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
