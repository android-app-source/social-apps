.class public Lavz;
.super Laur;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/database/lru/schema/LruSchema;


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 2

    .prologue
    .line 23
    invoke-static {p2, p3}, Lavz;->a(J)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Laur;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 24
    return-void
.end method

.method public static a(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "lru_key_value"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public synthetic bq_()Lcom/twitter/database/model/i;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lavz;->c()Lcom/twitter/database/lru/schema/LruSchema;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/twitter/database/lru/schema/LruSchema;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lavz;->a:Lcom/twitter/database/lru/schema/LruSchema;

    if-nez v0, :cond_0

    .line 35
    const-class v0, Lcom/twitter/database/lru/schema/LruSchema;

    new-instance v1, Lavb;

    invoke-direct {v1, p0}, Lavb;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;)V

    invoke-static {v0, v1}, Lcom/twitter/database/model/i$a;->a(Ljava/lang/Class;Lcom/twitter/database/model/a;)Lcom/twitter/database/model/i;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/lru/schema/LruSchema;

    iput-object v0, p0, Lavz;->a:Lcom/twitter/database/lru/schema/LruSchema;

    .line 37
    :cond_0
    iget-object v0, p0, Lavz;->a:Lcom/twitter/database/lru/schema/LruSchema;

    return-object v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 42
    const-class v0, Lcom/twitter/database/lru/schema/LruSchema;

    new-instance v1, Lava;

    invoke-direct {v1, p1}, Lava;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {v0, v1}, Lcom/twitter/database/model/i$a;->a(Ljava/lang/Class;Lcom/twitter/database/model/a;)Lcom/twitter/database/model/i;

    move-result-object v0

    .line 43
    invoke-interface {v0}, Lcom/twitter/database/model/i;->g()V

    .line 44
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 48
    const-class v0, Lcom/twitter/database/lru/schema/LruSchema;

    new-instance v1, Lava;

    invoke-direct {v1, p1}, Lava;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {v0, v1}, Lcom/twitter/database/model/j$a;->a(Ljava/lang/Class;Lcom/twitter/database/model/a;)Lcom/twitter/database/model/j;

    move-result-object v0

    .line 50
    new-instance v1, Lawa;

    invoke-direct {v1, v0, p1}, Lawa;-><init>(Lcom/twitter/database/model/j;Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {v1, p2, p3}, Lawa;->a(II)V

    .line 51
    return-void
.end method
