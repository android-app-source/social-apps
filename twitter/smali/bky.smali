.class public Lbky;
.super Lbkq;
.source "Twttr"


# instance fields
.field private final b:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

.field private final c:Ljava/lang/String;

.field private final d:Lcrr;

.field private final e:J


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/library/api/periscope/PeriscopeCapiModel;Ljava/lang/String;Lcrr;J)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lbkq;-><init>(Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 36
    iput-object p2, p0, Lbky;->b:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    .line 37
    iput-object p3, p0, Lbky;->c:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lbky;->d:Lcrr;

    .line 39
    iput-wide p5, p0, Lbky;->e:J

    .line 40
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/library/av/g;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 9

    .prologue
    .line 59
    iget-object v0, p0, Lbky;->c:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Ltv/periscope/android/library/e;->a(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 60
    iget-object v0, p0, Lbky;->d:Lcrr;

    invoke-virtual {v0}, Lcrr;->e()Lcom/twitter/util/network/c;

    move-result-object v4

    .line 61
    iget-object v0, p0, Lbky;->b:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v0}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 62
    new-instance v0, Lcom/twitter/library/av/model/PeriscopePlaylist;

    iget-object v1, p0, Lbky;->b:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v1}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbky;->c:Ljava/lang/String;

    iget-object v4, v4, Lcom/twitter/util/network/c;->b:Ljava/lang/String;

    iget-object v5, p0, Lbky;->b:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    .line 63
    invoke-virtual {v5}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->k()Z

    move-result v5

    iget-wide v6, p0, Lbky;->e:J

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/av/model/PeriscopePlaylist;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 77
    :goto_0
    return-object v0

    .line 65
    :cond_0
    iget-object v0, p0, Lbky;->b:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    iget-object v8, v0, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->a:Ljava/lang/Object;

    monitor-enter v8

    .line 67
    :try_start_0
    iget-object v0, p0, Lbky;->b:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    iget-object v0, v0, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->a:Ljava/lang/Object;

    invoke-virtual {p0}, Lbky;->b()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/lang/Object;->wait(J)V

    .line 68
    iget-object v0, p0, Lbky;->b:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v0}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 69
    new-instance v0, Lcom/twitter/library/av/model/PeriscopePlaylist;

    iget-object v1, p0, Lbky;->b:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v1}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbky;->c:Ljava/lang/String;

    iget-object v4, v4, Lcom/twitter/util/network/c;->b:Ljava/lang/String;

    iget-object v5, p0, Lbky;->b:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    .line 70
    invoke-virtual {v5}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->k()Z

    move-result v5

    iget-wide v6, p0, Lbky;->e:J

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/av/model/PeriscopePlaylist;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    monitor-exit v8

    goto :goto_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 73
    :catch_0
    move-exception v0

    .line 75
    :cond_1
    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 77
    new-instance v0, Lcom/twitter/model/av/InvalidPlaylist;

    invoke-direct {v0}, Lcom/twitter/model/av/InvalidPlaylist;-><init>()V

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;)Lcom/twitter/network/j;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Ljava/util/Map;Lcom/twitter/util/network/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/util/network/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 102
    return-void
.end method

.method protected a(Landroid/net/Uri$Builder;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri$Builder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    return-void
.end method

.method public b()J
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lbky;->b:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v0}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->a()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public b(Landroid/content/Context;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 1

    .prologue
    .line 53
    invoke-static {p1}, Lcom/twitter/library/av/g;->a(Landroid/content/Context;)Lcom/twitter/library/av/g;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbky;->a(Landroid/content/Context;Lcom/twitter/library/av/g;)Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lcom/twitter/network/j;Lcom/twitter/network/HttpOperation;Ljava/util/Map;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/j;",
            "Lcom/twitter/network/HttpOperation;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ")",
            "Lcom/twitter/model/av/AVMediaPlaylist;"
        }
    .end annotation

    .prologue
    .line 83
    const/4 v0, 0x0

    return-object v0
.end method
