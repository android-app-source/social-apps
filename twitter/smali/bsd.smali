.class public Lbsd;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a()Z
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Lbsd;->p()Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/twitter/model/moments/Moment;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/twitter/model/moments/Moment;->d:Z

    return v0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 31
    const-string/jumbo v0, "moments_config_moment_level_likes_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 39
    const-string/jumbo v0, "moments_config_new_gestures_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 43
    const-string/jumbo v0, "moments_config_heart_animations_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lbsd;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "moments_config_randomized_heart_animations_enabled"

    .line 48
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 47
    :goto_0
    return v0

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 52
    const-string/jumbo v0, "moments_config_moments_in_moments_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static g()Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method

.method public static h()Z
    .locals 1

    .prologue
    .line 60
    const-string/jumbo v0, "moments_config_report_moment_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static i()Z
    .locals 1

    .prologue
    .line 74
    const-string/jumbo v0, "moments_config_moments_htl_carousel_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static j()Lcom/twitter/util/math/Size;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 79
    const-string/jumbo v0, "moments_placeholder_cover_config_image_width"

    .line 80
    invoke-static {v0, v2}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v1, "moments_placeholder_cover_config_image_height"

    .line 81
    invoke-static {v1, v2}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v1

    .line 79
    invoke-static {v0, v1}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v0

    return-object v0
.end method

.method public static k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const-string/jumbo v0, "moments_placeholder_cover_config_image_url"

    invoke-static {v0}, Lcoj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static l()Z
    .locals 2

    .prologue
    .line 91
    const-string/jumbo v0, "moments_config_moments_injections_enabled"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static m()Z
    .locals 2

    .prologue
    .line 95
    const-string/jumbo v0, "moments_config_maker_per_page_theme_enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static n()Z
    .locals 1

    .prologue
    .line 99
    const-string/jumbo v0, "live_video_cards_in_moments_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static o()I
    .locals 2

    .prologue
    .line 103
    const-string/jumbo v0, "moments_config_capsule_response_tweets_max_count"

    const/16 v1, 0x32

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static p()Z
    .locals 2

    .prologue
    .line 64
    const-string/jumbo v0, "android_moments_holdback_3670"

    const-string/jumbo v1, "enabled"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
