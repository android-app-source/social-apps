.class public Lbya;
.super Lbxv;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbya$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbxv",
        "<",
        "Lbya$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

.field private final c:Lbxz;

.field private final d:Lcom/twitter/library/media/widget/TweetMediaView$a;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lbxz;Lcom/twitter/library/media/widget/TweetMediaView$a;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lbxv;-><init>(Landroid/app/Activity;)V

    .line 36
    iput-object p2, p0, Lbya;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 37
    iput-object p3, p0, Lbya;->c:Lbxz;

    .line 38
    iput-object p4, p0, Lbya;->d:Lcom/twitter/library/media/widget/TweetMediaView$a;

    .line 39
    return-void
.end method

.method static synthetic a(Lbya;)Lbxz;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lbya;->c:Lbxz;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/media/ui/image/BaseMediaImageView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lbya;->a:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lbya;->a:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/media/ui/image/BaseMediaImageView;

    move-result-object v0

    .line 51
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbya;->a:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lbya;->a:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->d()V

    .line 97
    :cond_0
    return-void
.end method

.method public a(Lbya$a;)V
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 56
    iget-object v3, p1, Lbya$a;->a:Lcom/twitter/model/core/Tweet;

    .line 57
    iget-object v0, v3, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    if-eqz v0, :cond_1

    move v0, v7

    .line 58
    :goto_0
    invoke-virtual {p0}, Lbya;->f()Landroid/app/Activity;

    move-result-object v1

    .line 59
    iget-object v2, p0, Lbya;->a:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    if-nez v2, :cond_0

    .line 60
    sget-object v2, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-virtual {p0, v1, p1, v3, v2}, Lbya;->a(Landroid/app/Activity;Ljava/lang/Object;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    move-result-object v1

    iput-object v1, p0, Lbya;->a:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    .line 61
    iget-object v1, p0, Lbya;->c:Lbxz;

    if-eqz v1, :cond_0

    .line 62
    iget-object v1, p0, Lbya;->a:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    new-instance v2, Lbya$1;

    invoke-direct {v2, p0}, Lbya$1;-><init>(Lbya;)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;->setOnMediaClickListener(Lcom/twitter/library/media/widget/TweetMediaView$b;)V

    .line 86
    :cond_0
    iget-object v1, p0, Lbya;->a:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    iget-object v2, p0, Lbya;->d:Lcom/twitter/library/media/widget/TweetMediaView$a;

    iget v4, p1, Lbya$a;->b:I

    iget v5, p1, Lbya$a;->c:I

    iget-object v6, p0, Lbya;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v9, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-eq v6, v9, :cond_2

    move v6, v7

    :goto_1
    iget-object v9, p0, Lbya;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v10, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-eq v9, v10, :cond_3

    :goto_2
    invoke-static/range {v0 .. v7}, Lbxw;->a(ZLcom/twitter/library/media/widget/AdaptiveTweetMediaView;Lcom/twitter/library/media/widget/TweetMediaView$a;Lcom/twitter/model/core/Tweet;IIZZ)V

    .line 90
    return-void

    :cond_1
    move v0, v8

    .line 57
    goto :goto_0

    :cond_2
    move v6, v8

    .line 86
    goto :goto_1

    :cond_3
    move v7, v8

    goto :goto_2
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lbya$a;

    invoke-virtual {p0, p1}, Lbya;->a(Lbya$a;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 107
    return-void
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lbya;->a:Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    return-object v0
.end method
