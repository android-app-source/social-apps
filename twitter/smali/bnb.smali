.class public abstract Lbnb;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(Landroid/accounts/AccountManager;)Lakn;
    .locals 3

    .prologue
    .line 69
    new-instance v0, Lakn;

    sget-object v1, Lcom/twitter/library/util/b;->a:Ljava/lang/String;

    new-instance v2, Lcom/twitter/library/util/b$a;

    invoke-direct {v2}, Lcom/twitter/library/util/b$a;-><init>()V

    invoke-direct {v0, p0, v1, v2}, Lakn;-><init>(Landroid/accounts/AccountManager;Ljava/lang/String;Lakn$b;)V

    return-object v0
.end method

.method static a()Lcom/twitter/async/service/c;
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v7, 0x1

    .line 87
    new-instance v1, Lcom/twitter/async/service/h;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Lcom/twitter/async/service/h;-><init>(I)V

    .line 88
    new-instance v2, Lcom/twitter/async/service/h;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Lcom/twitter/async/service/h;-><init>(I)V

    .line 89
    new-instance v3, Lcom/twitter/async/service/h;

    invoke-direct {v3, v0}, Lcom/twitter/async/service/h;-><init>(I)V

    .line 90
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v5

    .line 92
    new-instance v4, Lcom/twitter/async/service/h;

    if-le v5, v0, :cond_0

    const/16 v6, 0x10

    if-ge v5, v6, :cond_0

    :goto_0
    invoke-direct {v4, v0}, Lcom/twitter/async/service/h;-><init>(I)V

    .line 102
    new-instance v5, Lcom/twitter/async/service/h;

    invoke-direct {v5, v7}, Lcom/twitter/async/service/h;-><init>(I)V

    .line 103
    invoke-virtual {v5, v7}, Lcom/twitter/async/service/h;->setMaximumPoolSize(I)V

    .line 104
    invoke-virtual {v5, v7}, Lcom/twitter/async/service/h;->allowCoreThreadTimeOut(Z)V

    .line 106
    new-instance v6, Lcom/twitter/async/service/h;

    invoke-direct {v6, v7}, Lcom/twitter/async/service/h;-><init>(I)V

    .line 107
    invoke-virtual {v6, v7}, Lcom/twitter/async/service/h;->setMaximumPoolSize(I)V

    .line 108
    invoke-virtual {v6, v7}, Lcom/twitter/async/service/h;->allowCoreThreadTimeOut(Z)V

    .line 110
    new-instance v7, Lcom/twitter/async/service/d;

    invoke-direct {v7, v2}, Lcom/twitter/async/service/d;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 112
    new-instance v0, Lcom/twitter/async/service/c;

    invoke-direct/range {v0 .. v7}, Lcom/twitter/async/service/c;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;)V

    return-object v0

    :cond_0
    move v0, v7

    .line 92
    goto :goto_0
.end method

.method static a(Landroid/content/Context;)Lcom/twitter/library/client/v;
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcom/twitter/library/client/v;

    invoke-direct {v0, p0}, Lcom/twitter/library/client/v;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;)Lcpi;
    .locals 3

    .prologue
    .line 54
    new-instance v0, Lcpl;

    invoke-direct {v0}, Lcpl;-><init>()V

    .line 56
    new-instance v1, Lcom/twitter/library/scribe/m;

    new-instance v2, Lcom/twitter/library/scribe/m$a;

    invoke-direct {v2, p0}, Lcom/twitter/library/scribe/m$a;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/scribe/m;-><init>(Landroid/content/Context;Lcom/twitter/library/scribe/m$a;)V

    invoke-virtual {v0, v1}, Lcpl;->a(Lcpj;)V

    .line 58
    new-instance v1, Lbst;

    invoke-direct {v1, p0, p2, p1}, Lbst;-><init>(Landroid/content/Context;Lcom/twitter/library/client/p;Lcom/twitter/library/client/v;)V

    invoke-virtual {v0, v1}, Lcpl;->a(Lcpj;)V

    .line 59
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    new-instance v1, Lcom/twitter/library/scribe/h;

    invoke-direct {v1}, Lcom/twitter/library/scribe/h;-><init>()V

    invoke-virtual {v0, v1}, Lcpl;->a(Lcpj;)V

    .line 62
    :cond_0
    return-object v0
.end method

.method static b()Lcom/twitter/library/card/ae;
    .locals 1

    .prologue
    .line 148
    new-instance v0, Lcom/twitter/library/card/ae;

    invoke-direct {v0}, Lcom/twitter/library/card/ae;-><init>()V

    return-object v0
.end method

.method static b(Landroid/content/Context;)Lcom/twitter/library/util/aa;
    .locals 1

    .prologue
    .line 127
    new-instance v0, Lcom/twitter/library/util/aa;

    invoke-direct {v0, p0}, Lcom/twitter/library/util/aa;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static c(Landroid/content/Context;)Lcom/twitter/metrics/j;
    .locals 1

    .prologue
    .line 134
    new-instance v0, Lcom/twitter/metrics/j;

    invoke-direct {v0, p0}, Lcom/twitter/metrics/j;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static d(Landroid/content/Context;)Lcom/twitter/library/network/livepipeline/b;
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lcom/twitter/library/network/livepipeline/b;

    invoke-direct {v0, p0}, Lcom/twitter/library/network/livepipeline/b;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static e(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 161
    const-string/jumbo v0, "com.google.android.gcm"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method
