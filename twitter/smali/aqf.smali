.class public Laqf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/drafts/e;


# instance fields
.field final a:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Lcom/twitter/database/model/f;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/drafts/a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lauj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lauj",
            "<",
            "Lcom/twitter/database/model/f;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/drafts/a;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Laqf;->a:Lauj;

    .line 29
    return-void
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/drafts/a;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 34
    new-instance v0, Lcom/twitter/database/model/f$a;

    invoke-direct {v0}, Lcom/twitter/database/model/f$a;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "sending_state!=1"

    aput-object v2, v1, v4

    const-string/jumbo v2, "_id<>?"

    aput-object v2, v1, v3

    .line 35
    invoke-static {v1}, Laux;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/String;

    .line 36
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 35
    invoke-virtual {v0, v1, v2}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v0

    .line 38
    iget-object v1, p0, Laqf;->a:Lauj;

    invoke-interface {v1, v0}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Laqf;->a:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 44
    return-void
.end method
