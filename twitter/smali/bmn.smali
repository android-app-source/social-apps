.class public Lbmn;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lbmn;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/twitter/database/model/i;

.field private final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lbmn;->a:Ljava/util/Map;

    return-void
.end method

.method constructor <init>(Lcom/twitter/library/provider/j;J)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-virtual {p1}, Lcom/twitter/library/provider/j;->d()Lcom/twitter/database/schema/GlobalSchema;

    move-result-object v0

    iput-object v0, p0, Lbmn;->b:Lcom/twitter/database/model/i;

    .line 51
    iput-wide p2, p0, Lbmn;->c:J

    .line 52
    return-void
.end method

.method public static a(J)Lbmn;
    .locals 4

    .prologue
    .line 38
    sget-object v1, Lbmn;->a:Ljava/util/Map;

    monitor-enter v1

    .line 39
    :try_start_0
    sget-object v0, Lbmn;->a:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmn;

    .line 40
    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lbmn;

    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v2

    invoke-direct {v0, v2, p0, p1}, Lbmn;-><init>(Lcom/twitter/library/provider/j;J)V

    .line 42
    sget-object v2, Lbmn;->a:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    :cond_0
    monitor-exit v1

    .line 45
    return-object v0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private c()Lcom/twitter/database/model/g;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/model/g",
            "<",
            "Lawn$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lbmn;->b:Lcom/twitter/database/model/i;

    const-class v1, Lawn;

    .line 96
    invoke-interface {v0, v1}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Lawn;

    invoke-interface {v0}, Lawn;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    .line 97
    const-string/jumbo v1, "user_id"

    invoke-static {v1}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lbmn;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/nio/ByteBuffer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v1

    .line 58
    invoke-direct {p0}, Lbmn;->c()Lcom/twitter/database/model/g;

    move-result-object v2

    .line 60
    :goto_0
    :try_start_0
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, v2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lawn$a;

    invoke-interface {v0}, Lawn$a;->b()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    iget-object v0, v2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lawn$a;

    .line 62
    invoke-interface {v0}, Lawn$a;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 61
    invoke-virtual {v1, v3, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 65
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/twitter/database/model/g;->close()V

    throw v0

    :cond_0
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->close()V

    .line 67
    invoke-virtual {v1}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lbmn;->b:Lcom/twitter/database/model/i;

    const-class v1, Lawo;

    invoke-interface {v0, v1}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v1

    .line 103
    iget-object v0, p0, Lbmn;->b:Lcom/twitter/database/model/i;

    invoke-interface {v0}, Lcom/twitter/database/model/i;->h()Lcom/twitter/database/model/o;

    move-result-object v2

    .line 105
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 106
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "user_id"

    .line 107
    invoke-static {v7}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "remote_id"

    .line 108
    invoke-static {v7}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, v6

    .line 106
    invoke-static {v0}, Laux;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, p0, Lbmn;->c:J

    .line 109
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    .line 110
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v7

    .line 106
    invoke-interface {v1, v0, v6}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 114
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Lcom/twitter/database/model/o;->close()V

    throw v0

    .line 112
    :cond_0
    :try_start_1
    invoke-interface {v2}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    invoke-interface {v2}, Lcom/twitter/database/model/o;->close()V

    .line 116
    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/nio/ByteBuffer;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lbmn;->b:Lcom/twitter/database/model/i;

    const-class v1, Lawo;

    invoke-interface {v0, v1}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    .line 78
    invoke-interface {v0}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v3

    .line 79
    iget-object v0, p0, Lbmn;->b:Lcom/twitter/database/model/i;

    invoke-interface {v0}, Lcom/twitter/database/model/i;->h()Lcom/twitter/database/model/o;

    move-result-object v4

    .line 81
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {v3, v0}, Lcom/twitter/database/model/h;->a(Z)V

    .line 82
    iget-object v0, v3, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v0, Lawo$a;

    iget-wide v6, p0, Lbmn;->c:J

    invoke-interface {v0, v6, v7}, Lawo$a;->a(J)Lawo$a;

    .line 83
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 84
    iget-object v1, v3, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v1, Lawo$a;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-interface {v1, v2}, Lawo$a;->a([B)Lawo$a;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-interface {v1, v6, v7}, Lawo$a;->b(J)Lawo$a;

    move-result-object v0

    iget-wide v6, p0, Lbmn;->c:J

    invoke-interface {v0, v6, v7}, Lawo$a;->a(J)Lawo$a;

    .line 85
    invoke-virtual {v3}, Lcom/twitter/database/model/h;->b()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 89
    :catchall_0
    move-exception v0

    invoke-interface {v4}, Lcom/twitter/database/model/o;->close()V

    throw v0

    .line 87
    :cond_0
    :try_start_1
    invoke-interface {v4}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    invoke-interface {v4}, Lcom/twitter/database/model/o;->close()V

    .line 91
    return-void
.end method

.method public b()I
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lbmn;->b:Lcom/twitter/database/model/i;

    const-class v1, Lawo;

    invoke-interface {v0, v1}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method
