.class public abstract Lbjb;
.super Lbiy;
.source "Twttr"


# instance fields
.field protected final k:Lcom/twitter/model/av/AVMedia;


# direct methods
.method protected constructor <init>(Lcom/twitter/model/av/AVMedia;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lbiy;-><init>()V

    .line 19
    iput-object p1, p0, Lbjb;->k:Lcom/twitter/model/av/AVMedia;

    .line 20
    return-void
.end method


# virtual methods
.method public a(Lbiw;)Z
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 56
    instance-of v0, p1, Lbja;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbjb;->k:Lcom/twitter/model/av/AVMedia;

    check-cast p1, Lbja;

    iget-object v1, p1, Lbja;->a:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lcom/twitter/library/av/m;
    .locals 4

    .prologue
    .line 31
    invoke-super {p0}, Lbiy;->b()Lcom/twitter/library/av/m;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lbjb;->k:Lcom/twitter/model/av/AVMedia;

    iget-object v2, v0, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 34
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Snapshot media was not equal to media for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 35
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 34
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 38
    :cond_0
    return-object v0
.end method

.method public d()Lcom/twitter/library/av/m;
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lbiy;->b()Lcom/twitter/library/av/m;

    move-result-object v0

    return-object v0
.end method
