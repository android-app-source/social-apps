.class Lbzp;
.super Lbzo;
.source "Twttr"

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field private c:Landroid/view/Choreographer;

.field private d:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lbzo;-><init>()V

    return-void
.end method

.method static synthetic a(Lbzp;)Landroid/view/Choreographer;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lbzp;->c:Landroid/view/Choreographer;

    return-object v0
.end method

.method static synthetic a(Lbzp;Landroid/view/Choreographer;)Landroid/view/Choreographer;
    .locals 0

    .prologue
    .line 9
    iput-object p1, p0, Lbzp;->c:Landroid/view/Choreographer;

    return-object p1
.end method

.method static synthetic a(Lbzp;Z)Z
    .locals 0

    .prologue
    .line 9
    iput-boolean p1, p0, Lbzp;->d:Z

    return p1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lbzp;->a:Landroid/os/Handler;

    new-instance v1, Lbzp$1;

    invoke-direct {v1, p0}, Lbzp$1;-><init>(Lbzp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 35
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lbzp;->a:Landroid/os/Handler;

    new-instance v1, Lbzp$2;

    invoke-direct {v1, p0}, Lbzp$2;-><init>(Lbzp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 48
    return-void
.end method

.method public doFrame(J)V
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lbzp;->d:Z

    if-eqz v0, :cond_0

    .line 18
    iget-object v0, p0, Lbzp;->b:Lbzq;

    invoke-interface {v0, p1, p2}, Lbzq;->a(J)V

    .line 19
    iget-object v0, p0, Lbzp;->c:Landroid/view/Choreographer;

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 21
    :cond_0
    return-void
.end method
