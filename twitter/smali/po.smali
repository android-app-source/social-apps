.class public abstract Lpo;
.super Lbjb;
.source "Twttr"


# instance fields
.field protected final a:Lcom/twitter/library/av/playback/AVPlayer;

.field b:Z


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lpo;->b:Z

    .line 22
    iput-object p1, p0, Lpo;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 23
    return-void
.end method


# virtual methods
.method public a(Lbiw;)Z
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1}, Lbjb;->a(Lbiw;)Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p1, Lbjt;

    if-nez v0, :cond_1

    .line 39
    iget-boolean v0, p0, Lpo;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 41
    :goto_0
    return v0

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 41
    :cond_1
    instance-of v0, p1, Lbjt;

    goto :goto_0
.end method

.method public processLoop(Lbjt;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbjt;
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lpo;->k:Lcom/twitter/model/av/AVMedia;

    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lpo;->b:Z

    .line 30
    :cond_0
    return-void
.end method
