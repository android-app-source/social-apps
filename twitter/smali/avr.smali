.class public Lavr;
.super Lcbr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbr",
        "<",
        "Laww$a;",
        "Lcom/twitter/model/dms/Participant;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcbr;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Laww$a;)Lcom/twitter/model/dms/Participant;
    .locals 4

    .prologue
    .line 18
    const-class v0, Laxp$a;

    const-class v1, Lcom/twitter/model/core/TwitterUser;

    .line 19
    invoke-static {v0, v1}, Lcom/twitter/database/hydrator/b;->a(Ljava/lang/Class;Ljava/lang/Class;)Lcbr;

    move-result-object v0

    .line 21
    if-eqz v0, :cond_0

    invoke-interface {p1}, Laww$a;->g()Laxp$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcbr;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 22
    invoke-interface {p1}, Laww$a;->g()Laxp$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcbr;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 27
    :goto_0
    new-instance v1, Lcom/twitter/model/dms/Participant$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/Participant$a;-><init>()V

    .line 28
    invoke-interface {p1}, Laww$a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/model/dms/Participant$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/Participant$a;

    move-result-object v1

    .line 29
    invoke-interface {p1}, Laww$a;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/dms/Participant$a;->a(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v1

    .line 30
    invoke-interface {p1}, Laww$a;->d()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/dms/Participant$a;->b(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v1

    .line 31
    invoke-interface {p1}, Laww$a;->f()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/dms/Participant$a;->d(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v1

    .line 32
    invoke-interface {p1}, Laww$a;->e()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/dms/Participant$a;->c(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v1

    .line 33
    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/Participant$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcom/twitter/model/dms/Participant$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/Participant;

    .line 27
    return-object v0

    .line 24
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    check-cast p1, Laww$a;

    invoke-virtual {p0, p1}, Lavr;->a(Laww$a;)Lcom/twitter/model/dms/Participant;

    move-result-object v0

    return-object v0
.end method
