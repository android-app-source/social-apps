.class Lapk$b;
.super Landroid/widget/ArrayAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lapk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lccp;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 267
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lapk$b;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lapk$b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 262
    invoke-direct {p0}, Lapk$b;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lapk$b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 262
    invoke-direct {p0, p1}, Lapk$b;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lapk$b;->a:Ljava/lang/String;

    .line 301
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 272
    if-eqz p2, :cond_0

    .line 274
    :goto_0
    invoke-virtual {p0, p1}, Lapk$b;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccp;

    .line 276
    const v1, 0x7f13007d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 277
    const v2, 0x7f1301e9

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 278
    iget-object v5, v0, Lccp;->c:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    iget-object v1, v0, Lccp;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 281
    iget-object v0, v0, Lccp;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    :goto_1
    check-cast p3, Landroid/widget/ListView;

    invoke-virtual {p3}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    .line 287
    const/4 v0, -0x1

    if-eq v1, v0, :cond_2

    move v0, v3

    .line 288
    :goto_2
    if-nez v0, :cond_3

    move v0, v3

    :goto_3
    invoke-virtual {p2, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 289
    if-ne p1, v1, :cond_4

    :goto_4
    invoke-virtual {p2, v3}, Landroid/view/View;->setActivated(Z)V

    .line 290
    invoke-virtual {p2}, Landroid/view/View;->requestLayout()V

    .line 291
    return-object p2

    .line 272
    :cond_0
    invoke-virtual {p0}, Lapk$b;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400bf

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 283
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    move v0, v4

    .line 287
    goto :goto_2

    :cond_3
    move v0, v4

    .line 288
    goto :goto_3

    :cond_4
    move v3, v4

    .line 289
    goto :goto_4
.end method
