.class Lcdf$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcdf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcdf;",
        "Lcdf$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcdf$1;)V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Lcdf$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcdf$a;
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lcdf$a;

    invoke-direct {v0}, Lcdf$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcdf$a;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 147
    sget-object v0, Lcdd;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdd;

    .line 149
    new-instance v1, Lcdd$a;

    invoke-direct {v1, v0}, Lcdd$a;-><init>(Lcdd;)V

    invoke-virtual {p2, v1}, Lcdf$a;->a(Lcdd$a;)Lcdf$a;

    move-result-object v0

    .line 150
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcdf$a;->a(Ljava/lang/String;)Lcdf$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    sget-object v2, Lccz;->a:Lcom/twitter/util/serialization/l;

    .line 151
    invoke-static {p1, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcdf$a;->a(Ljava/util/Map;)Lcdf$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    .line 153
    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->d(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcdf$a;->a(Ljava/util/Set;)Lcdf$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    sget-object v2, Lccv;->a:Lcom/twitter/util/serialization/l;

    .line 154
    invoke-static {p1, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcdf$a;->b(Ljava/util/Map;)Lcdf$a;

    .line 156
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 124
    check-cast p2, Lcdf$a;

    invoke-virtual {p0, p1, p2, p3}, Lcdf$b;->a(Lcom/twitter/util/serialization/n;Lcdf$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcdf;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    sget-object v0, Lcdd;->a:Lcom/twitter/util/serialization/l;

    iget-object v1, p2, Lcdf;->c:Lcdd;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 130
    iget-object v0, p2, Lcdf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 131
    iget-object v0, p2, Lcdf;->e:Ljava/util/Map;

    sget-object v1, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    sget-object v2, Lccz;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Map;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)V

    .line 133
    iget-object v0, p2, Lcdf;->f:Ljava/util/Set;

    sget-object v1, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Set;Lcom/twitter/util/serialization/l;)V

    .line 134
    iget-object v0, p2, Lcdf;->g:Ljava/util/Map;

    sget-object v1, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    sget-object v2, Lccv;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Map;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)V

    .line 136
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    check-cast p2, Lcdf;

    invoke-virtual {p0, p1, p2}, Lcdf$b;->a(Lcom/twitter/util/serialization/o;Lcdf;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0}, Lcdf$b;->a()Lcdf$a;

    move-result-object v0

    return-object v0
.end method
