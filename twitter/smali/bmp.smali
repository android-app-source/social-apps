.class public Lbmp;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbmp$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/content/Context;JI)V
    .locals 5

    .prologue
    .line 48
    new-instance v0, Lcom/twitter/util/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 49
    invoke-static {}, Lbsx;->b()Lbsx;

    move-result-object v1

    .line 50
    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v2, "pref_contacts_live_sync_opt_in"

    invoke-virtual {v0, v2, p3}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;I)Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 52
    if-nez p3, :cond_0

    .line 53
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v2, Lbmm;

    .line 54
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lbmm;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 53
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 55
    new-instance v0, Lbsv;

    new-instance v2, Lcom/twitter/util/a;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    invoke-direct {v0, v2}, Lbsv;-><init>(Lcom/twitter/util/a;)V

    const/4 v2, 0x0

    .line 56
    invoke-virtual {v0, v2}, Lbsv;->a(Z)V

    .line 59
    :cond_0
    if-eqz v1, :cond_1

    .line 60
    invoke-virtual {v1}, Lbsx;->a()V

    .line 62
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 44
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "android.permission.READ_CONTACTS"

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;J)Z
    .locals 3

    .prologue
    .line 40
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    invoke-static {p0}, Lbmp;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, Lbmp;->c(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 73
    invoke-static {p0}, Lbmp;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 78
    if-eqz p1, :cond_2

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lbmp;->b(Landroid/content/Context;J)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;J)Z
    .locals 1

    .prologue
    .line 82
    invoke-static {p0, p1, p2}, Lbmp;->f(Landroid/content/Context;J)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;J)Z
    .locals 3

    .prologue
    .line 86
    invoke-static {p0, p1, p2}, Lbmp;->f(Landroid/content/Context;J)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;J)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 90
    invoke-static {p0, p1, p2}, Lbmp;->f(Landroid/content/Context;J)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Landroid/content/Context;J)I
    .locals 1

    .prologue
    .line 20
    invoke-static {p0, p1, p2}, Lbmp;->f(Landroid/content/Context;J)I

    move-result v0

    return v0
.end method

.method private static f(Landroid/content/Context;J)I
    .locals 3

    .prologue
    .line 95
    new-instance v0, Lcom/twitter/util/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    const-string/jumbo v1, "pref_contacts_live_sync_opt_in"

    const/4 v2, 0x0

    .line 96
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/a;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 97
    return v0
.end method
