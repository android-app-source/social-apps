.class public final Lauz$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lauz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MODE",
        "L:Ljava/lang/Object;",
        ">",
        "Lcom/twitter/util/object/i",
        "<",
        "Lauz",
        "<TMODE",
        "L;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/database/model/i;

.field private c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/twitter/database/model/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/model/l",
            "<*>;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TMODE",
            "L;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/net/Uri;

.field private g:Lcom/twitter/database/model/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/database/model/i;)V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 196
    iput-object p1, p0, Lauz$a;->a:Landroid/content/Context;

    .line 197
    iput-object p2, p0, Lauz$a;->b:Lcom/twitter/database/model/i;

    .line 198
    return-void
.end method

.method static synthetic a(Lauz$a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lauz$a;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lauz$a;)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lauz$a;->e:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic c(Lauz$a;)Lcom/twitter/database/model/f;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lauz$a;->g:Lcom/twitter/database/model/f;

    return-object v0
.end method

.method static synthetic d(Lauz$a;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lauz$a;->f:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic e(Lauz$a;)Lcom/twitter/database/model/l;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lauz$a;->d:Lcom/twitter/database/model/l;

    return-object v0
.end method

.method static synthetic f(Lauz$a;)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lauz$a;->c:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic g(Lauz$a;)Lcom/twitter/database/model/i;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lauz$a;->b:Lcom/twitter/database/model/i;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 242
    invoke-super {p0}, Lcom/twitter/util/object/i;->R_()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lauz$a;->c:Ljava/lang/Class;

    if-nez v0, :cond_0

    iget-object v0, p0, Lauz$a;->d:Lcom/twitter/database/model/l;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lauz$a;->c:Ljava/lang/Class;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lauz$a;->d:Lcom/twitter/database/model/l;

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lauz$a;->e:Ljava/lang/Class;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;)Lauz$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lauz$a",
            "<TMODE",
            "L;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    iput-object p1, p0, Lauz$a;->f:Landroid/net/Uri;

    .line 231
    return-object p0
.end method

.method public a(Lcom/twitter/database/model/f;)Lauz$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/model/f;",
            ")",
            "Lauz$a",
            "<TMODE",
            "L;",
            ">;"
        }
    .end annotation

    .prologue
    .line 236
    iput-object p1, p0, Lauz$a;->g:Lcom/twitter/database/model/f;

    .line 237
    return-object p0
.end method

.method public a(Ljava/lang/Class;)Lauz$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;)",
            "Lauz$a",
            "<TMODE",
            "L;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    iput-object p1, p0, Lauz$a;->c:Ljava/lang/Class;

    .line 204
    return-object p0
.end method

.method public b(Ljava/lang/Class;)Lauz$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TMODE",
            "L;",
            ">;)",
            "Lauz$a",
            "<TMODE",
            "L;",
            ">;"
        }
    .end annotation

    .prologue
    .line 216
    iput-object p1, p0, Lauz$a;->e:Ljava/lang/Class;

    .line 217
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lauz$a;->e()Lauz;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lauz;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lauz",
            "<TMODE",
            "L;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    new-instance v0, Lauz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lauz;-><init>(Lauz$a;Lauz$1;)V

    return-object v0
.end method
