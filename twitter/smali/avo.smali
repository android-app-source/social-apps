.class public Lavo;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcom/twitter/util/collection/Pair",
        "<",
        "Ljava/lang/String;",
        "Lcom/twitter/model/dms/q$a;",
        ">;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcbp;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/twitter/util/collection/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/dms/q$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x5

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 23
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 24
    new-instance v0, Lcom/twitter/model/dms/q$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/q$a;-><init>()V

    .line 25
    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/q$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    const/4 v4, 0x2

    .line 26
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/dms/q$a;->b(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    const/4 v4, 0x7

    .line 27
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/dms/q$a;->c(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v4

    const/4 v0, 0x3

    .line 28
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Lcom/twitter/model/dms/q$a;->a(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 29
    invoke-interface {p1, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-wide/16 v4, 0x0

    :goto_1
    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/dms/q$a;->a(J)Lcom/twitter/model/dms/q$a;

    move-result-object v4

    .line 31
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v4, v0}, Lcom/twitter/model/dms/q$a;->b(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v4

    const/4 v0, 0x4

    .line 32
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v4, v0}, Lcom/twitter/model/dms/q$a;->c(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    const/4 v4, 0x6

    .line 33
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-lez v4, :cond_4

    :goto_4
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->d(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v1

    const/16 v0, 0x8

    .line 35
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v2, Lcom/twitter/model/dms/x;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/x;

    .line 34
    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/q$a;->a(Lcom/twitter/model/dms/x;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 36
    invoke-static {v3, v0}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 28
    goto :goto_0

    .line 30
    :cond_1
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    goto :goto_1

    :cond_2
    move v0, v2

    .line 31
    goto :goto_2

    :cond_3
    move v0, v2

    .line 32
    goto :goto_3

    :cond_4
    move v1, v2

    .line 33
    goto :goto_4
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lavo;->a(Landroid/database/Cursor;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    return-object v0
.end method
