.class public Laee;
.super Laef;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laee$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laef",
        "<",
        "Lcah;",
        "Laee$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final b:Lcom/twitter/android/notificationtimeline/k;

.field private final c:Landroid/content/res/Resources;

.field private final d:J


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/twitter/android/notificationtimeline/k;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;J)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p4, p5}, Laef;-><init>(Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;)V

    .line 57
    iput-object p2, p0, Laee;->b:Lcom/twitter/android/notificationtimeline/k;

    .line 58
    iput-object p1, p0, Laee;->c:Landroid/content/res/Resources;

    .line 59
    iput-object p3, p0, Laee;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 60
    iput-wide p6, p0, Laee;->d:J

    .line 61
    return-void
.end method

.method static synthetic a(Laee;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Laee;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/ui/widget/TypefacesTextView;Lcom/twitter/model/core/Tweet;Z)V
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    .line 178
    if-eqz p3, :cond_0

    .line 179
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/TypefacesTextView;->setGravity(I)V

    .line 181
    :cond_0
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Laee$a;
    .locals 3

    .prologue
    .line 66
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040028

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 67
    new-instance v1, Laee$a;

    invoke-direct {v1, v0}, Laee$a;-><init>(Landroid/view/View;)V

    .line 68
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 69
    return-object v1
.end method

.method protected bridge synthetic a(Landroid/content/Context;Lbzz;I)Lcom/twitter/analytics/model/ScribeItem;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 43
    check-cast p2, Lcah;

    invoke-virtual {p0, p1, p2, p3}, Laee;->a(Landroid/content/Context;Lcah;I)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcah;I)Lcom/twitter/analytics/model/ScribeItem;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p2, Lcah;->b:Lcac;

    check-cast v0, Lcap;

    .line 149
    iget v1, v0, Lcap;->d:I

    packed-switch v1, :pswitch_data_0

    .line 170
    :pswitch_0
    iget v0, v0, Lcap;->d:I

    invoke-static {p3, v0}, Lcom/twitter/android/notificationtimeline/a;->a(II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    :goto_0
    return-object v0

    .line 151
    :pswitch_1
    invoke-virtual {v0}, Lcap;->f()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/TwitterUser;

    iget v0, v0, Lcap;->d:I

    invoke-static {v1, p3, v0}, Lcom/twitter/android/notificationtimeline/a;->a(Lcom/twitter/model/core/TwitterUser;II)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    goto :goto_0

    .line 155
    :pswitch_2
    invoke-virtual {p2}, Lcah;->d()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbzv;

    iget v0, v0, Lcap;->d:I

    invoke-static {v1, p3, v0}, Lcom/twitter/android/notificationtimeline/a;->a(Lbzv;II)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    goto :goto_0

    .line 167
    :pswitch_3
    invoke-virtual {p2}, Lcah;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/Tweet;

    iget v0, v0, Lcap;->d:I

    .line 166
    invoke-static {p1, v1, p3, v0}, Lcom/twitter/android/notificationtimeline/a;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;II)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    goto :goto_0

    .line 149
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public a(Laee$a;Lcah;)V
    .locals 11

    .prologue
    .line 74
    invoke-super {p0, p1, p2}, Laef;->a(Laef$a;Lbzz;)V

    .line 75
    invoke-virtual {p1}, Laee$a;->b()Landroid/view/View;

    move-result-object v3

    .line 76
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 77
    iget-object v0, p2, Lcah;->b:Lcac;

    check-cast v0, Lcap;

    .line 78
    invoke-virtual {v0}, Lcap;->f()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/TwitterUser;

    .line 79
    iget-object v4, p1, Laee$a;->a:Landroid/widget/ImageView;

    sget-object v2, Laeb;->a:Ljava/util/Map;

    iget v5, v0, Lcap;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laed;

    invoke-static {v4, v2}, Laea;->a(Landroid/widget/ImageView;Laed;)V

    .line 81
    invoke-virtual {p2}, Lcah;->e()I

    move-result v2

    invoke-virtual {p2}, Lcah;->a()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/Collection;)I

    move-result v4

    if-le v2, v4, :cond_0

    .line 82
    iget-object v2, p1, Laee$a;->c:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    iget-object v4, p0, Laee;->c:Landroid/content/res/Resources;

    const v5, 0x7f0e00c7

    .line 84
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    iget-object v7, p0, Laee;->c:Landroid/content/res/Resources;

    const v8, 0x7f0e00cb

    .line 85
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 83
    invoke-virtual {v3, v2, v4, v5, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 90
    :goto_0
    invoke-virtual {p2}, Lcah;->a()Ljava/util/List;

    move-result-object v7

    .line 91
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    .line 92
    if-lez v2, :cond_2

    .line 93
    iget-object v3, p1, Laee$a;->d:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 94
    iget-object v3, p1, Laee$a;->e:[Landroid/view/View;

    array-length v8, v3

    .line 95
    invoke-static {v2, v8}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 96
    const/4 v2, 0x0

    move v5, v2

    :goto_1
    if-ge v5, v4, :cond_1

    .line 97
    iget-object v2, p1, Laee$a;->e:[Landroid/view/View;

    aget-object v9, v2, v5

    .line 98
    iget-object v2, p1, Laee$a;->f:[Landroid/view/View;

    aget-object v2, v2, v5

    check-cast v2, Lcom/twitter/ui/widget/TypefacesTextView;

    .line 99
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/core/Tweet;

    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v10

    invoke-static {v6, v2, v3, v10}, Laee;->a(Landroid/content/Context;Lcom/twitter/ui/widget/TypefacesTextView;Lcom/twitter/model/core/Tweet;Z)V

    .line 100
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Landroid/view/View;->setVisibility(I)V

    .line 96
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    .line 87
    :cond_0
    iget-object v2, p1, Laee$a;->c:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v2, v4

    .line 102
    :goto_2
    if-ge v2, v8, :cond_3

    .line 103
    iget-object v3, p1, Laee$a;->e:[Landroid/view/View;

    aget-object v3, v3, v2

    .line 104
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 105
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 102
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 108
    :cond_2
    iget-object v2, p1, Laee$a;->d:Landroid/view/ViewGroup;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 111
    :cond_3
    iget-object v2, p1, Laee$a;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Lcap;->f()Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3}, Laea;->a(Landroid/view/ViewGroup;Ljava/util/List;)V

    .line 113
    iget-object v2, p1, Laee$a;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-static {}, Lbpi;->b()I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 114
    iget-object v2, p1, Laee$a;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 116
    iget v2, v0, Lcap;->d:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_4

    .line 119
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0490

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 120
    invoke-virtual {v0}, Lcap;->f()Ljava/util/List;

    move-result-object v0

    const/4 v7, 0x0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    aput-object v0, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p2}, Lcah;->d()Ljava/util/List;

    move-result-object v0

    const/4 v7, 0x0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzv;

    iget-object v0, v0, Lbzv;->c:Ljava/lang/String;

    aput-object v0, v4, v5

    .line 119
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 121
    iget-object v2, p1, Laee$a;->b:Landroid/widget/TextView;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v4, 0x0

    new-instance v5, Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v7, 0x1

    invoke-direct {v5, v6, v7}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v7, 0x1

    invoke-direct {v5, v6, v7}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v5, v3, v4

    const/16 v4, 0x22

    invoke-static {v3, v0, v4}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;C)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v0, p1, Laee$a;->i:Lcom/twitter/media/ui/image/UserImageView;

    new-instance v2, Laee$1;

    invoke-direct {v2, p0, v6, v1}, Laee$1;-><init>(Laee;Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;)V

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    :goto_3
    return-void

    .line 136
    :cond_4
    iget-object v10, p1, Laee$a;->b:Landroid/widget/TextView;

    iget-object v1, p0, Laee;->b:Lcom/twitter/android/notificationtimeline/k;

    iget v2, v0, Lcap;->d:I

    .line 137
    invoke-virtual {v0}, Lcap;->f()Ljava/util/List;

    move-result-object v3

    iget v4, v0, Lcap;->e:I

    invoke-virtual {p2}, Lcah;->a()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p2}, Lcah;->e()I

    move-result v6

    const/4 v7, 0x0

    iget-wide v8, p0, Laee;->d:J

    .line 136
    invoke-virtual/range {v1 .. v9}, Lcom/twitter/android/notificationtimeline/k;->a(ILjava/util/List;ILjava/util/List;ILjava/lang/String;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public bridge synthetic a(Laef$a;Lbzz;)V
    .locals 0

    .prologue
    .line 43
    check-cast p1, Laee$a;

    check-cast p2, Lcah;

    invoke-virtual {p0, p1, p2}, Laee;->a(Laee$a;Lcah;)V

    return-void
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 43
    check-cast p1, Laee$a;

    check-cast p2, Lcah;

    invoke-virtual {p0, p1, p2}, Laee;->a(Laee$a;Lcah;)V

    return-void
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Laee;->a(Landroid/view/ViewGroup;)Laee$a;

    move-result-object v0

    return-object v0
.end method
