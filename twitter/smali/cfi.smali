.class public Lcfi;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcfi$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcfi;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Lcfi;


# instance fields
.field public final b:J

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 20
    new-instance v0, Lcfi$a;

    invoke-direct {v0}, Lcfi$a;-><init>()V

    sput-object v0, Lcfi;->a:Lcom/twitter/util/serialization/l;

    .line 22
    new-instance v0, Lcfi;

    const-wide/16 v2, 0x0

    const/4 v1, 0x3

    invoke-direct {v0, v2, v3, v1}, Lcfi;-><init>(JI)V

    sput-object v0, Lcfi;->d:Lcfi;

    return-void
.end method

.method constructor <init>(JI)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-wide p1, p0, Lcfi;->b:J

    .line 38
    iput p3, p0, Lcfi;->c:I

    .line 39
    return-void
.end method

.method public static a()Lcfi;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcfi;->d:Lcfi;

    return-object v0
.end method

.method public static a(J)Lcfi;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcfi;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lcfi;-><init>(JI)V

    return-object v0
.end method

.method public static b(J)Lcfi;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lcfi;

    const/4 v1, 0x2

    invoke-direct {v0, p0, p1, v1}, Lcfi;-><init>(JI)V

    return-object v0
.end method
