.class public Lcia;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Lcgi;

.field public final d:Lcom/twitter/model/core/TwitterSocialProof;

.field public final e:Lcom/twitter/model/revenue/d;

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 28
    const-string/jumbo v0, "Tweet"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "NewsTweet"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "TweetFollowOnly"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "MomentTweet"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcia;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcgi;Lcom/twitter/model/core/TwitterSocialProof;Lcom/twitter/model/revenue/d;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcia;->b:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Lcia;->f:Ljava/lang/String;

    .line 55
    iput-object p3, p0, Lcia;->c:Lcgi;

    .line 56
    iput-object p4, p0, Lcia;->d:Lcom/twitter/model/core/TwitterSocialProof;

    .line 57
    iput-object p5, p0, Lcia;->e:Lcom/twitter/model/revenue/d;

    .line 58
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcia;->f:Ljava/lang/String;

    return-object v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcia;->d:Lcom/twitter/model/core/TwitterSocialProof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcia;->d:Lcom/twitter/model/core/TwitterSocialProof;

    iget v0, v0, Lcom/twitter/model/core/TwitterSocialProof;->k:I

    const/16 v1, 0x2b

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcia;->d:Lcom/twitter/model/core/TwitterSocialProof;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterSocialProof;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
