.class public abstract Lccb$a;
.super Lcbz$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lccb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lccb;",
        "B:",
        "Lccb$a",
        "<TE;TB;>;>",
        "Lcbz$a",
        "<TE;TB;>;"
    }
.end annotation


# instance fields
.field private f:J

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcbz$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lccb$a;)J
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lccb$a;->f:J

    return-wide v0
.end method

.method static synthetic b(Lccb$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lccb$a;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 4

    .prologue
    .line 72
    invoke-super {p0}, Lcbz$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lccb$a;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic a(Lcax;)Lcbz$a;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lccb$a;->b(Lcax;)Lccb$a;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcax;)Lccb$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcax;",
            ")TB;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-super {p0, p1}, Lcbz$a;->a(Lcax;)Lcbz$a;

    .line 81
    if-eqz p1, :cond_0

    .line 82
    const-string/jumbo v0, "feedback_id"

    invoke-virtual {p1, v0}, Lcax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 83
    const-string/jumbo v0, "display_name"

    invoke-virtual {p1, v0}, Lcax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    :goto_0
    const-wide/16 v2, -0x1

    invoke-static {v1, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lccb$a;->f:J

    .line 90
    iput-object v0, p0, Lccb$a;->g:Ljava/lang/String;

    .line 91
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccb$a;

    return-object v0

    :cond_0
    move-object v1, v0

    .line 86
    goto :goto_0
.end method
