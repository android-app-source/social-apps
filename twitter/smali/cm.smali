.class public Lcm;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/cache/disk/g;

.field private final c:Lcom/facebook/imagepipeline/memory/w;

.field private final d:Lcom/facebook/imagepipeline/memory/z;

.field private final e:Ljava/util/concurrent/Executor;

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:Ldc;

.field private final h:Lcv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcm;

    sput-object v0, Lcm;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/cache/disk/g;Lcom/facebook/imagepipeline/memory/w;Lcom/facebook/imagepipeline/memory/z;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcv;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcm;->b:Lcom/facebook/cache/disk/g;

    .line 57
    iput-object p2, p0, Lcm;->c:Lcom/facebook/imagepipeline/memory/w;

    .line 58
    iput-object p3, p0, Lcm;->d:Lcom/facebook/imagepipeline/memory/z;

    .line 59
    iput-object p4, p0, Lcm;->e:Ljava/util/concurrent/Executor;

    .line 60
    iput-object p5, p0, Lcm;->f:Ljava/util/concurrent/Executor;

    .line 61
    iput-object p6, p0, Lcm;->h:Lcv;

    .line 62
    invoke-static {}, Ldc;->a()Ldc;

    move-result-object v0

    iput-object v0, p0, Lcm;->g:Ldc;

    .line 63
    return-void
.end method

.method static synthetic a(Lcm;Lcom/facebook/cache/common/a;)Lcom/facebook/imagepipeline/memory/PooledByteBuffer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcm;->a(Lcom/facebook/cache/common/a;)Lcom/facebook/imagepipeline/memory/PooledByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/cache/common/a;)Lcom/facebook/imagepipeline/memory/PooledByteBuffer;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    :try_start_0
    sget-object v0, Lcm;->a:Ljava/lang/Class;

    const-string/jumbo v1, "Disk cache read for %s"

    invoke-interface {p1}, Lcom/facebook/cache/common/a;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbb;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 290
    iget-object v0, p0, Lcm;->b:Lcom/facebook/cache/disk/g;

    invoke-interface {v0, p1}, Lcom/facebook/cache/disk/g;->a(Lcom/facebook/cache/common/a;)Laa;

    move-result-object v0

    .line 291
    if-nez v0, :cond_0

    .line 292
    sget-object v0, Lcm;->a:Ljava/lang/Class;

    const-string/jumbo v1, "Disk cache miss for %s"

    invoke-interface {p1}, Lcom/facebook/cache/common/a;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbb;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 293
    iget-object v0, p0, Lcm;->h:Lcv;

    invoke-interface {v0}, Lcv;->j()V

    .line 294
    const/4 v0, 0x0

    .line 309
    :goto_0
    return-object v0

    .line 296
    :cond_0
    sget-object v1, Lcm;->a:Ljava/lang/Class;

    const-string/jumbo v2, "Found entry in disk cache for %s"

    invoke-interface {p1}, Lcom/facebook/cache/common/a;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lbb;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 297
    iget-object v1, p0, Lcm;->h:Lcv;

    invoke-interface {v1}, Lcv;->i()V

    .line 301
    invoke-interface {v0}, Laa;->a()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 303
    :try_start_1
    iget-object v2, p0, Lcm;->c:Lcom/facebook/imagepipeline/memory/w;

    invoke-interface {v0}, Laa;->b()J

    move-result-wide v4

    long-to-int v0, v4

    invoke-interface {v2, v1, v0}, Lcom/facebook/imagepipeline/memory/w;->b(Ljava/io/InputStream;I)Lcom/facebook/imagepipeline/memory/PooledByteBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 305
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 308
    sget-object v1, Lcm;->a:Ljava/lang/Class;

    const-string/jumbo v2, "Successful read from disk cache for %s"

    invoke-interface {p1}, Lcom/facebook/cache/common/a;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lbb;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 310
    :catch_0
    move-exception v0

    .line 314
    sget-object v1, Lcm;->a:Ljava/lang/Class;

    const-string/jumbo v2, "Exception reading from cache for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, Lcom/facebook/cache/common/a;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, Lbb;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 315
    iget-object v1, p0, Lcm;->h:Lcv;

    invoke-interface {v1}, Lcv;->k()V

    .line 316
    throw v0

    .line 305
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
.end method

.method static synthetic a(Lcm;)Ldc;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcm;->g:Ldc;

    return-object v0
.end method

.method static synthetic a(Lcm;Lcom/facebook/cache/common/a;Lds;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcm;->b(Lcom/facebook/cache/common/a;Lds;)V

    return-void
.end method

.method static synthetic b(Lcm;)Lcv;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcm;->h:Lcv;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcm;->a:Ljava/lang/Class;

    return-object v0
.end method

.method private b(Lcom/facebook/cache/common/a;Lds;)V
    .locals 6

    .prologue
    .line 327
    sget-object v0, Lcm;->a:Ljava/lang/Class;

    const-string/jumbo v1, "About to write to disk-cache for key %s"

    invoke-interface {p1}, Lcom/facebook/cache/common/a;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbb;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 329
    :try_start_0
    iget-object v0, p0, Lcm;->b:Lcom/facebook/cache/disk/g;

    new-instance v1, Lcm$4;

    invoke-direct {v1, p0, p2}, Lcm$4;-><init>(Lcm;Lds;)V

    invoke-interface {v0, p1, v1}, Lcom/facebook/cache/disk/g;->a(Lcom/facebook/cache/common/a;Lcom/facebook/cache/common/e;)Laa;

    .line 337
    sget-object v0, Lcm;->a:Ljava/lang/Class;

    const-string/jumbo v1, "Successful disk-cache write for key %s"

    invoke-interface {p1}, Lcom/facebook/cache/common/a;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbb;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 343
    :goto_0
    return-void

    .line 338
    :catch_0
    move-exception v0

    .line 341
    sget-object v1, Lcm;->a:Ljava/lang/Class;

    const-string/jumbo v2, "Failed to write to disk-cache for key %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, Lcom/facebook/cache/common/a;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, Lbb;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic c(Lcm;)Lcom/facebook/cache/disk/g;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcm;->b:Lcom/facebook/cache/disk/g;

    return-object v0
.end method

.method static synthetic d(Lcm;)Lcom/facebook/imagepipeline/memory/z;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcm;->d:Lcom/facebook/imagepipeline/memory/z;

    return-object v0
.end method


# virtual methods
.method public a()Lbolts/d;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbolts/d",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 263
    iget-object v0, p0, Lcm;->g:Ldc;

    invoke-virtual {v0}, Ldc;->b()V

    .line 265
    :try_start_0
    new-instance v0, Lcm$3;

    invoke-direct {v0, p0}, Lcm$3;-><init>(Lcm;)V

    iget-object v1, p0, Lcm;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, Lbolts/d;->a(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;)Lbolts/d;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 279
    :goto_0
    return-object v0

    .line 275
    :catch_0
    move-exception v0

    .line 278
    sget-object v1, Lcm;->a:Ljava/lang/Class;

    const-string/jumbo v2, "Failed to schedule disk-cache clear"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lbb;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 279
    invoke-static {v0}, Lbolts/d;->a(Ljava/lang/Exception;)Lbolts/d;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/facebook/cache/common/a;Ljava/util/concurrent/atomic/AtomicBoolean;)Lbolts/d;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/cache/common/a;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ")",
            "Lbolts/d",
            "<",
            "Lds;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    invoke-static {p1}, Lax;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    invoke-static {p2}, Lax;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    iget-object v0, p0, Lcm;->g:Ldc;

    invoke-virtual {v0, p1}, Ldc;->a(Lcom/facebook/cache/common/a;)Lds;

    move-result-object v0

    .line 131
    if-eqz v0, :cond_0

    .line 132
    sget-object v1, Lcm;->a:Ljava/lang/Class;

    const-string/jumbo v2, "Found image for %s in staging area"

    invoke-interface {p1}, Lcom/facebook/cache/common/a;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lbb;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 133
    iget-object v1, p0, Lcm;->h:Lcv;

    invoke-interface {v1}, Lcv;->g()V

    .line 134
    invoke-static {v0}, Lbolts/d;->a(Ljava/lang/Object;)Lbolts/d;

    move-result-object v0

    .line 187
    :goto_0
    return-object v0

    .line 138
    :cond_0
    :try_start_0
    new-instance v0, Lcm$1;

    invoke-direct {v0, p0, p2, p1}, Lcm$1;-><init>(Lcm;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/facebook/cache/common/a;)V

    iget-object v1, p0, Lcm;->e:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, Lbolts/d;->a(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;)Lbolts/d;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 179
    :catch_0
    move-exception v0

    .line 182
    sget-object v1, Lcm;->a:Ljava/lang/Class;

    const-string/jumbo v2, "Failed to schedule disk-cache read for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, Lcom/facebook/cache/common/a;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, Lbb;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    invoke-static {v0}, Lbolts/d;->a(Ljava/lang/Exception;)Lbolts/d;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/facebook/cache/common/a;Lds;)V
    .locals 7

    .prologue
    .line 198
    invoke-static {p1}, Lax;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    invoke-static {p2}, Lds;->e(Lds;)Z

    move-result v0

    invoke-static {v0}, Lax;->a(Z)V

    .line 202
    iget-object v0, p0, Lcm;->g:Ldc;

    invoke-virtual {v0, p1, p2}, Ldc;->a(Lcom/facebook/cache/common/a;Lds;)V

    .line 207
    invoke-static {p2}, Lds;->a(Lds;)Lds;

    move-result-object v1

    .line 209
    :try_start_0
    iget-object v0, p0, Lcm;->f:Ljava/util/concurrent/Executor;

    new-instance v2, Lcm$2;

    invoke-direct {v2, p0, p1, v1}, Lcm$2;-><init>(Lcm;Lcom/facebook/cache/common/a;Lds;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :goto_0
    return-void

    .line 221
    :catch_0
    move-exception v0

    .line 224
    sget-object v2, Lcm;->a:Ljava/lang/Class;

    const-string/jumbo v3, "Failed to schedule disk-cache write for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {p1}, Lcom/facebook/cache/common/a;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, Lbb;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    iget-object v0, p0, Lcm;->g:Ldc;

    invoke-virtual {v0, p1, p2}, Ldc;->b(Lcom/facebook/cache/common/a;Lds;)Z

    .line 230
    invoke-static {v1}, Lds;->d(Lds;)V

    goto :goto_0
.end method
