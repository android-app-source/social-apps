.class public Lcgi;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcgi$b;,
        Lcgi$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcgi;",
            "Lcgi$a;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcgi;


# instance fields
.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Z

.field public final i:Z

.field public final j:J

.field public final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcgi$b;

    invoke-direct {v0}, Lcgi$b;-><init>()V

    sput-object v0, Lcgi;->a:Lcom/twitter/util/serialization/b;

    .line 40
    new-instance v0, Lcgi$a;

    invoke-direct {v0}, Lcgi$a;-><init>()V

    invoke-virtual {v0}, Lcgi$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    sput-object v0, Lcgi;->b:Lcgi;

    return-void
.end method

.method public constructor <init>(Lcgi$a;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iget-object v0, p1, Lcgi$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcgi;->c:Ljava/lang/String;

    .line 63
    iget-object v0, p1, Lcgi$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcgi;->d:Ljava/lang/String;

    .line 64
    iget-wide v0, p1, Lcgi$a;->c:J

    iput-wide v0, p0, Lcgi;->e:J

    .line 65
    iget-object v0, p1, Lcgi$a;->d:Ljava/lang/String;

    iput-object v0, p0, Lcgi;->f:Ljava/lang/String;

    .line 66
    iget-object v0, p1, Lcgi$a;->e:Ljava/lang/String;

    iput-object v0, p0, Lcgi;->g:Ljava/lang/String;

    .line 67
    iget-boolean v0, p1, Lcgi$a;->f:Z

    iput-boolean v0, p0, Lcgi;->h:Z

    .line 68
    iget-boolean v0, p1, Lcgi$a;->g:Z

    iput-boolean v0, p0, Lcgi;->i:Z

    .line 69
    iget-wide v0, p1, Lcgi$a;->h:J

    iput-wide v0, p0, Lcgi;->j:J

    .line 70
    iget-object v0, p1, Lcgi$a;->i:Ljava/util/Set;

    invoke-static {v0}, Lcom/twitter/util/collection/ImmutableSet;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcgi;->k:Ljava/util/Set;

    .line 71
    iget-boolean v0, p1, Lcgi$a;->j:Z

    iput-boolean v0, p0, Lcgi;->l:Z

    .line 72
    return-void
.end method

.method public static a([B)Lcgi;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p0, v0}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    return-object v0
.end method

.method public static a(Lcgi;)[B
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p0, v0}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcgi;->hashCode()I

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 93
    const-string/jumbo v0, "Political"

    iget-object v1, p0, Lcgi;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 97
    const-string/jumbo v0, "Earned"

    iget-object v1, p0, Lcgi;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 101
    const-string/jumbo v0, "Promoted"

    iget-object v1, p0, Lcgi;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 4

    .prologue
    .line 105
    iget-wide v0, p0, Lcgi;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 76
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 80
    :goto_0
    return v0

    .line 77
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 79
    :cond_2
    check-cast p1, Lcgi;

    .line 80
    iget-object v0, p0, Lcgi;->c:Ljava/lang/String;

    iget-object v1, p1, Lcgi;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0}, Lcgi;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcgi;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x26

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public g()[B
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p0, v0}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcgi;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcgi;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "impressionId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcgi;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", disclosureType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcgi;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", promotedTrendId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcgi;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", socialContext: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcgi;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", advertiserName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcgi;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", advertiserId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcgi;->j:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", isPAcInTimeline: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcgi;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", isSuppressMediaForward: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcgi;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
