.class public final Lbip$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lbip;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/twitter/library/client/Session;

.field private c:Z

.field private d:Z

.field private e:[J

.field private f:[J

.field private g:Z

.field private h:Z

.field private i:Lcom/twitter/library/provider/t;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lbip$a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lbip$a;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lbip$a;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lbip$a;->b:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic c(Lbip$a;)Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lbip$a;->c:Z

    return v0
.end method

.method static synthetic d(Lbip$a;)Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lbip$a;->d:Z

    return v0
.end method

.method static synthetic e(Lbip$a;)[J
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lbip$a;->e:[J

    return-object v0
.end method

.method static synthetic f(Lbip$a;)[J
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lbip$a;->f:[J

    return-object v0
.end method

.method static synthetic g(Lbip$a;)Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lbip$a;->g:Z

    return v0
.end method

.method static synthetic h(Lbip$a;)Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lbip$a;->h:Z

    return v0
.end method

.method static synthetic i(Lbip$a;)Lcom/twitter/library/provider/t;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lbip$a;->i:Lcom/twitter/library/provider/t;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lbip$a;
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lbip$a;->a:Landroid/content/Context;

    .line 160
    return-object p0
.end method

.method public a(Lcom/twitter/library/client/Session;)Lbip$a;
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lbip$a;->b:Lcom/twitter/library/client/Session;

    .line 166
    return-object p0
.end method

.method public a(Lcom/twitter/library/provider/t;)Lbip$a;
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lbip$a;->i:Lcom/twitter/library/provider/t;

    .line 208
    return-object p0
.end method

.method public a([J)Lbip$a;
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lbip$a;->e:[J

    .line 202
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lbip$a;->e()Lbip;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lbip;
    .locals 1

    .prologue
    .line 214
    new-instance v0, Lbip;

    invoke-direct {v0, p0}, Lbip;-><init>(Lbip$a;)V

    return-object v0
.end method
