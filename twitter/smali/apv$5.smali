.class Lapv$5;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lapv;->u()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcci;

.field final synthetic c:Lapv;


# direct methods
.method constructor <init>(Lapv;JLcci;)V
    .locals 0

    .prologue
    .line 622
    iput-object p1, p0, Lapv$5;->c:Lapv;

    iput-wide p2, p0, Lapv$5;->a:J

    iput-object p4, p0, Lapv$5;->b:Lcci;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 625
    iget-object v0, p0, Lapv$5;->c:Lapv;

    iget-object v0, v0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 626
    iget-object v0, p0, Lapv$5;->c:Lapv;

    iget-object v1, v0, Lapv;->o:Lcom/twitter/app/dm/f;

    iget-object v0, p0, Lapv$5;->c:Lapv;

    iget-object v0, v0, Lapv;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-interface {v1, v0}, Lcom/twitter/app/dm/f;->b(Lcom/twitter/model/dms/e;)Z

    .line 637
    :goto_0
    return-void

    .line 628
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lapv$5;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread::shared_tweet_dm:click"

    aput-object v3, v1, v2

    .line 629
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 628
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 630
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 631
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    .line 632
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    iget-object v2, p0, Lapv$5;->b:Lcci;

    iget-object v2, v2, Lcci;->d:Lcom/twitter/model/core/r;

    iget-wide v2, v2, Lcom/twitter/model/core/r;->e:J

    .line 633
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 634
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 635
    iget-object v1, p0, Lapv$5;->c:Lapv;

    iget-object v1, v1, Lapv;->g:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lapv$5;->c:Lapv;

    iget-object v3, v3, Lapv;->g:Landroid/content/Context;

    const-class v4, Lcom/twitter/android/TweetActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
