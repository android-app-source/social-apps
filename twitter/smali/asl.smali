.class public Lasl;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lasl$a;,
        Lasl$e;,
        Lasl$c;,
        Lasl$d;,
        Lasl$b;
    }
.end annotation


# static fields
.field private static final a:[I

.field private static final b:[I


# instance fields
.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lasl$c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/text/TextWatcher;

.field private f:Lasl$a;

.field private g:Lasl$e;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    const/4 v0, 0x0

    sput-object v0, Lasl;->a:[I

    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010472

    aput v2, v0, v1

    sput-object v0, Lasl;->b:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lasl;->c:Ljava/util/List;

    .line 29
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lasl;->d:Ljava/util/Set;

    .line 35
    new-instance v0, Lasl$1;

    invoke-direct {v0, p0}, Lasl$1;-><init>(Lasl;)V

    iput-object v0, p0, Lasl;->e:Landroid/text/TextWatcher;

    .line 51
    return-void
.end method

.method private static a(Lasl$c;)I
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lasl$c;->a:Lasl$b;

    invoke-interface {v0}, Lasl$b;->a()Landroid/widget/EditText;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 127
    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 128
    iget-object v2, p0, Lasl$c;->b:Lask;

    invoke-interface {v2, v1, v0}, Lask;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lasl;)Lasl$a;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lasl;->f:Lasl$a;

    return-object v0
.end method

.method private static a(Lasl$c;I)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lasl$c;->a:Lasl$b;

    iget v1, p0, Lasl$c;->c:I

    invoke-interface {v0, p1, v1}, Lasl$b;->a(II)V

    .line 133
    return-void
.end method

.method static synthetic c()[I
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lasl;->a:[I

    return-object v0
.end method

.method static synthetic d()[I
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lasl;->b:[I

    return-object v0
.end method


# virtual methods
.method public a(Lasl$b;Lask;I)Lasl;
    .locals 2
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 82
    new-instance v0, Lasl$c;

    invoke-direct {v0, p1, p2, p3}, Lasl$c;-><init>(Lasl$b;Lask;I)V

    .line 83
    iget-object v1, p0, Lasl;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    invoke-interface {p1}, Lasl$b;->a()Landroid/widget/EditText;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lasl;->d:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    iget-object v1, p0, Lasl;->e:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 87
    new-instance v1, Lasl$2;

    invoke-direct {v1, p0}, Lasl$2;-><init>(Lasl;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 97
    iget-object v1, p0, Lasl;->d:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/ui/widget/TwitterEditText;Lask;I)Lasl;
    .locals 1
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 75
    new-instance v0, Lasl$d;

    invoke-direct {v0, p1}, Lasl$d;-><init>(Lcom/twitter/ui/widget/TwitterEditText;)V

    .line 76
    invoke-virtual {p0, v0, p2, p3}, Lasl;->a(Lasl$b;Lask;I)Lasl;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lasl;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasl$c;

    .line 63
    iget-object v0, v0, Lasl$c;->a:Lasl$b;

    invoke-interface {v0}, Lasl$b;->a()Landroid/widget/EditText;

    move-result-object v0

    .line 64
    iget-object v2, p0, Lasl;->e:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0

    .line 66
    :cond_0
    iget-object v0, p0, Lasl;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 67
    iget-object v0, p0, Lasl;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lasl;->g:Lasl$e;

    .line 69
    return-void
.end method

.method public a(Lasl$a;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lasl;->f:Lasl$a;

    .line 59
    return-void
.end method

.method public a(Lasl$e;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lasl;->g:Lasl$e;

    .line 55
    return-void
.end method

.method public b()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 103
    .line 105
    iget-object v0, p0, Lasl;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasl$c;

    .line 106
    invoke-static {v0}, Lasl;->a(Lasl$c;)I

    move-result v4

    .line 107
    invoke-static {v0, v4}, Lasl;->a(Lasl$c;I)V

    .line 108
    if-ne v4, v2, :cond_2

    .line 109
    const/4 v0, 0x0

    .line 112
    :goto_1
    if-eqz v4, :cond_1

    .line 117
    :goto_2
    iget-object v1, p0, Lasl;->g:Lasl$e;

    if-eqz v1, :cond_0

    .line 118
    iget-object v1, p0, Lasl;->g:Lasl$e;

    invoke-interface {v1, v0}, Lasl$e;->a(Z)V

    .line 121
    :cond_0
    return v0

    :cond_1
    move v1, v0

    .line 115
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
