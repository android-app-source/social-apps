.class public Lbxo;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lbxo;->a:Landroid/content/res/Resources;

    .line 35
    return-void
.end method

.method protected static a()J
    .locals 2

    .prologue
    .line 134
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 135
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;JLandroid/content/res/Resources;Lbxm$a;I)Lbxi;
    .locals 9

    .prologue
    .line 98
    invoke-static {p0}, Lbxo;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    invoke-static {p0}, Lbxd;->i(Lcom/twitter/model/core/Tweet;)Ljava/util/List;

    move-result-object v2

    .line 100
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 101
    invoke-static {p3, p1, p2, v2}, Lbxp;->a(Landroid/content/res/Resources;JLjava/util/List;)Ljava/lang/String;

    move-result-object v8

    .line 103
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [J

    .line 104
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 105
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    iget-wide v4, v0, Lcom/twitter/model/core/q;->c:J

    aput-wide v4, v3, v1

    .line 104
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 107
    :cond_0
    new-instance v1, Lbxm;

    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->C:J

    move-object v2, p4

    move-wide v6, p1

    invoke-direct/range {v1 .. v7}, Lbxm;-><init>(Lbxm$a;[JJJ)V

    .line 109
    new-instance v0, Lbxi;

    invoke-direct {v0, v8, v1, p5}, Lbxi;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;I)V

    .line 112
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/twitter/model/core/Tweet;JLandroid/content/res/Resources;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 65
    invoke-static {p0}, Lbxo;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-static {p0}, Lbxd;->i(Lcom/twitter/model/core/Tweet;)Ljava/util/List;

    move-result-object v0

    .line 67
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 68
    invoke-static {p3, p1, p2, v0}, Lbxp;->a(Landroid/content/res/Resources;JLjava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/r;JLandroid/content/res/Resources;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 85
    invoke-static {p0}, Lbxo;->b(Lcom/twitter/model/core/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    invoke-static {p0}, Lbxd;->c(Lcom/twitter/model/core/r;)Ljava/util/List;

    move-result-object v0

    .line 87
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 88
    invoke-static {p3, p1, p2, v0}, Lbxp;->a(Landroid/content/res/Resources;JLjava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static b(Lcom/twitter/model/core/Tweet;)Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbpj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static b(Lcom/twitter/model/core/r;)Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/twitter/model/core/r;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbpj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 43
    invoke-static {}, Lbxo;->a()J

    move-result-wide v0

    iget-object v2, p0, Lbxo;->a:Landroid/content/res/Resources;

    invoke-static {p1, v0, v1, v2}, Lbxo;->a(Lcom/twitter/model/core/Tweet;JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/r;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 52
    invoke-static {}, Lbxo;->a()J

    move-result-wide v0

    iget-object v2, p0, Lbxo;->a:Landroid/content/res/Resources;

    invoke-static {p1, v0, v1, v2}, Lbxo;->a(Lcom/twitter/model/core/r;JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
