.class public Lbho;
.super Lcom/twitter/library/service/s;
.source "Twttr"


# instance fields
.field public a:Lbic$a;

.field b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/library/service/v;

    invoke-direct {v0, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    new-instance v1, Lbic$a;

    invoke-direct {v1}, Lbic$a;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lbho;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lbic$a;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lbic$a;

    invoke-direct {v0}, Lbic$a;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lbho;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lbic$a;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lbic$a;)V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lbho;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 45
    iput-object p3, p0, Lbho;->a:Lbic$a;

    .line 46
    return-void
.end method


# virtual methods
.method protected a_(Lcom/twitter/library/service/u;)V
    .locals 21

    .prologue
    .line 56
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v14

    .line 58
    move-object/from16 v0, p0

    iget-object v2, v0, Lbho;->a:Lbic$a;

    iget-object v2, v2, Lbic$a;->b:[Ljava/lang/String;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lbho;->a:Lbic$a;

    iget-object v2, v2, Lbic$a;->b:[Ljava/lang/String;

    move-object v4, v2

    .line 60
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lbho;->a:Lbic$a;

    iget-object v2, v2, Lbic$a;->c:[Ljava/lang/String;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lbho;->a:Lbic$a;

    iget-object v2, v2, Lbic$a;->c:[Ljava/lang/String;

    move-object v5, v2

    .line 62
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lbho;->a:Lbic$a;

    iget-object v2, v2, Lbic$a;->a:[J

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lbho;->a:Lbic$a;

    iget-object v2, v2, Lbic$a;->a:[J

    move-object v6, v2

    .line 64
    :goto_2
    array-length v2, v4

    if-gtz v2, :cond_0

    array-length v2, v5

    if-lez v2, :cond_5

    :cond_0
    const/4 v2, 0x1

    move v7, v2

    .line 66
    :goto_3
    array-length v10, v4

    .line 67
    array-length v8, v5

    .line 68
    array-length v3, v6

    .line 69
    move-object/from16 v0, p0

    iget-object v2, v0, Lbho;->a:Lbic$a;

    iget-boolean v13, v2, Lbic$a;->h:Z

    .line 71
    add-int v2, v10, v8

    add-int/2addr v2, v3

    const/16 v9, 0x64

    invoke-static {v2, v9}, Lcom/twitter/library/network/ab;->a(II)I

    move-result v16

    .line 74
    move-object/from16 v0, p0

    iget-object v2, v0, Lbho;->a:Lbic$a;

    const/4 v9, 0x0

    iput v9, v2, Lbic$a;->i:I

    .line 76
    invoke-virtual/range {p0 .. p0}, Lbho;->M()Lcom/twitter/library/service/v;

    move-result-object v17

    .line 77
    const/4 v2, 0x0

    move v12, v2

    :goto_4
    move/from16 v0, v16

    if-ge v12, v0, :cond_7

    .line 78
    const/16 v9, 0x64

    .line 79
    move-object/from16 v0, p0

    iget-object v11, v0, Lbho;->a:Lbic$a;

    if-nez v12, :cond_6

    const-string/jumbo v2, "-1"

    :goto_5
    iput-object v2, v11, Lbic$a;->f:Ljava/lang/String;

    .line 81
    new-instance v2, Lbic;

    move-object/from16 v0, p0

    iget-object v11, v0, Lbho;->p:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-direct {v2, v11, v0}, Lbic;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 82
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lbic;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;

    move-result-object v2

    check-cast v2, Lbic;

    .line 83
    iget-object v11, v2, Lbic;->a:Lbic$a;

    move-object/from16 v0, p0

    iget-object v0, v0, Lbho;->a:Lbic$a;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lbic$a;->a(Lbic$a;)Lbic$a;

    .line 85
    if-lez v3, :cond_b

    .line 86
    array-length v11, v6

    sub-int/2addr v11, v3

    .line 87
    add-int v18, v11, v9

    array-length v0, v6

    move/from16 v19, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(II)I

    move-result v18

    .line 88
    move/from16 v0, v18

    invoke-static {v6, v11, v0}, Ljava/util/Arrays;->copyOfRange([JII)[J

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v13}, Lbic;->a([JZ)Lbic;

    .line 89
    sub-int v19, v18, v11

    sub-int v3, v3, v19

    .line 90
    sub-int v11, v18, v11

    sub-int/2addr v9, v11

    move v11, v3

    .line 93
    :goto_6
    if-lez v9, :cond_a

    if-lez v10, :cond_a

    .line 94
    array-length v3, v4

    sub-int v18, v3, v10

    .line 95
    add-int v3, v18, v9

    array-length v0, v4

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v19

    .line 96
    iget-object v0, v2, Lbic;->a:Lbic$a;

    move-object/from16 v20, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v4, v0, v1}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lbic$a;->a([Ljava/lang/String;)Lbic$a;

    .line 97
    sub-int v3, v19, v18

    sub-int/2addr v10, v3

    .line 98
    sub-int v3, v19, v18

    sub-int v3, v9, v3

    move v9, v10

    .line 101
    :goto_7
    if-lez v3, :cond_9

    if-lez v8, :cond_9

    .line 102
    array-length v10, v5

    sub-int/2addr v10, v8

    .line 103
    add-int/2addr v3, v10

    array-length v0, v5

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v18

    .line 104
    iget-object v0, v2, Lbic;->a:Lbic$a;

    move-object/from16 v19, v0

    .line 105
    move/from16 v0, v18

    invoke-static {v5, v10, v0}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    .line 104
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lbic$a;->b([Ljava/lang/String;)Lbic$a;

    .line 106
    sub-int v3, v18, v10

    sub-int v3, v8, v3

    .line 109
    :goto_8
    invoke-virtual {v2}, Lbic;->O()Lcom/twitter/library/service/u;

    move-result-object v8

    .line 110
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/twitter/library/service/u;->a(Lcom/twitter/library/service/u;)V

    .line 112
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 113
    const-string/jumbo v18, "page"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 114
    const-string/jumbo v18, "pages"

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 116
    invoke-virtual {v8}, Lcom/twitter/library/service/u;->b()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 117
    iget-object v0, v2, Lbic;->a:Lbic$a;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lbic$a;->a()I

    move-result v18

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lbho;->a:Lbic$a;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lbic$a;->i:I

    move/from16 v20, v0

    add-int v20, v20, v18

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lbic$a;->i:I

    .line 119
    const-string/jumbo v19, "inserted_count"

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 120
    const-string/jumbo v18, "result_code"

    invoke-virtual {v8}, Lcom/twitter/library/service/u;->d()I

    move-result v8

    move-object/from16 v0, v18

    invoke-virtual {v10, v0, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 122
    iget-object v2, v2, Lbic;->a:Lbic$a;

    invoke-virtual {v2}, Lbic$a;->b()I

    move-result v2

    .line 123
    const-string/jumbo v8, "num_users"

    invoke-virtual {v10, v8, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 125
    :cond_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lbho;->a(Ljava/lang/Object;)V

    .line 77
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    move v8, v3

    move v10, v9

    move v3, v11

    goto/16 :goto_4

    .line 58
    :cond_2
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    move-object v4, v2

    goto/16 :goto_0

    .line 60
    :cond_3
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    move-object v5, v2

    goto/16 :goto_1

    .line 62
    :cond_4
    const/4 v2, 0x0

    new-array v2, v2, [J

    move-object v6, v2

    goto/16 :goto_2

    .line 64
    :cond_5
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_3

    .line 79
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 127
    :cond_7
    if-eqz v7, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lbho;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 128
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    sub-long v4, v2, v14

    .line 129
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, v17

    iget-wide v6, v0, Lcom/twitter/library/service/v;->c:J

    invoke-direct {v2, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lbho;->b:Ljava/lang/String;

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "import_addressbook::import:done"

    aput-object v7, v3, v6

    .line 130
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 131
    invoke-virtual {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 129
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 132
    new-instance v2, Lcom/twitter/util/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbho;->p:Landroid/content/Context;

    move-object/from16 v0, v17

    iget-wide v4, v0, Lcom/twitter/library/service/v;->c:J

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    invoke-virtual {v2}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    const-string/jumbo v3, "addressbook_import_done"

    const/4 v4, 0x1

    .line 133
    invoke-virtual {v2, v3, v4}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/util/a$a;->apply()V

    .line 135
    :cond_8
    return-void

    :cond_9
    move v3, v8

    goto/16 :goto_8

    :cond_a
    move v3, v9

    move v9, v10

    goto/16 :goto_7

    :cond_b
    move v11, v3

    goto/16 :goto_6
.end method
