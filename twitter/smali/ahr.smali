.class public Lahr;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Laij;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lagr;


# direct methods
.method public constructor <init>(Lagr;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lahr;->a:Ljava/util/Map;

    .line 30
    iput-object p1, p0, Lahr;->b:Lagr;

    .line 31
    return-void
.end method

.method static synthetic a(Lahr;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lahr;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Laij;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lahr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 47
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laij;

    .line 48
    iget-object v2, p0, Lahr;->a:Ljava/util/Map;

    invoke-virtual {v0}, Laij;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 50
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/q;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<",
            "Ljava/util/List",
            "<",
            "Laij;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lahr;->b:Lagr;

    new-instance v1, Lahr$1;

    invoke-direct {v1, p0, p1}, Lahr$1;-><init>(Lahr;Lcom/twitter/util/q;)V

    invoke-virtual {v0, v1}, Lagr;->a(Lcom/twitter/util/q;)V

    .line 43
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lahr;->a:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Laij;
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lahr;->a:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laij;

    return-object v0
.end method
