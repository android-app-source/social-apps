.class public abstract Laum;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        "R:",
        "Lcom/twitter/library/service/s;",
        ">",
        "Ljava/lang/Object;",
        "Lauj",
        "<TA;TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;)Lcom/twitter/library/service/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)TR;"
        }
    .end annotation
.end method

.method protected abstract a(Lcom/twitter/library/service/s;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)TT;"
        }
    .end annotation
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public a_(Ljava/lang/Object;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 65
    new-instance v0, Laum$2;

    invoke-direct {v0, p0, p1}, Laum$2;-><init>(Laum;Ljava/lang/Object;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    .line 86
    invoke-virtual {p0}, Laum;->c()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 87
    invoke-virtual {p0}, Laum;->d()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Laum$1;

    invoke-direct {v1, p0}, Laum$1;-><init>(Laum;)V

    .line 88
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 94
    invoke-virtual {p0}, Laum;->b()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 65
    return-object v0
.end method

.method protected b(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)",
            "Lcom/twitter/library/service/u;"
        }
    .end annotation

    .prologue
    .line 99
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->O()Lcom/twitter/library/service/u;

    move-result-object v0

    return-object v0
.end method

.method protected b()Lrx/f;
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v0

    return-object v0
.end method

.method protected c()Lrx/f;
    .locals 2

    .prologue
    .line 43
    invoke-static {}, Lcom/twitter/async/service/c;->a()Lcom/twitter/async/service/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->a:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {v0, v1}, Lcom/twitter/async/service/c;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, Lcws;->a(Ljava/util/concurrent/Executor;)Lrx/f;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 0

    .prologue
    .line 104
    return-void
.end method

.method protected d()Lrx/f;
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v0

    return-object v0
.end method
