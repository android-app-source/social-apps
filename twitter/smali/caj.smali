.class public Lcaj;
.super Lcac;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcaj$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TweetType:",
        "Ljava/lang/Object;",
        ">",
        "Lcac;"
    }
.end annotation


# instance fields
.field public final d:Lcag;

.field public final e:Lcag;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Lcan;

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TTweetType;>;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/lang/String;

.field public final n:Lcae;

.field public final o:Ljava/lang/String;

.field public final p:Ljava/lang/String;

.field public final q:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcaj$a;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcaj$a",
            "<TTweetType;>;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-static {p1}, Lcaj$a;->a(Lcaj$a;)J

    move-result-wide v2

    invoke-static {p1}, Lcaj$a;->b(Lcaj$a;)J

    move-result-wide v4

    invoke-static {p1}, Lcaj$a;->c(Lcaj$a;)J

    move-result-wide v6

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcac;-><init>(JJJ)V

    .line 51
    invoke-static {p1}, Lcaj$a;->d(Lcaj$a;)Lcag;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcag;

    iput-object v0, p0, Lcaj;->d:Lcag;

    .line 52
    invoke-static {p1}, Lcaj$a;->e(Lcaj$a;)Lcag;

    move-result-object v0

    iput-object v0, p0, Lcaj;->e:Lcag;

    .line 53
    invoke-static {p1}, Lcaj$a;->f(Lcaj$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcaj;->f:Ljava/lang/String;

    .line 54
    invoke-static {p1}, Lcaj$a;->g(Lcaj$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcaj;->g:Ljava/lang/String;

    .line 55
    invoke-static {p1}, Lcaj$a;->h(Lcaj$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcaj;->j:Ljava/util/List;

    .line 56
    invoke-static {p1}, Lcaj$a;->i(Lcaj$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcaj;->k:Ljava/util/List;

    .line 57
    invoke-static {p1}, Lcaj$a;->j(Lcaj$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcaj;->l:Ljava/util/List;

    .line 58
    invoke-static {p1}, Lcaj$a;->k(Lcaj$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcaj;->m:Ljava/lang/String;

    .line 59
    invoke-static {p1}, Lcaj$a;->l(Lcaj$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcaj;->h:Ljava/lang/String;

    .line 60
    invoke-static {p1}, Lcaj$a;->m(Lcaj$a;)Lcan;

    move-result-object v0

    iput-object v0, p0, Lcaj;->i:Lcan;

    .line 61
    invoke-static {p1}, Lcaj$a;->n(Lcaj$a;)Lcae;

    move-result-object v0

    iput-object v0, p0, Lcaj;->n:Lcae;

    .line 62
    invoke-static {p1}, Lcaj$a;->o(Lcaj$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcaj;->o:Ljava/lang/String;

    .line 63
    invoke-static {p1}, Lcaj$a;->p(Lcaj$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcaj;->p:Ljava/lang/String;

    .line 64
    invoke-static {p1}, Lcaj$a;->q(Lcaj$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcaj;->q:Ljava/lang/String;

    .line 65
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcaj;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    invoke-virtual {p0}, Lcaj;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 80
    iget-wide v0, p0, Lcaj;->b:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lcaj;->b:J

    return-wide v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcaj;->c:J

    return-wide v0
.end method
