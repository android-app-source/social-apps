.class public final Lceo$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lceo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lceo;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/twitter/model/moments/MomentPageType;

.field c:Lcom/twitter/model/moments/n;

.field d:Lcom/twitter/model/moments/e;

.field e:Lcom/twitter/model/moments/w;

.field f:Lcem;

.field g:Lcom/twitter/model/moments/l;

.field h:Lcom/twitter/model/moments/o;

.field i:Lcom/twitter/model/moments/s;

.field j:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 94
    const-string/jumbo v0, ""

    iput-object v0, p0, Lceo$a;->a:Ljava/lang/String;

    .line 95
    sget-object v0, Lcom/twitter/model/moments/MomentPageType;->a:Lcom/twitter/model/moments/MomentPageType;

    iput-object v0, p0, Lceo$a;->b:Lcom/twitter/model/moments/MomentPageType;

    .line 106
    return-void
.end method


# virtual methods
.method public a(J)Lceo$a;
    .locals 1

    .prologue
    .line 158
    iput-wide p1, p0, Lceo$a;->j:J

    .line 159
    return-object p0
.end method

.method public a(Lcem;)Lceo$a;
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lceo$a;->f:Lcem;

    .line 153
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/MomentPageType;)Lceo$a;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lceo$a;->b:Lcom/twitter/model/moments/MomentPageType;

    invoke-static {p1, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/MomentPageType;

    iput-object v0, p0, Lceo$a;->b:Lcom/twitter/model/moments/MomentPageType;

    .line 129
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/e;)Lceo$a;
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lceo$a;->d:Lcom/twitter/model/moments/e;

    .line 141
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/l;)Lceo$a;
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lceo$a;->g:Lcom/twitter/model/moments/l;

    .line 165
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/n;)Lceo$a;
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lceo$a;->c:Lcom/twitter/model/moments/n;

    .line 135
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/o;)Lceo$a;
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lceo$a;->h:Lcom/twitter/model/moments/o;

    .line 171
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/s;)Lceo$a;
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lceo$a;->i:Lcom/twitter/model/moments/s;

    .line 177
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/w;)Lceo$a;
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lceo$a;->e:Lcom/twitter/model/moments/w;

    .line 147
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lceo$a;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lceo$a;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lceo$a;->a:Ljava/lang/String;

    .line 123
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lceo$a;->e()Lceo;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lceo;
    .locals 2

    .prologue
    .line 183
    new-instance v0, Lceo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lceo;-><init>(Lceo$a;Lceo$1;)V

    return-object v0
.end method
