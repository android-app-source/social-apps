.class Lagh$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lagh;->c()Lrx/functions/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Lail;",
        "Lcom/twitter/library/client/m;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lagh;


# direct methods
.method constructor <init>(Lagh;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lagh$1;->a:Lagh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lail;)Lcom/twitter/library/client/m;
    .locals 7

    .prologue
    .line 88
    new-instance v0, Laik;

    iget-object v1, p0, Lagh$1;->a:Lagh;

    .line 90
    invoke-static {v1}, Lagh;->a(Lagh;)Lage;

    move-result-object v1

    invoke-virtual {v1}, Lage;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lagh$1;->a:Lagh;

    .line 91
    invoke-static {v2}, Lagh;->a(Lagh;)Lage;

    move-result-object v2

    invoke-virtual {v2}, Lage;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    iget-object v3, p0, Lagh$1;->a:Lagh;

    .line 94
    invoke-static {v3}, Lagh;->b(Lagh;)Laii;

    move-result-object v3

    invoke-virtual {v3}, Laii;->a()Z

    move-result v5

    iget-object v3, p0, Lagh$1;->a:Lagh;

    .line 95
    invoke-static {v3}, Lagh;->b(Lagh;)Laii;

    move-result-object v3

    invoke-virtual {v3}, Laii;->b()Z

    move-result v6

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Laik;-><init>(Ljava/lang/String;Ljava/lang/String;Lail;ZZZ)V

    invoke-virtual {v0}, Laik;->hashCode()I

    move-result v1

    .line 96
    iget-object v0, p0, Lagh$1;->a:Lagh;

    .line 97
    invoke-static {v0}, Lagh;->a(Lagh;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->j()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/bs$a;->a(Landroid/content/Intent;)Lcom/twitter/android/bs$a;

    move-result-object v0

    const v2, 0x7f0a07db

    .line 98
    invoke-virtual {v0, v2}, Lcom/twitter/android/bs$a;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    const v2, 0x7f0a07dc

    .line 99
    invoke-virtual {v0, v2}, Lcom/twitter/android/bs$a;->c(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    const/4 v2, 0x1

    .line 100
    invoke-virtual {v0, v2}, Lcom/twitter/android/bs$a;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    iget-object v2, p0, Lagh$1;->a:Lagh;

    .line 101
    invoke-static {v2}, Lagh;->b(Lagh;)Laii;

    move-result-object v2

    invoke-virtual {v2}, Laii;->a()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/bs$a;->b(Z)Lcom/twitter/android/bs$a;

    move-result-object v0

    iget-object v2, p0, Lagh$1;->a:Lagh;

    .line 102
    invoke-static {v2}, Lagh;->b(Lagh;)Laii;

    move-result-object v2

    invoke-virtual {v2}, Laii;->b()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/bs$a;->c(Z)Lcom/twitter/android/bs$a;

    move-result-object v0

    iget v2, p1, Lail;->a:I

    .line 103
    invoke-virtual {v0, v2}, Lcom/twitter/android/bs$a;->a(I)Lcom/twitter/android/bs$a;

    move-result-object v0

    iget-boolean v2, p1, Lail;->b:Z

    .line 104
    invoke-virtual {v0, v2}, Lcom/twitter/android/bs$a;->a(Z)Lcom/twitter/android/bs$a;

    move-result-object v0

    .line 106
    sget-object v2, Lcom/twitter/util/y;->a:Ljava/security/SecureRandom;

    invoke-virtual {v2}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v2

    .line 107
    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/bs$a;->a(J)Lcom/twitter/android/bs$a;

    .line 109
    iget-object v2, p0, Lagh$1;->a:Lagh;

    invoke-static {}, Lcom/twitter/android/search/d;->d()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v0}, Lcom/twitter/android/bs$a;->a()Lcom/twitter/android/bs;

    move-result-object v0

    iget-object v4, p0, Lagh$1;->a:Lagh;

    iget v5, p1, Lail;->a:I

    .line 110
    invoke-static {v4, v5}, Lagh;->a(Lagh;I)I

    move-result v4

    .line 109
    invoke-static {v2, v3, v0, v4, v1}, Lagh;->a(Lagh;Ljava/lang/Class;Lcom/twitter/app/common/base/b;II)Lcom/twitter/library/client/m;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 84
    check-cast p1, Lail;

    invoke-virtual {p0, p1}, Lagh$1;->a(Lail;)Lcom/twitter/library/client/m;

    move-result-object v0

    return-object v0
.end method
