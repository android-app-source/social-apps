.class public Lcfh;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcfh;


# instance fields
.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lceo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lcfh;

    .line 23
    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v1

    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcfh;-><init>(Ljava/util/Map;Ljava/util/List;)V

    sput-object v0, Lcfh;->a:Lcfh;

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lceo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcfh;->b:Ljava/util/Map;

    .line 30
    iput-object p2, p0, Lcfh;->c:Ljava/util/List;

    .line 31
    return-void
.end method

.method private static a(Ljava/util/Map;)Lcqw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;)",
            "Lcqw",
            "<-",
            "Ljava/util/List",
            "<",
            "Lceo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lcfh$1;

    invoke-direct {v0, p0}, Lcfh$1;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method private static b()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<-",
            "Lcwc",
            "<",
            "Ljava/lang/Long;",
            "Lceo;",
            ">;+",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lceo;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Lcfh$2;

    invoke-direct {v0}, Lcfh$2;-><init>()V

    return-object v0
.end method

.method private static c()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<-",
            "Lceo;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Lcfh$3;

    invoke-direct {v0}, Lcfh$3;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcfh;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/MutableMap;->a(I)Ljava/util/Map;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcfh;->c:Ljava/util/List;

    invoke-static {v1}, Lrx/c;->a(Ljava/lang/Iterable;)Lrx/c;

    move-result-object v1

    .line 37
    invoke-static {}, Lcfh;->c()Lrx/functions/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/c;->g(Lrx/functions/d;)Lrx/c;

    move-result-object v1

    .line 38
    invoke-static {}, Lcfh;->b()Lrx/functions/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v1

    .line 39
    invoke-static {v0}, Lcfh;->a(Ljava/util/Map;)Lcqw;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 40
    return-object v0
.end method
