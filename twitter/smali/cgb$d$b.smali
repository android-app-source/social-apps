.class final Lcgb$d$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcgb$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcgb$d;",
        "Lcgb$d$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcgb$1;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcgb$d$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcgb$d$a;
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcgb$d$a;

    invoke-direct {v0}, Lcgb$d$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcgb$d$a;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 131
    sget-object v0, Lcgb$e;->a:Lcgb$e$b;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb$e;

    invoke-virtual {p2, v0}, Lcgb$d$a;->a(Lcgb$e;)Lcgb$d$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcgb$b;->a:Lcgb$b$b;

    .line 132
    invoke-static {v0, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v0}, Lcgb$d$a;->a(Ljava/util/Map;)Lcgb$d$a;

    move-result-object v1

    sget-object v0, Lcgb$c;->a:Lcgb$c$b;

    .line 135
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 134
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcgb$d$a;->a(Ljava/util/List;)Lcgb$d$a;

    .line 136
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 110
    check-cast p2, Lcgb$d$a;

    invoke-virtual {p0, p1, p2, p3}, Lcgb$d$b;->a(Lcom/twitter/util/serialization/n;Lcgb$d$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcgb$d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p2, Lcgb$d;->b:Lcgb$e;

    sget-object v1, Lcgb$e;->a:Lcgb$e$b;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 122
    iget-object v0, p2, Lcgb$d;->c:Ljava/util/Map;

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcgb$b;->a:Lcgb$b$b;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Map;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)V

    .line 124
    iget-object v0, p2, Lcgb$d;->d:Ljava/util/List;

    sget-object v1, Lcgb$c;->a:Lcgb$c$b;

    .line 125
    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 126
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    check-cast p2, Lcgb$d;

    invoke-virtual {p0, p1, p2}, Lcgb$d$b;->a(Lcom/twitter/util/serialization/o;Lcgb$d;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0}, Lcgb$d$b;->a()Lcgb$d$a;

    move-result-object v0

    return-object v0
.end method
