.class public Lchd;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcgz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcgz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lchd;->a:Ljava/util/List;

    .line 24
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 31
    const/4 v0, 0x0

    .line 32
    iget-object v1, p0, Lchd;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgz;

    .line 33
    instance-of v3, v0, Lcgr$a;

    if-eqz v3, :cond_0

    .line 34
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgr$a;

    .line 35
    iget v0, v0, Lcgr$a;->a:I

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 40
    goto :goto_0

    .line 36
    :cond_0
    instance-of v3, v0, Lcgs$a;

    if-eqz v3, :cond_2

    .line 37
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgs$a;

    .line 38
    iget v0, v0, Lcgs$a;->a:I

    add-int/2addr v0, v1

    goto :goto_1

    .line 41
    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public b()Lcom/twitter/model/timeline/u;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lchd;->a:Ljava/util/List;

    new-instance v1, Lchd$1;

    invoke-direct {v1, p0}, Lchd$1;-><init>(Lchd;)V

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    .line 56
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    .line 55
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lche$a;

    .line 57
    if-eqz v0, :cond_0

    .line 58
    iget-object v0, v0, Lche$a;->a:Lcom/twitter/model/timeline/u;

    .line 60
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
