.class public Lbvd;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbvd$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/util/collection/Pair",
        "<TR;",
        "Lcom/twitter/library/service/u;",
        ">;R:",
        "Lcom/twitter/library/service/s;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Laum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laum",
            "<",
            "Lbvd$a;",
            "TT;TR;>;"
        }
    .end annotation
.end field

.field private final b:Lrx/f;

.field private final c:Lbvo;


# direct methods
.method public constructor <init>(Laum;Lrx/f;Lbvo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laum",
            "<",
            "Lbvd$a;",
            "TT;TR;>;",
            "Lrx/f;",
            "Lbvo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lbvd;->a:Laum;

    .line 31
    iput-object p2, p0, Lbvd;->b:Lrx/f;

    .line 32
    iput-object p3, p0, Lbvd;->c:Lbvo;

    .line 33
    return-void
.end method

.method static synthetic a(Lbvd;)Lbvo;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lbvd;->c:Lbvo;

    return-object v0
.end method


# virtual methods
.method public a(Lbvd$a;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbvd$a;",
            ")",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lbvd;->a:Laum;

    invoke-virtual {v0, p1}, Laum;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lbvd;->b:Lrx/f;

    .line 43
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lbvd$1;

    invoke-direct {v1, p0, p1}, Lbvd$1;-><init>(Lbvd;Lbvd$a;)V

    .line 44
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lrx/c;->b()Lrx/g;

    move-result-object v0

    .line 42
    return-object v0
.end method
