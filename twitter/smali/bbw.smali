.class public abstract Lbbw;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbbw$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<TT;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field protected final a:I

.field protected final b:I

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lbmn;


# direct methods
.method protected constructor <init>(Lbbw$a;)V
    .locals 3

    .prologue
    .line 42
    invoke-static {p1}, Lbbw$a;->a(Lbbw$a;)Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbbw;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lbbw$a;->b(Lbbw$a;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 43
    invoke-static {p1}, Lbbw$a;->c(Lbbw$a;)I

    move-result v0

    iput v0, p0, Lbbw;->a:I

    .line 44
    invoke-static {p1}, Lbbw$a;->d(Lbbw$a;)I

    move-result v0

    iput v0, p0, Lbbw;->b:I

    .line 45
    invoke-static {p1}, Lbbw$a;->e(Lbbw$a;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbbw;->g:Ljava/util/Map;

    .line 46
    invoke-static {p1}, Lbbw$a;->f(Lbbw$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbbw;->c:Ljava/util/List;

    .line 47
    invoke-static {p1}, Lbbw$a;->b(Lbbw$a;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lbmn;->a(J)Lbmn;

    move-result-object v0

    iput-object v0, p0, Lbbw;->h:Lbmn;

    .line 48
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 56
    invoke-virtual {p0}, Lbbw;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 57
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "contacts"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 58
    invoke-virtual {p0}, Lbbw;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    iget-object v1, p0, Lbbw;->c:Ljava/util/List;

    .line 59
    invoke-static {v1}, Lbbx;->a(Ljava/util/List;)Lcom/twitter/network/apache/entity/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/apache/e;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 60
    invoke-virtual {p0, v0}, Lbbw;->a(Lcom/twitter/library/service/d$a;)V

    .line 61
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Lcom/twitter/library/service/d$a;)V
.end method

.method protected a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/json/contacts/JsonContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v2

    .line 71
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/contacts/JsonContact;

    .line 73
    iget-object v1, p0, Lbbw;->c:Ljava/util/List;

    iget v4, v0, Lcom/twitter/model/json/contacts/JsonContact;->b:I

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 74
    iget-object v4, p0, Lbbw;->g:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-wide v4, v0, Lcom/twitter/model/json/contacts/JsonContact;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    goto :goto_1

    .line 76
    :cond_1
    iget-object v1, p0, Lbbw;->h:Lbmn;

    invoke-virtual {v2}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v0}, Lbmn;->a(Ljava/util/Map;)V

    goto :goto_0
.end method

.method protected abstract b()Ljava/lang/String;
.end method
