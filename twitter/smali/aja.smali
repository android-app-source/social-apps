.class public Laja;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Laiz;)Lapb;
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 19
    iget v5, p0, Laiz;->a:I

    .line 20
    iget-wide v6, p0, Laiz;->b:J

    .line 21
    iget-wide v2, p0, Laiz;->c:J

    .line 22
    iget-boolean v8, p0, Laiz;->d:Z

    .line 23
    iget-object v9, p0, Laiz;->f:Ljava/lang/String;

    .line 28
    packed-switch v5, :pswitch_data_0

    .line 144
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid status type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :pswitch_1
    sget-object v0, Lcom/twitter/database/schema/a$x;->c:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 31
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 32
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 150
    :goto_0
    if-eqz v9, :cond_0

    .line 151
    const-string/jumbo v0, "timeline_timeline_tag=?"

    .line 152
    new-array v1, v10, [Ljava/lang/String;

    aput-object v9, v1, v11

    .line 172
    :goto_1
    new-instance v5, Lapb$a;

    invoke-direct {v5}, Lapb$a;-><init>()V

    .line 173
    invoke-static {v4, v6, v7}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v5, v4}, Lapb$a;->a(Landroid/net/Uri;)Lapb$a;

    move-result-object v4

    .line 174
    invoke-virtual {v4, v3}, Lapb$a;->b([Ljava/lang/String;)Lapb$a;

    move-result-object v3

    .line 175
    invoke-virtual {v3, v0}, Lapb$a;->a(Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    .line 176
    invoke-virtual {v0, v1}, Lapb$a;->a([Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    .line 177
    invoke-virtual {v0, v2}, Lapb$a;->b(Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    .line 178
    invoke-virtual {v0}, Lapb$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapb;

    .line 172
    return-object v0

    .line 36
    :pswitch_2
    sget-object v0, Lcom/twitter/database/schema/a$x;->b:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 37
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 38
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 39
    goto :goto_0

    .line 42
    :pswitch_3
    sget-object v0, Lcom/twitter/database/schema/a$x;->d:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 43
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 44
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 45
    goto :goto_0

    .line 48
    :pswitch_4
    sget-object v0, Lcom/twitter/database/schema/a$x;->e:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 49
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 50
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 51
    goto :goto_0

    .line 54
    :pswitch_5
    sget-object v0, Lcom/twitter/database/schema/a$x;->l:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 55
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 56
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 57
    goto :goto_0

    .line 60
    :pswitch_6
    sget-object v0, Lcom/twitter/database/schema/a$x;->r:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 61
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 62
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 63
    goto :goto_0

    .line 66
    :pswitch_7
    sget-object v0, Lcom/twitter/database/schema/a$x;->s:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 67
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 68
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 69
    goto/16 :goto_0

    .line 72
    :pswitch_8
    sget-object v0, Lcom/twitter/database/schema/a$x;->t:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 73
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 74
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 75
    goto/16 :goto_0

    .line 78
    :pswitch_9
    sget-object v0, Lcom/twitter/database/schema/a$x;->q:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 79
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 80
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 81
    goto/16 :goto_0

    .line 84
    :pswitch_a
    sget-object v0, Lcom/twitter/database/schema/a$x;->k:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 85
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 86
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 87
    goto/16 :goto_0

    .line 90
    :pswitch_b
    sget-object v0, Lcom/twitter/database/schema/a$x;->f:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 91
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 92
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 93
    goto/16 :goto_0

    .line 96
    :pswitch_c
    sget-object v0, Lcom/twitter/database/schema/a$x;->g:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 97
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 98
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 99
    goto/16 :goto_0

    .line 102
    :pswitch_d
    sget-object v0, Lcom/twitter/database/schema/a$x;->i:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 103
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 104
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 105
    goto/16 :goto_0

    .line 108
    :pswitch_e
    sget-object v0, Lcom/twitter/database/schema/a$x;->j:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 109
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 110
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 111
    goto/16 :goto_0

    .line 114
    :pswitch_f
    sget-object v0, Lcom/twitter/database/schema/a$x;->h:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 115
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 116
    const-string/jumbo v0, "timeline_sort_index DESC, status_groups_preview_draft_id ASC, timeline_updated_at ASC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 117
    goto/16 :goto_0

    .line 120
    :pswitch_10
    sget-object v0, Lcom/twitter/database/schema/a$x;->m:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 121
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 122
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index ASC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 123
    goto/16 :goto_0

    .line 126
    :pswitch_11
    sget-object v0, Lcom/twitter/database/schema/a$x;->n:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 127
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 128
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 129
    goto/16 :goto_0

    .line 132
    :pswitch_12
    sget-object v0, Lcom/twitter/database/schema/a$x;->o:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 133
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 134
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 135
    goto/16 :goto_0

    .line 138
    :pswitch_13
    sget-object v0, Lcom/twitter/database/schema/a$x;->p:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 139
    sget-object v2, Lbue;->a:[Ljava/lang/String;

    .line 140
    const-string/jumbo v0, "status_groups_preview_draft_id DESC, timeline_sort_index DESC, timeline_updated_at DESC, _id ASC"

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 141
    goto/16 :goto_0

    .line 153
    :cond_0
    if-eq v5, v10, :cond_1

    const/16 v0, 0x12

    if-ne v5, v0, :cond_3

    .line 154
    :cond_1
    const-string/jumbo v0, "p_format IS NULL OR p_format=?"

    .line 155
    if-eqz v8, :cond_2

    .line 156
    new-array v1, v10, [Ljava/lang/String;

    const-string/jumbo v5, "profile_self"

    aput-object v5, v1, v11

    goto/16 :goto_1

    .line 158
    :cond_2
    new-array v1, v10, [Ljava/lang/String;

    const-string/jumbo v5, "profile_other"

    aput-object v5, v1, v11

    goto/16 :goto_1

    .line 160
    :cond_3
    if-eqz v5, :cond_4

    const/16 v0, 0x11

    if-ne v5, v0, :cond_5

    .line 161
    :cond_4
    const-string/jumbo v0, "p_format IS NULL OR p_format=?"

    .line 162
    new-array v1, v10, [Ljava/lang/String;

    const-string/jumbo v5, "home_timeline"

    aput-object v5, v1, v11

    goto/16 :goto_1

    .line 163
    :cond_5
    const/4 v0, 0x2

    if-ne v5, v0, :cond_7

    .line 164
    iget-boolean v0, p0, Laiz;->e:Z

    if-eqz v0, :cond_6

    const-string/jumbo v0, "statuses_flags&50689!= 0"

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v0, "statuses_flags&50689!= 0 AND statuses_flags&64 = 0"

    goto/16 :goto_1

    :cond_7
    move-object v0, v1

    .line 169
    goto/16 :goto_1

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_9
        :pswitch_d
        :pswitch_0
        :pswitch_c
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_e
        :pswitch_f
        :pswitch_a
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method
