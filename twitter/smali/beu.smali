.class public Lbeu;
.super Lcom/twitter/library/service/s;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/client/Session;

.field private final b:Lcom/twitter/library/provider/t;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/t;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbeu;->c:Ljava/util/List;

    .line 35
    iput-object p3, p0, Lbeu;->a:Lcom/twitter/library/client/Session;

    .line 36
    iput-object p4, p0, Lbeu;->b:Lcom/twitter/library/provider/t;

    .line 37
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lbeu;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected a_(Lcom/twitter/library/service/u;)V
    .locals 8

    .prologue
    .line 41
    const-string/jumbo v1, "-1"

    .line 42
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    const/16 v0, 0xa

    if-ge v2, v0, :cond_0

    .line 43
    new-instance v0, Lbet;

    iget-object v3, p0, Lbeu;->p:Landroid/content/Context;

    .line 44
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lbeu;->a:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v3, v4, v5, v1}, Lbet;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 45
    invoke-virtual {v0, p0}, Lbet;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbet;

    .line 46
    invoke-virtual {v0}, Lbet;->O()Lcom/twitter/library/service/u;

    move-result-object v1

    .line 47
    invoke-virtual {p1, v1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/library/service/u;)V

    .line 49
    invoke-virtual {v1}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 75
    :cond_0
    return-void

    .line 53
    :cond_1
    invoke-virtual {v0}, Lbet;->g()Ljava/util/List;

    move-result-object v3

    .line 54
    iget-object v1, p0, Lbeu;->c:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 59
    iget-object v1, p0, Lbeu;->b:Lcom/twitter/library/provider/t;

    invoke-static {v3}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/twitter/library/provider/t;->a([J)Ljava/util/List;

    move-result-object v1

    .line 60
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ne v1, v4, :cond_2

    .line 61
    const-string/jumbo v0, "0"

    move-object v1, v0

    .line 67
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 68
    iget-object v0, p0, Lbeu;->b:Lcom/twitter/library/provider/t;

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/twitter/library/provider/t;->a(JILaut;)V

    goto :goto_2

    .line 63
    :cond_2
    invoke-virtual {v0}, Lbet;->h()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 71
    :cond_3
    const-string/jumbo v0, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lbeu;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    return-wide v0
.end method
