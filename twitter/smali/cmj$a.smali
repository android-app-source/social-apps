.class Lcmj$a;
.super Lcok$d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcmj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field a:Z

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z

.field i:Z


# direct methods
.method constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 86
    invoke-direct {p0}, Lcok$d;-><init>()V

    .line 87
    const-string/jumbo v0, "tweet_header_badging_android_4828"

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "verified_only"

    aput-object v2, v1, v3

    const-string/jumbo v2, "verified_and_protected"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcmj$a;->a:Z

    .line 90
    const-string/jumbo v0, "tweet_header_badging_android_4828"

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "protected_only"

    aput-object v2, v1, v3

    const-string/jumbo v2, "verified_and_protected"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcmj$a;->b:Z

    .line 93
    iget-boolean v0, p0, Lcmj$a;->c:Z

    if-nez v0, :cond_0

    .line 95
    const-string/jumbo v0, "reply_icon_change_4851"

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "fill_reply_icon"

    aput-object v2, v1, v3

    const-string/jumbo v2, "fill_reply_icon_no_dms"

    aput-object v2, v1, v4

    const-string/jumbo v2, "fill_reply_icon_last_no_dms"

    aput-object v2, v1, v5

    const-string/jumbo v2, "fill_reply_icon_middle_no_dms"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcmj$a;->d:Z

    .line 99
    const-string/jumbo v0, "reply_icon_change_4851"

    const-string/jumbo v1, "fill_reply_icon_last_no_dms"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcmj$a;->e:Z

    .line 102
    const-string/jumbo v0, "reply_icon_change_4851"

    const-string/jumbo v1, "fill_reply_icon_middle_no_dms"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcmj$a;->f:Z

    .line 105
    const-string/jumbo v0, "reply_icon_change_4851"

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "fill_no_dms"

    aput-object v2, v1, v3

    const-string/jumbo v2, "fill_reply_icon_no_dms"

    aput-object v2, v1, v4

    const-string/jumbo v2, "fill_reply_icon_last_no_dms"

    aput-object v2, v1, v5

    const-string/jumbo v2, "fill_reply_icon_middle_no_dms"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcmj$a;->g:Z

    .line 108
    iput-boolean v4, p0, Lcmj$a;->c:Z

    .line 111
    :cond_0
    const-string/jumbo v0, "cards_kernel_show_sensitivity_warning"

    invoke-static {v0, v3}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcmj$a;->h:Z

    .line 113
    const-string/jumbo v0, "android_tweet_anatomy_rounded_media_view_corners"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcmj$a;->i:Z

    .line 115
    return-void
.end method
