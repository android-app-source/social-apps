.class public Lcgb$c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcgb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcgb$c$b;,
        Lcgb$c$a;
    }
.end annotation


# static fields
.field public static final a:Lcgb$c$b;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 227
    new-instance v0, Lcgb$c$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcgb$c$b;-><init>(Lcgb$1;)V

    sput-object v0, Lcgb$c;->a:Lcgb$c$b;

    return-void
.end method

.method private constructor <init>(Lcgb$c$a;)V
    .locals 1

    .prologue
    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 252
    invoke-static {p1}, Lcgb$c$a;->a(Lcgb$c$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgb$c;->b:Ljava/lang/String;

    .line 253
    invoke-static {p1}, Lcgb$c$a;->b(Lcgb$c$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgb$c;->c:Ljava/lang/String;

    .line 254
    invoke-static {p1}, Lcgb$c$a;->c(Lcgb$c$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgb$c;->d:Ljava/lang/String;

    .line 255
    invoke-static {p1}, Lcgb$c$a;->d(Lcgb$c$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgb$c;->e:Ljava/lang/String;

    .line 256
    invoke-static {p1}, Lcgb$c$a;->e(Lcgb$c$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgb$c;->f:Ljava/lang/String;

    .line 257
    invoke-static {p1}, Lcgb$c$a;->f(Lcgb$c$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgb$c;->g:Ljava/lang/String;

    .line 258
    invoke-static {p1}, Lcgb$c$a;->g(Lcgb$c$a;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcgb$c;->h:Ljava/util/Map;

    .line 259
    return-void
.end method

.method synthetic constructor <init>(Lcgb$c$a;Lcgb$1;)V
    .locals 0

    .prologue
    .line 226
    invoke-direct {p0, p1}, Lcgb$c;-><init>(Lcgb$c$a;)V

    return-void
.end method
