.class public Lbcc;
.super Lbbw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbcc$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbbw",
        "<",
        "Lcom/twitter/model/json/contacts/JsonUploadAndMatchContactsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Z


# direct methods
.method private constructor <init>(Lbcc$a;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lbbw;-><init>(Lbbw$a;)V

    .line 30
    invoke-static {p1}, Lbcc$a;->a(Lbcc$a;)Z

    move-result v0

    iput-boolean v0, p0, Lbcc;->c:Z

    .line 31
    return-void
.end method

.method synthetic constructor <init>(Lbcc$a;Lbcc$1;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lbcc;-><init>(Lbcc$a;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/d$a;)V
    .locals 2

    .prologue
    .line 47
    const-string/jumbo v0, "include_relationships"

    iget-boolean v1, p0, Lbcc;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 48
    return-void
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/json/contacts/JsonUploadAndMatchContactsResponse;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-super/range {p0 .. p3}, Lbbw;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    .line 54
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v12, p0, Lbcc;->o:Landroid/os/Bundle;

    .line 56
    const-string/jumbo v0, "page"

    iget v1, p0, Lbcc;->a:I

    invoke-virtual {v12, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 57
    const-string/jumbo v0, "pages"

    iget v1, p0, Lbcc;->b:I

    invoke-virtual {v12, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 58
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/twitter/model/json/contacts/JsonUploadAndMatchContactsResponse;

    .line 59
    if-eqz v11, :cond_0

    .line 60
    invoke-virtual {v11}, Lcom/twitter/model/json/contacts/JsonUploadAndMatchContactsResponse;->a()Ljava/util/List;

    move-result-object v1

    .line 61
    invoke-virtual {p0}, Lbcc;->S()Laut;

    move-result-object v10

    .line 62
    invoke-virtual {p0}, Lbcc;->R()Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-virtual {p0}, Lbcc;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    const/16 v4, 0x27

    const-wide/16 v5, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual/range {v0 .. v10}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;ZLaut;)I

    move-result v0

    .line 64
    invoke-virtual {v10}, Laut;->a()V

    .line 65
    const-string/jumbo v2, "count"

    invoke-virtual {v12, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    const-string/jumbo v0, "num_users"

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v12, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 69
    iget-object v0, v11, Lcom/twitter/model/json/contacts/JsonUploadAndMatchContactsResponse;->c:Ljava/util/List;

    invoke-virtual {p0, v0}, Lbcc;->a(Ljava/util/List;)V

    .line 72
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 24
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbcc;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    const-string/jumbo v0, "upload_and_match"

    return-object v0
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/json/contacts/JsonUploadAndMatchContactsResponse;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    const-class v0, Lcom/twitter/model/json/contacts/JsonUploadAndMatchContactsResponse;

    const-class v1, Lcom/twitter/model/core/z;

    invoke-static {v0, v1}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lbcc;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
