.class public final Laiz$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laiz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Laiz;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:J

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Laiz$a;->a:I

    return-void
.end method

.method static synthetic a(Laiz$a;)J
    .locals 2

    .prologue
    .line 30
    iget-wide v0, p0, Laiz$a;->b:J

    return-wide v0
.end method

.method static synthetic b(Laiz$a;)J
    .locals 2

    .prologue
    .line 30
    iget-wide v0, p0, Laiz$a;->c:J

    return-wide v0
.end method

.method static synthetic c(Laiz$a;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Laiz$a;->d:Z

    return v0
.end method

.method static synthetic d(Laiz$a;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Laiz$a;->e:Z

    return v0
.end method

.method static synthetic e(Laiz$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Laiz$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Laiz$a;)I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Laiz$a;->a:I

    return v0
.end method


# virtual methods
.method public R_()Z
    .locals 2

    .prologue
    .line 72
    iget v0, p0, Laiz$a;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Laiz$a;
    .locals 0

    .prologue
    .line 41
    iput p1, p0, Laiz$a;->a:I

    .line 42
    return-object p0
.end method

.method public a(J)Laiz$a;
    .locals 1

    .prologue
    .line 46
    iput-wide p1, p0, Laiz$a;->b:J

    .line 47
    return-object p0
.end method

.method public a(Ljava/lang/String;)Laiz$a;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Laiz$a;->f:Ljava/lang/String;

    .line 67
    return-object p0
.end method

.method public a(Z)Laiz$a;
    .locals 0

    .prologue
    .line 56
    iput-boolean p1, p0, Laiz$a;->d:Z

    .line 57
    return-object p0
.end method

.method public b(J)Laiz$a;
    .locals 1

    .prologue
    .line 51
    iput-wide p1, p0, Laiz$a;->c:J

    .line 52
    return-object p0
.end method

.method public b(Z)Laiz$a;
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Laiz$a;->e:Z

    .line 62
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Laiz$a;->e()Laiz;

    move-result-object v0

    return-object v0
.end method

.method public e()Laiz;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Laiz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laiz;-><init>(Laiz$a;Laiz$1;)V

    return-object v0
.end method
