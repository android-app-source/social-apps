.class public Lbam;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Ljava/lang/Void;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 27
    const-string/jumbo v0, "EnrollSMS2FABouncer"

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 33
    invoke-virtual {p0}, Lbam;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "bouncer"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "opt_in"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 35
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "flow"

    const-string/jumbo v2, "two_factor"

    .line 36
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Ljava/lang/Void;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    invoke-static {v0}, Lcom/twitter/model/core/z;->a(Lcom/twitter/model/core/z;)[I

    move-result-object v0

    iput-object v0, p0, Lbam;->a:[I

    .line 52
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 21
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbam;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Ljava/lang/Void;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method public e()[I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lbam;->a:[I

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lbam;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
