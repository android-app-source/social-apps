.class public Lbiu;
.super Lbyd;
.source "Twttr"


# instance fields
.field public final a:J

.field public final b:J

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/av/PlayerLayoutStates$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JJIJLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJIJ",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/av/PlayerLayoutStates$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p5, p6, p7}, Lbyd;-><init>(IJ)V

    .line 33
    iput-wide p1, p0, Lbiu;->a:J

    .line 34
    iput-wide p3, p0, Lbiu;->b:J

    .line 35
    iput-object p8, p0, Lbiu;->c:Ljava/util/List;

    .line 36
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 40
    if-ne p0, p1, :cond_1

    move v2, v1

    .line 53
    :cond_0
    :goto_0
    return v2

    .line 43
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v0, v3, :cond_0

    move-object v0, p1

    .line 47
    check-cast v0, Lbiu;

    .line 49
    invoke-super {p0, p1}, Lbyd;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 53
    iget-wide v4, p0, Lbiu;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-wide v4, v0, Lbiu;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lbiu;->b:J

    .line 54
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-wide v4, v0, Lbiu;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lbiu;->c:Ljava/util/List;

    iget-object v0, v0, Lbiu;->c:Ljava/util/List;

    .line 55
    invoke-static {v3, v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    move v2, v0

    .line 53
    goto :goto_0

    :cond_2
    move v0, v2

    .line 55
    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 60
    invoke-super {p0}, Lbyd;->hashCode()I

    move-result v0

    iget-wide v2, p0, Lbiu;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p0, Lbiu;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lbiu;->c:Ljava/util/List;

    invoke-static {v1, v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
