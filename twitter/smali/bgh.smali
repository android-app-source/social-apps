.class public Lbgh;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/model/timeline/g;

.field private final b:Lcom/twitter/android/timeline/bk;

.field private final c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/model/timeline/g;Lcom/twitter/android/timeline/bk;Z)V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lbgh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 29
    iput-object p3, p0, Lbgh;->a:Lcom/twitter/model/timeline/g;

    .line 30
    iput-object p4, p0, Lbgh;->b:Lcom/twitter/android/timeline/bk;

    .line 31
    iput-boolean p5, p0, Lbgh;->c:Z

    .line 32
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lbgh;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method b()Lcom/twitter/library/service/d$a;
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lbgh;->b:Lcom/twitter/android/timeline/bk;

    iget-object v0, v0, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    .line 43
    iget-object v1, p0, Lbgh;->b:Lcom/twitter/android/timeline/bk;

    iget-object v1, v1, Lcom/twitter/android/timeline/bk;->g:Ljava/lang/String;

    .line 44
    invoke-virtual {p0}, Lbgh;->J()Lcom/twitter/library/service/d$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 45
    invoke-virtual {v2, v3}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "timelines"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "feedback"

    aput-object v5, v3, v4

    .line 46
    invoke-virtual {v2, v3}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    const-string/jumbo v3, "feedback_type"

    iget-object v4, p0, Lbgh;->a:Lcom/twitter/model/timeline/g;

    iget-object v4, v4, Lcom/twitter/model/timeline/g;->b:Ljava/lang/String;

    .line 47
    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    const-string/jumbo v3, "undo"

    iget-boolean v4, p0, Lbgh;->c:Z

    .line 48
    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v2

    .line 49
    if-eqz v1, :cond_3

    .line 50
    const-string/jumbo v3, "feedback_metadata"

    invoke-virtual {v2, v3, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 56
    :goto_0
    if-eqz v0, :cond_2

    .line 57
    iget-object v1, v0, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 58
    const-string/jumbo v1, "injection_type"

    iget-object v3, v0, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 60
    :cond_0
    iget-object v1, v0, Lcom/twitter/model/timeline/r;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 61
    const-string/jumbo v1, "controller_data"

    iget-object v3, v0, Lcom/twitter/model/timeline/r;->c:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 63
    :cond_1
    iget-object v1, v0, Lcom/twitter/model/timeline/r;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 64
    const-string/jumbo v1, "source_data"

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->d:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 68
    :cond_2
    return-object v2

    .line 52
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "Attempting to submit dismiss feedback without a dismissFeedbackKey"

    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return-object v0
.end method
