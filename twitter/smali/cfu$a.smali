.class public final Lcfu$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcfu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcfu;",
        ">;"
    }
.end annotation


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Lcom/twitter/model/core/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 58
    invoke-static {}, Lcom/twitter/model/core/f;->a()Lcom/twitter/model/core/f;

    move-result-object v0

    iput-object v0, p0, Lcfu$a;->g:Lcom/twitter/model/core/f;

    .line 57
    return-void
.end method

.method static synthetic a(Lcfu$a;)J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcfu$a;->a:J

    return-wide v0
.end method

.method static synthetic b(Lcfu$a;)J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcfu$a;->b:J

    return-wide v0
.end method

.method static synthetic c(Lcfu$a;)J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcfu$a;->c:J

    return-wide v0
.end method

.method static synthetic d(Lcfu$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcfu$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcfu$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcfu$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcfu$a;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcfu$a;->f:Z

    return v0
.end method

.method static synthetic g(Lcfu$a;)Lcom/twitter/model/core/f;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcfu$a;->g:Lcom/twitter/model/core/f;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 104
    iget-wide v0, p0, Lcfu$a;->a:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lcfu$a;->c:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcfu$a;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcfu$a;
    .locals 1

    .prologue
    .line 62
    iput-wide p1, p0, Lcfu$a;->a:J

    .line 63
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/f;)Lcfu$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;)",
            "Lcfu$a;"
        }
    .end annotation

    .prologue
    .line 98
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcfu$a;->g:Lcom/twitter/model/core/f;

    .line 99
    return-object p0

    .line 98
    :cond_0
    invoke-static {}, Lcom/twitter/model/core/f;->a()Lcom/twitter/model/core/f;

    move-result-object p1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcfu$a;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcfu$a;->d:Ljava/lang/String;

    .line 81
    return-object p0
.end method

.method public a(Z)Lcfu$a;
    .locals 0

    .prologue
    .line 92
    iput-boolean p1, p0, Lcfu$a;->f:Z

    .line 93
    return-object p0
.end method

.method public b(J)Lcfu$a;
    .locals 1

    .prologue
    .line 68
    iput-wide p1, p0, Lcfu$a;->b:J

    .line 69
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcfu$a;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcfu$a;->e:Ljava/lang/String;

    .line 87
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcfu$a;->e()Lcfu;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcfu$a;
    .locals 1

    .prologue
    .line 74
    iput-wide p1, p0, Lcfu$a;->c:J

    .line 75
    return-object p0
.end method

.method protected e()Lcfu;
    .locals 2

    .prologue
    .line 110
    new-instance v0, Lcfu;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcfu;-><init>(Lcfu$a;Lcfu$1;)V

    return-object v0
.end method
