.class public abstract Lbfy;
.super Lbfl;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbfy$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbfl",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# instance fields
.field protected final b:J

.field protected final c:Ljava/lang/String;

.field protected h:Lcom/twitter/model/timeline/p;

.field protected i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/twitter/model/core/TwitterUser;

.field private final k:J

.field private l:Z

.field private m:J

.field private r:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;JI)V
    .locals 10

    .prologue
    .line 72
    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide v6, p4

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lbfy;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;JI)V

    .line 73
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;I)V
    .locals 9

    .prologue
    .line 67
    const-wide/16 v6, -0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v8, p5

    invoke-direct/range {v1 .. v8}, Lbfy;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;JI)V

    .line 68
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;JI)V
    .locals 5

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3, p7}, Lbfl;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;I)V

    .line 78
    if-nez p4, :cond_0

    const-wide/16 v0, -0x1

    cmp-long v0, p5, v0

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Either timelineOwner or timelineOwnerId must be valid. timelineOwner: %s timelineOwnerId: %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p4, v2, v3

    const/4 v3, 0x1

    .line 82
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 80
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    iget-wide v0, p3, Lcom/twitter/library/service/v;->c:J

    iput-wide v0, p0, Lbfy;->b:J

    .line 85
    iget-object v0, p3, Lcom/twitter/library/service/v;->e:Ljava/lang/String;

    iput-object v0, p0, Lbfy;->c:Ljava/lang/String;

    .line 86
    iput-object p4, p0, Lbfy;->j:Lcom/twitter/model/core/TwitterUser;

    .line 87
    iput-wide p5, p0, Lbfy;->k:J

    .line 88
    return-void
.end method

.method static a(ILbgf;)Lcom/twitter/model/timeline/p;
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 283
    packed-switch p0, :pswitch_data_0

    .line 312
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string/jumbo v2, "Attempting to get the request cursor for an unsupported request type: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 313
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 312
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :pswitch_0
    new-instance v0, Lcom/twitter/model/timeline/p$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/p$a;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/model/timeline/p$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/p;

    .line 306
    :goto_0
    return-object v0

    .line 292
    :pswitch_1
    new-instance v0, Lcom/twitter/model/timeline/p$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/p$a;-><init>()V

    .line 293
    invoke-interface {p1}, Lbgf;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/p$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/p$a;

    move-result-object v0

    .line 294
    invoke-virtual {v0}, Lcom/twitter/model/timeline/p$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/p;

    goto :goto_0

    .line 299
    :pswitch_2
    new-instance v0, Lcom/twitter/model/timeline/p$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/p$a;-><init>()V

    .line 300
    invoke-interface {p1}, Lbgf;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/p$a;->b(Ljava/lang/String;)Lcom/twitter/model/timeline/p$a;

    move-result-object v0

    .line 301
    invoke-virtual {v0}, Lcom/twitter/model/timeline/p$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/p;

    goto :goto_0

    .line 306
    :pswitch_3
    new-instance v0, Lcom/twitter/model/timeline/p$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/p$a;-><init>()V

    .line 307
    invoke-interface {p1}, Lbgf;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/p$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/p$a;

    move-result-object v0

    .line 308
    invoke-interface {p1}, Lbgf;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/p$a;->b(Ljava/lang/String;)Lcom/twitter/model/timeline/p$a;

    move-result-object v0

    .line 309
    invoke-virtual {v0}, Lcom/twitter/model/timeline/p$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/p;

    goto :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lcom/twitter/library/service/d$a;)V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lbfy;->h:Lcom/twitter/model/timeline/p;

    if-nez v0, :cond_0

    .line 150
    invoke-virtual {p0}, Lbfy;->x()I

    move-result v0

    invoke-virtual {p0}, Lbfy;->B()Lbgf;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(ILbgf;)Lcom/twitter/model/timeline/p;

    move-result-object v0

    iput-object v0, p0, Lbfy;->h:Lcom/twitter/model/timeline/p;

    .line 152
    :cond_0
    iget-object v0, p0, Lbfy;->h:Lcom/twitter/model/timeline/p;

    iget-object v0, v0, Lcom/twitter/model/timeline/p;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    const-string/jumbo v0, "up_cursor"

    iget-object v1, p0, Lbfy;->h:Lcom/twitter/model/timeline/p;

    iget-object v1, v1, Lcom/twitter/model/timeline/p;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 155
    :cond_1
    iget-object v0, p0, Lbfy;->h:Lcom/twitter/model/timeline/p;

    iget-object v0, v0, Lcom/twitter/model/timeline/p;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 156
    const-string/jumbo v0, "down_cursor"

    iget-object v1, p0, Lbfy;->h:Lcom/twitter/model/timeline/p;

    iget-object v1, v1, Lcom/twitter/model/timeline/p;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 158
    :cond_2
    return-void
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lbfy;->r:Z

    return v0
.end method

.method protected B()Lbgf;
    .locals 8

    .prologue
    .line 161
    new-instance v1, Lbfz;

    invoke-virtual {p0}, Lbfy;->R()Lcom/twitter/library/provider/t;

    move-result-object v2

    .line 162
    invoke-virtual {p0}, Lbfy;->s()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/model/timeline/am$a;->b(I)I

    move-result v3

    invoke-virtual {p0}, Lbfy;->C()J

    move-result-wide v4

    iget-wide v6, p0, Lbfy;->m:J

    invoke-direct/range {v1 .. v7}, Lbfz;-><init>(Lcom/twitter/library/provider/t;IJJ)V

    .line 161
    return-object v1
.end method

.method protected C()J
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lbfy;->j:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbfy;->j:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lbfy;->k:J

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/api/t;)Lbfy$a;
    .locals 13

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 202
    invoke-virtual {p0}, Lbfy;->R()Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 203
    iget-object v6, p1, Lcom/twitter/library/api/t;->a:Ljava/util/List;

    .line 204
    invoke-virtual {p0}, Lbfy;->C()J

    move-result-wide v4

    .line 206
    invoke-virtual {p0}, Lbfy;->s()I

    move-result v1

    .line 208
    invoke-virtual {p0}, Lbfy;->x()I

    move-result v7

    .line 210
    invoke-virtual {p0}, Lbfy;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v5, v2}, Lcom/twitter/library/provider/t;->a(IJLjava/lang/String;)I

    move-result v12

    .line 211
    const/4 v2, 0x4

    if-ne v7, v2, :cond_0

    iget-wide v2, p0, Lbfy;->m:J

    .line 212
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/t;->a(IJJ)Z

    move-result v2

    if-eqz v2, :cond_0

    move v11, v9

    .line 213
    :goto_0
    const/4 v2, 0x3

    if-ne v7, v2, :cond_1

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    move v8, v9

    :goto_1
    move-object v2, p0

    move-object v3, p1

    move v6, v1

    .line 215
    invoke-virtual/range {v2 .. v8}, Lbfy;->a(Lcom/twitter/library/api/t;JIIZ)Lcom/twitter/library/provider/s;

    move-result-object v1

    .line 216
    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/library/provider/s;)I

    move-result v0

    .line 219
    if-lez v0, :cond_2

    const/4 v1, 0x2

    if-ne v7, v1, :cond_2

    .line 220
    :goto_2
    new-instance v1, Lbfy$a$a;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lbfy$a$a;-><init>(Lbfy$1;)V

    .line 221
    invoke-virtual {v1, v11}, Lbfy$a$a;->a(Z)Lbfy$a$a;

    move-result-object v1

    .line 222
    invoke-virtual {v1, v0}, Lbfy$a$a;->a(I)Lbfy$a$a;

    move-result-object v0

    .line 223
    invoke-virtual {v0, v8}, Lbfy$a$a;->b(Z)Lbfy$a$a;

    move-result-object v0

    .line 224
    invoke-virtual {v0, v9}, Lbfy$a$a;->c(Z)Lbfy$a$a;

    move-result-object v0

    iget-boolean v1, p1, Lcom/twitter/library/api/t;->e:Z

    .line 225
    invoke-virtual {v0, v1}, Lbfy$a$a;->d(Z)Lbfy$a$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/library/api/t;->d:Lcom/twitter/model/timeline/u;

    .line 226
    invoke-virtual {v0, v1}, Lbfy$a$a;->a(Lcom/twitter/model/timeline/u;)Lbfy$a$a;

    move-result-object v0

    .line 227
    invoke-virtual {v0, v12}, Lbfy$a$a;->b(I)Lbfy$a$a;

    move-result-object v0

    .line 228
    invoke-virtual {v0}, Lbfy$a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfy$a;

    .line 220
    return-object v0

    :cond_0
    move v11, v10

    .line 212
    goto :goto_0

    :cond_1
    move v8, v10

    .line 213
    goto :goto_1

    :cond_2
    move v9, v10

    .line 219
    goto :goto_2
.end method

.method public a(J)Lbfy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lbfy;",
            ">(J)TT;"
        }
    .end annotation

    .prologue
    .line 97
    iput-wide p1, p0, Lbfy;->m:J

    .line 98
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfy;

    return-object v0
.end method

.method public a(Lcom/twitter/model/timeline/p;)Lbfy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lbfy;",
            ">(",
            "Lcom/twitter/model/timeline/p;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 104
    iput-object p1, p0, Lbfy;->h:Lcom/twitter/model/timeline/p;

    .line 105
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfy;

    return-object v0
.end method

.method public final a(Z)Lbfy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lbfy;",
            ">(Z)TT;"
        }
    .end annotation

    .prologue
    .line 122
    iput-boolean p1, p0, Lbfy;->l:Z

    .line 123
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfy;

    return-object v0
.end method

.method a(Lcom/twitter/library/api/t;JIIZ)Lcom/twitter/library/provider/s;
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 236
    const-string/jumbo v0, "bottom"

    iget-object v1, p1, Lcom/twitter/library/api/t;->b:Lcom/twitter/model/timeline/q;

    iget-object v1, v1, Lcom/twitter/model/timeline/q;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 238
    iget-object v0, p0, Lbfy;->h:Lcom/twitter/model/timeline/p;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/p;

    .line 239
    invoke-virtual {v0}, Lcom/twitter/model/timeline/p;->a()I

    move-result v5

    .line 240
    if-eq v5, v6, :cond_0

    const/4 v1, 0x4

    if-ne v5, v1, :cond_2

    :cond_0
    move v1, v2

    .line 242
    :goto_0
    if-eqz v4, :cond_3

    if-eqz v1, :cond_3

    move v1, v2

    .line 243
    :goto_1
    invoke-virtual {p0}, Lbfy;->N()Z

    move-result v4

    if-nez v4, :cond_1

    if-ne v5, v6, :cond_1

    move v3, v2

    .line 245
    :cond_1
    new-instance v4, Lcom/twitter/library/api/t$a;

    invoke-direct {v4, p5, v0}, Lcom/twitter/library/api/t$a;-><init>(ILcom/twitter/model/timeline/p;)V

    .line 247
    invoke-virtual {p1, v4}, Lcom/twitter/library/api/t;->a(Lcom/twitter/library/api/t$a;)V

    .line 249
    invoke-static {p1}, Lcom/twitter/library/provider/s$a;->a(Lcom/twitter/library/api/t;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 250
    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/provider/s$a;->a(J)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 251
    invoke-virtual {v0, p4}, Lcom/twitter/library/provider/s$a;->a(I)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 252
    invoke-virtual {p0}, Lbfy;->t()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/library/provider/s$a;->a(Ljava/lang/String;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 253
    invoke-virtual {v0, p6}, Lcom/twitter/library/provider/s$a;->a(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 254
    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/s$a;->b(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 255
    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/s$a;->c(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 256
    invoke-virtual {v0, v2}, Lcom/twitter/library/provider/s$a;->d(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 257
    invoke-virtual {p0}, Lbfy;->N()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/s$a;->e(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Lcom/twitter/library/provider/s$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/s;

    .line 248
    return-object v0

    :cond_2
    move v1, v3

    .line 240
    goto :goto_0

    :cond_3
    move v1, v3

    .line 242
    goto :goto_1
.end method

.method protected a(Lbfy$a;)V
    .locals 1

    .prologue
    .line 187
    iget v0, p1, Lbfy$a;->g:I

    if-gtz v0, :cond_0

    iget v0, p1, Lbfy$a;->b:I

    if-gtz v0, :cond_0

    iget-boolean v0, p1, Lbfy$a;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lbfy$a;->c:Z

    if-eqz v0, :cond_1

    .line 189
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbfy;->r:Z

    .line 190
    invoke-virtual {p0}, Lbfy;->y()V

    .line 191
    iget-object v0, p0, Lbfy;->p:Landroid/content/Context;

    invoke-static {v0}, Lawc;->a(Landroid/content/Context;)V

    .line 193
    :cond_1
    iget v0, p1, Lbfy$a;->b:I

    invoke-virtual {p0, v0}, Lbfy;->d(I)Lbge;

    .line 194
    iget-boolean v0, p1, Lbfy$a;->e:Z

    invoke-virtual {p0, v0}, Lbfy;->a(Z)Lbfy;

    .line 195
    iget-object v0, p1, Lbfy$a;->f:Lcom/twitter/model/timeline/u;

    invoke-virtual {p0, v0}, Lbfy;->a(Lcom/twitter/model/timeline/u;)Lbge;

    .line 196
    return-void
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 3

    .prologue
    .line 174
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 175
    invoke-virtual {p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/t;

    .line 176
    iget-object v1, v0, Lcom/twitter/library/api/t;->a:Ljava/util/List;

    iput-object v1, p0, Lbfy;->i:Ljava/util/List;

    .line 178
    invoke-virtual {p0, v0}, Lbfy;->a(Lcom/twitter/library/api/t;)Lbfy$a;

    move-result-object v0

    .line 179
    invoke-virtual {p0, v0}, Lbfy;->a(Lbfy$a;)V

    .line 182
    iget-object v1, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "scribe_item_count"

    iget v0, v0, Lbfy$a;->b:I

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 184
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 36
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lbfy;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lbfy;->h()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected g()Lcom/twitter/library/service/d$a;
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 143
    invoke-virtual {p0}, Lbfy;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 144
    invoke-direct {p0, v0}, Lbfy;->a(Lcom/twitter/library/service/d$a;)V

    .line 145
    return-object v0
.end method

.method protected h()Lcom/twitter/library/api/y;
    .locals 2

    .prologue
    .line 168
    const/16 v0, 0x2c

    iget-object v1, p0, Lbfy;->j:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0, v1}, Lcom/twitter/library/api/y;->a(ILcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected abstract s()I
.end method

.method protected t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x0

    return-object v0
.end method

.method protected y()V
    .locals 0

    .prologue
    .line 199
    return-void
.end method

.method public z()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lbfy;->l:Z

    return v0
.end method
