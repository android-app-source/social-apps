.class public Labv;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:I

.field private final b:Landroid/view/View;

.field private final c:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final d:Laby;

.field private e:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private f:Landroid/animation/ValueAnimator;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/view/View;ILaby;I)V
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Labv;->g:Z

    .line 57
    iput-object p1, p0, Labv;->b:Landroid/view/View;

    .line 58
    iput p2, p0, Labv;->c:I

    .line 59
    iput-object p3, p0, Labv;->d:Laby;

    .line 60
    iput p4, p0, Labv;->a:I

    .line 61
    return-void
.end method

.method static synthetic a(Labv;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Labv;->a:I

    return v0
.end method

.method static synthetic a(Labv;I)I
    .locals 0

    .prologue
    .line 21
    iput p1, p0, Labv;->e:I

    return p1
.end method

.method public static a(Landroid/view/View;Landroid/content/Context;)Labv;
    .locals 4

    .prologue
    .line 44
    new-instance v0, Labv;

    const v1, 0x7f110030

    invoke-static {p1, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    new-instance v2, Laby;

    invoke-direct {v2}, Laby;-><init>()V

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Labv;-><init>(Landroid/view/View;ILaby;I)V

    return-object v0
.end method

.method public static a(Landroid/view/View;Landroid/content/Context;Laal;)Labv;
    .locals 4

    .prologue
    .line 51
    new-instance v0, Labv;

    const v1, 0x7f110030

    invoke-static {p1, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    new-instance v2, Laby;

    invoke-direct {v2}, Laby;-><init>()V

    .line 52
    invoke-virtual {p2}, Laal;->b()I

    move-result v3

    invoke-direct {v0, p0, v1, v2, v3}, Labv;-><init>(Landroid/view/View;ILaby;I)V

    .line 51
    return-object v0
.end method

.method public static a(Landroid/view/ViewGroup;Landroid/content/Context;Laal;Z)Labv;
    .locals 1

    .prologue
    .line 37
    if-eqz p3, :cond_0

    .line 38
    invoke-static {p0, p1, p2}, Labv;->a(Landroid/view/View;Landroid/content/Context;Laal;)Labv;

    move-result-object v0

    .line 37
    :goto_0
    return-object v0

    .line 39
    :cond_0
    invoke-static {p0, p1}, Labv;->a(Landroid/view/View;Landroid/content/Context;)Labv;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Labv;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Labv;->f:Landroid/animation/ValueAnimator;

    return-object p1
.end method

.method static synthetic b(Labv;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Labv;->e:I

    return v0
.end method

.method static synthetic c(Labv;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Labv;->c:I

    return v0
.end method

.method private c()Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/b",
            "<-",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Labv$1;

    invoke-direct {v0, p0}, Labv$1;-><init>(Labv;)V

    return-object v0
.end method

.method static synthetic d(Labv;)Landroid/view/View;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Labv;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Labv;)Z
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Labv;->g:Z

    return v0
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Labv;->d:Laby;

    iget v1, p0, Labv;->c:I

    .line 66
    invoke-virtual {v0, p1, v1}, Laby;->a(Landroid/graphics/Bitmap;I)Lrx/g;

    move-result-object v0

    .line 67
    invoke-direct {p0}, Labv;->c()Lrx/functions/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    .line 65
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Labv;->f:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Labv;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 98
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Labv;->g:Z

    .line 99
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Labv;->f:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Labv;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    .line 105
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Labv;->g:Z

    .line 106
    return-void
.end method
