.class public Lahm;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lahm$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/search/SearchSuggestionController;

.field private final b:Lahm$a;

.field private final c:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/search/SearchSuggestionController;Lahm$a;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lahm;->a:Lcom/twitter/android/search/SearchSuggestionController;

    .line 33
    iput-object p2, p0, Lahm;->b:Lahm$a;

    .line 34
    iget-object v0, p0, Lahm;->b:Lahm$a;

    new-instance v1, Lahm$1;

    invoke-direct {v1, p0}, Lahm$1;-><init>(Lahm;)V

    invoke-virtual {v0, v1}, Lahm$a;->a(Landroid/view/View$OnClickListener;)V

    .line 42
    invoke-static {}, Lcom/twitter/android/search/d;->a()Z

    move-result v0

    iput-boolean v0, p0, Lahm;->c:Z

    .line 43
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/android/search/SearchSuggestionController;)Lahm;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lahm;

    invoke-static {p0}, Lahm$a;->a(Landroid/content/Context;)Lahm$a;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lahm;-><init>(Lcom/twitter/android/search/SearchSuggestionController;Lahm$a;)V

    return-object v0
.end method

.method static synthetic a(Lahm;)Lcom/twitter/android/search/SearchSuggestionController;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lahm;->a:Lcom/twitter/android/search/SearchSuggestionController;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/net/Uri;Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 1

    .prologue
    .line 54
    invoke-static {p2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    iget-boolean v0, p0, Lahm;->c:Z

    if-eqz v0, :cond_0

    .line 56
    sget-object v0, Lcom/twitter/app/main/MainActivity;->e:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {p0, p2}, Lahm;->b(Lcom/twitter/internal/android/widget/ToolBar;)V

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-virtual {p0, p2}, Lahm;->c(Lcom/twitter/internal/android/widget/ToolBar;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 2

    .prologue
    const v1, 0x7f13088d

    .line 46
    iget-boolean v0, p0, Lahm;->c:Z

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    goto :goto_0
.end method

.method public b(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lahm;->b:Lahm$a;

    invoke-virtual {v0}, Lahm$a;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    .line 66
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowTitleEnabled(Z)V

    .line 67
    return-void
.end method

.method public c(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    .line 71
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowTitleEnabled(Z)V

    .line 72
    return-void
.end method
