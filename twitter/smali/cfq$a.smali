.class public final Lcfq$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcfq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcfq;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcgc;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcgb;

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcgb;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:D

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcfq$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcfq$a;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcfq$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcfq$a;->b:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(Lcfq$a;)Lcgb;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcfq$a;->c:Lcgb;

    return-object v0
.end method

.method static synthetic d(Lcfq$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcfq$a;->d:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic e(Lcfq$a;)Lcgb;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcfq$a;->e:Lcgb;

    return-object v0
.end method

.method static synthetic f(Lcfq$a;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcfq$a;->f:I

    return v0
.end method

.method static synthetic g(Lcfq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcfq$a;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcfq$a;)D
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcfq$a;->h:D

    return-wide v0
.end method

.method static synthetic i(Lcfq$a;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcfq$a;->i:Z

    return v0
.end method

.method static synthetic j(Lcfq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcfq$a;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcfq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcfq$a;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcfq$a;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcfq$a;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(D)Lcfq$a;
    .locals 1

    .prologue
    .line 129
    iput-wide p1, p0, Lcfq$a;->h:D

    .line 130
    return-object p0
.end method

.method public a(I)Lcfq$a;
    .locals 0

    .prologue
    .line 117
    iput p1, p0, Lcfq$a;->f:I

    .line 118
    return-object p0
.end method

.method public a(Lcgb;)Lcfq$a;
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcfq$a;->c:Lcgb;

    .line 148
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcfq$a;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcfq$a;->j:Ljava/lang/String;

    .line 100
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcfq$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcgc;",
            ">;)",
            "Lcfq$a;"
        }
    .end annotation

    .prologue
    .line 135
    iput-object p1, p0, Lcfq$a;->a:Ljava/util/List;

    .line 136
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcfq$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcfq$a;"
        }
    .end annotation

    .prologue
    .line 141
    iput-object p1, p0, Lcfq$a;->b:Ljava/util/Map;

    .line 142
    return-object p0
.end method

.method public a(Z)Lcfq$a;
    .locals 0

    .prologue
    .line 111
    iput-boolean p1, p0, Lcfq$a;->i:Z

    .line 112
    return-object p0
.end method

.method public b(Lcgb;)Lcfq$a;
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcfq$a;->e:Lcgb;

    .line 160
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcfq$a;
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcfq$a;->k:Ljava/lang/String;

    .line 106
    return-object p0
.end method

.method public b(Ljava/util/Map;)Lcfq$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcfq$a;"
        }
    .end annotation

    .prologue
    .line 153
    iput-object p1, p0, Lcfq$a;->d:Ljava/util/Map;

    .line 154
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcfq$a;->e()Lcfq;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcfq$a;
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcfq$a;->g:Ljava/lang/String;

    .line 124
    return-object p0
.end method

.method protected e()Lcfq;
    .locals 2

    .prologue
    .line 166
    new-instance v0, Lcfq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcfq;-><init>(Lcfq$a;Lcfq$1;)V

    return-object v0
.end method
