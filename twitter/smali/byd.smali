.class public Lbyd;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final d:I

.field public final e:J


# direct methods
.method public constructor <init>(IJ)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lbyd;->d:I

    .line 25
    iput-wide p2, p0, Lbyd;->e:J

    .line 26
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 30
    if-ne p0, p1, :cond_1

    .line 37
    :cond_0
    :goto_0
    return v0

    .line 33
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 34
    goto :goto_0

    .line 36
    :cond_3
    check-cast p1, Lbyd;

    .line 37
    iget v2, p0, Lbyd;->d:I

    iget v3, p1, Lbyd;->d:I

    if-ne v2, v3, :cond_4

    iget-wide v2, p0, Lbyd;->e:J

    iget-wide v4, p1, Lbyd;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 43
    iget v0, p0, Lbyd;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-wide v2, p0, Lbyd;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
