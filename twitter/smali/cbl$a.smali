.class public Lcbl$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcbl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/collection/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/h",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    iput-object v0, p0, Lcbl$a;->a:Lcom/twitter/util/collection/h;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Iterable;)Lcbl$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TT;>;)",
            "Lcbl$a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcbl$a;->a:Lcom/twitter/util/collection/h;

    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    .line 60
    return-object p0
.end method

.method public a(Ljava/lang/Object;)Lcbl$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcbl$a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcbl$a;->a:Lcom/twitter/util/collection/h;

    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 54
    return-object p0
.end method

.method public a()Lcbl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 65
    new-instance v1, Lcbl;

    iget-object v0, p0, Lcbl$a;->a:Lcom/twitter/util/collection/h;

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-direct {v1, v0}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    return-object v1
.end method
