.class public Ladw;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcah;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/List",
            "<",
            "Lbzv;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lbzv;->a:Lcom/twitter/util/serialization/l;

    .line 27
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Ladw;->a:Lcom/twitter/util/serialization/l;

    .line 26
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcbp;-><init>()V

    return-void
.end method

.method private a(Lads;IIILcap$a;)V
    .locals 3

    .prologue
    .line 72
    packed-switch p2, :pswitch_data_0

    .line 80
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Tried to hydrate activity with invalid source type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 85
    :goto_0
    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lads;->getBlob(I)[B

    move-result-object v0

    .line 86
    packed-switch p3, :pswitch_data_1

    .line 97
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Tried to hydrate activity with invalid target type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 102
    :goto_1
    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Lads;->getBlob(I)[B

    move-result-object v0

    .line 103
    packed-switch p4, :pswitch_data_2

    .line 117
    :goto_2
    return-void

    .line 74
    :pswitch_0
    const/16 v0, 0xb

    .line 75
    invoke-virtual {p1, v0}, Lads;->getBlob(I)[B

    move-result-object v0

    .line 74
    invoke-virtual {p1, v0}, Lads;->a([B)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p5, v0}, Lcap$a;->a(Ljava/util/List;)Lcap$a;

    goto :goto_0

    .line 88
    :pswitch_1
    invoke-virtual {p1, v0}, Lads;->b([B)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p5, v0}, Lcap$a;->b(Ljava/util/List;)Lcap$a;

    goto :goto_1

    .line 92
    :pswitch_2
    invoke-virtual {p1, v0}, Lads;->a([B)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p5, v0}, Lcap$a;->b(Ljava/util/List;)Lcap$a;

    goto :goto_1

    .line 105
    :pswitch_3
    sget-object v1, Ladw;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p5, v0}, Lcap$a;->c(Ljava/util/List;)Lcap$a;

    goto :goto_2

    .line 109
    :pswitch_4
    invoke-virtual {p1, v0}, Lads;->b([B)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p5, v0}, Lcap$a;->c(Ljava/util/List;)Lcap$a;

    goto :goto_2

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 86
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 103
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Ladw;->b(Landroid/database/Cursor;)Lcah;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 31
    invoke-static {p1}, Ladt;->b(Landroid/database/Cursor;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/database/Cursor;)Lcah;
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 37
    move-object v1, p1

    check-cast v1, Lads;

    .line 39
    const/16 v0, 0x9

    invoke-virtual {v1, v0}, Lads;->getInt(I)I

    move-result v0

    .line 41
    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lads;->getInt(I)I

    move-result v2

    .line 43
    const/16 v3, 0xf

    invoke-virtual {v1, v3}, Lads;->getInt(I)I

    move-result v3

    .line 45
    const/16 v4, 0x12

    invoke-virtual {v1, v4}, Lads;->getInt(I)I

    move-result v4

    .line 47
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Lads;->getInt(I)I

    move-result v5

    if-nez v5, :cond_0

    const/4 v6, 0x1

    .line 49
    :goto_0
    new-instance v5, Lcap$a;

    invoke-direct {v5}, Lcap$a;-><init>()V

    .line 50
    invoke-virtual {v5, v0}, Lcap$a;->a(I)Lcap$a;

    move-result-object v0

    const/4 v5, 0x5

    .line 51
    invoke-virtual {v1, v5}, Lads;->getLong(I)J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Lcap$a;->b(J)Lcap$a;

    move-result-object v0

    const/4 v5, 0x6

    .line 52
    invoke-virtual {v1, v5}, Lads;->getLong(I)J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Lcap$a;->c(J)Lcap$a;

    move-result-object v0

    const/16 v5, 0xd

    .line 53
    invoke-virtual {v1, v5}, Lads;->getInt(I)I

    move-result v5

    invoke-virtual {v0, v5}, Lcap$a;->b(I)Lcap$a;

    move-result-object v0

    .line 54
    invoke-virtual {v0, v2}, Lcap$a;->c(I)Lcap$a;

    move-result-object v0

    const/16 v5, 0x10

    .line 55
    invoke-virtual {v1, v5}, Lads;->getInt(I)I

    move-result v5

    invoke-virtual {v0, v5}, Lcap$a;->d(I)Lcap$a;

    move-result-object v0

    .line 56
    invoke-virtual {v0, v3}, Lcap$a;->e(I)Lcap$a;

    move-result-object v0

    const/16 v5, 0x13

    .line 58
    invoke-virtual {v1, v5}, Lads;->getInt(I)I

    move-result v5

    .line 57
    invoke-virtual {v0, v5}, Lcap$a;->f(I)Lcap$a;

    move-result-object v0

    .line 59
    invoke-virtual {v0, v4}, Lcap$a;->g(I)Lcap$a;

    move-result-object v5

    move-object v0, p0

    .line 61
    invoke-direct/range {v0 .. v5}, Ladw;->a(Lads;IIILcap$a;)V

    .line 62
    invoke-virtual {v5}, Lcap$a;->q()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcap;

    .line 64
    new-instance v0, Lbzy;

    iget-wide v2, v7, Lcap;->b:J

    iget-wide v4, v7, Lcap;->c:J

    move v1, v8

    invoke-direct/range {v0 .. v6}, Lbzy;-><init>(IJJZ)V

    .line 66
    new-instance v1, Lcah;

    invoke-direct {v1, v0, v7}, Lcah;-><init>(Lbzy;Lcap;)V

    return-object v1

    :cond_0
    move v6, v8

    .line 47
    goto :goto_0
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 25
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Ladw;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
