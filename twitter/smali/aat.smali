.class public Laat;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/ui/widget/ToggleTwitterButton;


# direct methods
.method public constructor <init>(Lcom/twitter/ui/widget/ToggleTwitterButton;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Laat;->a:Lcom/twitter/ui/widget/ToggleTwitterButton;

    .line 16
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Laat;->a:Lcom/twitter/ui/widget/ToggleTwitterButton;

    return-object v0
.end method

.method public a(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 48
    iget-object v0, p0, Laat;->a:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setText(I)V

    .line 49
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Laat;->a:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Laat;->a:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 53
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Laat;->a:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setToggledOn(Z)V

    .line 41
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Laat;->a:Lcom/twitter/ui/widget/ToggleTwitterButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setVisibility(I)V

    .line 32
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Laat;->a:Lcom/twitter/ui/widget/ToggleTwitterButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setVisibility(I)V

    .line 37
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Laat;->a:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->e()Z

    move-result v0

    return v0
.end method
