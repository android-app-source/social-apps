.class public Lbwd;
.super Lbvz;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbvz",
        "<",
        "Lcom/mopub/nativeads/NativeAd;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lbwd;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 15
    new-instance v0, Lbwd;

    .line 16
    invoke-static {v4}, Lcom/twitter/library/revenue/b;->a(I)I

    move-result v1

    const-wide/16 v2, 0x1e

    .line 18
    invoke-static {v2, v3}, Lcom/twitter/library/revenue/b;->a(J)J

    move-result-wide v2

    invoke-direct {v0, v1, v4, v2, v3}, Lbwd;-><init>(IIJ)V

    sput-object v0, Lbwd;->a:Lbwd;

    .line 15
    return-void
.end method

.method constructor <init>(IIJ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, Lbvz;-><init>(IIJ)V

    .line 26
    return-void
.end method

.method public static c()Lbwd;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lbwd;->a:Lbwd;

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mopub/nativeads/NativeAd;

    .line 41
    invoke-virtual {v0}, Lcom/mopub/nativeads/NativeAd;->destroy()V

    goto :goto_0

    .line 43
    :cond_0
    return-void
.end method

.method a(ZLjava/lang/String;Lcom/mopub/nativeads/NativeAd;Lcom/mopub/nativeads/NativeAd;)V
    .locals 0

    .prologue
    .line 35
    invoke-virtual {p3}, Lcom/mopub/nativeads/NativeAd;->destroy()V

    .line 36
    return-void
.end method

.method bridge synthetic a(ZLjava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    check-cast p3, Lcom/mopub/nativeads/NativeAd;

    check-cast p4, Lcom/mopub/nativeads/NativeAd;

    invoke-virtual {p0, p1, p2, p3, p4}, Lbwd;->a(ZLjava/lang/String;Lcom/mopub/nativeads/NativeAd;Lcom/mopub/nativeads/NativeAd;)V

    return-void
.end method
