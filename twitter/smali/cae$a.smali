.class public final Lcae$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcae;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcaf;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcad;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcae$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcae$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcae$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcae$a;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcae$a;)Lcad;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcae$a;->c:Lcad;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcae$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcae$a;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcad;)Lcae$a;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcae$a;->c:Lcad;

    .line 52
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcae$a;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcae$a;->a:Ljava/lang/String;

    .line 40
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcae$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcaf;",
            ">;)",
            "Lcae$a;"
        }
    .end annotation

    .prologue
    .line 45
    iput-object p1, p0, Lcae$a;->b:Ljava/util/List;

    .line 46
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcae$a;->e()Lcae;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcae;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcae;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcae;-><init>(Lcae$a;Lcae$1;)V

    return-object v0
.end method
