.class public Lbfm;
.super Lbfj;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbfm$a;
    }
.end annotation


# instance fields
.field private final b:J

.field private c:Lcgi;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V
    .locals 11

    .prologue
    .line 47
    .line 51
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v8

    const-class v0, Lcom/twitter/model/core/ac;

    .line 52
    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    .line 47
    invoke-direct/range {v1 .. v9}, Lbfm;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJLcom/twitter/library/provider/t;Lcom/twitter/library/api/i;)V

    .line 53
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJLcom/twitter/library/provider/t;Lcom/twitter/library/api/i;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "JJ",
            "Lcom/twitter/library/provider/t;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lbfj;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZLcom/twitter/library/provider/t;Lcom/twitter/library/api/i;)V

    .line 60
    iput-wide p5, p0, Lbfm;->b:J

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lbfm;->g:Ljava/lang/Boolean;

    .line 62
    const-string/jumbo v0, "tweet_type"

    const-string/jumbo v1, "organic"

    invoke-virtual {p0, v0, v1}, Lbfm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    new-instance v0, Lcom/twitter/library/service/o;

    invoke-direct {v0}, Lcom/twitter/library/service/o;-><init>()V

    invoke-virtual {p0, v0}, Lbfm;->a(Lcom/twitter/library/service/e;)V

    .line 64
    return-void
.end method


# virtual methods
.method public a(Lcgi;)Lbfm;
    .locals 2

    .prologue
    .line 101
    iput-object p1, p0, Lbfm;->c:Lcgi;

    .line 102
    iget-object v0, p0, Lbfm;->c:Lcgi;

    if-eqz v0, :cond_0

    .line 103
    const-string/jumbo v0, "tweet_type"

    const-string/jumbo v1, "ad"

    invoke-virtual {p0, v0, v1}, Lbfm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/Boolean;)Lbfm;
    .locals 2

    .prologue
    .line 110
    iput-object p1, p0, Lbfm;->g:Ljava/lang/Boolean;

    .line 111
    iget-object v0, p0, Lbfm;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 112
    const-string/jumbo v1, "has_media"

    iget-object v0, p0, Lbfm;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "true"

    :goto_0
    invoke-virtual {p0, v1, v0}, Lbfm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_0
    return-object p0

    .line 112
    :cond_1
    const-string/jumbo v0, "false"

    goto :goto_0
.end method

.method protected a()Lcom/twitter/library/service/d;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 69
    invoke-virtual {p0}, Lbfm;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 70
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "favorites"

    aput-object v3, v1, v2

    const-string/jumbo v2, "create"

    aput-object v2, v1, v4

    .line 71
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "send_error_codes"

    .line 72
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "id"

    iget-wide v2, p0, Lbfm;->b:J

    .line 73
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lbfm;->c:Lcgi;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbfm;->c:Lcgi;

    iget-object v1, v1, Lcgi;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 76
    const-string/jumbo v1, "impression_id"

    iget-object v2, p0, Lbfm;->c:Lcgi;

    iget-object v2, v2, Lcgi;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 77
    iget-object v1, p0, Lbfm;->c:Lcgi;

    invoke-virtual {v1}, Lcgi;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    const-string/jumbo v1, "earned"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 82
    :cond_0
    const-string/jumbo v1, "include_entities"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "include_media_features"

    .line 83
    invoke-virtual {v1, v2, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 84
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->b()Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 85
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->d()Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 86
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    .line 88
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lcom/twitter/async/service/j;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 119
    invoke-super {p0, p1}, Lbfj;->d(Lcom/twitter/async/service/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    :goto_0
    return v1

    .line 123
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 137
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v2

    const/16 v3, 0x194

    if-eq v2, v3, :cond_1

    .line 138
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v2, 0x193

    if-ne v0, v2, :cond_2

    :cond_1
    move v0, v1

    :goto_1
    move v1, v0

    .line 137
    goto :goto_0

    .line 138
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    const-string/jumbo v0, "app:twitter_service:favorite:create"

    return-object v0
.end method
