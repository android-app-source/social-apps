.class public final Lccd$b;
.super Lcbx$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lccd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbx$b",
        "<",
        "Lccd;",
        "Lccd$a;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcbx$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lccd$a;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lccd$a;

    invoke-direct {v0}, Lccd$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcbx$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 92
    check-cast p2, Lccd$a;

    invoke-virtual {p0, p1, p2, p3}, Lccd$b;->a(Lcom/twitter/util/serialization/n;Lccd$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lccd$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 114
    invoke-super {p0, p1, p2, p3}, Lcbx$b;->a(Lcom/twitter/util/serialization/n;Lcbx$a;I)V

    .line 115
    sget-object v0, Lcom/twitter/model/geo/b;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/b;

    invoke-virtual {p2, v0}, Lccd$a;->a(Lcom/twitter/model/geo/b;)Lccd$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    .line 116
    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lccd$a;->a(Ljava/util/List;)Lccd$a;

    .line 117
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 92
    check-cast p2, Lccd$a;

    invoke-virtual {p0, p1, p2, p3}, Lccd$b;->a(Lcom/twitter/util/serialization/n;Lccd$a;I)V

    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/util/serialization/o;Lcbx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    check-cast p2, Lccd;

    invoke-virtual {p0, p1, p2}, Lccd$b;->a(Lcom/twitter/util/serialization/o;Lccd;)V

    return-void
.end method

.method public a(Lcom/twitter/util/serialization/o;Lccd;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-super {p0, p1, p2}, Lcbx$b;->a(Lcom/twitter/util/serialization/o;Lcbx;)V

    .line 106
    invoke-static {p2}, Lccd;->a(Lccd;)Lcom/twitter/model/geo/b;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/geo/b;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 107
    invoke-virtual {p2}, Lccd;->j()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 108
    return-void
.end method

.method public synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    check-cast p2, Lccd;

    invoke-virtual {p0, p1, p2}, Lccd$b;->a(Lcom/twitter/util/serialization/o;Lccd;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lccd$b;->a()Lccd$a;

    move-result-object v0

    return-object v0
.end method
