.class public Lajg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lajg$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/v;

.field private final c:Lcom/twitter/library/client/p;

.field private final d:Lajk;

.field private final e:Lcom/twitter/model/core/Tweet;

.field private final f:Lcom/twitter/model/util/FriendshipCache;

.field private final g:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

.field private final h:Lcom/twitter/library/widget/h;

.field private final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lajg$a;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lajg$a;->a(Lajg$a;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lajg;->a:Landroid/content/Context;

    .line 39
    invoke-static {p1}, Lajg$a;->b(Lajg$a;)Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lajg;->b:Lcom/twitter/library/client/v;

    .line 40
    invoke-static {p1}, Lajg$a;->c(Lajg$a;)Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lajg;->c:Lcom/twitter/library/client/p;

    .line 41
    invoke-static {p1}, Lajg$a;->d(Lajg$a;)Lajk;

    move-result-object v0

    iput-object v0, p0, Lajg;->d:Lajk;

    .line 42
    invoke-static {p1}, Lajg$a;->e(Lajg$a;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iput-object v0, p0, Lajg;->e:Lcom/twitter/model/core/Tweet;

    .line 43
    invoke-static {p1}, Lajg$a;->f(Lajg$a;)Lcom/twitter/model/util/FriendshipCache;

    move-result-object v0

    iput-object v0, p0, Lajg;->f:Lcom/twitter/model/util/FriendshipCache;

    .line 44
    invoke-static {p1}, Lajg$a;->g(Lajg$a;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    iput-object v0, p0, Lajg;->g:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 45
    invoke-static {p1}, Lajg$a;->h(Lajg$a;)Lcom/twitter/library/widget/h;

    move-result-object v0

    iput-object v0, p0, Lajg;->h:Lcom/twitter/library/widget/h;

    .line 46
    invoke-static {p1}, Lajg$a;->i(Lajg$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lajg;->i:Ljava/lang/String;

    .line 47
    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 52
    iget-object v0, p0, Lajg;->f:Lcom/twitter/model/util/FriendshipCache;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    const-string/jumbo v2, "FriendshipCache is null in FollowAction"

    invoke-static {v0, v2}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 53
    iget-object v0, p0, Lajg;->f:Lcom/twitter/model/util/FriendshipCache;

    if-eqz v0, :cond_2

    .line 54
    iget-object v0, p0, Lajg;->f:Lcom/twitter/model/util/FriendshipCache;

    iget-object v2, p0, Lajg;->e:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->k(J)Z

    move-result v0

    if-nez v0, :cond_4

    .line 55
    :goto_1
    iget-object v0, p0, Lajg;->h:Lcom/twitter/library/widget/h;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lajg;->h:Lcom/twitter/library/widget/h;

    invoke-interface {v0, v1}, Lcom/twitter/library/widget/h;->c(Z)V

    .line 59
    :cond_0
    iget-object v0, p0, Lajg;->b:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 61
    if-eqz v1, :cond_5

    .line 62
    iget-object v0, p0, Lajg;->d:Lajk;

    iget-object v1, p0, Lajg;->i:Ljava/lang/String;

    const-string/jumbo v2, "follow"

    iget-object v4, p0, Lajg;->e:Lcom/twitter/model/core/Tweet;

    iget-object v5, p0, Lajg;->g:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-virtual {v0, v1, v2, v4, v5}, Lajk;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 63
    iget-object v0, p0, Lajg;->c:Lcom/twitter/library/client/p;

    new-instance v1, Lbhq;

    iget-object v2, p0, Lajg;->a:Landroid/content/Context;

    iget-object v4, p0, Lajg;->e:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v4, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v6, p0, Lajg;->e:Lcom/twitter/model/core/Tweet;

    .line 64
    invoke-virtual {v6}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 63
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 65
    iget-object v0, p0, Lajg;->f:Lcom/twitter/model/util/FriendshipCache;

    iget-object v1, p0, Lajg;->e:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->s:J

    .line 66
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lajg;->f:Lcom/twitter/model/util/FriendshipCache;

    iget-object v1, p0, Lajg;->e:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->s:J

    .line 67
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 68
    :cond_1
    or-int/lit8 v0, v7, 0x1

    or-int/lit8 v0, v0, 0x40

    .line 70
    iget-object v1, p0, Lajg;->f:Lcom/twitter/model/util/FriendshipCache;

    iget-object v2, p0, Lajg;->e:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/model/util/FriendshipCache;->b(JI)V

    .line 79
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v7

    .line 52
    goto :goto_0

    :cond_4
    move v1, v7

    .line 54
    goto :goto_1

    .line 72
    :cond_5
    iget-object v0, p0, Lajg;->d:Lajk;

    iget-object v1, p0, Lajg;->i:Ljava/lang/String;

    const-string/jumbo v2, "unfollow"

    iget-object v4, p0, Lajg;->e:Lcom/twitter/model/core/Tweet;

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v2, v4, v5}, Lajk;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 73
    iget-object v0, p0, Lajg;->c:Lcom/twitter/library/client/p;

    new-instance v1, Lbhs;

    iget-object v2, p0, Lajg;->a:Landroid/content/Context;

    iget-object v4, p0, Lajg;->e:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v4, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v6, p0, Lajg;->e:Lcom/twitter/model/core/Tweet;

    .line 75
    invoke-virtual {v6}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 73
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 76
    iget-object v0, p0, Lajg;->f:Lcom/twitter/model/util/FriendshipCache;

    iget-object v1, p0, Lajg;->e:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    goto :goto_2
.end method
