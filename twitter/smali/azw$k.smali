.class public final Lazw$k;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lazw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "k"
.end annotation


# static fields
.field public static final abbr_number_unit_billions:I = 0x7f0a0b7c

.field public static final abbr_number_unit_millions:I = 0x7f0a0b7a

.field public static final abbr_number_unit_thousands:I = 0x7f0a0b7b

.field public static final abc_action_bar_home_description:I = 0x7f0a0000

.field public static final abc_action_bar_home_description_format:I = 0x7f0a0001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f0a0002

.field public static final abc_action_bar_up_description:I = 0x7f0a0003

.field public static final abc_action_menu_overflow_description:I = 0x7f0a0004

.field public static final abc_action_mode_done:I = 0x7f0a0005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0a0006

.field public static final abc_activitychooserview_choose_application:I = 0x7f0a0007

.field public static final abc_capital_off:I = 0x7f0a0008

.field public static final abc_capital_on:I = 0x7f0a0009

.field public static final abc_font_family_body_1_material:I = 0x7f0a0b7d

.field public static final abc_font_family_body_2_material:I = 0x7f0a0b7e

.field public static final abc_font_family_button_material:I = 0x7f0a0b7f

.field public static final abc_font_family_caption_material:I = 0x7f0a0b80

.field public static final abc_font_family_display_1_material:I = 0x7f0a0b81

.field public static final abc_font_family_display_2_material:I = 0x7f0a0b82

.field public static final abc_font_family_display_3_material:I = 0x7f0a0b83

.field public static final abc_font_family_display_4_material:I = 0x7f0a0b84

.field public static final abc_font_family_headline_material:I = 0x7f0a0b85

.field public static final abc_font_family_menu_material:I = 0x7f0a0b86

.field public static final abc_font_family_subhead_material:I = 0x7f0a0b87

.field public static final abc_font_family_title_material:I = 0x7f0a0b88

.field public static final abc_search_hint:I = 0x7f0a000a

.field public static final abc_searchview_description_clear:I = 0x7f0a000b

.field public static final abc_searchview_description_query:I = 0x7f0a000c

.field public static final abc_searchview_description_search:I = 0x7f0a000d

.field public static final abc_searchview_description_submit:I = 0x7f0a000e

.field public static final abc_searchview_description_voice:I = 0x7f0a000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f0a0010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0a0011

.field public static final abc_toolbar_collapse_description:I = 0x7f0a0012

.field public static final accounts_dialog_add_account:I = 0x7f0a0030

.field public static final accounts_dialog_new_account:I = 0x7f0a0031

.field public static final ads_companion:I = 0x7f0a0051

.field public static final amazon_app_store_url_format:I = 0x7f0a0b8e

.field public static final app_download_url:I = 0x7f0a0b90

.field public static final app_name:I = 0x7f0a0061

.field public static final appbar_scrolling_view_behavior:I = 0x7f0a0b91

.field public static final attached_gif:I = 0x7f0a0078

.field public static final attached_photo:I = 0x7f0a0079

.field public static final attached_video:I = 0x7f0a007a

.field public static final av_playback_forbidden:I = 0x7f0a0095

.field public static final av_playback_forbidden_device:I = 0x7f0a0096

.field public static final av_player_button_collapse:I = 0x7f0a0097

.field public static final av_player_button_dock:I = 0x7f0a0098

.field public static final av_player_button_fullscreen:I = 0x7f0a0099

.field public static final av_player_live_badge_label:I = 0x7f0a009a

.field public static final av_playlist_download_failed:I = 0x7f0a009b

.field public static final av_preroll_countdown_text:I = 0x7f0a009c

.field public static final av_time_duration_text:I = 0x7f0a009d

.field public static final av_view_counts_text:I = 0x7f0a009e

.field public static final bottom_sheet_behavior:I = 0x7f0a0b94

.field public static final button_search:I = 0x7f0a00dd

.field public static final button_toolbar_back:I = 0x7f0a00e4

.field public static final button_toolbar_overflow:I = 0x7f0a00e5

.field public static final call_now_title:I = 0x7f0a00ea

.field public static final camera_photo_error:I = 0x7f0a00ed

.field public static final campaign_metrics_bar_engagements:I = 0x7f0a00f3

.field public static final campaign_metrics_bar_impressions:I = 0x7f0a00f5

.field public static final character_counter_pattern:I = 0x7f0a0b96

.field public static final com_crashlytics_android_build_id:I = 0x7f0a0b98

.field public static final commerce_cc_amex:I = 0x7f0a0136

.field public static final commerce_cc_diners:I = 0x7f0a0137

.field public static final commerce_cc_discover:I = 0x7f0a0138

.field public static final commerce_cc_jcb:I = 0x7f0a0139

.field public static final commerce_cc_mastercard:I = 0x7f0a013a

.field public static final commerce_cc_visa:I = 0x7f0a013b

.field public static final commerce_error_account_duplicate:I = 0x7f0a0148

.field public static final commerce_error_account_not_found:I = 0x7f0a0149

.field public static final commerce_error_address_not_supported:I = 0x7f0a014a

.field public static final commerce_error_amount_changed:I = 0x7f0a014b

.field public static final commerce_error_auth_error:I = 0x7f0a014c

.field public static final commerce_error_bad_request:I = 0x7f0a014d

.field public static final commerce_error_email_confirmation_required:I = 0x7f0a014f

.field public static final commerce_error_empty_email:I = 0x7f0a0150

.field public static final commerce_error_empty_phone:I = 0x7f0a0151

.field public static final commerce_error_invalid_address_address:I = 0x7f0a0154

.field public static final commerce_error_invalid_address_city:I = 0x7f0a0155

.field public static final commerce_error_invalid_address_country:I = 0x7f0a0156

.field public static final commerce_error_invalid_address_country_unknown:I = 0x7f0a0157

.field public static final commerce_error_invalid_address_email_length:I = 0x7f0a0158

.field public static final commerce_error_invalid_address_email_unknown:I = 0x7f0a0159

.field public static final commerce_error_invalid_address_name:I = 0x7f0a015a

.field public static final commerce_error_invalid_address_state:I = 0x7f0a015b

.field public static final commerce_error_invalid_address_zip:I = 0x7f0a015c

.field public static final commerce_error_invalid_amount:I = 0x7f0a015d

.field public static final commerce_error_invalid_card_ccv_number:I = 0x7f0a015e

.field public static final commerce_error_invalid_card_ccv_number_empty:I = 0x7f0a015f

.field public static final commerce_error_invalid_card_date:I = 0x7f0a0160

.field public static final commerce_error_invalid_card_declined:I = 0x7f0a0161

.field public static final commerce_error_invalid_card_expired:I = 0x7f0a0162

.field public static final commerce_error_invalid_card_last_four_digits:I = 0x7f0a0163

.field public static final commerce_error_invalid_card_month:I = 0x7f0a0164

.field public static final commerce_error_invalid_card_month_empty:I = 0x7f0a0165

.field public static final commerce_error_invalid_card_number_invalid:I = 0x7f0a0167

.field public static final commerce_error_invalid_card_payment_type:I = 0x7f0a0169

.field public static final commerce_error_invalid_card_type:I = 0x7f0a016a

.field public static final commerce_error_invalid_card_year:I = 0x7f0a016b

.field public static final commerce_error_invalid_card_year_empty:I = 0x7f0a016c

.field public static final commerce_error_invalid_currency:I = 0x7f0a016e

.field public static final commerce_error_invalid_email:I = 0x7f0a016f

.field public static final commerce_error_invalid_inventory:I = 0x7f0a0170

.field public static final commerce_error_invalid_parameters:I = 0x7f0a0171

.field public static final commerce_error_invalid_phone:I = 0x7f0a0172

.field public static final commerce_error_invalid_product:I = 0x7f0a0173

.field public static final commerce_error_invalid_profile:I = 0x7f0a0174

.field public static final commerce_error_network_error:I = 0x7f0a0175

.field public static final commerce_error_offer_already_saved:I = 0x7f0a0176

.field public static final commerce_error_offer_expired:I = 0x7f0a0177

.field public static final commerce_error_offer_inactive:I = 0x7f0a0178

.field public static final commerce_error_offer_not_found:I = 0x7f0a0179

.field public static final commerce_error_partner_error:I = 0x7f0a017a

.field public static final commerce_error_product_unavailable:I = 0x7f0a017b

.field public static final commerce_error_server_error:I = 0x7f0a017f

.field public static final commerce_error_service_unavailable:I = 0x7f0a0180

.field public static final commerce_error_unknown_error:I = 0x7f0a0183

.field public static final commerce_error_version_not_supported:I = 0x7f0a0185

.field public static final commerce_order_status_cancelled:I = 0x7f0a01bd

.field public static final commerce_order_status_processing:I = 0x7f0a01be

.field public static final commerce_order_status_sent_to_seller:I = 0x7f0a01bf

.field public static final common_google_play_services_enable_button:I = 0x7f0a0013

.field public static final common_google_play_services_enable_text:I = 0x7f0a0014

.field public static final common_google_play_services_enable_title:I = 0x7f0a0015

.field public static final common_google_play_services_install_button:I = 0x7f0a0016

.field public static final common_google_play_services_install_text_phone:I = 0x7f0a0017

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0a0018

.field public static final common_google_play_services_install_title:I = 0x7f0a0019

.field public static final common_google_play_services_notification_ticker:I = 0x7f0a001a

.field public static final common_google_play_services_unknown_issue:I = 0x7f0a001b

.field public static final common_google_play_services_unsupported_text:I = 0x7f0a001c

.field public static final common_google_play_services_unsupported_title:I = 0x7f0a001d

.field public static final common_google_play_services_update_button:I = 0x7f0a001e

.field public static final common_google_play_services_update_text:I = 0x7f0a001f

.field public static final common_google_play_services_update_title:I = 0x7f0a0020

.field public static final common_google_play_services_updating_text:I = 0x7f0a0021

.field public static final common_google_play_services_updating_title:I = 0x7f0a0022

.field public static final common_google_play_services_wear_update_text:I = 0x7f0a0023

.field public static final common_open_on_phone:I = 0x7f0a0024

.field public static final common_signin_button_text:I = 0x7f0a0025

.field public static final common_signin_button_text_long:I = 0x7f0a0026

.field public static final content_description_follow_button:I = 0x7f0a020f

.field public static final content_description_unfollow_button:I = 0x7f0a0210

.field public static final date_format_long:I = 0x7f0a0b71

.field public static final date_format_long_accessible:I = 0x7f0a0b9b

.field public static final date_format_short:I = 0x7f0a0b72

.field public static final date_format_short_accessible:I = 0x7f0a0b9c

.field public static final default_system_font_size_enabled:I = 0x7f0a0b9f

.field public static final default_user_value:I = 0x7f0a0261

.field public static final dm_added_you:I = 0x7f0a0280

.field public static final dm_added_you_to_a_group:I = 0x7f0a0281

.field public static final dm_conversation_title_many:I = 0x7f0a0285

.field public static final dm_conversation_title_three:I = 0x7f0a0286

.field public static final dm_conversation_title_two:I = 0x7f0a0287

.field public static final dm_group_name_changed:I = 0x7f0a029f

.field public static final dm_group_name_removed:I = 0x7f0a02a0

.field public static final dm_italicized_added_you:I = 0x7f0a02a5

.field public static final dm_italicized_cs_feedback_dismissed_text:I = 0x7f0a02a6

.field public static final dm_italicized_cs_feedback_submitted_text:I = 0x7f0a02a7

.field public static final dm_italicized_group_name_changed:I = 0x7f0a02a8

.field public static final dm_italicized_group_name_removed:I = 0x7f0a02a9

.field public static final dm_italicized_participant_added_by_deleted_user:I = 0x7f0a02aa

.field public static final dm_italicized_participant_added_by_user:I = 0x7f0a02ab

.field public static final dm_italicized_participant_added_by_you:I = 0x7f0a02ac

.field public static final dm_italicized_user_changed_group_name:I = 0x7f0a02ad

.field public static final dm_italicized_user_removed_group_name:I = 0x7f0a02ae

.field public static final dm_italicized_user_removed_group_photo:I = 0x7f0a02af

.field public static final dm_italicized_user_updated_group_photo:I = 0x7f0a02b0

.field public static final dm_italicized_you_changed_group_name:I = 0x7f0a02b1

.field public static final dm_italicized_you_removed_group_name:I = 0x7f0a02b2

.field public static final dm_italicized_you_removed_group_photo:I = 0x7f0a02b3

.field public static final dm_italicized_you_updated_group_photo:I = 0x7f0a02b4

.field public static final dm_participant_added_by_deleted_user:I = 0x7f0a02c0

.field public static final dm_participant_added_by_user:I = 0x7f0a02c1

.field public static final dm_participant_added_by_you:I = 0x7f0a02c2

.field public static final dm_participant_left_conversation:I = 0x7f0a02c3

.field public static final dm_sent_a_card:I = 0x7f0a02d4

.field public static final dm_sent_a_card_with_message:I = 0x7f0a02d5

.field public static final dm_sent_a_gif:I = 0x7f0a02d6

.field public static final dm_sent_a_gif_with_message:I = 0x7f0a02d7

.field public static final dm_sent_a_moment:I = 0x7f0a02d8

.field public static final dm_sent_a_moment_with_message:I = 0x7f0a02d9

.field public static final dm_sent_a_photo:I = 0x7f0a02da

.field public static final dm_sent_a_photo_with_message:I = 0x7f0a02db

.field public static final dm_sent_a_sticker:I = 0x7f0a02dc

.field public static final dm_sent_a_video:I = 0x7f0a02dd

.field public static final dm_sent_a_video_with_message:I = 0x7f0a02de

.field public static final dm_shared_someones_tweet:I = 0x7f0a02e7

.field public static final dm_shared_someones_tweet_in_a_group:I = 0x7f0a02e8

.field public static final dm_shared_someones_tweet_in_a_group_with_message:I = 0x7f0a02e9

.field public static final dm_shared_someones_tweet_with_message:I = 0x7f0a02ea

.field public static final dm_shared_tweet_with_you:I = 0x7f0a02eb

.field public static final dm_shared_tweet_with_you_with_message:I = 0x7f0a02ec

.field public static final dm_user_changed_group_name:I = 0x7f0a02f8

.field public static final dm_user_conversation_preview:I = 0x7f0a02f9

.field public static final dm_user_removed_group_name:I = 0x7f0a02fa

.field public static final dm_user_removed_group_photo:I = 0x7f0a02fb

.field public static final dm_user_sent_a_card:I = 0x7f0a02fc

.field public static final dm_user_sent_a_card_with_message:I = 0x7f0a02fd

.field public static final dm_user_sent_a_gif:I = 0x7f0a02fe

.field public static final dm_user_sent_a_gif_with_message:I = 0x7f0a02ff

.field public static final dm_user_sent_a_moment:I = 0x7f0a0300

.field public static final dm_user_sent_a_moment_with_message:I = 0x7f0a0301

.field public static final dm_user_sent_a_photo:I = 0x7f0a0302

.field public static final dm_user_sent_a_photo_with_message:I = 0x7f0a0303

.field public static final dm_user_sent_a_sticker:I = 0x7f0a0304

.field public static final dm_user_sent_a_video:I = 0x7f0a0305

.field public static final dm_user_sent_a_video_with_message:I = 0x7f0a0306

.field public static final dm_user_updated_group_photo:I = 0x7f0a0307

.field public static final dm_you_changed_group_name:I = 0x7f0a0309

.field public static final dm_you_removed_group_name:I = 0x7f0a030a

.field public static final dm_you_removed_group_photo:I = 0x7f0a030b

.field public static final dm_you_sent_a_card:I = 0x7f0a030c

.field public static final dm_you_sent_a_card_with_message:I = 0x7f0a030d

.field public static final dm_you_sent_a_gif:I = 0x7f0a030e

.field public static final dm_you_sent_a_gif_with_message:I = 0x7f0a030f

.field public static final dm_you_sent_a_moment:I = 0x7f0a0310

.field public static final dm_you_sent_a_moment_with_message:I = 0x7f0a0311

.field public static final dm_you_sent_a_photo:I = 0x7f0a0312

.field public static final dm_you_sent_a_photo_with_message:I = 0x7f0a0313

.field public static final dm_you_sent_a_sticker:I = 0x7f0a0314

.field public static final dm_you_sent_a_video:I = 0x7f0a0315

.field public static final dm_you_sent_a_video_with_message:I = 0x7f0a0316

.field public static final dm_you_shared_someones_tweet:I = 0x7f0a0317

.field public static final dm_you_shared_someones_tweet_with_message:I = 0x7f0a0318

.field public static final dm_you_shared_your_own_tweet:I = 0x7f0a0319

.field public static final dm_you_shared_your_own_tweet_with_message:I = 0x7f0a031a

.field public static final dm_you_updated_group_photo:I = 0x7f0a031b

.field public static final edit_birthdate_visibility_followers:I = 0x7f0a0342

.field public static final edit_birthdate_visibility_following:I = 0x7f0a0343

.field public static final edit_birthdate_visibility_mutualfollow:I = 0x7f0a0345

.field public static final edit_birthdate_visibility_public:I = 0x7f0a0346

.field public static final edit_birthdate_visibility_self:I = 0x7f0a0347

.field public static final education_in_this_conversation:I = 0x7f0a0361

.field public static final file_photo_name:I = 0x7f0a0ba5

.field public static final file_video_name:I = 0x7f0a0ba6

.field public static final foot_abbr:I = 0x7f0a0ba8

.field public static final gallery:I = 0x7f0a0ba9

.field public static final google_play_details_url_format:I = 0x7f0a0bc2

.field public static final google_play_web_details_url_format:I = 0x7f0a0bc3

.field public static final highlight_context_nearby:I = 0x7f0a03e2

.field public static final highlight_context_popular:I = 0x7f0a03e3

.field public static final icon_moderator:I = 0x7f0a0413

.field public static final icon_protected:I = 0x7f0a0414

.field public static final icon_translator:I = 0x7f0a0415

.field public static final icon_verified:I = 0x7f0a0416

.field public static final inline_dismiss_undo:I = 0x7f0a0420

.field public static final intent_chooser_title:I = 0x7f0a0422

.field public static final kilometer:I = 0x7f0a0434

.field public static final label_direct_message:I = 0x7f0a0435

.field public static final lifeline_alert:I = 0x7f0a0446

.field public static final list_share_link:I = 0x7f0a0bcd

.field public static final list_share_long_format:I = 0x7f0a048d

.field public static final list_share_short_format:I = 0x7f0a048e

.field public static final list_share_subject_long_format:I = 0x7f0a048f

.field public static final live_content_caret_content_description:I = 0x7f0a04a3

.field public static final live_video_geoblocked_playback_error_message:I = 0x7f0a04a7

.field public static final live_video_retry:I = 0x7f0a04b0

.field public static final media_error_audio_focus_rejected:I = 0x7f0a0513

.field public static final media_playback_error:I = 0x7f0a051c

.field public static final media_playback_error_debug:I = 0x7f0a051d

.field public static final media_tag_multiple_summary:I = 0x7f0a052b

.field public static final media_tag_two_summary:I = 0x7f0a0530

.field public static final media_tag_user_display_name:I = 0x7f0a0531

.field public static final media_tag_you:I = 0x7f0a0533

.field public static final media_tag_you_multiple_summary:I = 0x7f0a0534

.field public static final media_tag_you_two_summary:I = 0x7f0a0535

.field public static final media_type_gif:I = 0x7f0a0539

.field public static final media_type_video:I = 0x7f0a053a

.field public static final media_type_vine:I = 0x7f0a053b

.field public static final meter:I = 0x7f0a054d

.field public static final mile_abbr:I = 0x7f0a0bd6

.field public static final modern_nav_drawer_icon:I = 0x7f0a054e

.field public static final more_videos:I = 0x7f0a059e

.field public static final now:I = 0x7f0a0611

.field public static final pause:I = 0x7f0a0633

.field public static final pinned_tweet:I = 0x7f0a069c

.field public static final place_autocomplete_clear_button:I = 0x7f0a0027

.field public static final place_autocomplete_search_hint:I = 0x7f0a0028

.field public static final play:I = 0x7f0a06a1

.field public static final possibly_sensitive_allow:I = 0x7f0a06b3

.field public static final possibly_sensitive_always_allow:I = 0x7f0a06b4

.field public static final possibly_sensitive_appeal:I = 0x7f0a06b5

.field public static final possibly_sensitive_appeal_toast:I = 0x7f0a06b6

.field public static final possibly_sensitive_message:I = 0x7f0a06b7

.field public static final possibly_sensitive_title:I = 0x7f0a06b8

.field public static final preference_notification_error:I = 0x7f0a06c3

.field public static final preference_notification_success:I = 0x7f0a06c4

.field public static final promoted:I = 0x7f0a06f3

.field public static final promoted_by:I = 0x7f0a06f4

.field public static final promoted_without_advertiser:I = 0x7f0a06fb

.field public static final ps__abbrev_not_applicable:I = 0x7f0a0a45

.field public static final ps__action_autodelete_confirmation_title:I = 0x7f0a0a46

.field public static final ps__action_change_expiration:I = 0x7f0a0a47

.field public static final ps__action_change_expiration_subtext:I = 0x7f0a0a48

.field public static final ps__action_delete_broadcast_now:I = 0x7f0a0a49

.field public static final ps__action_dont_auto_delete:I = 0x7f0a0a4a

.field public static final ps__action_sheet_chat_reply:I = 0x7f0a0a4b

.field public static final ps__action_sheet_delete_broadcast:I = 0x7f0a0a4c

.field public static final ps__action_sheet_hide_chat:I = 0x7f0a0a4d

.field public static final ps__action_sheet_label_block:I = 0x7f0a0a4e

.field public static final ps__action_sheet_label_report:I = 0x7f0a0a4f

.field public static final ps__action_sheet_label_view_profile:I = 0x7f0a0a50

.field public static final ps__action_sheet_open_twitter_profile:I = 0x7f0a0a51

.field public static final ps__action_sheet_report_abuse:I = 0x7f0a0a52

.field public static final ps__action_sheet_report_other:I = 0x7f0a0a53

.field public static final ps__action_sheet_report_other_example:I = 0x7f0a0a54

.field public static final ps__action_sheet_report_snippet:I = 0x7f0a0a55

.field public static final ps__action_sheet_report_spam:I = 0x7f0a0a56

.field public static final ps__action_sheet_save_to_gallery:I = 0x7f0a0a57

.field public static final ps__action_sheet_saved_to_gallery:I = 0x7f0a0a58

.field public static final ps__action_sheet_show_chat:I = 0x7f0a0a59

.field public static final ps__app_name:I = 0x7f0a0a5a

.field public static final ps__audience_selection_create_channel_with:I = 0x7f0a0a5b

.field public static final ps__audience_selection_current_audience:I = 0x7f0a0a5c

.field public static final ps__audience_selection_done:I = 0x7f0a0a5d

.field public static final ps__audience_selection_header:I = 0x7f0a0a5e

.field public static final ps__audience_selection_public_audience:I = 0x7f0a0a5f

.field public static final ps__audience_selection_search_mutual_followers:I = 0x7f0a0a60

.field public static final ps__audience_selection_tap_to_change_audience:I = 0x7f0a0a61

.field public static final ps__audio_noisy:I = 0x7f0a0a62

.field public static final ps__bitrate_too_low:I = 0x7f0a0a63

.field public static final ps__bitrate_undefined:I = 0x7f0a0a64

.field public static final ps__block_dialog_btn_confirm:I = 0x7f0a0a65

.field public static final ps__block_limit_error:I = 0x7f0a0a66

.field public static final ps__block_unblock_dialog_btn_cancel:I = 0x7f0a0a67

.field public static final ps__blocked:I = 0x7f0a0a68

.field public static final ps__blocked_user:I = 0x7f0a0a69

.field public static final ps__branch_api_key:I = 0x7f0a0bed

.field public static final ps__broadcast_default_title_live:I = 0x7f0a0bee

.field public static final ps__broadcast_default_title_replay:I = 0x7f0a0bef

.field public static final ps__broadcast_following_chat_off:I = 0x7f0a0a6a

.field public static final ps__broadcast_following_chat_on:I = 0x7f0a0a6b

.field public static final ps__broadcast_limited:I = 0x7f0a0a6c

.field public static final ps__broadcast_limited_dialog_message:I = 0x7f0a0a6d

.field public static final ps__broadcast_limited_dialog_title:I = 0x7f0a0a6e

.field public static final ps__broadcast_live_label:I = 0x7f0a0a6f

.field public static final ps__broadcast_location_off:I = 0x7f0a0a70

.field public static final ps__broadcast_location_on:I = 0x7f0a0a71

.field public static final ps__broadcast_private:I = 0x7f0a0a72

.field public static final ps__broadcast_private_count:I = 0x7f0a0a73

.field public static final ps__broadcast_public:I = 0x7f0a0a74

.field public static final ps__broadcast_shared_by:I = 0x7f0a0a75

.field public static final ps__broadcast_title_hint:I = 0x7f0a0bf0

.field public static final ps__broadcast_to_channel:I = 0x7f0a0bf1

.field public static final ps__broadcast_too_full:I = 0x7f0a0a76

.field public static final ps__broadcast_too_full_dialog_message:I = 0x7f0a0a77

.field public static final ps__broadcast_too_full_dialog_title:I = 0x7f0a0a78

.field public static final ps__broadcast_twitter_disabled:I = 0x7f0a0a79

.field public static final ps__broadcast_twitter_post_off:I = 0x7f0a0a7a

.field public static final ps__broadcast_twitter_post_on:I = 0x7f0a0a7b

.field public static final ps__broadcaster:I = 0x7f0a0a7c

.field public static final ps__broadcaster_action_ask_for_follow:I = 0x7f0a0bf2

.field public static final ps__broadcaster_action_ask_for_follow_confirmation:I = 0x7f0a0bf3

.field public static final ps__broadcaster_action_ask_for_share:I = 0x7f0a0bf4

.field public static final ps__broadcaster_action_ask_for_share_confirmation:I = 0x7f0a0bf5

.field public static final ps__broadcaster_audio_muted:I = 0x7f0a0a7d

.field public static final ps__broadcaster_kick_block:I = 0x7f0a0a7e

.field public static final ps__broadcaster_kicked_me:I = 0x7f0a0a7f

.field public static final ps__broadcaster_kicked_me_detailed:I = 0x7f0a0a80

.field public static final ps__broadcaster_kicked_me_explanation:I = 0x7f0a0a81

.field public static final ps__broadcaster_unmute:I = 0x7f0a0a82

.field public static final ps__broadcasting_live:I = 0x7f0a0a83

.field public static final ps__btn_go_live:I = 0x7f0a0a84

.field public static final ps__btn_stop_broadcast:I = 0x7f0a0a85

.field public static final ps__camera_double_tap:I = 0x7f0a0a86

.field public static final ps__camera_swipe_down:I = 0x7f0a0a87

.field public static final ps__cannot_play:I = 0x7f0a0a88

.field public static final ps__channels_action_change_name:I = 0x7f0a0a89

.field public static final ps__channels_action_create:I = 0x7f0a0a8a

.field public static final ps__channels_action_create_anon:I = 0x7f0a0a8b

.field public static final ps__channels_action_delete:I = 0x7f0a0a8c

.field public static final ps__channels_action_leave:I = 0x7f0a0a8d

.field public static final ps__channels_action_member_add:I = 0x7f0a0a8e

.field public static final ps__channels_action_member_add_anon:I = 0x7f0a0a8f

.field public static final ps__channels_action_member_remove:I = 0x7f0a0a90

.field public static final ps__channels_action_member_remove_anon:I = 0x7f0a0a91

.field public static final ps__channels_action_mute_notifications:I = 0x7f0a0a92

.field public static final ps__channels_action_rename:I = 0x7f0a0a93

.field public static final ps__channels_action_rename_anon:I = 0x7f0a0a94

.field public static final ps__channels_action_unmute_notifications:I = 0x7f0a0a95

.field public static final ps__channels_action_view_history:I = 0x7f0a0bf6

.field public static final ps__channels_add_members:I = 0x7f0a0a96

.field public static final ps__channels_add_members_count:I = 0x7f0a0a97

.field public static final ps__channels_add_members_description:I = 0x7f0a0a98

.field public static final ps__channels_create:I = 0x7f0a0a99

.field public static final ps__channels_create_private_channel_description:I = 0x7f0a0bf7

.field public static final ps__channels_delete_warning_message:I = 0x7f0a0a9a

.field public static final ps__channels_delete_warning_ok:I = 0x7f0a0a9b

.field public static final ps__channels_delete_warning_title:I = 0x7f0a0a9c

.field public static final ps__channels_history:I = 0x7f0a0bf8

.field public static final ps__channels_invalid_name:I = 0x7f0a0a9d

.field public static final ps__channels_leave_warning_message:I = 0x7f0a0a9e

.field public static final ps__channels_leave_warning_ok:I = 0x7f0a0a9f

.field public static final ps__channels_leave_warning_title:I = 0x7f0a0aa0

.field public static final ps__channels_manage_private_channel_description:I = 0x7f0a0bf9

.field public static final ps__channels_members:I = 0x7f0a0aa1

.field public static final ps__channels_name_error:I = 0x7f0a0aa2

.field public static final ps__channels_name_hint:I = 0x7f0a0aa3

.field public static final ps__channels_private_divider:I = 0x7f0a0aa4

.field public static final ps__channels_public_divider:I = 0x7f0a0aa5

.field public static final ps__channels_remove_member_dialog_ok:I = 0x7f0a0aa6

.field public static final ps__channels_remove_member_dialog_title:I = 0x7f0a0aa7

.field public static final ps__channels_update_name_dialog_ok:I = 0x7f0a0aa8

.field public static final ps__chat_ask_for_share:I = 0x7f0a0bfa

.field public static final ps__chat_join:I = 0x7f0a0aa9

.field public static final ps__chat_join_new_user:I = 0x7f0a0bfb

.field public static final ps__chat_prompt_follow_broadcaster:I = 0x7f0a0aaa

.field public static final ps__chat_share_screenshot_twitter:I = 0x7f0a0aab

.field public static final ps__chat_status_moderation_disabled:I = 0x7f0a0aac

.field public static final ps__chat_status_moderation_disabled_global:I = 0x7f0a0aad

.field public static final ps__chat_status_moderation_limited:I = 0x7f0a0aae

.field public static final ps__chat_warning_dialog_btn_cancel:I = 0x7f0a0aaf

.field public static final ps__chat_warning_dialog_btn_send:I = 0x7f0a0ab0

.field public static final ps__chat_warning_dialog_message:I = 0x7f0a0ab1

.field public static final ps__chat_warning_dialog_title:I = 0x7f0a0ab2

.field public static final ps__comment_hint:I = 0x7f0a0ab3

.field public static final ps__connecting:I = 0x7f0a0ab4

.field public static final ps__connection_error:I = 0x7f0a0ab5

.field public static final ps__convicted_abuse:I = 0x7f0a0ab6

.field public static final ps__cta_app_name:I = 0x7f0a0ab7

.field public static final ps__cta_install_app:I = 0x7f0a0ab8

.field public static final ps__cta_open_app:I = 0x7f0a0ab9

.field public static final ps__cta_view_broadcasts:I = 0x7f0a0aba

.field public static final ps__days:I = 0x7f0a0abb

.field public static final ps__delete_broadcast_cancel:I = 0x7f0a0abc

.field public static final ps__delete_broadcast_confirm:I = 0x7f0a0abd

.field public static final ps__delete_broadcast_dialog:I = 0x7f0a0abe

.field public static final ps__delete_broadcast_error:I = 0x7f0a0abf

.field public static final ps__delete_broadcast_success:I = 0x7f0a0ac0

.field public static final ps__dialog_btn_cancel_end_broadcast:I = 0x7f0a0ac1

.field public static final ps__dialog_btn_confirm_end_broadcast:I = 0x7f0a0ac2

.field public static final ps__dialog_btn_no:I = 0x7f0a0ac3

.field public static final ps__dialog_btn_yes:I = 0x7f0a0ac4

.field public static final ps__dialog_message_cancel_broadcast:I = 0x7f0a0ac5

.field public static final ps__dialog_message_end_broadcast:I = 0x7f0a0ac6

.field public static final ps__dialog_moderator_learn_more_body:I = 0x7f0a0bfc

.field public static final ps__dialog_moderator_learn_more_button:I = 0x7f0a0ac7

.field public static final ps__dialog_moderator_learn_more_disable:I = 0x7f0a0ac8

.field public static final ps__dialog_moderator_learn_more_example:I = 0x7f0a0ac9

.field public static final ps__dialog_moderator_learn_more_message_body:I = 0x7f0a0aca

.field public static final ps__dialog_moderator_learn_more_title:I = 0x7f0a0acb

.field public static final ps__download_periscope:I = 0x7f0a0acc

.field public static final ps__duration:I = 0x7f0a0acd

.field public static final ps__ellipsis:I = 0x7f0a0ace

.field public static final ps__end_broadcast:I = 0x7f0a0acf

.field public static final ps__ended_broadcast:I = 0x7f0a0ad0

.field public static final ps__failed_to_mark_broadcast_persistent:I = 0x7f0a0ad1

.field public static final ps__featured:I = 0x7f0a0ad2

.field public static final ps__featured_digits:I = 0x7f0a0ad3

.field public static final ps__featured_most_loved:I = 0x7f0a0ad4

.field public static final ps__featured_popular:I = 0x7f0a0ad5

.field public static final ps__featured_twitter:I = 0x7f0a0ad6

.field public static final ps__featured_users:I = 0x7f0a0ad7

.field public static final ps__follow_action:I = 0x7f0a0bfd

.field public static final ps__follow_limit_error:I = 0x7f0a0ad8

.field public static final ps__followers:I = 0x7f0a0ad9

.field public static final ps__following:I = 0x7f0a0ada

.field public static final ps__following_action:I = 0x7f0a0bfe

.field public static final ps__four_following_in_chat:I = 0x7f0a0adb

.field public static final ps__four_plus_following_in_chat:I = 0x7f0a0adc

.field public static final ps__help_moderate_content:I = 0x7f0a0add

.field public static final ps__highlights_btn_play:I = 0x7f0a0ade

.field public static final ps__hours:I = 0x7f0a0adf

.field public static final ps__initializing:I = 0x7f0a0ae0

.field public static final ps__install_description:I = 0x7f0a0ae1

.field public static final ps__install_no_thanks:I = 0x7f0a0ae2

.field public static final ps__interstitial_tos:I = 0x7f0a0ae3

.field public static final ps__invited_followers:I = 0x7f0a0ae4

.field public static final ps__live:I = 0x7f0a0ae5

.field public static final ps__loading:I = 0x7f0a0ae6

.field public static final ps__local_prompt_conviction_broadcast_disabled:I = 0x7f0a0ae7

.field public static final ps__local_prompt_conviction_broadcast_disabled_with_body_and_reason:I = 0x7f0a0ae8

.field public static final ps__local_prompt_conviction_broadcast_disabled_with_reason:I = 0x7f0a0ae9

.field public static final ps__local_prompt_conviction_broadcast_suspended:I = 0x7f0a0aea

.field public static final ps__local_prompt_conviction_broadcast_suspended_with_body_and_reason:I = 0x7f0a0aeb

.field public static final ps__local_prompt_conviction_broadcast_suspended_with_reason:I = 0x7f0a0aec

.field public static final ps__local_prompt_conviction_global_disabled:I = 0x7f0a0aed

.field public static final ps__local_prompt_conviction_global_disabled_with_body_and_reason:I = 0x7f0a0aee

.field public static final ps__local_prompt_conviction_global_disabled_with_reason:I = 0x7f0a0aef

.field public static final ps__local_prompt_conviction_global_suspended:I = 0x7f0a0af0

.field public static final ps__local_prompt_conviction_global_suspended_with_body_and_reason:I = 0x7f0a0af1

.field public static final ps__local_prompt_conviction_global_suspended_with_reason:I = 0x7f0a0af2

.field public static final ps__local_prompt_moderator_feedback:I = 0x7f0a0af3

.field public static final ps__location_settings_off:I = 0x7f0a0af4

.field public static final ps__minutes:I = 0x7f0a0af5

.field public static final ps__moderate_wait_for_responses:I = 0x7f0a0af6

.field public static final ps__moderator_did_not_vote:I = 0x7f0a0af7

.field public static final ps__moderator_learn_more:I = 0x7f0a0af8

.field public static final ps__moderator_negative:I = 0x7f0a0af9

.field public static final ps__moderator_negative_spam:I = 0x7f0a0afa

.field public static final ps__moderator_neutral:I = 0x7f0a0afb

.field public static final ps__moderator_positive:I = 0x7f0a0afc

.field public static final ps__moderator_verdict:I = 0x7f0a0afd

.field public static final ps__moderator_verdict_consequence:I = 0x7f0a0afe

.field public static final ps__months:I = 0x7f0a0aff

.field public static final ps__more_viewers:I = 0x7f0a0b00

.field public static final ps__mutual_follow:I = 0x7f0a0b01

.field public static final ps__no_replay_viewers:I = 0x7f0a0b02

.field public static final ps__no_viewers:I = 0x7f0a0b03

.field public static final ps__now:I = 0x7f0a0b04

.field public static final ps__num_hearts:I = 0x7f0a0b05

.field public static final ps__number_format_million_grouping:I = 0x7f0a0b06

.field public static final ps__number_format_millions:I = 0x7f0a0b07

.field public static final ps__number_format_thousand_grouping:I = 0x7f0a0b08

.field public static final ps__number_format_thousands:I = 0x7f0a0b09

.field public static final ps__one_following_in_chat:I = 0x7f0a0b0a

.field public static final ps__other_languages:I = 0x7f0a0b0b

.field public static final ps__peak_value:I = 0x7f0a0b0c

.field public static final ps__permissions_broadcaster:I = 0x7f0a0b0d

.field public static final ps__permissions_btn_allow:I = 0x7f0a0b0e

.field public static final ps__permissions_btn_settings:I = 0x7f0a0b0f

.field public static final ps__permissions_required:I = 0x7f0a0b10

.field public static final ps__permissions_screenshots:I = 0x7f0a0b11

.field public static final ps__placeholder_for_value:I = 0x7f0a0b12

.field public static final ps__post_broadcast_twitter:I = 0x7f0a0b13

.field public static final ps__posted_on_twitter:I = 0x7f0a0b14

.field public static final ps__pp_url:I = 0x7f0a0bff

.field public static final ps__private_broadcast_count:I = 0x7f0a0b15

.field public static final ps__private_broadcast_description:I = 0x7f0a0b16

.field public static final ps__private_broadcast_dialog:I = 0x7f0a0b17

.field public static final ps__private_broadcast_title:I = 0x7f0a0b18

.field public static final ps__profile_copy_url:I = 0x7f0a0b19

.field public static final ps__profile_copy_url_copied:I = 0x7f0a0b1a

.field public static final ps__profile_sheet_more_options_block:I = 0x7f0a0b1b

.field public static final ps__profile_sheet_more_options_mute:I = 0x7f0a0c00

.field public static final ps__profile_sheet_more_options_unblock:I = 0x7f0a0b1c

.field public static final ps__profile_sheet_more_options_unfollow:I = 0x7f0a0c01

.field public static final ps__profile_viewer_not_found:I = 0x7f0a0b1d

.field public static final ps__rate_limited:I = 0x7f0a0b1e

.field public static final ps__replay_scrub_hint_fine_control:I = 0x7f0a0b1f

.field public static final ps__replay_scrub_hint_seek:I = 0x7f0a0b20

.field public static final ps__replay_scrub_zoom_fine:I = 0x7f0a0b21

.field public static final ps__replay_scrub_zoom_half:I = 0x7f0a0b22

.field public static final ps__replay_scrub_zoom_quarter:I = 0x7f0a0b23

.field public static final ps__replay_skip_tip:I = 0x7f0a0b24

.field public static final ps__report_broadcast_action:I = 0x7f0a0b25

.field public static final ps__report_broadcast_confirm:I = 0x7f0a0b26

.field public static final ps__report_broadcast_dialog_dont_like_message:I = 0x7f0a0b27

.field public static final ps__report_broadcast_dialog_dont_like_message_extra:I = 0x7f0a0b28

.field public static final ps__report_broadcast_dialog_feedback_message:I = 0x7f0a0b29

.field public static final ps__report_broadcast_dialog_feedback_title:I = 0x7f0a0b2a

.field public static final ps__report_broadcast_dialog_self_harm_message:I = 0x7f0a0b2b

.field public static final ps__report_broadcast_dialog_self_harm_title:I = 0x7f0a0b2c

.field public static final ps__report_broadcast_dialog_sexual_content_message:I = 0x7f0a0b2d

.field public static final ps__report_broadcast_dialog_sexual_content_title:I = 0x7f0a0b2e

.field public static final ps__report_broadcast_dialog_violence_message:I = 0x7f0a0b2f

.field public static final ps__report_broadcast_dialog_violence_title:I = 0x7f0a0b30

.field public static final ps__report_broadcast_error:I = 0x7f0a0b31

.field public static final ps__report_broadcast_reason_dont_like:I = 0x7f0a0b32

.field public static final ps__report_broadcast_reason_self_harm:I = 0x7f0a0b33

.field public static final ps__report_broadcast_reason_sexual_content:I = 0x7f0a0b34

.field public static final ps__report_broadcast_reason_snippet:I = 0x7f0a0b35

.field public static final ps__report_broadcast_reason_violence:I = 0x7f0a0b36

.field public static final ps__report_broadcast_success:I = 0x7f0a0b37

.field public static final ps__retweet_broadcast_action:I = 0x7f0a0b38

.field public static final ps__retweeted_on_twitter:I = 0x7f0a0b39

.field public static final ps__save_broadcast_unsuccessful:I = 0x7f0a0b3a

.field public static final ps__search_people_hint:I = 0x7f0a0b3b

.field public static final ps__seconds:I = 0x7f0a0b3c

.field public static final ps__select_all:I = 0x7f0a0b3d

.field public static final ps__settings:I = 0x7f0a0b3e

.field public static final ps__share_broadcast_action:I = 0x7f0a0b3f

.field public static final ps__share_broadcast_all_followers:I = 0x7f0a0b40

.field public static final ps__share_broadcast_copy_link:I = 0x7f0a0b41

.field public static final ps__share_broadcast_copy_link_copied:I = 0x7f0a0b42

.field public static final ps__share_broadcast_copy_link_copied_fail:I = 0x7f0a0b43

.field public static final ps__share_broadcast_count:I = 0x7f0a0b44

.field public static final ps__share_broadcast_description:I = 0x7f0a0b45

.field public static final ps__share_broadcast_failed:I = 0x7f0a0b46

.field public static final ps__share_broadcast_other_app:I = 0x7f0a0b47

.field public static final ps__share_broadcast_subset_followers:I = 0x7f0a0b48

.field public static final ps__share_broadcast_title:I = 0x7f0a0b49

.field public static final ps__share_post_tweet:I = 0x7f0a0b4a

.field public static final ps__share_post_tweet_fail:I = 0x7f0a0b4b

.field public static final ps__share_post_tweet_success:I = 0x7f0a0b4c

.field public static final ps__share_post_tweet_text:I = 0x7f0a0b4d

.field public static final ps__share_post_tweet_text_username:I = 0x7f0a0b4e

.field public static final ps__share_post_tweeting:I = 0x7f0a0b4f

.field public static final ps__show_stats:I = 0x7f0a0b50

.field public static final ps__start_broadcast_error:I = 0x7f0a0b51

.field public static final ps__starting_broadcast:I = 0x7f0a0b52

.field public static final ps__stat_live_viewer_only:I = 0x7f0a0b53

.field public static final ps__stat_live_viewers:I = 0x7f0a0b54

.field public static final ps__stat_replay_viewer_only:I = 0x7f0a0b55

.field public static final ps__stat_replay_viewers:I = 0x7f0a0b56

.field public static final ps__stat_total_viewers:I = 0x7f0a0b57

.field public static final ps__store_graph_to_disk_error:I = 0x7f0a0b58

.field public static final ps__superfan_of_action_sheet_text:I = 0x7f0a0c02

.field public static final ps__superfans_title:I = 0x7f0a0c03

.field public static final ps__three_following_in_chat:I = 0x7f0a0b59

.field public static final ps__time_per_viewer:I = 0x7f0a0b5a

.field public static final ps__time_per_viewer_live:I = 0x7f0a0b5b

.field public static final ps__time_per_viewer_replay:I = 0x7f0a0b5c

.field public static final ps__tip_broadcaster_1:I = 0x7f0a0c04

.field public static final ps__tip_broadcaster_11:I = 0x7f0a0c05

.field public static final ps__tip_broadcaster_2:I = 0x7f0a0c06

.field public static final ps__tip_broadcaster_3:I = 0x7f0a0c07

.field public static final ps__tip_broadcaster_4:I = 0x7f0a0c08

.field public static final ps__tip_broadcaster_5:I = 0x7f0a0c09

.field public static final ps__tip_broadcaster_6:I = 0x7f0a0c0a

.field public static final ps__tip_broadcaster_7:I = 0x7f0a0c0b

.field public static final ps__tip_broadcaster_8:I = 0x7f0a0c0c

.field public static final ps__tip_broadcaster_9:I = 0x7f0a0c0d

.field public static final ps__tip_pre_broadcast_1:I = 0x7f0a0c0e

.field public static final ps__tip_pre_broadcast_2:I = 0x7f0a0c0f

.field public static final ps__tip_pre_broadcast_3:I = 0x7f0a0c10

.field public static final ps__tip_pre_broadcast_4:I = 0x7f0a0c11

.field public static final ps__tip_pre_broadcast_5:I = 0x7f0a0c12

.field public static final ps__tip_pre_broadcast_6:I = 0x7f0a0c13

.field public static final ps__tip_pre_broadcast_8:I = 0x7f0a0c14

.field public static final ps__tip_pre_broadcast_9:I = 0x7f0a0c15

.field public static final ps__tip_viewer_1:I = 0x7f0a0c16

.field public static final ps__tip_viewer_2:I = 0x7f0a0c17

.field public static final ps__tip_viewer_3:I = 0x7f0a0c18

.field public static final ps__tip_viewer_4:I = 0x7f0a0c19

.field public static final ps__token:I = 0x7f0a0b5d

.field public static final ps__tos_url:I = 0x7f0a0c1a

.field public static final ps__total_time_watched:I = 0x7f0a0b5e

.field public static final ps__total_time_watched_live:I = 0x7f0a0b5f

.field public static final ps__total_time_watched_replay:I = 0x7f0a0b60

.field public static final ps__trying_to_reconnect:I = 0x7f0a0b61

.field public static final ps__tweet_broadcast:I = 0x7f0a0b62

.field public static final ps__tweet_broadcast_detailed:I = 0x7f0a0b63

.field public static final ps__tweet_broadcast_failed:I = 0x7f0a0b64

.field public static final ps__two_following_in_chat:I = 0x7f0a0b65

.field public static final ps__unblock_dialog_btn_confirm:I = 0x7f0a0b66

.field public static final ps__unblock_dialog_title:I = 0x7f0a0b67

.field public static final ps__username_format:I = 0x7f0a0b68

.field public static final ps__vip_badge_bronze:I = 0x7f0a0b69

.field public static final ps__vip_badge_description:I = 0x7f0a0b6a

.field public static final ps__vip_badge_gold:I = 0x7f0a0b6b

.field public static final ps__vip_badge_learn_more:I = 0x7f0a0b6c

.field public static final ps__vip_badge_silver:I = 0x7f0a0b6d

.field public static final ps__watch_live:I = 0x7f0a0b6e

.field public static final ps__weeks:I = 0x7f0a0b6f

.field public static final ps__years:I = 0x7f0a0b70

.field public static final quote_tweet_interstitial_text:I = 0x7f0a0737

.field public static final read_more_button:I = 0x7f0a073f

.field public static final recent_tweets_header_title:I = 0x7f0a074a

.field public static final replay:I = 0x7f0a075c

.field public static final reply_context_to_someone:I = 0x7f0a075d

.field public static final reply_context_to_someone_and_someone:I = 0x7f0a075e

.field public static final reply_context_to_someone_and_you:I = 0x7f0a075f

.field public static final reply_context_to_someone_you_and_someone:I = 0x7f0a0760

.field public static final reply_context_to_you:I = 0x7f0a0761

.field public static final reply_context_to_you_and_someone:I = 0x7f0a0762

.field public static final search_hashtag_share_link:I = 0x7f0a0c25

.field public static final search_menu_title:I = 0x7f0a0029

.field public static final search_share_link:I = 0x7f0a0c26

.field public static final search_share_long_format:I = 0x7f0a07e6

.field public static final search_share_short_format:I = 0x7f0a07e7

.field public static final search_share_subject_long_format:I = 0x7f0a07e8

.field public static final settings:I = 0x7f0a0800

.field public static final settings_enabled:I = 0x7f0a0831

.field public static final skip:I = 0x7f0a08d6

.field public static final social_both_follow:I = 0x7f0a08eb

.field public static final social_both_followed_by:I = 0x7f0a08ec

.field public static final social_context_mutual_follow:I = 0x7f0a08ed

.field public static final social_conversation_tweet:I = 0x7f0a08ee

.field public static final social_conversation_tweet_two:I = 0x7f0a08ef

.field public static final social_follow_and_follow:I = 0x7f0a08f0

.field public static final social_follow_and_follow_accessibility_description:I = 0x7f0a08f1

.field public static final social_follow_and_follow_with_two_users:I = 0x7f0a08f2

.field public static final social_follow_and_like:I = 0x7f0a08f3

.field public static final social_follow_and_like_accessibility_description:I = 0x7f0a08f4

.field public static final social_follow_and_more_follow:I = 0x7f0a08f5

.field public static final social_follow_and_reply:I = 0x7f0a08f6

.field public static final social_follow_and_reply_accessibility_description:I = 0x7f0a08f7

.field public static final social_follower_and_like:I = 0x7f0a08f8

.field public static final social_follower_and_like_accessibility_description:I = 0x7f0a08f9

.field public static final social_follower_and_reply:I = 0x7f0a08fa

.field public static final social_follower_and_reply_accessibility_description:I = 0x7f0a08fb

.field public static final social_follower_and_retweets:I = 0x7f0a08fc

.field public static final social_follower_and_retweets_accessibility_description:I = 0x7f0a08fd

.field public static final social_follower_of_follower:I = 0x7f0a08fe

.field public static final social_following:I = 0x7f0a08ff

.field public static final social_follows_you:I = 0x7f0a0900

.field public static final social_like_with_two_user:I = 0x7f0a0901

.field public static final social_like_with_two_user_accessibility_description:I = 0x7f0a0902

.field public static final social_like_with_user:I = 0x7f0a0903

.field public static final social_like_with_user_accessibility_description:I = 0x7f0a0904

.field public static final social_promoted_trend:I = 0x7f0a0905

.field public static final social_provides_support:I = 0x7f0a0906

.field public static final social_reply_to_follow:I = 0x7f0a0907

.field public static final social_reply_to_follower:I = 0x7f0a0908

.field public static final social_retweet_and_like_count:I = 0x7f0a0909

.field public static final social_retweet_with_user:I = 0x7f0a090a

.field public static final social_retweet_with_user_accessibility_description:I = 0x7f0a090b

.field public static final social_top_news:I = 0x7f0a090d

.field public static final social_trending_topic:I = 0x7f0a090e

.field public static final social_who_to_follow:I = 0x7f0a090f

.field public static final social_you_retweeted:I = 0x7f0a0910

.field public static final status_bar_notification_info_overflow:I = 0x7f0a002a

.field public static final stop:I = 0x7f0a0923

.field public static final string_shortening:I = 0x7f0a0934

.field public static final tagline_location:I = 0x7f0a094f

.field public static final tagline_location_poi:I = 0x7f0a0950

.field public static final tagline_people:I = 0x7f0a0951

.field public static final tagline_people_location:I = 0x7f0a0952

.field public static final tagline_people_location_poi:I = 0x7f0a0953

.field public static final tagline_separator:I = 0x7f0a0954

.field public static final tagline_views_separator:I = 0x7f0a0955

.field public static final time_of_day_format:I = 0x7f0a0c37

.field public static final timeline_share_long_format:I = 0x7f0a0959

.field public static final timeline_share_short_format:I = 0x7f0a095a

.field public static final timeline_share_subject_long_format:I = 0x7f0a095b

.field public static final timeline_tweet_format:I = 0x7f0a095c

.field public static final timeline_tweet_media_format:I = 0x7f0a095d

.field public static final top_tweet:I = 0x7f0a0966

.field public static final tweet_card_format:I = 0x7f0a0980

.field public static final tweet_date_format:I = 0x7f0a0c3a

.field public static final tweet_media_image_description:I = 0x7f0a0988

.field public static final tweet_share_link:I = 0x7f0a0c3b

.field public static final tweets_retweeted:I = 0x7f0a099e

.field public static final tweets_retweeted_accessibility_description:I = 0x7f0a099f

.field public static final tweets_share_long_format:I = 0x7f0a09a0

.field public static final tweets_share_short_format:I = 0x7f0a09a1

.field public static final tweets_share_status:I = 0x7f0a09a2

.field public static final tweets_share_subject_long_format:I = 0x7f0a09a3

.field public static final twitter_authority:I = 0x7f0a0c3e

.field public static final twitter_edit_text_counter_format_countdown:I = 0x7f0a0c3f

.field public static final twitter_edit_text_counter_format_normal:I = 0x7f0a0c40

.field public static final twitter_support_authority:I = 0x7f0a0c43

.field public static final unsupported_feature:I = 0x7f0a09c3

.field public static final user_share_link:I = 0x7f0a0c4b

.field public static final user_share_long_format:I = 0x7f0a09d0

.field public static final user_share_short_format:I = 0x7f0a09d1

.field public static final user_share_subject_long_format:I = 0x7f0a09d2

.field public static final verified_accounts_faqs:I = 0x7f0a0c4c

.field public static final video_cta_default_text:I = 0x7f0a0a12

.field public static final video_cta_download_app:I = 0x7f0a0a13

.field public static final video_cta_download_app_already_installed:I = 0x7f0a0a14

.field public static final video_cta_open_url:I = 0x7f0a0a15

.field public static final video_cta_watch_now:I = 0x7f0a0a16

.field public static final video_duration:I = 0x7f0a0a17

.field public static final video_playback_error:I = 0x7f0a0a1b
