.class public Lbor;
.super Lbou;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbou",
        "<",
        "Lcom/twitter/model/dms/v;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;JZZ)V
    .locals 0

    .prologue
    .line 17
    invoke-direct/range {p0 .. p5}, Lbou;-><init>(Landroid/database/sqlite/SQLiteDatabase;JZZ)V

    .line 18
    return-void
.end method


# virtual methods
.method bridge synthetic a(Lcom/twitter/model/dms/d;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Lcom/twitter/model/dms/v;

    invoke-virtual {p0, p1}, Lbor;->a(Lcom/twitter/model/dms/v;)V

    return-void
.end method

.method a(Lcom/twitter/model/dms/v;)V
    .locals 4

    .prologue
    .line 22
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/twitter/model/dms/v;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 24
    iget-object v1, p0, Lbor;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v2, "conversations"

    sget-object v3, Lawy;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 26
    iget-object v1, p0, Lbor;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v2, "conversation_entries"

    const-string/jumbo v3, "conversation_id=?"

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 28
    iget-object v1, p0, Lbor;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v2, "conversation_participants"

    const-string/jumbo v3, "conversation_id=?"

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 30
    return-void
.end method
