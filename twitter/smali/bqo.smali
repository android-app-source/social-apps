.class public Lbqo;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbqy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbqo$a;
    }
.end annotation


# static fields
.field private static a:Lbqo;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Lbqn;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private final f:Landroid/os/Handler;

.field private g:Landroid/location/Location;

.field private h:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lbqo;->e:I

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbqo;->b:Landroid/content/Context;

    .line 93
    new-instance v0, Lbqo$a;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lbqo$a;-><init>(Landroid/os/Looper;Lbqo;)V

    iput-object v0, p0, Lbqo;->f:Landroid/os/Handler;

    .line 96
    const-string/jumbo v0, "geo_mock_locations_file"

    .line 97
    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 98
    const-string/jumbo v1, "geo_mock_locations"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    const-string/jumbo v1, "geo_mock_locations"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbqo;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lbqo;->d:Ljava/util/List;

    .line 104
    :goto_0
    const-string/jumbo v1, "geo_mock_null_location"

    .line 105
    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbqo;->h:Z

    .line 106
    return-void

    .line 101
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lbqo;->d:Ljava/util/List;

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lbqo;
    .locals 2

    .prologue
    .line 79
    const-class v1, Lbqo;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbqo;->a:Lbqo;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lbqo;

    invoke-direct {v0, p0}, Lbqo;-><init>(Landroid/content/Context;)V

    sput-object v0, Lbqo;->a:Lbqo;

    .line 81
    const-class v0, Lbqo;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 83
    :cond_0
    sget-object v0, Lbqo;->a:Lbqo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 112
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 113
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-ge v0, v3, :cond_4

    .line 115
    :try_start_1
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 116
    new-instance v4, Landroid/location/Location;

    const-string/jumbo v5, "Source"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 117
    const-string/jumbo v5, "Latitude"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/location/Location;->setLatitude(D)V

    .line 118
    const-string/jumbo v5, "Longitude"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/location/Location;->setLongitude(D)V

    .line 119
    const-string/jumbo v5, "Timestamp"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/location/Location;->setTime(J)V

    .line 121
    const-string/jumbo v5, "H-Accuracy"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 122
    const-string/jumbo v5, "H-Accuracy"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    double-to-float v5, v6

    invoke-virtual {v4, v5}, Landroid/location/Location;->setAccuracy(F)V

    .line 124
    :cond_0
    const-string/jumbo v5, "Altitude"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 125
    const-string/jumbo v5, "Altitude"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/location/Location;->setAltitude(D)V

    .line 127
    :cond_1
    const-string/jumbo v5, "Bearing"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 128
    const-string/jumbo v5, "Bearing"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    double-to-float v5, v6

    invoke-virtual {v4, v5}, Landroid/location/Location;->setBearing(F)V

    .line 130
    :cond_2
    const-string/jumbo v5, "Speed"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 131
    const-string/jumbo v5, "Speed"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    double-to-float v3, v6

    invoke-virtual {v4, v3}, Landroid/location/Location;->setSpeed(F)V

    .line 134
    :cond_3
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 113
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 139
    :catch_0
    move-exception v0

    .line 142
    :cond_4
    return-object v1

    .line 135
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method private i()Lbqn;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lbqo;->c:Lbqn;

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lbqo;->b:Landroid/content/Context;

    invoke-static {v0}, Lbqn;->a(Landroid/content/Context;)Lbqn;

    move-result-object v0

    iput-object v0, p0, Lbqo;->c:Lbqn;

    .line 316
    :cond_0
    iget-object v0, p0, Lbqo;->c:Lbqn;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    .line 149
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 150
    iget-object v0, p0, Lbqo;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 151
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 153
    :try_start_0
    const-string/jumbo v4, "Latitude"

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 154
    const-string/jumbo v4, "Longitude"

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 155
    const-string/jumbo v4, "Timestamp"

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 156
    const-string/jumbo v4, "Source"

    invoke-virtual {v0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 158
    invoke-virtual {v0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 159
    const-string/jumbo v4, "H-Accuracy"

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v5

    float-to-double v6, v5

    invoke-virtual {v3, v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 161
    :cond_0
    invoke-virtual {v0}, Landroid/location/Location;->hasAltitude()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 162
    const-string/jumbo v4, "Altitude"

    invoke-virtual {v0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 164
    :cond_1
    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 165
    const-string/jumbo v4, "Bearing"

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v5

    float-to-double v6, v5

    invoke-virtual {v3, v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 167
    :cond_2
    invoke-virtual {v0}, Landroid/location/Location;->hasSpeed()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 168
    const-string/jumbo v4, "Speed"

    invoke-virtual {v0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    float-to-double v6, v0

    invoke-virtual {v3, v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :cond_3
    :goto_1
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 176
    :cond_4
    iget-object v0, p0, Lbqo;->b:Landroid/content/Context;

    const-string/jumbo v2, "geo_mock_locations_file"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "geo_mock_locations"

    .line 177
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 178
    return-void

    .line 170
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public declared-synchronized a(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 190
    monitor-enter p0

    if-nez p1, :cond_1

    .line 198
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 193
    :cond_1
    :try_start_0
    iget-object v0, p0, Lbqo;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    invoke-direct {p0}, Lbqo;->i()Lbqn;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbqn;->a(Z)V

    .line 195
    iget-object v0, p0, Lbqo;->g:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 196
    iput-object p1, p0, Lbqo;->g:Landroid/location/Location;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Landroid/location/Location;Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 231
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbqo;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 232
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 233
    iget-object v1, p0, Lbqo;->d:Ljava/util/List;

    invoke-interface {v1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    :cond_0
    monitor-exit p0

    return-void

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 298
    monitor-enter p0

    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 299
    iget-object v0, p0, Lbqo;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 310
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 301
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lbqo;->h:Z

    if-nez v0, :cond_2

    .line 302
    iget v0, p0, Lbqo;->e:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lbqo;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lbqo;->e:I

    .line 303
    iget-object v0, p0, Lbqo;->d:Ljava/util/List;

    iget v1, p0, Lbqo;->e:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 304
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 305
    invoke-direct {p0}, Lbqo;->i()Lbqn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbqn;->a(Landroid/location/Location;)V

    .line 306
    iput-object v0, p0, Lbqo;->g:Landroid/location/Location;

    .line 308
    :cond_2
    iget-object v0, p0, Lbqo;->f:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Z)V
    .locals 3

    .prologue
    .line 242
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbqo;->h:Z

    if-eq v0, p1, :cond_1

    .line 243
    invoke-direct {p0}, Lbqo;->i()Lbqn;

    move-result-object v0

    .line 244
    if-eqz p1, :cond_2

    .line 245
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbqn;->a(Z)V

    .line 246
    invoke-virtual {v0}, Lbqn;->g()V

    .line 252
    :cond_0
    :goto_0
    iget-object v0, p0, Lbqo;->b:Landroid/content/Context;

    const-string/jumbo v1, "geo_mock_locations_file"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "geo_mock_null_location"

    .line 253
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 254
    iput-boolean p1, p0, Lbqo;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    :cond_1
    monitor-exit p0

    return-void

    .line 247
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lbqo;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 250
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbqn;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 2

    .prologue
    .line 219
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbqo;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 220
    invoke-direct {p0}, Lbqo;->i()Lbqn;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbqn;->a(Z)V

    .line 221
    invoke-virtual {p0}, Lbqo;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    monitor-exit p0

    return-void

    .line 219
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 209
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbqo;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 210
    iget-object v0, p0, Lbqo;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbqo;->h:Z

    if-nez v0, :cond_0

    .line 211
    invoke-direct {p0}, Lbqo;->i()Lbqn;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbqn;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    :cond_0
    monitor-exit p0

    return-void

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()Z
    .locals 1

    .prologue
    .line 238
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbqo;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Z
    .locals 1

    .prologue
    .line 262
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbqo;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269
    iget-object v0, p0, Lbqo;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized f()Landroid/location/Location;
    .locals 1

    .prologue
    .line 274
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbqo;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 275
    const/4 v0, 0x0

    .line 277
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lbqo;->g:Landroid/location/Location;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g()V
    .locals 4

    .prologue
    .line 283
    const/4 v0, -0x1

    iput v0, p0, Lbqo;->e:I

    .line 284
    iget-object v0, p0, Lbqo;->f:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 285
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lbqo;->f:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 290
    return-void
.end method
