.class public Lcom/github/mikephil/charting/components/c;
.super Lcom/github/mikephil/charting/components/b;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lhk;

.field private c:Landroid/graphics/Paint$Align;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/github/mikephil/charting/components/b;-><init>()V

    .line 16
    const-string/jumbo v0, "Description Label"

    iput-object v0, p0, Lcom/github/mikephil/charting/components/c;->a:Ljava/lang/String;

    .line 26
    sget-object v0, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    iput-object v0, p0, Lcom/github/mikephil/charting/components/c;->c:Landroid/graphics/Paint$Align;

    .line 32
    const/high16 v0, 0x41000000    # 8.0f

    invoke-static {v0}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/components/c;->z:F

    .line 33
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/github/mikephil/charting/components/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Lhk;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/github/mikephil/charting/components/c;->b:Lhk;

    return-object v0
.end method

.method public c()Landroid/graphics/Paint$Align;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/github/mikephil/charting/components/c;->c:Landroid/graphics/Paint$Align;

    return-object v0
.end method
