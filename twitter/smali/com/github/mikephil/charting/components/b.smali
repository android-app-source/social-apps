.class public abstract Lcom/github/mikephil/charting/components/b;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected A:I

.field protected v:Z

.field protected w:F

.field protected x:F

.field protected y:Landroid/graphics/Typeface;

.field protected z:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, 0x40a00000    # 5.0f

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/github/mikephil/charting/components/b;->v:Z

    .line 24
    iput v1, p0, Lcom/github/mikephil/charting/components/b;->w:F

    .line 29
    iput v1, p0, Lcom/github/mikephil/charting/components/b;->x:F

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/github/mikephil/charting/components/b;->y:Landroid/graphics/Typeface;

    .line 39
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/github/mikephil/charting/components/b;->z:F

    .line 44
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/github/mikephil/charting/components/b;->A:I

    .line 49
    return-void
.end method


# virtual methods
.method public b(I)V
    .locals 0

    .prologue
    .line 142
    iput p1, p0, Lcom/github/mikephil/charting/components/b;->A:I

    .line 143
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 162
    iput-boolean p1, p0, Lcom/github/mikephil/charting/components/b;->v:Z

    .line 163
    return-void
.end method

.method public e(F)V
    .locals 1

    .prologue
    .line 88
    invoke-static {p1}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/components/b;->x:F

    .line 89
    return-void
.end method

.method public s()F
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/github/mikephil/charting/components/b;->w:F

    return v0
.end method

.method public t()F
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/github/mikephil/charting/components/b;->x:F

    return v0
.end method

.method public u()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/github/mikephil/charting/components/b;->y:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public v()F
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/github/mikephil/charting/components/b;->z:F

    return v0
.end method

.method public w()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/github/mikephil/charting/components/b;->A:I

    return v0
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/b;->v:Z

    return v0
.end method
