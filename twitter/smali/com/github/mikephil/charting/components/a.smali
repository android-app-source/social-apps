.class public abstract Lcom/github/mikephil/charting/components/a;
.super Lcom/github/mikephil/charting/components/b;
.source "Twttr"


# instance fields
.field private B:I

.field private C:F

.field private D:I

.field private E:F

.field private F:I

.field private G:Landroid/graphics/DashPathEffect;

.field private H:Landroid/graphics/DashPathEffect;

.field protected a:Lfd;

.field public b:[F

.field public c:[F

.field public d:I

.field public e:I

.field protected f:F

.field protected g:Z

.field protected h:Z

.field protected i:Z

.field protected j:Z

.field protected k:Z

.field protected l:Z

.field protected m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/github/mikephil/charting/components/LimitLine;",
            ">;"
        }
    .end annotation
.end field

.field protected n:Z

.field protected o:F

.field protected p:F

.field protected q:Z

.field protected r:Z

.field public s:F

.field public t:F

.field public u:F


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/high16 v5, 0x40a00000    # 5.0f

    const/4 v4, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 153
    invoke-direct {p0}, Lcom/github/mikephil/charting/components/b;-><init>()V

    .line 27
    const v0, -0x777778

    iput v0, p0, Lcom/github/mikephil/charting/components/a;->B:I

    .line 29
    iput v3, p0, Lcom/github/mikephil/charting/components/a;->C:F

    .line 31
    const v0, -0x777778

    iput v0, p0, Lcom/github/mikephil/charting/components/a;->D:I

    .line 33
    iput v3, p0, Lcom/github/mikephil/charting/components/a;->E:F

    .line 38
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/github/mikephil/charting/components/a;->b:[F

    .line 43
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/github/mikephil/charting/components/a;->c:[F

    .line 58
    const/4 v0, 0x6

    iput v0, p0, Lcom/github/mikephil/charting/components/a;->F:I

    .line 63
    iput v3, p0, Lcom/github/mikephil/charting/components/a;->f:F

    .line 71
    iput-boolean v1, p0, Lcom/github/mikephil/charting/components/a;->g:Z

    .line 76
    iput-boolean v1, p0, Lcom/github/mikephil/charting/components/a;->h:Z

    .line 81
    iput-boolean v4, p0, Lcom/github/mikephil/charting/components/a;->i:Z

    .line 86
    iput-boolean v4, p0, Lcom/github/mikephil/charting/components/a;->j:Z

    .line 91
    iput-boolean v4, p0, Lcom/github/mikephil/charting/components/a;->k:Z

    .line 93
    iput-boolean v1, p0, Lcom/github/mikephil/charting/components/a;->l:Z

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/github/mikephil/charting/components/a;->G:Landroid/graphics/DashPathEffect;

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/github/mikephil/charting/components/a;->H:Landroid/graphics/DashPathEffect;

    .line 113
    iput-boolean v1, p0, Lcom/github/mikephil/charting/components/a;->n:Z

    .line 118
    iput v2, p0, Lcom/github/mikephil/charting/components/a;->o:F

    .line 123
    iput v2, p0, Lcom/github/mikephil/charting/components/a;->p:F

    .line 128
    iput-boolean v1, p0, Lcom/github/mikephil/charting/components/a;->q:Z

    .line 133
    iput-boolean v1, p0, Lcom/github/mikephil/charting/components/a;->r:Z

    .line 138
    iput v2, p0, Lcom/github/mikephil/charting/components/a;->s:F

    .line 143
    iput v2, p0, Lcom/github/mikephil/charting/components/a;->t:F

    .line 148
    iput v2, p0, Lcom/github/mikephil/charting/components/a;->u:F

    .line 154
    const/high16 v0, 0x41200000    # 10.0f

    invoke-static {v0}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/components/a;->z:F

    .line 155
    invoke-static {v5}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/components/a;->w:F

    .line 156
    invoke-static {v5}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/components/a;->x:F

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/components/a;->m:Ljava/util/List;

    .line 158
    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 469
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/components/a;->b:[F

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 470
    :cond_0
    const-string/jumbo v0, ""

    .line 472
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/github/mikephil/charting/components/a;->p()Lfd;

    move-result-object v0

    iget-object v1, p0, Lcom/github/mikephil/charting/components/a;->b:[F

    aget v1, v1, p1

    invoke-interface {v0, v1, p0}, Lfd;->a(FLcom/github/mikephil/charting/components/a;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 670
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/github/mikephil/charting/components/a;->q:Z

    .line 671
    iput p1, p0, Lcom/github/mikephil/charting/components/a;->t:F

    .line 672
    iget v0, p0, Lcom/github/mikephil/charting/components/a;->s:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/components/a;->u:F

    .line 673
    return-void
.end method

.method public a(FF)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 718
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/a;->q:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/github/mikephil/charting/components/a;->t:F

    .line 719
    :goto_0
    iget-boolean v1, p0, Lcom/github/mikephil/charting/components/a;->r:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/github/mikephil/charting/components/a;->s:F

    .line 722
    :goto_1
    sub-float v2, v1, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 725
    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 726
    add-float/2addr v1, v4

    .line 727
    sub-float/2addr v0, v4

    .line 730
    :cond_0
    iput v0, p0, Lcom/github/mikephil/charting/components/a;->t:F

    .line 731
    iput v1, p0, Lcom/github/mikephil/charting/components/a;->s:F

    .line 734
    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/components/a;->u:F

    .line 735
    return-void

    .line 718
    :cond_1
    iget v0, p0, Lcom/github/mikephil/charting/components/a;->o:F

    sub-float v0, p1, v0

    goto :goto_0

    .line 719
    :cond_2
    iget v1, p0, Lcom/github/mikephil/charting/components/a;->p:F

    add-float/2addr v1, p2

    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/github/mikephil/charting/components/a;->i:Z

    .line 167
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 175
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/a;->i:Z

    return v0
.end method

.method public b(F)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 682
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/components/a;->a(F)V

    .line 683
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 184
    iput-boolean p1, p0, Lcom/github/mikephil/charting/components/a;->j:Z

    .line 185
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/a;->j:Z

    return v0
.end method

.method public c(F)V
    .locals 0

    .prologue
    .line 750
    iput p1, p0, Lcom/github/mikephil/charting/components/a;->o:F

    .line 751
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 293
    iput-boolean p1, p0, Lcom/github/mikephil/charting/components/a;->k:Z

    .line 294
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/a;->l:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/github/mikephil/charting/components/a;->d:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lcom/github/mikephil/charting/components/a;->B:I

    return v0
.end method

.method public d(F)V
    .locals 0

    .prologue
    .line 766
    iput p1, p0, Lcom/github/mikephil/charting/components/a;->p:F

    .line 767
    return-void
.end method

.method public e()F
    .locals 1

    .prologue
    .line 245
    iget v0, p0, Lcom/github/mikephil/charting/components/a;->E:F

    return v0
.end method

.method public f()F
    .locals 1

    .prologue
    .line 265
    iget v0, p0, Lcom/github/mikephil/charting/components/a;->C:F

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 283
    iget v0, p0, Lcom/github/mikephil/charting/components/a;->D:I

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/a;->k:Z

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 345
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/a;->h:Z

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/github/mikephil/charting/components/a;->F:I

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 361
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/a;->g:Z

    return v0
.end method

.method public l()F
    .locals 1

    .prologue
    .line 378
    iget v0, p0, Lcom/github/mikephil/charting/components/a;->f:F

    return v0
.end method

.method public m()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/github/mikephil/charting/components/LimitLine;",
            ">;"
        }
    .end annotation

    .prologue
    .line 430
    iget-object v0, p0, Lcom/github/mikephil/charting/components/a;->m:Ljava/util/List;

    return-object v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 444
    iget-boolean v0, p0, Lcom/github/mikephil/charting/components/a;->n:Z

    return v0
.end method

.method public o()Ljava/lang/String;
    .locals 5

    .prologue
    .line 455
    const-string/jumbo v1, ""

    .line 457
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/github/mikephil/charting/components/a;->b:[F

    array-length v0, v0

    if-ge v2, v0, :cond_0

    .line 458
    invoke-virtual {p0, v2}, Lcom/github/mikephil/charting/components/a;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 460
    if-eqz v0, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 457
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 464
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public p()Lfd;
    .locals 2

    .prologue
    .line 499
    iget-object v0, p0, Lcom/github/mikephil/charting/components/a;->a:Lfd;

    if-nez v0, :cond_1

    .line 500
    new-instance v0, Lfa;

    iget v1, p0, Lcom/github/mikephil/charting/components/a;->e:I

    invoke-direct {v0, v1}, Lfa;-><init>(I)V

    iput-object v0, p0, Lcom/github/mikephil/charting/components/a;->a:Lfd;

    .line 506
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/github/mikephil/charting/components/a;->a:Lfd;

    return-object v0

    .line 501
    :cond_1
    iget-object v0, p0, Lcom/github/mikephil/charting/components/a;->a:Lfd;

    invoke-interface {v0}, Lfd;->a()I

    move-result v0

    iget v1, p0, Lcom/github/mikephil/charting/components/a;->e:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/components/a;->a:Lfd;

    instance-of v0, v0, Lfa;

    if-eqz v0, :cond_0

    .line 503
    new-instance v0, Lfa;

    iget v1, p0, Lcom/github/mikephil/charting/components/a;->e:I

    invoke-direct {v0, v1}, Lfa;-><init>(I)V

    iput-object v0, p0, Lcom/github/mikephil/charting/components/a;->a:Lfd;

    goto :goto_0
.end method

.method public q()Landroid/graphics/DashPathEffect;
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lcom/github/mikephil/charting/components/a;->H:Landroid/graphics/DashPathEffect;

    return-object v0
.end method

.method public r()Landroid/graphics/DashPathEffect;
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lcom/github/mikephil/charting/components/a;->G:Landroid/graphics/DashPathEffect;

    return-object v0
.end method
